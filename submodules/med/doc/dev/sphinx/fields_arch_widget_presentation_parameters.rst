:orphan:

.. _fields_arch_widget_presentation_parameters_page:

*****************************************************
Plot3D presentation's parameters panel implementation
*****************************************************

Functions to access the cutting plane normal
--------------------------------------------

.. literalinclude:: ../../../src/MEDCalc/gui/dialogs/WidgetPresentationParameters.cxx
   :language: cpp
   :start-after: begin of setNormal
   :end-before: end of setNormal

.. literalinclude:: ../../../src/MEDCalc/gui/dialogs/WidgetPresentationParameters.cxx
   :language: cpp
   :start-after: begin of getNormal
   :end-before: end of getNormal

Functions to access the cutting plane position
----------------------------------------------

.. literalinclude:: ../../../src/MEDCalc/gui/dialogs/WidgetPresentationParameters.cxx
   :language: cpp
   :start-after: begin of setNormal
   :end-before: end of setNormal

.. literalinclude:: ../../../src/MEDCalc/gui/dialogs/WidgetPresentationParameters.cxx
   :language: cpp
   :start-after: begin of getNormal
   :end-before: end of getNormal

Signal-slot connections
-----------------------

.. literalinclude:: ../../../src/MEDCalc/gui/dialogs/WidgetPresentationParameters.cxx
   :language: cpp
   :start-after: begin of normal and plane connections
   :end-before: end of normal and plane connections

.. _fields_index_page:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FIELDS module: User guide for simplified MED visualisation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. toctree::

  fields_introduction.rst
  fields_preferences.rst
  fields_data_source.rst
  fields_view_modes.rst
  fields_presentations.rst

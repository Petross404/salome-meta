.. _fields_managing_presentations_page:

**********************
Managing presentations
**********************

To delete a presentation,

* Select it in the **Object Browser** and
* Choose **Presentations > Delete presentation** menu.

To dump Python script that generates given presentation into the terminal,

* Select presentation in the **Object Browser** and
* Choose **Presentations > Dump pipeline** menu.

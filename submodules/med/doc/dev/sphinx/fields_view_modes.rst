.. _fields_view_modes_page:

**********
View Modes
**********

View mode specifies a way presentations are created. The following view modes are supported:

* **Replace**: a newly created presentation is published in the Object Browser and displayed in the active 3D Viewer.
  All previously created presentations are deleted.
* **Overlap**: a newly created presentation is added into Object Browser and displayed in the active 3D Viewer.
  All previously created presentations are kept as is.
* **New Layout**: a newly created presentation is added into Object Browser. New 3D Viewer (layout) is created and
  presentation is displayed in this new 3D Viewer. All previously created presentations are kept as is.
* **Split**: a newly created presentation is added into Object Browser. An active 3D Viewer (layout) is split,
  and a new 3D Viewer is added to the layout. Newly created presentation is displayed in this new 3D Viewer;
  all previously created presentations are kept as is.

#  Check availability of MULTIPR module binary distribution
#
#  Author : Olivier LE ROUX (CS, 2006)
#------------------------------------------------------------

AC_DEFUN([CHECK_MULTIPR],[

AC_CHECKING(for Multipr)

MULTIPR_LDFLAGS=""
MULTIPR_CXXFLAGS=""

Multipr_ok=no

AC_ARG_WITH(multipr,
	    --with-multipr=DIR root directory path of MULTIPR installation,
	    MULTIPR_DIR="$withval",MULTIPR_DIR="")

if test "x$MULTIPR_DIR" = "x" ; then

# no --with-gui-dir option used

  if test "x$MULTIPR_ROOT_DIR" != "x" ; then

    # MULTIPR_ROOT_DIR environment variable defined
    MULTIPR_DIR=$MULTIPR_ROOT_DIR

  else

    # search multipr binaries in PATH variable
    AC_PATH_PROG(TEMP, libMULTIPR.so)
    if test "x$TEMP" != "x" ; then
      MULTIPR_BIN_DIR=`dirname $TEMP`
      MULTIPR_DIR=`dirname $MULTIPR_BIN_DIR`
    fi

  fi
#
fi

if test -f ${MULTIPR_DIR}/lib/salome/libMULTIPR.so  ; then
  Multipr_ok=yes
  AC_MSG_RESULT(Using MULTIPR distribution in ${MULTIPR_DIR})

  if test "x$MULTIPR_ROOT_DIR" == "x" ; then
    MULTIPR_ROOT_DIR=${MULTIPR_DIR}
  fi
  AC_SUBST(MULTIPR_ROOT_DIR)
  
  MULTIPR_LDFLAGS=-L${MULTIPR_DIR}/lib${LIB_LOCATION_SUFFIX}/salome
  MULTIPR_CXXFLAGS=-I${MULTIPR_DIR}/include/salome

  AC_SUBST(MULTIPR_LDFLAGS)
  AC_SUBST(MULTIPR_CXXFLAGS)
   
else
  AC_MSG_WARN("Cannot find compiled MULTIPR distribution")
fi
  
AC_MSG_RESULT(for MULTIPR: $Multipr_ok)
 
])dnl
 

// Project MULTIPR, IOLS WP1.2.1 - EDF/CS
// Partitioning/decimation module for the SALOME v3.2 platform

/**
* \file    MULTIPR_GUI_Dlg.cxx
*
* \brief   see MULTIPR_GUI_Dlg.h
*
* \author  Olivier LE ROUX - CS, Virtual Reality Dpt
* 
* \date    01/2007
*/

//*****************************************************************************
// Includes section
//*****************************************************************************

#include "MULTIPR_GUI_Dlg.h"
#include "MULTIPR_GUI.h"

// Salome Includes
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SalomeApp_Application.h>
#include <SalomeApp_CheckFileDlg.h>
#include <LightApp_SelectionMgr.h>

#include <SALOME_LifeCycleCORBA.hxx>

// QT Includes
#include <qapplication.h>
#include <qinputdialog.h>
#include <qlayout.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qvbox.h>
#include <qbuttongroup.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qvariant.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

// MED include
extern "C"
{
    #include "med.h"
}

using namespace std;


MULTIPR_GUI_Partition1Dlg::MULTIPR_GUI_Partition1Dlg(MULTIPR_GUI* theModule) : 
    QDialog(
        theModule->application()->desktop(), 
        0, 
        false, 
        WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu)
{
    mModule = theModule;
    
    buttonGroupProcess = new QButtonGroup( this, "buttonGroupProcess" );
    buttonGroupProcess->setGeometry( QRect( 10, 110, 450, 60 ) );

    pushButtonOK = new QPushButton( buttonGroupProcess, "pushButtonOK" );
    pushButtonOK->setGeometry( QRect( 10, 10, 110, 41 ) );
    
    pushButtonCancel = new QPushButton( buttonGroupProcess, "pushButtonCancel" );
    pushButtonCancel->setGeometry( QRect( 321, 10, 110, 41 ) );

    buttonGroupSelectMesh = new QButtonGroup( this, "buttonGroupSelectMesh" );
    buttonGroupSelectMesh->setGeometry( QRect( 10, 10, 450, 91 ) );

    comboBoxSelectMesh = new QComboBox( FALSE, buttonGroupSelectMesh, "comboBoxSelectMesh" );
    comboBoxSelectMesh->setGeometry( QRect( 160, 30, 280, 40 ) );
    MULTIPR_ORB::string_array* listMeshes = theModule->getMULTIPRObj()->getMeshes();
    for (int i=0; i<listMeshes->length() ; i++)
    {
        const char* strItem = (*listMeshes)[i];
        comboBoxSelectMesh->insertItem(strItem);
    }
    comboBoxSelectMesh->setEditable(false);

    textLabelSelectMesh = new QLabel( buttonGroupSelectMesh, "textLabelSelectMesh" );
    textLabelSelectMesh->setGeometry( QRect( 20, 30, 110, 40 ) );
    
    setCaption( tr( "Extract groups from sequential MED file" ) );
    buttonGroupProcess->setTitle( QString::null );
    pushButtonCancel->setText( tr( "Cancel" ) );
    pushButtonOK->setText( tr("OK") );
    buttonGroupSelectMesh->setTitle( tr( "Select mesh" ) );
    textLabelSelectMesh->setText( tr( "Mesh name" ) );
    
    resize( QSize(471, 185).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    
    connect(pushButtonOK,     SIGNAL(clicked()), this, SLOT(accept()));
    connect(pushButtonCancel, SIGNAL(clicked()), this, SLOT(reject()));
}


/*
*  Destroys the object and frees any allocated resources
*/
MULTIPR_GUI_Partition1Dlg::~MULTIPR_GUI_Partition1Dlg()
{
    // no need to delete child widgets, Qt does it all for us
}


void MULTIPR_GUI_Partition1Dlg::accept()
{
    const char* meshName = comboBoxSelectMesh->currentText().latin1();
    
    try
    {
        mModule->getMULTIPRObj()->setMesh(meshName);
        
        QApplication::setOverrideCursor(Qt::waitCursor);
        mModule->getMULTIPRObj()->partitionneDomaine();
        QApplication::restoreOverrideCursor();
        
    }
    catch(...)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Import MED file error", 
            "Unable to set mesh", 
            tr("OK") );
    }

    QDialog::accept();
}


void MULTIPR_GUI_Partition1Dlg::reject()
{
    QDialog::reject();
}


MULTIPR_GUI_Partition2Dlg::MULTIPR_GUI_Partition2Dlg(MULTIPR_GUI* theModule) : 
    QDialog(
        theModule->application()->desktop(), 
        0, 
        false, 
        WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu)
{
    mModule = theModule;
    
    buttonGroupSplitParameters = new QButtonGroup( this, "buttonGroupSplitParameters" );
    buttonGroupSplitParameters->setGeometry( QRect( 10, 10, 380, 140 ) );

    textLabelSelectNbParts = new QLabel( buttonGroupSplitParameters, "textLabelSelectNbParts" );
    textLabelSelectNbParts->setGeometry( QRect( 30, 30, 160, 31 ) );

    textLabelSelectSplitter = new QLabel( buttonGroupSplitParameters, "textLabelSelectSplitter" );
    textLabelSelectSplitter->setGeometry( QRect( 30, 80, 111, 31 ) );

    comboBoxSelectSplitter = new QComboBox( FALSE, buttonGroupSplitParameters, "comboBoxSelectSplitter" );
    comboBoxSelectSplitter->setGeometry( QRect( 210, 80, 150, 40 ) );
    comboBoxSelectSplitter->insertItem("METIS");
    comboBoxSelectSplitter->insertItem("SCOTCH");
    comboBoxSelectSplitter->setEditable(false);

    spinBoxNbParts = new QSpinBox( buttonGroupSplitParameters, "spinBoxNbParts" );
    spinBoxNbParts->setGeometry( QRect( 210, 30, 150, 30 ) );
    spinBoxNbParts->setMaxValue( 1000 );
    spinBoxNbParts->setMinValue( 2 );
    spinBoxNbParts->setValue( 2 );

    buttonGroupProcess = new QButtonGroup( this, "buttonGroupProcess" );
    buttonGroupProcess->setGeometry( QRect( 10, 160, 380, 60 ) );

    pushButtonOK = new QPushButton( buttonGroupProcess, "pushButtonOK" );
    pushButtonOK->setGeometry( QRect( 10, 10, 110, 41 ) );

    pushButtonCancel = new QPushButton( buttonGroupProcess, "pushButtonCancel" );
    pushButtonCancel->setGeometry( QRect( 250, 10, 110, 41 ) );
    
    setCaption( tr( "Split selected part" ) );
    buttonGroupSplitParameters->setTitle( tr( "Split parameters" ) );
    textLabelSelectNbParts->setText( tr( "Number of sub-parts" ) );
    textLabelSelectSplitter->setText( tr( "Splitter" ) );
    buttonGroupProcess->setTitle( QString::null );
    pushButtonOK->setText( tr("OK") );
    pushButtonCancel->setText( tr( "Cancel" ) );
    
    resize( QSize(403, 234).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    
    connect(pushButtonOK,     SIGNAL(clicked()), this, SLOT(accept()));
    connect(pushButtonCancel, SIGNAL(clicked()), this, SLOT(reject()));
}


/*
*  Destroys the object and frees any allocated resources
*/
MULTIPR_GUI_Partition2Dlg::~MULTIPR_GUI_Partition2Dlg()
{
    // no need to delete child widgets, Qt does it all for us
}


void MULTIPR_GUI_Partition2Dlg::accept()
{
    const char* strSplitter = comboBoxSelectSplitter->currentText().latin1();
    int nbParts = spinBoxNbParts->value();
    int partitionner = -1;
    if (strcmp(strSplitter, "METIS") == 0)
    {
        partitionner = 0;
    }
    else if (strcmp(strSplitter, "SCOTCH") == 0)
    {
        partitionner = 1;
    }
    
    QApplication::setOverrideCursor(Qt::waitCursor);
    
    try
    {
        const QStringList& partsList = mModule->getSelectedParts();
        for (QStringList::const_iterator it = partsList.begin(), last = partsList.end(); it != last; it++)
        {
            const QString& partName = (*it);
            //cout << "Split " << partName.latin1() << " #parts=" << nbParts << " splitter=" << strSplitter << endl;
            mModule->getMULTIPRObj()->partitionneGroupe(partName.latin1(), nbParts, partitionner);
        }
        
    }
    catch(...)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Split error", 
            "Error while splitting selected part(s)", 
            tr("OK") );
    }
    
    QApplication::restoreOverrideCursor();
    QDialog::accept();
}


void MULTIPR_GUI_Partition2Dlg::reject()
{
    QDialog::reject();
}


MULTIPR_GUI_DecimateDlg::MULTIPR_GUI_DecimateDlg(MULTIPR_GUI* theModule) :
    QDialog(
    theModule->application()->desktop(), 
        0, 
        false, 
        WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu)
{
    mModule = theModule;
    buttonGroupSelectField = new QButtonGroup( this, "buttonGroupSelectField" );
    buttonGroupSelectField->setGeometry( QRect( 10, 10, 710, 140 ) );

    textLabelSelectFieldName = new QLabel( buttonGroupSelectField, "textLabelSelectFieldName" );
    textLabelSelectFieldName->setGeometry( QRect( 30, 30, 141, 31 ) );

    textLabelSelectFieldIteration = new QLabel( buttonGroupSelectField, "textLabelSelectFieldIteration" );
    textLabelSelectFieldIteration->setGeometry( QRect( 30, 80, 111, 31 ) );

    const QStringList& partsList = mModule->getSelectedParts();
    // Lets get the fields !.
    QString allParts = partsList.join("|");
    MULTIPR_ORB::string_array* listFields = theModule->getMULTIPRObj()->getFields(allParts.latin1());
    int maxIteration = 0;
    int i, j;
    for (i = 0 ; i < listFields->length() ; ++i)
    {
        char* strItem = (*listFields)[i];
        for (j = 0; strItem[j] && strItem[j] != ' '; ++j);
        strItem[j] = 0;
        CORBA::Long nbIteration = theModule->getMULTIPRObj()->getTimeStamps(allParts.latin1(), strItem);
        strItem[j] = ' ';
        if (nbIteration > maxIteration) 
        {
            maxIteration = nbIteration;
        }
    }

    comboBoxSelectFieldIteration = new QComboBox( FALSE, buttonGroupSelectField, "comboBoxSelectFieldIteration" );
    comboBoxSelectFieldIteration->setGeometry( QRect( 150, 80, 540, 40 ) );
    for (int i=1 ; i<=maxIteration ; i++)
    {
        comboBoxSelectFieldIteration->insertItem(QString::number(i));
    }

    comboBoxSelectFieldName = new QComboBox( FALSE, buttonGroupSelectField, "comboBoxSelectFieldName" );
    comboBoxSelectFieldName->setGeometry( QRect( 150, 30, 540, 40 ) );
    for (int i=0 ; i<listFields->length() ; i++)
    {
        const char* strItem = (*listFields)[i];
        comboBoxSelectFieldName->insertItem(strItem);
    }
    comboBoxSelectFieldName->setEditable(false);
    QToolTip::add( comboBoxSelectFieldName, tr( "only scalar fields are listed (multi-component fields are not displayed)" ) );

    buttonGroupSelectFilter = new QButtonGroup( this, "buttonGroupSelectFilter" );
    buttonGroupSelectFilter->setGeometry( QRect( 10, 160, 710, 90 ) );

    textLabelSelectFilter = new QLabel( buttonGroupSelectFilter, "textLabelSelectFilter" );
    textLabelSelectFilter->setGeometry( QRect( 30, 30, 101, 31 ) );

    comboBoxSelectFilter = new QComboBox( FALSE, buttonGroupSelectFilter, "comboBoxSelectFilter" );
    comboBoxSelectFilter->setGeometry( QRect( 150, 30, 540, 40 ) );
    comboBoxSelectFilter->insertItem("Filtre_GradientMoyen");
	comboBoxSelectFilter->insertItem("Filtre_Direct");

    buttonGroupParameters = new QButtonGroup( this, "buttonGroupParameters" );
    buttonGroupParameters->setGeometry( QRect( 10, 260, 710, 210 ) );

    textLabelTMed = new QLabel( buttonGroupParameters, "textLabelTMed" );
    textLabelTMed->setGeometry( QRect( 20, 40, 242, 30 ) );

    textLabelTLow = new QLabel( buttonGroupParameters, "textLabelTLow" );
    textLabelTLow->setGeometry( QRect( 20, 80, 208, 30 ) );

    textLabelRadius = new QLabel( buttonGroupParameters, "textLabelRadius" );
    textLabelRadius->setGeometry( QRect( 20, 120, 211, 30 ) );

    textLabelBoxing = new QLabel( buttonGroupParameters, "textLabelBoxing" );
    textLabelBoxing->setGeometry( QRect( 20, 160, 241, 30 ) );

    lineEditTMed = new QLineEdit( buttonGroupParameters, "lineEditTMed" );
    lineEditTMed->setGeometry( QRect( 580, 40, 111, 30 ) );

    lineEditTLow = new QLineEdit( buttonGroupParameters, "lineEditTLow" );
    lineEditTLow->setGeometry( QRect( 580, 80, 111, 30 ) );

    lineEditRadius = new QLineEdit( buttonGroupParameters, "lineEditRadius" );
    lineEditRadius->setGeometry( QRect( 580, 120, 111, 30 ) );

    spinBoxBoxing = new QSpinBox( buttonGroupParameters, "spinBoxBoxing" );
    spinBoxBoxing->setGeometry( QRect( 580, 160, 111, 30 ) );
    spinBoxBoxing->setMaxValue( 200 );
    spinBoxBoxing->setMinValue( 2 );
    spinBoxBoxing->setValue( 100 );
    QToolTip::add( spinBoxBoxing, tr( "grid: number of cells along each axis" ) );

	infoGroup = new QButtonGroup(this, "infoGroup");
	infoGroup->setGeometry( QRect( 10, 480, 710, 60 ) );

	char* stats;
	int tmp;
	// Accumulator used to display the number of meshes in the current selection.
	int acum = 0;
    for (QStringList::const_iterator it = partsList.begin(), last = partsList.end(); it != last; it++)
    {
		const QString& partName = (*it);
		stats = theModule->getMULTIPRObj()->getMEDInfo(partName.latin1());
		sscanf(stats, "%d", &tmp);
		acum += tmp;
    }
	
	char buf[512];
	sprintf(buf, "%d", acum);
	textLabelInfo = new QLabel( infoGroup, "textLabelInfo");
	textLabelInfo->setText("Number of cells:");
    textLabelInfo->setGeometry( QRect( 20, 10, 242, 30 ) );	
	textLabelInfoValue = new QLabel( infoGroup, "textLabelInfoValue");
	textLabelInfoValue->setAlignment(AlignRight);
	textLabelInfoValue->setText(buf);
    textLabelInfoValue->setGeometry( QRect( 400, 10, 290, 30 ) );	
			
    buttonGroupProcess = new QButtonGroup( this, "buttonGroupProcess" );
    buttonGroupProcess->setGeometry( QRect( 10, 580, 710, 60 ) );

    pushButtonOK = new QPushButton( buttonGroupProcess, "pushButtonOK" );
    pushButtonOK->setGeometry( QRect( 10, 10, 110, 41 ) );
    
    pushButtonCancel = new QPushButton( buttonGroupProcess, "pushButtonCancel" );
    pushButtonCancel->setGeometry( QRect( 581, 10, 110, 41 ) );
    pushButtonCancel->setAccel( QKeySequence( tr( "Esc" ) ) );
    
    setCaption( tr( "Decimation" ) );
    buttonGroupSelectField->setTitle( tr( "Select field" ) );
    textLabelSelectFieldName->setText( tr( "Field name" ) );
    textLabelSelectFieldIteration->setText( tr( "Field iteration" ) );
    buttonGroupSelectFilter->setTitle( tr( "Select filter" ) );
    textLabelSelectFilter->setText( tr( "Filter name" ) );
    buttonGroupParameters->setTitle( tr( "Set parameters" ) );
    textLabelTMed->setText( tr( "Threshold for medium resolution" ) );
    textLabelTLow->setText( tr( "Threshold for low resolution" ) );
    textLabelRadius->setText( tr( "Radius (neighborhood)" ) );
    textLabelBoxing->setText( tr( "Acceleration structure (boxing)" ) );
    lineEditTMed->setText( tr( "0.1" ) );
    lineEditTLow->setText( tr( "0.2" ) );

    float defaultRadius = 0.5f;
    lineEditRadius->setText( QString::number(defaultRadius) );
    buttonGroupProcess->setTitle( QString::null );
    pushButtonCancel->setText( tr( "Cancel" ) );
    pushButtonOK->setText( tr("OK") );
    
    pushButtonThresholdAuto = new QPushButton( buttonGroupParameters, "pushButtonThresholdAuto" );
    pushButtonThresholdAuto->setGeometry( QRect( 520, 80, 50, 30 ) );
    QFont pushButtonThresholdAuto_font(  pushButtonThresholdAuto->font() );
    pushButtonThresholdAuto_font.setPointSize( 11 );
    pushButtonThresholdAuto->setFont( pushButtonThresholdAuto_font ); 
    pushButtonThresholdAuto->setText( tr( "auto" ) );
    QToolTip::add( pushButtonThresholdAuto, tr( "compute extremum for gradient (set medium=MIN and low=MAX)" ) );

    pushButtonRadiusAuto = new QPushButton( buttonGroupParameters, "pushButtonRadiusAuto" );
    pushButtonRadiusAuto->setGeometry( QRect( 520, 120, 50, 30 ) );
    QFont pushButtonRadiusAuto_font(  pushButtonRadiusAuto->font() );
    pushButtonRadiusAuto_font.setPointSize( 11 );
    pushButtonRadiusAuto->setFont( pushButtonRadiusAuto_font ); 
    pushButtonRadiusAuto->setText( tr( "auto" ) );
    QToolTip::add( pushButtonRadiusAuto, tr( "set radius automatically (average #neighbours equal to 8)" ) );
    
    resize( QSize(730, 654).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    
    connect(pushButtonOK,            SIGNAL(clicked()), this, SLOT(accept()));
    connect(pushButtonCancel,        SIGNAL(clicked()), this, SLOT(reject()));
    connect(pushButtonRadiusAuto,    SIGNAL(clicked()), this, SLOT(OnRadiusAuto()));
    connect(pushButtonThresholdAuto, SIGNAL(clicked()), this, SLOT(OnThresholdAuto()));
    connect(comboBoxSelectFilter,    SIGNAL(activated(const QString &)), this, SLOT(selectField(const QString &)));
}


/*
*  Destroys the object and frees any allocated resources
*/
MULTIPR_GUI_DecimateDlg::~MULTIPR_GUI_DecimateDlg()
{
    // no need to delete child widgets, Qt does it all for us
}


void MULTIPR_GUI_DecimateDlg::accept()
{
    const char*	strFieldNameTmp = comboBoxSelectFieldName->currentText().latin1();
    const char*	strFieldIt   = comboBoxSelectFieldIteration->currentText().latin1();
    int fieldIteration = atoi(strFieldIt);

    char		strFieldName[MED_TAILLE_NOM + 1];
    const		char* filterName = comboBoxSelectFilter->currentText().latin1();
    char		params[MULTIPR_GUI_MAX_PARAMS_LENGTH];
    int			i;

    // Get the field name.
    strncpy(strFieldName, strFieldNameTmp, MED_TAILLE_NOM);
    strFieldName[MED_TAILLE_NOM] = '\0';
    for (i = 0; strFieldName[i] && strFieldName[i] != ' '; ++i);
    strFieldName[i] = 0;

    // Fill the corresponding filter parameters string.
    if (strcmp(filterName, "Filtre_GradientMoyen") == 0)
    {
      if (this->setGradAvgFilterParams(params) == false)
      {
        return ;
      }
    }
    else if (strcmp(filterName, "Filtre_Direct") == 0)
    {
      if (this->setDirectFilterParams(params) == false)
      {
        return ;
      }
    }
    else
    {
      SUIT_MessageBox::error1(mModule->getAppli()->desktop(),
                              "Decimation error", 
                              "Unknown filter name.", 
                              tr("OK") );
    }

    QApplication::setOverrideCursor(Qt::waitCursor);

    MULTIPR_ORB::string_array*	list = NULL;
    try
    {
      const QStringList& partsList = mModule->getSelectedParts();
      for (QStringList::const_iterator it = partsList.begin(), last = partsList.end(); it != last; it++)
      {
        const QString& partName = (*it);

        list = mModule->getMULTIPRObj()->decimePartition(partName.latin1(),
                                                         strFieldName,
                                                         fieldIteration,
                                                         filterName,
                                                         params);
      }
    }
    catch(...)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation error",
            "Error while decimating selected part(s)",
            tr("OK") );
    }

    if (list != NULL && list->length() >= 4)
    {
      const char* rate = (*list)[list->length() - 1];
      SUIT_MessageBox::info1(mModule->getAppli()->desktop(),
                             "Compression rate", 
                             rate,
                             tr("OK") );
    }
    QApplication::restoreOverrideCursor();
    QDialog::accept();
}

bool MULTIPR_GUI_DecimateDlg::setGradAvgFilterParams(char* pParams)
{
    int boxing = spinBoxBoxing->value();

    double thresholdMed;
    int ret = sscanf(lineEditTMed->text().latin1(), "%lf", &thresholdMed);
    if ((ret != 1) || (thresholdMed <= 0.0f))
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Invalid medium threshold (should be > 0.0)", 
            tr("OK") );
            
        return false;
    }

    double thresholdLow;
    ret = sscanf(lineEditTLow->text().latin1(), "%lf", &thresholdLow);
    if ((ret != 1) || (thresholdLow <= 0.0f))
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Invalid low threshold (should be > 0.0)", 
            tr("OK") );
            
        return false;
    }
    
    if (thresholdMed >= thresholdLow)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Medium threshold must be < low threshold", 
            tr("OK") );
            
        return false;
    }

    double radius;
    ret = sscanf(lineEditRadius->text().latin1(), "%lf", &radius);
    if ((ret != 1) || (radius <= 0.0f))
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Invalid radius (should be > 0.0)", 
            tr("OK") );
            
        return false;
    }

    sprintf(pParams, "%lf %lf %lf %d", thresholdMed, thresholdLow, radius, boxing);

    return true;
}

bool MULTIPR_GUI_DecimateDlg::setDirectFilterParams(char* pParams)
{
    int boxing = spinBoxBoxing->value();

    double thresholdMed;
    int ret = sscanf(lineEditTMed->text().latin1(), "%lf", &thresholdMed);
    if (ret != 1)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Invalid medium threshold.", 
            tr("OK") );
            
        return false;
    }

    double thresholdLow;
    ret = sscanf(lineEditTLow->text().latin1(), "%lf", &thresholdLow);
    if (ret != 1)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Invalid low threshold.",
            tr("OK") );
            
        return false;
    }
    
    if (thresholdMed >= thresholdLow)
    {
        SUIT_MessageBox::error1( 
            mModule->getAppli()->desktop(),
            "Decimation parameters error", 
            "Medium threshold must be < low threshold", 
            tr("OK") );
            
        return false;
    }

	sprintf(pParams, "%lf %lf", thresholdMed, thresholdLow);
	
	return true;
}

void MULTIPR_GUI_DecimateDlg::selectField(const QString& valueText)
{
	if (valueText == "Filtre_GradientMoyen")
	{
		lineEditRadius->show();
		spinBoxBoxing->show();
		pushButtonRadiusAuto->show();
		textLabelRadius->show();
    	textLabelBoxing->show();
		//pushButtonThresholdAuto->show();
	}
	else if (valueText == "Filtre_Direct")
	{
		lineEditRadius->hide();
		spinBoxBoxing->hide();
		pushButtonRadiusAuto->hide();
		textLabelRadius->hide();
    	textLabelBoxing->hide();
		//pushButtonThresholdAuto->hide();
	}
	else
	{
        SUIT_MessageBox::error1(
            mModule->getAppli()->desktop(),
            "Decimation error", 
            "Unknown filter name.", 
            tr("OK") );
	}
}

void MULTIPR_GUI_DecimateDlg::reject()
{
    QDialog::reject();
}


void MULTIPR_GUI_DecimateDlg::OnRadiusAuto()
{
    // evaluates default radius for the first selected part
    const QStringList& partsList = mModule->getSelectedParts();
    const char* strFieldIt   = comboBoxSelectFieldIteration->currentText().latin1();
    int fieldIteration = atoi(strFieldIt);
    char* strPartInfo0 = mModule->getMULTIPRObj()->getPartInfo(partsList[0].latin1());
    
    char   lMeshName[256];
    int    lId;
    char   lPartName[256];
    char   lPath[256];
    char   lMEDFileName[256];
    
    // parse infos
    int ret = sscanf(strPartInfo0, "%s %d %s %s %s", 
        lMeshName,
        &lId,
        lPartName,
        lPath,
        lMEDFileName);
        
    if (ret != 5)
    {
        return;
    }
    
    QApplication::setOverrideCursor(Qt::waitCursor);
    float defaultRadius = 0.5f;
    try
    {
        char strParams[256];
        sprintf(strParams, "2");
        
        char* res = mModule->getMULTIPRObj()->evalDecimationParams(
            lPartName,
            comboBoxSelectFieldName->currentText().latin1(),
            fieldIteration,
            comboBoxSelectFilter->currentText().latin1(),
            strParams);
        
        sscanf(res, "%f", &defaultRadius);
    }
    catch (...)
    {
    }
    QApplication::restoreOverrideCursor();
    
    lineEditRadius->setText( QString::number(defaultRadius) );
}


void MULTIPR_GUI_DecimateDlg::OnThresholdAuto()
{
    // evaluates default radius for the first selected part
    const QStringList& partsList = mModule->getSelectedParts();
    const char* strFieldIt  = comboBoxSelectFieldIteration->currentText().latin1();
    int fieldIteration = atoi(strFieldIt);
    char* strPartInfo0 = mModule->getMULTIPRObj()->getPartInfo(partsList[0].latin1());
    QString filterName = comboBoxSelectFilter->currentText();

    char   lMeshName[256];
    int    lId;
    char   lPartName[256];
    char   lPath[256];
    char   lMEDFileName[256];
    
    // parse infos
    int ret = sscanf(strPartInfo0, "%s %d %s %s %s", 
        lMeshName,
        &lId,
        lPartName,
        lPath,
        lMEDFileName);
    
    QApplication::setOverrideCursor(Qt::waitCursor);
    if (filterName == "Filtre_GradientMoyen")
    {
        try
        {
            float radius;
            ret = sscanf(lineEditRadius->text().latin1(), "%f", &radius);
            if ((ret != 1) || (radius <= 0.0f))
            {
                SUIT_MessageBox::error1( 
                    mModule->getAppli()->desktop(),
                    "Decimation parameters error", 
                    "Invalid radius (should be > 0.0)", 
                    tr("OK") );
                    
                return;
            }
            
            char strParams[256];        
            sprintf(strParams, "1 %f %d", radius, spinBoxBoxing->value());
            
            char* res = mModule->getMULTIPRObj()->evalDecimationParams(
                lPartName,
                comboBoxSelectFieldName->currentText().latin1(),
                fieldIteration,
                comboBoxSelectFilter->currentText().latin1(),
                strParams);
            
            float gradMin, gradAvg, gradMax;
            sscanf(res, "%f %f %f", &gradMin, &gradAvg, &gradMax);
            
            lineEditTMed->setText( QString::number(gradMin) );
            lineEditTLow->setText( QString::number(gradMax) );
        }
        catch (...)
        {
        }
    }
    else if (filterName == "Filtre_Direct")
    {
        float   lMin = 1.0f;
        float   lMax = 2.0f;
        
        mModule->getMULTIPRObj()->getFieldMinMax(lPartName, comboBoxSelectFieldName->currentText().latin1(), lMin, lMax);
        lineEditTMed->setText(QString::number(lMin));
        lineEditTLow->setText(QString::number(lMax));
    }
    else
	{
        SUIT_MessageBox::error1(
            mModule->getAppli()->desktop(),
            "Decimation error", 
            "Unknown filter name.", 
            tr("OK") );
	}
    QApplication::restoreOverrideCursor();
}

//*****************************************************************************
// MULTIPR_GUI_ProgressCallbackDlg
// QT dialog box used to display progress in time consuming task (e.g. save)
//*****************************************************************************

MULTIPR_GUI_ProgressCallbackDlg::MULTIPR_GUI_ProgressCallbackDlg(QWidget* parent) :
    QProgressDialog(
        parent, 
        0, 
        false, 
        WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu)
{
    setLabel(new QLabel(this, "Please wait"));
    setLabelText("Please wait");
    setTotalSteps(100);
}


MULTIPR_GUI_ProgressCallbackDlg::~MULTIPR_GUI_ProgressCallbackDlg()
{
    // do nothing
}


void MULTIPR_GUI_ProgressCallbackDlg::start(const char* pTaskTitle, int pNumStep)
{
    setCaption(pTaskTitle);
    MULTIPR_ProgressCallback::init(pNumStep);
}


void MULTIPR_GUI_ProgressCallbackDlg::done()
{
    setProgress(100);
}

    
void MULTIPR_GUI_ProgressCallbackDlg::progress(float pPercent)
{
    setProgress(int(pPercent));
}


//*****************************************************************************
// MULTIPR_GUI_EmptyMeshCallbackDlg
// QT dialog box used to inform about empty meshes
//*****************************************************************************

MULTIPR_GUI_EmptyMeshCallbackDlg::MULTIPR_GUI_EmptyMeshCallbackDlg(QWidget* parent)
{
    mParent = parent;
}


MULTIPR_GUI_EmptyMeshCallbackDlg::~MULTIPR_GUI_EmptyMeshCallbackDlg()
{
    // do nothing
}


void MULTIPR_GUI_EmptyMeshCallbackDlg::reportEmptyMesh(string pInfo)
{
    char msg[256];
    sprintf(msg, "Empty mesh detected (%s)", pInfo.c_str());
    
    SUIT_MessageBox::warn1( 
        mParent,
        "Empty mesh detected", 
        msg, 
        "OK" );
}

// EOF

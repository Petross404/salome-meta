// Project MULTIPR, IOLS WP1.2.1 - EDF/CS
// Partitioning/decimation module for the SALOME v3.2 platform

/**
 * \file    MULTIPR_GUI_Dlg.h
 *
 * \brief   MULTIPR GUI Dialog (QT)
 *
 * \author  Olivier LE ROUX - CS, Virtual Reality Dpt
 * 
 * \date    01/2007
 */

#ifndef __MULTIPR_GUI_DLG__
#define __MULTIPR_GUI_DLG__


//*****************************************************************************
// Includes section
//*****************************************************************************

#include <qdialog.h>
#include <qprogressdialog.h>

#include "MULTIPR_ProgressCallback.hxx"


//*****************************************************************************
// Pre-declaration
//*****************************************************************************

class SalomeApp_Application;
class CAM_Module;

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QButtonGroup;
class QLabel;
class QComboBox;
class QLineEdit;
class QSpinBox;
class QPushButton;

class MULTIPR_GUI;

/**
 * Max length for filter parameters string.
 */
#define MULTIPR_GUI_MAX_PARAMS_LENGTH 1024

//*****************************************************************************
// Class MULTIPR_GUI_Partition1Dlg
// Dialog box used for extracting groups from sequential MED file
//*****************************************************************************

class MULTIPR_GUI_Partition1Dlg : public QDialog
{
    Q_OBJECT

public:
    MULTIPR_GUI_Partition1Dlg(MULTIPR_GUI* theModule);
    ~MULTIPR_GUI_Partition1Dlg();

    QButtonGroup* buttonGroupProcess;
    QPushButton* pushButtonCancel;
    QPushButton* pushButtonOK;
    QButtonGroup* buttonGroupSelectMesh;
    QComboBox* comboBoxSelectMesh;
    QLabel* textLabelSelectMesh;

protected slots:
    void accept();
    void reject();
    
private:
    MULTIPR_GUI* mModule;

};


//*****************************************************************************
// Class MULTIPR_GUI_Partition2Dlg
// Dialog box used for splitting a group (use MEDSPLITTER)
//*****************************************************************************

class MULTIPR_GUI_Partition2Dlg : public QDialog
{
    Q_OBJECT

public:
    MULTIPR_GUI_Partition2Dlg(MULTIPR_GUI* theModule);
    ~MULTIPR_GUI_Partition2Dlg();

    QButtonGroup* buttonGroupSplitParameters;
    QLabel* textLabelSelectNbParts;
    QLabel* textLabelSelectSplitter;
    QComboBox* comboBoxSelectSplitter;
    QSpinBox* spinBoxNbParts;
    QButtonGroup* buttonGroupProcess;
    QPushButton* pushButtonOK;
    QPushButton* pushButtonCancel;

protected slots:
    void accept();
    void reject();

private:
    MULTIPR_GUI* mModule;
};


//*****************************************************************************
// Class MULTIPR_GUI_DecimateDlg
// Dialog box used for decimating mesh (compute 2 lower resolution: medium and low)
//*****************************************************************************

class MULTIPR_GUI_DecimateDlg : public QDialog
{
    Q_OBJECT

public:
    MULTIPR_GUI_DecimateDlg(MULTIPR_GUI* theModule);
    ~MULTIPR_GUI_DecimateDlg();

    QButtonGroup* buttonGroupSelectField;
    QLabel* textLabelSelectFieldName;
    QLabel* textLabelSelectFieldIteration;
    QComboBox* comboBoxSelectFieldIteration;
    QComboBox* comboBoxSelectFieldName;
    QButtonGroup* buttonGroupSelectFilter;
    QLabel* textLabelSelectFilter;
    QComboBox* comboBoxSelectFilter;
    QButtonGroup* buttonGroupParameters;
    QLabel* textLabelTMed;
    QLabel* textLabelTLow;
    QLabel* textLabelRadius;
    QLabel* textLabelBoxing;
    QLineEdit* lineEditTMed;
    QLineEdit* lineEditTLow;
    QLineEdit* lineEditRadius;
    QSpinBox* spinBoxBoxing;
    QButtonGroup* buttonGroupProcess;
    QPushButton* pushButtonCancel;
    QPushButton* pushButtonOK;
    QPushButton* pushButtonThresholdAuto;
    QPushButton* pushButtonRadiusAuto;
    QButtonGroup* infoGroup;
    QLabel* textLabelInfo;
    QLabel* textLabelInfoValue;
	
protected:

protected slots:
    void accept();
    void reject();
    void OnRadiusAuto();
    void OnThresholdAuto();
    void selectField(const QString & valueText);

private:
    MULTIPR_GUI* mModule;

    /**
     * Create the parameter string for gradient average filter.
     * \param pParams The parameter string to fill.
     * \return True if the operation is successfull, false otherwise.
     */
    bool setGradAvgFilterParams(char* pParams);

    /**
     * Create the parameter string for direct filter.
     * \param pParams The parameter string to fill.
     * \return True if the operation is successfull, false otherwise.
     */
    bool setDirectFilterParams(char* pParams);
};


//*****************************************************************************
// Class MULTIPR_GUI_ProgressCallbackDlg
//*****************************************************************************

class MULTIPR_GUI_ProgressCallbackDlg : 
    public QProgressDialog,
    public MULTIPR_ProgressCallback
    
{
    Q_OBJECT
    
public:

    MULTIPR_GUI_ProgressCallbackDlg(QWidget* parent);
    
    ~MULTIPR_GUI_ProgressCallbackDlg();
    
    virtual void start(const char* pTaskTitle, int pNumStep);
    
    virtual void done();
    
protected:
    
    virtual void progress(float pPercent);
};


//*****************************************************************************
// Class MULTIPR_GUI_EmptyMeshCallbackDlg
//*****************************************************************************

class MULTIPR_GUI_EmptyMeshCallbackDlg : public MULTIPR_EmptyMeshCallback   
{   
public:

    MULTIPR_GUI_EmptyMeshCallbackDlg(QWidget* parent);
    
    ~MULTIPR_GUI_EmptyMeshCallbackDlg();
    
    virtual void reportEmptyMesh(std::string pInfo);
    
protected:

    QWidget* mParent;

};


#endif // __MULTIPR_GUI_DLG__


// EOF


// Project MULTIPR, IOLS WP1.2.1 - EDF/CS
// Partitioning/decimation module for the SALOME v3.2 platform

/**
 * \file    MULTIPR_GUI.h
 *
 * \brief   MULTIPR GUI (QT)
 *
 * \author  Olivier LE ROUX - CS, Virtual Reality Dpt
 * 
 * \date    01/2007
 */

#ifndef __MULTIPR_GUI__
#define __MULTIPR_GUI__


//*****************************************************************************
// Includes section
//*****************************************************************************

#include <SalomeApp_Module.h>
#include <LightApp_DataObject.h>
//#include <LightApp_DataModel.h>
#include <SalomeApp_DataModel.h>

#include <SALOMEconfig.h>
#include CORBA_CLIENT_HEADER(MULTIPR)

#include "MULTIPR_ProgressCallback.hxx"


//*****************************************************************************
// Pre-declaration
//*****************************************************************************

class SalomeApp_Application;
class CAM_Module;

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QButtonGroup;
class QLabel;
class QComboBox;
class QLineEdit;
class QSpinBox;
class QPushButton;

class QTimer;
//class MULTIPR_GUI_ProgressCallbackDlg;
class QProgressDialog;

//*****************************************************************************
// Class MULTIPR_GUI
// Salome Application
//*****************************************************************************

class MULTIPR_GUI: public SalomeApp_Module
{
    Q_OBJECT

public:

    MULTIPR_GUI();
    
    virtual ~MULTIPR_GUI();
    
    void initialize(CAM_Application*);
    
    QString engineIOR() const;
    
    void windows(QMap<int, int>&) const;
    
    MULTIPR_ORB::MULTIPR_Obj_ptr getMULTIPRObj();
    void setMULTIPRObj (MULTIPR_ORB::MULTIPR_Obj_ptr theObj);
    
    SalomeApp_Application* getAppli() const;
    
    void selected(QStringList&, const bool);
    
    const QStringList& getSelectedParts() const { return mSelectedParts; }

public slots:

    bool deactivateModule(SUIT_Study*);
    bool activateModule(SUIT_Study*);

protected slots:

    void OnImportFromMEDFile();
    void OnPartition1();
    void OnPartition2();
    void OnDecimate();
    void OnRemove();
    void OnSave();

    void timerDone(); // update saving progress dialog

protected:

    virtual CAM_DataModel* createDataModel();

    void retrieveSelectedParts();
    bool isPartExist(const char* partName);
    bool removeLowerResolution();
    
protected:

    enum 
    { 
        ACTION_IMPORT_MED = 190,
        ACTION_SAVE,
        ACTION_REMOVE,
        ACTION_SPLIT,
        ACTION_DECIMATE
    };
    
private:

    QString                      mMEDFileName;
    QStringList                  mSelectedParts;
    //MULTIPR_ORB::MULTIPR_Obj_ptr mMULTIPRObj;
    MULTIPR_ORB::MULTIPR_Obj_var mMULTIPRObj;

    QProgressDialog*             mProgress;
    //MULTIPR_GUI_ProgressCallbackDlg* mProgress;
    QTimer*                      mTimer;

}; // class MULTIPR_GUI


//*****************************************************************************
// Class MULTIPR_GUI_DataObject
// To use Object Browser
//*****************************************************************************

class MULTIPR_GUI_DataObject : public LightApp_DataObject
{
public:

    MULTIPR_GUI_DataObject(SUIT_DataObject* parent, const char* name);
    virtual ~MULTIPR_GUI_DataObject();
    
    virtual QString entry()   const;
    virtual QString name()    const;
    virtual QPixmap icon()    const;
    virtual QString toolTip() const;

protected:

    QString mName;
};


//*****************************************************************************
// Class MULTIPR_GUI_DataObject_Module
//*****************************************************************************

class MULTIPR_GUI_DataObject_Module : public MULTIPR_GUI_DataObject, public LightApp_ModuleObject
{
public:

    MULTIPR_GUI_DataObject_Module(CAM_DataModel* dm, SUIT_DataObject* parent, const char* name);
    virtual ~MULTIPR_GUI_DataObject_Module();
    
    virtual QString entry()   const;
    virtual QString name()    const;
    virtual QPixmap icon()    const;
    virtual QString toolTip() const;
};


//*****************************************************************************
// Class MULTIPR_GUI_DataObject_Mesh
//*****************************************************************************

class MULTIPR_GUI_DataObject_Mesh : public MULTIPR_GUI_DataObject
{
public:

    MULTIPR_GUI_DataObject_Mesh(SUIT_DataObject* parent, const char* name);
    virtual ~MULTIPR_GUI_DataObject_Mesh();
    
    virtual QString entry()   const;
    virtual QPixmap icon()    const;
    virtual QString toolTip() const;
};


//*****************************************************************************
// Class MULTIPR_GUI_DataObject_Part
//*****************************************************************************

class MULTIPR_GUI_DataObject_Part : public MULTIPR_GUI_DataObject
{
public:

    MULTIPR_GUI_DataObject_Part(SUIT_DataObject* parent, const char* name, const char* info);
    virtual ~MULTIPR_GUI_DataObject_Part();
    
    virtual QString entry()   const;
    virtual QPixmap icon()    const;
    virtual QString toolTip() const;

protected:

    QString mMeshName;
    int     mId;
    QString mPath;
    QString mMEDFileName;
    QString mTooltip;
};


//*****************************************************************************
// Class MULTIPR_GUI_DataObject_Resolution
//*****************************************************************************

class MULTIPR_GUI_DataObject_Resolution : public MULTIPR_GUI_DataObject_Part
{
public:

    MULTIPR_GUI_DataObject_Resolution(SUIT_DataObject* parent, const char* name, const char* info);
    virtual ~MULTIPR_GUI_DataObject_Resolution();
    
    virtual QString entry()   const;
    virtual QPixmap icon()    const;
    virtual QString toolTip() const;
};


//*****************************************************************************
// Class MULTIPR_GUI_DataModel
//*****************************************************************************

//class MULTIPR_GUI_DataModel : public LightApp_DataModel
class MULTIPR_GUI_DataModel : public SalomeApp_DataModel
{
public:

    MULTIPR_GUI_DataModel(CAM_Module*);
    virtual ~MULTIPR_GUI_DataModel();

    virtual void update (LightApp_DataObject* = 0, LightApp_Study* = 0);

protected:

    virtual void build();
    void buildAll (LightApp_Study* = 0);

private:

    MULTIPR_GUI* mMULTIPR_GUI;
};


#endif // __MULTIPR_GUI__


// EOF

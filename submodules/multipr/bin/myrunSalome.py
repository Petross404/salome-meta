#  Copyright (C) 2005  CEA/DEN, EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#!/usr/bin/env python

def test(clt):
   """
        Test function that creates an instance of MULTIPR component
        usage : multipr=test(clt)
   """
   # create an LifeCycleCORBA instance
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   multipr = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   return multipr

   
def multipr_test1(clt,path):
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   engine = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   m = engine.getObject(path+'/agregat100grains_12pas.med')
   liste_champs = m.getFields()
   for i in liste_champs:
      m.getTimeStamps(i)
   liste_maillages = m.getMeshes()
   m.setMesh(liste_maillages[0])
   m.partitionneDomaine()
   m.getParts()
   m.save('')
   m.decimePartition('MAIL_99', 'SIG_____SIEF_ELGA_______________', 12, 'Filtre_GradientMoyen', 10, 25, 0.3)
   liste_grains = m.getParts()
   m.save('')
   m.partitionneGrain('MAIL_98', 3, 0)
   m.save('')
   m.getParts()
   m.getMeshes()
   m.getFields()


def multipr_test2(clt,path):
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   engine = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   m = engine.getObject(path+'/agregat100grains_12pas.med')
   m.getMeshes()
   m.setMesh('MAIL')
   m.partitionneDomaine()
   m.getParts()
   liste_champs = m.getFields()
   m.getTimeStamps(liste_champs[3])
   m.decimePartition('MAIL_99', 'SIG_____SIEF_ELGA_______________', 12, 'Filtre_GradientMoyen', 10, 25, 0.3)
   m.save('')
   m.getMeshes()
   m.getParts()
   m.getFields()
   
      
def multipr_test3(clt,path):
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   engine = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   m = engine.getObject(path+'/agregat100grains_12pas.med')
   m.setMesh('MAIL')
   m.partitionneDomaine()
   m.getParts()
   liste_champs = m.getFields()
   m.getTimeStamps(liste_champs[3])
   m.decimePartition('MAIL_99', 'SIG_____SIEF_ELGA_______________', 12, 'Filtre_GradientMoyen', 10, 25, 0.3)
   m.partitionneGrain('MAIL_1', 3, 0)
   m.getParts()
   #m.partitionneGrain('MAIL_2', 4, 1)
   m.getMeshes()
   m.getParts()
   m.getFields()
   m.save('') 
   
      
def multipr_test4(clt,path):
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   engine = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   m = engine.getObject(path+'/agregat100grains_12pas_grains_maitre.med')
   m.getFields()
   m.getParts()
   for i in m.getParts():
      m.partitionneGrain(i, 3, 0);


def multipr_test5(clt,path):
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   engine = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   m = engine.getObject(path+'/agregat100grains_12pas.med')
   liste_champs = m.getFields()
   liste_maillages = m.getMeshes()
   m.setMesh(liste_maillages[0])
   liste_grains = m.partitionneDomaine()
   for grain in liste_grains[0:4]:
      m.decimePartition(grain, liste_champs[1], 12, 'Filtre_GradientMoyen', 10, 25, 0.3)
   m.getParts()
   m.save('')
   

def multipr_test6(clt,path):
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA(clt.orb)
   import MULTIPR_ORB
   engine = lcc.FindOrLoadComponent("FactoryServer", "MULTIPR")
   m = engine.getObject(path+'/agregat100grains_12pas.med')
   liste_champs = m.getFields()
   liste_maillages = m.getMeshes()
   m.setMesh(liste_maillages[0])
   liste_grains = m.partitionneDomaine()
   liste_pgrain = m.partitionneGrain(liste_grains[4], 16, 0)
   m.setBoxing(10)
   for grain in liste_pgrain[0:11]:
      m.decimePartition(grain, liste_champs[1], 12, 'Filtre_GradientMoyen', 10, 25, 0.5)
   m.getParts()
   m.save('')
   n = engine.getObject(path+'/agregat100grains_12pas_grains_maitre.med')
   n.getParts()
   
       
if __name__ == "__main__":
   import user
   from runSalome import *
   clt,args = main()
   
   #
   #  Impression arborescence Naming Service
   #
   
   if clt != None:
     print
     print " --- registered objects tree in Naming Service ---"
     clt.showNS()
     session=clt.waitNS("/Kernel/Session")
     catalog=clt.waitNS("/Kernel/ModulCatalog")
     import socket
     container =  clt.waitNS("/Containers/" + socket.gethostname().split('.')[0] + "/FactoryServer")

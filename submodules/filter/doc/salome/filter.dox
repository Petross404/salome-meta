/*!
\mainpage  FILTER Module Documentation

\section Introduction

<div align="justify">The research for results of simulation increasingly finer on complex geometries induced of the very important need on the visualization of the results obtained. The meshes used to carry out the numerical simulation cannot indeed be entirely visualized. The purpose of module FILTER is to decimate the grid according to the solution, in order to allow their visualization or more generally their postprocessing. In same time, it will have to preserve in the best way the geometry of the field and the precision of the solution.</div>


\section start Getting started

- FILTER module needs to have previously installed the \b filtoo tool of DISTENE company:

Distene S.A.S.
<br>P&ocirc;le Teratec - BARD-1
<br>Domaine du Grand Ru&eacute;
<br>91680 Bruy&egrave;res-le-Chatel
<br>FRANCE

- launch Salome with FILTER and VISU modules:

\code
runSalome --modules=FILTER,VISU
\endcode

- open a new study and select FILTER module in the Salome GUI.

- open a MED file in "File" menu and select "Open a MED file" option.

- a new window appears on screen showing meshes and fields inside the file:

\image html img1.jpg

- select a reference field <b>on nodes</b> in \b 3D mesh. If different time steps ara available, select one and then click "OK" in the window.

- open "DATA REDUCTION" menu and select "Filtering" option. This action opens a new window on screen. The name of the MED file, the name of the mesh, the name of the reference field and the number of the time step selected are displayed on the top of the window:

\image html img3.jpg

- choose to display histogram on reference field values or histogram on reference field gradient values. By default this histogram is displayed on 1024 intervals. You can change this parameter. The histogram can be displayed in linear or logarithm scales.

\image html img2.jpg

- select one or two thresholds to perform filtering. The idea is to keep all the nodes of the mesh of which the value is under or upper the threshold (in case of only one threshold), or inside or outside the two thresholds in this case. The reduction rate is displayed under the histogram.

- give the output MED file and click on "Process" button to perform filtering. The filtering process calls the \b filtoo tool of DISTENE company.

- you can load VISU module and import filtered MED file to visualize the result.

*/

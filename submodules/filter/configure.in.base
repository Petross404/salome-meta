#  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#  PLEASE DO NOT MODIFY configure.in FILE
#  ALL CHANGES WILL BE DISCARDED BY THE NEXT
#  build_configure COMMAND
#  CHANGES MUST BE MADE IN configure.in.base FILE
# Author : Marc Tajchman (CEA)
# Date : 28/06/2001
# Modified by : Patrick GOLDBRONN (CEA)
# Modified by : Marc Tajchman (CEA)
# Created from configure.in.base
#
AC_INIT(src)
AC_CONFIG_AUX_DIR(${KERNEL_ROOT_DIR}/salome_adm/unix/config_files/DEPRECATED)
AC_CANONICAL_HOST

PACKAGE=salome
AC_SUBST(PACKAGE)

VERSION=4.1.1
XVERSION=0x040101
AC_SUBST(VERSION)
AC_SUBST(XVERSION)

# set up MODULE_NAME variable for dynamic construction of directories (resources, etc.)
MODULE_NAME=filter
AC_SUBST(MODULE_NAME)

dnl
dnl Initialize source and build root directories
dnl

ROOT_BUILDDIR=`pwd`
ROOT_SRCDIR=`echo $0 | sed -e "s,[[^/]]*$,,;s,/$,,;s,^$,.,"`
cd $ROOT_SRCDIR
ROOT_SRCDIR=`pwd`
cd $ROOT_BUILDDIR

AC_SUBST(ROOT_SRCDIR)
AC_SUBST(ROOT_BUILDDIR)

echo
echo Source root directory : $ROOT_SRCDIR
echo Build  root directory : $ROOT_BUILDDIR
echo
echo

if test -z "$AR"; then
   AC_CHECK_PROGS(AR,ar xar,:,$PATH)
fi
AC_SUBST(AR)

dnl Export the AR macro so that it will be placed in the libtool file
dnl correctly.
export AR

echo
echo ---------------------------------------------
echo testing make
echo ---------------------------------------------
echo

AC_PROG_MAKE_SET
AC_PROG_INSTALL
dnl 
dnl libtool macro check for CC, LD, NM, LN_S, RANLIB, STRIP + pour les librairies dynamiques !

AC_ENABLE_DEBUG(yes)
AC_DISABLE_PRODUCTION

echo ---------------------------------------------
echo testing libtool
echo ---------------------------------------------

dnl first, we set static to no!
dnl if we want it, use --enable-static
AC_ENABLE_STATIC(no)

AC_LIBTOOL_DLOPEN
AC_PROG_LIBTOOL

dnl Fix up the INSTALL macro if it s a relative path. We want the
dnl full-path to the binary instead.
case "$INSTALL" in
   *install-sh*)
      INSTALL='\${KERNEL_ROOT_DIR}'/salome_adm/unix/config_files/DEPRECATED/install-sh
      ;;
esac

echo
echo ---------------------------------------------
echo testing C/C++
echo ---------------------------------------------
echo

cc_ok=no
dnl inutil car libtool
dnl AC_PROG_CC
AC_PROG_CXX
AC_DEPEND_FLAG
# AC_CC_WARNINGS([ansi])
cc_ok=yes

dnl Library libdl :
AC_CHECK_LIB(dl,dlopen)

dnl add library libm :
AC_CHECK_LIB(m,ceil)

dnl 
dnl Well we use sstream which is not in gcc pre-2.95.3
dnl We must test if it exists. If not, add it in include !
dnl

AC_CXX_USE_STD_IOSTREAM
AC_CXX_HAVE_SSTREAM



dnl
dnl ---------------------------------------------
dnl testing MPICH
dnl ---------------------------------------------
dnl

CHECK_MPICH

echo
echo ---------------------------------------------
echo testing python
echo ---------------------------------------------
echo

CHECK_PYTHON

echo
echo ---------------------------------------------
echo testing swig
echo ---------------------------------------------
echo

CHECK_SWIG

echo
echo ---------------------------------------------
echo testing threads
echo ---------------------------------------------
echo

ENABLE_PTHREADS

echo
echo ---------------------------------------------
echo testing omniORB
echo ---------------------------------------------
echo

CHECK_OMNIORB

echo
echo ---------------------------------------------
echo default ORB : omniORB
echo ---------------------------------------------
echo

DEFAULT_ORB=omniORB
CHECK_CORBA

AC_SUBST_FILE(CORBA)
corba=make_$ORB
CORBA=adm_local/unix/$corba

echo
echo ---------------------------------------------
echo testing QT
echo ---------------------------------------------
echo

CHECK_QT

echo
echo ---------------------------------------------
echo testing msg2qm
echo ---------------------------------------------
echo

CHECK_MSG2QM

echo
echo ---------------------------------------------
echo testing VTK
echo ---------------------------------------------
echo

CHECK_VTK

echo
echo ---------------------------------------------
echo testing HDF5
echo ---------------------------------------------
echo

CHECK_HDF5

echo
echo ---------------------------------------------
echo testing MED2
echo ---------------------------------------------
echo

CHECK_MED2

echo
echo ---------------------------------------------
echo BOOST Library
echo ---------------------------------------------
echo

CHECK_BOOST

echo
echo ---------------------------------------------
echo Testing OpenCascade
echo ---------------------------------------------
echo

CHECK_CAS

echo
echo ---------------------------------------------
echo Testing qwt
echo ---------------------------------------------
echo

CHECK_QWT

echo
echo ---------------------------------------------
echo Testing html generators
echo ---------------------------------------------
echo

CHECK_HTML_GENERATORS

echo
echo ---------------------------------------------
echo Testing GUI
echo ---------------------------------------------
echo

CHECK_SALOME_GUI

echo
echo ---------------------------------------------
echo Testing full GUI
echo ---------------------------------------------
echo

CHECK_CORBA_IN_GUI
if test "x${CORBA_IN_GUI}" != "xyes"; then
  echo "failed : For configure FILTER module necessary full GUI !"
  exit
fi

echo
echo ---------------------------------------------
echo Testing Kernel
echo ---------------------------------------------
echo

CHECK_KERNEL

echo
echo ---------------------------------------------
echo Testing Med
echo ---------------------------------------------
echo

CHECK_MED

echo
echo ---------------------------------------------
echo Testing filtoo
echo ---------------------------------------------
echo

CHECK_FILTOO

echo
echo ---------------------------------------------
echo Summary
echo ---------------------------------------------
echo

echo Configure
variables="cc_ok boost_ok python_ok swig_ok threads_ok qt_ok vtk_ok hdf5_ok med2_ok omniORB_ok occ_ok qwt_ok doxygen_ok graphviz_ok Kernel_ok Med_ok filtoo_ok"

for var in $variables
do
   printf "   %10s : " `echo \$var | sed -e "s,_ok,,"`
   eval echo \$$var
done

echo
echo "Default ORB   : $DEFAULT_ORB"
echo

dnl generals files which could be included in every makefile

AC_SUBST_FILE(COMMENCE) COMMENCE=adm_local/unix/make_commence
AC_SUBST_FILE(CONCLUDE) CONCLUDE=salome_adm/unix/make_conclude
AC_SUBST_FILE(MODULE) MODULE=salome_adm/unix/make_module

dnl les dependences
AC_SUBST_FILE(DEPEND) DEPEND=salome_adm/unix/depend

dnl We don t need to say when we re entering directories if we re using
dnl GNU make becuase make does it for us.
if test "X$GMAKE" = "Xyes"; then
   AC_SUBST(SETX) SETX=":"
else
   AC_SUBST(SETX) SETX="set -x"
fi

# make other build directories
for rep in salome_adm adm_local doc bin/salome include/salome lib${LIB_LOCATION_SUFFIX}/salome share/salome/resources/${MODULE_NAME} idl
do
#   if test ! -d $rep ; then
#      eval mkdir $rep
#   fi
    $INSTALL -d $rep
done

echo 
echo ---------------------------------------------
echo copying resource files, shell scripts, and 
echo xml files
echo ---------------------------------------------
echo


dnl copy resources directories

#for i in `find $ROOT_SRCDIR -name 'resources' -print`
#do
#  local_res=`echo $i | sed -e "s,$ROOT_SRCDIR,.,"`
#  local_res_dir=`echo $local_res | sed -e "s,[[^/]]*$,,;s,/$,,;s,^$,.,"`
#  mkdir -p $local_res_dir
#  cd $local_res_dir
#  ln -fs $i
#  echo $local_res
#  cd $ROOT_BUILDDIR
#done

dnl copy shells and utilities contained in the bin directory
dnl excluding .in files (treated in AC-OUTPUT below) and CVS 
dnl directory

mkdir -p bin/salome
cd bin/salome
for i in $ROOT_SRCDIR/bin/*
do
  local_bin=`echo $i | sed -e "s,$ROOT_SRCDIR,.,"`
  case "$local_bin" in
        *.in | *~)                    ;;
        ./bin/CVS | ./bin/salome)                    ;;
        *) /usr/bin/install $i .; echo $local_bin ;;
  esac
done
cd $ROOT_BUILDDIR

AC_SUBST_FILE(ENVSCRIPT) ENVSCRIPT=salome_adm/unix/envScript

dnl copy xml files to the build tree (lib directory)
dnl pourquoi ????

#cd lib
#for i in `find $ROOT_SRCDIR -name "*.xml" -print`
#do
#  ln -fs $i
#  echo `echo $i | sed -e "s,$ROOT_SRCDIR,.,"`
#done
#cd $ROOT_BUILDDIR


echo
echo ---------------------------------------------
echo generating Makefiles and configure files
echo ---------------------------------------------
echo

AC_OUTPUT_COMMANDS([ \
	chmod +x ./bin/* \
])

## do not delete this line

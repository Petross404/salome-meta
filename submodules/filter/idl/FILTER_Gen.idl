//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//=============================================================================
// File      : FILTER_Gen.idl
// Project   : SALOME
//=============================================================================
//
#ifndef _Filter_GEN_IDL_
#define _Filter_GEN_IDL_


#include "SALOME_Exception.idl"
#include "SALOME_Component.idl"

module SALOME_FILTER
{
  struct DT_IT {
    long dt;
    long it;
  };

  enum rfunc {F_FIELD,F_GRAD};
  typedef rfunc ref_func;
  typedef sequence<long> LongSeq;
  typedef sequence<string> StrSeq;
  typedef sequence<DT_IT> DTITSeq;

  interface FILTER_Gen : Engines::Component
  {
    exception FilterError{
      string error_msg;
    };

    void loadMED(in string inMEDFile);
    void unloadMED();
    StrSeq getMeshNames();
    StrSeq getFieldNames();
    long getMeshDimension(in string meshName);
    long getFieldEntity(in string fieldName,in long dt,in long it);
    boolean fieldIsOnAllElements(in string fieldName,in long dt,in long it);
    DTITSeq getFieldIteration(in string fieldName);
    string getMeshName(in string fieldName,in long dt,in long it);
    void readReferenceField(in string meshName,in string fieldName,in long ts);
    void buildGradient() raises(FilterError);
    void getMinMax(out double min,out double max,in ref_func rf);
    LongSeq getHistogram(in long size,in ref_func rf);
    void generateCriteria(in long nbthresh,in double fthresh,in double sthresh,in boolean areaFlag,in ref_func rf) raises(FilterError);
    void createEnsightInputFiles() raises(FilterError);
    void filtering() raises(FilterError);
    void projectFieldsOnDecimateMesh() raises(FilterError);
    void createMedOutputFile(in string outMedFile);
  };

};

#endif

//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  FILTER FILTER : implemetation of FILTER idl descriptions
//  File   : Filter_Gen_i.hxx
//  Author : Bernard SECHER, CEA
//  Module : FILTER
//  $Header$
//
#ifndef _FILTER_GEN_I_HXX_
#define _FILTER_GEN_I_HXX_

#include <SALOMEconfig.h>
#include <map>
#include CORBA_SERVER_HEADER(FILTER_Gen)

#include "utilities.h"
#include "MEDMEM_Med.hxx"
#include "Utils_SALOME_Exception.hxx"

#include "SALOME_Component_i.hxx"
#include "SALOME_NamingService.hxx"

class Filter_Gen_i:
  public POA_SALOME_FILTER::FILTER_Gen,
  public Engines_Component_i 
{
public:
  Filter_Gen_i();
  Filter_Gen_i(CORBA::ORB_ptr orb,
	    PortableServer::POA_ptr poa,
	    PortableServer::ObjectId * contId, 
	    const char *instanceName, 
	    const char *interfaceName);
  virtual ~Filter_Gen_i();

  void loadMED(const char* inMedFile);
  void unloadMED();
  SALOME_FILTER::StrSeq* getMeshNames();
  SALOME_FILTER::StrSeq* getFieldNames();
  CORBA::Long getMeshDimension(const char* meshName);
  CORBA::Long getFieldEntity(const char* fieldName,CORBA::Long dt,CORBA::Long it);
  CORBA::Boolean fieldIsOnAllElements(const char* fieldName,CORBA::Long dt,CORBA::Long it);
  SALOME_FILTER::DTITSeq* getFieldIteration(const char* fieldName);
  char* getMeshName(const char* fieldName,CORBA::Long dt,CORBA::Long it);
  void readReferenceField(const char* meshName, const char* fieldName, CORBA::Long ts);
  void buildGradient()  throw(SALOME_FILTER::FILTER_Gen::FilterError);
  void getMinMax(CORBA::Double& imin, CORBA::Double& imax,SALOME_FILTER::ref_func rf);
  SALOME_FILTER::LongSeq* getHistogram(CORBA::Long size,SALOME_FILTER::ref_func rf);
  void generateCriteria(CORBA::Long nbthresh,CORBA::Double fthresh,CORBA::Double thresh,CORBA::Boolean areaFlag,SALOME_FILTER::ref_func rf) throw(SALOME_FILTER::FILTER_Gen::FilterError);
  void createEnsightInputFiles() throw(SALOME_FILTER::FILTER_Gen::FilterError);
  void filtering() throw(SALOME_FILTER::FILTER_Gen::FilterError);
  void projectFieldsOnDecimateMesh() throw(SALOME_FILTER::FILTER_Gen::FilterError);
  void createMedOutputFile(const char* outMedFile);

private :
  void readMapping();
  int getNodeNumber(int);
  int getNeighbourVertex(int) throw(SALOME_Exception);

  static Filter_Gen_i*  _FILTERGen;    // Point to last created instance of the class

  bool   _duringLoad;
  SALOME_NamingService *_NS;

  int _nbvmap;
  int *_map;
  int _connL;
  const int *_conn;
  const int *_connI;
  string _file;

  ::MEDMEM::MED *_med, *_newMed;
  MESH* _mesh, *_newMesh;
  FIELD<double> * _myGradient;
  FIELD<double> * _myDField;
  FIELD<int> * _myIField;
  FIELD<int> *_criteria;

};

extern "C"
    PortableServer::ObjectId * FilterMEDEngine_factory(
            CORBA::ORB_ptr orb,
            PortableServer::POA_ptr poa,
            PortableServer::ObjectId * contId,
            const char *instanceName,
            const char *interfaceName);
#endif

#  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Author: Bernard Secher 2008 january
#
import os
import salome
import SALOME_FILTER
import libSALOME_LifeCycleCORBA
lcc=libSALOME_LifeCycleCORBA.SALOME_LifeCycleCORBA()
# load FILTER module
filter=lcc.FindOrLoad_Component("","FILTER")
inputFile=os.getenv("FILTER_ROOT_DIR")+"/share/salome/resources/filter/xx.aa.med"
outputFile="filtered.med"
# load input MED file
filter.loadMED(inputFile)
# read reference field for filtering
filter.readReferenceField("MeshFromEnsight","FieldFromEnsight",0)
# generate filtering criteria from thersholds on reference filed
filter.generateCriteria(1,2.2,0.,True,SALOME_FILTER.F_FIELD)
# create ensight input files for DISTENE filtoo tool
filter.createEnsightInputFiles()
# call DISTENNE filtoo tool
filter.filtering()
# project input fileds on new mesh
filter.projectFieldsOnDecimateMesh()
# create output MED file
filter.createMedOutputFile(outputFile)
# unload MED object
filter.unloadMED()

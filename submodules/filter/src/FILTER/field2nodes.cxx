//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#include<string>
#include<deque>

#include "MEDMEM_Exception.hxx"
#include "MEDMEM_define.hxx"

#include "MEDMEM_Med.hxx"
#include "MEDMEM_Mesh.hxx"
#include "MEDMEM_Family.hxx"
#include "MEDMEM_Support.hxx"
#include "MEDMEM_Field.hxx"

using namespace std;
using namespace MEDMEM;

void usage(char * name)
{
  cout << " ERROR ABOUT SYNTAX " << endl ;
  cout << "  " << name << " <input med file> <output med file> " << endl ;
  exit(-1);
}

int main (int argc, char ** argv) {

  string filenameIN ;
  string filenameOUT;
  set <int> listElements, supset;
  set <int>::iterator elemIt, ssIt;
  
  if ( argc == 3 ) {
    filenameIN  = argv[1] ;
    filenameOUT = argv[2] ;
    cout << "-> reading all into the Med file " << filenameIN << " and writing all into the Ensight file " << filenameOUT <<  endl ;

    MED myMed(MED_DRIVER,filenameIN) ;

    cout << "-> Read all meshes "  ;
    int NumberOfMeshes = myMed.getNumberOfMeshes() ;
    cout << "( "<<NumberOfMeshes << " ) :" << endl ;
    FIELD<double> **volume = new FIELD<double>*[NumberOfMeshes];
    deque<string> MeshName = myMed.getMeshNames() ;
    for (int i=0; i<NumberOfMeshes; i++) {
      volume[i] = NULL;
      myMed.getMesh(MeshName[i])->read() ;
      cout << "-> Mesh "<<i+1<<", named "<<MeshName[i]<<" is read !" << endl;
      int id = myMed.getMesh(MeshName[i])->addDriver(MED_DRIVER,filenameOUT,MeshName[i],MED_EN::MED_ECRI);
      myMed.getMesh(MeshName[i])->write(id);
      cout << "-> Mesh "<<i+1<<", named "<<MeshName[i]<<" is written !" << endl;
    }

    myMed.updateSupport() ;
    
    cout << "-> Read all fields " ;
    int NumberOfFields = myMed.getNumberOfFields() ;
    cout << "( "<<NumberOfFields << " ) :" << endl;
    deque<string> FieldName = myMed.getFieldNames() ;
    for (int i=0; i<NumberOfFields; i++) {
      deque<DT_IT_> FieldIteration = myMed.getFieldIteration(FieldName[i]) ;
      cout << "-> Field "<<i+1<<", named "<<FieldName[i] << " :" << endl ;
      int NumberOfIteration = FieldIteration.size() ;
      cout << "    Number of iteration pair : "<< NumberOfIteration << endl;
      for (int j=0; j<NumberOfIteration; j++) {
	FIELD_ * myField = myMed.getField(FieldName[i],FieldIteration[j].dt,FieldIteration[j].it) ;
	
	myField->read() ;
	cout << "    * Iteration "<<FieldIteration[j].dt<<" and  order number "<<FieldIteration[j].it<<" ) is read !" << endl;

	int NumberOfComponents = myField->getNumberOfComponents();
	switch(myField->getSupport()->getEntity()){
	case MED_CELL:
          {
            cout << "*************************** CHAMP AUX CELLULES" << endl;
            MESH *mesh = myField->getSupport()->getMesh();

            // create new support for new field
            SUPPORT *newSup;
            if( myField->getSupport()->isOnAllElements() )
              newSup = new SUPPORT(mesh,"Support",MED_NODE);
            else{
              int nbe = myField->getSupport()->getNumberOfElements(MED_ALL_ELEMENTS);
              const int *numb = myField->getSupport()->getNumber(MED_ALL_ELEMENTS);
              list<int> myList;
              for(int k=0;k<nbe;k++){
                myList.push_back(numb[k]);
                supset.insert(numb[k]);
              }
              newSup = mesh->buildSupportOnNodeFromElementList(myList,MED_CELL);
            }

            // read number of nodes
            int NumberOfNodes = newSup->getNumberOfElements(MED_ALL_ELEMENTS);
            // calculate reverse connectivity to have the list of elements which contains node i
            const int *revC = myField->getSupport()->getMesh()->getReverseConnectivity(MED_NODAL,MED_CELL);
            const int *indC = myField->getSupport()->getMesh()->getReverseConnectivityIndex(MED_NODAL,MED_CELL);
            int ivol;
            // calculate volume field on mesh
            for(int k=0;k<NumberOfMeshes;k++)
              if(strcmp(mesh->getName().c_str(),MeshName[k].c_str())==0)
                ivol = k;
            if( volume[ivol] == NULL )
              volume[ivol] = myField->getSupport()->getMesh()->getVolume(myField->getSupport());
            if (dynamic_cast<MEDMEM::FIELD<double>*>(myField)){
              FIELD<double> *myDField = (MEDMEM::FIELD<double>*)myField;
              FIELD<double> *newDField = new FIELD<double>(newSup,NumberOfComponents);
              newDField->setName(myField->getName());
              newDField->setIterationNumber(FieldIteration[j].dt);
              newDField->setOrderNumber(FieldIteration[j].it);
              newDField->setTime(myDField->getTime());
              double *val = new double[NumberOfComponents];
              if( newSup->isOnAllElements() ){
                for (int k=1; k<NumberOfNodes+1; k++){
                  // listElements contains elements which contains a node of element i
                  listElements.clear();
                  for(int l=indC[k-1];l<indC[k];l++){
                    // c element contains node k
                    int c=revC[l-1];
                    listElements.insert(c);
                  }

                  // calculate field value on node k 
                  double sigmaV = 0.;
                  for(int l=0;l<NumberOfComponents;l++)
                    val[l] = 0.;
                  for(elemIt=listElements.begin();elemIt!=listElements.end();elemIt++){
                    int elem = *elemIt;
                    double vol = volume[ivol]->getValueIJ(elem,1);
                    if( vol != 0. ){
                      sigmaV += 1./vol;
                      for(int l=1;l<=NumberOfComponents;l++)
                        val[l-1] += myDField->getValueIJ(elem,l)/vol;
                    }
                  }
                  for(int l=1;l<=NumberOfComponents;l++)
                    newDField->setValueIJ(k,l,val[l-1]/sigmaV);
                }
              }
              else{
                const int *numb = newSup->getNumber(MED_ALL_ELEMENTS);
                for (int k=1; k<=NumberOfNodes; k++){
                  int kk = numb[k-1];
                  // listElements contains elements which contains a node of element i
                  listElements.clear();
                  for(int l=indC[kk-1];l<indC[kk];l++){
                    // c element contains node k
                    int c=revC[l-1];
                    // add c element only if it is in partial support
                    ssIt = supset.find(c);
                    if(ssIt!=supset.end())
                      listElements.insert(c);
                  }

                  // calculate field value on node k 
                  double sigmaV = 0.;
                  for(int l=0;l<NumberOfComponents;l++)
                    val[l] = 0.;
                  for(elemIt=listElements.begin();elemIt!=listElements.end();elemIt++){
                    int elem = *elemIt;
                    double vol = volume[ivol]->getValueIJ(elem,1);
                    if( vol != 0. ){
                      sigmaV += 1./vol;
                      for(int l=1;l<=NumberOfComponents;l++)
                        val[l-1] += myDField->getValueIJ(elem,l)/vol;
                    }
                  }
                  for(int l=1;l<=NumberOfComponents;l++)
                    newDField->setValueIJ(k,l,val[l-1]/sigmaV);
                }
              }
              delete [] val;
              int id = newDField->addDriver(MED_DRIVER,filenameOUT,FieldName[i],MED_EN::MED_ECRI);
              newDField->write(id);
              delete newSup;
              delete newDField;
              cout << "    * Iteration "<<FieldIteration[j].dt<<" and  order number "<<FieldIteration[j].it<<" ) is written !" << endl;
            }
            else{
              FIELD<int> *myIField = (MEDMEM::FIELD<int>*)myField;
              FIELD<int> *newIField = new FIELD<int>(newSup,NumberOfComponents);
              newIField->setName(myField->getName());
              newIField->setIterationNumber(FieldIteration[j].dt);
              newIField->setOrderNumber(FieldIteration[j].it);
              newIField->setTime(myIField->getTime());
              double *val = new double[NumberOfComponents];
              if( newSup->isOnAllElements() ){
                for (int k=1; k<NumberOfNodes+1; k++){
                  // listElements contains elements which contains a node of element i
                  listElements.clear();
                  for(int l=indC[k-1];l<indC[k];l++){
                    // c element contains node i
                    int c=revC[l-1];
                    listElements.insert(c);
                  }

                  // calculate field value on node k 
                  double sigmaV = 0.;
                  for(int l=0;l<NumberOfComponents;l++)
                    val[l] = 0.;
                  for(elemIt=listElements.begin();elemIt!=listElements.end();elemIt++){
                    int elem = *elemIt;
                    double vol = volume[ivol]->getValueIJ(elem,1);
                    if( vol != 0. ){
                      sigmaV += 1./vol;
                      for(int l=1;l<=NumberOfComponents;l++)
                        val[l-1] += ((double)myIField->getValueIJ(elem,l))/vol;
                    }
                  }
                  for(int l=1;l<=NumberOfComponents;l++)
                    newIField->setValueIJ(k,l,(int)(val[l-1]/sigmaV));
                }
              }
              else{
                const int *numb = newSup->getNumber(MED_ALL_ELEMENTS);
                for (int k=1; k<=NumberOfNodes; k++){
                  int kk = numb[k-1];
                  // listElements contains elements which contains a node of element i
                  listElements.clear();
                  for(int l=indC[kk-1];l<indC[kk];l++){
                    // c element contains node k
                    int c=revC[l-1];
                    // add c element only if it is in partial support
                    ssIt = supset.find(c);
                    if(ssIt!=supset.end())
                      listElements.insert(c);
                  }

                  // calculate field value on node k 
                  double sigmaV = 0.;
                  for(int l=0;l<NumberOfComponents;l++)
                    val[l] = 0.;
                  for(elemIt=listElements.begin();elemIt!=listElements.end();elemIt++){
                    int elem = *elemIt;
                    double vol = volume[ivol]->getValueIJ(elem,1);
                    if( vol != 0. ){
                      sigmaV += 1./vol;
                      for(int l=1;l<=NumberOfComponents;l++)
                        val[l-1] += myIField->getValueIJ(elem,l)/vol;
                    }
                  }
                  for(int l=1;l<=NumberOfComponents;l++)
                    newIField->setValueIJ(k,l,(int)(val[l-1]/sigmaV));
                }
              }
              delete [] val;
              int id = newIField->addDriver(MED_DRIVER,filenameOUT,FieldName[i],MED_EN::MED_ECRI);
              newIField->write(id);
              delete newSup;
              delete newIField;
              cout << "    * Iteration "<<FieldIteration[j].dt<<" and  order number "<<FieldIteration[j].it<<" ) is written !" << endl;
            }
          }
	  break;
	case MED_FACE:
	  cout << "*************************** CHAMP AUX FACES" << endl;
	  break;
	case MED_EDGE:
	  cout << "*************************** CHAMP AUX ARETES" << endl;
	  break;
	case MED_NODE:
	  cout << "*************************** CHAMP AUX NOEUDS" << endl;
	  int id = myField->addDriver(MED_DRIVER,filenameOUT,FieldName[i],MED_EN::MED_ECRI);
	  myField->write(id);
	  cout << "    * Iteration "<<FieldIteration[j].dt<<" and  order number "<<FieldIteration[j].it<<" ) is written !" << endl;
	  break;
	}

      }
    }

  }
  else usage(argv[0]);
}

//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
using namespace std;

#include "SelectField.h"
#include "MEDMEM_define.hxx"

#include <qgroupbox.h>
#include <qframe.h>
#include <qlayout.h>
#include <qlistview.h>
#include <qslider.h>
#include <qlabel.h>
#include <qpushbutton.h>

SelectField::SelectField(FilterGUI* theModule,const QString& file, 
			 const char* name,
			 bool modal, WFlags fl)
  : QDialog(FILTER::GetDesktop( theModule ), name, modal, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu),
    myFilterGUI( theModule ),
    _file(file),
    _mesh(NULL),
    _field(NULL),
    _ts(0),
    _dimMesh(-1)
{
  QListViewItem *element;

  SCRUTE(_file);
  _filter = myFilterGUI->InitFilterGen();
  _filter->loadMED(_file);
  SALOME_FILTER::StrSeq* meshesNames = _filter->getMeshNames();
  int numberOfMeshes = (*meshesNames).length();

  SALOME_FILTER::StrSeq* fieldsNames = _filter->getFieldNames();
  int numberOfFields = (*fieldsNames).length();

  QGridLayout* _lay = new QGridLayout( this, 1, 1 );

  QGroupBox* _GroupC1 = new QGroupBox( this, "GroupC1" );
  _lay->addWidget( _GroupC1,0,0 );

  MESSAGE(basename((char*)(_file.ascii())));
  QString qs(tr("FILTER_FILE"));
  qs.append(basename((char*)(_file.ascii())));
  _GroupC1->setTitle(qs);
  _GroupC1->setColumnLayout(0, Qt::Vertical );
  _GroupC1->layout()->setSpacing( 0 );
  _GroupC1->layout()->setMargin( 0 );
  _myGroupLayout = new QGridLayout( _GroupC1->layout() );
  _myGroupLayout->setAlignment( Qt::AlignTop );
  _myGroupLayout->setSpacing( 6 );
  _myGroupLayout->setMargin( 11 );
  _myGroupLayout->setColStretch( 0, 0 );
  _myGroupLayout->setColStretch( 1, 1 );

  int row = 0;

  // 0)  label to select field on 3D mesh
  _myGroupLayout->addWidget( new QLabel(tr("FILTER_SEL_3D"),_GroupC1), row, 0 );
  row++;

  // 1)  tree to visualize meshes and fields
  _myList = new QListView( _GroupC1, "List of fields" );
  _myList->setMinimumSize( 500, 500 );
  _myList->setMaximumSize( 500, 500 );
  _myList->setRootIsDecorated(true);
  _myList->addColumn(tr("FILTER_NAME"));
  _myList->addColumn(tr("FILTER_TYPE"));
  _myList->addColumn(tr("FILTER_DIM"));

  for(int i=0;i<numberOfMeshes;i++){
    _dimMesh = _filter->getMeshDimension((*meshesNames)[i]);
    char strd[4];
    sprintf(strd,"%dD\0",_dimMesh);
    element = new QListViewItem( _myList, QString((*meshesNames)[i]), tr("FILTER_MESH") ,strd);
    element->setExpandable(true);
    _myList->setOpen(element,true);

    for (int j=0; j<numberOfFields; j++){
      SALOME_FILTER::DTITSeq *myIteration = _filter->getFieldIteration((*fieldsNames)[j]);
      string meshName = _filter->getMeshName((*fieldsNames)[j],(*myIteration)[0].dt,(*myIteration)[0].it);
      if( strcmp(meshName.c_str(),(*meshesNames)[i]) == 0){
	int ent = _filter->getFieldEntity((*fieldsNames)[j],(*myIteration)[0].dt,(*myIteration)[0].it);
	bool isOnAllElements = _filter->fieldIsOnAllElements((*fieldsNames)[j],(*myIteration)[0].dt,(*myIteration)[0].it);

	char stre[10];
	switch(ent){
	case MED_EN::MED_CELL:
	  strcpy(stre,"on cells");
	  break;
	case MED_EN::MED_FACE:
	  strcpy(stre,"on faces");
	  break;
	case MED_EN::MED_EDGE:
	  strcpy(stre,"on edges");
	  break;
	case MED_EN::MED_NODE:
	  strcpy(stre,"on nodes");
	  break;
	}
	QListViewItem *elem = new QListViewItem( element, QString((*fieldsNames)[j]), tr("FILTER_FIELD"),stre );
	if( (_dimMesh != 3) || (ent != MED_EN::MED_NODE) || !isOnAllElements )
	  elem->setSelectable(false);
      }
    }

    element->setSelectable(false);
  }
  _myGroupLayout->addWidget( _myList, row, 0 );
  row++;

  // 2)  label for time steps
  _myLab = new QLabel(tr("FILTER_SEL_TS"),_GroupC1);
  _myLab->hide();
  _myGroupLayout->addWidget( _myLab, row, 0 );
  row++;

  // 3)  slider to visualize time steps
  _mySlider = new QSlider(_GroupC1);
  _mySlider->setOrientation(Qt::Horizontal);
  _mySlider->setTickmarks(QSlider::Below);
  _myGroupLayout->addWidget( _mySlider, row, 0 );

  _mySlider->hide();
  row++;

  // 4) buttons Ok, Cancel and Help
  _GroupButtons = new QGroupBox(_GroupC1, "GroupButtons");
  _GroupButtons->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, _GroupButtons->sizePolicy().hasHeightForWidth()));
//   _GroupButtons->setGeometry(QRect(10, 10, 281, 48));
  _GroupButtons->setTitle(tr("" ));
  _GroupButtons->setColumnLayout(0, Qt::Vertical);
  _GroupButtons->layout()->setSpacing(0);
  _GroupButtons->layout()->setMargin(0);
  _GroupButtonsLayout = new QGridLayout(_GroupButtons->layout());
  _GroupButtonsLayout->setAlignment(Qt::AlignTop);
  _GroupButtonsLayout->setSpacing(6);
  _GroupButtonsLayout->setMargin(11);
  _buttonHelp = new QPushButton(_GroupButtons, "buttonHelp");
  _buttonHelp->setText(tr("FILTER_BUT_HELP" ));
  _buttonHelp->setAutoDefault(TRUE);
  _GroupButtonsLayout->addWidget(_buttonHelp, 0, 2);
  _buttonCancel = new QPushButton(_GroupButtons, "buttonClose");
  _buttonCancel->setText(tr("FILTER_BUT_CANCEL" ));
  _buttonCancel->setAutoDefault(TRUE);
  _GroupButtonsLayout->addWidget(_buttonCancel, 0, 1);
  _buttonOk = new QPushButton(_GroupButtons, "buttonOk");
  _buttonOk->setText(tr("FILTER_BUT_OK" ));
  _buttonOk->setAutoDefault(TRUE);
  _GroupButtonsLayout->addWidget(_buttonOk, 0, 0);
  _myGroupLayout->addWidget( _GroupButtons, row, 0 );
  row++;

  connect( _myList, SIGNAL(clicked(QListViewItem *)), this, SLOT(fieldSelected(QListViewItem *)));
  connect( _mySlider, SIGNAL(sliderReleased()), this, SLOT(tsSelected()));
  connect(_buttonCancel, SIGNAL(clicked()), this, SLOT(ClickOnCancel()));
  connect(_buttonOk, SIGNAL(clicked()), this, SLOT(ClickOnOk()));
  connect(_buttonHelp, SIGNAL(clicked()),   this, SLOT(ClickOnHelp()));

}

SelectField::~SelectField()
{
  // no need to delete child widgets, Qt does it all for us
  cout << "SelectField: destructor called" << endl;
  _filter->unloadMED();
}

void SelectField::fieldSelected(QListViewItem *lvi)
{
  if(lvi){
    if( (strcmp(lvi->text(1),"Field") == 0) && (_dimMesh == 3) ){
      _field = lvi->text(0);
      _mesh = lvi->parent()->text(0);
//       deque<DT_IT_> myIteration = _filterMED->getFieldIteration(lvi->text(0));
      SALOME_FILTER::DTITSeq *myIteration = _filter->getFieldIteration(lvi->text(0));
      int numberOfIteration = (*myIteration).length();
      if( numberOfIteration > 1 ){
	_mySlider->setRange((*myIteration)[0].dt,
			    (*myIteration)[numberOfIteration-1].dt);
	_myLab->show();
	_mySlider->show();
      }
      else{
	_ts = 0;
	_myLab->hide();
	_mySlider->hide();
      }
    }
  }
}

void SelectField::tsSelected()
{
  _ts = _mySlider->value();
  MESSAGE("File " << _file );
  MESSAGE("Mesh " << _mesh );
  MESSAGE("Field " << _field );
  MESSAGE("Time step " << _ts );
}

void SelectField::ClickOnOk()
{
  MESSAGE("click on Ok");
  accept();
}

void SelectField::ClickOnCancel()
{
  MESSAGE("click on Cancel");
  reject();
}

void SelectField::ClickOnHelp()
{
  MESSAGE("click on Help");
}

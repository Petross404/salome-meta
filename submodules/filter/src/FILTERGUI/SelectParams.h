//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#ifndef SELECTPARAMS_HEADER
#define SELECTPARAMS_HEADER

#include "utilities.h"

#include "SelectField.h"
#include <qwt_plot.h>
#include <vector>
#include <SalomeApp_Module.h>
#include <SUIT_Desktop.h>
#include <qdialog.h>

#include <SALOMEconfig.h>

class QLineEdit;
class QLabel;
class QButtonGroup;
class QGridLayout;
class QRadioButton;
class QPushButton;
class QGroupBox;
class QFrame;

class SelectParams: public QDialog
{
  Q_OBJECT

public:
  SelectParams(FilterGUI*,SelectField *sel,
	       const char* name = 0,
	       bool modal = FALSE,
	       WFlags fl = 0) throw(SALOME_Exception);
  virtual ~SelectParams();

protected:
  virtual void     buildFrame();
  virtual void     calcHisto();
  virtual void     displayHisto();
  virtual void     calcRateRed();
  virtual void     clearSThresh();
  virtual void     displayFThresh();
  virtual void     displaySThresh();
  virtual void     enableWidgets();

protected slots:
  virtual void     gradSelected();
  virtual void     enterSHisto();
  virtual void     updateHisto();
  virtual void     scaleSelected();
  virtual void     nbThreshSelected();
  virtual void     areaSelected();
  virtual void     enterFThresh();
  virtual void     enterSThresh();
  virtual void     moveThresh(const QMouseEvent &e);
  virtual void     getOutFileName();
  virtual void     process() throw(SALOME_Exception);
  virtual void     ClickOnCancel();
  virtual void     ClickOnHelp();

private:
  int _size;
  long _myHistoCurve, _myHistoFThresh, _myHistoSThresh;
  double _xmin, _xmax, _ymin, _ymax;
  double _fthresh, _sthresh;
  double *_x, *_y;
  double _xft[100], _yft[100], _xst[100], _yst[100];

  QLineEdit *_myLESH, *_myFThresh, *_myExpr, *_myLEST, *_myLEFT, *_myOFN;
  QButtonGroup *_myHSize, *_myFunc, *_myFScale, *_myNbThresh, *_myArea, *_myVThresh, *_myOutFile;
  QGridLayout *_myGroupLayout, *_myGroupLayout2, *_lay, *_GroupButtonsLayout;
  QRadioButton *_myFieldB, *_myGradB, *_myInt, *_myExt, *_myOneThresh, *_myTwoThresh, *_myLinear, *_myLog;
  QPushButton *_myHisto, *_myOFB, *_myProc, * _buttonCancel, * _buttonHelp;
  QGroupBox* _GroupC1, *_GroupC2, *_GroupButtons;
  QwtPlot *_myPlot;
  QLabel *_myLSH, *_myLFT, *_myLST, *_myLRR;
  QwtDiMap _qmap;
  QFrame *_fr;
  QString _inputFile, _inputMesh, _inputField;
  int _inputTS;
  SelectField *_sel;
  SALOME_FILTER::FILTER_Gen_ptr _filter;
};

#endif

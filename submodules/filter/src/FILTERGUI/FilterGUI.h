//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  FILTER FILTERGUI : FILTER component GUI implemetation
//  File   : FILTERGUI.h
//  Module : FILTER
//
#ifndef _FILTERGUI_H_
#define _FILTERGUI_H_

#include <SalomeApp_Module.h>
#include <SUIT_Desktop.h>

#include <SALOMEconfig.h>
#include CORBA_CLIENT_HEADER(FILTER_Gen)
#include CORBA_SERVER_HEADER(SALOMEDS_Attributes)

class SelectField;

class FilterGUI: public SalomeApp_Module
{
  Q_OBJECT

public:
  FilterGUI();

  virtual void initialize        ( CAM_Application* );
  virtual QString                engineIOR() const;
  virtual void windows( QMap<int, int>& mappa ) const;
  virtual bool OnGUIEvent        (int theCommandID);
  virtual bool OnKeyPress        (QKeyEvent* pe, SUIT_ViewWindow* );
  virtual bool OnMousePress      (QMouseEvent* pe, SUIT_ViewWindow* );
  virtual bool OnMouseMove       (QMouseEvent* pe, SUIT_ViewWindow* );

  void createFilterAction( const int, const QString&, const QString& = "" );
  void createPopupItem( const int, const QString&, const QString&, const QString& = "", const int = -1 );

  virtual void contextMenuPopup( const QString&, QPopupMenu*, QString& );

  SALOME_FILTER::FILTER_Gen_ptr InitFilterGen() const;

  static void setOrb();

  void EmitSignalCloseAllDialogs();

signals :
  void                        SignalCloseAllDialogs();

public slots:
  virtual bool                deactivateModule( SUIT_Study* );
  virtual bool                activateModule( SUIT_Study* );

protected:
private slots:
  void onGUIEvent();
  void onWindowActivated( SUIT_ViewWindow* );
  SelectField *_sel;

};

#endif

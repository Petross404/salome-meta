//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#ifndef SELECTFIELD_HEADER
#define SELECTFIELD_HEADER

#include "utilities.h"
#include "SalomeApp_Application.h"
#include <SalomeApp_Module.h>
#include <SUIT_Desktop.h>
#include <qdialog.h>
#include "FilterGUI.h"
#include "FILTERGUI_Utils.h"
#include CORBA_SERVER_HEADER(FILTER_Gen)

#include <SALOMEconfig.h>
class QListView;
class QListViewItem;
class QFrame;
class QSlider;
class QGridLayout;
class QGroupBox;
class QLabel;
class QPushButton;

class SelectField: public QDialog
{
  Q_OBJECT

public:
  SelectField(FilterGUI*,
	      const QString& file,
	      const char* name = 0,
	      bool modal = FALSE,
	      WFlags fl = 0);
  virtual ~SelectField();

  QString getFile() { return _file; }
  QString getMesh() { return _mesh; }
  QString getField() { return _field; }
  int getTimeStep() { return _ts; }
  SALOME_FILTER::FILTER_Gen_ptr getFilter() {return _filter;} /* current filter object */
  FilterGUI* myFilterGUI;              /* Current FilterGUI object */

protected:

protected slots:
  virtual void     fieldSelected(QListViewItem *lvi);
  virtual void     tsSelected();
  virtual void     ClickOnOk();
  virtual void     ClickOnCancel();
  virtual void     ClickOnHelp();

private:
  int _ts, _dimMesh;
  QGridLayout *_myGroupLayout, *_GroupButtonsLayout, *_lay;
  QGroupBox* _GroupC1, *_GroupButtons;
  QFrame *_fr;
  QString _file, _mesh, _field;
  QListView *_myList;
  QLabel *_myLab;
  QSlider *_mySlider;
  QPushButton* _buttonCancel, * _buttonHelp, * _buttonOk;
  SALOME_FILTER::FILTER_Gen_ptr _filter;
};

#endif

//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  MED MEDGUI_Selection
//  File   : FILTERGUI_Selection.h
//  Author : Alexander SOLOVYOV
//  Module : MED
//  $Header$
//
#ifndef FILTERGUI_SELECTION_HeaderFile
#define FILTERGUI_SELECTION_HeaderFile

#include "LightApp_Selection.h"

class FilterGUI_Selection : public LightApp_Selection
{
public:
  FilterGUI_Selection();
  virtual ~FilterGUI_Selection();
};

#endif

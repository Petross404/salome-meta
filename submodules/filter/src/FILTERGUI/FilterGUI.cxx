//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  FILTER FILTERGUI : FILTER component GUI implemetation
//  File   : FilterGUI.cxx
//  Module : FILTER
//
#include "utilities.h"
#include <FilterGUI_Selection.h>
#include "SelectParams.h"
#include "SelectField.h"
#include "FilterGUI.h"

// SALOME Includes
#include "Utils_ORB_INIT.hxx"
#include "Utils_SINGLETON.hxx"

#include <SALOME_LifeCycleCORBA.hxx>
#include <SALOME_InteractiveObject.hxx>
#include <SALOME_ListIO.hxx>
#include <SalomeApp_Tools.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Tools.h>
#include <SUIT_FileDlg.h>
#include <SUIT_ResourceMgr.h>

#include <CAM_Application.h>
#include <SalomeApp_Application.h>
#include <SalomeApp_DataModel.h>
#include <SalomeApp_Study.h>
#include <SALOMEDSClient_Study.hxx>
#include <SALOMEDSClient_SObject.hxx>

#include <LightApp_SelectionMgr.h>

#include <OB_Browser.h>

// QT Includes
#include <qinputdialog.h>
#include <qmessagebox.h>
#include <qcursor.h>

//VRV: porting on Qt 3.0.5
#if QT_VERSION >= 0x030005
#include <qlineedit.h>
#endif
//VRV: porting on Qt 3.0.5

using namespace std;

static CORBA::ORB_var   _orb;

//=============================================================================
/*!
 *
 */
//=============================================================================
FilterGUI::FilterGUI() :
  SalomeApp_Module( "FILTER" ),_sel(0)
{
}

//=============================================================================
/*!
 *
 */
//=============================================================================
void FilterGUI::createPopupItem( const int id,
                              const QString& clients,
                              const QString& types,
                              const QString& theRule,
			      const int pId )
{
  int parentId = pId;
  if( pId!=-1 )
    parentId = popupMgr()->actionId( action( pId ) );

  if( !popupMgr()->contains( popupMgr()->actionId( action( id ) ) ) )
    popupMgr()->insert( action( id ), parentId, 0 );

  QChar lc = QtxPopupMgr::Selection::defEquality();
  QString rule = "(%1)";
  if( !types.isEmpty() )
    rule += " and (%2) and (%3)";

  rule = rule.arg( QString( "client in {%1}" ).arg( clients ) );

  if( !types.isEmpty() )
  {
    rule = rule.arg( QString( "%1>0" ).arg( QtxPopupMgr::Selection::defSelCountParam() ) );
    rule = rule.arg( QString( "%1type in {%2}" ).arg( lc ).arg( types ) );
  }
  rule += theRule;
  popupMgr()->setRule( action( id ), rule, true );
}

void FilterGUI::createFilterAction( const int id, const QString& po_id, const QString& icon_id )
{
  QWidget* parent = application()->desktop();
  SUIT_ResourceMgr* mgr = application()->resourceMgr();

  QPixmap pix; QIconSet icon;
  if( !icon_id.isEmpty() )
    pix = mgr->loadPixmap( "FILTER", tr( icon_id ) );
//   else
//     pix = mgr->loadPixmap( "FILTER", tr( QString( "ICO_" )+po_id ) );

  if ( !pix.isNull() )
    icon = QIconSet( pix );

  createAction( id, tr( "TOP_" + po_id ), icon, tr( "MEN_" + po_id ), tr( "STB_" + po_id ), 0, parent, false, this, SLOT( onGUIEvent() ) );
}

//=============================================================================
/*!
 *
 */
//=============================================================================
void FilterGUI::initialize( CAM_Application* app )
{
  SalomeApp_Module::initialize( app );

//   QWidget* parent = application()->desktop();

  createFilterAction(  111, "OPEN" );

  createFilterAction( 936, "FILTERING" );

  int fileId   = createMenu( tr( "MEN_FILE" ),   -1,  1 );
  createMenu( 111, fileId, 11 );

  int FilterId = createMenu( tr( "DATA REDUCTION" ), -1, 50, 10 );
  createMenu( separator(), FilterId, 10 );
  createMenu( 936, FilterId, 11 );

}

void FilterGUI::contextMenuPopup( const QString& client, QPopupMenu* menu, QString& /*title*/ )
{
  FilterGUI_Selection sel;
  SalomeApp_Application* app = dynamic_cast<SalomeApp_Application*>( application() );
  if( app )
  {
    sel.init( client, app->selectionMgr() );
    popupMgr()->updatePopup( menu, &sel );
  }
}

//=============================================================================
/*!
 *
 */
//=============================================================================
SALOME_FILTER::FILTER_Gen_ptr FilterGUI::InitFilterGen() const
{
  SalomeApp_Application* app = dynamic_cast<SalomeApp_Application*>( application() );
  Engines::Component_var comp =
    SALOME_LifeCycleCORBA(app->namingService()).FindOrLoad_Component( "FactoryServer", "FILTER" );

  MESSAGE("_________________________________________");
  SALOME_FILTER::FILTER_Gen_var clr = SALOME_FILTER::FILTER_Gen::_narrow(comp);
  ASSERT(!CORBA::is_nil(clr));
  return clr._retn();
}

QString FilterGUI::engineIOR() const
{
  QString anIOR( "" );
  SALOME_FILTER::FILTER_Gen_ptr aFilterGen = InitFilterGen();
  if ( !CORBA::is_nil( aFilterGen) ){
    CORBA::String_var objStr = getApp()->orb()->object_to_string( aFilterGen );
    anIOR = QString( objStr.in() );
  }
  return anIOR;
}

void FilterGUI::windows( QMap<int, int>& mappa ) const
{
  mappa.insert( SalomeApp_Application::WT_ObjectBrowser, Qt::DockLeft );
  mappa.insert( SalomeApp_Application::WT_PyConsole, Qt::DockBottom );
}


//=============================================================================
/*!
 *
 */
//=============================================================================
void FilterGUI::onGUIEvent()
{
  const QObject* obj = sender();
  if ( !obj || !obj->inherits( "QAction" ) )
    return;
  int id = actionId((QAction*)obj);
  if ( id != -1 )
    OnGUIEvent( id );
}

//=============================================================================
/*!
 *
 */
//=============================================================================
void FilterGUI::EmitSignalCloseAllDialogs()
{
  emit SignalCloseAllDialogs();
}

//=============================================================================
/*!
 *
 */
//=============================================================================
bool FilterGUI::deactivateModule( SUIT_Study* study )
{
  setMenuShown( false );
  setToolShown( false );

  disconnect( application()->desktop(), SIGNAL( windowActivated( SUIT_ViewWindow* ) ),
	     this, SLOT( onWindowActivated( SUIT_ViewWindow* ) ) );

  EmitSignalCloseAllDialogs();

  return SalomeApp_Module::deactivateModule( study );
}

//=============================================================================
/*!
 *
 */
//=============================================================================
bool FilterGUI::activateModule( SUIT_Study* study )
{
  bool res = SalomeApp_Module::activateModule( study );

  setMenuShown( true );
  setToolShown( true );

  connect( application()->desktop(), SIGNAL( windowActivated( SUIT_ViewWindow* ) ),
	  this, SLOT( onWindowActivated( SUIT_ViewWindow* ) ) );
  return res;
}

//=============================================================================
/*!
 *
 */
//=============================================================================
void FilterGUI::onWindowActivated( SUIT_ViewWindow* )
{
}

//=============================================================================
/*!
 *
 */
//=============================================================================
bool FilterGUI::OnGUIEvent (int theCommandID)
{
  setOrb();

  SalomeApp_Study* myActiveStudy = dynamic_cast< SalomeApp_Study* >( application()->activeStudy() );
  if( !myActiveStudy )
    return false;

  _PTR(Study) aStudy = myActiveStudy->studyDS();
  //SALOME_NamingService* myNameService = parent->getNameService();

  QString file;
  QStringList filtersList ;

  filtersList.append( tr("FILTER_MEN_IMPORT_MED") );
  filtersList.append( tr("FILTER_MEN_ALL_FILES") ) ;

  SalomeApp_Application* app = dynamic_cast<SalomeApp_Application*>( application() );
  if( !app )
    return false;

  switch (theCommandID)
    {
    case 936:
      {
	MESSAGE("command " << theCommandID << " activated");

	if(_sel){
	  SelectParams *filter;
	  try {
	    application()->desktop()->setCursor(QCursor(Qt::WaitCursor));
	    filter = new SelectParams(this,_sel);
	    filter->exec();
	    application()->desktop()->setCursor(QCursor(Qt::ArrowCursor));
	    _sel = NULL;
	  }
	  catch ( SALOME_Exception& S_ex ) {
	    delete _sel;
	    _sel=NULL;
	    MESSAGE("Select an input Field in MED file before filtering!!");
	    QMessageBox::information( application()->desktop(),
				      "Filtering",
				      "Unable to select parameters for filtering.\n"
				      "You must select a reference field in a MED file before." );
	  }
	}
	else{
	  MESSAGE("Select an input Field in MED file before filtering!!");
	  QMessageBox::information( application()->desktop(),
				    "Filtering",
				    "Unable to select parameters for filtering.\n"
				    "You must select a reference field in a MED file before." );
	}
	break;
      }
    case 111:
      {
	MESSAGE("command " << theCommandID << " activated");

	// Selection du Fichier
	file = SUIT_FileDlg::getFileName(application()->desktop(),
					"",
					filtersList,
					tr("FILTER_MEN_IMPORT"),
					true);
	if (!file.isEmpty() ){
	  _sel = new SelectField(this,file);
	  if ( _sel->exec() == QDialog::Rejected ){
	    delete _sel;
	    _sel = NULL;
	  }
	}

        break;
      }
    }

  app->updateActions(); //SRN: To update a Save button in the toolbar

  return true;
}


//=============================================================================
/*!
 *
 */
//=============================================================================
bool FilterGUI::OnMousePress (QMouseEvent* pe ,
			   SUIT_ViewWindow* wnd )
{
  MESSAGE("FilterGUI::OnMousePress");
  return true;
}

//=============================================================================
/*!
 *
 */
//=============================================================================
bool FilterGUI::OnMouseMove (QMouseEvent* pe ,
			  SUIT_ViewWindow* wnd )
{
  //   MESSAGE("FilterGUI::OnMouseMouve");
  return true;
}

//=============================================================================
/*!
 *
 */
//=============================================================================
bool FilterGUI::OnKeyPress (QKeyEvent* pe,
			 SUIT_ViewWindow* wnd)
{
  MESSAGE("FilterGUI::OnKeyPress");
  return true;
}

//=============================================================================
/*!
 *
 */
//=============================================================================
void FilterGUI::setOrb()
{
  try {
    ORB_INIT &init = *SINGLETON_<ORB_INIT>::Instance();
    ASSERT(SINGLETON_<ORB_INIT>::IsAlreadyExisting());
    _orb = init( 0 , 0 );
  } catch (...) {
    INFOS("internal error : orb not found");
    _orb = 0;
  }
  ASSERT(! CORBA::is_nil(_orb));
}

extern "C" {
  Standard_EXPORT CAM_Module* createModule() {
    return new FilterGUI();
  }
}

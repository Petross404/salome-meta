//  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#include <stdlib.h>
#include <math.h>
#include "SelectParams.h"
#include "MEDMEM_MedMeshDriver.hxx"
#include "MEDMEM_EnsightMeshDriver.hxx"

#include <SUIT_FileDlg.h>

#include "Utils_SALOME_Exception.hxx"
#include <SalomeApp_Tools.h>

#include <qlabel.h>
#include <qgroupbox.h>
#include <qframe.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qpushbutton.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qcursor.h>

SelectParams::SelectParams(FilterGUI* theModule,SelectField *sel,
			   const char* name,
			   bool modal, WFlags fl) throw(SALOME_Exception)
  : QDialog(FILTER::GetDesktop( theModule ), name, modal, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu | Qt::WDestructiveClose),_size(1024),_sel(sel)
{
  MESSAGE("SelectParams constructor");
  if(sel){
    // read reference field values
    _filter = sel->getFilter();

    // Get reference field and time step
    _inputFile = sel->getFile();
    _inputMesh = sel->getMesh();
    _inputField = sel->getField();
    _inputTS = sel->getTimeStep();

    if( _inputFile.isEmpty() || _inputMesh.isEmpty() || _inputField.isEmpty())
      throw SALOME_Exception("Select an input Field in MED file before filtering!!");
    
    _filter->readReferenceField(_inputMesh,_inputField,_inputTS);

    // Build QT widgets
    buildFrame();
  }
  // if no reference field selection: throw exception
  else
    throw SALOME_Exception("Select an input Field in MED file before filtering!!");

  // Allocate histogram arrays
  _x = new double[_size];
  _y = new double[_size];

}

SelectParams::~SelectParams()
{
  cout << "SelectParams: destructor called" << endl;

  // destrcution of histogram arrays
  delete _x;
  delete _y;
  
  // destruction of SelectField object and Filter object in it
  delete _sel;
}

void SelectParams::buildFrame()
{
  // build widgets for select filtering parameters
  QGridLayout* _lay = new QGridLayout( this, 1, 2 );

  QGroupBox* _GroupC1 = new QGroupBox( this, "GroupC1" );
  _lay->addWidget( _GroupC1,0,0 );

  _GroupC1->setTitle( tr( "FILTER_PARAMS"  ) );
  _GroupC1->setColumnLayout(0, Qt::Vertical );
  _GroupC1->layout()->setSpacing( 0 );
  _GroupC1->layout()->setMargin( 0 );
  _myGroupLayout = new QGridLayout( _GroupC1->layout() );
  _myGroupLayout->setAlignment( Qt::AlignTop );
  _myGroupLayout->setSpacing( 6 );
  _myGroupLayout->setMargin( 11 );
  _myGroupLayout->setColStretch( 0, 0 );
  _myGroupLayout->setColStretch( 1, 1 );

  int row = 0;

  QString qs1(tr("FILTER_INPUT_FILE"));
  qs1.append(basename((char*)(_inputFile.ascii())));
  _myGroupLayout->addWidget( new QLabel( qs1, _GroupC1 ), row, 0 );
  row++;

  QString qs2(tr("FILTER_INPUT_MESH"));
  qs2.append(_inputMesh);
  _myGroupLayout->addWidget( new QLabel( qs2, _GroupC1 ), row, 0 );
  row++;

  QString qs3(tr("FILTER_INPUT_FIELD"));
  qs3.append(_inputField);
  _myGroupLayout->addWidget( new QLabel( qs3, _GroupC1 ), row, 0 );
  row++;

  QString qs4(tr("FILTER_INPUT_TS"));
  char strTS[128];
  sprintf(strTS,"%d\0",_inputTS);
  qs4.append(strTS);
  _myGroupLayout->addWidget( new QLabel( qs4, _GroupC1 ), row, 0 );
  row++;

  // 0)  field function to calculate histogram (radiogroup)
  _myFunc = new QButtonGroup( tr("FILTER_SELECT_FUNC"), _GroupC1 );
  _myFunc->setExclusive( true );
  _myFunc->setColumnLayout( 0, Qt::Horizontal );

  _myFieldB = new QRadioButton( tr("FILTER_FIELD"), _myFunc );
  _myFieldB->setChecked(true);

  QGridLayout* convLay = new QGridLayout( _myFunc->layout() );
  convLay->addWidget( _myFieldB, 0, 0 );
  convLay->addWidget( _myGradB = new QRadioButton( tr("FILTER_GRADIENT"), _myFunc ), 0, 1 );
  _myGroupLayout->addWidget( _myFunc, row, 0 );
  row++;

  // 01)  histogram size (line edit)
  _myHSize = new QButtonGroup( tr(""), _GroupC1 );
  _myHSize->setExclusive( true );
  _myHSize->setColumnLayout( 0, Qt::Horizontal );
  QGridLayout* shLay = new QGridLayout( _myHSize->layout() );
  shLay->addWidget( _myLSH = new QLabel( tr("FILTER_SIZE_HISTO") , _myHSize ), 0, 0 );
  shLay->addWidget( _myLESH = new QLineEdit( "1024", _myHSize ), 0, 1 );
  _myGroupLayout->addWidget( _myHSize, row, 0 );
  row++;

  // 1)  display histogram button (pushbutton)
  _myHisto = new QPushButton( "", _GroupC1 );
  _myHisto->setText(tr("FILTER_DISPLAY_HISTO"));
  _myHisto->setAutoDefault(FALSE);
  _myGroupLayout->addWidget( _myHisto, row, 0 );
  row++;

  // 2)  scale of histogram (radiogroup)
  _myFScale = new QButtonGroup( tr("FILTER_TYPE_DISPLAY"), _GroupC1 );
  _myFScale->setExclusive( true );
  _myFScale->setColumnLayout( 0, Qt::Horizontal );

  _myLinear = new QRadioButton( tr("FILTER_LINEAR"), _myFScale );
  _myLinear->setChecked(true);
  _myLog = new QRadioButton( tr("FILTER_LOG"), _myFScale );
  _myFScale->setDisabled(true);

  QGridLayout* scaleLay = new QGridLayout( _myFScale->layout() );
  scaleLay->addWidget( _myLinear, 0, 0 );
  scaleLay->addWidget( _myLog, 0, 1 );
  _myGroupLayout->addWidget( _myFScale, row, 0 );
  row++;

  // 3)  number of thresholds (radiogroup)
  _myNbThresh = new QButtonGroup( tr("FILTER_SEL_THRESH"), _GroupC1 );
  _myNbThresh->setExclusive( true );
  _myNbThresh->setColumnLayout( 0, Qt::Horizontal );
  QGridLayout* nbtLay = new QGridLayout( _myNbThresh->layout() );
  nbtLay->addWidget( _myOneThresh = new QRadioButton( tr("FILTER_ONE_THRESH"), _myNbThresh ), 0, 0 );
  nbtLay->addWidget( _myTwoThresh = new QRadioButton( tr("FILTER_TWO_THRESH"), _myNbThresh ), 0, 1 );
  _myGroupLayout->addWidget( _myNbThresh, row, 0 );

  _myOneThresh->setChecked(true);
  _myNbThresh->setDisabled(true);
  row++;

  // 4)  reference area on thresholds (radiogroup)
  _myArea = new QButtonGroup( tr("FILTER_REF_AREA"), _GroupC1 );
  _myArea->setExclusive( true );
  _myArea->setColumnLayout( 0, Qt::Horizontal );
  QGridLayout* areaLay = new QGridLayout( _myArea->layout() );
  areaLay->addWidget( _myInt = new QRadioButton( tr("FILTER_BOTTOM"), _myArea ), 0, 0 );
  areaLay->addWidget( _myExt = new QRadioButton( tr("FILTER_UP"), _myArea ), 0, 1 );
  _myGroupLayout->addWidget( _myArea, row, 0 );

  _myExt->setChecked(true);
  _myArea->setDisabled(true);
  row++;

  // 5)  threshold values (line edit)
  _myVThresh = new QButtonGroup( tr("FILTER_TRESH_VAL"), _GroupC1 );
  _myVThresh->setExclusive( true );
  _myVThresh->setColumnLayout( 0, Qt::Horizontal );
  QGridLayout* ftLay = new QGridLayout( _myVThresh->layout() );
  ftLay->addWidget( _myLFT = new QLabel( tr("FILTER_VAL_TRESH") , _myVThresh ), 0, 0 );
  ftLay->addWidget( _myLEFT = new QLineEdit( "", _myVThresh ), 0, 1 );
  ftLay->addWidget( _myLST = new QLabel( tr("FILTER_VAL_2_TRESH") , _myVThresh ), 1, 0 );
  ftLay->addWidget( _myLEST = new QLineEdit( "", _myVThresh ), 1, 1 );
  _myGroupLayout->addWidget( _myVThresh, row, 0 );

  _myVThresh->setDisabled(true);
  _myLST->hide();
  _myLEST->hide();
  row++;

  // 6)  output file name (line edit)
  _myOutFile = new QButtonGroup( tr("FILTER_OUT_FILE"), _GroupC1 );
  _myOutFile->setExclusive( true );
  _myOutFile->setColumnLayout( 0, Qt::Horizontal );

  _myOFB = new QPushButton( "", _myOutFile );
  _myOFB->setText(tr("FILTER_BROWSE"));
  _myOFB->setAutoDefault(FALSE);

  QGridLayout* outLay = new QGridLayout( _myOutFile->layout() );
  outLay->addWidget( _myOFB, 0, 0 );
  outLay->addWidget( _myOFN = new QLineEdit( "", _myOutFile ), 0, 1 );
  _myGroupLayout->addWidget( _myOutFile, row, 0 );

  _myOutFile->setDisabled(true);
  row++;

  // 8)  buttons Process, Close and Help 
  _GroupButtons = new QGroupBox(_GroupC1, "GroupButtons");
  _GroupButtons->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, _GroupButtons->sizePolicy().hasHeightForWidth()));
  _GroupButtons->setTitle(tr("" ));
  _GroupButtons->setColumnLayout(0, Qt::Vertical);
  _GroupButtons->layout()->setSpacing(0);
  _GroupButtons->layout()->setMargin(0);
  _GroupButtonsLayout = new QGridLayout(_GroupButtons->layout());
  _GroupButtonsLayout->setAlignment(Qt::AlignTop);
  _GroupButtonsLayout->setSpacing(6);
  _GroupButtonsLayout->setMargin(11);
  _myProc = new QPushButton(_GroupButtons, "buttonProcess");
  _myProc->setText(tr("FILTER_PROCESS"));
  _myProc->setAutoDefault(FALSE);
  _GroupButtonsLayout->addWidget(_myProc, 0, 0);
  _buttonHelp = new QPushButton(_GroupButtons, "buttonHelp");
  _buttonHelp->setText(tr("FILTER_BUT_HELP" ));
  _buttonHelp->setAutoDefault(FALSE);
  _GroupButtonsLayout->addWidget(_buttonHelp, 0, 2);
  _buttonCancel = new QPushButton(_GroupButtons, "buttonCancel");
  _buttonCancel->setText(tr("FILTER_BUT_CANCEL" ));
  _buttonCancel->setAutoDefault(FALSE);
  _GroupButtonsLayout->addWidget(_buttonCancel, 0, 1);
  _myGroupLayout->addWidget( _GroupButtons, row, 0 );
  _GroupButtons->setDisabled(true);
  row++;

  _GroupC2 = new QGroupBox( this, "GroupC2" );
  _lay->addWidget( _GroupC2,0,1 );

  _GroupC2->setTitle( tr( "FILTER_HISTO" ) );
  _GroupC2->setColumnLayout(0, Qt::Vertical );
  _GroupC2->layout()->setSpacing( 0 );
  _GroupC2->layout()->setMargin( 0 );
  _myGroupLayout2 = new QGridLayout( _GroupC2->layout() );

  // 9)  histogram curve
  _myPlot = new QwtPlot(_GroupC2);
  _myHistoCurve = _myPlot->insertCurve( QString() );
  _myPlot->setCurvePen( _myHistoCurve, QPen( Qt::red, 1 ) );
  _myPlot->setCurveTitle( _myHistoCurve, "Histogram" );

  _myGroupLayout2->addWidget( _myPlot, 0, 0 );

  // 10)  reduction rate (label)
  QString qs5(tr("FILTER_RED_RATE"));
  qs5.append(" = 0.5");
  _myLRR = new QLabel( qs5, _GroupC2 );
  _myGroupLayout2->addWidget( _myLRR, 1, 0 );

  _GroupC2->hide();

  _myHistoFThresh = _myPlot->insertCurve( QString() );
  _myPlot->setCurvePen( _myHistoFThresh, QPen( Qt::black, 1 ) );
  _myHistoSThresh = _myPlot->insertCurve( QString() );
  _myPlot->setCurvePen( _myHistoSThresh, QPen( Qt::black, 1 ) );

  connect( _myGradB, SIGNAL(clicked()), this, SLOT(gradSelected()));
  connect( _myLESH, SIGNAL(returnPressed()), this, SLOT(enterSHisto()));
  connect( _myHisto, SIGNAL(clicked()), this, SLOT(updateHisto()));
  connect( _myLinear, SIGNAL(clicked()), this, SLOT(scaleSelected()));
  connect( _myLog, SIGNAL(clicked()), this, SLOT(scaleSelected()));
  connect( _myOneThresh, SIGNAL(clicked()), this, SLOT(nbThreshSelected()));
  connect( _myTwoThresh, SIGNAL(clicked()), this, SLOT(nbThreshSelected()));
  connect( _myInt, SIGNAL(clicked()), this, SLOT(areaSelected()));
  connect( _myExt, SIGNAL(clicked()), this, SLOT(areaSelected()));
  connect( _myLEFT, SIGNAL(returnPressed()), this, SLOT(enterFThresh()));
  connect( _myLEST, SIGNAL(returnPressed()), this, SLOT(enterSThresh()));
  connect( _myPlot, SIGNAL(plotMouseMoved(const QMouseEvent &)), this, SLOT(moveThresh(const QMouseEvent &)));
  connect( _myOFB, SIGNAL(clicked()), this, SLOT(getOutFileName()));
  connect( _myProc, SIGNAL(clicked()), this, SLOT(process()));
  connect(_buttonCancel, SIGNAL(clicked()), this, SLOT(ClickOnCancel()));
  connect(_buttonHelp, SIGNAL(clicked()),   this, SLOT(ClickOnHelp()));

  _GroupC2->setMinimumSize( 500, 500 );
  _GroupC2->setMaximumSize( 500, 500 );
  setMinimumSize( 750, 500 );
  setMaximumSize( 750, 500 );

}

void SelectParams::gradSelected()
{
  try{
    setCursor(QCursor(Qt::WaitCursor));
    _filter->buildGradient();
    setCursor(QCursor(Qt::ArrowCursor));
  }
  catch(SALOME_FILTER::FILTER_Gen::FilterError){
    _myFieldB->setChecked(true);
    QMessageBox::information( this,
			      "Filtering",
			      "Unable to calculate gradient on vector field.\n"
			      "You must select a reference scalar field." );
  }
  catch(...){
    MESSAGE("unknownException");
  }
}

void SelectParams::enterSHisto()
{
  // Deallocate old histogram arrays
  delete _x;
  delete _y;

  // get new histogram size
  _size = atoi(_myLESH->text());

  // Allocate new histogram arrays
  _x = new double[_size];
  _y = new double[_size];
}

void SelectParams::scaleSelected()
{
  // draw linear or log Y scale depend on user
  if( _myLinear->isChecked() ){
    _ymin = 0.0;
    _myPlot->setAxisOptions(_myPlot->curveYAxis( _myHistoCurve ), QwtAutoScale::None );
  }
  else{
    // set min to 0.1 for log scale
    _ymin = 0.1;
    _myPlot->setAxisOptions(_myPlot->curveYAxis( _myHistoCurve ), QwtAutoScale::Logarithmic );
  }
  _myPlot->setAxisScale( _myPlot->curveYAxis( _myHistoCurve ), _ymin, _ymax );
  _myPlot->replot();
}

void SelectParams::nbThreshSelected()
{
  if( _myOneThresh->isChecked() ){
    // if one threshold choice between bottom and up for reference area
    _myInt->setText(tr("FILTER_BOTTOM"));
    _myExt->setText(tr("FILTER_UP"));
    _myLFT->setText(tr("FILTER_VAL_TRESH"));
    _myLST->hide();
    _myLEST->hide();
    // draw first threshold
    displayFThresh();
    // erase second threshold
    clearSThresh();
  }
  else{
    // if two thresholds choice between interior and exterior fir reference area
    _myInt->setText(tr("FILTER_INT"));
    _myExt->setText(tr("FILTER_EXT"));
    _myLFT->setText(tr("FILTER_VAL_1_TRESH"));
    if(_myLST->isHidden())
      _myLST->show();
    if(_myLEST->isHidden())
      _myLEST->show();
    // draw two thresholds
    displayFThresh();
    displaySThresh();
  }
  calcRateRed();
}

void SelectParams::areaSelected()
{
  // calculate reduction rate after thresholds selection
  calcRateRed();
}

void SelectParams::enterFThresh()
{
  displayFThresh();
  calcRateRed();
}

void SelectParams::enterSThresh()
{
  displaySThresh();
  calcRateRed();
}

void SelectParams::calcHisto()
{
  char strth[128];
  CORBA::Double min, max;
  CORBA::Long size = _size;
  SALOME_FILTER::LongSeq* histo;

  if( _myFieldB->isChecked() ){
    // calculate histogram values on field
    _filter->getMinMax(min,max,SALOME_FILTER::F_FIELD);
    histo = _filter->getHistogram(size,SALOME_FILTER::F_FIELD);
  }
  else{
    // calculate histogram values on gradient
     _filter->getMinMax(min,max,SALOME_FILTER::F_GRAD);
    histo = _filter->getHistogram(size,SALOME_FILTER::F_GRAD);
  }
  _xmin = min;
  _xmax = max;
  vector<int> myh(_size);
  for(int i=0;i<_size;i++)
    myh[i] = (*histo)[i];

  if( _myLinear->isChecked() )
    _ymin = 0.0;
  else
    _ymin = 0.1;
  _ymax = 0.0;

  for(int i=0;i<_size;i++){
    // calculate absisses for histogram values
    _x[i]=_xmin+(i*(_xmax-_xmin))/_size;
    // set zero to 0.01 because pb of zeros in log display with qwt
    if(myh[i])
      _y[i]=(double)myh[i];
    else
      _y[i]=0.01;
    if( _y[i] > _ymax )
      _ymax = _y[i];
  }

  // init thresholds values
  _fthresh = (_xmin + _xmax)/2.0;
  _sthresh = (_xmin + 3.*_xmax)/4.0;
  sprintf(strth,"%g",_fthresh);
  _myLEFT->setText(QString(strth));
  sprintf(strth,"%g",_sthresh);
  _myLEST->setText(QString(strth));
}

void SelectParams::displayHisto()
{
  // associate values to curve
  _myPlot->setCurveData( _myHistoCurve, _x, _y, _size );
  // give extrema values for each axis
  _myPlot->setAxisScale( _myPlot->curveXAxis( _myHistoCurve ), _xmin, _xmax );
  _myPlot->setAxisScale( _myPlot->curveYAxis( _myHistoCurve ), _ymin, _ymax );
  if( _myLinear->isChecked() )
    _myPlot->setAxisOptions(_myPlot->curveYAxis( _myHistoCurve ), QwtAutoScale::None );
  else
    _myPlot->setAxisOptions(_myPlot->curveYAxis( _myHistoCurve ), QwtAutoScale::Logarithmic );
  // associate mapping to plot to move thresholds on display
  _qmap = _myPlot->canvasMap(_myPlot->curveXAxis( _myHistoCurve ));
  _qmap.setDblRange(_xmin,_xmax);
  _myPlot->replot();
}

void SelectParams::enableWidgets()
{
  if(_GroupC2->isHidden()){
    _myFScale->setEnabled(true);
    _myNbThresh->setEnabled(true);
    _myArea->setEnabled(true);
    _myVThresh->setEnabled(true);
    _myOutFile->setEnabled(true);
    _GroupButtons->setEnabled(true);
    _GroupC2->show();
  }
}

void SelectParams::updateHisto()
{
  calcHisto();
  displayHisto();
  displayFThresh();
  if( _myTwoThresh->isChecked() )
    displaySThresh();
  calcRateRed();
  enableWidgets();
}

void SelectParams::displayFThresh()
{
  _fthresh = atof(_myLEFT->text());

  // draw first threshold curve
  for(int i=0;i<100;i++){
    _xft[i]=_fthresh;
    _yft[i]=((i-1)*_ymax)/100.0;
  }
  _myPlot->setCurveData( _myHistoFThresh, _xft, _yft, 100 );
  _myPlot->replot();
}

void SelectParams::displaySThresh()
{
  _sthresh = atof(_myLEST->text());

  // draw second threshold curve
  for(int i=0;i<100;i++){
    _xst[i]=_sthresh;
    _yst[i]=((i-1)*_ymax)/100.0;
  }
  _myPlot->setCurveData( _myHistoSThresh, _xst, _yst, 100 );
  _myPlot->replot();
}

void SelectParams::clearSThresh()
{
  _sthresh = atof(_myLEST->text());

  // erase second threshold curve
  for(int i=0;i<100;i++){
    _xst[i]=_sthresh;
    _yst[i]=0.0;
  }
  _myPlot->setCurveData( _myHistoSThresh, _xst, _yst, 100 );
  _myPlot->replot();
}

void SelectParams::moveThresh(const QMouseEvent &e)
{
  char strth[128];

  // move threshold curve with mouse
  int delta = abs(e.x()-_qmap.transform(_fthresh));
  if( delta < 5 ){
    // moving first threshold curve
    sprintf(strth,"%g",_qmap.invTransform(e.x()));
    _myLEFT->setText(QString(strth));
    displayFThresh();
  }
  else if( _myTwoThresh->isChecked() ){
    delta = abs(e.x()-_qmap.transform(_sthresh));
    if( delta < 5 ){
      // moving second threshold curve
      sprintf(strth,"%g",_qmap.invTransform(e.x()));
      _myLEST->setText(QString(strth));
      displaySThresh();
    }
  }
  calcRateRed();
}

void SelectParams::getOutFileName()
{
  // get output MED file name
  QString file = QFileDialog::getSaveFileName("",
					      "*.med",
					      _myOFB,
					      "save file dialog",
					      "Choose a file to save" );
  if(!file.isEmpty())
    _myOFN->setText(file);

  // Selection du Fichier
//   file = SUIT_FileDlg::getFileName(application()->desktop(),
// 				   "",
// 				   filtersList,
// 				   "Output MED file name",
// 				   true);
}

void SelectParams::process() throw(SALOME_Exception)
{
  int nbthresh;
  string command;

  MESSAGE("Input MED File : "<<_inputFile);
  MESSAGE("Input Mesh : "<<_inputMesh);
  MESSAGE("Input Field : "<<_inputField);
  MESSAGE("Input Time Step : "<<_inputTS);
  MESSAGE("Output file name: " << _myOFN->text() );

  setCursor(QCursor(Qt::WaitCursor));

  MESSAGE("Generate Criteria");
  // generate MED integer field (0 or 1) for filtoo input
  try{
    if( _myOneThresh->isChecked() )
      nbthresh = 1;
    else
      nbthresh = 2;
    SALOME_FILTER::ref_func rf;
    if(_myFieldB->isChecked())
      rf = SALOME_FILTER::F_FIELD;
    else
      rf = SALOME_FILTER::F_GRAD;
    _filter->generateCriteria(nbthresh,_fthresh,_sthresh,_myExt->isChecked(),rf);
  }
  catch (SALOME_FILTER::FILTER_Gen::FilterError){
    QMessageBox::information( this,
			      "Filtering",
			      "Unable to process filtering.\n"
			      "You must select a reference field on nodes." );
    reject();
    return;
  }

  // create ensight input files to filtoo
  try{
    _filter->createEnsightInputFiles();
  }
  catch (SALOME_FILTER::FILTER_Gen::FilterError& ex){
    throw SALOME_Exception(ex.error_msg);
  }

  // call filtoo
  try{
    _filter->filtering();
  }
  catch (SALOME_FILTER::FILTER_Gen::FilterError){
    QMessageBox::information( this,
			      "Filtering",
			      "Unable to process filtering.\n"
			      "FILTOO ERROR see /tmp/filter.log file." );
    reject();
    return;
  }
 
  // project input fields on new mesh
  try{
    _filter->projectFieldsOnDecimateMesh();
  }
  catch (SALOME_FILTER::FILTER_Gen::FilterError& ex){
    throw SALOME_Exception(ex.error_msg);
  }

  // create new MED file with new mesh and fields
  _filter->createMedOutputFile(_myOFN->text());

  QMessageBox::information( this,
			    "Filtering",
			    "Filtering done.\n"
			    "" );
  // close the window
  accept();

}

void SelectParams::calcRateRed()
{
  int i1, i2, i;
  int atot=0, asel=0, atot1;
  double rateRed=0.0;

  // calculate reduction rate depend on reference area defined by threshold values
  i1 = (int)((double)_size * ( _fthresh - _xmin ) / ( _xmax - _xmin ));
  if( _myOneThresh->isChecked() ){
    for(i=0;i<i1;i++)
      atot += (int)_y[i];
    asel = atot;
    for(i=i1;i<_size;i++)
      atot += (int)_y[i];
    if( _myExt->isChecked() )
      asel = atot - asel;
  }
  else{
    i2 = (int)((double)_size * ( _sthresh - _xmin ) / ( _xmax - _xmin ));
    if( i2 < i1 ){
      i=i1;
      i1=i2;
      i2=i;
    }
    for(i=0;i<i1;i++)
      atot += (int)_y[i];
    atot1=atot;
    for(i=i1;i<i2;i++)
      atot += (int)_y[i];
    asel = atot - atot1;
    for(i=i2;i<_size;i++)
      atot += (int)_y[i];
    if( _myExt->isChecked() )
      asel = atot - asel;
  }
  rateRed = (double)asel / (double) atot;

  // display reduction rate value
  QString qs(tr("FILTER_RED_RATE"));
  char str[128];
  sprintf(str," = %4.2g",rateRed);
  qs.append(str);
  _myLRR->setText( qs );

}

void SelectParams::ClickOnCancel()
{
  MESSAGE("click on Cancel");
  reject();
}

void SelectParams::ClickOnHelp()
{
  MESSAGE("click on Help");
}

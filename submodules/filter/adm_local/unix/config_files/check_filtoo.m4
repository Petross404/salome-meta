dnl  Copyright (C) 2007-2008  CEA/DEN, EDF R&D
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public
dnl  License as published by the Free Software Foundation; either
dnl  version 2.1 of the License.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free Software
dnl  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
dnl
dnl  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
dnl
AC_DEFUN([CHECK_FILTOO],[

filtoo_ok=no

AC_CHECK_PROG(FILTOO, filtoo,yes,no)

if test "x$FILTOO" == xno ; then
  AC_MSG_WARN(filtoo program not found in PATH variable)
else
  AC_CHECK_PROG(FILGHS3D, filghs3d,yes,no)

  if test "x$FILGHS3D" == xno ; then
    AC_MSG_WARN(filghs3d program not found in PATH variable)
  else
    AC_CHECK_PROG(FILYAMS, filyams,yes,no)

    if test "x$FILYAMS" == xno ; then
      AC_MSG_WARN(filyams program not found in PATH variable)
    else
      filtoo_ok=yes
   fi
 fi
fi

AC_MSG_RESULT(for filtoo: $filtoo_ok)

])dnl

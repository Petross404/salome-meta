// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Prs3dDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//
#ifndef VISUGUI_PRS3DDLG_H
#define VISUGUI_PRS3DDLG_H

#include <QDialog>

#include "SALOME_GenericObjPointer.hh"
#include "VISU_ColoredPrs3d_i.hh"

class QButtonGroup;
class QGroupBox;
class QLabel;
class QCheckBox;
class QPushButton;
class QRadioButton;
class QLineEdit;
class QComboBox;
class QToolButton;
class QTabWidget;

class SalomeApp_DoubleSpinBox;

class SalomeApp_Module;
class SalomeApp_IntSpinBox;
class SVTK_FontWidget;
class VISU_ScalarMapAct;
class VISU_ScalarMapPL;

namespace VISU
{
  class ColoredPrs3d_i;
  class ScalarMap_i;
}


//----------------------------------------------------------------------------
class VisuGUI_TextPrefDlg: public QDialog
{
  Q_OBJECT;

 public:
  VisuGUI_TextPrefDlg (QWidget* parent);
  ~VisuGUI_TextPrefDlg() {};

  QString getTitleText();
  void setTitleText( const QString& theText);

  void setTitleVisible( bool isVisible );

  void storeBeginValues();

 private:
  void keyPressEvent( QKeyEvent* e );

 public:
  SVTK_FontWidget* myTitleFont;
  SVTK_FontWidget* myLabelFont;

 protected slots:
  void accept();
  void reject();
  void onHelp();

 private:
  QLineEdit* myTitleEdt;
  QString    myTitle;
  QColor     myColors[2];
  int        myComboVals[2];
  bool       myCheckVals[6];
};


//----------------------------------------------------------------------------
class VisuGUI_BarPrefDlg: public QDialog
{
  Q_OBJECT;

 public:
  VisuGUI_BarPrefDlg (QWidget* parent);
  ~VisuGUI_BarPrefDlg() {};

  void setRatios(int titleRatioSize, int labelRatioWidth,
                 int barRatioWidth, int barRatioHeight);
  void getRatios(int& titleRatioSize, int& labelRatioWidth,
                 int& barRatioWidth, int& barRatioHeight);

  void setUnitsVisible(bool isVisible);
  bool isUnitsVisible();

  void setLabelsPrecision( const int p );
  int getLabelsPrecision() const;

  void setOrientation( const int ori );
  int getOrientation() const;


 private:
  void keyPressEvent( QKeyEvent* e );

 signals:
  void updatePreview();

 protected slots:
  void accept();
  void reject();
  void onHelp();

 protected:
  SalomeApp_IntSpinBox*  myTitleSizeSpin;
  SalomeApp_IntSpinBox*  myLabelSizeSpin;
  SalomeApp_IntSpinBox*  myBarWidthSpin;
  SalomeApp_IntSpinBox*  myBarHeightSpin;
  QCheckBox* myUnitsChk;  
  SalomeApp_IntSpinBox*  myPrecisionSpin;

  int        myTitleSize;
  int        myLabelSize;
  int        myBarWidth;
  int        myBarHeight;
  bool       myUnits;
  int        myPrecision;
  int        myOrientation;
};


//----------------------------------------------------------------------------
class VisuGUI_ScalarBarPane : public QWidget//QVBox
{
  Q_OBJECT;

 public:
  VisuGUI_ScalarBarPane(QWidget* parent, bool theIsDisplayGaussMetric = false, bool thePreview = FALSE);
  ~VisuGUI_ScalarBarPane();

  void    setRange( double imin, double imax, bool sbRange );
  void    setDefaultRange(double imin, double imax);
  int     getOrientation();
  void    setPosAndSize( double x, double y, double w, double h, bool vert );
  void    setScalarBarData( int colors, int labels );
  bool    isIRange();
  double  getMin();
  double  getMax();
  double  getX();
  double  getY();
  double  getWidth();
  double  getHeight();
  int     getNbColors();
  int     getNbLabels();
  bool    isLogarithmic();
  void    setLogarithmic( bool on );
  bool    isShowDistribution();
  void    setShowDistribution( bool on );
  bool    isToSave();

  void storeToResources();
  void initFromPrsObject(VISU::ColoredPrs3d_i* thePrs);

  int storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

  bool check();

 protected:
  QButtonGroup*   RangeGroup;
  QRadioButton*   RBFrange;
  QRadioButton*   RBIrange;
  QLineEdit*      MinEdit;
  QLineEdit*      MaxEdit;

  QRadioButton*   RBhori;
  QRadioButton*   RBvert;

  SalomeApp_DoubleSpinBox*  XSpin;
  SalomeApp_DoubleSpinBox*  YSpin;

  SalomeApp_DoubleSpinBox*  WidthSpin;
  SalomeApp_DoubleSpinBox*  HeightSpin;

  SalomeApp_IntSpinBox* ColorSpin;
  SalomeApp_IntSpinBox* LabelSpin;

  QCheckBox*      CBSave;
  QCheckBox*      CBLog;
  QCheckBox*      CBDistr;
  QCheckBox*      myHideBar;

  QLabel*         myModeLbl;
  QComboBox*      myModeCombo;
  QLabel*         myGaussMetricLabel;
  QComboBox*      myGaussMetric;
  QPushButton*    myTextBtn;
  QPushButton*    myBarBtn;
  VisuGUI_TextPrefDlg* myTextDlg;
  VisuGUI_BarPrefDlg* myBarDlg;

  double          myHorX, myHorY, myHorW, myHorH;
  double          myVerX, myVerY, myVerW, myVerH;
  int             myHorTS, myHorLS, myHorBW, myHorBH;
  int             myVerTS, myVerTH, myVerLS, myVerBW, myVerBH;
  bool            myIsStoreTextProp;

 private slots:
  void changeDefaults( int );
 void changeRange( int );
 void onFieldRange( bool );
 void onImposedRange( bool );
  void XYChanged( double );
  void changeScalarMode( int );
  void changeGaussMetric( int );
  void onTextPref();
  void onBarPref();
  void onPreviewCheck(bool thePreview);
  void updatePreview();  
  void onShowDistribution(bool);

 private:
  void createScalarBar();
  void deleteScalarBar();

  QCheckBox*         myPreviewCheck;
  VISU_ScalarMapAct* myPreviewActor;
  VISU::ScalarMap_i* myScalarMap;
  VISU_ScalarMapPL*  myScalarMapPL;
  std::string        myTitle;

  bool myBusy;
  bool myIsDisplayGaussMetric;
};


//----------------------------------------------------------------------------
class VisuGUI_Prs3dDlg : public QDialog
{
  Q_OBJECT;

 public:
  VisuGUI_Prs3dDlg( SalomeApp_Module* theModule );
  ~VisuGUI_Prs3dDlg() {}

  virtual void     initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                      bool theInit ) = 0;

  virtual int      storeToPrsObject( VISU::ColoredPrs3d_i* thePrs ) = 0;

 protected:
  virtual QString  GetContextHelpFilePath() = 0;

 protected slots:
  void onHelp();

 private:
  void keyPressEvent( QKeyEvent* e );
};


//----------------------------------------------------------------------------
class VisuGUI_ScalarBarBaseDlg : public VisuGUI_Prs3dDlg
{
  Q_OBJECT;

 public:
  VisuGUI_ScalarBarBaseDlg( SalomeApp_Module* theModule, bool theIsDisplayGaussMetric = false, bool thePreview = FALSE );
  ~VisuGUI_ScalarBarBaseDlg();

  virtual void     initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                      bool theInit );
 protected slots:
  void accept();
  void reject();

 protected:
  VisuGUI_ScalarBarPane* GetScalarPane();

 private:
  VisuGUI_ScalarBarPane* myScalarPane;
};


//----------------------------------------------------------------------------

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_GaussPointsSelectionPane.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#include "VisuGUI_GaussPointsSelectionPane.h"
#include "VisuGUI_ViewTools.h"

#include "VISU_GaussPtsAct.h"
#include "VISU_GaussPtsSettings.h"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_PickingSettings.h"

#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include "CAM_Module.h"

#include "SVTK_Selector.h"
#include "SVTK_ViewWindow.h"
//#include "SVTK_MainWindow.h"
#include "SVTK_RenderWindowInteractor.h"

#include "VTKViewer_Algorithm.h"
#include "SVTK_Functor.h"

#include <vtkActorCollection.h>
#include <vtkCallbackCommand.h>
#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkGenericRenderWindowInteractor.h>
#include <vtkSmartPointer.h>

#include "utilities.h"

#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QKeyEvent>
#include <QIntValidator>
#include <QToolButton>

#include "LightApp_Application.h"

namespace
{
  struct SelectorHelper
  {
  public:
    SelectorHelper( const SalomeApp_Module* theModule ):
      myModule( theModule )
    {}

    bool
    get()
    {
      bool aResult = false;
      myMapIndex.Clear();
      mySelector = NULL;
      myPipeLine = NULL;
      myActor = NULL;

      SVTK_RenderWindowInteractor* anInteractor = NULL;
      if( SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>( myModule ) )
        //if( SVTK_MainWindow* aMainWindow = aViewWindow->getMainWindow() )
          anInteractor = aViewWindow->GetInteractor();

      if ( !anInteractor )
        return aResult;
      
      mySelector = anInteractor->GetSelector();
      if ( !mySelector )
        return aResult;
      
      const SALOME_ListIO& aListIO = mySelector->StoredIObjects();
      if ( aListIO.Extent() != 1 ) 
        return aResult;
      
      myIO = aListIO.First();
      if ( mySelector->HasIndex( myIO ) )
        mySelector->GetIndex(myIO, myMapIndex);
        
      VTK::ActorCollectionCopy aCopy(anInteractor->getRenderer()->GetActors());
      myActor = SVTK::Find<VISU_GaussPtsAct>(aCopy.GetActors(),
                                             SVTK::TIsSameIObject<VISU_GaussPtsAct>( myIO ));
      if ( !myActor )
        return aResult;
      
      myPipeLine = myActor->GetGaussPointsPL();

      return true;
    }

    const SalomeApp_Module* myModule;
    TColStd_IndexedMapOfInteger myMapIndex;
    Handle(SALOME_InteractiveObject) myIO;
    SVTK_Selector* mySelector;

    VISU_GaussPointsPL* myPipeLine;
    VISU_GaussPtsAct* myActor;
  };


  
  class GaussPtsIDValidator: public QIntValidator
  {
  public:
    GaussPtsIDValidator( const SalomeApp_Module* theModule,
                         QObject * theParent ):
      QIntValidator( 0, VTK_LARGE_ID, theParent ),
      myHelper(theModule)
    {}

    virtual 
    State
    validate ( QString & theInput, int & thePos ) const
    {
      if ( QIntValidator::validate( theInput, thePos ) == QValidator::Invalid)
        return QValidator::Invalid;
      
      if ( !myHelper.get() )
        return QValidator::Invalid;

      return QValidator::Acceptable;
    }

  protected:
    mutable SelectorHelper myHelper;
  };


  class GaussCellIDValidator: public GaussPtsIDValidator
  {
  public:
    GaussCellIDValidator( QLineEdit* theLocalPointLabel,
                          const SalomeApp_Module* theModule,
                          QObject * theParent ):
      GaussPtsIDValidator( theModule, theParent ),
      myLocalPointLineEdit( theLocalPointLabel )
    {}

    virtual 
    State
    validate ( QString & theInput, int & thePos ) const
    {
      if( theInput.isEmpty() )
        return QValidator::Acceptable;

      if ( GaussPtsIDValidator::validate( theInput, thePos ) == QValidator::Invalid)
        return QValidator::Invalid;

      VISU::TCellID aCellID = theInput.toInt();
      VISU::TLocalPntID aLocalPntID = myLocalPointLineEdit->text().toInt();
      VISU::PGaussPtsIDMapper anIDMapper = myHelper.myPipeLine->GetGaussPtsIDMapper();
      if ( anIDMapper->GetVTKID( VISU::TGaussPointID( aCellID, aLocalPntID ) ) < 0 )
        return QValidator::Intermediate;

      return QValidator::Acceptable;
    }

  private:
    QLineEdit* myLocalPointLineEdit;
  };


  class GaussLocalPointIDValidator: public GaussPtsIDValidator
  {
  public:
    GaussLocalPointIDValidator( QLineEdit* theParentElementLineEdit,
                                const SalomeApp_Module* theModule,
                                QObject * theParent ):
      GaussPtsIDValidator( theModule, theParent ),
      myParentElementLineEdit( theParentElementLineEdit )
    {}

    virtual 
    State
    validate ( QString & theInput, int & thePos ) const
    {
      if( theInput.isEmpty() )
        return QValidator::Acceptable;

      if ( GaussPtsIDValidator::validate( theInput, thePos ) == QValidator::Invalid)
        return QValidator::Invalid;

      VISU::TLocalPntID aLocalPntID = theInput.toInt();
      VISU::TCellID aCellID = myParentElementLineEdit->text().toInt();
      VISU::PGaussPtsIDMapper anIDMapper = myHelper.myPipeLine->GetGaussPtsIDMapper();
      if ( anIDMapper->GetVTKID( VISU::TGaussPointID( aCellID, aLocalPntID ) ) < 0 )
        return QValidator::Intermediate;

      return QValidator::Acceptable;
    }

  private:
    QLineEdit* myParentElementLineEdit;
  };
}


VisuGUI_ValidatedLineEdit::VisuGUI_ValidatedLineEdit( QWidget* parent ):
  QLineEdit( parent )
{
  connect( this, SIGNAL( textChanged( const QString& ) ), this, SLOT( MarkValidated( const QString& ) ) );
}

void VisuGUI_ValidatedLineEdit::MarkValidated( const QString& theText )
{
  if ( !validator() )
    return;
  
  int aPos;
  QString aText( theText );
  QPalette pal = palette();
  switch ( validator()->validate( aText, aPos ) ) {
  case QValidator::Invalid:
  case QValidator::Intermediate:
    pal.setColor( foregroundRole(), QColor( 255, 0, 0 ));
    setPalette( pal );
    break;
  case QValidator::Acceptable:
    pal.setColor( foregroundRole(), QColor( 0, 0, 0 ));
    setPalette( pal );
    break;
  }
}

//---------------------------------------------------------------------------------

VisuGUI_GaussPointsSelectionPane::VisuGUI_GaussPointsSelectionPane( const SalomeApp_Module* theModule,
                                                                    QWidget* theParent ) :
  QWidget( theParent ),
  myModule( theModule ),
  myEventCallbackCommand( vtkCallbackCommand::New() )
{
  myPriority = 0.0;
  myEventCallbackCommand->Delete();
  myEventCallbackCommand->SetClientData(this); 
  myEventCallbackCommand->SetCallback(VisuGUI_GaussPointsSelectionPane::ProcessEvents);

  QVBoxLayout* TopLayout = new QVBoxLayout( this );
  //TopLayout->setSpacing(6);

  // Display parent mesh element
  QGroupBox* PositionGroup = new QGroupBox( tr( "DATA_POSITION" ), this );
  QGridLayout* PositionGroupLayout = new QGridLayout (PositionGroup);
  PositionGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  //PositionGroupLayout->setSpacing(6);

  QLabel* ParentElementLabel = new QLabel( tr( "PARENT_ELEMENT" ), PositionGroup );
  ParentElementLabel->setToolTip( tr( "PARENT_ELEMENT_TIP" ) );
  myParentElementLineEdit = new VisuGUI_ValidatedLineEdit( PositionGroup );
  connect( myParentElementLineEdit, SIGNAL( textChanged( const QString& ) ), this, SLOT( onSelectionValidate() ) );
  connect( myParentElementLineEdit, SIGNAL( returnPressed() ), this, SLOT( onSelectionApply() ) );

  QToolButton* ParentElementBtn = new QToolButton( PositionGroup );
  ParentElementBtn->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_OK" ) ) );
  connect(ParentElementBtn, SIGNAL(clicked()), this, SLOT(onSelectionApply()));

  QLabel* LocalPointLabel = new QLabel( tr( "LOCAL_POINT" ), PositionGroup );
  LocalPointLabel->setToolTip( tr( "LOCAL_POINT_TIP" ) );
  myLocalPointLineEdit = new VisuGUI_ValidatedLineEdit( PositionGroup );
  connect( myLocalPointLineEdit,    SIGNAL( textChanged( const QString& ) ), this, SLOT( onSelectionValidate() ) );
  connect( myLocalPointLineEdit,    SIGNAL( returnPressed() ), this, SLOT( onSelectionApply() ) );

  QToolButton* LocalPointBtn = new QToolButton( PositionGroup );
  LocalPointBtn->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_OK" ) ) );
  connect(LocalPointBtn, SIGNAL(clicked()), this, SLOT(onSelectionApply()));

  myCellIDValidator = new GaussCellIDValidator( myLocalPointLineEdit, myModule, myParentElementLineEdit );
  myParentElementLineEdit->setValidator( myCellIDValidator );

  myLocalPointIDValidator = new GaussLocalPointIDValidator( myParentElementLineEdit, myModule, myLocalPointLineEdit );
  myLocalPointLineEdit->setValidator( myLocalPointIDValidator );

  myDisplayParentMeshCheckBox = new QCheckBox( tr( "DISPLAY_PARENT_MESH" ), PositionGroup );

  PositionGroupLayout->addWidget( ParentElementLabel,          0, 0, 1, 3 );
  PositionGroupLayout->addWidget( myParentElementLineEdit,     0, 3 );
  PositionGroupLayout->addWidget( ParentElementBtn,            0, 4 );
  PositionGroupLayout->addWidget( LocalPointLabel,             1, 0, 1, 3 );
  PositionGroupLayout->addWidget( myLocalPointLineEdit,        1, 3 );
  PositionGroupLayout->addWidget( LocalPointBtn,               1, 4 );
  PositionGroupLayout->addWidget( myDisplayParentMeshCheckBox, 2, 0, 1, 5 );

  connect( myDisplayParentMeshCheckBox, SIGNAL( toggled( bool ) ), this, SLOT( onApplyDisplayParentMesh( bool ) ) );

  TopLayout->addWidget( PositionGroup );
  TopLayout->addStretch();
}

VisuGUI_GaussPointsSelectionPane::~VisuGUI_GaussPointsSelectionPane()
{
}

void VisuGUI_GaussPointsSelectionPane::update()
{
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();
  myDisplayParentMeshCheckBox->setChecked( aPickingSettings->GetDisplayParentMesh() );
}

void VisuGUI_GaussPointsSelectionPane::setIds( const int theParentId, const int theLocalId )
{
  myParentElementLineEdit->setText( theParentId < 0 ? "" : QString::number( theParentId ) );
  myLocalPointLineEdit->setText( theLocalId < 0 ? "" : QString::number( theLocalId ) );
  onSelectionApply();
}

void VisuGUI_GaussPointsSelectionPane::setInteractor( SVTK_RenderWindowInteractor* theInteractor )
{
  //printf( "VisuGUI_GaussPointsSelectionPane::setInteractor( %p )\n", theInteractor );
  vtkGenericRenderWindowInteractor* aDevice = theInteractor->GetDevice();
  if( aDevice->HasObserver(vtkCommand::KeyPressEvent) )
    aDevice->RemoveObservers(vtkCommand::KeyPressEvent);

  //printf( "AddObserver(vtkCommand::KeyPressEvent)\n" );
  aDevice->AddObserver(vtkCommand::KeyPressEvent, 
                       myEventCallbackCommand.GetPointer(), 
                       myPriority);

  if( aDevice->HasObserver(vtkCommand::EndPickEvent) )
    aDevice->RemoveObservers(vtkCommand::EndPickEvent);

  //printf( "AddObserver(vtkCommand::EndPickEvent)\n" );
  aDevice->AddObserver(vtkCommand::EndPickEvent, 
                       myEventCallbackCommand.GetPointer(), 
                       myPriority);
}

SVTK_RenderWindowInteractor* VisuGUI_GaussPointsSelectionPane::getInteractor()
{
  if( SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>( myModule ) )
  {
    //if( SVTK_MainWindow* aMainWindow = aViewWindow->getMainWindow() )
    //{
      SVTK_RenderWindowInteractor* anInteractor = aViewWindow->GetInteractor();
      return anInteractor;
      //}
  }
  return 0;
}

void VisuGUI_GaussPointsSelectionPane::ProcessEvents(vtkObject* vtkNotUsed(theObject),
                                                     unsigned long theEvent,
                                                     void* theClientData,
                                                     void* vtkNotUsed(theCallData))
{
  VisuGUI_GaussPointsSelectionPane* self = reinterpret_cast<VisuGUI_GaussPointsSelectionPane*>(theClientData);

  switch(theEvent){
  case vtkCommand::KeyPressEvent:
    self->KeyPressed();
    break;
  case vtkCommand::EndPickEvent:
    self->SelectionEvent();
    break;
  }
}

void VisuGUI_GaussPointsSelectionPane::KeyPressed()
{
  //printf( "VisuGUI_GaussPointsSelectionPane::KeyPressed()\n" );
  if( getInteractor()->GetDevice()->GetKeyCode() == 'P' )
  {
    VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

    bool aDisplayParentMesh = !aPickingSettings->GetDisplayParentMesh();
    myDisplayParentMeshCheckBox->setChecked( aDisplayParentMesh );

    aPickingSettings->SetDisplayParentMesh( aDisplayParentMesh );
    aPickingSettings->InvokeEvent(VISU::UpdatePickingSettingsEvent,NULL);
  }
}

void VisuGUI_GaussPointsSelectionPane::SelectionEvent()
{
  //printf( "VisuGUI_GaussPointsSelectionPane::SelectionEvent()\n" );
  clearIDControls();

  SelectorHelper aHelper( myModule );
  if( aHelper.get() )
  {
    const TColStd_IndexedMapOfInteger& aMapIndex = aHelper.myMapIndex;
    if( aMapIndex.Extent() == 1 )
    {
      int anObjId = aHelper.myMapIndex(1);
      VISU::TGaussPointID aGaussPointID = aHelper.myPipeLine->GetObjID( anObjId );
      VISU::TCellID aCellID = aGaussPointID.first;
      VISU::TLocalPntID aLocalPntID = aGaussPointID.second;

      myParentElementLineEdit->setText( QString::number( aCellID ) );
      myLocalPointLineEdit->setText( QString::number( aLocalPntID ) );
    }
  }
}

void VisuGUI_GaussPointsSelectionPane::onSelectionValidate() 
{
  myParentElementLineEdit->MarkValidated( myParentElementLineEdit->text() );
  myLocalPointLineEdit->MarkValidated( myLocalPointLineEdit->text() );
}

void VisuGUI_GaussPointsSelectionPane::onSelectionApply() 
{
  //printf( "VisuGUI_GaussPointsSelectionPane::onSelectionApply()\n" );
  SelectorHelper aHelper( myModule );
  if( !aHelper.get() )
    return;

  vtkIdType anObjVtkId = -1;
  VISU::TCellID aCellId;
  VISU::TLocalPntID aLocalPntId;

  bool ok = false;
  aCellId = myParentElementLineEdit->text().toInt( &ok );
  if( ok )
  {
    ok = false;
    aLocalPntId = myLocalPointLineEdit->text().toInt( &ok );
    if( ok )
    {
      VISU::PGaussPtsIDMapper anIDMapper = aHelper.myPipeLine->GetGaussPtsIDMapper();
      anObjVtkId = anIDMapper->GetVTKID( VISU::TGaussPointID( aCellId, aLocalPntId ) );
    }
  }

  if( anObjVtkId < 0 )
    aHelper.mySelector->ClearIndex();
  else
  {
    aHelper.myMapIndex.Clear();
    aHelper.myMapIndex.Add( anObjVtkId );
    aHelper.mySelector->AddOrRemoveIndex( aHelper.myIO, aHelper.myMapIndex, false );
  }
  aHelper.myActor->Highlight( aHelper.myIO );
  getInteractor()->GetDevice()->CreateTimer( VTKI_TIMER_FIRST );
}

void VisuGUI_GaussPointsSelectionPane::clearIDControls()
{
  myParentElementLineEdit->setText( "" );
  myLocalPointLineEdit->setText( "" );
}

void VisuGUI_GaussPointsSelectionPane::apply()
{
  onSelectionApply();
}

void VisuGUI_GaussPointsSelectionPane::onApplyDisplayParentMesh( bool theValue )
{
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();
  aPickingSettings->SetDisplayParentMesh( theValue );
  aPickingSettings->InvokeEvent( VISU::UpdatePickingSettingsEvent,NULL );
}

/*
void VisuGUI_GaussPointsSelectionPane::Help()
{
  QString aHelpFileName = "picking.htm";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(app->activeModule() ? app->moduleName(app->activeModule()->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning( 0,
                              QObject::tr("WRN_WARNING"),
                              QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                                arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                              QObject::tr("BUT_OK"));
  }
}

void VisuGUI_GaussPointsSelectionPane::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      Help();
    }
}
*/

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_CursorDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_CURSORDLG_H
#define VISUGUI_CURSORDLG_H

#include <QDialog>
class QLabel;
class QPushButton;
class SalomeApp_IntSpinBox;
class QGroupBox;

class VisuGUI_CursorDlg : public QDialog
{
    Q_OBJECT

public:
    VisuGUI_CursorDlg( QWidget* parent = 0, const char* name = 0, bool modal = FALSE );
    ~VisuGUI_CursorDlg();

    QGroupBox*   TopGroupBox;
    QLabel*      Comment1;
    QLabel*      Comment2;
    SalomeApp_IntSpinBox* SpinBox1;
    QGroupBox*   GroupButtons;
    QPushButton* buttonOk;
    QPushButton* buttonCancel;
    QPushButton* buttonHelp;
    QString      helpFileName;

private:
    void keyPressEvent( QKeyEvent* e );

private slots:
    void onHelp();

};

#endif // VISUGUI_CURSORDLG_H

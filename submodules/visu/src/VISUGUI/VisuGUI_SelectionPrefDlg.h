// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_SelectionPrefDlg.cxx
//  Author : Oleg UVAROV
//  Module : SALOME
//
#ifndef VisuGUI_SELECTIONPREFDLG_H
#define VisuGUI_SELECTIONPREFDLG_H

#include <QDialog>

class QComboBox;
class QGroupBox;
class QPushButton;

class QtxColorButton;
class SalomeApp_DoubleSpinBox;
class SalomeApp_IntSpinBox;

class VisuGUI_SelectionPrefDlg : public QDialog
{ 
  Q_OBJECT

public:
  VisuGUI_SelectionPrefDlg( QWidget* parent = 0 );
  ~VisuGUI_SelectionPrefDlg();
    
public:
  void                      update();

private:
  void                      keyPressEvent( QKeyEvent* e );
    
protected slots:
  void                      accept();

  void                      onApply();
  void                      onHelp();
  
private:
  SalomeApp_DoubleSpinBox*  myCursorSizeSpinBox;
  SalomeApp_DoubleSpinBox*  myPyramidHeightSpinBox;
  QtxColorButton*           mySelectionColorButton;
  SalomeApp_DoubleSpinBox*  myPointToleranceSpinBox;

  QGroupBox*                myInfoWindowGroup;
  SalomeApp_IntSpinBox*     myTransparencySpinBox;
  QComboBox*                myPositionComboBox;

  QGroupBox*                myCameraGroup;
  SalomeApp_DoubleSpinBox*  myZoomFactorSpinBox;
  SalomeApp_IntSpinBox*     myStepNumberSpinBox;

  QPushButton*              myButtonOk;
  QPushButton*              myButtonApply;
  QPushButton*              myButtonCancel;
  QPushButton*              myButtonHelp;
};

#endif // VisuGUI_SELECTIONPREFDLG_H

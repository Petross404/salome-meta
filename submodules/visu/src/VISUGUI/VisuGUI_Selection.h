// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Selection.h
//  Author : Sergey Anikin 
//  Module : VISU
//
#ifndef VisuGUI_Selection_HeaderFile
#define VisuGUI_Selection_HeaderFile

#include <LightApp_Selection.h>

//////////////////////////////////////////////////
// Class: VisuGUI_Selection
//////////////////////////////////////////////////

class SalomeApp_Module;
class SalomeApp_Study;

class VisuGUI_Selection : public LightApp_Selection
{
public:
  VisuGUI_Selection( SalomeApp_Module* theModule )
    : LightApp_Selection(), myModule( theModule ) {};
  virtual ~VisuGUI_Selection() {};

  virtual QVariant parameter( const int, const QString& ) const;

private:
  QString          type( const int ) const;
  bool             isFieldPrs( const int ) const;
  QString          nbComponents( const int ) const;
  QString          medEntity( const int ) const;
  QString          medSource( const int ) const;
  QString          nbTimeStamps( const int ) const;
  QString          representation( const int ) const;
  int              nbChildren( const int ) const;
  int              nbNamedChildren( const int ) const;
  QString          isVisible( const int ) const;
  QString          isShrunk( const int ) const;
  bool             hasActor( const int ) const;
  QString          isShading( const int ) const;
  QString          isScalarMapAct( const int ) const;
  QString          isGaussPtsAct( const int ) const;
  bool             isScalarBarVisible( const int ) const;
  bool             isVisuComponent( const int ) const;
  QString          isValuesLabeled( const int ) const;

  QString          fullResolution( const int ) const;
  QString          mediumResolution( const int ) const;
  QString          lowResolution( const int ) const;
  QString          resolutionState( const int ) const;

  QString          quadratic2DMode( const int ) const;
  bool             hasDeviation(const int) const;
  bool             isDeviationDisplayed(const int) const;

private:
  bool             findDisplayedCurves( const int, bool ) const;
  bool             hasCurves( const int ) const;
  bool             Plot2dViewerType( const int ) const;

  int              nbChild( const int, const bool ) const;
  SalomeApp_Study* GetStudy() const;

  QString          resolutions( const int ) const;
  QString          resolution( const int, char theResoltuion ) const;

private:
  SalomeApp_Module* myModule;
};

#endif

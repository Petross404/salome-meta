// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef DIALOGBOX_OFFSET_H
#define DIALOGBOX_OFFSET_H

#include "VTKViewer.h"
#include "VISU_Prs3d_i.hh"
#include "VISU_PointMap3d_i.hh"

// QT Includes
#include <QDialog>
#include <QList>

class QCheckBox;
class SalomeApp_DoubleSpinBox;
class VisuGUI;
class LightApp_SelectionMgr;

struct OffsetStruct
{
  double myOffset[3];

  OffsetStruct()
  {
    myOffset[0] = myOffset[1] = myOffset[2] = 0;
  }

  OffsetStruct(double theX,
               double theY,
               double theZ)
  {
    myOffset[0] = theX;
    myOffset[1] = theY;
    myOffset[2] = theZ;
  }
};

class VisuGUI_OffsetDlg: public QDialog
{
  Q_OBJECT
 public:
  VisuGUI_OffsetDlg (VisuGUI* theModule);
  ~VisuGUI_OffsetDlg() {};

  virtual void setVisible(bool);

  void addPresentation (VISU::Prs3d_i* thePrs);
  void addPointMapPresentation (VISU::PointMap3d_i* thePrs);
  int  getPrsCount() const { return myPrsList.count() + myPointMapList.count(); }
  void clearPresentations();

  void setOffset (const double* theOffset);
  void getOffset (double* theOffset) const;
  bool isToSave() const;

 private:
  void keyPressEvent( QKeyEvent* e );

 public slots:
  void onReset();

 protected slots:
  virtual void accept();
  virtual void reject();
  void onApply();
  void onHelp();
  void onSelectionChanged();

 private:
  void updateOffset (VISU::Prs3d_i* thePrs, double* theOffset);
  void updatePointMapOffset (VISU::PointMap3d_i* thePrs, double* theOffset);

  VisuGUI * myModule;
  LightApp_SelectionMgr*  mySelectionMgr;

  SalomeApp_DoubleSpinBox * myDxEdt;
  SalomeApp_DoubleSpinBox * myDyEdt;
  SalomeApp_DoubleSpinBox * myDzEdt;
  QCheckBox     * mySaveChk;

  QList<VISU::Prs3d_i*>  myPrsList;
  QList<OffsetStruct> myOldOffsets;

  QList<VISU::PointMap3d_i*> myPointMapList;
  QList<OffsetStruct>        myOldPointMapOffsets;
};

#endif // DIALOGBOX_OFFSET_H

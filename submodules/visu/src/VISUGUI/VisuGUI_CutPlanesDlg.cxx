// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_CutPlanesDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU

#include "VisuGUI_CutPlanesDlg.h"

#include "VISU_ColoredPrs3dFactory.hh"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_CutPlanes_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_CutPlanesPL.hxx"

//#include "OB_Browser.h"

#include "VISU_TableDlg.h"

#include "SVTK_ViewWindow.h"

#include "LightApp_Application.h"

#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include "SUIT_Application.h"
#include "SUIT_Desktop.h"
#include "SUIT_ViewWindow.h"
#include "SUIT_ViewManager.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include <QLayout>
#include <QLabel>
#include <QButtonGroup>
#include <QCheckBox>
#include <QStyle>
#include <QValidator>
#include <QLineEdit>
#include <QTabWidget>
#include <QKeyEvent>
#include <QRadioButton>
#include <QTableWidget>
#include <QGroupBox>
#include <QPushButton>
#include <QHeaderView>
#include <QComboBox>

#include <vtkRenderer.h>
#include <vtkPolyData.h>
#include <vtkAppendPolyData.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSet.h>

#include <limits>

using namespace std;

//extern VisuGUI *visuGUI;

VisuGUI_CutPlanesPane::VisuGUI_CutPlanesPane (QWidget* theParent)
     : QFrame(theParent), myPreviewActor(0)
{
  QGridLayout* TopLayout = new QGridLayout( this );
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin( 10 );

  QButtonGroup* SelPlane = new QButtonGroup ( this );
  QGroupBox* aGB = new QGroupBox( tr("TXT_ORIENTATION"), this );
  //SelPlane->setTitle(tr("TXT_ORIENTATION"));
  //SelPlane->setColumnLayout(0, Qt::Vertical);
  //SelPlane->layout()->setSpacing( 0 );
  //SelPlane->layout()->setMargin( 0 );
  QGridLayout* SelPlaneLayout = new QGridLayout( aGB );
  SelPlaneLayout->setAlignment( Qt::AlignTop );
  SelPlaneLayout->setSpacing( 6 );
  SelPlaneLayout->setMargin( 11 );

  RBxy = new QRadioButton (tr("PARALLEL_XOY"), aGB);
  RByz = new QRadioButton (tr("PARALLEL_YOZ"), aGB);
  RBzx = new QRadioButton (tr("PARALLEL_ZOX"), aGB);
  SelPlaneLayout->addWidget( RBxy, 0, 0 );
  SelPlaneLayout->addWidget( RByz, 0, 1 );
  SelPlaneLayout->addWidget( RBzx, 0, 2 );
  SelPlane->addButton( RBxy );
  SelPlane->addButton( RByz );
  SelPlane->addButton( RBzx );

  QLabel* LabelPosi_3 = new QLabel (tr("LBL_NB_PLANS"), this);

  nbPlan = new SalomeApp_IntSpinBox( this );
  nbPlan->setAcceptNames( false );
  nbPlan->setMinimum( 1 );
  nbPlan->setMaximum(100);
  nbPlan->setSingleStep( 1 );
  nbPlan->setValue( 1 );
  nbPlan->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myPosTable = new QTableWidget (this);
  myPosTable->setMaximumHeight( 227 );
  myPosTable->setMinimumWidth( 294 );
  myPosTable->setColumnCount(2);
  myPosTable->setRowCount(0);
  connect( myPosTable, SIGNAL(cellChanged(int, int)), this, SLOT(onValueChanged(int, int)) );

  myPosTable->setItemDelegateForColumn( 0,
         new NumDelegateItem( myPosTable, NumDelegateItem::NV_Real) );

  myPosTable->setEditTriggers(
         (QAbstractItemView::EditTriggers)(QAbstractItemView::AllEditTriggers &(~QAbstractItemView::CurrentChanged)));

  QStringList aLabels;
  aLabels.append( tr("Position") );
  aLabels.append( tr("Set default") );
  
  myPosTable->setHorizontalHeaderLabels( aLabels );

  QGroupBox* GBrot = new QGroupBox (tr("LBL_ROTATION"), this);
  //GBrot->setColumnLayout(0, Qt::Vertical);
  //GBrot->layout()->setSpacing( 0 );
  //GBrot->layout()->setMargin( 0 );
  QGridLayout* GBrotLayout = new QGridLayout( GBrot );
  GBrotLayout->setAlignment( Qt::AlignTop );
  GBrotLayout->setSpacing( 6 );
  GBrotLayout->setMargin( 11 );

  LabelRot1 = new QLabel (tr("LBL_ROT_X"), GBrot );
  GBrotLayout->addWidget( LabelRot1, 0, 0 );

  Rot1 = new SalomeApp_DoubleSpinBox( GBrot );
  VISU::initSpinBox( Rot1, -180., 180., 10., "angle_precision" );  
  Rot1->setValue( 0 );
  Rot1->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  connect( Rot1, SIGNAL( valueChanged( double )), this, SLOT( onRotation( double ) ) );
  GBrotLayout->addWidget( Rot1, 0, 1 );

  LabelRot2 = new QLabel (tr("LBL_ROT_Y"), GBrot );
  GBrotLayout->addWidget( LabelRot2, 1, 0 );

  Rot2 = new SalomeApp_DoubleSpinBox( GBrot );
  VISU::initSpinBox( Rot2, -180., 180., 10., "angle_precision" );
  Rot2->setValue( 0 );
  Rot2->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  connect( Rot2, SIGNAL( valueChanged( double )), this, SLOT( onRotation( double ) ) );
  GBrotLayout->addWidget( Rot2, 1, 1 );

  QLabel* aPosLbl = new QLabel(tr( "LBL_POS" ), this);
  myPosSpn = new SalomeApp_DoubleSpinBox( this );
  VISU::initSpinBox( myPosSpn, 0., 1., .1, "parametric_precision" );

  GDeformation = new QGroupBox(tr("LBL_DEFORMATION"), this);
  GDeformation->setCheckable(true);
  GDeformation->setChecked(false);
  QGridLayout* GDeformationLayout = new QGridLayout( GDeformation );
  GDeformationLayout->setSpacing( 6 );
  GDeformationLayout->setMargin( 11 );
  connect(GDeformation, SIGNAL(toggled(bool)), this, SLOT(onDeformationCheck(bool)));

  QLabel* LabelDeformation1 = new QLabel (tr("LBL_DEFROMATION_VECT"), GDeformation);
  GDeformationLayout->addWidget( LabelDeformation1, 0, 0 );

  myVectorialFieldCombo = new QComboBox (GDeformation);
  GDeformationLayout->addWidget( myVectorialFieldCombo, 0, 1 );

  QLabel* LabelDeformation2 = new QLabel (tr("LBL_DEFROMATION_SCALE"), GDeformation);
  GDeformationLayout->addWidget( LabelDeformation2, 1, 0 );
  myScaleSpn = new SalomeApp_DoubleSpinBox (GDeformation);
  VISU::initSpinBox( myScaleSpn, 0., 1.0E+38, 0.1, "visual_data_precision" );
  myScaleSpn->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  GDeformationLayout->addWidget( myScaleSpn, 1, 1 );
  connect(myVectorialFieldCombo,     SIGNAL(activated(int)), this, SLOT(onVectorialFieldChanged(int)));
  connect(myScaleSpn, SIGNAL(valueChanged(double)), this, SLOT(onScaleFactorChanged(double)));
  myScaleSpn->setValue(0.1);

  myPreviewCheck = new QCheckBox(tr("LBL_SHOW_PREVIEW"), this);
  myPreviewCheck->setChecked(false);
  connect( myPreviewCheck, SIGNAL( toggled( bool )), this, SLOT( onPreviewCheck( bool ) ) );

  TopLayout->addWidget( aGB, 0, 0, 1, 2 );
  TopLayout->addWidget( GBrot, 1, 0, 1, 2 );
  TopLayout->addWidget( LabelPosi_3,   2, 0 );
  TopLayout->addWidget( nbPlan,        2, 1 );
  TopLayout->addWidget( myPosTable, 3, 0, 1, 2 );
  TopLayout->setRowStretch ( 3, 3 );
  TopLayout->addWidget(aPosLbl, 4, 0 );
  TopLayout->addWidget(myPosSpn, 4, 1 );
  TopLayout->addWidget( GDeformation, 5, 0, 1, 2 );
  TopLayout->addWidget( myPreviewCheck, 6, 0, 1, 2 );

  // signals and slots connections
  connect( SelPlane, SIGNAL( buttonClicked( int )), this, SLOT( orientationChanged( int ) ) );
  connect( nbPlan, SIGNAL( valueChanged( int )), this, SLOT( DrawTable( ) ) );
  connect( myPosSpn, SIGNAL( valueChanged( double )), this, SLOT( DrawTable( ) ) );
  connect( myPosTable->horizontalHeader(), SIGNAL( sectionClicked(int)), this, SLOT( setDefault(int) ) );

  // default values
  hasInit = false;
  X1 = 0; X2 = 0;
  Y1 = 0; Y2 = 0;
  Z1 = 0; Z2 = 0;
  RByz->setChecked( true );

} // End of Constructor

VisuGUI_CutPlanesPane::~VisuGUI_CutPlanesPane()
{
  deletePlanes();
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
    vf->Repaint();
}

void VisuGUI_CutPlanesPane::initFromPrsObject (VISU::CutPlanes_i* thePrs)
{
  setNbPlanes(thePrs->GetNbPlanes());
  setRotation(thePrs->GetRotateX()*180./M_PI, thePrs->GetRotateY()*180./M_PI);
  setPlanePos(thePrs->GetOrientationType());
  myPosSpn->setValue(thePrs->GetDisplacement());
  InitEntity2VectorialFieldsMap(thePrs);
  InsertAllVectorialFields();
  setScaleFactor(thePrs->GetScale());
  if(myEntity2VectorialFields.size() < 1)
    GDeformation->setDisabled(true);
  else{
    if(thePrs->IsDeformed()){
      GDeformation->setChecked(true);
      int idx = myVectorialFieldCombo->findText(thePrs->GetVectorialFieldName());
      if (idx >= 0)
        myVectorialFieldCombo->setCurrentIndex(idx);
      else {
        myVectorialFieldCombo->addItem(thePrs->GetVectorialFieldName());
        myVectorialFieldCombo->setCurrentIndex(myVectorialFieldCombo->count()-1);
      }
    }
  }
    
  hasInit = true;
  // init table
  myCutPlanes = VISU::TSameAsFactory<VISU::TCUTPLANES>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);
  myCutPlanes->SameAs(thePrs);
  DrawTable();

  // Draw Preview
  if (myPreviewCheck->isChecked()) {
    createPlanes();
  }
}

void VisuGUI_CutPlanesPane::InitEntity2VectorialFieldsMap(VISU::ColoredPrs3d_i* thePrs){

  VISU::Result_var theResult = thePrs->GetResultObject();
  VISU::Result_i* aResult = dynamic_cast<VISU::Result_i*>(GetServant(theResult).in());
  VISU::Result::Entities_var aEntities = aResult->GetEntities(thePrs->GetMeshName());
  VISU::Result::TimeStampNumbers_var aScalarTimeStamp = aResult->GetTimeStampNumbers(thePrs->GetMeshName(),
                                                                                     thePrs->GetEntity(),
                                                                                     thePrs->GetFieldName());
  CORBA::Long aTimeStampNumber = aScalarTimeStamp->length();
  
  for(size_t iEntity = 0; iEntity < aEntities->length(); iEntity++){
    VISU::Entity aEntity = aEntities[iEntity];
    VISU::Result::EntityNames_var aFields = aResult->GetFields(thePrs->GetMeshName(),aEntity);
    for(size_t iField = 0; iField < aFields->length(); iField++){

      TVectorialFieldsList aVectorialFields;
      VISU::Result::TimeStampNumbers_var aTimeStamps =  aResult->GetTimeStampNumbers(thePrs->GetMeshName(),aEntity,aFields[iField] );
      
      if((aResult->GetNumberOfComponents(thePrs->GetMeshName(),aEntity,aFields[iField] ) > 1) &&
         (aTimeStamps->length() >= aTimeStampNumber)){
        aVectorialFields.push_back(QString(aFields[iField].in()));
      }
      if(aVectorialFields.size() > 0){
        myEntity2VectorialFields.insert(TEntVectPair(aEntity,aVectorialFields));
      }
    }
  }
}

void VisuGUI_CutPlanesPane::InsertAllVectorialFields(){
  TEntity2VectorialFields::const_iterator aFieldIter = myEntity2VectorialFields.begin();
  for( ;aFieldIter != myEntity2VectorialFields.end();aFieldIter++ ){
    TVectorialFieldsList aVectorialFields = aFieldIter->second;
    for(size_t iField = 0;iField < aVectorialFields.size(); iField++)
      myVectorialFieldCombo->addItem(aVectorialFields[iField]);
  }
}

double VisuGUI_CutPlanesPane::getScaleFactor(){
  return myScaleSpn->value();
}

void VisuGUI_CutPlanesPane::setScaleFactor(double theFactor)
{
  double step = 0.1;
  if (fabs(theFactor) > std::numeric_limits<double>::epsilon()) {
    int degree = int(log10(fabs(theFactor))) - 1;
    if (fabs(theFactor) < 1) {
      // as logarithm value is negative in this case
      // and it is truncated to the bigger integer
      degree -= 1;
    }
    step = pow(10., double(degree));
  }

  myScaleSpn->setSingleStep(step);
  myScaleSpn->setValue(theFactor);
}

void VisuGUI_CutPlanesPane::createPlanes()
{
  if (VISU::GetActiveViewWindow<SVTK_ViewWindow>() == NULL) return;
  if (!(bool)myCutPlanes) return;
  if (myPreviewActor != 0) return;

  vtkDataSet* aDataSet = myCutPlanes->GetSpecificPL()->GetWarpVectorOutput();
  if (!aDataSet->GetNumberOfCells()) {
    onPreviewCheck(false);
    return;
  }
  vtkDataSetMapper* aPlaneMapper = vtkDataSetMapper::New();
  aPlaneMapper->SetInputData(aDataSet);
  aPlaneMapper->ScalarVisibilityOff();

  myPreviewActor = SALOME_Actor::New();
  myPreviewActor->PickableOff();
  myPreviewActor->SetMapper(aPlaneMapper);
  aPlaneMapper->Delete();
  VISU::GetActiveViewWindow<SVTK_ViewWindow>()->AddActor(myPreviewActor);
}

int VisuGUI_CutPlanesPane::storeToPrsObject (VISU::CutPlanes_i* thePrs)
{
  thePrs->SetOrientation(getOrientaion(), getRotation1()*M_PI/180., getRotation2()*M_PI/180.);
  thePrs->SetNbPlanes(getNbPlanes());
  thePrs->SetDisplacement(myPosSpn->value());
  for (int i = 0; i < getNbPlanes(); ++i) {
    QTableWidgetItem* anItem = myPosTable->item(i, 1);
    if (!anItem)
      break;
    if (anItem->checkState() != Qt::Checked )
      thePrs->SetPlanePosition(i, myPosTable->item(i, 0)->text().toDouble());
    else
      thePrs->SetDefault(i);
  }

  thePrs->UseDeformation(GDeformation->isChecked());
  
  if(GDeformation->isChecked()){
    TEntity2VectorialFields::const_iterator anIter = myEntity2VectorialFields.begin();
    VISU::Entity anEntity;
    QString aFieldName;
    bool isInited = false;
    for(;anIter != myEntity2VectorialFields.end();anIter++){
      TVectorialFieldsList aFields = anIter->second;
      for(int it = 0;it < aFields.size();it++)
        if(!QString::compare(aFields[it], myVectorialFieldCombo->currentText ())){
          anEntity = anIter->first;
          aFieldName = aFields[it];
          isInited = true;
          break;
        }
      if(isInited)
        break;
    }
    if(isInited)
      thePrs->SetVectorialField(anEntity,aFieldName.toLatin1().constData());
    
    thePrs->SetScale(myScaleSpn->value());
  }
  return 1;
}

/*!
  Called when orientation is changed
*/
void VisuGUI_CutPlanesPane::orientationChanged (int Id)
{
  if ( RBxy->isChecked() ) { // RBxy->isChecked()
    LabelRot1->setText(tr("LBL_ROT_X"));
    LabelRot2->setText(tr("LBL_ROT_Y"));
  } else if ( RByz->isChecked() ) { // RByz->isChecked()
    LabelRot1->setText(tr("LBL_ROT_Y"));
    LabelRot2->setText(tr("LBL_ROT_Z"));
  } else if ( RBzx->isChecked() ) {
    LabelRot1->setText(tr("LBL_ROT_Z"));
    LabelRot2->setText(tr("LBL_ROT_X"));
  }
  DrawTable();
}

/*!
  Sets planes orientation and position
*/
void VisuGUI_CutPlanesPane::setPlanePos (const VISU::CutPlanes::Orientation orient)
{
  if (orient == VISU::CutPlanes::XY)           // xy
    {
      RBxy->setChecked( true );
      orientationChanged( 0 );
    }
  else if (orient == VISU::CutPlanes::YZ)       // yz
    {
      RByz->setChecked( true );
      orientationChanged( 1 );
    }
  else                         // zx
   {
     RBzx->setChecked( true );
     orientationChanged( 2 );
   }

}

/*!
  Gets planes orientation
*/
VISU::CutPlanes::Orientation VisuGUI_CutPlanesPane::getOrientaion()
{
  VISU::CutPlanes::Orientation orient;
  if ( RBxy->isChecked() )     // xy
    orient = VISU::CutPlanes::XY;
  else if ( RByz->isChecked() )     // yz
    orient = VISU::CutPlanes::YZ;
  else if ( RBzx->isChecked() )     // zx
    orient = VISU::CutPlanes::ZX;
  return orient;
}

/*!
  Sets planes rotation
*/
void VisuGUI_CutPlanesPane::setRotation (const double r1, const double r2)
{
  Rot1->setValue( r1 );
  Rot2->setValue( r2 );
}

void VisuGUI_CutPlanesPane::onScaleFactorChanged(double theFactor){
  DrawTable();
}

void VisuGUI_CutPlanesPane::onVectorialFieldChanged(int pos){
  InitVectorialField();
  DrawTable();
}

void VisuGUI_CutPlanesPane::onDeformationCheck(bool Flag){
  if(!(bool)myCutPlanes || !hasInit) return;
  myCutPlanes->UseDeformation(Flag);
  InitVectorialField();
  DrawTable();
}

void VisuGUI_CutPlanesPane::InitVectorialField(){
  if(myCutPlanes->IsDeformed()){
    TEntity2VectorialFields::const_iterator anIter = myEntity2VectorialFields.begin();
    VISU::Entity anEntity;
    QString aFieldName;
    bool isInited = false;
    for(;anIter != myEntity2VectorialFields.end();anIter++){
      TVectorialFieldsList aFields = anIter->second;
      for(int it = 0;it < aFields.size();it++)
        if(!QString::compare(aFields[it], myVectorialFieldCombo->currentText ())){
          anEntity = anIter->first;
          aFieldName = aFields[it];
          isInited = true;
          break;
        }
      if(isInited)
        break;
    }
    if(isInited)
      myCutPlanes->SetVectorialField(anEntity,aFieldName.toLatin1().constData());
  }
}

/*!
  Draw the table of planes positions
*/
void VisuGUI_CutPlanesPane::DrawTable()
{
  if (!(bool)myCutPlanes) return;
  if (!hasInit) return;

  int aNbRows = myPosTable->rowCount();
  int aNbPlanes = getNbPlanes();

  myCutPlanes->SetOrientation(getOrientaion(),getRotation1()*M_PI/180.,getRotation2()*M_PI/180.);
  myCutPlanes->SetNbPlanes(aNbPlanes);
  myCutPlanes->SetDisplacement(myPosSpn->value());
  myCutPlanes->SetScale(getScaleFactor());
  if (aNbRows>0)
    for (int i = 0; i < aNbRows; ++i) {
      QTableWidgetItem* aItem = myPosTable->item( i, 1 );
      if (aItem == 0) break;
      if (aItem->checkState() != Qt::Checked )
        myCutPlanes->SetPlanePosition(i, myPosTable->item(i, 0)->text().toDouble());
    }

  if (aNbPlanes > aNbRows)
    while( aNbPlanes > myPosTable->rowCount() )
      myPosTable->insertRow( myPosTable->rowCount() );
  else if (aNbPlanes < aNbRows)
    myPosTable->setRowCount(aNbPlanes);

  QStringList aLabels;
  QString str(tr("PLANE_NUMBER"));
  int aMin = (aNbRows < aNbPlanes)? aNbRows : aNbPlanes;
  for (int i = 0; i<aMin; i++) {
    myPosTable->item( i, 0 )->setText( QString::number(myCutPlanes->GetPlanePosition(i)));
    myPosTable->item( i, 1 )->setCheckState( myCutPlanes->IsDefault(i) ? Qt::Checked : Qt::Unchecked);
    aLabels.append( str.arg(i+1) );
  }

  //QHeader *vh = myPosTable->verticalHeader();
  for (int i=aMin; i<aNbPlanes; i++) {
    QTableWidgetItem* aEditItem = new QTableWidgetItem();
    aEditItem->setText( QString::number(myCutPlanes->GetPlanePosition(i)));
    //aEditItem->setReplaceable(false);
    //aEditItem->setEnabled(!myCutPlanes->IsDefault(i));
    Qt::ItemFlags aFlags = aEditItem->flags();
    if ( myCutPlanes->IsDefault(i) )
      aFlags |= Qt::ItemIsEnabled;
    else
      aFlags &= (~Qt::ItemIsEnabled);
    myPosTable->setItem(i, 0, aEditItem);

    /*QCheckTableItem* aCheck = new QCheckTableItem(myPosTable, 0);
    aCheck->setChecked(myCutPlanes->IsDefault(i));
    aCheck->setReplaceable(false);*/
    QTableWidgetItem* aCheck = new QTableWidgetItem();
    aFlags = aCheck->flags();
    //aFlags |=  Qt::ItemIsUserCheckable;
    aFlags &=  (~Qt::ItemIsEditable);
    aCheck->setFlags( aFlags );
    aCheck->setCheckState( myCutPlanes->IsDefault(i) ?
                           Qt::Checked : Qt::Unchecked );
    myPosTable->setItem(i, 1, aCheck);

    //vh->setLabel( i, str.arg(i+1) );
    aLabels.append( str.arg(i+1) );
  }
  myPosTable->setVerticalHeaderLabels( aLabels );

  // Update preview
  if (myPreviewCheck->isChecked()) {
    if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
      myCutPlanes->GetPipeLine()->Update();
      deletePlanes();
      createPlanes();
      vf->Repaint();
    }
  }
}

void VisuGUI_CutPlanesPane::setDefault (int all)
{
  myPosTable->setCurrentCell(-1, 1);
  myPosTable->clearSelection();
  if (all == 0) return;
  if (all == 1)
    for (int i = 0; i < getNbPlanes(); ++i)
      myPosTable->item( i, 1 )->setCheckState(Qt::Checked);
  //((QCheckTableItem*)myPosTable->item( i, 1 ))->setChecked(true);

  for (int i = 0; i < getNbPlanes(); ++i) {
    if ( myPosTable->item( i, 1 )->checkState() == Qt::Checked )
    {
      myCutPlanes->SetDefault(i);
      myPosTable->item( i, 0)->setText( QString::number(myCutPlanes->GetPlanePosition(i)));
      myPosTable->item( i, 0 )->setFlags( myPosTable->item( i, 0 )->flags() &(~Qt::ItemIsEnabled ) );
    }
    else
      myPosTable->item( i, 0 )->setFlags( myPosTable->item( i, 0 )->flags() | Qt::ItemIsEnabled );
  }
}

void VisuGUI_CutPlanesPane::onValueChanged (int theRow, int theCol)
{
  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myPosTable->model() );
  if ( !aModel )
    return;
  
  if ( !myPosTable->item(theRow, 1) || !myPosTable->item(theRow, 0) )
    return;

  QModelIndex anIndex;

  QTableWidgetItem* anItem = myPosTable->item(theRow, theCol);
  if (!anItem )
    return;

  bool isDefault = myPosTable->item(theRow, 1)->checkState() == Qt::Checked;
  if (theCol == 0 )
  {
    anIndex = aModel->index( theRow, 0 );
    if ( !isDefault)
      myCutPlanes->SetPlanePosition( theRow, anIndex.data().toDouble());
          
            
    if ( myPreviewCheck->isChecked() )
    {
      //Update Preview
      if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
      {
        myCutPlanes->GetPipeLine()->Update();
        deletePlanes();
        createPlanes();
        vf->Repaint();
      }
    }
  }
  else
  {
    anItem = myPosTable->item(theRow, 0);
    Qt::ItemFlags flags = anItem->flags();
    if ( isDefault )
    {
      myCutPlanes->SetDefault(theRow);
      anIndex = aModel->index( theRow, 0 );
      aModel->setData( anIndex, myCutPlanes->GetPlanePosition(theRow), Qt::DisplayRole );

      /*if ( myPreviewCheck->isChecked() )
        {
        //not needed update presentation, because item->setFlags generates signal cellChanged() again
        }*/
      flags &= (~Qt::ItemIsEnabled );
    }
    else
      flags |=  Qt::ItemIsEnabled;
   
    anItem->setFlags( flags );
  }
  
  /*if (theCol == 0) {
    bool isChanged = !anIndex.data().isNull();
    QTableWidgetItem* anItem = myPosTable->item(theRow, 1);
    if ( !anItem  )
    {
      anItem = new QTableWidgetItem();
      myPosTable->setItem( theRow, 1, anItem );
      Qt::ItemFlags flags = anItem->flags();
      //flags |=  Qt::ItemIsUserCheckable;
      flags &=  (~Qt::ItemIsEditable);
      anItem->setFlags( flags );
    }
    anItem->setCheckState(!isChanged ? Qt::Checked : Qt::Unchecked);

    if (myPreviewCheck->isChecked()) {
      //Update Preview
      if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
        myCutPlanes->SetPlanePosition( theRow, anIndex.data().toDouble());
        myCutPlanes->GetPipeLine()->Update();
        deletePlanes();
        createPlanes();
        vf->Repaint();
      }
    }
  } else {
    bool isDefault = myPosTable->item(theRow, 1)->checkState() == Qt::Checked;
    QTableWidgetItem* anItem = myPosTable->item(theRow, 0);
    if ( !anItem  )
    {
      anItem = new QTableWidgetItem();
      myPosTable->setItem( theRow, 0, anItem );
    }
      
    if (isDefault) {
      myCutPlanes->SetDefault(theRow);
      //myPosTable->setText(theRow, 0, QString::number(myCutPlanes->GetPlanePosition(theRow)));
      anIndex = aModel->index( theRow, 0 );
      aModel->setData( anIndex, myCutPlanes->GetPlanePosition(theRow), Qt::DisplayRole );
      anItem->setFlags( anItem->flags() | Qt::ItemIsEnabled );
      if (myPreviewCheck->isChecked()) {
        if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
          //Update Preview
          myCutPlanes->GetPipeLine()->Update();
          deletePlanes();
          createPlanes();
          vf->Repaint();
        }
      }
    }
    //myPosTable->item(theRow, 0)->setEnabled(!isDefault);
    anItem->setFlags( anItem->flags() &(~Qt::ItemIsEnabled ) );
    }*/
}

void VisuGUI_CutPlanesPane::deletePlanes()
{
  if (myPreviewActor == 0) return;
  vtkRenderer* aRend       = myPreviewActor->GetRenderer();
  vtkRenderWindow* aWnd = aRend->GetRenderWindow();

  if(aRend && aWnd)
    myPreviewActor->RemoveFromRender(aRend);
//  Bug IPAL11962: preview is still in the viewer, if viewer not active.
//   if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
//     vf->RemoveActor(myPreviewActor);
  myPreviewActor->Delete();
  myPreviewActor = 0;
}

void VisuGUI_CutPlanesPane::onRotation (double theValue)
{
  if (!(bool)myCutPlanes) return;
  if (myPreviewCheck->isChecked()) {
    if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
      myCutPlanes->SetOrientation(getOrientaion(),getRotation1()*M_PI/180.,getRotation2()*M_PI/180.);
      myCutPlanes->GetPipeLine()->Update();
      deletePlanes();
      createPlanes();
      vf->Repaint();
    }
  }
}

void VisuGUI_CutPlanesPane::onPreviewCheck (bool thePreview)
{
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
    if (thePreview) {
      storeToPrsObject(myCutPlanes);
      myCutPlanes->GetPipeLine()->Update();
      createPlanes();
      if(VISU::GetResourceMgr()->booleanValue("VISU","automatic_fit_all",false))
        vf->onFitAll(); // Repaint() is called inside
      else
        vf->Repaint();
    } else {
      deletePlanes();
      vf->Repaint();
    }
  }
}

//###################################################################


/*!
  Constructor
*/
VisuGUI_CutPlanesDlg::VisuGUI_CutPlanesDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule)
{
  setWindowTitle(tr("Cut Planes Definition"));
  setSizeGripEnabled(true);

  QVBoxLayout* TopLayout = new QVBoxLayout (this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);

  // Tab pane
  myTabBox = new QTabWidget(this);
  myCutPane = new VisuGUI_CutPlanesPane(this/*, myMgr*/);
  myTabBox->addTab(myCutPane, tr("Cut Planes"));
  myInputPane = new VisuGUI_InputPane(VISU::TCUTPLANES, theModule, this);
  myTabBox->addTab(GetScalarPane(), tr("Scalar Bar"));
  myTabBox->addTab(myInputPane, tr("Input"));

  TopLayout->addWidget(myTabBox);

  QGroupBox* GroupButtons = new QGroupBox(this);
  GroupButtons->setGeometry(QRect(10, 10, 281, 48));
  GroupButtons->setTitle("");
  //GroupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout(GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  QPushButton* buttonOk = new QPushButton(tr("BUT_OK"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);
  GroupButtonsLayout->addItem(new QSpacerItem(5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 1);

  QPushButton* buttonCancel = new QPushButton(tr("BUT_CANCEL"), GroupButtons);
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 2);

  QPushButton* buttonHelp = new QPushButton(tr("BUT_HELP"), GroupButtons);
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 3);

  TopLayout->addWidget(GroupButtons);

  connect(buttonOk,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));
}

VisuGUI_CutPlanesDlg::~VisuGUI_CutPlanesDlg()
{}

void VisuGUI_CutPlanesDlg::initFromPrsObject ( VISU::ColoredPrs3d_i* thePrs,
                                               bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TCUTPLANES>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  myCutPane->initFromPrsObject(myPrsCopy);

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

int VisuGUI_CutPlanesDlg::storeToPrsObject (VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;

  int anIsOk = myInputPane->storeToPrsObject(myPrsCopy);
  anIsOk &= GetScalarPane()->storeToPrsObject(myPrsCopy);
  anIsOk &= myCutPane->storeToPrsObject(myPrsCopy);

  VISU::TSameAsFactory<VISU::TCUTPLANES>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

void VisuGUI_CutPlanesDlg::accept()
{
  VisuGUI_ScalarBarBaseDlg::accept();
}

void VisuGUI_CutPlanesDlg::reject() {
  VisuGUI_ScalarBarBaseDlg::reject();
}

QString VisuGUI_CutPlanesDlg::GetContextHelpFilePath()
{
  return "cut_planes_page.html";
}

//###################################################################

/*QWidget* VisuGUI_NumEditItem::createEditor() const
{
  QLineEdit *editline = new QLineEdit(text(), table()->viewport());
  QDoubleValidator *dvalidator = new QDoubleValidator(table()->viewport());
  dvalidator->setDecimals(32);
  editline->setValidator(dvalidator);
  return editline;
}*/

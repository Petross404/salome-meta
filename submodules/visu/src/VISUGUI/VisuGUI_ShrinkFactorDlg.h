// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_ShrinkFactorDlg.h
//  Author : Vadim SANDLER, Open CASCADE S.A.S. (vadim.sandler@opencascade.com)
//
#ifndef VISUGUI_SHRINKFACTORDLG_H
#define VISUGUI_SHRINKFACTORDLG_H

// QT Includes
#include <QDialog>

class QLabel;
class QPushButton;
class SalomeApp_IntSpinBox;
class QGroupBox;
class LightApp_SelectionMgr;
class SalomeApp_Module;
class SVTK_Selector;
class SVTK_ViewWindow;
class VisuGUI;

//=================================================================================
// class    : VisuGUI_ShrinkFactorDlg
// purpose  :
//=================================================================================
class VisuGUI_ShrinkFactorDlg : public QDialog
{ 
  Q_OBJECT

public:
  VisuGUI_ShrinkFactorDlg( VisuGUI* = 0, 
                           bool modal = false );

  ~VisuGUI_ShrinkFactorDlg();

private:
  void                    keyPressEvent( QKeyEvent* );

private :
  LightApp_SelectionMgr*  mySelectionMgr;
  SVTK_ViewWindow*        myViewWindow;
  const SalomeApp_Module* myModule;

  QGroupBox*              GroupC1;
  QPushButton*            buttonOk;
  QPushButton*            buttonHelp;
  QLabel*                 ValueLab;
  SalomeApp_IntSpinBox*   ValueSpin;

public slots:
  void help(); 
  void setShrinkFactor();
  void onSelectionChanged();
};

#endif // VISUGUI_SHRINKFACTORDLG_H

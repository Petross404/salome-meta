// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_CursorDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#include <string>
using namespace std;

#include "VisuGUI.h"
#include "VisuGUI_CursorDlg.h"

#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include "LightApp_Application.h"

#include <SalomeApp_IntSpinBox.h>

#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QGroupBox>
#include <QKeyEvent>

/*!
  Constructor
*/
VisuGUI_CursorDlg::VisuGUI_CursorDlg( QWidget* parent,  const char* name, bool modal )
    : QDialog( parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint )//WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu )
{
  setWindowTitle( name );
  setModal( modal );
  setSizeGripEnabled( TRUE );

  QGridLayout* TopLayout = new QGridLayout( this );
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin( 11 );

  TopGroupBox = new QGroupBox( this );
  //TopGroupBox->setColumnLayout(0, Qt::Vertical );
  //TopGroupBox->layout()->setSpacing( 0 );
  //TopGroupBox->layout()->setMargin( 0 );
  QGridLayout* TopGroupBoxLayout = new QGridLayout( TopGroupBox );
  TopGroupBoxLayout->setAlignment( Qt::AlignTop );
  TopGroupBoxLayout->setSpacing( 6 );
  TopGroupBoxLayout->setMargin( 11 );

  Comment1 = new QLabel( tr( "TextLabel1" ), TopGroupBox );
  Comment1->setAlignment( Qt::AlignCenter );
  TopGroupBoxLayout->addWidget( Comment1, 0, 0 );

  Comment2 = new QLabel(  tr( "TextLabel2" ), TopGroupBox );
  Comment2->setAlignment( Qt::AlignCenter );
  TopGroupBoxLayout->addWidget( Comment2, 1, 0 );

  SpinBox1 = new SalomeApp_IntSpinBox( TopGroupBox );
  SpinBox1->setAcceptNames( false );
  SpinBox1->setMinimum( 0 );
  SpinBox1->setMaximum(100 );
  SpinBox1->setSingleStep( 1 );
  SpinBox1->setValue( 100 );
  SpinBox1->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  SpinBox1->setMinimumSize( 70, 0 );
  QHBoxLayout* SLayout = new QHBoxLayout;
  SLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
  SLayout->addWidget( SpinBox1 );
  SLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
  TopGroupBoxLayout->addLayout( SLayout, 2, 0 );

  GroupButtons = new QGroupBox (this);
  GroupButtons->setGeometry(QRect(10, 10, 281, 48));
  //GroupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout (GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  buttonOk = new QPushButton (tr("&OK"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);

  GroupButtonsLayout->addItem(new QSpacerItem (5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 1);

  buttonCancel = new QPushButton(tr("&Cancel") , GroupButtons);
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 2);

  buttonHelp = new QPushButton(tr("&Help") , GroupButtons);
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 3);

  TopLayout->addWidget(TopGroupBox,  0, 0);
  TopLayout->addWidget(GroupButtons, 1, 0);

  // signals and slots connections
  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
}

/*!
  Destructor
*/
VisuGUI_CursorDlg::~VisuGUI_CursorDlg()
{
}

/*!
  function : onHelp()
  purpose  :
*/
void VisuGUI_CursorDlg::onHelp()
{
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), helpFileName);
  }
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).
                             arg(helpFileName) );
  }
}

/*!
  function : keyPressEvent()
  purpose  :
*/
void VisuGUI_CursorDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VisuGUI : GUI for VISU component
//  File   : VisuGUI_TransparencyDlg.h
//  Author : Nicolas REJNERI
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_TRANSPARENCYDLG_H
#define VISUGUI_TRANSPARENCYDLG_H

// QT Includes
#include <QDialog>

class QLabel;
class QPushButton;
class QSlider;
class LightApp_SelectionMgr;
class SalomeApp_Module;
class SVTK_Selector;
class SVTK_ViewWindow;
class VisuGUI;

//=================================================================================
// class    : VisuGUI_TransparencyDlg
// purpose  :
//=================================================================================
class VisuGUI_TransparencyDlg : public QDialog
{ 
  Q_OBJECT

public:
  VisuGUI_TransparencyDlg( VisuGUI* = 0, 
                           bool modal = false );

  ~VisuGUI_TransparencyDlg();

private:
  void                    keyPressEvent( QKeyEvent* e );

private :
  LightApp_SelectionMgr*  mySelectionMgr;
  SVTK_ViewWindow*        myViewWindow;
  const SalomeApp_Module* myModule;

  QPushButton*            buttonOk;
  QPushButton*            buttonHelp;
  QLabel*                 TextLabelOpaque;
  QLabel*                 ValueLab;
  QLabel*                 TextLabelTransparent;
  QSlider*                Slider1;

public slots:
  void ClickOnOk();
  void ClickOnHelp(); 
  void ValueHasChanged();
  void SetTransparency();
  void onSelectionChanged();
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_InputPanel.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#include "VisuGUI_InputPanel.h"
#include "VisuGUI_BasePanel.h"

#include <QVBoxLayout>

/*!
 * \brief Constructor creates enmpty docable window with invisible QVBox
          to be used as container of child widgets
 */
VisuGUI_InputPanel::VisuGUI_InputPanel( QWidget* theParent )
: QtxDockWidget( tr( "WINDOW_TITLE" ), theParent ),
  myCurrentPanel( 0 )
{
  myGrp = new QWidget( this );
  new QVBoxLayout( myGrp );
  setWidget( myGrp );
}

/*!
 * \brief Destructor: VisuGUI_InputPanel
 */
VisuGUI_InputPanel::~VisuGUI_InputPanel()
{
}

/*!
  \brief Shows thePanel in input panel. If there is visible 
         panel then it is hidden 
  \param thePanel widget
*/
void VisuGUI_InputPanel::showPanel( VisuGUI_BasePanel* thePanel )
{
  if ( !thePanel )
    return;

  setUpdatesEnabled( false );

  if ( myCurrentPanel )
    myCurrentPanel->hide();

  if( isEmpty() )
    show();

  if ( !myPanels.contains( thePanel ) )
  {
    myPanels.insert( thePanel, true );
    thePanel->setParent( myGrp );
    myGrp->layout()->addWidget( thePanel );
    connect( thePanel, SIGNAL( bpClose() ), this, SLOT( onClosePanel() ) );
  }

  thePanel->show();
  myCurrentPanel = thePanel;
  myPanels[ thePanel ] = true;

  setUpdatesEnabled( true );
  repaint();
}

/*!
  \brief Hides thePanel in input panel. 
  \param thePanel widget
*/
void VisuGUI_InputPanel::hidePanel( VisuGUI_BasePanel* thePanel )
{
  if ( !thePanel || myCurrentPanel != thePanel )
    return;

  thePanel->hide();
  myCurrentPanel = 0;
  myPanels[ thePanel ] = false;

  if( isEmpty() )
    hide();
}

/*!
  \brief Hide all children panels
*/
void VisuGUI_InputPanel::clear()
{
  if ( myCurrentPanel )
    hidePanel( myCurrentPanel );
}

/*!
  \brief Returns true if no panels are shown
*/
bool VisuGUI_InputPanel::isEmpty() const
{
  QMap<VisuGUI_BasePanel*, bool>::const_iterator it = myPanels.begin(), itEnd = myPanels.end();
  for( ; it != itEnd; ++it )
  {
    if( it.value() )
      return false;
  }
  return true;
}

/*!
  \brief Returns true if the panel is shown
*/
bool VisuGUI_InputPanel::isShown( VisuGUI_BasePanel* thePanel ) const
{
  return myPanels[ thePanel ];
}

/*!
  \brief Close panel which emits signal close().
*/
void VisuGUI_InputPanel::onClosePanel()
{
  if( VisuGUI_BasePanel* aPanel = dynamic_cast<VisuGUI_BasePanel*>( sender() ) )
    hidePanel( aPanel );
}

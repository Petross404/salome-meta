// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_BuildProgressDlg.h
//  Author : Oleg UVAROV
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_BUILDPROGRESSDLG_H
#define VISUGUI_BUILDPROGRESSDLG_H

#include "VISU_Gen_i.hh"

#include <QDateTime>
#include <QDialog>

class QCheckBox;
class QGroupBox;
class QLCDNumber;
class QLineEdit;
class QPushButton;
class QTimer;


//! Build Progress Dialog.
class VisuGUI_BuildProgressDlg : public QDialog
{
  Q_OBJECT

public:
  VisuGUI_BuildProgressDlg( QWidget* );
  ~VisuGUI_BuildProgressDlg();

  virtual void     setGenerator( VISU::VISU_Gen_i* theGenerator ) { myGenerator = theGenerator; }
  virtual void     show();

  QString          fileName() const { return myFileName; }
  void             setFileName( const QString& theFileName );

private:
  void             keyPressEvent( QKeyEvent* e );

protected slots:
  void             done( int );

  void             onStart();
  void             onClose();
  void             onHelp();

  bool             onBrowseFile();
  void             onTimer();

  void             onBuildCheckBoxClicked();

private:
  void             updateButton( QPushButton*, bool );

private:
  VISU::Result_var myResult;
  VISU::VISU_Gen_i* myGenerator;

  QString          myFileName;
  QTime            myTime;
  QTimer*          myTimer;

  QGroupBox*       mySettingsBox;

  QLineEdit*       myFileNameLineEdit;
  QCheckBox*       myBuildAllCheckBox;
  QCheckBox*       myBuildAtOnceCheckBox;

  QGroupBox*       myProgressBox;

  QPushButton*  myBuildEntitiesButton;

  QCheckBox*       myBuildFieldsCheckBox;
  QPushButton*     myBuildFieldsButton;

  QCheckBox*       myBuildMinMaxCheckBox;
  QPushButton*     myBuildMinMaxButton;

  QCheckBox*       myBuildGroupsCheckBox;
  QPushButton*     myBuildGroupsButton;

  QGroupBox*       myTimeBox;
  QLCDNumber*      myTimeLCDNumber;

  QCheckBox*       myCloseCheckBox;

  QPushButton*     myStartButton;

  double           myCurrentTime;
  bool             myIsRaiseColor;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_SelectionPanel.h
//  Author : Laurent CORNABE & Hubert ROLLAND 
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_SELECTIONPANEL_H
#define VISUGUI_SELECTIONPANEL_H

#include "VisuGUI_Panel.h"

#include <QMap>

#include <vtkSystemIncludes.h>

class QLabel;
class QLineEdit;
class QModelIndex;
class QStackedWidget;
class QTableWidget;
class QTabWidget;

class VisuGUI_GaussPointsSelectionPane;
class VisuGUI_FindPane;
class VisuGUI_SelectionPrefDlg;

typedef vtkIdType TPointID;
typedef vtkIdType TCellID;

struct TValueData
{
  QString Scalar;
  QString Vector;
};

struct TPointData
{
  double X;
  double Y;
  double Z;
  vtkIdType I;
  vtkIdType J;
  vtkIdType K;
  TValueData ValueData;
};

typedef QMap<TPointID, TPointData> TPointDataMap;
typedef QMap<TCellID,  TValueData> TCellDataMap;

struct TCellToPointData
{
  TValueData CellData;
  TPointDataMap PointDataMap;
};

struct TPointToCellData
{
  TPointData PointData;
  TCellDataMap CellDataMap;
};
  
typedef QMap<TCellID,  TCellToPointData> TCellToPointDataMap;
typedef QMap<TPointID, TPointToCellData> TPointToCellDataMap;

class VisuGUI_SelectionPanel: public VisuGUI_Panel
{
  Q_OBJECT

  enum MeshType { StdMesh = 0, ElnoMesh };
  enum TableId { CellStdCell = 0, CellStdPoint, PointStd, CellElno, PointElno };
  enum ColumnId { Cell = 0, Point, X, Y, Z, I, J, K, Scalar, Vector };

public:
  VisuGUI_SelectionPanel( VisuGUI* theModule, QWidget* theParent = 0 );
  virtual ~VisuGUI_SelectionPanel ();

public:
  void                      setSelectionMode( int theId );

protected slots:
  virtual void              onModuleActivated();
  virtual void              onModuleDeactivated();

protected:
  virtual void              keyPressEvent( QKeyEvent* theEvent );
  virtual void              showEvent( QShowEvent* theEvent );
  virtual void              closeEvent( QCloseEvent* theEvent );

private slots:
  virtual void              onApply();
  virtual void              onClose();
  virtual void              onHelp();

  void                      onPreferences();

  void                      onSelectionModeChanged( int theId );
  void                      onSelectionEvent();
  void                      onPointIdEdit();
  void                      onCellIdEdit();
  void                      onDoubleClicked( const QModelIndex& theIndex );

  void                      onIdChanged( int theFirstId, int theSecondId );

signals:
  void                      selectionModeChanged( int );

private:
  VisuGUI_SelectionPrefDlg* preferencesDlg();
  void                      clearFields();

  int                       column( int theTableId, int theColumnId );
  QVariant                  data( int theTableId, int theRow, int theColumnId );
  void                      setData( int theTableId, int theRow, int theColumnId, const QVariant& theValue );
  void                      setRowSpan( int theTableId, int theRow, int theColumnId, int theRowSpan );

private:
  QLabel*                   myMeshName;
  QLabel*                   myFieldName;

  QTabWidget*               myTabWidget;
  QWidget*                  myPointsPane;
  QWidget*                  myCellsPane;

  QLineEdit*                myIDValLbl;
  QStackedWidget*           myPointStackedWg;

  QLineEdit*                myCellIDValLbl;
  QStackedWidget*           myCellStackedWg;

  QMap<int, QTableWidget*>  myTables;

  QWidget*                  myActorsPane;
  QLabel*                   myXPosLbl;
  QLabel*                   myYPosLbl;
  QLabel*                   myZPosLbl;
  QLabel*                   myDXLbl;
  QLabel*                   myDYLbl;
  QLabel*                   myDZLbl;

  VisuGUI_GaussPointsSelectionPane* myGaussPointsPane;

  VisuGUI_FindPane*         myFindPane;

  VisuGUI_SelectionPrefDlg* myPreferencesDlg;

  bool                      myFl;

  QMap< int, QList<int> >   myColumnData;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_ScalarBarDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_SCALARBARDLG_H
#define VISUGUI_SCALARBARDLG_H

#include "VisuGUI_Prs3dDlg.h"

class QTabWidget;


class VisuGUI_InputPane;

namespace VISU
{
  class ScalarMap_i;
}

class VisuGUI_ScalarBarDlg : public VisuGUI_ScalarBarBaseDlg
{
  Q_OBJECT;

 public:
  VisuGUI_ScalarBarDlg (SalomeApp_Module* theModule);
  ~VisuGUI_ScalarBarDlg();

  virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                  bool theInit );

  virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

 protected:
  virtual QString        GetContextHelpFilePath();

 protected:
  VisuGUI_InputPane* myInputPane;
  QTabWidget* myTabBox;

  SALOME::GenericObjPtr<VISU::ScalarMap_i> myPrsCopy;
};

#endif // VISUGUI_SCALARBARDLG_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Slider.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_Sweep.h"
#include "VisuGUI_Tools.h"

#include "SUIT_ResourceMgr.h"

#include "LightApp_SelectionMgr.h"

#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include "VISU_Actor.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_Actor.h"

#include "QtxDockWidget.h"

#include <QMainWindow>
#include <QComboBox>
#include <QFont>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QToolButton>
#include <QToolTip>
#include <QSlider>
#include <QGroupBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QTimer>
#include <QAction>

#include <vtkMath.h>

/*!
  Constructor
*/
VisuGUI_Sweep::VisuGUI_Sweep( VisuGUI* theModule, 
                              QMainWindow* theParent,
                              LightApp_SelectionMgr* theSelectionMgr )
  : QWidget( theParent )
  , myModule( theModule )
  , myViewWindow( NULL )
{
  setWindowTitle( tr("TITLE") );
  setObjectName( tr("TITLE") );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  //----------------------------------------------------------------------------
  QVBoxLayout* aVBoxLayout = new QVBoxLayout( this );

  QTabWidget* aTabWidget = new QTabWidget( this );
  aTabWidget->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed );
  aVBoxLayout->addWidget( aTabWidget );

  {
    QWidget* aNavigationTab = new QWidget();
    {
      QVBoxLayout* aVBoxLayout = new QVBoxLayout( aNavigationTab );
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
        
        QLabel* aStartSweepPosition = new QLabel( aNavigationTab );
        aStartSweepPosition->setText( tr( "START_SWEEP_POSITION" ) );
        aHBoxLayout->addWidget( aStartSweepPosition );
      
        mySweepSlider = new QSlider( aNavigationTab );
        mySweepSlider->setOrientation( Qt::Horizontal );
        mySweepSlider->setMinimum( 0 );
        aHBoxLayout->addWidget( mySweepSlider );
        
        QLabel* anEndSweepPosition = new QLabel( aNavigationTab );
        anEndSweepPosition->setText( tr( "END_SWEEP_POSITION" ) );
        aHBoxLayout->addWidget( anEndSweepPosition );
        
        aVBoxLayout->addLayout( aHBoxLayout );
      }
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
      
        myFirstButton = new QToolButton( aNavigationTab );
        myFirstButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_FIRST" ) ) );
        myFirstButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        connect( myFirstButton, SIGNAL( clicked() ), SLOT( onFirst() ) );
        aHBoxLayout->addWidget( myFirstButton );
        
        myPreviousButton = new QToolButton( aNavigationTab );
        myPreviousButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PREVIOUS" ) ) );
        myPreviousButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        connect( myPreviousButton, SIGNAL( clicked() ), SLOT( onPrevious() ) );
        aHBoxLayout->addWidget( myPreviousButton );
        
        myPlayButton = new QToolButton( aNavigationTab );
        myPlayButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PLAY" ) ) );
        myPlayButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        myPlayButton->setCheckable( true );
        aHBoxLayout->addWidget( myPlayButton );
        
        myNextButton = new QToolButton( aNavigationTab );
        myNextButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_NEXT" ) ) );
        myNextButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        connect( myNextButton, SIGNAL( clicked() ), SLOT( onNext() ) );
        aHBoxLayout->addWidget( myNextButton );
        
        myLastButton = new QToolButton( aNavigationTab );
        myLastButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_LAST" ) ) );
        myLastButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        connect( myLastButton, SIGNAL( clicked() ), SLOT( onLast() ) );
        aHBoxLayout->addWidget( myLastButton );
        
        aVBoxLayout->addLayout( aHBoxLayout );
      }
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
      
        QToolButton* aStopButton = new QToolButton( aNavigationTab );
        aStopButton->setIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SWEEP_STOP" ) ) );
        aStopButton->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
        aStopButton->setCheckable( false );
        connect( aStopButton, SIGNAL( clicked( ) ), SLOT( onStop( ) ) );
        aHBoxLayout->addWidget( aStopButton );
        
        myIsCycled = new QCheckBox( aNavigationTab );
        myIsCycled->setText( tr( "IS_CYCLED" ) );
        myIsCycled->setChecked( false );
        aHBoxLayout->addWidget( myIsCycled );
        
        aVBoxLayout->addLayout( aHBoxLayout );
      }

      aTabWidget->addTab( aNavigationTab, tr( "NAVIGATION_TAB" ) );
    }
  }
  {
    QWidget* aPropertiesTab = new QWidget();
    {
      QGridLayout* aGridLayout = new QGridLayout( aPropertiesTab );     
      {
        QHBoxLayout* aHBoxLayout = new QHBoxLayout();
        
        QLabel* aModeAnnotation = new QLabel( aPropertiesTab );
        aModeAnnotation->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Preferred );
        aModeAnnotation->setText( tr( "MODE" ) );
        aHBoxLayout->addWidget( aModeAnnotation );
        
        mySweepMode = new QComboBox( aPropertiesTab );
        mySweepMode->insertItems( 0, QStringList() 
                                  << tr( "LINEAR" ) 
                                  << tr( "COSINUSOIDAL" ) 
                                  << tr( "SINUSOIDAL" ) );
        mySweepMode->setCurrentIndex( aResourceMgr->integerValue( "VISU", "sweeping_modes", 0 ) );
        aHBoxLayout->addWidget( mySweepMode );
        
        myIntervals = new QComboBox( aPropertiesTab );
        myIntervals->insertItems( 0, QStringList() 
                                  << tr( "[ 0, +PI ]" ) 
                                  << tr( "[ -PI, +PI ]" ) );
        myIntervals->setCurrentIndex( aResourceMgr->integerValue( "VISU", "sweeping_is2PI", 0 ) );
        aHBoxLayout->addWidget( myIntervals );
        
        aGridLayout->addLayout( aHBoxLayout, 0, 0, 1, 2 );
      }
      
      QLabel* aNumberOfStepsAnnotation = new QLabel( aPropertiesTab );
      aNumberOfStepsAnnotation->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Preferred );
      aNumberOfStepsAnnotation->setText( tr( "NUMBER_OF_STEPS" ) );
      aGridLayout->addWidget( aNumberOfStepsAnnotation, 1, 0, 1, 1 );
      
      myNumberOfSteps = new SalomeApp_IntSpinBox( aPropertiesTab );
      myNumberOfSteps->setAcceptNames( false );
      connect( myNumberOfSteps, SIGNAL( valueChanged( int ) ), SLOT( onNumberOfStepsChanged( int ) ) );
      myNumberOfSteps->setValue( aResourceMgr->integerValue( "VISU", "sweeping_number_steps", 40 ) );
      aGridLayout->addWidget( myNumberOfSteps, 1, 1, 1, 1 );
      
      QLabel* aStepDelayAnnotation = new QLabel( aPropertiesTab );
      aStepDelayAnnotation->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Preferred );
      aStepDelayAnnotation->setText( tr( "DELAY_BETWEEN_STEPS" ) );
      aGridLayout->addWidget( aStepDelayAnnotation, 2, 0, 1, 1 );
      
      myStepDelay = new SalomeApp_DoubleSpinBox( aPropertiesTab );
      VISU::initSpinBox( myStepDelay, 0., 99.99, .1, "parametric_precision" );
      myStepDelay->setValue( aResourceMgr->doubleValue("VISU", "sweeping_time_step", 0.1) );
      aGridLayout->addWidget( myStepDelay, 2, 1, 1, 1 );
    }

    aTabWidget->addTab( aPropertiesTab, tr( "PROPERTIES_TAB" ) );
  }

  {
    QSpacerItem* aSpacerItem = new QSpacerItem( 16, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    aVBoxLayout->addItem( aSpacerItem );
  }


  //----------------------------------------------------------------------------
  QtxDockWidget* aQtxDockWidget = new QtxDockWidget( true, theParent );
  theParent->addDockWidget( Qt::BottomDockWidgetArea , aQtxDockWidget );
  aQtxDockWidget->setObjectName( objectName() );
  aQtxDockWidget->setWidget( this );

  myToggleViewAction = aQtxDockWidget->toggleViewAction();
  myToggleViewAction->setIcon( QIcon( aResourceMgr->loadPixmap( "VISU", tr( "ICON_SWEEP_PANEL" ) ) ) );
  myToggleViewAction->setToolTip( tr( "MEN_SWEEP_PANE" ) );
  myToggleViewAction->setText( tr( "MEN_SWEEP_PANE" ) );
  myToggleViewAction->setCheckable( true );
  aQtxDockWidget->setVisible( false );

  connect( myToggleViewAction, SIGNAL( toggled( bool ) ), this, SLOT( onToggleView( bool ) ) );

  //----------------------------------------------------------------------------
  myTimer = new QTimer( this );

  myPlayPixmap = aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PLAY" ) );
  myPausePixmap = aResourceMgr->loadPixmap( "VISU", tr( "ICON_SLIDER_PAUSE" ) );

  connect( myTimer, SIGNAL( timeout() ), SLOT( onTimeout() ) );

  connect( myPlayButton, SIGNAL( toggled( bool ) ), SLOT( onPlay( bool ) ) );

  connect( mySweepSlider, SIGNAL( valueChanged( int ) ), SLOT( onValueChanged( int ) ) );

  connect( myStepDelay, SIGNAL( valueChanged( double ) ), SLOT( onDelayChanged( double ) ) );

  connect( mySweepMode, SIGNAL( currentIndexChanged( int ) ), SLOT( onModeChanged( int ) ) );

  connect( theModule, SIGNAL( moduleDeactivated() ), SLOT( onModuleDeactivated() ) );

  connect( theModule, SIGNAL( moduleActivated() ), SLOT( onModuleActivated() ) );

  connect( theSelectionMgr, SIGNAL( currentSelectionChanged() ), SLOT( onSelectionChanged() ) );

  onSelectionChanged();
  
  onModeChanged( mySweepMode->currentIndex() );
}


//----------------------------------------------------------------------------
VisuGUI_Sweep::~VisuGUI_Sweep()
{}


//----------------------------------------------------------------------------
QAction* VisuGUI_Sweep::toggleViewAction()
{
  return myToggleViewAction;
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onModuleDeactivated()
{
  setHidden( true );

  onEnable( true );
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onModuleActivated()
{
  setHidden( false );
}


//----------------------------------------------------------------------------
namespace
{
  struct TEnabler
  {
    VisuGUI_Sweep* myWidget;
    bool& myIsValidSelection;

    TEnabler( VisuGUI_Sweep* theWidget, bool& theIsValidSelection )
      : myWidget( theWidget )
      , myIsValidSelection( theIsValidSelection )
    {}
    
    ~TEnabler()
    {
      myWidget->onEnable( !myIsValidSelection );
    }
  };
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onSelectionChanged()
{
  VISU_Actor* anActor = NULL;
  VISU::Prs3d_i* aPrs3d = NULL;
  SVTK_ViewWindow* aViewWindow = NULL;

  bool anIsValidSelection = VISU::GetPrs3dSelectionInfo( myModule, aPrs3d, aViewWindow, anActor );

  // To instantiate special helper class which will analyze validity of selection 
  // and make appropritate actions
  TEnabler anEnabler( this, anIsValidSelection );

  anIsValidSelection &= bool( aViewWindow );

  // To keep the pointer on the view window properly it is necessary to get known when
  // it will be destroyed
  if ( myViewWindow != aViewWindow ) {
    if ( myViewWindow )
      disconnect( myViewWindow, SIGNAL( destroyed( QObject * ) ), this, SLOT( onSelectionChanged() ) );

    myViewWindow = aViewWindow;

    if ( myViewWindow )
      connect( myViewWindow, SIGNAL( destroyed( QObject * ) ), this, SLOT( onSelectionChanged() ) );
  }

  if(!VISU::GetActiveViewWindow<SVTK_ViewWindow>(myModule)) {
    myActor = NULL;
    return;
  }

  VISU::ColoredPrs3d_i* aColoredPrs3d = dynamic_cast< VISU::ColoredPrs3d_i* >( aPrs3d );
  anIsValidSelection &= ( aColoredPrs3d && aColoredPrs3d->IsTimeStampFixed() );

  myColoredPrs3d = aColoredPrs3d;

  if ( myColoredPrs3d ) {
    myColoredPrs3d->SetMapScale( 1.0 );
    myColoredPrs3d->UpdateActors();
  }

  anIsValidSelection &= ( anActor && anActor->GetVisibility() );

  myActor = anActor;
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onValueChanged( int value )
{
  bool anIsValidSelection = ( myColoredPrs3d != 0 ) && ( myActor != 0 );

  if ( !anIsValidSelection )
    return;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  int aSweepMode = mySweepMode->currentIndex();
  int aNbSteps = myNumberOfSteps->value();
  
  double anArgument = double( value ) / aNbSteps;

  int anIntervalMode = myIntervals->currentIndex();
  // To correct the input value according to the given interval mode
  if ( aSweepMode != 0 ) {
    switch ( anIntervalMode ) {
    case 0 :
      anArgument = vtkMath::Pi() * anArgument;
      break;
    case 1 :
      anArgument = -vtkMath::Pi() + 2.0 * vtkMath::Pi() * anArgument;
      break;
    default :
      break;
    }
  }
  
  // To correct the input value according to the given sweep mode
  double aValue = anArgument;
  switch ( aSweepMode ) {
  case 1 :
    aValue = ( 1.0 - cos( anArgument ) ) / 2.0;
    break;
  case 2 :
    aValue = sin( anArgument - vtkMath::Pi() / 2.0 );
    break;
  default :
    break;
  }

  try {
    myColoredPrs3d->SetMapScale( aValue );
    myColoredPrs3d->UpdateActor( myActor );
    if(myViewWindow)
      myViewWindow->Repaint();
  } catch (std::exception& exc) {
    INFOS( "Follow exception was occured :\n" << exc.what() );
  } catch (...) {
    INFOS( "Unknown exception was occured!" );
  }

  QApplication::restoreOverrideCursor();
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onDelayChanged( double value )
{
  myTimer->start( int( value * 1000 ) );
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onNumberOfStepsChanged( int value )
{
  int anOldNumberOfSteps = mySweepSlider->maximum();
  mySweepSlider->setMaximum( value );

  double aValue = double( value );

  double aNewSweepValue = aValue / anOldNumberOfSteps * mySweepSlider->value();
  mySweepSlider->setValue( int( aNewSweepValue + 1 ) );

  double aNewPageStep = aValue / 10;
  mySweepSlider->setPageStep( int( aNewPageStep + 1 ) );
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onModeChanged( int )
{
  myIntervals->setEnabled( mySweepMode->currentIndex() != 0 );
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onFirst()
{
  mySweepSlider->setValue( mySweepSlider->minimum() );
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onPrevious()
{
  mySweepSlider->setValue( mySweepSlider->value() - 1 );
}

void VisuGUI_Sweep::onNext()
{
  mySweepSlider->setValue( mySweepSlider->value() + 1 );
}

void VisuGUI_Sweep::onLast()
{
  mySweepSlider->setValue( mySweepSlider->maximum() );
}

//----------------------------------------------------------------------------
void VisuGUI_Sweep::onPlay( bool on )
{
  if ( on ) {
    myPlayButton->setIcon( myPausePixmap );
    myTimer->start( int( myStepDelay->value() * 1000 ) );
    myPlayButton->setChecked( true );
  } else {
    myTimer->stop();
    myPlayButton->setChecked( false );
    myPlayButton->setIcon( myPlayPixmap );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onToggleView( bool on )
{
  if ( !on ) {
    onPlay( on );
  }
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onStop()
{
  onPlay( false );
  mySweepSlider->setValue( mySweepSlider->maximum() );

  if ( myViewWindow )
    myViewWindow->Repaint();
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onEnable( bool on )
{
  onStop();

  mySweepSlider->setEnabled( !on );
  
  myFirstButton->setEnabled( !on );
  myPreviousButton->setEnabled( !on );
  
  myPlayButton->setEnabled( !on );
  
  myNextButton->setEnabled( !on );
  myLastButton->setEnabled( !on );
}


//----------------------------------------------------------------------------
void VisuGUI_Sweep::onTimeout()
{
  int value = mySweepSlider->value();
  if ( value < mySweepSlider->maximum() ) {
    mySweepSlider->setValue( value + 1 );
  } else {
    if ( myIsCycled->isChecked() )
      mySweepSlider->setValue( 0 );
    else
      myPlayButton->setChecked( false );
    //myStopButton->setEnabled( false );
  }
}


//----------------------------------------------------------------------------

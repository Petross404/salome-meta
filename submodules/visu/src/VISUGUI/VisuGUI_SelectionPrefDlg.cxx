// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_SelectionPrefDlg.cxx
//  Author : Oleg UVAROV
//  Module : SALOME
//
#include "VisuGUI_SelectionPrefDlg.h"
#include "VisuGUI.h"
#include "VisuGUI_Tools.h"

#include "VISU_PickingSettings.h"

#include <SUIT_Session.h>
#include <SUIT_Application.h>
#include <SUIT_Desktop.h>
#include <SUIT_Tools.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>

#include <LightApp_Application.h>

#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include <QtxColorButton.h>

#include <QComboBox>
#include <QGroupBox>
#include <QKeyEvent>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QPushButton>

/*!
  Constructor
*/
VisuGUI_SelectionPrefDlg::VisuGUI_SelectionPrefDlg( QWidget* parent )
    : QDialog( parent ? parent : SUIT_Session::session()->activeApplication()->desktop(), 
               Qt::WindowTitleHint | Qt::WindowSystemMenuHint )
{
  setWindowTitle( tr("TLT_SELECTION_PREFERENCES") );
  setSizeGripEnabled( TRUE );
  setModal( true );

  QVBoxLayout* topLayout = new QVBoxLayout( this );
  topLayout->setMargin( 11 ); topLayout->setSpacing( 6 );

  // Cursor (gauss points)
  QGroupBox* CursorGroup = new QGroupBox( tr( "CURSOR_TITLE" ), this );
  QGridLayout* CursorGroupLayout = new QGridLayout (CursorGroup);
  CursorGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
  CursorGroupLayout->setSpacing(6);
  CursorGroupLayout->setMargin(11);

  QLabel* CursorSizeLabel = new QLabel( tr( "CURSOR_SIZE" ), CursorGroup );
  myCursorSizeSpinBox = new SalomeApp_DoubleSpinBox( CursorGroup );
  VISU::initSpinBox( myCursorSizeSpinBox, 0, 1, 0.1, "parametric_precision" );
  myCursorSizeSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  QLabel* PyramidHeightLabel = new QLabel( tr( "PYRAMID_HEIGHT" ), CursorGroup );
  double aHeightMin=1.e-7;
  double aHeightMax=10.;
  double aHeightStep=0.1;
  myPyramidHeightSpinBox = new SalomeApp_DoubleSpinBox(CursorGroup );
  VISU::initSpinBox( myPyramidHeightSpinBox, aHeightMin, aHeightMax, aHeightStep, "length_precision" );  
  myPyramidHeightSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  
  QLabel* SelectionColorLabel = new QLabel( tr( "SELECTION_COLOR" ), CursorGroup );
  mySelectionColorButton = new QtxColorButton( CursorGroup );
  mySelectionColorButton->setColor( Qt::blue );

  CursorGroupLayout->addWidget( CursorSizeLabel, 0, 0 );
  CursorGroupLayout->addWidget( myCursorSizeSpinBox, 0, 1 );
  CursorGroupLayout->addWidget( PyramidHeightLabel, 1, 0 );
  CursorGroupLayout->addWidget( myPyramidHeightSpinBox, 1, 1 );
  CursorGroupLayout->addWidget( SelectionColorLabel, 2, 0 );
  CursorGroupLayout->addWidget( mySelectionColorButton, 2, 1 );

  topLayout->addWidget( CursorGroup );

  // Tolerance (gauss points)
  QGroupBox* ToleranceGroup = new QGroupBox( tr( "TOLERANCE_TITLE" ), this );
  QGridLayout* ToleranceGroupLayout = new QGridLayout (ToleranceGroup);
  ToleranceGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
  ToleranceGroupLayout->setSpacing(6);
  ToleranceGroupLayout->setMargin(11);

  QLabel* PointToleranceLabel = new QLabel( tr( "POINT_TOLERANCE" ), ToleranceGroup );
  myPointToleranceSpinBox = new SalomeApp_DoubleSpinBox(ToleranceGroup );
  VISU::initSpinBox( myPointToleranceSpinBox, 0.001, 10.0, 0.01, "len_tol_precision" );  
  myPointToleranceSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  ToleranceGroupLayout->addWidget( PointToleranceLabel, 0, 0 );
  ToleranceGroupLayout->addWidget( myPointToleranceSpinBox, 0, 1 );

  topLayout->addWidget( ToleranceGroup );

  // Information window
  myInfoWindowGroup = new QGroupBox( tr( "INFO_WINDOW_TITLE" ), this );
  myInfoWindowGroup->setCheckable( true );

  QGridLayout* InfoWindowGroupLayout = new QGridLayout (myInfoWindowGroup);
  InfoWindowGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);

  QLabel* TransparencyLabel = new QLabel( tr( "TRANSPARENCY" ), myInfoWindowGroup );
  myTransparencySpinBox = new SalomeApp_IntSpinBox( 0, 100, 10, myInfoWindowGroup );
  myTransparencySpinBox->setAcceptNames( false );
  myTransparencySpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  InfoWindowGroupLayout->addWidget( TransparencyLabel, 0, 0 );
  InfoWindowGroupLayout->addWidget( myTransparencySpinBox, 0, 1 );

  QLabel* PositionLabel = new QLabel( tr( "POSITION" ), myInfoWindowGroup );
  myPositionComboBox = new QComboBox( myInfoWindowGroup );
  myPositionComboBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myPositionComboBox->addItem( tr( "BELOW_POINT" ) );
  myPositionComboBox->addItem( tr( "TOP_LEFT_CORNER" ) );

  InfoWindowGroupLayout->addWidget( TransparencyLabel, 0, 0 );
  InfoWindowGroupLayout->addWidget( myTransparencySpinBox, 0, 1 );
  InfoWindowGroupLayout->addWidget( PositionLabel, 1, 0 );
  InfoWindowGroupLayout->addWidget( myPositionComboBox, 1, 1 );

  topLayout->addWidget( myInfoWindowGroup );

  // Movement of the camera
  myCameraGroup = new QGroupBox( tr( "CAMERA_TITLE" ), this );
  myCameraGroup->setCheckable( true );

  QGridLayout* CameraGroupLayout = new QGridLayout (myCameraGroup);
  CameraGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);

  QLabel* ZoomFactorLabel = new QLabel( tr( "ZOOM_FACTOR" ), myCameraGroup );
  myZoomFactorSpinBox = new SalomeApp_DoubleSpinBox( myCameraGroup );
  VISU::initSpinBox( myZoomFactorSpinBox, 0.1, 10.0, 0.1, "parametric_precision" );
  myZoomFactorSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  QLabel* StepNumberLabel = new QLabel( tr( "STEP_NUMBER" ), myCameraGroup );
  StepNumberLabel->setToolTip( tr( "STEP_NUMBER_TIP" ) );
  myStepNumberSpinBox = new SalomeApp_IntSpinBox( 1, 100, 1, myCameraGroup );
  myStepNumberSpinBox->setAcceptNames( false );
  myStepNumberSpinBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  CameraGroupLayout->addWidget( ZoomFactorLabel, 0, 0 );
  CameraGroupLayout->addWidget( myZoomFactorSpinBox, 0, 1 );
  CameraGroupLayout->addWidget( StepNumberLabel, 1, 0 );
  CameraGroupLayout->addWidget( myStepNumberSpinBox, 1, 1 );

  topLayout->addWidget( myCameraGroup );

  // Common buttons
  QGroupBox* GroupButtons = new QGroupBox( this );
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setMargin( 11 ); GroupButtonsLayout->setSpacing( 6 );
  
  myButtonOk = new QPushButton( GroupButtons );
  myButtonOk->setText( tr( "BUT_OK"  ) );
  myButtonOk->setAutoDefault( TRUE ); myButtonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonOk );

  myButtonApply = new QPushButton( GroupButtons );
  myButtonApply->setText( tr( "BUT_APPLY"  ) );
  myButtonApply->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonApply );

  GroupButtonsLayout->addStretch();
  
  myButtonCancel = new QPushButton( GroupButtons );
  myButtonCancel->setText( tr( "BUT_CANCEL"  ) );
  myButtonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonCancel );

  myButtonHelp = new QPushButton( GroupButtons );
  myButtonHelp->setText( tr( "BUT_HELP"  ) );
  myButtonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( myButtonHelp );
  /***************************************************************/
  
  //topLayout->addWidget( GroupC1 );
  topLayout->addWidget( GroupButtons );
  
  // signals and slots connections
  connect( myButtonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( myButtonApply,  SIGNAL( clicked() ), this, SLOT( onApply() ) );
  connect( myButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( myButtonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
  
  /* Move widget on the botton right corner of main widget */
  SUIT_Tools::centerWidget( this, parent );
}

/*!
  Destructor
*/
VisuGUI_SelectionPrefDlg::~VisuGUI_SelectionPrefDlg()
{
}

void VisuGUI_SelectionPrefDlg::update()
{
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

  myCursorSizeSpinBox->setValue( aPickingSettings->GetCursorSize() );
  myPyramidHeightSpinBox->setValue( aPickingSettings->GetPyramidHeight() );
  myPointToleranceSpinBox->setValue( aPickingSettings->GetPointTolerance() );

  double* aColor = aPickingSettings->GetColor();
  mySelectionColorButton->setColor( QColor( ( int )( aColor[0] * 255.0 ),
                                            ( int )( aColor[1] * 255.0 ),
                                            ( int )( aColor[2] * 255.0 ) ) );

  myInfoWindowGroup->setChecked( aPickingSettings->GetInfoWindowEnabled() );
  // VSR 28.06.2011 : IPAL 22513: add 0.5 to eliminate any prevision problems
  double transparency = aPickingSettings->GetInfoWindowTransparency() * 100.0 + 0.5;
  myTransparencySpinBox->setValue( (int) transparency );
  myPositionComboBox->setCurrentIndex( aPickingSettings->GetInfoWindowPosition() );
  myCameraGroup->setChecked( aPickingSettings->GetCameraMovementEnabled() );
  myZoomFactorSpinBox->setValue( aPickingSettings->GetZoomFactor() );
  myStepNumberSpinBox->setValue( aPickingSettings->GetStepNumber() );
}

void VisuGUI_SelectionPrefDlg::accept()
{
  onApply();

  QDialog::accept();
}

void VisuGUI_SelectionPrefDlg::onApply()
{
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

  aPickingSettings->SetCursorSize( myCursorSizeSpinBox->value() );
  aPickingSettings->SetPyramidHeight( myPyramidHeightSpinBox->value() );
  aPickingSettings->SetPointTolerance( myPointToleranceSpinBox->value() );

  QColor aButtonColor = mySelectionColorButton->color();
  double aColor[3];
  aColor[0] = aButtonColor.red() / 255.0;
  aColor[1] = aButtonColor.green() / 255.0;
  aColor[2] = aButtonColor.blue() / 255.0;
  aPickingSettings->SetColor( aColor );

  aPickingSettings->SetInfoWindowEnabled( myInfoWindowGroup->isChecked() );
  aPickingSettings->SetInfoWindowTransparency( myTransparencySpinBox->value() / 100.0 );
  aPickingSettings->SetInfoWindowPosition( myPositionComboBox->currentIndex() );
  aPickingSettings->SetCameraMovementEnabled( myCameraGroup->isChecked() );
  aPickingSettings->SetZoomFactor( myZoomFactorSpinBox->value() );
  aPickingSettings->SetStepNumber( myStepNumberSpinBox->value() );

  aPickingSettings->InvokeEvent( VISU::UpdatePickingSettingsEvent,NULL );
}

void VisuGUI_SelectionPrefDlg::onHelp()
{
  QString aHelpFileName = "selection_preferences_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName) );
  }
}

void VisuGUI_SelectionPrefDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_PrimitiveBox.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_PRIMITIVEBOX_H
#define VISUGUI_PRIMITIVEBOX_H

#include <QGroupBox>

class QLabel;
class QLineEdit;
class QPushButton;
class QRadioButton;

class SalomeApp_DoubleSpinBox;
class SalomeApp_IntSpinBox;

class VisuGUI_PrimitiveBox : public QGroupBox
{
  Q_OBJECT

public:
  VisuGUI_PrimitiveBox( QWidget* );
  ~VisuGUI_PrimitiveBox() {}

public:
  int                      getPrimitiveType() const { return myPrimitiveType; }
  void                     setPrimitiveType( int );

  float                    getClamp() const;
  void                     setClamp( float );
  void                     setClampMaximum( float );

  QString                  getMainTexture() const { return myMainTexture; }
  void                     setMainTexture( const QString& );

  QString                  getAlphaTexture() const { return myAlphaTexture; }
  void                     setAlphaTexture( const QString& );

  float                    getAlphaThreshold() const;
  void                     setAlphaThreshold( float );

  int                      getResolution() const;
  void                     setResolution( int );

  int                      getFaceNumber() const;
  void                     setFaceNumber( int );

  int                      getFaceLimit() const;
  void                     setFaceLimit( int );

protected slots:
  void                     onTogglePointSprite();
  void                     onToggleOpenGLPoint();
  void                     onToggleGeomSphere();

  void                     onBrowseMainTexture();
  void                     onBrowseAlphaTexture();

  void                     onResolutionChanged( int );

private:
  int                      myPrimitiveType;

  QRadioButton*            myPointSpriteButton;
  QRadioButton*            myOpenGLPointButton;
  QRadioButton*            myGeomSphereButton;

  QString                  myMainTexture;
  QString                  myAlphaTexture;

  QLabel*                  myClampLabel;
  SalomeApp_DoubleSpinBox* myClampSpinBox;

  QLabel*                  myMainTextureLabel;
  QLineEdit*               myMainTextureLineEdit;
  QPushButton*             myMainTextureButton;

  QLabel*                  myAlphaTextureLabel;
  QLineEdit*               myAlphaTextureLineEdit;
  QPushButton*             myAlphaTextureButton;

  QLabel*                  myAlphaThresholdLabel;
  SalomeApp_DoubleSpinBox* myAlphaThresholdSpinBox;

  QLabel*                  myResolutionLabel;
  SalomeApp_IntSpinBox*    myResolutionSpinBox;

  QLabel*                  myFaceNumberLabel;
  QLineEdit*               myFaceNumberLineEdit;

  QLabel*                  myFaceLimitLabel;
  SalomeApp_IntSpinBox*    myFaceLimitSpinBox;
};



#endif

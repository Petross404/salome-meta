// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef DIALOGBOX_CLIPPINGPANEL_H
#define DIALOGBOX_CLIPPINGPANEL_H


class VisuGUI;
class QListWidget;
class QTableWidget;
class VisuGUI_ClippingPlaneDlg;
class QCheckBox;
class SVTK_ViewWindow;
class SUIT_ViewWindow;
class vtkPolyDataMapper;
class vtkImageData;
class vtkEDFCutter;

#include "VISU_Prs3d_i.hh"
#include "VisuGUI_Panel.h"
//#include <VisuGUI_SegmentationMgr.h>
#include <VISU_ClippingPlaneMgr.hxx>

#include <QList>
#include <vtkActor.h>
#include <vtkPlane.h>

#include <SALOMEDSClient_SObject.hxx>


/*class CutPlaneFunction: public vtkPlane
{
public:
  static CutPlaneFunction* New();

  vtkTypeMacro(CutPlaneFunction, vtkPlane);

  virtual double EvaluateFunction(double x[3]);
  virtual double EvaluateFunction(double x, double y, double z);

  void setActive(bool theActive);
  bool isActive() const { return myIsActive; }

  void setPlaneObject(_PTR(SObject) aSOPlane) { mySObject = aSOPlane; }
  _PTR(SObject) getPlaneObject() const { return mySObject; }

protected:
  CutPlaneFunction();
  ~CutPlaneFunction();

private:
  bool myIsActive;
  _PTR(SObject) mySObject;
};



struct PlaneDef
{
  vtkSmartPointer<CutPlaneFunction> plane;
  bool isAuto;
  QString name;
};
*/

class PreviewPlane
{
public:
  //PreviewPlane(SVTK_ViewWindow* theWindow, const PlaneDef& thePlane, const double* theBounds);
  PreviewPlane(SVTK_ViewWindow* theWindow, VISU_CutPlaneFunction* thePlane, const double* theBounds);
  ~PreviewPlane();

  void setVisible(bool theVisible)
  { myActor->SetVisibility(theVisible); }

private:
  SVTK_ViewWindow* myWindow;

  vtkActor* myActor;
  vtkPolyDataMapper* myMapper;
  const double* myBounds;
  vtkPlane* myPlane;
  vtkImageData* myBox;
  vtkEDFCutter* myCutter;
};


//typedef QList<PlaneDef> QListOfPlanes;
typedef QList<vtkPlane*> ListOfvtkPlanes;
typedef QList<PreviewPlane*> ListOfPreview;





class VisuGUI_ClippingPanel: public VisuGUI_Panel
{
  Q_OBJECT
public:
  VisuGUI_ClippingPanel(VisuGUI* theModule, QWidget* theParent = 0);
  ~VisuGUI_ClippingPanel();

  void init();

  //protected:
  //virtual void showEvent(QShowEvent* event);
  //virtual void hideEvent(QHideEvent* event);

public slots:
  void onPresentationCreate(VISU::Prs3d_i* thePrs);

protected slots:
  virtual void onApply();
  virtual void onHelp();

private slots:
  void onNew();
  void onNewPlane();
  void onEdit();
  void onPlaneEdited();
  void onPlaneDelete();
  void onCancelDialog();
  void setPlanesNonActive(bool theState);
  void setPlanesVisible(bool theVisible);
  void onWindowActivated(SUIT_ViewWindow* theWindow);
  void onCellChanged(int row, int col);
  void onObjectDelete(QString theEntry);
  void onPrsSelected(int thePrs);
  void onWindowDestroyed(QObject* theWnd);

 private:
  void fillPrsList();
  void fillPlanesList();

  bool isAutoApply() const;

  VISU_ClippingPlaneMgr& getPlanesMgr() const;


  //void updatePlane(_PTR(SObject) theObject, PlaneDef& thePlane);
  //PlaneDef createEmptyPlane();
  //static bool containsPlane(VISU::Prs3d_i* thePrs, const PlaneDef& thePlane);

  QString getPrsName(VISU::Prs3d_i* thePrs);

  bool isVISUDataReady();

  void applyPlaneToPrs(int thePlaneNum, int thePrsNum, bool isChecked);

  //QList<VISU::Prs3d_i*> getPrsList(_PTR(Study) theStudy, _PTR(SObject) theObject);
  QStringList getPrsList(_PTR(Study) theStudy, _PTR(SObject) theObject);
  VISU::Prs3d_i* getPrs(QString theEntry);

  void updateAllPrsActors();

  QListWidget* myPrsList;
  QTableWidget* myPlanesList;

  QCheckBox* myShowPlanes;
  QCheckBox* myNonActivePlanes;
  QCheckBox* myAutoApply;
  
  //QListOfPlanes myPlanes;
  //  QList<VISU::Prs3d_i*> myPresentations;
  QStringList myPresentations; // Stores entries of presentation objects
  ListOfPreview myPreview;

  VisuGUI_ClippingPlaneDlg* myPlaneDlg;
  //int myEditingPlanePos;
  SVTK_ViewWindow* myViewWindow;
  bool myIsApplied;
  //QList<int> myCheckedPlanes;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_TransparencyDlg.cxx
//  Author : Nicolas REJNERI
//  Module : VISU
//  $Header$
//
#include "VisuGUI_TransparencyDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_Tools.h"
#include "VISU_Actor.h"

#include "SUIT_Desktop.h"
#include "SUIT_OverrideCursor.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"
#include "SALOME_InteractiveObject.hxx"

#include "LightApp_Study.h"
#include "LightApp_SelectionMgr.h"
#include "LightApp_Application.h"

#include "SVTK_ViewWindow.h"

// QT Includes
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QLayout>
#include <QGroupBox>
#include <QKeyEvent>

const int DEFAULT_OPACITY = 100;
const int UNDEF_OPACITY = -1;

//=================================================================================
// class    : VisuGUI_TransparencyDlg()
// purpose  :
//
//=================================================================================
VisuGUI_TransparencyDlg::VisuGUI_TransparencyDlg( VisuGUI* theModule,
                                                  bool modal )
  : QDialog( VISU::GetDesktop( theModule ), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
    myModule(theModule),
    mySelectionMgr( VISU::GetSelectionMgr( theModule ) ),
    myViewWindow( VISU::GetActiveViewWindow<SVTK_ViewWindow>(theModule) )
{
  setModal( modal );
  setAttribute( Qt::WA_DeleteOnClose );
    
  setWindowTitle(tr("TRANSPARENCY_TITLE" ));
  setSizeGripEnabled(TRUE);
  QGridLayout* VisuGUI_TransparencyDlgLayout = new QGridLayout(this);
  VisuGUI_TransparencyDlgLayout->setSpacing(6);
  VisuGUI_TransparencyDlgLayout->setMargin(11);

  /*************************************************************************/
  QGroupBox* GroupC1 = new QGroupBox(this);
  //GroupC1->setColumnLayout(0, Qt::Vertical);
  //GroupC1->layout()->setSpacing(0);
  //GroupC1->layout()->setMargin(0);
  QGridLayout* GroupC1Layout = new QGridLayout(GroupC1);
  GroupC1Layout->setAlignment(Qt::AlignTop);
  GroupC1Layout->setSpacing(6);
  GroupC1Layout->setMargin(11);

  TextLabelTransparent = new QLabel(tr("TRANSPARENCY_TRANSPARENT" ), GroupC1);
  TextLabelTransparent->setAlignment(Qt::AlignRight);
  GroupC1Layout->addWidget(TextLabelTransparent, 0, 2);

  ValueLab = new QLabel(GroupC1);
  ValueLab->setAlignment(Qt::AlignCenter);
  ValueLab->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  QFont fnt = ValueLab->font(); fnt.setBold(true); ValueLab->setFont(fnt);
  GroupC1Layout->addWidget(ValueLab, 0, 1);

  TextLabelOpaque = new QLabel(tr("TRANSPARENCY_OPAQUE" ), GroupC1);
  TextLabelOpaque->setAlignment(Qt::AlignLeft);
  GroupC1Layout->addWidget(TextLabelOpaque, 0, 1);

  Slider1 = new QSlider( Qt::Horizontal, GroupC1 );
  Slider1->setMinimum( 0 );
  Slider1->setMaximum( 100 );
  Slider1->setPageStep( 10 );
  Slider1->setSingleStep( 1 );  
  Slider1->setFocusPolicy(Qt::NoFocus);
  Slider1->setMinimumSize(300, 0);
  Slider1->setTickPosition(QSlider::TicksAbove);
  Slider1->setTickInterval(10);
  Slider1->setTracking(true);
  GroupC1Layout->addWidget(Slider1, 1, 0, 1, 3);

  /*************************************************************************/
  QGroupBox* GroupButtons = new QGroupBox(this);
  //GroupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout(GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  buttonOk = new QPushButton(GroupButtons);
  buttonOk->setText(tr("BUT_CLOSE"));
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);

  buttonHelp = new QPushButton(GroupButtons);
  buttonHelp->setText(tr("BUT_HELP"));
  buttonHelp->setAutoDefault(TRUE);

  GroupButtonsLayout->addWidget(buttonOk, 0, 0);
  GroupButtonsLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 1);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 2);

  VisuGUI_TransparencyDlgLayout->addWidget(GroupC1,      0, 0);
  VisuGUI_TransparencyDlgLayout->addWidget(GroupButtons, 1, 0);

  // Initial state
  this->onSelectionChanged();

  // signals and slots connections : after ValueHasChanged()
  connect(buttonOk, SIGNAL(clicked()),         this, SLOT(ClickOnOk()));
  connect(buttonHelp, SIGNAL(clicked()),       this, SLOT(ClickOnHelp()));
  connect(Slider1,  SIGNAL(valueChanged(int)), this, SLOT(SetTransparency()));
  connect(Slider1,  SIGNAL(sliderMoved(int)),  this, SLOT(ValueHasChanged()));
  connect(mySelectionMgr,  SIGNAL(currentSelectionChanged()), this, SLOT(onSelectionChanged()));
}

//=================================================================================
// function : ~VisuGUI_TransparencyDlg()
// purpose  :
//=================================================================================
VisuGUI_TransparencyDlg::~VisuGUI_TransparencyDlg()
{
  // no need to delete child widgets, Qt does it all for us
}

//=======================================================================
// function : ClickOnOk()
// purpose  :
//=======================================================================
void VisuGUI_TransparencyDlg::ClickOnOk()
{
  close();
}

//=======================================================================
// function : ClickOnHelp()
// purpose  :
//=======================================================================
void VisuGUI_TransparencyDlg::ClickOnHelp()
{
  QString aHelpFileName = "viewing_3d_presentations_page.html#width_opacity_anchor";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
                QString platform;
#ifdef WIN32
                platform = "winapplication";
#else
                platform = "application";
#endif
    SUIT_MessageBox::warning(0, tr("WRN_WARNING"),
                             tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             tr("BUT_OK"));
  }
}

//=================================================================================
// function : SetTransparency()
// purpose  : Called when value of slider change
//          : or the first time as initilisation
//=================================================================================
void VisuGUI_TransparencyDlg::SetTransparency()
{
  if( myViewWindow ) {
    SUIT_OverrideCursor wc;
    float opacity = (100 - this->Slider1->value() ) / 100.;

    SALOME_ListIO aList;
    mySelectionMgr->selectedObjects(aList);

    SALOME_ListIteratorOfListIO anIter(aList);
    for (; anIter.More(); anIter.Next()) {
      Handle(SALOME_InteractiveObject) anIO = anIter.Value();
      VISU_Actor* anActor = VISU::FindActor(VISU::GetAppStudy(myModule), myViewWindow, anIO->getEntry());
      if (anActor)
        anActor->SetOpacity(opacity);
      else {
        VISU_ActorBase* anActor = VISU::FindActorBase(VISU::GetAppStudy(myModule), myViewWindow, anIO->getEntry());
	if (anActor)
	  anActor->SetOpacity(opacity);
      }
    }
    myViewWindow->Repaint();
  }
  ValueHasChanged();
}

//=================================================================================
// function : ValueHasChanged()
// purpose  : Called when user moves a slider
//=================================================================================
void VisuGUI_TransparencyDlg::ValueHasChanged()
{
  ValueLab->setText(QString::number(this->Slider1->value()) + "%");
}

//=================================================================================
// function : onSelectionChanged()
// purpose  : Called when selection is changed
//=================================================================================
void VisuGUI_TransparencyDlg::onSelectionChanged()
{
  if ( myViewWindow ) {
    int opacity = UNDEF_OPACITY;

    SALOME_ListIO aList;
    mySelectionMgr->selectedObjects(aList);

    SALOME_ListIteratorOfListIO It (aList);
    for (; It.More(); It.Next()) {
      Handle(SALOME_InteractiveObject) IO = It.Value();
      if (!IO.IsNull()) {
	int op = UNDEF_OPACITY;
	VISU_Actor* anActor = VISU::FindActor(VISU::GetAppStudy(myModule), myViewWindow, IO->getEntry());
	if (anActor) {
	  op = int(anActor->GetOpacity() * 100. + 0.5);
	}
	else {
	  VISU_ActorBase* anActor = VISU::FindActorBase(VISU::GetAppStudy(myModule), myViewWindow, IO->getEntry());
	  if (anActor)
	    op = int(anActor->GetOpacity() * 100. + 0.5);
	}
	if ( op != UNDEF_OPACITY ) {
	  if ( opacity == UNDEF_OPACITY )
	    opacity = op;
	  else if ( opacity != op ) {
	    opacity = DEFAULT_OPACITY;
	    break;
	  }
	}
      }
    }
    Slider1->setValue( 100 - ( opacity == UNDEF_OPACITY ? DEFAULT_OPACITY : opacity ) );
  }
  ValueHasChanged();
}

//=================================================================================
// function : keyPressEvent()
// purpose  :
//=================================================================================
void VisuGUI_TransparencyDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      ClickOnHelp();
    }
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_CutLinesDlg.cxx
//  Author : VSV
//  Module : VISU

#include "VisuGUI_CutLinesDlg.h"

#include "VISU_ColoredPrs3dFactory.hh"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_Gen_i.hh"
#include "VISU_CutLines_i.hh"
#include "VISU_ColoredPrs3dFactory.hh"

#include "VISU_PipeLine.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_CutLinesPL.hxx"

#include "SVTK_ViewWindow.h"

#include "LightApp_Application.h"
#include "SalomeApp_Study.h"
#include <SalomeApp_DoubleSpinBox.h>

#include "SUIT_Desktop.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

//TO DO
//#include "OB_Browser.h"

#include "VISU_TableDlg.h"

#include <QLayout>
#include <QTabWidget>
#include <QLineEdit>
#include <QValidator>
#include <QButtonGroup>
#include <QKeyEvent>
#include <QLabel>
#include <QPushButton>
#include <QHeaderView>
#include <QRadioButton>
#include <QTableWidget>

#include <vtkRenderer.h>
#include <vtkPolyData.h>
#include <vtkAppendPolyData.h>
#include <vtkDataSetMapper.h>
#include <vtkGlyph3D.h>
#include <vtkPolyDataMapper.h>
#include <vtkFloatArray.h>
#include <vtkGlyphSource2D.h>

using namespace std;

VisuGUI_CutLinesDlg::VisuGUI_CutLinesDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule),
    myPreviewActor(0),
    myPreviewActorGlyphs(0)
{
  setWindowTitle("Cut Lines Definition");
  setSizeGripEnabled(true);

  QVBoxLayout* aMainLayout = new QVBoxLayout (this);
  aMainLayout->setMargin( 7 );
  aMainLayout->setSpacing(5);

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  hasInit = false;  

  // Tab pane
  myTabBox = new QTabWidget(this);

  // Plane of lines
  QFrame* aPlanePane = new QFrame(this);
  QVBoxLayout* aPlaneLayout = new QVBoxLayout(aPlanePane);
  aPlaneLayout->setMargin( 5 );
  aPlaneLayout->setSpacing(6);

  mySelPlane = new QButtonGroup( aPlanePane);
  QGroupBox* aGB = new QGroupBox( tr( "TXT_ORIENTATION" ), aPlanePane );
  QHBoxLayout* aHBLay = new QHBoxLayout( aGB );
  //mySelPlane->setInsideSpacing( 5 );
  //mySelPlane->setInsideMargin( 5 );

  QRadioButton* aBxy = new QRadioButton( tr( "PARALLEL_XOY" ), aGB);  // 0
  QRadioButton* aByz = new QRadioButton( tr( "PARALLEL_YOZ" ), aGB);  // 1
  QRadioButton* aBzx = new QRadioButton( tr( "PARALLEL_ZOX" ), aGB);  // 2
  //aBxy->setChecked(false);
  //aByz->setChecked(false);
  aHBLay->addWidget( aBxy );
  aHBLay->addWidget( aByz );
  aHBLay->addWidget( aBzx );

  mySelPlane->addButton( aBxy, 0 );
  mySelPlane->addButton( aByz, 1 );
  mySelPlane->addButton( aBzx, 2 );

  aBzx->setChecked(true);

  aPlaneLayout->addWidget( aGB );

  QGroupBox* aRotBox = new QGroupBox( tr( "LBL_ROTATION" ), aPlanePane );
  //aRotBox->setColumnLayout(2, Qt::Horizontal );
  QGridLayout* aGridLay = new QGridLayout( aRotBox );

  myRotXLbl = new QLabel( tr( "LBL_ROT_X" ), aRotBox);
  aGridLay->addWidget( myRotXLbl, 0, 0 );
  myRotXSpn = new SalomeApp_DoubleSpinBox( aRotBox );
  VISU::initSpinBox( myRotXSpn, -45., 45., 5., "angle_precision" );
  aGridLay->addWidget( myRotXSpn, 0, 1 );
  myRotXSpn->setValue( 0 );
  myRotYLbl = new QLabel( tr( "LBL_ROT_Y" ), aRotBox );
  aGridLay->addWidget( myRotYLbl, 1, 0 );
  myRotYSpn = new SalomeApp_DoubleSpinBox( aRotBox );
  VISU::initSpinBox( myRotYSpn, -45., 45., 5., "angle_precision" );  
  aGridLay->addWidget( myRotYSpn, 1, 1 );
  myRotYSpn->setValue( 0 );  

  aPlaneLayout->addWidget( aRotBox );

  QGroupBox* aBaseBox = new QGroupBox (tr("BASE_PLANE_POS"), aPlanePane);
  aHBLay = new QHBoxLayout( aBaseBox );
  myBasePlanePos = new QLineEdit (aBaseBox);
  aHBLay->addWidget( myBasePlanePos );
  mydvalidator = new QDoubleValidator(this);
  mydvalidator->setDecimals(32);
  myBasePlanePos->setValidator(mydvalidator);

  myCBSetDef = new QCheckBox (tr("SET_DEFAULT"),aBaseBox);
  aHBLay->addWidget( myCBSetDef );

  aPlaneLayout->addWidget(aBaseBox);

  QWidget* aPosBox = new QWidget (aPlanePane);
  aHBLay = new QHBoxLayout( aPosBox );  
  aHBLay->setSpacing(5);
  
  aHBLay->addWidget( new QLabel (tr("LBL_POS"), aPosBox) );
  myPosSpn = new SalomeApp_DoubleSpinBox (aPosBox);
  VISU::initSpinBox( myPosSpn, 0., 1., .1, "parametric_precision" );
  myPosSpn->setValue(0.5);
  aHBLay->addWidget( myPosSpn );
  aPlaneLayout->addWidget(aPosBox);

  myPreviewCheck = new QCheckBox (tr("LBL_SHOW_PREVIEW"), aPlanePane);
  myPreviewCheck->setChecked( aResourceMgr->booleanValue("VISU", "show_preview", false) );
  aPlaneLayout->addWidget(myPreviewCheck);

  myAllCurvesInvertedCheck = new QCheckBox(tr("LBL_INVERT_CURVES"), aPlanePane);
  myAllCurvesInvertedCheck->setChecked(false);
  aPlaneLayout->addWidget(myAllCurvesInvertedCheck);

  myUseAbsoluteLengthCheck = new QCheckBox(tr("LBL_ABSOLUTE_LENGTH"), aPlanePane);
  myUseAbsoluteLengthCheck->setChecked(false);
  aPlaneLayout->addWidget(myUseAbsoluteLengthCheck);

  myCreateTable = new QCheckBox (tr("LBL_GENERATE_TABLE"), aPlanePane);
  myCreateTable->setChecked( aResourceMgr->booleanValue("VISU", "generate_data_table", true) );
  aPlaneLayout->addWidget( myCreateTable );

  QWidget* aCheckPane = new QWidget(aPlanePane);
  aHBLay = new QHBoxLayout( aCheckPane );
  aHBLay->setMargin( 0 );
  aHBLay->setSpacing( 0 );
  aHBLay->insertSpacing( 0, 20 );
  myCurvesCheck = new QCheckBox(tr("LBL_GENERATE_CURVES"), aCheckPane);
  aHBLay->addWidget( myCurvesCheck );
  myCurvesCheck->setChecked( aResourceMgr->booleanValue("VISU", "generate_curves", true) );
  myCurvesCheck->setEnabled( aResourceMgr->booleanValue("VISU", "generate_data_table", true) );
  aPlaneLayout->addWidget( aCheckPane );

  aPlaneLayout->addStretch();

  myTabBox->addTab( aPlanePane, tr("LBL_LINES_PLANE") );

  // Number of lines
  QFrame* aLinesPane = new QFrame(this);
  QVBoxLayout* aLinesLayout = new QVBoxLayout( aLinesPane );
  aLinesLayout->setMargin( 5 );
  aLinesLayout->setSpacing( 6 );

  mySelPlane2 = new QButtonGroup( aLinesPane);
  aGB = new QGroupBox( tr( "TXT_ORIENTATION" ), aLinesPane );
  aHBLay = new QHBoxLayout( aGB );
  //mySelPlane2->setInsideSpacing( 5 );
  //mySelPlane2->setInsideMargin( 5 );

  QRadioButton* aBxy2 = new QRadioButton( tr( "PARALLEL_XOY" ), aGB);  // 0
  QRadioButton* aByz2 = new QRadioButton( tr( "PARALLEL_YOZ" ), aGB);  // 1
  QRadioButton* aBzx2 = new QRadioButton( tr( "PARALLEL_ZOX" ), aGB);  // 2

  aHBLay->addWidget( aBxy2 );
  aHBLay->addWidget( aByz2 );
  aHBLay->addWidget( aBzx2 );

  mySelPlane2->addButton( aBxy2, 0 );
  mySelPlane2->addButton( aByz2, 1 );
  mySelPlane2->addButton( aBzx2, 2 );
  
  aBzx2->setEnabled(false);
  aByz2->setChecked(true);
  aLinesLayout->addWidget( aGB );

  QGroupBox* aRotBox2 = new QGroupBox( tr( "LBL_ROTATION" ), aLinesPane );
  //aRotBox2->setColumnLayout(2, Qt::Horizontal );
  aGridLay = new QGridLayout( aRotBox2 );

  myRotXLbl2 = new QLabel( tr( "LBL_ROT_X" ), aRotBox2);
  aGridLay->addWidget( myRotXLbl2, 0, 0 );
  myRotXSpn2 = new SalomeApp_DoubleSpinBox( aRotBox2 );
  VISU::initSpinBox( myRotXSpn2, -45., 45., 5., "angle_precision" );
  aGridLay->addWidget( myRotXSpn2, 0, 1 );
  myRotXSpn2->setValue( 0 );
  myRotYLbl2 = new QLabel( tr( "LBL_ROT_Y" ), aRotBox2 );
  aGridLay->addWidget( myRotYLbl2, 1, 0 );
  myRotYSpn2 = new SalomeApp_DoubleSpinBox( aRotBox2 );
  VISU::initSpinBox( myRotYSpn2, -45., 45., 5., "angle_precision" );
  aGridLay->addWidget( myRotYSpn2, 1, 1 );
  myRotYSpn2->setValue( 0 );
  aLinesLayout->addWidget( aRotBox2 );

  QWidget* aNbBox = new QWidget(aLinesPane);
  aHBLay = new QHBoxLayout( aNbBox ); 
  aHBLay->setSpacing(5);
  aHBLay->addWidget( new QLabel( tr( "LBL_NB_PLANS" ), aNbBox ) );
  myNbSpn = new SalomeApp_DoubleSpinBox( aNbBox );
  VISU::initSpinBox( myNbSpn, 1., 100., 1., "parametric_precision" );
  myNbSpn->setValue( 10 );
  aHBLay->addWidget( myNbSpn );

  aLinesLayout->addWidget( aNbBox );

  myPosTable = new QTableWidget(aLinesPane );
  myPosTable->setMaximumHeight( 227 );
  myPosTable->setMinimumWidth( 294 );
  myPosTable->setColumnCount(2);
  myPosTable->setRowCount(0);

  myPosTable->setItemDelegateForColumn( 0,
         new NumDelegateItem( myPosTable, NumDelegateItem::NV_Real) );

  myPosTable->setEditTriggers( (QAbstractItemView::EditTriggers)(QAbstractItemView::AllEditTriggers &(~QAbstractItemView::CurrentChanged)));

  QStringList aLabels;
  aLabels.append( tr("Position" ));
  aLabels.append( tr("Set default" ));
  myPosTable->setHorizontalHeaderLabels( aLabels );
  
  aLinesLayout->addWidget( myPosTable );

  QWidget* aPosBox2 = new QWidget(aLinesPane);
  aHBLay = new QHBoxLayout( aPosBox2 );
  aHBLay->setSpacing(5);
  aHBLay->addWidget( new QLabel( tr( "LBL_POS" ), aPosBox2 ) );
  myPosSpn2 = new SalomeApp_DoubleSpinBox( aPosBox2 );
  VISU::initSpinBox( myPosSpn2, 0., 1., .1, "parametric_precision" );
  myPosSpn2->setValue( 0.5 );
  aHBLay->addWidget( myPosSpn2 );

  
  aLinesLayout->addWidget( aPosBox2 );
  aLinesLayout->addStretch();

  myTabBox->addTab( aLinesPane, tr("LBL_LINES_CUT") );

  myInputPane = new VisuGUI_InputPane(VISU::TCUTLINES, theModule, this);
  myTabBox->addTab(GetScalarPane(), tr("Scalar Bar"));
  myTabBox->addTab(myInputPane, tr("Input"));

  aMainLayout->addWidget(myTabBox);

  // Dialog buttons
  QFrame* aBtnBox = new QFrame(this);
  aHBLay = new QHBoxLayout( aBtnBox );
  aBtnBox->setFrameStyle(QFrame::Box | QFrame::Sunken);
  aBtnBox->setLineWidth( 1 );
  //aHBLay->setAutoAdd( false );
  aHBLay->setSpacing( 5 );
  aHBLay->setMargin( 11 );

  QPushButton* aOkBtn = new QPushButton (tr("BUT_OK"), aBtnBox);
  aOkBtn->setAutoDefault( TRUE );
  aOkBtn->setDefault( true );
  aHBLay->addWidget(aOkBtn);

  aHBLay->addStretch();

  QPushButton* aCloseBtn = new QPushButton(tr("BUT_CANCEL"), aBtnBox);
  aHBLay->addWidget(aCloseBtn);

  QPushButton* aHelpBtn = new QPushButton(tr("BUT_HELP"), aBtnBox);
  aHBLay->addWidget(aHelpBtn);

  aMainLayout->addWidget(aBtnBox);

  // signals and slots connections
  connect(mySelPlane    , SIGNAL(buttonClicked(int))          , this, SLOT(onPlaneSelect(int)));
  connect(myCBSetDef    , SIGNAL(toggled(bool))         , this, SLOT(setBaseDefault()));
  connect(myPosSpn      , SIGNAL(valueChanged(double))  , this, SLOT(setBaseDefault()));
  connect(mySelPlane2   , SIGNAL(buttonClicked(int))          , this, SLOT(onCutSelect(int)));
  connect(myNbSpn       , SIGNAL(valueChanged(double))  , this, SLOT(DrawTable()));
  connect(myPosTable->horizontalHeader(), SIGNAL(sectionClicked(int))          , this, SLOT(setDefault(int)));
  connect(myPosSpn2     , SIGNAL(valueChanged(double))  , this, SLOT(DrawTable()));
  connect(myPreviewCheck, SIGNAL(toggled(bool))         , this, SLOT(onPreviewCheck(bool)));
  connect(myAllCurvesInvertedCheck, SIGNAL(toggled(bool)),this, SLOT(onAllCurvesInvertedCheck(bool)));
  connect(myPosTable    , SIGNAL(cellChanged(int, int)), this, SLOT(onValueChanged(int, int)));
  connect(myRotXSpn     , SIGNAL(valueChanged(double))  , this, SLOT(onRotation(double)));
  connect(myRotYSpn     , SIGNAL(valueChanged(double))  , this, SLOT(onRotation(double)));
  connect(myRotXSpn2    , SIGNAL(valueChanged(double))  , this, SLOT(onRotation(double)));
  connect(myRotYSpn2    , SIGNAL(valueChanged(double))  , this, SLOT(onRotation(double)));
  connect(myBasePlanePos, SIGNAL(textChanged(const QString&)), this, SLOT(onPlanePos(const QString&)));
  connect(myCreateTable , SIGNAL(toggled(bool)), myCurvesCheck, SLOT(setEnabled(bool)));

  connect(aOkBtn, SIGNAL(clicked()), this, SLOT(accept()));
  connect(aCloseBtn, SIGNAL(clicked()), this, SLOT(reject()));
  connect(aHelpBtn, SIGNAL(clicked()), this, SLOT(onHelp()));
}

/*!
  Destructor
*/
VisuGUI_CutLinesDlg::~VisuGUI_CutLinesDlg()
{
  deletePlanes();
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
    vf->Repaint();
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::initFromPrsObject ( VISU::ColoredPrs3d_i* thePrs,
                                              bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TCUTLINES>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  myRotXSpn->setValue(myPrsCopy->GetRotateX()*180./M_PI);
  myRotYSpn->setValue(myPrsCopy->GetRotateY()*180./M_PI);
  myPosSpn->setValue(myPrsCopy->GetDisplacement());
  setOrientation(myPrsCopy->GetOrientationType());

  myNbSpn->setValue( (int)myPrsCopy->GetNbLines() );
  myRotXSpn2->setValue(myPrsCopy->GetRotateX2()*180./M_PI);
  myRotYSpn2->setValue(myPrsCopy->GetRotateY2()*180./M_PI);
  myPosSpn2->setValue(myPrsCopy->GetDisplacement2());
  setOrientation2(myPrsCopy->GetOrientationType2());

  hasInit = true;
  myCutLines = VISU::TSameAsFactory<VISU::TCUTLINES>().Create(myPrsCopy, VISU::ColoredPrs3d_i::EDoNotPublish);
  myCutLines->CopyCurvesInverted(myPrsCopy->GetCurvesInverted());
  myAllCurvesInvertedCheck->setChecked( myCutLines->IsAllCurvesInverted() );
  myUseAbsoluteLengthCheck->setChecked( myCutLines->IsUseAbsoluteLength() );
  myBasePlanePos->setText( QString::number(myCutLines->GetBasePlanePosition()) );
  myCBSetDef->setChecked(myPrsCopy->IsDefault());
  DrawTable();

  // Draw Preview
  if (myPreviewCheck->isChecked()) {
    createPlanes();
  }

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

//------------------------------------------------------------------------------
int VisuGUI_CutLinesDlg::storeToPrsObject (VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );

  myPrsCopy->SetOrientation(getOrientaion(),
                            myRotXSpn->value()*M_PI/180.,
                            myRotYSpn->value()*M_PI/180.);
  myPrsCopy->SetDisplacement(myPosSpn->value());
  myPrsCopy->SetNbLines((int)myNbSpn->value());

  myPrsCopy->SetOrientation2(getOrientaion(false),
                             myRotXSpn2->value()*M_PI/180.,
                             myRotYSpn2->value()*M_PI/180.);
  myPrsCopy->SetDisplacement2(myPosSpn2->value());

  if (!myCBSetDef->isChecked())
    myPrsCopy->SetBasePlanePosition(myBasePlanePos->text().toDouble());
  else myPrsCopy->SetDefault();

  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myPosTable->model() );
  if ( aModel )
  {
    for (int i = 0; i < (int)myNbSpn->value(); ++i)
    {
      QTableWidgetItem* aItem = myPosTable->item( i, 1 );
      if (aItem == 0) break;
      if (aItem->checkState() != Qt::Checked )
      {        
        QVariant aVar = aModel->index( i, 0 ).data();
        myPrsCopy->SetLinePosition( i, aVar.toDouble() );
      }      
      else 
        myPrsCopy->SetDefaultPosition(i);
    }
  }

  myPrsCopy->SetAllCurvesInverted( myAllCurvesInvertedCheck->isChecked() );
  myPrsCopy->SetUseAbsoluteLength( myUseAbsoluteLengthCheck->isChecked() );

  VISU::TSameAsFactory<VISU::TCUTLINES>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::createPlanes()
{
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView == NULL) return;
  if (!(bool)myCutLines) return;
  if (myPreviewActor != 0) return;

  storeToPrsObject(myCutLines);
  myCutLines->GetPipeLine()->Update();
  vtkAppendPolyData* aPolyData = myCutLines->GetSpecificPL()->GetAppendPolyData();
  vtkDataSetMapper* aPlaneMapper = vtkDataSetMapper::New();
  aPlaneMapper->SetInputConnection(aPolyData->GetOutputPort());
  aPlaneMapper->ScalarVisibilityOff();
  
  myPreviewActorGlyphs = SALOME_Actor::New();
  myPreviewActorGlyphs->PickableOff();
  updateGlyphs(false);
  
  myPreviewActor = SALOME_Actor::New();
  myPreviewActor->PickableOff();
  myPreviewActor->SetMapper(aPlaneMapper);
  aPlaneMapper->Delete();
  aView->AddActor(myPreviewActor);
  aView->AddActor(myPreviewActorGlyphs);
  aView->getRenderer()->Render();
  aView->onFitAll();
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::deletePlanes()
{
  if (myPreviewActor == 0) return;
  vtkRenderer* aRend       = myPreviewActor->GetRenderer();
  vtkRenderer* aRendGlyphs = myPreviewActorGlyphs->GetRenderer();
  
  vtkRenderWindow* aWnd1 = aRend->GetRenderWindow();
  vtkRenderWindow* aWnd2 = aRendGlyphs->GetRenderWindow();
  if(aRend && aWnd1)
    myPreviewActor->RemoveFromRender(aRend);
  if(aRendGlyphs && aWnd2)
    myPreviewActorGlyphs->RemoveFromRender(aRendGlyphs);

  myPreviewActor->Delete();
  myPreviewActorGlyphs->Delete();
  myPreviewActor = 0;
  myPreviewActorGlyphs = 0;
}


//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::onPlaneSelect (int theId)
{
  QList<QAbstractButton*> aBtns = mySelPlane2->buttons();
  for (QList<QAbstractButton*>::Iterator it = aBtns.begin();
       it != aBtns.end(); it++ )    
   (*it)->setEnabled(true);
  
  QAbstractButton* aBtn = mySelPlane2->button(theId);
  aBtn->setEnabled(false);
  switch (theId) {
  case 0:
    myRotXLbl->setText( tr("LBL_ROT_X"));
    myRotYLbl->setText( tr("LBL_ROT_Y"));
    mySelPlane2->button(1)->setChecked(true);
    onCutSelect(1, false);
    break;
  case 1:
    myRotXLbl->setText( tr("LBL_ROT_Y"));
    myRotYLbl->setText( tr("LBL_ROT_Z"));
    mySelPlane2->button(2)->setChecked(true);
    onCutSelect(2, false);
    break;
  case 2:
    myRotXLbl->setText( tr("LBL_ROT_Z"));
    myRotYLbl->setText( tr("LBL_ROT_X"));
    mySelPlane2->button(0)->setChecked(true);
    onCutSelect(0, false);
  }
  setBaseDefault(false);
  DrawTable();
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::onCutSelect (int theId, bool theUpdate)
{
  switch (theId) {
  case 0:
    myRotXLbl2->setText( tr("LBL_ROT_X"));
    myRotYLbl2->setText( tr("LBL_ROT_Y"));
    break;
  case 1:
    myRotXLbl2->setText( tr("LBL_ROT_Y"));
    myRotYLbl2->setText( tr("LBL_ROT_Z"));
    break;
  case 2:
    myRotXLbl2->setText( tr("LBL_ROT_Z"));
    myRotYLbl2->setText( tr("LBL_ROT_X"));
  }
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView) {
    if (theUpdate && myPreviewCheck->isChecked() && (bool)myCutLines) {
      deletePlanes();
      createPlanes();
    }
  }
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::setBaseDefault (bool theUpdate)
{
  if (!hasInit) return;

  if (myCBSetDef->isChecked()) {
    if (!myCutLines->IsDefault()) myCutLines->SetDefault();
    myCutLines->SetDisplacement(myPosSpn->value());
    SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
    if (aView) {
      if (theUpdate && myPreviewCheck->isChecked() && (bool)myCutLines) {
        deletePlanes();
        createPlanes();
      }
    }
    double aPos = myCutLines->GetBasePlanePosition();
    myBasePlanePos->setText( QString::number(aPos) );
    myBasePlanePos->setEnabled(false);
  } else {
    myBasePlanePos->setEnabled(true);
  }
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::DrawTable()
{
  if (!hasInit) return;

  int aNbRows = myPosTable->rowCount();
  int aNbPlanes = (int)myNbSpn->value();

  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myPosTable->model() );
  if ( !aModel )
    return;

  myCutLines->SetOrientation(getOrientaion(),
                             myRotXSpn->value()*M_PI/180.,
                             myRotYSpn->value()*M_PI/180.);

  myCutLines->SetNbLines(aNbPlanes);
  myCutLines->SetDisplacement2(myPosSpn2->value());
  myCutLines->SetOrientation2(getOrientaion(false),
                              myRotXSpn2->value()*M_PI/180.,
                              myRotYSpn2->value()*M_PI/180.);

  if (aNbRows>0)
    for (int i = 0; i < aNbRows; ++i)
    {
      QTableWidgetItem* aItem = myPosTable->item( i, 1 );
      if (aItem == 0) break;
      if (aItem->checkState() != Qt::Checked )
      {
        QVariant aVar = aModel->index( i, 0 ).data();
        myCutLines->SetLinePosition(i, aVar.toDouble());
      }
    }

  //if (aNbPlanes > aNbRows)
    //  myPosTable->addRows(aNbRows, aNbPlanes - aNbRows );
  //else if (aNbPlanes < aNbRows)
  //myPosTable->setRowCount(aNbPlanes);
  if (aNbPlanes > aNbRows)
    while( aNbPlanes > myPosTable->rowCount() )
      myPosTable->insertRow( myPosTable->rowCount() );
  else if (aNbPlanes < aNbRows)
    myPosTable->setRowCount(aNbPlanes);

  QString str(tr("PLANE_NUMBER"));
  QStringList aLabels;
  int aMin = (aNbRows < aNbPlanes)? aNbRows : aNbPlanes;
  for (int i = 0; i<aMin; i++)
  {
    QModelIndex anIndex = aModel->index( i, 0 );
    aModel->setData( anIndex, QVariant( myCutLines->GetLinePosition(i) ), Qt::DisplayRole );
    //myPosTable->setText(i, 0, QString::number(myCutLines->GetLinePosition(i)));
    myPosTable->item( i, 1 )->setCheckState(myCutLines->IsDefaultPosition(i) ? Qt::Checked : Qt::Unchecked );
    aLabels.append( str.arg(i+1) );
  }

  //QHeader *vh = myPosTable->verticalHeader();
  for (int i=aMin; i<aNbPlanes; i++) {
    /*VisuGUI_NumEditItem* aEditItem = new VisuGUI_NumEditItem
      (myPosTable, QTableItem::OnTyping, QString::number(myCutLines->GetLinePosition(i)));*/
    QTableWidgetItem* aEditItem = new QTableWidgetItem();
    aEditItem->setText( QString::number(myCutLines->GetLinePosition(i)) );
    //aEditItem->setReplaceable(false);
    //aEditItem->setEnabled(!myCutLines->IsDefaultPosition(i));
    Qt::ItemFlags aFlags = aEditItem->flags();
    if ( myCutLines->IsDefaultPosition(i) )
      aFlags |= Qt::ItemIsEnabled;
    else
      aFlags &= (~Qt::ItemIsEnabled);
    
    myPosTable->setItem(i, 0, aEditItem);

    /*QCheckTableItem* aCheck = new QCheckTableItem(myPosTable, 0);
    aCheck->setChecked(myCutLines->IsDefaultPosition(i));
    aCheck->setReplaceable(false);
    myPosTable->setItem(i, 1, aCheck);*/

    QTableWidgetItem* aCheck = new QTableWidgetItem();
    aCheck->setCheckState( myCutLines->IsDefaultPosition(i) ?
                           Qt::Checked : Qt::Unchecked );
    myPosTable->setItem(i, 1, aCheck);

    //vh->setLabel( i, str.arg(i+1) );
    aLabels.append( str.arg(i+1) );
  }
  myPosTable->setVerticalHeaderLabels( aLabels );
  // Update preview
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView) {
    if (myPreviewCheck->isChecked()) {
      deletePlanes();
      createPlanes();
    }
  }
}

void VisuGUI_CutLinesDlg::updateGlyphs(bool update){
  if (myPreviewActorGlyphs == 0 ) return;
  const double *aDirLn = myCutLines->GetSpecificPL()->GetRealDirLn();
  double aSecondPnt[3];
  double aBoundCenter[3];

  vtkAppendPolyData* aPolyData = myCutLines->GetSpecificPL()->GetAppendPolyData();
  vtkDataSetMapper* aPlaneMapper = vtkDataSetMapper::New();
  aPlaneMapper->SetInputConnection(aPolyData->GetOutputPort());
  double bounds[6];
  aPlaneMapper->GetBounds(bounds);

  for(int i=0; i<3; i++) aBoundCenter[i] = (bounds[i*2] + bounds[i*2+1])/2.0;
  for(int i=0; i<3; i++){ 
    if (!myAllCurvesInvertedCheck->isChecked())
      aSecondPnt[i] = aDirLn[i];
    else
      aSecondPnt[i] = - aDirLn[i];
  }
  
  double max_bound = 0;
  max_bound < bounds[1]-bounds[0] ? max_bound = bounds[1] - bounds[0] : max_bound = max_bound;
  max_bound < bounds[3]-bounds[2] ? max_bound = bounds[3] - bounds[2] : max_bound = max_bound;
  max_bound < bounds[5]-bounds[4] ? max_bound = bounds[5] - bounds[4] : max_bound = max_bound;

  vtkPolyData* profile = vtkPolyData::New();
  vtkPoints* aPoints = vtkPoints::New();
  vtkGlyph3D* glyphs = vtkGlyph3D::New();
  vtkFloatArray *aFloatArray = vtkFloatArray::New();
  vtkGlyphSource2D* source = vtkGlyphSource2D::New();
  
  source->FilledOn();
  source->SetGlyphTypeToArrow();
  
  aPoints->InsertNextPoint(aBoundCenter);
  profile->SetPoints(aPoints);
  
  aFloatArray->SetNumberOfComponents(3);
  for(int i=0; i<3 ;i++)
    aFloatArray->InsertNextValue(aSecondPnt[i]);
  
  vtkDataSetAttributes* aDataSetAttributes;
  aDataSetAttributes = profile->GetPointData();
  aDataSetAttributes->SetVectors(aFloatArray);
  
  glyphs->SetScaleFactor(0.25*max_bound);
  glyphs->SetVectorModeToUseVector();
  glyphs->SetScaleModeToScaleByVector();
  glyphs->SetInputData(profile);
  glyphs->SetSourceConnection(source->GetOutputPort());

  vtkPolyDataMapper* aGlyphsMapper = vtkPolyDataMapper::New();
  aGlyphsMapper->ScalarVisibilityOff();
  aGlyphsMapper->SetInputConnection(glyphs->GetOutputPort());
  
  myPreviewActorGlyphs->SetMapper(aGlyphsMapper);

  profile->Delete();
  glyphs->Delete();
  aPoints->Delete();
  aGlyphsMapper->Delete();
  aFloatArray->Delete();
  source->Delete();
  aPlaneMapper->Delete();

  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
    if (update)
      vf->Repaint();
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::setDefault (int all)
{
  myPosTable->setCurrentCell(0, 0);
  myPosTable->clearSelection();

  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myPosTable->model() );
  if ( !aModel )
    return;
  
  if (all == 0) return;
  if (all == 1)
    for (int i = 0; i < (int)myNbSpn->value(); ++i)
      myPosTable->item( i, 1 )->setCheckState(Qt::Checked);

  for (int i = 0; i < (int)myNbSpn->value(); ++i)
  {
    if ( myPosTable->item( i, 1 )->checkState() == Qt::Checked )
    {
      myCutLines->SetDefaultPosition(i);
      QModelIndex anIndex = aModel->index( i, 0 );
      aModel->setData( anIndex, QVariant( myCutLines->GetLinePosition(i) ), Qt::DisplayRole );
      //myPosTable->setText( i, 0, QString::number(myCutLines->GetLinePosition(i)));
      //myPosTable->item( i, 0 )->setEnabled(false);
      myPosTable->item( i, 0 )->setFlags( myPosTable->item( i, 0 )->flags() &(~Qt::ItemIsEnabled ) );
    }
    else
      myPosTable->item( i, 0 )->setFlags( myPosTable->item( i, 0 )->flags() | Qt::ItemIsEnabled );
  }
}

//------------------------------------------------------------------------------
VISU::CutPlanes::Orientation VisuGUI_CutLinesDlg::getOrientaion (bool IsBasePlane)
{
  QButtonGroup* aBG;

  if (IsBasePlane)
    aBG = mySelPlane;
  else
    aBG = mySelPlane2;

  VISU::CutPlanes::Orientation orient;
  switch (aBG->id(aBG->checkedButton())) {
  case 0:
    orient = VISU::CutPlanes::XY;
    break;
  case 1:
    orient = VISU::CutPlanes::YZ;
    break;
  case 2:
    orient = VISU::CutPlanes::ZX;
  }
  return orient;
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::setOrientation (const VISU::CutPlanes::Orientation orient)
{
  switch (orient) {
  case VISU::CutPlanes::XY:
    mySelPlane->button(0)->setChecked(true);
    onPlaneSelect(0);
    break;
  case VISU::CutPlanes::YZ:
    mySelPlane->button(1)->setChecked(true);
    onPlaneSelect(1);
    break;
  case VISU::CutPlanes::ZX:
    mySelPlane->button(2)->setChecked(true);
    onPlaneSelect(2);
  }
}

//------------------------------------------------------------------------------
void VisuGUI_CutLinesDlg::setOrientation2 (const VISU::CutPlanes::Orientation orient)
{
  switch (orient) {
  case VISU::CutPlanes::XY:
    mySelPlane2->button(0)->setChecked(true);
    onCutSelect(0);
    break;
  case VISU::CutPlanes::YZ:
    mySelPlane2->button(1)->setChecked(true);
    onCutSelect(1);
    break;
  case VISU::CutPlanes::ZX:
    mySelPlane2->button(2)->setChecked(true);
    onCutSelect(2);
  }
}

void VisuGUI_CutLinesDlg::onValueChanged (int theRow, int theCol)
{
  /*QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myPosTable->model() );
  if ( !aModel )
    return;

  QModelIndex anIndex = aModel->index( theRow, theCol );
  
  if (theCol == 0)
  {
    bool isChanged = !anIndex.data().isNull();
    myPosTable->item(theRow, 1)->setCheckState(!isChanged ? Qt::Checked : Qt::Unchecked);
    SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
    if (aView)
    {
      if (myPreviewCheck->isChecked())
      {
        deletePlanes();
        createPlanes();
      }
    }
  }
  else if (theCol == 1)
  {
    bool isDefault = myPosTable->item(theRow, 1)->checkState() == Qt::Checked;
    if (isDefault)
    {
      SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
      if (aView)
      {
        if (myPreviewCheck->isChecked())
        {
          deletePlanes();
          createPlanes();
        }
      }
      aModel->setData( anIndex, myCutLines->GetLinePosition(theRow), Qt::DisplayRole );
      myPosTable->item( theRow, 0 )->setFlags( myPosTable->item( theRow, 0 )->flags() | Qt::ItemIsEnabled );
    }
    else
      myPosTable->item( theRow, 0 )->setFlags( myPosTable->item( theRow, 0 )->flags() &(~Qt::ItemIsEnabled ) );
  }*/

  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myPosTable->model() );
  if ( !aModel )
    return;
  
  if ( !myPosTable->item(theRow, 1) || !myPosTable->item(theRow, 0) )
    return;

  QModelIndex anIndex;

  QTableWidgetItem* anItem = myPosTable->item(theRow, theCol);
  if (!anItem )
    return;

  bool isDefault = myPosTable->item(theRow, 1)->checkState() == Qt::Checked;
  if (theCol == 0 )
  {
    anIndex = aModel->index( theRow, 0 );
    //if ( !isDefault)
      //myCutLines->SetLinePosition( theRow, anIndex.data().toDouble());
          
            
    if ( myPreviewCheck->isChecked() )
    {
      //Update Preview
      if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
      {
        //myCutLines->GetPipeLine()->Update();
        deletePlanes();
        createPlanes();
        //vf->Repaint();
      }
    }
  }
  else
  {
    anItem = myPosTable->item(theRow, 0);
    Qt::ItemFlags flags = anItem->flags();
    if ( isDefault )
    {
      myCutLines->SetDefaultPosition(theRow);
      anIndex = aModel->index( theRow, 0 );
      aModel->setData( anIndex, myCutLines->GetLinePosition(theRow), Qt::DisplayRole );

      /*if ( myPreviewCheck->isChecked() )
        {
        //not needed update presentation, because item->setFlags generates signal cellChanged() again
        }*/
      flags &= (~Qt::ItemIsEnabled );
    }
    else
      flags |=  Qt::ItemIsEnabled;
   
    anItem->setFlags( flags );
  }
}

void VisuGUI_CutLinesDlg::accept()
{
  VisuGUI_ScalarBarBaseDlg::accept();
}

void VisuGUI_CutLinesDlg::reject()
{
  VisuGUI_ScalarBarBaseDlg::reject();
}

void VisuGUI_CutLinesDlg::onPreviewCheck (bool thePreview)
{
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
    if (thePreview) {
      createPlanes();
    } else {
      deletePlanes();
      vf->Repaint();
    }
  }
}

/*! Inverting all curves in the table
 */
void VisuGUI_CutLinesDlg::onAllCurvesInvertedCheck(bool theInvert)
{
  bool anIsAllCurvesInverted = myAllCurvesInvertedCheck->isChecked();

  myCutLines->SetAllCurvesInverted( anIsAllCurvesInverted );
  myPrsCopy->SetAllCurvesInverted( anIsAllCurvesInverted );

  updateGlyphs(true);
}

void VisuGUI_CutLinesDlg::onRotation (double theValue)
{
  if (!(bool)myCutLines) return;
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView) {
    if (myPreviewCheck->isChecked()) {
      deletePlanes();
      createPlanes();
    }
  }
}

void VisuGUI_CutLinesDlg::onPlanePos (const QString& theValue)
{
  if (!(bool)myCutLines) return;
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView) {
    if (myPreviewCheck->isChecked()) {
      deletePlanes();
      createPlanes();
    }
  }
}

QString VisuGUI_CutLinesDlg::GetContextHelpFilePath()
{
  return "cut_lines_page.html";
}

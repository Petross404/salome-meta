// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_ViewExtender.cxx
//  Author : Vitaly Smetannikov
//  Module : VISU
//
#include "VisuGUI_ViewExtender.h"

#include "VisuGUI.h"
#include "VisuGUI_SegmentationMgr.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_Tools.h"
#include <VTKViewer_Actor.h>

#include <LightApp_SelectionMgr.h>
#include <SalomeApp_Application.h>

#include <VISU_PipeLine.hxx>
#include <SUIT_ViewManager.h>
#include <SUIT_ViewWindow.h>

#include <SVTK_ViewModel.h>
#include <SVTK_ViewWindow.h>



//using namespace std;

VisuGUI_ViewExtender::VisuGUI_ViewExtender(VisuGUI* theModule):
  myModule(theModule)
{
}

//****************************************************************
VisuGUI_ViewExtender::~VisuGUI_ViewExtender()
{
  QMapIterator<SVTK_ViewWindow*, VisuGUI_SegmentationMgr*> aIt(myViewMgrMap);
  while (aIt.hasNext()) {
    aIt.next();
    delete aIt.value();
  }
}

//****************************************************************
int VisuGUI_ViewExtender::createToolbar(SUIT_ViewWindow* theView)
{
  SVTK_ViewWindow* aViewWindow = dynamic_cast<SVTK_ViewWindow*>(theView);
  if (!aViewWindow) return -1;

  VisuGUI_SegmentationMgr* aMgr = getSegmentationMgr(aViewWindow);
  return aMgr->createToolbar(theView->toolMgr());
}

//****************************************************************
void VisuGUI_ViewExtender::contextMenuPopup(QMenu* theMenu)
{
//   SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myModule);
//   if (!aViewWindow) return;

//   SalomeApp_Application* anApp = myModule->getApp();
//   LightApp_SelectionMgr* aSelectionMgr = VISU::GetSelectionMgr(myModule);
//   myListIO.Clear();
//   aSelectionMgr->selectedObjects(myListIO);
//   if (myListIO.IsEmpty()) return;

//   theMenu->addSeparator();
//   theMenu->addAction(tr("VISU_SETPLANES_MNU"), this, SLOT(onSetPlanes()));
}


//****************************************************************
void VisuGUI_ViewExtender::activate(SUIT_ViewModel* theViewer)
{
  // Connect to signal on destroy ViewWindow
  SVTK_Viewer* aViewer = dynamic_cast<SVTK_Viewer*>(theViewer);
  if (aViewer) {
    if (!myViewers.contains(aViewer)) {
      SUIT_ViewManager* aMgr = aViewer->getViewManager();
      connect(aMgr, SIGNAL(deleteView(SUIT_ViewWindow*)), 
              this, SLOT(onViewDeleted(SUIT_ViewWindow*)));
      connect(aViewer, SIGNAL(actorAdded(SVTK_ViewWindow*, VTKViewer_Actor*)), 
              this, SLOT(onAddActor(SVTK_ViewWindow*, VTKViewer_Actor*)));
      myViewers.append(aViewer);
    }
  }
}

//****************************************************************
void VisuGUI_ViewExtender::deactivate(SUIT_ViewModel*)
{
  QMapIterator<SVTK_ViewWindow*, VisuGUI_SegmentationMgr*> aIt(myViewMgrMap);
  while (aIt.hasNext()) {
    aIt.next();
    aIt.value()->deactivate();
  }
}


//****************************************************************
VisuGUI_SegmentationMgr* VisuGUI_ViewExtender::getSegmentationMgr(SVTK_ViewWindow* theWindow)
{
  if (myViewMgrMap.contains(theWindow)) 
    return myViewMgrMap[theWindow];
  
  VisuGUI_SegmentationMgr* aMgr = new VisuGUI_SegmentationMgr(myModule, theWindow);
  myViewMgrMap[theWindow] = aMgr;
  return aMgr;
}

//****************************************************************
void VisuGUI_ViewExtender::onViewDeleted(SUIT_ViewWindow* theWindow)
{
  SVTK_ViewWindow* aWindow = dynamic_cast<SVTK_ViewWindow*>(theWindow);
  if (!aWindow) return;

  if (!myViewMgrMap.contains(aWindow)) return;
  delete myViewMgrMap[aWindow];
  myViewMgrMap.remove(aWindow);
}

//****************************************************************
void VisuGUI_ViewExtender::onAddActor(SVTK_ViewWindow* theWindow, VTKViewer_Actor* theActor)
{
  if (!myViewMgrMap.contains(theWindow)) return;
  myViewMgrMap[theWindow]->onAddActor(theActor);
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_EditContainerDlg.h
//  Author : VSV
//  Module : VISU
//
#ifndef VISUGUI_EDITCONTAINER_H
#define VISUGUI_EDITCONTAINER_H

#include <QDialog>
#include <QVector>

class QTreeWidget;
class QToolButton;


#include "SALOMEDSClient_SObject.hxx"

class VisuGUI;

namespace VISU {
  class Container_i;
}


struct CurveStruct {
  QString TableName;
  QString CurveName;
  QString CurveEntry;
};


class VisuGUI_EditContainerDlg: public QDialog
{
  Q_OBJECT;

 public:
  VisuGUI_EditContainerDlg (VisuGUI* theModule, bool theIsModal = true);
  ~VisuGUI_EditContainerDlg() {};

  void initFromPrsObject (VISU::Container_i* theContainer);
  void storeToPrsObject  (VISU::Container_i* theContainer);
  
 private:
  void keyPressEvent( QKeyEvent* e );

 private slots:
  void onLeftClicked();
  void onRightClicked();
  void onLeftSelected();
  void onRightSelected();
  void onHelp();

 private:
  QString getSObjectName (_PTR(SObject) theSObject);

  QTreeWidget* myStudyLst;
  QTreeWidget* myContainerLst;
  QToolButton* myLeftBtn;
  QToolButton* myRightBtn;

  VisuGUI* myVisuGUI;
};

#endif // VISUGUI_EDITCONTAINER_H

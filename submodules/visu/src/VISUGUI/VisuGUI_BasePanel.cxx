// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_BasePanel.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#include "VisuGUI_BasePanel.h"
#include "VisuGUI_Tools.h"

#include "SUIT_ResourceMgr.h"

#include <QScrollArea>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QPushButton>
//#include <QToolButton>

/*!
  \class MainFrame
  \internal
  \brief Frame inserted in viewport with redefined sizeHint method 
         in order to avoid unreasonable increasing of viewport size
*/
class VisuGUI_BasePanel::MainFrame : public QFrame
{
public:
  /*!
    \brief Constructor.
    \param theParent parent widget
  */
  MainFrame( QWidget* theParent = 0 )
  : QFrame( theParent )
  {
  }
  
  /*!
    \brief Gets frame size hint
    \return frame size hint
  */
  virtual QSize sizeHint() const
  {
    return minimumSizeHint();
  }
};

/*!
  \class VisuGUI_BasePanel
  \brief Base class for VISU interactive sub-panels.

  Set of classes is derived from this class and are intended for representing widgets 
  (GUI controls) for different operations. VisuGUI_BasePanel consists of main frame 
  inserted in scroll view and four push buttons. So all widgets of derived sub-panels 
  should be inherited from mainframe() instead of �this� pointer.
*/

/*!
  \brief Constructor creates panels look and feel
  \param theName name of the panel
  \param theParent parent widget
*/
VisuGUI_BasePanel::VisuGUI_BasePanel( const QString& theName, 
                                      QWidget* theParent,
                                      const int theBtns  )
  : QGroupBox( theName, theParent ),
    myOK( 0 ),
    myApply( 0 ),
    myClose( 0 ),
    myHelp( 0 )
{
  // Create scroll view
  myView = new QScrollArea( this );

  // Create main frame
  myMainFrame = new MainFrame( myView );
  myMainFrame->setFrameStyle( QFrame::Plain | QFrame::NoFrame );
  
  myView->setWidget( myMainFrame );
  myView->setAlignment( Qt::AlignCenter );
  myView->setWidgetResizable( true );
  myView->setMinimumWidth( myMainFrame->sizeHint().width() + 22 );
  
  // Create buttons
  QWidget* aBtnWg = new QWidget( this );
  QHBoxLayout* aBtnWgLayout = new QHBoxLayout( aBtnWg );
  //aBtnWgLayout->setSpacing( 5 );

  aBtnWgLayout->addStretch();

  if( theBtns & OKBtn )
  {
    //myOK = new QToolButton( aBtnWg );
    //myOK->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_OK" ) ) );
    myOK = new QPushButton( tr( "BUT_OK" ), aBtnWg );
    aBtnWgLayout->addWidget( myOK );
    connect( myOK, SIGNAL( clicked() ), SLOT( onOK() ) );
  }
  if( theBtns & ApplyBtn )
  {
    //myApply = new QToolButton( aBtnWg );
    //myApply->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_APPLY" ) ) );
    myApply = new QPushButton( tr( "BUT_APPLY" ), aBtnWg );
    aBtnWgLayout->addWidget( myApply );
    connect( myApply, SIGNAL( clicked() ), SLOT( onApply() ) );
  }
  if( theBtns & CloseBtn )
  {
    //myClose = new QToolButton( aBtnWg );
    //myClose->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_CLOSE" ) ) );
    myClose = new QPushButton( tr( "BUT_CLOSE" ), aBtnWg );
    aBtnWgLayout->addWidget( myClose );
    connect( myClose, SIGNAL( clicked() ), SLOT( onClose() ) );
  }
  if( theBtns & HelpBtn )
  {
    //myHelp = new QToolButton( aBtnWg );
    //myHelp->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr( "ICON_HELP" ) ) );
    myHelp = new QPushButton( tr( "BUT_HELP" ), aBtnWg );
    aBtnWgLayout->addWidget( myHelp );
    connect( myHelp, SIGNAL( clicked() ), SLOT( onHelp() ) );
  }

  aBtnWgLayout->addStretch();

  // fill layout
  QVBoxLayout* aLay = new QVBoxLayout( this );
  aLay->setContentsMargins( 0, 0, 0, 0 );
  //aLay->setSpacing( 5 );
  aLay->addWidget( myView, 1 );
  aLay->addWidget( aBtnWg );
}

/*!
  \brief Destructor
*/
VisuGUI_BasePanel::~VisuGUI_BasePanel()
{
}

/*!
  \brief Verifies validity of input data

  This virtual method should be redefined in derived classes. Usually operator 
  corresponding to the sub-panel calls this method to check validity of input 
  data when Apply/OK button is pressed.

  \param theErrMsg Error message. 
  
        If data is invalid when panel can return message using this parameter given 
        clear explanation what is wrong

  \return TRUE if data is valid, FALSE otherwise 
*/
bool VisuGUI_BasePanel::isValid( QString& /*theErrMsg*/ )
{
  return true;
}
/*!
  \brief Virtual methods should be redefined in derived classes and 
         clears all GUI controls
*/
void VisuGUI_BasePanel::clear()
{
}

/*!
  \brief Virtual slot called when �OK� button pressed emits corresponding signal.

  This slot moves focus in OK button before emitting signal. Mainly it provides 
  application with correct moving data from currently edited controls to internal 
  structure. For example QTable moves data from cell editor to table item when 
  focus is out.

*/
void VisuGUI_BasePanel::onOK()
{
  if ( myOK )
  {
    myOK->setFocus();
    qApp->processEvents();
  }
  emit bpOk();
}

/*!
  \brief Virtual slot called when �Apply� button pressed emits corresponding signal.
  \sa onOK
*/
void VisuGUI_BasePanel::onApply()
{
  if ( myApply )
  {
    myApply->setFocus();
    qApp->processEvents();
  }
  emit bpApply();
}

/*!
  \brief Virtual slot called when �Close� button pressed emits corresponding signal.
  \sa onOK
*/
void VisuGUI_BasePanel::onClose()
{
  if ( myClose )
    myClose->setFocus();
  emit bpClose();
}

/*!
  \brief Virtual slot called when �Help� button pressed emits corresponding signal.
  \sa onOK
*/
void VisuGUI_BasePanel::onHelp()
{
  if ( myHelp )
    myHelp->setFocus();
  emit bpHelp();
}

/*!
  \brief Gets frame inserted in scroll view. All controls of derived 
         panels should use it as parent
  \return QFrame* object 
*/
QFrame* VisuGUI_BasePanel::mainFrame()
{
  return myMainFrame;
}

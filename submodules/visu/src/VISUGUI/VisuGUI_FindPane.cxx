// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_FindPane.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#include "VisuGUI_FindPane.h"
#include "VisuGUI_Tools.h"

#include "VISU_Actor.h"
#include "VISU_Event.h"
#include "VISU_GaussPtsAct.h"

#include "VISU_IDMapper.hxx"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include <QComboBox>
#include <QLabel>
#include <QLayout>
#include <QListWidget>
#include <QLineEdit>
#include <QToolButton>

#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDataSet.h>
#include <vtkMapper.h>
#include <vtkPointData.h>

#define PAGE_SIZE 10

VisuGUI_FindPane::VisuGUI_FindPane( QWidget* theParent ) :
  QGroupBox( theParent ),
  myCurrentPage( 0 ),
  mySelectionMode( -1 ),
  myActor( 0 )
{
  setTitle( tr( "FIND_TITLE" ) );

  QGridLayout* aTopLayout = new QGridLayout( this );

  QLabel* aConditionLabel = new QLabel( tr( "CONDITION" ), this );

  myConditionBox = new QComboBox( this );
  myConditionBox->addItems( QStringList()
                            << tr( "MINIMUM" )
                            << tr( "MAXIMUM" )
                            << "="
                            << "<="
                            << ">="
                            << tr( "BETWEEN" ) );

  connect( myConditionBox, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onConditionChanged( int ) ) );

  QDoubleValidator* aDoubleValidator = new QDoubleValidator( this );

  myLeftValue = new QLineEdit( this );
  myLeftValue->setValidator( aDoubleValidator );

  myDashLabel = new QLabel( "-", this );

  myRightValue = new QLineEdit( this );
  myRightValue->setValidator( aDoubleValidator );

  QToolButton* anApplyBtn = new QToolButton( this );
  anApplyBtn->setIcon( VISU::GetResourceMgr()->loadPixmap( "VISU", tr( "ICON_APPLY" ) ) );
  connect( anApplyBtn, SIGNAL( clicked() ), this, SLOT( onApply() ) );


  QWidget* anIdsWidget = new QWidget( this );
  QGridLayout* anIdsLayout = new QGridLayout( anIdsWidget );
  anIdsLayout->setMargin( 0 );

  myIdsListWidget = new QListWidget( anIdsWidget );
  myIdsListWidget->setFlow( QListView::LeftToRight );
  myIdsListWidget->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
  myIdsListWidget->setFixedHeight( 45 );

  connect( myIdsListWidget, SIGNAL( itemSelectionChanged() ), this, SLOT( onIdChanged() ) );

  myPageLabel = new QLabel( anIdsWidget );
  myPageLabel->setAlignment( Qt::AlignHCenter );

  myPrevBtn = new QToolButton( anIdsWidget );
  myPrevBtn->setIcon( VISU::GetResourceMgr()->loadPixmap( "VISU", tr( "ICON_SLIDER_PREVIOUS" ) ) );
  connect( myPrevBtn, SIGNAL( clicked() ), this, SLOT( onPrevPage() ) );

  myNextBtn = new QToolButton( anIdsWidget );
  myNextBtn->setIcon( VISU::GetResourceMgr()->loadPixmap( "VISU", tr( "ICON_SLIDER_NEXT" ) ) );
  connect( myNextBtn, SIGNAL( clicked() ), this, SLOT( onNextPage() ) );

  anIdsLayout->addWidget( myIdsListWidget, 0, 0, 2, 1 );
  anIdsLayout->addWidget( myPageLabel,     0, 1, 1, 2 );
  anIdsLayout->addWidget( myPrevBtn,       1, 1 );
  anIdsLayout->addWidget( myNextBtn,       1, 2 );


  aTopLayout->addWidget( aConditionLabel, 0, 0, 1, 5 );
  aTopLayout->addWidget( myConditionBox,  1, 0 );
  aTopLayout->addWidget( myLeftValue,     1, 1 );
  aTopLayout->addWidget( myDashLabel,     1, 2 );
  aTopLayout->addWidget( myRightValue,    1, 3 );
  aTopLayout->addWidget( anApplyBtn,      1, 4 );
  aTopLayout->addWidget( anIdsWidget,     2, 0, 1, 5 );

  onConditionChanged( 0 );

  setSelectionMode( ActorSelection );
}

VisuGUI_FindPane::~VisuGUI_FindPane()
{
}

void VisuGUI_FindPane::setSelectionMode( const Selection_Mode theSelectionMode )
{
  if( mySelectionMode != theSelectionMode )
    clearIds();

  mySelectionMode = theSelectionMode;
  setEnabled( mySelectionMode != ActorSelection );
}


void VisuGUI_FindPane::setActor( VISU_Actor* theActor )
{
  if( myActor != theActor )
    clearIds();

  myActor = theActor;
}

void VisuGUI_FindPane::onConditionChanged( int theId )
{
  myLeftValue->setEnabled( theId >= 2 );
  myDashLabel->setEnabled( theId == 5 );
  myRightValue->setEnabled( theId == 5 );
}

void VisuGUI_FindPane::onApply()
{
  if( !isValid() )
  {
    SUIT_MessageBox::warning( this, tr( "WRN_VISU" ), tr( "INCORRECT_VALUES" ) );
    return;
  }

  myIdsListWidget->clear();

  if( !myActor )
    return;

  vtkDataSet* aDataSet = myActor->GetInput();

  vtkDataArray* aScalars = 0;
  if( mySelectionMode == NodeSelection )
    aScalars = aDataSet->GetPointData()->GetScalars();
  else if( mySelectionMode == CellSelection )
    aScalars = aDataSet->GetCellData()->GetScalars();
  else if( mySelectionMode == GaussPointSelection )
  {
    if( VISU_GaussPtsAct* aGaussPtsAct = dynamic_cast<VISU_GaussPtsAct*>( myActor ) )
      aScalars = aGaussPtsAct->GetInput()->GetPointData()->GetScalars();
  }


  if( !aScalars )
    return;

  int aCondition = myConditionBox->currentIndex();
  double aLeftValue = myLeftValue->text().toDouble();
  double aRightValue = myRightValue->text().toDouble();

  myIdsList.clear();

  double eps = 1.0 / VTK_LARGE_FLOAT;

  double anExtremum = 0;
  if( aCondition == 0 )
    anExtremum = VTK_LARGE_FLOAT;
  else if( aCondition == 1 )
    anExtremum = -VTK_LARGE_FLOAT;

  for( int aVTKId = 0, aNbVal = aScalars->GetNumberOfTuples(); aVTKId < aNbVal; aVTKId++ )
  {
    double aValue = *aScalars->GetTuple( aVTKId );

    TFindId anId( -1, -1 );
    if( mySelectionMode == NodeSelection )
      anId.first = VISU::GetNodeObjID( aDataSet, aVTKId );
    else if( mySelectionMode == CellSelection )
      anId.first = VISU::GetElemObjID( aDataSet, aVTKId );
    else if( mySelectionMode == GaussPointSelection )
    {
      if( VISU_GaussPtsAct* aGaussPtsAct = dynamic_cast<VISU_GaussPtsAct*>( myActor ) )
      {
        VISU::TGaussPointID aGaussPointID = aGaussPtsAct->GetGaussPointsPL()->GetObjID( aVTKId );
        anId.first = aGaussPointID.first;
        anId.second = aGaussPointID.second;
      }
    }
    //printf( "(%d) <%d - %d> %f\n", aVTKId, anId.first, anId.second, aValue );

    if( anId.first < 0 ||
        anId.second < 0 && mySelectionMode == GaussPointSelection )
      continue;

    bool ok = false;
    switch( aCondition )
    {
    case 0: // Minimum
      ok = ( aValue - anExtremum ) < eps;
      break;
    case 1: // Maximum
      ok = ( aValue - anExtremum ) > -eps;
      break;
    case 2: // =
      ok = fabs( aValue - aLeftValue ) < eps;
      break;
    case 3: // <=
      ok = ( aValue - aLeftValue ) < eps;
      break;
    case 4: // >=
      ok = ( aValue - aLeftValue ) > -eps;
      break;
    case 5: // Between
      ok = ( aValue - aLeftValue ) > -eps && ( aValue - aRightValue ) < eps;
      break;
    default:
      ok = true;
      break;
    }

    if( ok )
    {
      if( aCondition <= 1 && fabs( aValue - anExtremum ) > eps )
      {
        anExtremum = aValue;
        myIdsList.clear();
      }
      if( !myIdsList.contains( anId ) )
        myIdsList.append( anId );
    }
  }

  qSort( myIdsList );

  myCurrentPage = 0;
  displayIds();
}

void VisuGUI_FindPane::onIdChanged()
{
  int aFirstId = -1, aSecondId = -1;

  QList<QListWidgetItem*> aSelectedItems = myIdsListWidget->selectedItems();
  if( aSelectedItems.size() == 1 )
  {
    QString aStr = aSelectedItems.first()->text();

    bool ok = false;
    aFirstId = aStr.toInt( &ok );
    if( !ok ) // try to parse the string as a pair of ids - "[aFirstId aSecondId]"
    {
      aStr.remove( '[' );
      aStr.remove( ']' );
      aFirstId = aStr.section( ' ', 0, 0 ).toInt( &ok );
      if( !ok )
        aFirstId = -1;
      else
      {
        ok = false;
        aSecondId = aStr.section( ' ', 1, 1 ).toInt( &ok );
        if( !ok )
          aSecondId = -1;
      }
    }
  }

  emit idChanged( aFirstId, aSecondId );
}

void VisuGUI_FindPane::onPrevPage()
{
  myCurrentPage--;
  displayIds();
}

void VisuGUI_FindPane::onNextPage()
{
  myCurrentPage++;
  displayIds();
}

bool VisuGUI_FindPane::isValid() const
{
  bool ok = false;;
  double aLeftValue = myLeftValue->text().toDouble( &ok );
  if( myLeftValue->isEnabled() && !ok )
    return false;

  ok = false;
  double aRightValue = myRightValue->text().toDouble( &ok );
  if( myRightValue->isEnabled() && ( !ok || aRightValue < aLeftValue ) )
    return false;

  return true;
}

void VisuGUI_FindPane::clearIds()
{
  myIdsList.clear();
  myCurrentPage = 0;

  displayIds();
}

void VisuGUI_FindPane::displayIds()
{
  myIdsListWidget->clear();

  int aSize = myIdsList.size();

  myPrevBtn->setEnabled( myCurrentPage != 0 );
  myNextBtn->setEnabled( ( myCurrentPage + 1 ) * PAGE_SIZE < aSize );
  myPageLabel->setText( QString( "Page %1/%2" )
                        .arg( aSize > 0 ? myCurrentPage + 1 : 0 )
                        .arg( aSize > 0 ? ( aSize - 1 ) / PAGE_SIZE + 1 : 0 ) );

  int aFirstIndex = myCurrentPage * PAGE_SIZE;
  int aLastIndex = aFirstIndex + PAGE_SIZE - 1;
  if( aLastIndex >= aSize )
    aLastIndex = aSize - 1;

  for( int anIndex = aFirstIndex; anIndex <= aLastIndex; anIndex++ )
  {
    TFindId anId = myIdsList[ anIndex ];
    int aFirstId = anId.first, aSecondId = anId.second;
    QString aStr = aSecondId < 0 ?
      QString( "%1" ).arg( aFirstId ) :
      QString( "[%1 %2]" ).arg( aFirstId ).arg( aSecondId );
    myIdsListWidget->addItem( aStr );
  }
}

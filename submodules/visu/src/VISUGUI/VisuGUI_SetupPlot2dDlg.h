// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_SetupPlot2dDlg.h
//  Author : Vadim SANDLER
//  Module : SALOME
//
#ifndef VISUGUI_SetupPlot2dDlg_H
#define VISUGUI_SetupPlot2dDlg_H

#include "Plot2d_Curve.h"

#include <SALOMEDSClient_SObject.hxx>

#include <VISU_Table_i.hh>

#include <QDialog>
#include <QList>

//=================================================================================
// class    : VisuGUI_SetupPlot2dDlg
// purpose  : Dialog box for setup Plot2d view
//=================================================================================

class QScrollArea;
class QPushButton;
class QLabel;
class QCheckBox;
class QComboBox;
class SalomeApp_IntSpinBox;
class QToolButton;
class VisuGUI_ItemContainer;
class QtxColorButton;

class VisuGUI_SetupPlot2dDlg : public QDialog
{ 
  Q_OBJECT

public:
  VisuGUI_SetupPlot2dDlg( _PTR(SObject) object, VISU::Table_i* table ,QWidget* parent = 0 );
  ~VisuGUI_SetupPlot2dDlg();

  void getCurves( QList<Plot2d_Curve*>& container );
  void getCurvesSource( int& horIndex, QList<int>& verIndexes, QList<int>& ver2Indexes, QList<int>& zIndexes );
  bool getCurveAttributes( const int vIndex, bool& isAuto, int& marker, int& line, int& lineWidth, QColor& color);
  int idx(const int i) const {return myIdxMap[i];}

private:
  void keyPressEvent( QKeyEvent* e );

private slots:
  void onHBtnToggled( bool );
  void onVBtnToggled( bool );
  void onV2BtnToggled( bool );
  void onHelp();
  void enableControls();

private:
  QScrollArea*                myView;
  QPushButton*                myOkBtn;
  QPushButton*                myCancelBtn;
  QPushButton*                myHelpBtn;
  QList<VisuGUI_ItemContainer*> myItems;
  QMap<int,int>                 myIdxMap;

  _PTR(SObject)               myObject;
  VISU::Table_i*              myTable;
};

class VisuGUI_ItemContainer : public QObject
{
  Q_OBJECT

public:
  VisuGUI_ItemContainer( QObject* parent = 0 );
  
  void   createWidgets( QWidget* parentWidget, const QStringList& );
  void   enableWidgets( bool enable );

  void   setHorizontalOn( bool on );
  bool   isHorizontalOn() const;
  void   setVerticalOn( bool on );
  bool   isVerticalOn() const;
  void   setVertical2On( bool on );
  bool   isVertical2On() const;
  bool   isAutoAssign() const;
  void   setAutoAssign( bool on );
  void   setLine( const int line, const int width );
  int    getLine() const;
  int    getLineWidth() const;
  void   setMarker( const int marker );
  int    getMarker() const;
  void   setColor( const QColor& color );
  QColor getColor() const;
  int    assigned() const;

protected:
  void   updateState();

signals:
  void   autoClicked();
  void   horToggled( bool );
  void   verToggled( bool );
  void   ver2Toggled( bool );

public slots:
  void   onAutoChanged();
//void   onColorChanged();
  void   onHVToggled( bool );

public:
  bool                  myEnabled;
  QToolButton*          myHBtn;
  QToolButton*          myVBtn;
  QToolButton*          myV2Btn;
  QLabel*               myTitleLab;
  QLabel*               myUnitLab;
  QLabel*               myDvtnLab;
  QCheckBox*            myAutoCheck;
  QComboBox*            myLineCombo;
  SalomeApp_IntSpinBox* myLineSpin;
  QComboBox*            myMarkerCombo, *myAssigned;
  QtxColorButton*       myColorBtn;
};

#endif // VISUGUI_SetupPlot2dDlg_H


// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_MagnitudeDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#include "VisuGUI_DeformedShapeDlg.h"

#include "VisuGUI_Tools.h"
#include "VisuGUI_InputPane.h"
#include "VisuGUI.h"
#include "VisuGUI_Prs3dTools.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_DeformedShape_i.hh"
#include "VisuGUI_ViewTools.h"
#include "VISU_Gen_i.hh"
#include "VISU_Result_i.hh"

#include "SVTK_ViewWindow.h"
#include "SalomeApp_Module.h"
#include <SalomeApp_DoubleSpinBox.h>
#include "LightApp_Application.h"
#include "SUIT_Desktop.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include <QLayout>
#include <QDialog>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QTabWidget>
#include <QKeyEvent>

#include <math.h>
#include <limits>

using namespace std;

/*!
 * Constructor
 */
VisuGUI_DeformedShapeDlg::VisuGUI_DeformedShapeDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule)
{
  setWindowTitle(tr("DLG_TITLE"));
  setSizeGripEnabled(TRUE);
  myModule = theModule;
  isApplyed = false;

  QVBoxLayout* TopLayout = new QVBoxLayout (this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);

  myTabBox = new QTabWidget (this);

  // Deformed shape pane
  QWidget* aBox = new QWidget (this);
  QVBoxLayout* aVBLay = new QVBoxLayout( aBox );
  aVBLay->setMargin(11);
  QFrame* TopGroup = new QFrame ( aBox );
  aVBLay->addWidget( TopGroup );
  TopGroup->setFrameStyle(QFrame::Box | QFrame::Sunken);
  TopGroup->setLineWidth(1);
  QGridLayout* TopGroupLayout = new QGridLayout (TopGroup);
  TopGroupLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
  TopGroupLayout->setSpacing(6);
  TopGroupLayout->setMargin(11);

  //   Scale factor
  QLabel* ScaleLabel = new QLabel (tr("SCALE_FACTOR"), TopGroup );
  TopGroupLayout->addWidget(ScaleLabel, 0, 0);

  ScalFact = new SalomeApp_DoubleSpinBox (TopGroup);
  VISU::initSpinBox( ScalFact, 0., 1.0E+38, .1, "visual_data_precision" );  
  ScalFact->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  ScalFact->setValue(0.1);
  TopGroupLayout->addWidget(ScalFact, 0, 1);

  //   Magnitude coloring
  UseMagn = new QCheckBox (tr("MAGNITUDE_COLORING"), TopGroup);
  UseMagn->setChecked(true);
  TopGroupLayout->addWidget(UseMagn, 1, 0, 1, 2);

  myTabBox->addTab(aBox, tr("DEFORMED_SHAPE_TAB"));

  // Scalar bar pane
  myInputPane = new VisuGUI_InputPane(VISU::TDEFORMEDSHAPE, theModule, this);

  myTabBox->addTab(GetScalarPane(), tr("SCALAR_BAR_TAB"));
  myTabBox->addTab(myInputPane, tr("INPUT_TAB"));

  // Buttons
  QGroupBox* GroupButtons = new QGroupBox (this);
  GroupButtons->setGeometry(QRect(10, 10, 281, 48));
  //oupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout (GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  QPushButton* buttonOk = new QPushButton (tr("A&pply and Close"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);

  QPushButton* buttonApply = new QPushButton (tr("&Apply"), GroupButtons);
  buttonApply->setAutoDefault(TRUE);
  buttonApply->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonApply, 0, 1);

  QPushButton* buttonCancel = new QPushButton (tr("BUT_CANCEL") , GroupButtons);
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 2);

  QPushButton* buttonHelp = new QPushButton (tr("BUT_HELP") , GroupButtons);
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 3);

  // Add Tab box and Buttons to the top layout
  TopLayout->addWidget(myTabBox);
  TopLayout->addWidget(GroupButtons);

  // signals and slots connections
  connect(buttonOk,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(buttonApply,  SIGNAL(clicked()), this, SLOT(onApply()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));
}

VisuGUI_DeformedShapeDlg::~VisuGUI_DeformedShapeDlg()
{}

void VisuGUI_DeformedShapeDlg::initFromPrsObject (VISU::ColoredPrs3d_i* thePrs,
                                                  bool theInit)
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TDEFORMEDSHAPE>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);
  setFactor(myPrsCopy->GetScale());
  UseMagn->setChecked(myPrsCopy->IsColored());

  if (!theInit)
    return;

  myInputPane->initFromPrsObject(myPrsCopy);
  myTabBox->setCurrentIndex(0);
}

void VisuGUI_DeformedShapeDlg::setFactor(double theFactor)
{
  double step = 0.1;
  if (fabs(theFactor) > numeric_limits<double>::epsilon()) {
    int degree = int(log10(fabs(theFactor))) - 1;
    if (fabs(theFactor) < 1) {
      // as logarithm value is negative in this case
      // and it is truncated to the bigger integer
      degree -= 1;
    }
    step = pow(10., double(degree));
  }

  ScalFact->setSingleStep(step);
  ScalFact->setValue(theFactor);
}

int VisuGUI_DeformedShapeDlg::storeToPrsObject(VISU::ColoredPrs3d_i* thePrs) 
{
  if( !isValid() )
    return 0;

  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );

  myPrsCopy->SetScale(getFactor());
  myPrsCopy->ShowColored(isColored());

  VISU::TSameAsFactory<VISU::TDEFORMEDSHAPE>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

void VisuGUI_DeformedShapeDlg::updatePrsCopy( VISU::Prs3d_i* thePrs )
{
  SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  VisuGUI* aVisuGUI = dynamic_cast <VisuGUI*>(myModule);
  if ( myPrsCopy->GetNumberOfActors() == 0 ) {
    PublishInView(aVisuGUI, myPrsCopy, aViewWindow);

    // Bug 0020821
    if( thePrs ) {
      if( SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>( myModule ) ) {
        VISU_Actor* anActorSource = VISU::FindActor( aViewWindow, thePrs );
        VISU_Actor* anActorCopy = VISU::FindActor( aViewWindow, myPrsCopy );
        if( anActorSource && anActorCopy )
          anActorCopy->DeepCopy( anActorSource );
      }
    }

    if(VISU::GetResourceMgr()->booleanValue("VISU","automatic_fit_all",false))
      aViewWindow->onFitAll();
    int aPos = VISU::GetFreePositionOfDefaultScalarBar(aVisuGUI, aViewWindow);
    VISU::AddScalarBarPosition(aVisuGUI, aViewWindow, myPrsCopy, aPos);
    } else {
    try {
      myPrsCopy->UpdateActors();
    } catch (std::runtime_error& exc) {
      myPrsCopy->RemoveActors();
      
      INFOS(exc.what());
      SUIT_MessageBox::warning(VISU::GetDesktop(myModule),
                               tr("WRN_VISU"),
                               tr("ERR_CANT_BUILD_PRESENTATION") + ": " + exc.what());
    }
    aViewWindow->Repaint();
  }
}

void VisuGUI_DeformedShapeDlg::accept()
{
  VisuGUI_ScalarBarBaseDlg::done( isValid() );
}

void VisuGUI_DeformedShapeDlg::reject()
{
  if ( isApplyed )
    VisuGUI_ScalarBarBaseDlg::done( 1 );
  else
    VisuGUI_ScalarBarBaseDlg::reject();
}

QString VisuGUI_DeformedShapeDlg::GetContextHelpFilePath()
{
  return "deformed_shape_page.html";
}

bool VisuGUI_DeformedShapeDlg::onApply()
{
  if ( storeToPrsObject( myPrsCopy ) ) 
  {
    updatePrsCopy( myPrsCopy );
    isApplyed = true;
    return true;
  }
  return false;
}

////////////////////////////////////////////////////////////////////////////////
//  Create and Edit
////////////////////////////////////////////////////////////////////////////////

VISU::Prs3d_i* VisuGUI_DeformedShapeDlg::CreatePrs3d(VisuGUI* theModule)
{
  VISU::DeformedShape_i* aPrs3d = NULL;
  _PTR(SObject) aTimeStamp;
  Handle(SALOME_InteractiveObject) anIO;
  VISU::ColoredPrs3d_i::EPublishInStudyMode aPublishInStudyMode;

  if (VISU::CheckTimeStamp(theModule, aTimeStamp, anIO, aPublishInStudyMode))
    {
      VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap(aTimeStamp);
      VISU::VISUType aType = VISU::Storable::RestoringMap2Type(aRestoringMap);
      if ( aType == VISU::TTIMESTAMP )
        {
  
          QString aMeshName = aRestoringMap["myMeshName"];
          QString anEntity = aRestoringMap["myEntityId"];
          QString aFieldName = aRestoringMap["myFieldName"];
          QString aTimeStampId = aRestoringMap["myTimeStampId"];
          
          //typedef VISU::DeformedShape_i TPrs3d_i; 
          
          aPrs3d =
            VISU::CreatePrs3dFromFactory<VISU::DeformedShape_i>(theModule,
                                                   aTimeStamp,
                                                   aMeshName.toLatin1().data(),
                                                   (VISU::Entity)anEntity.toInt(),
                                                   aFieldName.toLatin1().data(),
                                                   aTimeStampId.toInt(),
                                                   aPublishInStudyMode);

          if (aPrs3d) {
            VisuGUI_DeformedShapeDlg* aDlg = new VisuGUI_DeformedShapeDlg (theModule);
            aDlg->initFromPrsObject(aPrs3d, true);
            aDlg->UseMagn->setChecked(true);
            VisuGUI_DialogRunner r(aDlg);
            int dlgResult = r.run( false );
            
            if ( dlgResult == 0 )
              DeletePrs3d(theModule,aPrs3d);
            else {
              aDlg->storeToPrsObject( aPrs3d );
              SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
              PublishInView(theModule, aPrs3d, aViewWindow);
              if(VISU::GetResourceMgr()->booleanValue("VISU","automatic_fit_all",false))
                aViewWindow->onFitAll();
              int aPos = VISU::GetFreePositionOfDefaultScalarBar(theModule, aViewWindow);
              VISU::AddScalarBarPosition(theModule, aViewWindow, aPrs3d, aPos);
            }
            
            VISU::UpdateObjBrowser(theModule, true, aTimeStamp);
            delete aDlg;
          }
        }
    }
  return aPrs3d;
}

////////////////////////////////////////////////////////////////////////////////

void VisuGUI_DeformedShapeDlg::EditPrs3d(VisuGUI* theModule, VISU::Prs3d_i* thePrs3d, Handle(SALOME_InteractiveObject)& theIO)
{
  typedef VISU::DeformedShape_i TPrs3d_i;

  if (TPrs3d_i* aPrs3d = dynamic_cast<TPrs3d_i*>(thePrs3d)) {
    VisuGUI_DeformedShapeDlg* aDlg = new VisuGUI_DeformedShapeDlg (theModule);
    aDlg->initFromPrsObject(aPrs3d, true);
    aDlg->updatePrsCopy(thePrs3d);

    //Hide thePrs3d from Viewer
    SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(theModule);
    if (aViewWindow)
      if (VISU_Actor* anActor = FindActor(aViewWindow, thePrs3d))
        if (anActor->GetVisibility())
          anActor->VisibilityOff();

    VisuGUI_DialogRunner r(aDlg);
    int dlgResult = r.run( true );
    
    if ( dlgResult != 0 )
      aDlg->storeToPrsObject( aPrs3d );
    
    try {
      thePrs3d->UpdateActors();
    }
    catch (std::runtime_error& exc) {
      thePrs3d->RemoveActors();
      
      INFOS(exc.what());
      SUIT_MessageBox::warning
        (VISU::GetDesktop(theModule), QObject::tr("WRN_VISU"),
         QObject::tr("ERR_CANT_BUILD_PRESENTATION") + ": " + QObject::tr(exc.what()));
    }

    if (aViewWindow)
      if (VISU_Actor* anActor = FindActor(aViewWindow, thePrs3d))
        if (!anActor->GetVisibility())
          anActor->VisibilityOn();
    
    delete aDlg;    
  }
}

///////////////////////////////////////////////////////////////////////////////////
int VisuGUI_DeformedShapeDlg::isValid()
{
  if(myInputPane->check() && GetScalarPane()->check())
    return 1; 
  else
    return 0;
}

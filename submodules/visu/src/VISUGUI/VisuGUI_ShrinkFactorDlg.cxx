// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_ShrinkFactorDlg.cxx
//  Author : Vadim SANDLER, Open CASCADE S.A.S. (vadim.sandler@opencascade.com)
//
#include "VisuGUI_ShrinkFactorDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_Tools.h"
#include "VISU_Actor.h"

#include "SUIT_Desktop.h"
#include "SUIT_OverrideCursor.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"
#include "SALOME_InteractiveObject.hxx"

#include <SalomeApp_IntSpinBox.h>

#include "LightApp_Study.h"
#include "LightApp_SelectionMgr.h"
#include "LightApp_Application.h"

#include "SVTK_ViewWindow.h"

// QT Includes
#include <QLabel>
#include <QPushButton>
#include <QLayout>
#include <QGroupBox>
#include <QKeyEvent>

const int DEFAULT_SHRINK = 80;
const int UNDEF_SHRINK   = -1;

//=================================================================================
// class    : VisuGUI_ShrinkFactorDlg()
// purpose  :
//
//=================================================================================
VisuGUI_ShrinkFactorDlg::VisuGUI_ShrinkFactorDlg( VisuGUI* theModule, bool modal )
  : QDialog( VISU::GetDesktop( theModule ), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
    myModule(theModule),
    mySelectionMgr( VISU::GetSelectionMgr( theModule ) ),
    myViewWindow( VISU::GetActiveViewWindow<SVTK_ViewWindow>(theModule) )
{
  setModal( modal );
  setAttribute( Qt::WA_DeleteOnClose );
    
  setWindowTitle( tr( "SHRINKFACTOR_TITLE" ) );
  setSizeGripEnabled( true );
  QVBoxLayout* topLayout = new QVBoxLayout( this );
  topLayout->setSpacing( 6 );
  topLayout->setMargin( 11 );

  /*************************************************************************/
  GroupC1 = new QGroupBox( this );
  QHBoxLayout* GroupC1Layout = new QHBoxLayout( GroupC1 );
  GroupC1Layout->setSpacing( 6 );
  GroupC1Layout->setMargin( 11 );

  ValueLab = new QLabel( tr( "SHRINKFACTOR_VALUE" ), GroupC1 );
  ValueSpin = new SalomeApp_IntSpinBox( GroupC1 );
  ValueSpin->setAcceptNames( false );
  ValueSpin->setRange( 20, 100 ); 
  ValueSpin->setSingleStep( 10 );
  ValueSpin->setMinimumWidth( 70 );

  GroupC1Layout->addWidget( ValueLab );
  GroupC1Layout->addWidget( ValueSpin );

  /*************************************************************************/
  QGroupBox* GroupButtons = new QGroupBox( this );
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout( GroupButtons );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  buttonOk = new QPushButton( tr( "BUT_CLOSE" ), GroupButtons );
  buttonOk->setAutoDefault( true );
  buttonOk->setDefault( true );

  buttonHelp = new QPushButton( tr( "BUT_HELP" ), GroupButtons);
  buttonHelp->setAutoDefault( true );

  GroupButtonsLayout->addWidget( buttonOk );
  GroupButtonsLayout->addSpacing( 10 );
  GroupButtonsLayout->addStretch();
  GroupButtonsLayout->addWidget( buttonHelp );

  topLayout->addWidget( GroupC1 );
  topLayout->addWidget( GroupButtons );

  // Initial state
  onSelectionChanged();

  // signals and slots connections : after ValueHasChanged()
  connect(buttonOk,       SIGNAL( clicked() ),                 this, SLOT( close() ) );
  connect(buttonHelp,     SIGNAL( clicked() ),                 this, SLOT( help() ) );
  connect(ValueSpin,      SIGNAL( valueChanged( int ) ),       this, SLOT( setShrinkFactor() ) );
  connect(mySelectionMgr, SIGNAL( currentSelectionChanged() ), this, SLOT( onSelectionChanged() ) );
}

//=================================================================================
// function : ~VisuGUI_ShrinkFactorDlg()
// purpose  :
//=================================================================================
VisuGUI_ShrinkFactorDlg::~VisuGUI_ShrinkFactorDlg()
{
}

//=======================================================================
// function : help()
// purpose  :
//=======================================================================
void VisuGUI_ShrinkFactorDlg::help()
{
  QString aHelpFileName = "viewing_3d_presentations_page.html#width_shrink_factor_anchor";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule( aVisuGUI ? app->moduleName( aVisuGUI->moduleName() ) : QString(""), aHelpFileName );
  }
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning( this,
                              tr( "WRN_WARNING" ),
                              tr( "EXTERNAL_BROWSER_CANNOT_SHOW_PAGE" ).
                              arg( app->resourceMgr()->stringValue( "ExternalBrowser", platform ) ).arg( aHelpFileName ),
                              tr( "BUT_OK" ) );
  }
}

//=================================================================================
// function : setShrinkFactor()
// purpose  : Called when value of spin box is changed
//          : or the first time as initilisation
//=================================================================================
void VisuGUI_ShrinkFactorDlg::setShrinkFactor()
{
  if ( myViewWindow ) {
    SUIT_OverrideCursor wc;
    float shrink = ValueSpin->value() / 100.;

    SALOME_ListIO aList;
    mySelectionMgr->selectedObjects( aList );

    SALOME_ListIteratorOfListIO anIter( aList );
    for ( ; anIter.More(); anIter.Next() ) {
      Handle(SALOME_InteractiveObject) anIO = anIter.Value();
      VISU_Actor* anActor = VISU::FindActor( VISU::GetAppStudy( myModule ), myViewWindow, anIO->getEntry() );
      if ( anActor ) {
	if ( anActor->IsShrunkable() )
	  anActor->SetShrinkFactor( shrink );
      }
      else {
	VISU_ActorBase* anActor = VISU::FindActorBase(VISU::GetAppStudy(myModule), myViewWindow, anIO->getEntry());
	if ( anActor ) {
	  if ( anActor->IsShrunkable() )
	    anActor->SetShrinkFactor( shrink );
	}
      }
    }
    myViewWindow->Repaint();
  }
}

//=================================================================================
// function : onSelectionChanged()
// purpose  : Called when selection is changed
//=================================================================================
void VisuGUI_ShrinkFactorDlg::onSelectionChanged()
{
  bool shrinkable = false;
  if ( myViewWindow ) {
    int shrinkFactor = UNDEF_SHRINK;

    SALOME_ListIO aList;
    mySelectionMgr->selectedObjects( aList );

    SALOME_ListIteratorOfListIO It( aList );
    for ( ; It.More(); It.Next() ) {
      Handle(SALOME_InteractiveObject) IO = It.Value();
      if (!IO.IsNull()) {
	bool ashrinkable = false;
	int shr = UNDEF_SHRINK;
	VISU_Actor* anActor = VISU::FindActor( VISU::GetAppStudy( myModule ), myViewWindow, IO->getEntry() );
	if ( anActor ) {
	  ashrinkable = anActor->IsShrunkable();
	  if ( ashrinkable ) shr = int( anActor->GetShrinkFactor() * 100. + 0.5 );
	} 
	else {
	  VISU_ActorBase* anActor = VISU::FindActorBase(VISU::GetAppStudy(myModule), myViewWindow, IO->getEntry());
	  if ( anActor ) {
	    ashrinkable = anActor->IsShrunkable();
	    if ( ashrinkable ) shr = int( anActor->GetShrinkFactor() * 100. + 0.5 );
	  }
	}
	shrinkable = shrinkable || ashrinkable;
	if ( shr != UNDEF_SHRINK ) {
	  if ( shrinkFactor == UNDEF_SHRINK )
	    shrinkFactor = shr;
	  else if ( shrinkFactor != shr ) {
	    shrinkFactor = DEFAULT_SHRINK;
	    break;
	  }
	}
      }
    }
    ValueSpin->blockSignals( true );
    ValueSpin->setValue( shrinkFactor == UNDEF_SHRINK ? DEFAULT_SHRINK : shrinkFactor );
    ValueSpin->blockSignals( false );
  }
  GroupC1->setEnabled( shrinkable );
}

//=================================================================================
// function : keyPressEvent()
// purpose  :
//=================================================================================
void VisuGUI_ShrinkFactorDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 ) {
    e->accept();
    help();
  }
}

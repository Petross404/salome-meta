// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_CutSegmentDlg.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_CUTSEGMENTDLG_H
#define VISUGUI_CUTSEGMENTDLG_H

#include "VisuGUI_CutPlanesDlg.h"

#include <QCheckBox>
#include <QList>

class QTabWidget;

class SalomeApp_Module;
class VisuGUI_InputPane;
class SalomeApp_DoubleSpinBox;

namespace VISU
{
  class CutSegment_i;
}

class VisuGUI_CutSegmentDlg: public VisuGUI_ScalarBarBaseDlg
{
  Q_OBJECT

public:
  VisuGUI_CutSegmentDlg (SalomeApp_Module* theModule);
  ~VisuGUI_CutSegmentDlg();

  virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                  bool theInit );

  virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

  bool isGenerateTable() { return myCreateTable->isChecked(); }
  bool isGenerateCurves() { return myCreateTable->isChecked() && myCurvesCheck->isChecked(); }

protected:
  virtual QString GetContextHelpFilePath();
 
protected slots:
  void accept();
  void reject();

private slots:
  void onPreviewCheck(bool thePreview);
  void onAllCurvesInvertedCheck(bool theInvert);
  void onPointModified();

private:
  void createPlanes();
  void deletePlanes();
  void updateGlyphs(bool update);
  void updatePoints(bool update);

  QCheckBox* myCreateTable;
  QCheckBox* myCurvesCheck;

  bool hasInit;

  QList<SalomeApp_DoubleSpinBox*> myPoint1;
  QList<SalomeApp_DoubleSpinBox*> myPoint2;

  SALOME::GenericObjPtr<VISU::CutSegment_i> myCutSegment;

  QTabWidget*            myTabBox;
  VisuGUI_InputPane*     myInputPane;

  SALOME_Actor* myPreviewActor;
  SALOME_Actor* myPreviewActorGlyphs;
  SALOME_Actor* myPreviewActorPoints;
  QCheckBox* myPreviewCheck;
  QCheckBox* myAllCurvesInvertedCheck;
  QCheckBox* myUseAbsoluteLengthCheck;

  SALOME::GenericObjPtr<VISU::CutSegment_i> myPrsCopy;
};

#endif // VISUGUI_CUTSEGMENTDLG_H

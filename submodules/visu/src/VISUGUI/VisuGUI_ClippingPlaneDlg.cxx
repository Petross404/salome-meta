// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VisuGUI_ClippingPlaneDlg.h"
#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"

#include <VISU_Gen_i.hh>

#include <LightApp_Application.h>
#include <SUIT_Desktop.h>
#include <SUIT_Session.h>
#include <SUIT_ViewManager.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>
#include <SVTK_ViewWindow.h>
#include <VTKViewer_Utilities.h>
#include <SalomeApp_DoubleSpinBox.h>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QWidget>
#include <QGroupBox>
#include <QGridLayout>
#include <QCheckBox>
#include <QPushButton>

#include <vtkCallbackCommand.h>
#include <vtkImplicitPlaneWidget.h>


#define SIZEFACTOR 1.1




//****************************************************************
//****************************************************************
//****************************************************************
VisuGUI_ClippingPlaneDlg::VisuGUI_ClippingPlaneDlg(VisuGUI* theModule)
  : QDialog(VISU::GetDesktop(theModule), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
    myModule(theModule),
    myCallback( vtkCallbackCommand::New() ),
    myPreviewWidget(0),
    myViewWindow(0),
    myPlaneId(-1)
{
  myViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>(myModule);
  VISU::ComputeVisiblePropBounds(myViewWindow, myBounds);

  setWindowTitle(tr("TITLE"));
  setSizeGripEnabled(true);
  setModal(false);

  myCallback->SetClientData(this); 
  myCallback->SetCallback(VisuGUI_ClippingPlaneDlg::ProcessEvents);

  QVBoxLayout* aMainLayout = new QVBoxLayout(this);

  QWidget* aPlanesWgt = new QWidget(this);
  aMainLayout->addWidget(aPlanesWgt);
  QVBoxLayout* aFrameLayout = new QVBoxLayout(aPlanesWgt);

  QWidget* aNameBox = new QWidget(aPlanesWgt);
  aFrameLayout->addWidget(aNameBox);
  QHBoxLayout* aNameLayout = new QHBoxLayout(aNameBox);

  aNameLayout->addWidget(new QLabel(tr("LBL_NAME"), aPlanesWgt));
  myNameEdt = new QLineEdit();
  
  _PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myModule));
  _PTR(SObject) aFolderSO;
  if (VISU::getClippingPlanesFolder(aStudy, aFolderSO)) {
    _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aFolderSO);
    int i = 1;
    for (; aIter->More(); aIter->Next()) i++;

    myNameEdt->setText(QString("Plane %1").arg(i));
  }
  aNameLayout->addWidget(myNameEdt);

  QGroupBox* aOriginGroup = new QGroupBox( tr( "ORIGIN_TITLE" ), aPlanesWgt );
  aFrameLayout->addWidget(aOriginGroup);
  QHBoxLayout* aOriginLayout = new QHBoxLayout(aOriginGroup);

  aOriginLayout->addWidget( new QLabel("X", aOriginGroup) );
  myXOrigin = new SalomeApp_DoubleSpinBox( aOriginGroup );
  VISU::initSpinBox( myXOrigin, -1000.0, 1000.0, 0.1, "length_precision" );
  myXOrigin->setValue( 0.0 );
  connect(myXOrigin, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged()));
  aOriginLayout->addWidget( myXOrigin );

  aOriginLayout->addWidget( new QLabel("Y", aOriginGroup) );
  myYOrigin = new  SalomeApp_DoubleSpinBox( aOriginGroup );
  VISU::initSpinBox( myYOrigin, -1000.0, 1000.0, 0.1, "length_precision" );  
  myYOrigin->setValue( 0.0 );
  connect(myYOrigin, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged()));
  aOriginLayout->addWidget( myYOrigin );

  aOriginLayout->addWidget( new QLabel("Z", aOriginGroup) );
  myZOrigin = new SalomeApp_DoubleSpinBox( aOriginGroup );
  VISU::initSpinBox( myZOrigin, -1000.0, 1000.0, 0.1, "length_precision" );   
  myZOrigin->setValue( 0.0 );
  connect(myZOrigin, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged()));
  aOriginLayout->addWidget( myZOrigin );
  
  QGroupBox* aDirGroup = new QGroupBox( tr( "DIRECTION_TITLE" ), aPlanesWgt );
  aFrameLayout->addWidget(aDirGroup);
  QHBoxLayout* aDirLayout = new QHBoxLayout(aDirGroup);

  aDirLayout->addWidget( new QLabel("dX", aDirGroup) );
  myXDir = new SalomeApp_DoubleSpinBox( aDirGroup );
  VISU::initSpinBox( myXDir, -1000.0, 1000.0, 0.1, "length_precision" );  
  myXDir->setValue( 0.0 );
  connect(myXDir, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged()));
  aDirLayout->addWidget( myXDir );

  aDirLayout->addWidget( new QLabel("dY", aDirGroup) );
  myYDir = new SalomeApp_DoubleSpinBox( aDirGroup );
  VISU::initSpinBox( myYDir, -1000.0, 1000.0, 0.1, "length_precision" );  
  myYDir->setValue( 0.0 );
  connect(myYDir, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged()));
  aDirLayout->addWidget( myYDir );

  aDirLayout->addWidget( new QLabel("dZ", aDirGroup) );
  myZDir = new SalomeApp_DoubleSpinBox( aDirGroup );
  VISU::initSpinBox( myZDir, -1000.0, 1000.0, 0.1, "length_precision" );   
  myZDir->setValue( 1.0 );
  connect(myZDir, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged()));
  aDirLayout->addWidget( myZDir );

  myAutoApply = new QCheckBox(tr("CHK_AUTOAPPLY"), aPlanesWgt);
  myAutoApply->setCheckState(Qt::Checked);
  aFrameLayout->addWidget(myAutoApply);

  // Dialog buttons
  QGroupBox* aGroupButtons = new QGroupBox (this);
  aMainLayout->addWidget(aGroupButtons);

  QSizePolicy aSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed );
  QHBoxLayout* aButtonsLayout = new QHBoxLayout(aGroupButtons);

  QPushButton* aBtnOk = new QPushButton(tr("BUT_OK"), aGroupButtons);
  aButtonsLayout->addWidget(aBtnOk);

  aButtonsLayout->addStretch();

  QPushButton* aBtnClose = new QPushButton(tr("BUT_CANCEL"), aGroupButtons);
  aButtonsLayout->addWidget(aBtnClose);

  QPushButton* aBtnHelp = new QPushButton(tr("BUT_HELP"), aGroupButtons);
  aButtonsLayout->addWidget(aBtnHelp);

  connect(aBtnOk   , SIGNAL(clicked()), this, SLOT(accept()));
  connect(aBtnClose, SIGNAL(clicked()), this, SLOT(reject()));
  connect(aBtnHelp , SIGNAL(clicked()), this, SLOT(onHelp()));


  myPreviewWidget = createPreviewWidget();
  myViewWindow->Repaint();
}

VisuGUI_ClippingPlaneDlg::~VisuGUI_ClippingPlaneDlg()
{
  if (myPreviewWidget) {
    myPreviewWidget->Off();
    myPreviewWidget->Delete();
  }
  myPreviewWidget = 0;
  myCallback->Delete();
}

//****************************************************************
void VisuGUI_ClippingPlaneDlg::ProcessEvents(vtkObject* theObject, 
                                             unsigned long theEvent,
                                             void* theClientData, 
                                             void* vtkNotUsed(theCallData))
{
  vtkImplicitPlaneWidget* aWidget = vtkImplicitPlaneWidget::SafeDownCast(theObject);
  if (aWidget == NULL) return;
  if (theClientData == NULL) return;

  VisuGUI_ClippingPlaneDlg* aDlg = (VisuGUI_ClippingPlaneDlg*) theClientData;

  double aOrigin[3];
  double aDir[3];

  switch(theEvent){
  case vtkCommand::InteractionEvent:
    aWidget->GetOrigin(aOrigin);
    aWidget->GetNormal(aDir);

    aDlg->setOrigin(aOrigin);
    aDlg->setDirection(aDir);

    break;
  }
}

//****************************************************************
void VisuGUI_ClippingPlaneDlg::setOrigin(double theVal[3])
{
  myXOrigin->setValue(theVal[0]);
  myYOrigin->setValue(theVal[1]);
  myZOrigin->setValue(theVal[2]);
}

//****************************************************************
void VisuGUI_ClippingPlaneDlg::setDirection(double theVal[3])
{
  myXDir->setValue(theVal[0]);
  myYDir->setValue(theVal[1]);
  myZDir->setValue(theVal[2]);
}

//****************************************************************
void VisuGUI_ClippingPlaneDlg::onValueChanged()
{
  if (!myPreviewWidget) return;
  double aOrigin[3];
  double aDir[3];
  aOrigin[0] = myXOrigin->value();
  aOrigin[1] = myYOrigin->value();
  aOrigin[2] = myZOrigin->value();

  aDir[0] = myXDir->value();
  aDir[1] = myYDir->value();
  aDir[2] = myZDir->value();

  myPreviewWidget->SetOrigin(aOrigin);
  myPreviewWidget->SetNormal(aDir);
  myViewWindow->Repaint();
}

//****************************************************************
vtkImplicitPlaneWidget* VisuGUI_ClippingPlaneDlg::createPreviewWidget()
{
  vtkImplicitPlaneWidget* aPlaneWgt = vtkImplicitPlaneWidget::New();
  aPlaneWgt->SetInteractor(myViewWindow->getInteractor());
  aPlaneWgt->SetPlaceFactor(SIZEFACTOR);
  aPlaneWgt->ScaleEnabledOff();
  aPlaneWgt->PlaceWidget(myBounds[0],myBounds[1],myBounds[2],myBounds[3],myBounds[4],myBounds[5]);
  aPlaneWgt->SetOrigin(0,0,0);
  aPlaneWgt->SetNormal(0,0,1);
  aPlaneWgt->On();

  //aPlaneWgt->OutlineTranslationOff();
  //aPlaneWgt->ScaleEnabledOn();
  aPlaneWgt->AddObserver(vtkCommand::InteractionEvent, 
                         myCallback.GetPointer(), 
                         0.);
  return aPlaneWgt;
}


//****************************************************************
void VisuGUI_ClippingPlaneDlg::setPlaneId(int theId)
{
  myPlaneId = theId;
  VISU_ClippingPlaneMgr& aMgr = VISU::GetVisuGen(myModule)->GetClippingPlaneMgr();

  VISU_CutPlaneFunction* aPlane =  aMgr.GetClippingPlane(myPlaneId);
  if (aPlane) {
    myNameEdt->setText(QString(aPlane->getName().c_str()));
    double aOrigin[3], aDir[3];
    aPlane->GetOrigin(aOrigin);
    aPlane->GetNormal(aDir);

    myXOrigin->setValue(aOrigin[0]);
    myYOrigin->setValue(aOrigin[1]);
    myZOrigin->setValue(aOrigin[2]);
    myPreviewWidget->SetOrigin(aOrigin);

    myXDir->setValue(aDir[0]);
    myYDir->setValue(aDir[1]);
    myZDir->setValue(aDir[2]);

    myPreviewWidget->SetNormal(aDir);

    myAutoApply->setCheckState((aPlane->isAuto())? Qt::Checked : Qt::Unchecked);
    myViewWindow->Repaint();
  }
}
/*void VisuGUI_ClippingPlaneDlg::setPlaneObj(_PTR(SObject) thePlaneObj)
{
  myPlaneObj = thePlaneObj;

  myNameEdt->setText(QString(myPlaneObj->GetName().c_str()));

  _PTR(GenericAttribute) anAttr;
  if (myPlaneObj->FindAttribute(anAttr, "AttributeSequenceOfReal")) {
    _PTR(AttributeSequenceOfReal) aArray(anAttr);
    myXOrigin->setValue(aArray->Value(1));
    myYOrigin->setValue(aArray->Value(2));
    myZOrigin->setValue(aArray->Value(3));

    myPreviewWidget->SetOrigin(aArray->Value(1), aArray->Value(2), aArray->Value(3));

    myXDir->setValue(aArray->Value(4));
    myYDir->setValue(aArray->Value(5));
    myZDir->setValue(aArray->Value(6));

    myPreviewWidget->SetNormal(aArray->Value(4), aArray->Value(5), aArray->Value(6));
  }
  if (myPlaneObj->FindAttribute(anAttr, "AttributeInteger")) {
    _PTR(AttributeInteger) aFlag(anAttr);
    myAutoApply->setCheckState((aFlag->Value() == 1)? Qt::Checked : Qt::Unchecked);
  }

  myViewWindow->Repaint();
}*/

//****************************************************************
void VisuGUI_ClippingPlaneDlg::accept()
{
  _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );
  if(!aStudy->GetProperties()->IsLocked()) {
    VISU_ClippingPlaneMgr& aMgr = VISU::GetVisuGen(myModule)->GetClippingPlaneMgr();
    if (myPlaneId == -1) { // Create a New plane
      myPlaneId = aMgr.CreateClippingPlane(myXOrigin->value(), myYOrigin->value(), myZOrigin->value(),
                                           myXDir->value(), myYDir->value(), myZDir->value(),
                                           myAutoApply->checkState() == Qt::Checked,
                                           qPrintable(myNameEdt->text()));
    } else { // Edit Plane
      aMgr.EditClippingPlane(myPlaneId,
                             myXOrigin->value(), myYOrigin->value(), myZOrigin->value(),
                             myXDir->value(), myYDir->value(), myZDir->value(),
                             myAutoApply->checkState() == Qt::Checked,
                             qPrintable(myNameEdt->text()));
    }
    VISU::UpdateObjBrowser(myModule);
  }
  /*  _PTR(Study) aStudy = VISU::GetCStudy( VISU::GetAppStudy( myModule ) );
  if(!aStudy->GetProperties()->IsLocked()) {
    _PTR(SObject) aFolder;
    if (VISU::getClippingPlanesFolder(aStudy, aFolder)) {
      _PTR(StudyBuilder) aBuilder = aStudy->NewBuilder();
      if (myPlaneObj == 0) { // Create a New plane
        myPlaneObj = aBuilder->NewObject(aFolder);
      } 
      // Save Name
      _PTR(GenericAttribute) anAttr;
      anAttr = aBuilder->FindOrCreateAttribute(myPlaneObj,"AttributeName");
      _PTR(AttributeName) aName(anAttr);
      aName->SetValue(myNameEdt->text().toStdString());

      //Save Parameters
      double aParams[6];
      aParams[0] = myXOrigin->value();
      aParams[1] = myYOrigin->value();
      aParams[2] = myZOrigin->value();
      aParams[3] = myXDir->value();
      aParams[4] = myYDir->value();
      aParams[5] = myZDir->value();

      anAttr = aBuilder->FindOrCreateAttribute(myPlaneObj,"AttributeSequenceOfReal");
      _PTR(AttributeSequenceOfReal) aArray(anAttr);
      if (aArray->Length() == 6) {
        for (int i = 0; i < 6; i++)
          aArray->ChangeValue(i+1, aParams[i]);
      } else {
        for (int i = 0; i < 6; i++)
          aArray->Add(aParams[i]);
      }
      // Save Bool Flag
      anAttr = aBuilder->FindOrCreateAttribute(myPlaneObj,"AttributeInteger");
      _PTR(AttributeInteger) aFlag(anAttr);
      aFlag->SetValue((myAutoApply->checkState() == Qt::Checked)? 1 : 0);

      VISU::UpdateObjBrowser(myModule);
    }
    }*/
  QDialog::accept();
}


//****************************************************************
void VisuGUI_ClippingPlaneDlg::onHelp()
{
  QString aHelpFileName = "clipping_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myModule ? app->moduleName(myModule->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             QObject::tr("BUT_OK"));
  }
}


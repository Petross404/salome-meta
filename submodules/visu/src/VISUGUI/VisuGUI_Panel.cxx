// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Panel.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#include "VisuGUI_Panel.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"

#include "SUIT_ResourceMgr.h"

#include <QScrollArea>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QPushButton>

/*!
  \class MainFrame
  \internal
  \brief Frame inserted in viewport with redefined sizeHint method 
         in order to avoid unreasonable increasing of viewport size
*/
class VisuGUI_Panel::MainFrame : public QWidget
{
public:
  /*!
    \brief Constructor.
    \param theParent parent widget
  */
  MainFrame( QWidget* theParent = 0 )
  : QWidget( theParent )
  {
  }
  
  /*!
    \brief Gets frame size hint
    \return frame size hint
  */
  virtual QSize sizeHint() const
  {
    return minimumSizeHint();
  }
};

/*!
  \class VisuGUI_Panel
  \brief Base class for VISU interactive dockable panels.

  Set of classes is derived from this class and are intended for representing widgets 
  (GUI controls) for different operations. VisuGUI_Panel consists of main frame 
  inserted in scroll view and four push buttons. So all widgets of derived sub-panels 
  should be inherited from mainframe() instead of �this� pointer.
*/

/*!
  \brief Constructor creates panels look and feel
  \param theName panel title
  \param theModule parent VISU GUI module
  \param theParent parent widget
  \param theBtns panel buttons
*/
VisuGUI_Panel::VisuGUI_Panel( const QString& theName,
                              VisuGUI*       theModule, 
                              QWidget*       theParent,
                              const int      theBtns  )
  : QtxDockWidget( true, theParent ),
    myModule( theModule ),
    myOK( 0 ),
    myApply( 0 ),
    myClose( 0 ),
    myHelp( 0 )
{
  setObjectName( theName );

  QWidget* aGrp = new QWidget( this );
  setWidget( aGrp );

  // Create scroll view
  myView = new QScrollArea( aGrp );
  myView->setFrameStyle( QFrame::Plain | QFrame::NoFrame );

  // Create main frame
  myMainFrame = new MainFrame( myView );
  
  myView->setWidget( myMainFrame );
  myView->setAlignment( Qt::AlignCenter );
  myView->setWidgetResizable( true );
  myView->setMinimumWidth( myMainFrame->sizeHint().width() + 22 );
  
  // Create buttons
  QHBoxLayout* aBtnWgLayout = new QHBoxLayout;
  aBtnWgLayout->setMargin( 0 );

  aBtnWgLayout->addStretch();

  if( theBtns & OKBtn )
  {
    myOK = new QPushButton( tr( "BUT_OK" ), aGrp );
    aBtnWgLayout->addWidget( myOK );
    connect( myOK, SIGNAL( clicked() ), SLOT( onOK() ) );
  }
  if( theBtns & ApplyBtn )
  {
    myApply = new QPushButton( tr( "BUT_APPLY" ), aGrp );
    aBtnWgLayout->addWidget( myApply );
    connect( myApply, SIGNAL( clicked() ), SLOT( onApply() ) );
  }
  if( theBtns & CloseBtn )
  {
    myClose = new QPushButton( tr( "BUT_CLOSE" ), aGrp );
    aBtnWgLayout->addWidget( myClose );
    connect( myClose, SIGNAL( clicked() ), SLOT( onClose() ) );
  }
  if( theBtns & HelpBtn )
  {
    myHelp = new QPushButton( tr( "BUT_HELP" ), aGrp );
    aBtnWgLayout->addWidget( myHelp );
    connect( myHelp, SIGNAL( clicked() ), SLOT( onHelp() ) );
  }

  aBtnWgLayout->addStretch();

  // fill layout
  QVBoxLayout* aLay = new QVBoxLayout( aGrp );
  aLay->setMargin( 2 );
  aLay->addWidget( myView, 1 );
  aLay->addLayout( aBtnWgLayout );

  connect( theModule, SIGNAL( moduleDeactivated() ), SLOT( onModuleDeactivated() ) );
  connect( theModule, SIGNAL( moduleActivated() ),   SLOT( onModuleActivated() ) );
}

/*!
  \brief Destructor
*/
VisuGUI_Panel::~VisuGUI_Panel()
{
}

/*!
  \brief Verifies validity of input data

  This virtual method should be redefined in derived classes. Usually operator 
  corresponding to the sub-panel calls this method to check validity of input 
  data when Apply/OK button is pressed.

  \param theErrMsg Error message. 
  
        If data is invalid when panel can return message using this parameter given 
        clear explanation what is wrong

  \return TRUE if data is valid, FALSE otherwise 
*/
bool VisuGUI_Panel::isValid( QString& /*theErrMsg*/ )
{
  return true;
}
/*!
  \brief Virtual methods should be redefined in derived classes and 
         clears all GUI controls
*/
void VisuGUI_Panel::clear()
{
}

/*!
  \brief Virtual slot called when �OK� button pressed emits corresponding signal.

  This slot moves focus in OK button before emitting signal. Mainly it provides 
  application with correct moving data from currently edited controls to internal 
  structure. For example QTable moves data from cell editor to table item when 
  focus is out.

*/
void VisuGUI_Panel::onOK()
{
  if ( myOK )
  {
    myOK->setFocus();
    qApp->processEvents();
  }
}

/*!
  \brief Virtual slot called when �Apply� button pressed emits corresponding signal.
  \sa onOK
*/
void VisuGUI_Panel::onApply()
{
  if ( myApply )
  {
    myApply->setFocus();
    qApp->processEvents();
  }
}

/*!
  \brief Virtual slot called when �Close� button pressed emits corresponding signal.
  \sa onOK
*/
void VisuGUI_Panel::onClose()
{
  if ( myClose )
    myClose->setFocus();
  hide();
}

/*!
  \brief Virtual slot called when �Help� button pressed emits corresponding signal.
  \sa onOK
*/
void VisuGUI_Panel::onHelp()
{
  if ( myHelp )
    myHelp->setFocus();
}

/*!
  \brief Gets frame inserted in scroll view. All controls of derived 
         panels should use it as parent
  \return QFrame* object 
*/
QWidget* VisuGUI_Panel::mainFrame()
{
  return myMainFrame;
}

void VisuGUI_Panel::onModuleActivated()
{
  widget()->setHidden( false );
}

void VisuGUI_Panel::onModuleDeactivated()
{
  widget()->setHidden( true );
}

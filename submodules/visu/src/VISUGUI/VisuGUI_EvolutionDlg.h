// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_EvolutionDlg.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_EVOLUTIONDLG_H
#define VISUGUI_EVOLUTIONDLG_H

#include <QtxDialog.h>

#include <SALOMEDSClient_Study.hxx>

#include <SVTK_Selection.h>

class QComboBox;
class QIntValidator;
class QKeyEvent;
class QLineEdit;

class SALOME_Actor;
class SVTK_ViewWindow;

class VisuGUI;
class VISU_Evolution;

class VisuGUI_EvolutionDlg: public QtxDialog
{
  Q_OBJECT

public:
  VisuGUI_EvolutionDlg( VisuGUI* theModule, _PTR(Study) theStudy );
  ~VisuGUI_EvolutionDlg();

public:
  bool                      setField( _PTR(SObject) theField );

  void                      restoreFromStudy( _PTR(SObject) theEvolution );

protected:
  virtual void              setVisible( bool theIsVisible );
  virtual void              keyPressEvent( QKeyEvent* theEvent );

protected slots:
  virtual void              accept();

private:
  void                      updateFromEngine();

private slots:
  void                      onPointEdited( const QString& );
  void                      onSelectionEvent();
  void                      onViewWindowClosed();
  void                      onHelp();

private:
  VisuGUI*                  myModule;
  VISU_Evolution*           myEngine;

  QLineEdit*                myFieldLE;
  QLineEdit*                myPointLE;
  QComboBox*                myComponentCB;

  QIntValidator*            myPointValidator;

  SVTK_ViewWindow*          myViewWindow;
  SALOME_Actor*             myPreviewActor;
  Selection_Mode            myStoredSelectionMode;
};

#endif //VISUGUI_EVOLUTIONDLG_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_TimeAnimation.h
//  Author : Vitaly SMETANNIKOV
//  Module : VISU
//
#ifndef VISUGUI_TIMEANIMATION_H
#define VISUGUI_TIMEANIMATION_H

#include <SalomeApp_DoubleSpinBox.h>
#include "SALOMEDSClient_Study.hxx"

#include "VTKViewer.h"

#include <QDialog>
#include <QButtonGroup>
#include <QStackedWidget>
#include <QMap>

// IDL headers
#include "SALOMEconfig.h"
#include CORBA_SERVER_HEADER(VISU_Gen)

#include <vector>
#include <map>

class VisuGUI;
class SalomeApp_Module;
class SalomeApp_IntSpinBox;
class VISU_TimeAnimation;

namespace VISU
{
  class Prs3d_i;
  class PointMap3d_i;
}

class SVTK_ViewWindow;

class QLabel;
class QSlider;
class QListWidget;
class QLineEdit;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QToolButton;

/*!
 * Auxilliary class for presentations definition
 */
class ArrangeDlg: public QDialog
{
  Q_OBJECT
 public:
  enum { AutoMode, ManualMode };
  enum { XAxis, YAxis, ZAxis };

  ArrangeDlg(QWidget* theParent,
             VISU_TimeAnimation* theAnimator);

  ArrangeDlg(QWidget* theParent,
             const SalomeApp_Module* theModule,
             SVTK_ViewWindow* theViewWindow);

  ~ArrangeDlg() {};

  int getMode() const { return  myStackWgt->indexOf(myStackWgt->currentWidget()); }
  double getDistance() const { return myDistVal->value(); }
  int getAxis() const { return myAxisGrp->id(myAxisGrp->checkedButton()); }

 protected slots:
  virtual void accept();
  void onFieldChange(int);

 private:
  struct Offset {
    double myOffset[3];
  };


  void init();
  void acceptAnimation();
  void acceptViewWindow();

  VISU_TimeAnimation* myAnimator;
  SVTK_ViewWindow   * myViewWindow;

  QStackedWidget*     myStackWgt;
  QButtonGroup*       myAxisGrp;
  SalomeApp_DoubleSpinBox*    myDistVal;
  QListWidget*        myFieldLst;
  SalomeApp_DoubleSpinBox*    myCoord[3];
  QList<Offset>       myOffsets;

  QCheckBox*          mySaveChk;

  int               myCurrent;
  QMap<VISU::Prs3d_i*, int> myPrsMap;
  QMap<VISU::PointMap3d_i*, int> myPointMapPrsMap;
};


class SetupDlg : public QDialog
{
  Q_OBJECT

 class LineEdit;

 public:
  SetupDlg( QWidget* theParent, VisuGUI* theModule,
            VISU_TimeAnimation* theAnimator );
  ~SetupDlg() {};

 public:
  void initialize();

  bool isRegenerate() const { return myIsRegenerate; }
  void setIsRegenerate( const bool on ) { myIsRegenerate = on; }

 private slots:
  void onClose();
  void onFieldChange(int theIndex);
  void onTypeChanged(int theIndex);
  //  void onScalarBarDlg();
  void onPreferencesDlg();
  void onRangeCheck(bool theCheck);
  //  void onMinValue(double theVal);
  //  void onMaxValue(double theVal);
  void onMinValue();
  void onMaxValue();
  void onMinValueEdit(const QString&);
  void onMaxValueEdit(const QString&);
  void onSequenceCheck(bool theCheck);
  void onIndicesChanged(const QString& theIndices);
  void onValuesChanged();
  void onArrangeDlg();

 private:
  VisuGUI* myModule;
  VISU_TimeAnimation* myAnimator;

  bool myIsRegenerate;

  QListWidget* myFieldLst;
  QComboBox* myTypeCombo;
  std::vector<int> myComboId2TypeId;
  std::map<int,int> myTypeId2ComboId;

  QPushButton* myPropBtn;
  QPushButton* myArrangeBtn;
  QGroupBox* myUseRangeBox;
  //  SalomeApp_DoubleSpinBox* myMinVal;
  //  SalomeApp_DoubleSpinBox* myMaxVal;
  QLineEdit* myMinVal;
  QLineEdit* myMaxVal;

  QGroupBox* myUseSequenceBox;
  QLineEdit* myIndices;
  QListWidget* myValues;
};

class VisuGUI_TimeAnimationDlg: public QDialog
{
    Q_OBJECT
 public:
    VisuGUI_TimeAnimationDlg(VisuGUI* theModule,
                             _PTR(Study) theStudy,
                             VISU::Animation::AnimationMode theMode = VISU::Animation::PARALLEL);
    ~VisuGUI_TimeAnimationDlg();

    bool addField(_PTR(SObject) theField);
    void clearView();

    void restoreFromStudy(_PTR(SObject) theAnimation);

 protected:
    virtual void closeEvent(QCloseEvent* theEvent);
    virtual void showEvent(QShowEvent* theEvent);
    virtual void keyPressEvent(QKeyEvent* theEvent);
    void stopAnimation();

 private slots:
   void onTypeChange(int index);
   void onPlayPressed();
   void onBackPressed();
   void onForvardPressed();
   void onLastPressed();
   void onFirstPressed();
   void onSetupDlg();
   void onWindowChanged(int index);
   void createFrames();
   void onSpeedChange(double theSpeed);
   void onExecution(long theNewFrame, double theTime);
   void onBrowse();
   void onBrowseAVI();
   void onCheckDump(bool);
   void onStop();
   void onHelp();
   void saveToStudy();
   void publishToStudy();
   /*!Sets dump format for myAnimator (setDumpFormat(...)), from myPicsFormat.*/
   void onPicsFormatChanged();
   /*!Sets path for myAnimator (dumpTo(...)), from myPathEdit.*/
   void onPathChanged();

 private:
    // Data
    VisuGUI* myModule;
    VISU_TimeAnimation* myAnimator;

    // widgets
    QSlider* mySlider;
    QComboBox* myTypeCombo;
    QPushButton* myPropBtn;
    QPushButton* myPlayBtn;
    QLabel* myTimeLbl;
    QPushButton* mySetupBtn;
    QPushButton* myGenBtn;

    QFrame* myPlayFrame;
    SetupDlg* mySetupDlg;

    QCheckBox* mySaveCheck;
    QComboBox* myPicsFormat;
    QLineEdit* myPathEdit;

    QCheckBox*   mySaveAVICheck;
    QLineEdit*   myPathAVIEdit;
    QLabel*      myPathAVILbl;
    QPushButton* myBrowseAVIBtn;
    QLabel*      myFrequencyLbl;
    SalomeApp_IntSpinBox* myFrequencySpin;

    QPushButton* myPublishBtn;
    QPushButton* mySaveBtn;
};

#endif  //VISUGUI_TIMEANIMATION_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_CutPlanesDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_CUTPLANESDLG_H
#define VISUGUI_CUTPLANESDLG_H

#include "VisuGUI_Prs3dDlg.h"

#include <SALOME_Actor.h>
#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include <QFrame>

class QRadioButton;
class QTabWidget;
class QTableWidget;
class QCheckBox;
class QComboBox;

#include "SALOMEconfig.h"
#include CORBA_CLIENT_HEADER(VISU_Gen)

#include <map>
#include <vector>

namespace VISU 
{
  class CutPlanes_i;
  class Result_i;
};

class SUIT_ViewWindow;
class SUIT_ViewManager;
class SalomeApp_Module;
class VisuGUI_InputPane;

class VisuGUI_CutPlanesPane : public QFrame
{
    Q_OBJECT

public:
    VisuGUI_CutPlanesPane (QWidget* parent);
    ~VisuGUI_CutPlanesPane();

    void   setNbPlanes( const int nbp ) {nbPlan->setValue( nbp );}
    int    getNbPlanes() {return nbPlan->value();}
    void   setPlanePos( const VISU::CutPlanes::Orientation orient/*, const double pos1, const double pos2 */);
    VISU::CutPlanes::Orientation  getOrientaion();
    void   setRotation( const double r1, const double r2 );
    double getRotation1() {return Rot1->value();}
    double getRotation2() {return Rot2->value();}

    double getScaleFactor();
    void   setScaleFactor(double factor);

    void   initFromPrsObject(VISU::CutPlanes_i* thePrs);
    int    storeToPrsObject(VISU::CutPlanes_i* thePrs);

private:
    typedef std::vector<QString> TVectorialFieldsList;
    typedef std::map<VISU::Entity, TVectorialFieldsList> TEntity2VectorialFields;
    typedef std::pair<VISU::Entity,TVectorialFieldsList> TEntVectPair;
    TEntity2VectorialFields myEntity2VectorialFields;

    void createPlanes();
    void deletePlanes();

    void InitEntity2VectorialFieldsMap(VISU::ColoredPrs3d_i* thePrs);
    void InsertAllVectorialFields();
    void InitVectorialField();

    QLabel* LabelRot1;
    QLabel* LabelRot2;
    QGroupBox* GDeformation;
    SalomeApp_IntSpinBox* nbPlan;
    SalomeApp_DoubleSpinBox* Rot1;
    SalomeApp_DoubleSpinBox* Rot2;
    QRadioButton* RBzx;
    QRadioButton* RByz;
    QRadioButton* RBxy;
    SalomeApp_DoubleSpinBox* myPosSpn;
    QTableWidget* myPosTable;
    SALOME::GenericObjPtr<VISU::CutPlanes_i> myCutPlanes;
    QCheckBox* myPreviewCheck;
    QComboBox* myVectorialFieldCombo;
    SalomeApp_DoubleSpinBox* myScaleSpn;
    double          X1, X2;
    double          Y1, Y2;
    double          Z1, Z2;
    bool hasInit;

    SALOME_Actor* myPreviewActor;

private slots:
    void orientationChanged( int );
    void DrawTable();
    void setDefault( int all = -1);
    void onValueChanged(int theRow, int theCol);
    void onRotation(double theValue);
    void onPreviewCheck(bool thePreview);
    void onScaleFactorChanged(double);
    void onDeformationCheck(bool);
    void onVectorialFieldChanged(int);
};


class VisuGUI_CutPlanesDlg : public VisuGUI_ScalarBarBaseDlg
{
    Q_OBJECT

public:
    VisuGUI_CutPlanesDlg (SalomeApp_Module* theModule);
    ~VisuGUI_CutPlanesDlg();

    virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                    bool theInit );

    virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

protected:
    virtual QString        GetContextHelpFilePath();

protected slots:
    void accept();
    void reject();

private:
    QTabWidget*            myTabBox;
    VisuGUI_CutPlanesPane* myCutPane;
    VisuGUI_InputPane*     myInputPane;
    SALOME::GenericObjPtr<VISU::CutPlanes_i> myPrsCopy;
};


/*class VisuGUI_NumEditItem: public QTableItem
{
public:
    VisuGUI_NumEditItem(QTableWidget* table, const QString& text ):
      QTableItem(table, et, text) {};
    ~VisuGUI_NumEditItem() {};

    QWidget* createEditor() const;
};*/

#endif // VISUGUI_CUTPLANESDLG_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_SizeBox.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_SIZEBOX_H
#define VISUGUI_SIZEBOX_H

#include <QWidget>

class QLabel;
class QLineEdit;
class QPushButton;
class QCheckBox;
class QGroupBox;

class SalomeApp_DoubleSpinBox;
class SalomeApp_IntSpinBox;
class QtxColorButton;

class VisuGUI_SizeBox : public QWidget
{
  Q_OBJECT

public:
  enum { Results = 0, Geometry, Inside, Outside };

public:
  VisuGUI_SizeBox( QWidget* );
  ~VisuGUI_SizeBox() {}

public:
  int                      getType() const { return myType; }
  void                     setType( int );

  float                    getOutsideSize() const;
  void                     setOutsideSize( float );

  float                    getGeomSize() const;
  void                     setGeomSize( float );

  float                    getMinSize() const;
  void                     setMinSize( float );

  float                    getMaxSize() const;
  void                     setMaxSize( float );

  float                    getMagnification() const;
  void                     setMagnification( float );

  float                    getIncrement() const;
  void                     setIncrement( float );

  bool                     getUniform() const;
  void                     setUniform( bool );

  QColor                   getColor() const;
  void                     setColor( const QColor& );

  void                     enableSizeControls( bool enabled );

protected slots:
  void                     onToggleResults();
  void                     onToggleGeometry();
  void                     onToggleInside();
  void                     onToggleOutside();

  //void                     onColorButtonPressed();

private:
  int                      myType;

  QLabel*                  myOutsideSizeLabel;
  SalomeApp_IntSpinBox*    myOutsideSizeSpinBox;

  QLabel*                  myGeomSizeLabel;
  SalomeApp_IntSpinBox*    myGeomSizeSpinBox;

  QLabel*                  myMinSizeLabel;
  SalomeApp_IntSpinBox*    myMinSizeSpinBox;

  QLabel*                  myMaxSizeLabel;
  SalomeApp_IntSpinBox*    myMaxSizeSpinBox;

  QLabel*                  myMagnificationLabel;
  SalomeApp_IntSpinBox*    myMagnificationSpinBox;

  QLabel*                  myIncrementLabel;
  SalomeApp_DoubleSpinBox* myIncrementSpinBox;

  QCheckBox*               myUniformCheckBox;  

  QGroupBox*               myColorGroup;
  QLabel*                  myColorLabel;
  QtxColorButton*          myColorButton;
};



#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_CacheDlg.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_CACHEDLG_H
#define VISUGUI_CACHEDLG_H

#include "SALOMEconfig.h"
#include CORBA_SERVER_HEADER(VISU_Gen)

#include <QDialog>

class QLineEdit;
class QRadioButton;
class SalomeApp_DoubleSpinBox;

class SalomeApp_Module;

class VisuGUI_CacheDlg : public QDialog
{
  Q_OBJECT

public:
  VisuGUI_CacheDlg( VISU::ColoredPrs3dCache_var aCache,
                    SalomeApp_Module* theModule );
  virtual ~VisuGUI_CacheDlg();

public:
  bool            isLimitedMemory();
  double          getLimitedMemory();

protected slots:
  virtual void    accept();

  void            onHelp();

private:
  VISU::ColoredPrs3dCache_var myCache;

  QRadioButton*   myMimimalMemoryButton;
  QRadioButton*   myLimitedMemoryButton;
  SalomeApp_DoubleSpinBox* myLimitedMemory;

  QLineEdit*      myUsedMemory;
  QLineEdit*      myFreeMemory;
};

#endif

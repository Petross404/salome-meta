// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : Displayer for VISU module
//  File   : VisuGUI_Displayer.h
//  Author : Alexander SOLOVYOV
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_Displayer.h
//
#ifndef VISUGUI_DISPLAYER_HEADER
#define VISUGUI_DISPLAYER_HEADER

#include <LightApp_Displayer.h>
#include <VISU_Prs3d_i.hh>
#include <VISU_Table_i.hh>
#include <VISU_PointMap3d_i.hh>

class SalomeApp_Application;
class SVTK_ViewWindow;
class Plot2d_ViewWindow;
class SPlot2d_Prs;

class VisuGUI_Displayer : public LightApp_Displayer
{
public:
  VisuGUI_Displayer( SalomeApp_Application* );
  ~VisuGUI_Displayer();

  virtual SALOME_Prs* buildPresentation( const QString&, SALOME_View* = 0 );
  virtual bool        canBeDisplayed( const QString& /*entry*/, const QString& /*viewer_type*/ ) const;
  virtual bool        IsDisplayed( const QString&, SALOME_View* = 0 ) const;
  
  virtual void        AfterDisplay ( SALOME_View*, const SALOME_Prs2d* );
  virtual void        AfterErase( SALOME_View*, const SALOME_Prs2d* );

protected:
          bool         addCurve      ( SPlot2d_Prs*, Plot2d_ViewWindow*, VISU::Curve_i* ) const;
  virtual void         buildPrs3d    ( SVTK_ViewWindow*,   VISU::Prs3d_i* ) const;
  virtual SPlot2d_Prs* buildCurve    ( Plot2d_ViewWindow*, VISU::Curve_i* ) const;
  virtual SPlot2d_Prs* buildContainer( Plot2d_ViewWindow*, VISU::Container_i* ) const;
  virtual SPlot2d_Prs* buildTable    ( Plot2d_ViewWindow*, VISU::Table_i* ) const;

private:
  SalomeApp_Application*   myApp;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_FieldFilter.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_FieldFilter.h"

#include "VisuGUI_Tools.h"

#include "VISUConfig.hh"
#include "VISU_ColoredPrs3dFactory.hh"

#include <SUIT_Session.h>

#include <SalomeApp_Study.h>
#include <LightApp_DataOwner.h>

using namespace VISU;

VisuGUI_FieldFilter::VisuGUI_FieldFilter( VISU::VISUType theType ) : 
  myType( theType )
{
}

VisuGUI_FieldFilter::~VisuGUI_FieldFilter() 
{
}

void VisuGUI_FieldFilter::setPrs3dEntry( const QString& theEntry )
{
  myPrs3dEntry = theEntry;
}

bool VisuGUI_FieldFilter::isOk( const SUIT_DataOwner* theDataOwner ) const
{
  const LightApp_DataOwner* anOwner =
    dynamic_cast<const LightApp_DataOwner*>( theDataOwner );

  SalomeApp_Study* anAppStudy = dynamic_cast<SalomeApp_Study*>
    (SUIT_Session::session()->activeApplication()->activeStudy());

  if( anOwner && anAppStudy )
  {
    if(myPrs3dEntry == anOwner->entry())
      return true;

    _PTR(Study) aStudy = anAppStudy->studyDS();
    _PTR(SObject) aSObject = aStudy->FindObjectID( (const char*)anOwner->entry().toLatin1() );
    if (!aSObject)
      return false;

    _PTR(SObject) aRefSO;
    if( aSObject->ReferencedObject( aRefSO ) )
      aSObject = aRefSO;

    if( !aSObject )
      return false;

    VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap( aSObject );
    if( aRestoringMap.empty() )
      return false;

    VISU::VISUType aType = VISU::Storable::RestoringMap2Type( aRestoringMap );
    if( aType == VISU::TFIELD ) {
      VISU::Result_var aResult = FindResult( VISU::GetSObject( aSObject ).in() );
      QString aMeshName =  aRestoringMap["myMeshName"];
      int anEntity = aRestoringMap["myEntityId"].toInt();
      QString aFieldName = aRestoringMap["myName"];

      VISU::ColoredPrs3dHolder::BasicInput anInput;
      anInput.myResult = aResult;
      anInput.myMeshName = CORBA::string_dup( (const char*)aMeshName.toLatin1() );
      anInput.myEntity = (VISU::Entity)anEntity;
      anInput.myFieldName = CORBA::string_dup( (const char*)aFieldName.toLatin1() );
      anInput.myTimeStampNumber = 1;

      QApplication::setOverrideCursor(Qt::WaitCursor);

      size_t isOk = VISU::CheckIsPossible( myType, anInput, true );

      QApplication::restoreOverrideCursor();

      return isOk > 0;
    }
  }
  return false;
}

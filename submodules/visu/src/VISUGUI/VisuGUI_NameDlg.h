// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_NameDlg.h
//  Author : Vadim SANDLER
//  Module : SALOME
//
#ifndef VisuGUI_NAMEDLG_H
#define VisuGUI_NAMEDLG_H

#include <QDialog>

class QLineEdit;
class QPushButton;

//=================================================================================
// class    : VisuGUI_NameDlg
// purpose  : Common <Rename> dialog box class
//=================================================================================
class VisuGUI_NameDlg : public QDialog
{ 
  Q_OBJECT

public:
  VisuGUI_NameDlg( QWidget* parent = 0 );
  ~VisuGUI_NameDlg();
    
  void            setName( const QString& name );
  QString         name();
    
  static QString  getName( QWidget* parent = 0, const QString& oldName = QString::null );

private:
  void keyPressEvent( QKeyEvent* e );
    
protected slots:
  void accept();
  void onHelp(); 
  
private:
  QPushButton*    myButtonOk;
  QPushButton*    myButtonCancel;
  QPushButton*    myButtonHelp;
  QLineEdit*      myLineEdit;
};

#endif // VisuGUI_NAMEDLG_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_FindPane.h
//  Author : Oleg Uvarov
//  Module : VISU
//
#ifndef VISUGUI_FINDPANE_H
#define VISUGUI_FINDPANE_H

#include <SVTK_Selection.h>

#include <QGroupBox>

class QComboBox;
class QLabel;
class QLineEdit;
class QListWidget;
class QToolButton;

class VISU_Actor;

typedef QPair<int,int> TFindId;
typedef QList<TFindId> TListFindId;

//! Find pane.
/*!
 * Used for filter and highlight mesh-elements by their scalar values.
 */
class VisuGUI_FindPane : public QGroupBox
{
  Q_OBJECT

public:
  VisuGUI_FindPane( QWidget* theParent );
  ~VisuGUI_FindPane();

public:
  void             setSelectionMode( const Selection_Mode );
  void             setActor( VISU_Actor* );

protected slots:
  void             onConditionChanged( int );
  void             onApply();

  void             onIdChanged();
  void             onPrevPage();
  void             onNextPage();

private:
  bool             isValid() const;
  void             clearIds();
  void             displayIds();

signals:
  void             idChanged( int, int );

private:
  QComboBox*       myConditionBox;
  QLineEdit*       myLeftValue;
  QLabel*          myDashLabel;
  QLineEdit*       myRightValue;

  QListWidget*     myIdsListWidget;
  QLabel*          myPageLabel;
  QToolButton*     myPrevBtn;
  QToolButton*     myNextBtn;

  int              myCurrentPage;
  TListFindId      myIdsList;

  Selection_Mode   mySelectionMode;
  VISU_Actor*      myActor;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef DIALOGBOX_CLIPPINGPLANEDLG_H
#define DIALOGBOX_CLIPPINGPLANEDLG_H

class VisuGUI;
class QLineEdit;
class QCheckBox;

class SalomeApp_DoubleSpinBox;
class vtkImplicitPlaneWidget;
class vtkCallbackCommand;
class vtkObject;
class SVTK_ViewWindow;

#include <QDialog>
#include <vtkSmartPointer.h>
#include "VisuGUI_ClippingPanel.h"
#include <SALOMEDSClient_SObject.hxx>


class VisuGUI_ClippingPlaneDlg: public QDialog
{
  Q_OBJECT
public:
  VisuGUI_ClippingPlaneDlg(VisuGUI* theModule);
  ~VisuGUI_ClippingPlaneDlg();

  void setOrigin(double theVal[3]);
  void setDirection(double theVal[3]);

  void setPlaneId(int theId);
  int planeId() const { return myPlaneId; }
  //void setPlaneObj(_PTR(SObject) thePlaneObj);
  //_PTR(SObject) getPlaneObj() const { return myPlaneObj; }

protected slots:
  virtual void accept();

private slots:
  void onHelp();
  void onValueChanged();

 private:
  //void setEmptyPlane(PlaneDef& thePlane) const;
  vtkImplicitPlaneWidget* createPreviewWidget();


  static void ProcessEvents(vtkObject* theObject, unsigned long theEvent,
                            void* theClientData, void* theCallData);

  VisuGUI*   myModule;
  SVTK_ViewWindow* myViewWindow;

  QLineEdit* myNameEdt;

  SalomeApp_DoubleSpinBox* myXOrigin;
  SalomeApp_DoubleSpinBox* myYOrigin;
  SalomeApp_DoubleSpinBox* myZOrigin;
 
  SalomeApp_DoubleSpinBox* myXDir;
  SalomeApp_DoubleSpinBox* myYDir;
  SalomeApp_DoubleSpinBox* myZDir;

  QCheckBox* myAutoApply;

  vtkImplicitPlaneWidget* myPreviewWidget;
  vtkSmartPointer<vtkCallbackCommand> myCallback;

  double myBounds[6];
  //_PTR(SObject) myPlaneObj;
  int myPlaneId;
};

#endif

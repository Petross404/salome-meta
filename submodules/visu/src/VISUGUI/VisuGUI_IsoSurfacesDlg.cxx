// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_IsoSurfacesDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#include "VisuGUI_IsoSurfacesDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_InputPane.h"

#include <VISU_ColoredPrs3dFactory.hh>

#include <LightApp_Application.h>

#include <SalomeApp_IntSpinBox.h>

#include <QtxColorButton.h>
#include <SUIT_Desktop.h>
#include <SUIT_Session.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ResourceMgr.h>

#include <limits>

#include <QLayout>
#include <QValidator>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QTabWidget>
#include <QKeyEvent>
#include <QColorDialog>
#include <QButtonGroup>
#include <QRadioButton>

VisuGUI_IsoSurfPane::VisuGUI_IsoSurfPane (QWidget* parent,
                                          VisuGUI_ScalarBarPane* theScalarPane)
  : QWidget(parent),
    myScalarPane(theScalarPane)
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  QFrame* TopGroup = new QFrame( this );
  aMainLayout->addWidget( TopGroup );
  
  TopGroup->setFrameStyle(QFrame::Box | QFrame::Sunken);
  TopGroup->setLineWidth(1);

  QGridLayout* TopGroupLayout = new QGridLayout(TopGroup);
  TopGroupLayout->setAlignment( Qt::AlignTop );
  TopGroupLayout->setSpacing( 6 );
  TopGroupLayout->setMargin( 11 );

  QGroupBox* aRangeBox = new QGroupBox( tr( "RANGE" ), this );
  QRadioButton* aUseScalarBarRange = new QRadioButton( tr( "USE_SCALAR_BAR_RANGE" ), aRangeBox );
  QRadioButton* aUseCustomRange = new QRadioButton( tr( "USE_CUSTOM_RANGE" ), aRangeBox );

  myRangeGrp = new QButtonGroup( aRangeBox );
  myRangeGrp->addButton( aUseScalarBarRange, ScalarBarRange );
  myRangeGrp->addButton( aUseCustomRange, CustomRange );
  aUseScalarBarRange->setChecked( true );

  connect( myRangeGrp, SIGNAL( buttonClicked( int ) ), this, SLOT( onRangeButtonClicked( int ) ) );

  QLabel* LabelMin = new QLabel( tr( "MIN_VALUE" ), aRangeBox );
  MinIso = new QLineEdit( aRangeBox );
  MinIso->setValidator( new QDoubleValidator( aRangeBox ) );
  MinIso->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  MinIso->setMinimumSize( 70, 0 );
  LabelMin->setBuddy( MinIso );

  QLabel* LabelMax = new QLabel( tr( "MAX_VALUE" ), aRangeBox );
  MaxIso = new QLineEdit( aRangeBox );
  MaxIso->setValidator( new QDoubleValidator( aRangeBox ) );
  MaxIso->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  MaxIso->setMinimumSize( 70, 0 );
  LabelMax->setBuddy( MaxIso );

  QPushButton* aUpdateBtn = new QPushButton( tr("Update scalar bar range with these values"), aRangeBox );
  aUpdateBtn->setEnabled( false );
  connect( aUpdateBtn, SIGNAL( clicked() ), this, SLOT(onCBUpdate() ) );
  connect( aUseCustomRange, SIGNAL( toggled( bool ) ), aUpdateBtn, SLOT( setEnabled( bool ) ) );

  QGridLayout* aRangeLayout = new QGridLayout( aRangeBox );
  aRangeLayout->setSpacing( 6 );
  aRangeLayout->setMargin( 11 );
  aRangeLayout->addWidget( aUseScalarBarRange, 0, 0 );
  aRangeLayout->addWidget( aUseCustomRange, 0, 1 );
  aRangeLayout->addWidget( LabelMin, 1, 0 );
  aRangeLayout->addWidget( MinIso, 1, 1 );
  aRangeLayout->addWidget( LabelMax, 2, 0 );
  aRangeLayout->addWidget( MaxIso, 2, 1 );
  aRangeLayout->addWidget( aUpdateBtn, 3, 0, 1, 2 );

  TopGroupLayout->addWidget( aRangeBox, 0, 0, 1, 2 );

  QLabel* LabelNbr = new QLabel (tr("NB_SURFACES"), TopGroup);
  TopGroupLayout->addWidget( LabelNbr, 1, 0 );
  NbrIso = new SalomeApp_IntSpinBox( TopGroup );
  NbrIso->setAcceptNames( false );
  NbrIso->setMaximum( 100 );
  NbrIso->setMinimum( 1 );
  NbrIso->setSingleStep( 1 );
  NbrIso->setValue( 1 );
  TopGroupLayout->addWidget( NbrIso, 1, 1 );

  myUseMagnitude = new QCheckBox(tr("MAGNITUDE_COLORING_CHK"), TopGroup);
  myUseMagnitude->setChecked(true);
  TopGroupLayout->addWidget( myUseMagnitude, 2, 0 );

  mySelColor = new QtxColorButton( TopGroup );
  mySelColor->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  TopGroupLayout->addWidget( mySelColor, 2, 1 );
  //connect( mySelColor, SIGNAL( clicked() ), this, SLOT( setColor() ) );
  connect( myUseMagnitude, SIGNAL( toggled(bool) ), mySelColor, SLOT( setDisabled(bool) ) );

  myUseLabels = new QCheckBox(tr("SHOW_VALUES_CHK"), TopGroup);
  myUseLabels->setChecked(false);
  TopGroupLayout->addWidget( myUseLabels, 3, 0 );
  myNbLabels = new SalomeApp_IntSpinBox( TopGroup );
  myNbLabels->setAcceptNames( false );
  myNbLabels->setMinimum(1);
  myNbLabels->setMaximum(100);
  myNbLabels->setSingleStep(1);
  myNbLabels->setEnabled(false);
  TopGroupLayout->addWidget( myNbLabels, 3, 1 );
  connect( myUseLabels, SIGNAL( toggled(bool) ), myNbLabels, SLOT( setEnabled(bool) ) );

  mySelColor->setEnabled( !myUseMagnitude->isChecked() );
}

void VisuGUI_IsoSurfPane::initFromPrsObject (VISU::IsoSurfaces_i* thePrs)
{
  NbrIso->setValue(thePrs->GetNbSurfaces());
  MinIso->setText(QString::number(thePrs->GetSubMin()));
  MaxIso->setText(QString::number(thePrs->GetSubMax()));

  int anId = thePrs->IsSubRangeFixed() ? ScalarBarRange : CustomRange;
  bool anIsSubRangeFixed = thePrs->IsSubRangeFixed();
  myRangeGrp->button( anId )->setChecked( true );
  onRangeButtonClicked( anId );

  myUseMagnitude->setChecked(thePrs->IsColored());
  SALOMEDS::Color anOldColor = thePrs->GetColor();
  QColor aColor = QColor(int(255*anOldColor.R),int(255*anOldColor.G),int(255*anOldColor.B));
  setColor(aColor);

  myUseLabels->setChecked(thePrs->IsLabeled());
  myNbLabels->setValue(thePrs->GetNbLabels());
  mySelColor->setEnabled( !myUseMagnitude->isChecked() );
}

int VisuGUI_IsoSurfPane::storeToPrsObject (VISU::IsoSurfaces_i* thePrs)
{
  thePrs->SetNbSurfaces(NbrIso->value());

  if( myRangeGrp->checkedId() == ScalarBarRange )
  {
    thePrs->SetSubRange( myScalarPane->getMin(), myScalarPane->getMax() );
    thePrs->SetSubRangeFixed( true );
  }
  else // CustomRange
  {
    thePrs->SetSubRange( MinIso->text().toDouble(), MaxIso->text().toDouble() );
    thePrs->SetSubRangeFixed( false );
  }

  thePrs->ShowLabels(myUseLabels->isChecked(), myNbLabels->value());
  thePrs->ShowColored(myUseMagnitude->isChecked());
  if(!thePrs->IsColored()){
    QColor aQColor = color();
    SALOMEDS::Color aColor;
    aColor.R = aQColor.red()/255.;
    aColor.G = aQColor.green()/255.;
    aColor.B = aQColor.blue()/255.;
    thePrs->SetColor(aColor);
  }
  return 1;
}

void VisuGUI_IsoSurfPane::onRangeButtonClicked( int theId )
{
  bool isCustomRange = theId == 1;
  MinIso->setEnabled( isCustomRange );
  MaxIso->setEnabled( isCustomRange );
}

void VisuGUI_IsoSurfPane::onCBUpdate()
{
  myScalarPane->setRange(MinIso->text().toDouble(), MaxIso->text().toDouble(), true);
}

bool VisuGUI_IsoSurfPane::check()
{
  if (MinIso->text().toDouble() >= MaxIso->text().toDouble()) {
    MESSAGE(tr("MSG_MINMAX_VALUES").toLatin1().data());
    SUIT_MessageBox::warning( this,tr("WRN_VISU"),
                              tr("MSG_MINMAX_VALUES"),
                              tr("BUT_OK"));
    return false;
  }
  return true;
}

void VisuGUI_IsoSurfPane::setColor()
{
  QColor cnew = QColorDialog::getColor( color(), this );
  if ( cnew.isValid() )
    setColor(cnew);
}

void VisuGUI_IsoSurfPane::setColor( const QColor& theColor)
{
  /*  QPalette pal = mySelColor->palette();
  pal.setColor(mySelColor->backgroundRole(), theColor);
  mySelColor->setPalette(pal);*/
  mySelColor->setColor( theColor );
}

QColor VisuGUI_IsoSurfPane::color() const
{
  return mySelColor->color();
}


/*!
  Constructor
*/
VisuGUI_IsoSurfacesDlg::VisuGUI_IsoSurfacesDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule)
{
  setWindowTitle(tr("DEFINE_ISOSURFACES"));
  setSizeGripEnabled( TRUE );

  QVBoxLayout* TopLayout = new QVBoxLayout(this);
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin(11);

  myTabBox = new QTabWidget(this);
  myIsoPane = new  VisuGUI_IsoSurfPane(this, GetScalarPane());
  if ( myIsoPane->layout() )
    myIsoPane->layout()->setMargin( 5 );
  myTabBox->addTab(myIsoPane, tr("Iso Surface"));
  myInputPane = new VisuGUI_InputPane(VISU::TISOSURFACES, theModule, this);
  myTabBox->addTab(GetScalarPane(), tr("Scalar Bar"));
  myTabBox->addTab(myInputPane, tr("Input"));

  TopLayout->addWidget(myTabBox);

  QGroupBox* GroupButtons = new QGroupBox( this );
  GroupButtons->setGeometry( QRect( 10, 10, 281, 48 ) );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setSpacing( 0 );
  //GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr( "BUT_OK" ), GroupButtons );
  buttonOk->setAutoDefault( TRUE );
  buttonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonOk, 0, 0 );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );
  QPushButton* buttonCancel = new QPushButton( tr( "BUT_CANCEL" ) , GroupButtons );
  buttonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonCancel, 0, 2 );
  QPushButton* buttonHelp = new QPushButton( tr( "BUT_HELP" ) , GroupButtons );
  buttonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonHelp, 0, 3 );

  TopLayout->addWidget(GroupButtons);

  // signals and slots connections
  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
}

VisuGUI_IsoSurfacesDlg::~VisuGUI_IsoSurfacesDlg()
{}

void VisuGUI_IsoSurfacesDlg::accept()
{
  if ( myIsoPane->check() )
    VisuGUI_ScalarBarBaseDlg::accept();
}

void VisuGUI_IsoSurfacesDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                                bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TISOSURFACES>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  myIsoPane->initFromPrsObject(myPrsCopy);

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

int VisuGUI_IsoSurfacesDlg::storeToPrsObject(VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );
  anIsOk &= myIsoPane->storeToPrsObject( myPrsCopy );

  VISU::TSameAsFactory<VISU::TISOSURFACES>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

QString VisuGUI_IsoSurfacesDlg::GetContextHelpFilePath()
{
  return "iso_surfaces_page.html";
}

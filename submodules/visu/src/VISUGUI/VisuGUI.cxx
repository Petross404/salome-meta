// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI.cxx
//  Author :
//  Module : VISU

#include <Standard_math.hxx>  // E.A. must be included before Python.h to fix compilation on windows
#ifdef HAVE_FINITE
#undef HAVE_FINITE            // VSR: avoid compilation warning on Linux : "HAVE_FINITE" redefined
#endif
#include "Python.h"

#include "VisuGUI.h"

// STL Includes
#include <exception>
#include <typeinfo>
#include <vector>

// QT Includes
#include <QList>
#include <QVector>
#include <QColorDialog>
#include <QDateTime>
#include <QMenu>

// VTK Includes
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkActorCollection.h>
#include <vtkTimerLog.h>

// SALOME Includes
#include "SALOME_LifeCycleCORBA.hxx"

#include "SUIT_ResourceMgr.h"
#include "SUIT_MessageBox.h"

#include "SUIT_ViewWindow.h"
#include "SUIT_ViewManager.h"
#include "SUIT_Desktop.h"
#include "SUIT_DataBrowser.h"
#include "SUIT_OverrideCursor.h"

#include "CAM_Module.h"

#include "SVTK_ViewWindow.h"
#include "SVTK_ViewModel.h"
#include "SVTK_Functor.h"
//#include "SVTK_MainWindow.h"

#include "VTKViewer_Algorithm.h"
#include "VTKViewer_MarkerDlg.h"

#include "SPlot2d_ViewModel.h"
#include "VisuGUI_SetupPlot2dDlg.h"
#include "Plot2d_SetupCurveDlg.h"
#include "Plot2d_ViewManager.h"
#include "Plot2d_SetupCurveScaleDlg.h"

#include "QtxPagePrefMgr.h"
#include "OB_Browser.h"

#include "SALOME_ListIteratorOfListIO.hxx"

#include "SalomeApp_Application.h"
#include "SalomeApp_DataModel.h"
#include "SalomeApp_Study.h"
#include "SalomeApp_DataObject.h"
#include "SalomeApp_CheckFileDlg.h"

#include "LightApp_SelectionMgr.h"
#include "LightApp_Selection.h"
#include "LightApp_Preferences.h"
#include "LightApp_DataOwner.h"

#include "QtxAction.h"
#include "QtxFontEdit.h"

#include "VISUConfig.hh"
#include "VISU_Gen_i.hh"
#include "VISU_Mesh_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_PointMap3d_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_View_i.hh"
#include "VISU_ViewManager_i.hh"
#include "VISU_Plot3D_i.hh"

#include "VISU_Actor.h"

#include "VisuGUI_Prs3dTools.h"
#include "VisuGUI_Tools.h"

#include "VisuGUI_SelectionPanel.h"
#include "VisuGUI_FeatureEdgesPanel.h"
#include "VisuGUI_NameDlg.h"
#include "VisuGUI_CursorDlg.h"
#include "VisuGUI_Selection.h"
#include "VisuGUI_TimeAnimation.h"
#include "VisuGUI_EvolutionDlg.h"
#include "VisuGUI_EditContainerDlg.h"
#include "VisuGUI_ClippingDlg.h"
#include "VisuGUI_Plot3DDlg.h"
#include "VisuGUI_OffsetDlg.h"
#include "VisuGUI_Displayer.h"
#include "VisuGUI_BuildProgressDlg.h"
#include "VisuGUI_TransparencyDlg.h"
#include "VisuGUI_ShrinkFactorDlg.h"
//#include "VisuGUI_CacheDlg.h"
#include "VisuGUI_Table3dDlg.h"
#include "VisuGUI_ClippingPanel.h"
#include "VisuGUI_FilterScalarsDlg.h"

#include "VISU_ScalarMap_i.hh"
#include "VisuGUI_ScalarBarDlg.h"

#include "VISU_DeformedShape_i.hh"
#include "VisuGUI_DeformedShapeDlg.h"

#include "VISU_DeformedShapeAndScalarMap_i.hh"
#include "VisuGUI_DeformedShapeAndScalarMapDlg.h"

#include "VISU_IsoSurfaces_i.hh"
#include "VisuGUI_IsoSurfacesDlg.h"

#include "VISU_CutPlanes_i.hh"
#include "VisuGUI_CutPlanesDlg.h"

#include "VISU_CutLines_i.hh"
#include "VisuGUI_CutLinesDlg.h"

#include "VISU_CutSegment_i.hh"
#include "VisuGUI_CutSegmentDlg.h"

#include "VISU_StreamLines_i.hh"
#include "VisuGUI_StreamLinesDlg.h"

#include "VISU_Vectors_i.hh"
#include "VisuGUI_VectorsDlg.h"

#include "VISU_TableDlg.h"
#include "VisuGUI_FileInfoDlg.h"

#ifdef WITH_MEDGEN
#include "SALOMEconfig.h"
#include CORBA_SERVER_HEADER(MED_Gen)
#endif // WITH_MEDGEN

#include "utilities.h"

#include "VisuGUI_ActionsDef.h"

#include "VisuGUI_Timer.h"

//#include "VVTK_ViewModel.h"

#include "VisuGUI_Slider.h"
#include "VisuGUI_Sweep.h"
#include <VISU_ScalarMapAct.h>
#include <VISU_GaussPtsAct.h>
#include "VisuGUI_ValuesLabelingDlg.h"

using namespace VISU;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

#ifdef WIN32
#define usleep(A) _sleep(A/1000)
#endif

#include <vtkMatplotlibMathTextUtilities.h>

class MatplotlibMathTextUtilities : public vtkMatplotlibMathTextUtilities
{
public:
  static void Disable()
  {
    vtkMatplotlibMathTextUtilities::MPLMathTextAvailable = vtkMatplotlibMathTextUtilities::UNAVAILABLE;
  }

private:
  MatplotlibMathTextUtilities() {}
  ~MatplotlibMathTextUtilities() {}
};

//////////////////////////////////////////////////
// Class: VisuGUI
//////////////////////////////////////////////////

VisuGUI
::VisuGUI():
  SalomeApp_Module( "VISU" ),
  myDisplayer( 0 ),
  mySlider( NULL ),
  mySweep( NULL )
{
}

VisuGUI
::~VisuGUI()
{
  if( myDisplayer ) {
    delete myDisplayer;
    myDisplayer = 0;
  }

  if( mySlider ) {
    delete mySlider;
    mySlider = 0;
  }

  if( mySweep ) {
    delete mySweep;
    mySweep = 0;
  }
}

void
VisuGUI
::OnImportFromSauvFile()
{
  if(MYDEBUG) MESSAGE("VisuGUI::OnImportFromSauvFile()");

  if ( CheckLock(GetCStudy(GetAppStudy(this)),GetDesktop(this)) )
    return;

  SUIT_ResourceMgr* aResourceMgr = GetResourceMgr();

  // Get file name(s)
  QStringList aFilter;
  aFilter.append( tr( "FLT_SAUV_FILES" ) );
  aFilter.append( tr( "FLT_ALL_FILES" ) );

  bool toUseBuildProgress = aResourceMgr->booleanValue("VISU", "use_build_progress", false);

  SalomeApp_CheckFileDlg fd( GetDesktop(this), true, tr("USE_BUILD_PROGRESS") );
  fd.setWindowTitle( tr( "IMPORT_FROM_FILE" ) );
  fd.setFileMode( SalomeApp_CheckFileDlg::ExistingFiles );
  fd.setFilters( aFilter );
  fd.SetChecked( toUseBuildProgress );
  if ( SalomeApp_CheckFileDlg::getLastVisitedPath().isEmpty() )
    fd.setDirectory( QDir::currentPath() );

  QStringList fileNames;
  if ( !fd.exec() )
    return; // cancelled, return

  fileNames = fd.selectedFiles();
  toUseBuildProgress = fd.IsChecked();

  if ( fileNames.count() == 0 )
    return; // nothing selected, return

  SUIT_OverrideCursor wc;
  QStringList errors;

  for ( QStringList::ConstIterator it = fileNames.begin(); it != fileNames.end(); ++it ) {
    QFileInfo aFileInfo( *it );

    if ( !aFileInfo.exists() ) {
      // file not exist
      errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).
                     arg( tr( "ERR_FILE_NOT_EXISTS" ) ) );
    }
    else {
      std::string file_in = aFileInfo.filePath().toLatin1().constData();
      std::string file_tmp = file_in + ".med";
#ifdef WNT
      std::string cmd = "%PYTHONBIN% ";
#else
      std::string cmd = "python ";
#endif
      cmd += "-c \"";
      cmd += "from medutilities import convert ; convert(r'" + file_in + "', 'GIBI', 'MED', 1, r'" + file_tmp + "')";
      cmd += "\"";
      system(cmd.c_str());
      QString fff(file_tmp.c_str());
      aFileInfo = fff;
      try {
        if ( !toUseBuildProgress ) {
          // build progress is not used, use build flags from settings
          bool toBuildFields = aResourceMgr->booleanValue( "VISU", "build_fields",     true );
          bool toBuildMinMax = aResourceMgr->booleanValue( "VISU", "build_min_max",    true );
          bool toBuildGroups = aResourceMgr->booleanValue( "VISU", "build_groups",     true );
          bool toBuildAll    = aResourceMgr->booleanValue( "VISU", "full_med_loading", false );
          bool toBuildAtOnce = aResourceMgr->booleanValue( "VISU", "build_at_once",    false );

          QString anInfo( tr( "IMPORT_FROM_FILE" ) + " " + aFileInfo.filePath() + " ... " );
          application()->putInfo( anInfo );

          VISU::Result_var aResult = GetVisuGen( this )->CreateResult( aFileInfo.filePath().toLatin1().constData() );

          if ( CORBA::is_nil( aResult.in() ) ) {
            errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).
                           arg( tr( "ERR_ERROR_IN_THE_FILE" ) ) );
          }
          else {
            aResult->SetBuildFields( toBuildFields, toBuildMinMax );
            aResult->SetBuildGroups( toBuildGroups );
            aResult->Build( toBuildAll, toBuildAtOnce );
            application()->putInfo( anInfo + tr("INF_DONE") );
          }
        } // if ( !toUseBuildProgress )
        else {
          // use build progress
          wc.suspend();
          VisuGUI_BuildProgressDlg* aBuildProgressDlg = new VisuGUI_BuildProgressDlg( GetDesktop(this) );
          aBuildProgressDlg->setFileName( aFileInfo.filePath() );
          aBuildProgressDlg->setGenerator( GetVisuGen(this) );
          aBuildProgressDlg->exec();
          wc.resume();
        }
      } // try
      catch(...) {
        errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).arg( tr( "UNKNOWN_IMPORT_ERROR" ) ) );
      }
    } // else // if ( !aFileInfo.exists() )
  } // for (...)

  if ( errors.count() > 0 ) {
    SUIT_MessageBox::critical( GetDesktop(this),
                               tr("ERR_ERROR"),
                               tr("IMPORT_ERRORS" ) + "\n" + errors.join( "\n" ) );
  }
  else {
    UpdateObjBrowser(this);
  }
}

void
VisuGUI
::OnImportFromFile()
{
  if(MYDEBUG) MESSAGE("VisuGUI::OnImportFromFile()");

  if ( CheckLock(GetCStudy(GetAppStudy(this)),GetDesktop(this)) )
    return;

  SUIT_ResourceMgr* aResourceMgr = GetResourceMgr();

  // Get file name(s)
  QStringList aFilter;
  aFilter.append( tr( "FLT_MED_FILES" ) );
  aFilter.append( tr( "FLT_DISTRIBUTED_MED_FILES" ) );
  aFilter.append( tr( "FLT_ALL_FILES" ) );

  bool toUseBuildProgress = aResourceMgr->booleanValue("VISU", "use_build_progress", false);

  SalomeApp_CheckFileDlg fd( GetDesktop(this), true, tr("USE_BUILD_PROGRESS") );
  fd.setWindowTitle( tr( "IMPORT_FROM_FILE" ) );
  fd.setFileMode( SalomeApp_CheckFileDlg::ExistingFiles );
  fd.setFilters( aFilter );
  fd.SetChecked( toUseBuildProgress );
  if ( SalomeApp_CheckFileDlg::getLastVisitedPath().isEmpty() )
    fd.setDirectory( QDir::currentPath() );

  QStringList fileNames;
  if ( !fd.exec() )
    return; // cancelled, return

  fileNames = fd.selectedFiles();
  toUseBuildProgress = fd.IsChecked();

  if ( fileNames.count() == 0 )
    return; // nothing selected, return

  SUIT_OverrideCursor wc;
  QStringList errors;
  QStringList anEntryList;

  for ( QStringList::ConstIterator it = fileNames.begin(); it != fileNames.end(); ++it ) {
    QFileInfo aFileInfo( *it );

    if ( !aFileInfo.exists() ) {
      // file not exist
      errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).
                     arg( tr( "ERR_FILE_NOT_EXISTS" ) ) );
    }
    else {
      try {
        if ( !toUseBuildProgress ) {
          // build progress is not used, use build flags from settings
          bool toBuildFields = aResourceMgr->booleanValue( "VISU", "build_fields",     true );
          bool toBuildMinMax = aResourceMgr->booleanValue( "VISU", "build_min_max",    true );
          bool toBuildGroups = aResourceMgr->booleanValue( "VISU", "build_groups",     true );
          bool toBuildAll    = aResourceMgr->booleanValue( "VISU", "full_med_loading", false );
          bool toBuildAtOnce = aResourceMgr->booleanValue( "VISU", "build_at_once",    false );

          QString anInfo( tr( "IMPORT_FROM_FILE" ) + " " + aFileInfo.filePath() + " ... " );
          application()->putInfo( anInfo );

          VISU::Result_var aResult = GetVisuGen( this )->CreateResult( aFileInfo.filePath().toLatin1().constData() );

          if ( CORBA::is_nil( aResult.in() ) ) {
            errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).
                           arg( tr( "ERR_ERROR_IN_THE_FILE" ) ) );
          }
          else {
            aResult->SetBuildFields( toBuildFields, toBuildMinMax );
            aResult->SetBuildGroups( toBuildGroups );
            aResult->Build( toBuildAll, toBuildAtOnce );
            if( VISU::Result_i* aCResult = dynamic_cast<VISU::Result_i*>( VISU::GetServant( aResult ).in() ) )
              anEntryList.append( aCResult->GetEntry().c_str() );
            application()->putInfo( anInfo + tr("INF_DONE") );
          }
        } // if ( !toUseBuildProgress )
        else {
          // use build progress
          wc.suspend();
          VisuGUI_BuildProgressDlg* aBuildProgressDlg = new VisuGUI_BuildProgressDlg( GetDesktop(this) );
          aBuildProgressDlg->setFileName( aFileInfo.filePath() );
          aBuildProgressDlg->setGenerator( GetVisuGen(this) );
          aBuildProgressDlg->exec();
          wc.resume();
        }
      } // try
      catch(...) {
        errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).arg( tr( "UNKNOWN_IMPORT_ERROR" ) ) );
      }
    } // else // if ( !aFileInfo.exists() )
  } // for (...)

  if ( errors.count() > 0 ) {
    SUIT_MessageBox::critical( GetDesktop(this),
                               tr("ERR_ERROR"),
                               tr("IMPORT_ERRORS" ) + "\n" + errors.join( "\n" ) );
  }
  else {
    UpdateObjBrowser(this);
    if( getApp() )
      getApp()->browseObjects( anEntryList );
  }
}

void
VisuGUI
::OnImportTableFromFile()
{
  if(MYDEBUG) MESSAGE("VisuGUI::OnImportTableFromFile()");
  if ( CheckLock(GetCStudy(GetAppStudy(this)),GetDesktop(this)) )
    return;

  QStringList aFilter;
  aFilter.append( tr("FLT_TABLE_FILES") );
  aFilter.append( tr("FLT_ALL_FILES") );

  //QString anInitialPath = "";
  //if ( SUIT_FileDlg::getLastVisitedPath().isEmpty() )
  //  anInitialPath = QDir::currentPath();

  //QStringList aFiles = SUIT_FileDlg::getOpenFileNames(GetDesktop(this),
  //                                                    anInitialPath,
  //                                                    aFilter,
  //                                                    tr("TLT_IMPORT_TABLE"));

  SUIT_ResourceMgr* aResourceMgr = GetResourceMgr();
  bool aFirstStrAsTitle = aResourceMgr->booleanValue("VISU", "first_str_as_title", false);

  SalomeApp_CheckFileDlg fd( GetDesktop(this), true, tr("FIRST_STR_AS_TITLE") );
  fd.setWindowTitle( tr( "IMPORT_FROM_FILE" ) );
  fd.setFileMode( SalomeApp_CheckFileDlg::ExistingFiles );
  fd.setFilters( aFilter );
  fd.SetChecked( aFirstStrAsTitle );
  if ( SalomeApp_CheckFileDlg::getLastVisitedPath().isEmpty() )
    fd.setDirectory( QDir::currentPath() );

  QStringList aFiles;
  if ( !fd.exec() )
    return; // cancelled, return
  aFiles = fd.selectedFiles();
  aFirstStrAsTitle = fd.IsChecked();

  if ( aFiles.count() == 0 )
    return; // nothing selected

  QStringList errors;
  QStringList anEntryList;
  for ( QStringList::ConstIterator it = aFiles.begin(); it != aFiles.end(); ++it ) {
    QFileInfo aFileInfo( *it );

    if ( !aFileInfo.exists() ) {
      errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).
                     arg( tr( "ERR_FILE_NOT_EXISTS" ) ) );
    }
    else {
      try {
        QString anInfo( tr("TLT_IMPORT_TABLE") + " " + aFileInfo.filePath() + " ..." );
        application()->putInfo( anInfo );

        SALOMEDS::SObject_var anObject =
          GetVisuGen(this)->ImportTables(aFileInfo.filePath().toLatin1().constData(),
                                         aFirstStrAsTitle);

        if (CORBA::is_nil(anObject.in())) {
          errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).
                         arg( tr( "ERR_ERROR_IN_THE_FILE" ) ) );
        }
        else {
          anEntryList.append( anObject->GetID() );
          application()->putInfo( anInfo + tr("INF_DONE") );
        }
      }
      catch(...) {
        errors.append( QString( "%1 :\n\t%2" ).arg( aFileInfo.filePath() ).arg( tr( "UNSUPPORTED_FILE_FORMAT" ) ) );
      }
    }
  }

  if ( errors.count() > 0 ) {
    SUIT_MessageBox::critical( GetDesktop(this),
                               tr("ERR_ERROR"),
                               tr("IMPORT_ERRORS" ) + "\n" + errors.join( "\n" ) );
  }
  else {
    UpdateObjBrowser(this);
    if( getApp() )
      getApp()->browseObjects( anEntryList );
  }
}

void
VisuGUI
::OnExportTableToFile()
{
  if(MYDEBUG) MESSAGE("VisuGUI::OnExportTableToFile()");

  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  if (aListIO.Extent() > 0) {
    const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();
    if (anIO->hasEntry()) {
      _PTR(SObject) aSObj = aCStudy->FindObjectID(anIO->getEntry());
      if (!aSObj) return;

      // find table attribute
      bool isTOR = true;
      _PTR(GenericAttribute) anAttr;

      if (!aSObj->FindAttribute(anAttr, "AttributeTableOfReal")) {
        isTOR = false;
        if (!aSObj->FindAttribute(anAttr, "AttributeTableOfInteger")) {
          // if the current SObject is a table title
          // we take a father contaning the table
          aSObj = aSObj->GetFather();

          if (aSObj->FindAttribute(anAttr, "AttributeTableOfReal")) {
            isTOR = true;
          } else {
            if (!aSObj->FindAttribute(anAttr, "AttributeTableOfInteger")) {
              return;
            }
          }
        }
      }

      // get table title in order to have default name of the file
      QString aTitle;
      if (isTOR) {
        _PTR(AttributeTableOfReal) aTabAttr (anAttr);
        if (!aTabAttr) return;
        aTitle = aTabAttr->GetTitle().c_str();
      } else {
        _PTR(AttributeTableOfInteger) aTabAttr (anAttr);
        if (!aTabAttr) return;
        aTitle = aTabAttr->GetTitle().c_str();
      }
      aTitle.simplified();
      aTitle = aTitle.replace(QRegExp(" "), "_").replace(QRegExp("\\*"), "" );

      // get default path for the file
      SUIT_ResourceMgr* aResourceMgr = GetResourceMgr();
      QString aDir = aResourceMgr->stringValue("VISU","OutputDir","");
      if (aDir.isEmpty()) {
        aDir = aResourceMgr->stringValue("VISU","InputDir","");
        if (aDir.isEmpty()) {
          aDir = getenv("CSF_PATHData");
        }
      }
      QString aPath = Qtx::addSlash(aDir) + aTitle;

      // get name for the file
      QStringList aFilter;
      aFilter.append("Table Files (*.txt *.tab)");
      aFilter.append("CSV Table Files (*.csv)");

      QFileInfo aFileInfo =
        SUIT_FileDlg::getFileName(GetDesktop(this),
                                  aPath,
                                  aFilter,
                                  tr("MEN_EXPORT_TABLE"), // "Export Table To File"
                                  false);

      // export
      QString aFile = aFileInfo.filePath();
      if (!aFile.isEmpty()) {
        application()->putInfo(tr("MEN_EXPORT_TABLE") + " " + aFile + " ...");
        aDir = Qtx::dir(aFile, true);
        aResourceMgr->setValue("VISU", "OutputDir", aDir);

        try {
          GetVisuGen(this)->ExportTableToFile(GetSObject(aSObj), (const char*)aFile.toLatin1());
          application()->putInfo(aFile + " " + tr("INF_DONE"));
        } catch(std::exception& exc) {
          INFOS(exc.what());
          SUIT_MessageBox::warning(GetDesktop(this),
                                   tr("WRN_VISU"),
                                   tr("ERR_ERROR_DURING_EXPORT") + " " + tr(exc.what()) );

        } catch(...) {
          INFOS((const char*)tr("ERR_ERROR_DURING_EXPORT").toLatin1());
          SUIT_MessageBox::warning( GetDesktop(this),
                                    tr("WRN_VISU"),
                                    tr("ERR_ERROR_DURING_EXPORT") );

        }
      }
    }
  }
}

void
VisuGUI
::OnImportMedField()
{
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;
  SALOMEDS::Study_var aStudy = GetDSStudy(aCStudy);

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  QStringList anEntryList;
  SALOME_ListIteratorOfListIO It (aListIO);
  QApplication::setOverrideCursor(Qt::WaitCursor);
  for (; It.More(); It.Next()) {
    Handle(SALOME_InteractiveObject) anIO = It.Value();
    SALOMEDS::SObject_var aSObject = aStudy->FindObjectID(anIO->getEntry());
    if (!aSObject->_is_nil()) {

      // try to load a component data from an opened (presumably) study
      // The same in SUPERVISION: NPAL14522
      {
        SALOMEDS::SComponent_ptr aSComp = aSObject->GetFatherComponent();
        CORBA::String_var aCompIOR;
        if (!aSComp->ComponentIOR(aCompIOR)) { // not loaded
          CORBA::String_var aCompDataType = aSComp->ComponentDataType();

          // obtain a driver by a component data type
          // like it is done in SALOMEDS_DriverFactory_i::GetDriverByType
          SALOMEDS::Driver_var anEngine = SALOMEDS::Driver::_nil();
          SALOME_LifeCycleCORBA * LCC = getApp()->lcc();
          try {
            CORBA::Object_var
              anEngineObj = LCC->FindOrLoad_Component("FactoryServer", aCompDataType);
            if (CORBA::is_nil(anEngineObj))
              anEngineObj = LCC->FindOrLoad_Component("FactoryServerPy", aCompDataType);

            if (!CORBA::is_nil(anEngineObj))
              anEngine = SALOMEDS::Driver::_narrow(anEngineObj);

            if (!CORBA::is_nil(anEngine)) {
              // try to load
              SALOMEDS::StudyBuilder_ptr aStudyBuilder = aStudy->NewBuilder();
              aStudyBuilder->LoadWith(aSComp, anEngine);
            }
          }
          catch (...) {
          }
        }
      }

      VISU::Result_var aResult;
      CORBA::Object_var anObject = VISU::SObjectToObject(aSObject);
      if (!CORBA::is_nil(anObject)) {
        SALOME_MED::MED_var aMED = SALOME_MED::MED::_narrow(anObject);
        if (!CORBA::is_nil(aMED.in()))
          aResult = GetVisuGen(this)->ImportMed(aSObject);
        SALOME_MED::FIELD_var aField = SALOME_MED::FIELD::_narrow(anObject);
        if (!CORBA::is_nil(aField.in()))
          aResult = GetVisuGen(this)->ImportMedField(aField);
      } else {
        SALOMEDS::SObject_var aSFather = aSObject->GetFather();
        SALOMEDS::GenericAttribute_var anAttr;
        aSFather->FindAttribute(anAttr, "AttributeName");
        SALOMEDS::AttributeName_var aName = SALOMEDS::AttributeName::_narrow(anAttr);
        CORBA::String_var aValue = aName->Value();
        if (strcmp(aValue.in(), "MEDFIELD") == 0)
          aResult = GetVisuGen(this)->ImportMed(aSObject);
      }

      if( !CORBA::is_nil( aResult ) )
        if( VISU::Result_i* aCResult = dynamic_cast<VISU::Result_i*>( VISU::GetServant( aResult ).in() ) )
          anEntryList.append( aCResult->GetEntry().c_str() );
    }
  }
  UpdateObjBrowser(this, true);
  if( getApp() )
    getApp()->browseObjects( anEntryList );
  QApplication::restoreOverrideCursor();
}

void VisuGUI::OnLoadComponentData()
{
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;
  SALOMEDS::Study_var aStudy = GetDSStudy(aCStudy);

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  SALOME_ListIteratorOfListIO It (aListIO);
  QApplication::setOverrideCursor(Qt::WaitCursor);
  for (; It.More(); It.Next()) {
    Handle(SALOME_InteractiveObject) anIO = It.Value();
    SALOMEDS::SObject_var aSObject = aStudy->FindObjectID(anIO->getEntry());
    if (!aSObject->_is_nil()) {

      // try to load a component data from an opened (presumably) study
      // The same in SUPERVISION: NPAL14522
      {
        SALOMEDS::SComponent_ptr aSComp = aSObject->GetFatherComponent();
        CORBA::String_var aCompIOR;
        if (!aSComp->ComponentIOR(aCompIOR)) { // not loaded
          CORBA::String_var aCompDataType = aSComp->ComponentDataType();

          QString anInfo ("Loading ");
          anInfo = anInfo + aCompDataType.in() + " Component Data ...";
          application()->putInfo( anInfo );

          // obtain a driver by a component data type
          // like it is done in SALOMEDS_DriverFactory_i::GetDriverByType
          SALOMEDS::Driver_var anEngine = SALOMEDS::Driver::_nil();
          SALOME_LifeCycleCORBA * LCC = getApp()->lcc();
          try {
            CORBA::Object_var
              anEngineObj = LCC->FindOrLoad_Component("FactoryServer", aCompDataType);
            if (CORBA::is_nil(anEngineObj))
              anEngineObj = LCC->FindOrLoad_Component("FactoryServerPy", aCompDataType);

            if (!CORBA::is_nil(anEngineObj))
              anEngine = SALOMEDS::Driver::_narrow(anEngineObj);

            if (!CORBA::is_nil(anEngine)) {
              // try to load
              SALOMEDS::StudyBuilder_ptr aStudyBuilder = aStudy->NewBuilder();
              aStudyBuilder->LoadWith(aSComp, anEngine);
            }

            application()->putInfo( anInfo + tr("INF_DONE"));
          }
          catch (...) {
            application()->putInfo( anInfo + tr("INF_FAILED"));
          }
        }
      }
    }
  }
  QApplication::restoreOverrideCursor();
}

void
CreateCurves( SalomeApp_Module* theModule,
              VISU::CutLinesBase_i* thePrs,
              QDialog* theDlg,
              const bool theCreate = true )
{
  if ( !thePrs )
    return;

  // temporary code, to be revised (VisuGUI_CutLinesDlg and VisuGUI_CutSegmentDlg classes
  // should have common intermediate interface with at least two methods called below)
  bool isGenerateTable = false;
  bool isGenerateCurves = false;
  if ( VisuGUI_CutLinesDlg* aCutDlg = dynamic_cast<VisuGUI_CutLinesDlg*>( theDlg ) ) {
    isGenerateTable = aCutDlg->isGenerateTable();
    isGenerateCurves = aCutDlg->isGenerateCurves();
  }
  else if ( VisuGUI_CutSegmentDlg* aCutDlg = dynamic_cast<VisuGUI_CutSegmentDlg*>( theDlg ) ) {
    isGenerateTable = aCutDlg->isGenerateTable();
    isGenerateCurves = aCutDlg->isGenerateCurves();
  }
  else
    return;

  _PTR(Study)   aStudy = GetCStudy( GetAppStudy( theModule ) );
  _PTR(SObject) aSObject = aStudy->FindObjectID( thePrs->GetEntry() );

  if ( !theCreate && aSObject ) {
    // Remove old Table
    _PTR(StudyBuilder) aBuilder = aStudy->NewBuilder();
    _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aSObject);
    for (; aIter->More(); aIter->Next()) {
      _PTR(SObject) aTblObj = aIter->Value();
      if( aTblObj ) {
        _PTR(GenericAttribute) anAttr;
        if (aTblObj->FindAttribute( anAttr, "AttributeName" ) ) {
          _PTR(ChildIterator) aChildIter = aStudy->NewChildIterator( aTblObj );
          SalomeApp_Study* salome_study = GetAppStudy( theModule );
          for( aChildIter->InitEx( false ); aChildIter->More(); aChildIter->Next() ){
            salome_study->deleteReferencesTo( aChildIter->Value() );
            _PTR(SObject) aSO = aChildIter->Value();
            if(aSO){
              Storable::TRestoringMap aRestoringMap = Storable::GetStorableMap(aSO);
              if (aRestoringMap["myComment"] == "CURVE") {
                CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(aSO);
                if(!CORBA::is_nil(aCORBAObject)){
                  PortableServer::ServantBase_var aServant = VISU::GetServant(aCORBAObject);
                  if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aServant.in())){
                    PlotRemoveCurve(theModule, aCurve);
                  }
                }
              }
            }

          }

          aBuilder->RemoveObjectWithChildren( aTblObj ); // We should have only one child

          //UpdateObjBrowser(theModule,true,aTblObj);
          break;
        }
      }
    }
  }

  if ( isGenerateTable ) {
    GetVisuGen( theModule )->CreateTable( thePrs->GetEntry().c_str() );
    if ( isGenerateCurves ) {
      if ( aSObject ) {
        _PTR(StudyBuilder) aBuilder = aStudy->NewBuilder();
        _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aSObject);
        for (; aIter->More(); aIter->Next()) {
          _PTR(SObject) aTblObj = aIter->Value();
          if ( aTblObj ) {
            _PTR(GenericAttribute) anAttr;
            if ( aTblObj->FindAttribute( anAttr, "AttributeName" ) ) {
              CreatePlot( theModule, aTblObj );
            }
          }
        }
      }
    }
  }

  if (!theCreate && aSObject) {
    UpdateObjBrowser(theModule);
  }
}

void
VisuGUI
::OnCreateMesh()
{
  _PTR(Study) aStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aStudy,GetDesktop(this)))
    return;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  // Get selected SObject
  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
  if (anIO.IsNull() || !anIO->hasEntry())
    return;

  // create a VTK view window if it does not exist
  SVTK_ViewWindow* aViewWindow = GetViewWindow<SVTK_Viewer>(this);

  // create mesh presentation and display it in aViewWindow
  VISU::Mesh_i* aPrs = CreateMesh(this, anIO, aViewWindow);
  aViewWindow->highlight(anIO, 1);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateManyMesh()
{
  _PTR(Study) aStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aStudy,GetDesktop(this)))
    return;

  // create a VTK view window if it does not exist
  SVTK_ViewWindow* aViewWindow = GetViewWindow<SVTK_Viewer>(this);

  // Get selected SObject
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);
  SALOME_ListIteratorOfListIO anIter (aListIO);
  for (; anIter.More(); anIter.Next()) {
    Handle(SALOME_InteractiveObject) anIO = anIter.Value();
    if (anIO.IsNull() || !anIO->hasEntry())
      return;

    // create mesh presentation and display it in aViewWindow
    VISU::Mesh_i* aPrs = CreateMesh(this, anIO, aViewWindow);
    aViewWindow->highlight(anIO, 1);
    processPresentationCreated(aPrs);
  }
}

void
VisuGUI
::OnCreateScalarMap()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::ScalarMap_i,VisuGUI_ScalarBarDlg,1>(this);
  processPresentationCreated(aPrs);
}


void
VisuGUI
::OnCreateDeformedShape()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::DeformedShape_i,VisuGUI_DeformedShapeDlg,1>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateDeformedShapeAndScalarMap()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::DeformedShapeAndScalarMap_i,VisuGUI_DeformedShapeAndScalarMapDlg,1>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateVectors()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::Vectors_i,VisuGUI_VectorsDlg,1>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateIsoSurfaces()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::IsoSurfaces_i,VisuGUI_IsoSurfacesDlg,1>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateCutPlanes()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::CutPlanes_i,VisuGUI_CutPlanesDlg,0>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateCutLines()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::CutLines_i,VisuGUI_CutLinesDlg,0>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateCutSegment()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::CutSegment_i,VisuGUI_CutSegmentDlg,0>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreateStreamLines()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::StreamLines_i,VisuGUI_StreamLinesDlg,1>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreatePlot3D()
{
  Prs3d_i* aPrs = CreatePrs3d<VISU::Plot3D_i,VisuGUI_Plot3DDlg,0>(this);
  processPresentationCreated(aPrs);
}

void
VisuGUI
::OnCreatePlot2dView()
{
  CheckLock(GetCStudy(GetAppStudy(this)),GetDesktop(this));
  VISU::Container_var aContainer = GetVisuGen( this )->CreateContainer();
  UpdateObjBrowser(this);

  if( !CORBA::is_nil( aContainer.in() ) ) {
    if( VISU::Container_i* aCContainer = dynamic_cast<VISU::Container_i*>( VISU::GetServant( aContainer ).in() ) ) {
      QStringList anEntryList;
      anEntryList.append( aCContainer->GetEntry().c_str() );
      if( getApp() )
        getApp()->browseObjects( anEntryList );
    }
  }
}

void
VisuGUI
::OnDisplayPrs()
{
  if(MYDEBUG) MESSAGE("VisuGUI::OnDisplayPrs");

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  if (!aSelectionMgr)
    return;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  SALOME_ListIO aSel, aList;
  aSelectionMgr->selectedObjects(aSel);
  extractContainers(aSel, aList);

  for (SALOME_ListIteratorOfListIO it (aList); it.More(); it.Next()) {
    Handle(SALOME_InteractiveObject) anIO = it.Value();
    TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(this), anIO->getEntry());
    VISU::Base_i* aBase = anObjectInfo.myBase;
    if(!aBase)
      continue;

    VISU::VISUType aType = aBase->GetType();
    switch (aType) {
    case VISU::TPOINTMAP3D:
      if(SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(this)){
        displayer()->Display(anIO->getEntry());
        aViewWindow->highlight(anIO, 1);
        aViewWindow->getRenderer()->ResetCameraClippingRange();
        aViewWindow->Repaint();
        break;
      }
      else {
        if (SPlot2d_Viewer* aPlot2d = GetPlot2dViewer(this, false))
          aPlot2d->Repaint();
        displayer()->Display(anIO->getEntry());
      }
    case VISU::TCURVE:
    case VISU::TCONTAINER:
    case VISU::TTABLE:
      getViewManager(SPlot2d_Viewer::Type(), true);
      displayer()->Display(anIO->getEntry());
      aSelectionMgr->setSelectedObjects(aList);
      break;
    default: {
      VISU::Prs3d_i* aPrs3d = VISU::GetPrs3dFromBase(aBase);
      if(aPrs3d){
        if(SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(this)){
          displayer()->Display(anIO->getEntry());
          aViewWindow->highlight(anIO, 1);
          aViewWindow->getRenderer()->ResetCameraClippingRange();
          aViewWindow->Repaint();
        }
      }
    }
    }
  }

  QApplication::restoreOverrideCursor();
}

void
VisuGUI
::OnDisplayOnlyPrs()
{
  /// to correct IMP 0016100
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  if (!aSelectionMgr)
    return;

  SALOME_ListIO aSel, aList;
  aSelectionMgr->selectedObjects(aSel);
  extractContainers(aSel, aList);
  Handle(SALOME_InteractiveObject) anIO = aList.First();
  activateAppropriateViewer(anIO);
  OnEraseAll();
  OnDisplayPrs();
}

void VisuGUI::activateAppropriateViewer( Handle(SALOME_InteractiveObject) theIO )
{
  TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(this), theIO->getEntry());
  VISU::Base_i* aBase = anObjectInfo.myBase;
  if(!aBase)
    return;

  std::string anEntry = theIO->getEntry();
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  _PTR(SObject) SO = aCStudy->FindObjectID( theIO->getEntry() );

  CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(SO);
  if(CORBA::is_nil(aCORBAObject))
    return;

  VISU::VISUType aType = aBase->GetType();

  if ( VISU::GetPrs3dFromBase(aBase) ) {
    // activate SVTK Viewer
    getApp()->getViewManager( SVTK_Viewer::Type(), true );
    return;
  }

  switch (aType) {
  case VISU::TCURVE:
  case VISU::TCONTAINER:
  case VISU::TTABLE: {
    // Activate Plot2d
    GetPlot2dViewer(this, true);
    break;
  }
  case VISU::TCOLOREDPRS3DHOLDER :
  case VISU::TPOINTMAP3D : {
    //find curves under pointmap 3d
    bool hasCurvesChildren = false;

    SUIT_ViewManager* aViewMgr = getApp()->activeViewManager();
    QString aViewerType = aViewMgr->getType();

    _PTR(ChildIterator) anIter = aCStudy->NewChildIterator(SO);
    _PTR(SObject) aChild;
    for (; anIter->More(); anIter->Next()) {
      aChild = anIter->Value();
      CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(aChild);
      if(!CORBA::is_nil(aCORBAObject)){
        PortableServer::ServantBase_var aServant = VISU::GetServant(aCORBAObject);
        if( dynamic_cast<VISU::Curve_i*>(aServant.in()))
          hasCurvesChildren = true;
      }
    }

    if (hasCurvesChildren && ( aViewerType == SPlot2d_Viewer::Type() || aViewerType == SVTK_Viewer::Type() ) ) {
      return;
    } else {
      // activate SVTK Viewer
      getApp()->getViewManager( SVTK_Viewer::Type(), true );
      return;
    }
    break;
  }
  }
}

void
VisuGUI
::OnErasePrs()
{
  if(MYDEBUG) MESSAGE("OnErasePrs");
  QApplication::setOverrideCursor(Qt::WaitCursor);

  SALOME_ListIO aList, aSel;
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  aSelectionMgr->selectedObjects(aSel);
  extractContainers(aSel, aList);

  for (SALOME_ListIteratorOfListIO it (aList); it.More(); it.Next()) {
    Handle(SALOME_InteractiveObject) anIO = it.Value();
    TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(this), anIO->getEntry());
    VISU::Base_i* aBase = anObjectInfo.myBase;
    ErasePrs(this, aBase, /*repaint_view_window = */true);
  }

  QApplication::restoreOverrideCursor();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEraseAll()
{
  startOperation( myEraseAll );
  if (SVTK_ViewWindow* vw = GetActiveViewWindow<SVTK_ViewWindow>(this)) {
    vw->unHighlightAll();
    if (vtkRenderer *aRen = vw->getRenderer()) {
      vtkActor *anActor;
      VTK::ActorCollectionCopy aCopy(aRen->GetActors());
      vtkActorCollection *anActColl = aCopy.GetActors();
      for (anActColl->InitTraversal(); (anActor = anActColl->GetNextActor()) != NULL; ) {
        if (anActor->GetVisibility() > 0)
          if (VISU_Actor* anVISUActor = VISU_Actor::SafeDownCast(anActor)) {
            RemoveScalarBarPosition(this, vw, anVISUActor->GetPrs3d());
            anVISUActor->VisibilityOff();
          }
      }
      vw->Repaint();
    }
  } else if (SPlot2d_Viewer* aPlot2d = GetPlot2dViewer(this, false)) {
    aPlot2d->EraseAll();
  }
}


namespace
{
  //----------------------------------------------------------------------------
  VISU::Prs3d_i*
  GetPrsToModify(const SalomeApp_Module* theModule,
                 Handle(SALOME_InteractiveObject)& theIO)
  {
    VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(theModule);
    if(aSelectionInfo.empty())
      return NULL;

    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    theIO = aSelectionItem.myIO;
    return VISU::GetPrs3dToModify(theModule, aSelectionItem.myObjectInfo.myBase);
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditScalarMap()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::ScalarMap_i, VisuGUI_ScalarBarDlg, 1>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditDeformedShape()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    VisuGUI_DeformedShapeDlg::EditPrs3d(this, aPrs3d, anIO);
  //    EditPrs3d<VISU::DeformedShape_i, VisuGUI_DeformedShapeDlg, 1>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditDeformedShapeAndScalarMap()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::DeformedShapeAndScalarMap_i, VisuGUI_DeformedShapeAndScalarMapDlg, 1>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditCutPlanes()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::CutPlanes_i, VisuGUI_CutPlanesDlg, 0>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditCutLines()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::CutLines_i, VisuGUI_CutLinesDlg, 0>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditCutSegment()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::CutSegment_i, VisuGUI_CutSegmentDlg, 0>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditIsoSurfaces()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::IsoSurfaces_i, VisuGUI_IsoSurfacesDlg, 1>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditVectors()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::Vectors_i, VisuGUI_VectorsDlg, 1>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditStreamLines()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::StreamLines_i, VisuGUI_StreamLinesDlg, 1>(this, anIO, aPrs3d);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditPlot3D()
{
  Handle(SALOME_InteractiveObject) anIO;
  if (VISU::Prs3d_i* aPrs3d = GetPrsToModify(this, anIO))
    EditPrs3d<VISU::Plot3D_i, VisuGUI_Plot3DDlg, 0>(this, anIO, aPrs3d);
}


//----------------------------------------------------------------------------
void
VisuGUI
::OnMakeSurfaceframe()
{
  ChangeRepresentation(this, VISU::SURFACEFRAME);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMakeInsideframe()
{
  ChangeRepresentation(this, VISU::INSIDEFRAME);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMakeWireframe()
{
  ChangeRepresentation(this, VISU::WIREFRAME);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMakeSurface()
{
  ChangeRepresentation(this, VISU::SHADED);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMakePoints()
{
  ChangeRepresentation(this, VISU::POINT);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMakeShrink()
{
  ChangeRepresentation(this, VISU::SHRINK);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMakeFeatureEdges()
{
  ChangeRepresentation(this, VISU::FEATURE_EDGES);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnSetShadingOn()
{
  SetShading(this, true);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnSetShadingOff()
{
  SetShading(this, false);
}


//----------------------------------------------------------------------------
void
VisuGUI
::OnChangeColor()
{
  VISU_Actor* anActor = NULL;
  VISU::Prs3d_i* aPrs3d = NULL;
  SVTK_ViewWindow* aViewWindow = NULL;
  if(!GetPrs3dSelectionInfo(this, aPrs3d, aViewWindow, anActor))
    return;

  VISU::Mesh_i* aMesh = dynamic_cast<VISU::Mesh_i*>(aPrs3d);
  VISU::DeformedShape_i* aDeformedShape = dynamic_cast<VISU::DeformedShape_i*>(aPrs3d);

  SALOMEDS::Color anOldColor, aNewColor;
  int aRepresent = anActor->GetRepresentation();
  if (aMesh) {
    switch (aRepresent) {
      case VISU::POINT :
        anOldColor = aMesh->GetNodeColor();
        break;
      case VISU::WIREFRAME :
      case VISU::INSIDEFRAME :
      case VISU::FEATURE_EDGES :	
        anOldColor = aMesh->GetLinkColor();
        break;
      case VISU::SHADED :
      case VISU::SURFACEFRAME :
        anOldColor = aMesh->GetCellColor();
        break;
    }
  } else if (aDeformedShape) {
    anOldColor = aDeformedShape->GetColor();
  } else {
    return;
  }

  QColor aColor (int(255*anOldColor.R),
                 int(255*anOldColor.G),
                 int(255*anOldColor.B));
  QColor aColorNew = QColorDialog::getColor(aColor, GetDesktop(this));
  if (aColorNew.isValid()) {
    aNewColor.R = aColorNew.red()/255.;
    aNewColor.G = aColorNew.green()/255.;
    aNewColor.B = aColorNew.blue()/255.;
    if (aMesh) {
      switch (aRepresent) {
        case VISU::POINT :
          aMesh->SetNodeColor(aNewColor);
          break;
        case VISU::WIREFRAME :
        case VISU::INSIDEFRAME :
          aMesh->SetLinkColor(aNewColor);
          break;
        case VISU::SHADED :
        case VISU::SURFACEFRAME :
        case VISU::FEATURE_EDGES :
          aMesh->SetCellColor(aNewColor);
          break;
      }
    } else {
      aDeformedShape->SetColor(aNewColor);
    }
    RecreateActor(this, aPrs3d);
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnChangeWireframeColor()
{
  VISU_Actor* anActor = NULL;
  VISU::Prs3d_i* aPrs3d = NULL;
  SVTK_ViewWindow* aViewWindow = NULL;
  if(!GetPrs3dSelectionInfo(this, aPrs3d, aViewWindow, anActor))
    return;

  VISU::Mesh_i* aMesh = dynamic_cast<VISU::Mesh_i*>(aPrs3d);
  VISU::ScalarMap_i* aScalarMap = dynamic_cast<VISU::ScalarMap_i*>(aPrs3d);
  if(!aMesh && !aScalarMap)
    return;

  SALOMEDS::Color anOldColor, aNewColor;
  if (aMesh)
    anOldColor = aMesh->GetLinkColor();
  else if (aScalarMap)
    anOldColor = aScalarMap->GetLinkColor();


  QColor aColor (int(255*anOldColor.R),
                 int(255*anOldColor.G),
                 int(255*anOldColor.B));
  QColor aColorNew = QColorDialog::getColor(aColor, GetDesktop(this));
  if (aColorNew.isValid()) {
    aNewColor.R = aColorNew.red()/255.;
    aNewColor.G = aColorNew.green()/255.;
    aNewColor.B = aColorNew.blue()/255.;
    if (aMesh)
      aMesh->SetLinkColor(aNewColor);
    else
      aScalarMap->SetLinkColor(aNewColor);
    RecreateActor(this, aPrs3d);
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnChangeOpacity()
{
  VisuGUI_TransparencyDlg* aDialog = new VisuGUI_TransparencyDlg( this );
  aDialog->show();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnChangeLines()
{
  VISU_Actor* anActor = NULL;
  VISU_ActorBase* anActorBase = NULL;
  VISU::Prs3d_i* aPrs3d = NULL;
  SVTK_ViewWindow* aViewWindow = NULL;
  float oldlwid = 1;
  bool isPrs3d = false;

  if (GetPrs3dSelectionInfo(this, aPrs3d, aViewWindow, anActor)) {
    isPrs3d = true;
    oldlwid = anActor->GetLineWidth();
  }
  else {
    aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(this);
    VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    VISU::PointMap3d_i* aTable = dynamic_cast<VISU::PointMap3d_i*>(aSelectionItem.myObjectInfo.myBase);
    anActorBase = VISU::FindActorBase(aViewWindow, aTable);
    oldlwid = anActorBase->GetLineWidth();
  }


  VisuGUI_CursorDlg* CursorDlg =
    new VisuGUI_CursorDlg (GetDesktop(this), (const char*)tr("DLG_LINEWIDTH_TITLE").toLatin1(), TRUE);

  CursorDlg->helpFileName = "viewing_3d_presentations_page.html#width_opacity_anchor";
  CursorDlg->Comment1->setText(tr("DLG_LINEWIDTH_CMT1"));
  CursorDlg->Comment2->setText(tr("DLG_LINEWIDTH_CMT2"));
  CursorDlg->SpinBox1->setMinimum(1);
  CursorDlg->SpinBox1->setMaximum(10);

  int intlwid = int(oldlwid);
  CursorDlg->SpinBox1->setValue(intlwid);

  int ret = CursorDlg->exec();
  if (ret == 1) {
    intlwid  = CursorDlg->SpinBox1->value();
    float newlwid = intlwid;
    if (isPrs3d)
      anActor->SetLineWidth(newlwid);
    else
      anActorBase->SetLineWidth(newlwid);
  }
  delete CursorDlg;
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnChangePointMarker()
{
  SALOME_ListIO aListIO;
  GetSelectionMgr( this )->selectedObjects( aListIO );
  if( aListIO.Extent() < 1 )
    return;

  VISU::StudyId2MarkerMap& aMarkerMap = GetVisuGen( this )->GetMarkerMap();
  _PTR(Study) aCStudy = GetCStudy( GetAppStudy( this ) );
  int aStudyId = aCStudy->StudyId();

  QPixmap aPointSpritePixmap;
  if( SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr() )
    aPointSpritePixmap = aResourceMgr->loadPixmap( "VISU", tr( "ICON_POINT_SPRITE" ) );

  bool update = false;
  for( SALOME_ListIteratorOfListIO It( aListIO ); It.More(); It.Next() )
  {
    Handle(SALOME_InteractiveObject)& anIO = It.Value();
    std::vector<VISU::Prs3d_i*> aPrsList = GetPrs3dList( this, anIO );
    for( int i = 0, n = aPrsList.size(); i < n; i++ )
    {
      if( VISU::Prs3d_i* aPrs = aPrsList[i] )
      {
        VTKViewer_MarkerDlg* aDlg = new VTKViewer_MarkerDlg( GetDesktop( this ) );
        aDlg->setHelpData( "VISU", "point_marker_page.html" );

        aDlg->setCustomMarkers( aMarkerMap[ aStudyId ] );

        aDlg->addMarker( VTK::MT_POINT_SPRITE, aPointSpritePixmap );

        VISU::MarkerType aMarkerTypeCurrent = aPrs->GetMarkerType();
        VISU::MarkerScale aMarkerScaleCurrent = aPrs->GetMarkerScale();
        int aMarkerTextureCurrent = aPrs->GetMarkerTexture();
        if( aMarkerTypeCurrent != VISU::MT_USER )
          aDlg->setMarker( (VTK::MarkerType)aMarkerTypeCurrent, (VTK::MarkerScale)aMarkerScaleCurrent );
        else
          aDlg->setCustomMarker( aMarkerTextureCurrent );

        if( aDlg->exec() )
        {
          aMarkerMap[ aStudyId ] = aDlg->customMarkers();

          VISU::MarkerType aMarkerTypeNew = (VISU::MarkerType)aDlg->markerType();
          VISU::MarkerScale aMarkerScaleNew = (VISU::MarkerScale)aDlg->markerScale();
          int aMarkerTextureNew = aDlg->markerId();
          if( aMarkerTypeNew != VISU::MT_USER )
            aPrs->SetMarkerStd( aMarkerTypeNew, aMarkerScaleNew );
          else
            aPrs->SetMarkerTexture( aMarkerTextureNew );

          aPrs->UpdateActors();
          update = true;
        }

        delete aDlg;
      }
    }
  }

  if( update )
    if( SVTK_ViewWindow* vw = GetActiveViewWindow<SVTK_ViewWindow>( this ) )
      vw->Repaint();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnChangeShrinkFactor()
{
  VisuGUI_ShrinkFactorDlg* aDialog = new VisuGUI_ShrinkFactorDlg( this );
  aDialog->show();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnShowTable()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
  _PTR(SObject) aSObject = aSelectionItem.myObjectInfo.mySObject;
  if(VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(aBase)) {
    aSObject = GetCStudy( GetAppStudy( this ) )->FindObjectID( aTable->GetObjectEntry() );
    if( IsSObjectTable( aSObject ) ) {
      VISU_TableDlg* dlg = new VISU_TableDlg( GetDesktop( this ),
                                              GetCStudy( GetAppStudy( this ) ),
                                              aTable,
                                              false,
                                              //SAL2670 Orientation of show tables
                                              VISU_TableDlg::ttAuto,
                                              Qt::Vertical );
      dlg->show();
    }
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnShowTablePlot()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
  _PTR(SObject) aSObject = aSelectionItem.myObjectInfo.mySObject;
  if(VISU::PointMap3d_i* aTable = dynamic_cast<VISU::PointMap3d_i*>(aBase)) {
    VisuGUI_Table3DDlg*aDlg = new VisuGUI_Table3DDlg( this );
    aDlg->initFromPrsObject( aTable );
    if ( runAndWait(aDlg, 0) > 0 ) {
      aDlg->storeToPrsObject(aTable);
      aTable->UpdateActors();
    }
    delete aDlg;
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnShowScalarBar()
{
  SetScalarBarVisibility(true);
}

void
VisuGUI
::OnHideScalarBar()
{
  SetScalarBarVisibility(false);
}

void
VisuGUI
::SetScalarBarVisibility(bool theVisibility)
{
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  SALOME_ListIteratorOfListIO It (aListIO);

  bool update = false;
  for (; It.More(); It.Next()) {
    Handle(SALOME_InteractiveObject)& anIO = It.Value();
    std::vector<VISU::Prs3d_i*> aPrsList = GetPrs3dList(this, anIO, true);
    if ( aPrsList.size()>0 ) {
      for (int i = 0, n = aPrsList.size(); i < n; i++) {
        VISU::Prs3d_i* aPrsObject = aPrsList[i];
        if (aPrsObject) {
          if (VISU::ScalarMap_i* aScalar = dynamic_cast<VISU::ScalarMap_i*>(aPrsObject)) {
            aScalar->SetBarVisible( theVisibility );
            RecreateActor(this, aScalar);
            update = true;
          } else if (VISU::GaussPoints_i* aGauss = dynamic_cast<VISU::GaussPoints_i*>(aPrsObject)) {
            aGauss->SetBarVisible( theVisibility );
            RecreateActor(this, aGauss);
            update = true;
          }
        }
      }
    }
  }
  if (update) {
    if (SVTK_ViewWindow* vw = GetActiveViewWindow<SVTK_ViewWindow>(this))
      vw->Repaint();
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnCreateTable()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  _PTR(SObject) aSObject = aSelectionItem.myObjectInfo.mySObject;
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;

  if(aBase &&
     (aBase->GetType() == VISU::TCUTLINES || aBase->GetType() == VISU::TCUTSEGMENT) ||
     IsSObjectTable(aSObject)) {
    GetVisuGen( this )->CreateTable( aSObject->GetID().c_str() );
    UpdateObjBrowser(this);
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnDeleteObjects()
{
  SalomeApp_Study* anAppStudy = GetAppStudy(this);
  _PTR(Study) aCStudy = GetCStudy(anAppStudy);
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;

  SALOME_ListIO aList;
  LightApp_SelectionMgr* mgr = GetSelectionMgr(this);
  mgr->selectedObjects(aList,QString::null,false);
  int i = 0, nbSelected = aList.Extent();
  if (nbSelected < 1) return;

  std::vector<std::string> anEntries (nbSelected);
  QString aNameList;
  Handle(SALOME_InteractiveObject) anIO;
  for (SALOME_ListIteratorOfListIO it (aList); it.More(); it.Next()) {
    anIO = it.Value();
    if (anIO->hasEntry()) {
      if (IsRemovable(anIO->getEntry(), this)) {
        aNameList.append("\n    - ");
        // Get Name Of the Main Object if it's a referenced object
        VISU::TObjectInfo anObjectInfo = GetObjectByEntry(GetAppStudy(this), anIO->getEntry());
        if(_PTR(SObject) aSObject = anObjectInfo.mySObject){
          _PTR(SObject) aRefSObject;
          if (aSObject->ReferencedObject(aRefSObject)) {
            QString aRefName = QString::fromStdString ( aRefSObject->GetName() );
            aNameList.append( aRefName );
          }
          else
            aNameList.append(anIO->getName());
        }
        anEntries[i++] = anIO->getEntry();
      }
    }
  }
  nbSelected = i;
  if (nbSelected < 1) {
    SUIT_MessageBox::warning(GetDesktop(this),
                             QObject::tr("WRN_VISU"),
                             QObject::tr("WRN_NO_APPROPRIATE_SELECTION"));
    return;
  }

  if ( SUIT_MessageBox::question( GetDesktop(this),
                                  QObject::tr( "WRN_VISU" ),
                                  QObject::tr( "VISU_REALLY_DELETE" ).arg( nbSelected ).arg( aNameList ),
                                  SUIT_MessageBox::Yes | SUIT_MessageBox::No ) != SUIT_MessageBox::Yes )
    return;

  // There is a transaction
  _PTR(StudyBuilder) aStudyBuilder = aCStudy->NewBuilder();
  aStudyBuilder->NewCommand();

  for (i = 0; i < nbSelected; i++) {
    VISU::TObjectInfo anObjectInfo = GetObjectByEntry(GetAppStudy(this), anEntries[i]);
    if(_PTR(SObject) aSObject = anObjectInfo.mySObject){
      emit beforeObjectDelete(QString(aSObject->GetID().c_str()));
      _PTR(SObject) aRefSObject;
      if (aSObject->ReferencedObject(aRefSObject)) {
        // It can be a reference on curve, published under a container.
        // In this case the curve should be removed from the container.
        // See bug 10441.
        VISU::TObjectInfo aRefObjectInfo = GetObjectByEntry(GetAppStudy(this), aRefSObject->GetID());
        VISU::Base_i* aRefBase = aRefObjectInfo.myBase;
        if(aRefBase && aRefBase->GetType() == VISU::TMESH){
          DeleteSObject(this, aCStudy, aRefSObject);
          UpdateObjBrowser(this, true, aRefSObject);
        }else if(aRefBase && aRefBase->GetType() == VISU::TCURVE){
          CORBA::Object_var anObject = ClientSObjectToObject(aRefSObject);
          VISU::Curve_var aCurve = VISU::Curve::_narrow(anObject);
          _PTR(SObject) aParentSO = aSObject->GetFather();
          VISU::TObjectInfo aParentObjectInfo = GetObjectByEntry(GetAppStudy(this), aParentSO->GetID());
          if(VISU::Base_i* aParentBase = aParentObjectInfo.myBase){
            if(VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(aParentBase)){
              if(aContainer->GetNbCurves() > 0){
                aContainer->RemoveCurve(aCurve);
                UpdateObjBrowser(this, true, aParentSO);
              }
            }
          }
        }
      }
      else{
        DeleteSObject(this, aCStudy, aSObject);
        UpdateObjBrowser(this, true, aSObject);
      }
    }
  }

  // Finish transaction
  aStudyBuilder->CommitCommand();
  mgr->clearSelected();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnPlotData()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  if(VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase){
    if(aBase->GetType() == VISU::TTABLE || aBase->GetType() == VISU::TPOINTMAP3D) {
      if(VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(aBase)){
        _PTR(Study) aStudy = GetCStudy(GetAppStudy(this));
        _PTR(SObject) aSObject = aStudy->FindObjectID( aTable->GetObjectEntry() );
        if(IsSObjectTable(aSObject)){
          VisuGUI_SetupPlot2dDlg* aDlg = new VisuGUI_SetupPlot2dDlg(aSObject, aTable ,GetDesktop(this));
          if(aDlg->exec() == QDialog::Accepted){
            if(!IsStudyLocked(aStudy)){
              // if study is not locked - create new container, create curves and insert them
              // into container, then plot container if current viewer is of VIEW_PLOT2D type
              int horIndex;
              QList<int> verIndices, ver1Indices, ver2Indices, zIndices;
              aDlg->getCurvesSource( horIndex, ver1Indices, ver2Indices, zIndices );
              verIndices = ver1Indices + ver2Indices;
              if( horIndex >= 0 && verIndices.count() > 0 ){
                CORBA::Object_var aContainerObj = GetVisuGen(this)->CreateContainer();
                if(VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(VISU::GetServant(aContainerObj).in())){
                  for( int i = 0; i < verIndices.count(); i++ ){
                    bool isV2 = ver2Indices.contains(verIndices[i]);
                    VISU::Curve_var aCurveObject =
                      GetVisuGen(this)->CreateCurveWithZExt(aTable->_this(), 
                                                            aDlg->idx(horIndex)+1, 
                                                            aDlg->idx(verIndices[i])+1, 
                                                            aDlg->idx(zIndices[i])+1, isV2 );
                    if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurveObject).in())){
                      bool isAuto;
                      int  marker, line, lineWidth;
                      QColor color;
                      if ( aDlg->getCurveAttributes(verIndices[i], isAuto, marker,
                                                    line, lineWidth, color) && !isAuto )
                      {
                        SALOMEDS::Color c;
                        c.R = color.red()  /255.;
                        c.G = color.green()/255.;
                        c.B = color.blue() /255.;
                        aCurve->SetColor( c );
                        aCurve->SetMarker( ( VISU::Curve::MarkerType )marker );
                        aCurve->SetLine( ( VISU::Curve::LineType )line, lineWidth );
                      }
                      aContainer->AddCurve(aCurveObject);
                    }
                  }
                  UpdateObjBrowser(this);
                  PlotContainer( this, aContainer, VISU::eDisplay );
                }
              }
            } else {
              // get name of SObject
              QString aSObjectName;
              _PTR(GenericAttribute) anAttr;
              if(aSObject->FindAttribute( anAttr, "AttributeName" )){
                _PTR(AttributeName) aName = anAttr;
                aSObjectName = QString( aName->Value().c_str() );
              }
              // if study is locked just get curves info and plot them
              // if current viewer is of VIEW_PLOT2D type
              QList<Plot2d_Curve*> container;
              aDlg->getCurves( container );
              if ( !container.isEmpty() ) {
                GetPlot2dViewer( this )->getActiveViewFrame()->displayCurves( container, true );
                GetPlot2dViewer( this )->getActiveViewFrame()->setTitle( aSObjectName );
              }
            }
          }
          delete aDlg;
        }
      }
    }
  }else if(!aSelectionItem.myIO.IsNull()){
    // check if Table SObject is selected
    _PTR(SObject) aSObject = aSelectionItem.myObjectInfo.mySObject;
    if(IsSObjectTable(aSObject)){
      VisuGUI_SetupPlot2dDlg* aDlg = new VisuGUI_SetupPlot2dDlg(aSObject, NULL, GetDesktop(this));
      if(aDlg->exec() == QDialog::Accepted){
        _PTR(Study) aStudy = GetCStudy(GetAppStudy(this));
        if(!IsStudyLocked(aStudy)){
          // if study is not locked - create new table and container objects, create curves
          // and insert them into container, then plot container if current viewer is of VIEW_PLOT2D type
          int horIndex;
          QList<int> verIndices, ver1Indices, ver2Indices, zIndices;
          aDlg->getCurvesSource( horIndex, verIndices, ver2Indices, zIndices );
          verIndices = ver1Indices + ver2Indices;
          if ( horIndex >= 0 && verIndices.count() > 0 ) {
            VISU::Table_var aTableObject = GetVisuGen(this)->CreateTable(aSObject->GetID().c_str());
            if(!CORBA::is_nil(aTableObject)){
              VISU::Container_var aContainerObject = GetVisuGen(this)->CreateContainer();
              VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(VISU::GetServant(aContainerObject).in());
              for ( int i = 0; i < verIndices.count(); i++ ) {
                bool isV2 = ver2Indices.contains(verIndices[i]);
                VISU::Curve_var aCurveObject = GetVisuGen(this)->CreateCurveWithZExt
                  ( aTableObject, horIndex+1, verIndices[i]+1, zIndices[i]+1, isV2 );
                if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurveObject).in())){
                  bool isAuto;
                  int  marker, line, lineWidth;
                  QColor color;
                  if ( aDlg->getCurveAttributes(verIndices[i], isAuto, marker,
                                                line, lineWidth, color) && !isAuto )
                  {
                    SALOMEDS::Color c;
                    c.R = color.red()/255.;
                    c.G = color.green()/255.;
                    c.B = color.blue()/255.;
                    aCurve->SetColor( c );
                    aCurve->SetMarker( ( VISU::Curve::MarkerType )marker );
                    aCurve->SetLine( ( VISU::Curve::LineType )line, lineWidth );
                  }
                  aContainerObject->AddCurve(aCurveObject);
                }
              }
              UpdateObjBrowser(this);
              PlotContainer( this, aContainer, VISU::eDisplay );
            }
          }
        } else {
          // get name of SObject
          QString aSObjectName;
          _PTR(GenericAttribute) anAttr;
          if(aSObject->FindAttribute(anAttr, "AttributeName")){
            _PTR(AttributeName) aName = anAttr;
            aSObjectName = QString( aName->Value().c_str() );
          }
          // if study is locked just get curves info and plot them
          QList<Plot2d_Curve*> container;
          aDlg->getCurves( container );
          if ( !container.isEmpty() ) {
            GetPlot2dViewer( this )->getActiveViewFrame()->displayCurves( container, true );
            GetPlot2dViewer( this )->getActiveViewFrame()->setTitle( aSObjectName );
          }
        }
      }
      delete aDlg;
    }
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnCurveProperties()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.size() != 1)
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
  if(!aBase)
    return;

  if(aBase->GetType() == VISU::TCURVE) {
    // Curve object
    if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aBase)){
      if(!IsStudyLocked(GetCStudy(GetAppStudy(this)))){
        Plot2d_SetupCurveDlg aDlg(GetDesktop( this ));

        aDlg.setLine( (Plot2d::LineType)aCurve->GetLine(), aCurve->GetLineWidth() );
        aDlg.setMarker( (Plot2d::MarkerType)aCurve->GetMarker() );
        SALOMEDS::Color aColor = aCurve->GetColor();
        aDlg.setColor( QColor( (int)(aColor.R*255.), (int)(aColor.G*255.), (int)(aColor.B*255.) ) );
        if( aDlg.exec() == QDialog::Accepted ) {
          aCurve->SetLine( (VISU::Curve::LineType)aDlg.getLine(), aDlg.getLineWidth() );
          aCurve->SetMarker( (VISU::Curve::MarkerType)aDlg.getMarker());
          SALOMEDS::Color newColor;
          newColor.R = aDlg.getColor().red()/255.;
          newColor.G = aDlg.getColor().green()/255.;
          newColor.B = aDlg.getColor().blue()/255.;
          aCurve->SetColor( newColor );
          PlotCurve(this, aCurve, VISU::eDisplay);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnCurveScale()
{
  if(IsStudyLocked(GetCStudy(GetAppStudy(this))))
    return;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects( this );
  int nbSel = aSelectionInfo.size();
  QList<VISU::Curve_i*> curList;
  bool isValuesEquals = true;			
  double value;

  for (int i = 0; i < nbSel; i++ ) {
    VISU::TSelectionItem aSelectionItem = aSelectionInfo[i];
    VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
    if(!aBase)
      continue;

    if(aBase->GetType() == VISU::TCURVE) 
      // Curve object
      if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aBase)) {
        curList.append(aCurve);
        if( i>0 && isValuesEquals )
          isValuesEquals = (value == aCurve->GetScale());
	  value = aCurve->GetScale();
      }
  }

  Plot2d_SetupCurveScaleDlg aDlg(GetDesktop( this ));
  if( isValuesEquals )
     aDlg.setScale(value);

  if( aDlg.exec() == QDialog::Accepted )
	for ( int i = 0; i < curList.size(); ++i )
	  curList.at(i)->SetScale( aDlg.getScale() );
}
//----------------------------------------------------------------------------
void
VisuGUI
::OnClearContainer()
{
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
  if(!aBase)
    return;

  if(aBase->GetType() == VISU::TCONTAINER){
    // Container object
    if(VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(aBase)){
      if(aContainer && aContainer->GetNbCurves() > 0){
        aContainer->Clear();
	VISU::updateContainerVisibility(aContainer);
        UpdateObjBrowser(this);
      }
    }
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnEditContainer()
{
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
  if(!aBase)
    return;

  VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(aBase);
  if(!aContainer)
    return;

  VisuGUI_EditContainerDlg* aDlg = new VisuGUI_EditContainerDlg (this);
  aDlg->initFromPrsObject(aContainer);
  if (aDlg->exec()) {
    aDlg->storeToPrsObject(aContainer);
    UpdateObjBrowser(this, true);
    delete aDlg;
    VISU::updateContainerVisibility(aContainer);
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnSaveViewParams()
{
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;

  SUIT_ViewManager* aViewMgr = getApp()->activeViewManager();
  if (aViewMgr->getType() != SVTK_Viewer::Type())
    return;

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);
  if (aListIO.Extent() > 1)
    return;

  if (aListIO.Extent() == 0) {
    VISU::View3D_i::SaveViewParams(aViewMgr, (const char*)VISU::View3D_i::GenerateViewParamsName().toLatin1());
  } else {
    const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();
    VISU::View3D_i::SaveViewParams(aViewMgr, anIO->getName());
  }
  UpdateObjBrowser(this);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnRestoreViewParams()
{
  SUIT_ViewManager* aViewMgr = getApp()->activeViewManager();
  if (aViewMgr->getType() != SVTK_Viewer::Type())
    return;

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);
  if (aListIO.Extent() != 1)
    return;

  const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();
  //jfa tmp:VISU::View3D_i::RestoreViewParams(aViewMgr, anIO->getName());
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));//jfa tmp
  _PTR(SObject) aSObj = aCStudy->FindObjectID(anIO->getEntry());//jfa tmp
  if ( aSObj )
    VISU::View3D_i::RestoreViewParams(aViewMgr, aSObj->GetName().c_str());//jfa tmp
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnClippingPlanes()
{
  new VisuGUI_ClippingDlg (this, false);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnSweep()
{
  OnDisplayPrs();
  if ( !mySweep->toggleViewAction()->isChecked() )
    mySweep->toggleViewAction()->trigger();
  mySweep->onSelectionChanged();
  mySweep->onFirst();
  mySweep->onPlay(true);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnTimeAnimation(VISU::Animation::AnimationMode theMode)
{
  if (!VISU::GetActiveViewWindow<SVTK_ViewWindow>(this))
    return;

  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  VisuGUI_TimeAnimationDlg* aAnimationDlg =
    new VisuGUI_TimeAnimationDlg (this, aCStudy, theMode);

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  SALOME_ListIteratorOfListIO It (aListIO);
  for (; It.More(); It.Next()) {
    _PTR(SObject) aSObject = aCStudy->FindObjectID(It.Value()->getEntry());
    if ( !aAnimationDlg->addField(aSObject) ) {
      SUIT_MessageBox::warning(GetDesktop(this),
                               tr("WRN_VISU"),
                               tr("ERR_CANT_CREATE_ANIMATION") );
      delete aAnimationDlg;
      return;
    }
  }
  aAnimationDlg->show();
}

//----------------------------------------------------------------------------
void
VisuGUI::
OnParallelTimeAnimation()
{
  OnTimeAnimation(VISU::Animation::PARALLEL);
}

void
VisuGUI::
OnSucccessiveTimeAnimation()
{
  OnTimeAnimation(VISU::Animation::SUCCESSIVE);
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnShowAnimation()
{
  if (!VISU::GetActiveViewWindow<SVTK_ViewWindow>(this))
    return;

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  if (aListIO.Extent() != 1)
    return;

  const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();

  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));

  _PTR(SObject) aSObj = aCStudy->FindObjectID(anIO->getEntry());
  if (!aSObj)
    return;

  VISU::VISUType aType = VISU::Storable::SObject2Type( aSObj );
  if (aType != VISU::TANIMATION)
    return;

  VisuGUI_TimeAnimationDlg* aAnimationDlg =
    new VisuGUI_TimeAnimationDlg(this, aCStudy);
  aAnimationDlg->restoreFromStudy(aSObj);
  aAnimationDlg->show();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnPointEvolution()
{
  _PTR(Study) aCStudy = GetCStudy( GetAppStudy( this ) );
  VisuGUI_EvolutionDlg* aDlg = new VisuGUI_EvolutionDlg( this, aCStudy );

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr( this );
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects( aListIO );

  SALOME_ListIteratorOfListIO It( aListIO );
  for( ; It.More(); It.Next() )
  {
    _PTR(SObject) aSObject = aCStudy->FindObjectID( It.Value()->getEntry() );
    if( !aDlg->setField( aSObject ) )
    {
      SUIT_MessageBox::warning( GetDesktop( this ),
                                tr( "WRN_VISU" ),
                                tr( "ERR_CANT_CREATE_EVOLUTION" ) );
      delete aDlg;
      return;
    }
  }
  aDlg->show();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnShowEvolution()
{
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr( this );
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects( aListIO );

  if( aListIO.Extent() != 1 )
    return;

  const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();

  _PTR(Study) aCStudy = GetCStudy( GetAppStudy( this ) );
  _PTR(SObject) aSObj = aCStudy->FindObjectID( anIO->getEntry() );
  if( !aSObj )
    return;

  VISU::VISUType aType = VISU::Storable::SObject2Type( aSObj );
  if( aType != VISU::TEVOLUTION )
    return;

  VisuGUI_EvolutionDlg* aDlg = new VisuGUI_EvolutionDlg( this, aCStudy );
  aDlg->restoreFromStudy( aSObj );
  aDlg->show();
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnCopyPresentation()
{
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
  if(!aBase)
    return;

  VISU::Prs3d_i* aPrs3d = VISU::GetPrs3dToModify(this, aBase);
  if(!aPrs3d)
    return;

  if(aBase->GetType() != VISU::TMESH){
    VISU::ColoredPrs3d_i* aColoredPrs3d = dynamic_cast<VISU::ColoredPrs3d_i*>(aBase);
    switch (aBase->GetType()) {
    case VISU::TSCALARMAP:
      TSameAsFactory<VISU::TSCALARMAP>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TDEFORMEDSHAPE:
      TSameAsFactory<VISU::TDEFORMEDSHAPE>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TSCALARMAPONDEFORMEDSHAPE:
    case VISU::TDEFORMEDSHAPEANDSCALARMAP:
      TSameAsFactory<VISU::TDEFORMEDSHAPEANDSCALARMAP>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TCUTPLANES:
      TSameAsFactory<VISU::TCUTPLANES>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TCUTLINES:
      TSameAsFactory<VISU::TCUTLINES>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TCUTSEGMENT:
      TSameAsFactory<VISU::TCUTSEGMENT>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TISOSURFACES:
      TSameAsFactory<VISU::TISOSURFACES>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TSTREAMLINES:
      TSameAsFactory<VISU::TSTREAMLINES>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TVECTORS:
      TSameAsFactory<VISU::TVECTORS>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    case VISU::TPLOT3D:
      TSameAsFactory<VISU::TPLOT3D>().Create(aColoredPrs3d, ColoredPrs3d_i::EPublishUnderTimeStamp, false);
      break;
    }
    UpdateObjBrowser(this);
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnToggleSelectionPanel()
{
  if (GetActiveViewWindow<SVTK_ViewWindow>(this))
  {
    if( myPanels.find( SelectionPanelId ) != myPanels.end() )
    {
      if( VisuGUI_SelectionPanel* aSelectionPanel =
          dynamic_cast<VisuGUI_SelectionPanel*>( myPanels[ SelectionPanelId ] ) )
      {
        if( aSelectionPanel->isVisible() )
          aSelectionPanel->hide();
        else
          aSelectionPanel->show();
      }
    }
  }
  else
    SUIT_MessageBox::warning(GetDesktop(this),
                             tr("WRN_VISU"),
                             tr("ERR_ACTIVATE_VIEW3D") );
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnSwitchSelectionMode()
{
  QAction* anAction = dynamic_cast<QAction*>( sender() );
  if( !anAction )
    return;

  bool isAnyChecked = false;
  for( int anId = VISU_ACTOR_SELECTION; anId <= VISU_GAUSS_POINT_SELECTION; anId++ )
  {
    QAction* aRefAction = action( anId );
    isAnyChecked |= aRefAction->isChecked();
    if( anAction != aRefAction )
      aRefAction->setChecked( false );
    else if( myPanels.find( SelectionPanelId ) != myPanels.end() )
    {
      if( VisuGUI_SelectionPanel* aSelectionPanel =
          dynamic_cast<VisuGUI_SelectionPanel*>( myPanels[ SelectionPanelId ] ) )
        aSelectionPanel->setSelectionMode( anId - VISU_ACTOR_SELECTION );
    }
  }
  // At least one action must be checked
  if( !isAnyChecked )
    anAction->setChecked( true );
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnSwitchSelectionMode( int theId )
{
  for( int anId = VISU_ACTOR_SELECTION; anId <= VISU_GAUSS_POINT_SELECTION; anId++ )
  {
    QAction* aRefAction = action( anId );
    aRefAction->setChecked( anId == VISU_ACTOR_SELECTION + theId );
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnMergeScalarBars()
{
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  SALOME_ListIteratorOfListIO It (aListIO);

  // first find the bounds
  double aMin, aMax; bool first = true;
  for (; It.More(); It.Next()) {
    Handle(SALOME_InteractiveObject)& anIO = It.Value();
    std::vector<VISU::Prs3d_i*> aPrsList = GetPrs3dList(this, anIO);
    if (!aPrsList.empty()) {
      for (int i = 0, n = aPrsList.size(); i < n; i++) {
        VISU::Prs3d_i* aPrsObject = aPrsList[i];
        if (aPrsObject) {
          VISU::ScalarMap_i* aScalar = dynamic_cast<VISU::ScalarMap_i*>(aPrsObject);
          if (aScalar) {
            if (first) {
              first = false;
              aMin = aScalar->GetMin(); aMax = aScalar->GetMax();
            } else {
              if (aScalar->GetMin() < aMin) aMin = aScalar->GetMin();
              if (aScalar->GetMax() > aMax) aMax = aScalar->GetMax();
            }
          }
        }
      }
    }
  }

  // set the computed range to every selected ScalarMap
  bool update = false;
  for (It.Initialize(aListIO); It.More(); It.Next() ) {
    Handle(SALOME_InteractiveObject)& anIO = It.Value();
    std::vector<VISU::Prs3d_i*> aPrsList = GetPrs3dList(this, anIO);
    if (!aPrsList.empty()) {
      for (int i = 0, n = aPrsList.size(); i < n; i++) {
        VISU::Prs3d_i* aPrsObject = aPrsList[i];
        if(aPrsObject){
          VISU::ScalarMap_i* aScalar = dynamic_cast<VISU::ScalarMap_i*>(aPrsObject);
          if (aScalar) {
            aScalar->SetRange(aMin, aMax);
            RecreateActor(this, aScalar);
            update = true;
          }
        }
      }
    }
  }
  if (update) {
    if (SVTK_ViewWindow* vw = GetActiveViewWindow<SVTK_ViewWindow>(this)) {
      vw->getRenderer()->ResetCameraClippingRange();
      vw->Repaint();
    }
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnFreeScalarBars()
{
  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  SALOME_ListIteratorOfListIO It (aListIO);

  // restore the source range for every ScalarMap
  bool update = false;
  for (; It.More(); It.Next()) {
    Handle(SALOME_InteractiveObject)& anIO = It.Value();
    std::vector<VISU::Prs3d_i*> aPrsList = GetPrs3dList(this, anIO);
    if (!aPrsList.empty()) {
      for (int i = 0, n = aPrsList.size(); i < n; i++) {
        VISU::Prs3d_i* aPrsObject = aPrsList[i];
        if (aPrsObject) {
          VISU::ScalarMap_i* aScalar = dynamic_cast<VISU::ScalarMap_i*>(aPrsObject);
          if (aScalar) {
            aScalar->SetSourceRange();
            RecreateActor(this, aScalar);
            update = true;
          }
        }
      }
    }
  }
  if (update) {
    if (SVTK_ViewWindow* vw = GetActiveViewWindow<SVTK_ViewWindow>(this)) {
      vw->getRenderer()->ResetCameraClippingRange();
      vw->Repaint();
    }
  }
}

//----------------------------------------------------------------------------
void
VisuGUI
::OnTranslatePrs()
{
  if(MYDEBUG) MESSAGE("VisuGUI::OnTranslatePrs");
  VisuGUI_OffsetDlg* aDlg = new VisuGUI_OffsetDlg (this);
  aDlg->exec();
}

void
VisuGUI
::OnArrangeActors()
{
  if(SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(this)){
    ArrangeDlg* aDlg = new ArrangeDlg (GetDesktop(this), this, aViewWindow);
    aDlg->exec();
    delete aDlg;
  }
}


void
VisuGUI
::initialize( CAM_Application* theApp )
{
  MatplotlibMathTextUtilities::Disable();

  SalomeApp_Module::initialize( theApp );

  mySlider = new VisuGUI_Slider( this, getApp()->desktop(), getApp()->selectionMgr() );

  mySweep = new VisuGUI_Sweep( this, getApp()->desktop(), getApp()->selectionMgr() );

  createActions();
  createMenus();
  createToolBars();
  createPopupMenus();
  createPanels();

  // tmp
  action( VISU_ACTOR_SELECTION )->setChecked( true );
}

void
VisuGUI
::createActions()
{
  QPixmap aPixmap;
  QWidget* aParent = application()->desktop();
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  // Create actions

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_IMPORT_MED"));
  createAction( VISU_IMPORT_FROM_FILE, tr("IMPORT_FROM_FILE"), QIcon(aPixmap),
                tr("MEN_IMPORT_FROM_FILE"), "", (Qt::CTRL + Qt::Key_I), aParent, false,
                this, SLOT(OnImportFromFile()));

  createAction( VISU_IMPORT_SAUV, "", QIcon(),
                tr("MEN_IMPORT_SAUV"), "", 0, aParent, false,
                this, SLOT(OnImportFromSauvFile()));

  createAction( VISU_IMPORT_TABLE, "", QIcon(),
                tr("MEN_IMPORT_TABLE"), "", 0, aParent, false,
                this, SLOT(OnImportTableFromFile()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_SCALAR_MAP"));
  createAction( VISU_SCALAR_MAP, tr("MEN_SCALAR_MAP"), QIcon(aPixmap),
                tr("MEN_SCALAR_MAP"), "", 0, aParent, false,
                this, SLOT(OnCreateScalarMap()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_DEFORMED_SHAPE"));
  createAction( VISU_DEFORMED_SHAPE, tr("MEN_DEFORMED_SHAPE"), QIcon(aPixmap),
                tr("MEN_DEFORMED_SHAPE"), "", 0, aParent, false,
                this, SLOT(OnCreateDeformedShape()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_SCALAR_MAP_ON_DEFORMED_SHAPE"));
  createAction( VISU_DEFORMED_SHAPE_AND_SCALAR_MAP, tr("MEN_DEFORMED_SHAPE_AND_SCALAR_MAP"), QIcon(aPixmap),
                tr("MEN_DEFORMED_SHAPE_AND_SCALAR_MAP"), "", 0, aParent, false,
                this, SLOT(OnCreateDeformedShapeAndScalarMap()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_VECTORS"));
  createAction( VISU_VECTORS, tr("MEN_VECTORS"), QIcon(aPixmap),
                tr("MEN_VECTORS"), "", 0, aParent, false,
                this, SLOT(OnCreateVectors()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_ISO_SURFACES"));
  createAction( VISU_ISO_SURFACES, tr("MEN_ISO_SURFACES"), QIcon(aPixmap),
                tr("MEN_ISO_SURFACES"), "", 0, aParent, false,
                this, SLOT(OnCreateIsoSurfaces()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_CUT_PLANES"));
  createAction( VISU_CUT_PLANES, tr("MEN_CUT_PLANES"), QIcon(aPixmap),
                tr("MEN_CUT_PLANES"), "", 0, aParent, false,
                this, SLOT(OnCreateCutPlanes()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_STREAM_LINES"));
  createAction( VISU_STREAM_LINES, tr("MEN_STREAM_LINES"), QIcon(aPixmap),
                tr("MEN_STREAM_LINES"), "", 0, aParent, false,
                this, SLOT(OnCreateStreamLines()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_CUT_LINES"));
  createAction( VISU_CUT_LINES, tr("MEN_CUT_LINES"), QIcon(aPixmap),
                tr("MEN_CUT_LINES"), "", 0, aParent, false,
                this, SLOT(OnCreateCutLines()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_CUT_SEGMENT"));
  createAction( VISU_CUT_SEGMENT, tr("MEN_CUT_SEGMENT"), QIcon(aPixmap),
                tr("MEN_CUT_SEGMENT"), "", 0, aParent, false,
                this, SLOT(OnCreateCutSegment()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_PLOT_3D"));
  createAction( VISU_PLOT_3D, tr("MEN_PLOT_3D"), QIcon(aPixmap),
                tr("MEN_PLOT_3D"), "", 0, aParent, false,
                this, SLOT(OnCreatePlot3D()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_PLOT2D"));
  createAction( VISU_PLOT2D, tr("MEN_CREATE_PLOT2D"), QIcon(aPixmap),
                tr("MEN_CREATE_PLOT2D"), "", 0, aParent, false,
                this, SLOT(OnCreatePlot2dView()));

  createAction( VISU_DELETE_OBJS, tr("MEN_DELETE_OBJS"), QIcon(),
                tr("MEN_DELETE_OBJS"), "", Qt::Key_Delete, aParent, false,
                this, SLOT(OnDeleteObjects()));

  createAction( VISU_SHOW_TABLE, tr("MEN_SHOW_TABLE"), QIcon(),
                tr("MEN_SHOW_TABLE"), "", 0, aParent, false,
                this, SLOT(OnShowTable()));

  createAction( VISU_EDIT_POINTMAP3D, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnShowTablePlot()));

  createAction( VISU_CREATE_CURVES, tr("MEN_CREATE_CURVES"), QIcon(),
                tr("MEN_CREATE_CURVES"), "", 0, aParent, false,
                this, SLOT(OnPlotData()));

  createAction( VISU_EXPORT_TABLE, tr("MEN_EXPORT_TABLE"), QIcon(),
                tr("MEN_EXPORT_TABLE"), "", 0, aParent, false,
                this, SLOT(OnExportTableToFile()));

  createAction( VISU_IMPORT_MED_STRUCTURE, tr("MEN_IMPORT_MED_STRUCTURE"), QIcon(),
                tr("MEN_IMPORT_MED_STRUCTURE"), "", 0, aParent, false,
                this, SLOT(OnImportMedField()));

  createAction( VISU_IMPORT_MED_TIMESTAMP, tr("MEN_IMPORT_MED_TIMESTAMP"), QIcon(),
                tr("MEN_IMPORT_MED_TIMESTAMP"), "", 0, aParent, false,
                this, SLOT(OnImportMedField()));

  createAction( VISU_IMPORT_MED_FIELD, tr("MEN_IMPORT_MED_FIELD"), QIcon(),
                tr("MEN_IMPORT_MED_FIELD"), "", 0, aParent, false,
                this, SLOT(OnImportMedField()));

  createAction( VISU_LOAD_COMPONENT_DATA, tr("MEN_LOAD_COMPONENT_DATA"), QIcon(),
                tr("MEN_LOAD_COMPONENT_DATA"), "", 0, aParent, false,
                this, SLOT(OnLoadComponentData()));

  createAction( VISU_CREATE_PRS, tr("MEN_CREATE_PRS"), QIcon(),
                tr("MEN_CREATE_PRS"), "", 0, aParent, false,
                this, SLOT(OnCreateMesh()));

  createAction( VISU_CREATE_MANY_PRS, tr("MEN_CREATE_MANY_PRS"), QIcon(),
                tr("MEN_CREATE_MANY_PRS"), "", 0, aParent, false,
                this, SLOT(OnCreateManyMesh()));

  createAction( VISU_TRANSLATE_PRS, tr("MEN_TRANSLATE_PRS"), QIcon(),
                tr("MEN_TRANSLATE_PRS"), "", 0, aParent, false,
                this, SLOT(OnTranslatePrs()));

  createAction( VISU_MERGE_SCALAR_BARS, tr("MEN_MERGE_SCALAR_BARS"), QIcon(),
                tr("MEN_MERGE_SCALAR_BARS"), "", 0, aParent, false,
                this, SLOT(OnMergeScalarBars()));

  createAction( VISU_FREE_SCALAR_BARS, tr("MEN_FREE_SCALAR_BARS"), QIcon(),
                tr("MEN_FREE_SCALAR_BARS"), "", 0, aParent, false,
                this, SLOT(OnFreeScalarBars()));

  createAction( VISU_ERASE, tr("MEN_HIDE"), QIcon(),
                tr("MEN_HIDE"), "", 0, aParent, false,
                this, SLOT(OnErasePrs()));

  createAction( VISU_DISPLAY, tr("MEN_SHOW"), QIcon(),
                tr("MEN_SHOW"), "", 0, aParent, false,
                this, SLOT(OnDisplayPrs()));

  createAction( VISU_DISPLAY_ONLY, tr("MEN_DISPLAY_ONLY"), QIcon(),
                tr("MEN_DISPLAY_ONLY"), "", 0, aParent, false,
                this, SLOT(OnDisplayOnlyPrs()));

  createAction( VISU_SHOW_SCALAR_BAR, tr("MEN_SHOW_SCALAR_BAR"), QIcon(),
                tr("MEN_SHOW_SCALAR_BAR"), "", 0, aParent, false,
                this, SLOT(OnShowScalarBar()));

  createAction( VISU_HIDE_SCALAR_BAR, tr("MEN_HIDE_SCALAR_BAR"), QIcon(),
                tr("MEN_HIDE_SCALAR_BAR"), "", 0, aParent, false,
                this, SLOT(OnHideScalarBar()));

  createAction( VISU_COPY_PRS, tr("MEN_COPY_PRS"), QIcon(),
                tr("MEN_COPY_PRS"), "", 0, aParent, false,
                this, SLOT(OnCopyPresentation()));

  createAction( VISU_CURVE_PROPS, tr("MEN_CURVE_PROPS"), QIcon(),
                tr("MEN_CURVE_PROPS"), "", 0, aParent, false,
                this, SLOT(OnCurveProperties()));

  createAction( VISU_CURVE_SCALE, tr("MEN_CURVE_SCALE"), QIcon(),
                tr("MEN_CURVE_SCALE"), "", 0, aParent, false,
                this, SLOT(OnCurveScale()));

  createAction( VISU_EDIT_CONTAINER, tr("MEN_EDIT_CONTAINER"), QIcon(),
                tr("MEN_EDIT_CONTAINER"), "", 0, aParent, false,
                this, SLOT(OnEditContainer()));

  createAction( VISU_CLEAR_CONTAINER, tr("MEN_CLEAR_CONTAINER"), QIcon(),
                tr("MEN_CLEAR_CONTAINER"), "", 0, aParent, false,
                this, SLOT(OnClearContainer()));

  createAction( VISU_SAVE_VIEW_PARAMS, tr("MEN_SAVE_VIEWPARAMS"), QIcon(),
                tr("MEN_SAVE_VIEWPARAMS"), "", 0, aParent, false,
                this, SLOT(OnSaveViewParams()));
  createAction( VISU_SAVE_VIEW_PARAMS_1, tr("MEN_SAVE_VIEWPARAMS"), QIcon(),
                tr("MEN_SAVE_VIEWPARAMS"), "", 0, aParent, false,
                this, SLOT(OnSaveViewParams()));

  createAction( VISU_RESTORE_VIEW_PARAMS, tr("MEN_RESTORE_VIEWPARAMS"), QIcon(),
                tr("MEN_RESTORE_VIEWPARAMS"), "", 0, aParent, false,
                this, SLOT(OnRestoreViewParams()));

  //createAction( VISU_DELETE_VIEW_PARAMS, tr("MEN_DELETE_VIEWPARAMS"), QIcon(),
  //              tr("MEN_DELETE_VIEWPARAMS"), "", 0, aParent, false,
  //              this, SLOT(OnDeleteObjects()));

  createAction( VISU_ARRANGE_ACTORS, tr("MEN_ARRANGE_ACTORS"), QIcon(),
                tr("MEN_ARRANGE_ACTORS"), "", 0, aParent, false,
                this, SLOT(OnArrangeActors()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_POINTS"));
  createAction( VISU_POINTS, tr("MEN_POINTS"), QIcon(aPixmap),
                tr("MEN_POINTS"), "", 0, aParent, false,
                this, SLOT(OnMakePoints()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_WIREFRAME"));
  createAction( VISU_WIREFRAME, tr("MEN_WIREFRAME"), QIcon(aPixmap),
                tr("MEN_WIREFRAME"), "", 0, aParent, false,
                this, SLOT(OnMakeWireframe()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_SURFACE"));
  createAction( VISU_SURFACE, tr("MEN_SURFACE"), QIcon(aPixmap),
                tr("MEN_SURFACE"), "", 0, aParent, false,
                this, SLOT(OnMakeSurface()));

  createAction( VISU_INSIDEFRAME, tr("MEN_INSIDEFRAME"), QIcon(),
                tr("MEN_INSIDEFRAME"), "", 0, aParent, false,
                this, SLOT(OnMakeInsideframe()));

  createAction( VISU_SURFACEFRAME, tr("MEN_SURFACEFRAME"), QIcon(),
                tr("MEN_SURFACEFRAME"), "", 0, aParent, false,
                this, SLOT(OnMakeSurfaceframe()));

  createAction( VISU_SHRINK, tr("MEN_SHRINK"), QIcon(),
                tr("MEN_SHRINK"), "", 0, aParent, false,
                this, SLOT(OnMakeShrink()));

  createAction( VISU_UNSHRINK, tr("MEN_UNSHRINK"), QIcon(),
                tr("MEN_UNSHRINK"), "", 0, aParent, false,
                this, SLOT(OnMakeShrink()));

  createAction( VISU_FEATURE_EDGES, tr("MEN_FEATURE_EDGES"), QIcon(),
                tr("MEN_FEATURE_EDGES"), "", 0, aParent, false,
                this, SLOT(OnMakeFeatureEdges()));

  createAction( VISU_SHADING, tr("MEN_SHADING"), QIcon(),
                tr("MEN_SHADING"), "", 0, aParent, false,
                this, SLOT(OnSetShadingOn()));

  createAction( VISU_NOSHADING, tr("MEN_NOSHADING"), QIcon(),
                tr("MEN_NOSHADING"), "", 0, aParent, false,
                this, SLOT(OnSetShadingOff()));

  createAction( VISU_CELL_COLOR, tr("MEN_CELL_COLOR"), QIcon(),
                tr("MEN_CELL_COLOR"), "", 0, aParent, false,
                this, SLOT(OnChangeColor()));

  createAction( VISU_COLOR, tr("MEN_COLOR"), QIcon(),
                tr("MEN_COLOR"), "", 0, aParent, false,
                this, SLOT(OnChangeColor()));

  createAction( VISU_EDGE_COLOR, tr("MEN_EDGE_COLOR"), QIcon(),
                tr("MEN_EDGE_COLOR"), "", 0, aParent, false,
                this, SLOT(OnChangeWireframeColor()));

  createAction( VISU_OPACITY, tr("MEN_TRANSPARENCY"), QIcon(),
                tr("MEN_TRANSPARENCY"), "", 0, aParent, false,
                this, SLOT(OnChangeOpacity()));

  createAction( VISU_LINE_WIDTH, tr("MEN_LINE_WIDTH"), QIcon(),
                tr("MEN_LINE_WIDTH"), "", 0, aParent, false,
                this, SLOT(OnChangeLines()));

  createAction( VISU_POINT_MARKER, tr("MEN_POINT_MARKER"), QIcon(),
                tr("MEN_POINT_MARKER"), "", 0, aParent, false,
                this, SLOT(OnChangePointMarker()));

  createAction( VISU_SHRINK_FACTOR, tr("MEN_SHRINK_FACTOR"), QIcon(),
                tr("MEN_SHRINK_FACTOR"), "", 0, aParent, false,
                this, SLOT(OnChangeShrinkFactor()));


  createAction( VISU_EDIT_SCALARMAP, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditScalarMap()));

  createAction( VISU_EDIT_DEFORMEDSHAPE, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditDeformedShape()));

  createAction( VISU_EDIT_CUTPLANES, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditCutPlanes()));

  createAction( VISU_EDIT_CUTLINES, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditCutLines()));

  createAction( VISU_EDIT_CUTSEGMENT, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditCutSegment()));

  createAction( VISU_EDIT_ISOSURFACES, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditIsoSurfaces()));

  createAction( VISU_EDIT_VECTORS, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditVectors()));

  createAction( VISU_EDIT_STREAMLINES, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditStreamLines()));

  createAction( VISU_EDIT_PLOT3D, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditPlot3D()));

  createAction( VISU_EDIT_DEFORMEDSHAPEANDSCALARMAP, tr("MEN_EDIT_PRS"), QIcon(),
                tr("MEN_EDIT_PRS"), "", 0, aParent, false,
                this, SLOT(OnEditDeformedShapeAndScalarMap()));

  createAction( VISU_CREATE_TABLE, tr("MEN_CREATE_TABLE"), QIcon(),
                tr("MEN_CREATE_TABLE"), "", 0, aParent, false,
                this, SLOT(OnCreateTable()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_SWEEP_PANEL"));
  createAction( VISU_SWEEP, tr("MEN_SWEEP"), QIcon(aPixmap),
                tr("MEN_SWEEP"), "", 0, aParent, false,
                this, SLOT(OnSweep()));

  createAction( VISU_CLIPPING, tr("MEN_CLIPPING"), QIcon(),
                tr("MEN_CLIPPING"), "", 0, aParent, false,
                this, SLOT(OnClippingPlanes()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_TIMEANIMATION"));
  createAction( VISU_PARALLELANIMATION, tr("MEN_PARALLEL_ANIMATION"), QIcon(aPixmap),
                tr("MEN_PARALLEL_ANIMATION"), "", 0, aParent, false,
                this, SLOT(OnParallelTimeAnimation()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_TIMEANIMATION"));
  createAction( VISU_SUCCCESSIVEANIMATION, tr("MEN_SUCCCESSIVE_ANIMATION"), QIcon(aPixmap),
                tr("MEN_SUCCCESSIVE_ANIMATION"), "", 0, aParent, false,
                this, SLOT(OnSucccessiveTimeAnimation()));

  createAction( VISU_POINT_EVOLUTION, tr("MEN_POINT_EVOLUTION"), QIcon(),
                tr("MEN_POINT_EVOLUTION"), "", 0, aParent, false,
                this, SLOT(OnPointEvolution()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_ERASE_ALL"));
  createAction( VISU_ERASE_ALL, tr("MEN_ERASE_ALL"), QIcon(aPixmap),
                tr("MEN_ERASE_ALL"), "", 0, aParent, false,
                this, SLOT(OnEraseAll()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_SELECTION_PANEL"));
  createAction( VISU_SELECTION_PANEL, tr("MEN_SELECTION_PANEL"), QIcon(aPixmap),
                tr("MEN_SELECTION_PANEL"), "", 0, aParent, false,
                this, SLOT(OnToggleSelectionPanel()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_ACTOR_SELECTION"));
  createAction( VISU_ACTOR_SELECTION, tr("MEN_ACTOR_SELECTION"), QIcon(aPixmap),
                tr("MEN_ACTOR_SELECTION"), "", 0, aParent, true,
                this, SLOT(OnSwitchSelectionMode()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_CELL_SELECTION"));
  createAction( VISU_CELL_SELECTION, tr("MEN_CELL_SELECTION"), QIcon(aPixmap),
                tr("MEN_CELL_SELECTION"), "", 0, aParent, true,
                this, SLOT(OnSwitchSelectionMode()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_POINT_SELECTION"));
  createAction( VISU_POINT_SELECTION, tr("MEN_POINT_SELECTION"), QIcon(aPixmap),
                tr("MEN_POINT_SELECTION"), "", 0, aParent, true,
                this, SLOT(OnSwitchSelectionMode()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_GAUSS_POINT_SELECTION"));
  createAction( VISU_GAUSS_POINT_SELECTION, tr("MEN_GAUSS_POINT_SELECTION"), QIcon(aPixmap),
                tr("MEN_GAUSS_POINT_SELECTION"), "", 0, aParent, true,
                this, SLOT(OnSwitchSelectionMode()));

  // Definition of the actions for the "Navigation" tool bar
  registerAction( VISU_SLIDER_PANEL, mySlider->toggleViewAction() );
  registerAction( VISU_SWEEP_PANEL, mySweep->toggleViewAction() );

  createAction( VISU_SHOW_ANIMATION, tr("MEN_SHOW_ANIMATION"), QIcon(),
                tr("MEN_SHOW_ANIMATION"), "", 0, aParent, false,
                this, SLOT(OnShowAnimation()));

  createAction( VISU_SHOW_EVOLUTION, tr("MEN_SHOW_EVOLUTION"), QIcon(),
                tr("MEN_SHOW_EVOLUTION"), "", 0, aParent, false,
                this, SLOT(OnShowEvolution()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_PLOT_3D"));
  createAction( VISU_PLOT3D_FROM_CUTPLANE, tr("MEN_PLOT3D_FROM_CUTPLANE"), QIcon(aPixmap),
                tr("MEN_PLOT3D_FROM_CUTPLANE"), "", 0, aParent, false,
                this, SLOT(OnPlot3dFromCutPlane()));

  createAction( VISU_MANAGE_CACHE, tr("MEN_MANAGE_CACHE"), QIcon(),
                tr("MEN_MANAGE_CACHE"), "", 0, aParent, false,
                this, SLOT( OnManageCache() ) );

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_MULTIPR_VIEW_FULL"));
  createAction( VISU_MULTIPR_FULL_RES, tr("MEN_MULTIPR_VIEW_FULL_RES"), QIcon(aPixmap),
                tr("MEN_MULTIPR_VIEW_FULL_RES"), "", 0, aParent, false,
                this, SLOT(OnMultiprViewFullRes()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_MULTIPR_VIEW_MEDIUM"));
  createAction( VISU_MULTIPR_MED_RES, tr("MEN_MULTIPR_VIEW_MEDIUM_RES"), QIcon(aPixmap),
                tr("MEN_MULTIPR_VIEW_MEDIUM_RES"), "", 0, aParent, false,
                this, SLOT(OnMultiprViewMediumRes()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_MULTIPR_VIEW_LOW"));
  createAction( VISU_MULTIPR_LOW_RES, tr("MEN_MULTIPR_VIEW_LOW_RES"), QIcon(aPixmap),
                tr("MEN_MULTIPR_VIEW_LOW_RES"), "", 0, aParent, false,
                this, SLOT(OnMultiprViewLowRes()));

  aPixmap = aResourceMgr->loadPixmap("VISU",tr("ICON_MULTIPR_VIEW_HIDE"));
  createAction( VISU_MULTIPR_HIDE, tr("MEN_MULTIPR_VIEW_HIDE"), QIcon(aPixmap),
                tr("MEN_MULTIPR_VIEW_HIDE"), "", 0, aParent, false,
                this, SLOT(OnMultiprViewHide()));

  createAction( VISU_FILE_INFO, tr("MEN_FILE_INFO"), QIcon(),
                tr("MEN_FILE_INFO"), "", 0, aParent, false,
                this, SLOT(OnFileInfo()));

  createAction( VISU_EXPORT_MED, tr("VISU_MEN_EXPORT_MED"), QIcon(),
                tr("VISU_MEN_EXPORT_MED"), "", 0, aParent, false,
                this, SLOT(OnExportMED()));

  createAction( VISU_FILTERSCALARS, tr("MEN_FILTER_SCALARS"), QIcon(),
                tr("MEN_FILTER_SCALARS"), "", 0, aParent, false,
                this, SLOT(OnFilterScalars()));

  createAction( VISU_VALUES_LABELING, tr("MEN_VALUES_LABELING"), QIcon(),
                tr("MEN_VALUES_LABELING"), "", 0, aParent, true,
                this, SLOT(OnValuesLabeling()));

  createAction( VISU_VALUES_LABELING_PARAMS, tr("VISU_VALUES_LABELING_PARAMS"), QIcon(),
                tr("VISU_VALUES_LABELING_PARAMS"), "", 0, aParent, false,
                this, SLOT(OnValuesLabelingParams()));

  createAction(VISU_ARCQUAD_MODE, tr("MEN_VISU_ARCQUAD_MODE"), QIcon(),
               tr("MEN_VISU_ARCQUAD_MODE"), "",0, aParent, true,
               this, SLOT(OnArcQuadMode()));

  createAction(VISU_LINEQUAD_MODE, tr("MEN_VISU_LINEQUAD_MODE"),QIcon(),
               tr("MEN_VISU_LINEQUAD_MODE"), "",0, aParent, true,
               this, SLOT(OnLineQuadMode()));

  createAction(VISU_SHOW_DEVIATION, tr("MEN_VISU_SHOW_DEVIATION"),QIcon(),
               tr("MEN_VISU_SHOW_DEVIATION"), "",0, aParent, true,
               this, SLOT(OnShowDeviation()));

}

void
VisuGUI
::createMenus()
{
  // Add actions to menus
  int aMenuId;
  aMenuId = createMenu( tr( "MEN_DESK_FILE" ), -1 );
  createMenu( separator(), aMenuId, -1, 10 );

  int anImportId = createMenu( tr( "MEN_IMPORT" ), aMenuId, -1, 10 );
  createMenu( VISU_IMPORT_FROM_FILE, anImportId ); // import from file
  createMenu( VISU_IMPORT_SAUV, anImportId ); // import sauv file
  createMenu( VISU_IMPORT_TABLE, anImportId ); // import table

  aMenuId = createMenu( tr( "MEN_VISUALIZATION" ), -1, -1, 30 );
  createMenu( VISU_SCALAR_MAP, aMenuId, 10 ); // scalar map
  createMenu( VISU_DEFORMED_SHAPE, aMenuId, 10 ); // deformed shape
  createMenu( VISU_VECTORS, aMenuId, 10 ); // vectors
  createMenu( VISU_ISO_SURFACES, aMenuId, 10 ); // iso surfaces
  createMenu( VISU_CUT_PLANES, aMenuId, 10 ); // cut planes
  createMenu( VISU_CUT_LINES, aMenuId, 10 ); // cut lines
  createMenu( VISU_CUT_SEGMENT, aMenuId, 10 ); // cut segment
  createMenu( VISU_STREAM_LINES, aMenuId, 10 ); // stream lines
  createMenu( VISU_PLOT_3D, aMenuId, 10 ); // Plot3d
  createMenu( VISU_DEFORMED_SHAPE_AND_SCALAR_MAP, aMenuId, 10 ); // scalar map on deformed shape

  aMenuId = createMenu( tr( "MEN_REPRESENTATION" ), -1, -1, 30 );
  int parentId =
    createMenu( tr( "MEN_DISPLAY_SELECTION" ), aMenuId, 10 ); // display selection
  createMenu( VISU_POINTS, parentId, 10 ); //   points
  createMenu( VISU_WIREFRAME, parentId, 10 ); //   wireframe
  createMenu( VISU_SURFACE, parentId, 10 ); //   surface
  createMenu( VISU_ERASE_ALL, aMenuId, 10 ); // erase all

  parentId = createMenu( tr( "MEN_2D_QUADRATIC_MODE" ), aMenuId, 10 ); // 2D quadratic mode
  createMenu( VISU_LINEQUAD_MODE, parentId, 10 ); //   lines
  createMenu( VISU_ARCQUAD_MODE, parentId, 10 ); //   arcs

}

void
VisuGUI
::createToolBars()
{
  int aToolId = createTool( tr("TOOL_IMPORT") );
  createTool( VISU_IMPORT_FROM_FILE, aToolId );

  aToolId = createTool(tr("TOOL_VISUALISATION"));
  createTool( VISU_SCALAR_MAP, aToolId );
  createTool( VISU_DEFORMED_SHAPE, aToolId );
  createTool( VISU_VECTORS, aToolId );
  createTool( VISU_ISO_SURFACES, aToolId );
  createTool( VISU_CUT_PLANES, aToolId );
  createTool( VISU_CUT_LINES, aToolId );
  createTool( VISU_CUT_SEGMENT, aToolId );
  createTool( VISU_STREAM_LINES, aToolId );
  createTool( VISU_PLOT_3D, aToolId );
  createTool( VISU_DEFORMED_SHAPE_AND_SCALAR_MAP, aToolId );

  aToolId = createTool(tr("TOOL_REPRESENTATION"));
  createTool( VISU_POINTS, aToolId );
  createTool( VISU_WIREFRAME, aToolId );
  createTool( VISU_SURFACE, aToolId );
  createTool( VISU_ERASE_ALL, aToolId );

  aToolId = createTool(tr("TOOL_SELECTION"));
  createTool( VISU_SELECTION_PANEL, aToolId );
  createTool( separator(), aToolId );
  createTool( VISU_ACTOR_SELECTION, aToolId );
  createTool( VISU_CELL_SELECTION, aToolId );
  createTool( VISU_POINT_SELECTION, aToolId );
  createTool( VISU_GAUSS_POINT_SELECTION, aToolId );

  aToolId = createTool( tr( "TOOL_NAVIGATION" ) );
  createTool( VISU_SLIDER_PANEL, aToolId );
  createTool( separator(), aToolId );
  createTool( VISU_SWEEP_PANEL, aToolId );
}

void
VisuGUI
::createPopupMenus()
{
  // Prepare popup menus
  QtxPopupMgr* mgr = popupMgr();

  // VISU root commands
  mgr->insert( action( VISU_IMPORT_FROM_FILE ), -1, -1, -1 ); // import MED file
  mgr->insert( action( VISU_IMPORT_SAUV ), -1, -1, -1 ); // import SAUV file
  mgr->insert( action( VISU_IMPORT_TABLE ), -1, -1, -1 ); // import tables from file
  mgr->insert( action( VISU_PLOT2D ), -1, -1, -1 ); // create Plot2d View

  //mgr->insert( action( VISU_IMPORT_MED ), -1, -1, -1 ); // import MED structure from MED module

  // create
  mgr->insert( action( VISU_SCALAR_MAP ), -1, -1, -1 ); // scalar bar
  mgr->insert( action( VISU_ISO_SURFACES ), -1, -1, -1 ); // iso surface
  mgr->insert( action( VISU_CUT_PLANES ), -1, -1, -1 ); // cut planes
  mgr->insert( action( VISU_CUT_LINES ), -1, -1, -1 ); // cut lines
  mgr->insert( action( VISU_CUT_SEGMENT ), -1, -1, -1 ); // cut segment
  mgr->insert( action( VISU_DEFORMED_SHAPE ), -1, -1, -1 ); // deformed shape
  mgr->insert( action( VISU_VECTORS ), -1, -1, -1 ); // vectors
  mgr->insert( action( VISU_STREAM_LINES ), -1, -1, -1 ); // stream lines
  mgr->insert( action( VISU_PLOT_3D ), -1, -1, -1 ); // Plot3d
  mgr->insert( action( VISU_DEFORMED_SHAPE_AND_SCALAR_MAP ), -1, -1, -1 ); // scalar map on deformed shape

  mgr->insert( action( VISU_CREATE_PRS ), -1, -1, -1 ); // create presentation
  mgr->insert( action( VISU_CREATE_MANY_PRS ), -1, -1, -1 ); // create presentations

  mgr->insert( action( VISU_CREATE_TABLE ), -1, -1, -1 ); // create table

  // edit
  mgr->insert( action( VISU_EDIT_SCALARMAP ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_DEFORMEDSHAPE ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_CUTPLANES ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_CUTLINES ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_CUTSEGMENT ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_ISOSURFACES ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_VECTORS ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_STREAMLINES ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_PLOT3D ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_DEFORMEDSHAPEANDSCALARMAP ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_CONTAINER ), -1, -1, -1 );
  mgr->insert( action( VISU_EDIT_POINTMAP3D ), -1, -1, -1 );

  mgr->insert( action( VISU_FILTERSCALARS ), -1, -1, -1 );

  // copy
  mgr->insert( action( VISU_COPY_PRS ), -1, -1, -1 );

  // delete
  mgr->insert( action( VISU_CLEAR_CONTAINER ), -1, -1, -1 );

  // labeling
  mgr->insert( separator(), -1, -1, -1 );
  mgr->insert( action( VISU_VALUES_LABELING ), -1, -1, -1 );
  mgr->insert( action( VISU_VALUES_LABELING_PARAMS ), -1, -1, -1 );
  mgr->insert( separator(), -1, -1, -1 );

  // table commands
  mgr->insert( action( VISU_SHOW_TABLE ), -1, -1, -1 ); // show table
  mgr->insert( action( VISU_CREATE_CURVES ), -1, -1, -1 ); // create curves
  mgr->insert( action( VISU_EXPORT_TABLE ), -1, -1, -1 ); // export table

  mgr->insert( separator(), -1, -1, -1 );

  mgr->insert( action( VISU_ERASE ), -1, -1, -1 ); // erase
  mgr->insert( action( VISU_DISPLAY ), -1, -1, -1 ); // display
  mgr->insert( action( VISU_DISPLAY_ONLY ), -1, -1, -1 ); // display only

  mgr->insert( action( VISU_HIDE_SCALAR_BAR ), -1, -1, -1 ); // hide scalar bar
  mgr->insert( action( VISU_SHOW_SCALAR_BAR ), -1, -1, -1 ); // show scalar bar

  // "Representation" submenu
  int parentId = mgr->insert( tr( "MEN_REPRESENTATION" ), -1, -1 );
  mgr->insert( action( VISU_POINTS )      , parentId, -1, -1 ); // points
  mgr->insert( action( VISU_WIREFRAME )   , parentId, -1, -1 ); // wireframe
  mgr->insert( action( VISU_SURFACE )     , parentId, -1, -1 ); // surface
  mgr->insert( action( VISU_INSIDEFRAME ) , parentId, -1, -1 ); // insideframe
  mgr->insert( action( VISU_SURFACEFRAME ), parentId, -1, -1 ); // surfaceframe
  mgr->insert( action( VISU_FEATURE_EDGES ), parentId, -1, -1 ); // feature edges
  mgr->insert( action( VISU_SHRINK )      , parentId, -1, -1 ); // shrink
  mgr->insert( action( VISU_UNSHRINK )    , parentId, -1, -1 ); // unshrink
  mgr->insert( separator(), parentId, -1, -1 );
  mgr->insert( action( VISU_SHADING )     , parentId, -1, -1 ); // shading
  mgr->insert( action( VISU_NOSHADING )   , parentId, -1, -1 ); // noshading

  //"2D Quadratic" submenu
  parentId = mgr->insert( tr( "MEN_2D_QUADRATIC_MODE" ), -1, -1 );
  mgr->insert( action( VISU_LINEQUAD_MODE ), parentId, -1, -1 ); // line representation
  mgr->insert( action( VISU_ARCQUAD_MODE ), parentId, -1, -1 ); // arc representation

  // "Properties" submenu
  parentId = mgr->insert( tr( "MEN_PROPERTIES" ), -1, -1 );
  mgr->insert( action( VISU_CELL_COLOR ),    parentId, -1, -1 ); // cell color
  mgr->insert( action( VISU_EDGE_COLOR ),    parentId, -1, -1 ); // edge color
  mgr->insert( action( VISU_COLOR )     ,    parentId, -1, -1 ); // color
  mgr->insert( action( VISU_OPACITY )   ,    parentId, -1, -1 ); // opacity
  mgr->insert( action( VISU_LINE_WIDTH ),    parentId, -1, -1 ); // line width
  mgr->insert( action( VISU_POINT_MARKER ),  parentId, -1, -1 ); // point marker
  mgr->insert( action( VISU_SHRINK_FACTOR ), parentId, -1, -1 ); // shrink factor

  mgr->insert( separator(), -1, -1, -1 );

  // Other presentations management
  mgr->insert( action( VISU_TRANSLATE_PRS ), -1, -1, -1 );
  mgr->insert( action( VISU_CLIPPING ), -1, -1, -1 );

  mgr->insert( action( VISU_MERGE_SCALAR_BARS ), -1, -1, -1 );
  mgr->insert( action( VISU_FREE_SCALAR_BARS ), -1, -1, -1 );

  mgr->insert( action( VISU_SWEEP ), -1, -1, -1 );
  mgr->insert( action( VISU_PARALLELANIMATION ), -1, -1, -1 );
  mgr->insert( action( VISU_SUCCCESSIVEANIMATION ), -1, -1, -1 );

  mgr->insert( separator(), -1, -1, -1 );

  mgr->insert( action( VISU_POINT_EVOLUTION ), -1, -1, -1 );

  mgr->insert( separator(), -1, -1, -1 );

  // view parameters commands
  mgr->insert( action( VISU_SAVE_VIEW_PARAMS ), -1, -1, -1 ); // save view params
  mgr->insert( action( VISU_SAVE_VIEW_PARAMS_1 ), -1, -1, -1 ); // save view params
  mgr->insert( action( VISU_RESTORE_VIEW_PARAMS ), -1, -1, -1 ); // restore view params
  //mgr->insert( action( VISU_DELETE_VIEW_PARAMS ), -1, -1, -1 ); // delete view params

  mgr->insert( action( VISU_ARRANGE_ACTORS ), -1, -1, -1 ); // arrange actors

  mgr->insert( action( VISU_CURVE_PROPS ), -1, -1, -1 ); // curve properties
  mgr->insert( action( VISU_CURVE_SCALE ), -1, -1, -1 ); // curve scale
  //Show deviation command on curve
  mgr->insert( action( VISU_SHOW_DEVIATION ), -1, -1, -1 ); // deviation

  mgr->insert( action( VISU_MULTIPR_FULL_RES ), -1, -1, -1 );
  mgr->insert( action( VISU_MULTIPR_MED_RES ), -1, -1, -1 );
  mgr->insert( action( VISU_MULTIPR_LOW_RES ), -1, -1, -1 );
  mgr->insert( action( VISU_MULTIPR_HIDE ), -1, -1, -1 );

  mgr->insert( action( VISU_FILE_INFO ), -1, -1, -1 );
  mgr->insert( action( VISU_EXPORT_MED ), -1, -1, -1 );

  // Rules

  QString aPrsAll ("'VISU::TSCALARMAP' 'VISU::TISOSURFACES' 'VISU::TDEFORMEDSHAPE' 'VISU::TCUTPLANES' "
                   "'VISU::TCUTLINES' 'VISU::TCUTSEGMENT' 'VISU::TVECTORS' 'VISU::TSTREAMLINES' 'VISU::TPLOT3D' "
                   "'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP' 'VISU::TCOLOREDPRS3DHOLDER'");
  QString aSimplePrsAll ("'VISU::TSCALARMAP' 'VISU::TISOSURFACES' 'VISU::TDEFORMEDSHAPE' 'VISU::TCUTPLANES' "
                         "'VISU::TCUTLINES' 'VISU::TCUTSEGMENT' 'VISU::TVECTORS' 'VISU::TSTREAMLINES' 'VISU::TPLOT3D' "
                         "'VISU::TSCALARMAPONDEFORMEDSHAPE'");

  // VISU root commands
  QString aRule = "client='ObjectBrowser' and selcount=1 and type='VISU::TVISUGEN'";
  mgr->setRule( action( VISU_IMPORT_FROM_FILE ), aRule );
  mgr->setRule( action( VISU_IMPORT_SAUV ), aRule );
  mgr->setRule( action( VISU_IMPORT_TABLE ), aRule );
  mgr->setRule( action( VISU_PLOT2D ), aRule );

  // timestamp commands
  aRule = "client='ObjectBrowser' and selcount=1 and type in {'VISU::TTIMESTAMP' 'VISU::TFIELD'}";
  mgr->setRule( action( VISU_SCALAR_MAP ), aRule );
  mgr->setRule( action( VISU_ISO_SURFACES ), aRule );
  mgr->setRule( action( VISU_CUT_PLANES ), aRule );
  mgr->setRule( action( VISU_CUT_LINES ), aRule );
  mgr->setRule( action( VISU_CUT_SEGMENT ), aRule );
  mgr->setRule( action( VISU_PLOT_3D ), aRule );
  aRule += " and nbComponents>1";
  mgr->setRule( action( VISU_DEFORMED_SHAPE ), aRule );
  mgr->setRule( action( VISU_VECTORS ), aRule );
  mgr->setRule( action( VISU_STREAM_LINES ), aRule );
  mgr->setRule( action( VISU_DEFORMED_SHAPE_AND_SCALAR_MAP ), aRule );

  // display/erase commands
  QString andInvisible = " and (((not isVisible) and isActiveView) or (not isActiveView))";
  QString aTableOrContHide = "(($type in {'VISU::TTABLE' 'VISU::TPOINTMAP3D' 'VISU::TCONTAINER'}) and isPlot2dViewer and hasCurves and isThereAreVisibleCurves)";
  QString aTableOrContShow = "(($type in {'VISU::TTABLE' 'VISU::TPOINTMAP3D' 'VISU::TCONTAINER'}) and isPlot2dViewer and hasCurves and isThereAreHiddenCurves)";
  QString orCurveVisible   = "or (type='VISU::TCURVE' and isVisible)";
  QString orCurveInvisible = "or (type='VISU::TCURVE'" + andInvisible + ")";
  QString aPrsVisible   = "(($type in {'VISU::TMESH' 'VISU::TPOINTMAP3D' 'VISU::TGAUSSPOINTS'" + aPrsAll + "}) and isVisible and (not isPlot2dViewer))";
  QString aPrsInvisible = "(($type in {'VISU::TMESH' 'VISU::TPOINTMAP3D' 'VISU::TGAUSSPOINTS'" + aPrsAll + "})" + andInvisible + " and (not isPlot2dViewer))";
  QString aComponent = "( selcount=1 and canBeDisplayed and isVisuComponent )";

  QString anEraseRule = "( selcount>0 and ({true} in $canBeDisplayed or type in {'VISU::TCONTAINER' 'VISU::TCURVE'}) and (" + aPrsVisible +
    " or (   (" + aTableOrContHide + orCurveVisible + "))) ) or " + aComponent;

  QString aDisplayRule = "( selcount>0 and ({true} in $canBeDisplayed or type in {'VISU::TCONTAINER' 'VISU::TCURVE'}) and (" + aPrsInvisible +
    " or (   (" + aTableOrContShow + orCurveInvisible + "))) ) or " + aComponent;

  QString aDOnlyRule = "( selcount>0 and ({true} in $canBeDisplayed or type in {'VISU::TCONTAINER' 'VISU::TCURVE'}) and (($type in {'VISU::TMESH' 'VISU::TPOINTMAP3D' 'VISU::TGAUSSPOINTS'" + aPrsAll + "})"
    " or (   ((type='VISU::TTABLE' and nbChildren>0) or (type='VISU::TCONTAINER' and hasCurves) or"
    " (type='VISU::TCURVE')))) ) or" + aComponent;

  QString aScalarBarHideRule = "( selcount>0 and ($type in {'VISU::TMESH' 'VISU::TPOINTMAP3D' 'VISU::TGAUSSPOINTS'" + aPrsAll + "}) and (isScalarMapAct=1  or isGaussPtsAct=1 ) and " + aPrsVisible + " and isScalarBarVisible )";
  QString aScalarBarShowRule = "( selcount>0 and ($type in {'VISU::TMESH' 'VISU::TPOINTMAP3D' 'VISU::TGAUSSPOINTS'" + aPrsAll + "}) and ( isScalarMapAct=1 or isGaussPtsAct=1 ) and " + aPrsVisible + " and (not isScalarBarVisible) )";

  mgr->setRule( action( VISU_ERASE ), anEraseRule );
  mgr->setRule( action( VISU_DISPLAY ), aDisplayRule );
  mgr->setRule( action( VISU_DISPLAY_ONLY ), aDOnlyRule );

  mgr->setRule( action( VISU_HIDE_SCALAR_BAR ), aScalarBarHideRule );
  mgr->setRule( action( VISU_SHOW_SCALAR_BAR ), aScalarBarShowRule );

  aRule = "selcount>0 and $type in {'VISU::TMESH' 'VISU::TPOINTMAP3D' 'VISU::TGAUSSPOINTS'" + aPrsAll + "}";
  mgr->setRule( action( VISU_TRANSLATE_PRS ), aRule );

  aRule = "selcount>1 and $type in {'VISU::TMESH' " + aPrsAll + "}";
  mgr->setRule( action( VISU_MERGE_SCALAR_BARS ), aRule );
  mgr->setRule( action( VISU_FREE_SCALAR_BARS ), aRule );

  aRule = "selcount=1 and $type in { " + aSimplePrsAll + "}";
  mgr->setRule( action( VISU_FILTERSCALARS ), aRule );

  // labeling

  aRule = "hasActor=1 and selcount=1 and $type in { 'VISU::TGAUSSPOINTS'"  + aPrsAll + " }";
  mgr->setRule( action( VISU_VALUES_LABELING ), aRule, QtxPopupMgr::VisibleRule );
  mgr->setRule( action( VISU_VALUES_LABELING ), "{true} in $isValuesLabeled", QtxPopupMgr::ToggleRule );
  mgr->setRule( action( VISU_VALUES_LABELING_PARAMS ), aRule, QtxPopupMgr::VisibleRule );

  //2D quadratic
  aRule = "isVisible and hasActor=1 and selcount=1 and $type in {'VISU::TMESH' 'VISU::TSCALARMAP' 'VISU::TDEFORMEDSHAPE' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP'}";
  mgr->setRule (action(VISU_ARCQUAD_MODE), aRule , QtxPopupMgr::VisibleRule);
  mgr->setRule (action(VISU_LINEQUAD_MODE),aRule , QtxPopupMgr::VisibleRule);

  mgr->setRule (action(VISU_ARCQUAD_MODE), aRule + " and quadratic2DMode='VISU::ARCS'", QtxPopupMgr::ToggleRule);
  mgr->setRule (action(VISU_LINEQUAD_MODE),aRule + " and quadratic2DMode='VISU::LINES'", QtxPopupMgr::ToggleRule);


  // view parameters
  aRule = "selcount=1 and type='VISU::TVIEW3D' and activeView='VTKViewer'";
  mgr->setRule( action( VISU_SAVE_VIEW_PARAMS_1 ), aRule );
  mgr->setRule( action( VISU_SAVE_VIEW_PARAMS ), "client='VTKViewer' and selcount=0 and activeView='VTKViewer'" );
  mgr->setRule( action( VISU_RESTORE_VIEW_PARAMS ), "selcount=1 and type='VISU::TVIEW3D' and activeView='VTKViewer'" );
  //mgr->setRule( action( VISU_DELETE_VIEW_PARAMS ), "selcount=1 and type='VISU::TVIEW3D'", true );

  mgr->setRule( action( VISU_ARRANGE_ACTORS ), "$client in {'VTKViewer' 'SVTK'} and selcount=0" );

  // 3D presentations commands
  QString aPrsType    = " and $type in {'VISU::TMESH' 'VISU::TGAUSSPOINTS' " + aPrsAll + "}";
  QString aPrsType2    = " and $type in {'VISU::TMESH' 'VISU::TPOINTMAP3D'" + aPrsAll + "}";
  QString anInsideType = " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP' 'VISU::TDEFORMEDSHAPE' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP'}";
  //QString aSurfFrameType   = " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP''VISU::TDEFORMEDSHAPE' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP'}";
  QString aSurfFrameType   = " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP''VISU::TDEFORMEDSHAPE' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP' 'VISU::TCUTPLANES' 'VISU::TISOSURFACES' 'VISU::TPLOT3D' }";
  //QString aPrsType    = " and $type in {'VISU::TMESH' " + aPrsAll + "}";
  QString aSurfType   = " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP' 'VISU::TSCALARMAP' 'VISU::TISOSURFACES' 'VISU::TDEFORMEDSHAPE' 'VISU::TCUTPLANES' "
                        " 'VISU::TPLOT3D' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP' 'VISU::TPOINTMAP3D'}";
  QString aShrinkType = " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP' 'VISU::TDEFORMEDSHAPE' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP' 'VISU::TPOINTMAP3D'}";
  QString aLineType   = " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP' 'VISU::TSCALARMAP' 'VISU::TISOSURFACES' 'VISU::TDEFORMEDSHAPE' "
                        "'VISU::TCUTPLANES' 'VISU::TCUTLINES' 'VISU::TCUTSEGMENT' 'VISU::TSTREAMLINES' 'VISU::TPLOT3D' 'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP' 'VISU::TPOINTMAP3D'}";

  QString aFeatureEdgesType =  " and $type in {'VISU::TMESH' 'VISU::TSCALARMAP''VISU::TDEFORMEDSHAPE' 'VISU::TSCALARMAPONDEFORMEDSHAPE'}";

  aRule = "selcount=1";
  mgr->setRule( action( VISU_CLIPPING ), aRule + aPrsType );
  aPrsType = " and $type in {'VISU::TMESH' " + aPrsAll + "}";

  // "Representation" submenu
  QString aNotPoints = " and $representation in {'VISU::WIREFRAME' 'VISU::SHADED' 'VISU::INSIDEFRAME' 'VISU::SURFACEFRAME' 'VISU::FEATURE_EDGES'}";
  QString aNotWirefr = " and $representation in {'VISU::POINT' 'VISU::SHADED' 'VISU::INSIDEFRAME' 'VISU::SURFACEFRAME' 'VISU::FEATURE_EDGES'}";
  QString aNotSurfac = " and $representation in {'VISU::POINT' 'VISU::WIREFRAME' 'VISU::INSIDEFRAME' 'VISU::SURFACEFRAME' 'VISU::FEATURE_EDGES'}";
  QString aNotInside = " and $representation in {'VISU::POINT' 'VISU::WIREFRAME' 'VISU::SHADED' 'VISU::SURFACEFRAME' 'VISU::FEATURE_EDGES'}";
  QString aNotSurffr = " and $representation in {'VISU::POINT' 'VISU::WIREFRAME' 'VISU::SHADED' 'VISU::INSIDEFRAME' 'VISU::FEATURE_EDGES'}";
  QString aNotFeatureEdges =  " and $representation in {'VISU::POINT' 'VISU::WIREFRAME' 'VISU::SHADED' 'VISU::INSIDEFRAME' 'VISU::SURFACEFRAME'}";

  mgr->setRule( action( VISU_POINTS )      , aRule + aPrsType + aNotPoints );
  mgr->setRule( action( VISU_WIREFRAME )   , aRule + aPrsType2 + aNotWirefr );
  mgr->setRule( action( VISU_SURFACE )     , aRule + aSurfType + aNotSurfac );
  mgr->setRule( action( VISU_INSIDEFRAME ) , aRule + anInsideType + aNotInside );
  mgr->setRule( action( VISU_SURFACEFRAME ), aRule + aSurfFrameType + aNotSurffr );
  mgr->setRule( action( VISU_FEATURE_EDGES ), aRule + aFeatureEdgesType + aNotFeatureEdges );

  mgr->setRule( action( VISU_SHRINK )  , aRule + aShrinkType + " and isShrunk=0" );
  mgr->setRule( action( VISU_UNSHRINK ), aRule + aShrinkType + " and isShrunk=1" );

  mgr->setRule( action( VISU_SHADING )  , aRule + " and isScalarMapAct=1 and isShading=0" );
  mgr->setRule( action( VISU_NOSHADING ), aRule + " and isScalarMapAct=1 and isShading=1" );

  // "Properties" submenu
  mgr->setRule( action( VISU_CELL_COLOR ), aRule + " and type='VISU::TMESH' and representation='VISU::SURFACEFRAME'" );
  mgr->setRule( action( VISU_EDGE_COLOR ), aRule + aSurfFrameType + " and representation='VISU::SURFACEFRAME'" );
  mgr->setRule( action( VISU_COLOR ), aRule + " and ((type='VISU::TMESH'"
               " and $representation in {'VISU::POINT' 'VISU::WIREFRAME' 'VISU::SHADED' 'VISU::INSIDEFRAME' 'VISU::FEATURE_EDGES'}) "
               "or (type='VISU::TDEFORMEDSHAPE' and hasActor=1))" );
  mgr->setRule( action( VISU_OPACITY ), aRule + " and hasActor=1" );
  mgr->setRule( action( VISU_LINE_WIDTH ), aRule + aLineType + " and hasActor=1 and representation!='VISU::POINT'" );
  mgr->setRule( action( VISU_POINT_MARKER ), aRule + aLineType + " and hasActor=1 and representation='VISU::POINT'" );
  mgr->setRule( action( VISU_SHRINK_FACTOR ), aRule + aShrinkType + " and isShrunk=1" );

  QString aCurveAll = "'VISU::TTABLE' 'VISU::TCURVE' 'VISU::TCONTAINER' 'VISU::POINTMAP3D'";

  // copy presentation command
  aRule = "selcount=1 and ($type in {" + aPrsAll + "})";
  mgr->setRule( action( VISU_COPY_PRS ), aRule );

  // Curve commands
  aRule = "selcount=1 and type='VISU::TCURVE'";
  mgr->setRule( action( VISU_CURVE_PROPS ), aRule );
  mgr->setRule( action( VISU_CURVE_SCALE ), "selcount > 0 and type='VISU::TCURVE'");

  // Table Commands
  aRule = "selcount=1 and ( type='VISU::TPOINTMAP3D' or type='VISU::TTABLE')";
  mgr->setRule( action( VISU_SHOW_TABLE ), aRule );
  mgr->setRule( action( VISU_CREATE_CURVES ), aRule );
  mgr->setRule( action( VISU_EXPORT_TABLE ), aRule );

  // curve container commands
  aRule = "client='ObjectBrowser' and selcount=1 and type='VISU::TCONTAINER'";
  mgr->setRule( action( VISU_EDIT_CONTAINER ), aRule );
  mgr->setRule( action( VISU_CLEAR_CONTAINER ), aRule );

  mgr->setRule( action( VISU_EDIT_SCALARMAP ),
                "selcount=1 and type='VISU::TSCALARMAP'" );
  mgr->setRule( action( VISU_EDIT_DEFORMEDSHAPE ),
                "selcount=1 and type='VISU::TDEFORMEDSHAPE'" );
  mgr->setRule( action( VISU_EDIT_DEFORMEDSHAPEANDSCALARMAP ),
                "selcount=1 and $type in {'VISU::TSCALARMAPONDEFORMEDSHAPE' 'VISU::TDEFORMEDSHAPEANDSCALARMAP'}" );
  mgr->setRule( action( VISU_EDIT_CUTPLANES ),
                "selcount=1 and type='VISU::TCUTPLANES'" );
  mgr->setRule( action( VISU_EDIT_CUTLINES ),
                "selcount=1 and type='VISU::TCUTLINES'" );
  mgr->setRule( action( VISU_EDIT_CUTSEGMENT ),
                "selcount=1 and type='VISU::TCUTSEGMENT'" );
  mgr->setRule( action( VISU_EDIT_ISOSURFACES ),
                "selcount=1 and type='VISU::TISOSURFACES'" );
  mgr->setRule( action( VISU_EDIT_VECTORS ),
                "selcount=1 and type='VISU::TVECTORS'" );
  mgr->setRule( action( VISU_EDIT_STREAMLINES ),
                "selcount=1 and type='VISU::TSTREAMLINES'" );
  mgr->setRule( action( VISU_EDIT_PLOT3D ),
                "selcount=1 and type='VISU::TPLOT3D'" );
  mgr->setRule( action( VISU_EDIT_POINTMAP3D ),
                "selcount=1 and type='VISU::TPOINTMAP3D'" );

  aRule = "selcount=1 and $type in {'VISU::TCUTLINES' 'VISU::TCUTSEGMENT'} and nbNamedChildren=0";
  mgr->setRule( action( VISU_CREATE_TABLE ), aRule );

  aRule = "selcount=1 and ($type in {" + aSimplePrsAll + "}) and isFieldPrs=0";
  mgr->setRule( action( VISU_SWEEP ), aRule );

  aRule = "client='ObjectBrowser' and selcount>0";
  aRule += " and $type in {'VISU::TFIELD'}";
  aRule += " and nbTimeStamps>1";
  aRule += " and activeView in {'VTKViewer' 'SVTK'}";
  mgr->setRule( action( VISU_PARALLELANIMATION ), aRule );

  aRule = "client='ObjectBrowser'";
  aRule += " and (selcount=1 and nbTimeStamps>1 or selcount>1 and nbTimeStamps>0)";
  aRule += " and $type in {'VISU::TFIELD'}";
  aRule += " and activeView in {'VTKViewer' 'SVTK'}";
  mgr->setRule( action( VISU_SUCCCESSIVEANIMATION ), aRule );

  aRule = "client='ObjectBrowser' and selcount>0";
  aRule += " and $type in {'VISU::TFIELD'}";
  aRule += " and medEntity='NODE_ENTITY'";
  aRule += " and nbTimeStamps>1";
  mgr->setRule( action( VISU_POINT_EVOLUTION ), aRule );

  aRule = "client='ObjectBrowser' and $type in {'VISU::TENTITY' 'VISU::TFAMILY' 'VISU::TGROUP'}";
  mgr->setRule( action( VISU_CREATE_PRS ), aRule + " and selcount=1" );
  mgr->setRule( action( VISU_CREATE_MANY_PRS ), aRule + " and selcount>1" );
  mgr->setRule( action( VISU_EDIT_PLOT3D ),
                "selcount=1 and type='VISU::TPLOT3D'" );

  //aRule = "client='ObjectBrowser' and type='MEDFIELD'";
  //mgr->setRule( action( VISU_IMPORT_MED ), aRule, true );

  aRule = "client='ObjectBrowser' and selcount>=1 and $type in {'VISU::TPART'} and ";
  {
    QString aCustomRule = aRule + "fullResolution=1 and resolutionState!='F'";
    mgr->setRule( action( VISU_MULTIPR_FULL_RES ), aCustomRule );
  }
  {
    QString aCustomRule = aRule + "mediumResolution=1 and resolutionState!='M'";
    mgr->setRule( action( VISU_MULTIPR_MED_RES ), aCustomRule );
  }
  {
    QString aCustomRule = aRule + "lowResolution=1 and resolutionState!='L'";
    mgr->setRule( action( VISU_MULTIPR_LOW_RES ), aCustomRule );
  }
  {
    QString aCustomRule = aRule + "resolutionState!='H'";
    mgr->setRule( action( VISU_MULTIPR_HIDE), aCustomRule );
  }

  mgr->setRule( action( VISU_FILE_INFO ), "selcount=1 and type in {'VISU::TRESULT'}" );
  mgr->setRule( action( VISU_EXPORT_MED ), "selcount=1 and type in {'VISU::TRESULT'}" );

  aRule = "isPlot2dViewer and isVisible and selcount > 0 and type='VISU::TCURVE' and hasDeviation";
  mgr->setRule (action(VISU_SHOW_DEVIATION), aRule , QtxPopupMgr::VisibleRule);
  aRule = "isPlot2dViewer and isVisible and selcount > 0 and type='VISU::TCURVE' and hasDeviation and isDeviationDisplayed";
  mgr->setRule (action(VISU_SHOW_DEVIATION), aRule , QtxPopupMgr::ToggleRule);
}

void VisuGUI::createPanels()
{
  myPanels[SelectionPanelId]      = new VisuGUI_SelectionPanel( this );
  myPanels[FeatureEdgesPanelId]   = new VisuGUI_FeatureEdgesPanel( this );
  myPanels[ClippingPlanesPanelId] = new VisuGUI_ClippingPanel( this );

  VisuGUI_Panel* aPanel;
  foreach( aPanel, myPanels ) {
    getApp()->desktop()->addDockWidget( Qt::RightDockWidgetArea, aPanel );
    aPanel->setVisible( false );
  }
}

//***************************************************************************
void VisuGUI::contextMenuPopup( const QString& theClient, QMenu* theMenu, QString& theTitle )
{
  SalomeApp_Module::contextMenuPopup(theClient, theMenu, theTitle);

  //TODO: object Browser DONE
  SUIT_DataBrowser* ob = getApp()->objectBrowser();
  bool isOBClient = (ob && theClient == ob->popupClientType());

  LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
  SALOME_ListIO aListIO;
  aSelectionMgr->selectedObjects(aListIO);

  if (aListIO.Extent() < 1) return;

  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));

  // Check if "Merge Scalar Range" can be applied
  int nbPrs = 0;
  SALOME_ListIteratorOfListIO It (aListIO);
  for (; It.More(); It.Next()) {
    Handle(SALOME_InteractiveObject)& anIO = It.Value();

    if (!anIO.IsNull() && anIO->hasEntry()) {
      _PTR(SObject) aSObject = aCStudy->FindObjectID(anIO->getEntry());
      if ( !aSObject )
        continue;

      if ( !isOBClient ) {
        QString aName = QString( aSObject->GetName().c_str() );
        while ( aName.at( aName.length() - 1 ) == ' ' ) // Remove extraspaces in Name of popup
          aName.remove( (aName.length() - 1), 1 );
        theTitle = aName;
      }

      CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(aSObject);
      if (CORBA::is_nil(aCORBAObject)) {
        VISU::VISUType aType = VISU::Storable::SObject2Type( aSObject );
        if (aType == VISU::TTIMESTAMP) {
          _PTR(ChildIterator) anIter = aCStudy->NewChildIterator(aSObject);
          _PTR(SObject) aPrs;
          for (; anIter->More(); anIter->Next()) {
            aPrs = anIter->Value();
            if (!aPrs)
              continue;
            std::vector<VISU::Prs3d_i*> aSubList = GetPrs3dList(this, aPrs);
            nbPrs += aSubList.size();
          }
        }
      }
    }
  }
  if (nbPrs > 1) {
    theMenu->addSeparator();
    theMenu->addAction( action( VISU_MERGE_SCALAR_BARS ) );
    theMenu->addAction( action( VISU_FREE_SCALAR_BARS  ) );
  }

  // Check if some curves selected (for bug PAL10611)
  bool isCurves = false;
  bool isOneCutPlane = false;
  SALOME_ListIteratorOfListIO It1 (aListIO);
  for (; It1.More() && !isCurves; It1.Next()) {
    Handle(SALOME_InteractiveObject)& anIO = It1.Value();
    if (!anIO.IsNull() && anIO->hasEntry()) {
      _PTR(SObject) aSObject = aCStudy->FindObjectID(anIO->getEntry());
      if ( !aSObject )
        continue;
      VISU::VISUType aType = VISU::Storable::SObject2Type( aSObject );
      if (aType == VISU::TCURVE) {
        isCurves = true;
      }  else {
        VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(this), anIO->getEntry());
        if(VISU::Prs3d_i* aPrsObject = VISU::GetPrs3dFromBase(anObjectInfo.myBase)){
          if (aPrsObject->GetType() == VISU::TCUTPLANES) {
            if (VISU::CutPlanes_i* aCutPrs = dynamic_cast<VISU::CutPlanes_i*>(aPrsObject))
              isOneCutPlane = aCutPrs->GetNbPlanes() == 1;
          }
        }
      }
    }
  }

  if (isOneCutPlane)
    theMenu->addAction( action( VISU_PLOT3D_FROM_CUTPLANE ) );

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
  Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
  _PTR(SObject) aSObject = aSelectionItem.myObjectInfo.mySObject;
  Storable::TRestoringMap aMap = Storable::GetStorableMap(aSObject);
  CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObject);

  if(VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase){
    if(aBase->GetType() == VISU::TCOLOREDPRS3DCACHE)
      theMenu->addAction( action( VISU_MANAGE_CACHE ) );
  }

  theMenu->addSeparator();

  // Check if some (nb > 0) removable objects selected
  if (isOBClient && IsRemovableSelected(this)) {
    theMenu->addAction( action( VISU_DELETE_OBJS ) );
  }
  if (isCurves) {
    theMenu->removeAction( action( myDisplay ) );
    theMenu->removeAction( action( myErase ) );
    theMenu->removeAction( action( myDisplayOnly ) );
    theMenu->removeAction( action( myEraseAll ) );
  }

  // Check single selection
  if (aListIO.Extent() != 1)
    return;

  // Check if the object's data is loaded
  _PTR(SComponent) aSComp = aSObject->GetFatherComponent();
  std::string aCompIOR;
  if (!aSComp->ComponentIOR(aCompIOR)) { // not loaded
    //std::string aCompDataType = aSComp->ComponentDataType();
    theMenu->addAction( action( VISU_LOAD_COMPONENT_DATA ) ); // "Load Component Data"
  }

  VISU::VISUType aType = VISU::Storable::RestoringMap2Type( aMap );
  if (aType == VISU::TANIMATION) {
    theMenu->addAction( action( VISU_SHOW_ANIMATION ) );
  } else if (aType == VISU::TEVOLUTION) {
    theMenu->addAction( action( VISU_SHOW_EVOLUTION ) );
  } else if (isOBClient) {
    if (IsSObjectTable(aSObject)) {
      /*      action( VISU_SHOW_TABLE )->addTo(theMenu);
      theMenu->addAction( action( VISU_CREATE_CURVES ) );
      theMenu->addAction( action( VISU_EXPORT_TABLE ) );
      theMenu->addSeparator();
      action( VISU_DELETE_OBJS )->addTo(theMenu);*/// dmv - code was moved up
    } else {
      if (!CORBA::is_nil(anObject)) {
        SALOME_MED::MED_var aMED = SALOME_MED::MED::_narrow(anObject);
        if (!CORBA::is_nil(aMED.in())) {
          theMenu->addAction( action( VISU_IMPORT_MED_STRUCTURE ) ); // "Import Structure"
        }

        SALOME_MED::FIELD_var aField = SALOME_MED::FIELD::_narrow(anObject);
        if (!CORBA::is_nil(aField)) {
          theMenu->addAction( action( VISU_IMPORT_MED_TIMESTAMP ) ); // "Import TimeStamp"
        }
      } else {
        _PTR(SObject) aSFather1 = aSObject->GetFather();
        if (aSFather1) {
          std::string aValue = aSFather1->GetName();
          if (strcmp(aValue.c_str(), "MEDFIELD") == 0) {
            theMenu->addAction( action( VISU_IMPORT_MED_FIELD ) ); // "Import Field"
          }
        }
      }
    }
  }
}


//***************************************************************************
void
VisuGUI
::windows( QMap<int, int>& theMap ) const
{
  theMap.clear();
  theMap.insert( SalomeApp_Application::WT_ObjectBrowser, Qt::LeftDockWidgetArea );
  theMap.insert( SalomeApp_Application::WT_PyConsole, Qt::BottomDockWidgetArea );
}


void
VisuGUI
::viewManagers( QStringList& theList ) const
{
  theList.clear();
  // append SVTK viewer only if there is no opened VVTK
//   SUIT_ViewManager* vm = getApp()->getViewManager( VVTK_Viewer::Type(), false );
//   if( vm && vm->getViewsCount()>0 )
//     theList.append( VVTK_Viewer::Type() );
//   else
    theList.append( SVTK_Viewer::Type() );
}


QString
VisuGUI
::engineIOR() const
{
  CORBA::String_var anIOR = GetVisuGen(this)->GetID();
  return QString(anIOR.in());
}


bool
VisuGUI
::activateModule( SUIT_Study* theStudy )
{
  bool aResult = SalomeApp_Module::activateModule( theStudy );

  studyActivated();
  setMenuShown( true );
  setToolShown( true );

  // import Python module that manages VISU plugins (need to be here because SalomePyQt API uses active module)
  PyGILState_STATE gstate = PyGILState_Ensure();
  PyObject* pluginsmanager=PyImport_ImportModuleNoBlock((char*)"salome_pluginsmanager");
  if(pluginsmanager==NULL)
    PyErr_Print();
  else
    {
      PyObject* result=PyObject_CallMethod( pluginsmanager, (char*)"initialize", (char*)"isss",1,"visu",tr("MEN_VISUALIZATION").toStdString().c_str(),tr("VISU_PLUGINS_OTHER").toStdString().c_str());
      if(result==NULL)
        PyErr_Print();
      Py_XDECREF(result);
    }
  PyGILState_Release(gstate);
  // end of GEOM plugins loading

  // Reset actions accelerator keys
  QList<QKeySequence> shortcuts;
  shortcuts.append( QKeySequence(Qt::CTRL + Qt::Key_I) );
  action(VISU_IMPORT_FROM_FILE)->setShortcuts(shortcuts);

  action(VISU_DELETE_OBJS)->setEnabled(true); // Delete: Key_Delete

  ((VisuGUI_ClippingPanel*) myPanels[ClippingPlanesPanelId])->init();
  GetVisuGen( this )->GetClippingPlaneMgr().SetStudy(GetCStudy(dynamic_cast<SalomeApp_Study*>(theStudy)),true);
  emit moduleActivated();

  return aResult;
}


bool
VisuGUI
::deactivateModule( SUIT_Study* theStudy )
{
  setMenuShown( false );
  setToolShown( false );

  // Unset actions accelerator keys
  action(VISU_IMPORT_FROM_FILE)->setShortcuts(QKeySequence::UnknownKey); // Import: CTRL + Key_I

  action(VISU_DELETE_OBJS)->setEnabled(false); // Delete: Key_Delete

  bool aResult = SalomeApp_Module::deactivateModule( theStudy );

  emit moduleDeactivated();

  return aResult;
}

LightApp_Selection*
VisuGUI
::createSelection() const
{
  return new VisuGUI_Selection( (SalomeApp_Module*)this );
}

void
VisuGUI
::processPresentationCreated( VISU::Prs3d_i* thePrs )
{
  if( thePrs )
  {
    QString anEntry = thePrs->GetEntry().c_str();
    if( anEntry.isEmpty() )
      if( VISU::ColoredPrs3d_i* aColoredPrs = dynamic_cast<VISU::ColoredPrs3d_i*>( thePrs ) )
        anEntry = aColoredPrs->GetHolderEntry().c_str();

    QStringList anEntryList( anEntry );
    if( getApp() )
      getApp()->browseObjects( anEntryList );

    emit presentationCreated( thePrs );
  }
}

//******************************************************************
void VisuGUI::studyActivated()
{
  GetVisuGen(this)->SetCurrentStudy(GetDSStudy(GetCStudy(GetAppStudy(this))));

}

/*
extern "C" {
  CAM_Module*
  createModule()
  {
    return new VisuGUI();
  }
}
*/

void VisuGUI::createPreferences()
{
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  // TAB: General
  int genTab   = addPreference( tr( "VISU_PREF_GENERAL" ) );
  // Quantities with individual precision settings
  int precGroup = addPreference( tr( "VISU_PREF_GROUP_PRECISION" ), genTab );
  setPreferenceProperty( precGroup, "columns", 2 );

  // Default precision used by some input fieklds and data visualization code
  // Moved here from "Representation" tab to have all precisions in the same place
  int point_precision = addPreference( tr( "VISU_PREF_visual_data_precision" ), precGroup,
                                       LightApp_Preferences::IntSpin, "VISU", "visual_data_precision" );
  setPreferenceProperty( point_precision, "min", -16 );
  setPreferenceProperty( point_precision, "max", 16 );
  setPreferenceProperty( point_precision, "step", 1 );
  
  const int nbQuantities = 5;
  int precs[nbQuantities], ii = 0;
  precs[ii++] = addPreference( tr( "VISU_PREF_length_precision" ), precGroup,
                            LightApp_Preferences::IntSpin, "VISU", "length_precision" );  
  precs[ii++] = addPreference( tr( "VISU_PREF_angle_precision" ), precGroup,
                            LightApp_Preferences::IntSpin, "VISU", "angle_precision" );
  precs[ii++] = addPreference( tr( "VISU_PREF_len_tol_precision" ), precGroup,
                            LightApp_Preferences::IntSpin, "VISU", "len_tol_precision" );
  precs[ii++] = addPreference( tr( "VISU_PREF_parametric_precision" ), precGroup,
                            LightApp_Preferences::IntSpin, "VISU", "parametric_precision" );
  precs[ii  ] = addPreference( tr( "VISU_PREF_memory_precision" ), precGroup,
                            LightApp_Preferences::IntSpin, "VISU", "memory_precision" );
  
  // Set property for precision value for spinboxes
  for ( ii = 0; ii < nbQuantities; ii++ ){
    setPreferenceProperty( precs[ii], "min", ii == 2 ? 0 : -14 );
    setPreferenceProperty( precs[ii], "max", ii == 2 ? 3 : 14 );
    setPreferenceProperty( precs[ii], "precision", 2 );
  }   


  // TAB: "MED import"
  int importTab = addPreference( tr( "MED import" ) );
  //printf( "++++++++++++++++++ Tab index %d\n", importTab );

  //int importFrame = addPreference( tr( "MED FRAME" ), importTab );
  //printf( "Frame index %d\n", importFrame );

  // group: "MED files import"
  int importGr = addPreference( tr( "MED files import" ), importTab );
  //printf( "Group index %d\n", importGr );
  setPreferenceProperty( importGr, "columns", 2 );

  addPreference( tr( "Use build progress" ), importGr, LightApp_Preferences::Bool, "VISU", "use_build_progress" );
  addPreference( tr( "Full MED loading" ), importGr, LightApp_Preferences::Bool, "VISU", "full_med_loading" );
  addPreference( tr( "Build at once" ), importGr, LightApp_Preferences::Bool, "VISU", "build_at_once" );
  addPreference( tr( "Build fields" ), importGr, LightApp_Preferences::Bool, "VISU", "build_fields" );
  addPreference( tr( "Build min/max" ), importGr, LightApp_Preferences::Bool, "VISU", "build_min_max" );
  addPreference( tr( "Build groups" ), importGr, LightApp_Preferences::Bool, "VISU", "build_groups" );
  addPreference( tr( "Close dialog at finish" ), importGr, LightApp_Preferences::Bool, "VISU", "close_at_finish" );

  // TAB: "Scalar Bar"
  int sbarTab = addPreference( tr( "VISU_SCALAR_BAR" ) );

  int rangeGr = addPreference( tr( "VISU_SCALAR_RANGE" ), sbarTab );
  setPreferenceProperty( rangeGr, "columns", 2 );

  int mode = addPreference( tr( "VISU_SCALAR_MODE" ), rangeGr, LightApp_Preferences::Selector, "VISU", "scalar_bar_mode" );
  QStringList modes;
  modes.append( tr( "VISU_MODULUS" ) );
  modes.append( tr( "VISU_COMPONENT" ) + tr("1") );
  modes.append( tr( "VISU_COMPONENT" ) + tr("2") );
  modes.append( tr( "VISU_COMPONENT" ) + tr("3") );
  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  indices.append( 3 );
  setPreferenceProperty( mode, "strings", modes );
  setPreferenceProperty( mode, "indexes", indices );

  addPreference( tr( "VISU_LOGARITHMIC_SCALING" ), rangeGr, LightApp_Preferences::Bool, "VISU", "scalar_bar_logarithmic" );

  int rangetype = addPreference( tr( "VISU_RANGE_TO_USE" ), rangeGr, LightApp_Preferences::Selector, "VISU", "scalar_range_type" );
  QStringList types;
  types.append( tr( "VISU_FIELD_RANGE" ) );
  types.append( tr( "VISU_IMPOSED_RANGE" ) );
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( rangetype, "strings", types );
  setPreferenceProperty( rangetype, "indexes", indices );

  int gaussMetric = addPreference( tr( "VISU_GAUSS_METRIC" ), rangeGr, LightApp_Preferences::Selector, "VISU", "scalar_gauss_metric" );
  QStringList metrics;
  metrics.append( tr( "VISU_GAUSS_METRIC_AVERAGE" ) );
  metrics.append( tr( "VISU_GAUSS_METRIC_MINIMUM" ) );
  metrics.append( tr( "VISU_GAUSS_METRIC_MAXIMUM" ) );
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  setPreferenceProperty( gaussMetric, "strings", metrics );
  setPreferenceProperty( gaussMetric, "indexes", indices );

  int imposedGr = addPreference( tr( "VISU_MINMAX_IMPOSED_RANGE" ), sbarTab );
  setPreferenceProperty( imposedGr, "columns", 4 );

  int min = addPreference( tr( "VISU_MIN" ), imposedGr, LightApp_Preferences::Double, "VISU", "scalar_range_min" );
  int max = addPreference( tr( "VISU_MAX" ), imposedGr, LightApp_Preferences::Double, "VISU", "scalar_range_max" );
  setPreferenceProperty( min, "decimals", qAbs( aResourceMgr->integerValue( "VISU", "visual_data_precision", 0 ) ) );
  setPreferenceProperty( max, "decimals", qAbs( aResourceMgr->integerValue( "VISU", "visual_data_precision", 0 ) ) );

  // group: "Font"
  int fontGr = addPreference( tr( "VISU_FONT" ), sbarTab );
  setPreferenceProperty( fontGr, "columns", 2 );

  addVtkFontPref( tr( "VISU_TITLE" ), fontGr, "scalar_bar_title_font", false );
  addPreference( tr( "VISU_TITLE" ), fontGr, LightApp_Preferences::Color, "VISU", "scalar_bar_title_color" );

  addVtkFontPref( tr( "VISU_LABELS" ), fontGr, "scalar_bar_label_font", false );
  addPreference( tr( "VISU_LABELS" ), fontGr, LightApp_Preferences::Color, "VISU", "scalar_bar_label_color" );

  // group: "Colors & Labels"
  int colorsLabelsGr = addPreference( tr( "VISU_COLORS_AND_LABELS" ), sbarTab );
  setPreferenceProperty( colorsLabelsGr, "columns", 4 );

  int numcol = addPreference( tr( "VISU_NB_COLORS" ), colorsLabelsGr,
                              LightApp_Preferences::IntSpin, "VISU", "scalar_bar_num_colors" );
  setPreferenceProperty( numcol, "min", 2 );
  setPreferenceProperty( numcol, "max", 256 );

  addPreference( tr( "VISU_IS_UNITS" ), colorsLabelsGr,
                 LightApp_Preferences::Bool, "VISU", "scalar_bar_display_units" );

  int numlab = addPreference( tr( "VISU_NB_LABELS" ), colorsLabelsGr,
                              LightApp_Preferences::IntSpin, "VISU", "scalar_bar_num_labels" );
  setPreferenceProperty( numlab, "min", 2 );
  setPreferenceProperty( numlab, "max", 65 );

  int lPrec = addPreference( tr( "PRECISION" ), colorsLabelsGr,
    LightApp_Preferences::IntSpin, "VISU", "scalar_bar_label_precision" );

  setPreferenceProperty( lPrec, "min", 1 );
  setPreferenceProperty( lPrec, "max", 100 );

  int frame = addPreference( "", sbarTab, LightApp_Preferences::Frame, "", "" );
  //setPreferenceProperty( frame, "orientation", "vertical" );
  setPreferenceProperty( frame, "columns", "2" );

  // group: "Orientation"
  int orientGr = addPreference( tr( "VISU_ORIENTATION" ), frame, LightApp_Preferences::GroupBox );
  setPreferenceProperty( orientGr, "columns", 2 );
  int orient = addPreference( tr( "VISU_ORIENTATION" ), orientGr,
                              LightApp_Preferences::Selector, "VISU", "scalar_bar_orientation" );
  QStringList orients;
  orients.append( tr( "VISU_VERTICAL" ) );
  orients.append( tr( "VISU_HORIZONTAL" ) );
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( orient, "strings", orients );
  setPreferenceProperty( orient, "indexes", indices );

  // group: "Scalar bars default position"
  int scalarBarGr = addPreference( tr( "Scalar bars default position" ), frame, LightApp_Preferences::GroupBox );
  addPreference( tr( "Arrange Scalar Bars" ), scalarBarGr, LightApp_Preferences::Bool, "VISU", "scalar_bars_default_position" );


  // group: "Origin & Size Vertical"
  //int psSzFrame = addPreference( "", sbarTab, LightApp_Preferences::Frame, "", "" );
  //setPreferenceProperty( psSzFrame, "orientation", "horizontal" );
  //setPreferenceProperty( psSzFrame, "columns", "2" );

  int posVSizeGr = addPreference( tr( "VISU_ORIGIN_AND_SIZE" ) + " " + tr( "VISU_VERTICAL" ),
                                  frame, LightApp_Preferences::GroupBox );
  setPreferenceProperty( posVSizeGr, "columns", 2 );
  int xv = addPreference( tr( "VISU_X" ), posVSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_vertical_x" );
  int yv = addPreference( tr( "VISU_Y" ), posVSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_vertical_y" );

  int wv = addPreference( tr( "VISU_WIDTH" ), posVSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_vertical_width" );
  int hv = addPreference( tr( "VISU_HEIGHT" ), posVSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_vertical_height" );

  int twv = addPreference( tr( "VISU_TITLE_SIZE" ), posVSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_vertical_title_size" );

  int lwv = addPreference( tr( "VISU_LABEL_SIZE" ), posVSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_vertical_label_size" );

  int bwv = addPreference( tr( "VISU_BAR_WIDTH" ), posVSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_vertical_bar_width" );
  int bhv = addPreference( tr( "VISU_BAR_HEIGHT" ), posVSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_vertical_bar_height" );

  setPreferenceProperty( xv, "step", 0.1 );
  setPreferenceProperty( yv, "step", 0.1 );
  setPreferenceProperty( wv, "step", 0.1 );
  setPreferenceProperty( hv, "step", 0.1 );
  setPreferenceProperty( xv, "min", 0.0 );
  setPreferenceProperty( yv, "min", 0.0 );
  setPreferenceProperty( wv, "min", 0.0 );
  setPreferenceProperty( hv, "min", 0.0 );
  setPreferenceProperty( twv, "min", 0 );
  setPreferenceProperty( twv, "special", "auto" );
  setPreferenceProperty( lwv, "min", 0 );
  setPreferenceProperty( lwv, "special", "auto" );
  setPreferenceProperty( bwv, "min", 0 );
  setPreferenceProperty( bwv, "special", "auto" );
  setPreferenceProperty( bhv, "min", 0 );
  setPreferenceProperty( bhv, "special", "auto" );
  setPreferenceProperty( xv, "max", 1.0 );
  setPreferenceProperty( yv, "max", 1.0 );
  setPreferenceProperty( wv, "max", 1.0 );
  setPreferenceProperty( hv, "max", 1.0 );
  setPreferenceProperty( twv, "max", 100 );
  setPreferenceProperty( lwv, "max", 100 );
  setPreferenceProperty( bwv, "max", 100 );
  setPreferenceProperty( bhv, "max", 100 );

  // group: "Origin & Size Horizontal"
  int posHSizeGr = addPreference( tr( "VISU_ORIGIN_AND_SIZE" ) + " " + tr( "VISU_HORIZONTAL" ),
                                  frame, LightApp_Preferences::GroupBox );
  setPreferenceProperty( posHSizeGr, "columns", 2 );

  int xh = addPreference( tr( "VISU_X" ), posHSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_horizontal_x" );
  int yh = addPreference( tr( "VISU_Y" ), posHSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_horizontal_y" );

  int wh = addPreference( tr( "VISU_WIDTH" ), posHSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_horizontal_width" );
  int hh = addPreference( tr( "VISU_HEIGHT" ), posHSizeGr,
                          LightApp_Preferences::DblSpin, "VISU", "scalar_bar_horizontal_height" );

  int twh = addPreference( tr( "VISU_TITLE_SIZE" ), posHSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_horizontal_title_size" );

  int lwh = addPreference( tr( "VISU_LABEL_SIZE" ), posHSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_horizontal_label_size" );

  int bwh = addPreference( tr( "VISU_BAR_WIDTH" ), posHSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_horizontal_bar_width" );
  int bhh = addPreference( tr( "VISU_BAR_HEIGHT" ), posHSizeGr,
                          LightApp_Preferences::IntSpin, "VISU", "scalar_bar_horizontal_bar_height" );

  setPreferenceProperty( xh, "step", 0.1 );
  setPreferenceProperty( yh, "step", 0.1 );
  setPreferenceProperty( wh, "step", 0.1 );
  setPreferenceProperty( hh, "step", 0.1 );
  setPreferenceProperty( xh, "min", 0.0 );
  setPreferenceProperty( yh, "min", 0.0 );
  setPreferenceProperty( wh, "min", 0.0 );
  setPreferenceProperty( hh, "min", 0.0 );
  setPreferenceProperty( twh, "min", 0 );
  setPreferenceProperty( twh, "special", "auto" );
  setPreferenceProperty( lwh, "min", 0 );
  setPreferenceProperty( lwh, "special", "auto" );
  setPreferenceProperty( bwh, "min", 0 );
  setPreferenceProperty( bwh, "special", "auto" );
  setPreferenceProperty( bhh, "min", 0 );
  setPreferenceProperty( bhh, "special", "auto" );
  setPreferenceProperty( xh, "max", 1.0 );
  setPreferenceProperty( yh, "max", 1.0 );
  setPreferenceProperty( wh, "max", 1.0 );
  setPreferenceProperty( hh, "max", 1.0 );
  setPreferenceProperty( twh, "max", 100 );
  setPreferenceProperty( lwh, "max", 100 );
  setPreferenceProperty( bwh, "max", 100 );
  setPreferenceProperty( bhh, "max", 100 );

  // TAB: "CutLines"
  int cutLineTab = addPreference( tr( "VISU_CUT_LINES_SEGMENT" ) );

  // group: "CutLines preferences"
  int cutLineGr = addPreference( tr( "VISU_CUT_LINES_SEGMENT_PREF" ), cutLineTab );
  setPreferenceProperty( cutLineGr, "columns", 1 );
  addPreference( tr( "Show preview" ), cutLineGr, LightApp_Preferences::Bool, "VISU", "show_preview" );
  addPreference( tr( "Invert all curves" ), cutLineGr, LightApp_Preferences::Bool, "VISU", "invert_all_curves" );
  addPreference( tr( "Use absolute length" ), cutLineGr, LightApp_Preferences::Bool, "VISU", "use_absolute_length" );
  addPreference( tr( "Generate data table" ), cutLineGr, LightApp_Preferences::Bool, "VISU", "generate_data_table" );
  addPreference( tr( "Generate curves" ), cutLineGr, LightApp_Preferences::Bool, "VISU", "generate_curves" );

  // TAB: "StreamLines"
  int streamLinesTab = addPreference( tr( "VISU_STREAM_LINES" ) );

  // group: "StreamLines preferences"
  int streamLinesGr = addPreference( tr( "VISU_STREAM_LINES_PREF" ), streamLinesTab );
  setPreferenceProperty( cutLineGr, "columns", 1 );
  int usedPoints = addPreference( tr( "VISU_STREAM_LINES_USED_POINTS" ), streamLinesGr,
                                  LightApp_Preferences::DblSpin, "VISU", "stream_lines_used_points" );
  setPreferenceProperty( usedPoints, "min",  0 );
  setPreferenceProperty( usedPoints, "max",  1 );
  setPreferenceProperty( usedPoints, "step", 0.1 );

  // TAB: "Sweep, Animation, 3D Cache"
  int animationTab = addPreference( tr( "Sweep, Animation" ) );
  {
    // group: "3D Cache System prefereces"
    {
      int cacheGroup = addPreference( tr( "VISU_3DCACHE_PREFERENCES" ), animationTab );
      setPreferenceProperty( cacheGroup, "columns", 2 );

      int memory_mode = addPreference( tr( "VISU_MEMORY_MODE" ), cacheGroup,
                                       LightApp_Preferences::Selector, "VISU", "cache_memory_mode" );

      modes.clear();
      modes.append( tr( "VISU_MINIMAL" ) );
      modes.append( tr( "VISU_LIMITED" ) );
      setPreferenceProperty( memory_mode, "strings", modes );

      indices.clear();
      indices.append( 0 );
      indices.append( 1 );
      setPreferenceProperty( memory_mode, "indexes", indices );

      int memory_limit = addPreference( tr( "VISU_MEMORY_LIMIT" ), cacheGroup,
                                        LightApp_Preferences::IntSpin, "VISU", "cache_memory_limit" );
      setPreferenceProperty( memory_limit, "min", 1 );
      setPreferenceProperty( memory_limit, "max", 5000 );
      setPreferenceProperty( memory_limit, "step", 10 );
    }

    // group: "Animation preferences"
    {
      int animationGroup = addPreference( tr( "VISU_ANIMATION_PREFERENCES" ), animationTab );
      setPreferenceProperty( animationGroup, "columns", 1 );

      int speed = addPreference( tr( "Speed" ), animationGroup,
                                 LightApp_Preferences::IntSpin, "VISU", "speed" );
      addPreference( tr( "Cycled animation" ), animationGroup,
                     LightApp_Preferences::Bool, "VISU", "cycled_animation" );
      addPreference( tr( "Use proportional timing" ), animationGroup,
                     LightApp_Preferences::Bool, "VISU", "use_proportional_timing" );
      addPreference( tr( "Clean memory at each frame" ), animationGroup,
                     LightApp_Preferences::Bool, "VISU", "clean_memory_at_each_frame" );

      setPreferenceProperty( speed, "min", 1 );
      setPreferenceProperty( speed, "max", 99 );

      int dump_mode = addPreference( tr( "VISU_DUMP_MODE" ), animationGroup,
                                     LightApp_Preferences::Selector, "VISU", "dump_mode" );

      modes.clear();
      modes.append( tr( "VISU_NO_DUMP" ) );
      modes.append( tr( "VISU_DUMP_PICTURES" ) );
      modes.append( tr( "VISU_DUMP_VIDEO" ) );
      setPreferenceProperty( dump_mode, "strings", modes );

      indices.clear();
      indices.append( 0 );
      indices.append( 1 );
      indices.append( 2 );
      setPreferenceProperty( dump_mode, "indexes", indices );

      int time_stamp_frequency = addPreference( tr( "VISU_TIME_STAMP_FREQUENCY" ), animationGroup,
                                                LightApp_Preferences::IntSpin, "VISU", "time_stamp_frequency" );
      setPreferenceProperty( time_stamp_frequency, "min", 1 );
      setPreferenceProperty( time_stamp_frequency, "max", 100 );
    }

    // group: "Sweeping preferences"
    {
      int sweepGroup = addPreference( tr( "VISU_SWEEPING_PREF" ), animationTab );
      setPreferenceProperty( sweepGroup, "columns", 2 );

      int modeSw = addPreference( tr( "VISU_SWEEPING_MODES" ), sweepGroup,
                                  LightApp_Preferences::Selector, "VISU", "sweeping_modes" );
      QStringList sweep_modes;
      sweep_modes.append( tr( "VISU_LINEAR_SWEEP" ) );
      sweep_modes.append( tr( "VISU_COS_SWEEP" ) );
      sweep_modes.append( tr( "VISU_SIN_SWEEP" ) );
      indices.clear();
      indices.append( 0 );
      indices.append( 1 );
      indices.append( 2 );
      setPreferenceProperty( modeSw, "strings", sweep_modes );
      setPreferenceProperty( modeSw, "indexes", indices );

      int timestep   = addPreference( tr( "VISU_TIME_STEP" ), sweepGroup,
                                      LightApp_Preferences::DblSpin, "VISU", "sweeping_time_step" );
      /*    int nbcycles   = addPreference( tr( "VISU_NB_CYCLES" ), sweepGroup,
            LightApp_Preferences::IntSpin, "VISU", "sweeping_number_cycles" );*/
      int nbsteps    = addPreference( tr( "VISU_NB_STEPS" ),  sweepGroup,
                                      LightApp_Preferences::IntSpin, "VISU", "sweeping_number_steps" );
      int rangeSw = addPreference( tr( "VISU_IS2PI" ), sweepGroup, LightApp_Preferences::Selector, "VISU", "sweeping_is2PI" );
      QStringList ranges;
      ranges.append( tr( "PERIOD_PI" ) );
      ranges.append( tr( "PERIOD_2PI" ) );
      indices.clear();
      indices.append( 0 );
      indices.append( 1 );
      setPreferenceProperty( rangeSw, "strings", ranges );
      setPreferenceProperty( rangeSw, "indexes", indices );

      setPreferenceProperty( timestep, "min",  0.1 );
      setPreferenceProperty( timestep, "step", 0.1 );
      setPreferenceProperty( timestep, "max",  1000 );
      //    setPreferenceProperty( nbcycles, "max",  100  );
      setPreferenceProperty( nbsteps,  "max",  200  );
    }
  }

  // TAB: Representation ; group: "Representation properties"
  int representationTab = addPreference( tr( "Representation" ) );

  int representGr = addPreference( tr( "VISU_REPRESENT_PROPS" ), representationTab );
  setPreferenceProperty( representGr, "columns", 2 );

  QStringList mesh_modes;
  mesh_modes.append( tr("MEN_POINTS") );
  mesh_modes.append( tr("MEN_WIREFRAME") );
  mesh_modes.append( tr("MEN_INSIDEFRAME") );
  mesh_modes.append( tr("MEN_SURFACEFRAME") );
  mesh_modes.append( tr("MEN_SURFACE") );
  mesh_modes.append( tr("VISU_FEATURE_EDGES") );
  QList<QVariant> mesh_indices;
  mesh_indices.append( 0 );
  mesh_indices.append( 1 );
  mesh_indices.append( 3 );
  mesh_indices.append( 4 );
  mesh_indices.append( 2 );
  mesh_indices.append( 5 );

  QStringList modes0 = mesh_modes;
  modes0.removeAll( "Feature edges" );
  QList<QVariant> indices0 = mesh_indices;
  indices0.removeAll( 5 );

  QStringList modes1 = modes0;
  modes1.removeAll( "Insideframe" );
  QList<QVariant> indices1 = indices0;
  indices1.removeAll( 3 );

  QStringList modes2 = modes1;
  modes2.removeAll( "Surfaceframe" );
  QList<QVariant> indices2 = indices1;
  indices2.removeAll( 4 );

  QStringList modes3 = modes2;
  modes3.removeAll( "Surface" );
  QList<QVariant> indices3 = indices2;
  indices3.removeAll( 2 );


  int mesh_represent = addPreference( tr( "VISU_MESH" ), representGr, LightApp_Preferences::Selector, "VISU", "mesh_represent" );
  setPreferenceProperty( mesh_represent, "strings", mesh_modes );
  setPreferenceProperty( mesh_represent, "indexes", mesh_indices );
  addPreference( tr( "VISU_SHRINK" ), representGr, LightApp_Preferences::Bool, "VISU", "mesh_shrink" );

  int scalar_map_represent = addPreference( tr( "VISU_SCALAR_MAP" ), representGr, LightApp_Preferences::Selector, "VISU", "scalar_map_represent" );
  setPreferenceProperty( scalar_map_represent, "strings", modes0 );
  setPreferenceProperty( scalar_map_represent, "indexes", indices0 );
  addPreference( tr( "VISU_SHRINK" ), representGr, LightApp_Preferences::Bool, "VISU", "scalar_map_shrink" );

  int iso_surfaces_represent = addPreference( tr( "VISU_ISO_SURFACES" ), representGr, LightApp_Preferences::Selector, "VISU", "iso_surfaces_represent" );
  setPreferenceProperty( iso_surfaces_represent, "strings", modes1 );
  setPreferenceProperty( iso_surfaces_represent, "indexes", indices1 );
  int sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  int cut_planes_represent = addPreference( tr( "VISU_CUT_PLANES" ), representGr, LightApp_Preferences::Selector, "VISU", "cut_planes_represent" );
  setPreferenceProperty( cut_planes_represent, "strings", modes1 );
  setPreferenceProperty( cut_planes_represent, "indexes", indices1 );
  addPreference( "", representGr, LightApp_Preferences::Space );

  int cut_lines_represent = addPreference( tr( "VISU_CUT_LINES" ), representGr, LightApp_Preferences::Selector, "VISU", "cut_lines_represent" );
  setPreferenceProperty( cut_lines_represent, "strings", modes3 );
  setPreferenceProperty( cut_lines_represent, "indexes", indices3 );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  int cut_segment_represent = addPreference( tr( "VISU_CUT_SEGMENT" ), representGr, LightApp_Preferences::Selector, "VISU", "cut_segment_represent" );
  setPreferenceProperty( cut_segment_represent, "strings", modes3 );
  setPreferenceProperty( cut_segment_represent, "indexes", indices3 );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  int deformed_shape_represent = addPreference( tr( "VISU_DEFORMED_SHAPE" ), representGr, LightApp_Preferences::Selector, "VISU", "deformed_shape_represent" );
  setPreferenceProperty( deformed_shape_represent, "strings", modes0 );
  setPreferenceProperty( deformed_shape_represent, "indexes", indices0 );
  addPreference( tr( "VISU_SHRINK" ), representGr, LightApp_Preferences::Bool, "VISU", "deformed_shape_shrink" );

  int vectors_represent = addPreference( tr( "VISU_VECTORS" ), representGr, LightApp_Preferences::Selector, "VISU", "vectors_represent" );
  setPreferenceProperty( vectors_represent, "strings", modes3 );
  setPreferenceProperty( vectors_represent, "indexes", indices3 );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  int stream_lines_represent = addPreference( tr( "VISU_STREAM_LINES" ), representGr, LightApp_Preferences::Selector, "VISU", "stream_lines_represent" );
  setPreferenceProperty( stream_lines_represent, "strings", modes3 );
  setPreferenceProperty( stream_lines_represent, "indexes", indices3 );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  int plot3d_represent = addPreference( tr( "VISU_PLOT3D" ), representGr, LightApp_Preferences::Selector, "VISU", "plot3d_represent" );
  setPreferenceProperty( plot3d_represent, "strings", modes1 );
  setPreferenceProperty( plot3d_represent, "indexes", indices1 );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  int scalar_def_represent = addPreference( tr( "VISU_DEFORMED_SHAPE_AND_SCALAR_MAP" ), representGr, LightApp_Preferences::Selector, "VISU", "scalar_def_represent" );
  setPreferenceProperty( scalar_def_represent, "strings", modes1 );
  setPreferenceProperty( scalar_def_represent, "indexes", indices1 );
  addPreference( tr( "VISU_SHRINK" ), representGr, LightApp_Preferences::Bool, "VISU", "scalar_def_shrink" );

  int quadraticmode = addPreference( tr( "QUADRATIC_REPRESENT_MODE" ), representGr, LightApp_Preferences::Selector, "VISU", "quadratic_mode" );
  QStringList quadraticModes;
  quadraticModes.append("Lines");
  quadraticModes.append("Arcs");
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  setPreferenceProperty( quadraticmode, "strings", quadraticModes );
  setPreferenceProperty( quadraticmode, "indexes", indices );

  int maxAngle = addPreference( tr( "MAX_ARC_ANGLE" ), representGr, LightApp_Preferences::IntSpin,
                              "VISU", "max_angle" );
  setPreferenceProperty( maxAngle, "min", 1 );
  setPreferenceProperty( maxAngle, "max", 90 );

  int elem0DSize = addPreference( tr( "VISU_ELEM0D_SIZE" ), representGr, LightApp_Preferences::IntSpin, "VISU", "elem0d_size" );
  setPreferenceProperty( elem0DSize, "min", 1 );
  setPreferenceProperty( elem0DSize, "max", 10 );
  addPreference( "", representGr, LightApp_Preferences::Space );

  int shrFac = addPreference( tr( "VISU_SHRINK_FACTOR" ), representGr, LightApp_Preferences::IntSpin, "VISU", "shrink_factor" );
  setPreferenceProperty( shrFac, "min", 20 );
  setPreferenceProperty( shrFac, "max", 100 );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  addPreference( tr( "VISU_EDGE_COLOR" ), representGr, LightApp_Preferences::Color, "VISU", "edge_color" );
  addPreference( "", representGr, LightApp_Preferences::Space );

  int typeOfMarker = addPreference( tr( "VISU_TYPE_OF_MARKER" ), representGr, LightApp_Preferences::Selector, "VISU", "type_of_marker" );

  QList<QVariant> aMarkerTypeIndicesList;
  QList<QVariant> aMarkerTypeIconsList;
  for ( int i = VTK::MT_POINT; i <= VTK::MT_O_X; i++ ) {
    QString icoFile = QString( "ICON_VERTEX_MARKER_%1" ).arg( i );
    QPixmap pixmap = aResourceMgr->loadPixmap( "VTKViewer", tr( qPrintable( icoFile ) ) );
    aMarkerTypeIndicesList << i;
    aMarkerTypeIconsList << pixmap;
  }
  aMarkerTypeIndicesList << VTK::MT_POINT_SPRITE;
  aMarkerTypeIconsList << aResourceMgr->loadPixmap( "VISU", tr( "ICON_POINT_SPRITE" ) );
  setPreferenceProperty( typeOfMarker, "indexes", aMarkerTypeIndicesList );
  setPreferenceProperty( typeOfMarker, "icons",   aMarkerTypeIconsList );

  int markerScale = addPreference( tr( "VISU_MARKER_SCALE" ), representGr, LightApp_Preferences::Selector, "VISU", "marker_scale" );

  QList<QVariant> aMarkerScaleIndicesList;
  QStringList     aMarkerScaleValuesList;
  for ( int i = VISU::MS_10; i <= VISU::MS_70; i++ ) {
    aMarkerScaleIndicesList << i;
    aMarkerScaleValuesList  << QString::number( (i-(int)VISU::MS_10)*0.5 + 1.0 );
  }
  setPreferenceProperty( markerScale, "strings", aMarkerScaleValuesList );
  setPreferenceProperty( markerScale, "indexes", aMarkerScaleIndicesList );

  addPreference( tr( "VISU_USE_SHADING" ), representGr, LightApp_Preferences::Bool, "VISU", "represent_shading" );
  sp = addPreference( "", representGr, LightApp_Preferences::Space );
  setPreferenceProperty( sp, "hstretch", 0 );
  setPreferenceProperty( sp, "vstretch", 0 );

  addPreference( tr( "VISU_DISP_ONLY" ), representGr, LightApp_Preferences::Bool, "VISU", "display_only" );
  addPreference( "", representGr, LightApp_Preferences::Space );

  addPreference( tr( "VISU_AUTOMATIC_FIT_ALL" ), representGr,
                 LightApp_Preferences::Bool, "VISU", "automatic_fit_all" );

  // TAB: Tables ; group: "Tables editing properties"
  int tablesTab = addPreference( tr( "VISU_TABLES" ) );

  int tablesGr = addPreference( tr( "VISU_TABLES_EDITING_PROPS" ), tablesTab );
  setPreferenceProperty( tablesGr, "columns", 1 );

  addPreference( tr( "VISU_TABLES_ENABLE_EDITING" ), tablesGr, LightApp_Preferences::Bool, "VISU", "tables_enable_editing" );

  int sortPolicyPref = addPreference( tr( "VISU_TABLES_SORT_POLICY" ), tablesGr, LightApp_Preferences::Selector, "VISU", "tables_sort_policy" );
  QStringList policies;
  policies.append( tr( "VISU_TABLES_EMPTY_LOWEST" ) );
  policies.append( tr( "VISU_TABLES_EMPTY_HIGHEST" ) );
  policies.append( tr( "VISU_TABLES_EMPTY_FIRST" ) );
  policies.append( tr( "VISU_TABLES_EMPTY_LAST" ) );
  policies.append( tr( "VISU_TABLES_EMPTY_IGNORE" ) );
  indices.clear();
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  indices.append( 3 );
  indices.append( 4 );
  setPreferenceProperty( sortPolicyPref, "strings", policies );
  setPreferenceProperty( sortPolicyPref, "indexes", indices );

  // TAB: Feature edges ; group: "Feature edges properties"
  int featureEdgesTab = addPreference( tr( "VISU_FEATURE_EDGES" ) );

  int featureEdgesGr = addPreference( tr( "VISU_FEATURE_EDGES_PROPS" ), featureEdgesTab );
  setPreferenceProperty( featureEdgesGr, "columns", 2 );

  int featureEdgesAnglePref = addPreference( tr( "VISU_FEATURE_EDGES_ANGLE" ), featureEdgesGr,
                                             LightApp_Preferences::DblSpin, "VISU", "feature_edges_angle" );
  setPreferenceProperty( featureEdgesAnglePref, "step", 10.0 );
  setPreferenceProperty( featureEdgesAnglePref, "min", 0.0 );
  setPreferenceProperty( featureEdgesAnglePref, "max", 90.0 );

  addPreference( tr( "VISU_SHOW_FEATURE_EDGES" ), featureEdgesGr, LightApp_Preferences::Bool, "VISU", "show_feature_edges" );
  addPreference( tr( "VISU_SHOW_BOUNDARY_EDGES" ), featureEdgesGr, LightApp_Preferences::Bool, "VISU", "show_boundary_edges" );
  addPreference( tr( "VISU_SHOW_MANIFOLD_EDGES" ), featureEdgesGr, LightApp_Preferences::Bool, "VISU", "show_manifold_edges" );
  addPreference( tr( "VISU_SHOW_NON_MANIFOLD_EDGES" ), featureEdgesGr, LightApp_Preferences::Bool, "VISU", "show_non_manifold_edges" );
  //addPreference( tr( "VISU_FEATURE_EDGES_COLORING" ), featureEdgesGr, LightApp_Preferences::Bool, "VISU", "feature_edges_coloring" );

  // TAB: "Values labels"

  int valLblTab = addPreference( tr( "VALUES_LABELING" ) );

  // "Font" group
  int valLblFontGr = addPreference( tr( "VISU_FONT" ), valLblTab );
  setPreferenceProperty( valLblFontGr, "columns", 2 );

  // font
  addVtkFontPref( tr( "LABELS" ), valLblFontGr, "values_labeling_font", true );

  // color
  addPreference( tr( "COLOR" ), valLblFontGr,
    LightApp_Preferences::Color, "VISU", "values_labeling_color" );

}

void VisuGUI::preferencesChanged( const QString& a, const QString& b)
{
  if (a != QString("VISU")) return;
  if (b == QString("scalar_bar_orientation")) return;
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  float sbX1,sbY1,sbW,sbH;
  float aTol = 1.00000009999999;
  QString aWarning;
  if(b == QString("scalar_bar_vertical_x") || b == QString("scalar_bar_vertical_width")){
    sbX1 = aResourceMgr->doubleValue("VISU", "scalar_bar_vertical_x", 0.01);
    sbW = aResourceMgr->doubleValue("VISU", "scalar_bar_vertical_width", 0.05);
    if(sbX1+sbW > aTol){
      aWarning = "Origin and Size Vertical: X+Width > 1\n";
      sbX1=0.01;
      sbW=0.05;
      aResourceMgr->setValue("VISU", "scalar_bar_vertical_x", sbX1);
      aResourceMgr->setValue("VISU", "scalar_bar_vertical_width", sbW);
    }
  }
  else if(b == QString("scalar_bar_vertical_y") || b == QString("scalar_bar_vertical_height")){
    sbY1 = aResourceMgr->doubleValue("VISU", "scalar_bar_vertical_y", 0.01);
    sbH = aResourceMgr->doubleValue("VISU", "scalar_bar_vertical_height",0.5);
    if(sbY1+sbH > aTol){
      aWarning = "Origin and Size Vertical: Y+Height > 1\n";
      sbY1=0.01;
      sbH=0.5;
      aResourceMgr->setValue("VISU", "scalar_bar_vertical_y", sbY1);
      aResourceMgr->setValue("VISU", "scalar_bar_vertical_height",sbH);
    }
  }
  else if(b ==  QString("scalar_bar_horizontal_x") || b ==  QString("scalar_bar_horizontal_width")){
    sbX1 = aResourceMgr->doubleValue("VISU", "scalar_bar_horizontal_x", 0.2);
    sbW = aResourceMgr->doubleValue("VISU", "scalar_bar_horizontal_width", 0.6);
    if(sbX1+sbW > aTol){
      aWarning = "Origin and Size Horizontal: X+Width > 1\n";
      sbX1=0.2;
      sbW=0.6;
      aResourceMgr->setValue("VISU", "scalar_bar_horizontal_x", sbX1);
      aResourceMgr->setValue("VISU", "scalar_bar_horizontal_width", sbW);
    }
  }
  else if(b ==  QString("scalar_bar_horizontal_y") || b ==  QString("scalar_bar_horizontal_height")){
    sbY1 = aResourceMgr->doubleValue("VISU", "scalar_bar_horizontal_y", 0.01);
    sbH = aResourceMgr->doubleValue("VISU", "scalar_bar_horizontal_height",0.12);
    if(sbY1+sbH > aTol){
      aWarning = "Origin and Size Horizontal: Y+Height > 1\n";
      sbY1=0.01;
      sbH=0.12;
      aResourceMgr->setValue("VISU", "scalar_bar_horizontal_y", sbY1);
      aResourceMgr->setValue("VISU", "scalar_bar_horizontal_height",sbH);
    }
  }
  else if(b == "visual_data_precision") {
    LightApp_Preferences* pref = preferences();
    QtxPreferenceItem* min = pref->findItem( tr( "VISU_MIN" ), true );
    QtxPreferenceItem* max = pref->findItem( tr( "VISU_MAX" ), true );
    if ( min )
      setPreferenceProperty( min->id(), "decimals", qAbs( aResourceMgr->integerValue( "VISU", "visual_data_precision", 0 ) ) );
    if ( max )
      setPreferenceProperty( max->id(), "decimals", qAbs( aResourceMgr->integerValue( "VISU", "visual_data_precision", 0 ) ) );
  }

  if ( !aWarning.isEmpty() ){
    aWarning += "The default values are applied instead.";
    SUIT_MessageBox::warning(GetDesktop(this),
                             tr("ERR_SCALARBAR_PARAMS"),
                             aWarning );
  }
}

SUIT_ViewManager*
VisuGUI
::getViewManager(const QString& theType,
                 const bool theIsCreate)
{
  return getApp()->getViewManager(theType,theIsCreate);
}

VISU::TViewToPrs3d&
VisuGUI
::getScalarBarsMap()
{
  return myScalarBarsMap;
}

LightApp_Displayer* VisuGUI::displayer()
{
  if( !myDisplayer )
    myDisplayer = new VisuGUI_Displayer( getApp() );

  return myDisplayer;
}


void VisuGUI::OnPlot3dFromCutPlane()
{
 _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
 if (CheckLock(aCStudy, GetDesktop(this)))
   return;

 LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(this);
 SALOME_ListIO aListIO;
 aSelectionMgr->selectedObjects(aListIO);

 if (aListIO.Extent() < 1) return;


 VISU::CutPlanes_i* aCutPrs = 0;
 Handle(SALOME_InteractiveObject) anIO;
 _PTR(SObject) aSObject;
 SALOME_ListIteratorOfListIO It (aListIO);
 for (; It.More(); It.Next()) {
   anIO = It.Value();

   if (!anIO.IsNull() && anIO->hasEntry()) {
     aSObject = aCStudy->FindObjectID(anIO->getEntry());
     if ( !aSObject )
        continue;
     VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(this), anIO->getEntry());
     if(VISU::Prs3d_i* aPrsObject = VISU::GetPrs3dFromBase(anObjectInfo.myBase)){
       if (aPrsObject->GetType() == VISU::TCUTPLANES) {
         aCutPrs = dynamic_cast<VISU::CutPlanes_i*>(aPrsObject);
         if (aCutPrs)
           break;
       }
     }
   }
 }

 if (aCutPrs) {
   _PTR(SObject) aTimeStampSObj = aSObject->GetFather();
   if (SUIT_ViewManager* aViewManager = getApp()->activeViewManager()) {
     QString aViewType = aViewManager->getType();
     //bool anIsVVTK = (aViewType == VVTK_Viewer::Type());

     Storable::TRestoringMap aRestoringMap = Storable::GetStorableMap(aTimeStampSObj);
     VISU::VISUType aType = VISU::Storable::RestoringMap2Type( aRestoringMap );
     if (aType != TTIMESTAMP )
        return;

     QString aMeshName = aRestoringMap["myMeshName"];
     QString anEntity = aRestoringMap["myEntityId"];
     QString aFieldName = aRestoringMap["myFieldName"];
     QString aTimeStampId = aRestoringMap["myTimeStampId"];

     int aPos;
     SUIT_ViewWindow* aViewWindow = 0;
//      if (anIsVVTK) {
//      aViewWindow = GetViewWindow<VVTK_Viewer>(this);
//      aPos = GetFreePositionOfDefaultScalarBar(this, dynamic_cast<VVTK_ViewWindow*>(aViewWindow));
//      } else {
        aViewWindow = GetViewWindow<SVTK_Viewer>(this);
        aPos = GetFreePositionOfDefaultScalarBar(this, dynamic_cast<SVTK_ViewWindow*>(GetViewWindow<SVTK_Viewer>(this)));
        //     }
     GetResourceMgr()->setValue("VISU", "scalar_bar_position_num", aPos);

     VISU::Plot3D_i* aPrs3d =
       CreatePrs3dFromFactory<VISU::Plot3D_i>(this,
                                              aTimeStampSObj,
                                              (const char*)aMeshName.toLatin1(),
                                              (Entity)anEntity.toInt(),
                                              (const char*)aFieldName.toLatin1(),
                                              aTimeStampId.toInt(),
                                              ColoredPrs3d_i::EPublishUnderTimeStamp);
     if (aPrs3d) {
        SUIT_ResourceMgr* aResourceMgr = GetResourceMgr();
        int aValue = aResourceMgr->integerValue("VISU","BuildDefaultPrs3d",0);
        if (!aValue) {
          VisuGUI_Plot3DDlg* aDlg = new VisuGUI_Plot3DDlg(this);
          aDlg->initFromPrsObject(aPrs3d, true);
          int    aOrient = aCutPrs->GetOrientationType(); // Plot3d has the same orientation type
          double aRotX = aCutPrs->GetRotateX();
          double aRotY = aCutPrs->GetRotateY();
          double aPlanePos = aCutPrs->GetPlanePosition(0);
          aDlg->setPlane(aOrient, aRotX, aRotY, aPlanePos);

          if (!(runAndWait(aDlg, 0) && (aDlg->storeToPrsObject(aPrs3d)))) {
            DeletePrs3d(this, aPrs3d);
            delete aDlg;
            return;
          }
          delete aDlg;
          UpdateObjBrowser(this, true, aTimeStampSObj);
        }
        SVTK_ViewWindow* aSVtkWindow = dynamic_cast<SVTK_ViewWindow*>(aViewWindow);
        if ( aSVtkWindow ) {
          PublishInView(this, aPrs3d, aSVtkWindow, /*highlight = */true);
          aSVtkWindow->onFitAll();

          AddScalarBarPosition(this, aSVtkWindow, aPrs3d, aPos);
        }
        processPresentationCreated(aPrs3d);
     }
   }
   application()->putInfo(QObject::tr("INF_DONE"));
 }
}

void VisuGUI::OnManageCache()
{
  mySlider->show();
}


//---------------------------------------------------------------
void VisuGUI::OnMultiprViewFullRes()
{
  OnMultiprChangeRes(VISU::Result::FULL);
}


//---------------------------------------------------------------
void VisuGUI::OnMultiprViewMediumRes()
{
  OnMultiprChangeRes(VISU::Result::MEDIUM);
}


//---------------------------------------------------------------
void VisuGUI::OnMultiprViewLowRes()
{
  OnMultiprChangeRes(VISU::Result::LOW);
}


//---------------------------------------------------------------
void VisuGUI::OnMultiprViewHide()
{
  OnMultiprChangeRes(VISU::Result::HIDDEN);
}


//---------------------------------------------------------------
void VisuGUI::OnMultiprChangeRes(VISU::Result::Resolution theResolution)
{
  _PTR(Study) aCStudy = GetCStudy(GetAppStudy(this));
  if (CheckLock(aCStudy,GetDesktop(this)))
    return;

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  typedef SALOME::GenericObjPtr<VISU::Result_i> TResultPtr;
  typedef std::map<TResultPtr, _PTR(SObject)> TResult2SObject;
  TResult2SObject aResult2SObject;

  for (int i=0; i < aSelectionInfo.size(); i++) {
    VISU::TSelectionItem aSelectionItem = aSelectionInfo[i];

    _PTR(SObject) aSObject = aSelectionItem.myObjectInfo.mySObject;
    if (!aSObject)
      continue;

    VISU::Result_var aResult = FindResult( VISU::GetSObject( aSObject ).in() );
    if(CORBA::is_nil(aResult.in()))
      continue;

    VISU::Result_i* aCResult = dynamic_cast<VISU::Result_i*>(VISU::GetServant(aResult).in());
    if(!aCResult)
      continue;

    Storable::TRestoringMap aRestoringMap = Storable::GetStorableMap(aSObject);
    bool anIsFound = false;
    QString aMeshName = Storable::FindValue(aRestoringMap, "myMeshName", &anIsFound);
    if(!anIsFound)
      continue;

    std::string aPartName = aSObject->GetName();

    aCResult->SetResolution(aMeshName.toLatin1().data(), aPartName.c_str(), theResolution);

    aResult2SObject[TResultPtr(aCResult)] = aSObject;
  }

  // To update all result observers
  TResult2SObject::const_iterator anIter = aResult2SObject.begin();
  for(; anIter != aResult2SObject.end(); anIter++){
    const TResultPtr& aResultPtr = anIter->first;
    const _PTR(SObject)& aSObject = anIter->second;
    aResultPtr->UpdateObservers();
    VISU::UpdateObjBrowser(this, false, aSObject);
  }

  QApplication::restoreOverrideCursor();
}

//---------------------------------------------------------------
void VisuGUI::OnFileInfo()
{
  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( getApp()->activeStudy() );
  SUIT_DataOwnerPtrList lst;
  getApp()->selectionMgr()->selected( lst );
  LightApp_DataOwner* owner = dynamic_cast<LightApp_DataOwner*>( lst.first().get() );
  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry( aStudy, owner->entry().toLatin1().constData() );
  VISU::Result_i* aCResult = dynamic_cast<VISU::Result_i*>( anObjectInfo.myBase );
  if(!aCResult)
    return;
  VisuGUI_FileInfoDlg dlg( getApp()->desktop(), aCResult->GetMEDFileInfo() );
  dlg.exec();
}

//---------------------------------------------------------------
void VisuGUI::OnExportMED()
{
  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( getApp()->activeStudy() );
  SUIT_DataOwnerPtrList lst;
  getApp()->selectionMgr()->selected( lst );
  LightApp_DataOwner* owner = dynamic_cast<LightApp_DataOwner*>( lst.first().get() );
  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry( aStudy, owner->entry().toLatin1().constData() );
  VISU::Result_i* aResult = dynamic_cast<VISU::Result_i*>( anObjectInfo.myBase );
  if(!aResult)
    return;
  //  VisuGUI_FileInfoDlg dlg( getApp()->desktop(), aResult->GetMEDFileInfo() );
  //  dlg.exec();

  SUIT_ResourceMgr* aResourceMgr = GetResourceMgr();
  QString aDir = aResourceMgr->stringValue("VISU","OutputDir","");

  // get name for the file
  QStringList aFilter;
  aFilter.append("MED Files (*.med)");

  std::string anInitFileName = aResult->GetInitFileName();
  QFileInfo anInitFileInfo(anInitFileName.c_str());
  QString aBaseName = anInitFileInfo.fileName();

  QString aPath = Qtx::addSlash(aDir) + aBaseName;

  QFileInfo aFileInfo =
    SUIT_FileDlg::getFileName(GetDesktop(this),
                              aPath,
                              aFilter,
                              tr("VISU_MEN_EXPORT_MED"), // "Export To Med File"
                              false);

  QString aFile = aFileInfo.filePath();
  if ( aFile.length() > 0 ) {
    aResult->ExportMED((const char*)aFile.toLatin1());
  }
}

void VisuGUI::OnFilterScalars()
{
  VisuGUI_FilterScalarsDlg* aDlg = new VisuGUI_FilterScalarsDlg( this );
  aDlg->show();
}

/*!
  \brief SLOT called when "Labeling" popup menu item of presentation is clicked
   activates/deactivates labeling of results of the screen. This functionality works
   to numbering in SMESH module but is intended for showing results on the screen
   instead of elements numbers
   \sa OnValuesLabelingParams()
*/
void VisuGUI::OnValuesLabeling()
{
  // to do: rewrite for several actors if necessary

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects( this );
  if ( aSelectionInfo.empty() )
    return;

  TSelectionItem aSelectionItem = GetSelectedObjects(this).front();
  VISU::Prs3d_i* aPrs3d = GetPrs3dFromBase(aSelectionItem.myObjectInfo.myBase);
  if (!aPrs3d)
    return;

  SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(this);
  if (!aViewWindow)
    return;

  VISU_Actor* anActor = FindActor(aViewWindow, aPrs3d);
  if (!anActor)
    return;

  if ( VISU_ScalarMapAct* aScalarMapActor = dynamic_cast<VISU_ScalarMapAct*>(anActor) )
  {
    aScalarMapActor->SetValuesLabeled( !aScalarMapActor->GetValuesLabeled() );
    aViewWindow->Repaint();
  }
  else if ( VISU_GaussPtsAct* aGaussPtsAct = dynamic_cast<VISU_GaussPtsAct*>(anActor) )
  {
    aGaussPtsAct->SetValuesLabeled( !aGaussPtsAct->GetValuesLabeled() );
    aViewWindow->Repaint();
  }
}

/*!
  \brief SLOT called when "Labeling parameters" popup menu item of presentation
  is clicked launch dialog box for changing parameters of labeling
  \sa OnValuesLabeling()
*/
void VisuGUI::OnValuesLabelingParams()
{
  Handle(SALOME_InteractiveObject) anIO;
  if ( VISU::Prs3d_i* aPrs3d = GetPrsToModify( this, anIO ) )
    EditPrs3d<VISU::ColoredPrs3d_i, VisuGUI_ValuesLabelingDlg, 1>(this, anIO, aPrs3d);
}
/*!
  \brief Adds preferences for dfont of VTK viewer
  \param label label
  \param pIf group identifier
  \param param parameter
  \return identifier of preferences
*/
int VisuGUI::addVtkFontPref(
  const QString& label,
  const int pId,
  const QString& param,
  const bool useSize )
{
  int tfont = addPreference( label, pId, LightApp_Preferences::Font, "VISU", param );

  setPreferenceProperty( tfont, "mode", QtxFontEdit::Custom );

  QStringList fam;
  fam.append( tr( "VISU_FONT_ARIAL" ) );
  fam.append( tr( "VISU_FONT_COURIER" ) );
  fam.append( tr( "VISU_FONT_TIMES" ) );

  setPreferenceProperty( tfont, "fonts", fam );

  int f = QtxFontEdit::Family | QtxFontEdit::Bold | QtxFontEdit::Italic | QtxFontEdit::Shadow;
  if ( useSize )
    f |= QtxFontEdit::UserSize | QtxFontEdit::Size;

  setPreferenceProperty( tfont, "features", f );

  return tfont;
}

/*!
  \brief SLOT called when "2D Quadratic -> Arcs" popup menu item
  \      of presentation is cliked
 */
void VisuGUI::OnArcQuadMode(){
  ChangeQuadratic2DRepresentation(this,VISU::ARCS);
}

/*!
  \brief SLOT called when "2D Quadratic -> Lines" popup menu item
  \      of presentation is cliked
 */
void VisuGUI::OnLineQuadMode(){
  ChangeQuadratic2DRepresentation(this,VISU::LINES);
}

/*!
  \brief Return \c true if object can be renamed
*/
bool VisuGUI::renameAllowed( const QString& entry) const {  
  
  SalomeApp_Application* app = dynamic_cast< SalomeApp_Application* >( SUIT_Session::session()->activeApplication() );
  SalomeApp_Study* appStudy = app ? dynamic_cast<SalomeApp_Study*>( app->activeStudy() ) : 0; 
  SalomeApp_DataObject* obj = appStudy ? dynamic_cast<SalomeApp_DataObject*>(appStudy->findObjectByEntry(entry)) : 0;
  
  if(!app || !appStudy || !obj)
    return false;

  if(appStudy->isComponent(entry) || obj->isReference())
    return false;

  
  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry( GetAppStudy(this), qPrintable(entry) );
  VISU::Base_i* aBase = anObjectInfo.myBase;
  if(aBase){
    VISU::VISUType aType = aBase->GetType();
    if( aType == VISU::TCOLOREDPRS3DHOLDER ) {
      CORBA::Object_var anObject = ClientSObjectToObject(anObjectInfo.mySObject);
      VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);
      aType = aHolder->GetPrsType();
    }
    if (aType == VISU::TSCALARMAP || aType == VISU::TISOSURFACES ||
        aType == VISU::TDEFORMEDSHAPE || aType == VISU::TCUTPLANES ||
        aType == VISU::TCUTLINES || aType == VISU::TCUTSEGMENT || aType == VISU::TVECTORS ||
        aType == VISU::TSTREAMLINES || aType == VISU::TPLOT3D ||
        aType == VISU::TSCALARMAPONDEFORMEDSHAPE || aType == VISU::TCOLOREDPRS3DHOLDER ||
        aType == VISU::TTABLE || aType == VISU::TCURVE || aType == VISU::TCONTAINER ||
        aType == VISU::TPOINTMAP3D || aType == VISU::TVIEW3D ||
        aType == VISU::TPOINTMAP3D || aType == VISU::TGAUSSPOINTS)
      return true;
  } else {
    VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);
    bool anIsExist;
    QString aVal = VISU::Storable::FindValue(aMap, "myComment", &anIsExist);
    return anIsExist && "VIEW3D" == aVal;
  }
  return false;
}

void VisuGUI::OnShowDeviation() {

  if(IsStudyLocked(GetCStudy(GetAppStudy(this))))
    return;

  QAction* anAction = action(VISU_SHOW_DEVIATION);
  if(!anAction)
    return;

  bool anIsShow = anAction->isChecked();

  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(this);
  if(aSelectionInfo.empty())
    return;

  TSelectionInfo::iterator anIter = aSelectionInfo.begin();
  for( ; anIter != aSelectionInfo.end(); anIter++ )
  {
    VISU::TSelectionItem aSelectionItem = *anIter;
    Handle(SALOME_InteractiveObject) anIO = aSelectionItem.myIO;
    VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase;
    if(aBase && aBase->GetType() == VISU::TCURVE) {
      // Curve object
      if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aBase)) {
        aCurve->ShowDeviation(anIsShow);
        if( displayer()->IsDisplayed( anIO->getEntry() ) )
          PlotCurve(this, aCurve, VISU::eDisplay);
      }
    }
  }
}

/*!
  Rename object by entry.
  \param entry entry of the object
  \param name new name of the object
  \brief Return \c true if rename operation finished successfully, \c false otherwise.
*/
bool VisuGUI::renameObject( const QString& entry, const QString& name) {    

  SalomeApp_Application* app = dynamic_cast< SalomeApp_Application* >( SUIT_Session::session()->activeApplication());
  SalomeApp_Study* appStudy = app ? dynamic_cast<SalomeApp_Study*>( app->activeStudy() ) : 0;
  
  if(!appStudy)
    return false;
  
  _PTR(Study) aStudy = appStudy->studyDS();
  
  if(!aStudy)
    return false;
  
  bool aLocked = (_PTR(AttributeStudyProperties)(appStudy->studyDS()->GetProperties()))->IsLocked();
  if ( aLocked ) {
    SUIT_MessageBox::warning ( app->desktop(), QObject::tr("WRN_WARNING"), QObject::tr("WRN_STUDY_LOCKED") );
    return false;
  }
  
  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(this) , qPrintable(entry) );
  
  _PTR(SObject) aSObject = anObjectInfo.mySObject;
  if (aSObject) {
    VISU::Base_i* aBase = anObjectInfo.myBase;
    VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);
    if(aBase){
      VISU::VISUType aType = aBase->GetType();
      if( aType == VISU::TCOLOREDPRS3DHOLDER ) {
	CORBA::Object_var anObject = ClientSObjectToObject(anObjectInfo.mySObject);
	VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);
	aType = aHolder->GetPrsType();
      }
      if (aType == VISU::TSCALARMAP || aType == VISU::TISOSURFACES ||
	  aType == VISU::TDEFORMEDSHAPE || aType == VISU::TCUTPLANES ||
	  aType == VISU::TCUTLINES || aType == VISU::TCUTSEGMENT || aType == VISU::TVECTORS ||
	  aType == VISU::TSTREAMLINES || aType == VISU::TPLOT3D ||
	  aType == VISU::TSCALARMAPONDEFORMEDSHAPE || aType == VISU::TCOLOREDPRS3DHOLDER ||
	  aType == VISU::TTABLE || aType == VISU::TCURVE || aType == VISU::TCONTAINER ||
	  aType == VISU::TPOINTMAP3D || aType == VISU::TVIEW3D ||
	  aType == VISU::TPOINTMAP3D || aType == VISU::TGAUSSPOINTS) {
	_PTR(GenericAttribute) anAttr;
	if (aSObject->FindAttribute(anAttr, "AttributeName")) {
	  _PTR(AttributeName) aName (anAttr);
	  if (!name.isEmpty()) {
	    QApplication::setOverrideCursor(Qt::WaitCursor);
	    // rename specific objects
	    if (VISU::Base_i* aBase = anObjectInfo.myBase) {
	      switch (aBase->GetType()) {
	      case VISU::TCURVE: { // Curve object
		if (VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aBase))
		  aCurve->SetName(qPrintable(name), true);
		break;
	      }
	      case VISU::TTABLE: { // Table object
		if (VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(aBase))
		  aTable->SetName(qPrintable(name), true);
		break;
	      }
	      case VISU::TPOINTMAP3D: { // Table object
		if (VISU::PointMap3d_i* aTable3d = dynamic_cast<VISU::PointMap3d_i*>(aBase)) {
		  aTable3d->SetTitle(qPrintable(name));
		  aTable3d->UpdateActors();
		}
		break;
	      }
	      case VISU::TCONTAINER: { // Container object
		if (VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(aBase))
		  aContainer->SetName(qPrintable(name), true);
		break;
	      }
	      default: {
	      }}
	    }

	    // rename the study object
	    aName->SetValue( qPrintable(name) ); // rename the SObject
	    
	    ViewManagerList aViewManagerList;
	    getApp()->viewManagers(SPlot2d_Viewer::Type(), aViewManagerList);
	    SUIT_ViewManager* aViewManager;
	    Handle(SALOME_InteractiveObject) anIO = 
	      new SALOME_InteractiveObject ( qPrintable(entry), "VISU", qPrintable(name) );
	    foreach( aViewManager, aViewManagerList ) {
	      if (Plot2d_ViewManager* aManager = dynamic_cast<Plot2d_ViewManager*>(aViewManager)) {
		if (SPlot2d_Viewer* aViewer = dynamic_cast<SPlot2d_Viewer*>(aManager->getViewModel()))
		  aViewer->renameAll( anIO, qPrintable(name) );
	      }
	    }
	    QApplication::restoreOverrideCursor();	    
	    return true;
	  }
	}
	//Rename visual params
      }
    } else {
      bool anIsExist;
      QString aVal = VISU::Storable::FindValue(aMap, "myComment", &anIsExist);
      if(anIsExist && "VIEW3D" == aVal) {
	_PTR(GenericAttribute) anAttr;
	if (aSObject->FindAttribute(anAttr, "AttributeName")) {
	  _PTR(AttributeName) aName (anAttr);
	  if (!name.isEmpty()) {
	    aName->SetValue(qPrintable(name));
	  }
	}
      }
    }
  }
  return false;
}

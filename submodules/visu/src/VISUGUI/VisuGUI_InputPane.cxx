// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_InputPane.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_InputPane.h"

#include "VisuGUI_Prs3dDlg.h"
#include "VisuGUI_FieldFilter.h"
#include "VisuGUI_Tools.h"

#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_ScalarMap_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"

#include "SalomeApp_Module.h"

#include "LightApp_Application.h"
#include "LightApp_SelectionMgr.h"

#include "SALOME_ListIO.hxx"

#include "SALOMEDSClient_AttributeString.hxx"
#include "SALOMEDSClient_AttributeName.hxx"

//#include "QtxListBox.h"

#include <QCheckBox>
#include <QComboBox>
#include <QToolButton>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QKeyEvent>
#include <QListWidget>


//---------------------------------------------------------------
class VisuGUI_ListWidget: public QListWidget
{
 public:
  VisuGUI_ListWidget( QWidget * parent = 0 ):
    QListWidget(parent ) 
  {};

  virtual QSize sizeHint() const 
  { 
    return minimumSizeHint(); }
  ;
};


//---------------------------------------------------------------
VisuGUI_InputPane::VisuGUI_InputPane( VISU::VISUType theType,
                                      SalomeApp_Module* theModule,
                                      VisuGUI_Prs3dDlg* theDialog ) :
  QGroupBox( theDialog ),
  myModule( theModule ),
  myDialog( theDialog ),
  myPrs( NULL ),
  isRestoreInitialSelection(true)
{
  //setFrameStyle( QFrame::NoFrame );

  LightApp_SelectionMgr* aSelectionMgr = VISU::GetSelectionMgr(theModule);

  connect( aSelectionMgr, SIGNAL( selectionChanged() ), SLOT( onSelectionChanged() ) );

  QGridLayout* aMainLayout = new QGridLayout( this );
  aMainLayout->setMargin( 11 );
  aMainLayout->setSpacing( 6 );

  // Definition of data source
  myDataSourceGroupBox = new QGroupBox( this );
  aMainLayout->addWidget( myDataSourceGroupBox, 0, 0 );
  myDataSourceGroupBox->setTitle( tr( "PRS_DATA_SOUIRCE" ) );
  {
    QGroupBox* aGroupBox = myDataSourceGroupBox;
    //aGroupBox->setColumnLayout(0, Qt::Vertical );
    //aGroupBox->layout()->setSpacing( 6 );
    //aGroupBox->layout()->setMargin( 11 );

    QGridLayout* aGroupBoxLayout = new QGridLayout( aGroupBox );
    aGroupBoxLayout->setAlignment( Qt::AlignTop );
    {
      QLabel* aLabel = new QLabel( tr( "MED_FILE" ), aGroupBox );
      aGroupBoxLayout->addWidget( aLabel, 0, 0 );

      myMedFile = new QLineEdit( aGroupBox );
      myMedFile->setReadOnly( true );
      myMedFile->setEnabled( false );
      QPalette aPal = myMedFile->palette();
      aPal.setColor( myMedFile->backgroundRole(), Qt::black );
      myMedFile->setPalette( aPal );
      //myMedFile->setPaletteForegroundColor( Qt::black );
      aGroupBoxLayout->addWidget( myMedFile, 0, 1 );
    }
    {
      QLabel* aLabel = new QLabel( tr( "MESH" ), aGroupBox );
      aGroupBoxLayout->addWidget( aLabel, 1, 0 );
      
      myMeshName = new QLineEdit( aGroupBox );
      myMeshName->setReadOnly( true );
      myMeshName->setEnabled( false );
      QPalette aPal = myMeshName->palette();
      aPal.setColor( myMeshName->backgroundRole(), Qt::black );
      myMeshName->setPalette( aPal );
      //myMeshName->setPaletteForegroundColor( Qt::black );
      aGroupBoxLayout->addWidget( myMeshName, 1, 1 );
    }
    {
      QLabel* aLabel = new QLabel( tr( "ENTITY" ), aGroupBox );
      aGroupBoxLayout->addWidget( aLabel, 2, 0 );
      
      myEntityName = new QLineEdit( aGroupBox );
      myEntityName->setReadOnly( true );
      myEntityName->setEnabled( false );
      QPalette aPal = myEntityName->palette();
      aPal.setColor( myEntityName->backgroundRole(), Qt::black );
      myEntityName->setPalette( aPal );
      //myEntityName->setPaletteForegroundColor( Qt::black );
      aGroupBoxLayout->addWidget( myEntityName, 2, 1 );
    }
    {
      QLabel* aLabel = new QLabel( tr( "FIELD" ), aGroupBox );
      aGroupBoxLayout->addWidget( aLabel, 3, 0 );
      
      myFieldName = new QLineEdit( aGroupBox );
      myFieldName->setReadOnly( true );
      aGroupBoxLayout->addWidget( myFieldName, 3, 1 );
    }
    {
      QLabel* aLabel = new QLabel( tr( "TIME_STAMP" ), aGroupBox );
      aGroupBoxLayout->addWidget( aLabel, 4, 0 );
      
      myTimeStamps = new QComboBox( aGroupBox );
      aGroupBoxLayout->addWidget( myTimeStamps, 4, 1 );
    }

    myReInit = new QCheckBox( tr( "REINITIALIZE" ), aGroupBox );
    myReInit->setChecked( true );
    aGroupBoxLayout->addWidget( myReInit, 5, 1 );
  }

  // Definition of used groups
  myUseGroupsGroupBox = new QGroupBox( this );
  aMainLayout->addWidget( myUseGroupsGroupBox, 1, 0 );
  myUseGroupsGroupBox->setTitle( tr( "PRS_ON_GROUPS" ) );
  myUseGroupsGroupBox->setCheckable( TRUE );
  {
    QGroupBox* aGroupBox = myUseGroupsGroupBox;
    //aGroupBox->setColumnLayout(0, Qt::Vertical );
    //aGroupBox->layout()->setSpacing( 6 );
    //aGroupBox->layout()->setMargin( 11 );

    QGridLayout* aGroupBoxLayout = new QGridLayout( aGroupBox );
    aGroupBoxLayout->setAlignment( Qt::AlignTop );
    {
      myAllGroups = new VisuGUI_ListWidget(aGroupBox);
      myAllGroups->setSelectionMode(QAbstractItemView::ExtendedSelection);
      aGroupBoxLayout->addWidget( myAllGroups, 0, 0, 4, 1 );
    }
    {
      mySelectedGroups = new VisuGUI_ListWidget(aGroupBox );
      mySelectedGroups->setSelectionMode(QAbstractItemView::ExtendedSelection);
      mySelectedGroups->installEventFilter(this);
      aGroupBoxLayout->addWidget( mySelectedGroups, 0, 2, 4, 1 );
    }
    {
      myAddButton = new QToolButton(aGroupBox);
      myAddButton->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr("ICON_ADD")) );
      aGroupBoxLayout->addWidget( myAddButton, 1, 1 );
    }
    {
      myRemoveButton = new QToolButton(aGroupBox);
      myRemoveButton->setIcon( VISU::GetResourceMgr()->loadPixmap("VISU", tr("ICON_REMOVE")) );
      aGroupBoxLayout->addWidget( myRemoveButton, 2, 1 );
    }
    {
      QSpacerItem* aSpacer = new QSpacerItem( 20, 51, QSizePolicy::Minimum, QSizePolicy::Expanding );
      aGroupBoxLayout->addItem( aSpacer, 0, 1 );
    }
    {
      QSpacerItem* aSpacer = new QSpacerItem( 20, 61, QSizePolicy::Minimum, QSizePolicy::Expanding );
      aGroupBoxLayout->addItem( aSpacer, 3, 1 );
    }
  }

  connect( myTimeStamps,   SIGNAL( activated( int ) ), this, SLOT( changeTimeStamp( int ) ) );
  connect( myReInit, SIGNAL( toggled( bool ) ), SLOT( onReInitialize( bool ) ) );

  connect( myUseGroupsGroupBox, SIGNAL( toggled( bool ) ), this, SLOT( onTypeChanged() ) );
  connect( myAllGroups,  SIGNAL( itemDoubleClicked( QListWidgetItem* ) ), this, SLOT( onListDoubleClicked( QListWidgetItem* ) ) );
  connect( mySelectedGroups,  SIGNAL( itemDoubleClicked( QListWidgetItem* ) ), this, SLOT( onListDoubleClicked( QListWidgetItem* ) ) );
  connect( myAddButton, SIGNAL(clicked()), this, SLOT(onAdd()));
  connect( myRemoveButton, SIGNAL(clicked()), this, SLOT(onRemove()));

  // Save current selection (to be restored in destructor)
  aSelectionMgr->selectedObjects(mySavedSelection);

  onTypeChanged();
  onSelectionChanged();

  myFieldFilter = new VisuGUI_FieldFilter( theType );

  hide();
}


//---------------------------------------------------------------
VisuGUI_InputPane::~VisuGUI_InputPane()
{
  if ( myModule->getApp() ) {
    LightApp_SelectionMgr* aSelectionMgr = VISU::GetSelectionMgr(myModule);
    
    if (myFieldFilter)
      {
        aSelectionMgr->removeFilter(myFieldFilter);
        delete myFieldFilter;
      }

    // Restore initial selection
    if(isRestoreInitialSelection)
      aSelectionMgr->setSelectedObjects(mySavedSelection);
  }
}

void VisuGUI_InputPane::SetRestoreInitialSelection(bool on){
  isRestoreInitialSelection = on;
}

bool VisuGUI_InputPane::GetRestoreInitialSelection(){
  return isRestoreInitialSelection;
}

//---------------------------------------------------------------
/*!
  Event filter
*/
bool VisuGUI_InputPane::eventFilter (QObject* object, QEvent* event)
{
  if (event->type() == QEvent::KeyPress) {
    QKeyEvent* aKeyEvent = (QKeyEvent*)event;
    if (object == mySelectedGroups && aKeyEvent->key() == Qt::Key_Delete)
      onRemove();
  }
  return QObject::eventFilter(object, event);
}


//---------------------------------------------------------------
/*!
  Called when the checkbox is toggled
*/
void VisuGUI_InputPane::onTypeChanged( )
{
  bool toEnable = myUseGroupsGroupBox->isChecked();
  if (!toEnable)
    {
      myAllGroups->clearSelection();
      mySelectedGroups->clearSelection();
    }
  myAllGroups->setEnabled( toEnable );
  mySelectedGroups->setEnabled( toEnable );
  myAddButton->setEnabled( toEnable );
  myRemoveButton->setEnabled( toEnable );
}


//---------------------------------------------------------------
/*!
  Called when add button is clicked, adds item to choosen groups
*/
void VisuGUI_InputPane::onAdd()
{
  QStringList aList;
  
  //for (int i = 0; i < myAllGroups->count(); i++)
  //  if (myAllGroups->isSelected(i))
  //    aList.append(myAllGroups->text(i));
  QList<QListWidgetItem*> aItemList = myAllGroups->selectedItems();
  QList<QListWidgetItem*>::Iterator it = aItemList.begin();
  for ( ; it != aItemList.end(); it++ )
    aList.append( (*it)->text() );  
  
  for (int i = 0; i < mySelectedGroups->count(); i++)
    aList.removeAll(mySelectedGroups->item(i)->text());
    
  mySelectedGroups->insertItems( mySelectedGroups->count(), aList);
}


//---------------------------------------------------------------
/*!
  Called when remove button is clicked, remove selected items from choosen
*/
void VisuGUI_InputPane::onRemove()
{
  QList<QListWidgetItem*> aList = mySelectedGroups->selectedItems();
  
  //aList.setAutoDelete(false);
  //for (int i = 0; i < mySelectedGroups->count(); i++)
  //  if (mySelectedGroups->isSelected(i))
  //    aList.append(mySelectedGroups->item(i));
  
  for (int i = 0; i < aList.count(); i++)
    delete aList.at(i);
}


//---------------------------------------------------------------
/*!
  Called when an item of listbox is double-clicked
*/
void VisuGUI_InputPane::onListDoubleClicked( QListWidgetItem* theItem )
{
  QListWidget* aListWidget = theItem->listWidget();

  if (aListWidget == myAllGroups)
    {
      QList<QListWidgetItem*> aList = mySelectedGroups->findItems( theItem->text(), Qt::MatchExactly );
      if ( aList.isEmpty() )
        mySelectedGroups->insertItem( mySelectedGroups->count(), theItem->text()  );
    }
  else if (aListWidget == mySelectedGroups)
    delete theItem;
}


//---------------------------------------------------------------
bool VisuGUI_InputPane::check()
{
  return myTimeStamps->count() != 0;
}


//---------------------------------------------------------------
void VisuGUI_InputPane::clear()
{
  myMedFile->clear();
  myMeshName->clear();
  myEntityName->clear();
  myFieldName->clear();
  myTimeStamps->clear();
}


//---------------------------------------------------------------
void VisuGUI_InputPane::onSelectionChanged()
{
  //clear();

  SALOME_ListIO aListIO;
  VISU::GetSelectionMgr( myModule )->selectedObjects(aListIO);

  if (aListIO.Extent() != 1)
    return;

  const Handle(SALOME_InteractiveObject)& anIO = aListIO.First();

  _PTR(Study) aCStudy = VISU::GetCStudy(VISU::GetAppStudy(myModule));
  _PTR(SObject) aSObject = aCStudy->FindObjectID(anIO->getEntry());
  if (!aSObject)
    return;

  VISU::VISUType aType = VISU::Storable::SObject2Type(aSObject);
  if (aType == VISU::TFIELD)
  {
    _PTR(SObject) aMedObject = aSObject->GetFather()->GetFather()->GetFather();
    if( !aMedObject )
      return;

    myTimeStamps->clear();

    QString anEntityName, aTimeStampName;

    _PTR(StudyBuilder) aBuilder = aCStudy->NewBuilder();
    _PTR(ChildIterator) aIter = aCStudy->NewChildIterator(aSObject);
    for( ; aIter->More(); aIter->Next() )
    {
      _PTR(SObject) aChildObj = aIter->Value();
      if( !aChildObj )
        return;

      if( anEntityName.isNull() )
      {
        _PTR(SObject) aRefObj;
        if( aChildObj->ReferencedObject( aRefObj ) )
          anEntityName = aRefObj->GetName().c_str();
      }

      VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap(aChildObj);
      if( aRestoringMap["myComment"] == "TIMESTAMP" )
      {
        aTimeStampName = aChildObj->GetName().c_str();
        myTimeStamps->addItem( aTimeStampName );
      }
    }

    myResult = VISU::FindResult( VISU::GetSObject( aSObject ).in() );

    VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap(aSObject);
    myEntity = aRestoringMap["myEntityId"].toInt();

    QString aMedFile = aMedObject->GetName().c_str();
    QString aMeshName = aRestoringMap["myMeshName"];
    QString aFieldName = aRestoringMap["myName"];

    myMedFile->setText( aMedFile );
    myMeshName->setText( aMeshName );
    myEntityName->setText( anEntityName );
    myFieldName->setText( aFieldName );
    myTimeStamps->setCurrentIndex( 0 );

    if( myReInit->isChecked() && myPrs )
    {
      QApplication::setOverrideCursor(Qt::WaitCursor);

      myPrs->SetResultObject( myResult );
      myPrs->SetMeshName( aMeshName.toLatin1().data() );
      myPrs->SetEntity( VISU::Entity( myEntity ) );
      myPrs->SetFieldName( aFieldName.toLatin1().data() );
      myPrs->SetTimeStampNumber( myPrs->GetTimeStampNumberByIndex( 0 ) );
      myPrs->Apply( true );

      myDialog->initFromPrsObject( myPrs, false );

      QApplication::restoreOverrideCursor();
    }
  }
}


//---------------------------------------------------------------
void VisuGUI_InputPane::changeTimeStamp( int theTimeStamp )
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  myPrs->SetTimeStampNumber( myPrs->GetTimeStampNumberByIndex( theTimeStamp ) );
  myPrs->Apply( true );
  myDialog->initFromPrsObject( myPrs, false );

  QApplication::restoreOverrideCursor();
}


//---------------------------------------------------------------
void VisuGUI_InputPane::onReInitialize( bool on )
{
  if( on )
    onSelectionChanged();
}


//---------------------------------------------------------------
void VisuGUI_InputPane::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs )
{
  if( myPrs == thePrs )
    return;

  myPrs = thePrs;

  clear();

  CORBA::Long aTimeStampNumber = thePrs->GetTimeStampNumber();
  VISU::ColoredPrs3dHolder::TimeStampsRange_var aTimeStampsRange = thePrs->GetTimeStampsRange();
  CORBA::Long aLength = aTimeStampsRange->length();

  for( int index = 0; index < aLength; index++ )
  {
    VISU::ColoredPrs3dHolder::TimeStampInfo anInfo = aTimeStampsRange[ index ];
    QString aTime = anInfo.myTime.in();
    myTimeStamps->addItem( aTime );
  }

  myResult = thePrs->GetResultObject();
  VISU::Result_i* aResult = dynamic_cast<VISU::Result_i*>(GetServant(myResult).in());
  myMedFile->setText( aResult->GetName().c_str() );

  myEntity = (int)thePrs->GetEntity();

  QString anEntityName;
  switch( myEntity )
  {
    case VISU::NODE_ENTITY: 
      anEntityName = "onNodes"; 
      break;
    case VISU::EDGE_ENTITY: 
      anEntityName = "onEdges"; 
      break;
    case VISU::FACE_ENTITY: 
      anEntityName = "onFaces"; 
      break;
    case VISU::CELL_ENTITY: 
      anEntityName = "onCells"; 
      break;
    default: break;
  }
  myEntityName->setText( anEntityName );

  myMeshName->setText( thePrs->GetMeshName() );
  myFieldName->setText( thePrs->GetFieldName() );
  myTimeStamps->setCurrentIndex( thePrs->GetTimeStampIndexByNumber( aTimeStampNumber ) );

  myFieldFilter->setPrs3dEntry( thePrs->GetHolderEntry().c_str() );
  bool anIsTimeStampFixed = myPrs->IsTimeStampFixed();
  myDataSourceGroupBox->setEnabled(!anIsTimeStampFixed);
  if(!anIsTimeStampFixed)
    VISU::GetSelectionMgr( myModule )->installFilter( myFieldFilter );
  
  // type of presentation and groups
  VISU::Result_i::PInput anInput = aResult->GetInput();
  const VISU::TMeshMap& aMeshMap = anInput->GetMeshMap();
  std::string aMeshName = thePrs->GetCMeshName();
  VISU::TMeshMap::const_iterator aMeshIter = aMeshMap.find(aMeshName);
  if(aMeshIter != aMeshMap.end()){
    const VISU::PMesh& aMesh = aMeshIter->second;
    const VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
    VISU::TGroupMap::const_iterator aGroupIter = aGroupMap.begin();
    for(; aGroupIter != aGroupMap.end(); aGroupIter++){
      const std::string& aGroupName = aGroupIter->first;
      myAllGroups->insertItem( myAllGroups->count(), aGroupName.c_str());
    }
  }

  if(myAllGroups->count() < 1){
    myAllGroups->insertItem( myAllGroups->count(), tr("NO_GROUPS") );
    myUseGroupsGroupBox->setEnabled(false);
  }else{
    const VISU::ColoredPrs3d_i::TGroupNames& aGroupNames = thePrs->GetGroupNames();
    VISU::ColoredPrs3d_i::TGroupNames::const_iterator anIter = aGroupNames.begin();
    for(; anIter != aGroupNames.end(); anIter++){
      const std::string aGroupName = *anIter;
      mySelectedGroups->insertItem(mySelectedGroups->count(), aGroupName.c_str());
    }
    myUseGroupsGroupBox->setEnabled(true);
  }
  myUseGroupsGroupBox->setChecked(mySelectedGroups->count() > 0);
}


//---------------------------------------------------------------
int VisuGUI_InputPane::storeToPrsObject( VISU::ColoredPrs3d_i* thePrs )
{
  if(myUseGroupsGroupBox->isChecked()){
    thePrs->RemoveAllGeom();
    for(int i = 0; i < mySelectedGroups->count(); i++)
      thePrs->AddMeshOnGroup(mySelectedGroups->item(i)->text().toLatin1().data() );
  }else
    thePrs->SetSourceGeometry();
  
  thePrs->SetResultObject( myResult );

  thePrs->SetMeshName( myMeshName->text().toLatin1().data() );
  thePrs->SetEntity( VISU::Entity( myEntity ) );
  thePrs->SetFieldName( myFieldName->text().toLatin1().data() );
  thePrs->SetTimeStampNumber( thePrs->GetTimeStampNumberByIndex( myTimeStamps->currentIndex() ) );
  return ( int )thePrs->Apply( false );
}


//---------------------------------------------------------------

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef DIALOGBOX_CLIPPING_H
#define DIALOGBOX_CLIPPING_H

#include "SALOME_InteractiveObject.hxx"

#include "SalomeApp_DoubleSpinBox.h"

// QT Includes
#include <QDialog>
#include <QPointer>

// VTK Includes
#include <vtkSmartPointer.h>
#include <vtkPlane.h>

// STL Includes
#include <vector>

class QLabel;
class QPushButton;
class QCheckBox;
class QGroupBox;
class QComboBox;
class QButtonGroup;
class QTabWidget;
class QListWidget;

class SALOME_Actor;

class SVTK_ViewWindow;

class LightApp_SelectionMgr;

class SalomeApp_IntSpinBox;

class VisuGUI;
class vtkPlaneSource;
class vtkDataSetMapper;
class OrientedPlane;
class VISU_Actor;
class PreviewPlane;

namespace VISU {
  class Prs3d_i;

  typedef vtkSmartPointer<OrientedPlane> TVTKPlane;
  typedef std::vector<TVTKPlane> TPlanes;
  enum Orientation {XY, YZ, ZX};
};

//=================================================================================
//class    : OrientedPlane
//purpose  :
//=================================================================================
class OrientedPlane: public vtkPlane
{
  QPointer<SVTK_ViewWindow> myViewWindow;
  vtkDataSetMapper* myMapper;

public:
  static OrientedPlane* New();
  static OrientedPlane* New (SVTK_ViewWindow* vw);

  vtkTypeMacro(OrientedPlane, vtkPlane);

  VISU::Orientation myOrientation;
  float myDistance;
  double myAngle[2];

  vtkPlaneSource* myPlaneSource;
  SALOME_Actor* myActor;

  void SetOrientation(VISU::Orientation theOrientation);
  VISU::Orientation GetOrientation();

  void SetDistance(float theDistance);
  float GetDistance();

  void ShallowCopy(OrientedPlane* theOrientedPlane);

protected:
  OrientedPlane();
  OrientedPlane(SVTK_ViewWindow* vw);
  ~OrientedPlane();

  void Init();

private:
  OrientedPlane(const OrientedPlane&);
  void operator=(const OrientedPlane&);
};

//=================================================================================
// class    : VisuGUI_ClippingDlg
// purpose  :
//=================================================================================
class VisuGUI_ClippingDlg : public QDialog
{
    Q_OBJECT

public:
    VisuGUI_ClippingDlg( VisuGUI* theModule,
                         bool modal = false );

    float  getDistance()  { return (float)SpinBoxDistance->value(); }
    void   setDistance(const float theDistance) { SpinBoxDistance->setValue(theDistance); }
    double getRotation1() { return SpinBoxRot1->value(); }
    double getRotation2() { return SpinBoxRot2->value(); }
    void   setRotation(const double theRot1, const double theRot2);
    void Sinchronize();

    ~VisuGUI_ClippingDlg();

private:
    void keyPressEvent( QKeyEvent* e );
  
    void SetPrs3d(VISU::Prs3d_i* thePrs);
  
    VISU_Actor* getSelectedActor();

    void applyLocalPlanes();

    void removeAllClippingPlanes(VISU::Prs3d_i* thePrs);

private:

    LightApp_SelectionMgr* mySelectionMgr;
    Handle(SALOME_InteractiveObject) myIO;

    VisuGUI       * myVisuGUI;
    VISU::Prs3d_i * myPrs3d;
    VISU::TPlanes   myPlanes;

    QComboBox*        ComboBoxPlanes;
    QPushButton*      buttonNew;
    QPushButton*      buttonDelete;

    QLabel*           TextLabelOrientation;
    QLabel*           TextLabelDistance;
    QLabel*           TextLabelRot1;
    QLabel*           TextLabelRot2;

    QTabWidget*       TabPane;

    QComboBox*        ComboBoxOrientation;
    SalomeApp_DoubleSpinBox*  SpinBoxDistance;
    SalomeApp_DoubleSpinBox*  SpinBoxRot1;
    SalomeApp_DoubleSpinBox*  SpinBoxRot2;

    QWidget*          WidgetIJKTab;
    QButtonGroup*     ButtonGroupIJKAxis;
    QGroupBox*        GroupBoxIJKAxis;
    QLabel*           TextLabelIJKIndex;
    SalomeApp_IntSpinBox* SpinBoxIJKIndex;
    QCheckBox*        CheckBoxIJKPlaneReverse;

    QCheckBox*        PreviewCheckBox;
    QCheckBox*        AutoApplyCheckBox;

    QPushButton*      buttonOk;
    QPushButton*      buttonCancel;
    QPushButton*      buttonApply;
    QPushButton*      buttonHelp;

    bool myIsSelectPlane;

    VISU_Actor* myDSActor;
    QList<PreviewPlane*> myPreviewList;

protected:
    QWidget* createParamsTab();
    QWidget* createIJKParamsTab();
    void setIJKByNonStructured();
    bool isStructured() const;

public slots:

    void onSelectPlane(int theIndex);
    void ClickOnNew();
    void ClickOnDelete();
    void onSelectOrientation(int theItem);
    void SetCurrentPlaneParam();
    void SetCurrentPlaneIJKParam();
    void onIJKAxisChanged(int axis);
    void onTabChanged(QWidget* newTab);
    void onSelectionChanged();
    void OnPreviewToggle(bool theIsToggled);
    void ClickOnOk();
    void ClickOnCancel();
    void ClickOnApply();
    void ClickOnHelp();
};

#endif // DIALOGBOX_CLIPPING_H



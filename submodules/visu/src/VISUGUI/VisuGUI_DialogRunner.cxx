// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:        VisuGUI_DialogRunner.cxx
// Created:     Thu Oct  6 10:17:39 2005
// Author:      Alexander SOLOVYOV
//              <asl@multiplex.nnov.opencascade.com>
//
#include "VisuGUI_DialogRunner.h"

#include <QDialog>
#include <QEventLoop>
#include <QEvent>

VisuGUI_DialogRunner::VisuGUI_DialogRunner( QDialog* dlg )
: QObject(),
  myDlg( dlg )
{
  if( myDlg )
  {
    connect( myDlg, SIGNAL( destroyed() ), this, SLOT( onDialogDelete() ) );
    myDlg->installEventFilter( this );
    myEventLoop = new QEventLoop( this );
  }
}

VisuGUI_DialogRunner::~VisuGUI_DialogRunner()
{
}

int VisuGUI_DialogRunner::run( const bool modal )
{
  if( myEventLoop->isRunning() || !myDlg )
    return -1;

  if( modal )
    return myDlg->exec();

  myDlg->show();
  myEventLoop->exec();
  return myDlg->result();
}

void VisuGUI_DialogRunner::onDialogDelete()
{
  if( myEventLoop->isRunning() )
    myEventLoop->quit();

  myDlg = 0;
}

bool VisuGUI_DialogRunner::eventFilter( QObject* o, QEvent* e )
{
  if( o==myDlg && e && ( e->type()==QEvent::Close || e->type()==QEvent::Hide ) )
  {
    if( myEventLoop->isRunning() && !myDlg->isMinimized() )
      myEventLoop->quit();
    return false;
  }
  else
    return QObject::eventFilter( o, e );
}

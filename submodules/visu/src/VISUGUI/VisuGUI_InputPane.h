// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_InputPane.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISUGUI_INPUTPANE_H
#define VISUGUI_INPUTPANE_H

#include "VISUConfig.hh"

#include "SALOME_ListIO.hxx"

#include <QGroupBox>

class QCheckBox;
class QComboBox;
class QLineEdit;
class QListWidgetItem;
class QToolButton;

class SalomeApp_Module;
class LightApp_SelectionMgr;

class VisuGUI_ListWidget;
class VisuGUI_Prs3dDlg;
class VisuGUI_FieldFilter;

namespace VISU
{
  class ColoredPrs3d_i;
  class Result_i;
}

class VisuGUI_InputPane : public QGroupBox
{
  Q_OBJECT

public:
  VisuGUI_InputPane( VISU::VISUType theType,
                     SalomeApp_Module* theModule,
                     VisuGUI_Prs3dDlg* theDialog );
  virtual ~VisuGUI_InputPane();

public:
  virtual bool       check();
  virtual void       clear();

  void               initFromPrsObject( VISU::ColoredPrs3d_i* );
  int                storeToPrsObject( VISU::ColoredPrs3d_i* );

  bool               eventFilter (QObject* object, QEvent* event);
  void               SetRestoreInitialSelection(bool on);
  bool               GetRestoreInitialSelection();

public slots:
  virtual void       onSelectionChanged();
  virtual void       onReInitialize( bool );
  virtual void       changeTimeStamp( int );

private slots:
  void               onTypeChanged();
  void               onListDoubleClicked( QListWidgetItem* theItem );
  void               onAdd();
  void               onRemove();

private:
  SalomeApp_Module*  myModule;
  VisuGUI_Prs3dDlg*  myDialog;
  VISU::ColoredPrs3d_i* myPrs;

  QGroupBox*         myUseGroupsGroupBox;
  VisuGUI_ListWidget* myAllGroups;
  VisuGUI_ListWidget* mySelectedGroups;
  QToolButton*       myAddButton;
  QToolButton*       myRemoveButton;

  QGroupBox*         myDataSourceGroupBox;
  QLineEdit*         myMedFile;
  QLineEdit*         myMeshName;
  QLineEdit*         myEntityName;
  QLineEdit*         myFieldName;
  QComboBox*         myTimeStamps;
  QCheckBox*         myReInit;

  VISU::Result_var   myResult;
  int                myEntity;
  bool               isRestoreInitialSelection;

  VisuGUI_FieldFilter* myFieldFilter;
  SALOME_ListIO        mySavedSelection;
};

#endif

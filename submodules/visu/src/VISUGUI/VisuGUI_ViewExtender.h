// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_ViewExtender.h
//  Author : Vitaly Smetannikov
//  Module : VISU
//
#ifndef VisuGUI_ViewExtender_HeaderFile
#define VisuGUI_ViewExtender_HeaderFile

#include <CAM_ViewExtender.h>
#include <SALOME_ListIO.hxx>
#include <QObject>
#include <QMap>

class VisuGUI;
//class QtxAction;
//class QDialog;

class VisuGUI_SegmentationMgr;
class SVTK_Viewer;
class SVTK_ViewWindow;
class SUIT_ViewWindow;
class VTKViewer_Actor;


class VisuGUI_ViewExtender:  public QObject, public CAM_ViewExtender
{
  Q_OBJECT

 public:
  VisuGUI_ViewExtender(VisuGUI* theModule);

  virtual ~VisuGUI_ViewExtender();

  virtual int createToolbar(SUIT_ViewWindow* theView);
  virtual void contextMenuPopup(QMenu* theMenu);

  virtual void activate(SUIT_ViewModel*);
  virtual void deactivate(SUIT_ViewModel*);

  VisuGUI_SegmentationMgr* getSegmentationMgr(SVTK_ViewWindow* theWindow);
 

private slots:
  //  void onPlanesMgr();
  // void onDialogDestroy(); 
  // void onShowPlanes(bool);
  // void onDeactivatePlanes(bool);
 void onViewDeleted(SUIT_ViewWindow* theWindow);
 void onAddActor(SVTK_ViewWindow* theWindow, VTKViewer_Actor* theActor);

 private:

 //enum { ClippingPlaneMgrId, ShowClippingPlanesId, DeactivateClippingPlanesId };

 //QMap<int, QtxAction*> myActionsMap;
  VisuGUI* myModule;

  //QDialog* myNonModalDlg;

  //VisuGUI_SegmentationMgr* mySegmentationMgr;
  QMap<SVTK_ViewWindow*, VisuGUI_SegmentationMgr*> myViewMgrMap;

  QList<SVTK_Viewer*> myViewers;

  //SALOME_ListIO myListIO;
  
};


#endif

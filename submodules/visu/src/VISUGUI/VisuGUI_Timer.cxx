// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Timer.cxx
//  Module : SALOME
//
#include "VisuGUI_Timer.h"

#include "SUIT_Desktop.h"

#include "utilities.h"

#ifndef WNT
static struct timezone *tz=(struct timezone*) malloc(sizeof(struct timezone));
#else
//timezone *tz=_timezone;
#endif

#ifndef CLK_TCK
# define CLK_TCK      CLOCKS_PER_SEC
#endif

VisuGUI_Timer::VisuGUI_Timer() :
  Utils_Timer()
{
}

VisuGUI_Timer::~VisuGUI_Timer()
{
}

void VisuGUI_Timer::Start()
{
  if (Stopped) {
    Stopped = 0;
#ifndef WNT
    times(RefToInitialTMS);
    gettimeofday(RefToInitialTimeB,tz);
#else
    SYSTEMTIME st;
    GetSystemTime(&st);
    SystemTimeToFileTime(&st, RefToInitialTMS);
          time(RefToCurrentTimeB);
#endif
  }
}

void VisuGUI_Timer::Stop()
{
  if (!Stopped) {
#ifndef WNT
    times(RefToCurrentTMS);
    int diffr_user = RefToCurrentTMS->tms_utime - RefToInitialTMS->tms_utime;
    int diffr_sys  = RefToCurrentTMS->tms_stime - RefToInitialTMS->tms_stime;
    gettimeofday(RefToCurrentTimeB,tz);

    static long aCLK_TCK=sysconf(_SC_CLK_TCK);
    Cumul_user += (double) diffr_user / aCLK_TCK ;
    Cumul_sys  += (double) diffr_sys  / aCLK_TCK ;
#else
    SYSTEMTIME st;
    GetSystemTime(&st);
    SystemTimeToFileTime(&st, RefToCurrentTMS);
    Cumul_user += (int)(((ULARGE_INTEGER*)(RefToCurrentTMS))->QuadPart - ((ULARGE_INTEGER*)(RefToInitialTMS))->QuadPart) / 10000000;
          Cumul_sys = Cumul_user;
          time(RefToCurrentTimeB);
#endif
   Stopped = 1;
  }
}

void VisuGUI_Timer::Reset() {
  Stopped     = 1;
  Cumul_user  = Cumul_sys = 0. ;
}

QString VisuGUI_Timer::GetTime() {
  bool StopSav = Stopped;
  if (!StopSav) Stop();

  return QString::number( Cumul_user );

  if (!StopSav) Start();
}

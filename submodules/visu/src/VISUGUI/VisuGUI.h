// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//
#ifndef VisuGUI_HeaderFile
#define VisuGUI_HeaderFile

#include <SalomeApp_Module.h>

#include <SVTK_ViewWindow.h>
#include <VISU_Prs3d_i.hh>

#include "VisuGUI_Panel.h"

#include <map>
#include <set>

class SUIT_ViewManager;
class VisuGUI_Slider;
class VisuGUI_Sweep;

namespace VISU
{
  class Prs3d_i;
  typedef std::pair<Prs3d_i*,int> TPrs3dToInd; // prs pointer and default index of scalar bar placement
  typedef std::set<TPrs3dToInd> TSetPrs3d;
  typedef std::map<SVTK_ViewWindow*,TSetPrs3d> TViewToPrs3d;
}

class VisuGUI: public SalomeApp_Module
{
  Q_OBJECT;

public:
  enum PanelId { SelectionPanelId = 0,
                 FeatureEdgesPanelId,
                 ClippingPlanesPanelId };

public:
  VisuGUI();
  virtual ~VisuGUI();

  virtual LightApp_Displayer*    displayer();
  virtual void initialize( CAM_Application* );
  virtual void windows( QMap<int, int>& ) const;
  virtual void viewManagers( QStringList& ) const;

  virtual QString engineIOR() const;

  virtual void  studyActivated();

  virtual void  contextMenuPopup( const QString& theClient, QMenu* theMenu, QString& theTitle );

  virtual void createPreferences();
  virtual void preferencesChanged( const QString&, const QString& );

  virtual
  SUIT_ViewManager*
  getViewManager(const QString& theType, 
                 const bool theIsCreate);

  VISU::TViewToPrs3d&
  getScalarBarsMap();

  virtual bool renameAllowed( const QString& ) const;
  virtual bool renameObject( const QString&, const QString& );
  

signals:
  void moduleDeactivated();
  void moduleActivated();
  void presentationCreated(VISU::Prs3d_i* thePrs);
  void beforeObjectDelete(QString theEntry);

public slots:
  virtual bool deactivateModule( SUIT_Study* );
  virtual bool activateModule( SUIT_Study* );
  virtual void OnEraseAll();

protected slots:
  void OnImportFromFile();
  void OnImportFromSauvFile();
  void OnImportTableFromFile();
  void OnExportTableToFile();
  void OnImportMedField();

  void OnLoadComponentData();

  void OnCreateMesh();
  void OnCreateScalarMap();
  void OnCreateDeformedShape();
  void OnCreateVectors();
  void OnCreateIsoSurfaces();
  void OnCreateCutPlanes();
  void OnCreateCutLines();
  void OnCreateCutSegment();
  void OnCreateStreamLines();
  void OnCreatePlot3D();
  void OnCreateManyMesh();
  void OnCreatePlot2dView();
  void OnCreateDeformedShapeAndScalarMap();

  void OnEditScalarMap();
  void OnEditDeformedShape();
  void OnEditCutPlanes();
  void OnEditCutLines();
  void OnEditCutSegment();
  void OnEditIsoSurfaces();
  void OnEditVectors();
  void OnEditStreamLines();
  void OnEditPlot3D();
  void OnEditDeformedShapeAndScalarMap();

  virtual void OnDisplayPrs();
  virtual void OnDisplayOnlyPrs();
  virtual void OnErasePrs();
  //  virtual void OnEraseAll();

  void OnToggleSelectionPanel();
  void OnSwitchSelectionMode();
  void OnSwitchSelectionMode( int );

  void OnMakeSurfaceframe();
  void OnMakeInsideframe();
  void OnMakeWireframe();
  void OnMakeSurface();
  void OnMakePoints();
  void OnMakeShrink();

  void OnMakeFeatureEdges();

  void OnSetShadingOn();
  void OnSetShadingOff();

  void OnChangeColor();
  void OnChangeWireframeColor();
  void OnChangeOpacity();
  void OnChangeLines();
  void OnChangePointMarker();
  void OnChangeShrinkFactor();

  void OnShowTable();
  void OnShowTablePlot();
  void OnShowScalarBar();
  void OnHideScalarBar();
  void OnCreateTable();
  void OnDeleteObjects();
  void OnPlotData();
  void OnCurveProperties();
  void OnCurveScale();
  void OnClearContainer();
  void OnEditContainer();

  void OnMergeScalarBars();
  void OnFreeScalarBars();

  virtual void OnSaveViewParams();
  virtual void OnRestoreViewParams();

  void OnClippingPlanes();
  void OnSweep();
  void OnParallelTimeAnimation();
  void OnSucccessiveTimeAnimation();
  void OnShowAnimation();

  void OnPointEvolution();
  void OnShowEvolution();

  void OnCopyPresentation();

  void OnTranslatePrs();
  void OnArrangeActors();
  void OnPlot3dFromCutPlane();

  void OnManageCache();

  void OnFilterScalars();
  
  void OnValuesLabeling();
  void OnValuesLabelingParams();

  void OnArcQuadMode();
  void OnLineQuadMode();

  void OnShowDeviation();

  // MULTIPR
  void OnMultiprViewFullRes();
  void OnMultiprViewMediumRes();
  void OnMultiprViewLowRes();
  void OnMultiprViewHide();
  void OnMultiprChangeRes(VISU::Result::Resolution theResolution);

  void OnFileInfo();
  void OnExportMED();

protected:
  virtual LightApp_Selection* createSelection() const;

  virtual void processPresentationCreated( VISU::Prs3d_i* thePrs );

  void OnTimeAnimation(VISU::Animation::AnimationMode theMode);

private:
  void createActions();
  void createMenus();
  void createToolBars();
  void createPopupMenus();
  void createPanels();
  void SetScalarBarVisibility( bool );
  void activateAppropriateViewer( Handle(SALOME_InteractiveObject) theIO );
  int  addVtkFontPref( const QString& label, const int pId, const QString& param, const bool useSize );

private:
  QMap<int,VisuGUI_Panel*> myPanels;
  VisuGUI_Slider*          mySlider;
  VisuGUI_Sweep*           mySweep; 

  LightApp_Displayer*      myDisplayer;
  VISU::TViewToPrs3d       myScalarBarsMap;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Plot3DDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_PLOT3D_H
#define VISUGUI_PLOT3D_H

#include "VisuGUI_Prs3dDlg.h"


class QButtonGroup;
class QTabWidget;
class QGroupBox;

class SalomeApp_Module;
class SalomeApp_IntSpinBox;

class VisuGUI_InputPane;
class SalomeApp_DoubleSpinBox;

namespace VISU 
{
  class Plot3D_i;
};

class VISU_Plot3DPL;
class SVTK_ViewWindow;
class SALOME_Actor;

class VisuGUI_Plot3DPane : public QWidget//QVBox
{
  Q_OBJECT

 public:
  VisuGUI_Plot3DPane(QWidget* parent);
  ~VisuGUI_Plot3DPane();

  void initFromPrsObject (VISU::Plot3D_i* thePrs);
  int  storeToPrsObject  (VISU::Plot3D_i* thePrs);

  bool check();

  VISU::Plot3D_i* GetPrs() { return myPrs; }

  void setPlane(int theOrientation, double theXRotation, double theYRotation, double thePlanePos);

 private:
  bool                 myInitFromPrs;
  SALOME_Actor*        myPreviewActor;
  SVTK_ViewWindow*     myViewWindow;
  VISU::Plot3D_i*      myPrs;
  VISU_Plot3DPL*       myPipeCopy;

  void storePrsParams();
  void restorePrsParams();

  QButtonGroup  * GBOrientation;
  QGroupBox     * GBoxOrient;
  QLabel        * LabelRot1;
  QLabel        * LabelRot2;
  SalomeApp_DoubleSpinBox * Rot1;
  SalomeApp_DoubleSpinBox * Rot2;
  SalomeApp_DoubleSpinBox * PositionSpn;
  QCheckBox     * RelativeChkB;
  SalomeApp_DoubleSpinBox * ScaleSpn;
  QButtonGroup  * GBPrsType;
  SalomeApp_IntSpinBox * NbContoursSpn;
  QCheckBox     * PreviewChkB;

 private slots:

  void orientationChanged( int );
  void onRelativePos( bool );
  void onPrsType( int );
  void onPositionSpn();
  void updatePreview();
};


class VisuGUI_Plot3DDlg : public VisuGUI_ScalarBarBaseDlg
{
  Q_OBJECT

 public:
  VisuGUI_Plot3DDlg (SalomeApp_Module* theModule);
  ~VisuGUI_Plot3DDlg();

  virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                  bool theInit );

  virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

  void setPlane(int theOrientation, double theXRotation, double theYRotation, double thePlanePos);

 protected:
  virtual QString GetContextHelpFilePath();
 
 protected slots:
  void accept();
  void reject();

 private:
  QTabWidget*            myTabBox;
  VisuGUI_Plot3DPane*    myIsoPane;
  VisuGUI_InputPane*     myInputPane;

  SALOME::GenericObjPtr<VISU::Plot3D_i> myPrsCopy;
};

#endif // VISUGUI_PLOT3D_H

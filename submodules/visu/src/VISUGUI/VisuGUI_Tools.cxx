// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Tools.cxx
//  Author : Sergey Anikin
//  Module : VISU

#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_Prs3dTools.h"
#include "VisuGUI_DialogRunner.h"

#include "VisuGUI.h"

#include "VISU_Tools.h"

#include "VISU_ColoredPrs3dHolder_i.hh"
#include "VISU_Gen_i.hh"
#include "VISU_Prs3d_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_Table_i.hh"
//#include "VISU_Mesh_i.hh"
#include "VISU_ViewManager_i.hh"

#include "VISU_Actor.h"
#include "VISU_ScalarMapAct.h"

#include "SalomeApp_Module.h"
#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"
#include "SalomeApp_IntSpinBox.h"
#include "SalomeApp_DoubleSpinBox.h"

#include "LightApp_DataObject.h"
#include "LightApp_SelectionMgr.h"

//TODO
//#include "OB_Browser.h"

#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"

#include "SVTK_ViewWindow.h"
#include "SVTK_Functor.h"

#include "VTKViewer_Algorithm.h"

#include "SPlot2d_ViewModel.h"
#include "Plot2d_ViewFrame.h"
#include "Plot2d_ViewManager.h"

#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_Desktop.h"
#include "SUIT_ViewWindow.h"
#include "SUIT_ResourceMgr.h"

#include "CAM_DataModel.h"

#include <vtkRenderer.h>
#include <vtkActorCollection.h>

#include <QString>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QVariant>

using namespace std;

//=============================================================================
namespace VISU
{
  //------------------------------------------------------------
  SUIT_Desktop*
  GetDesktop(const CAM_Module* theModule)
  {
    return theModule && theModule->application() ? theModule->application()->desktop() : 0;
  }

  //------------------------------------------------------------
  LightApp_SelectionMgr*
  GetSelectionMgr(const SalomeApp_Module* theModule)
  {
    return theModule && theModule->getApp() ? theModule->getApp()->selectionMgr() : 0;
  }

  //------------------------------------------------------------
  SalomeApp_Study*
  GetAppStudy(const CAM_Module* theModule)
  {
    return theModule && theModule->application() ?
      dynamic_cast<SalomeApp_Study*>(theModule->application()->activeStudy()) : 0;
  }

  //------------------------------------------------------------
  _PTR(Study)
  GetCStudy(const SalomeApp_Study* theStudy)
  {
    return theStudy ? theStudy->studyDS() : _PTR(Study)();
  }

  //------------------------------------------------------------
  bool
  IsStudyLocked( _PTR(Study) theStudy )
  {
    return theStudy ? theStudy->GetProperties()->IsLocked() : true;
  }

  //------------------------------------------------------------
  bool
  CheckLock( _PTR(Study) theStudy,
             QWidget* theWidget )
  {
    if(IsStudyLocked(theStudy)){
      SUIT_MessageBox::warning(theWidget,
                               QObject::tr("WRN_VISU_WARNING"),
                               QObject::tr("WRN_STUDY_LOCKED") );
        return true;
    }
    return false;
  }

  //------------------------------------------------------------
  int
  runAndWait( QDialog* dlg, const bool modal )
  {
    VisuGUI_DialogRunner r( dlg );
    return r.run( modal );
  }

  //------------------------------------------------------------
  LightApp_DataObject*
  FindDataObject(SUIT_DataObject* theDataObject,
                 const QString& theEntry,
                 int theLevel)
  {
    int aLevel = theDataObject->level() + 2;
    QString aSourceEntry = theEntry.section(':',0,aLevel);
    DataObjectList aList;
    theDataObject->children(aList);
    DataObjectList::Iterator aDataObjectIter = aList.begin();
    while( aDataObjectIter!= aList.end() ) {
      if(LightApp_DataObject* aChildDataObject = dynamic_cast<LightApp_DataObject*>(*aDataObjectIter)){
        QString anEntry = aChildDataObject->entry();
        QString aCurrentEntry = anEntry.section(':',0,aLevel);
        if(aSourceEntry == aCurrentEntry){
          if(theLevel == aLevel){
            return aChildDataObject;
          }else{
            return FindDataObject(aChildDataObject,theEntry,theLevel);
          }
        }
      }
      aDataObjectIter++;
    }
    return NULL;
  }

  //------------------------------------------------------------
  LightApp_DataObject*
  FindDataObject(CAM_Module* theModule,
                 _PTR(SObject) theSObject)
  {
    CAM_DataModel* aDataModel = theModule->dataModel();
    CAM_DataObject* aRootDataObject = aDataModel->root();
    if(_PTR(SComponent) aComponent = theSObject)
      return dynamic_cast<LightApp_DataObject*>(aRootDataObject);

    int aLevel = theSObject->Depth();
    std::string anEntry = theSObject->GetID();
    return FindDataObject(aRootDataObject,anEntry.c_str(),aLevel);
  }

  //------------------------------------------------------------
  void
  UpdateObjBrowser(SalomeApp_Module* theModule,
                   bool theIsUpdateDataModel,
                   _PTR(SObject) theSObject)
  {
    LightApp_DataObject* aDataObject = NULL;
    if(theSObject)
      aDataObject = FindDataObject(theModule,theSObject);

    theModule->updateObjBrowser(theIsUpdateDataModel,aDataObject);
    theModule->getApp()->updateActions();
  }

  //------------------------------------------------------------
  bool
  IsSObjectTable( _PTR(SObject) theSObject )
  {
    if ( theSObject ) {
      _PTR(GenericAttribute) anAttr;
      if (theSObject->FindAttribute( anAttr, "AttributeTableOfInteger" ))
        return true;
      if (theSObject->FindAttribute( anAttr, "AttributeTableOfReal" ))
        return true;
    }
    return false;
  }

  //------------------------------------------------------------
  VISU_Gen_i*
  GetVisuGen(const CAM_Module* theModule)
  {
    static VISU_Gen_i* aGen = 0;
    if(!aGen){
      SALOME_LifeCycleCORBA aLCC(SalomeApp_Application::namingService());
      Engines::EngineComponent_var aComponent = aLCC.FindOrLoad_Component("FactoryServer","VISU");
      VISU_Gen_var aVISU = VISU_Gen::_narrow(aComponent);
      if(!CORBA::is_nil(aVISU))
        aGen = VISU_Gen_i::GetVisuGenImpl();
    }

    if(aGen)
      aGen->SetCurrentStudy(GetDSStudy(GetCStudy(GetAppStudy(theModule))));
    else
      throw std::runtime_error(QObject::tr("ERR_CANT_FIND_VISU_COMPONENT").toLatin1().data());
    return aGen;
  }

#ifdef WITH_MEDGEN
  //------------------------------------------------------------
  SALOME_MED::MED_Gen_var
  GetMEDEngine()
  {
    static SALOME_MED::MED_Gen_var aGen;
    if(CORBA::is_nil(aGen)){
      SALOME_LifeCycleCORBA aLCC(SalomeApp_Application::namingService());
      Engines::EngineComponent_var aComponent = aLCC.FindOrLoad_Component("FactoryServer","MED");
      aGen = SALOME_MED::MED_Gen::_narrow(aComponent);
    }
    if(CORBA::is_nil(aGen))
      throw std::runtime_error(QObject::tr("ERR_CANT_FIND_MED_COMPONENT").toLatin1().data());
    return aGen;
  }
#endif // WITH_MEDGEN

  //----------------------------------------------------------------------------
  // Selection
  TSelectionInfo
  GetSelectedObjects(const SalomeApp_Module* theModule)
  {
    TSelectionInfo aSelectionInfo;
    const SalomeApp_Study* aStudy = GetAppStudy(theModule);
    LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(theModule);
    if ( aStudy && aSelectionMgr ) {
      SALOME_ListIO aListIO;
      aSelectionMgr->selectedObjects(aListIO);
      SALOME_ListIteratorOfListIO anIter(aListIO);
      while(anIter.More())
      {
        Handle(SALOME_InteractiveObject) anIO = anIter.Value();
        if(anIO->hasEntry())
        {
          TObjectInfo anObjectInfo = GetObjectByEntry(aStudy, anIO->getEntry());
          if(anObjectInfo.mySObject){
            TSelectionItem aSelectionItem;
            aSelectionItem.myObjectInfo = anObjectInfo;
            aSelectionItem.myIO = anIO;
            aSelectionInfo.push_back(aSelectionItem);
          }
        }
        anIter.Next(); // MULTIPR fixed
      }
    }
    return aSelectionInfo;
  }

  //----------------------------------------------------------------------------
  TObjectInfo
  GetObjectByEntry(const SalomeApp_Study* theStudy,
                   const std::string& theEntry)
  {
    TObjectInfo anObjectInfo;
    if(!theStudy || theEntry == "")
      return anObjectInfo;

    _PTR(Study) aStudy = GetCStudy(theStudy);
    if(_PTR(SObject) aSObject = aStudy->FindObjectID(theEntry)){
      anObjectInfo.mySObject = aSObject;
      CORBA::Object_var anObject = ClientSObjectToObject(aSObject);
      if(VISU::Base_i* aBase = GetServantInterface<VISU::Base_i>(anObject))
        anObjectInfo.myBase = aBase;
    }

    return anObjectInfo;
  }


  //----------------------------------------------------------------------------
  VISU::Prs3d_i*
  GetPrs3dToModify(const SalomeApp_Module* theModule,
                   Base_i* theBase)
  {
    if (CheckLock(GetCStudy(GetAppStudy(theModule)), GetDesktop(theModule)))
      return NULL;

    return GetPrs3dFromBase(theBase);
  }

  //----------------------------------------------------------------------------
  VISU::Prs3d_i*
  GetPrs3dFromBase(Base_i* theBase)
  {
    if(theBase && theBase->GetType() == VISU::TCOLOREDPRS3DHOLDER){
      CORBA::Object_var anObject = theBase->_this();
      VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);
      VISU::Prs3d_var aPrs3d = aHolder->GetDevice();
      return VISU::GetServantInterface<VISU::Prs3d_i>(aPrs3d);
    }
    return dynamic_cast<VISU::Prs3d_i*>(theBase);
  }


  //----------------------------------------------------------------------------
  bool
  GetPrs3dSelectionInfo(const SalomeApp_Module* theModule,
                        VISU::Prs3d_i*& thePrs3d,
                        SVTK_ViewWindow*& theViewWindow,
                        VISU_Actor*& thenActor)
  {
    VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(theModule);
    if(aSelectionInfo.empty())
      return false;

    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    thePrs3d = GetPrs3dFromBase(aSelectionItem.myObjectInfo.myBase);
    if(!thePrs3d)
      return false;

    theViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule);
    if(!theViewWindow)
      return false;

    thenActor = FindActor(theViewWindow, thePrs3d);
    if(!thenActor)
      return false;

    return true;
  }


  //----------------------------------------------------------------------------
  void
  Add(LightApp_SelectionMgr* theSelectionMgr,
      const Handle(SALOME_InteractiveObject)& theIO)
  {
    SALOME_ListIO aListIO;
    theSelectionMgr->selectedObjects(aListIO);
    aListIO.Append(theIO);
    theSelectionMgr->setSelectedObjects(aListIO);
  }

  //------------------------------------------------------------
  void
  Remove(LightApp_SelectionMgr* theSelectionMgr,
         const Handle(SALOME_InteractiveObject)& theIO)
  {
    if (theIO.IsNull()) return;
    SALOME_ListIO aListIO, aNewListIO;
    theSelectionMgr->selectedObjects(aListIO);
    SALOME_ListIteratorOfListIO anIter (aListIO);
    for (; anIter.More(); anIter.Next()) {
      Handle(SALOME_InteractiveObject) anIO = anIter.Value();
      if (!anIO->isSame(theIO)) {
        aNewListIO.Append(theIO);
      }
    }
    theSelectionMgr->setSelectedObjects(aNewListIO);
  }

  /*!
   * \brief Check, if the object can be deleted
   *
   * \param theEntry - entry of the object to check
   * \param theModule - is used to access Study and VISU_Gen
   * \retval bool - returns TRUE if the object is removable
   */
  bool
  IsRemovable (const std::string theEntry,
               const SalomeApp_Module* theModule)
  {
    // asv : if selected object is a Save Point object selected in object browser - return false
    if ( QString( theEntry.c_str() ).startsWith( QObject::tr( "SAVE_POINT_DEF_NAME" ) ) )
      return false;

    _PTR(Study) aStudy = GetCStudy(GetAppStudy(theModule));

    _PTR(SObject) aSObject = aStudy->FindObjectID(theEntry);
    VISU::Storable::TRestoringMap pMap;
    if (aSObject) {
      CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(aSObject);
      if (!CORBA::is_nil(aCORBAObject)) {
        VISU::RemovableObject_var aRemovableObj = VISU::RemovableObject::_narrow(aCORBAObject);
        if (CORBA::is_nil(aRemovableObj)) {
          // Not removable CORBA object
          return false;
        }
      } else {
        // Can be removed, if lays directly under VISU
        // (first sub-level) or is a child of such an object
        string aNAME, aVisuNAME = GetVisuGen(theModule)->ComponentDataType();
        _PTR(GenericAttribute) anAttr;
        _PTR(AttributeString) aComment;

        _PTR(SObject) aRefSObject;
        if (aSObject->ReferencedObject(aRefSObject)) {
          // It can be a reference on curve, published under a container. IPAL 20317
          VISU::TObjectInfo aRefObjectInfo = GetObjectByEntry(GetAppStudy(theModule), aRefSObject->GetID());
          VISU::Base_i* aRefBase = aRefObjectInfo.myBase;
          if( (aRefBase && aRefBase->GetType() == VISU::TCURVE) || (aRefBase && aRefBase->GetType() == VISU::TMESH) )
            return true;
        }

        bool isUnderVISU = false;
        _PTR(SObject) aFatherSObject = aSObject->GetFather();
        if (aFatherSObject->FindAttribute(anAttr, "AttributeName")) {
          // mkr : 24.11.2006 : use AttributeName and module title for correct "Delete"
          //                    popup item displaying in object browser popup
          _PTR(AttributeName) aComment (anAttr);
          aNAME = aComment->Value();
          QString aVisuTITLE = theModule->getApp()->moduleTitle(aVisuNAME.c_str());
          if (!aVisuTITLE.compare(aNAME.c_str())) {
            isUnderVISU = true;
          }
        }
        if (!isUnderVISU) {
          // Not directly under VISU component, check father
          aCORBAObject = VISU::ClientSObjectToObject(aFatherSObject);
          if (!CORBA::is_nil(aCORBAObject)) {
            // Father has IOR
            return false;
          }

          isUnderVISU = false;
          aFatherSObject = aFatherSObject->GetFather();
          if (aFatherSObject->FindAttribute(anAttr, "AttributeString")) {
            _PTR(AttributeString) aComment (anAttr);
            aNAME = aComment->Value();
            if (aNAME == aVisuNAME) {
              isUnderVISU = true;
            }
          }
          if (!isUnderVISU) {
            // Father is not directly under VISU component
            return false;
          }
        }
      }
    }
    return true;
  }

  /*!
   * \brief Check, if "Delete" popup-menu can be put on current selection
   *
   * \param theModule - is used to access SelectionManager, Study and VISU_Gen
   * \retval bool - returns TRUE if all currently selected objects are removable
   */
  bool
  IsRemovableSelected (const SalomeApp_Module* theModule)
  {
    LightApp_SelectionMgr* aSelectionMgr = GetSelectionMgr(theModule);
    SALOME_ListIO aListIO;
    aSelectionMgr->selectedObjects(aListIO);

    if (aListIO.Extent() < 1)
      return false;

    _PTR(Study) aStudy = GetCStudy(GetAppStudy(theModule));
    if (!aStudy) return false;

    SALOME_ListIteratorOfListIO anIter (aListIO);
    for (; anIter.More(); anIter.Next()) {
      Handle(SALOME_InteractiveObject) anIO = anIter.Value();
      if (anIO->hasEntry()) {
        if (!IsRemovable(anIO->getEntry(), theModule))
          return false;
      }
    }
    return true;
  }

  //------------------------------------------------------------
  void
  DeleteSObject(VisuGUI* theModule,
                _PTR(Study) theStudy,
                _PTR(SObject) theSObject)
  {
    _PTR(ChildIterator) aChildIter = theStudy->NewChildIterator(theSObject);
    for (aChildIter->InitEx(true); aChildIter->More(); aChildIter->Next()) {
      _PTR(SObject) aSObject = aChildIter->Value();
      TObjectInfo anObjectInfo = GetObjectByEntry(GetAppStudy(theModule), aSObject->GetID());
      ErasePrs(theModule, anObjectInfo.myBase, /*repaint_view_window = */false);
    }

    TObjectInfo anObjectInfo = GetObjectByEntry(GetAppStudy(theModule), theSObject->GetID());
    if(anObjectInfo.myBase){
      ErasePrs(theModule, anObjectInfo.myBase, /*repaint_view_window = */true);

      CORBA::Object_var anObject = ClientSObjectToObject(theSObject);
      VISU::RemovableObject_var aRemovableObject = VISU::RemovableObject::_narrow(anObject);
      if (!CORBA::is_nil(aRemovableObject)) {
        aRemovableObject->RemoveFromStudy();
      }
    } else {
      // Remove aSObject together with all its sub-objects
      VISU::RemoveFromStudy(theSObject,
                            false,  // remove not only IOR attribute, but Object With Children
                            false); // not UnRegister() sub-objects
    }
  }

  //------------------------------------------------------------
  void
  DeletePrs3d(VisuGUI* theModule,
              VISU::Prs3d_i* thePrs)
  {
    if (!thePrs)
      return;

    if (CheckLock(GetCStudy(GetAppStudy(theModule)),GetDesktop(theModule)))
      return;

    if(SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule))
      RemoveScalarBarPosition(theModule, aViewWindow, thePrs);

    if(VISU::ColoredPrs3d_i* aColoredPrs3d = dynamic_cast<VISU::ColoredPrs3d_i*>(thePrs)){
      std::string anEntry = aColoredPrs3d->GetHolderEntry();
      if(anEntry != ""){
        VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(theModule), anEntry);
        if(VISU::Base_i* aBase = anObjectInfo.myBase){
          if(aBase->GetType() == VISU::TCOLOREDPRS3DHOLDER){
            CORBA::Object_var anObject = aBase->_this();
            VISU::RemovableObject_var aRemovableObject = VISU::RemovableObject::_narrow(anObject);
            aRemovableObject->RemoveFromStudy();
            return;
          }
        }
      }
    }

    thePrs->RemoveFromStudy();
  }

  //------------------------------------------------------------
  // Presentation management
  void
  ChangeRepresentation (const SalomeApp_Module* theModule,
                        VISU::PresentationType  theType)
  {
    TSelectionInfo aSelectionInfo = GetSelectedObjects(theModule);
    if(aSelectionInfo.empty())
      return;

    VISU::Prs3d_i* aPrs3d;
    VISU::PointMap3d_i* aTable;
    VISU_Actor* anActor;
    VISU_ActorBase* anActorBase;

    TSelectionItem aSelectionItem = aSelectionInfo.front();
    aPrs3d = GetPrs3dFromBase(aSelectionItem.myObjectInfo.myBase);
    if(!aPrs3d) {
      aTable = dynamic_cast<VISU::PointMap3d_i*>(aSelectionItem.myObjectInfo.myBase);
      if(!aTable)
        return;
    }

    SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule);
    if(!aViewWindow)
      return;

    if (aPrs3d) {
      anActor = FindActor(aViewWindow, aPrs3d);
      if(!anActor)
        return;
    } else {
      anActorBase = VISU::FindActorBase(aViewWindow, aTable);
      if(!anActorBase)
        return;
    }

    switch (theType) {
    case VISU::SHRINK:
      if (aPrs3d) {
        bool toShrink;
        if (anActor->IsShrunk()) {
          anActor->UnShrink();
          toShrink = false;
        }
        else {
          anActor->SetShrink();
          toShrink = true;
        }
        if (VISU::Mesh_i* aMesh = dynamic_cast<VISU::Mesh_i*>(aPrs3d)) {
          aMesh->SetShrink(toShrink);
        }
      } else if (aTable) {
        if (anActorBase->IsShrunk())
          anActorBase->UnShrink();
        else
          anActorBase->SetShrink();
      }
      break;
    default:
      if (aPrs3d) {
        if (VISU::Mesh_i* aMesh = dynamic_cast<VISU::Mesh_i*>(aPrs3d)) {
          aMesh->SetPresentationType(theType);
          RecreateActor(theModule, aMesh);
        } else {
          anActor->SetRepresentation(theType);
        }
      }
      else if (aTable) {
        anActorBase->SetRepresentation(theType);
      }
    }
    aViewWindow->Repaint();
  }


  void ChangeQuadratic2DRepresentation (const SalomeApp_Module* theModule,
                                        VISU::Quadratic2DPresentationType  theType)
  {
    TSelectionInfo aSelectionInfo = GetSelectedObjects(theModule);
    if(aSelectionInfo.empty())
      return;

    TSelectionItem aSelectionItem = aSelectionInfo.front();

    VISU::Prs3d_i* aPrs3d = GetPrs3dFromBase(aSelectionItem.myObjectInfo.myBase);

    SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule);

    if(!aPrs3d || !aViewWindow)
      return;

    VISU_Actor *anActor = FindActor(aViewWindow, aPrs3d);
    if(!anActor)
      return;

    if (VISU::Mesh_i* aMesh = dynamic_cast<VISU::Mesh_i*>(aPrs3d)) {
      aMesh->SetQuadratic2DPresentationType(theType);
      RecreateActor(theModule, aMesh);
    } else {
      switch(theType){
      case VISU::LINES:
        anActor->SetQuadratic2DRepresentation(VISU_Actor::eLines);
        break;
      case VISU::ARCS:
        anActor->SetQuadratic2DRepresentation(VISU_Actor::eArcs);
        break;
      default:
        break;
      }
    }
  }

  //------------------------------------------------------------
  void
  SetShading ( const SalomeApp_Module* theModule,
              bool theOn )
  {
    TSelectionInfo aSelectionInfo = GetSelectedObjects(theModule);
    if(aSelectionInfo.empty())
      return;

    TSelectionItem aSelectionItem = GetSelectedObjects(theModule).front();
    VISU::Prs3d_i* aPrs3d = GetPrs3dFromBase(aSelectionItem.myObjectInfo.myBase);
    if(!aPrs3d)
      return;

    SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule);
    if(!aViewWindow)
      return;

    VISU_Actor* anActor = FindActor(aViewWindow, aPrs3d);
    if(!anActor)
      return;

    if(VISU_ScalarMapAct* aScalarMapActor = dynamic_cast<VISU_ScalarMapAct*>(anActor)){
      aScalarMapActor->SetShading(theOn);
      aViewWindow->Repaint();
    }
  }

  //------------------------------------------------------------
  // SObject type
  bool
  CheckTimeStamp(const SalomeApp_Module* theModule,
                 _PTR(SObject)&          theSObject,
                 Handle(SALOME_InteractiveObject)& theIO,
                 ColoredPrs3d_i::EPublishInStudyMode& thePublishInStudyMode)
  {
    TSelectionInfo aSelectionInfo = GetSelectedObjects(theModule);
    if(!aSelectionInfo.empty()){
      TSelectionItem aSelectionItem = aSelectionInfo.front();
      theIO = aSelectionItem.myIO;
      theSObject = aSelectionItem.myObjectInfo.mySObject;
      VISU::VISUType aType = VISU::Storable::SObject2Type(theSObject);
      if (VISU::TTIMESTAMP == aType){
        thePublishInStudyMode = ColoredPrs3d_i::EPublishUnderTimeStamp;
        return true;
      }
      if (VISU::TFIELD == aType){
        thePublishInStudyMode = ColoredPrs3d_i::EPublishIndependently;
        if(theSObject->FindSubObject(2,theSObject))
          return true;
      }
    }
    SUIT_MessageBox::warning(GetDesktop(theModule),
                             QObject::tr("WRN_VISU"),
                             QObject::tr("WRN_NO_AVAILABLE_DATA") );
    return false;
  }

  //------------------------------------------------------------
  VISU::Result_i*
  CheckResult(const SalomeApp_Module* theModule,
              _PTR(SObject)           theSource,
              VISU::Result_var&       theResult)
  {
    if(theSource->Depth() < 3) // Bug of SALOMEDS : can\t get father from root object
      return NULL;

    _PTR(SObject) aSObj = theSource->GetFather();
    if (!aSObj)
      return NULL;

    aSObj = aSObj->GetFather();
    if (!aSObj)
      return NULL;

    aSObj = aSObj->GetFather();
    if (!aSObj)
      return NULL;

    CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObj);
    if (CORBA::is_nil(anObject)) {
      aSObj = aSObj->GetFather();
      if (!aSObj)
        return NULL;
      anObject = VISU::ClientSObjectToObject(aSObj);
    }

    if (CORBA::is_nil(anObject))
      return NULL;

    theResult = VISU::Result::_narrow(anObject);
    return dynamic_cast<VISU::Result_i*>(VISU::GetServant(anObject).in());
  }

  //------------------------------------------------------------
  // VTK View
  VISU_Actor*
  PublishMeshInView(const SalomeApp_Module* theModule,
                    VISU::Prs3d_i* thePrs,
                    SVTK_ViewWindow* theViewWindow)
  {
    VISU_Actor* anActor = NULL;
    if (!thePrs || !theViewWindow)
      return anActor;

    SalomeApp_Application *anApp = dynamic_cast<SalomeApp_Application*>(theModule->application());
    if(!anApp)
      return anActor;
    
    SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(anApp->activeStudy());

    if(!aStudy)
      return anActor;
    

    QApplication::setOverrideCursor( Qt::WaitCursor );
    try {
      if ((anActor = thePrs->CreateActor())) {
        theViewWindow->AddActor(anActor);

	if(anActor->hasIO() && anActor->getIO()->hasEntry())
	  aStudy->setVisibilityState(anActor->getIO()->getEntry(), Qtx::ShownState);
      }
    } catch(std::exception& exc) {
      SUIT_MessageBox::warning
        (GetDesktop(theModule), QObject::tr("WRN_VISU"),
         QObject::tr("ERR_CANT_CREATE_ACTOR") + ": " + QObject::tr(exc.what()) );
    }
    QApplication::restoreOverrideCursor();

    return anActor;
  }

  //------------------------------------------------------------
  void
  RepaintViewWindows (const SalomeApp_Module* theModule,
                      const Handle(SALOME_InteractiveObject)& theIObject)
  {
    TViewWindows aViewWindows;
    if (SalomeApp_Application* anApp = theModule->getApp()) {
      ViewManagerList aViewManagerList;
      anApp->viewManagers(SVTK_Viewer::Type(),aViewManagerList);
      QList<SUIT_ViewManager*>::Iterator anIter = aViewManagerList.begin();
      while ( anIter != aViewManagerList.end() )
      {
        QVector<SUIT_ViewWindow*> aViews = (*anIter)->getViews();
        for (int i = 0, iEnd = aViews.size(); i < iEnd; i++) {
          if (SUIT_ViewWindow* aViewWindow = aViews.at(i)) {
            if (SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(aViewWindow)) {
              if (vw->isVisible(theIObject)) {
                vw->getRenderer()->ResetCameraClippingRange();
                vw->Repaint();
                vw->highlight(theIObject, true, true);
              }
            }
          }
        }
        anIter++;
      }
    }
  }

  //------------------------------------------------------------
  VISU_Actor*
  FindActor(const SalomeApp_Study* theStudy,
            SVTK_ViewWindow* theViewWindow,
            const QString& theEntry)
  {
    TObjectInfo anObjectInfo = GetObjectByEntry(theStudy, theEntry.toLatin1().data());
    VISU::Prs3d_i* aPrs3d = GetPrs3dFromBase(anObjectInfo.myBase);
    return FindActor(theViewWindow, aPrs3d);
  }

  //------------------------------------------------------------
  VISU_ActorBase*
  FindActorBase(const SalomeApp_Study* theStudy,
                SVTK_ViewWindow* theViewWindow,
                const QString& theEntry)
  {
    TObjectInfo anObjectInfo = GetObjectByEntry(theStudy, theEntry.toLatin1().constData());
    VISU::PointMap3d_i* aTable = dynamic_cast<VISU::PointMap3d_i*>(anObjectInfo.myBase);
    return FindActorBase(theViewWindow, aTable);
  }

  //------------------------------------------------------------
  void
  RecreateActor (const SalomeApp_Module* theModule,
                 VISU::Prs3d_i* thePrs)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    /*    SALOMEDS::SObject_var aSObject = thePrs->GetSObject();
          CORBA::String_var anEntry = aSObject->GetID();*/

    try {
      thePrs->UpdateActors();
    } catch (std::runtime_error& exc) {
      INFOS(exc.what());
      QApplication::restoreOverrideCursor();
      SUIT_MessageBox::warning
        (GetDesktop(theModule), QObject::tr("WRN_VISU"),
         QObject::tr("ERR_CANT_BUILD_PRESENTATION") + ": " + QObject::tr(exc.what()) );

      thePrs->RemoveActors();
      return;
    }
    QApplication::restoreOverrideCursor();
  }

  //------------------------------------------------------------
  bool
  ComputeVisiblePropBounds(SVTK_ViewWindow* theViewWindow,
                           double allBounds[6],
                           const char* theActorClassName)
  {
    vtkRenderer *aRen = theViewWindow->getRenderer();
    VTK::ActorCollectionCopy aCopy(aRen->GetActors());
    vtkActorCollection *anActColl = aCopy.GetActors();
    vtkProp *prop;
    double *bounds;
    int somethingVisible = false;

    allBounds[0] = allBounds[2] = allBounds[4] = VTK_LARGE_FLOAT;
    allBounds[1] = allBounds[3] = allBounds[5] = -VTK_LARGE_FLOAT;
    // loop through all props
    for (anActColl->InitTraversal(); (prop = anActColl->GetNextProp()); ) {
      // if it's invisible, or has no geometry, we can skip the rest
      if (prop->GetVisibility() && prop->IsA(theActorClassName)) {
        bounds = prop->GetBounds();
        // make sure we haven't got bogus bounds
        if (bounds != NULL &&
            bounds[0] > -VTK_LARGE_FLOAT && bounds[1] < VTK_LARGE_FLOAT &&
            bounds[2] > -VTK_LARGE_FLOAT && bounds[3] < VTK_LARGE_FLOAT &&
            bounds[4] > -VTK_LARGE_FLOAT && bounds[5] < VTK_LARGE_FLOAT)
        {
          somethingVisible = true;
          if (bounds[0] < allBounds[0]) allBounds[0] = bounds[0];
          if (bounds[1] > allBounds[1]) allBounds[1] = bounds[1];
          if (bounds[2] < allBounds[2]) allBounds[2] = bounds[2];
          if (bounds[3] > allBounds[3]) allBounds[3] = bounds[3];
          if (bounds[4] < allBounds[4]) allBounds[4] = bounds[4];
          if (bounds[5] > allBounds[5]) allBounds[5] = bounds[5];
        }//not bogus
      }
    }
    return somethingVisible;
  }

  //------------------------------------------------------------
  void SetFitAll(SVTK_ViewWindow* theViewWindow)
  {
    static double PRECISION = 0.000001;
    static double DEVIATION = 600;
    double XYZ_Bnd[6];
    if (!ComputeVisiblePropBounds(theViewWindow, XYZ_Bnd)) return;

    double absX = XYZ_Bnd[1] - XYZ_Bnd[0];
    double absY = XYZ_Bnd[3] - XYZ_Bnd[2];
    double absZ = XYZ_Bnd[5] - XYZ_Bnd[4];

    enum CameraOrient {e3D, eFront, eLeft, eTop};
    CameraOrient aCameraOrient = e3D;
    if (absX <= PRECISION) aCameraOrient = eFront;
    else {
      if (absY <= PRECISION) aCameraOrient = eLeft;
      else {
        if (absZ <= PRECISION) aCameraOrient = eTop;
        else {
          // all the three dimensions exceeds precision
          double dev_abs_XY = absX / absY;
          double dev_abs_YZ = absY / absZ;
          double dev_abs_XZ = absX / absZ;
          if (dev_abs_XY >= DEVIATION || 1./dev_abs_YZ >= DEVIATION)
            aCameraOrient = eLeft;
          else {
            if (1./dev_abs_XY >= DEVIATION || 1./dev_abs_XZ >= DEVIATION)
              aCameraOrient = eFront;
            else {
              if (dev_abs_XZ >= DEVIATION || dev_abs_YZ >= DEVIATION)
                aCameraOrient = eTop;
            }
          }
        }
      }
    }

    switch (aCameraOrient) {
    case eFront: theViewWindow->onFrontView(); break;
    case eLeft:  theViewWindow->onLeftView();  break;
    case eTop:   theViewWindow->onTopView();   break;
    case e3D:    theViewWindow->onResetView(); break;
    }
    theViewWindow->getRenderer()->ResetCameraClippingRange();
    theViewWindow->onFitAll();
  }

  //************************************************************
  // Plot2d View

  SPlot2d_Viewer*
  GetPlot2dViewer(const SalomeApp_Module* theModule, const bool theCreate)
  {
    if(SalomeApp_Application* anApp = theModule->getApp()){
      if(SUIT_ViewManager* aViewManager = anApp->getViewManager( Plot2d_Viewer::Type(), theCreate )){
        return dynamic_cast<SPlot2d_Viewer*>(aViewManager->getViewModel());
      }
    }
    return NULL;
  }

  //------------------------------------------------------------
  void
  PlotTable(const SalomeApp_Module* theModule,
            VISU::Table_i* table,
            int theDisplaying)
  {
    SalomeApp_Study* aStudy = GetAppStudy( theModule );
    if( !aStudy )
      return;

    SPlot2d_Viewer* aView = GetPlot2dViewer( theModule, true ); // create if necessary
    if ( !aView )
      return;
    Plot2d_ViewFrame* aPlot = aView->getActiveViewFrame();
    if ( !aPlot )
      return;

    // implementation moved to VISU_I package (see VISU_Tools.h)
    VISU::PlotTable( aStudy, aPlot, table, theDisplaying );
  }

  //------------------------------------------------------------
  void
  PlotCurve(const SalomeApp_Module* theModule,
            VISU::Curve_i* theCurve,
            int theDisplaying)
  {
    SPlot2d_Viewer* aView = GetPlot2dViewer( theModule, true );
    if ( !aView )
      return;
    Plot2d_ViewFrame* aPlot = aView->getActiveViewFrame();
    if ( !aPlot )
      return;

    // implementation moved to VISU_I package (see VISU_Tools.h)
    VISU::PlotCurve( aPlot, theCurve, theDisplaying );
  }

  //------------------------------------------------------------
  void
  PlotRemoveCurve(const SalomeApp_Module* theModule,
                  VISU::Curve_i* pCrv)
  {
    // implementation moved to VISU_I package (see VISU_Tools.h)
    VISU::PlotRemoveCurve( theModule->getApp(), pCrv );
  }

  //------------------------------------------------------------
  void
  PlotContainer(const SalomeApp_Module* theModule,
                VISU::Container_i* container,
                int theDisplaying)
  {
    SPlot2d_Viewer* aView = GetPlot2dViewer( theModule, true );
    if ( !aView )
      return;
    Plot2d_ViewFrame* aPlot = aView->getActiveViewFrame();
    if ( !aPlot )
      return;

    // implementation moved to VISU_I package (see VISU_Tools.h)
    VISU::PlotContainer( aPlot, container, theDisplaying );
  }

  //------------------------------------------------------------
  void
  CreatePlot(SalomeApp_Module* theModule,
             _PTR(SObject) theTableSO)
  {
    SPlot2d_Viewer* aView = GetPlot2dViewer( theModule, true );
    if ( !aView )
      return;
    Plot2d_ViewFrame* aPlot = aView->getActiveViewFrame();
    if ( !aPlot )
      return;

    VISU::VISU_Gen_i* aVisuGen = VISU::GetVisuGen( theModule );

    // implementation moved to VISU_I package (see VISU_Tools.h)
    VISU::CreatePlot( aVisuGen, aPlot, theTableSO );
  }

  //************************************************************
  // Others
  VISU::Mesh_i*
  CreateMesh(VisuGUI* theModule,
             const Handle(SALOME_InteractiveObject)& theIO,
             SVTK_ViewWindow* theViewWindow)
  {
        _PTR(Study) aStudy = GetCStudy(GetAppStudy(theModule));
    //if (CheckLock(aStudy))
    //  return;

    _PTR(SObject) aResultSObj = aStudy->FindObjectID(theIO->getEntry());

    // Get VISU::Result
    VISU::Result_var aResult;
    VISU::Result_i* pResult = CheckResult(theModule, aResultSObj, aResult);
    if (pResult == NULL)
      return NULL;

    Storable::TRestoringMap aMap = Storable::GetStorableMap(aResultSObj);
    if (aMap.empty())
      return NULL;

    CORBA::Object_var aMesh;
    string aComment = Storable::FindValue(aMap,"myComment").toLatin1().data();
    string aMeshName = Storable::FindValue(aMap,"myMeshName").toLatin1().data();
#ifdef CHECKTIME
    Utils_Timer timer;
    timer.Start();
#endif
    if (aComment == "ENTITY") {
      VISU::Entity anEntity = (VISU::Entity)Storable::FindValue(aMap,"myId").toInt();
      if (VISU::Mesh_i::IsPossible(pResult,aMeshName.c_str(),anEntity))
        aMesh = GetVisuGen(theModule)->MeshOnEntity(aResult,aMeshName.c_str(),anEntity);
    } else if (aComment == "FAMILY") {
      VISU::Entity anEntity = (VISU::Entity)Storable::FindValue(aMap,"myEntityId").toInt();
      string aFamilyName = Storable::FindValue(aMap,"myName").toLatin1().data();
      if (VISU::Mesh_i::IsPossible(pResult,aMeshName.c_str(),anEntity,aFamilyName.c_str()))
        aMesh = GetVisuGen(theModule)->FamilyMeshOnEntity
          (aResult,aMeshName.c_str(),anEntity,aFamilyName.c_str());
    } else if (aComment == "GROUP") {
      string aGroupName = Storable::FindValue(aMap,"myName").toLatin1().data();
      if (VISU::Mesh_i::IsPossible(pResult,aMeshName.c_str(),aGroupName.c_str()))
        aMesh = GetVisuGen(theModule)->GroupMesh(aResult,aMeshName.c_str(),aGroupName.c_str());
    }
#ifdef CHECKTIME
    timer.Stop();
    MESSAGE("VisuGUI::CreateMesh() - CREATE MESH");
    timer.Show();
#endif

    //QApplication::restoreOverrideCursor();
    VISU::Mesh_i* pPresent = NULL;
    if (!CORBA::is_nil(aMesh))
      pPresent = dynamic_cast<VISU::Mesh_i*>(VISU::GetServant(aMesh).in());
    if (pPresent == NULL) {
      SUIT_MessageBox::warning (GetDesktop(theModule),
                                QObject::tr("WRN_VISU"),
                                QObject::tr("ERR_CANT_BUILD_PRESENTATION") );
      return NULL;
    }

    if (theViewWindow) {
      try {
#ifdef CHECKTIME
        Utils_Timer timer;
        timer.Start();
#endif
        if(GetResourceMgr()->booleanValue("VISU","display_only",false)){
          const VisuGUI* av = dynamic_cast<const VisuGUI*>(theModule);
          if(av)(const_cast<VisuGUI*>(av))->OnEraseAll();
        }
        PublishMeshInView(theModule, pPresent, theViewWindow);
        if(GetResourceMgr()->booleanValue("VISU","automatic_fit_all",false)){
          SetFitAll(theViewWindow);
        }
#ifdef CHECKTIME
        timer.Stop();
        MESSAGE("VisuGUI::CreateMesh() - DISPLAY MESH");
        timer.Show();
#endif
      } catch (std::runtime_error& exc) {
        INFOS(exc.what());
        SUIT_MessageBox::warning(GetDesktop(theModule), QObject::tr("WRN_VISU"),
                                 QObject::tr("ERR_CANT_CREATE_ACTOR") + ": " + QObject::tr(exc.what()) );
      }
    }

    UpdateObjBrowser(theModule, false, aResultSObj);

    theModule->application()->putInfo(QObject::tr("INF_DONE"));
    // Make "Save" button active
    theModule->getApp()->updateActions();
    return pPresent;
  }

  // ========================================================================================
  // GetPrs3dList: find list of presentations for the given object
  // ========================================================================================
  std::vector<VISU::Prs3d_i*> GetPrs3dList (const SalomeApp_Module* theModule,
                                            const Handle(SALOME_InteractiveObject)& theIO,
                                            bool theGP)
  {
    std::vector<VISU::Prs3d_i*> aList;
    if (!theIO.IsNull() && theIO->hasEntry()) {
      _PTR(Study) aCStudy = GetCStudy(GetAppStudy(theModule));
      _PTR(SObject) aSObject = aCStudy->FindObjectID(theIO->getEntry());
      aList = GetPrs3dList(theModule, aSObject, theGP);
    }
    return aList;
  }

  std::vector<VISU::Prs3d_i*> GetPrs3dList (const SalomeApp_Module* theModule,
                                            _PTR(SObject) theObject,
                                            bool theGP)
  {
    std::vector<VISU::Prs3d_i*> aList; int k = 0;
    if (!theObject)
      return aList;

    _PTR(Study) aCStudy = GetCStudy(GetAppStudy(theModule));

    CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(theObject);
    if (!CORBA::is_nil(aCORBAObject)) {
      VISU::Base_var aVisuObj = VISU::Base::_narrow(aCORBAObject);
      if (!CORBA::is_nil(aVisuObj)) {
        VISU::VISUType aType = aVisuObj->GetType();
        switch (aType) {
        case VISU::TGAUSSPOINTS:
          if ( !theGP ) break;
        case VISU::TSCALARMAP:
        case VISU::TISOSURFACES:
        case VISU::TDEFORMEDSHAPE:
        case VISU::TCUTPLANES:
        case VISU::TCUTLINES:
        case VISU::TCUTSEGMENT:
        case VISU::TVECTORS:
        case VISU::TSTREAMLINES:
        case VISU::TPLOT3D:
        case VISU::TSCALARMAPONDEFORMEDSHAPE:
        case VISU::TDEFORMEDSHAPEANDSCALARMAP:
        case VISU::TMESH:
          {
            PortableServer::ServantBase_var aServant = VISU::GetServant(aCORBAObject);
            if (aServant.in()) {
              VISU::Prs3d_i* aPrsObject = dynamic_cast<VISU::Prs3d_i*>(aServant.in());
              aList.resize(k+1);
              aList[k] = aPrsObject;
              k++;
            }
          }
          break;
        case VISU::TFIELD:
          {
            _PTR(ChildIterator) anIter = aCStudy->NewChildIterator(theObject);
            _PTR(SObject) aTimeStamp;
            anIter->Next(); // First is reference on support
            for (; anIter->More(); anIter->Next()) {
              aTimeStamp = anIter->Value();
              if (!aTimeStamp) continue;
              std::vector<VISU::Prs3d_i*> aSubList = GetPrs3dList(theModule, aTimeStamp);
              if (!aSubList.empty()) {
                int n = aSubList.size();
                aList.resize(k+n);
                for (int i = 0; i < n; i++) {
                  aList[k] = aSubList[i];
                  k++;
                }
              }
            }
          }
          break;
        case VISU::TCOLOREDPRS3DHOLDER:
          {
            PortableServer::ServantBase_var aServant = VISU::GetServant(aCORBAObject);
            if (aServant.in()) {
              VISU::ColoredPrs3dHolder_i* aPrsHolderObject = dynamic_cast<VISU::ColoredPrs3dHolder_i*>(aServant.in());
              if( aPrsHolderObject ) {
                VISU::Prs3d_i* aPrsObject = aPrsHolderObject->GetPrs3dDevice();
                aList.resize(k+1);
                aList[k] = aPrsObject;
                k++;
              }
            }
          }
        }
      }
    } else {
      VISU::VISUType aType = VISU::Storable::SObject2Type(theObject);
      switch (aType) {
      case VISU::TFIELD: {
        _PTR(ChildIterator) anIter = aCStudy->NewChildIterator(theObject);
        _PTR(SObject) aTimeStamp;
        anIter->Next(); // First is reference on support
        for (; anIter->More(); anIter->Next()) {
          aTimeStamp = anIter->Value();
          if (!aTimeStamp) continue;
          std::vector<VISU::Prs3d_i*> aSubList = GetPrs3dList(theModule, aTimeStamp);
          if (!aSubList.empty()) {
            int n = aSubList.size();
            aList.resize(k+n);
            for (int i = 0; i < n; i++) {
              aList[k] = aSubList[i];
              k++;
            }
          }
        }
        break;
      }
      case VISU::TTIMESTAMP: {
        _PTR(ChildIterator) anIter = aCStudy->NewChildIterator(theObject);
        _PTR(SObject) aPrs;
        for (; anIter->More(); anIter->Next()) {
          aPrs = anIter->Value();
          if (!aPrs) continue;
          std::vector<VISU::Prs3d_i*> aSubList = GetPrs3dList(theModule, aPrs);
          if (!aSubList.empty()) {
            int n = aSubList.size();
            aList.resize(k+n);
            for (int i = 0; i < n; i++) {
              aList[k] = aSubList[i];
              k++;
            }
          }
        }
        break;
      }}
    }
    return aList;
  }

  //------------------------------------------------------------
  int GetFreePositionOfDefaultScalarBar(VisuGUI* theModule, SVTK_ViewWindow* theViewWindow)
  {
    int minIndx = 1;
    std::set<int> aIndexes;
    TViewToPrs3d aMap = theModule->getScalarBarsMap();
    TViewToPrs3d::const_iterator aViewToPrsIter = aMap.find(theViewWindow);
    if (aViewToPrsIter != aMap.end()) {
      TSetPrs3d::const_iterator aPrsIter = (aViewToPrsIter->second).begin();
      for (; aPrsIter != (aViewToPrsIter->second).end(); aPrsIter++) {
        aIndexes.insert((*aPrsIter).second);
      }
    }
    std::set<int>::const_iterator aIter = aIndexes.begin();
    for (int i = 1,length = aIndexes.size(); i <= length; i++) {
      std::set<int>::const_iterator aIter = aIndexes.find(i);
      if (aIter == aIndexes.end()) { minIndx = i; break; }
      else minIndx = i + 1;
    }
    return minIndx;
  }

  //------------------------------------------------------------
  void AddScalarBarPosition (VisuGUI* theModule, SVTK_ViewWindow* theViewWindow,
                             VISU::Prs3d_i* thePrs3d, int pos)
  {
    TViewToPrs3d& aMap = theModule->getScalarBarsMap();
    TPrs3dToInd aPair; aPair.first = thePrs3d; aPair.second = pos;
    aMap[theViewWindow].insert(aPair);
  }

  //------------------------------------------------------------
  void RemoveScalarBarPosition(VisuGUI* theModule, SVTK_ViewWindow* theViewWindow,
                               VISU::Prs3d_i* thePrs3d)
  {
    TViewToPrs3d& aMap = theModule->getScalarBarsMap();
    TSetPrs3d::iterator aIter = aMap[theViewWindow].begin();
    for (; aIter != aMap[theViewWindow].end(); aIter++)
      if ((*aIter).first == thePrs3d) {
        aMap[theViewWindow].erase(*aIter);
        return;
      }
  }
  //------------------------------------------------------------
  bool getClippingPlanesFolder(_PTR(Study) theStudy, _PTR(SObject)& theSObject)
  {
    _PTR(SComponent) aVisuSO = theStudy->FindComponent("VISU");
    if (!aVisuSO) return false;
    _PTR(SObject) aFolder = theStudy->FindObject(CLIP_PLANES_FOLDER);
    if (!aFolder) {
      _PTR(StudyBuilder) aBuilder = theStudy->NewBuilder();
      aFolder = aBuilder->NewObject(aVisuSO);

      _PTR(GenericAttribute) anAttr;
      anAttr = aBuilder->FindOrCreateAttribute(aFolder,"AttributeName");
      _PTR(AttributeName) aName(anAttr);
      aName->SetValue(CLIP_PLANES_FOLDER);
    }
    theSObject = aFolder;
    return true;
  }

  //------------------------------------------------------------
  void initSpinBox( SalomeApp_IntSpinBox* sb,
                    const int bottom,
                    const int top,
                    const int step )
  {
    sb->setAcceptNames( false );
    sb->setRange( bottom, top );
    sb->setSingleStep( step );
  }

  //------------------------------------------------------------
  void initSpinBox( SalomeApp_DoubleSpinBox* sb,
                    const double& bottom,
                    const double& top,
                    const double& step,
                    const char* quantity )
  {
    // Obtain precision from preferences
    SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();
    int precision = resMgr->integerValue( "VISU", quantity, 3 );

    sb->setPrecision   ( precision );
    sb->setDecimals    ( qAbs(precision) );
    sb->setRange       ( bottom, top );
    sb->setSingleStep  ( step );
    sb->setDefaultValue( bottom );
    sb->setAcceptNames ( false );

    // Add a hint for the user saying how to tune precision
    QString userPropName = QObject::tr( QString( "VISU_PREF_%1" ).arg( quantity ).toLatin1().constData() );
    sb->setProperty( "validity_tune_hint",
                     QVariant( QObject::tr( "VISU_PRECISION_HINT" ).arg( userPropName ) ) );
  }
}

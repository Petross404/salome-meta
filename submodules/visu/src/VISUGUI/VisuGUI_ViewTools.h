// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Tools.h
//  Author : Sergey Anikin
//  Module : VISU
//
#ifndef VisuGUI_ViewTools_HeaderFile
#define VisuGUI_ViewTools_HeaderFile

#include "VisuGUI.h"

#include "VisuGUI_Tools.h"

#include "VISU_Prs3d_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_PointMap3d_i.hh"
#include "VISU_ViewManager_i.hh"

#include "VISU_Actor.h"

#include "SalomeApp_Module.h"
#include "SalomeApp_Application.h"

//#include "VVTK_ViewModel.h"
//#include "VVTK_ViewWindow.h"

#include "SVTK_Functor.h"
#include "SVTK_ViewModel.h"
#include "SVTK_ViewWindow.h"

#include "SPlot2d_ViewModel.h"

#include "VTKViewer_Algorithm.h"

#include "SUIT_Session.h"
#include "SUIT_Desktop.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ViewManager.h"
#include "SUIT_ViewWindow.h"

#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"

#include <vtkActorCollection.h>
#include <vtkRenderer.h>

//class VVTK_Viewer;

namespace VISU 
{
  /*! Return active view window, if it instantiates TViewer::TViewWindow class,
   *  overwise find or create corresponding view window, make it active and return it.
   *  \note Active VVTK_ViewWindow can be returned by request GetViewWindow<SVTK_Viewer>(),
   *        because VVTK_ViewWindow inherits SVTK_ViewWindow.
   */
  template<class TViewer>
  inline
  typename TViewer::TViewWindow*
  GetViewWindow(VisuGUI* theModule)
  {
    typedef typename TViewer::TViewWindow TViewWindow;
    if (SalomeApp_Application* anApp = theModule->getApp()) {
      if (TViewWindow* aView = dynamic_cast<TViewWindow*>(anApp->desktop()->activeWindow()))
        return aView;
    }
    SUIT_ViewManager* aViewManager =
      theModule->getViewManager(TViewer::Type(), /*create = */true);
    if (aViewManager) {
      if (SUIT_ViewWindow* aViewWindow = aViewManager->getActiveView()) {
        if (TViewWindow* aView = dynamic_cast<TViewWindow*>(aViewWindow)) {
          aViewWindow->raise();
          aViewWindow->setFocus();
          return aView;
        }
      }
    }
    return NULL;
  }

  /*! Return active view window, if it instantiates TViewWindow class, overwise return NULL.
   *  \note Active VVTK_ViewWindow can be returned by request GetActiveViewWindow<SVTK_ViewWindow>(),
   *        because VVTK_ViewWindow inherits SVTK_ViewWindow.
   */
  template<class TViewWindow>
  inline
  TViewWindow*
  GetActiveViewWindow(const SalomeApp_Module* theModule = NULL)
  {
    SalomeApp_Application* anApp = NULL;
    if (theModule)
      anApp = theModule->getApp();
    else
      anApp = dynamic_cast<SalomeApp_Application*>
        (SUIT_Session::session()->activeApplication());

    if (anApp)
      if (SUIT_ViewManager* aViewManager = anApp->activeViewManager())
        if (SUIT_ViewWindow* aViewWindow = aViewManager->getActiveView())
          return dynamic_cast<TViewWindow*>(aViewWindow);

    return NULL;
  }


  //---------------------------------------------------------------
  VISU_Actor*
  PublishInView(VisuGUI* theModule,
                Prs3d_i* thePrs,
                SVTK_ViewWindow* theViewWindow,
                const bool theIsHighlight = false);  


  //---------------------------------------------------------------
  VISU_Actor*
  UpdateViewer(VisuGUI* theModule,
               VISU::Prs3d_i* thePrs,
               bool theDispOnly = false,
               const bool theIsHighlight = false);


  //---------------------------------------------------------------
  template<class TViewer>
  inline
  void
  OnEraseAll(VisuGUI* theModule)
  {
    typedef typename TViewer::TViewWindow TViewWindow;
    if (TViewWindow* aViewWindow = GetActiveViewWindow<TViewWindow>(theModule)) {
      aViewWindow->unHighlightAll();
      if (vtkRenderer *aRen = aViewWindow->getRenderer()) {
        VTK::ActorCollectionCopy aCopy(aRen->GetActors());
        vtkActorCollection *aCollection = aCopy.GetActors();
        aCollection->InitTraversal();
        while (vtkActor *anAct = aCollection->GetNextActor()) {
          if (anAct->GetVisibility() > 0)
            if (SALOME_Actor* anActor = dynamic_cast<SALOME_Actor*>(anAct)) {
              anActor->VisibilityOff();
            }
        }
        aViewWindow->Repaint();
      }
    }
  }

  template<>
  inline
  void
  OnEraseAll<SPlot2d_Viewer>(VisuGUI* theModule)
  {
    if (SPlot2d_Viewer* aPlot2d = GetPlot2dViewer(theModule, false))
      aPlot2d->EraseAll();
  }


  //---------------------------------------------------------------
  void
  ErasePrs3d(VisuGUI* theModule,
             VISU::Prs3d_i* thePrs,
             bool theDoRepaint = true);

  void
  ErasePrs(VisuGUI* theModule,
           Base_i* theBase, 
           bool theDoRepaintVW);
}

#endif

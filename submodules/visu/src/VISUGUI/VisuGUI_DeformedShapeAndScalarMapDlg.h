// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_DeformedShapeAndScalarMapDlg.h
//  Author : Eugeny Nikolaev
//  Module : VISU
//
#ifndef VISUGUI_DEFORMEDSHAPEANDSCALARMAPDLS_H
#define VISUGUI_DEFORMEDSHAPEANDSCALARMAPDLS_H

#include "VisuGUI_Prs3dDlg.h"

#include "VISUConfig.hh"

#include <QDialog>

#include <set>
#include <vector>

class SalomeApp_Module;
class VisuGUI_InputPane;
class SalomeApp_DoubleSpinBox;
class QComboBox;
class QTabWidget;

namespace VISU
{
  class DeformedShapeAndScalarMap_i;
}

class VisuGUI_DeformedShapeAndScalarMapDlg : public VisuGUI_ScalarBarBaseDlg
{
    Q_OBJECT

public:
    VisuGUI_DeformedShapeAndScalarMapDlg (SalomeApp_Module* theModule);
    ~VisuGUI_DeformedShapeAndScalarMapDlg();

    double getFactor() const;
    void setFactor(double theFactor);
    
    virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                    bool theInit );

    virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

    int getCurrentScalarFieldNamePos();
    QString getCurrentScalarFieldName();
    int getCurrentScalarNbIterations();
    VISU::Entity getCurrentScalarEntity();
    void SetScalarField(int theIter,QString theFieldName=QString(""), const bool = true );

    bool IsScalarFieldValid() const { return myIsScalarFieldValid; }

protected:
  virtual QString GetContextHelpFilePath();

protected slots:
  void accept();
  void reject();

private slots:
  void onFieldChanged(int i=0);
  void onTimeStampChanged(int i=0);

private:
 SalomeApp_DoubleSpinBox* ScalFact;
 QTabWidget* myTabBox;
 VisuGUI_ScalarBarPane* myScalarPane;
 VisuGUI_InputPane*     myInputPane;
 QComboBox *myFieldsCombo;
 QComboBox *myTimeStampsCombo;
 QPushButton* myButtonOk;
 
 typedef std::map<int, QString> TTimeStampNumber2Time; // Times map definition (iteration time, real value of time)
 typedef std::map<QString, TTimeStampNumber2Time> TFieldName2TimeStamps; // Field name and enity to Times
 typedef std::map<VISU::Entity, TFieldName2TimeStamps> TEntity2Fields; // Mesh to fields map
 
 TEntity2Fields myEntity2Fields;
 int myCurrScalarIter;
 bool myIsAnimation;
 bool myUpdateScalars;
 std::vector<int> myTimeStampID;
 
 _PTR(SObject) mySelectionObj;
 SALOME::GenericObjPtr<VISU::DeformedShapeAndScalarMap_i> myPrsCopy;
 SalomeApp_Module* myVisuGUI;

 bool myIsScalarFieldValid;
 
protected:
 void UpdateScalarField();
 void SetScalarField( const bool = true );
 void AddAllFieldNames();
 void AddAllTimes(const QString& theFieldName);
 QString GetFloatValueOfTimeStamp(VISU::Entity theEntity,
                                  const std::string& theFieldName,
                                  int theTimeStampNumber);
 void updateControls();
};

#endif // VISUGUI_DEFORMEDSHAPEDLS_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_BuildProgressDlg.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_BuildProgressDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"

#include "SUIT_FileDlg.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include "LightApp_Application.h"

#include <vtkTimerLog.h>

#include <QCheckBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLCDNumber>
#include <QLineEdit>
#include <QPushButton>
#include <QTimer>
#include <QKeyEvent>

/*!
 * Constructor
 */
VisuGUI_BuildProgressDlg::VisuGUI_BuildProgressDlg( QWidget* theParent ):
  QDialog( theParent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint),
  myIsRaiseColor( false )
{
  setAttribute( Qt::WA_DeleteOnClose );
  setWindowTitle( tr( "DLG_BUILD_PROGRESS_TITLE" ) );

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  QVBoxLayout* aTopLayout = new QVBoxLayout( this );
  aTopLayout->setSpacing( 6 );
  aTopLayout->setMargin( 6 );
  //aTopLayout->setAutoAdd( true );

  // Settings
  mySettingsBox = new QGroupBox( tr( "IMPORT_SETTINGS" ), this );
  aTopLayout->addWidget( mySettingsBox );
  //mySettingsBox->setColumnLayout( 0, Qt::Vertical );
  //mySettingsBox->layout()->setSpacing( 0 );
  //mySettingsBox->layout()->setMargin( 0 );

  QGridLayout* aSettingsLayout = new QGridLayout( mySettingsBox );
  aSettingsLayout->setSpacing( 6 );
  aSettingsLayout->setMargin( 11 );

  QLabel* aFileNameLabel = new QLabel( tr( "FILE_NAME" ), mySettingsBox );
  myFileNameLineEdit = new QLineEdit( mySettingsBox );
  myFileNameLineEdit->setMinimumWidth( 250 );
  myFileNameLineEdit->setReadOnly( true );
  /*
  QPushButton* aFileNameButton = new QPushButton( mySettingsBox );
  aFileNameButton->setAutoDefault( false );
  aFileNameButton->setPixmap( aResourceMgr->loadPixmap( "VISU", tr( "ICON_LOAD_TEXTURE" ) ) );
  connect( aFileNameButton, SIGNAL( clicked() ), this, SLOT( onBrowseFile() ) );
  */
  myBuildAllCheckBox = new QCheckBox( tr( "BUILD_ALL" ), mySettingsBox );
  myBuildAllCheckBox->setChecked( aResourceMgr->booleanValue( "VISU", "full_med_loading", false ) );
  connect( myBuildAllCheckBox, SIGNAL( clicked() ), this, SLOT( onBuildCheckBoxClicked() ) );

  myBuildAtOnceCheckBox = new QCheckBox( tr( "BUILD_AT_ONCE" ), mySettingsBox );
  myBuildAtOnceCheckBox->setChecked( aResourceMgr->booleanValue( "VISU", "build_at_once", false ) );
  connect( myBuildAtOnceCheckBox, SIGNAL( clicked() ), this, SLOT( onBuildCheckBoxClicked() ) );

  myCloseCheckBox = new QCheckBox( tr( "CLOSE_AT_FINISH" ), mySettingsBox );
  myCloseCheckBox->setChecked( aResourceMgr->booleanValue( "VISU", "close_at_finish", true ) );

  aSettingsLayout->addWidget(  aFileNameLabel,       0, 0 );
  aSettingsLayout->addWidget( myFileNameLineEdit,    1, 0, 1, 2 );
  //aSettingsLayout->addWidget(  aFileNameButton,      1, 2 );
  aSettingsLayout->addWidget( myBuildAllCheckBox,    2, 0 );
  aSettingsLayout->addWidget( myBuildAtOnceCheckBox, 3, 0 );
  aSettingsLayout->addWidget( myCloseCheckBox,       4, 0 );

  // Progress
  myProgressBox = new QGroupBox( tr( "IMPORT_PROGRESS" ), this );
  aTopLayout->addWidget( myProgressBox );
  //myProgressBox->setColumnLayout( 0, Qt::Vertical );
  //myProgressBox->layout()->setSpacing( 0 );
  //myProgressBox->layout()->setMargin( 0 );

  QGridLayout* aProgressLayout = new QGridLayout( myProgressBox );
  aProgressLayout->setSpacing( 6 );
  aProgressLayout->setMargin( 11 );

  QLabel* aBuildEntitiesLabel = new QLabel( tr( "BUILD_ENTITIES" ), myProgressBox );
  myBuildEntitiesButton = new QPushButton( myProgressBox );
  myBuildEntitiesButton->setEnabled( false );
  myBuildEntitiesButton->setFixedSize( 30, 30 );
  QPalette aPal = myBuildEntitiesButton->palette();
  aPal.setColor( myBuildEntitiesButton->backgroundRole(), Qt::red );
  myBuildEntitiesButton->setPalette( aPal );

  myBuildFieldsCheckBox = new QCheckBox( tr( "BUILD_FIELDS" ), myProgressBox );
  myBuildFieldsCheckBox->setChecked( aResourceMgr->booleanValue( "VISU", "build_fields", true ) );
  myBuildFieldsButton = new QPushButton( myProgressBox );
  myBuildFieldsButton->setEnabled( false );
  myBuildFieldsButton->setFixedSize( 30, 30 );
  aPal = myBuildFieldsButton->palette();
  aPal.setColor( myBuildFieldsButton->backgroundRole(), myBuildFieldsCheckBox->isChecked() ? Qt::red : Qt::gray );
  myBuildFieldsButton->setPalette( aPal );

  connect( myBuildFieldsCheckBox, SIGNAL( clicked() ), this, SLOT( onBuildCheckBoxClicked() ) );

  myBuildMinMaxCheckBox = new QCheckBox( tr( "BUILD_MINMAX" ), myProgressBox );
  myBuildMinMaxCheckBox->setChecked( aResourceMgr->booleanValue( "VISU", "build_min_max", true ) );
  myBuildMinMaxButton = new QPushButton( myProgressBox );
  myBuildMinMaxButton->setEnabled( false );
  myBuildMinMaxButton->setFixedSize( 30, 30 );
  aPal = myBuildMinMaxButton->palette();
  aPal.setColor( myBuildMinMaxButton->backgroundRole(), myBuildMinMaxCheckBox->isChecked() ? Qt::red : Qt::gray );
  myBuildMinMaxButton->setPalette( aPal );
  
  connect( myBuildMinMaxCheckBox, SIGNAL( clicked() ), this, SLOT( onBuildCheckBoxClicked() ) );

  myBuildGroupsCheckBox = new QCheckBox( tr( "BUILD_GROUPS" ), myProgressBox );
  myBuildGroupsCheckBox->setChecked( aResourceMgr->booleanValue( "VISU", "build_groups", true ) );
  myBuildGroupsButton = new QPushButton( myProgressBox );
  myBuildGroupsButton->setEnabled( false );
  myBuildGroupsButton->setFixedSize( 30, 30 );
  aPal = myBuildGroupsButton->palette();
  aPal.setColor( myBuildGroupsButton->backgroundRole(), myBuildGroupsCheckBox->isChecked() ? Qt::red : Qt::gray );
  myBuildGroupsButton->setPalette( aPal );

  connect( myBuildGroupsCheckBox, SIGNAL( clicked() ), this, SLOT( onBuildCheckBoxClicked() ) );

  aProgressLayout->addWidget( aBuildEntitiesLabel, 0, 0 );
  aProgressLayout->addWidget( myBuildEntitiesButton,   0, 1 );
  aProgressLayout->addWidget( myBuildFieldsCheckBox,   1, 0 );
  aProgressLayout->addWidget( myBuildFieldsButton,     1, 1 );
  aProgressLayout->addWidget( myBuildMinMaxCheckBox,   2, 0 );
  aProgressLayout->addWidget( myBuildMinMaxButton,     2, 1 );
  aProgressLayout->addWidget( myBuildGroupsCheckBox,   3, 0 );
  aProgressLayout->addWidget( myBuildGroupsButton,     3, 1 );

  // Time
  myTime = QTime( 0, 0, 0, 0 );

  myTimeBox = new QGroupBox( tr( "IMPORT_TIME" ), this );
  aTopLayout->addWidget( myTimeBox );
  //myTimeBox->setColumnLayout( 0, Qt::Vertical );
  //myTimeBox->layout()->setSpacing( 0 );
  //myTimeBox->layout()->setMargin( 0 );

  QGridLayout* aTimeLayout = new QGridLayout( myTimeBox );
  aTimeLayout->setSpacing( 6 );
  aTimeLayout->setMargin( 11 );

  QLabel* aTimeLabel = new QLabel( tr( "TIME" ), myTimeBox );

  myTimeLCDNumber = new QLCDNumber( myTimeBox );
  myTimeLCDNumber->setSegmentStyle( QLCDNumber::Filled );
  myTimeLCDNumber->setStyleSheet( "color: white; background-color: black" );
  //QPalette aPal = myTimeLCDNumber->palette();
  //aPal.setColor( myTimeLCDNumber->backgroundRole(), Qt::black );
  //aPal.setColor( myTimeLCDNumber->foregroundRole(), Qt::white );
  //myTimeLCDNumber->setPalette( aPal );
    
  myTimeLCDNumber->setNumDigits( 8 );
  myTimeLCDNumber->display( myTime.toString( "hh:mm:ss.zzz" ) );

  aTimeLayout->addWidget(  aTimeLabel,     0, 0 );
  aTimeLayout->addWidget( myTimeLCDNumber, 0, 1 );

  // Start / Close
  QGroupBox* CommonGroup = new QGroupBox( this );
  aTopLayout->addWidget( CommonGroup );
  //CommonGroup->setColumnLayout(0, Qt::Vertical );
  //CommonGroup->layout()->setSpacing( 0 );
  //CommonGroup->layout()->setMargin( 0 );
  QGridLayout* CommonGroupLayout = new QGridLayout( CommonGroup );
  CommonGroupLayout->setAlignment( Qt::AlignTop );
  CommonGroupLayout->setSpacing( 6 );
  CommonGroupLayout->setMargin( 11 );

  myStartButton = new QPushButton( tr( "START" ), CommonGroup );
  myStartButton->setAutoDefault( true );
  myStartButton->setDefault( true );
  CommonGroupLayout->addWidget( myStartButton, 0, 0 );
  CommonGroupLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* aCloseButton = new QPushButton( tr( "CLOSE" ), CommonGroup );
  aCloseButton->setAutoDefault( true );
  CommonGroupLayout->addWidget( aCloseButton, 0, 2 );

  QPushButton* aHelpButton = new QPushButton( tr( "HELP" ), CommonGroup );
  aHelpButton->setAutoDefault( true );
  CommonGroupLayout->addWidget( aHelpButton, 0, 3 );

  connect( myStartButton, SIGNAL( clicked() ), this, SLOT( onStart() ) );
  connect(  aCloseButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );
  connect(  aHelpButton,  SIGNAL( clicked() ), this, SLOT( onHelp() ) );

  myTimer = new QTimer( this );
  connect( myTimer, SIGNAL( timeout() ), this, SLOT( onTimer() ) );
}

VisuGUI_BuildProgressDlg::~VisuGUI_BuildProgressDlg()
{
}

void VisuGUI_BuildProgressDlg::show()
{
  if( !myFileName.isNull() || onBrowseFile() )
    QWidget::show();
}

void VisuGUI_BuildProgressDlg::onStart()
{
  if( myFileName.isNull() )
    return;

  myResult = myGenerator->CreateResult( (const char*)myFileName.toLatin1() );

  if (CORBA::is_nil(myResult.in())) {
    SUIT_MessageBox::warning(this,
                             tr("WRN_VISU"),
                             tr("ERR_ERROR_IN_THE_FILE"),
                             tr("BUT_OK"));
    onClose();
  }else{
    myResult->SetBuildFields( myBuildFieldsCheckBox->isChecked(), myBuildMinMaxCheckBox->isChecked() );
    myResult->SetBuildGroups( myBuildGroupsCheckBox->isChecked() );
    
    //setModal( false );
    myFileNameLineEdit->setReadOnly( true );
    myStartButton->setEnabled( false );
    
    bool aBuildAtOnce = myBuildAtOnceCheckBox->isChecked();
    if( aBuildAtOnce )
    {
      QApplication::setOverrideCursor( Qt::WaitCursor );
      myCurrentTime = vtkTimerLog::GetUniversalTime();
    }

    myTime.setHMS( 0, 0, 0 );
    myTimer->start( 100 );
    
    bool aBuildAll = myBuildAllCheckBox->isChecked();
    myResult->Build( aBuildAll, aBuildAtOnce );
  }
}

void VisuGUI_BuildProgressDlg::onClose()
{
  done( 0 );
}

void VisuGUI_BuildProgressDlg::onHelp()
{
  QString aHelpFileName = "importing_med_objects_page.html"; // ?
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app) {
    VisuGUI* aVisuGUI = dynamic_cast<VisuGUI*>( app->activeModule() );
    app->onHelpContextModule(aVisuGUI ? app->moduleName(aVisuGUI->moduleName()) : QString(""), aHelpFileName);
  }
  else {
                QString platform;
#ifdef WIN32
                platform = "winapplication";
#else
                platform = "application";
#endif
    SUIT_MessageBox::warning(0, ("WRN_WARNING"),
                             tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             tr("BUT_OK"));
  }
}

bool VisuGUI_BuildProgressDlg::onBrowseFile()
{
  QString aRootDir = QString( getenv( "VISU_ROOT_DIR") );

  QStringList aFilter;
  aFilter.append( tr( "FLT_MED_FILES" ) );
  aFilter.append( tr( "FLT_ALL_FILES" ) );

  QFileInfo aFileInfo = SUIT_FileDlg::getFileName( this, "", aFilter, tr( "IMPORT_FROM_FILE" ), true );
  QString aFileName = aFileInfo.filePath();

  if( aFileName.isNull() )
    return false;

  myFileName = aFileName;
  myFileNameLineEdit->setText( aFileName.section( '/', -1 ) );

  return true;
}

void VisuGUI_BuildProgressDlg::onTimer()
{
  try {
    bool aBuildAtOnce = myBuildAtOnceCheckBox->isChecked();

    if( !aBuildAtOnce )
    {
      myTime = myTime.addMSecs( 100 );
      if( myTime.minute() > 9 && myTimeLCDNumber->numDigits() < 9 )
        myTimeLCDNumber->setNumDigits( 9 );
      myTimeLCDNumber->display( myTime.toString( "m:ss.zzz" ) );

      bool isEntitiesDone = myResult->IsEntitiesDone();
      bool isFieldsDone = myResult->IsFieldsDone();
      bool isMinMaxDone = myResult->IsMinMaxDone();
      bool isGroupsDone = myResult->IsGroupsDone();

      updateButton( myBuildEntitiesButton, isEntitiesDone );
      updateButton( myBuildFieldsButton, isFieldsDone );
      updateButton( myBuildMinMaxButton, isMinMaxDone );
      updateButton( myBuildGroupsButton, isGroupsDone );
    }

    if( myResult->IsDone() )
    {
      myTimer->stop();

      if( aBuildAtOnce )
      {
        updateButton( myBuildEntitiesButton, true );
        updateButton( myBuildFieldsButton, true );
        updateButton( myBuildMinMaxButton, true );
        updateButton( myBuildGroupsButton, true );

        QApplication::restoreOverrideCursor();

        QTime aTime;
        int mSecs = ( int )( 1000 * ( vtkTimerLog::GetUniversalTime() - myCurrentTime ) );
        aTime = aTime.addMSecs( mSecs );
        if( aTime.minute() > 9 )
          myTimeLCDNumber->setNumDigits( 9 );
        myTimeLCDNumber->display( aTime.toString( "m:ss.zzz" ) );
      }

      if( myCloseCheckBox->isChecked() )
        done( 0 );
      else
        SUIT_MessageBox::warning( this,
                                  tr( "INF_VISU" ),
                                  tr( "IMPORT_DONE" ),
                                  tr( "BUT_OK" ) );
    }
  }
  catch( ... ) {
    done( 1 );
  }
}

void VisuGUI_BuildProgressDlg::updateButton( QPushButton* theButton, bool theIsDone )
{
  QPalette aPal = theButton->palette();
  QColor aCurrentColor = aPal.color( theButton->backgroundRole() );
  if( aCurrentColor == Qt::gray || aCurrentColor == Qt::green )
    return;

  QColor aNewColor = Qt::green;

  if( !theIsDone )
  {
    int r, g, b;
    aCurrentColor.getRgb( &r, &g, &b );
    if( g == 0 )
      myIsRaiseColor = true;
    else if( g == 255 )
      myIsRaiseColor = false;

    int gNew = g + ( myIsRaiseColor ? 1 : -1 ) * 51;

    aNewColor.setRgb( 255, gNew, 0 );    
  }
  aPal.setColor( theButton->backgroundRole(), aNewColor );
  theButton->setPalette( aPal );
}

void VisuGUI_BuildProgressDlg::done( int r )
{
  QApplication::restoreOverrideCursor();
  myTimer->stop();
  QDialog::done( r );
}

void VisuGUI_BuildProgressDlg::onBuildCheckBoxClicked()
{
  QCheckBox* aCheckBox = ( QCheckBox* )sender();
  if( !aCheckBox )
    return;

  bool anIsChecked = aCheckBox->isChecked();

  QColor aColor = anIsChecked ? Qt::red : Qt::gray;

  if( aCheckBox == myBuildAllCheckBox )
  {
    if( anIsChecked && !myBuildAtOnceCheckBox->isChecked() )
      myBuildAtOnceCheckBox->animateClick();
  }
  else if( aCheckBox == myBuildAtOnceCheckBox )
  {
    if( !anIsChecked && myBuildAllCheckBox->isChecked() )
      myBuildAllCheckBox->animateClick();
  }
  else if( aCheckBox == myBuildFieldsCheckBox )
  {
    QPalette aPal = myBuildFieldsButton->palette();
    aPal.setColor( myBuildFieldsButton->backgroundRole(), aColor );
    myBuildFieldsButton->setPalette( aPal );
    if( !anIsChecked && myBuildMinMaxCheckBox->isChecked() )
      myBuildMinMaxCheckBox->animateClick();
  }
  else if( aCheckBox == myBuildMinMaxCheckBox )
  {
    QPalette aPal = myBuildMinMaxButton->palette();
    aPal.setColor( myBuildMinMaxButton->backgroundRole(), aColor );
    myBuildMinMaxButton->setPalette( aPal );
    if( anIsChecked && !myBuildFieldsCheckBox->isChecked() )
      myBuildFieldsCheckBox->animateClick();
  }
  else if( aCheckBox == myBuildGroupsCheckBox )
  {
    QPalette aPal = myBuildGroupsButton->palette();
    aPal.setColor( myBuildGroupsButton->backgroundRole(), aColor );
    myBuildGroupsButton->setPalette( aPal );
  }

}

void VisuGUI_BuildProgressDlg::setFileName( const QString& theFileName )
{
  if ( !theFileName.isNull() )
    {
      myFileName = theFileName;
      myFileNameLineEdit->setText( myFileName.section( '/', -1 ) );
    }
}

void VisuGUI_BuildProgressDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

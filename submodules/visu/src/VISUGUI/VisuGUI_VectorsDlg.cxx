// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_VectorsDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND 
//  Module : VISU
//  $Header$
//
#include "VisuGUI_VectorsDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_Vectors_i.hh"
#include "LightApp_Application.h"
#include "SalomeApp_Module.h"
#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include "SUIT_Desktop.h"
#include "SUIT_MessageBox.h"

#include <QtxColorButton.h>

#include <QLayout>
#include <QColorDialog>
#include <QTabWidget>
#include <QButtonGroup>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QKeyEvent>

#include <limits>

using namespace std;

/*!
  Constructor
*/
VisuGUI_VectorsDlg::VisuGUI_VectorsDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule)
{
  setWindowTitle(tr("DLG_TITLE"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* TopLayout = new QVBoxLayout( this ); 
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin( 11 );

  myTabBox = new QTabWidget(this);

  QWidget* aBox = new QWidget(this);
  QVBoxLayout* aVBLay = new QVBoxLayout( aBox );
  aVBLay->setMargin( 11 );

  TopGroup = new QGroupBox( aBox );
  aVBLay->addWidget( TopGroup );
  //TopGroup->setColumnLayout(0, Qt::Vertical );
  //TopGroup->layout()->setSpacing( 0 );
  //TopGroup->layout()->setMargin( 5 );
  QGridLayout* TopGroupLayout = new QGridLayout( TopGroup );
  TopGroupLayout->setAlignment( Qt::AlignTop );
  TopGroupLayout->setSpacing( 6 );
  TopGroupLayout->setMargin( 11 );

  // Scale factor
  ScaleLabel = new QLabel (tr("LBL_SCALE_FACTOR"), TopGroup );

  ScalFact = new SalomeApp_DoubleSpinBox( TopGroup );
  VISU::initSpinBox( ScalFact, 0., 1.0E+38, .1, "visual_data_precision" );
  ScalFact->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  ScalFact->setValue( 0.1 );

  TopGroupLayout->addWidget( ScaleLabel, 0, 0 );
  TopGroupLayout->addWidget( ScalFact, 0, 1 );

  // Line width
  LineWidLabel = new QLabel (tr("LBL_LINE_WIDTH"), TopGroup );

  LinWid = new SalomeApp_IntSpinBox( TopGroup );
  LinWid->setAcceptNames( false );
  LinWid->setMinimum( 1 );
  LinWid->setMaximum( 10 );
  LinWid->setSingleStep( 1 );
  
  LinWid->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  LinWid->setValue( 1 );

  TopGroupLayout->addWidget( LineWidLabel, 1, 0 );
  TopGroupLayout->addWidget( LinWid, 1, 1 );

  // Color
  UseMagn = new QCheckBox (tr("MAGNITUDE_COLORING_CHK"), TopGroup);
  //UseMagn->setText(tr("MAGNITUDE_COLORING_CHK"));
  SelColor = new QtxColorButton (TopGroup);
  SelColor->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  SelColor->setText( tr("SEL_COLOR_BTN") );

  /*  ColorLab = new QLabel( TopGroup, "ColorLab" );
  ColorLab->setFixedSize( SelColor->sizeHint().height(), SelColor->sizeHint().height() );
  ColorLab->setFrameStyle( QLabel::Plain | QLabel::Box );
  */
  TopGroupLayout->addWidget( UseMagn,  2, 0 );
  //TopGroupLayout->addWidget( ColorLab, 2, 1 );
  TopGroupLayout->addWidget( SelColor, 2, 1 );

  // Gliphs
  UseGlyph = new QCheckBox (tr("USE_GLYPHS_CHK"), TopGroup);
  
  TypeGlyph = new QButtonGroup ( TopGroup);
  TypeGB = new QGroupBox( tr("GLYPH_TYPE_GRP"), TopGroup );
  //TypeGlyph->setColumnLayout(0, Qt::Vertical );
  //TypeGlyph->layout()->setSpacing( 0 );
  //TypeGlyph->layout()->setMargin( 0 );
  QGridLayout* TypeGlyphLayout = new QGridLayout( TypeGB );
  TypeGlyphLayout->setAlignment( Qt::AlignTop );
  TypeGlyphLayout->setSpacing( 6 );
  TypeGlyphLayout->setMargin( 11 );

  RBArrows = new QRadioButton (tr("ARROWS_BTN"), TypeGB );
  TypeGlyphLayout->addWidget( RBArrows, 0, 0 );
  RBCones2 = new QRadioButton (tr("CONES2_BTN"), TypeGB );
  TypeGlyphLayout->addWidget( RBCones2, 1, 0 );
  RBCones6 = new QRadioButton (tr("CONES6_BTN"), TypeGB );
  TypeGlyphLayout->addWidget( RBCones6, 2, 0 );

  TypeGlyph->addButton( RBArrows );
  TypeGlyph->addButton( RBCones2 );
  TypeGlyph->addButton( RBCones6 );

  PosGlyph = new QButtonGroup ( TopGroup );
  PosGB = new QGroupBox( tr("GLYPH_POSITION_GRP"), TopGroup );
  //PosGlyph->setColumnLayout(0, Qt::Vertical );
  //PosGlyph->layout()->setSpacing( 0 );
  //PosGlyph->layout()->setMargin( 0 );
  QGridLayout* PosGlyphLayout = new QGridLayout( PosGB );
  PosGlyphLayout->setAlignment( Qt::AlignTop );
  PosGlyphLayout->setSpacing( 6 );
  PosGlyphLayout->setMargin( 11 );
  
  RBTail = new QRadioButton (tr("TAIL_BTN"  ), PosGB );
  PosGlyphLayout->addWidget( RBTail, 0, 0 );
  RBCent = new QRadioButton (tr("CENTER_BTN"), PosGB );
  PosGlyphLayout->addWidget( RBCent, 1, 0 );
  RBHead = new QRadioButton (tr("HEAD_BTN"  ), PosGB );
  PosGlyphLayout->addWidget( RBHead, 2, 0 );

  PosGlyph->addButton( RBTail );
  PosGlyph->addButton( RBCent );
  PosGlyph->addButton( RBHead );

  TopGroupLayout->addWidget( UseGlyph, 3, 0, 1, 3 );
  TopGroupLayout->addWidget( TypeGB, 4, 0 );
  TopGroupLayout->addWidget( PosGB, 4, 1, 1, 2 );

  // Common buttons ===========================================================
  GroupButtons = new QGroupBox( this );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setSpacing( 0 );
  //GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  buttonOk = new QPushButton( tr( "BUT_OK" ), GroupButtons );
  buttonOk->setAutoDefault( TRUE );
  buttonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonOk, 0, 0 );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );
  buttonCancel = new QPushButton( tr( "BUT_CANCEL" ) , GroupButtons );
  buttonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonCancel, 0, 2 );
  buttonHelp = new QPushButton( tr( "BUT_HELP" ) , GroupButtons );
  buttonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonHelp, 0, 3 );

  // top layout
  myTabBox->addTab(aBox, tr("VISU_VECTORS"));
  myInputPane = new VisuGUI_InputPane(VISU::TVECTORS, theModule, this);
  myTabBox->addTab(GetScalarPane(), tr("SCALAR_BAR_TAB"));
  myTabBox->addTab(myInputPane, tr("INPUT_TAB"));

  TopLayout->addWidget( myTabBox );
  TopLayout->addWidget( GroupButtons );
  
  // signals and slots connections
  connect( UseGlyph,     SIGNAL( clicked() ), this, SLOT( enableGlyphType() ) );
  //connect( SelColor,     SIGNAL( clicked() ), this, SLOT( setVColor() ) );
  connect( UseMagn,      SIGNAL( clicked() ), this, SLOT( enableSetColor() ) );
  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
  
  // default values
  UseMagn->setChecked( TRUE );
  UseGlyph->setChecked( TRUE );
  RBArrows->setChecked( TRUE );
  RBTail->setChecked( TRUE );
  setColor( QColor( 255, 0, 0 ) );
  enableGlyphType();
  enableSetColor();
}

VisuGUI_VectorsDlg::~VisuGUI_VectorsDlg()
{}

void VisuGUI_VectorsDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                            bool theInit )
{
  if ( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TVECTORS>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  setScaleFactor(myPrsCopy->GetScale());
  setLineWidth((int)myPrsCopy->GetLineWidth());
  setUseMagnColor(myPrsCopy->IsColored());
  SALOMEDS::Color anOldColor = myPrsCopy->GetColor();
  QColor aColor = QColor(int(255*anOldColor.R),int(255*anOldColor.G),int(255*anOldColor.B));
  setColor(aColor);
  
  if (myPrsCopy->GetGlyphType() != VISU::Vectors::NONE) {
    setUseGlyphs(true);
    setGlyphType(myPrsCopy->GetGlyphType());
    setGlyphPos(myPrsCopy->GetGlyphPos());
  } else 
    setUseGlyphs(false);

  enableSetColor();

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}


int VisuGUI_VectorsDlg::storeToPrsObject(VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );
  
  myPrsCopy->SetScale(getScaleFactor());
  myPrsCopy->SetLineWidth(getLineWidth());
  myPrsCopy->ShowColored(getUseMagnColor());
  if(!myPrsCopy->IsColored()){
    QColor aSelectedColor = SelColor->color();
    SALOMEDS::Color aColor;
    aColor.R = aSelectedColor.red()/255.;
    aColor.G = aSelectedColor.green()/255.;
    aColor.B = aSelectedColor.blue()/255.;
    myPrsCopy->SetColor(aColor);
  }
  if (getUseGlyphs()) {
    myPrsCopy->SetGlyphPos(getGlyphPos());
    myPrsCopy->SetGlyphType(getGlyphType());
  } else 
    myPrsCopy->SetGlyphType(VISU::Vectors::NONE);

  VISU::TSameAsFactory<VISU::TVECTORS>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}



/*!
  Called when "Use glyphs" check box clicked
*/
void VisuGUI_VectorsDlg::enableGlyphType()
{
  TypeGB->setEnabled( UseGlyph->isChecked() );
  PosGB->setEnabled( UseGlyph->isChecked() );
}

/*!
  Called when "Magnitude Coloring" check box clicked
*/
void VisuGUI_VectorsDlg::enableSetColor()
{
  SelColor->setEnabled(!UseMagn->isChecked() );
  //ColorLab->setEnabled( UseMagn->isEnabled() && !UseMagn->isChecked() );
}

/*!
  Called when "Select Color" buttonx clicked
*/
/*void VisuGUI_VectorsDlg::setVColor()
{
  QColor cnew = QColorDialog::getColor( myColor, this );
  if ( cnew.isValid() )
    setColor( cnew );
}*/

/*!
  Sets Scale factor
*/
void VisuGUI_VectorsDlg::setScaleFactor(double theFactor)
{
  double step = 0.1;
  if (fabs(theFactor) > std::numeric_limits<double>::epsilon()) {
    int degree = int(log10(fabs(theFactor))) - 1;
    if (fabs(theFactor) < 1) {
      // as logarithm value is negative in this case
      // and it is truncated to the bigger integer
      degree -= 1;
    }
    step = pow(10., double(degree));
  }

  ScalFact->setSingleStep(step);
  ScalFact->setValue(theFactor);
}

/*!
  Gets Scale factor
*/
double VisuGUI_VectorsDlg::getScaleFactor()
{
  return ScalFact->value();
}

/*!
  Sets Line width
*/
void VisuGUI_VectorsDlg::setLineWidth( int lw )
{
  LinWid->setValue( lw );
}

/*!
  Gets Line width
*/
int VisuGUI_VectorsDlg::getLineWidth() 
{
  return LinWid->value();
}

/*!
  Sets "Use Magnitude Coloring" flag
*/
void VisuGUI_VectorsDlg::setUseMagnColor( bool on )
{
  UseMagn->setChecked( on );
  enableSetColor();
}

/*!
  Gets "Use Magnitude Coloring" flag state
*/
bool VisuGUI_VectorsDlg::getUseMagnColor()
{
  return UseMagn->isChecked();
}

/*!
  Sets "Use Glyphs" flag
*/
void VisuGUI_VectorsDlg::setUseGlyphs( bool on ) 
{
  UseGlyph->setChecked( on );
  enableGlyphType();
}

/*!
  Gets "Use Glyphs" flag state
*/
bool VisuGUI_VectorsDlg::getUseGlyphs()
{
  return UseGlyph->isChecked();
}

/*!
  Sets color
*/
void VisuGUI_VectorsDlg::setColor( QColor color )
{
  //myColor = color;
  //QPalette aPal = SelColor->palette();
  //aPal.setColor( SelColor->backgroundRole(), myColor );
  //SelColor->setPalette( aPal );
  SelColor->setColor( color );
}

/*!
  Sets glyphs type : 0 - arrows, 1 - cones2, 2 - cones6
*/
void VisuGUI_VectorsDlg::setGlyphType(VISU::Vectors::GlyphType type )
{
  if ( type ==  VISU::Vectors::CONE2) 
    RBCones2->setChecked( true );
  else if ( type == VISU::Vectors::CONE6)
    RBCones6->setChecked( true );
  else
    RBArrows->setChecked( true );
}

/*!
  Gets glyphs type : 0 - arrows, 1 - cones2, 2 - cones6
*/
VISU::Vectors::GlyphType VisuGUI_VectorsDlg::getGlyphType()
{
  VISU::Vectors::GlyphType type;
  if ( RBCones2->isChecked() )
    type = VISU::Vectors::CONE2;
  else if ( RBCones6->isChecked() )
    type = VISU::Vectors::CONE6;
  else
    type = VISU::Vectors::ARROW;
  return type;
}

/*!
  Sets glyph position : -1 - tail, 0 - center, 1 - head
*/
void VisuGUI_VectorsDlg::setGlyphPos(VISU::Vectors::GlyphPos pos)
{
  if ( pos == VISU::Vectors::TAIL) 
    RBTail->setChecked(true);
  else if ( pos == VISU::Vectors::HEAD)
    RBHead->setChecked(true);
  else
    RBCent->setChecked(true);
}

/*!
  Gets glyph position : -1 - tail, 0 - center, 1 - head
*/
VISU::Vectors::GlyphPos VisuGUI_VectorsDlg::getGlyphPos()
{
  VISU::Vectors::GlyphPos pos;
  if ( RBTail->isChecked() )
    pos = VISU::Vectors::TAIL;
  else if ( RBHead->isChecked() )
    pos = VISU::Vectors::HEAD;
  else
    pos = VISU::Vectors::CENTER;
  return pos;
}

/*!
  Enbled/disables magnitude coloring
*/
void VisuGUI_VectorsDlg::enableMagnColor( bool enable )
{
  UseMagn->setEnabled( enable );
  enableSetColor();
}

QString VisuGUI_VectorsDlg::GetContextHelpFilePath()
{
  return "vectors_page.html";
}

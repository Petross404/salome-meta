// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_ValuesLabelingDlg.cxx
//  Author : Litonin SERGEY
//  Module : SALOME
//
#include "VisuGUI_ValuesLabelingDlg.h"
#include "VisuGUI.h"

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QtxColorButton.h>
#include <QtxFontEdit.h>
#include <QVBoxLayout>

/*!
  \class VisuGUI_ValuesLabelingDlg
  \brief This class is intended for changing parameters of labeling values
  of 3D colored presentations
*/

/*!
  \brief Constructor
  \param theModule module
*/
VisuGUI_ValuesLabelingDlg::VisuGUI_ValuesLabelingDlg( SalomeApp_Module* theModule )
: VisuGUI_Prs3dDlg( theModule )
{
  setWindowTitle( tr( "PARAMETERS_OF_VALUES_LABELING" ) );

  // font groups

  QGroupBox* aFGrp = new QGroupBox( tr( "LABELS" ), this );

  QLabel* aFont = new QLabel( tr( "FONT" ) );
  QLabel* aColor = new QLabel( tr( "COLOR" ) );
  myFont = new QtxFontEdit( aFGrp );
  myColor = new  QtxColorButton( aFGrp );
  myColor->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  QGridLayout* aFLay = new QGridLayout( aFGrp );
  aFLay->setSpacing( 5 );
  aFLay->setMargin( 5 );

  aFLay->addWidget( aFont, 0, 0 );
  aFLay->addWidget( myFont, 0, 1 );
  aFLay->addWidget( aColor, 1, 0 );
  aFLay->addWidget( myColor, 1, 1 );

  // buttons

  QGroupBox* aBtnGrp = new QGroupBox( this );
  QGridLayout* aBtnLay = new QGridLayout( aBtnGrp );
  aBtnLay->setAlignment( Qt::AlignTop );
  aBtnLay->setSpacing( 5 );
  aBtnLay->setMargin( 5 );

  QPushButton* anOK = new QPushButton( tr("BUT_OK"), aBtnGrp );
  anOK->setAutoDefault( true );
  anOK->setDefault( true );
  aBtnLay->addWidget( anOK, 0, 0 );
  aBtnLay->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* aCancel = new QPushButton( tr("BUT_CANCEL") , aBtnGrp );
  aCancel->setAutoDefault( true );
  aBtnLay->addWidget( aCancel, 0, 2 );

  QPushButton* aHelp = new QPushButton( tr("BUT_HELP") , aBtnGrp );
  aHelp->setAutoDefault( true );
  aBtnLay->addWidget( aHelp, 0, 3 );

  myFont->setMode( QtxFontEdit::Custom );
  myFont->setFeatures( QtxFontEdit::Family | QtxFontEdit::Bold | QtxFontEdit::Italic |
    QtxFontEdit::Shadow | QtxFontEdit::UserSize | QtxFontEdit::Size );
  QStringList fam;
  fam.append( tr( "VISU_FONT_ARIAL" ) );
  fam.append( tr( "VISU_FONT_COURIER" ) );
  fam.append( tr( "VISU_FONT_TIMES" ) );
  myFont->setFonts( fam );

  // Layout widgets

  QVBoxLayout* aMainLay = new QVBoxLayout( this );
  aMainLay->setSpacing( 5 );
  aMainLay->setMargin( 5 );
  aMainLay->addWidget( aFGrp );
  aMainLay->addWidget( aBtnGrp );

  // Connect signals and slots
  connect( anOK,SIGNAL( clicked() ), SLOT( accept() ) );
  connect( aCancel, SIGNAL( clicked() ), SLOT( reject() ) );
  connect( aHelp, SIGNAL( clicked() ), SLOT( onHelp() ) );

  setFixedSize( minimumSizeHint() );
}

/*!
  \brief Destructor
*/
VisuGUI_ValuesLabelingDlg::~VisuGUI_ValuesLabelingDlg()
{
}

/*!
  \brief Initializes dialog fields using specified presentation
  \param thePrs presentation
  \param theInit not used
  \sa storeToPrsObject()
*/
void VisuGUI_ValuesLabelingDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs, bool )
{
  if ( !thePrs )
    return;

  int aFType = thePrs->GetValLblFontType();

  double aSize = thePrs->GetValLblFontSize();

  bool isBold = thePrs->IsBoldValLbl();
  bool isItalic = thePrs->IsItalicValLbl();
  bool isShadow = thePrs->IsShadowValLbl();

  double aGRB[ 3 ];
  thePrs->GetValLblFontColor( aGRB[ 0 ], aGRB[ 1 ], aGRB[ 2 ] );

  // font
  QFont aFont;

  QString aFamily;
  if ( aFType == VTK_ARIAL )
    aFamily = "Arial";
  else if ( aFType == VTK_COURIER )
    aFamily = "Courier";
  else if ( aFType == VTK_TIMES )
    aFamily = "Times";

  aFont.setFamily( aFamily );
  aFont.setPointSize( aSize );
  aFont.setBold( isBold );
  aFont.setItalic( isItalic );
  aFont.setOverline( isShadow );

  myFont->setCurrentFont( aFont );

  // color
  QColor aColor( aGRB[ 0 ] * 255, aGRB[ 1 ] * 255, aGRB[ 2 ] * 255 );
  myColor->setColor( aColor );
}

/*!
  \brief Updates presentation in accordance with dialog input data
  \param thePrs to be updated
  \sa initFromPrsObject()
*/
int VisuGUI_ValuesLabelingDlg::storeToPrsObject( VISU::ColoredPrs3d_i* thePrs )
{
  if ( !thePrs )
    return 0;

  QFont aFont = myFont->currentFont();

  // type
  if ( aFont.family() == "Arial" )
    thePrs->SetValLblFontType( VTK_ARIAL );
  else if ( aFont.family() == "Courier" )
    thePrs->SetValLblFontType( VTK_COURIER );
  else if ( aFont.family() == "Times" )
    thePrs->SetValLblFontType( VTK_TIMES );

  // size
  int aSize = aFont.pointSize();
  if ( aSize > 0 )
    thePrs->SetValLblFontSize( aSize );

  // bold, italic, shadow
  thePrs->SetBoldValLbl( aFont.bold() );
  thePrs->SetItalicValLbl( aFont.italic() );
  thePrs->SetShadowValLbl( aFont.overline() );

  // color
  QColor aColor = myColor->color();
  thePrs->SetValLblFontColor(
    aColor.red() / 255., aColor.green() / 255., aColor.blue() / 255. );

  return 1;
}

/*!
  \brief Gets help file name
*/
QString VisuGUI_ValuesLabelingDlg::GetContextHelpFilePath()
{
  return "viewing_3d_presentations_page.html#labeling_parameters_anchor";
}

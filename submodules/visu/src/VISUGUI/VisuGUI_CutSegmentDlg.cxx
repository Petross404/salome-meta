// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_CutSegmentDlg.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_CutSegmentDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_Gen_i.hh"
#include "VISU_CutSegment_i.hh"
#include "VISU_ColoredPrs3dFactory.hh"

#include "VISU_PipeLine.hxx"
#include "VISU_CutSegmentPL.hxx"

#include "SVTK_ViewWindow.h"

#include "LightApp_Application.h"
#include "SalomeApp_Study.h"
#include <SalomeApp_DoubleSpinBox.h>

#include "SUIT_ResourceMgr.h"

#include <QLayout>
#include <QTabWidget>
#include <QLabel>
#include <QPushButton>

#include <vtkAppendPolyData.h>
#include <vtkCellArray.h>
#include <vtkDataSetMapper.h>
#include <vtkFloatArray.h>
#include <vtkGlyph3D.h>
#include <vtkGlyphSource2D.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>

using namespace std;

VisuGUI_CutSegmentDlg::VisuGUI_CutSegmentDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule),
    myPreviewActor(0),
    myPreviewActorGlyphs(0),
    myPreviewActorPoints(0)
{
  setWindowTitle(tr("Cut Segment Definition"));
  setSizeGripEnabled(true);

  QVBoxLayout* aMainLayout = new QVBoxLayout (this);
  aMainLayout->setMargin( 7 );
  aMainLayout->setSpacing(5);

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  hasInit = false;  

  // Tab pane
  myTabBox = new QTabWidget(this);

  // Cut Segment
  QFrame* aCutSegmentPane = new QFrame( this );

  QGroupBox* aSegmentBox = new QGroupBox( tr( "LBL_SEGMENT" ), aCutSegmentPane );

  QLabel* aPoint1Label = new QLabel( tr( "LBL_POINT_1" ), aSegmentBox );
  QLabel* aPoint1XLabel = new QLabel( tr( "LBL_X" ), aSegmentBox );
  QLabel* aPoint1YLabel = new QLabel( tr( "LBL_Y" ), aSegmentBox );
  QLabel* aPoint1ZLabel = new QLabel( tr( "LBL_Z" ), aSegmentBox );
  QLabel* aPoint2Label = new QLabel( tr( "LBL_POINT_2" ), aSegmentBox );
  QLabel* aPoint2XLabel = new QLabel( tr( "LBL_X" ), aSegmentBox );
  QLabel* aPoint2YLabel = new QLabel( tr( "LBL_Y" ), aSegmentBox );
  QLabel* aPoint2ZLabel = new QLabel( tr( "LBL_Z" ), aSegmentBox );
  for( int i = 0; i < 3; i++ ) {
    myPoint1.append( new SalomeApp_DoubleSpinBox( aSegmentBox ) );
    myPoint2.append( new SalomeApp_DoubleSpinBox( aSegmentBox ) );
  }

  QListIterator<SalomeApp_DoubleSpinBox*> anIter( myPoint1 + myPoint2 );
  while( anIter.hasNext() ) {
    SalomeApp_DoubleSpinBox* aSpinBox = anIter.next();
    // Use default range - see QDoubleSpinBox minimum/maximum properties
    VISU::initSpinBox( aSpinBox, 0., 99.99, 1., "length_precision" );
    aSpinBox->setMinimumWidth( 100 );
  }

  QGridLayout* aSegmentBoxLayout = new QGridLayout( aSegmentBox );
  aSegmentBoxLayout->addWidget( aPoint1Label,  0, 0 );
  aSegmentBoxLayout->addWidget( aPoint1XLabel, 0, 1 );
  aSegmentBoxLayout->addWidget( myPoint1[0],   0, 2 );
  aSegmentBoxLayout->addWidget( aPoint1YLabel, 0, 3 );
  aSegmentBoxLayout->addWidget( myPoint1[1],   0, 4 );
  aSegmentBoxLayout->addWidget( aPoint1ZLabel, 0, 5 );
  aSegmentBoxLayout->addWidget( myPoint1[2],   0, 6 );
  aSegmentBoxLayout->addWidget( aPoint2Label,  1, 0 );
  aSegmentBoxLayout->addWidget( aPoint2XLabel, 1, 1 );
  aSegmentBoxLayout->addWidget( myPoint2[0],   1, 2 );
  aSegmentBoxLayout->addWidget( aPoint2YLabel, 1, 3 );
  aSegmentBoxLayout->addWidget( myPoint2[1],   1, 4 );
  aSegmentBoxLayout->addWidget( aPoint2ZLabel, 1, 5 );
  aSegmentBoxLayout->addWidget( myPoint2[2],   1, 6 );

  myPreviewCheck = new QCheckBox( tr( "LBL_SHOW_PREVIEW" ), aCutSegmentPane );
  myPreviewCheck->setChecked( aResourceMgr->booleanValue( "VISU", "show_preview", false ) );

  myAllCurvesInvertedCheck = new QCheckBox( tr( "LBL_INVERT_CURVES" ), aCutSegmentPane );
  myAllCurvesInvertedCheck->setChecked( false );

  myUseAbsoluteLengthCheck = new QCheckBox( tr( "LBL_ABSOLUTE_LENGTH" ), aCutSegmentPane );
  myUseAbsoluteLengthCheck->setChecked( false );

  myCreateTable = new QCheckBox( tr("LBL_GENERATE_TABLE"), aCutSegmentPane );
  myCreateTable->setChecked( aResourceMgr->booleanValue( "VISU", "generate_data_table", true ) );

  QWidget* aCheckPane = new QWidget( aCutSegmentPane );
  myCurvesCheck = new QCheckBox( tr( "LBL_GENERATE_CURVES" ), aCheckPane );
  myCurvesCheck->setChecked( aResourceMgr->booleanValue( "VISU", "generate_curves", true ) );
  myCurvesCheck->setEnabled( aResourceMgr->booleanValue( "VISU", "generate_data_table", true ) );

  QHBoxLayout* aCheckLayout = new QHBoxLayout( aCheckPane );
  aCheckLayout->setMargin( 0 );
  aCheckLayout->setSpacing( 0 );
  aCheckLayout->insertSpacing( 0, 20 );
  aCheckLayout->addWidget( myCurvesCheck );

  QVBoxLayout* aCutSegmentLayout = new QVBoxLayout( aCutSegmentPane );
  aCutSegmentLayout->setMargin( 5 );
  aCutSegmentLayout->setSpacing( 6 );
  aCutSegmentLayout->addWidget( aSegmentBox );
  aCutSegmentLayout->addWidget( myPreviewCheck );
  aCutSegmentLayout->addWidget( myAllCurvesInvertedCheck );
  aCutSegmentLayout->addWidget( myUseAbsoluteLengthCheck );
  aCutSegmentLayout->addWidget( myCreateTable );
  aCutSegmentLayout->addWidget( aCheckPane );
  aCutSegmentLayout->addStretch();

  myTabBox->addTab( aCutSegmentPane, tr("LBL_CUT_SEGMENT") );

  myInputPane = new VisuGUI_InputPane(VISU::TCUTSEGMENT, theModule, this);
  myTabBox->addTab(GetScalarPane(), tr("Scalar Bar"));
  myTabBox->addTab(myInputPane, tr("Input"));

  aMainLayout->addWidget(myTabBox);

  // Dialog buttons
  QFrame* aBtnBox = new QFrame(this);
  QHBoxLayout* aHBLay = new QHBoxLayout( aBtnBox );
  aBtnBox->setFrameStyle(QFrame::Box | QFrame::Sunken);
  aBtnBox->setLineWidth( 1 );
  aHBLay->setSpacing( 5 );
  aHBLay->setMargin( 11 );

  QPushButton* aOkBtn = new QPushButton (tr("BUT_OK"), aBtnBox);
  aOkBtn->setAutoDefault( TRUE );
  aOkBtn->setDefault( true );
  aHBLay->addWidget(aOkBtn);

  aHBLay->addStretch();

  QPushButton* aCloseBtn = new QPushButton(tr("BUT_CANCEL"), aBtnBox);
  aHBLay->addWidget(aCloseBtn);

  QPushButton* aHelpBtn = new QPushButton(tr("BUT_HELP"), aBtnBox);
  aHBLay->addWidget(aHelpBtn);

  aMainLayout->addWidget(aBtnBox);

  // signals and slots connections
  connect(myPreviewCheck, SIGNAL(toggled(bool))         , this, SLOT(onPreviewCheck(bool)));
  connect(myAllCurvesInvertedCheck, SIGNAL(toggled(bool)),this, SLOT(onAllCurvesInvertedCheck(bool)));
  for( int i = 0; i < 3; i++ ) {
    connect(myPoint1[ i ], SIGNAL(valueChanged(double)), this, SLOT(onPointModified()));
    connect(myPoint2[ i ], SIGNAL(valueChanged(double)), this, SLOT(onPointModified()));
  }
  connect(myCreateTable , SIGNAL(toggled(bool)), myCurvesCheck, SLOT(setEnabled(bool)));

  connect(aOkBtn, SIGNAL(clicked()), this, SLOT(accept()));
  connect(aCloseBtn, SIGNAL(clicked()), this, SLOT(reject()));
  connect(aHelpBtn, SIGNAL(clicked()), this, SLOT(onHelp()));
}

/*!
  Destructor
*/
VisuGUI_CutSegmentDlg::~VisuGUI_CutSegmentDlg()
{
  deletePlanes();
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
    vf->Repaint();
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::initFromPrsObject ( VISU::ColoredPrs3d_i* thePrs,
                                                bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TCUTSEGMENT>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  hasInit = true;
  myCutSegment = VISU::TSameAsFactory<VISU::TCUTSEGMENT>().Create(myPrsCopy, VISU::ColoredPrs3d_i::EDoNotPublish);
  myCutSegment->CopyCurvesInverted(myPrsCopy->GetCurvesInverted());
  myAllCurvesInvertedCheck->setChecked( myCutSegment->IsAllCurvesInverted() );
  myUseAbsoluteLengthCheck->setChecked( myCutSegment->IsUseAbsoluteLength() );

  vtkDataSet* anInput = myCutSegment->GetPipeLine()->GetInput();
  myCutSegment->GetPipeLine()->Update();

  double bounds[6];
  anInput->GetBounds( bounds );

  for( int i = 0; i < 3; i++ ) {
    double min = bounds[ 2*i ], max = bounds[ 2*i+1 ];
    myPoint1[ i ]->setRange( min, max );
    myPoint2[ i ]->setRange( min, max );

    myPoint1[ i ]->setSingleStep( ( max - min ) / 10 );
    myPoint2[ i ]->setSingleStep( ( max - min ) / 10 );
  }

  double p1[3], p2[3];
  myPrsCopy->GetPoint1( p1[0], p1[1], p1[2] );
  myPrsCopy->GetPoint2( p2[0], p2[1], p2[2] );

  for( int i = 0; i < 3; i++ ) {
    myPoint1[i]->setValue( p1[i] );
    myPoint2[i]->setValue( p2[i] );
  }

  // Draw Preview
  if (myPreviewCheck->isChecked()) {
    createPlanes();
  }

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

//------------------------------------------------------------------------------
int VisuGUI_CutSegmentDlg::storeToPrsObject (VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );

  myPrsCopy->SetAllCurvesInverted( myAllCurvesInvertedCheck->isChecked() );
  myPrsCopy->SetUseAbsoluteLength( myUseAbsoluteLengthCheck->isChecked() );

  double p1[3], p2[3];
  for( int i = 0; i < 3; i++ ) {
    p1[i] = myPoint1[i]->value();
    p2[i] = myPoint2[i]->value();
  }
  myPrsCopy->SetPoint1( p1[0], p1[1], p1[2] );
  myPrsCopy->SetPoint2( p2[0], p2[1], p2[2] );

  VISU::TSameAsFactory<VISU::TCUTSEGMENT>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::createPlanes()
{
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView == NULL) return;
  if (!(bool)myCutSegment) return;
  if (myPreviewActor != 0) return;

  storeToPrsObject(myCutSegment);
  myCutSegment->GetPipeLine()->Update();
  vtkAppendPolyData* aPolyData = myCutSegment->GetSpecificPL()->GetAppendPolyData();
  vtkDataSetMapper* aPlaneMapper = vtkDataSetMapper::New();
  aPlaneMapper->SetInputConnection(aPolyData->GetOutputPort());
  aPlaneMapper->ScalarVisibilityOff();
  
  myPreviewActorGlyphs = SALOME_Actor::New();
  myPreviewActorGlyphs->PickableOff();
  updateGlyphs(false);
  
  myPreviewActorPoints = SALOME_Actor::New();
  myPreviewActorPoints->PickableOff();
  updatePoints(false);
  
  myPreviewActor = SALOME_Actor::New();
  myPreviewActor->PickableOff();
  myPreviewActor->SetMapper(aPlaneMapper);
  aPlaneMapper->Delete();
  aView->AddActor(myPreviewActor);
  aView->AddActor(myPreviewActorGlyphs);
  aView->AddActor(myPreviewActorPoints);
  aView->getRenderer()->Render();
  aView->onFitAll();
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::deletePlanes()
{
  if (myPreviewActor == 0) return;
  vtkRenderer* aRend       = myPreviewActor->GetRenderer();
  vtkRenderer* aRendGlyphs = myPreviewActorGlyphs->GetRenderer();
  vtkRenderer* aRendPoints = myPreviewActorPoints->GetRenderer();
  
  vtkRenderWindow* aWnd1 = aRend->GetRenderWindow();
  vtkRenderWindow* aWnd2 = aRendGlyphs->GetRenderWindow();
  vtkRenderWindow* aWnd3 = aRendPoints->GetRenderWindow();
  if(aRend && aWnd1)
    myPreviewActor->RemoveFromRender(aRend);
  if(aRendGlyphs && aWnd2)
    myPreviewActorGlyphs->RemoveFromRender(aRendGlyphs);
  if(aRendPoints && aWnd3)
    myPreviewActorPoints->RemoveFromRender(aRendPoints);

  myPreviewActor->Delete();
  myPreviewActorGlyphs->Delete();
  myPreviewActorPoints->Delete();
  myPreviewActor = 0;
  myPreviewActorGlyphs = 0;
  myPreviewActorPoints = 0;
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::updateGlyphs(bool update){
  if (myPreviewActorGlyphs == 0 ) return;
  const double *aDirLn = myCutSegment->GetSpecificPL()->GetRealDirLn();
  double aSecondPnt[3];
  double aBoundCenter[3];

  vtkAppendPolyData* aPolyData = myCutSegment->GetSpecificPL()->GetAppendPolyData();
  vtkDataSetMapper* aPlaneMapper = vtkDataSetMapper::New();
  aPlaneMapper->SetInputConnection(aPolyData->GetOutputPort());
  double bounds[6];
  aPlaneMapper->GetBounds(bounds);

  for(int i=0; i<3; i++) aBoundCenter[i] = (bounds[i*2] + bounds[i*2+1])/2.0;
  for(int i=0; i<3; i++){ 
    if (!myAllCurvesInvertedCheck->isChecked())
      aSecondPnt[i] = aDirLn[i];
    else
      aSecondPnt[i] = - aDirLn[i];
  }
  
  double max_bound = 0;
  max_bound < bounds[1]-bounds[0] ? max_bound = bounds[1] - bounds[0] : max_bound = max_bound;
  max_bound < bounds[3]-bounds[2] ? max_bound = bounds[3] - bounds[2] : max_bound = max_bound;
  max_bound < bounds[5]-bounds[4] ? max_bound = bounds[5] - bounds[4] : max_bound = max_bound;

  vtkPolyData* profile = vtkPolyData::New();
  vtkPoints* aPoints = vtkPoints::New();
  vtkGlyph3D* glyphs = vtkGlyph3D::New();
  vtkFloatArray *aFloatArray = vtkFloatArray::New();
  vtkGlyphSource2D* source = vtkGlyphSource2D::New();
  
  source->FilledOn();
  source->SetGlyphTypeToArrow();
  
  aPoints->InsertNextPoint(aBoundCenter);
  profile->SetPoints(aPoints);
  
  aFloatArray->SetNumberOfComponents(3);
  for(int i=0; i<3 ;i++)
    aFloatArray->InsertNextValue(aSecondPnt[i]);
  
  vtkDataSetAttributes* aDataSetAttributes;
  aDataSetAttributes = profile->GetPointData();
  aDataSetAttributes->SetVectors(aFloatArray);
  
  glyphs->SetScaleFactor(0.25*max_bound);
  glyphs->SetVectorModeToUseVector();
  glyphs->SetScaleModeToScaleByVector();
  glyphs->SetInputData(profile);
  glyphs->SetSourceConnection(source->GetOutputPort());

  vtkPolyDataMapper* aGlyphsMapper = vtkPolyDataMapper::New();
  aGlyphsMapper->ScalarVisibilityOff();
  aGlyphsMapper->SetInputConnection(glyphs->GetOutputPort());
  
  myPreviewActorGlyphs->SetMapper(aGlyphsMapper);

  profile->Delete();
  glyphs->Delete();
  aPoints->Delete();
  aGlyphsMapper->Delete();
  aFloatArray->Delete();
  source->Delete();
  aPlaneMapper->Delete();

  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>())
    if (update)
      vf->Repaint();
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::updatePoints( bool update )
{
  if( !myPreviewActorPoints )
    return;

  double aPoint1[3], aPoint2[3];
  myCutSegment->GetSpecificPL()->GetPoint1( aPoint1[0], aPoint1[1], aPoint1[2] );
  myCutSegment->GetSpecificPL()->GetPoint2( aPoint2[0], aPoint2[1], aPoint2[2] );

  vtkPoints* aPoints = vtkPoints::New();
  aPoints->InsertNextPoint( aPoint1 );
  aPoints->InsertNextPoint( aPoint2 );

  vtkCellArray* aVerts = vtkCellArray::New();
  for( vtkIdType i = 0, n = aPoints->GetNumberOfPoints(); i < n; i++ )
    aVerts->InsertNextCell( 1, &i );

  vtkPolyData* aProfile = vtkPolyData::New();
  aProfile->SetPoints( aPoints );
  aProfile->SetVerts( aVerts );

  vtkPolyDataMapper* aPointsMapper = vtkPolyDataMapper::New();
  aPointsMapper->ScalarVisibilityOff();
  aPointsMapper->SetInputData( aProfile );
  
  myPreviewActorPoints->SetMapper( aPointsMapper );
  myPreviewActorPoints->GetProperty()->SetRepresentationToPoints();
  myPreviewActorPoints->GetProperty()->SetPointSize( 5 );

  aProfile->Delete();
  aPoints->Delete();
  aVerts->Delete();

  if( SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>() )
    if( update )
      vf->Repaint();
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::accept()
{
  VisuGUI_ScalarBarBaseDlg::accept();
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::reject()
{
  VisuGUI_ScalarBarBaseDlg::reject();
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::onPreviewCheck (bool thePreview)
{
  if (SVTK_ViewWindow* vf = VISU::GetActiveViewWindow<SVTK_ViewWindow>()) {
    if (thePreview) {
      createPlanes();
    } else {
      deletePlanes();
      vf->Repaint();
    }
  }
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::onAllCurvesInvertedCheck(bool theInvert)
{
  bool anIsAllCurvesInverted = myAllCurvesInvertedCheck->isChecked();

  myCutSegment->SetAllCurvesInverted( anIsAllCurvesInverted );
  myPrsCopy->SetAllCurvesInverted( anIsAllCurvesInverted );

  updateGlyphs(true);
  updatePoints(true);
}

//------------------------------------------------------------------------------
void VisuGUI_CutSegmentDlg::onPointModified ()
{
  if (!(bool)myCutSegment) return;
  SVTK_ViewWindow* aView = VISU::GetActiveViewWindow<SVTK_ViewWindow>();
  if (aView) {
    if (myPreviewCheck->isChecked()) {
      deletePlanes();
      createPlanes();
    }
  }
}

//------------------------------------------------------------------------------
QString VisuGUI_CutSegmentDlg::GetContextHelpFilePath()
{
  return "cut_segment_page.html";
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_IsoSurfacesDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header$
//
#ifndef VISUGUI_ISOSURFACESDLG_H
#define VISUGUI_ISOSURFACESDLG_H

#include "VisuGUI_Prs3dDlg.h"

class QTabWidget;
class QLineEdit;
class SalomeApp_IntSpinBox;
class QButtonGroup;
class QtxColorButton;

namespace VISU 
{
  class IsoSurfaces_i;
};

class SalomeApp_Module;
class VisuGUI_InputPane;

class VisuGUI_IsoSurfPane : public QWidget
{
  Q_OBJECT;

public:
  enum { ScalarBarRange = 0, CustomRange };

public:
  VisuGUI_IsoSurfPane (QWidget* parent,
                       VisuGUI_ScalarBarPane* theScalarPane);
  ~VisuGUI_IsoSurfPane() {};

  void initFromPrsObject(VISU::IsoSurfaces_i* thePrs);
  int storeToPrsObject(VISU::IsoSurfaces_i* thePrs);

  bool check();

  void setColor(const QColor& theColor);
  QColor color() const;

protected slots:
  void onRangeButtonClicked( int );
  void onCBUpdate();
  void setColor();

private:
  QButtonGroup*          myRangeGrp;
  QLineEdit*             MinIso;
  QLineEdit*             MaxIso;
  SalomeApp_IntSpinBox*  NbrIso;
  QCheckBox*             myUseMagnitude;
  QtxColorButton*        mySelColor;
  QCheckBox*             myUseLabels;
  SalomeApp_IntSpinBox*  myNbLabels;
  VisuGUI_ScalarBarPane* myScalarPane;
};


class VisuGUI_IsoSurfacesDlg : public VisuGUI_ScalarBarBaseDlg
{
  Q_OBJECT;

public:
  VisuGUI_IsoSurfacesDlg (SalomeApp_Module* theModule);
  ~VisuGUI_IsoSurfacesDlg();

  virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                  bool theInit );

  virtual int storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

protected:
  virtual QString  GetContextHelpFilePath();

 protected slots:
  void accept();

private:
  QTabWidget*            myTabBox;
  VisuGUI_IsoSurfPane*   myIsoPane;
  VisuGUI_InputPane*     myInputPane;

  SALOME::GenericObjPtr<VISU::IsoSurfaces_i> myPrsCopy;
};

#endif // VISUGUI_ISOSURFACESDLG_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_GaussPointsSelectionPane.h
//  Author : Oleg Uvarov
//  Module : VISU
//
#ifndef VISUGUI_GAUSSPOINTSSELECTIONPANE_H
#define VISUGUI_GAUSSPOINTSSELECTIONPANE_H

#include <vtkObject.h>
#include <vtkSmartPointer.h>

#include <QLineEdit>

class vtkActorCollection;
class vtkCallbackCommand;
class vtkImageData;

class QLabel;
class QComboBox;
class QCheckBox;
class QPushButton;

class QtxAction;

class VISU_GaussPtsAct;
class VISU_PickingSettings;

class SalomeApp_Module;
class SVTK_RenderWindowInteractor;
class VisuGUI_ValidatedLineEdit;

//! Picking Dialog.
/*!
 * Uses for set up picking preferenses and apply
 * them to all actors in the current renderer.
 */
class VisuGUI_GaussPointsSelectionPane : public QWidget
{
  Q_OBJECT

public:
  VisuGUI_GaussPointsSelectionPane( const SalomeApp_Module* theModule, QWidget* theParent );

  ~VisuGUI_GaussPointsSelectionPane();

  //! update dialog contents.
  void                                  update();

  void                                  setIds( const int, const int );

  void                                  setInteractor( SVTK_RenderWindowInteractor* );

  void                                  apply();
  //void                                  help();

protected:
  SVTK_RenderWindowInteractor*          getInteractor();

protected slots:
  void                                  onSelectionValidate();
  void                                  onSelectionApply();

private slots:
  void                                  onApplyDisplayParentMesh( bool );

private:
  static void                           ProcessEvents(vtkObject* theObject, 
                                                      unsigned long theEvent,
                                                      void* theClientData, 
                                                      void* theCallData);

  void                                  KeyPressed();
  void                                  SelectionEvent();
  //void                                  keyPressEvent( QKeyEvent* e );

  void                                  clearIDControls();

private:
  const SalomeApp_Module*               myModule;

  QCheckBox*                            myDisplayParentMeshCheckBox;

  VisuGUI_ValidatedLineEdit*            myParentElementLineEdit;
  VisuGUI_ValidatedLineEdit*            myLocalPointLineEdit;

  QValidator*                           myCellIDValidator;
  QValidator*                           myLocalPointIDValidator;

  float                                 myPriority;
  vtkSmartPointer<vtkCallbackCommand>   myEventCallbackCommand;
};

class VisuGUI_ValidatedLineEdit : public QLineEdit
{
  Q_OBJECT;
public:
  VisuGUI_ValidatedLineEdit( QWidget* parent );
  
public slots:
  void MarkValidated( const QString& theText );
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VisuGUI_EditContainerDlg.cxx
//  Author : VSV
//  Module : VISU
//
#include "VisuGUI_EditContainerDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"

#include "VISU_Table_i.hh"

#include "SalomeApp_Application.h"
#include "SUIT_Desktop.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"

#include <QLayout>
#include <QCheckBox>
#include <QGroupBox>
#include <QPushButton>
#include <QLabel>
#include <QTreeWidget>
#include <QToolButton>
#include <QKeyEvent>


using namespace std;


/* XPM */
static const char * left_xpm[] = {
"24 24 61 1",
"       c None",
".      c #323232",
"+      c #010101",
"@      c #4A4A4A",
"#      c #040404",
"$      c #979797",
"%      c #7B7B7B",
"&      c #939393",
"*      c #EEEEEE",
"=      c #686868",
"-      c #030303",
";      c #9C9C9C",
">      c #FFFFFF",
",      c #B1B1B1",
"'      c #5E5E5E",
")      c #060606",
"!      c #C1C1C1",
"~      c #626262",
"{      c #000000",
"]      c #989898",
"^      c #A1A1A1",
"/      c #A5A5A5",
"(      c #909090",
"_      c #868686",
":      c #616161",
"<      c #959595",
"[      c #919191",
"}      c #848484",
"|      c #606060",
"1      c #0B0B0B",
"2      c #545454",
"3      c #727272",
"4      c #A2A2A2",
"5      c #969696",
"6      c #838383",
"7      c #5F5F5F",
"8      c #101010",
"9      c #434343",
"0      c #6B6B6B",
"a      c #858585",
"b      c #8E8E8E",
"c      c #373737",
"d      c #696969",
"e      c #8D8D8D",
"f      c #B5B5B5",
"g      c #111111",
"h      c #393939",
"i      c #898989",
"j      c #B0B0B0",
"k      c #191919",
"l      c #3A3A3A",
"m      c #646464",
"n      c #535353",
"o      c #050505",
"p      c #353535",
"q      c #585858",
"r      c #4C4C4C",
"s      c #0D0D0D",
"t      c #3E3E3E",
"u      c #020202",
"v      c #0A0A0A",
"                        ",
"                        ",
"         .+        .+   ",
"        .@#       .@#   ",
"       .$%#      .$%#   ",
"      .&*=-     .&*=-   ",
"     .;>,')    .;>,')   ",
"    .;>!;~{   .;>!;~{   ",
"   .]>!^&~{  .]>!^&~{   ",
"  ./>!/(_:{ ./>!/(_:{   ",
" .<*!^[}}|{.<*!^[}}|{   ",
" 123}45667{123}45667{   ",
"  890a45b7{ 890a45b7{   ",
"   8cdef5'{  8cdef5'{   ",
"    gh0ij7k   gh0ij7k   ",
"     8lm0no    8lm0no   ",
"      8pqr-     8pqr-   ",
"       sht-      sht-   ",
"        1.u       1.u   ",
"         v{        v{   ",
"                        ",
"                        ",
"                        ",
"                        "};

static QPixmap MYLeftPix(left_xpm);


static const char * right_xpm[] = {
"24 24 43 1",
"       g None",
".      g #323232",
"+      g #5D5D5D",
"@      g #000000",
"#      g #C4C4C4",
"$      g #313131",
"%      g #C5C5C5",
"&      g #E4E4E4",
"*      g #2D2D2D",
"=      g #B7B7B7",
"-      g #EFEFEF",
";      g #DCDCDC",
">      g #282828",
",      g #AFAFAF",
"'      g #E0E0E0",
")      g #242424",
"!      g #C7C7C7",
"~      g #9A9A9A",
"{      g #8E8E8E",
"]      g #1F1F1F",
"^      g #A5A5A5",
"/      g #989898",
"(      g #888888",
"_      g #1B1B1B",
":      g #ADADAD",
"<      g #858585",
"[      g #838383",
"}      g #868686",
"|      g #929292",
"1      g #C1C1C1",
"2      g #161616",
"3      g #909090",
"4      g #747474",
"5      g #3A3A3A",
"6      g #121212",
"7      g #0D0D0D",
"8      g #7A7A7A",
"9      g #8A8A8A",
"0      g #090909",
"a      g #040404",
"b      g #707070",
"c      g #6E6E6E",
"d      g #3F3F3F",
"                        ",
"                        ",
" ..        ..           ",
" .+@       .+@          ",
" .#+@      .#+@         ",
" $%&+@     $%&+@        ",
" *=-;+@    *=-;+@       ",
" >,'=;+@   >,'=;+@      ",
" ),!~{;+@  ),!~{;+@     ",
" ]^!/({;+@ ]^!/({;+@    ",
" _~:<[}|1+@_~:<[}|1+@   ",
" 23~[[{:45@23~[[{:45@   ",
" 6</[{:45@ 6</[{:45@    ",
" 789{:45@  789{:45@     ",
" 08~:45@   08~:45@      ",
" a4~45@    a4~45@       ",
" @b45@     @b45@        ",
" @c5@      @c5@         ",
" @d@       @d@          ",
" @@        @@           ",
"                        ",
"                        ",
"                        ",
"                        "};

static QPixmap MYRightPix(right_xpm);


VisuGUI_EditContainerDlg::VisuGUI_EditContainerDlg (VisuGUI* theModule, bool theIsModal)
     : QDialog(VISU::GetDesktop(theModule), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
       myVisuGUI(theModule)
{
  setModal( theIsModal );
  if (!theIsModal) {
    setAttribute( Qt::WA_DeleteOnClose, true );
  }
  setWindowTitle("Edit Plot 2D Presentation");
  setSizeGripEnabled(true);

  QVBoxLayout* TopLayout = new QVBoxLayout (this );
  TopLayout->setMargin( 6 );
  TopLayout->setSpacing( 11);

  /***************************************************************/
  QFrame* aControlFrame = new QFrame (this);
  aControlFrame->setFrameStyle(QFrame::Box | QFrame::Sunken);

  QGridLayout* aControlLay = new QGridLayout (aControlFrame);
  aControlLay->setSpacing(6);
  aControlLay->setMargin(11);
  //aControlLay->addRowSpacing(1, 30);
  //aControlLay->addRowSpacing(4, 30);
  aControlLay->setRowStretch(1, 1);
  aControlLay->setRowStretch(4, 1);
  //aControlLay->addColSpacing(0, 180);
  //aControlLay->addColSpacing(2, 180);

  QLabel* aSelectLbl = new QLabel (tr("LBL_STUDY"), aControlFrame);
  aControlLay->addWidget(aSelectLbl, 0, 0);

  myStudyLst = new QTreeWidget (aControlFrame);
  myStudyLst->setSelectionMode(QAbstractItemView::ExtendedSelection);
  myStudyLst->setColumnCount( 3 );
  QStringList aLabels;
  aLabels.append( tr("TXT_TABLE") );
  aLabels.append( tr("TXT_CURVE") );
  aLabels.append( "" );
  myStudyLst->setHeaderLabels( aLabels );
  myStudyLst->setColumnWidth( 0, 80 );
  myStudyLst->setColumnWidth( 1, 50 );
  myStudyLst->setColumnWidth( 2, 0 );
  
  //myStudyLst->addColumn(tr("TXT_TABLE"), 80);
  //myStudyLst->addColumn(tr("TXT_CURVE"), 50);
  //myStudyLst->addColumn(tr(""), 0);
  
  myStudyLst->setAllColumnsShowFocus(true);
  //myStudyLst->setMinimumHeight(130);
  connect(myStudyLst, SIGNAL(itemSelectionChanged()), this, SLOT(onLeftSelected()));
  aControlLay->addWidget(myStudyLst, 1, 0, 5, 1);

  myRightBtn = new QToolButton (aControlFrame);
  myRightBtn->setIcon(MYRightPix);
  myRightBtn->setEnabled(false);
  connect(myRightBtn, SIGNAL(clicked()), this, SLOT(onRightClicked()));
  aControlLay->addWidget(myRightBtn, 2, 1);

  myLeftBtn = new QToolButton(aControlFrame);
  myLeftBtn->setIcon(MYLeftPix);
  myLeftBtn->setEnabled(false);
  connect(myLeftBtn, SIGNAL(clicked()), this, SLOT(onLeftClicked()));
  aControlLay->addWidget(myLeftBtn, 3, 1);

  QLabel* aForceLbl = new QLabel (tr("LBL_CONTAINER"), aControlFrame);
  aControlLay->addWidget(aForceLbl, 0, 2);

  myContainerLst = new QTreeWidget(aControlFrame);
  myContainerLst->setSelectionMode(QAbstractItemView::ExtendedSelection);
  //myContainerLst->addColumn(tr("TXT_TABLE"), 80);
  //myContainerLst->addColumn(tr("TXT_CURVE"), 50);
  //myContainerLst->addColumn(tr(""), 0);
  myContainerLst->setColumnCount( 3 );
  myContainerLst->setHeaderLabels( aLabels );
  myContainerLst->setColumnWidth( 0, 80 );
  myContainerLst->setColumnWidth( 1, 50 );
  myContainerLst->setColumnWidth( 2, 0 );

  
  //myContainerLst->setMinimumWidth(130);
  connect(myContainerLst, SIGNAL(itemSelectionChanged()), this, SLOT(onRightSelected()));
  aControlLay->addWidget(myContainerLst, 1, 2, 5, 1);

  TopLayout->addWidget(aControlFrame);

  // Common buttons ===========================================================
  QGroupBox* GroupButtons = new QGroupBox(this);
  //GroupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout(GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  QPushButton* buttonOk = new QPushButton(tr("&OK"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);
  GroupButtonsLayout->addItem(new QSpacerItem(5, 5, QSizePolicy::Expanding,
                                              QSizePolicy::Minimum), 0, 1);

  QPushButton* buttonCancel = new QPushButton(tr("&Cancel") , GroupButtons);
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 2);

  QPushButton* buttonHelp = new QPushButton(tr("&Help") , GroupButtons);
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 3);

  TopLayout->addWidget(GroupButtons);

  connect(buttonOk,     SIGNAL(clicked()),      this, SLOT(accept()));
  connect(buttonCancel, SIGNAL(clicked()),      this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()),      this, SLOT(onHelp()));
}

void VisuGUI_EditContainerDlg::initFromPrsObject (VISU::Container_i* theContainer)
{
  _PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myVisuGUI));
  _PTR(SComponent) aVisuSO = aStudy->FindComponent("VISU");
  if (!aVisuSO) {
    return;
  }
  QList<CurveStruct> aStudyCurves;
  QList<CurveStruct> aContainerCurves;
  // Find curves in container
  for (int i = 1; i <= theContainer->GetNbCurves(); i++) {
    VISU::Curve_i* aCurve = theContainer->GetCurve(i);
    if (aCurve == NULL) continue;
    CurveStruct aEntry;
    aEntry.CurveName = aCurve->GetTitle();
    aEntry.CurveEntry = aCurve->GetEntry().c_str();
    _PTR(SObject) aTableSO = aStudy->FindObjectID(aCurve->GetTableID());
    if (!aTableSO) continue;
    aEntry.TableName = getSObjectName(aTableSO);
    aContainerCurves.append(aEntry);
    QStringList aList;
    aList.append( aEntry.TableName );
    aList.append( aEntry.CurveName );
    aList.append( aEntry.CurveEntry );
    new QTreeWidgetItem(myContainerLst, aList);
  }
  // Find curves in study
  _PTR(ChildIterator) aIter = aStudy->NewChildIterator(aVisuSO);
  for (aIter->InitEx(true); aIter->More(); aIter->Next()) {
    _PTR(SObject) aSObject = aIter->Value();
    CORBA::Object_var anObject = VISU::ClientSObjectToObject(aSObject);
    VISU::Base_var aVisuObj = VISU::Base::_narrow(anObject);
    if (!CORBA::is_nil(aVisuObj)) {
      if (aVisuObj->GetType() == VISU::TCURVE) {
        _PTR(SObject) aTableSO = aSObject->GetFather();
        CurveStruct aEntry;
        aEntry.TableName = getSObjectName(aTableSO);
        aEntry.CurveName = getSObjectName(aSObject);
        aEntry.CurveEntry = aSObject->GetID().c_str();
        aStudyCurves.append(aEntry);
      }
    }
  }
  //Show Curves which are not in Curve
  QList<CurveStruct>::iterator it;
  QList<CurveStruct>::iterator it2;
  bool isExist = false;
  for (it = aStudyCurves.begin(); it != aStudyCurves.end(); ++it) {
    for (it2 = aContainerCurves.begin(); it2 != aContainerCurves.end(); ++it2) {
      if (isExist = ((*it).CurveEntry == (*it2).CurveEntry))
        break;
    }
    if (!isExist)
    {
      QStringList aList;
      aList.append( (*it).TableName );
      aList.append( (*it).CurveName );
      aList.append( (*it).CurveEntry );
      new QTreeWidgetItem(myStudyLst, aList );
    }
  }
}

void VisuGUI_EditContainerDlg::storeToPrsObject (VISU::Container_i* theContainer)
{
  theContainer->Clear();

  _PTR(Study) aStudy = VISU::GetCStudy(VISU::GetAppStudy(myVisuGUI));
  QTreeWidgetItem* anItem;
  for ( int i= 0; i< myContainerLst->topLevelItemCount(); i++  )
  {
    anItem = myContainerLst->topLevelItem( i );
    if ( !anItem )
      continue;
    _PTR(SObject) aCurveSO = aStudy->FindObjectID((const char*)anItem->text(2).toLatin1());
    if (aCurveSO) {
      CORBA::Object_var aObject = VISU::ClientSObjectToObject(aCurveSO);
      if (!CORBA::is_nil(aObject)) {
        VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aObject).in());
        if (aCurve) theContainer->AddCurve(aCurve->_this());
      }
    }
  }
}

QString VisuGUI_EditContainerDlg::getSObjectName (_PTR(SObject) theSObject)
{
  if (!theSObject) return QString("");

  _PTR(GenericAttribute) anAttr;
  if (theSObject->FindAttribute(anAttr, "AttributeName")) {
    _PTR(AttributeName) aName (anAttr);
    return QString(aName->Value().c_str());
  }
  return QString("");
}

void VisuGUI_EditContainerDlg::onLeftClicked()
{
  /*QListViewItem* anItem = myContainerLst->firstChild();
  while (anItem) {
    if (anItem->isSelected()) {
      QListViewItem* anTmpItem = anItem;
      anItem = anTmpItem->nextSibling();
      myContainerLst->takeItem(anTmpItem);
      myStudyLst->insertItem(anTmpItem);
    } else {
      anItem = anItem->nextSibling();
    }
    }*/
  QList<QTreeWidgetItem*> selItem = myContainerLst->selectedItems();
  QList<QTreeWidgetItem*>::Iterator anIt = selItem.begin();

  QList<QTreeWidgetItem*> topSelectedItems;
  for ( ; anIt != selItem.end(); anIt++  )
  {
    int index = myContainerLst->indexOfTopLevelItem( *anIt );
    if ( index != -1 )
      topSelectedItems.append( myContainerLst->takeTopLevelItem( index ) );
  }

  myStudyLst->addTopLevelItems(topSelectedItems);
}

void VisuGUI_EditContainerDlg::onRightClicked()
{
  /*QListViewItem* anItem = myStudyLst->firstChild();
  while (anItem) {
    if (anItem->isSelected()) {
      QListViewItem* anTmpItem = anItem;
      anItem = anTmpItem->nextSibling();
      myStudyLst->takeItem(anTmpItem);
      myContainerLst->insertItem(anTmpItem);
    } else {
      anItem = anItem->nextSibling();
    }
    }*/
  QList<QTreeWidgetItem*> selItem = myStudyLst->selectedItems();
  QList<QTreeWidgetItem*>::Iterator anIt = selItem.begin();

  QList<QTreeWidgetItem*> topSelectedItems;
  for ( ; anIt != selItem.end(); anIt++  )
  {
    int index = myStudyLst->indexOfTopLevelItem( *anIt );
    if ( index != -1 )
      topSelectedItems.append( myStudyLst->takeTopLevelItem( index ) );
  }

  myContainerLst->addTopLevelItems(topSelectedItems);
}

void VisuGUI_EditContainerDlg::onLeftSelected()
{
  myRightBtn->setEnabled( myStudyLst->selectedItems().count() > 0 );
}

void VisuGUI_EditContainerDlg::onRightSelected()
{
  myLeftBtn->setEnabled( myContainerLst->selectedItems().count() > 0 );
}

void VisuGUI_EditContainerDlg::onHelp()
{
  QString aHelpFileName = "creating_xy_plot_page.html";
  SalomeApp_Application* app = (SalomeApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myVisuGUI ? app->moduleName(myVisuGUI->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, QObject::tr("WRN_WARNING"),
                             QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             QObject::tr("BUT_OK"));
  }
}

void VisuGUI_EditContainerDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 )
    {
      e->accept();
      onHelp();
    }
}

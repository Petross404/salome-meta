// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_CacheDlg.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VisuGUI_CacheDlg.h"

#include "VisuGUI_Tools.h"

#include "VISU_PipeLine.hxx"

#include "SUIT_Desktop.h"
#include "SUIT_MessageBox.h"
#include "SUIT_Session.h"
#include "SUIT_ResourceMgr.h"

#include "SalomeApp_Module.h"
#include <SalomeApp_DoubleSpinBox.h>

#include "LightApp_Application.h"

#include <QButtonGroup>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>

using namespace std;

VisuGUI_CacheDlg::VisuGUI_CacheDlg( VISU::ColoredPrs3dCache_var theCache,
                                    SalomeApp_Module* theModule )
  : QDialog( VISU::GetDesktop( theModule ), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
    myCache( theCache )
{
  setModal( true );
  setWindowTitle( tr( "CACHE_TITLE" ) );
  setAttribute( Qt::WA_DeleteOnClose );

  QVBoxLayout* aTopLayout = new QVBoxLayout( this );
  aTopLayout->setSpacing( 6 );
  aTopLayout->setMargin( 6 );
  //aTopLayout->setAutoAdd( true );

  long aMb = 1024 * 1024;
  bool isLimitedMemory = myCache->GetMemoryMode() == VISU::ColoredPrs3dCache::LIMITED;
  double aLimitedMemory = myCache->GetLimitedMemory();
  double aFreeMemory = (double)VISU_PipeLine::GetAvailableMemory( 8192 * aMb ) / (double)aMb;
  double anUsedMemory = myCache->GetMemorySize();
  double aLimitedMemoryMax = 
#ifdef WNT
          max
#else
          std::max
#endif
          (anUsedMemory + aFreeMemory, aLimitedMemory);

  // Settings
  QButtonGroup* aMemoryGroup = new QButtonGroup(  this );
  QGroupBox* aGB = new QGroupBox( tr( "MEMORY_MODE" ), this );
  aTopLayout->addWidget( aGB );
  QGridLayout* aGridLay = new QGridLayout( aGB );
  //aMemoryGroup->setRadioButtonExclusive( true );

  myLimitedMemoryButton = new QRadioButton( tr( "LIMITED_MEMORY" ), aGB );
  myLimitedMemoryButton->setChecked( isLimitedMemory );
  aGridLay->addWidget( myLimitedMemoryButton, 0, 0 );

  myMimimalMemoryButton = new QRadioButton( tr( "MINIMAL_MEMORY" ), aGB );
  myMimimalMemoryButton->setChecked( !isLimitedMemory );
  aGridLay->addWidget( myMimimalMemoryButton, 1, 0 );

  myLimitedMemory = new SalomeApp_DoubleSpinBox( aGB );
  VISU::initSpinBox( myLimitedMemory, 1.0, aLimitedMemoryMax, 10.0, "memory_precision" );
  myLimitedMemory->setSuffix( " Mb" );
  myLimitedMemory->setValue( aLimitedMemory );
  myLimitedMemory->setEnabled( isLimitedMemory );
  aGridLay->addWidget( myLimitedMemory, 0, 1 );
  

  connect( myLimitedMemoryButton, SIGNAL( toggled( bool ) ), myLimitedMemory, SLOT( setEnabled( bool ) ) );

  // Current state
  QGroupBox* aStateGroup = new QGroupBox( tr( "MEMORY STATE" ), this );
  aTopLayout->addWidget( aStateGroup );
  //aStateGroup->setColumnLayout( 0, Qt::Vertical );
  //aStateGroup->layout()->setSpacing( 0 );
  //aStateGroup->layout()->setMargin( 0 );

  QGridLayout* aStateLayout = new QGridLayout( aStateGroup );
  aStateLayout->setSpacing(6);
  aStateLayout->setMargin(6);

  QLabel* aUsedMemoryLabel = new QLabel( tr( "USED_BY_CACHE" ), aStateGroup );
  myUsedMemory = new QLineEdit( aStateGroup );
  myUsedMemory->setText( QString::number( anUsedMemory ) + " Mb" );
  myUsedMemory->setReadOnly( true );
  myUsedMemory->setEnabled( false );
  QPalette aPal = myUsedMemory->palette();
  aPal.setColor( myUsedMemory->foregroundRole(), Qt::black );
  myUsedMemory->setPalette( aPal );
  //myUsedMemory->setPaletteForegroundColor( Qt::black );

  QLabel* aFreeMemoryLabel = new QLabel( tr( "FREE" ), aStateGroup );
  myFreeMemory = new QLineEdit( aStateGroup );
  myFreeMemory->setText( QString::number( aFreeMemory ) + " Mb" );
  myFreeMemory->setReadOnly( true );
  myFreeMemory->setEnabled( false );
  aPal = myFreeMemory->palette();
  aPal.setColor( myFreeMemory->foregroundRole(), Qt::black );
  myFreeMemory->setPalette( aPal );
  //myFreeMemory->setPaletteForegroundColor( Qt::black );

  aStateLayout->addWidget( aUsedMemoryLabel, 0, 0 );
  aStateLayout->addWidget( myUsedMemory, 0, 1 );
  aStateLayout->addWidget( aFreeMemoryLabel, 1, 0 );
  aStateLayout->addWidget( myFreeMemory, 1, 1 );

  // Ok / Cancel
  QGroupBox* GroupButtons = new QGroupBox( this );
  aTopLayout->addWidget( GroupButtons );
  //GroupButtons->setColumnLayout(0, Qt::Vertical );
  //GroupButtons->layout()->setSpacing( 0 );
  //GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  QPushButton* buttonOk = new QPushButton( tr( "BUT_OK" ), GroupButtons );
  buttonOk->setAutoDefault( TRUE );
  buttonOk->setDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonOk, 0, 0 );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );

  QPushButton* buttonCancel = new QPushButton( tr( "BUT_CANCEL" ) , GroupButtons );
  buttonCancel->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonCancel, 0, 2 );

  QPushButton* buttonHelp = new QPushButton( tr( "BUT_HELP" ) , GroupButtons );
  buttonHelp->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( buttonHelp, 0, 3 );

  connect( buttonOk,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( buttonHelp,   SIGNAL( clicked() ), this, SLOT( onHelp() ) );
}

VisuGUI_CacheDlg::~VisuGUI_CacheDlg()
{
}

bool VisuGUI_CacheDlg::isLimitedMemory()
{
  return myLimitedMemoryButton->isChecked();
}

double VisuGUI_CacheDlg::getLimitedMemory()
{
  return myLimitedMemory->value();
}

void VisuGUI_CacheDlg::accept()
{
  if( isLimitedMemory() )
  {
    myCache->SetMemoryMode( VISU::ColoredPrs3dCache::LIMITED );
    myCache->SetLimitedMemory( (float)getLimitedMemory() );
  }
  else
    myCache->SetMemoryMode( VISU::ColoredPrs3dCache::MINIMAL );


  QDialog::accept();
}

void VisuGUI_CacheDlg::onHelp()
{
  QString aHelpFileName;// = "types_of_gauss_points_presentations_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(app->activeModule() ?
                             app->moduleName(app->activeModule()->moduleName()) : QString(""), aHelpFileName);
  else {
    SUIT_MessageBox::warning(0, tr("WRN_WARNING"),
                             tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", "application")).arg(aHelpFileName),
                             tr("BUT_OK"));
  }
}

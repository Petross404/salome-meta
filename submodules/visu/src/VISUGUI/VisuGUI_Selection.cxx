// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Selection.cxx
//  Author : Sergey Anikin 
//  Module : VISU
//
#include "VisuGUI_Selection.h"

#include "VisuGUI_ViewTools.h"
#include "VisuGUI_Tools.h"

#include "VISU_Result_i.hh"
#include "VISU_Gen_i.hh"

#include "VISU_Actor.h"
#include "VISU_ScalarMapAct.h"
#include "VISU_GaussPtsAct.h"

#include "SalomeApp_Study.h"
#include "LightApp_Study.h"
#include "LightApp_Displayer.h"

using namespace VISU;

//////////////////////////////////////////////////
// Class: VisuGUI_Selection
//////////////////////////////////////////////////


//---------------------------------------------------------------
QVariant VisuGUI_Selection::parameter( const int ind, const QString& p ) const
{
  QVariant val( LightApp_Selection::parameter( ind, p ) );
  if ( !val.isValid() ) {
    if      ( p == "type"           ) val = QVariant( type( ind ) );
    else if ( p == "isFieldPrs"     ) val = QVariant( isFieldPrs( ind ) );
    else if ( p == "nbComponents"   ) val = QVariant( nbComponents( ind ) );
    else if ( p == "medEntity"   ) val = QVariant( medEntity( ind ) );
    else if ( p == "medSource"   ) val = QVariant( medSource( ind ) );
    else if ( p == "representation" ) val = QVariant( representation( ind ) );
    else if ( p == "nbTimeStamps"   ) val = QVariant( nbTimeStamps( ind ) );
    else if ( p == "nbChildren"     ) val = QVariant( nbChildren( ind ) );
    else if ( p == "nbNamedChildren") val = QVariant( nbNamedChildren( ind ) );
    else if ( p == "isVisible"      ) val = QVariant( isVisible( ind ) );
    else if ( p == "isShrunk"       ) val = QVariant( isShrunk( ind ) );
    else if ( p == "hasActor"       ) val = QVariant( hasActor( ind ) );
    else if ( p == "isShading"      ) val = QVariant( isShading( ind ) );
    else if ( p == "isScalarMapAct" ) val = QVariant( isScalarMapAct( ind ) );
    else if ( p == "isGaussPtsAct" ) val = QVariant( isGaussPtsAct( ind ) );
    else if ( p == "isVisuComponent") val = QVariant( isVisuComponent( ind ) );
    else if ( p == "fullResolution"   ) val = QVariant( fullResolution( ind ) );
    else if ( p == "mediumResolution"   ) val = QVariant( mediumResolution( ind ) );
    else if ( p == "lowResolution"   ) val = QVariant( lowResolution( ind ) );
    else if ( p == "resolutionState"   ) val = QVariant( resolutionState( ind ) );
    else if ( p == "isThereAreVisibleCurves" ) val = QVariant( findDisplayedCurves( ind, false ) );
    else if ( p == "isThereAreHiddenCurves" ) val = QVariant( findDisplayedCurves( ind, true ) );
    else if ( p == "hasCurves"      ) val = QVariant( hasCurves( ind ) );
    else if ( p == "isPlot2dViewer"      ) val = QVariant( Plot2dViewerType( ind ) );
    else if ( p == "isValuesLabeled"  ) val = QVariant( isValuesLabeled( ind ) );
    else if ( p == "isScalarBarVisible" ) val = QVariant( isScalarBarVisible( ind ) );
    else if ( p == "quadratic2DMode" ) val = QVariant( quadratic2DMode(ind) );
    else if ( p == "hasDeviation" ) val = QVariant( hasDeviation(ind) );
    else if ( p == "isDeviationDisplayed" ) val = QVariant( isDeviationDisplayed(ind) );
  }

  return val;
}


//---------------------------------------------------------------
// Macro for converting VISU enumeration items into corresponding strings
#define ENUM2STRING( x, y ) \
  case y: \
    x = QString( #y ); \
    break

QString VisuGUI_Selection::type( const int ind ) const
{
  QString aResStr;
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return aResStr;

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, (const char*)entry( ind ).toLatin1());
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);

  VISU::Base_i* aBase = anObjectInfo.myBase;
  if(aBase){
    VISU::VISUType aType = aBase->GetType();
    if(aType == VISU::TCOLOREDPRS3DHOLDER){
      CORBA::Object_var anObject = ClientSObjectToObject(anObjectInfo.mySObject);
      VISU::ColoredPrs3dHolder_var aHolder = VISU::ColoredPrs3dHolder::_narrow(anObject);
      aType = aHolder->GetPrsType();
    }
    switch (aType) {
      ENUM2STRING( aResStr, VISU::TVISUGEN );
      ENUM2STRING( aResStr, VISU::TRESULT );
      ENUM2STRING( aResStr, VISU::TTABLE );
      ENUM2STRING( aResStr, VISU::TCURVE );
      ENUM2STRING( aResStr, VISU::TCONTAINER );
      ENUM2STRING( aResStr, VISU::TMESH );
      ENUM2STRING( aResStr, VISU::TSCALARMAP );
      ENUM2STRING( aResStr, VISU::TISOSURFACES );
      ENUM2STRING( aResStr, VISU::TDEFORMEDSHAPE );
      ENUM2STRING( aResStr, VISU::TSCALARMAPONDEFORMEDSHAPE );
      ENUM2STRING( aResStr, VISU::TCUTPLANES );
      ENUM2STRING( aResStr, VISU::TCUTLINES );
      ENUM2STRING( aResStr, VISU::TCUTSEGMENT );
      ENUM2STRING( aResStr, VISU::TVECTORS );
      ENUM2STRING( aResStr, VISU::TSTREAMLINES );
      ENUM2STRING( aResStr, VISU::TPLOT3D );
      ENUM2STRING( aResStr, VISU::TANIMATION );
      ENUM2STRING( aResStr, VISU::TPOINTMAP3D );
    }
  }

  if(aResStr.isNull()){
    VISU::VISUType aType = VISU::Storable::RestoringMap2Type(aMap);
    switch (aType) {
      ENUM2STRING( aResStr, VISU::TENTITY );
      ENUM2STRING( aResStr, VISU::TFAMILY );
      ENUM2STRING( aResStr, VISU::TGROUP );
      ENUM2STRING( aResStr, VISU::TVIEW3D );
      ENUM2STRING( aResStr, VISU::TFIELD );
      ENUM2STRING( aResStr, VISU::TTIMESTAMP );
    }
  }

  if(aResStr.isNull()){
    bool anIsExist;
    QString aVal = VISU::Storable::FindValue(aMap, "myComment", &anIsExist);
    if ( anIsExist && aVal != "MESH" )
      aResStr = "VISU::T" + aVal;
  }

  return aResStr;
}


//---------------------------------------------------------------
bool VisuGUI_Selection::isFieldPrs( const int ind ) const
{
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return false;

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, (const char*)entry( ind ).toLatin1());
  VISU::Base_i* aBase = anObjectInfo.myBase;
  return ( aBase && aBase->GetType() == VISU::TCOLOREDPRS3DHOLDER );
}


//---------------------------------------------------------------
QString VisuGUI_Selection::nbComponents( const int ind ) const
{
  QString aResStr;
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return aResStr;

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, (const char*)entry( ind ).toLatin1());
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);

  bool isExist;
  QString aVal = VISU::Storable::FindValue(aMap,"myNumComponent",&isExist);
  if ( isExist )
    aResStr = aVal;
  return aResStr;
}


//---------------------------------------------------------------
QString VisuGUI_Selection::resolutions( const int ind ) const
{
  QString aResStr;
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return aResStr;

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, entry( ind ).toLatin1().data());
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);

  bool isExist;
  QString aVal = VISU::Storable::FindValue(aMap,"myResolutions",&isExist);
  if ( isExist )
    aResStr = aVal;

  return aResStr;
}


//---------------------------------------------------------------
QString VisuGUI_Selection::resolution( const int ind, char theResoltuion ) const
{
  QString aResStr;

  QString aResolutions = resolutions( ind );
  if(aResolutions.isEmpty())
    return aResStr;
  
  if(aResolutions.indexOf(theResoltuion) != -1)
    aResStr = "1";
  else
    aResStr = "0";

  return aResStr;
}


//---------------------------------------------------------------
QString VisuGUI_Selection::fullResolution( const int ind ) const
{
  return resolution( ind, 'F');
}


//---------------------------------------------------------------
QString VisuGUI_Selection::mediumResolution( const int ind ) const
{
  return resolution( ind, 'M');
}


//---------------------------------------------------------------
QString VisuGUI_Selection::lowResolution( const int ind ) const
{
  return resolution( ind, 'L');
}


//---------------------------------------------------------------
QString VisuGUI_Selection::resolutionState( const int ind ) const
{
  QString aResStr;
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return aResStr;

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, entry( ind ).toLatin1().data());
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);

  bool isExist;
  QString aVal = VISU::Storable::FindValue(aMap,"myState",&isExist);
  if ( isExist ) {
    if ( aVal.toInt() == VISU::Result::FULL )
      aResStr = "F";
    if ( aVal.toInt() == VISU::Result::MEDIUM )
      aResStr = "M";
    if ( aVal.toInt() == VISU::Result::LOW )
      aResStr = "L";
    if ( aVal.toInt() == VISU::Result::HIDDEN )
      aResStr = "H";
  }

  return aResStr;
}


//---------------------------------------------------------------
QString VisuGUI_Selection::medEntity( const int ind ) const
{
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return QString();

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, (const char*)entry( ind ).toLatin1());
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);

  bool isExist;
  QString aVal = VISU::Storable::FindValue(aMap,"myEntityId",&isExist);
  if ( isExist ) {
    using namespace VISU;
    TEntity anEntityId = TEntity(aVal.toInt());
    switch(anEntityId){
    case NODE_ENTITY :
      return "NODE_ENTITY";
    case EDGE_ENTITY :
      return "EDGE_ENTITY";
    case FACE_ENTITY :
      return "FACE_ENTITY";
    case CELL_ENTITY :
      return "CELL_ENTITY";
    default:
      return QString();
    }
  }
  return QString();
}

QString VisuGUI_Selection::medSource( const int ind ) const
{
  _PTR(Study) aStudyDS = GetStudy()->studyDS();
  if(_PTR(SObject) aSObject = aStudyDS->FindObjectID((const char*)entry(ind).toLatin1())){
    VISU::Result_var aRes;
    if(VISU::Result_i* aResult = CheckResult(myModule,aSObject,aRes)){
      using namespace VISU;
      Result_i::ECreationId aCreationId = aResult->GetCreationId();
      switch(aCreationId){
      case Result_i::eImportFile :
        return "eImportFile";
      case Result_i::eCopyAndImportFile :
        return "eCopyAndImportFile";
      case Result_i::eImportMed :
        return "eImportMed";
      case Result_i::eImportMedField :
        return "eImportMedField";
      default:
        return QString();
      }      
    }
  }
  return QString();
}

QString VisuGUI_Selection::nbTimeStamps( const int ind ) const
{
  QString aResStr;
  SalomeApp_Study* aStudy = GetStudy();
  if ( !aStudy )
    return aResStr;

  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(aStudy, (const char*)entry( ind ).toLatin1());
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap(anObjectInfo.mySObject);

  bool isExist;
  const QString& aVal = VISU::Storable::FindValue(aMap,"myNbTimeStamps",&isExist);
  if ( isExist )
    aResStr = aVal;
  return aResStr;
}


//----------------------------------------------------------------------------
template<class TPopupFunctor>
struct TPopupDispatcher
{
  QString
  operator()(const SalomeApp_Module* theModule, 
             const QString& theEntry)
  {
    if(SUIT_ViewManager* aViewManager = theModule->getApp()->activeViewManager()){
      QString aType = aViewManager->getType();
      TPopupFunctor aFunctor;
      if(aType == SVTK_Viewer::Type())
        return aFunctor.template Get<SVTK_Viewer>(theModule, theEntry);
      //else if(aType == VVTK_Viewer::Type())
      //return aFunctor.template Get<VVTK_Viewer>(theModule, theEntry);
    }
    return QString();
  }    
};


//----------------------------------------------------------------------------
template<class TViewer>
bool
GetPrs3dSelectionInfo(const SalomeApp_Module* theModule,
                      const QString& theEntry,
                      VISU::Prs3d_i*& thePrs3d,
                      SVTK_ViewWindow*& theViewWindow,
                      VISU_Actor*& thenActor)
{
  VISU::TObjectInfo anObjectInfo = VISU::GetObjectByEntry(GetAppStudy(theModule), (const char*)theEntry.toLatin1());
  thePrs3d = GetPrs3dFromBase(anObjectInfo.myBase);
  if(!thePrs3d)
    return false;
  
  typedef typename TViewer::TViewWindow TViewWindow;
  theViewWindow = GetActiveViewWindow<TViewWindow>(theModule);
  if(!theViewWindow)
    return false;
  
  thenActor = FindActor(theViewWindow, thePrs3d);
  if(!thenActor)
    return false;
  
  return true;
}


//----------------------------------------------------------------------------
struct TViewFunctor
{
  template<class TViewer>
  QString
  Get(const SalomeApp_Module* theModule, 
      const QString& theEntry)
  {
    VISU_Actor* anActor = NULL;
    VISU::Prs3d_i* aPrs3d = NULL;
    VISU_ActorBase* anActorBase = NULL;
    VISU::PointMap3d_i* aPointMap3d = NULL;
    SVTK_ViewWindow* aViewWindow = NULL;
    if(GetPrs3dSelectionInfo<TViewer>(theModule, theEntry, aPrs3d, aViewWindow, anActor))
      return get(aPrs3d, aViewWindow, anActor);
    else {
      aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule);
      VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects(theModule);
      if ( aSelectionInfo.empty() )
        return QString();
      VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
      aPointMap3d = dynamic_cast<VISU::PointMap3d_i*>(aSelectionItem.myObjectInfo.myBase);
      anActorBase = VISU::FindActorBase(aViewWindow, aPointMap3d);
      return getPointMap(aPointMap3d, aViewWindow, anActorBase);
    }
  }
  
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    return QString();
  }

  QString
  virtual
  getPointMap(VISU::PointMap3d_i* thePrs3d,
              SVTK_ViewWindow* theViewWindow,
              VISU_ActorBase* theActor)
  {
    return QString();
  }
};


//----------------------------------------------------------------------------
struct TGetRepesentationFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    QString aResStr;
    switch (theActor->GetRepresentation()) {
      ENUM2STRING( aResStr, VISU::POINT );
      ENUM2STRING( aResStr, VISU::WIREFRAME );
      ENUM2STRING( aResStr, VISU::SHADED );
      ENUM2STRING( aResStr, VISU::INSIDEFRAME );
      ENUM2STRING( aResStr, VISU::SURFACEFRAME );
      ENUM2STRING( aResStr, VISU::FEATURE_EDGES );
    }
    return aResStr;
  }

  QString
  virtual
  getPointMap(VISU::PointMap3d_i* thePrs3d,
              SVTK_ViewWindow* theViewWindow,
              VISU_ActorBase* theActorBase)
  {
    QString aResStr = "";
    if (theActorBase) {
      switch (theActorBase->GetRepresentation()) {
        ENUM2STRING( aResStr, VISU::WIREFRAME ); // = 1
        ENUM2STRING( aResStr, VISU::SHADED ); // = 2
      }
    }
    return aResStr;
  }
};

QString VisuGUI_Selection::representation( const int ind ) const
{
  return TPopupDispatcher<TGetRepesentationFunctor>()(myModule, entry(ind));
}

//----------------------------------------------------------------------------
SalomeApp_Study* VisuGUI_Selection::GetStudy() const
  
{
  LightApp_Study* aLightStudy = const_cast<LightApp_Study*>( study() );
  return dynamic_cast<SalomeApp_Study*>( aLightStudy );
}

//----------------------------------------------------------------------------
int VisuGUI_Selection::nbChild( const int ind, const bool named ) const
{
  int cnt = 0;
  SalomeApp_Study* aSStudy = GetStudy();
  if ( !aSStudy )
    return cnt;
  _PTR(Study) aStudy =  GetCStudy( aSStudy );
  if ( aStudy ){
    _PTR(SObject) SO = aStudy->FindObjectID( (const char*)entry( ind ).toLatin1() );
    if ( SO ){
      for ( _PTR(ChildIterator) Iter = aStudy->NewChildIterator( SO ); Iter->More(); Iter->Next() ) {
        _PTR(SObject) refSO;
        if ( !Iter->Value()->ReferencedObject( refSO ) && ( !named || Iter->Value()->GetName().size() ) )
          cnt++;
      }
    }
  }
  return cnt;
}

//----------------------------------------------------------------------------
int VisuGUI_Selection::nbChildren( const int ind ) const
{
  return nbChild( ind, false );
}

//----------------------------------------------------------------------------
int VisuGUI_Selection::nbNamedChildren( const int ind ) const
{
  return nbChild( ind, true );
}


//----------------------------------------------------------------------------
struct TIsVisibleFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    return theActor->GetVisibility() ? "true" : "false";
  }
};

QString VisuGUI_Selection::isVisible( const int ind ) const
{
  return TPopupDispatcher<TIsVisibleFunctor>()(myModule, entry(ind));
}


//----------------------------------------------------------------------------
struct TIsShrunkFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    return theActor->IsShrunk() ? "1" : "0";
  }

  QString
  virtual
  getPointMap(VISU::PointMap3d_i* thePointMap,
              SVTK_ViewWindow* theViewWindow,
              VISU_ActorBase* theActorBase)
  {
    if (theActorBase)
      return theActorBase->IsShrunk() ? "1" : "0";
    else return "0";
  }
};

QString VisuGUI_Selection::isShrunk( const int ind ) const
{
  return TPopupDispatcher<TIsShrunkFunctor>()(myModule, entry(ind));
}


//----------------------------------------------------------------------------
bool VisuGUI_Selection::hasActor( const int ind ) const
{
  return !representation( ind ).isEmpty();
}


//----------------------------------------------------------------------------
struct TIsShadingFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    if(VISU_ScalarMapAct* anActor = dynamic_cast<VISU_ScalarMapAct*>(theActor))
      return anActor->IsShading() ? "1" : "0";
    return QString();
  }
};

QString VisuGUI_Selection::isShading( const int ind ) const
{
  return TPopupDispatcher<TIsShadingFunctor>()(myModule, entry(ind));
}


//----------------------------------------------------------------------------
struct TIsScalarMapActFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    return dynamic_cast<VISU_ScalarMapAct*>(theActor)? "1" : "0";
  }
};


//---------------------------------------------------------------
QString VisuGUI_Selection::isScalarMapAct( const int ind ) const
{
  return TPopupDispatcher<TIsScalarMapActFunctor>()(myModule, entry(ind));
}

//----------------------------------------------------------------------------
struct TIsGaussPtsActFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    return dynamic_cast<VISU_GaussPtsAct*>(theActor)? "1" : "0";
  }
};


//---------------------------------------------------------------
QString VisuGUI_Selection::isGaussPtsAct( const int ind ) const
{
  return TPopupDispatcher<TIsGaussPtsActFunctor>()(myModule, entry(ind));
}

//----------------------------------------------------------------------------
bool VisuGUI_Selection::isVisuComponent( const int ind ) const
{
  SalomeApp_Study* study = GetStudy();
  if ( !study )
    return false;
  
  _PTR(SObject) obj = study->studyDS()->FindObjectID( (const char*)entry( ind ).toLatin1() );
  if ( !obj )
    return false;
  CORBA::Object_var anObj = VISU::ClientSObjectToObject( obj );
  if( CORBA::is_nil( anObj ) )
    return false;

  return dynamic_cast<VISU::VISU_Gen_i*>( VISU::GetServant( anObj ).in() );
}

//---------------------------------------------------------------------------
bool VisuGUI_Selection::findDisplayedCurves( const int ind, bool findHidden ) const
{
  // findHidden == false - find at least one Visible curve in the childs of ind
  // findHidden == true - find at least one Hidden curve in the childs of ind
  SalomeApp_Study* aSStudy = GetStudy();
  if ( !aSStudy )
    return false;

  QString entryId;
  _PTR(Study) aStudy = GetCStudy( aSStudy );
  if ( aStudy ){
    _PTR(SObject) SO = aStudy->FindObjectID( (const char*) entry( ind ).toLatin1() );
    if ( SO ){
      for ( _PTR(ChildIterator) Iter = aStudy->NewChildIterator( SO ); Iter->More(); Iter->Next() ) {
        _PTR(SObject) refSO;
        if ( Iter->Value()->ReferencedObject(refSO) )
          entryId = refSO->GetID().c_str();
        else
          entryId = Iter->Value()->GetID().c_str();

        LightApp_Displayer* displayer = LightApp_Displayer::FindDisplayer( myModule->moduleName(), false );
        if ( displayer->IsDisplayed( entryId ) && findHidden == false )
          return true;
        else if ( !displayer->IsDisplayed( entryId ) && findHidden == true )
          return true;
      }
    }
  }
  return false;
}

//---------------------------------------------------------------------------
bool VisuGUI_Selection::hasCurves( const int ind ) const
{
  SalomeApp_Study* aSStudy = GetStudy();
  if ( !aSStudy )
    return false;

  QString entryId;
  _PTR(Study) aStudy = GetCStudy( aSStudy );
  if ( aStudy ){
    _PTR(SObject) SO = aStudy->FindObjectID( (const char*) entry( ind ).toLatin1() );
    if ( SO ){
      for ( _PTR(ChildIterator) Iter = aStudy->NewChildIterator( SO ); Iter->More(); Iter->Next() ) {
        _PTR(SObject) refSO;
        if ( Iter->Value()->ReferencedObject(refSO) ) {
          // reference on curve
        }
        else
          refSO = Iter->Value();

        CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(refSO);
        if(!CORBA::is_nil(aCORBAObject)){
          PortableServer::ServantBase_var aServant = VISU::GetServant(aCORBAObject);
          if(dynamic_cast<VISU::Curve_i*>(aServant.in()))
            return true;
        }
      }
    }
  }
  return false;
}

//---------------------------------------------------------------------------
bool VisuGUI_Selection::Plot2dViewerType( const int ind ) const
{
  QString viewerType;
  SUIT_Session* session = SUIT_Session::session();
  if(  SUIT_Application* app = session->activeApplication() )
    if( LightApp_Application* sApp = dynamic_cast<LightApp_Application*>( app ) )
      if( SUIT_ViewManager* vman = sApp->activeViewManager() )
        if( SUIT_ViewModel* vmod = vman->getViewModel() ) {
          viewerType = vmod->getType();
          if (viewerType ==SPlot2d_Viewer::Type())
            return true;
        }
  return false;
}

//----------------------------------------------------------------------------
struct TIsValuesLabeled : TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    return theActor && theActor->GetValuesLabeled() ? "true" : "false";
  }
};

QString VisuGUI_Selection::isValuesLabeled( const int ind ) const
{
  return TPopupDispatcher<TIsValuesLabeled>()(myModule, entry(ind));
}

//----------------------------------------------------------------------------
struct TIsScalarBarVisibleFunctor: TViewFunctor
{
  QString
  virtual
  get(VISU::Prs3d_i* thePrs3d,
      SVTK_ViewWindow* theViewWindow,
      VISU_Actor* theActor)
  {
    if(VISU_ScalarMapAct* anActor = dynamic_cast<VISU_ScalarMapAct*>(theActor))
      return anActor->GetBarVisibility() ? "true" : "false";
    else if ( VISU_GaussPtsAct* anActor = dynamic_cast<VISU_GaussPtsAct*>(theActor))
      return anActor->GetBarVisibility() ? "true" : "false";
    return QString();
  }
};

bool VisuGUI_Selection::isScalarBarVisible( const int ind ) const
{
  return TPopupDispatcher<TIsScalarBarVisibleFunctor>()(myModule, entry(ind)) == "true";
}

struct TGetQuadratic2DRepresentation: TViewFunctor
{
  QString virtual get (VISU::Prs3d_i* thePrs3d,
                       SVTK_ViewWindow* theViewWindow,
                       VISU_Actor* theActor)
  {
    if(theActor->GetQuadratic2DRepresentation() == VISU_Actor::eLines)
      return "VISU::LINES";
    else if (theActor->GetQuadratic2DRepresentation() == VISU_Actor::eArcs)
      return "VISU::ARCS";
    
    return QString();
  }
};


QString VisuGUI_Selection::quadratic2DMode( const int ind) const
{
  return TPopupDispatcher<TGetQuadratic2DRepresentation>()(myModule, entry(ind));
}


bool VisuGUI_Selection::hasDeviation(const int ind) const {
  SalomeApp_Study* aSStudy = GetStudy();
  if ( !aSStudy )
    return false;

  _PTR(Study) aStudy = GetCStudy( aSStudy );
  if ( aStudy ) {
    _PTR(SObject) SO = aStudy->FindObjectID( (const char*) entry( ind ).toLatin1() );
    if ( SO ) {
      CORBA::Object_var anObj = VISU::ClientSObjectToObject( SO );
      if( !CORBA::is_nil( anObj )) {
        if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>( VISU::GetServant( anObj ).in()) ) {
          return aCurve->hasDeviation();
        }
      }
    }
  }
  return false;
}

bool VisuGUI_Selection::isDeviationDisplayed(const int ind) const {
  SalomeApp_Study* aSStudy = GetStudy();
  if ( !aSStudy )
    return false;

  _PTR(Study) aStudy = GetCStudy( aSStudy );
  if ( aStudy ) {
    _PTR(SObject) SO = aStudy->FindObjectID( (const char*) entry( ind ).toLatin1() );
    if ( SO ) {
      CORBA::Object_var anObj = VISU::ClientSObjectToObject( SO );
      if( !CORBA::is_nil( anObj )) {
        if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>( VISU::GetServant( anObj ).in()) ) {
          return aCurve->isDeviationShown();
        }
      }
    }
  }
  return false;
}

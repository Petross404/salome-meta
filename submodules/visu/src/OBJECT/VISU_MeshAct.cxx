// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MeshAct.cxx
//  Author : 
//  Module : VISU
//  $Header$
//
#include "VISU_MeshAct.h"

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkTexture.h>

#include <vtkFeatureEdges.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSet.h>
#include <vtkMatrix4x4.h>
#include <vtkMapperCollection.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_MeshAct);


//----------------------------------------------------------------------------
VISU_MeshAct
::VISU_MeshAct()
{
  vtkMatrix4x4 *m;
  m = vtkMatrix4x4::New();

  mySurfaceActor = SVTK_DeviceActor::New();
  mySurfaceActor->SetRepresentation(VTKViewer::Representation::Surface);
  mySurfaceActor->SetUserMatrix(m);
  mySurfaceActor->SetCoincident3DAllowed(true);

  myEdgeActor = SVTK_DeviceActor::New();
  myEdgeActor->SetRepresentation(VTKViewer::Representation::Wireframe);
  myEdgeActor->SetUserMatrix(m);
  myEdgeActor->SetCoincident3DAllowed(true);

  myNodeActor = SVTK_DeviceActor::New();
  myNodeActor->SetRepresentation(VTKViewer::Representation::Points);
  myNodeActor->SetUserMatrix(m);

  m->Delete();
  SetRepresentation(VTKViewer::Representation::Surface);

  SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();
  //Quadratic 2D elements representation
  if(aResourceMgr) {
    //----------------------------------------------------------------------------
    int aQuadraticAngle = aResourceMgr->integerValue( "VISU", "max_angle", 2);
    mySurfaceActor->SetQuadraticArcAngle(aQuadraticAngle);
    myEdgeActor->SetQuadraticArcAngle(aQuadraticAngle);

    int anElem0DSize = aResourceMgr->integerValue( "VISU", "elem0d_size", 5);
    GetSurfaceProperty()->SetPointSize(anElem0DSize);
  }  
}

VISU_MeshAct
::~VISU_MeshAct()
{
  mySurfaceActor->Delete();
  myEdgeActor->Delete();
  myNodeActor->Delete();
}


//----------------------------------------------------------------------------
void
VISU_MeshAct
::DeepCopy(VISU_Actor *theActor)
{
  if(VISU_MeshAct* anActor = dynamic_cast<VISU_MeshAct*>(theActor)){
    Superclass::DeepCopy(theActor);
    GetSurfaceProperty()->DeepCopy(anActor->GetSurfaceProperty());
    GetEdgeProperty()->DeepCopy(anActor->GetEdgeProperty());
    GetNodeProperty()->DeepCopy(anActor->GetNodeProperty());
  }
}


//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetMapperInput(vtkDataSet* theDataSet)
{
  Superclass::SetMapperInput(theDataSet);

  mySurfaceActor->SetInputData(theDataSet);
  myEdgeActor->SetInputData(theDataSet);
  myNodeActor->SetInputData(theDataSet);
}


//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetTransform(VTKViewer_Transform* theTransform)
{
  Superclass::SetTransform(theTransform);

  mySurfaceActor->SetTransform(theTransform);
  myEdgeActor->SetTransform(theTransform);
  myNodeActor->SetTransform(theTransform);
}


//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetShrinkable(bool theIsShrinkable)
{
  Superclass::SetShrinkable(theIsShrinkable);

  mySurfaceActor->SetShrinkable(theIsShrinkable);
  myEdgeActor->SetShrinkable(theIsShrinkable);
}

void
VISU_MeshAct
::SetShrinkFactor(double theValue)
{
  Superclass::SetShrinkFactor(theValue);

  mySurfaceActor->SetShrinkFactor(theValue);
  myEdgeActor->SetShrinkFactor(theValue);
}


//----------------------------------------------------------------------------
vtkProperty* 
VISU_MeshAct
::GetSurfaceProperty()
{
  return mySurfaceActor->GetProperty();
}

vtkProperty* 
VISU_MeshAct
::GetEdgeProperty()
{
  return myEdgeActor->GetProperty();
}

vtkProperty* 
VISU_MeshAct
::GetNodeProperty()
{
  return myNodeActor->GetProperty();
}

//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetOpacity(double theValue)
{
  GetSurfaceProperty()->SetOpacity(theValue);
}

double
VISU_MeshAct
::GetOpacity()
{
  return GetSurfaceProperty()->GetOpacity();
}

//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetLineWidth(double theLineWidth)
{
  GetEdgeProperty()->SetLineWidth(theLineWidth);
}

double
VISU_MeshAct::GetLineWidth()
{
  return GetEdgeProperty()->GetLineWidth();
}

//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetShrink()
{
  if(myRepresentation == VTK_POINTS)
    return;

  Superclass::SetShrink();

  mySurfaceActor->SetShrink();
  myEdgeActor->SetShrink();
}

void
VISU_MeshAct
::UnShrink()
{
  Superclass::UnShrink();

  mySurfaceActor->UnShrink();
  myEdgeActor->UnShrink();
}

//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed)
{
  Superclass::SetFeatureEdgesAllowed(theIsFeatureEdgesAllowed);

  mySurfaceActor->SetFeatureEdgesAllowed(theIsFeatureEdgesAllowed);
}

void
VISU_MeshAct
::SetFeatureEdgesAngle(double theValue)
{
  Superclass::SetFeatureEdgesAngle(theValue);

  mySurfaceActor->SetFeatureEdgesAngle(theValue);
}

void
VISU_MeshAct
::SetFeatureEdgesFlags(bool theIsFeatureEdges,
                       bool theIsBoundaryEdges,
                       bool theIsManifoldEdges,
                       bool theIsNonManifoldEdges)
{
  Superclass::SetFeatureEdgesFlags(theIsFeatureEdges,
                                   theIsBoundaryEdges,
                                   theIsManifoldEdges,
                                   theIsNonManifoldEdges);

  mySurfaceActor->SetFeatureEdgesFlags(theIsFeatureEdges,
                                       theIsBoundaryEdges,
                                       theIsManifoldEdges,
                                       theIsNonManifoldEdges);
}

void
VISU_MeshAct
::SetFeatureEdgesColoring(bool theIsColoring)
{
  Superclass::SetFeatureEdgesColoring(theIsColoring);

  mySurfaceActor->SetFeatureEdgesColoring(theIsColoring);
}

//----------------------------------------------------------------------------
void
VISU_MeshAct
::SetFeatureEdgesEnabled(bool theIsFeatureEdgesEnabled)
{
  if(theIsFeatureEdgesEnabled && myRepresentation == VTK_POINTS)
    return;

  Superclass::SetFeatureEdgesEnabled(theIsFeatureEdgesEnabled);

  mySurfaceActor->SetFeatureEdgesEnabled(theIsFeatureEdgesEnabled);
}

//----------------------------------------------------------------------------
void 
VISU_MeshAct
::SetRepresentation(int theMode)
{
  Superclass::SetRepresentation(theMode);

  if(theMode == VTKViewer::Representation::Insideframe)
    myEdgeActor->SetRepresentation(VTKViewer::Representation::Insideframe);
  else
    myEdgeActor->SetRepresentation(VTKViewer::Representation::Wireframe);
}

//----------------------------------------------------------------------------
unsigned long int
VISU_MeshAct
::GetMemorySize()
{
  vtkDataSet* aDataSet;
  unsigned long int aSize = Superclass::GetMemorySize();
  {
    aDataSet = mySurfaceActor->GetInput();
    aSize += aDataSet->GetActualMemorySize() * 1024;

    aDataSet = mySurfaceActor->GetMapper()->GetInput();
    aSize += aDataSet->GetActualMemorySize() * 1024 * 2;
  }
  {
    aDataSet = myEdgeActor->GetInput();
    aSize += aDataSet->GetActualMemorySize() * 1024;

    aDataSet = myEdgeActor->GetMapper()->GetInput();
    aSize += aDataSet->GetActualMemorySize() * 1024 * 2;
  }
  {
    aDataSet = myNodeActor->GetInput();
    aSize += aDataSet->GetActualMemorySize() * 1024;

    aDataSet = myNodeActor->GetMapper()->GetInput();
    aSize += aDataSet->GetActualMemorySize() * 1024 * 2;
  }
  return aSize;
}

//----------------------------------------------------------------------------
int
VISU_MeshAct
::RenderOpaqueGeometry(vtkViewport *ren)
{
  GetMatrix(myNodeActor->GetUserMatrix());
  GetMatrix(myEdgeActor->GetUserMatrix());
  GetMatrix(mySurfaceActor->GetUserMatrix());

  using namespace VTKViewer::Representation;
  switch(GetRepresentation()){
  case Points : 
    myNodeActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
    myNodeActor->RenderOpaqueGeometry(ren);
    break;
  case Wireframe : 
  case Insideframe : 
    myEdgeActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
    myEdgeActor->RenderOpaqueGeometry(ren);
    break;
  case Surface : 
  case FeatureEdges :
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
    mySurfaceActor->RenderOpaqueGeometry(ren);
    break;
  case Surfaceframe : 
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime/2.0,ren);
    mySurfaceActor->RenderOpaqueGeometry(ren);

    myEdgeActor->SetAllocatedRenderTime(this->AllocatedRenderTime/2.0,ren);
    myEdgeActor->RenderOpaqueGeometry(ren);
    break;
  }
  return 1;
}

int
VISU_MeshAct
#if (VTK_XVERSION < 0x050100)
::RenderTranslucentGeometry(vtkViewport *ren)
#else
::RenderTranslucentPolygonalGeometry(vtkViewport *ren)
#endif
{
  GetMatrix(myNodeActor->GetUserMatrix());
  GetMatrix(myEdgeActor->GetUserMatrix());
  GetMatrix(mySurfaceActor->GetUserMatrix());

  using namespace VTKViewer::Representation;
  switch(GetRepresentation()){
  case Points : 
    myNodeActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
    myNodeActor->RenderTranslucentGeometry(ren);
#else
    myNodeActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    break;
  case Wireframe : 
  case Insideframe : 
    myEdgeActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
    myEdgeActor->RenderTranslucentGeometry(ren);
#else
    myEdgeActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    break;
  case Surface : 
  case FeatureEdges :
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
    mySurfaceActor->RenderTranslucentGeometry(ren);
#else
    mySurfaceActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    break;
  case Surfaceframe : 
    mySurfaceActor->SetAllocatedRenderTime(this->AllocatedRenderTime,ren);
#if (VTK_XVERSION < 0x050100)
    mySurfaceActor->RenderTranslucentGeometry(ren);
#else
    mySurfaceActor->RenderTranslucentPolygonalGeometry(ren);
#endif

    myEdgeActor->SetAllocatedRenderTime(this->AllocatedRenderTime/2.0,ren);
#if (VTK_XVERSION < 0x050100)
    myEdgeActor->RenderTranslucentGeometry(ren);
#else
    myEdgeActor->RenderTranslucentPolygonalGeometry(ren);
#endif
    break;
  }
  return 1;
}

#if (VTK_XVERSION >= 0x050100)
int
VISU_MeshAct
::HasTranslucentPolygonalGeometry()
{
  int result = 0; 

  using namespace VTKViewer::Representation;
  switch ( GetRepresentation() ) {
  case Points:
    result |= myNodeActor->HasTranslucentPolygonalGeometry();
    break;
  case Wireframe: 
  case Insideframe: 
    result |= myEdgeActor->HasTranslucentPolygonalGeometry();
    break;
  case Surface:
  case FeatureEdges:
    result |= mySurfaceActor->HasTranslucentPolygonalGeometry();
    break;
  case Surfaceframe:
    result |= mySurfaceActor->HasTranslucentPolygonalGeometry();
    result |= myEdgeActor->HasTranslucentPolygonalGeometry();
    break;
  default:
    break;
  }

  return result;
}
#endif

VISU_Actor::EQuadratic2DRepresentation 
VISU_MeshAct::GetQuadratic2DRepresentation() const
{
  bool mode = (mySurfaceActor->GetQuadraticArcMode() && myEdgeActor->GetQuadraticArcMode());
  if(mode){
    return VISU_Actor::eArcs;
  }
  else
    return VISU_Actor::eLines;
}

void VISU_MeshAct::SetQuadratic2DRepresentation( EQuadratic2DRepresentation theMode )
{
  Superclass::SetQuadratic2DRepresentation( theMode );
  switch(theMode) {
  case VISU_Actor::eArcs:
    mySurfaceActor->SetQuadraticArcMode(true);
    myEdgeActor->SetQuadraticArcMode(true);
    break;
  case VISU_Actor::eLines:
    mySurfaceActor->SetQuadraticArcMode(false);
    myEdgeActor->SetQuadraticArcMode(false);
    break;
  default:
    break;
  }    
}

void VISU_MeshAct::SetMarkerStd( VTK::MarkerType theMarkerType, VTK::MarkerScale theMarkerScale )
{
  Superclass::SetMarkerStd( theMarkerType, theMarkerScale );
  myNodeActor->SetMarkerStd( theMarkerType, theMarkerScale );
}

void VISU_MeshAct::SetMarkerTexture( int theMarkerId, VTK::MarkerTexture theMarkerTexture )
{
  Superclass::SetMarkerTexture( theMarkerId, theMarkerTexture );
  myNodeActor->SetMarkerTexture( theMarkerId, theMarkerTexture );
}

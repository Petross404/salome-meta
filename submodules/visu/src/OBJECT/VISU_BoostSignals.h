// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header$
//
#ifndef VISU_BOOST_SIGNALS_H
#define VISU_BOOST_SIGNALS_H

#ifdef signals
#  ifndef QOBJECTDEFS_H
#    error "Cannot recover the siganlas macro definition"
#  else
#    define EXTERNAL_SIGNALS_DEFINITION
#    undef signals
#  endif
#endif

#include <boost/signals.hpp>

namespace boost {
  namespace signalslib = BOOST_SIGNALS_NAMESPACE;
}

#ifdef EXTERNAL_SIGNALS_DEFINITION
#  undef EXTERNAL_SIGNALS_DEFINITION
#  ifdef QT_MOC_CPP
#    define signals signals
#  else
#    define signals protected
#  endif
#endif

#endif //VISU_BOOST_SIGNALS_H

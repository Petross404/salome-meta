// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_VectorsAct.cxx
//  Author : Laurent CORNABE with help of Nicolas REJNERI
//  Module : VISU
//
#include "VISU_VectorsAct.h"
#include "VISU_VectorsPL.hxx"
#include <SVTK_Actor.h>
 
// VTK Includes
#include <vtkObjectFactory.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyData.h>
#include <vtkPassThroughFilter.h>
#include <VISU_UsedPointsFilter.hxx>
#include <vtkUnstructuredGrid.h>
#include <VISU_MergeFilter.hxx>

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_VectorsAct);


//----------------------------------------------------------------------------
VISU_VectorsAct
::VISU_VectorsAct():
  myVectorsPL(NULL)
{}


//----------------------------------------------------------------------------
VISU_VectorsAct
::~VISU_VectorsAct()
{}


//----------------------------------------------------------------------------
void 
VISU_VectorsAct
::SetPipeLine(VISU_PipeLine* thePipeLine) 
{
  myVectorsPL = dynamic_cast<VISU_VectorsPL*>( thePipeLine );

  Superclass::SetPipeLine( myVectorsPL.GetPointer() );
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_VectorsAct
::GetInput()
{
  return GetCurrentPL()->GetOutput();
}


//----------------------------------------------------------------------------
void
VISU_VectorsAct
::SetTransform(VTKViewer_Transform* theTransform)
{
  // To perform only vector's specific action,
  // there is nothing to be done for the Superclass
  myVectorsPL->SetTransform( theTransform );
}


//----------------------------------------------------------------------------
void
VISU_VectorsAct
::SetMapper(vtkMapper* theMapper)
{
  if(theMapper){
    myPassFilter[0]->SetInputConnection(theMapper->GetInputConnection(0, 0));
    
    myPassFilter[1]->SetInputConnection(myPassFilter[0]->GetOutputPort());

    myPassFilter[2]->SetInputConnection(myPassFilter[1]->GetOutputPort());

    myPassFilter[3]->SetInputConnection(myPassFilter[2]->GetOutputPort());

    theMapper->SetInputConnection(myPassFilter[3]->GetOutputPort());
  }

  vtkLODActor::SetMapper(theMapper);
}


//----------------------------------------------------------------------------
unsigned long int
VISU_VectorsAct
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  aSize += GetPipeLine()->GetMemorySize();

  return aSize;
}

//----------------------------------------------------------------------------
vtkDataSet*
VISU_VectorsAct
::GetValLabelsInput()
{
  VISU_VectorsPL* aPL = dynamic_cast<VISU_VectorsPL*>( GetPipeLine() );
  if ( aPL )
  {
    return aPL->GetMergedInput();
    /*VISU_UsedPointsFilter* aFilter = aPL->GetUsedPointsFilter();
    vtkSmartPointer< VISU_MergeFilter > aFilter = aPL->myMergeFilter;
    if ( aFilter )
    {
      aFilter->Update();
      return aFilter->GetOutput();
    }*/
  }

  return Superclass::GetValLabelsInput();
}

//--------------------------------------------------------------------------------------
vtkDataSet* VISU_VectorsAct::GetHighlightedDataSet() {
  return myPassFilter[3]->GetOutput();
}

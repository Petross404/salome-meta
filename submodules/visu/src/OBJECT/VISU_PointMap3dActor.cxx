// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header$
//
#include "VISU_PointMap3dActor.h"
#include "VISU_PipeLine.hxx"
#include "VISU_ScalarBarActor.hxx"
#include "VISU_DeformedGridPL.hxx"

#include <SALOME_InteractiveObject.hxx>

#include "utilities.h"
#include <vtkRenderer.h>
#include <vtkObjectFactory.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

vtkStandardNewMacro(VISU_PointMap3dActor);

//==================================================================
// function: Constructor
// purpose :
//==================================================================

VISU_PointMap3dActor
::VISU_PointMap3dActor()
{
  if(MYDEBUG) MESSAGE("VISU_PointMap3dActor::VISU_PointMap3dActor - this = "<<this);
  myScalarBar = VISU_ScalarBarActor::New();
}

//==================================================================
// function: Destructor
// purpose :
//==================================================================

VISU_PointMap3dActor
::~VISU_PointMap3dActor()
{
  if(MYDEBUG) MESSAGE("VISU_PointMap3dActor::~VISU_PointMap3dActor - this = "<<this);
  myScalarBar->Delete();
}

//==================================================================
// function: AddToRender
// purpose :
//==================================================================
void
VISU_PointMap3dActor
::AddToRender(vtkRenderer* theRenderer)
{
  Superclass::AddToRender(theRenderer);
  theRenderer->AddActor2D(myScalarBar);
}

void
VISU_PointMap3dActor
::RemoveFromRender(vtkRenderer* theRenderer)
{
  if(myScalarBar)
    theRenderer->RemoveActor(myScalarBar);

  Superclass::RemoveFromRender(theRenderer);
}

//==================================================================
// function: Set & Get PipeLine
// purpose :
//==================================================================
void
VISU_PointMap3dActor
::SetPipeLine(VISU_DeformedGridPL* thePipeLine)
{
  myPipeLine = thePipeLine;
  SetMapper(thePipeLine->GetMapper());
  myScalarBar->SetLookupTable(thePipeLine->GetBarTable());
}

VISU_DeformedGridPL*
VISU_PointMap3dActor
::GetPipeLine()
{
  return myPipeLine.GetPointer();
}

//==================================================================
// function: Visibility
// purpose :
//==================================================================

void
VISU_PointMap3dActor
::SetVisibility(int theMode)
{
  Superclass::SetVisibility( theMode );

  //  myPointsActor->SetVisibility( theMode );

  if(myScalarBar)
    myScalarBar->SetVisibility(theMode);
}

VISU_ScalarBarActor*
VISU_PointMap3dActor
::GetScalarBar()
{
  return myScalarBar;
}

//==================================================================
// function: SetIO
// purpose :
//==================================================================

void 
VISU_PointMap3dActor
::setIO(const Handle(SALOME_InteractiveObject)& theIO)
{
  Superclass::setIO(theIO); 
  myName = theIO->getName(); 
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SMESH OBJECT : interactive object for SMESH visualization
//  File   : 
//  Author : 
//  Module : 
//  $Header$
//
#ifndef VISU_GAUSS_PTS_DEVICE_ACTOR_H
#define VISU_GAUSS_PTS_DEVICE_ACTOR_H

#include "VISU_OBJECT.h"
#include "VISU_Actor.h"
#include "VTKViewer_GeometryFilter.h"

#include <vtkLODActor.h>
#include <vtkSmartPointer.h>

class VTKViewer_Transform;
class VTKViewer_TransformFilter;

class VISU_OpenGLPointSpriteMapper;
class VISU_GaussPointsPL;

class vtkPassThroughFilter;
class vtkImageData;
class SALOME_ExtractPolyDataGeometry;


//============================================================================
namespace VISU
{
  typedef vtkSmartPointer<vtkImageData> TTextureValue;

  VISU_OBJECT_EXPORT
  TTextureValue
  GetTexture(const std::string& theMainTexture, 
             const std::string& theAlphaTexture);
}


//============================================================================
class VISU_OBJECT_EXPORT VISU_GaussDeviceActorBase: public vtkLODActor
{
 public:
  vtkTypeMacro(VISU_GaussDeviceActorBase, vtkLODActor);
  
  static 
  VISU_GaussDeviceActorBase* 
  New();

  virtual
  void
  Render(vtkRenderer *, vtkMapper *);

  //----------------------------------------------------------------------------
  void
  SetTransform(VTKViewer_Transform* theTransform); 

  //----------------------------------------------------------------------------
  void
  SetPointSpriteMapper(VISU_OpenGLPointSpriteMapper* theMapper) ;

  virtual
  void
  DoMapperShallowCopy( vtkMapper* theMapper,
                       bool theIsCopyInput );

  VISU_OpenGLPointSpriteMapper*
  GetPointSpriteMapper();

  //----------------------------------------------------------------------------
  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  //----------------------------------------------------------------------------
  void SetExtractor(SALOME_ExtractPolyDataGeometry* theExtractor) 
    { myPolyDataExtractor = theExtractor; }

  SALOME_ExtractPolyDataGeometry* GetExtractor() const
    { return myPolyDataExtractor; }
 
 protected:
  //----------------------------------------------------------------------------
  vtkSmartPointer<VISU_OpenGLPointSpriteMapper> myMapper;
  vtkSmartPointer<VTKViewer_TransformFilter> myTransformFilter;

  typedef vtkSmartPointer<vtkPassThroughFilter> PPassThroughFilter;
  std::vector<PPassThroughFilter> myPassFilter;

  SALOME_ExtractPolyDataGeometry* myPolyDataExtractor;

  VISU_GaussDeviceActorBase();
  ~VISU_GaussDeviceActorBase();

 private:
  VISU_GaussDeviceActorBase(const VISU_GaussDeviceActorBase&); // Not implemented
  void operator=(const VISU_GaussDeviceActorBase&); // Not implemented
};


//============================================================================
class VISU_GaussPtsDeviceActor: public VISU_GaussDeviceActorBase
{
 public:
  vtkTypeMacro(VISU_GaussPtsDeviceActor, VISU_GaussDeviceActorBase);

  static 
  VISU_GaussPtsDeviceActor* 
  New();

  //----------------------------------------------------------------------------
  void
  AddToRender(vtkRenderer* theRenderer); 

  void
  RemoveFromRender(vtkRenderer* theRenderer);

  //----------------------------------------------------------------------------
  VISU_GaussPointsPL* 
  GetPipeLine();

  void
  SetPipeLine(VISU_GaussPointsPL* thePipeLine) ;

  void
  ShallowCopyPL(VISU_GaussPointsPL* thePipeLine);

  virtual
  int
  GetPickable();

  //----------------------------------------------------------------------------
  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();
 
 protected:
  //----------------------------------------------------------------------------
  vtkSmartPointer<VISU_GaussPointsPL> myPipeLine;

  VISU_GaussPtsDeviceActor();
  ~VISU_GaussPtsDeviceActor();

 private:
  VISU_GaussPtsDeviceActor(const VISU_GaussPtsDeviceActor&); // Not implemented
  void operator=(const VISU_GaussPtsDeviceActor&); // Not implemented
};


//============================================================================
class vtkActor;
class vtkConeSource;
class vtkAppendPolyData;
class vtkPolyDataMapper;

#include <vtkLODActor.h>

class VISU_CursorPyramid : public vtkLODActor 
{
public:
  vtkTypeMacro(VISU_CursorPyramid, vtkObject);

  static
  VISU_CursorPyramid* 
  New();

  virtual
  void
  Render(vtkRenderer *, vtkMapper *);

  void
  AddToRender(vtkRenderer* theRenderer);

  void
  RemoveFromRender(vtkRenderer* theRenderer);

  void
  Init(double theHeight,
       double theCursorSize,
       double theRadius,
       double theMagnification,
       double theClamp,
       double thePos[3],
       double theColor[3]);

  void SetPreferences(double theHeight,
                      double theCursorSize);

protected:
  VISU_CursorPyramid();

  void
  Init(double theHeight,
       double theRadius);

  int myNbCones;
  vtkSmartPointer<vtkConeSource> mySources[6];
  vtkSmartPointer<vtkAppendPolyData> myAppendFilter;
  vtkSmartPointer<vtkPolyDataMapper> myMapper;
  //
  double myHeight;
  double myCursorSize;
  double myRadius;
  double myMagnification;
  double myClamp;

 private:
  VISU_CursorPyramid(const VISU_CursorPyramid&); // Not implemented
  void operator=(const VISU_CursorPyramid&); // Not implemented
};


#endif //VISU_GAUSS_PTS_DEVICE_ACTOR_H

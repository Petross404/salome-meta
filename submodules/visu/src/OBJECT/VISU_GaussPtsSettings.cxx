// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header$
//
#include "VISU_GaussPtsSettings.h"
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include <vtkObjectFactory.h>
#include <vtkImageData.h>

#include <QColor>

//----------------------------------------------------------------
vtkStandardNewMacro( VISU_GaussPtsSettings );
vtkStandardNewMacro( VISU_InsideCursorSettings );
vtkStandardNewMacro( VISU_OutsideCursorSettings );

//----------------------------------------------------------------
VISU_GaussPtsSettings::VISU_GaussPtsSettings()
{
  this->Initial                = true;

  this->PrimitiveType          = -1;
  this->Clamp                  = -1;
  this->Texture                = NULL;
  this->AlphaThreshold         = -1;
  this->Resolution             = -1;
  this->Magnification          = -1;
  this->Increment              = -1;
}

VISU_GaussPtsSettings::~VISU_GaussPtsSettings()
{
  this->SetTexture( NULL );
}

//----------------------------------------------------------------
VISU_InsideCursorSettings::VISU_InsideCursorSettings() :
  VISU_GaussPtsSettings()
{
  this->MinSize                = -1;
  this->MaxSize                = -1;
}

VISU_InsideCursorSettings::~VISU_InsideCursorSettings()
{
}

//----------------------------------------------------------------
VISU_OutsideCursorSettings::VISU_OutsideCursorSettings() :
  VISU_GaussPtsSettings()
{
  this->Size                   = -1;
  this->Uniform                = false;
  this->Color[0]               = -1;
  this->Color[1]               = -1;
  this->Color[2]               = -1;
}

VISU_OutsideCursorSettings::~VISU_OutsideCursorSettings()
{
}

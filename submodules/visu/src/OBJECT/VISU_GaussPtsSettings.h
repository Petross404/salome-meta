// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header$
//
#ifndef VISU_GaussPtsSettings_HeaderFile
#define VISU_GaussPtsSettings_HeaderFile

#include "VISU_OBJECT.h"

#include <vtkObject.h>
#include <vtkCommand.h>

#include "VTKViewer.h"

class vtkImageData;

#include "VISU_Actor.h"

//============================================================================
namespace VISU
{
  const vtkIdType UpdateFromSettingsEvent        = vtkCommand::UserEvent + 100; 
  const vtkIdType UpdateInsideSettingsEvent      = vtkCommand::UserEvent + 101; 
  const vtkIdType UpdateOutsideSettingsEvent     = vtkCommand::UserEvent + 102; 
}


//! Base class of Gauss Points settings.
class VISU_OBJECT_EXPORT VISU_GaussPtsSettings : public vtkObject
{
 public:
  vtkTypeMacro( VISU_GaussPtsSettings, vtkObject );

  VISU_GaussPtsSettings();
  virtual ~VISU_GaussPtsSettings();

  static
  VISU_GaussPtsSettings*
  New();

  vtkSetMacro( Initial, bool );
  vtkGetMacro( Initial, bool );

  vtkSetMacro( PrimitiveType, int );
  vtkGetMacro( PrimitiveType, int );

  vtkSetMacro( Clamp, double );
  vtkGetMacro( Clamp, double );

  vtkSetMacro( Texture, vtkImageData* );
  vtkGetMacro( Texture, vtkImageData* );

  vtkSetMacro( AlphaThreshold, double );
  vtkGetMacro( AlphaThreshold, double );

  vtkSetMacro( Resolution, int );
  vtkGetMacro( Resolution, int );

  vtkSetMacro( Magnification, double );
  vtkGetMacro( Magnification, double );

  vtkSetMacro( Increment, double );
  vtkGetMacro( Increment, double );

 protected:
  bool                Initial;

  int                 PrimitiveType;
  double Clamp;
  vtkImageData*       Texture;
  double AlphaThreshold;
  int                 Resolution;
  double Magnification;
  double Increment;
};


//! Class of Inside Cursor Gauss Points settings.
/*!
 * Contains information about the point sprite parameters:
 * Clamp, Texture, Alpha threshold, Const size and Color.
 * Used by Gauss Points Actor.
 */
class VISU_OBJECT_EXPORT VISU_InsideCursorSettings : public VISU_GaussPtsSettings
{
 public:
  vtkTypeMacro( VISU_InsideCursorSettings, vtkObject );

  VISU_InsideCursorSettings();
  virtual ~VISU_InsideCursorSettings();

  static
  VISU_InsideCursorSettings*
  New();

  vtkSetMacro( MinSize, double );
  vtkGetMacro( MinSize, double );

  vtkSetMacro( MaxSize, double );
  vtkGetMacro( MaxSize, double );

 protected:
  double MinSize;
  double MaxSize;
};


//============================================================================
//! Class of Outside Cursor Gauss Points settings.
/*!
 * Contains information about the point sprite parameters:
 * Clamp, Texture, Alpha threshold, Const size and Color.
 * Used by Gauss Points Actor.
 */
class VISU_OBJECT_EXPORT VISU_OutsideCursorSettings : public VISU_GaussPtsSettings
{
 public:
  vtkTypeMacro( VISU_OutsideCursorSettings, vtkObject );

  VISU_OutsideCursorSettings();
  virtual ~VISU_OutsideCursorSettings();

  static
  VISU_OutsideCursorSettings*
  New();

  vtkSetMacro( Size, double );
  vtkGetMacro( Size, double );

  vtkSetMacro( Uniform, bool );
  vtkGetMacro( Uniform, bool );

  vtkSetVector3Macro( Color, double );
  vtkGetVector3Macro( Color, double );

 protected:
  double Size;
  bool                Uniform;
  double Color[3];
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_IsoSurfActor.cxx
//  Author : Vitaly Smetannikov
//  Module : VISU
//
#include "VISU_IsoSurfActor.h"
#include "VISU_PipeLine.hxx"
#include "VISU_LabelPointsFilter.hxx"


#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkActor2D.h>
//#include <vtkMaskPoints.h>
#include <vtkLabeledDataMapper.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>



//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_IsoSurfActor);


//----------------------------------------------------------------------------
VISU_IsoSurfActor::VISU_IsoSurfActor():
  VISU_ScalarMapAct(),
  myIsLinesLabeled(true)
{
  myLabelsDataSet = vtkPolyData::New();

//   myMaskPoints = vtkMaskPoints::New();
//   myMaskPoints->SetInput(myLabelsDataSet);
//   myMaskPoints->SetOnRatio(100);

  myMaskPoints = VISU_LabelPointsFilter::New();
  myMaskPoints->SetInputData(myLabelsDataSet);

  myLabeledDataMapper = vtkLabeledDataMapper::New();
  myLabeledDataMapper->SetInputConnection(myMaskPoints->GetOutputPort());
  //myLabeledDataMapper->SetLabelFormat("%e");
  myLabeledDataMapper->SetLabelModeToLabelScalars();
    
  myLabels = vtkActor2D::New();
  myLabels->SetMapper(myLabeledDataMapper);
  myLabels->GetProperty()->SetColor(1,1,1);
  myLabels->SetVisibility(myIsLinesLabeled);
}


//----------------------------------------------------------------------------
VISU_IsoSurfActor::~VISU_IsoSurfActor()
{
  myLabelsDataSet->Delete();
  myMaskPoints->Delete();
  myLabeledDataMapper->Delete();
  myLabels->Delete();
}

//----------------------------------------------------------------------------
void VISU_IsoSurfActor::SetLinesLabeled(bool theIsLinesLabeled, int theNbLbl)
{
  myIsLinesLabeled = theIsLinesLabeled;
  myMaskPoints->SetPointsNb(theNbLbl);
  Modified();
}
 
//----------------------------------------------------------------------------
int VISU_IsoSurfActor::GetNbLabels() const 
{ 
  return myMaskPoints->GetPointsNb(); 
}


//----------------------------------------------------------------------------
void VISU_IsoSurfActor::UpdateLabels()
{
  if (myIsLinesLabeled) {
    vtkDataSet* aDataSet = GetPipeLine()->GetOutput();
    if (aDataSet != NULL) {
      unsigned long aTime = myLabelsDataSet->GetMTime();
      unsigned long anObjTime = GetMTime();
      if (aTime < anObjTime) {
        myLabelsDataSet->ShallowCopy(aDataSet);
        myLabelsDataSet->Modified();
      }
    }
  }
  myLabels->SetVisibility(myIsLinesLabeled);
}


//----------------------------------------------------------------------------
void VISU_IsoSurfActor::AddToRender(vtkRenderer* theRenderer)
{
  VISU_ScalarMapAct::AddToRender(theRenderer);
  theRenderer->AddActor2D(myLabels);
}

//----------------------------------------------------------------------------
void VISU_IsoSurfActor::RemoveFromRender(vtkRenderer* theRenderer)
{
  theRenderer->RemoveActor(myLabels);
  VISU_ScalarMapAct::RemoveFromRender(theRenderer);
}

//From vtkFollower
int VISU_IsoSurfActor::RenderOpaqueGeometry(vtkViewport *vp)
{
  UpdateLabels();
  return VISU_ScalarMapAct::RenderOpaqueGeometry(vp);
}

#if (VTK_XVERSION < 0x050100)
int VISU_IsoSurfActor::RenderTranslucentGeometry(vtkViewport *vp)
#else
int VISU_IsoSurfActor::RenderTranslucentPolygonalGeometry(vtkViewport *vp)
#endif
{
  UpdateLabels();
#if (VTK_XVERSION < 0x050100)
  return VISU_ScalarMapAct::RenderTranslucentGeometry(vp);
#else
  return VISU_ScalarMapAct::RenderTranslucentPolygonalGeometry(vp);
#endif
}

void VISU_IsoSurfActor::SetVisibility(int theMode){
  VISU_ScalarMapAct::SetVisibility(theMode);
  if(GetVisibility())
    myLabels->VisibilityOn();
  else
    myLabels->VisibilityOff();
  Modified();
}


///!!!! For test purposes only
// void VISU_IsoSurfActor::SetMapperInput(vtkDataSet* theDataSet)
// {
//   VISU_ScalarMapAct::SetMapperInput(theDataSet);

//   vtkFeatureEdges* aFilter = vtkFeatureEdges::New();
//   aFilter->SetInput(VISU_ScalarMapAct::GetInput());
//   SetInput(aFilter->GetOutput());
// }

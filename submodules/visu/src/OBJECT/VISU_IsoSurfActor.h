// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_IsoSurfActor.h
//  Author : Vitaly Smetannikov
//  Module : VISU
//
#ifndef VISU_VectorsAct_HeaderFile
#define VISU_VectorsAct_HeaderFile

#include "VISU_OBJECT.h"
#include "VISU_ScalarMapAct.h"

class vtkActor2D;
//class vtkMaskPoints;
class vtkLabeledDataMapper;
class vtkPolyData;
class VISU_LabelPointsFilter;


class VISU_OBJECT_EXPORT VISU_IsoSurfActor : public VISU_ScalarMapAct 
{
 public:
  vtkTypeMacro(VISU_IsoSurfActor, VISU_ScalarMapAct);
  
  static VISU_IsoSurfActor* New();
  
  
  //----------------------------------------------------------------------------
  virtual void AddToRender( vtkRenderer* ); 
  
  virtual void RemoveFromRender( vtkRenderer* );

  virtual int RenderOpaqueGeometry(vtkViewport *viewport);
#if (VTK_XVERSION < 0x050100)
  virtual int RenderTranslucentGeometry(vtkViewport *viewport);
#else
  virtual int RenderTranslucentPolygonalGeometry(vtkViewport *viewport);
#endif
  
  //virtual void SetPipeLine(VISU_PipeLine* thePipeLine);

  void SetLinesLabeled(bool theIsLinesLabeled, int theNbLbl);
  bool GetLinesLabeled() const { return myIsLinesLabeled;}
   
  int GetNbLabels() const;


  //----------------------------------------------------------------------------
  //! Visibility management
  virtual void SetVisibility( int );


 protected:

  VISU_IsoSurfActor();
  ~VISU_IsoSurfActor();

  void UpdateLabels();

  vtkActor2D *myLabels;
  vtkLabeledDataMapper* myLabeledDataMapper;
  //vtkMaskPoints* myMaskPoints;
  VISU_LabelPointsFilter* myMaskPoints;
  vtkPolyData* myLabelsDataSet;

  bool myIsLinesLabeled;
  //int myNbLabels;
};


#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PickingSettings.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_PickingSettings.h"

#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include <vtkObjectFactory.h>

vtkStandardNewMacro( VISU_PickingSettings );

VISU_PickingSettings* VISU_PickingSettings::Get()
{
  static VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::New();

  if( aPickingSettings->GetIsInitial() )
  {
    SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();

    aPickingSettings->SetCursorSize( aResourceMgr->doubleValue( "VISU", "picking_cursor_size", 0.5 ) );
    aPickingSettings->SetPyramidHeight( aResourceMgr->doubleValue( "VISU", "picking_pyramid_height", 10.0 ) );
    aPickingSettings->SetPointTolerance( aResourceMgr->doubleValue( "VISU", "picking_point_tolerance", 0.1 ) );
    aPickingSettings->SetInfoWindowEnabled( aResourceMgr->booleanValue( "VISU", "picking_info_window", false ) );
    aPickingSettings->SetInfoWindowTransparency( aResourceMgr->integerValue( "VISU", "picking_transparency", 50 ) / 100.0 );
    aPickingSettings->SetInfoWindowPosition( aResourceMgr->integerValue( "VISU", "picking_position", VISU_PickingSettings::TopLeftCorner ) );
    aPickingSettings->SetCameraMovementEnabled( aResourceMgr->booleanValue( "VISU", "picking_camera_movement", true ) );
    aPickingSettings->SetZoomFactor( aResourceMgr->doubleValue( "VISU", "picking_zoom_factor", 1.5 ) );
    aPickingSettings->SetStepNumber( aResourceMgr->integerValue( "VISU", "picking_step_number", 10 ) );
    aPickingSettings->SetDisplayParentMesh( aResourceMgr->booleanValue( "VISU", "picking_display_parent_mesh", true ) );

    QColor aSelectionColor = aResourceMgr->colorValue( "VISU", "picking_selection_color", Qt::yellow );
    double aColor[3];
    aColor[0] = aSelectionColor.red() / 255.0;
    aColor[1] = aSelectionColor.green() / 255.0;
    aColor[2] = aSelectionColor.blue() / 255.0;
    aPickingSettings->SetColor( aColor );

    aPickingSettings->SetIsInitial( false );
  }

  return aPickingSettings;
}

VISU_PickingSettings::VISU_PickingSettings()
{
  this->IsInitial              = true;

  this->PyramidHeight          = -1;
  this->CursorSize             = -1;
  this->PointTolerance         = -1;
  this->Color[0]               = -1;
  this->Color[1]               = -1;
  this->Color[2]               = -1;
  this->InfoWindowEnabled      = false;
  this->InfoWindowTransparency = -1;
  this->InfoWindowPosition     = -1;
  this->CameraMovementEnabled  = false;
  this->ZoomFactor             = -1;
  this->StepNumber             = -1;
  this->DisplayParentMesh      = false;
}

VISU_PickingSettings::~VISU_PickingSettings()
{
}

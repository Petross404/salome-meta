// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//  $Header$
//
#ifndef VISU_ACTOR_FACTORY_H
#define VISU_ACTOR_FACTORY_H

#include "VISU_BoostSignals.h"
#include "VISU_ActorBase.h"

class VISU_ActorBase;

//----------------------------------------------------------------------------
namespace VISU 
{ 
  //! This class defines an abstaract interface to manage actors
  /*!
    Actors are created by corresponding presentations and published in the defined view.
    Each actor can be published only into one view but one presentation can have many actors.
    Due to the complexity of the actor presentation interaction the new interface defines common 
    and simply way to manage them properly.
    @note
    This interface inherits from boost::bsignals::trackable in order to provide automatic 
    diconnection from defined signals if the object is destroyed.
  */
  struct TActorFactory: public virtual boost::signalslib::trackable
  {
    //! Just to make this class virtual
    virtual 
    ~TActorFactory()
    {}

    //! Gets know whether the factory instance can be used for actor management or not
    virtual
    bool 
    GetActiveState() = 0;

    //! Return modified time of the factory
    virtual
    unsigned long int 
    GetMTime() = 0;

    //! To update the actor
    virtual 
    void
    UpdateActor(VISU_ActorBase* theActor) = 0;

    //! To unregister the actor
    virtual 
    void
    RemoveActor(VISU_ActorBase* theActor) = 0;
  };
}

#endif //VISU_ACTOR_FACTORY_H

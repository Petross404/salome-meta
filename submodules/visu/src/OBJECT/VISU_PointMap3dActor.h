// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PointMap3dActor.h
//  Author : Dmitry MATVEICHEV with help of Alexey PETROV
//  Module : VISU
//  $Header$
//
#ifndef VISU_PointMap3dActor_HeaderFile
#define VISU_PointMap3dActor_HeaderFile

#include "VISU_OBJECT.h"
#include "SALOME_Actor.h"
#include "VISU_DataSetActor.h"
#include "VISU_BoostSignals.h"

class VISU_ScalarBarActor;

#ifdef _WIN32
#define VTKOCC_EXPORT __declspec (dllexport)
#else
#define VTKOCC_EXPORT VTK_EXPORT
#endif

class VISU_DeformedGridPL;

class VISU_OBJECT_EXPORT VISU_PointMap3dActor : public VISU_ActorBase
{
 public:
  vtkTypeMacro(VISU_PointMap3dActor,SALOME_Actor);

  static
  VISU_PointMap3dActor* 
  New();

  ~VISU_PointMap3dActor();
  
//----------------------------------------------------------------------------
  virtual
    void
    AddToRender( vtkRenderer* ); 
  
  virtual
    void
    RemoveFromRender( vtkRenderer* );
  
  virtual
    void
    SetVisibility(int theMode);
  
  virtual
    VISU_ScalarBarActor* 
    GetScalarBar();

//----------------------------------------------------------------------------
  virtual
  void
  setIO(const Handle(SALOME_InteractiveObject)& theIO);

//----------------------------------------------------------------------------
  virtual
  VISU_DeformedGridPL* 
  GetPipeLine();

  virtual 
  void
  SetPipeLine(VISU_DeformedGridPL* thePipeLine);
  
 protected:
  VISU_PointMap3dActor();
  vtkSmartPointer<VISU_DeformedGridPL> myPipeLine;

  VISU_ScalarBarActor* myScalarBar;
};

#endif

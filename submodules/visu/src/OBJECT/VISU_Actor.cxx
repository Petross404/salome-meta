// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   :
//  Author :
//  Module : VISU
//
#include "VISU_Actor.h"

#include "VISU_ActorFactory.h"
#include "VISU_PickingSettings.h"
#include "VISU_GaussPtsDeviceActor.h"
#include "VISU_SelectVisiblePoints.h"

#include "VISU_PipeLine.hxx"
#include "VISU_UsedPointsFilter.hxx"

#include "SVTK_Actor.h"
#include "SVTK_Event.h"

#include "VTKViewer_CellCenters.h"
#include "VTKViewer_FramedTextActor.h"
#include "VTKViewer_ShrinkFilter.h"
#include "VTKViewer_GeometryFilter.h"

#include "SALOME_InteractiveObject.hxx"

#include "SUIT_Session.h"
#include "SUIT_ResourceMgr.h"

#include <stdexcept>
#include <sstream>
#include <cmath> // to use std::abs( int )

// VTK Includes
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkTextMapper.h>
#include <vtkTextActor.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkCellPicker.h>
#include <vtkCell.h>
#include <vtkPointPicker.h>
#include <vtkPoints.h>
#include <vtkInteractorStyle.h>
#include <vtkDataSet.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPassThroughFilter.h>
#include <vtkFeatureEdges.h>
#include <vtkActor2D.h>
#include <vtkMaskPoints.h>
#include <vtkLabeledDataMapper.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>

#include <vtkShrinkFilter.h>
#include <vtkShrinkPolyData.h>

#include <vtkGeometryFilter.h>
#include <vtkObjectFactory.h>

#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkRenderWindowInteractor.h>

#include <boost/bind.hpp>

#include "utilities.h"

#include "VISU_PipeLineUtils.hxx"

using namespace std;

static int MYVTKDEBUG = 0;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//#define ENABLE_ANNOTATION

//----------------------------------------------------------------------------
//vtkStandardNewMacro(VISU_Actor);

//----------------------------------------------------------------------------
VISU_Actor
::VISU_Actor():
  myEventCallbackCommand(vtkCallbackCommand::New()),
  myPriority(0.0),
  myIsVTKMapping(false),
  myPrs3d(NULL),
  myIsShrunk(false),
  myIsShrinkable(false),
  myShrinkFilter(VTKViewer_ShrinkFilter::New()),
  myAnnotationMapper(vtkTextMapper::New()),
#if (VTK_XVERSION < 0x050100)
  myAnnotationActor(vtkTextActor::New()),
#else
  myAnnotationActor(vtkActor2D::New()),
#endif
  myTextActor(VTKViewer_FramedTextActor::New()),
  myIsFeatureEdgesAllowed(false),
  myIsFeatureEdgesEnabled(false),
  myFeatureEdges(vtkFeatureEdges::New()),
  myLastSelectionMode(ActorSelection),
  myIsSubElementsHighlighted(false)
{
  if(MYDEBUG) MESSAGE("VISU_Actor::VISU_Actor - this = "<<this);

  myShrinkFilter->Delete();

  myStoreMapping = true;

  myShrinkFilter->SetStoreMapping(true);

  myAnnotationMapper->Delete();
  myAnnotationActor->vtkActor2D::SetMapper(myAnnotationMapper.GetPointer());

  myAnnotationActor->Delete();
  myAnnotationActor->SetVisibility(0);

  myTextActor->Delete();
  myTextActor->SetVisibility(false);
  myTextActor->SetPickable(false);

  myFeatureEdges->Delete();

  myEventCallbackCommand->Delete();
  myEventCallbackCommand->SetClientData(this);
  myEventCallbackCommand->SetCallback(VISU_Actor::ProcessEvents);

  if( VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get() )
    aPickingSettings->AddObserver(VISU::UpdatePickingSettingsEvent,
                                  myEventCallbackCommand.GetPointer(),
                                  myPriority);

  //Definition of values labeling pipeline

  myValLblDataSet = vtkUnstructuredGrid::New();

  myValCellCenters = VTKViewer_CellCenters::New();
  myValCellCenters->SetInputData(myValLblDataSet);

  myValUsedPoints = VISU_UsedPointsFilter::New();
  myValUsedPoints->SetInputData(myValLblDataSet);

  myValMaskPoints = vtkMaskPoints::New();
  myValMaskPoints->SetInputConnection(myValCellCenters->GetOutputPort());
  myValMaskPoints->SetOnRatio(1);

  myValSelectVisiblePoints = VISU_SelectVisiblePoints::New();
  myValSelectVisiblePoints->SetInputConnection(myValMaskPoints->GetOutputPort());
  myValSelectVisiblePoints->SelectInvisibleOff();
  myValSelectVisiblePoints->SetTolerance(0.1);

  char aFormat[16] = "%g";
  SUIT_ResourceMgr* aResourceMgr = SUIT_Session::session()->resourceMgr();
  if (aResourceMgr) {
    // san: precision can be negative - this is used by double spin boxes
    int aFloatingPrec = std::abs( aResourceMgr->integerValue("VISU", "visual_data_precision", 6) );
    sprintf(aFormat, "%%.%dg", aFloatingPrec);
    //cout << "$$$ aFormat = " << aFormat << endl;
  }

  myValLabeledDataMapper = vtkLabeledDataMapper::New();
  myValLabeledDataMapper->SetInputConnection(myValSelectVisiblePoints->GetOutputPort());
  //myValLabeledDataMapper->SetLabelFormat("%g");
  //myValLabeledDataMapper->SetLabelFormat("%.20g");
  myValLabeledDataMapper->SetLabelFormat(aFormat);
  myValLabeledDataMapper->SetLabelModeToLabelScalars();

  vtkTextProperty* aClsTextProp = vtkTextProperty::New();
  aClsTextProp->SetFontFamilyToTimes();
  static int aCellsFontSize = 12;
  aClsTextProp->SetFontSize(aCellsFontSize);
  aClsTextProp->SetBold(1);
  aClsTextProp->SetItalic(0);
  aClsTextProp->SetShadow(0);
  myValLabeledDataMapper->SetLabelTextProperty(aClsTextProp);
  aClsTextProp->Delete();

  myIsValLabeled = false;

  myValLabels = vtkActor2D::New();
  myValLabels->SetMapper(myValLabeledDataMapper);
  myValLabels->GetProperty()->SetColor(0,1,0);
  myValLabels->SetVisibility( myIsValLabeled );

  // to allow modification of the reference coordinate in redefined SetPosition() methods
  vtkCoordinate* aValLabelsCoordinate = vtkCoordinate::New();
  myValLabels->GetPositionCoordinate()->SetReferenceCoordinate( aValLabelsCoordinate );
  aValLabelsCoordinate->Delete();
}

//----------------------------------------------------------------------------
void
VISU_Actor
::DeepCopy(VISU_Actor *theActor)
{
  highlight(theActor->isHighlighted());
  SetRepresentation(theActor->GetRepresentation());
  SetShrinkable(theActor->IsShrunkable());
  SetShrinkFactor(theActor->GetShrinkFactor());
  if(theActor->IsShrunk())
    SetShrink();
  else
    UnShrink();

  SetFeatureEdgesAllowed(theActor->IsFeatureEdgesAllowed());
  SetFeatureEdgesEnabled(theActor->IsFeatureEdgesEnabled());
  SetFeatureEdgesAngle(theActor->GetFeatureEdgesAngle());
  bool aFeatureEdges = false, aBoundaryEdges = false, aManifoldEdges = false, aNonManifoldEdges = false;
  theActor->GetFeatureEdgesFlags( aFeatureEdges, aBoundaryEdges, aManifoldEdges, aNonManifoldEdges );
  SetFeatureEdgesFlags( aFeatureEdges, aBoundaryEdges, aManifoldEdges, aNonManifoldEdges );
  SetFeatureEdgesColoring(theActor->GetFeatureEdgesColoring());

  SetOpacity(theActor->GetOpacity());
  SetLineWidth(theActor->GetLineWidth());
  SetPosition(theActor->GetPosition());
}


//----------------------------------------------------------------------------
void
VISU_Actor
::ShallowCopyPL(VISU_PipeLine* thePipeLine)
{
  myPipeLine->ShallowCopy(thePipeLine, true);
  GetMapper()->Update();
}

//----------------------------------------------------------------------------
VISU_Actor
::~VISU_Actor()
{
  // Deleting of values labeling pipeline
  myValLblDataSet->Delete();
  myValLabeledDataMapper->RemoveAllInputs();
  myValLabeledDataMapper->Delete();
  myValSelectVisiblePoints->Delete();
  myValMaskPoints->Delete();
  myValUsedPoints->Delete();
  myValCellCenters->Delete();
  myValLabels->Delete();

  if(MYDEBUG) MESSAGE("~VISU_Actor() - this = "<<this);
  Superclass::SetProperty(NULL);
  SetDebug(MYVTKDEBUG);
}

//----------------------------------------------------------------------------
void
VISU_Actor
::setIO(const Handle(SALOME_InteractiveObject)& theIO)
{
  Superclass::setIO(theIO);
  myName = theIO->getName();
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetPrs3d(VISU::Prs3d_i* thePrs3d)
{
  myPrs3d = thePrs3d;
}

VISU::Prs3d_i*
VISU_Actor
::GetPrs3d()
{
  return myPrs3d;
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetPipeLine(VISU_PipeLine* thePipeLine)
{
  myPipeLine = thePipeLine;
  if(thePipeLine){
    if(vtkMapper *aMapper = myPipeLine->GetMapper()){
      if(vtkDataSet *aDataSet = aMapper->GetInput()){
        SetShrinkable(thePipeLine->IsShrinkable());
        SetFeatureEdgesAllowed(thePipeLine->IsFeatureEdgesAllowed());
        SetMapperInput(aDataSet);
      }
    }
  }
  this->Modified();
}

VISU_PipeLine*
VISU_Actor
::GetPipeLine()
{
  return myPipeLine.GetPointer();
}

VISU_PipeLine*
VISU_Actor
::GetCurrentPL()
{
  return GetPipeLine();
}


//----------------------------------------------------------------------------
void
VISU_Actor
::SetRepresentation(int theMode)
{
  Superclass::SetRepresentation(theMode);
  if(myRepresentation == VTK_POINTS)
  {
    UnShrink();
  }
  SetFeatureEdgesEnabled( theMode == VTKViewer::Representation::FeatureEdges );
}


//----------------------------------------------------------------------------
void
VISU_Actor
::SetShrink()
{
  if(!myIsShrinkable)
    return;
  if(vtkAlgorithmOutput* aDataSet = myPassFilter[0]->GetOutputPort()){
    myShrinkFilter->SetInputConnection(aDataSet);
    myPassFilter[1]->SetInputConnection(myShrinkFilter->GetOutputPort());
    myIsShrunk = true;
  }
}

void
VISU_Actor
::UnShrink()
{
  if(!myIsShrunk)
    return;
  if(vtkAlgorithmOutput* aDataSet = myPassFilter[0]->GetOutputPort()){
    myPassFilter[1]->SetInputConnection(aDataSet);
    myPassFilter[1]->Modified();
    myIsShrunk = false;
    Modified();
  }
}

bool
VISU_Actor
::IsShrunk()
{
  return myIsShrunk;
}

void
VISU_Actor
::SetShrinkable(bool theIsShrinkable)
{
  myIsShrinkable = theIsShrinkable;
}

bool
VISU_Actor
::IsShrunkable()
{
  return myIsShrinkable;
}

void
VISU_Actor
::SetShrinkFactor(double theValue)
{
  myShrinkFilter->SetShrinkFactor(theValue);
  Modified();
}

double
VISU_Actor
::GetShrinkFactor()
{
  return myShrinkFilter->GetShrinkFactor();
}


//----------------------------------------------------------------------------
bool
VISU_Actor
::IsFeatureEdgesAllowed()
{
  return myIsFeatureEdgesAllowed;
}

void
VISU_Actor
::SetFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed)
{
  myIsFeatureEdgesAllowed = theIsFeatureEdgesAllowed;
}

bool
VISU_Actor
::IsFeatureEdgesEnabled()
{
  return myIsFeatureEdgesEnabled;
}

void
VISU_Actor
::SetFeatureEdgesEnabled(bool theIsFeatureEdgesEnabled)
{
  if ( !myIsFeatureEdgesAllowed || myIsFeatureEdgesEnabled == theIsFeatureEdgesEnabled )
    return;

  if ( vtkAlgorithmOutput* aPolyData = myPassFilter[ 2 ]->GetOutputPort() )
  {
    if( theIsFeatureEdgesEnabled )
    {
      myFeatureEdges->SetInputConnection( aPolyData );
      myPassFilter[ 3 ]->SetInputConnection( myFeatureEdges->GetOutputPort() );
      myIsFeatureEdgesEnabled = true;
    }
    else
    {
      myPassFilter[3]->SetInputConnection( aPolyData );
      myPassFilter[3]->Modified();
      myIsFeatureEdgesEnabled = false;
      Modified();
    }
    myIsFeatureEdgesEnabled = theIsFeatureEdgesEnabled;
  }
}

double
VISU_Actor
::GetFeatureEdgesAngle()
{
  return myFeatureEdges->GetFeatureAngle();
}

void
VISU_Actor
::SetFeatureEdgesAngle(double theValue)
{
  myFeatureEdges->SetFeatureAngle(theValue);
  Modified();
}

void
VISU_Actor
::GetFeatureEdgesFlags(bool& theIsFeatureEdges,
                       bool& theIsBoundaryEdges,
                       bool& theIsManifoldEdges,
                       bool& theIsNonManifoldEdges)
{
  theIsFeatureEdges = myFeatureEdges->GetFeatureEdges();
  theIsBoundaryEdges = myFeatureEdges->GetBoundaryEdges();
  theIsManifoldEdges = myFeatureEdges->GetManifoldEdges();
  theIsNonManifoldEdges = myFeatureEdges->GetNonManifoldEdges();
}

void
VISU_Actor
::SetFeatureEdgesFlags(bool theIsFeatureEdges,
                       bool theIsBoundaryEdges,
                       bool theIsManifoldEdges,
                       bool theIsNonManifoldEdges)
{
  myFeatureEdges->SetFeatureEdges(theIsFeatureEdges);
  myFeatureEdges->SetBoundaryEdges(theIsBoundaryEdges);
  myFeatureEdges->SetManifoldEdges(theIsManifoldEdges);
  myFeatureEdges->SetNonManifoldEdges(theIsNonManifoldEdges);
  Modified();
}

bool
VISU_Actor
::GetFeatureEdgesColoring()
{
  return myFeatureEdges->GetColoring();
}

void
VISU_Actor
::SetFeatureEdgesColoring(bool theIsColoring)
{
  myFeatureEdges->SetColoring(theIsColoring);
  Modified();
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetOpacity(double theValue)
{
  GetProperty()->SetOpacity(theValue);
}

double
VISU_Actor
::GetOpacity()
{
  return GetProperty()->GetOpacity();
}

void
VISU_Actor
::SetLineWidth(double theLineWidth)
{
  GetProperty()->SetLineWidth(theLineWidth);
}

double
VISU_Actor
::GetLineWidth()
{
  return GetProperty()->GetLineWidth();
}

//==================================================================
// function: AddToRender
// purpose :
//==================================================================
void
VISU_Actor
::AddToRender(vtkRenderer* theRenderer)
{
  Superclass::AddToRender(theRenderer);
  theRenderer->AddActor(myAnnotationActor.GetPointer());
  theRenderer->AddActor(myTextActor.GetPointer());

  myValSelectVisiblePoints->SetRenderer( theRenderer );
  theRenderer->AddActor2D( myValLabels );

}

//==================================================================
// function: RemoveFromRender
// purpose :
//==================================================================
void
VISU_Actor
::RemoveFromRender(vtkRenderer* theRenderer)
{
  theRenderer->RemoveActor(myAnnotationActor.GetPointer());
  theRenderer->RemoveActor(myTextActor.GetPointer());
  theRenderer->RemoveActor(myValLabels);
  Superclass::RemoveFromRender(theRenderer);
  myDestroySignal(this);
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetVisibility(int theMode)
{
  Superclass::SetVisibility( theMode );
  myValLabels->SetVisibility( myIsValLabeled && theMode );

  // Moved from VISU_GaussPtsAct::SetVisibility() (due to IPAL21159)
  Highlight(isHighlighted());
}
//----------------------------------------------------------------------------
  //! Gets know whether the actor should be displayed or not
bool
VISU_Actor
::ShouldBeDisplayed()
{
  return GetFactory()->GetActiveState();
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetPosition( double thePosition[3] )
{
  Superclass::SetPosition( thePosition );
  if( vtkCoordinate* aCoord = myValLabels->GetPositionCoordinate()->GetReferenceCoordinate() )
    aCoord->SetValue( thePosition );
  myValSelectVisiblePoints->SetOffset( thePosition );
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetPosition( double theX, double theY, double theZ )
{
  Superclass::SetPosition( theX, theY, theZ );
  if( vtkCoordinate* aCoord = myValLabels->GetPositionCoordinate()->GetReferenceCoordinate() )
    aCoord->SetValue( theX, theY, theZ );
  myValSelectVisiblePoints->SetOffset( theX, theY, theZ );
}

//----------------------------------------------------------------------------
void
VISU_Actor
::SetVTKMapping(bool theIsVTKMapping)
{
  myIsVTKMapping = theIsVTKMapping;
}

bool
VISU_Actor
::IsVTKMapping() const
{
  return myIsVTKMapping;
}

//----------------------------------------------------------------------------
vtkDataSet*
VISU_Actor
::GetInput()
{
  if(myIsVTKMapping)
    return Superclass::GetInput();

  return GetCurrentPL()->GetOutput();
}

//----------------------------------------------------------------------------
unsigned long int
VISU_Actor
::GetMemorySize()
{
  static double ERR_SIZE_CALC = 1.00;
  vtkDataSet* aDataSet = GetMapper()->GetInput();
  unsigned long int aSize = size_t(aDataSet->GetActualMemorySize() * 1024 * ERR_SIZE_CALC);

  aDataSet = myGeomFilter->GetOutput();
  aSize += aDataSet->GetActualMemorySize() * 1024;

  if(IsShrunk()){
    aDataSet = myShrinkFilter->GetOutput();
    aSize += aDataSet->GetActualMemorySize() * 1024;
  }

  if(IsFeatureEdgesEnabled()){
    vtkPolyData* aPolyData = myFeatureEdges->GetOutput();
    aSize += aPolyData->GetActualMemorySize() * 1024;
  }

  return aSize;
}

//----------------------------------------------------------------------------
vtkIdType
VISU_Actor
::GetNodeObjId(vtkIdType theID)
{
  if(myIsVTKMapping)
    return Superclass::GetNodeObjId(theID);

  return VISU::GetNodeObjID(GetMapper()->GetInput(), theID);
}

vtkIdType
VISU_Actor
::GetNodeVTKID(vtkIdType theID)
{
  if(myIsVTKMapping)
    return theID;

  return VISU::GetNodeVTKID(GetMapper()->GetInput(), theID);
}

double*
VISU_Actor
::GetNodeCoord(vtkIdType theObjID)
{
  if(myIsVTKMapping)
    return Superclass::GetNodeCoord(theObjID);

  return VISU::GetNodeCoord(GetInput(), theObjID);
}


//----------------------------------------------------------------------------
vtkIdType
VISU_Actor
::GetElemObjId(vtkIdType theID)
{
  if(myIsVTKMapping)
    return Superclass::GetElemObjId(theID);

  return VISU::GetElemObjID(GetMapper()->GetInput(), theID);
}

vtkIdType
VISU_Actor
::GetElemVTKID(vtkIdType theID)
{
  if(myIsVTKMapping)
    return theID;

  return VISU::GetElemVTKID(GetMapper()->GetInput(), theID);
}

vtkCell*
VISU_Actor
::GetElemCell(vtkIdType theObjID)
{
  if(myIsVTKMapping)
    return Superclass::GetElemCell(theObjID);

  return VISU::GetElemCell(GetInput(), theObjID);
}


//----------------------------------------------------------------------------
bool
VISU_Actor
::isSubElementsHighlighted()
{
  return myIsSubElementsHighlighted;
}


//----------------------------------------------------------------------------
inline
void
ChangeZoom(double theZoomFactor,
           vtkRenderer* theRenderer,
           vtkIdType theInitialHasIndex,
           vtkIdType theCurrentHasIndex)
{
  //printf( "VISU_Actor::ChangeZoom( %d, %d )", theInitialHasIndex, theCurrentHasIndex );
  if(theInitialHasIndex + theCurrentHasIndex == 1){
    vtkCamera *aCamera = theRenderer->GetActiveCamera();

    double aScale = aCamera->GetParallelScale();
    if ( !theInitialHasIndex && theCurrentHasIndex ) {
      //printf( " : +%f", theZoomFactor );
      aCamera->SetParallelScale( aScale / theZoomFactor );
    }
    else {
      //printf( " : -%f", theZoomFactor );
      aCamera->SetParallelScale( aScale * theZoomFactor );
    }
  }
  //printf( "\n" );
}

/*!
  Updates visibility of the highlight devices
*/
void
VISU_Actor
::highlight(bool theIsHighlight)
{
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

  bool aShowTextActor = aPickingSettings->GetInfoWindowEnabled();
  if( theIsHighlight && mySelectionMode != myLastSelectionMode )
  {
    if( mySelectionMode == ActorSelection )
      ResetTextActor();
    myLastSelectionMode = mySelectionMode;
  }

  myTextActor->SetVisibility( GetVisibility() && theIsHighlight && aShowTextActor &&
                              ( mySelectionMode == ActorSelection || isSubElementsHighlighted() ) );

  bool anInitialHasIndex = isHighlighted();
  bool aCurrentHasIndex = theIsHighlight;

  if( !theIsHighlight && mySelectionMode == ActorSelection && isSubElementsHighlighted() )
  {
    myIsSubElementsHighlighted = false;

    // Zoom
    if( GetVisibility() && aPickingSettings->GetCameraMovementEnabled() )
    {
      double aZoomFactor = aPickingSettings->GetZoomFactor();
      ChangeZoom(aZoomFactor,
                 GetRenderer(),
                 anInitialHasIndex,
                 aCurrentHasIndex);
    }
  }

  Superclass::highlight(theIsHighlight);
}

/*!
  To process prehighlight (called from SVTK_InteractorStyle)
*/
bool
VISU_Actor
::PreHighlight(vtkInteractorStyle* theInteractorStyle,
               SVTK_SelectionEvent* theSelectionEvent,
               bool theIsHighlight)
{
  bool aRet = Superclass::PreHighlight(theInteractorStyle,
                                       theSelectionEvent,
                                       theIsHighlight);
#ifndef ENABLE_ANNOTATION
  return aRet;
#endif
  //
  myAnnotationActor->SetVisibility(0);
  if(theIsHighlight){
    switch(mySelectionMode){
    case CellSelection:{
      vtkRenderer* aRenderer = theInteractorStyle->GetCurrentRenderer();
      myCellPicker->Pick(theSelectionEvent->myX,
                         theSelectionEvent->myY,
                         0.0,
                         aRenderer);

      if(myCellPicker->GetActor() != this)
        return false;

      vtkIdType aVTKId = myCellPicker->GetCellId();
      if(aVTKId >= 0  && mySelector->IsValid(this,aVTKId,true) && hasIO()){
        vtkIdType anObjId = GetElemObjId(aVTKId);
        if(vtkCell* aCell = GetElemCell(anObjId)){
          vtkPoints* aPts = aCell->GetPoints();
          if(int aNbPts = aCell->GetNumberOfPoints()){
            double aCoord[3] = {0.0, 0.0, 0.0};
            for(int i = 0; i < aNbPts; i++){
              double *aPntCoord = aPts->GetPoint(i);
              aCoord[0] += aPntCoord[0];
              aCoord[1] += aPntCoord[1];
              aCoord[2] += aPntCoord[2];
            }
            // Display coordinates
            double aWorldCoord[4] = {aCoord[0]/aNbPts, aCoord[1]/aNbPts, aCoord[2]/aNbPts, 1.0};
            aRenderer->SetWorldPoint(aWorldCoord);
            aRenderer->WorldToDisplay();
            double aSelectionPoint[3];
            aRenderer->GetDisplayPoint(aSelectionPoint);
            myAnnotationActor->SetPosition(aSelectionPoint);
            //
            // To prepare the annotation text
            std::ostringstream aStr;
            aStr<<"Cell ID: "<< anObjId;
            std::string aString = aStr.str();
            myAnnotationMapper->SetInput(aString.c_str());

            myAnnotationActor->SetVisibility(1);
            return true;
          }
        }
      }
      break;
    }
    case NodeSelection:{
      vtkRenderer* aRenderer = theInteractorStyle->GetCurrentRenderer();
      myPointPicker->Pick(theSelectionEvent->myX,
                          theSelectionEvent->myY,
                          0.0,
                          aRenderer);

      if(myPointPicker->GetActor() != this)
        return false;

      vtkIdType aVtkId = myPointPicker->GetPointId();
      if(aVtkId >= 0  && mySelector->IsValid(this,aVtkId,true) && hasIO()){
        vtkIdType anObjId = GetNodeObjId( aVtkId );
        if(double* aCoord = GetNodeCoord(anObjId)){
          // Display coordinates
          double aWorldCoord[4] = {aCoord[0], aCoord[1], aCoord[2], 1.0};
          aRenderer->SetWorldPoint(aWorldCoord);
          aRenderer->WorldToDisplay();
          double aSelectionPoint[3];
          aRenderer->GetDisplayPoint(aSelectionPoint);
          myAnnotationActor->SetPosition(aSelectionPoint);
          //
          // To prepare the annotation text
          std::ostringstream aStr;
          aStr<<"Node ID: "<< anObjId;
          std::string aString = aStr.str();
          myAnnotationMapper->SetInput(aString.c_str());

          myAnnotationActor->SetVisibility(1);
          return true;
        }
      }
      break;
    }
    case EdgeOfCellSelection:
      break;
    default:
      break;
    }
  }

  return aRet;
}

void VISU_Actor::RemoveAllClippingPlanes()
{
}

vtkIdType VISU_Actor::GetNumberOfClippingPlanes()
{
  return 0;
}

bool VISU_Actor::AddClippingPlane(vtkPlane* thePlane)
{
  return false;
}

vtkPlane* VISU_Actor::GetClippingPlane(vtkIdType theID)
{
  return NULL;
}

vtkImplicitFunctionCollection* VISU_Actor::GetClippingPlanes()
{
  return NULL;
}

//----------------------------------------------------------------------------
template<class TData> std::string getScalar(TData* theData, int theId)
{
  std::ostringstream aStr;
  if (vtkDataArray *aScalar = theData->GetScalars()){
    double aVal = aScalar->GetTuple1(theId);
    aStr << "\nScalar: " << aVal;
  }
  return aStr.str();
}

template<class TData> std::string getVector(TData* theData, int theId)
{
  std::ostringstream aStr;
  if (vtkDataArray *aVector = theData->GetVectors()) {
    double *aVal = aVector->GetTuple3(theId);
    aStr << "\nVector: " << "{" << aVal[0] << "; " << aVal[1] << "; " << aVal[2] << "}";
  }
  return aStr.str();
}

/*!
  To process highlight (called from SVTK_InteractorStyle)
*/
bool
VISU_Actor
::Highlight(vtkInteractorStyle* theInteractorStyle,
            SVTK_SelectionEvent* theSelectionEvent,
            bool theIsHighlight)
{
  return Superclass::Highlight(theInteractorStyle,
                               theSelectionEvent,
                               theIsHighlight);
}

//-------------------------------------------------------------------------
void
VISU_Actor
::Highlight(bool theIsHighlight)
{
  Superclass::Highlight(theIsHighlight);

  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

  bool anInitialHasIndex = isSubElementsHighlighted() && mySelectionMode != ActorSelection;

  TColStd_IndexedMapOfInteger aMapIndex;
  mySelector->GetIndex( getIO(), aMapIndex );
  bool aCurrentHasIndex = aMapIndex.Extent() == 1;

  myIsSubElementsHighlighted = aCurrentHasIndex;

  bool aFlyTo = false;
  double aFlyToCoord[3] = { 0.0, 0.0, 0.0 };
  vtkRenderer *aRenderer = GetRenderer();

  if( theIsHighlight )
  {
    vtkDataSet* aDataSet = GetMapper()->GetInput();
    switch(mySelectionMode)
    {
      case ActorSelection:
      {
        ResetTextActor();
        break;
      }
      case CellSelection:
      {
        if( !aCurrentHasIndex )
        {
          myTextActor->SetVisibility(false);
          break;
        }

        int anObjId = aMapIndex(1);
        vtkCellData* aCellData = aDataSet->GetCellData();
        if(vtkCell* aCell = GetElemCell(anObjId)){
          vtkPoints* aPts = aCell->GetPoints();
          if(int aNbPts = aCell->GetNumberOfPoints()){
            double aCoord[3] = {0.0, 0.0, 0.0};
            for(int i = 0; i < aNbPts; i++){
              double *aPntCoord = aPts->GetPoint(i);
              aCoord[0] += aPntCoord[0];
              aCoord[1] += aPntCoord[1];
              aCoord[2] += aPntCoord[2];
            }

            aFlyTo = true;
            aFlyToCoord[0] = aCoord[0] / aNbPts;
            aFlyToCoord[1] = aCoord[1] / aNbPts;
            aFlyToCoord[2] = aCoord[2] / aNbPts;

            double aWorldCoord[4] = {aCoord[0]/aNbPts, aCoord[1]/aNbPts, aCoord[2]/aNbPts, 1.0};
            aRenderer->SetWorldPoint(aWorldCoord);
            aRenderer->WorldToDisplay();
            double aSelectionPoint[3];
            aRenderer->GetDisplayPoint(aSelectionPoint);
            myTextActor->SetPosition(aSelectionPoint);
            myTextActor->SetModePosition(aPickingSettings->GetInfoWindowPosition());
            myTextActor->SetTransparency(aPickingSettings->GetInfoWindowTransparency());
            myTextActor->SetWorldPoint(aWorldCoord);

            std::ostringstream aStr;
            aStr << "Cell ID: " << anObjId;

            vtkCell* aCell = GetElemCell(anObjId);
            int aVTKID = GetElemVTKID(anObjId);
            if (aCell) {
              int aNbOfPoints = aCell->GetNumberOfPoints();
              if ( aNbOfPoints > 0 ) {
                aStr << getScalar(aCellData, aVTKID);
                aStr << getVector(aCellData, aVTKID);
              }
            }

            std::string aString = aStr.str();
            myTextActor->SetText(aString.c_str());
          }
        }
        break;
      }
      case NodeSelection:
      {
        if( !aCurrentHasIndex )
        {
          myTextActor->SetVisibility(false);
          break;
        }

        int anObjId = aMapIndex(1);
        vtkPointData* aPntData = aDataSet->GetPointData();
        if(double* aCoord = GetNodeCoord(anObjId)){
          aFlyTo = true;
          aFlyToCoord[0] = aCoord[0];
          aFlyToCoord[1] = aCoord[1];
          aFlyToCoord[2] = aCoord[2];

          double aWorldCoord[4] = {aCoord[0], aCoord[1], aCoord[2], 1.0};
          aRenderer->SetWorldPoint(aWorldCoord);
          aRenderer->WorldToDisplay();
          double aSelectionPoint[3];
          aRenderer->GetDisplayPoint(aSelectionPoint);
          myTextActor->SetPosition(aSelectionPoint);
          myTextActor->SetModePosition(aPickingSettings->GetInfoWindowPosition());
          myTextActor->SetTransparency(aPickingSettings->GetInfoWindowTransparency());
          myTextActor->SetWorldPoint(aWorldCoord);

          std::ostringstream aStr;
          aStr << "Point ID: " << anObjId;

          int aVTKID = GetNodeVTKID(anObjId);
          if(aVTKID >= 0) {
            aStr << getScalar(aPntData, aVTKID);
            aStr << getVector(aPntData, aVTKID);
          }

          const VISU::PIDMapper& aMapper = GetPipeLine()->GetIDMapper();
          //VISU::TIdTypeVector aVec = aMapper->GetIndexesOfNode(anObjId);
          VISU::TStructuredId aVec = aMapper->GetIndexesOfNode(anObjId);

          aStr << "\nCoordinates: " << "[";
          aStr << aCoord[0];
          //if( aVec.size() > 0 )
          if (aVec[0] != -1)
            aStr << " (" << aVec[0] << ")";
          aStr << "; ";

          aStr << aCoord[1];
          //if( aVec.size() > 1 )
          if (aVec[1] != -1)
            aStr << " (" << aVec[1] << ")";
          aStr << "; ";

          aStr << aCoord[2];
          //if( aVec.size() > 2 )
          if (aVec[2] != -1)
            aStr << " (" << aVec[2] << ")";
          aStr << "]";

          std::string aString = aStr.str();
          myTextActor->SetText(aString.c_str());
        }
        break;
      }
      case EdgeOfCellSelection:
        break;
      default:
        break;
    }
  }

  // Zoom
  if( GetVisibility() && aPickingSettings->GetCameraMovementEnabled() )
  {
    double aZoomFactor = aPickingSettings->GetZoomFactor();
    ChangeZoom(aZoomFactor,
               GetRenderer(),
               anInitialHasIndex,
               aCurrentHasIndex);
  }

  // FlyTo
  if( GetVisibility() && aPickingSettings->GetCameraMovementEnabled() && aFlyTo )
  {
    vtkRenderWindowInteractor* anInteractor = myInteractor;
    double aDollyWas = anInteractor->GetDolly();
    int aNumberOfFlyFramesWas = anInteractor->GetNumberOfFlyFrames();

    double aPosition[3];
    GetPosition( aPosition );
    for( int i = 0; i < 3; i++ )
      aFlyToCoord[i] += aPosition[i];

    anInteractor->SetDolly(0.0);
    anInteractor->SetNumberOfFlyFrames(aPickingSettings->GetStepNumber());
    anInteractor->FlyTo(aRenderer, aFlyToCoord);
    aRenderer->ResetCameraClippingRange();
    anInteractor->SetDolly(aDollyWas);
    anInteractor->SetNumberOfFlyFrames(aNumberOfFlyFramesWas);
    anInteractor->InvokeEvent(SVTK::ChangeRotationPoint, aFlyToCoord);
  }
}

//-------------------------------------------------------------------------
void
VISU_Actor
::ResetTextActor()
{
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

  double aCoord[6];
  GetBounds(aCoord);

  double aWorldCoord[4] = { ( aCoord[0] + aCoord[1] ) / 2,
                                          ( aCoord[2] + aCoord[3] ) / 2,
                                          ( aCoord[4] + aCoord[5] ) / 2, 1.0};
  vtkRenderer* aRenderer = GetRenderer();
  aRenderer->SetWorldPoint(aWorldCoord);
  aRenderer->WorldToDisplay();
  double aSelectionPoint[3];
  aRenderer->GetDisplayPoint(aSelectionPoint);
  myTextActor->SetPosition(aSelectionPoint);
  myTextActor->SetModePosition(aPickingSettings->GetInfoWindowPosition());
  myTextActor->SetTransparency(aPickingSettings->GetInfoWindowTransparency());
  myTextActor->SetWorldPoint(aWorldCoord);

  std::ostringstream aStr;
  /*
  if( const char* aName = getName() )
    aStr << aName << "\n";
  */
  aStr << "Position: " << "[" << aCoord[0] << "; " << aCoord[2] << "; " << aCoord[4] << "]";
  aStr << "\nSize: " << "[" <<
    fabs(aCoord[1]-aCoord[0]) << "; " <<
    fabs(aCoord[3]-aCoord[2]) << "; " <<
    fabs(aCoord[5]-aCoord[4]) << "]";

  std::string aString = aStr.str();
  myTextActor->SetText(aString.c_str());
}

//-------------------------------------------------------------------------
void
VISU_Actor
::ProcessEvents(vtkObject* vtkNotUsed(theObject),
                unsigned long theEvent,
                void* theClientData,
                void* vtkNotUsed(theCallData))
{
  if(vtkObject* anObject = reinterpret_cast<vtkObject*>(theClientData))
    if(VISU_Actor* self = dynamic_cast<VISU_Actor*>(anObject)) {
      if(theEvent == VISU::UpdatePickingSettingsEvent)
        self->UpdatePickingSettings();
    }
}

//----------------------------------------------------------------------------
void
VISU_Actor
::UpdatePickingSettings()
{
  //printf( "VISU_Actor::UpdatePickingSettings()\n" );
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();
  if( !aPickingSettings )
    return;

  myTextActor->SetModePosition(aPickingSettings->GetInfoWindowPosition());
  myTextActor->SetTransparency(aPickingSettings->GetInfoWindowTransparency());

  Highlight(isHighlighted());

  Update();
}

// ---------------------------------------------------------------

void VISU_Actor::SetValuesLabeled( const bool theIsValLabeled )
{
  vtkDataSet* aGrid = GetValLabelsInput();
  if ( !aGrid )
    return;

  bool isOnPnt = VISU::IsDataOnPoints( aGrid );
  bool isOnCell = VISU::IsDataOnCells( aGrid );
  if ( !isOnPnt && !isOnCell )
  {
    // try to specify location of scalars "manually"
    vtkCellData* aCData = aGrid->GetCellData();
    if ( aCData )
    {
      vtkDataArray* anArr = aCData->GetScalars();
      if ( anArr && anArr->GetNumberOfTuples() )
        isOnCell = true;
    }

    if ( !isOnCell )
    {
      vtkPointData* aPData = aGrid->GetPointData();
      if ( aPData )
      {
        vtkDataArray* anArr = aPData->GetScalars();
        if ( anArr && anArr->GetNumberOfTuples() )
          isOnPnt = true;
      }
    }

    if ( !isOnPnt && !isOnCell )
    {
      myValLabels->SetVisibility( false );
      return;
    }
  }

  myIsValLabeled = theIsValLabeled;

  if ( myIsValLabeled )
  {
    vtkDataSet* aDataSet = aGrid;

    if ( isOnCell )
    {
      myValCellCenters->SetInputData( aDataSet );
      myValMaskPoints->SetInputConnection( myValCellCenters->GetOutputPort() );
    }
    else if ( isOnPnt )
    {
      myValUsedPoints->SetInputData( aDataSet );
      myValMaskPoints->SetInputConnection( myValUsedPoints->GetOutputPort() );
    }

    myValLabels->SetVisibility( GetVisibility() );
  }
  else
    myValLabels->SetVisibility( false );

  Modified();
}

//----------------------------------------------------------------------------

bool VISU_Actor::GetValuesLabeled() const
{
  return myIsValLabeled;
}

//----------------------------------------------------------------------------

vtkTextProperty* VISU_Actor::GetsValLabelsProps() const
{
  return myValLabeledDataMapper->GetLabelTextProperty();
}

//----------------------------------------------------------------------------

vtkDataSet* VISU_Actor::GetValLabelsInput()
{
  vtkDataSet* aDataSet = 0;
  VISU_PipeLine* aPL = GetPipeLine();
  if ( aPL )
    aDataSet = aPL->GetOutput();
  if ( !aDataSet )
    aDataSet = GetInput();
  return aDataSet;
}


VISU_Actor::EQuadratic2DRepresentation
VISU_Actor::GetQuadratic2DRepresentation() const
{
  if(Superclass::GetQuadraticArcMode()){
    return VISU_Actor::eArcs;
  }
  else
    return VISU_Actor::eLines;
}

void VISU_Actor::SetQuadratic2DRepresentation( EQuadratic2DRepresentation theMode )
{
  switch(theMode) {
  case VISU_Actor::eArcs:
    myPreHighlightActor->SetQuadraticArcMode(true);
    myHighlightActor->SetQuadraticArcMode(true);
    break;
  case VISU_Actor::eLines:
    myPreHighlightActor->SetQuadraticArcMode(false);
    myHighlightActor->SetQuadraticArcMode(false);
    break;
  default:
    break;
  }
}

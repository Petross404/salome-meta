#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : visu_apply_properties_consecutive.py
#  Module : VISU
#
import os
import time
import math
import VISU
import SALOMEDS
import salome
from visu_gui import *

aDelay = 0

myVisu.SetCurrentStudy(salome.myStudy)

myViewManager = myVisu.GetViewManager();

myView = myViewManager.Create3DView();
myView.SetTitle("The viewer for Animation")
print "myViewManager.Create3DView()"
time.sleep(aDelay)

medFile = "TimeStamps.med"
myFieldName = "temperature"

medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile
myResult = myVisu.ImportFile(medFile)

anAnim = myVisu.CreateAnimation(myView);
anAnim.setAnimationMode(VISU.Animation.SUCCESSIVE)
    
aSObj = myStudy.FindObjectIOR(myResult.GetID())
aSObj = aSObj.FindSubObject(1)[1]
aSObj = aSObj.FindSubObject(3)[1]
aSObj = aSObj.FindSubObject(1)[1]
print "addField 1 : ", anAnim.addField(aSObj)

medFile1 = "TimeStamps.med"
myFieldName1 = "pression"

medFile1 = os.getenv('DATA_DIR') + '/MedFiles/' + medFile1
myResult1 = myVisu.ImportFile(medFile1)

aSObj1 = myStudy.FindObjectIOR(myResult1.GetID())
aSObj1 = aSObj1.FindSubObject(1)[1]
aSObj1 = aSObj1.FindSubObject(3)[1]
aSObj1 = aSObj1.FindSubObject(3)[1]
print "addField 2 : ", anAnim.addField(aSObj1)

print "Generate presentations for ", anAnim.getNbFields(), " fields"
for i in range(0,anAnim.getNbFields()):
    anAnim.setPresentationType(i,VISU.TPLOT3D)
    anAnim.generatePresentations(i)

print "Generate frames"
anAnim.generateFrames()

print "Start Animation"
anAnim.setSpeed(22)
anAnim.startAnimation()
myView.FitAll()

while 1:
    time.sleep(1+aDelay)
    if not anAnim.isRunning():
        anAnim.stopAnimation()
        break

anAnim.publishInStudy()
anAnim.saveAnimation()

if anAnim.getAnimationMode() == VISU.Animation.SUCCESSIVE:
   print "End of succcessive animation for 2 fields with presentation type VISU.Animation.SUCCESSIVE"

#for i in range(0,anAnim.getNbFields()):
aPrs = anAnim.getPresentation(0,0)
aPlot3D = aPrs._narrow(VISU.Plot3D)

if aPlot3D:
    aNbLabels = aPlot3D.GetLabels()
    aPlot3D.SetLabels(aNbLabels+3)
    
    aWidth = aPlot3D.GetWidth()
    aHeight = aPlot3D.GetHeight()
    aPlot3D.SetSize(aWidth+0.07,aHeight+0.07)
    
    print "Apply properties for all time stamps of all fields."
    try:
        anAnim.ApplyProperties(0,aPlot3D)
    except SALOME.SALOME_Exception, inst:
        msg = "SALOME.SALOME_Exception caught, see details:"
        msg+="\n******* text:\t" + inst.details.text
        msg+="\n******* type:\t" + str(inst.details.type)
        msg+="\n******* where:\t" + inst.details.sourceFile + ":" + \
              str(inst.details.lineNumber)    
        print msg
        raise RuntimeError, "There are some errors were occured... For more info see ERRORs above..."
    
print "Generate frames"
anAnim.generateFrames()

print "Start Animation"
anAnim.startAnimation()
myView.FitAll()

while 1:
    time.sleep(1+aDelay)
    if not anAnim.isRunning():
        anAnim.stopAnimation()
        break

anAnim.publishInStudy()
anAnim.saveAnimation()

anAnim.clearFields()
print "The number of fields in animation after clearFields method is", anAnim.getNbFields()


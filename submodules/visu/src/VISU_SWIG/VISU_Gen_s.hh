// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Gen_s.hh
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef __VISU_VISU_Gen_s_H__
#define __VISU_VISU_Gen_s_H__

class VISU_Convertor;
class Convertor{
  VISU_Convertor* myConvertor;
public:
  Convertor() : myConvertor(0) {};
  Convertor(const char* theFileName);
  
  VISU_Convertor* GetImpl(){ return myConvertor;}
};

class VISU_ScalarMapPL;
class ScalarMap{
  VISU_ScalarMapPL* myScalarMap;
public:
  ScalarMap() : myScalarMap(0) {};
  ScalarMap(Convertor* theConvertor, const char* theMeshName, int theEntity, 
            const char* theFieldName, int theIteration);

  VISU_ScalarMapPL* GetImpl(){ return myScalarMap;}
};

class vtkRenderer;
class vtkRenderWindow;
class vtkRenderWindowInteractor;
class View3D{
  vtkRenderer *myRen;
  vtkRenderWindow *myRenWin;
  vtkRenderWindowInteractor *myIRen;
public:
  View3D();
  ~View3D();

  void SetPosition(int theX, int theY);
  void Display(ScalarMap* theScalarMap);
};


#endif

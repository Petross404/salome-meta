#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : visu_prs_exam.py
#  Module : VISU
#
import sys
import os
import salome
import SALOMEDS
import SALOME
import SALOME_MED
import VISU

if salome.hasDesktop():
    from libSALOME_Swig import *
    sg = SALOMEGUI_Swig()
import visu_gui
myVisu = visu_gui.myVisu

myVisu.SetCurrentStudy(salome.myStudy)

medFile = "pointe.med"
medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile
print medFile

studyCurrent = salome.myStudyName

med_comp = salome.lcc.FindOrLoadComponent("FactoryServer", "MED")
#myVisu = salome.lcc.FindOrLoadComponent("FactoryServer", "Visu")

try:
    if os.access(medFile, os.R_OK) :
       if os.access(medFile, os.W_OK) :
           med_comp.readStructFileWithFieldType(medFile,studyCurrent)
           med_obj = visu_gui.visu.getMedObjectFromStudy()
           print "med_obj - ", med_obj

           myField = visu_gui.visu.getFieldObjectFromStudy(2,1)
           aMeshName = "FILED_DOUBLE_MESH"
           anEntity = VISU.NODE
           aTimeStampId = 0
           
           myResult1 = myVisu.ImportMedField(myField)
           aMesh1 = myVisu.MeshOnEntity(myResult1, aMeshName, anEntity);
           
           aScalarMap1= myVisu.ScalarMapOnField(myResult1, aMeshName, anEntity, myField.getName(), aTimeStampId)
           if(myField.getNumberOfComponents() > 1) :
               aVectors = myVisu.VectorsOnField(myResult1, aMeshName, anEntity, myField.getName(), aTimeStampId)

           myResult2 = myVisu.ImportFile(medFile)
           aMeshName = "maa1"
           anEntity = VISU.NODE
           aMesh2 = myVisu.MeshOnEntity(myResult2, aMeshName, anEntity)

           aScalarMap2 = myVisu.ScalarMapOnField(myResult2, aMeshName, anEntity, myField.getName(), aTimeStampId)
           if(myField.getNumberOfComponents() > 1) :
             aCutPlanes = myVisu.CutPlanesOnField(myResult2, aMeshName, anEntity, myField.getName(), aTimeStampId)
           if salome.hasDesktop():  
               sg.updateObjBrowser(0)
       else :  print "We have no permission to rewrite medFile, so readStructFileWithFieldType can't open this file";
    else :  print  "We have no permission to read medFile, it will not be opened"; 
except:
    if sys.exc_type == SALOME.SALOME_Exception :
        print "There is no permission to read " + medFile
    else :
        print sys.exc_type 
        print sys.exc_value
        print sys.exc_traceback


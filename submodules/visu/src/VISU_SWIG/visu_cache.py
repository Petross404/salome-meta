#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

import os
import time
import VISU
import SALOMEDS
from batchmode_visu import *

#---------------------------------------------------------------
def WalkTroughTimeStamps(theVISUType,
                         theInput,
                         theViewManager):
  aView = theViewManager.Create3DView();
  aView.SetTitle("To test presentation of %s type" % theVISUType)

  aCache = myVisu.GetColoredPrs3dCache(myVisu.GetCurrentStudy())
  aHolder = aCache.CreateHolder(theVISUType, theInput)

  if not aHolder:
    print "It is impossible to create such kind of holder (%s) with the given parameters" % theVISUType
    print "\ttheMeshName = '%s'" % theInput.myMeshName
    print "\ttheEntity = %s" % theInput.myEntity
    print "\ttheFieldName = '%s'" % theInput.myFieldName
    print "\ttheTimeStampNumber = %s" % theInput.myTimeStampNumber
    return

  aPrs3d = aHolder.GetDevice()

  if not aHolder.Apply(aPrs3d, theInput, aView):
    print "It is impossible to create such kind of presentation (%s) with the given parameters" % theVISUType
    print "\ttheMeshName = '%s'" % theInput.myMeshName
    print "\ttheEntity = %s" % theInput.myEntity
    print "\ttheFieldName = '%s'" % theInput.myFieldName
    print "\ttheTimeStampNumber = %s" % theInput.myTimeStampNumber
    return

  aView.FitAll()

  aDelay = 0.0
  aRange = aHolder.GetTimeStampsRange()
  for anInfo in aRange:
    print "%d (%s); " % (anInfo.myNumber, anInfo.myTime)
    theInput.myTimeStampNumber = anInfo.myNumber
    aHolder.Apply(aPrs3d, theInput, aView)
    time.sleep(aDelay)
    pass
  pass

#---------------------------------------------------------------
PRS3D_TYPE_LIST = []
PRS3D_TYPE_LIST.append(VISU.TGAUSSPOINTS)
PRS3D_TYPE_LIST.append(VISU.TSCALARMAP)
PRS3D_TYPE_LIST.append(VISU.TISOSURFACES)
PRS3D_TYPE_LIST.append(VISU.TCUTPLANES)
PRS3D_TYPE_LIST.append(VISU.TCUTLINES)
PRS3D_TYPE_LIST.append(VISU.TCUTSEGMENT)
PRS3D_TYPE_LIST.append(VISU.TPLOT3D)
PRS3D_TYPE_LIST.append(VISU.TDEFORMEDSHAPE)
PRS3D_TYPE_LIST.append(VISU.TVECTORS)
PRS3D_TYPE_LIST.append(VISU.TSTREAMLINES)
PRS3D_TYPE_LIST.append(VISU.TSCALARMAPONDEFORMEDSHAPE)
PRS3D_TYPE_LIST.append(VISU.TDEFORMEDSHAPEANDSCALARMAP)


#---------------------------------------------------------------
aMedFile = "TimeStamps.med"
aMedFile = "ResOK_0000.med"
aMedFile = os.getenv('DATA_DIR') + '/MedFiles/' + aMedFile
aResult = myVisu.ImportFile(aMedFile)

aMeshName ="dom"
anEntity = VISU.NODE
aFieldName = "vitesse";
aTimeStampNumber = 1

anInput = VISU.ColoredPrs3dHolder.BasicInput(aResult,aMeshName,anEntity,aFieldName,aTimeStampNumber);

aViewManager = myVisu.GetViewManager();

WalkTroughTimeStamps(VISU.TSCALARMAP, anInput, aViewManager)

for aVISUType in PRS3D_TYPE_LIST:
  WalkTroughTimeStamps(aVISUType, anInput, aViewManager)
  pass

anInput.myEntity = VISU.CELL;
anInput.myFieldName = "pression";
WalkTroughTimeStamps(VISU.TGAUSSPOINTS, anInput, aViewManager)

#execfile('/data/apo/a.py')

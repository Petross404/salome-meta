#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Displaying curves attached to different Y axes of Plot2d view
#
import salome
import time
import SALOMEDS
import VISU

sleep_delay = 1

# >>> Getting study builder
myStudy = salome.myStudy
myBuilder = myStudy.NewBuilder()

# >>> Getting (loading) VISU component
myVisu = salome.lcc.FindOrLoadComponent("FactoryServer", "VISU")
myComponent = myStudy.FindComponent("VISU")
myVisu.SetCurrentStudy(myStudy)
if not myComponent:
   myComponent = myBuilder.NewComponent("VISU")
   aName = myBuilder.FindOrCreateAttribute(myComponent, "AttributeName")
   aName.SetValue( salome.sg.getComponentUserName("VISU") )

   A2 = myBuilder.FindOrCreateAttribute(myComponent, "AttributePixMap");
   aPixmap = A2._narrow(SALOMEDS.AttributePixMap);
   aPixmap.SetPixMap( "ICON_OBJBROWSER_Visu" );
   
   myBuilder.DefineComponentInstance(myComponent,myVisu)

# >>> Creating object with Table
aTableObject = myBuilder.NewObject(myComponent)
aTableName = myBuilder.FindOrCreateAttribute(aTableObject, "AttributeName")
aTableName.SetValue("TestTable")
aTable = myBuilder.FindOrCreateAttribute(aTableObject, "AttributeTableOfReal")

aTable.AddRow([0,1,2,3,4,5,6,7,8,9,10])
aTable.AddRow([2000,1900,1800,1700,1600,1500,1400,1300,1200,1100,1000])
aTable.AddRow([1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0])
aTable.SetTitle("Test table")
aTable.SetRowTitle(1,"Time")
aTable.SetRowUnit(1,"s")
aTable.SetRowTitle(2,"Mass")
aTable.SetRowUnit(2,"kg")
aTable.SetRowTitle(3,"Temperature")
aTable.SetRowUnit(3,"K")

# >>> Create table of integer
aVisuTable = myVisu.CreateTable(aTableObject.GetID())

# >>> Create curve attached to the left axis
aCurve1 = myVisu.CreateCurveWithZExt(aVisuTable, 1, 2, 0, False)

# >>> Create curve attached to the right axis
aCurve2 = myVisu.CreateCurveWithZExt(aVisuTable, 1, 3, 1, True)

# >>> Create container and insert curves
aContainer = myVisu.CreateContainer()
aContainer.AddCurve(aCurve1)
aContainer.AddCurve(aCurve2)

# >>> Create XY plot and display container
myViewManager = myVisu.GetViewManager();
myView = myViewManager.CreateXYPlot();
myView.SetTitle("The viewer for curves")
myView.Display(aContainer)

# >>> Update Object Browser
salome.sg.updateObjBrowser(1)

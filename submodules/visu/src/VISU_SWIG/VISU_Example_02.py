#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# T 2.24: Save/retrieve view parameters.
# Uses MED file fra.med from ${DATA_DIR}/MedFiles directory.
# This script is equivalent to non-regression test script 003/A3
#
import salome
import visu_gui
import SALOMEDS
import VISU
import os
import sys

medFile = os.getenv("DATA_DIR") + "/MedFiles/fra.med"

#%====================Stage1: Creating a new study====================%

print "**** Stage1: Creating a new study "

print "Creating a new study..................", 
myVisu = visu_gui.myVisu
myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager()
if myViewManager is None : print "Error"
else : print "OK"

#%====================Stage2: Saving view parameters before import====================%

print "**** Stage2: Saving view parameters before import "

myView = myViewManager.Create3DView()

print "Zooming trihedron.....................",
aScale = myView.GetParallelScale()
myView.SetParallelScale(aScale*4)
print "OK"

print "Rotating trihedron....................",
aPoint = (100,100,100)
myView.SetPointOfView(aPoint) 
print "OK"

print "Saving view parameters................",
aViewParamsName1 = 'ViewParams:1'
aSaveRes = myView.SaveViewParams(aViewParamsName1)
if aSaveRes != 1 : print "Error"
else : print "OK"

#%====================Stage3: Import MED file====================%

print "**** Stage3: Import MED file"

print 'Importing "fra.med"...................',
myResult = myVisu.ImportFile(medFile)
if myResult is None : print "Error"
else : print "OK"

print "Creating mesh.........................",
myMeshName = 'LE VOLUME'
myCellEntity = VISU.CELL
mesh = myVisu.MeshOnEntity(myResult,myMeshName,myCellEntity);
if mesh is None : print "Error"
else : print "OK"

myView.Maximize()
myView.Display(mesh)
myView.FitAll()

#%====================Stage4: Saving view parameters after import====================%

print "**** Stage4:  Saving view parameters after import"

print "Creating Scalar Map...................",
myFieldName = 'TAUX_DE_VIDE'
myNodeEntity = VISU.NODE
scalarmap = myVisu.ScalarMapOnField(myResult,myMeshName,myNodeEntity,myFieldName,1);
if scalarmap is None : print "Error"
else : print "OK"
myView.DisplayOnly(scalarmap)

print "Zooming 3D view.......................",
aScale = myView.GetParallelScale()
myView.SetParallelScale(aScale*2)
print "OK"

print "Setting view point(LEFT)..............",
myView.SetView((VISU.View3D.LEFT))
print "OK"

print "Saving view parameters................",
aViewParamsName2 = 'ViewParams:2'
aSaveRes = myView.SaveViewParams(aViewParamsName2)
if aSaveRes != 1 : print "Error"
else : print "OK"

print "Creating Cut Planes...................",
cutplanes = myVisu.CutPlanesOnField(myResult,myMeshName,myNodeEntity,myFieldName,1);
if cutplanes is None : print "Error"
else : print "OK"
cutplanes.SetNbPlanes(4)
myView.DisplayOnly(cutplanes)

print "Setting scaling.......................",

#Define parameters of scaling:
myXParam = 5
myYParam = 5
myZParam = 1

myXAxis = VISU.View3D.XAxis
myYAxis = VISU.View3D.YAxis
myZAxis = VISU.View3D.ZAxis

myView.ScaleView(myXAxis,myXParam)
myView.ScaleView(myYAxis,myYParam)
myView.ScaleView(myZAxis,myZParam)
print "OK"

print "Rotating 3d view......................",
aPoint = (100,40,0)
myView.SetPointOfView(aPoint) 
print "OK"

print "Fit All...............................",
myView.FitAll()
print "OK"

print "Saving view parameters................",
aViewParamsName3 = 'ViewParams:3'
aSaveRes = myView.SaveViewParams(aViewParamsName3)
if aSaveRes != 1 : print "Error"
else : print "OK"

#%====================Stage5: Restoring view parameters====================%

print "**** Stage5: Restoring view parameters"

print "Restoring first view parameters.......",
aRestoreRes = myView.RestoreViewParams(aViewParamsName1)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Restoring second view parameters......",
aRestoreRes = myView.RestoreViewParams(aViewParamsName2)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Displaing only Scalar Map.............",
myView.DisplayOnly(scalarmap)
print "OK"

print "Displaing only Cut Planes.............",
myView.DisplayOnly(cutplanes)
print "OK"

print "Restoring third view parameters.......",
aRestoreRes = myView.RestoreViewParams(aViewParamsName3)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Displaing only Mesh...................",
myView.DisplayOnly(mesh)
print "OK"

print "Displaing only Scalar Map.............",
myView.DisplayOnly(scalarmap)
print "OK"

print "Displaing only Cut Planes.............",
myView.DisplayOnly(cutplanes)
print "OK"

#%====================Stage6: Changing of view parameters====================%

print "**** Stage6: Changing of view parameters"

print "Remove scaling........................",
myView.RemoveScale()
print "OK"

print "Fit All...............................",
myView.FitAll()
print "OK"

print "Rotating 3d view......................",
aPoint = (0,60,150)
myView.SetPointOfView(aPoint) 
print "OK"

print "Resaving first view parameters........",
aSaveRes = myView.SaveViewParams(aViewParamsName1)
if aSaveRes != 1 : print "Error"
else : print "OK"

print "Restoring third view parameters.......",
aRestoreRes = myView.RestoreViewParams(aViewParamsName3)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Restoring first view parameters.......",
aRestoreRes = myView.RestoreViewParams(aViewParamsName1)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Displaying only Mesh..................",
myView.DisplayOnly(mesh)
print "OK"

print "Displaying only Scalar Map............",
myView.DisplayOnly(scalarmap)
print "OK"

#%====================Stage7: Saving of created view parameters====================%

print "**** Stage7: Saving of created view parameters"

print "Saving study..........................",

str = os.getenv("TmpDir")
if str == None:
	str = "/tmp"

file = str+'/VISU_005.hdf'

salome.myStudyManager.SaveAs(file, salome.myStudy, 0)
study_id = salome.myStudy._get_StudyId()
print "OK"

file_new = str+'/VISU_005_new.hdf'

if not sys.platform == "win32":
  command = "mv " + file + " " + file_new
else:
  command = "move /Y " + file + " " + file_new
  
os.system(command)
file = file_new

# Opening a study tempoparily commented due to a problem
# with python initialization. To be restored after bug 10000 fixing.

#print "Opening just saved study..............",
#
#openedStudy = salome.myStudyManager.Open(file)]
openedStudy = salome.myStudy # temporarily
#myVisu.SetCurrentStudy(openedStudy)
myViewManager = myVisu.GetViewManager()
myView1 = myViewManager.Create3DView()
if myView1 is None : print "Error"
else : print "OK"

import time
time.sleep(1)

print "Restoring first view parameters.......",
aRestoreRes = myView1.RestoreViewParams(aViewParamsName1)
if aRestoreRes != 1 : print "Error"
else : print "OK"

Builder = openedStudy.NewBuilder()
#SCom = openedStudy.FindComponent("VISU")
#Builder.LoadWith(SCom ,myVisu)

print "Displaying Scalar Map.................",
SObj = openedStudy.FindObject('ScalarMap')
scalarmap1 = visu_gui.visu.SObjectToObject(SObj)
if scalarmap1 is None : print "Error"
else : print "OK"
myView1.DisplayOnly(scalarmap1)

print "Displaying Cut Planes.................",
SObj = openedStudy.FindObject('CutPlanes')
cutplanes1 = visu_gui.visu.SObjectToObject(SObj)
if cutplanes1 is None : print "Error"
else : print "OK"
myView1.DisplayOnly(cutplanes1)

print "Restoring second view parameters......",
aRestoreRes = myView1.RestoreViewParams(aViewParamsName2)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Displaying Scalar Map.................",
myView1.DisplayOnly(scalarmap1)
print "OK"

print "Restoring third view parameters.......",
aRestoreRes = myView1.RestoreViewParams(aViewParamsName3)
if aRestoreRes != 1 : print "Error"
else : print "OK"

print "Displaying Cut Planes.................",
myView1.DisplayOnly(cutplanes1)
print "OK"

#%====================Stage8: Changing of saved session====================%

print "**** Stage8: Changing of saved session"

print "Deleting ViewParams:3,1,2.............",
SObj = openedStudy.FindObject(aViewParamsName3)
Builder.RemoveObject(SObj)
SObj = openedStudy.FindObject(aViewParamsName1)
Builder.RemoveObject(SObj)
SObj = openedStudy.FindObject(aViewParamsName2)
Builder.RemoveObject(SObj)
print "OK"

SObjList2 = openedStudy.FindObjectByName(aViewParamsName1, "VISU")
print "FindObjectByName(aViewParamsName1, VISU) returned ", len(SObjList2), " objects"

print "Zooming trihedron.....................",
aScale = myView.GetParallelScale()
myView1.SetParallelScale(aScale*2)
myView1.Update()
print "OK"

print "Rotating trihedron....................",
aPoint = (200,40,-40)
myView1.SetPointOfView(aPoint) 
print "OK"

print "Saving view parameters................",
aSaveRes = myView1.SaveViewParams(aViewParamsName1)
if aSaveRes != 1 : print "Error"
else : print "OK"

print "Renaming ViewParams:1.................",
SObj = openedStudy.FindObject(aViewParamsName1)
SObj.Name = "New_view"
print "OK"

print "Setting scaling.......................",
#Define parameters of scaling:
myXParam = 4
myYParam = 4
myZParam = 8

myXAxis = VISU.View3D.XAxis
myYAxis = VISU.View3D.YAxis
myZAxis = VISU.View3D.ZAxis

myView1.ScaleView(myXAxis,myXParam)
myView1.ScaleView(myYAxis,myYParam)
myView1.ScaleView(myZAxis,myZParam)
print "OK"

myView1.FitAll()

print "Saving view parameters................",
aSaveRes = myView1.SaveViewParams(aViewParamsName2)
if aSaveRes != 1 : print "Error"
else : print "OK"

print "Restoring first view parameters.......",
aRestoreRes = myView1.RestoreViewParams(aViewParamsName1)
if aSaveRes != 1 : print "Error"
else : print "OK"

print "Restoring second view parameters......",
aRestoreRes = myView1.RestoreViewParams(aViewParamsName2)
if aRestoreRes != 1 : print "Error"
else : print "OK"

# Remove the study file
if not sys.platform == "win32":
  command = "rm -r " + file
else:
  command = "del /F " + file
os.system(command)

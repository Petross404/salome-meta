#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# T 2.23, 28: Displaying fields (scalar and vectorial) on nodes, change presentation options.
# Uses MED files ResOK_0000.med and Fields_group3D.med
# This script is equivalent to non-regression test script 003/A1
#
import salome
import visu_gui
import SALOMEDS
import VISU
import os

datadir = os.getenv("DATA_DIR") + "/MedFiles/"

#%====================Stage1: Creating a new study====================%

print "**** Stage1: Creating a new study"

print "Creating a new study..................", 
myVisu = visu_gui.myVisu
myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager()
if myViewManager is None : print "Error"
else : print "OK"

#%====================Stage2: Importing MED file====================%

print "**** Stage2: Importing MED file"

print 'Import "ResOK_0000.med"...............',
medFile = datadir + "ResOK_0000.med"
myResult = myVisu.ImportFile(medFile)
if myResult is None : print "Error"
else : print "OK"

print 'Creating new View3D...................',
myView = myViewManager.Create3DView()
if myView is None : print "Error"
else : print "OK"

myMeshName = 'dom'
myCellEntity = VISU.CELL
myNodeEntity = VISU.NODE

#%====================Stage3: Displaying vector field====================%

print "**** Stage3: Displaying vector field"

print "Creating Scalar Map.......",
scalarmap = myVisu.ScalarMapOnField(myResult,myMeshName,myNodeEntity,'vitesse',1);
if scalarmap is None : print "Error"
else : print "OK"
scalarmap.SetSize(0.15, 0.8)
myView.DisplayOnly(scalarmap)
myView.FitAll()

print "Creating Stream Lines.....",
streamlines = myVisu.StreamLinesOnField(myResult,myMeshName,myNodeEntity,'vitesse',1);
if streamlines is None : print "Error"
else : print "OK"
myView.DisplayOnly(streamlines)

print "Creating Vectors..........",
vectors = myVisu.VectorsOnField(myResult,myMeshName,myNodeEntity,'vitesse',1);
if vectors is None : print "Error"
else : print "OK"
myView.DisplayOnly(vectors)


print "Creating Iso Surfaces.....",
isosurfaces = myVisu.IsoSurfacesOnField(myResult,myMeshName,myNodeEntity,'vitesse',1);
if isosurfaces is None : print "Error"
else : print "OK"
myView.DisplayOnly(isosurfaces)

print "Creating Cut Planes.......",
cutplanes = myVisu.CutPlanesOnField(myResult,myMeshName,myNodeEntity,'vitesse',1);
if cutplanes is None : print "Error"
else : print "OK"
cutplanes.SetNbPlanes(30)
cutplanes.SetOrientation(VISU.CutPlanes.YZ, 0, 0)
myView.DisplayOnly(cutplanes)

print "Creating Scalar Map On Deformed Shape.......",
scalarmapondefshape = myVisu.DeformedShapeAndScalarMapOnField(myResult,myMeshName,myNodeEntity,'vitesse',1);
if scalarmapondefshape is None : print "Error"
else : print "OK"
scalarmapondefshape.SetScalarField(myCellEntity,'pression',2);
myView.DisplayOnly(scalarmapondefshape)

#%====================Stage4: Opening a new study and Med file import====================%

# Opening a new study tempoparily commented due to a problem
# with python initialization. To be restored after bug 10000 fixing.

#print "**** Stage4: Opening a new study and Med file import"
print "**** Stage4: Med file import"

#print "Creating a new study..................", 
#newStudy = salome.myStudyManager.NewStudy('newStudy')
newStudy = salome.myStudy # temporarily
#myVisu.SetCurrentStudy(newStudy)
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()
if myView is None : print "Error"
else : print "OK"

print 'Importing "Fields_group3D.med"........',
medFile = datadir + "Fields_group3D.med"
myResult1 = myVisu.ImportFile(medFile)
if myResult1 is None : print "Error"
myView1 = myViewManager.Create3DView()
if myView1 is None : print "Error"
else : print "OK"

#%====================Stage5: Displaying scalar field====================%

print "**** Stage5: Displaying scalar field"

myMeshName1 = 'mailles_MED'

print "Creating Scalar Map.......",
scalarmap1 = myVisu.ScalarMapOnField(myResult1,myMeshName1,myCellEntity,'scalar field',1);
if scalarmap1 is None : print "Error"
else : print "OK"
myView1.DisplayOnly(scalarmap1)
myView1.FitAll()

print "Creating Iso Surfaces.....",
isosurfaces1 = myVisu.IsoSurfacesOnField(myResult1,myMeshName1,myCellEntity,'scalar field',1);
if isosurfaces1 is None : print "Error"
else : print "OK"
myView1.DisplayOnly(isosurfaces1)

print "Creating Cut Planes.......",
cutplanes1 = myVisu.CutPlanesOnField(myResult1,myMeshName1,myCellEntity,'scalar field',1);
if cutplanes1 is None : print "Error"
else : print "OK"
cutplanes1.SetOrientation(VISU.CutPlanes.YZ, 0, 0)
myView1.DisplayOnly(cutplanes1)

print "Creating Scalar Map On Deformed Shape.......",
scalarmapondefshape1 = myVisu.DeformedShapeAndScalarMapOnField(myResult1,myMeshName1,myCellEntity,'vectoriel field',1);
if scalarmapondefshape1 is None : print "Error"
else : print "OK"
myView1.DisplayOnly(scalarmapondefshape1)
#%====================Stage6: Object browser popup====================%

print "**** Stage6: Object browser popup"

print "Creating mesh.............",
mesh = myVisu.MeshOnEntity(myResult1,myMeshName1,myCellEntity);
if mesh is None : print "Error"
else : print "OK"
myView1.DisplayOnly(mesh)


print "Changing type of presentation of mesh:"
mesh.SetPresentationType(VISU.WIREFRAME)
PrsType = mesh.GetPresentationType()
print "Presentation type..", PrsType
myView1.DisplayOnly(mesh)

mesh.SetPresentationType(VISU.SHADED)
PrsType = mesh.GetPresentationType()
print "Presentation type.....", PrsType
myView1.DisplayOnly(mesh)

mesh.SetPresentationType(VISU.POINT)
PrsType = mesh.GetPresentationType()
print "Presentation type......", PrsType
myView1.DisplayOnly(mesh)
myView1.Update()

mesh.SetPresentationType(VISU.SHRINK)
PrsType = mesh.GetPresentationType()
print "Presentation type.....", PrsType
myView1.DisplayOnly(mesh)

print "Changing color of mesh....",
aColor = SALOMEDS.Color(0,0,1)
mesh.SetCellColor(aColor)
myView1.DisplayOnly(mesh)
print "OK"

print "Renaming ScalarMap........",
SObj = newStudy.FindObjectIOR(scalarmap1.GetID())
newName = 'Renamed Object'
SObj.Name = newName
print "OK"

print "Deleting Cut Planes.......",
SObj = newStudy.FindObjectIOR(cutplanes1.GetID())
myBuilder = newStudy.NewBuilder()
myBuilder.RemoveObject(SObj)
print "OK"

print "Changing first IsoSurfaces",
myVisu.SetCurrentStudy(salome.myStudy)
myView.DisplayOnly(isosurfaces)
isosurfaces.SetNbSurfaces(25)
print "OK"

myView.Maximize()
myView.DisplayOnly(isosurfaces)

print "Hide IsoSurfaces..........",
myView.Erase(isosurfaces)
print "OK"

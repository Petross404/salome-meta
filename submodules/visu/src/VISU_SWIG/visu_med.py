#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : visu_med.py
#  Module : VISU
#
import os
import salome
import SALOMEDS
import SALOME_MED
import VISU

if salome.hasDesktop():
    from libSALOME_Swig import *
    sg = SALOMEGUI_Swig()

def getMedObjectFromStudy():
    mySO = salome.myStudy.FindObject("Objet MED")
    anAttr = mySO.FindAttribute("AttributeIOR")[1]
    obj = salome.orb.string_to_object(anAttr.Value())
    myObj = obj._narrow(SALOME_MED.MED)
    return myObj

def getFieldObjectFromStudy(number,subnumber):
    mySO = salome.myStudy.FindObject("MEDFIELD")
    mysub = mySO.FindSubObject(number)[1]
    if mysub:
        mysubsub = mysub.FindSubObject(subnumber)[1]
        if mysubsub:
            Builder = salome.myStudy.NewBuilder()
            anAttr = Builder.FindOrCreateAttribute(mysubsub, "AttributeIOR")
            obj = salome.orb.string_to_object(anAttr.Value())
            myObj = obj._narrow(SALOME_MED.FIELDINT)
            if (myObj == None):
	        myObj = obj._narrow(SALOME_MED.FIELDDOUBLE)
            return myObj
    else:
        print "ERROR: No Field Object stored in this Study"
        return None

med_comp = salome.lcc.FindOrLoadComponent("FactoryServer", "MED")

medDir = os.getenv('DATA_DIR') + '/MedFiles/'

def importMedFrom(medDir,medFile):
    medFile = medDir + medFile
    med_comp.readStructFileWithFieldType(medFile,salome.myStudyName)
    if salome.hasDesktop():
        sg.updateObjBrowser(1)
  
def importMed(medFile):
    importMedFrom(medDir,medFile)

#med_obj = getMedObjectFromStudy()
myVisu = salome.lcc.FindOrLoadComponent("FactoryServer", "VISU")
myVisu.SetCurrentStudy(salome.myStudy)
print "Use importMed(medFile) or importMedFrom(medDir,medFile) functions !"

#myField = getFieldObjectFromStudy(2,1)
#myResult = myVisu.ImportMed(myField)
#aMesh = myVisu.CreateMesh(myResult);
#aScalarMap = myVisu.CreateScalarMap(myResult,myField.getName(),0)
#if(myField.getNumberOfComponents() > 1) :
#  aScalarMap = myVisu.CreateVectors(myResult,myField.getName(),0)

#myResult = myVisu.ImportFile(medFile)
#aMesh = myVisu.CreateMesh(myResult);
#aScalarMap = myVisu.CreateScalarMap(myResult,myField.getName(),0)
#if(myField.getNumberOfComponents() > 1) :
#  aScalarMap = myVisu.CreateCutPlanes(myResult,myField.getName(),0)

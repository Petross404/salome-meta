#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : visu.py
#  Module : VISU
#
import os
import re
from time import sleep

import VISU
import SALOME
import SALOME_Session_idl
import SALOMEDS
import SALOME_MED
import SALOME_ModuleCatalog
from omniORB import CORBA
from string import *
from time import sleep
from salome import ObjectToID

myORB = None
myNamingService = None
myLifeCycleCORBA = None
myNamingService = None
myLocalStudyManager = None
myLocalStudy = None
myLocalVisu = None
myDelay = None
mySession = None
 
def Initialize(theORB, theNamingService, theLifeCycleCORBA, theStudyManager, theStudy, theDelay) :
    global myORB, myNamingService, myLifeCycleCORBA, myLocalStudyManager, myLocalStudy
    global mySession, myLocalVisu, myDelay
    myDelay = theDelay
    myORB = theORB
    myNamingService = theNamingService
    myLifeCycleCORBA = theLifeCycleCORBA
    myLocalStudyManager = theStudyManager
    while mySession == None:
	mySession = myNamingService.Resolve("/Kernel/Session")
    mySession = mySession._narrow(SALOME.Session)
    mySession.GetInterface()
    myDelay = theDelay
    sleep(myDelay)
    myLocalVisu = myLifeCycleCORBA.FindOrLoadComponent("FactoryServer", "VISU")
    myLocalStudy = theStudy
    myLocalVisu.SetCurrentStudy(myLocalStudy)
    return myLocalVisu

def PublishComponent(theStudyDocument):
    aComponentDataType = myLocalVisu.ComponentDataType()
    aSComponent = theStudyDocument.FindComponent(aComponentDataType)
    if aSComponent == None:
        aStudyBuilder = theStudyDocument.NewBuilder()
        aStudyBuilder.NewCommand()
        aLocked = theStudyDocument.GetProperties().IsLocked()
        if aLocked:
            theStudyDocument.GetProperties().SetLocked(0)
            pass

        aSComponent = aStudyBuilder.NewComponent(aComponentDataType);
        aName = aStudyBuilder.FindOrCreateAttribute(aSComponent,"AttributeName")
        aModuleCatalogObj = myNamingService.Resolve("/Kernel/ModulCatalog");
        aModuleCatalog = aModuleCatalogObj._narrow(SALOME_ModuleCatalog.ModuleCatalog)
        aComponent = aModuleCatalog.GetComponent(aComponentDataType);
        if aComponent != None:
            aComponentUserName = aComponent._get_componentusername()
            aName.SetValue(aComponentUserName)
            pass
        
        aPixMap = aStudyBuilder.FindOrCreateAttribute(aSComponent,"AttributePixMap")
        aPixMap.SetPixMap("ICON_OBJBROWSER_Visu")
        
        aStudyBuilder.DefineComponentInstance(aSComponent,myLocalVisu);
        if aLocked:
            theStudyDocument.GetProperties().SetLocked(1);
            pass
        
        aStudyBuilder.CommitCommand();
        pass
    
    return aSComponent;

def SetName(theObj, theName):
    anIOR  = myORB.object_to_string(theObj)
    aSObj  = myLocalStudy.FindObjectIOR(anIOR)
    anAttr = aSObj.FindAttribute("AttributeName")[1]
    anAttr.SetValue(theName)
        
def StrToMap(theString) :
    aPairList = split(theString,";")
    aMap = {}
    iEnd = len(aPairList)
    for i in range(iEnd) :
        aPair = split(aPairList[i],"=")
        aMap[aPair[0]] = aPair[1]
    return aMap

def IntToEntity(theInt) :
    theInt = atoi(theInt)
    if theInt == 0 :
        return VISU.NODE;
    elif theInt == 1 :
        return VISU.EDGE;
    elif theInt == 2 :
        return VISU.FACE;
    else :
        return VISU.CELL;

def getObjectID(obj):
   ID = ""
   entry = ObjectToID(obj)
   if entry is not None:
	lst = entry.split(":")
	if len(entry) > 6:
        	ID = entry[6:]            
        	return "VISU_" + ID
	return ID

        
def HasValue(theList, theValue) :
    try :
        return theList.index(theValue) + 1
    except StandardError, e :
        return 0

def CreateStudyForMed(theVisu, theFileName) :
    print "CreateStudyForMed ", theFileName,
    if os.path.isfile(theFileName) :
        if re.search("\.med$", theFileName) is not None :
            print "OK"
            aStudyName = os.path.basename(theFileName)
            aStudyName = re.sub(r'\.med', "", aStudyName)
            aStudyName = generateName(aStudyName)
            print "myLocalStudyManager.NewStudy -", aStudyName, "...",
            aStudy = myLocalStudyManager.NewStudy(aStudyName)
            if aStudy is None : raise RuntimeError, "Error"
            else : print "OK"
            
            print "CreateStudyForMed - Creating a 3D viewer...",
            theVisu.SetCurrentStudy(aStudy)
            myViewManager = theVisu.GetViewManager()
            if myViewManager is None : raise RuntimeError, "Error - theVisu.GetViewManager"
            myView = myViewManager.Create3DView()
            if myView is None : raise RuntimeError, "Error - myViewManager.Create3DView"
            myView.Maximize()
            print "OK"

            return [aStudy, myViewManager, myView]
    raise RuntimeError, "Error: There is no such file."

def getMedSObject(theStudy = myLocalStudy ):
    if theStudy is None:
        theStudy = myLocalStudy
    aSO = theStudy.FindObject("Med")
    anIsPresetn, aSObject = aSO.FindSubObject(1)
    return aSObject

def getMedObjectFromStudy(theStudy = myLocalStudy):
    aSObject = getMedSObject(theStudy)
    if aSObject:
        anAttr = aSObject.FindAttribute("AttributeIOR")[1]
        anObj = myORB.string_to_object(anAttr.Value())
        return  anObj._narrow(SALOME_MED.MED)
    return None

def getFieldObjectFromStudy(number, subnumber, theStudy = myLocalStudy):
    if theStudy is None:
        theStudy = myLocalStudy
    mySO = theStudy.FindObject("MEDFIELD")
    if mySO is None:
        raise Runtime, "getFieldObjectFromStudy mySO is None"
    mysub = mySO.FindSubObject(number)[1]
    if mysub:
        mysubsub = mysub.FindSubObject(subnumber)[1]
        if mysubsub:
            Builder = theStudy.NewBuilder()
            anAttr = Builder.FindOrCreateAttribute(mysubsub, "AttributeIOR")
            obj = myORB.string_to_object(anAttr.Value())
            myObj = obj._narrow(SALOME_MED.FIELDINT)
            if (myObj == None):
	        myObj = obj._narrow(SALOME_MED.FIELDDOUBLE)
            return myObj
    else:
        print "ERROR: No Field Object stored in this Study"
        return None

def getSObjectByFatherPathAndName(theStudy, thePath, theName):
    father = theStudy.FindObjectByPath(thePath)
    itr = theStudy.NewChildIterator(father)
    while itr.More():
        so = itr.Value()
        if so.GetName()==theName: return so
        itr.Next()
        pass
    return None

def SObjectToObject(theSObject) :
     #    global myORB
    if theSObject is None :
        print "SObjectToObject : argument is None"
    anObj = None                         
    res,Attr = theSObject.FindAttribute("AttributeIOR")
    if (res != 0) and (Attr is not None)  : 
      anIOR  = Attr._narrow(SALOMEDS.AttributeIOR);
      aValue = anIOR.Value();
      if(len(aValue) != 0) :
	anObj = myORB.string_to_object(aValue);
      else:
	print "SObjectToObject - IOR = ''"
    return anObj;
 
def CreatePrsForMed(theVisu, theFileName, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete = 0) :
#    try:
        global myLifeCycleCORBA
        print "lcc.FindOrLoadComponent...",
        aMedComp = myLifeCycleCORBA.FindOrLoadComponent("FactoryServer", "MED")
        if aMedComp is None : print "Error"
        else : print "OK"
        
        aVISUObjList = [myLocalStudy, myViewManager, myView] = CreateStudyForMed(theVisu, theFileName)

        print "aMedComp.readStructFileWithFieldType...",
        aMedComp.readStructFileWithFieldType(theFileName,myLocalStudy._get_Name())
        aMedSObj = getMedSObject(myLocalStudy)
        if aMedSObj is None : raise RuntimeError, "Error"
        else : print "OK"
        
        print "theVisu.ImportMed...",
        aResult = theVisu.ImportMed(aMedSObj)
        if aResult is None : raise RuntimeError, "Error"
        else : print "OK"

        aVISUObjList.extend(CreatePrsForResult(theVisu, aResult, myView, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete))
        if theIsAutoDelete :
            aResult.RemoveFromStudy()
        else :
            aVISUObjList.append(aResult)
        
        aFolderIter = myLocalStudy.NewChildIterator(aMedSObj.GetFather());
        while aFolderIter.More() :
            aFolderSObj = aFolderIter.Value()
            aFolderIter.Next()
            anAttr = aFolderSObj.FindAttribute("AttributeName")[1]
            anAttr = anAttr._narrow(SALOMEDS.AttributeName);
            aFolderName = anAttr.Value()
            print "  ", aFolderName
            
            if aFolderName == 'MEDMESH' :
                aMeshIter = myLocalStudy.NewChildIterator(aFolderSObj);
                while aMeshIter.More() :
                    aMeshSObj = aMeshIter.Value()
                    aMeshIter.Next()
                    anAttr = aMeshSObj.FindAttribute("AttributeName")[1]
                    anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                    aMeshName = anAttr.Value()
                    print "    ", aMeshName
                
                    aSupportIter = myLocalStudy.NewChildIterator(aMeshSObj);
                    while aSupportIter.More() :
                        aSupportSObj = aSupportIter.Value()
                        aSupportIter.Next()
                        anAttr = aSupportSObj.FindAttribute("AttributeName")[1]
                        anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                        aSupportName = anAttr.Value()
                        print "      ", aSupportName

            if aFolderName == 'MEDFIELD' :
                aFieldIter = myLocalStudy.NewChildIterator(aFolderSObj);
                while aFieldIter.More() :
                    aFieldSObj = aFieldIter.Value()
                    aFieldIter.Next()
                    anAttr = aFieldSObj.FindAttribute("AttributeName")[1]
                    anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                    aFieldName = anAttr.Value()
                    print "    ", aFieldName
                
                    print "theVisu.ImportMed...",
                    aResult = theVisu.ImportMed(aFieldSObj)
                    if aResult is None : raise RuntimeError, "Error"
                    else : print "OK"

                    aVISUObjList.extend(CreatePrsForResult(theVisu, aResult, myView, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete))
                    if theIsAutoDelete :
                        aResult.RemoveFromStudy()
                    else :
                        aVISUObjList.append(aResult)

                    aTimeStampIter = myLocalStudy.NewChildIterator(aFieldSObj);
                    if aTimeStampIter.More() :
                        aTimeStampIter.Next()
                        while aTimeStampIter.More() :
                            aTimeStampSObj = aTimeStampIter.Value()
                            aTimeStampIter.Next()
                            anAttr = aTimeStampSObj.FindAttribute("AttributeName")[1]
                            anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                            aTimeStampName = anAttr.Value()
                            print "        ", aTimeStampName

                            print "theVisu.ImportMed...",
                            aMedField = SObjectToObject(aTimeStampSObj)
                            aResult = theVisu.ImportMedField(aMedField)
                            if aResult is None : raise RuntimeError, "Error"
                            else : print "OK"

                            aVISUObjList.extend(CreatePrsForResult(theVisu, aResult, myView, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete))
                            if theIsAutoDelete :
                                aResult.RemoveFromStudy()
                            else :
                                aVISUObjList.append(aResult)
                    
        return aVISUObjList
    
#    except Exception, e:
#        print str(e)
        
def CreatePrsForFile(theVisu, theFileName, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete = 0) :
 #   try:
        aVISUObjList = [myLocalStudy, myViewManager, myView] = CreateStudyForMed(theVisu, theFileName)

        print "theVisu.ImportMed...",
        aResult = theVisu.ImportFile(theFileName)
        if aResult is None : raise RuntimeError, "Error"
        else : print "OK"
        
        aVISUObjList.extend(CreatePrsForResult(theVisu, aResult, myView, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete))

        if theIsAutoDelete :
            aResult.RemoveFromStudy()
        else :
            aVISUObjList.append(aResult)

        return aVISUObjList
    
#    except Exception, e:
#        print str(e)
                
def CreatePrsForResult(theVisu, theResult, theView, thePrsTypeList, thePictureDir, thePictureExt, theIsAutoDelete = 0) :
    aVISUObjList = None
    if theIsAutoDelete is not None :
	aVISUObjList = []
	pass
	
    print "CreatePrsForResult - myLocalStudy.FindObjectIOR...",
    myLocalStudy = theVisu.GetCurrentStudy()

    aSObj = myLocalStudy.FindObjectIOR(theResult.GetID())
    if aSObj is None : raise RuntimeError, "Error"
    else : print "OK"
    aMeshIter = myLocalStudy.NewChildIterator(aSObj);
    while aMeshIter.More() :
        aMeshSObj = aMeshIter.Value()
        aMeshIter.Next()
        anAttr = aMeshSObj.FindAttribute("AttributeName")[1]
        if anAttr is None :
            aMeshSObj = aMeshIter.Value()
            aMeshIter.Next()
            anAttr = aMeshSObj.FindAttribute("AttributeName")[1]
        anAttr = anAttr._narrow(SALOMEDS.AttributeName);
        aMeshName = anAttr.Value()
        aMeshNamePic = re.sub(".","_",aMeshName)
        print "  ", aMeshName
        
        aFolderIter = myLocalStudy.NewChildIterator(aMeshSObj);
        while aFolderIter.More() :
            aFolderSObj = aFolderIter.Value()
            aFolderIter.Next()
            anIsFound, anAttr = aFolderSObj.FindAttribute("AttributeName")
            if not anIsFound :
                continue
            anAttr = anAttr._narrow(SALOMEDS.AttributeName);
            aFolderName = anAttr.Value()
            print "    ", aFolderName
            
            if aFolderName == 'Families' :
                anEntityIter = myLocalStudy.NewChildIterator(aFolderSObj);
                while anEntityIter.More() :
                    anEntitySObj = anEntityIter.Value()
                    anEntityIter.Next()
                    anAttr = anEntitySObj.FindAttribute("AttributeName")[1]
                    anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                    anEntityName = anAttr.Value()
                    
                    anEntityId = "3"
                    if anEntityName == 'onNodes' :
                        anEntity = VISU.NODE
                        anEntityId = "0"
                    if anEntityName == 'onEdges' :
                        anEntity = VISU.EDGE
                        anEntityId = "1"
                    if anEntityName == 'onFaces' :
                        anEntity = VISU.FACE
                        anEntityId = "2"
                    if anEntityName == 'onCells' :
                        anEntity = VISU.CELL
                        anEntityId = "3"

                    if HasValue(thePrsTypeList,VISU.TMESH) :
                        print "      ", anEntityName, aMeshName,
                        aMesh = theVisu.MeshOnEntity(theResult,aMeshName,anEntity)
                        if aMesh is None : print "Error"
                        else :
                            print ",OK"
                            theView.DisplayOnly(aMesh)
                            theView.FitAll()
                            aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "." + thePictureExt
                            aPictureName = re.sub("\s+","_", aPictureName);
                            theView.SavePicture(aPictureName)
                            if theIsAutoDelete :
                                aMesh.RemoveFromStudy()
                            else :
                                aVISUObjList.append(aMesh)
                        
                        anFamilyIter = myLocalStudy.NewChildIterator(anEntitySObj);
                        while anFamilyIter.More() :
                            aFamilySObj = anFamilyIter.Value()
                            anFamilyIter.Next()
                            anAttr = aFamilySObj.FindAttribute("AttributeName")[1]
                            anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                            anFamilyName = anAttr.Value()
                            if HasValue(thePrsTypeList,VISU.TMESH) :
                                print "        ", anFamilyName,
                                aMesh = theVisu.FamilyMeshOnEntity(theResult,aMeshName,anEntity,anFamilyName)
                                if aMesh is None : print "Error"
                                else :
                                    print ",OK"
                                    theView.DisplayOnly(aMesh)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + anFamilyName + "." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName);
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
                                        aMesh.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aMesh)
                                
            if aFolderName == 'Groups' :
                aGroupIter = myLocalStudy.NewChildIterator(aFolderSObj);
                while aGroupIter.More() :
                    aGroupSObj = aGroupIter.Value()
                    aGroupIter.Next()
                    anAttr = aGroupSObj.FindAttribute("AttributeName")[1]
                    anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                    aGroupName = anAttr.Value()
                    if HasValue(thePrsTypeList,VISU.TMESH) :
                        print "      ", aGroupName,
                        aMesh = theVisu.GroupMesh(theResult,aMeshName,aGroupName)
                        if aMesh is None : print "Error"
                        else :
                            print ",OK"
                            theView.DisplayOnly(aMesh)
                            theView.FitAll()
                            aPictureName = thePictureDir + aMeshNamePic + "_" + aGroupName + "." + thePictureExt
                            aPictureName = re.sub("\s+","_", aPictureName);
                            theView.SavePicture(aPictureName)
                            if theIsAutoDelete :
                                aMesh.RemoveFromStudy()
                            else :
                                aVISUObjList.append(aMesh)
                        
            if aFolderName == 'Fields' :
                aFieldIter = myLocalStudy.NewChildIterator(aFolderSObj);
                while aFieldIter.More() :
                    aFieldSObj = aFieldIter.Value()
                    aFieldIter.Next()
                    anAttr = aFieldSObj.FindAttribute("AttributeName")[1]
                    anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                    aFieldName = anAttr.Value()
                    print "      ", aFieldName
                    
                    anAttr = aFieldSObj.FindAttribute("AttributeString")[1]
                    anAttr = anAttr._narrow(SALOMEDS.AttributeString);
                    aFieldComment = anAttr.Value()
                    aMap = StrToMap(aFieldComment)
                    
                    aTimeStampIter = myLocalStudy.NewChildIterator(aFieldSObj);
                    if aTimeStampIter.More() :
                        aTimeStampIter.Next()
                        while aTimeStampIter.More() :
                            aTimeStampSObj = aTimeStampIter.Value()
                            aTimeStampIter.Next()
                            anAttr = aTimeStampSObj.FindAttribute("AttributeName")[1]
                            anAttr = anAttr._narrow(SALOMEDS.AttributeName);
                            aTimeStampName = anAttr.Value()
                            print "        ", aTimeStampName
                            
                            anAttr = aTimeStampSObj.FindAttribute("AttributeString")[1]
                            anAttr = anAttr._narrow(SALOMEDS.AttributeString);
                            aTimeStampComment = anAttr.Value()
                            aMap = StrToMap(aTimeStampComment)
                            aMeshName = aMap["myMeshName"]
                            aFieldName = aMap["myFieldName"]
                            aTimeStampId = atoi(aMap["myTimeStampId"])
                            anEntity = IntToEntity(aMap["myEntityId"])
                            anEntityId = str(aMap["myEntityId"]);
                            #print aMeshName, aFieldName, anEntity, aTimeStampId
                            if HasValue(thePrsTypeList,VISU.TSCALARMAP) :
                                print "          Creating ScalarMapOnField",
                                aPrsObj = theVisu.ScalarMapOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                if aPrsObj is None : print "Error"
                                else :
                                    print ",OK"
                                    theView.DisplayOnly(aPrsObj)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TSCALARMAP." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName);
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
	                                aPrsObj.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aPrsObj)

                            if HasValue(thePrsTypeList,VISU.TISOSURFACES) :
                                print "          Creating IsoSurfacesOnField",
                                aPrsObj = theVisu.IsoSurfacesOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                if aPrsObj is None : print "Error"
                                else :
                                    print ",OK"
                                    theView.DisplayOnly(aPrsObj)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TISOSURFACES." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName);
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
	                                aPrsObj.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aPrsObj)
                                
                            if HasValue(thePrsTypeList,VISU.TCUTPLANES) :
                                print "          Creating CutPlanesOnField",
                                aPrsObj = theVisu.CutPlanesOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                if aPrsObj is None : print "Error"
                                else :
                                    print ",OK"
                                    aPrsObj.SetOrientation(VISU.CutPlanes.ZX,aPrsObj.GetRotateX(),aPrsObj.GetRotateY())
                                    theView.DisplayOnly(aPrsObj)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TCUTPLANES." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName)
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
	                                aPrsObj.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aPrsObj)
                                
                            if HasValue(thePrsTypeList,VISU.TCUTLINES) :
                                print "          Creating CutLinesOnField",
                                aPrsObj = theVisu.CutLinesOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                if aPrsObj is None : print "Error"
                                else :
                                    print ",OK"
                                    aPrsObj.SetOrientation(VISU.CutPlanes.XY,aPrsObj.GetRotateX(),aPrsObj.GetRotateY())
                                    aPrsObj.SetOrientation2(VISU.CutPlanes.ZX,aPrsObj.GetRotateX2(),aPrsObj.GetRotateY2())
                                    theView.DisplayOnly(aPrsObj)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TCUTLINES." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName)
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
	                                aPrsObj.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aPrsObj)
                                
                            if HasValue(thePrsTypeList,VISU.TCUTSEGMENT) :
                                print "          Creating CutSegmentOnField",
                                aPrsObj = theVisu.CutSegmentOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                if aPrsObj is None : print "Error"
                                else :
                                    print ",OK"
                                    theView.DisplayOnly(aPrsObj)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TCUTSEGMENT." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName)
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
	                                aPrsObj.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aPrsObj)
                                
                            if HasValue(thePrsTypeList,VISU.TPLOT3D) :
                                print "          Creating Plot3DOnField",
                                aPrsObj = theVisu.Plot3DOnField(theResult,aMeshName,anEntity,
                                                                aFieldName,aTimeStampId)
                                if aPrsObj is None : print "Error"
                                else :
                                    print ",OK"
                                #aPrsObj.SetOrientation(VISU.CutPlanes.ZX,
                                #                       aPrsObj.GetRotateX(),
                                #                       aPrsObj.GetRotateY())
                                    theView.DisplayOnly(aPrsObj)
                                    theView.FitAll()
                                    aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TPLOT3D." + thePictureExt
                                    aPictureName = re.sub("\s+","_", aPictureName)
                                    theView.SavePicture(aPictureName)
                                    if theIsAutoDelete :
	                                aPrsObj.RemoveFromStudy()
                                    else :
                                        aVISUObjList.append(aPrsObj)
                                
                            aNumComponent = atoi(aMap["myNumComponent"])
                            if aNumComponent > 1 :
                                if HasValue(thePrsTypeList,VISU.TDEFORMEDSHAPE) :
                                    print "          Creating DeformedShapeOnField",
                                    aPrsObj = theVisu.DeformedShapeOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                    if aPrsObj is None : print "Error"
                                    else :
                                        print ",OK"
                                        theView.DisplayOnly(aPrsObj)
                                        theView.FitAll()
                                        aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TDEFORMEDSHAPE." + thePictureExt
                                        aPictureName = re.sub("\s+","_", aPictureName)
                                        theView.SavePicture(aPictureName)
                                        if theIsAutoDelete :
                                            aPrsObj.RemoveFromStudy()
                                        else :
                                            aVISUObjList.append(aPrsObj)

                                if HasValue(thePrsTypeList,VISU.TVECTORS) :
                                    print "          Creating VectorsOnField",
                                    aPrsObj = theVisu.VectorsOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                    if aPrsObj is None : print "Error"
                                    else :
                                        print ",OK"
                                        theView.DisplayOnly(aPrsObj)
                                        theView.FitAll()
                                        aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TVECTORS." + thePictureExt
                                        aPictureName = re.sub("\s+","_", aPictureName)
                                        theView.SavePicture(aPictureName)
                                        if theIsAutoDelete :
                                            aPrsObj.RemoveFromStudy()
                                        else :
                                            aVISUObjList.append(aPrsObj)
                                    
                                if HasValue(thePrsTypeList,VISU.TSTREAMLINES) :
                                    print "          Creating StreamLinesOnField",
                                    aPrsObj = theVisu.StreamLinesOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                    if aPrsObj is None : print "Error"
                                    else :
                                        print ",OK"
                                        theView.DisplayOnly(aPrsObj)
                                        theView.FitAll()
                                        aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TSTREAMLINES." + thePictureExt
                                        aPictureName = re.sub("\s+","_", aPictureName)
                                        theView.SavePicture(aPictureName)
                                        if theIsAutoDelete :
                                            aPrsObj.RemoveFromStudy()
                                        else :
                                            aVISUObjList.append(aPrsObj)

                                if HasValue(thePrsTypeList,VISU.TSCALARMAPONDEFORMEDSHAPE) or HasValue(thePrsTypeList,VISU.TDEFORMEDSHAPEANDSCALARMAP) :
                                    print "          Creating DeformedShapeAndScalarMapOnField",
                                    aPrsObj = theVisu.DeformedShapeAndScalarMapOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                    if aPrsObj is None : print "Error"
                                    else :
                                        print ",OK"
                                        theView.DisplayOnly(aPrsObj)
                                        theView.FitAll()
                                        aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TDEFORMEDSHAPEANDSCALARMAP." + thePictureExt
                                        aPictureName = re.sub("\s+","_", aPictureName)
                                        theView.SavePicture(aPictureName)
                                        if theIsAutoDelete :
                                            aPrsObj.RemoveFromStudy()
                                        else :
                                            aVISUObjList.append(aPrsObj)

                                if HasValue(thePrsTypeList,VISU.TGAUSSPOINTS) :
                                    print "          Creating GaussPointsOnField",
                                    aPrsObj = theVisu.GaussPointsOnField(theResult,aMeshName,anEntity,aFieldName,aTimeStampId)
                                    if aPrsObj is None : print "Error"
                                    else :
                                        print ",OK"
                                        theView.DisplayOnly(aPrsObj)
                                        theView.FitAll()
                                        aPictureName = thePictureDir + aMeshNamePic + "_" + anEntityId + "_" + aFieldName + "_" + str(aTimeStampId) + "_TGAUSSPOINTS." + thePictureExt
                                        aPictureName = re.sub("\s+","_", aPictureName)
                                        theView.SavePicture(aPictureName)
                                        if theIsAutoDelete :
                                            aPrsObj.RemoveFromStudy()
                                            pass
                                        else :
                                            aVISUObjList.append(aPrsObj)
                                            pass
                                        pass
                                    pass
                                
    return aVISUObjList


def generateName(prefix = None):
    import random;
    int = random.randint(1,1000);
    if prefix is None:
        return "Study" + str(int)
    else :
        return prefix + str(int)
    


# ----------------------
# MESH
# ----------------------
def try_mesh_parameters(theMeshPattern):
    aResult = []
    if theMeshPattern is None : return aResult ;
    theMeshPattern =  theMeshPattern._narrow(VISU.Mesh)
    if theMeshPattern is None : return aResult ;

    aTYPES = [VISU.POINT, VISU.WIREFRAME, VISU.SHADED, VISU.INSIDEFRAME, VISU.SHRINK]
    import copy; import os;
    for ind in aTYPES:
        aNewMesh = copy.deepcopy(theMeshPattern);
        aNewMesh.SetPresentationType(ind)
        aResult.append(aNewMesh)

    return aResult


# ----------------------------    
# SCALAR MAP 
# ----------------------------    
ind=1  #try safe way
def try_scalarmap_parameters(thePattern, dump = 0):

    if thePattern  is None : return None 

    SCALING = [VISU.LINEAR, VISU.LOGARITHMIC]
    import copy
    import random

    anObj = thePattern#copy.deepcopy(thePattern);
    #ind = random.randint(1,2)
    if ind%2 :
            #try incorrect value deliberately (but allowed by idl description)
            #try SetScalarMode(long)
            mode = random.randint(-100000,100000); #incorrect value deliberately
    else:
            #correct value of ScalarMode
            mode = random.randint(0, 3)

    if dump : print "\tSetScalarMode(" + str(mode) +")"
    anObj.SetScalarMode(mode)

    # --- SCALING ---
    scal = random.randint(0,1)
    if dump : print "\tSetScaling(" + str(SCALING[scal]) +")"
    anObj.SetScaling(SCALING[scal])
        
    # --- BOUNDARIES ---
    if ind%2 :
            alfa =  random.random()*random.randint(-100000,100000)
            betta = random.random()*random.randint(-100000,100000)
            aMin = alfa; aMax = betta
    else:
            #more correct set
            aPMin = thePattern.GetMin()
            aPMax = thePattern.GetMax()
            aLen = aPMax - aPMin
            alfa =  random.random()%0.5
            betta = random.random()%0.5
            aMin = alfa*aLen*random.randint(-1,1) + aPMin
            aMax = betta*aLen*random.randint(-1,1) + aPMax
    if dump : print "\tSetRange(" + str(aMin) + ", " + str(aMax) + ")"
    anObj.SetRange(aMin, aMax)

        # --- POSITION ---
    if ind%2:
            X=random.random()*random.randint(-100000,100000)
            Y=random.random()*random.randint(-100000,100000)
    else :
             X=random.random()
             Y=random.random()
    if dump : print "SetPosition("+ str(X) + ", " + str(Y) + " )"
    anObj.SetPosition(X, Y)

        # --- SCALAR BAR SIZE ---
    if ind%2:
            aWidth=random.random()*random.randint(-100000,100000)
            aHeight=random.random()*random.randint(-100000,100000)
    else :
             aWidth=random.random()
             aHeight=random.random()
    if dump : print " SCALAR BAR Width = " + str(aWidth) + " Height = ", str(aHeight)
    anObj.SetSize(aWidth, aHeight)
    
    return anObj


def dump_scalarmap_parameters(anObj):
    
    print "\tGetScalarMode() = " + str(anObj.GetScalarMode()) 
    print "\tGetScaling() = " + str(anObj.GetScaling()) 
    print "\tGetMin() = " + str(anObj.GetMin()) + "  GetMax() = " + str (anObj.GetMax())
    print "\tGetOrientation() = " + str(anObj.GetOrientation())
    print "\tGetPosX() = ", str(anObj.GetPosX()) + "  GetPosY() = ", str(anObj.GetPosY())
    print "\tGetWidth() = ", str ( anObj.GetWidth()) + "  GetHeight() = " + str(anObj.GetHeight())

# ----------------------
# DEFORMED SHAPE
# ----------------------
def try_deformedshape_parameters(thePattern) :

    if thePattern  is None : return None 
    import copy
    import random

    anObj = try_scalarmap_parameters(thePattern)

    # --- SCALING ---
    if ind%2:
        anObj.SetScale( random.random()*random.randint(-100000, 100000))
    else :
        anObj.SetScale( anObj.GetScale()*random.random())

    return anObj


def dump_deformedshape_parameters(theObject):
    dump_scalarmap_parameters(theObject)
    print "GetScale() = ", theObject.GetScale()
    
# ----------------------
# CUT PLANES
# ----------------------
def try_cutplanes_parameters(thePattern) :

    if thePattern  is None : return None 
    import copy
    import random

    ORIENT = [VISU.CutPlanes.XY, VISU.CutPlanes.YZ, VISU.CutPlanes.ZX]

    ind = random.randint(1,2)
    anObj = try_scalarmap_parameters(thePattern)
        
    if ind%2 :   anObj.SetNbPlanes(random.randint(-40,40))
    else :       anObj.SetNbPlanes(random.randint(0,10)) #try behaivor if NbPlanes=0

    # --- DISPLACEMENT ---
    anObj.SetDisplacement(random.randint(-100000,100000))

    # --- PLANE POSITION ---
    if ind%2:
        PlaneNb = random.randint(-100000,100000) #incorrect value is possible
    else    : PlaneNb = random.randint(0, anObj.GetNbPlanes())
        
    anObj.SetPlanePosition(PlaneNb, random.random()*random.randint(-100000,100000))

    # --- SET DEFAULT ---
    anObj.SetDefault(PlaneNb)

    # --- SET X,Y,Z ROTATION ---
    if ind%2 :
            angle1 = random.random()*random.randint(-100000,100000)
            angle2 = random.random()*random.randint(-100000,100000)
    else :
            angle1 = random.random()*3.14
            angle2 = random.random()*3.14
        
    # --- ORIENTATION ---
    anObj.SetOrientation(ORIENT[random.randint(0,2)],angle1,angle2)

    return anObj
        
def dump_cutplanes_parameters(theObject):
        dump_saclarmap_parameters(theObject)

        print "GetOrientationType = " + str(theObject.GetOrientationType())
        PlanesNb = theObject.GetNbPlanes()
        print "GetNbPlanes() = ", str(PlanesNb)
        for i in range(0,PlanesNb+1):
            if theObject.IsDefault(i) :
                print "Default plane : "+str(i); break
        print "GetPlanePosition(" + str(i) + ") = ", theObject.GetPlanePosition(i)
        print "GetDisplacement() = ", str(theObject.GetDisplacement())
        print "GetRotateX() = ", str(theObject.GetRotateX())
        print "GetRotateY() = ", str(theObject.GetRotateY())
        print "GetRotateZ() = ", str(theObject.GetRotateZ())

# ----------------------
# CUT LINES
# ----------------------
def try_cutlines_parameters(thePattern):

    if thePattern  is None : return None 
    import copy
    import random

    ORIENT = [VISU.CutPlanes.XY, VISU.CutPlanes.YZ, VISU.CutPlanes.ZX]
    ind = random.randint(1,2)
    anObj = try_scalarmap_parameters(thePattern)

    # --- ORIENTATION ---
    anObj.SetOrientation(ORIENT[random.randint(0,2)],
                         random.randint(-100,100)*random.random(),
                         random.randint(-100,100)*random.random())
    anObj.SetOrientation2(ORIENT[random.randint(0,2)],
                          random.randint(-100,100)*random.random(),
                          random.randint(-100,100)*random.random())

    # --- Base Plane Position ---
    anObj.SetBasePlanePosition( random.random()*random.randint(-100000,100000))

    # --- NUMBER OF LINES ---
    if ind%2:
            anObj.SetNbLines(random.randint(-5, 50))

    return anObj

def dump_cutlines_parameters(theObject):
    dump_scalarmap_parameters(theObject)

    print "GetOrientationType() = " + str(theObject.GetOrientationType())
    print "GetOrientationType2() = " + str(theObject.GetOrientationType2())
    print "GetBasePlanePosition() = "+ str(theObject.GetBasePlanePosition())
    print "GetNbLines() = " + str(theObject.GetNbLines())
    print "GetRotateX() = ", str(theObject.GetRotateX())
    print "GetRotateX2() = ", str(theObject.GetRotateX2())
    print "GetRotateY() = ", str(theObject.GetRotateY())
    print "GetRotateY2() = ", str(theObject.GetRotateY2())

# ----------------------
# CUT SEGMENT
# ----------------------
def try_cutsegment_parameters(thePattern):

    if thePattern  is None : return None 
    import copy
    import random

    anObj = try_scalarmap_parameters(thePattern)

    anObj.SetPoint1(random.randint(-100,100)*random.random(),
                    random.randint(-100,100)*random.random(),
                    random.randint(-100,100)*random.random())
    anObj.SetPoint2(random.randint(-100,100)*random.random(),
                    random.randint(-100,100)*random.random(),
                    random.randint(-100,100)*random.random())

    return anObj

def dump_cutsegment_parameters(theObject):
    dump_scalarmap_parameters(theObject)

    x1 = y1 = z1 = 0
    x2 = y2 = z2 = 0
    theObject.GetPoint1(x1, y1, z1)
    theObject.GetPoint1(x2, y2, z2)
    print "GetPoint1() = " + str(x1) + ", " + str(y1) + ", " + str(z1)
    print "GetPoint2() = " + str(x2) + ", " + str(y2) + ", " + str(z2)

# ----------------------
# STREAM LINES
# ----------------------
def try_streamlines_parameters(thePattern):

    if thePattern  is None : return None 
    import copy
    import random

    DIRECTION = [VISU.StreamLines.FORWARD, VISU.StreamLines.BACKWARD, VISU.StreamLines.BOTH]

    ind = random.randint(1,2)
    anObj = (try_deformedshape_parameters(thePattern))[0]

    # --- DIREACTION ---
    anObj.SetDirection(DIRECTION[random.randint(0,2)])

    # --- STEP LENGTH ---
    if ind%2 : anObj.SetStepLength(random.random()*random.randint(-1000,1000))
    else :
            aLen = anObj.GetMax() - anObj.GetMin()
            anObj.SetStepLength(aLen/random.randint(1,100))
            
    # --- PROPAGATION TIME ---
    anObj.SetPropagationTime(random.random()*random.randint(1,100))

    # --- INTEGRATION STEP ---
    if ind%2 :
            anObj.SetIntegrationStep(random.random()*random.randint(-1000,1000))
    else:
            anObj.SetIntegrationStep(random.random())

    # --- USED POINT ---
    anObj.SetUsedPoints(random.random()*random.randint(-10000,10000))

    return anObj
        
def dump_streamlines_parameters(theObject):
    
    dump_deformedshape_parameters(theObject)
    
    print "GetDirection() = "      + str(theObject.GetDirection())
    print "GetStepLength() = "     + str(theObject.GetStepLength())
    print "GetPropagationTime() =" + str(theObject.GetPropagationTime())
    print "GetIntegrationStep() =" + str(theObject.GetIntegrationStep())
    print "GetUsedPoints()      =" + str(theObject.GetUsedPoints())
    
# ----------------------
# VECTORS     
# ----------------------
def try_vectors_parameters(thePattern, theNum):

    if thePattern  is None : return None 
    import copy
    import random
    GLIPH_TYPE = [VISU.Vectors.ARROW, VISU.Vectors.CONE2, VISU.Vectors.CONE6, VISU.Vectors.NONE]
    GLIPH_POS = [VISU.Vectors.CENTER, VISU.Vectors.TAIL, VISU.Vectors.HEAD]
    ind = random.randint(1,2)
    anObj = (try_deformedshape_parameters(thePattern))[0]

    # --- LINE WIDTH ---
    if ind%2 :
            anObj.SetLineWidth(random.random()*random.randint(-10000,10000))
    else :
            anObj.SetLineWidth(random.randint(1, 10))

    # --- GLIPH TYPE ---
    anObj.SetGlyphType(GLIPH_TYPE[random.randint(0, len(GLIPH_TYPE)-1)])
        
    # --- GLIPH POS ---
    anObj.SetGlyphPos(GLIPH_POS[random.randint(0, len(GLIPH_POS)-1)])
    
    return anObj

def dump_vectors_parameters(theObject):
    
    dump_deformedshape_parameters(theObject)
    
    print "GetLineWidth() = " +str(theObject.GetLineWidth())
    print "GetGlyphType() = " +str(theObject.GetGlyphType())
    print "GetGlyphPos()  = " +str(theObject.GetGlyphPos())


# ----------------------
# ISO SURFACES     
# ----------------------

def try_isosurfaces_parameters(thePattern) :
    if thePattern  is None : return None 
    import copy
    import random
    
    anObj = try_scalarmap_parameters(thePattern)
    ind = random.randint(1,2)   
    # --- SURFACES NUMBER ---
    if ind%2 :
            anObj.SetNbSurfaces(random.randint(-100000,100000))
    else:
            anObj.SetNbSurfaces(random.randint(1, 50))
            
    return anObj

def dump_isosurfaces_parameters(theObject):
    
    dump_scalarmap_parameters(theObject)
    print "GetNbSurfaces() = "+ str(theObject.GetNbSurfaces())

# ----------------------------
# SCALAR MAP ON DEFORMED SHAPE
# ----------------------------
def dump_scalarmapondeformedshape_parameters(theObject):
    dump_scalarmap_parameters(theObject)
    print "GetScale()          =", theObject.GetScale()
    print "GetScalarCMeshName ()=|",theObject.GetScalarCMeshName(),"|"
    print "GetScalarCFieldName()=|",theObject.GetScalarCFieldName(),"|"
    print "GetScalarEEntity()   =",theObject.GetScalarEEntity()
    print "GetScalarLIteration()=",theObject.GetScalarLIteration()
    pass

def try_scalarmapondeformedshape_parameters(thePattern):

    if thePattern  is None : return None 
    import copy
    import random

    anObj = try_scalarmap_parameters(thePattern)

    # --- SCALING ---
    print 
    anObj.SetScale( anObj.GetScale()/random.random()*random.randint(1,10))

    # --- Scalar Field ---
    # not implemented yet.
    
    return anObj
# ----------------------
# PLOT 3D 
# ----------------------
def dump_plot3d_parameters(theObject):
    dump_scalarmap_parameters(theObject)
    print "GetOrientationType()=",str(theObject.GetOrientationType())
    print "GetRotateX()=",theObject.GetRotateX()
    print "GetRotateY()=",theObject.GetRotateY()
    print "GetPlanePosition()=",theObject.GetPlanePosition()
    print "IsPositionRelative()=",theObject.IsPositionRelative()
    print "GetScaleFactor()=",theObject.GetScaleFactor()
    print "GetIsContourPrs()=",theObject.GetIsContourPrs()
    print "GetNbOfContours()=",theObject.GetNbOfContours()
    pass

def try_plot3d_parameters(thePattern):
    if thePattern is None : return None
    import random
    
    anObj = try_scalarmap_parameters(thePattern)

    ORIENT = [VISU.Plot3D.XY, VISU.Plot3D.YZ, VISU.Plot3D.ZX]

    theXAngle = random.random()*random.randint(-180,180)
    theYAngle = random.random()*random.randint(-180,180)

    # ORIENTATION and ANGLES
    anObj.SetOrientation(ORIENT[random.randint(0,2)],theXAngle,theYAngle)

    # Plane position (revative)
    anObj.SetPlanePosition(random.random(),1)

    # SCALE FACTOR
    anObj.SetScaleFactor(random.random()*random.randint(-10000,10000))

    # CONTOUR PRS
    anObj.SetContourPrs(random.randint(0,1))
    if anObj.GetIsContourPrs():
        anObj.SetNbOfContours(random.randint(1,999))
        pass
    
    pass

# ----------------------
# Gauss points
# ----------------------
def dump_gausspoints_parameters(theObject):
    
    print "\tGetScalarMode() = " + str(anObj.GetScalarMode())
    print "\tGetMin() = " + str(anObj.GetMin()) + "  GetMax() = " + str (anObj.GetMax())
    print "\tGetPosX() = ", str(anObj.GetPosX()) + "  GetPosY() = ", str(anObj.GetPosY())
    print "\tGetWidth() = ", str ( anObj.GetWidth()) + "  GetHeight() = " + str(anObj.GetHeight())
    print "\tGetNbColors() = " + str(anObj.GetNbColors())
    print "\tGetLabels() = " + str(anObj.GetLabels())
    print "\tGetTitle() = " + str(anObj.GetTitle())
    pass

def try_plot3d_parameters(thePattern):
    if thePattern is None : return None
    import random
    
    pass

# ----------------------
# TABLES AND CURVES
# ----------------------

def FillTable( theTable, theValues, theRows, theColumns, theRowTitles, theRowUnits, theColumnTitles ):
    if theTable is None: return
    if len(theRows) != len(theColumns): return
    if len(theRows) != len(theValues): return
    i = 0
    for value in theValues:
        theTable.PutValue( value, theRows[ i ], theColumns[ i ])
        i = i + 1
        pass
    i = 1
    for title in theRowTitles:
        theTable.SetRowTitle( i, title )
        theTable.SetRowUnit( i, theRowUnits[ i - 1 ])
        i = i + 1
        pass
    i = 1
    for title in theColumnTitles:
        theTable.SetColumnTitle( i, title )
        i = i + 1
        pass
    pass

def CreateCurve( theTable, theHRow, theVRow, theTitle, theColor, theMarker, theLineType, theLineWidth ):
    if theTitle is None: return
    curve = myLocalVisu.CreateCurve( theTable, theHRow, theVRow );
    if curve:
        curve.SetTitle( theTitle )
        curve.SetColor( theColor )
        curve.SetMarker( theMarker )
        curve.SetLine( theLineType, theLineWidth )
        pass
    return curve

def CreateCurveWithZ( theTable, theHRow, theVRow, theZRow, theTitle, theColor, theMarker, theLineType, theLineWidth ):
    if theTitle is None: return
    curve = myLocalVisu.CreateCurveWithZ( theTable, theHRow, theVRow, theZRow );
    if curve:
        curve.SetTitle( theTitle )
        curve.SetColor( theColor )
        curve.SetMarker( theMarker )
        curve.SetLine( theLineType, theLineWidth )
        pass
    return curve

def CreateCurveWithZExt( theTable, theHRow, theVRow, theZRow, theIsV2, theTitle, theColor, theMarker, theLineType, theLineWidth ):
    if theTitle is None: return
    curve = myLocalVisu.CreateCurveWithZExt( theTable, theHRow, theVRow, theZRow, theIsV2 );
    if curve:
        curve.SetTitle( theTitle )
        curve.SetColor( theColor )
        curve.SetMarker( theMarker )
        curve.SetLine( theLineType, theLineWidth )
        pass
    return curve

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Gen_s.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Gen_s.hh"

#include "VISU_Convertor.hxx"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_ScalarBarActor.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>

#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h> 
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkActor.h>

using namespace std;

Convertor::Convertor(const char* theFileName) : myConvertor(CreateConvertor(theFileName)){}


ScalarMap::ScalarMap(Convertor* theConvertor, const char* theMeshName, int theEntity, 
		     const char* theFieldName, int theIteration)
  : myScalarMap(NULL)
{
  if(VISU_Convertor* aConvertor = theConvertor->GetImpl()){
    VISU::PUnstructuredGridIDMapper anIDMapper = 
      aConvertor->GetTimeStampOnMesh(theMeshName,VISU::TEntity(theEntity),theFieldName,theIteration);
    if(anIDMapper){
      myScalarMap = VISU_ScalarMapPL::New();
      myScalarMap->SetUnstructuredGridIDMapper(anIDMapper);
    }
  }
}


View3D::View3D(){
  myRen = vtkRenderer::New();
  myRenWin = vtkRenderWindow::New();
  myRenWin->AddRenderer(myRen);
  myRenWin->SetSize(300, 300);
  myRen->Delete();
  myRen->GetActiveCamera()->ParallelProjectionOn();
  myIRen = vtkRenderWindowInteractor::New();
  myIRen->SetRenderWindow(myRenWin);
  myRenWin->Delete();
}

void View3D::SetPosition(int theX, int theY) {
  myRenWin->SetPosition(theX, theY);
}

View3D::~View3D(){
  myIRen->Delete();
}

void View3D::Display(ScalarMap* theScalarMap){
  if(VISU_ScalarMapPL* aScalarMap = theScalarMap->GetImpl()){
    aScalarMap->Update();
    
    myRen->RemoveAllViewProps();
    vtkActor* anActor = vtkActor::New();
    anActor->SetMapper(aScalarMap->GetMapper());
    
    VISU_ScalarBarActor * aScalarBar = VISU_ScalarBarActor::New();
    aScalarBar->SetLookupTable(aScalarMap->GetBarTable());
    
    myRen->AddActor(anActor);
    myRen->AddActor2D(aScalarBar);
    myRen->ResetCameraClippingRange();
    
    myRenWin->Render();
  }
}

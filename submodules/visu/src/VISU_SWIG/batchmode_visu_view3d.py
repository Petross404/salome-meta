#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : batchmode_visu_view3d.py
#  Module : VISU
#
import os
import time
import math
import VISU
import SALOMEDS
from batchmode_visu import *

aDelay = 0

#mySession = batchmode_visu.mySession

myVisu.SetCurrentStudy(salome.myStudy)

myViewManager = myVisu.GetViewManager();

myView = myViewManager.Create3DView();
myView.SetTitle("The window will be soon destroyed!")
print "myViewManager.Create3DView()"
time.sleep(aDelay)

myView.Maximize()
print "myView.Maximize()"
time.sleep(aDelay)

myView.Restore()
print "myView.Restore()"
time.sleep(aDelay)

myView.Minimize()
print "myView.Minimize()"
time.sleep(aDelay)

myViewManager.Destroy(myView)
print "myViewManager.Destroy(myView)"
time.sleep(aDelay)



medFile = "fra.med"
myFieldName = "VITESSE";

aMeshName ="LE VOLUME"
anEntity = VISU.NODE
aTimeStampId = 1

medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile
myResult = myVisu.ImportFile(medFile)



myView = myViewManager.Create3DView();

print "myView.SetBackground(...)"
aColor = SALOMEDS.Color(0.0,0.3,1.0)
myView.SetBackground(aColor);

myView.SetTitle("The viewer will display ScalarMap")
print "myViewManager.Create3DView()"

aScalarMap = myVisu.ScalarMapOnField(myResult,aMeshName,anEntity,myFieldName,aTimeStampId)

myView.Display(aScalarMap);
print "myView.Display(aScalarMap)"
myView.SetFocalPoint([0,0,0]);
print "myView.SetFocalPoint(...)"
myView.SetParallelScale(2);
print "myView.SetParallelScale(...)"
myView.FitAll();

aMax = aScalarMap.GetMax()
aMin = aScalarMap.GetMin()
aDelta = (aMax - aMin)/2.0
aNbColors = aScalarMap.GetNbColors()
aNbColors = 64
for i in range(2,aNbColors) :
    aScalarMap.SetNbColors(i)
    aX = aMin + aDelta*i/aNbColors
    aY = aMax - aDelta*i/aNbColors
    aScalarMap.SetRange(aX,aY)
    myView.Update();
    time.sleep(aDelay)

aScalarMap.SetRange(aMin,aMax)

print "myView.Update()"
myView.Update();
print "myView.FitAll()"
myView.FitAll();



myView = myViewManager.Create3DView();
myView.SetTitle("Here we will display CutPlanes")

print "myView.SetBackground(...)"
aColor = SALOMEDS.Color(0.0,0.7,0.0)
myView.SetBackground(aColor);

aCutPlanes = myVisu.CutPlanesOnField(myResult,aMeshName,anEntity,myFieldName,aTimeStampId)
print "myVisu.CutPlanesOnField(...)"

myView.DisplayOnly(aCutPlanes);
print "myView.DisplayOnly(aCutPlanes)"

aPoint = myView.GetPointOfView();
aPoint[0] = aPoint[0] + 10;
myView.SetPointOfView(aPoint);
print "myView.SetPointOfView(...)"
myView.ScaleView(VISU.View3D.YAxis,10.0);
myView.ScaleView(VISU.View3D.XAxis,3.0);
print "myView.ScaleView(...)"
time.sleep(aDelay)

myView.FitAll();

aNbPlanes = aCutPlanes.GetNbPlanes()
aNbPlanes = 30
aXAngle = aCutPlanes.GetRotateX()
aYAngle = aCutPlanes.GetRotateY()
anOrientation = aCutPlanes.GetOrientationType()
for i in range(aNbPlanes,1,-1) :
    aCutPlanes.SetNbPlanes(i)
    aX = math.pi/2.0*(aNbPlanes-i)/aNbPlanes
    aY = math.pi/2.0*(aNbPlanes-i)/aNbPlanes
    aCutPlanes.SetOrientation(anOrientation,aX,aY)
    myView.Update();
    time.sleep(aDelay)

aNbPlanes = 10
aCutPlanes.SetOrientation(VISU.CutPlanes.ZX,0,0)
for i in range(1,aNbPlanes) :
    aCutPlanes.SetNbPlanes(i)
    myView.Update();
    time.sleep(aDelay)

myView.SaveViewParams('AAA')
print "myView.SaveViewParams('AAA')"
time.sleep(aDelay)

aCutPlanes.SetOrientation(VISU.CutPlanes.XY,0,0)
myView.RemoveScale();

print "myView.Update()"
myView.Update();
print "myView.FitAll()"
myView.FitAll();



myView = myViewManager.Create3DView();
print "myViewManager.Create3DView()"

myView.SetTitle("IsoSurface's viewer")

print "myView.SetBackground(...)"
aColor = SALOMEDS.Color(1.0,0.7,0.0)
myView.SetBackground(aColor);
time.sleep(aDelay)

aIsoSurfaces = myVisu.IsoSurfacesOnField(myResult, aMeshName, anEntity, myFieldName, aTimeStampId)

myView.DisplayOnly(aIsoSurfaces);
myView.FitAll();
print "myView.DisplayOnly(aCutPlanes)"
time.sleep(aDelay)

aNbSurfaces = aIsoSurfaces.GetNbSurfaces()
aNbSurfaces = 32
for i in range(2,aNbSurfaces) :
    aIsoSurfaces.SetNbSurfaces(i)
    myView.Update();
    time.sleep(aDelay)

aIsoSurfaces.SetNbSurfaces(10)

print "myView.Update()"
myView.Update();
print "myView.FitAll()"
myView.FitAll();



myView = myViewManager.Create3DView();
myView.SetTitle("The viewer for CutLines")
print "myViewManager.Create3DView()"
time.sleep(aDelay)

print "myView.SetBackground(...)"
aColor = SALOMEDS.Color(0.7,0.7,0.7)
myView.SetBackground(aColor);
time.sleep(aDelay)

aCutLines = myVisu.CutLinesOnField(myResult, aMeshName, anEntity, myFieldName, aTimeStampId)
aCutLines.SetOrientation(VISU.CutPlanes.ZX,0,0)
aCutLines.SetOrientation2(VISU.CutPlanes.YZ,0,0)

myView.DisplayOnly(aCutLines);
myView.FitAll();
print "myView.DisplayOnly(aCutLines)"
time.sleep(aDelay)

aSObj = myStudy.FindObjectIOR(aCutLines.GetID())
aTable = myVisu.CreateTable( aSObj.GetID() )
print "myVisu.CreateTable(...)"

aTableView = myViewManager.CreateTableView(aTable)
aTableView.SetTitle('Changed Title')

aContainer = myVisu.CreateContainer()
print "myVisu.CreateContainer(...)"

aNbCurve = aTable.GetNbRows() - 1
for i in range(2,aNbCurve):
   aCurve = myVisu.CreateCurve( aTable, 1, i )
   print i, aCurve
   aContainer.AddCurve(aCurve)


myView = myViewManager.CreateXYPlot();
myView.SetTitle("The viewer for Curves from CutLines")
print "myViewManager.CreateXYPlot()"
time.sleep(aDelay)

myView.Display(aContainer)



myView = myViewManager.Create3DView();
myView.SetTitle("The viewer for CutSegment")
print "myViewManager.Create3DView()"
time.sleep(aDelay)

print "myView.SetBackground(...)"
aColor = SALOMEDS.Color(0.0,0.7,0.3)
myView.SetBackground(aColor);
time.sleep(aDelay)

aCutSegment = myVisu.CutSegmentOnField(myResult, aMeshName, anEntity, myFieldName, aTimeStampId)
aCutSegment.SetPoint1(0.0,0.0,0.0)
aCutSegment.SetPoint2(0.4,0.05,1.0)

myView.DisplayOnly(aCutSegment);
myView.FitAll();
print "myView.DisplayOnly(aCutSegment)"
time.sleep(aDelay)

aSObj = myStudy.FindObjectIOR(aCutSegment.GetID())
aTable = myVisu.CreateTable( aSObj.GetID() )
print "myVisu.CreateTable(...)"

aTableView = myViewManager.CreateTableView(aTable)
aTableView.SetTitle('Changed Title')

aContainer = myVisu.CreateContainer()
print "myVisu.CreateContainer(...)"

aCurve = myVisu.CreateCurve( aTable, 1, 2 )
print aCurve
aContainer.AddCurve(aCurve)


myView = myViewManager.CreateXYPlot();
myView.SetTitle("The viewer for Curve from CutSegment")
print "myViewManager.CreateXYPlot()"
time.sleep(aDelay)

myView.Display(aContainer)



myView = myViewManager.Create3DView();
myView.SetTitle("The viewer for Animation")
print "myViewManager.Create3DView()"
time.sleep(aDelay)

medFile = "TimeStamps.med"
myFieldName = "vitesse";

medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile
myResult = myVisu.ImportFile(medFile)

anAnim = myVisu.CreateAnimation(myView);
aSObj = myStudy.FindObjectIOR(myResult.GetID())
aSObj = aSObj.FindSubObject(1)[1]
aSObj = aSObj.FindSubObject(3)[1]
aSObj = aSObj.FindSubObject(2)[1]
anAnim.addField(aSObj)
anAnim.setPresentationType(0,VISU.TISOSURFACES)
print "Generate presentations"
anAnim.generatePresentations(0)
print "Generate frames"
anAnim.generateFrames()
print "Start Animation"
anAnim.setSpeed(99)
#anAnim.setCycling(1)
anAnim.startAnimation()
myView.FitAll()
while 1:
    time.sleep(1+aDelay)
    if not anAnim.isRunning():
        anAnim.stopAnimation()
        break

#anAnim.clearView()

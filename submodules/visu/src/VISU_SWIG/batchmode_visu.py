#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : batchmode_visu.py
#  Module : VISU
#
from batchmode_salome import *
import salome
import visu

myVisu = visu.Initialize(orb,naming_service,lcc,myStudyManager,myStudy,10)
if myVisu is None:
    raise RuntimeError, "myVisu is none, VISU component is not loaded  or found"
    
myVisu.SetCurrentStudy(salome.myStudy)
    
def try_mesh_parameters(theMeshPattern):
    aResult = []
    if theMeshPattern is None : return aResult ;
    theMeshPattern =  theMeshPattern._narrow(VISU.Mesh)
    if theMeshPattern is None : return aResult ;

    aTYPES = [VISU.POINT, VISU.WIREFRAME, VISU.SHADED, VISU.INSIDEFRAME, VISU.SHRINK]
    import copy; import os;
    for ind in aTYPES:
        aNewMesh = copy.deepcopy(theMeshPattern);
        aNewMesh.SetPresentationType(ind)
        aResult.append(aNewMesh)

    return aResult
    
def try_scalarmap_parameters(thePattern, theNum):
    aList = []
    if thePattern  is None : return aList 
    thePattern =  thePattern._narrow(VISU.ScalarMap)
    if thePattern  is None : return aList 
    SCALING = [VISU.LINEAR, VISU.LOGARITHMIC]
    import copy
    import random
    for ind in range(0,theNum):
        anObj = copy.deepcopy(thePattern);
        if ind%2 :
            #try incorrect value deliberately (but allowed by idl description)
            #try SetScalarMode(long)
            mode = random.randint(-100000,100000); #incorrect value deliberately
        else:
            #correct value of ScalarMode
            mode = random.randint(0, 3)

        print "\tSetScalarMode(" + str(mode) +")"
        anObj.SetScalarMode(mode)

        # --- SCALING ---
        scal = random.randint(0,1)
        print "\tSetScaling(" + str(SCALING[scal]) +")"
        anObj.SetScaling(SCALING[scal])
        
        # --- BOUNDARIES ---
        if ind%2 :
            alfa =  random.random()*random.randint(-100000,100000)
            betta = random.random()*random.randint(-100000,100000)
            aMin = alfa; aMax = betta
        else:
            #more correct set
            aPMin = thePattern.GetMin()
            aPMax = thePattern.GetMax()
            aLen = aPMax - aPMin
            alfa =  random.random()%0.5
            betta = random.random()%0.5
            aMin = alfa*aLen*random.randint(-1,1) + aPMin
            aMax = betta*aLen*random.randint(-1,1) + aPMax
        print "\tSetRange(" + str(aMin) + ", " + str(aMax) + ")"
        anObj.SetRange(aMin, aMax)
        aList.append(anObj) 
       
    return aList

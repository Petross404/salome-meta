#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU VISU_SWIG : binding of C++ implementation and Python
#  File   : visu_evolution.py
#  Module : VISU
#
import os
import VISU
from visu_gui import *

myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager();

myView = myViewManager.CreateXYPlot();
myView.SetTitle("The viewer for Evolution")
print "myViewManager.CreateXYPlot()"

medFile = "TimeStamps.med"
medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile
myResult = myVisu.ImportFile(medFile)

anEvolution = myVisu.CreateEvolution(myView);

aSObj = myStudy.FindObjectIOR(myResult.GetID())
aSObj = aSObj.FindSubObject(1)[1] # dom
aSObj = aSObj.FindSubObject(3)[1] # Fields
aSObj = aSObj.FindSubObject(2)[1] # vitesse
print "setField : ", anEvolution.setField(aSObj)

anEvolution.setComponentId(1) # x component

anEvolution.setPointId(500)

print "Show Evolution"
anEvolution.showEvolution()

myView.FitAll()

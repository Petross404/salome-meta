// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MultiResult_i.hh
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef __VISU_MULTI_RESULT_I_H__
#define __VISU_MULTI_RESULT_I_H__

#ifndef ENABLE_MULTIPR
#  define RESULT_CLASS_NAME Result_i
#else
#  define RESULT_CLASS_NAME MultiResult_i

#include "VISU_Result_i.hh"
#include "MULTIPR_Obj.hxx"

#include <set>

namespace VISU
{
  //----------------------------------------------------------------------------
  class MultiResult_i : public virtual Result_i
  {
    MultiResult_i(const MultiResult_i&);

  public:
    typedef Result_i TSuperClass;

    MultiResult_i(SALOMEDS::Study_ptr theStudy,
		  const ESourceId& theSourceId,
		  const ECreationId& theCreationId,
		  CORBA::Boolean theIsBuildImmediately,
		  CORBA::Boolean theIsBuildFields,
		  CORBA::Boolean theIsBuildMinMax,
		  CORBA::Boolean theIsBuildGroups);

    MultiResult_i();

    virtual
    ~MultiResult_i();

    virtual
    CORBA::Boolean
    IsDone();

    virtual
    VISU::Result::EntityNames* 
    GetPartNames(const char* theMeshName);

    virtual
    VISU::Result::Resolutions* 
    GetResolutions(const char* theMeshName, 
		   const char* thePartName);

    virtual
    VISU::Result::Resolution
    GetResolution(const char* theMeshName, 
		  const char* thePartName);

    virtual
    void 
    SetResolution(const char* theMeshName, 
		  const char* thePartName, 
		  VISU::Result::Resolution theResolution);


    typedef std::string TPartName;
    typedef std::vector<TPartName> TPartNames;

    typedef size_t TPartID;
    typedef std::string TPath;
    typedef std::string TMeshName;

    struct TPartInfo
    {
      TMeshName myMeshName;
      TPartName myName;
      TPartID myPartID;
      TPath myPath;
      TFileName myFileName;
    };

    typedef std::map<TFileName, TPartInfo> TPartInfos;
    TPartInfos myPartInfos;

    typedef std::map<TPartName, TFileName> TPartName2FileName;
    typedef std::map<TPartName, VISU::Result::Resolution> TPartName2Resolution;
    typedef std::set<VISU::Result::Resolution> TResolutions;

    typedef std::set<std::string> TRepresentationKey;
    typedef std::map<TRepresentationKey, PInput> TRepresentation2Input;

    typedef std::set<TPartName> TSubPartNames;
    typedef std::map<TPartName, TSubPartNames> TMainPart2SubPartNames;

  private:
    multipr::Obj myMultiprObj;
    TPartName2FileName myPartName2FileName;
    TPartName2Resolution myPartName2Resolution;
    TMainPart2SubPartNames myMainPart2SubPartNames;

    TRepresentation2Input myRepresentation2Input;

  protected:
    virtual
    Storable* 
    Build(SALOMEDS::SObject_ptr theSObject,
	  CORBA::Boolean theIsAtOnce = true) ;

    virtual
    void
    BuildDataTree(const std::string& theResultEntry);

  public:
    virtual 
    size_t
    IsPossible();

    virtual
    Storable* 
    Create(const char* theFileName);

    virtual 
    Storable*
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap,
	    const std::string& thePrefix,
	    CORBA::Boolean theIsMultiFile);

    //---------------------------------------------------------------
    virtual 
    bool
    Save(SALOMEDS::SComponent_ptr theComponent,
	 const std::string& theURL,
	 bool theIsMultiFile,
	 bool theIsASCII,
	 TFileNames& theFileNames,
	 TFileNames& theFiles);

    virtual 
    CORBA::Boolean 
    CanCopy(SALOMEDS::SObject_ptr theObject);

    virtual
    void
    ToStream(std::ostringstream& theStr);

    virtual
    PInput
    GetInput(const std::string& theMeshName = "",
	     VISU::Entity theEntity = VISU::NONE,
	     const std::string& theFieldName = "", 
	     CORBA::Long theTimeStampNumber = -1);
  };

}

#endif // ENABLE_MULTIPR

#endif // __VISU_MULTI_RESULT_I_H__

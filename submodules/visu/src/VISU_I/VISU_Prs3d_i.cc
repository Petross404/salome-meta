// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Prs3d_i.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Prs3d_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Gen_i.hh"
#include "VISU_PipeLine.hxx"

#include "VISU_Result_i.hh"
#include "VISU_Actor.h"

#include "SALOME_Event.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_MessageBox.h"

#include <VTKViewer_MarkerUtils.h>

#include <vtkActorCollection.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSet.h>
#include <vtkMapper.h>

#include <boost/bind.hpp>

#ifdef _DEBUG_
static int MYDEBUG = 1;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
VISU::Prs3d_i::Prs3d_i() :
  PrsObject_i(SALOMEDS::Study::_nil()),
  myActorCollection(vtkActorCollection::New()),
  myIsActiveSatate(true),
  myIsForcedHidden(false)
{
  if(MYDEBUG) MESSAGE("Prs3d_i::Prs3d_i - this = "<<this);
  myOffset[0] = myOffset[1] = myOffset[2] = 0;
  myActorCollection->Delete();

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  myMarkerType = (VISU::MarkerType)aResourceMgr->integerValue("VISU", "type_of_marker", 1); // dot
  myMarkerScale = (VISU::MarkerScale)aResourceMgr->integerValue("VISU", "marker_scale", 9); // 5 pixels
  myMarkerId = 0;
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SameAs(const Prs3d_i* theOrigin)
{
  if(Prs3d_i* anOrigin = const_cast<Prs3d_i*>(theOrigin)){
    VISU::TSetModified aModified(this);

    GetPipeLine()->SameAs(anOrigin->GetPipeLine());
    anOrigin->GetOffset(myOffset);

    myMarkerType = anOrigin->GetMarkerType();
    myMarkerScale = anOrigin->GetMarkerScale();
    myMarkerId = anOrigin->GetMarkerTexture();

    SetForcedHidden(anOrigin->IsForcedHidden());
  }
}


//----------------------------------------------------------------------------
namespace VISU
{
  struct TInvokeSignalEvent: public SALOME_Event
  {
    typedef boost::signal0<void> TSignal;
    const TSignal& mySignal;
    
    TInvokeSignalEvent(const TSignal& theSignal):
      mySignal(theSignal)
    {}
    
    virtual
    void
    Execute()
    {
      mySignal();
    }
  };
}

//----------------------------------------------------------------------------
VISU::Prs3d_i::~Prs3d_i()
{
  if(MYDEBUG) MESSAGE("Prs3d_i::~Prs3d_i - this = "<<this);
  ProcessVoidEvent(new TInvokeSignalEvent(myRemoveActorsFromRendererSignal));
}


//----------------------------------------------------------------------------
bool 
VISU::Prs3d_i
::SetInput(bool theReInit)
{
  if(GetCResult()){
    if(myMeshName != ""){
      myPreviousResult = myResult;
      myPreviousMeshName = myMeshName;
      return true;
    }
  }
  return false;
}


//----------------------------------------------------------------------------
void 
VISU::Prs3d_i
::OnRestoreInput()
{
  SetCResult(myPreviousResult);
  myMeshName = myPreviousMeshName;
}


//----------------------------------------------------------------------------
CORBA::Boolean 
VISU::Prs3d_i
::Apply(bool theReInit)
{
  try{
    if(SetInput(theReInit)){
      if(myActorCollection->GetNumberOfItems())
	UpdateActors();
      return true;
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  OnRestoreInput();
  return false;
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetCResult(VISU::Result_i* theResult)
{
  if(GetCResult() == theResult)
    return;

  if(theResult) {
    SetStudyDocument(theResult->GetStudyDocument());
  }
  
  VISU::TSetModified aModified(this);
  
  myResult = theResult;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetResultEntry(const std::string& theResultEntry)
{
  SetCResult(VISU::GetResult(GetStudyDocument(), theResultEntry));
}

//----------------------------------------------------------------------------
std::string
VISU::Prs3d_i
::GetResultEntry()
{
  if(VISU::Result_i* aResult = GetCResult())
    return aResult->GetEntry();
  return "";
}

//----------------------------------------------------------------------------
VISU::Result_i* 
VISU::Prs3d_i
::GetCResult() const 
{ 
  return myResult;
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetResultObject(VISU::Result_ptr theResult)
{
  SetCResult(dynamic_cast<VISU::Result_i*>(GetServant(theResult).in()));
}


//----------------------------------------------------------------------------
VISU::Result_ptr
VISU::Prs3d_i
::GetResultObject()
{
  return GetCResult()->_this();
}


//----------------------------------------------------------------------------
void 
VISU::Prs3d_i
::SetMeshName(const char* theMeshName)
{
  if(myMeshName == theMeshName)
    return;

  VISU::TSetModified aModified(this);

  myMeshName = theMeshName;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
char*
VISU::Prs3d_i
::GetMeshName()
{
  return CORBA::string_dup(myMeshName.c_str());
}


//----------------------------------------------------------------------------
std::string
VISU::Prs3d_i
::GetCMeshName() const
{
  return myMeshName;
}


//----------------------------------------------------------------------------
unsigned long int 
VISU::Prs3d_i
::GetMTime()
{
  unsigned long int aTime = myParamsTime.GetMTime();
  if(IsPipeLineExists())
    aTime = std::max(aTime, GetPipeLine()->GetMTime());
  return aTime;
}

//----------------------------------------------------------------------------
bool 
VISU::Prs3d_i
::GetActiveState()
{
  return myIsActiveSatate;
}

//----------------------------------------------------------------------------
void 
VISU::Prs3d_i
::SetActiveState(bool theState)
{
  myIsActiveSatate = theState;
}

//----------------------------------------------------------------------------
VISU::Storable* 
VISU::Prs3d_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  SetStudyDocument(aStudy);

  bool anIsExists = false;
  QString aResultEntry = VISU::Storable::FindValue(theMap,"myResultEntry", &anIsExists);
  if(!anIsExists){
    SALOMEDS::SObject_var aSObject = SALOMEDS::SObject::_duplicate(theSObject);
    for(; aSObject->Depth() > 2 && !aResultEntry.isEmpty(); aSObject = aSObject->GetFather()){
      CORBA::Object_var anObject = VISU::SObjectToObject(aSObject);
      if(CORBA::is_nil(anObject))
	continue;

      VISU::Result_var aResult = VISU::Result::_narrow(anObject);
      if(CORBA::is_nil(aResult))
	continue;

      CORBA::String_var anEntry = aSObject->GetID();
      aResultEntry = anEntry.in();
    }
  }
  SetResultEntry(aResultEntry.toLatin1().data());
  if(!GetCResult())
    return NULL;

  SetMeshName((const char*)VISU::Storable::FindValue(theMap,"myMeshName").toLatin1());
  SetName((const char*)VISU::Storable::FindValue(theMap,"myName").toLatin1(), false);
  myOffset[0] = VISU::Storable::FindValue(theMap,"myOffset[0]").toFloat();
  myOffset[1] = VISU::Storable::FindValue(theMap,"myOffset[1]").toFloat();
  myOffset[2] = VISU::Storable::FindValue(theMap,"myOffset[2]").toFloat();
  myMarkerType = VISU::MarkerType(VISU::Storable::FindValue(theMap,"myMarkerType").toInt());
  myMarkerScale = VISU::MarkerScale(VISU::Storable::FindValue(theMap,"myMarkerScale").toInt());
  myMarkerId = VISU::Storable::FindValue(theMap,"myMarkerId").toInt();
  myParamsTime.Modified();
  return this;
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::ToStream(std::ostringstream& theStr)
{
  Storable::DataToStream( theStr, "myResultEntry", GetResultEntry().c_str() );
  Storable::DataToStream( theStr, "myMeshName", GetCMeshName().c_str() );
  Storable::DataToStream( theStr, "myName", GetName().c_str() );
  Storable::DataToStream( theStr, "myOffset[0]", myOffset[0] );
  Storable::DataToStream( theStr, "myOffset[1]", myOffset[1] );
  Storable::DataToStream( theStr, "myOffset[2]", myOffset[2] );
  Storable::DataToStream( theStr, "myMarkerType", int(myMarkerType) );
  Storable::DataToStream( theStr, "myMarkerScale", int(myMarkerScale) );
  Storable::DataToStream( theStr, "myMarkerId", myMarkerId );
}


//----------------------------------------------------------------------------
SALOMEDS::SObject_var
VISU::Prs3d_i
::GetSObject()
{
  const SALOMEDS::Study_var& aStudy = GetStudyDocument();
  if(!CORBA::is_nil(aStudy.in())){
    CORBA::String_var anIOR = GetID();
    return aStudy->FindObjectIOR(anIOR);
  }
  return SALOMEDS::SObject::_nil();
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::Update()
{
  if(GetMTime() < myUpdateTime.GetMTime())
    return;

  if(MYDEBUG) MESSAGE("Prs3d_i::Update - this = "<<this);

  try{
    ProcessVoidEvent(new TVoidMemFunEvent<VISU_PipeLine>
		     (GetPipeLine(), &VISU_PipeLine::Update));
    myUpdateTime.Modified();
  }catch(std::exception&){
    throw;
  }catch(...){
    throw std::runtime_error("Prs3d_i::Update >> unexpected exception was caught!!!");
  }
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::CheckDataSet()
{
  vtkMapper *aMapper = GetPipeLine()->GetMapper();
  vtkDataSet *aDataSet = aMapper->GetInput();
  if (!aDataSet)
    throw std::runtime_error("There is no input data !!!");

  aMapper->Update();
  static float eps = VTK_LARGE_FLOAT * 0.1 ;
  if (!aDataSet->GetNumberOfCells())
    throw std::runtime_error("There are no visible elements");

  if (aDataSet->GetLength() > eps)
    throw std::runtime_error("Diagonal of the actor is too large !!!");
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::RemoveFromStudy()
{
  struct TEvent: public TInvokeSignalEvent
  {
    VISU::Prs3d_i* myRemovable;
  
    TEvent(const TSignal& theSignal,
	   VISU::Prs3d_i* theRemovable):
      TInvokeSignalEvent(theSignal),
      myRemovable(theRemovable)
    {}
  
    virtual
    void
    Execute()
    {
      //TInvokeSignalEvent::Execute();
      myRemovable->UnRegister();
    }
  };

  ProcessVoidEvent(new TEvent(myRemoveActorsFromRendererSignal, this));
}


//----------------------------------------------------------------------------
VISU_PipeLine*
VISU::Prs3d_i
::GetPipeLine() const
{
  if(!myPipeLine.GetPointer())
    throw std::runtime_error("VISU::Prs3d_i::myPipeLine == NULL !!!");

  return myPipeLine.GetPointer();
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetPipeLine(VISU_PipeLine* thePipeLine)
{
  myPipeLine = thePipeLine;
  if(thePipeLine)
    thePipeLine->Delete();
}

//----------------------------------------------------------------------------
bool 
VISU::Prs3d_i
::IsPipeLineExists()
{
  return myPipeLine.GetPointer() != NULL;
}

//----------------------------------------------------------------------------
VISU_PipeLine* 
VISU::Prs3d_i
::GetActorPipeLine()
{
  return GetPipeLine();
}

//----------------------------------------------------------------------------
vtkDataSet* 
VISU::Prs3d_i
::GetInput()
{
  return GetPipeLine()->GetInput();
}

//----------------------------------------------------------------------------
Handle(SALOME_InteractiveObject)
VISU::Prs3d_i
::GetIO()
{
  if( myIO.IsNull() )
    myIO = new SALOME_InteractiveObject(GetActorEntry().c_str(), "VISU", GetName().c_str());

  return myIO;
}

//----------------------------------------------------------------------------
std::string
VISU::Prs3d_i
::GetActorEntry()
{
  return GetEntry();
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::CreateActor(VISU_Actor* theActor)
{
  try{
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

    Handle(SALOME_InteractiveObject) anIO = GetIO();
    if(!anIO.IsNull() && anIO->hasEntry()){
      theActor->setIO(anIO);
    }

    Update();
    CheckDataSet();

    theActor->SetPrs3d(this);
    theActor->SetShrinkFactor(aResourceMgr->integerValue("VISU", "shrink_factor", 80)/100.);
    theActor->SetPosition(myOffset[0],myOffset[1],myOffset[2]);

    if( myMarkerType != VISU::MT_USER )
      theActor->SetMarkerStd( (VTK::MarkerType)myMarkerType, (VTK::MarkerScale)myMarkerScale );
    else if( myMarkerId > 0 ) {
      VTK::MarkerTexture aMarkerTexture;
      if( LoadMarkerTexture( myMarkerId, aMarkerTexture ) )
        theActor->SetMarkerTexture( myMarkerId, aMarkerTexture );
    }

    theActor->SetPipeLine(GetActorPipeLine());
    if(theActor->GetPipeLine() != GetPipeLine()){
       // To decrease actor'ss pipeline reference counter
      theActor->GetPipeLine()->Delete();
    }

    theActor->SetFactory(this);
    theActor->ConnectToFactory(myUpdateActorsSignal, myRemoveActorsFromRendererSignal);

    myActorCollection->AddItem(theActor);
    theActor->Delete();
  }catch(std::bad_alloc& ex){
    throw std::runtime_error("CreateActor >> No enough memory");
    throw ex;
  } catch(std::exception&){
    throw;
  }catch(...) {
    throw std::runtime_error("CreateActor >> unexpected exception was caught!!!");
  }
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::RemoveActor(VISU_ActorBase* theActor)
{
  if(MYDEBUG) MESSAGE("Prs3d_i::RemoveActor - this = "<<this<<"; theActor = "<<theActor<<"; "<<theActor->GetReferenceCount());
  myActorCollection->RemoveItem(theActor);
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::RemoveActors()
{
  if(MYDEBUG) MESSAGE("Prs3d_i::RemoveActors - this = "<<this);
  ProcessVoidEvent(new TInvokeSignalEvent(myRemoveActorsFromRendererSignal));
  myActorCollection->RemoveAllItems();
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::UpdateActor(VISU_ActorBase* theActor)
{
  if(VISU_Actor* anActor = dynamic_cast<VISU_Actor*>(theActor)){
    if(MYDEBUG) MESSAGE("Prs3d_i::UpdateActor - this = "<<this<<"; theActor = "<<anActor);
    anActor->SetPosition(myOffset[0],myOffset[1],myOffset[2]);

    if( myMarkerType != VISU::MT_USER )
      anActor->SetMarkerStd( (VTK::MarkerType)myMarkerType, (VTK::MarkerScale)myMarkerScale );
    else if( myMarkerId > 0 ) {
      VTK::MarkerTexture aMarkerTexture;
      if( LoadMarkerTexture( myMarkerId, aMarkerTexture ) )
        anActor->SetMarkerTexture( myMarkerId, aMarkerTexture );
    }

    anActor->ShallowCopyPL(GetPipeLine());
    anActor->highlight(anActor->isHighlighted());
  }
}

void
VISU::Prs3d_i
::UpdateActors()
{
  if(MYDEBUG) MESSAGE("Prs3d_i::UpdateActors - this = "<<this);
  Update();
  try {
    CheckDataSet();
  } catch( std::exception& ex ) {
    if( !IsForcedHidden() ) {
      if( HasVisibleActors() )
        SetForcedHidden( true );
      RemoveActors();
      SUIT_MessageBox::warning( 0,
                                QObject::tr("WRN_VISU"),
                                QString( ex.what() ),
                                QObject::tr("BUT_OK") );
    }
    return;
  }
  ProcessVoidEvent(new TInvokeSignalEvent(myUpdateActorsSignal));
}


//----------------------------------------------------------------------------
// Clipping planes
void
VISU::Prs3d_i
::RemoveAllClippingPlanes()
{
  GetPipeLine()->RemoveAllClippingPlanes();
}

//----------------------------------------------------------------------------
bool
VISU::Prs3d_i
::AddClippingPlane(vtkPlane* thePlane)
{
  return GetPipeLine()->AddClippingPlane(thePlane);
}

//----------------------------------------------------------------------------
vtkIdType
VISU::Prs3d_i
::GetNumberOfClippingPlanes() const
{
  return GetPipeLine()->GetNumberOfClippingPlanes();
}

//----------------------------------------------------------------------------
vtkPlane* 
VISU::Prs3d_i::
GetClippingPlane(vtkIdType theID) const
{
  return GetPipeLine()->GetClippingPlane(theID);
}

//----------------------------------------------------------------------------
void VISU::Prs3d_i::RemoveClippingPlane(vtkIdType theID)
{
  GetPipeLine()->RemoveClippingPlane(theID);
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetPlaneParam(double theDir[3], 
		double theDist, 
		vtkPlane* thePlane) 
{
  GetPipeLine()->SetPlaneParam(theDir, theDist, thePlane);
}


//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::GetBounds(double aBounds[6])
{
  GetPipeLine()->GetMapper()->GetBounds(aBounds);
}

//----------------------------------------------------------------------------
void 
VISU::Prs3d_i
::SetOffset(const CORBA::Float* theOffsets)
{
  VISU::TSetModified aModified(this);
  myOffset[0] = theOffsets[0];
  myOffset[1] = theOffsets[1];
  myOffset[2] = theOffsets[2];
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetOffset(CORBA::Float theDx,
	    CORBA::Float theDy,
	    CORBA::Float theDz)
{
  VISU::TSetModified aModified(this);
  myOffset[0] = theDx;
  myOffset[1] = theDy;
  myOffset[2] = theDz;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::GetOffset(CORBA::Float* theOffsets)
{
  theOffsets[0] = myOffset[0];
  theOffsets[1] = myOffset[1];
  theOffsets[2] = myOffset[2];
}

//----------------------------------------------------------------------------
void 
VISU::Prs3d_i
::GetOffset(CORBA::Float& theDx,
	    CORBA::Float& theDy,
	    CORBA::Float& theDz)
{
  theDx = myOffset[0];
  theDy = myOffset[1];
  theDz = myOffset[2];
}


//----------------------------------------------------------------------------
CORBA::Float
VISU::Prs3d_i
::GetMemorySize()
{
  // To calculate memory used by VISU PipeLine
  CORBA::Float aSize = GetPipeLine()->GetMemorySize();
  //cout<<"Prs3d_i::GetMemorySize - "<<this<<"; GetPipeLine = "<<aSize / (1024.0 * 1024.0)<<endl;

  // To calculate memory used by VISU Actos
  int anEnd = myActorCollection->GetNumberOfItems();
  for(int anId = 0; anId < anEnd; anId++)
    if(vtkObject* anObject = myActorCollection->GetItemAsObject(anId))
      if(VISU_Actor* anActor = dynamic_cast<VISU_Actor*>(anObject)){
	aSize += anActor->GetMemorySize();
	//cout<<"Prs3d_i::GetMemorySize - "<<this<<"; anActor = "<<aSize / (1024.0 * 1024.0)<<endl;
      }

  // Convert to mega bytes
  return aSize / (1024.0 * 1024.0);
}

int
VISU::Prs3d_i
::GetNumberOfActors()
{
  return myActorCollection->GetNumberOfItems();
}

bool
VISU::Prs3d_i
::HasVisibleActors()
{
  myActorCollection->InitTraversal();
  while( vtkActor* anActor = myActorCollection->GetNextActor() )
    if( VISU_Actor* aVISUActor = dynamic_cast<VISU_Actor*>( anActor ) )
      if( aVISUActor->GetVisibility() )
        return true;
  return false;
}

bool
VISU::Prs3d_i
::IsForcedHidden() const
{
  return myIsForcedHidden;
}

void
VISU::Prs3d_i
::SetForcedHidden( bool theFlag )
{
  myIsForcedHidden = theFlag;
}

//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetMarkerStd(VISU::MarkerType theMarkerType, VISU::MarkerScale theMarkerScale)
{
  myMarkerType = theMarkerType;
  myMarkerScale = theMarkerScale;
  if( myMarkerType == VISU::MT_POINT_SPRITE )
    myMarkerScale = VISU::MS_NONE;
  myParamsTime.Modified();
}
  
//----------------------------------------------------------------------------
void
VISU::Prs3d_i
::SetMarkerTexture(CORBA::Long theMarkerId)
{
  myMarkerType = VISU::MT_USER;
  myMarkerId = theMarkerId;
  myParamsTime.Modified();
}
  
//----------------------------------------------------------------------------
VISU::MarkerType
VISU::Prs3d_i
::GetMarkerType()
{
  return myMarkerType;
}

//----------------------------------------------------------------------------
VISU::MarkerScale
VISU::Prs3d_i
::GetMarkerScale()
{
  return myMarkerScale;
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::Prs3d_i
::GetMarkerTexture()
{
  return myMarkerId;
}

//----------------------------------------------------------------------------
bool
VISU::Prs3d_i
::LoadMarkerTexture(int theMarkerId, VTK::MarkerTexture& theMarkerTexture)
{
  VISU::VISU_Gen_i* aVisuGen = VISU::VISU_Gen_i::GetVisuGenImpl();
  if( !aVisuGen )
    return false;

  const VISU::StudyId2MarkerMap& aStudyId2MarkerMap = aVisuGen->GetMarkerMap();

  const SALOMEDS::Study_var& aStudy = GetStudyDocument();
  if( CORBA::is_nil( aStudy.in() ) )
    return false;

  int aStudyId = aStudy->StudyId();
  VISU::StudyId2MarkerMap::const_iterator aStudyId2MarkerIter = aStudyId2MarkerMap.find( aStudyId );
  if( aStudyId2MarkerIter == aStudyId2MarkerMap.end() )
    return false;

  VTK::MarkerMap aMarkerMap = aStudyId2MarkerIter->second;
  VTK::MarkerMap::const_iterator aMarkerIter = aMarkerMap.find( theMarkerId );
  if( aMarkerIter == aMarkerMap.end() )
    return false;

  theMarkerTexture = aMarkerIter->second.second;
  return true;
}

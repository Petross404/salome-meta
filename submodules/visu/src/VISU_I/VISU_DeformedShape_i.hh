// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_DeformedShape_i_HeaderFile
#define VISU_DeformedShape_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_MonoColorPrs_i.hh"

class VISU_DeformedShapePL;

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT DeformedShape_i : public virtual POA_VISU::DeformedShape,
					public virtual MonoColorPrs_i
  {
    static int myNbPresent;
    DeformedShape_i(const DeformedShape_i&);

  public:
    //----------------------------------------------------------------------------
    typedef MonoColorPrs_i TSuperClass;
    typedef VISU::DeformedShape TInterface;

    explicit
    DeformedShape_i(EPublishInStudyMode thePublishInStudyModep);

    virtual 
    void 
    SameAs(const Prs3d_i* theOrigin);

    virtual 
    ~DeformedShape_i();

    virtual 
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TDEFORMEDSHAPE;
    }

    virtual
    void 
    SetScale(CORBA::Double theScale);

    virtual
    CORBA::Double 
    GetScale();

    VISU_DeformedShapePL* 
    GetSpecificPL() const
    { 
      return myDeformedShapePL; 
    }
    
  protected:
    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual 
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    VISU_DeformedShapePL *myDeformedShapePL;

  public:
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    virtual 
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    static const std::string myComment;

    virtual 
    const char* 
    GetComment() const;

    virtual
    QString 
    GenerateName();

    virtual
    const char* 
    GetIconName();

    virtual
    void
    ToStream(std::ostringstream& theStr);

    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual
    void
    SetMapScale(double theMapScale = 1.0);

    virtual 
    VISU_Actor* 
    CreateActor(bool toSupressShrinking);

  };
}

#endif

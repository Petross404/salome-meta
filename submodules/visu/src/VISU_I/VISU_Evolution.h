// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Evolution.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_EVOLUTION_H
#define VISU_EVOLUTION_H

#include "VISUConfig.hh"

#include <VISU_ConvertorDef_impl.hxx>
#include <SALOMEDSClient_Study.hxx>

#include <QList>
#include <QMap>
#include <QPair>

#include <vtkType.h>

#include <string>

class Plot2d_ViewFrame;

namespace VISU
{
  class Result_i;
};

namespace VISU
{
  typedef QMap< vtkIdType, double >         Comp2Value;
  typedef QMapIterator< vtkIdType, double > Comp2ValueIterator;

  typedef QMap< vtkIdType, Comp2Value >                   Elem2Comp2Value;
  typedef QMapIterator< vtkIdType, Comp2Value >           Elem2Comp2ValueIterator;

  typedef QPair< double, std::string >                    TimeStampData;
  typedef QList< TimeStampData >                          TimeStampDataList;
  typedef QListIterator< TimeStampData >                  TimeStampDataListIterator;

  typedef QPair< QString, QString >                       ComponentData;
  typedef QList< ComponentData >                          ComponentDataList;
  typedef QListIterator< ComponentData >                  ComponentDataListIterator;

  typedef QList< double >                   TimeStampValueList;
  typedef QListIterator< double >           TimeStampValueListIterator;
};

class VISU_I_EXPORT VISU_Evolution
{
protected:
  bool _showEvolution();

public:
  VISU_Evolution( _PTR(Study) theStudy,
                  VISU::XYPlot_ptr theXYPlot = VISU::XYPlot::_nil() );
  ~VISU_Evolution();

  virtual VISU::VISUType    GetType() { return VISU::TNONE; }

  _PTR(Study)               getStudy() const { return myStudy; }

  bool                      setField( _PTR(SObject) theField );
  bool                      setField( SALOMEDS::SObject_ptr theField );

  int                       getNbPoints() const;

  void                      setPointId( int thePointId );
  int                       getPointId() const { return myPointId; }

  void                      setComponentId( int theComponentId );
  int                       getComponentId() const { return myComponentId; }

  bool                      showEvolution();

  void                      setViewer( Plot2d_ViewFrame* theView ) { myView = theView; }
  Plot2d_ViewFrame*         getViewer() const { return myView; }

  VISU::Result_i*           getResult() const { return myResult; }
  const VISU::PFieldImpl&   getField() const { return myFieldImpl; }

  std::string               getFieldEntry() const { return myFieldEntry; }
  std::string               getEvolutionEntry() const { return myEvolutionEntry; }
  std::string               getMeshName() const { return myMeshName; }
  VISU::TEntity             getEntity() const { return myEntity; }
  std::string               getFieldName() const { return myFieldName; }

  const VISU::TimeStampDataList& getTimeStampDataList() const { return myTimeStampDataList; }
  const VISU::ComponentDataList& getComponentDataList() const { return myComponentDataList; }

  void                      restoreFromStudy( SALOMEDS::SObject_ptr theObj );
  void                      restoreFromStudy( _PTR(SObject) theObj );

private:
  bool                      extractData( int thePointId,
                                         int theComponentId,
                                         VISU::TimeStampValueList& theTimeStampValueList );

private:
  _PTR(Study)               myStudy;
  _PTR(SObject)             myField;

  int                       myPointId;
  int                       myComponentId;

  Plot2d_ViewFrame*         myView;

  VISU::Result_i*           myResult;
  VISU::PFieldImpl          myFieldImpl;

  std::string               myFieldEntry;
  std::string               myEvolutionEntry;
  std::string               myMeshName;
  VISU::TEntity             myEntity;
  std::string               myFieldName;

  VISU::TimeStampDataList   myTimeStampDataList;
  VISU::ComponentDataList   myComponentDataList;
};


class VISU_I_EXPORT VISU_Evolution_i: public virtual POA_VISU::Evolution,
                                      public virtual VISU::Base_i
{
  VISU_Evolution* myEngine;
public:
  VISU_Evolution_i( SALOMEDS::Study_ptr theStudy,
                    VISU::XYPlot_ptr theXYPlot = VISU::XYPlot::_nil() );
  ~VISU_Evolution_i();

  virtual VISU::VISUType    GetType() { return VISU::TEVOLUTION; }

  virtual bool              setField(SALOMEDS::SObject_ptr theField);

  virtual void              setPointId(CORBA::Long thePointId);

  virtual void              setComponentId(CORBA::Long theComponentId);

  virtual CORBA::Boolean    showEvolution();

  virtual void              restoreFromStudy(SALOMEDS::SObject_ptr theField);
};

#endif //VISU_EVOLUTION_H

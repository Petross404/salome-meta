// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MonoColorPrs_i.hxx
//  Author : Vitaly Smetannikov
//  Module : VISU
//
#ifndef VISU_MonoColorPrs_i_HeaderFile
#define VISU_MonoColorPrs_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_ScalarMap_i.hh"

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT MonoColorPrs_i : public virtual POA_VISU::MonoColorPrs,
				       public virtual ScalarMap_i
  {
    MonoColorPrs_i(const MonoColorPrs_i&);

  public:
    //----------------------------------------------------------------------------
    typedef ScalarMap_i TSuperClass;
    typedef VISU::MonoColorPrs TInterface;

    explicit MonoColorPrs_i(EPublishInStudyMode thePublishInStudyModep);

    virtual void SameAs(const Prs3d_i* theOrigin);

    virtual ~MonoColorPrs_i();

    virtual CORBA::Boolean IsColored();

    virtual void ShowColored(CORBA::Boolean theColored);

    virtual SALOMEDS::Color GetColor();

    virtual void SetColor(const SALOMEDS::Color& theColor);

  protected:
    SALOMEDS::Color myColor;
    bool myIsColored;

  public:
    virtual Storable* Create(const std::string& theMeshName, 
			     VISU::Entity theEntity,
			     const std::string& theFieldName, 
			     CORBA::Long theTimeStampNumber);

    virtual void ToStream(std::ostringstream& theStr);

    virtual Storable* Restore(SALOMEDS::SObject_ptr theSObject,
			      const Storable::TRestoringMap& theMap);

    virtual VISU_Actor* CreateActor();

    virtual VISU_Actor* CreateActor(bool toSupressShrinking);

    virtual void UpdateActor(VISU_ActorBase* theActor);
  };
};
#endif

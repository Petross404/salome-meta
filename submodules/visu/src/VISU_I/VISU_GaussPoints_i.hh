// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_GaussPoints_i_HeaderFile
#define VISU_GaussPoints_i_HeaderFile

#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_GaussPtsActorFactory.h"

class VISU_LookupTable;
class VISU_ScalarBarActor;

class VISU_GaussPointsPL;

#include <QColor>

namespace VISU
{
  //! Class of the Gauss Points presentation.
  class VISU_I_EXPORT GaussPoints_i : public virtual POA_VISU::GaussPoints,
				      public virtual TGaussPtsActorFactory,
				      public virtual ColoredPrs3d_i
  {
    static int myNbPresent;
    GaussPoints_i(const GaussPoints_i&);

  public:
    //----------------------------------------------------------------------------
    typedef ColoredPrs3d_i TSuperClass;
    typedef VISU::GaussPoints TInterface;

    explicit
    GaussPoints_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    ~GaussPoints_i();

    virtual 
    VISU::VISUType 
    GetType() 
    {
      return VISU::TGAUSSPOINTS;
    }

    //----------------------------------------------------------------------------
    //! Gets memory size actually used by the presentation (Mb).
    virtual
    CORBA::Float
    GetMemorySize();

    //----------------------------------------------------------------------------
    CORBA::Long
    GetFaceLimit();

    void
    SetFaceLimit( CORBA::Long theFaceLimit );

    CORBA::Boolean 
    GetIsDeformed();

    void
    SetIsDeformed( CORBA::Boolean theIsDeformed );

    CORBA::Double
    GetScaleFactor();

    void
    SetScaleFactor( CORBA::Double theScaleFactor );

    //! Get color for Geometry mode of the presentation.
    SALOMEDS::Color
    GetColor();

    QColor
    GetQColor();

    //! Set color for Geometry mode of the presentation.
    void
    SetColor( const SALOMEDS::Color& theColor );

    void
    SetQColor( const QColor& theColor );

    //! Get flag indicating which mode of the presentation is active.
    /*! When Results mode is active, returns true. Geometry - false. */
    bool 
    GetIsColored();

    //! Get flag indicating which mode of the presentation is active.
    void
    SetIsColored( bool theIsColored );

    //! Set path to the image using for Main Point Sprite texture.
    bool
    SetMainTexture( const QString& theMainTexture );

    //! Get path to the image using for Main Point Sprite texture.
    QString
    GetQMainTexture();
    
    char*
    GetMainTexture();

    //! Set path to the image using for Alpha Point Sprite texture.
    bool
    SetAlphaTexture( const QString& theAlphaTexture );

    //! Get path to the image using for Alpha Point Sprite texture.
    QString
    GetQAlphaTexture();

    char*
    GetAlphaTexture();

    //! Convert Main and AlphaMask images to VTI format and set them to pipeline.
    void
    SetQTextures( const QString& theMainTexture,
		  const QString& theAlphaTexture );

    void
    SetTextures( const char* theMainTexture, 
		 const char* theAlphaTexture );

    //! Redirect the request to VISU_GaussPointsPL::SetAlphaThreshold.
    void
    SetAlphaThreshold(CORBA::Double theAlphaThreshold);

    //! Redirect the request to VISU_GaussPointsPL::GetAlphaThreshold.
    CORBA::Double
    GetAlphaThreshold();

    //! Redirect the request to VISU_GaussPointsPL::SetResolution.
    void
    SetResolution(CORBA::Long theResolution);

    //! Redirect the request to VISU_GaussPointsPL::GetResolution.
    CORBA::Long
    GetResolution();

    void
    SetPrimitiveType(VISU::GaussPoints::PrimitiveType thePrimitiveType);

    VISU::GaussPoints::PrimitiveType
    GetPrimitiveType();

    //! Redirect the request to VISU_GaussPointsPL::GetMaximumSupportedSize.
    double 
    GetMaximumSupportedSize();

    //! Redirect the request to VISU_GaussPointsPL::SetClamp.
    void
    SetClamp(CORBA::Double theClamp);

    //! Redirect the request to VISU_GaussPointsPL::GetClamp.
    CORBA::Double
    GetClamp();

    //! Redirect the request to VISU_GaussPointsPL::SetSize.
    void
    SetGeomSize(CORBA::Double theGeomSize);

    //! Redirect the request to VISU_GaussPointsPL::GetSize.
    CORBA::Double 
    GetGeomSize();

    //! Redirect the request to VISU_GaussPointsPL::SetMinSize.
    void
    SetMinSize(CORBA::Double theMinSize);

    //! Redirect the request to VISU_GaussPointsPL::GetMinSize.
    CORBA::Double 
    GetMinSize();

    //! Redirect the request to VISU_GaussPointsPL::SetMaxSize.
    void
    SetMaxSize(CORBA::Double theMaxSize);

    //! Redirect the request to VISU_GaussPointsPL::GetMaxSize.
    CORBA::Double 
    GetMaxSize();

    //! Redirect the request to VISU_GaussPointsPL::SetMagnification.
    void
    SetMagnification(CORBA::Double theMagnification);

    //! Redirect the request to VISU_GaussPointsPL::GetMagnification.
    CORBA::Double
    GetMagnification();

    //! Redirect the request to VISU_GaussPointsPL::SetMagnificationIncrement.
    void
    SetMagnificationIncrement(CORBA::Double theIncrement);

    //! Redirect the request to VISU_GaussPointsPL::GetMagnificationIncrement.
    CORBA::Double
    GetMagnificationIncrement();

    //! Set flag indicating which scalar bar is active.
    void
    SetIsActiveLocalScalarBar(CORBA::Boolean theIsActiveLocalScalarBar);

    //! Get flag indicating which scalar bar is active.
    CORBA::Boolean
    GetIsActiveLocalScalarBar();

    //! Set flag indicating visibility of global scalar bar.
    void
    SetIsDispGlobalScalarBar(CORBA::Boolean theIsDispGlobalScalarBar);

    //! Get flag indicating visibility of global scalar bar.
    CORBA::Boolean
    GetIsDispGlobalScalarBar();

    //! Redirect the request to VISU_GaussPointsPL::SetBicolor.
    void
    SetBiColor(CORBA::Boolean theIsBiColor);

    //! Redirect the request to VISU_GaussPointsPL::GetBicolor.
    CORBA::Boolean
    GetBiColor();

    //! Set value of the distance between global and local scalar bars.
    void
    SetSpacing(CORBA::Double theSpacing);

    //! Get value of the distance between global and local scalar bars.
    CORBA::Double
    GetSpacing();

    //! Let know is the global range is already calculated
    bool
    IsGlobalRangeDefined() const;

    virtual
    CORBA::Double 
    GetSourceMin();

    virtual
    CORBA::Double 
    GetSourceMax();

    virtual 
    void
    SetSourceRange();

    VISU_GaussPointsPL* 
    GetSpecificPL() const
    { 
      return myGaussPointsPL; 
    }
    
    virtual CORBA::Boolean IsBarVisible() { return myShowBar; }
       
    virtual void SetBarVisible(CORBA::Boolean theVisible);

    //----------------------------------------------------------------------------
    //! Sets initial source geometry
    virtual
    void
    SetSourceGeometry();

    //! Add geometry of mesh as group. \retval the id of added group.
    virtual 
    void
    AddMeshOnGroup(const char* theGroupName);
    
    //! Removes all geometries.
    virtual
    void
    RemoveAllGeom();

    virtual
    double
    GetComponentMin(vtkIdType theCompID);

    virtual
    double
    GetComponentMax(vtkIdType theCompID);
    
  protected:
    //! Redefines VISU_ColoredPrs3d_i::DoSetInput
    virtual
    void
    DoSetInput(bool theIsInitilizePipe, bool theReInit);

    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

    virtual 
    const char* 
    GetIconName();

    virtual 
    VISU_PipeLine* 
    GetActorPipeLine();
    
    bool
    OnCreateActor(VISU_GaussPtsAct* theActor);

    VISU_GaussPtsAct1* 
    OnCreateActor1();

    VISU_GaussPtsAct2* 
    OnCreateActor2();

    void
    UpdateScalarBar(VISU_ScalarBarActor *theScalarBar,
		    VISU_LookupTable* theLookupTable);

  protected:
    VISU_GaussPointsPL *myGaussPointsPL;
    
    bool myIsDispGlobalScalarBar;
    bool myIsActiveLocalScalarBar;
    QColor myColor;
    double mySpacing;
    int myFaceLimit;

    QString myMainTexture;
    QString myAlphaTexture;

  public:
    static 
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);
    virtual
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString
    GenerateName();

    virtual
    void
    ToStream(std::ostringstream& theStr);

    virtual 
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    //----------------------------------------------------------------------------
    virtual 
    VISU_Actor* 
    CreateActor();

    virtual 
    VISU_GaussPtsAct2* 
    CloneActor(VISU_GaussPtsAct1* theActor);

    virtual
    void
    UpdateActor(VISU_ActorBase* theActor);

    virtual 
    void
    UpdateFromActor(VISU_GaussPtsAct* theActor);
    
    private:
      bool myShowBar;
  };
}

#endif

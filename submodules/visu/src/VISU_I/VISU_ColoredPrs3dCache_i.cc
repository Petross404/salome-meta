// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ColoredPrs3dCache_i.cc
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifdef WNT
#define NOGDI
#endif

#include "VISU_ColoredPrs3dCache_i.hh"

#include "VISU_ColoredPrs3dHolder_i.hh"
#include "VISU_ColoredPrs3dFactory.hh"

#include "VISU_ViewManager_i.hh"
#include "VISU_View_i.hh"
#include "VISU_Actor.h"

#include "VISU_PipeLine.hxx"
#include "VISU_Tools.h"

#include "SALOME_Event.h"

#include "VTKViewer_Algorithm.h"
#include "SVTK_Functor.h"
#include "SVTK_ViewWindow.h"

#include "SUIT_ResourceMgr.h"

#include <vtkRenderWindow.h>

#include <QtGlobal>

#include "utilities.h"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

namespace
{
  //----------------------------------------------------------------------------
  inline
  bool
  IsSameField(const VISU::ColoredPrs3dHolder::BasicInput& theReferenceInput,
	      const VISU::ColoredPrs3dHolder::BasicInput& thePrs3dInput)
  {
    return thePrs3dInput.myResult->_is_equivalent(theReferenceInput.myResult) &&
      thePrs3dInput.myEntity == theReferenceInput.myEntity &&
      !strcmp(thePrs3dInput.myFieldName.in(), theReferenceInput.myFieldName.in());
  }


  //----------------------------------------------------------------------------
  inline
  bool
  IsSameTimeStamp(const VISU::ColoredPrs3dHolder::BasicInput& theReferenceInput,
		  const VISU::ColoredPrs3dHolder::BasicInput& thePrs3dInput)
  {
    return IsSameField(theReferenceInput, thePrs3dInput) &&
      thePrs3dInput.myTimeStampNumber == theReferenceInput.myTimeStampNumber;
  }
  
  
  //----------------------------------------------------------------------------
  VISU::ColoredPrs3d_i*
  FindSameFieldPrs(const VISU::TColoredPrs3dHolderMap& theHolderMap,
		   const VISU::ColoredPrs3dHolder::BasicInput& theInput,
		   VISU::VISUType theType)
  {
    VISU::TColoredPrs3dHolderMap::const_iterator aHolderIter = theHolderMap.begin();
    VISU::TColoredPrs3dHolderMap::const_iterator aHolderIterEnd = theHolderMap.end();
    for(; aHolderIter != aHolderIterEnd; aHolderIter++){
      const VISU::TLastVisitedPrsList& aPrsList = aHolderIter->second;
      VISU::TLastVisitedPrsList::const_iterator aPrsIter = aPrsList.begin();
      VISU::TLastVisitedPrsList::const_iterator aPrsIterEnd = aPrsList.end();
      for(; aPrsIter != aPrsIterEnd; aPrsIter++){
	VISU::TPrs3dPtr aPrs3d = *aPrsIter;
	if(aPrs3d->GetType() != theType)
	  break;
	VISU::ColoredPrs3dHolder::BasicInput_var anInput = aPrs3d->GetBasicInput();
	if(IsSameField(theInput, anInput))
	  return aPrs3d;
      }
    }
    return NULL;
  }
  
  
  //----------------------------------------------------------------------------
  CORBA::Float
  EstimateMemorySize(const VISU::TColoredPrs3dHolderMap& theHolderMap,
		     const VISU::ColoredPrs3dHolder::BasicInput& theInput,
		     VISU::VISUType theType,
		     const size_t theRawEstimatedMemorySize)
  {
    VISU::ColoredPrs3d_i* aPrs3d = FindSameFieldPrs(theHolderMap, theInput, theType);
    if(aPrs3d)
      return aPrs3d->GetMemorySize();
    return CORBA::Float(theRawEstimatedMemorySize/(1024.0*1024.0)); // convert to Mb
  }


  //----------------------------------------------------------------------------
  bool
  SelectPrs3dToBeDeleted(CORBA::Float theRequiredMemory,
			 const std::string& theActiveHolderEntry,
			 const VISU::TColoredPrs3dHolderMap& theHolderMap,
			 VISU::TColoredPrs3dHolderMap& theColoredPrs3dHolderMap)
  {
    if( theRequiredMemory < 1.0 / VTK_LARGE_FLOAT )
      return false;
    
    VISU::TColoredPrs3dHolderMap::const_iterator aHolderIter = theHolderMap.begin();
    VISU::TColoredPrs3dHolderMap::const_iterator aHolderIterEnd = theHolderMap.end();
    
    // To calculate the maximum length of the cache history among all holders
    int anIteration = 0;
    for( ; aHolderIter != aHolderIterEnd; aHolderIter++ ){
      if( aHolderIter->first == theActiveHolderEntry )
	continue;
      const VISU::TLastVisitedPrsList& aPrsList = aHolderIter->second;
      anIteration = qMax( (int)aPrsList.size() - 1, anIteration );
    }

    // To estimate what amount of memory can be obtained
    // by cleaning of non-active holder's presentation
    CORBA::Float aGatheredMemory = 0.0;
    for(; anIteration > 0; anIteration--){ // To take into account only non-devices
      aHolderIter = theHolderMap.begin();
      for(; aHolderIter != aHolderIterEnd; aHolderIter++ ){
	const std::string& aHolderEntry = aHolderIter->first;
	if( aHolderEntry == theActiveHolderEntry )
	  continue;
	const VISU::TLastVisitedPrsList& aPrsList = aHolderIter->second;
	if( anIteration < aPrsList.size() ){
	  VISU::TPrs3dPtr aPrs3d = aPrsList[anIteration];
	  aGatheredMemory += aPrs3d->GetMemorySize();
	  theColoredPrs3dHolderMap[aHolderEntry].push_back(aPrs3d);
	  if( aGatheredMemory > theRequiredMemory )
	    return true;
	}
      }
    }
    
    // To estimate what amount of memory can be obtained
    // by cleaning of active holder's presentation
    if( theActiveHolderEntry != "" ){
      aHolderIter = theHolderMap.find( theActiveHolderEntry );
      if(aHolderIter == theHolderMap.end())
	return false;

      const VISU::TLastVisitedPrsList& aPrsList = aHolderIter->second;

      // To prefere "move" action instead of destroy / create presentation
      if(aPrsList.back()->GetMemorySize() >= theRequiredMemory)
	return false;

      VISU::TLastVisitedPrsList::const_reverse_iterator aPrsIter = aPrsList.rbegin();
      // Do not porcess first item to avoid of the device destruction
      VISU::TLastVisitedPrsList::const_reverse_iterator aPrsIterEnd = aPrsList.rend()++;
      for(; aPrsIter != aPrsIterEnd; aPrsIter++){
	VISU::TPrs3dPtr aPrs3d = *aPrsIter;
	aGatheredMemory += aPrs3d->GetMemorySize();
	theColoredPrs3dHolderMap[theActiveHolderEntry].push_back(aPrs3d);
	if( aGatheredMemory > theRequiredMemory )
	  return true;
      }
    }
    
    return false;
  }


  //----------------------------------------------------------------------------
  void
  ErasePrs3d(VISU::TLastVisitedPrsList& thePrsList,
	     const VISU::TPrs3dPtr& thePrs3d)
  {
    VISU::TLastVisitedPrsList::iterator anIter = thePrsList.begin();
    VISU::TLastVisitedPrsList::iterator aEndIter = thePrsList.end();
    for(; anIter != aEndIter; anIter++){
      VISU::TPrs3dPtr aPrs3d = *anIter;
      if(aPrs3d == thePrs3d)
	thePrsList.erase(anIter);
    }
  }
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dCache_i
::ColoredPrs3dCache_i(SALOMEDS::Study_ptr theStudy,
		      bool thePublishInStudy):
  RemovableObject_i()
{
  if(MYDEBUG) MESSAGE("ColoredPrs3dCache_i::ColoredPrs3dCache_i - this = "<<this);
  SetStudyDocument(theStudy);

  SetName(GetFolderName(), false);

  if( thePublishInStudy )
  {
    CORBA::String_var anIOR = GetID();
    SALOMEDS::SComponent_var aSComponent = VISU::FindOrCreateVisuComponent(theStudy);
    CORBA::String_var aFatherEntry = aSComponent->GetID();
    CreateAttributes(GetStudyDocument(), aFatherEntry.in(), "", anIOR.in(), GetName(), "", "", true);
  }

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  int aMemoryMode = aResourceMgr->integerValue( "VISU", "cache_memory_mode", 0 );
  SetMemoryMode( aMemoryMode == 0 ? VISU::ColoredPrs3dCache::MINIMAL : VISU::ColoredPrs3dCache::LIMITED );

  float aLimitedMemory = aResourceMgr->doubleValue( "VISU", "cache_memory_limit", 1024.0 );
  SetLimitedMemory( aLimitedMemory );
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dCache_i
::~ColoredPrs3dCache_i() 
{
  if(MYDEBUG) MESSAGE("ColoredPrs3dCache_i::~ColoredPrs3dCache_i - this = "<<this);
}


//----------------------------------------------------------------------------
const string VISU::ColoredPrs3dCache_i::myComment = "COLOREDPRS3DCACHE";

const char* 
VISU::ColoredPrs3dCache_i
::GetComment() const 
{ 
  return myComment.c_str();
}


//----------------------------------------------------------------------------
CORBA::Float
VISU::ColoredPrs3dCache_i
::GetMemorySize()
{ 
  CORBA::Float aMemoryUsed = 0.0;
  TColoredPrs3dHolderMap::const_iterator aHolderIter = myHolderMap.begin();
  TColoredPrs3dHolderMap::const_iterator aHolderIterEnd = myHolderMap.end();
  for(; aHolderIter != aHolderIterEnd; aHolderIter++){
    const TLastVisitedPrsList& aPrsList = aHolderIter->second;
    TLastVisitedPrsList::const_iterator aPrsIter = aPrsList.begin();
    TLastVisitedPrsList::const_iterator aPrsIterEnd = aPrsList.end();
    for(; aPrsIter != aPrsIterEnd; aPrsIter++){
      if(TPrs3dPtr aPrs3d = *aPrsIter)
	aMemoryUsed += aPrs3d->GetMemorySize();
    }
  }
  return aMemoryUsed; 
}


//----------------------------------------------------------------------------
CORBA::Float
VISU::ColoredPrs3dCache_i
::GetDeviceMemorySize()
{ 
  CORBA::Float aMemoryUsed = 0.0;
  TColoredPrs3dHolderMap::const_iterator aHolderIter = myHolderMap.begin();
  TColoredPrs3dHolderMap::const_iterator aHolderIterEnd = myHolderMap.end();
  for(; aHolderIter != aHolderIterEnd; aHolderIter++){
    const TLastVisitedPrsList& aPrsList = aHolderIter->second;
    if(TPrs3dPtr aPrs3d = aPrsList.front())
      aMemoryUsed += aPrs3d->GetMemorySize();
  }
  return aMemoryUsed; 
}


//----------------------------------------------------------------------------
int
VISU::ColoredPrs3dCache_i
::IsPossible(VISU::VISUType theType,
	     const VISU::ColoredPrs3dHolder::BasicInput& theInput,
	     CORBA::Float& theRequiredMemory,
	     const std::string theHolderEntry)
{
  size_t aRawEstimatedMemorySize = VISU::CheckIsPossible(theType, theInput, true);
  if(aRawEstimatedMemorySize > 0){
    if(GetMemoryMode() == VISU::ColoredPrs3dCache::LIMITED){
      CORBA::Float aMemoryUsed = GetMemorySize();
      CORBA::Float aMemoryLimit = GetLimitedMemory();
      CORBA::Float aMemoryNeeded = EstimateMemorySize(myHolderMap,
						      theInput,
						      theType,
						      aRawEstimatedMemorySize);

      if( aMemoryUsed + aMemoryNeeded < aMemoryLimit )
	return true;
      
      theRequiredMemory = aMemoryNeeded - ( aMemoryLimit - aMemoryUsed );
      TColoredPrs3dHolderMap aColoredPrs3dHolderMap;
      return SelectPrs3dToBeDeleted(theRequiredMemory, 
				    theHolderEntry, 
				    myHolderMap,
				    aColoredPrs3dHolderMap);
    }
  }
  return aRawEstimatedMemorySize > 0;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dCache::EnlargeType
VISU::ColoredPrs3dCache_i
::GetRequiredMemory(VISU::VISUType theType,
		    const VISU::ColoredPrs3dHolder::BasicInput& theInput,
		    CORBA::Float& theRequiredMemory)
{
  VISU::ColoredPrs3dCache::EnlargeType anEnlargeType = VISU::ColoredPrs3dCache::NO_ENLARGE;

  size_t aRawEstimatedMemorySize = VISU::CheckIsPossible(theType, theInput, true);
  if(aRawEstimatedMemorySize > 0){
    if(GetMemoryMode() == VISU::ColoredPrs3dCache::LIMITED){
      CORBA::Float aMemoryUsed = GetDeviceMemorySize();
      CORBA::Float aMemoryLimit = GetLimitedMemory();
      CORBA::Float aMemoryNeeded = EstimateMemorySize(myHolderMap,
						      theInput,
						      theType,
						      aRawEstimatedMemorySize);
      if( aMemoryUsed + aMemoryNeeded < aMemoryLimit )
	return anEnlargeType;

      theRequiredMemory = int( aMemoryUsed + aMemoryNeeded ) + 1;

      size_t aMb = 1024 * 1024;
      double aFreeMemory = double(VISU_PipeLine::GetAvailableMemory(8192*(double)aMb)) / double(aMb);
      anEnlargeType = aMemoryNeeded < aFreeMemory ?
	VISU::ColoredPrs3dCache::ENLARGE : VISU::ColoredPrs3dCache::IMPOSSIBLE;
    }
  }
  return anEnlargeType;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dCache_i*
VISU::ColoredPrs3dCache_i
::GetInstance_i(SALOMEDS::Study_ptr theStudy)
{
  std::string aFolderName = VISU::ColoredPrs3dCache_i::GetFolderName();
  SALOMEDS::SObject_var aSObject = theStudy->FindObject(aFolderName.c_str());
  if (CORBA::is_nil(aSObject)) {
    aSObject = theStudy->FindObject("3D Cache System");
    if (!CORBA::is_nil(aSObject)) {
      SALOMEDS::StudyBuilder_var aBuilder = theStudy->NewBuilder();
      SALOMEDS::GenericAttribute_var anAttr = aBuilder->FindOrCreateAttribute( aSObject, "AttributeName" );
      SALOMEDS::AttributeName_var aNameAttr = SALOMEDS::AttributeName::_narrow( anAttr );
      aNameAttr->SetValue( GetFolderName().c_str() );
    }
  }
  if(!CORBA::is_nil(aSObject)){
    CORBA::Object_var anObject = aSObject->GetObject();
    VISU::ColoredPrs3dCache_var aCache = VISU::ColoredPrs3dCache::_narrow(anObject);
    if(!CORBA::is_nil(aCache))
      return dynamic_cast<VISU::ColoredPrs3dCache_i*>(GetServant(aCache).in());
  }
  
  return new VISU::ColoredPrs3dCache_i(theStudy);
}


VISU::ColoredPrs3dCache_ptr
VISU::ColoredPrs3dCache_i
::GetInstance(SALOMEDS::Study_ptr theStudy)
{
  VISU::ColoredPrs3dCache_i* aServant = GetInstance_i(theStudy);
  VISU::ColoredPrs3dCache_var anObject = aServant->_this();
  return anObject._retn();
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dHolder_ptr
VISU::ColoredPrs3dCache_i
::CreateHolder(VISU::VISUType theType,
	       const VISU::ColoredPrs3dHolder::BasicInput& theInput)
{
  if(MYDEBUG) MESSAGE ("CreateHolder "<<theType);
  CORBA::Float aRequiredMemory = 0.0;
  if(IsPossible(theType, theInput, aRequiredMemory, "")){
    if(VISU::ColoredPrs3d_i* aColoredPrs3d = CreateColoredPrs3d(theType, theInput)){
      VISU::ColoredPrs3dHolder_i* aHolder = new VISU::ColoredPrs3dHolder_i(*this);
      std::string aComment = std::string("myComment=") + aColoredPrs3d->GetComment();
      std::string aName = (const char*)aColoredPrs3d->GenerateName().toLatin1();
      aHolder->PublishInStudy(aName, aColoredPrs3d->GetIconName(), aComment);
      RegisterInHolder(aColoredPrs3d, aHolder->GetEntry());
      if( aRequiredMemory > 1.0 / VTK_LARGE_FLOAT )
	ClearMemory( aRequiredMemory, aHolder->GetEntry() );
      return aHolder->_this();
    }
  }
  return VISU::ColoredPrs3dHolder::_nil();
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dCache_i
::SetMemoryMode(VISU::ColoredPrs3dCache::MemoryMode theMode)
{
  VISU::ColoredPrs3dCache::MemoryMode aCurrentMode = GetMemoryMode();

  if( aCurrentMode == VISU::ColoredPrs3dCache::LIMITED &&
      theMode == VISU::ColoredPrs3dCache::MINIMAL )
  {
    ClearCache();
  }

  myMemoryMode = theMode;
  GetStudyDocument()->Modified();
}

VISU::ColoredPrs3dCache::MemoryMode
VISU::ColoredPrs3dCache_i
::GetMemoryMode()
{ 
  return myMemoryMode; 
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dCache_i
::SetLimitedMemory(CORBA::Float theMemorySize)
{
  if( fabs( myLimitedMemory - theMemorySize ) < 1 / VTK_LARGE_FLOAT )
    return;

  size_t aMb = 1024 * 1024;
  double aFreeMemory = double(VISU_PipeLine::GetAvailableMemory(8192*(double)aMb)) / double(aMb);
  CORBA::Float aMemoryUsed = GetDeviceMemorySize();
  CORBA::Float aMemoryNeeded = theMemorySize - aMemoryUsed;
  if( aMemoryNeeded > aFreeMemory )
    return;

  ClearCache(theMemorySize);
  myLimitedMemory = theMemorySize;
  GetStudyDocument()->Modified();
}

CORBA::Float
VISU::ColoredPrs3dCache_i
::GetLimitedMemory()
{ 
  return myLimitedMemory; 
}


void
VISU::ColoredPrs3dCache_i
::RemoveFromStudy() 
{
  CORBA::String_var anIOR = GetID();
  SALOMEDS::SObject_var aSObject = GetStudyDocument()->FindObjectIOR(anIOR.in());
  VISU::RemoveFromStudy(aSObject, false, true);
  UnRegister();
}

std::string
VISU::ColoredPrs3dCache_i
::GetFolderName() 
{ 
  //return "3D Cache System"; 
  return "Presentations"; 
}

//----------------------------------------------------------------------------
VISU::ColoredPrs3d_i*
VISU::ColoredPrs3dCache_i
::CreateColoredPrs3d(VISU::VISUType theType,
		     VISU::ColoredPrs3dHolder::BasicInput theInput)
{
  VISU::ColoredPrs3d_i* aPrs3d = VISU::CreatePrs3d_i(theType, GetStudyDocument(), ColoredPrs3d_i::ERegisterInCache);
  aPrs3d->SetResultObject( theInput.myResult );  
  aPrs3d->SetMeshName( theInput.myMeshName );  
  aPrs3d->SetEntity( theInput.myEntity );  
  aPrs3d->SetFieldName( theInput.myFieldName );  
  aPrs3d->SetTimeStampNumber( theInput.myTimeStampNumber );
  if(aPrs3d->Apply( false ))
    return aPrs3d;
  return NULL;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3d_i*
VISU::ColoredPrs3dCache_i
::RegisterInHolder(VISU::ColoredPrs3d_i* thePrs3d,
		   const std::string& theHolderEntry)
{
  if(MYDEBUG) MESSAGE("RegisterInHolder "<<theHolderEntry.c_str()<<" "<<thePrs3d);
  if(thePrs3d){
    TPrs3dPtr aPrs3d(thePrs3d);
    myHolderMap[theHolderEntry].push_front(aPrs3d);  
    thePrs3d->SetHolderEntry( theHolderEntry );
    thePrs3d->UnRegister();
  }

  // It seems strange but calling this method here (note that is should be called
  // for the whole cache, not for the specified prs) is fully enough to avoid the bug
  // 0051852: TC7.2.0: Segmentation violation after loading a file.
  // This method, in particular, forces the presentations to update their pipelines -
  // probably, this could be a key.
  GetMemorySize();

  return thePrs3d;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3d_i*
VISU::ColoredPrs3dCache_i
::CreatePrs(VISU::VISUType theType,
	    VISU::ColoredPrs3dHolder::BasicInput theInput,
	    VISU::ColoredPrs3dHolder_i* theHolder)
{
  return RegisterInHolder(CreateColoredPrs3d(theType, theInput), 
			  theHolder->GetEntry());
}


//----------------------------------------------------------------------------
VISU::TLastVisitedPrsList&
VISU::ColoredPrs3dCache_i
::GetLastVisitedPrsList(VISU::ColoredPrs3dHolder_i* theHolder)
{
  return myHolderMap[theHolder->GetEntry()];
}


//----------------------------------------------------------------------------
VISU::TPrs3dPtr
VISU::ColoredPrs3dCache_i
::GetLastVisitedPrs(VISU::ColoredPrs3dHolder_i* theHolder)
{
  const TLastVisitedPrsList& aList = GetLastVisitedPrsList(theHolder);
  if(MYDEBUG) MESSAGE("GetLastVisitedPrs " << theHolder->GetEntry().c_str() << " " << aList.size() );
  if( !aList.empty() )
    return aList.front();
  return VISU::TPrs3dPtr();
}


//----------------------------------------------------------------------------
VISU::TPrs3dPtr
VISU::ColoredPrs3dCache_i
::FindPrsByInput(TLastVisitedPrsList& theLastVisitedPrsList,
		 const VISU::ColoredPrs3dHolder::BasicInput& theInput)
{
  TLastVisitedPrsList::iterator anIter = theLastVisitedPrsList.begin();
  TLastVisitedPrsList::iterator aEndIter = theLastVisitedPrsList.end();
  for(; anIter != aEndIter; anIter++){
    TPrs3dPtr aPrs3d = *anIter;
    VISU::ColoredPrs3dHolder::BasicInput_var anInput = aPrs3d->GetBasicInput();
    if(IsSameTimeStamp(theInput, anInput)){
      theLastVisitedPrsList.erase(anIter);
      return aPrs3d;
    }
  }
  return VISU::TPrs3dPtr();
}

struct TFindActorEvent: public SALOME_Event
{
  VISU::TPrs3dPtr myPrs3d;
  SVTK_ViewWindow* myViewWindow;

  typedef VISU_Actor* TResult;
  TResult myResult;

  TFindActorEvent(VISU::TPrs3dPtr thePrs3d, SVTK_ViewWindow* theViewWindow):
    myPrs3d(thePrs3d),
    myViewWindow(theViewWindow),
    myResult(NULL)
  {}

  virtual
  void
  Execute()
  {
    myResult = VISU::FindActor(myViewWindow, myPrs3d);
  }
};

struct TAddActorEvent: public SALOME_Event
{
  VISU_Actor* myActor;
  SVTK_ViewWindow* myViewWindow;
public:
  TAddActorEvent(VISU_Actor* theActor, SVTK_ViewWindow* theViewWindow):
    myActor(theActor),
    myViewWindow(theViewWindow)
  {}
  virtual void Execute(){
    myViewWindow->AddActor(myActor);
  }
};

struct TRenderEvent: public SALOME_Event
{
  SVTK_ViewWindow* myViewWindow;
public:
  TRenderEvent(SVTK_ViewWindow* theViewWindow):
    myViewWindow(theViewWindow)
  {}
  virtual void Execute(){
    myViewWindow->getRenderWindow()->Render();
  }
};

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3dCache_i
::UpdateLastVisitedPrs(VISU::ColoredPrs3dHolder_i* theHolder,
		       VISU::ColoredPrs3d_i* thePrs,
		       const VISU::ColoredPrs3dHolder::BasicInput& theInput,
		       VISU::View3D_ptr theView3D)
{
  if(MYDEBUG) MESSAGE( "VISU::ColoredPrs3dCache_i::UpdateLastVisitedPrs" );
  TPrs3dPtr aPrs3d;
  try{
    TPrs3dPtr aLastVisitedPrs3d = GetLastVisitedPrs(theHolder);
    TLastVisitedPrsList& aLastVisitedPrsList = GetLastVisitedPrsList(theHolder);
    bool anIsCheckPossible = GetMemoryMode() == VISU::ColoredPrs3dCache::LIMITED;
    std::string aHolderEntry = theHolder->GetEntry();
    VISU::VISUType aPrsType = theHolder->GetPrsType();
    CORBA::Float aRequiredMemory = 0.0;
    if(aPrs3d = FindPrsByInput(aLastVisitedPrsList, theInput)){
      aLastVisitedPrsList.push_front(aPrs3d);
      if(MYDEBUG) MESSAGE( "FindPrsByInput " << aPrs3d );
    }else if(anIsCheckPossible && IsPossible(aPrsType, theInput, aRequiredMemory, aHolderEntry)){
      if( aRequiredMemory > 1.0 / VTK_LARGE_FLOAT )
	ClearMemory(aRequiredMemory, aHolderEntry);
      aPrs3d = CreatePrs(aPrsType, theInput, theHolder);
      if(MYDEBUG) MESSAGE( "Created " << aPrs3d );
    }else{
      aPrs3d = aLastVisitedPrsList.back();
      aPrs3d->SetResultObject(theInput.myResult);
      aPrs3d->SetMeshName(theInput.myMeshName);
      aPrs3d->SetEntity(theInput.myEntity);
      aPrs3d->SetFieldName(theInput.myFieldName);
      aPrs3d->SetTimeStampNumber(theInput.myTimeStampNumber);
      aLastVisitedPrsList.pop_back();
      aLastVisitedPrsList.push_front(aPrs3d);
      if(MYDEBUG) MESSAGE( "Move only " << aPrs3d );
    }
    //if(MYDEBUG) PrintCache();
    
    aPrs3d->SameAs(thePrs);
    
    // rnv: fix for the 20870: EDF 1410 VISU: Anomaly in the Gauss point representation.
    // special case for the "Gauss Points" presentation,
    // update the LookupTable in the mapper, after assign properties of the presentation
    // using SameAs(...) method.
    VISU::GaussPoints_i* gPoints  = dynamic_cast<VISU::GaussPoints_i*>( aPrs3d.get() );
    if(gPoints) {
      gPoints->UpdateMapperLookupTable();
    }
    
    // special case for deformed shape
    VISU::DeformedShapeAndScalarMap_i* dShape =
      dynamic_cast<VISU::DeformedShapeAndScalarMap_i*>( aPrs3d.get() );
    if ( dShape && dShape->GetScalarTimeStampNumber() != theInput.myTimeStampNumber )
    {
      dShape->SetScalarField( dShape->GetScalarEntity(),
        dShape->GetScalarFieldName(), theInput.myTimeStampNumber );            
    }                    

    if(CORBA::is_nil(theView3D))
      return false;

    PortableServer::ServantBase_var aServant = GetServant(theView3D);
    if(VISU::View3D_i* aView3d = dynamic_cast<VISU::View3D_i*>(aServant.in())){
      if(SUIT_ViewWindow* aView = aView3d->GetViewWindow()){
	if(SVTK_ViewWindow* aViewWindow = dynamic_cast<SVTK_ViewWindow*>(aView)){
	  // Find actor that corresponds to the holder
	  VISU_Actor* anActor = ProcessEvent(new TFindActorEvent(aLastVisitedPrs3d,aViewWindow));
	  //VISU_Actor* anActor = VISU::FindActor(aViewWindow, aLastVisitedPrs3d);

	  // If the holder was erased from view then do nothing
	  if(anActor && !anActor->GetVisibility())
	    return true;

	  if(!anActor)
	  {
	    anActor = aLastVisitedPrs3d->CreateActor();
	    ProcessVoidEvent(new TAddActorEvent(anActor,aViewWindow));
	    //aViewWindow->AddActor(anActor);
	    anActor->SetVisibility(true);
	    SetVisibilityState( aHolderEntry, Qtx::ShownState);
	  }

	  if(aPrs3d != aLastVisitedPrs3d)
	  {
	    // To erase non active presentation
	    aLastVisitedPrs3d->SetActiveState(false);
	    if(anActor)
	      anActor->SetVisibility(false);

	    // To find the new one that corresponds to fresh presentation
	    VISU_Actor* aNewActor = ProcessEvent(new TFindActorEvent(aPrs3d,aViewWindow));
	    //VISU_Actor* aNewActor = VISU::FindActor(aViewWindow, aPrs3d);
	    if(!aNewActor){
	      aNewActor = aPrs3d->CreateActor();
	      ProcessVoidEvent(new TAddActorEvent(aNewActor,aViewWindow));
	      //aViewWindow->AddActor(aNewActor);
	    }else {
	      aNewActor->SetVisibility(true);
	      SetVisibilityState( aHolderEntry, Qtx::ShownState);
	    }
	    aNewActor->DeepCopy(anActor);

	    aPrs3d->SetActiveState(true);
	  }

	  aPrs3d->UpdateActors();
	  ProcessVoidEvent(new TRenderEvent(aViewWindow));
	  //aViewWindow->getRenderWindow()->Render();
	  return true;
	}
      }
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }

  return false;
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dCache_i
::ClearCache(float theMemory)
{
  CORBA::Float aCurrentMemory = GetMemorySize();
  ClearMemory( aCurrentMemory - theMemory, "" );
}


//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3dCache_i
::ClearMemory(CORBA::Float theRequiredMemory, 
	      const std::string& theHolderEntry)
{
  CORBA::Float aInitialMemorySize = GetMemorySize();
  TColoredPrs3dHolderMap aHolder2PrsToBeDeletedMap;
  SelectPrs3dToBeDeleted(theRequiredMemory, theHolderEntry, myHolderMap, aHolder2PrsToBeDeletedMap);
  TColoredPrs3dHolderMap::const_iterator aHolderIter = aHolder2PrsToBeDeletedMap.begin();
  TColoredPrs3dHolderMap::const_iterator anEndHolderIter = aHolder2PrsToBeDeletedMap.end();
  for(; aHolderIter != anEndHolderIter; aHolderIter++){
    const std::string aHolderEntry = aHolderIter->first;
    TColoredPrs3dHolderMap::iterator anHolderMapIter = myHolderMap.find(aHolderEntry);
    if(anHolderMapIter != myHolderMap.end()){
      TLastVisitedPrsList& aLastVisitedPrsList = anHolderMapIter->second;
      
      const TLastVisitedPrsList& aPrsToBeDeletedList = aHolderIter->second;
      TLastVisitedPrsList::const_iterator anIter = aPrsToBeDeletedList.begin();
      TLastVisitedPrsList::const_iterator aEndIter = aPrsToBeDeletedList.end();
      for(; anIter != aEndIter; anIter++){
	TPrs3dPtr aPrs3d = *anIter;
	ErasePrs3d(aLastVisitedPrsList, aPrs3d);
      }
    }
  }
  CORBA::Float aCurrentMemory = GetMemorySize();
  return (aInitialMemorySize - aCurrentMemory >= theRequiredMemory);
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dCache_i
::PrintCache()
{
  if(MYDEBUG)
  {
    MESSAGE_BEGIN(std::endl << "--------------CACHE-----------------" );
    MESSAGE_ADD(std::endl << "Cache memory - " << GetMemorySize() << " Mb" );
    TColoredPrs3dHolderMap::const_iterator aHolderIter = myHolderMap.begin();
    TColoredPrs3dHolderMap::const_iterator aHolderIterEnd = myHolderMap.end();
    for(; aHolderIter != aHolderIterEnd; aHolderIter++){
      const TLastVisitedPrsList& aPrsList = aHolderIter->second;
      TLastVisitedPrsList::const_iterator aPrsIter = aPrsList.begin();
      TLastVisitedPrsList::const_iterator aPrsIterEnd = aPrsList.end();

      MESSAGE_ADD(std::endl << "--------------------------" );
      MESSAGE_ADD(std::endl <<  "Holder - " << aHolderIter->first.c_str() );
      MESSAGE_ADD(std::endl <<  "Size   - " << aPrsList.size() );
      for(; aPrsIter != aPrsIterEnd; aPrsIter++)
	if(TPrs3dPtr aPrs3d = *aPrsIter)
	{
	  MESSAGE_ADD(std::endl <<  aPrs3d << " (" << aPrs3d->GetMemorySize() << " Mb)");
	  if(aPrsIter == aPrsList.begin())
	    MESSAGE_ADD( " (device)" );
	}
    }
    MESSAGE_END(std::endl <<  "------------------------------------" );
  }
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dCache_i
::RemoveHolder(VISU::ColoredPrs3dHolder_i* theHolder)
{
  TColoredPrs3dHolderMap::iterator anIter = myHolderMap.find(theHolder->GetEntry());
  if(anIter != myHolderMap.end())
    myHolderMap.erase(anIter);
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dCache_i
::ToStream(std::ostringstream& theStr) 
{
  Storable::DataToStream( theStr, "myMemoryMode", GetMemoryMode() );
  Storable::DataToStream( theStr, "myLimitedMemory", GetLimitedMemory() );
}

//---------------------------------------------------------------
VISU::Storable*
VISU::ColoredPrs3dCache_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  SetMemoryMode( (VISU::ColoredPrs3dCache::MemoryMode)VISU::Storable::FindValue( theMap, "myMemoryMode" ).toInt() );
  SetLimitedMemory( VISU::Storable::FindValue( theMap, "myLimitedMemory" ).toInt() );
  
  return this;
}

//---------------------------------------------------------------
VISU::Storable*
VISU::ColoredPrs3dCache_i
::StorableEngine(SALOMEDS::SObject_ptr theSObject,
		 const Storable::TRestoringMap& theMap,
		 const std::string& thePrefix,
		 CORBA::Boolean theIsMultiFile)
{
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  VISU::ColoredPrs3dCache_i* aCache = new VISU::ColoredPrs3dCache_i(aStudy, false);
  return aCache->Restore(theSObject, theMap);
}

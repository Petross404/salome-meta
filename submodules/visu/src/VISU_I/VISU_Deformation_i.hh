// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Deformation_i.hxx
//  Author : 
//  Module : VISU
//
#ifndef VISU_Deformation_i_HeaderFile
#define VISU_Deformation_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_DeformationPL.hxx"



namespace VISU{

  class VISU_I_EXPORT Deformation_i : public virtual POA_VISU::Deformation
  {
    Deformation_i(const Deformation_i&);
  public:
    typedef VISU::Deformation TInterface;

    Deformation_i(VISU::ColoredPrs3d_i* theColoredPrs3d);
    virtual ~Deformation_i();

    virtual 
    void
    SetScale(CORBA::Double theScale);

    virtual
    CORBA::Double 
    GetScale();

    virtual
    void
    DeformationToStream(std::ostringstream& theStr);

    virtual
    void
    RestoreDeformation(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void 
    SameAsDeformation(const Deformation_i *aDeformedPrs);

    virtual
    VISU::Entity
    GetVectorialFieldEntity();

    virtual
    char* 
    GetVectorialFieldName();

    virtual void SetVectorialField(Entity theEntity, const char* theFieldName);

  protected:
    virtual
    void
    InitDeformedPipeLine(VISU_DeformationPL* theDeformedPipeLine);

    VISU_DeformationPL*
    GetSpecificDeformedPL() const
    { 
      return myDeformationPL; 
    }

    ColoredPrs3d_i* GetColoredPrs3d(){
      return myColoredPrs3d;
    }
    
    
  private:
    VISU_DeformationPL *myDeformationPL;

    PField myVectorialField;
    VISU::Entity myVectorialEntity;
    std::string myVectorialFieldName;
    ColoredPrs3d_i *myColoredPrs3d;
    
  };
}

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Evolution.cxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_Evolution.h"

#include "VISU_Tools.h"
#include "VISU_Gen_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_View_i.hh"

#include <VISU_MeshValue.hxx>
#include <VISU_Structures_impl.hxx>
#include <VISU_VTKTypeList.hxx>

#include <SUIT_Session.h>

#include <SalomeApp_Application.h>
#include <SalomeApp_Study.h>

#include <SALOME_Event.h>

//------------------------------------------------------------------------
template<int EDataType> bool ProcessValForTime( VISU::PValForTimeImpl theValForTime,
                                                VISU::Elem2Comp2Value& theElem2Comp2Value )
{
  theElem2Comp2Value.clear();

  typedef typename VISU::TL::TEnum2VTKBasicType< EDataType >::TResult TVTKBasicType;
  typedef VISU::TTMeshValue< TVTKBasicType > TMeshValue;
  typedef MED::SharedPtr< TMeshValue > TMeshValuePtr;

  const TMeshValuePtr aMeshValue = theValForTime->GetMeshValue( VISU::ePOINT1 );
  if( !aMeshValue )
    return false;

  vtkIdType aNbElem = aMeshValue->GetNbElem();
  vtkIdType aNbComp = aMeshValue->GetNbComp();
  vtkIdType aNbGauss = aMeshValue->GetNbGauss();

  for( vtkIdType iElem = 0; iElem < aNbElem; iElem++ )
  {
    typename TMeshValue::TCValueSliceArr aMValueSliceArr = aMeshValue->GetCompValueSliceArr( iElem );
    VISU::Comp2Value& aComp2Value = theElem2Comp2Value[ iElem ];

    double& aModulusValue = aComp2Value[ 0 ];
    aModulusValue = 0.0;

    for( vtkIdType iComp = 0; iComp < aNbComp; iComp++ )
    {
      const typename TMeshValue::TCValueSlice& aMValueSlice = aMValueSliceArr[ iComp ];

      double& aValue = aComp2Value[ iComp+1 ];
      aValue = 0.0;

      for(vtkIdType iGauss = 0; iGauss < aNbGauss; iGauss++)
      {
        const double& aVal = aMValueSlice[iGauss];
        aValue += aVal;
      }
      if( aNbGauss != 0 )
        aValue /= aNbGauss;

      aModulusValue += aValue * aValue;
    }
    aModulusValue = sqrt( aModulusValue );
  }

  return true;
}

//------------------------------------------------------------------------
template<int EDataType> int GetNbPoints( VISU::PValForTimeImpl theValForTime )
{
  typedef typename VISU::TL::TEnum2VTKBasicType< EDataType >::TResult TVTKBasicType;
  typedef VISU::TTMeshValue< TVTKBasicType > TMeshValue;
  typedef MED::SharedPtr< TMeshValue > TMeshValuePtr;

  const TMeshValuePtr aMeshValue = theValForTime->GetMeshValue( VISU::ePOINT1 );
  if( !aMeshValue )
    return 0;

  return aMeshValue->GetNbElem();
}

//------------------------------------------------------------------------
VISU_Evolution::VISU_Evolution( _PTR(Study) theStudy,
                                VISU::XYPlot_ptr theXYPlot )
{
  myStudy = theStudy;
  myView = 0;

  if( !CORBA::is_nil( theXYPlot ) )
  {
    VISU::XYPlot_i* pPlot = dynamic_cast<VISU::XYPlot_i*>( GetServant( theXYPlot ).in() );
    Plot2d_ViewFrame* aVF = pPlot->GetView();
    setViewer( aVF );
  }

  myFieldEntry = "";
  myEvolutionEntry = "";
  myMeshName = "";
  myEntity = VISU::NODE_ENTITY;
  myFieldName = "";

  myPointId = 0;
  myComponentId = 0;
}

//------------------------------------------------------------------------
VISU_Evolution::~VISU_Evolution()
{
}

//------------------------------------------------------------------------
bool VISU_Evolution::setField( _PTR(SObject) theField )
{
  myField = theField;
  if( !theField )
    return false;

  myFieldEntry = theField->GetID();

  VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap( theField );
  if( aRestoringMap.empty() )
    return false;

  VISU::VISUType aType = VISU::Storable::RestoringMap2Type( aRestoringMap );
  if( aType != VISU::TFIELD )
    return false;

  myMeshName = aRestoringMap["myMeshName"].toStdString();
  myEntity = VISU::TEntity( aRestoringMap["myEntityId"].toInt() );
  myFieldName = aRestoringMap["myName"].toStdString();

  if( myEntity != VISU::NODE_ENTITY )
    return false;

  VISU::Result_var aResult = VISU::FindResult( VISU::GetSObject( theField ).in() );
  if( CORBA::is_nil( aResult.in() ) )
    return false;

  myResult = dynamic_cast<VISU::Result_i*>( VISU::GetServant( aResult ).in() );
  if( !myResult )
    return false;

  const VISU::TMeshMap& aMeshMap = myResult->GetInput()->GetMeshMap();
  if( aMeshMap.empty() )
    return false;

  VISU::TMeshMap::const_iterator aMeshIter = aMeshMap.find( myMeshName );
  if( aMeshIter == aMeshMap.end() )
    return false;
  
  const VISU::PMesh& aMesh = aMeshIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if( aMeshOnEntityMap.empty() )
    return false;

  VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityIter = aMeshOnEntityMap.find( myEntity );
  if( aMeshOnEntityIter == aMeshOnEntityMap.end() )
    return false;

  const VISU::PMeshOnEntity& aMeshOnEntity = aMeshOnEntityIter->second;
  const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
  if( aFieldMap.empty() )
    return false;

  VISU::TFieldMap::const_iterator aFieldIter = aFieldMap.find( myFieldName );
  if( aFieldIter == aFieldMap.end() )
    return false;

  myFieldImpl = aFieldIter->second;
  const VISU::TNames& aCompNames = myFieldImpl->myCompNames;
  const VISU::TNames& aUnitNames = myFieldImpl->myUnitNames;
  int aNbComp = myFieldImpl->myNbComp;

  // fill myComponentDataList
  myComponentDataList.clear();

  QString aModulusComponent( "<Modulus>" );
  myComponentDataList.append( VISU::ComponentData( aModulusComponent, "" ) );
  for( int i = 0; i < aNbComp; i++ )
  {
    QString aComponent = QString( aCompNames[i].c_str() ).simplified();
    if( aComponent.isEmpty() )
      aComponent = "Component " + QString::number( i+1 );

    QString anUnit = QString( aUnitNames[i].c_str() ).simplified();

    myComponentDataList.append( VISU::ComponentData( aComponent, anUnit ) );
  }

  return true;
}

//------------------------------------------------------------------------
bool VISU_Evolution::setField( SALOMEDS::SObject_ptr theField )
{
  SALOMEDS::SObject_var theFieldDup = SALOMEDS::SObject::_duplicate( theField );
  _PTR(SObject) aField = VISU::GetClientSObject( theFieldDup, myStudy );
  return setField( aField );
}

//------------------------------------------------------------------------
void VISU_Evolution::setPointId( int thePointId )
{
  myPointId = thePointId;
}

//------------------------------------------------------------------------
void VISU_Evolution::setComponentId( int theComponentId )
{
  myComponentId = theComponentId;
}

//------------------------------------------------------------------------
int VISU_Evolution::getNbPoints() const
{
  if( !myFieldImpl )
    return 0;

  vtkIdType aDataType = myFieldImpl->GetDataType();
  const VISU::TValField& aValField = myFieldImpl->myValField;
  if( aValField.empty() )
    return 0;

  VISU::TValField::const_iterator aValFieldIter = aValField.begin();
  for( ; aValFieldIter != aValField.end(); aValFieldIter++ )
  {
    const vtkIdType& aTimeStamp = aValFieldIter->first;
    VISU::PValForTimeImpl aValForTime = aValFieldIter->second;

    // to force method VISU::MedConvertor::FillValForTime() to be called
    myResult->GetInput()->GetTimeStampOnMesh( myMeshName, myEntity, myFieldName, aTimeStamp );

    switch( aDataType )
    {
      case VTK_DOUBLE : return GetNbPoints<VTK_DOUBLE>( aValForTime );
      case VTK_FLOAT : return GetNbPoints<VTK_FLOAT>( aValForTime );
      case VTK_INT : return GetNbPoints<VTK_INT>( aValForTime );
      case VTK_LONG: return GetNbPoints<VTK_LONG>( aValForTime );
      default: return 0;
    }
  }

  return 0;
}

//------------------------------------------------------------------------
bool VISU_Evolution::extractData( int thePointId,
                                  int theComponentId,
                                  VISU::TimeStampValueList& theTimeStampValueList )
{
  theTimeStampValueList.clear();
  myTimeStampDataList.clear();

  if( !myFieldImpl )
    return false;

  vtkIdType aDataType = myFieldImpl->GetDataType();
  const VISU::TValField& aValField = myFieldImpl->myValField;
  if( aValField.empty() )
    return false;

  typedef QList< VISU::Elem2Comp2Value >         TimeStamp2Elem2Comp2Value;
  typedef QListIterator< VISU::Elem2Comp2Value > TimeStamp2Elem2Comp2ValueIterator;
  TimeStamp2Elem2Comp2Value aData;

  VISU::TValField::const_iterator aValFieldIter = aValField.begin();
  for( ; aValFieldIter != aValField.end(); aValFieldIter++ )
  {
    const vtkIdType& aTimeStamp = aValFieldIter->first;
    VISU::PValForTimeImpl aValForTime = aValFieldIter->second;
    VISU::TTime aTime = aValForTime->myTime;
    double aTimeValue = aTime.first;
    std::string aTimeUnits = aTime.second;

    // to force method VISU::MedConvertor::FillValForTime() to be called
    // (we need data of all timestamps of the result)
    myResult->GetInput()->GetTimeStampOnMesh( myMeshName, myEntity, myFieldName, aTimeStamp );

    // data should be sorted by time value
    int anOrder = 0;
    VISU::TimeStampDataListIterator anIter( myTimeStampDataList );
    while( anIter.hasNext() )
    {
      VISU::TimeStampData aTimeStampData = anIter.next();
      if( aTimeValue > aTimeStampData.first )
        anOrder++;
    }

    VISU::Elem2Comp2Value anElem2Comp2Value;

    bool ok = false;
    switch( aDataType )
    {
      case VTK_DOUBLE : ok = ProcessValForTime<VTK_DOUBLE>( aValForTime, anElem2Comp2Value ); break;
      case VTK_FLOAT : ok = ProcessValForTime<VTK_FLOAT>( aValForTime, anElem2Comp2Value ); break;
      case VTK_INT : ok = ProcessValForTime<VTK_INT>( aValForTime, anElem2Comp2Value ); break;
      case VTK_LONG: ok = ProcessValForTime<VTK_LONG>( aValForTime, anElem2Comp2Value ); break;
      default: break;
    }

    if( !ok )
      return false;

    aData.insert( anOrder, anElem2Comp2Value );
    myTimeStampDataList.insert( anOrder, VISU::TimeStampData( aTimeValue, aTimeUnits ) );
  }

  if( theComponentId < 0 || theComponentId >= myComponentDataList.size() )
    return false;

  int aTimeStamp = 0;
  TimeStamp2Elem2Comp2ValueIterator it1( aData );
  while( it1.hasNext() )
  {
    VISU::Elem2Comp2Value anElem2Comp2Value = it1.next();
    VISU::Elem2Comp2Value::const_iterator it2 = anElem2Comp2Value.find( thePointId );
    if( it2 != anElem2Comp2Value.end() )
    {
      VISU::Comp2Value aComp2Value = it2.value();
      VISU::Comp2Value::const_iterator it3 = aComp2Value.find( theComponentId );
      if( it3 != aComp2Value.end() )
      {
        double aValue = it3.value();
        theTimeStampValueList.append( aValue );
      }
    }
    aTimeStamp++;
  }

  if( theTimeStampValueList.size() != myTimeStampDataList.size() )
    return false;

  return true;
}

//------------------------------------------------------------------------
bool VISU_Evolution::showEvolution()
{
  return ProcessEvent(new TMemFunEvent<VISU_Evolution,bool>
                      (this,&VISU_Evolution::_showEvolution));
}

//------------------------------------------------------------------------
bool VISU_Evolution::_showEvolution()
{
  bool isEdit = ( myEvolutionEntry != "" );

  // 1) extract data
  VISU::TimeStampValueList aTimeStampValueList;
  if( !extractData( myPointId, myComponentId, aTimeStampValueList ) )
    return false;

  // 2) publish new objects in study (or edit them)
  _PTR(StudyBuilder) aStudyBuilder = myStudy->NewBuilder();
  aStudyBuilder->NewCommand(); // open transaction

  QString anEvolutionComment;
  anEvolutionComment.sprintf( "myComment=EVOLUTION;myPointId=%d;myComponentId=%d",
                              myPointId,
                              myComponentId );

  _PTR(SObject) anEvolutionObject, aFieldRefObject, aTableObject;
  if( !isEdit ) // creation mode
  {
    _PTR(SComponent) aSComponent = VISU::ClientFindOrCreateVisuComponent( myStudy );
    std::string aSComponentEntry = aSComponent->GetID();

    // create an evolution object
    myEvolutionEntry = VISU::CreateAttributes( myStudy,
                                               aSComponentEntry.c_str(),
                                               VISU::NO_ICON,
                                               VISU::NO_IOR,
                                               VISU::GenerateName( "Evolution", 0 ).toLatin1().constData(),
                                               VISU::NO_PERFSITENT_REF,
                                               anEvolutionComment.toLatin1().constData(),
                                               true );
    anEvolutionObject = myStudy->FindObjectID( myEvolutionEntry.c_str() );

    // create a reference to the field object
    aFieldRefObject = aStudyBuilder->NewObject( anEvolutionObject );
    aStudyBuilder->Addreference( aFieldRefObject, myField );

    // create a table object
    std::string aTableEntry = VISU::CreateAttributes( myStudy,
                                                      aFieldRefObject->GetID().c_str(),
                                                      VISU::NO_ICON,
                                                      VISU::NO_IOR,
                                                      VISU::GenerateName( "Table", 0 ).toLatin1().constData(),
                                                      VISU::NO_PERFSITENT_REF,
                                                      "myComment=TABLE",
                                                      true );
    aTableObject = myStudy->FindObjectID( aTableEntry.c_str() );
  }
  else // edition mode
  {
    // edit an evolution object
    anEvolutionObject = myStudy->FindObjectID( myEvolutionEntry.c_str() );
    _PTR(GenericAttribute) anAttr;
    anAttr = aStudyBuilder->FindOrCreateAttribute( anEvolutionObject, "AttributeString" );
    _PTR(AttributeString) aComment( anAttr );
    aComment->SetValue( anEvolutionComment.toLatin1().constData() );

    // get a reference to the field object
    _PTR(ChildIterator) anIter = myStudy->NewChildIterator( anEvolutionObject );
    anIter->Init();
    if( anIter->More() )
      aFieldRefObject = anIter->Value();

    // get a table object
    anIter = myStudy->NewChildIterator( aFieldRefObject );
    anIter->Init();
    if( anIter->More() )
      aTableObject = anIter->Value();
  }

  aStudyBuilder->CommitCommand(); // commit transaction

  // 3) update table attribute of the table object
  _PTR(GenericAttribute) anAttr = aStudyBuilder->FindOrCreateAttribute( aTableObject, "AttributeTableOfReal" );
  _PTR(AttributeTableOfReal) aTableOfReal( anAttr );

  aTableOfReal->SetNbColumns( aTimeStampValueList.size() );

  QString aTimeRowUnits;
  int aTimeStamp = 0;
  VISU::TimeStampValueListIterator it( aTimeStampValueList );
  while( it.hasNext() )
  {
    double aValue = it.next();

    VISU::TimeStampData aTimeStampData = myTimeStampDataList[ aTimeStamp ];
    double aTimeValue = aTimeStampData.first;
    std::string aTimeUnits = aTimeStampData.second;

    if( aTimeRowUnits.isEmpty() )
      aTimeRowUnits = QString( aTimeUnits.c_str() ).simplified();

    aTableOfReal->SetColumnTitle( aTimeStamp+1, QString::number( aTimeStamp+1 ).toLatin1().constData() );
    aTableOfReal->PutValue( aTimeValue, 1, aTimeStamp+1 );
    aTableOfReal->PutValue( aValue, 2, aTimeStamp+1 );

    aTimeStamp++;
  }

  if( aTimeRowUnits.isEmpty() )
    aTimeRowUnits = "s";

  aTableOfReal->SetRowTitle( 1, "Time" );
  aTableOfReal->SetRowUnit( 1, aTimeRowUnits.toLatin1().constData() );

  const VISU::ComponentData& aComponentData = myComponentDataList[ myComponentId ];
  QString aValueTitle = QString( "Point %1" ).arg( myPointId );
  aTableOfReal->SetRowTitle( 2, aValueTitle.toLatin1().constData() );
  aTableOfReal->SetRowUnit( 2, aComponentData.second.toLatin1().constData() );

  QString aTitle = QString( "%1, %2" ).arg( myFieldName.c_str() ).arg( aComponentData.first );
  aTableOfReal->SetTitle( aTitle.toLatin1().constData() );

  // 4) get active study
  SalomeApp_Study* aSalomeStudy = NULL;
  SUIT_Session* aSession = SUIT_Session::session();
  QList<SUIT_Application*> anApplications = aSession->applications();
  QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
  while ( anIter !=  anApplications.end() ) {
    SUIT_Application* anApp = *anIter;
    if (SUIT_Study* aSStudy = anApp->activeStudy()) {
      if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
        if (_PTR(Study) aCStudy = aStudy->studyDS()) {
          if (aCStudy->Name() == myStudy->Name()) {
            aSalomeStudy = aStudy;
            break;
          }
        }
      }
      anIter++;
    }
  }

  // 5) create curves and plot
  if( aSalomeStudy )
  {
    // 5.1) remove old curves (edition mode)
    if( isEdit ) {
      _PTR(GenericAttribute) anAttr;
      if(aTableObject->FindAttribute( anAttr, "AttributeName" ) ) {
        _PTR(ChildIterator) aChildIter = myStudy->NewChildIterator( aTableObject );
        for( aChildIter->InitEx( false ); aChildIter->More(); aChildIter->Next() ) {
          aSalomeStudy->deleteReferencesTo( aChildIter->Value() );
          _PTR(SObject) aCurveObject = aChildIter->Value();
          if(aCurveObject) {
            VISU::Storable::TRestoringMap aRestoringMap = VISU::Storable::GetStorableMap(aCurveObject);
            if (aRestoringMap["myComment"] == "CURVE") {
              CORBA::Object_var aCORBAObject = VISU::ClientSObjectToObject(aCurveObject);
              if(!CORBA::is_nil(aCORBAObject)) {
                PortableServer::ServantBase_var aServant = VISU::GetServant(aCORBAObject);
                if(VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(aServant.in())){
                  if(SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aSalomeStudy->application()))
                    VISU::PlotRemoveCurve(anApp, aCurve);
                }
              }
            }
            aStudyBuilder->RemoveObject( aCurveObject );
          }
        }
      }
    }

    // 5.2) create new curves
    VISU::VISU_Gen_i* aVisuGen = VISU::VISU_Gen_i::GetVisuGenImpl();
    aVisuGen->CreateTable( aTableObject->GetID().c_str() );

    // 5.3) create plot
    VISU::CreatePlot( aVisuGen, getViewer(), aTableObject );
  }

  return true;
}

//------------------------------------------------------------------------
void VISU_Evolution::restoreFromStudy( SALOMEDS::SObject_ptr theObj )
{
  _PTR(SObject) anEvolutionObject = VISU::GetClientSObject( theObj, myStudy );
  restoreFromStudy( anEvolutionObject );
}

//------------------------------------------------------------------------
void VISU_Evolution::restoreFromStudy( _PTR(SObject) theObj )
{
  VISU::Storable::TRestoringMap aMap = VISU::Storable::GetStorableMap( theObj );
  if( aMap.empty() )
    return;

  bool isExist;
  myPointId     = VISU::Storable::FindValue( aMap, "myPointId",     &isExist ).toInt();
  myComponentId = VISU::Storable::FindValue( aMap, "myComponentId", &isExist ).toInt();

  _PTR(ChildIterator) anIter = myStudy->NewChildIterator( theObj );
  for( anIter->Init(); anIter->More(); anIter->Next() )
  {
    _PTR(SObject) aRefObj = anIter->Value();
    _PTR(SObject) aFieldObj;

    if( !aRefObj->ReferencedObject( aFieldObj ) ) 
      continue;

    int nbAttr = aFieldObj->GetAllAttributes().size();
    if( nbAttr < 1 )
      continue;

    setField( aFieldObj );
    break;
  }

  myEvolutionEntry = theObj->GetID();
}

//------------------------------------------------------------------------
struct TNewEvolutionEvent: public SALOME_Event
{
  std::string myStudyName;
  VISU::XYPlot_ptr myXYPlot;

  typedef VISU_Evolution* TResult;
  TResult myResult;

  TNewEvolutionEvent (std::string theStudyName, VISU::XYPlot_ptr theXYPlot):
    myStudyName(theStudyName),
    myXYPlot(VISU::XYPlot::_duplicate(theXYPlot)),
    myResult(NULL)
  {}

  virtual
  void
  Execute()
  {
    SUIT_Session* aSession = SUIT_Session::session();
    QList<SUIT_Application*> anApplications = aSession->applications();
    QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
    while ( anIter !=  anApplications.end() ) {
      SUIT_Application* anApp = *anIter;
      if (SUIT_Study* aSStudy = anApp->activeStudy()) {
        if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
          if (_PTR(Study) aCStudy = aStudy->studyDS()) {
            if (myStudyName == aCStudy->Name()) {
              myResult = new VISU_Evolution (aCStudy, myXYPlot);
              break;
            }
          }
        }
      }
      anIter++;
    }
  }
};

//------------------------------------------------------------------------
VISU_Evolution_i::VISU_Evolution_i( SALOMEDS::Study_ptr theStudy,
                                    VISU::XYPlot_ptr theXYPlot )
{
  std::string aStudyName = theStudy->Name();
  myEngine = ProcessEvent( new TNewEvolutionEvent( aStudyName, theXYPlot ) );
}

//------------------------------------------------------------------------
VISU_Evolution_i::~VISU_Evolution_i()
{
  delete myEngine;
}

//------------------------------------------------------------------------
bool VISU_Evolution_i::setField( SALOMEDS::SObject_ptr theField )
{
  return myEngine->setField( theField );
}

//------------------------------------------------------------------------
void VISU_Evolution_i::setPointId( CORBA::Long thePointId )
{
  myEngine->setPointId( thePointId );
}

//------------------------------------------------------------------------
void VISU_Evolution_i::setComponentId( CORBA::Long theComponentId )
{
  myEngine->setComponentId( theComponentId );
}

//------------------------------------------------------------------------
CORBA::Boolean VISU_Evolution_i::showEvolution()
{
  return myEngine->showEvolution();
}

//------------------------------------------------------------------------
void VISU_Evolution_i::restoreFromStudy( SALOMEDS::SObject_ptr theObj )
{
  myEngine->restoreFromStudy( theObj );
}

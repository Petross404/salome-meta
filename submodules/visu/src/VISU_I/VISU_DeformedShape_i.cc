// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_DeformedShape_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_ScalarMapAct.h"
#include "VISU_DeformedShapePL.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <vtkDataSetMapper.h>
#include <vtkProperty.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
size_t
VISU::DeformedShape_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  try{
    size_t aResult = TSuperClass::IsPossible(theResult,
					     theMeshName,
					     theEntity,
					     theFieldName,
					     theTimeStampNumber,
					     theIsMemoryCheck);
    if(aResult){
      VISU::Result_i::PInput anInput = theResult->GetInput(theMeshName,
							   theEntity,
							   theFieldName,
							   theTimeStampNumber);
      VISU::PField aField = anInput->GetField(theMeshName,
					      (VISU::TEntity)theEntity,
					      theFieldName);
      if(aField->myNbComp > 1)
	return aResult;
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return 0;
}


//---------------------------------------------------------------
int VISU::DeformedShape_i::myNbPresent = 0;

//---------------------------------------------------------------
QString 
VISU::DeformedShape_i
::GenerateName() 
{ 
  return VISU::GenerateName("Def.Shape",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::DeformedShape_i::myComment = "DEFORMEDSHAPE";

//---------------------------------------------------------------
const char* 
VISU::DeformedShape_i
::GetComment() const 
{ 
  return myComment.c_str();
}


//---------------------------------------------------------------
const char*
VISU::DeformedShape_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_DEFORMED_SHAPE";
  else
    return "ICON_TREE_DEFORMED_SHAPE_GROUPS";
}  


//---------------------------------------------------------------
VISU::DeformedShape_i
::DeformedShape_i(EPublishInStudyMode thePublishInStudyMode):
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  MonoColorPrs_i(thePublishInStudyMode),
  myDeformedShapePL(NULL)
{
  if(MYDEBUG) MESSAGE("DeformedShape_i::DeformedShape_i");
}


//---------------------------------------------------------------
void 
VISU::DeformedShape_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const DeformedShape_i* aPrs3d = dynamic_cast<const DeformedShape_i*>(theOrigin)){
    DeformedShape_i* anOrigin = const_cast<DeformedShape_i*>(aPrs3d);
    SetColor(anOrigin->GetColor());
    ShowColored(anOrigin->IsColored());
  }
}

//---------------------------------------------------------------
VISU::Storable* 
VISU::DeformedShape_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::DeformedShape_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetScale(VISU::Storable::FindValue(theMap,"myFactor").toDouble());

  return this;
}


//---------------------------------------------------------------
void
VISU::DeformedShape_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myFactor", GetScale() );
}


//---------------------------------------------------------------
VISU::DeformedShape_i
::~DeformedShape_i()
{
  if(MYDEBUG) MESSAGE("DeformedShape_i::~DeformedShape_i()");
}


//---------------------------------------------------------------
void 
VISU::DeformedShape_i
::SetScale(CORBA::Double theScale) 
{ 
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedShapePL, double>
		   (GetSpecificPL(), &VISU_DeformedShapePL::SetScale, theScale));
}

//---------------------------------------------------------------
CORBA::Double 
VISU::DeformedShape_i
::GetScale()
{
  return myDeformedShapePL->GetScale();
}


//---------------------------------------------------------------
void
VISU::DeformedShape_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myDeformedShapePL = VISU_DeformedShapePL::New();
  }else
    myDeformedShapePL = dynamic_cast<VISU_DeformedShapePL*>(thePipeLine);

  myDeformedShapePL->GetMapper()->SetScalarVisibility(IsColored());

  TSuperClass::CreatePipeLine(myDeformedShapePL);
}


//---------------------------------------------------------------
bool
VISU::DeformedShape_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),
		    GetCMeshName(),
		    GetEntity(),
		    GetCFieldName(),
		    GetTimeStampNumber(),
		    true);
}


//---------------------------------------------------------------
void 
VISU::DeformedShape_i
::SetMapScale(double theMapScale)
{
  VISU::TSetModified aModified(this);
  
  myDeformedShapePL->SetMapScale(theMapScale);
}


//---------------------------------------------------------------
VISU_Actor* 
VISU::DeformedShape_i
::CreateActor(bool toSupressShrinking) 
{
  VISU_Actor* anActor = TSuperClass::CreateActor(true);
  anActor->SetVTKMapping(false);
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int  aDispMode = aResourceMgr->integerValue("VISU", "deformed_shape_represent", 1);
  bool toShrink  = aResourceMgr->booleanValue("VISU", "deformed_shape_shrink", false);
  anActor->SetRepresentation(aDispMode);
  if (toShrink && !toSupressShrinking) anActor->SetShrink();
  return anActor;
}



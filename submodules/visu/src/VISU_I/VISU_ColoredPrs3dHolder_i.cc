// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ColoredPrs3dHolder_i.cc
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_ColoredPrs3dHolder_i.hh"

#include "VISU_ColoredPrs3dCache_i.hh"
#include "VISU_ColoredPrs3d_i.hh"

#include "SALOME_Event.h"

using namespace VISU;
using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
int VISU::ColoredPrs3dHolder_i::myNbHolders = 0;

//---------------------------------------------------------------
QString 
VISU::ColoredPrs3dHolder_i
::GenerateName() 
{ 
  return VISU::GenerateName("Holder",myNbHolders++);
}

//----------------------------------------------------------------------------
const string VISU::ColoredPrs3dHolder_i::myComment = "COLOREDPRS3DHOLDER";

const char* 
VISU::ColoredPrs3dHolder_i
::GetComment() const 
{ 
  return myComment.c_str();
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dHolder_i
::ColoredPrs3dHolder_i(VISU::ColoredPrs3dCache_i& theCache) :
  PrsObject_i(theCache.GetStudyDocument()),
  myCache(theCache)
{
  if(MYDEBUG) MESSAGE("ColoredPrs3dHolder_i::ColoredPrs3dHolder_i - this = "<<this);
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dHolder_i
::~ColoredPrs3dHolder_i() 
{
  if(MYDEBUG) MESSAGE("ColoredPrs3dHolder_i::~ColoredPrs3dHolder_i - this = "<<this);
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dHolder_i
::PublishInStudy(const std::string& theName, 
		 const std::string& theIconName, 
		 const std::string& theComment)
{
  SetName(theName, false);
  CORBA::String_var anIOR = GetID();
  std::string aFatherEntry = myCache.GetEntry();
  CreateAttributes(GetStudyDocument(), 
		   aFatherEntry, 
		   theIconName, 
		   anIOR.in(), 
		   GetName(), 
		   "", 
		   theComment, 
		   true);
}

//----------------------------------------------------------------------------
struct TApplyEvent: public SALOME_Event
{
  VISU::ColoredPrs3dCache_i& myCache;
  VISU::ColoredPrs3dHolder_i* myHolder;
  VISU::ColoredPrs3d_i* myPrs3d;
  VISU::ColoredPrs3dHolder::BasicInput myInput;
  VISU::View3D_ptr myView3D;

  typedef CORBA::Boolean TResult;
  TResult myResult;

  TApplyEvent(VISU::ColoredPrs3dCache_i& theCache,
	      VISU::ColoredPrs3dHolder_i* theHolder,
	      VISU::ColoredPrs3d_i* thePrs3d,
	      VISU::ColoredPrs3dHolder::BasicInput theInput,
	      VISU::View3D_ptr theView3D):
    myCache(theCache),
    myHolder(theHolder),
    myPrs3d(thePrs3d),
    myInput(theInput),
    myView3D(theView3D)
  {}

  virtual
  void
  Execute()
  {
    myResult = myCache.UpdateLastVisitedPrs(myHolder, myPrs3d, myInput, myView3D);
  }
};

//----------------------------------------------------------------------------
CORBA::Boolean 
VISU::ColoredPrs3dHolder_i
::Apply(VISU::ColoredPrs3d_ptr thePrs3d,
	const VISU::ColoredPrs3dHolder::BasicInput& theInput,
	VISU::View3D_ptr theView3D)
{

  VISU::ColoredPrs3d_i* aPrs3d = dynamic_cast<VISU::ColoredPrs3d_i*>( VISU::GetServant(thePrs3d).in() );
  return ProcessEvent(new TApplyEvent(myCache, this, aPrs3d, theInput, theView3D));
  //return myCache.UpdateLastVisitedPrs(this, aPrs3d, theInput, theView3D);
}

//----------------------------------------------------------------------------
VISU::ColoredPrs3d_i*
VISU::ColoredPrs3dHolder_i
::GetPrs3dDevice()
{
  try{
    return myCache.GetLastVisitedPrs(this);
  }catch(...){}

  return NULL;
}


VISU::ColoredPrs3d_ptr
VISU::ColoredPrs3dHolder_i
::GetDevice()
{
  if( VISU::ColoredPrs3d_i* aDevice = GetPrs3dDevice() )
    return aDevice->_this();

  return VISU::ColoredPrs3d::_nil();
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dHolder::TimeStampsRange*
VISU::ColoredPrs3dHolder_i
::GetTimeStampsRange()
{
  if( VISU::ColoredPrs3d_i* aDevice = GetPrs3dDevice() )
    return aDevice->GetTimeStampsRange();

  return NULL;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dHolder::BasicInput*
VISU::ColoredPrs3dHolder_i
::GetBasicInput()
{
  if( VISU::ColoredPrs3d_ptr aDevice = GetDevice() )
  {
    VISU::ColoredPrs3d_i* aPrs3d = dynamic_cast<VISU::ColoredPrs3d_i*>( VISU::GetServant(aDevice).in() );
    if( aPrs3d )
      return aPrs3d->GetBasicInput();
  }

  return NULL;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dCache_ptr
VISU::ColoredPrs3dHolder_i
::GetCache()
{
  return myCache._this();
}


//----------------------------------------------------------------------------
CORBA::Float
VISU::ColoredPrs3dHolder_i
::GetMemorySize()
{
  return GetDevice()->GetMemorySize();
}


//----------------------------------------------------------------------------
VISU::VISUType
VISU::ColoredPrs3dHolder_i
::GetPrsType()
{
  return GetPrs3dDevice()->GetType();
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dHolder_i
::RemoveFromStudy() 
{
  myCache.RemoveHolder(this);
  CORBA::String_var anIOR = GetID();
  SALOMEDS::SObject_var aSObject = GetStudyDocument()->FindObjectIOR(anIOR.in());
  VISU::RemoveFromStudy(aSObject, false);
  UnRegister();
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3dHolder_i
::ToStream(std::ostringstream& theStr) 
{
  Storable::DataToStream( theStr, "myPrsType", GetPrsType() );
  GetPrs3dDevice()->ToStream(theStr);
}

//---------------------------------------------------------------
VISU::Storable*
VISU::ColoredPrs3dHolder_i
::StorableEngine(SALOMEDS::SObject_ptr theSObject,
		 const Storable::TRestoringMap& theMap,
		 const std::string& thePrefix,
		 CORBA::Boolean theIsMultiFile)
{
  using namespace VISU;
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  VISUType aType = VISUType(Storable::FindValue(theMap,"myPrsType").toInt());
  if(ColoredPrs3d_i* aColoredPrs3d = CreatePrs3d_i(aType, aStudy, ColoredPrs3d_i::EDoNotPublish)){
    if(ColoredPrs3dCache_i* aCache = ColoredPrs3dCache_i::GetInstance_i(aStudy))
      if(ColoredPrs3dHolder_i* aHolder = new ColoredPrs3dHolder_i(*aCache)){
	// To postpone restoring of the device
	aColoredPrs3d->SaveRestoringState(theSObject, theMap); 
	CORBA::String_var anEntry = theSObject->GetID();
	aCache->RegisterInHolder(aColoredPrs3d, anEntry.in());
	return aHolder;
      }
  }
  return NULL;
}

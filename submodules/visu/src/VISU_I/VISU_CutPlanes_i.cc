// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_CutPlanesPL.hxx"
#include "VISU_Convertor.hxx"
#include "VISU_Prs3dUtils.hh"
#include "VISU_CutPlanes_i.hh"

#include "VISU_Result_i.hh"
#include "VISU_Actor.h"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//----------------------------------------------------------------------------
size_t 
VISU::CutPlanes_i
::IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck)
{
  return TSuperClass::IsPossible(theResult,theMeshName,theEntity,theFieldName,theTimeStampNumber,theIsMemoryCheck);
}


//----------------------------------------------------------------------------
int VISU::CutPlanes_i::myNbPresent = 0;

//----------------------------------------------------------------------------
QString
VISU::CutPlanes_i
::GenerateName() 
{ 
  return VISU::GenerateName("CutPlanes",myNbPresent++);
}

//----------------------------------------------------------------------------
const string VISU::CutPlanes_i::myComment = "CUTPLANES";

//----------------------------------------------------------------------------
const char* 
VISU::CutPlanes_i
::GetComment() const 
{ 
  return myComment.c_str();
}


//----------------------------------------------------------------------------
const char* 
VISU::CutPlanes_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_CUT_PLANES";
  else
    return "ICON_TREE_CUT_PLANES_GROUPS";
}


//----------------------------------------------------------------------------
VISU::CutPlanes_i
::CutPlanes_i(EPublishInStudyMode thePublishInStudyMode):
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  myCutPlanesPL(NULL),
  Deformation_i(this),
  OptionalDeformation_i(this)
{
  if(MYDEBUG) MESSAGE("CutPlanes_i::CutPlanes_i()");
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::CutPlanes_i
::Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::CutPlanes_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetNbPlanes(VISU::Storable::FindValue(theMap,"myNbPlanes").toInt());
  SetDisplacement(VISU::Storable::FindValue(theMap,"myDisplacement").toDouble());
  SetOrientation(VISU::CutPlanes::Orientation(VISU::Storable::FindValue(theMap,"myBasePlane").toInt()),
		 Storable::FindValue(theMap,"aRot[0]").toDouble(),
		 Storable::FindValue(theMap,"aRot[1]").toDouble());
  QStringList aPosList = VISU::Storable::FindValue(theMap,"myPlanePosition").split("|", QString::SkipEmptyParts );
  QStringList aCondList = VISU::Storable::FindValue(theMap,"myPlaneCondition").split("|", QString::SkipEmptyParts );
  for(int i = 0, iEnd = GetNbPlanes(); i < iEnd; i++)
    if(aCondList[i].toInt() == 0)
      SetPlanePosition(i,aPosList[i].toDouble());

  OptionalDeformation_i::RestoreDeformation(theSObject,theMap);
  
  return this;
}


//----------------------------------------------------------------------------
void VISU::CutPlanes_i::ToStream(std::ostringstream& theStr){
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myNbPlanes", int(GetNbPlanes()));
  Storable::DataToStream( theStr, "myDisplacement", GetDisplacement());
  Storable::DataToStream( theStr, "myBasePlane", int(GetOrientationType()));
  Storable::DataToStream( theStr, "aRot[0]", GetRotateX());
  Storable::DataToStream( theStr, "aRot[1]", GetRotateY());

  QString aStrPos, aStrCon;
  for(int i = 0, iEnd = GetNbPlanes(); i < iEnd; i++){
    aStrPos.append(QString::number(GetPlanePosition(i)) + "|");
    aStrCon.append(QString::number(IsDefault(i)) + "|");
  }
  Storable::DataToStream( theStr, "myPlanePosition",  (const char*)aStrPos.toLatin1());
  Storable::DataToStream( theStr, "myPlaneCondition", (const char*)aStrCon.toLatin1());
  OptionalDeformation_i::DeformationToStream(theStr);
}


//----------------------------------------------------------------------------
VISU::CutPlanes_i
::~CutPlanes_i()
{
  if(MYDEBUG) MESSAGE("CutPlanes_i::~CutPlanes_i()");
}


//----------------------------------------------------------------------------
void
VISU::CutPlanes_i
::SetOrientation(VISU::CutPlanes::Orientation theOrient,
		 CORBA::Double theXAngle, 
		 CORBA::Double theYAngle)
{
  struct TEvent: public SALOME_Event 
  {
    VISU_CutPlanesPL* myPipeLine;
    VISU_CutPlanesPL::PlaneOrientation myOrient;
    CORBA::Double myXAngle;
    CORBA::Double myYAngle;

    TEvent(VISU_CutPlanesPL* thePipeLine,
	   VISU_CutPlanesPL::PlaneOrientation theOrient,
	   CORBA::Double theXAngle, 
	   CORBA::Double theYAngle):
      myPipeLine(thePipeLine),
      myOrient(theOrient),
      myXAngle(theXAngle),
      myYAngle(theYAngle)
    {}

    virtual
    void
    Execute()
    {
      myPipeLine->SetOrientation(myOrient,
				 myXAngle,
				 myYAngle);
    }
  };

  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TEvent(GetSpecificPL(),
			      VISU_CutPlanesPL::PlaneOrientation(theOrient),
			      theXAngle,
			      theYAngle));
}

//----------------------------------------------------------------------------
VISU::CutPlanes::Orientation 
VISU::CutPlanes_i
::GetOrientationType() 
{ 
  return VISU::CutPlanes::Orientation(myCutPlanesPL->GetPlaneOrientation());
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::CutPlanes_i
::GetRotateX()
{
  return myCutPlanesPL->GetRotateX();
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::CutPlanes_i
::GetRotateY()
{
  return myCutPlanesPL->GetRotateY();
}


//----------------------------------------------------------------------------
void
VISU::CutPlanes_i
::SetDisplacement(CORBA::Double theDisp) 
{  
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_CutPlanesPL, double, int>
		   (GetSpecificPL(), &VISU_CutPlanesPL::SetDisplacement, theDisp, 0));
}

//----------------------------------------------------------------------------
CORBA::Double
VISU::CutPlanes_i
::GetDisplacement() 
{ 
  return myCutPlanesPL->GetDisplacement();
}


//----------------------------------------------------------------------------
void
VISU::CutPlanes_i
::SetPlanePosition(CORBA::Long thePlaneNumber, 
		   CORBA::Double thePlanePosition)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_CutPlanesPL, int, double>
		   (GetSpecificPL(), &VISU_CutPlanesPL::SetPartPosition, thePlaneNumber, thePlanePosition));
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::CutPlanes_i
::GetPlanePosition(CORBA::Long thePlaneNumber)
{ 
  return myCutPlanesPL->GetPartPosition(thePlaneNumber);
}


//----------------------------------------------------------------------------
void
VISU::CutPlanes_i
::SetDefault(CORBA::Long thePlaneNumber)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_CutPlanesPL, int>
		   (GetSpecificPL(), &VISU_CutPlanesPL::SetPartDefault, thePlaneNumber));
}

//----------------------------------------------------------------------------
CORBA::Boolean 
VISU::CutPlanes_i
::IsDefault(CORBA::Long thePlaneNumber)
{ 
  return myCutPlanesPL->IsPartDefault(thePlaneNumber);
}


//----------------------------------------------------------------------------
void 
VISU::CutPlanes_i
::SetNbPlanes(CORBA::Long theNb) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_CutPlanesPL, int>
		   (GetSpecificPL(), &VISU_CutPlanesPL::SetNbParts, theNb));
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::CutPlanes_i
::GetNbPlanes() 
{ 
  return myCutPlanesPL->GetNbParts();
}


//----------------------------------------------------------------------------
void 
VISU::CutPlanes_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){ 
    myCutPlanesPL = VISU_CutPlanesPL::New();
  }else
    myCutPlanesPL = dynamic_cast<VISU_CutPlanesPL*>(thePipeLine);

  InitDeformedPipeLine(myCutPlanesPL);
  TSuperClass::CreatePipeLine(myCutPlanesPL);
}


//---------------------------------------------------------------
bool
VISU::CutPlanes_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

//----------------------------------------------------------------------------
VISU_Actor* 
VISU::CutPlanes_i
::CreateActor()
{
  if(VISU_Actor* anActor = TSuperClass::CreateActor()){
    anActor->SetVTKMapping(true);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aDispMode = aResourceMgr->integerValue("VISU" , "cut_planes_represent", 1);
    anActor->SetRepresentation(aDispMode);
    return anActor;
  }
  return NULL;
}

void
VISU::CutPlanes_i::
SameAs(const Prs3d_i* theOrigin){
  if(MYDEBUG) MESSAGE("CutPlanes_i::SameAs()");
  TSuperClass::SameAs(theOrigin);
  OptionalDeformation_i::SameAsDeformation(dynamic_cast<const Deformation_i*>(theOrigin));
}

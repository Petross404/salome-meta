// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Deformation_i.cc
//  Author : 
//  Module : VISU
//
#include "VISU_Deformation_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_Prs3dUtils.hh"

#include "VISU_DeformationPL.hxx"
#include "VISU_Convertor.hxx"
#include "VISU_DeformationPL.hxx"
#include "VISUConfig.hh"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//---------------------------------------------------------------
VISU::Deformation_i::Deformation_i(VISU::ColoredPrs3d_i *thePrs3d):
  myColoredPrs3d(thePrs3d)
{
  if(MYDEBUG) MESSAGE("Deformation_i::Deformation_i()");
}

//---------------------------------------------------------------
VISU::Deformation_i::~Deformation_i()
{
  if(MYDEBUG) MESSAGE("Deformation_i::~Deformation_i()");
}

//---------------------------------------------------------------
void VISU::Deformation_i::SetScale(CORBA::Double theScale)
{
  if(MYDEBUG) MESSAGE("Deformation_i::SetScale()");

  VISU::TSetModified aModified(GetColoredPrs3d());

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformationPL, double>
		   (GetSpecificDeformedPL(), &VISU_DeformationPL::SetScale, theScale));
}

//---------------------------------------------------------------
void VISU::Deformation_i::InitDeformedPipeLine(VISU_DeformationPL* theDeformedPipeLine){
  
  if(MYDEBUG) MESSAGE("Deformation_i::InitDeformedPipeLine()");
  myDeformationPL = theDeformedPipeLine;
}

//---------------------------------------------------------------
CORBA::Double 
VISU::Deformation_i
::GetScale()
{
  if(MYDEBUG) MESSAGE("Deformation_i::GetScale()");
  return GetSpecificDeformedPL()->GetScale();
}

//---------------------------------------------------------------
VISU::Entity VISU::Deformation_i::GetVectorialFieldEntity(){
  return myVectorialEntity;
}

//---------------------------------------------------------------
char* VISU::Deformation_i::GetVectorialFieldName(){
  return CORBA::string_dup(myVectorialFieldName.c_str());
}

//---------------------------------------------------------------
void VISU::Deformation_i::
DeformationToStream(std::ostringstream& theStr)
{
  Storable::DataToStream(theStr,"myScaleFactor", GetScale());
  Storable::DataToStream(theStr,"myVectorialField", GetVectorialFieldName());
  Storable::DataToStream(theStr,"myVectorialEntiry", GetVectorialFieldEntity());

}

//---------------------------------------------------------------
void
VISU::Deformation_i::RestoreDeformation(SALOMEDS::SObject_ptr theSObject,
					const Storable::TRestoringMap& theMap)
{
  SetScale(VISU::Storable::FindValue(theMap,"myScaleFactor").toDouble());
  VISU::Entity anEntity = VISU::Entity(VISU::Storable::FindValue(theMap, "myVectorialEntiry").toInt());
  
  SetVectorialField(anEntity,
		    VISU::Storable::FindValue(theMap, "myVectorialField").toLatin1().constData()); 
}

//---------------------------------------------------------------
void
VISU::Deformation_i::SameAsDeformation(const Deformation_i *aDeformedPrs){
  if(const Deformation_i* aPrs = dynamic_cast<const Deformation_i*>(aDeformedPrs)) {
    Deformation_i* anOrigin = const_cast<Deformation_i*>(aPrs);
    
    CORBA::String_var aVectorialFieldName = anOrigin->GetVectorialFieldName();
    VISU::Entity anEntity = anOrigin->GetVectorialFieldEntity();
    this->SetVectorialField(anEntity,
			    aVectorialFieldName);
    this->SetScale(anOrigin->GetScale());
  }
}

void VISU::Deformation_i::SetVectorialField(Entity theEntity, const char* theFieldName){
  if(MYDEBUG) MESSAGE("CutPlanes_i::SetVectorialField()");

  bool anIsModified = false;
  if(!anIsModified)
    anIsModified |= GetVectorialFieldEntity() != theEntity;
  
  if(!anIsModified)
    anIsModified |= GetVectorialFieldName() != theFieldName;

  if(!anIsModified)
    return;

  ColoredPrs3d_i *aColoredPrs = GetColoredPrs3d();
  int aTimeStampNumber = aColoredPrs->GetTimeStampNumber();

  VISU::TEntity aEntity = VISU::TEntity(theEntity);

  VISU::Result_i::PInput anInput = aColoredPrs->GetCResult()->GetInput(aColoredPrs->GetCMeshName(),
								       theEntity,
								       theFieldName,
								       aTimeStampNumber);

  PField aVectorialField = anInput->GetField(aColoredPrs->GetCMeshName(), aEntity, theFieldName);

  VISU::PUnstructuredGridIDMapper anIDMapper = 
    anInput->GetTimeStampOnMesh(aColoredPrs->GetCMeshName(),
				aEntity,
				theFieldName,
				aTimeStampNumber);
  if(GetSpecificDeformedPL() && anIDMapper) {
    ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformationPL, VISU::PUnstructuredGridIDMapper>
		     (GetSpecificDeformedPL(), &VISU_DeformationPL::SetVectorialField, anIDMapper));
    
    VISU::TSetModified aModified(GetColoredPrs3d());
    
    myVectorialEntity = theEntity;
    myVectorialFieldName = theFieldName;
    myVectorialField = anIDMapper;
  }
};

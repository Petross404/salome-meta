// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ViewManager_i.hh
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_ViewManager_i_HeaderFile
#define VISU_ViewManager_i_HeaderFile

#include "VISUConfig.hh"

class SalomeApp_Application;

class VISU_Actor;
class VISU_ActorBase;

class SUIT_ViewWindow;

class SVTK_ViewWindow;
class Plot2d_ViewFrame;

class vtkRenderer;
class vtkCamera;


namespace VISU {
  class Prs3d_i;
  class Curve_i;
  class TActorFactory;

  class VISU_I_EXPORT ViewManager_i : public virtual POA_VISU::ViewManager,
                        public virtual Base_i
  {
  public:
    ViewManager_i(SALOMEDS::Study_ptr theStudy);
    virtual ~ViewManager_i();
    virtual VISU::VISUType GetType() { return VISU::TVIEWMANAGER;};

    virtual View3D_ptr    Create3DView();
    virtual View_ptr      GetCurrentView();
    virtual TableView_ptr CreateTableView(VISU::Table_ptr theTable);
    virtual XYPlot_ptr    CreateXYPlot();
    virtual void          Destroy(View_ptr theView);

  protected:
    SalomeApp_Application *myApplication;
  };

  vtkRenderer*     GetRenderer   (SUIT_ViewWindow* theViewWindow);
  vtkCamera*       GetCamera     (SUIT_ViewWindow* theViewWindow);

  void RepaintView (SUIT_ViewWindow* theViewWindow);

  enum Displaing {eDisplayAll, eDisplay, eDisplayOnly, eErase, eEraseAll, eUpdateData};
  VISU_I_EXPORT VISU_Actor* UpdateViewer (SUIT_ViewWindow* theViewWindow, int theDisplaing, Prs3d_i* thePrs = NULL);
  void UpdatePlot2d (Curve_i* theCurve, int theDisplaying, Plot2d_ViewFrame* theView = NULL);

  VISU_I_EXPORT VISU_Actor* FindActor(SVTK_ViewWindow* theViewWindow, VISU::Prs3d_i* thePrs3d);
  VISU_I_EXPORT VISU_ActorBase* FindActorBase(SVTK_ViewWindow* theViewWindow, VISU::TActorFactory* theActor);

  void DeleteActors (VISU::Prs3d_i* thePrs);
  void DeleteActors (VISU::Curve_i* thePrs);
}

#endif

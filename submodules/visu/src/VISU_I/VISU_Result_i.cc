// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Result_i.cc
//  Author : Alexey PETROV
//  Module : VISU

#include "VISU_MultiResult_i.hh"
#include "VISU_ResultUtils.hh"
#include "VISU_Prs3d_i.hh"

#include "VISU_Convertor_impl.hxx"
#include "VISU_CorbaMedConvertor.hxx"
#include "VISU_ConvertorUtils.hxx"
#include "VISU_PipeLine.hxx"

#include "SUIT_ResourceMgr.h"

#include "SALOMEDS_Tool.hxx"
#include "HDFascii.hxx"
#include "Basics_Utils.hxx"

#include "SUIT_Session.h"
#include "SALOME_Event.h"
#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"

#include "MED_Factory.hxx"

#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

// QT Includes
#include <QString>
#include <QFileInfo>
#include <QSemaphore>
#include <QThread>
#include <QDir>
#include <QList>

// VTK Includes
#include <vtkCell.h>
#include <vtkUnstructuredGrid.h>

// OCCT Includes
#include <Bnd_Box.hxx>

#ifdef _DEBUG_
static int MYDEBUG = 1;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

namespace VISU
{
  //---------------------------------------------------------------
  Result_var
  FindResult(SALOMEDS::SObject_ptr theSObject)
  {
    SALOMEDS::SComponent_var aSComponent = theSObject->GetFatherComponent();
    SALOMEDS::SObject_var aFather = theSObject->GetFather();
    CORBA::String_var aComponentID (aSComponent->GetID());
    CORBA::String_var aFatherID    (aFather->GetID());
    Result_var aResult;
    while (strcmp(aComponentID, aFatherID) != 0) {
      CORBA::Object_var anObject = SObjectToObject(aFather);
      if (!CORBA::is_nil(anObject)) {
        aResult = Result::_narrow(anObject);
        if (!aResult->_is_nil()) return aResult;
      }
      aFather = aFather->GetFather();
      aFatherID = aFather->GetID();
    }
    return aResult;
  }


  //---------------------------------------------------------------
  Result_i*
  GetResult(SALOMEDS::Study_ptr theStudy,
            const std::string& theResultEntry)
  {
    Result_i* result = NULL;
    if (!CORBA::is_nil(theStudy)) {
      SALOMEDS::SComponent_var aSComponent = theStudy->FindComponent("VISU");
      if (!CORBA::is_nil(aSComponent)) {
        std::string compid = aSComponent->GetID();
        std::string aResultEntry = theResultEntry;
        if (theResultEntry.substr(0, compid.length()) != compid) aResultEntry = compid + theResultEntry.substr(compid.length());
        SALOMEDS::SObject_var aSObject = theStudy->FindObjectID(aResultEntry.c_str());
        CORBA::Object_var anObject = SObjectToObject(aSObject);
        result = dynamic_cast<VISU::Result_i*>(GetServant(anObject).in());
      }
    }
    return result;
  }


  //---------------------------------------------------------------
  std::string
  GetComponentDataType (SALOMEDS::SObject_ptr theSObject)
  {
    SALOMEDS::SComponent_var aCompRefSObj = theSObject->GetFatherComponent();
    CORBA::String_var aDataType = aCompRefSObj->ComponentDataType();
    return aDataType.in();
  }


  //---------------------------------------------------------------
  void
  CreateReference (SALOMEDS::Study_ptr theStudyDocument,
                   const string& theFatherEntry,
                   const string& theRefEntry)
  {
    SALOMEDS::StudyBuilder_var aStudyBuilder = theStudyDocument->NewBuilder();
    SALOMEDS::SObject_var aFather = theStudyDocument->FindObjectID(theFatherEntry.c_str());
    SALOMEDS::SObject_var newObj = aStudyBuilder->NewObject(aFather);
    SALOMEDS::SObject_var aRefSObj = theStudyDocument->FindObjectID(theRefEntry.c_str());
    aStudyBuilder->Addreference(newObj,aRefSObj);
  }

  //---------------------------------------------------------------
  // Issue 0021403. Remove a ColoredPrs3dHolder if a Result it refers to is removed
  //
  void RemoveHolders(VISU::Result_i* theResult)
  {
    CORBA::String_var resultID1 = theResult->GetID();
    SALOMEDS::SObject_var theResSObject = theResult->GetSObject();
    SALOMEDS::Study_var aStudyDocument = theResSObject->GetStudy();
    SALOMEDS::SComponent_var aCompSObj = theResSObject->GetFatherComponent();
    SALOMEDS::ChildIterator_var aChildIter = aStudyDocument->NewChildIterator(aCompSObj);
    for (aChildIter->InitEx(true); aChildIter->More(); aChildIter->Next())
    {
      SALOMEDS::SObject_var aChildSObject = aChildIter->Value();
      if ( CORBA::is_nil( aChildSObject )) continue;
      CORBA::Object_var aChildObj = aChildSObject->GetObject();
      if ( CORBA::is_nil( aChildObj )) continue;
      VISU::ColoredPrs3dHolder_var prsHolder = ColoredPrs3dHolder::_narrow( aChildObj );
      if ( prsHolder->_is_nil() ) continue;
      // check if the prsHolder refers to theResult
      VISU::ColoredPrs3dHolder::BasicInput_var input = prsHolder->GetBasicInput();
      CORBA::String_var resultID2 = input->myResult->GetID();
      if ( strcmp( resultID1, resultID2 ) == 0 )
        prsHolder->RemoveFromStudy();
    }
  }

//---------------------------------------------------------------
}


//---------------------------------------------------------------
const string VISU::Result_i::myComment = "RESULT";
const char* VISU::Result_i::GetComment() const { return myComment.c_str();}

//---------------------------------------------------------------
VISU::Result_i
::Result_i(SALOMEDS::Study_ptr theStudy,
           const ESourceId& theSourceId,
           const ECreationId& theCreationId,
           CORBA::Boolean theIsBuildImmediately,
           CORBA::Boolean theIsBuildFields,
           CORBA::Boolean theIsBuildMinMax,
           CORBA::Boolean theIsBuildGroups):
  myStudyDocument(SALOMEDS::Study::_duplicate(theStudy)),
  myCreationId(theCreationId),
  mySourceId(theSourceId),
  myIsBuildImmediately(theIsBuildImmediately),
  myIsBuildFields(theIsBuildFields),
  myIsBuildMinMax(theIsBuildMinMax),
  myIsBuildGroups(theIsBuildGroups),
  myIsEntitiesDone(false),
  myIsFieldsDone(false),
  myIsGroupsDone(false),
  myIsMinMaxDone(false),
  myIsAllDone(false),
  myIsBuildParts(false),
  myIsPartsDone(false)
{
  if(MYDEBUG) MESSAGE("Result_i::Result_i - this = "<<this);
  myStudy = VISU::GetStudy(myStudyDocument);
}


//---------------------------------------------------------------
VISU::Result_i
::Result_i():
  myIsEntitiesDone(false),
  myIsFieldsDone(false),
  myIsGroupsDone(false),
  myIsMinMaxDone(false),
  myIsAllDone(false),
  myIsBuildParts(false),
  myIsPartsDone(false)
{
  if(MYDEBUG) MESSAGE("Result_i::Result_i - this = "<<this);
}


//---------------------------------------------------------------
VISU::Result_i
::~Result_i()
{
  MESSAGE("Result_i::~Result_i() - this = "<<this);
  if (GetSourceId() == eRestoredFile)
    VISU::RemoveFile(myFileInfo.filePath().toLatin1().data());
}


//---------------------------------------------------------------
void
VISU::Result_i
::RemoveFromStudy()
{
  struct TRemoveFromStudy: public SALOME_Event
  {
    VISU::Result_i* myRemovable;
    TRemoveFromStudy(VISU::Result_i* theRemovable):
      myRemovable(theRemovable)
    {}

    virtual
    void
    Execute()
    {
      RemoveHolders(myRemovable); 
      VISU::RemoveFromStudy(myRemovable->GetSObject(),false);
      myRemovable->UnRegister();
    }
  };

  // Remove the result with all presentations and other possible sub-objects
  ProcessVoidEvent(new TRemoveFromStudy(this));
}


//---------------------------------------------------------------
void
VISU::Result_i
::ConnectObserver(TResultObserver* theObserver,
                  boost::signalslib::connection& theConnection)
{
  if(theConnection.connected())
    theConnection.disconnect();

  theConnection =
    myUpdateObserverSignal.connect(boost::bind(boost::bind(&TResultObserver::UpdateFromResult,
                                                           theObserver,
                                                           _1),
                                               this));
}


//---------------------------------------------------------------
void
VISU::Result_i
::UpdateObservers()
{
  struct TEvent: public SALOME_Event
  {
    VISU::Result_i::TUpdateObserverSignal& myUpdateObserverSignal;

    TEvent(VISU::Result_i::TUpdateObserverSignal& theUpdateObserverSignal):
      myUpdateObserverSignal(theUpdateObserverSignal)
    {}

    virtual
    void
    Execute()
    {
      myUpdateObserverSignal();
    }
  };

  ProcessVoidEvent(new TEvent(myUpdateObserverSignal));
}


//---------------------------------------------------------------
size_t
VISU::Result_i
::IsPossible()
{
  try{
    size_t aSize = myInput->GetSize();
    size_t aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
    MESSAGE("Result_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<float(aResult));
    return aResult;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return 0;
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::Result_i
::BuildAll()
{
  if(MYDEBUG) MESSAGE("Result_i::Build - myIsAllDone = "<<myIsAllDone);
  if(myIsAllDone)
    return 1;
  if(!IsPossible())
    return 0;
  try{
    const VISU::TMeshMap& aMeshMap = myInput->GetMeshMap();
    VISU::TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
    for(; aMeshMapIter != aMeshMap.end(); aMeshMapIter++){
      const string& aMeshName = aMeshMapIter->first;
      const VISU::PMesh aMesh = aMeshMapIter->second;
      const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter;
      //Import fields
      aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
          const VISU::TEntity& anEntity = aMeshOnEntityMapIter->first;
          const VISU::PMeshOnEntity aMeshOnEntity = aMeshOnEntityMapIter->second;
          const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
          VISU::TFieldMap::const_iterator aFieldMapIter = aFieldMap.begin();
          for(; aFieldMapIter != aFieldMap.end(); aFieldMapIter++){
            const string& aFieldName = aFieldMapIter->first;
            const VISU::PField aField = aFieldMapIter->second;
            const VISU::TValField& aValField = aField->myValField;
            VISU::TValField::const_iterator aValFieldIter = aValField.begin();
            for(; aValFieldIter != aValField.end(); aValFieldIter++){
              int aTimeStamp = aValFieldIter->first;
            try{
              myInput->GetTimeStampOnMesh(aMeshName,anEntity,aFieldName,aTimeStamp);
            }catch(std::exception& exc){
              INFOS("Follow exception was occured :\n"<<exc.what());
            }catch(...){
              INFOS("Unknown exception was occured!!!");
            }
            }
          }
        //Importing groups
        const VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
        VISU::TGroupMap::const_iterator aGroupMapIter = aGroupMap.begin();
        for(; aGroupMapIter != aGroupMap.end(); aGroupMapIter++){
          const string& aGroupName = aGroupMapIter->first;
          try{
            myInput->GetMeshOnGroup(aMeshName,aGroupName);
          }catch(std::exception& exc){
            INFOS("Follow exception was occured :\n"<<exc.what());
          }catch(...){
            INFOS("Unknown exception was occured!!!");
          }
        }
        //Import families
        const VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
        VISU::TFamilyMap::const_iterator aFamilyMapIter = aFamilyMap.begin();
        for(; aFamilyMapIter != aFamilyMap.end(); aFamilyMapIter++){
          const string& aFamilyName = aFamilyMapIter->first;
          try{
            myInput->GetFamilyOnEntity(aMeshName,anEntity,aFamilyName);
          }catch(std::exception& exc){
            INFOS("Follow exception was occured :\n"<<exc.what());
          }catch(...){
            INFOS("Unknown exception was occured!!!");
          }
        }
        //Import mesh on entity
        try{
          myInput->GetMeshOnEntity(aMeshName,anEntity);
        }catch(std::exception& exc){
          INFOS("Follow exception was occured :\n"<<exc.what());
        }catch(...){
          INFOS("Unknown exception was occured!!!");
        }
      }
    }
    myIsAllDone = 1;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!!!");
  }
  return myIsAllDone;
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::Result_i
::Build(CORBA::Boolean theIsBuildAll,
        CORBA::Boolean theIsAtOnce)
{
  if(theIsBuildAll)
    theIsAtOnce = true;

  if(Build(SALOMEDS::SObject::_nil(), theIsAtOnce)){

    // Set icon
    SALOMEDS::StudyBuilder_var aStudyBuilder = myStudyDocument->NewBuilder();

    SALOMEDS::GenericAttribute_var anAttr;
    SALOMEDS::AttributePixMap_var  aPixmap;
    SALOMEDS::AttributeUserID_var aId;

    anAttr  = aStudyBuilder->FindOrCreateAttribute( mySObject, "AttributePixMap" );
    aPixmap = SALOMEDS::AttributePixMap::_narrow( anAttr );
    aPixmap ->SetPixMap("ICON_TREE_RESULT");

    std::string auid = "AttributeUserID";
    auid += Kernel_Utils::GetGUID(Kernel_Utils::ObjectdID);
    anAttr  = aStudyBuilder->FindOrCreateAttribute( mySObject, auid.c_str() );
    aId     = SALOMEDS::AttributeUserID::_narrow( anAttr );
    aId     ->SetValue("VISU.RESULT");

    if(theIsBuildAll)
      return BuildAll();
    return true;
  }

  return false;
}


//---------------------------------------------------------------
VISU::Storable* VISU::Result_i::Build(SALOMEDS::SObject_ptr theSObject,
                                      CORBA::Boolean theIsAtOnce)
{
  if(!myInput)
    return NULL;

  if(IsDone())
    return this;

  mySComponent = FindOrCreateVisuComponent(myStudyDocument);
  CORBA::String_var aSComponentEntry = mySComponent->GetID();
  CORBA::String_var anIOR(GetID());
  QString aComment;
  aComment.sprintf("myComment=%s;myFileName=%s;myInitFileName=%s",
                   GetComment(),
                   myFileInfo.filePath().toLatin1().data(),
                   GetInitFileName().c_str()); // Restoring of Python dump
  std::string aResultEntry =
    CreateAttributes(myStudy,
                     aSComponentEntry.in(),
                     NO_ICON,
                     anIOR.in(),
                     GetName(),
                     NO_PERFSITENT_REF,
                     aComment.toLatin1().data(),
                     true);
  mySObject = myStudyDocument->FindObjectID(aResultEntry.c_str());
  if(!CORBA::is_nil(theSObject)){
    CORBA::String_var aString = theSObject->GetID();
    CreateReference(myStudyDocument, aResultEntry, aString.in());
  }

  if(theIsAtOnce){
    BuildEntities(this,
                  myInput,
                  &myIsEntitiesDone,
                  aResultEntry,
                  theIsAtOnce,
                  myIsBuildGroups,
                  myIsBuildFields,
                  myIsBuildParts,
                  myStudy);

    BuildGroups(this,
                myInput,
                &myIsGroupsDone,
                myIsBuildGroups,
                theIsAtOnce,
                myStudy);

    BuildFieldDataTree(this,
                       myInput,
                       &myIsFieldsDone,
                       myIsBuildFields,
                       &myIsMinMaxDone,
                       myIsBuildMinMax,
                       myStudy);
  } else {
    boost::thread aThread(boost::bind(boost::bind(&Result_i::BuildDataTree, this, _1),
                                      aResultEntry));
  }

  return this;
}


//---------------------------------------------------------------
void
VISU::Result_i
::BuildDataTree(const std::string& theResultEntry)
{
  BuildEntities(this,
                myInput,
                &myIsEntitiesDone,
                theResultEntry,
                false,
                myIsBuildGroups,
                myIsBuildFields,
                myIsBuildParts,
                myStudy);

  {
    boost::thread aThread(boost::bind(&BuildGroups,
                                      this,
                                      myInput,
                                      &myIsGroupsDone,
                                      myIsBuildGroups,
                                      false,
                                      myStudy));
  }

  {
    boost::thread aThread(boost::bind(&BuildFieldDataTree,
                                      this,
                                      myInput,
                                      &myIsFieldsDone,
                                      myIsBuildFields,
                                      &myIsMinMaxDone,
                                      myIsBuildMinMax,
                                      myStudy));
  }
}

//---------------------------------------------------------------
VISU::Storable*
VISU::Result_i
::BuildAll(SALOMEDS::SObject_ptr theSObject)
{
  if(MYDEBUG) MESSAGE("Result_i::Build");
  try{
    Build(theSObject);
    BuildAll();
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
    return NULL;
  }catch(...){
    INFOS("Unknown exception was occured!!!");
    return NULL;
  }

  return this;
}


//---------------------------------------------------------------
VISU::Storable*
VISU::Result_i
::Create(const char* theFileName)
{        
  try {
    myFileInfo.setFile(theFileName);
    myFileName = myFileInfo.fileName().toLatin1().data();

    if(!myIsBuildParts){
      SetInitFileName(myFileInfo.filePath().toLatin1().data());
      SetName(VISU::GenerateName(myFileInfo.fileName().toLatin1().data()).toLatin1().data(), false);
    }

    if(mySourceId == eRestoredFile){
      QString aTmpDir(SALOMEDS_Tool::GetTmpDir().c_str());
      std::string aFileName = VISU::MakeFileName(myFileInfo.fileName().toLatin1().data(), this);
      QString aPathToCopy(aTmpDir + aFileName.c_str());
      if(!VISU::CopyFile(myFileInfo.absoluteFilePath().toLatin1().data(), aPathToCopy.toLatin1().data()))
        return NULL;

      myFileInfo.setFile(aPathToCopy);
      myFileName = myFileInfo.fileName().toLatin1().data();
    }

    myInput.reset(CreateConvertor(myFileInfo.absoluteFilePath().toLatin1().data()));

    if(myInput){
      if(myIsBuildImmediately)
        Build(SALOMEDS::SObject::_nil());
      return this;
    }
  }catch(std::exception& exc){
    MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
  }catch(...){
    MSG(MYDEBUG,"Unknown exception !!!");
  }
  return NULL;
}


//---------------------------------------------------------------
VISU::Storable*
VISU::Result_i
::Create(SALOMEDS::SObject_ptr theMedSObject)
{
  if(MYDEBUG)  MESSAGE("Result_i::Create MedObject from SALOMEDS::SObject_ptr");
  try{
    myInput.reset(CreateMEDConvertor(theMedSObject));
    if(!myInput)
      return NULL;

    myInput->Build();

    string aCompDataType = GetComponentDataType(theMedSObject);
    myFileInfo.setFile(aCompDataType.c_str());
    myFileName = VISU::GenerateName("aResult").toLatin1().data();

    SetInitFileName(aCompDataType);
    SetName(VISU::GenerateName(myFileInfo.fileName().toLatin1().data()).toLatin1().data(), false);

    return Build(theMedSObject);
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!!!");
  }
  return NULL;
}


//---------------------------------------------------------------
VISU::Storable*
VISU::Result_i
::Create(SALOME_MED::FIELD_ptr theField)
{
  if(MYDEBUG)  MESSAGE("Result_i::Create MedObject from SALOME_MED::FIELD_ptr");
  try{
    myInput.reset(CreateMEDFieldConvertor(theField));
    if(!myInput)
      return NULL;

    myInput->Build();

    string aCompDataType = "MED";
    myFileInfo.setFile(aCompDataType.c_str());

    myFileName = VISU::GenerateName("aResult").toLatin1().data();
    SetInitFileName(aCompDataType);
    SetName(VISU::GenerateName(myFileInfo.fileName().toLatin1().data()).toLatin1().data(), false);

    CORBA::String_var anIOR = myStudyDocument->ConvertObjectToIOR(theField);
    SALOMEDS::SObject_var aFieldSObject = myStudyDocument->FindObjectIOR(anIOR);

    return Build(aFieldSObject);
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!!!");
  }
  return NULL;
}


//---------------------------------------------------------------
bool
VISU::Result_i
::Save(SALOMEDS::SComponent_ptr theComponent,
       const std::string& theURL,
       bool theIsMultiFile,
       bool theIsASCII,
       TFileNames& theFileNames,
       TFileNames& theFiles)
{
  switch(GetCreationId()){
  case Result_i::eImportFile:
  case Result_i::eCopyAndImportFile: {
    INITMSG(MYDEBUG, "Result::Save - this = "<<this<<"\n");
    INITMSGA(MYDEBUG, 0, "theIsMultiFile = "<<theIsMultiFile<<"; theIsASCII = "<<theIsASCII<<"\n");

    std::string aPrefix;
    if (theIsMultiFile) {
      CORBA::String_var anURL = GetStudyDocument()->URL();
      aPrefix = SALOMEDS_Tool::GetNameFromPath(anURL.in());
    }

    std::string aFileName = aPrefix + "_" + GetFileName().c_str();
    BEGMSG(MYDEBUG, "aFileName = '"<<aFileName<<"'\n");

    std::string aFile = GetFileInfo().filePath().toLatin1().data();
    BEGMSG(MYDEBUG, "aFile = '"<<aFile<<"'\n");

    if(theIsMultiFile || theIsASCII){
      std::string aPathToCopy(theURL + aFileName);
      BEGMSG(MYDEBUG, "aPathToCopy = '"<<aPathToCopy<<"'\n");

      if(!VISU::CopyFile(aFile, aPathToCopy))
        return false;

      if(theIsASCII)
        HDFascii::ConvertFromHDFToASCII(const_cast<char*>(aPathToCopy.c_str()), true);
    }

    theFileNames.push_back(aFileName);
    theFiles.push_back(aFile);

    return true;
  }}

  return false;
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::Result_i
::CanCopy(SALOMEDS::SObject_ptr theObject)
{
  switch(GetCreationId()){
  case Result_i::eImportFile:
  case Result_i::eCopyAndImportFile:
    return true;
  }

  return false;
}


//---------------------------------------------------------------
bool
VISU::Result_i
::CopyFrom(SALOMEDS::SObject_ptr theObject,
           CORBA::Long& theObjectID,
           const std::string& theTmpDir,
           TFileNames& theFileNames)
{
  if(!Storable::CopyFrom(theObject, theObjectID, theTmpDir, theFileNames))
    return false;

  SALOMEDS::Study_var aStudy = theObject->GetStudy();
  CORBA::String_var anURL = aStudy->URL();
  std::string aPrefix = SALOMEDS_Tool::GetNameFromPath(anURL.in());
  std::string aFileName = aPrefix + GetFileInfo().fileName().toLatin1().data();

  static QString aCommand;
  std::string aFilePath =  theTmpDir + aFileName;
  aCommand.sprintf(" \"%s\" \"%s\"", GetFileInfo().filePath().toLatin1().data(), aFilePath.c_str());
  aCommand = QDir::convertSeparators( aCommand );
  aCommand.prepend( COPY_COMMAND );

  if(system(aCommand.toLatin1().data()) != 0) {
    if(MYDEBUG) MESSAGE("VISU_Gen_i::Copy - Cann't execute the command :"<<aCommand.toLatin1().data());
    return false;
  }

  SALOMEDS::SComponent_var aSComponent = aStudy->FindComponent("VISU");

  theFileNames.push_back(aFileName);

  return true;
}


//---------------------------------------------------------------
VISU::Storable*
VISU::Result_i
::Restore(SALOMEDS::SObject_ptr theSObject,
          const Storable::TRestoringMap& theMap,
          const string& thePrefix,
          CORBA::Boolean theIsMultiFile)
{
  if(MYDEBUG) MESSAGE("Result_i::Restore - " << thePrefix);
  mySObject = SALOMEDS::SObject::_duplicate(theSObject);
  mySObject->Register(); // Increment reference counter to stored SObject
  mySComponent = mySObject->GetFatherComponent();
  myStudyDocument = theSObject->GetStudy();
  myStudy = VISU::GetStudy(myStudyDocument);

  myCreationId = ECreationId(Storable::FindValue(theMap, "myCreationId").toInt());
  mySourceId = eRestoredFile;
  if(myCreationId == eImportMed || myCreationId == eImportMedField)
    mySourceId = eRestoredComponent;

  myIsBuildFields = Storable::FindValue(theMap, "myIsBuildFields", "1").toInt();
  myIsBuildMinMax = Storable::FindValue(theMap, "myIsBuildMinMax", "1").toInt();
  myIsBuildGroups = Storable::FindValue(theMap, "myIsBuildGroups", "1").toInt();

  myIsBuildImmediately = true;

  try {
    myFileName = VISU::Storable::FindValue(theMap, "myName").toLatin1().data();
#ifdef WIN32
    for (int i = 0; i < myFileName.length(); i++)
    {
      if (myFileName[i] == ':')
        myFileName[i] = '_';
    }
#endif
    SetInitFileName(VISU::Storable::FindValue(theMap, "myInitFileName").toLatin1().data());

    SALOMEDS::SObject_var aRefSObj, aTargetRefSObj;
    if (theSObject->FindSubObject(1, aRefSObj) && aRefSObj->ReferencedObject(aTargetRefSObj)) {
      if(MYDEBUG) MESSAGE("Result_i::GetInput - There is some reference.");
      SALOMEDS::SComponent_var aCompRefSObj = aTargetRefSObj->GetFatherComponent();
      CORBA::String_var aDataType = aCompRefSObj->ComponentDataType();
      myFileInfo.setFile(aDataType.in());
      if(MYDEBUG) MESSAGE("Result_i::GetInput - aDataType = " << aDataType);
      Engines::EngineComponent_var aEngComp =
        Base_i::myEnginesLifeCycle->FindOrLoad_Component("FactoryServer", aDataType.in());
      if (CORBA::is_nil(aEngComp))
        throw std::runtime_error("Restore - There is no aEngComp for the aDataType !!!");
      SALOMEDS::StudyBuilder_var aStudyBuilder = myStudyDocument->NewBuilder();
      SALOMEDS::Driver_var aDriver = SALOMEDS::Driver::_narrow(aEngComp);
      aStudyBuilder->LoadWith(aCompRefSObj, aDriver);
      // create field or MED converter
      CORBA::Object_var aMedObject = VISU::SObjectToObject(aTargetRefSObj);
      SALOME_MED::FIELD_var aField = SALOME_MED::FIELD::_narrow(aMedObject);
      if (!CORBA::is_nil(aField)) {
        // create field converter
        myInput.reset(CreateMEDFieldConvertor(aField));
        myInput->Build();
      }
      else if (strcmp(aDataType, "MED") == 0) {
        // create MED converter
        myInput.reset(CreateMEDConvertor(aTargetRefSObj));
        myInput->Build();
      }
      else {
        throw std::runtime_error("GetInput - There is no convertor for the aDataType !!!");
      }
    } else {
      myFileInfo.setFile(thePrefix.c_str());

      std::string aStudyPrefix ("");
      if (theIsMultiFile) {
        CORBA::String_var anURL(GetStudyDocument()->URL());
        aStudyPrefix = SALOMEDS_Tool::GetNameFromPath(anURL.in());
      }
      if (!myFileInfo.isFile()) {
        std::string aFileName = thePrefix + aStudyPrefix + "_" + myFileName;
        myFileInfo.setFile(aFileName.c_str());
      }
      if(MYDEBUG)
        MESSAGE("Result_i::Restore - aFileName = " << myFileInfo.filePath().toLatin1().data() << "; " << myFileInfo.isFile());

      if (HDFascii::isASCII(myFileInfo.filePath().toLatin1().data())) {
        MESSAGE("ConvertFromASCIIToHDF(" << myFileInfo.filePath().toLatin1().data() << ")");
        char* aResultPath = HDFascii::ConvertFromASCIIToHDF(myFileInfo.filePath().toLatin1().data());
        MESSAGE("ConvertFromASCIIToHDF() DONE : " << aResultPath);
        char* aHDFFileName = new char[strlen(aResultPath) + 19];
        sprintf(aHDFFileName, "%shdf_from_ascii.hdf", aResultPath);
        
        if (theIsMultiFile) { // set this file as new - temporary
          static QString aCommand;
          aCommand.sprintf(" \"%s\" \"%s%s\"",aHDFFileName, aResultPath, myFileInfo.baseName().toLatin1().data());
          aCommand = QDir::convertSeparators( aCommand );
          aCommand.prepend( MOVE_COMMAND );
        
          if (system(aCommand.toLatin1().data()) == -1) {
            if(MYDEBUG) MESSAGE("VISU::Result_i::Restore - Can't execute the command :" << aCommand.toLatin1().data());
            return NULL;
          } else {
            if(MYDEBUG) MESSAGE("VISU::Result_i::Restore - " << aCommand.toLatin1().data());
          }
          myFileInfo.setFile(QString(aResultPath) + myFileInfo.baseName());
        } else { // change current temporary file to the new: with hdf-format
          static QString aCommand;
          aCommand.sprintf(" \"%s\" \"%s\"\0",aHDFFileName, myFileInfo.filePath().toLatin1().data());
          aCommand = QDir::convertSeparators( aCommand );
          aCommand.prepend( MOVE_COMMAND );
        
          if (system(aCommand.toLatin1().data()) == -1) {
            if(MYDEBUG) MESSAGE("VISU::Result_i::Restore - Can't execute the command :" << aCommand.toLatin1().data());
            return NULL;
          } else {
            if(MYDEBUG) MESSAGE("VISU::Result_i::Restore - " << aCommand.toLatin1().data());
          }
          SALOMEDS::ListOfFileNames_var anEmptyList = new SALOMEDS::ListOfFileNames;
          SALOMEDS_Tool::RemoveTemporaryFiles(aResultPath, anEmptyList.in(), true);
        }
        mySourceId = eRestoredFile;
        delete(aResultPath);
        delete(aHDFFileName);
      } else if (!theIsMultiFile) {
        mySourceId = eRestoredFile;
      } else {
        mySourceId = eSavedFile;
      }
      if(MYDEBUG) MESSAGE("VISU::Result_i::Restore - mySourceId = " << mySourceId);
      myInput.reset(CreateConvertor(myFileInfo.filePath().toLatin1().data()));

      myInput->BuildEntities();
      if(myIsBuildFields){
        myInput->BuildFields();
        myIsFieldsDone = true;
        if(myIsBuildMinMax){
          myInput->BuildMinMax();
          myIsMinMaxDone = true;
        }
      }
      if(myIsBuildGroups){
        myInput->BuildGroups();
        myIsGroupsDone = true;
      }

      QString aComment;
      aComment.sprintf("myComment=%s;myFileName=%s;myInitFileName=%s",
                       GetComment(),
                       myFileInfo.filePath().toLatin1().data(),
                       GetInitFileName().c_str()); // Restoring of Python dump
      SALOMEDS::GenericAttribute_var anAttr;
      if (!theSObject->FindAttribute(anAttr, "AttributeString"))
        throw std::runtime_error("Build - There is no AttributeString for the SObject !!!");
      SALOMEDS::AttributeString_var aCmnt = SALOMEDS::AttributeString::_narrow(anAttr);
      aCmnt->SetValue(aComment.toLatin1().data());
    }
    bool anIsBuildAll = VISU::GetResourceMgr()->booleanValue("VISU", "full_med_loading", false);
    if(anIsBuildAll)
      BuildAll();
    return this;
  } catch(std::exception& exc) {
    INFOS("Follow exception was occured :\n"<<exc.what());
  } catch(...) {
    INFOS("Unknown exception was occured!!!");
  }
  return NULL;
}


//---------------------------------------------------------------
VISU::Result_i::PInput
VISU::Result_i
::GetInput(const std::string& theMeshName,
           VISU::Entity theEntity,
           const std::string& theFieldName,
           CORBA::Long theTimeStampNumber)
{
  return myInput;
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::Result_i
::IsDone()
{
  return
    myIsEntitiesDone &&
    (myIsBuildFields? myIsFieldsDone: true) &&
    (myIsBuildMinMax? myIsMinMaxDone: true) &&
    (myIsBuildGroups? myIsGroupsDone: true);
}

CORBA::Boolean
VISU::Result_i
::IsEntitiesDone()
{
  return myIsEntitiesDone;
}

void
VISU::Result_i
::SetBuildFields(CORBA::Boolean theIsBuildFields,
                 CORBA::Boolean theIsCalculateMinMax)
{
  myIsBuildFields = theIsBuildFields;
  if(theIsBuildFields)
    myIsBuildMinMax = theIsCalculateMinMax;
  else
    myIsBuildMinMax = false;
}

void
VISU::Result_i
::SetBuildGroups(CORBA::Boolean theIsBuildGroups)
{
  myIsBuildGroups = theIsBuildGroups;
}

CORBA::Boolean
VISU::Result_i
::IsFieldsDone()
{
  return myIsFieldsDone;
}

CORBA::Boolean
VISU::Result_i
::IsGroupsDone()
{
  return myIsGroupsDone;
}

CORBA::Boolean
VISU::Result_i
::IsMinMaxDone()
{
  return myIsMinMaxDone;
}

CORBA::Boolean
VISU::Result_i
::IsPartsDone()
{
  return myIsPartsDone;
}


//---------------------------------------------------------------
VISU::Result::EntityNames*
VISU::Result_i
::GetMeshNames()
{
  VISU::Result::EntityNames_var aResult = new VISU::Result::EntityNames();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult._retn();

  aResult->length(aMeshMap.size());
  VISU::TMeshMap::const_iterator anIter = aMeshMap.begin();
  for(size_t anId = 0; anIter != aMeshMap.end(); anIter++, anId++){
    const std::string& aName = anIter->first;
    aResult[anId] = aName.c_str();
  }

  return aResult._retn();
}


//---------------------------------------------------------------
VISU::Result::Entities*
VISU::Result_i
::GetEntities(const char* theMeshName)
{
  VISU::Result::Entities_var anEntities = new VISU::Result::Entities();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return anEntities._retn();

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return anEntities._retn();

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if(aMeshOnEntityMap.empty())
    return anEntities._retn();

  {
    anEntities->length(aMeshOnEntityMap.size());
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.begin();
    for(size_t anId = 0; anIter != aMeshOnEntityMap.end(); anIter++, anId++){
      const VISU::TEntity& anEntity = anIter->first;
      anEntities[anId] = VISU::Entity(anEntity);
    }
  }

  return anEntities._retn();
}


//---------------------------------------------------------------
VISU::Result::EntityNames*
VISU::Result_i
::GetFamilies(const char* theMeshName,
              VISU::Entity theEntity)
{
  VISU::Result::EntityNames_var aResult = new VISU::Result::EntityNames();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult._retn();

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return aResult._retn();

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if(aMeshOnEntityMap.empty())
    return aResult._retn();

  {
    VISU::TEntity anEntity = VISU::TEntity(theEntity);
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.find(anEntity);
    if(anIter == aMeshOnEntityMap.end())
      return aResult._retn();

    const VISU::PMeshOnEntity& aMeshOnEntity = anIter->second;
    const VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
    if(aFamilyMap.empty())
      return aResult._retn();

    {
      aResult->length(aFamilyMap.size());
      VISU::TFamilyMap::const_iterator anIter = aFamilyMap.begin();
      for(size_t anId = 0; anIter != aFamilyMap.end(); anIter++, anId++){
        const std::string& aName = anIter->first;
        aResult[anId] = aName.c_str();
      }
    }
  }

  return aResult._retn();
}


//---------------------------------------------------------------
VISU::Result::EntityNames*
VISU::Result_i
::GetGroups(const char* theMeshName)
{
  VISU::Result::EntityNames_var aResult = new VISU::Result::EntityNames();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult._retn();

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return aResult._retn();

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
  if(aGroupMap.empty())
    return aResult._retn();

  {
    aResult->length(aGroupMap.size());
    VISU::TGroupMap::const_iterator anIter = aGroupMap.begin();
    for(size_t anId = 0; anIter != aGroupMap.end(); anIter++, anId++){
      const std::string& aName = anIter->first;
      aResult[anId] = aName.c_str();
    }
  }

  return aResult._retn();
}


//---------------------------------------------------------------
VISU::Result::EntityNames*
VISU::Result_i
::GetFields(const char* theMeshName,
            VISU::Entity theEntity)
{
  VISU::Result::EntityNames_var aResult = new VISU::Result::EntityNames();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult._retn();

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return aResult._retn();

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if(aMeshOnEntityMap.empty())
    return aResult._retn();

  {
    VISU::TEntity anEntity = VISU::TEntity(theEntity);
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.find(anEntity);
    if(anIter == aMeshOnEntityMap.end())
      return aResult._retn();

    const VISU::PMeshOnEntity& aMeshOnEntity = anIter->second;
    const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
    if(aFieldMap.empty())
      return aResult._retn();

    {
      aResult->length(aFieldMap.size());
      VISU::TFieldMap::const_iterator anIter = aFieldMap.begin();
      for(size_t anId = 0; anIter != aFieldMap.end(); anIter++, anId++){
        const std::string& aName = anIter->first;
        aResult[anId] = aName.c_str();
      }
    }
  }

  return aResult._retn();
}


//---------------------------------------------------------------
VISU::Result::TimeStampNumbers*
VISU::Result_i
::GetTimeStampNumbers(const char* theMeshName,
                      VISU::Entity theEntity,
                      const char* theFieldName)
{
  VISU::Result::TimeStampNumbers_var aResult = new VISU::Result::TimeStampNumbers();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult._retn();

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return aResult._retn();

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if(aMeshOnEntityMap.empty())
    return aResult._retn();

  {
    VISU::TEntity anEntity = VISU::TEntity(theEntity);
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.find(anEntity);
    if(anIter == aMeshOnEntityMap.end())
      return aResult._retn();

    const VISU::PMeshOnEntity& aMeshOnEntity = anIter->second;
    const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
    if(aFieldMap.empty())
      return aResult._retn();

    {
      VISU::TFieldMap::const_iterator anIter = aFieldMap.find(theFieldName);
      if(anIter == aFieldMap.end())
        return aResult._retn();

      {
        const VISU::PField& aField = anIter->second;
        const VISU::TValField& aValField = aField->myValField;
        if(aValField.empty())
          return aResult._retn();

        {
          aResult->length(aValField.size());
          VISU::TValField::const_iterator anIter = aValField.begin();
          for(size_t anId = 0; anIter != aValField.end(); anIter++, anId++){
            const vtkIdType& aTimeStampNumber = anIter->first;
            aResult[anId] = aTimeStampNumber;
          }
        }
      }
    }
  }

  return aResult._retn();
}


//---------------------------------------------------------------
VISU::double_array*
VISU::Result_i
::GetTimeStampValues(const char* theMeshName,
                     VISU::Entity theEntity,
                     const char* theFieldName)
{
  VISU::double_array_var aResult = new VISU::double_array();
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult._retn();

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return aResult._retn();

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if(aMeshOnEntityMap.empty())
    return aResult._retn();

  {
    VISU::TEntity anEntity = VISU::TEntity(theEntity);
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.find(anEntity);
    if(anIter == aMeshOnEntityMap.end())
      return aResult._retn();

    const VISU::PMeshOnEntity& aMeshOnEntity = anIter->second;
    const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
    if(aFieldMap.empty())
      return aResult._retn();

    {
      VISU::TFieldMap::const_iterator anIter = aFieldMap.find(theFieldName);
      if(anIter == aFieldMap.end())
        return aResult._retn();

      {
        const VISU::PField& aField = anIter->second;
        const VISU::TValField& aValField = aField->myValField;
        if(aValField.empty())
          return aResult._retn();

        {
          aResult->length(aValField.size());
          VISU::TValField::const_iterator anIter = aValField.begin();
          for(size_t anId = 0; anIter != aValField.end(); anIter++, anId++){
            const PValForTime& aValForTime = anIter->second;
            aResult[anId] = aValForTime->myTime.first;
          }
        }
      }
    }
  }

  return aResult._retn();
}


//---------------------------------------------------------------
CORBA::Long
VISU::Result_i
::GetNumberOfComponents(const char* theMeshName,
                        VISU::Entity theEntity,
                        const char* theFieldName)
{
  CORBA::Long aResult = 0;
  const VISU::TMeshMap& aMeshMap = GetInput()->GetMeshMap();
  if(aMeshMap.empty())
    return aResult;

  VISU::TMeshMap::const_iterator anIter = aMeshMap.find(theMeshName);
  if(anIter == aMeshMap.end())
    return aResult;

  const VISU::PMesh& aMesh = anIter->second;
  const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  if(aMeshOnEntityMap.empty())
    return aResult;

  {
    VISU::TEntity anEntity = VISU::TEntity(theEntity);
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.find(anEntity);
    if(anIter == aMeshOnEntityMap.end())
      return aResult;

    const VISU::PMeshOnEntity& aMeshOnEntity = anIter->second;
    const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
    if(aFieldMap.empty())
      return aResult;

    {
      VISU::TFieldMap::const_iterator anIter = aFieldMap.find(theFieldName);
      if(anIter == aFieldMap.end())
        return aResult;

      {
        const VISU::PField& aField = anIter->second;
        aResult = aField->myNbComp;
      }
    }
  }

  return aResult;
}


//---------------------------------------------------------------
VISU::Result::EntityNames*
VISU::Result_i
::GetPartNames(const char* theMeshName)
{
  VISU::Result::EntityNames_var aResult = new VISU::Result::EntityNames();
  return aResult._retn();
}


//---------------------------------------------------------------
VISU::Result::Resolutions*
VISU::Result_i
::GetResolutions(const char* theMeshName,
                 const char* thePartName)
{
  VISU::Result::Resolutions_var aResult = new VISU::Result::Resolutions();
  return aResult._retn();
}


//---------------------------------------------------------------
VISU::Result::Resolution
VISU::Result_i
::GetResolution(const char* theMeshName,
                const char* thePartName)
{
  return VISU::Result::HIDDEN;
}


//---------------------------------------------------------------
void
VISU::Result_i
::SetResolution(const char* theMeshName,
                const char* thePartName,
                VISU::Result::Resolution theResolution)
{}


//---------------------------------------------------------------
VISU::Result_i*
VISU::Result_i
::New(SALOMEDS::Study_ptr theStudy,
      const ESourceId& theSourceId,
      const ECreationId& theCreationId,
      CORBA::Boolean theIsBuildImmediately,
      CORBA::Boolean theIsBuildFields,
      CORBA::Boolean theIsBuildMinMax,
      CORBA::Boolean theIsBuildGroups)
{
  return new RESULT_CLASS_NAME(theStudy,
                               theSourceId,
                               theCreationId,
                               theIsBuildImmediately,
                               theIsBuildFields,
                               theIsBuildMinMax,
                               theIsBuildGroups);
}


//---------------------------------------------------------------
VISU::Storable*
VISU::Result_i
::StorableEngine(SALOMEDS::SObject_ptr theSObject,
                 const Storable::TRestoringMap& theMap,
                 const std::string& thePrefix,
                 CORBA::Boolean theIsMultiFile)
{
  VISU::Result_i* aResult = new RESULT_CLASS_NAME();
  return aResult->Restore(theSObject, theMap, thePrefix, theIsMultiFile);
}


//---------------------------------------------------------------
void
VISU::Result_i
::ToStream(std::ostringstream& theStr)
{
  Storable::DataToStream(theStr,"myName", GetFileName().c_str());
  Storable::DataToStream(theStr,"myInitFileName", GetInitFileName().c_str());
  Storable::DataToStream(theStr,"myCreationId", myCreationId);
  Storable::DataToStream(theStr,"myIsBuildFields", myIsFieldsDone);
  Storable::DataToStream(theStr,"myIsBuildMinMax", myIsMinMaxDone);
  Storable::DataToStream(theStr,"myIsBuildGroups", myIsGroupsDone);
}

//---------------------------------------------------------------

bool
VISU::Result_i
::ExportMED(const char* theTargetFileName)
{
  static QString aCommand;
  const char* aTempFileName = myFileInfo.absoluteFilePath().toLatin1();
  aCommand.sprintf(" \"%s\" \"%s\"", aTempFileName, theTargetFileName);
  aCommand = QDir::convertSeparators( aCommand );
  aCommand.prepend( COPY_COMMAND );

  if(system(aCommand.toLatin1().data()) != 0) {
    if(MYDEBUG) MESSAGE("VISU_Gen_i::Copy - Cann't execute the command :"<<aCommand.toLatin1().data());
    return false;
  }
  return true;
}


//---------------------------------------------------------------
std::string
VISU::Result_i
::GetInitFileName() const
{
  return myInitFileName;
}


void
VISU::Result_i
::SetInitFileName(const std::string& theFileName)
{
  myInitFileName = theFileName;
}


//---------------------------------------------------------------
std::string
VISU::Result_i
::GetFileName() const
{
  if(GetSourceId() == eFile)
    return VISU::MakeFileName(myFileName, this);

  return myFileName;
}


QFileInfo
VISU::Result_i
::GetFileInfo() const
{
  return myFileInfo;
}

VISU::Result_i::ECreationId
VISU::Result_i
::GetCreationId() const
{
  return myCreationId;
}


VISU::Result_i::ESourceId
VISU::Result_i
::GetSourceId() const
{
  return mySourceId;
}


//---------------------------------------------------------------
std::string
VISU::Result_i
::GetEntry()
{
  CORBA::String_var anEntry = mySObject->GetID();
  return anEntry.in();
}

SALOMEDS::SObject_var
VISU::Result_i
::GetSObject() const
{
  return mySObject;
}

SALOMEDS::Study_var
VISU::Result_i
::GetStudyDocument() const
{
  return myStudyDocument;
}

_PTR(Study)
VISU::Result_i
::GetStudy() const
{
  return myStudy;
}

SALOMEDS::SComponent_var
VISU::Result_i
::GetSComponent() const
{
  return mySComponent;
}

std::string
VISU::Result_i
::GetEntry(const Storable::TRestoringMap& theRestoringMap)
{
  return Storable::FindEntry(myStudyDocument,
                             GetEntry(),
                             theRestoringMap);
}


//=======================================================================
//function : GetAxisInfo
//purpose  :
//=======================================================================
const VISU::Result_i::TAxisInfo*
VISU::Result_i
::GetAxisInfo(const string& theMeshName,
              TAxis         theAxis,
              gp_Dir&       thePlaneNormal)
{
  const TAxisInfo* components = NULL;

  if ( theAxis < AXIS_X || theAxis > AXIS_Z ) {
    MESSAGE(" Bad axis index " << theAxis );
    return components;
  }

  map< string, TGridInfo >::iterator name_info;
  name_info = myMeshName2GridInfoMap.find( theMeshName );
  TGridInfo * gInfo = 0;

  if ( name_info != myMeshName2GridInfoMap.end() )
  {
    gInfo = & name_info->second;
  }
  else if ( myInput && IsPossible() && theAxis >= AXIS_X && theAxis <= AXIS_Z )
  {
    // check presence of theMeshName
    const VISU::TMeshMap& meshMap = myInput->GetMeshMap();
    PMesh theMesh;
    VISU::TMeshMap::const_iterator aIter = meshMap.find( theMeshName );
    if ( aIter == meshMap.end() ) {
      MESSAGE("No mesh named " << theMeshName );
      return components;
    } else
      theMesh = aIter->second;

    VISU::PUnstructuredGridIDMapper anIDMapper = myInput->GetMeshOnEntity(theMeshName,
                                                                          CELL_ENTITY);
    vtkUnstructuredGrid* aMesh = anIDMapper->GetUnstructuredGridOutput();

    if ( !aMesh || aMesh->GetNumberOfCells() == 0 ) {
      MESSAGE( "No cells in the mesh: " << theMeshName );
      return components;
    }

    if ( !theMesh->IsStructured() || theMesh->myIsPolarType ) {
      // define axis directions and min cell size in each direction
      const int nbAxes = 3;
      int iAx;
      gp_Vec axDirs[ nbAxes ];
      float minSize[3] = { FLT_MAX, FLT_MAX, FLT_MAX };
      bool axesComputed = false;
      for ( vtkIdType iCell = 0; iCell < aMesh->GetNumberOfCells(); ++iCell ) {
        vtkCell* cell = aMesh->GetCell( iCell );
        
        if (cell->GetCellType() != VTK_HEXAHEDRON &&
            cell->GetCellType() != VTK_QUADRATIC_HEXAHEDRON )
          continue;
        vtkPoints * points = cell->GetPoints();
        double coords[ 4 ][3];
        points->GetPoint( 0, coords[0] );
        points->GetPoint( 1, coords[1] );
        points->GetPoint( 3, coords[2] );
        points->GetPoint( 4, coords[3] );
        gp_Pnt p0( coords[0][0], coords[0][1], coords[0][2] );
        for ( iAx = 0; iAx < nbAxes; ++iAx ) {
          double* coo = coords[ iAx + 1 ];
          gp_Pnt p( coo[0], coo[1], coo[2] );
          // min size
          double size = p0.SquareDistance( p );
          if ( size > FLT_MIN && size < minSize[ iAx ] )
            minSize[ iAx ] = size;
          // axis direction
          if ( !axesComputed ) {
            gp_Vec dir( p0, p );
            if ( dir.SquareMagnitude() <= gp::Resolution() ) {
              break;
            }
            axDirs[ iAx ] = dir;
          }
        }
        if ( iAx == nbAxes )
          axesComputed = true;
      }
      if ( !axesComputed ) {
        MESSAGE("No good hexahedrons in the mesh: " << theMeshName );
        return components;
      }

      // compute axes dirs
      gInfo = & myMeshName2GridInfoMap[ theMeshName ];
      for ( iAx = 0; iAx < nbAxes; ++iAx ) {
        int iPrev = ( iAx == 0 ) ? 2 : iAx - 1;
        int iNext = ( iAx == 2 ) ? 0 : iAx + 1;
        gInfo->myAxis[ iAx ] = axDirs[ iPrev ] ^ axDirs[ iNext ];
      }

      // get and sort intermediate component values - projections of nodes
      // on axis direction; define bnd box
      set< double > comps[ 3 ];
      Bnd_Box box;
      vtkPoints * points = aMesh->GetPoints();
      vtkIdType iP, nbP = aMesh->GetNumberOfPoints();
      for ( iP = 0; iP < nbP; ++iP ) {
        double coo[3];
        points->GetPoint( iP, coo );
        gp_Pnt p( coo[0], coo[1], coo[2] );
        box.Add( p );
        for ( iAx = 0; iAx < nbAxes; ++iAx ) {
          const gp_Dir& dir = gInfo->myAxis[ iAx ];
          double dot = dir.XYZ() * p.XYZ();
          comps[ iAx ].insert( dot );
        }
      }

      // find a range of projections of bnd box corners on each axis
      double range[3], firstValue[3];
      double x[2],y[2],z[2];
      box.Get(x[0],y[0],z[0],x[1],y[1],z[1]);
      for ( iAx = 0; iAx < nbAxes; ++iAx ) {
        set< double > bndComps;
        const gp_Dir& dir = gInfo->myAxis[ iAx ];
        for ( int iX = 0; iX < 2; ++iX ) {
          for ( int iY = 0; iY < 2; ++iY ) {
            for ( int iZ = 0; iZ < 2; ++iZ ) {
              gp_Pnt p( x[ iX ], y[ iY ], z[ iZ ] );
              double dot = dir.XYZ() * p.XYZ();
              bndComps.insert( dot );
            }
          }
        }
        firstValue[ iAx ] = *bndComps.begin();
        range[ iAx ] = *bndComps.rbegin() - *bndComps.begin();
      }

      // compute component values
      for ( iAx = 0; iAx < nbAxes; ++iAx ) {
        list< double > values;
        int nbVals = 0;
        set< double >& comp = comps[ iAx ];
        set< double >::iterator val = comp.begin();
        double bnd = -1., rng = range[ iAx ], first = firstValue[ iAx ];
        double tol = 0.1 * sqrt( minSize[ iAx ]) / rng;
        for ( ; val != comp.end(); ++val ) {
          double value = ( *val - first ) / rng;
          if ( value > bnd ) {
            values.push_back( value );
            bnd = value + tol;
            nbVals++;
          }
        }
        // store values in gInfo
        vector< double >& myComp = gInfo->myComponets[ iAx ];
        myComp.resize( nbVals );
        list< double >::iterator v = values.begin();
        for ( int i = 0; v != values.end(); ++v ){
          myComp[ i++ ] = *v;
        }
      }
    }
    else {
      vtkIdType aMeshDim = theMesh->myDim;
      gInfo = & myMeshName2GridInfoMap[ theMeshName ];
      switch ( aMeshDim ) {
      case 3: {
        gp_Dir aDir(0.0,0.0,1.0);
        gInfo->myAxis[ 2 ] = aDir;
      }
      case 2: {
        gp_Dir aDir(0.0,1.0,0.0);
        gInfo->myAxis[ 1 ] = aDir;
      }
      case 1: {
        gp_Dir aDir(1.0,0.0,0.0);
        gInfo->myAxis[ 0 ] = aDir;
      }}

      TStructuredId aStructuredId = theMesh->GetStructure();
      vector<double> PointsCoords[3];
      vtkPoints* aPoints = aMesh->GetPoints();
      switch ( aMeshDim ) {
      case 3: {
        TStructuredId aCoordIJK;
        vtkIdType nbZ = aStructuredId[2];
        for ( int i = 0; i < nbZ; i++ ) {
          aCoordIJK[2] = i + 1;
          vtkIdType aObjID = anIDMapper->GetObjectIDByIndexes(aCoordIJK);
          vtkIdType aVTKID = anIDMapper->GetNodeVTKID(aObjID);
          double* aPCoord = aPoints->GetPoint(aVTKID);
          PointsCoords[2].push_back(aPCoord[2]);
        }
      }
      case 2: {
        TStructuredId aCoordIJK;
        vtkIdType nbJ = aStructuredId[1];
        for ( int i = 0; i < nbJ; i++ ) {
          aCoordIJK[1] = i + 1;
          vtkIdType aObjID = anIDMapper->GetObjectIDByIndexes(aCoordIJK);
          vtkIdType aVTKID = anIDMapper->GetNodeVTKID(aObjID);
          double* aPCoord = aPoints->GetPoint(aVTKID);
          PointsCoords[1].push_back(aPCoord[1]);
        }
      }
      case 1: {
        TStructuredId aCoordIJK;
        vtkIdType nbI = aStructuredId[0];
        for ( int i = 0; i < nbI; i++ ) {
          aCoordIJK[0] = i + 1;
          vtkIdType aObjID = anIDMapper->GetObjectIDByIndexes(aCoordIJK);
          vtkIdType aVTKID = anIDMapper->GetNodeVTKID(aObjID);
          double* aPCoord = aPoints->GetPoint(aVTKID);
          PointsCoords[0].push_back(aPCoord[0]);
        }
      }}
      for ( int i = 0; i < aMeshDim; i++ ) {
        vector< double >& myComp = gInfo->myComponets[ i ];
        int aSize = PointsCoords[i].size();
        if ( aSize > 0 ) {
          double aLen = PointsCoords[i][aSize-1] - PointsCoords[i][0];
          myComp.resize(aSize);
          myComp[0] = 0;
          for ( int k = 1; k < aSize; k++ ) {
            myComp[k]=myComp[k-1] + (PointsCoords[i][k]-PointsCoords[i][k-1])/aLen;
          }
        }
      }
    }
  }

  // set return values
  if ( gInfo ) {
    thePlaneNormal = gInfo->myAxis[ theAxis ];
    components = & gInfo->myComponets[ theAxis ];
  }

  return components;
}

SALOME_MED::MedFileInfo* VISU::Result_i::GetMEDFileInfo()
{
  SALOME_MED::MedFileInfo_var res = new SALOME_MED::MedFileInfo();

  QString fpath = myFileInfo.absoluteFilePath();
  res->fileName = fpath.toLatin1().constData();
  res->fileSize = myFileInfo.size();
  int major, minor, release;
  if( !MED::getMEDVersion( fpath.toLatin1().constData(), major, minor, release ) )
  {
    major = -1;
    minor = -1;
    release = -1;
  }
  res->major = major;
  res->minor = minor;
  res->release = release;
  return res._retn();
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_ScalarMap_i.hh"
#include "VISU_Prs3dUtils.hh"

#include "VISU_Result_i.hh"
#include "VISU_ResultUtils.hh"

#include "VISU_ScalarMapAct.h"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_LookupTable.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_ScalarBarActor.hxx"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"

#include "SUIT_Session.h"
#include "SALOME_Event.h"
#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"

#include <vtkDataSetMapper.h>
#include <vtkTextProperty.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

static int INCMEMORY = 4;

using namespace std;

//----------------------------------------------------------------------------
size_t
VISU::ScalarMap_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity, 
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber, 
	     bool theIsMemoryCheck)
{
  size_t aResult = 0;
  try{
    if(theResult){
      bool anIsEstimated = true;
      VISU::Result_i::PInput anInput = theResult->GetInput(theMeshName,
							   theEntity,
							   theFieldName,
							   theTimeStampNumber);
      size_t aSize = anInput->GetTimeStampOnMeshSize(theMeshName,
						     (VISU::TEntity)theEntity,
						     theFieldName,
						     theTimeStampNumber,
						     anIsEstimated);
      aResult = 1;
      if(theIsMemoryCheck){
	if(anIsEstimated)
	  aSize *= INCMEMORY;
	aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
	if(MYDEBUG) 
	  MESSAGE("ScalarMap_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<aResult);
      }
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return aResult;
}

//----------------------------------------------------------------------------
namespace
{
  bool
  IsSameColor(const SALOMEDS::Color& theLeft,
	      const SALOMEDS::Color& theRight)
  {
    return 
      VISU::CheckIsSameValue(theLeft.R, theRight.R) &&
      VISU::CheckIsSameValue(theLeft.G, theRight.G) &&
      VISU::CheckIsSameValue(theLeft.B, theRight.B);
  }
}

//----------------------------------------------------------------------------
int VISU::ScalarMap_i::myNbPresent = 0;

//----------------------------------------------------------------------------
QString
VISU::ScalarMap_i
::GenerateName() 
{ 
  return VISU::GenerateName("ScalarMap",myNbPresent++);
}

//----------------------------------------------------------------------------
const string VISU::ScalarMap_i::myComment = "SCALARMAP";

//----------------------------------------------------------------------------
const char* 
VISU::ScalarMap_i
::GetComment() const 
{ 
  return myComment.c_str();
}

//----------------------------------------------------------------------------
const char*
VISU::ScalarMap_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_SCALAR_MAP";
  else
    return "ICON_TREE_SCALAR_MAP_GROUPS";
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::UpdateIcon()
{
  SALOMEDS::SObject_var aSObject = GetSObject();
  if(CORBA::is_nil(aSObject))
    return;

  SALOMEDS::Study_var aStudyDocument = GetStudyDocument();
  SALOMEDS::StudyBuilder_var aStudyBuilder = aStudyDocument->NewBuilder();

  // Check if the icon needs to be updated, update if necessary
  SALOMEDS::GenericAttribute_var anAttr = 
    aStudyBuilder->FindOrCreateAttribute(aSObject, "AttributePixMap");
  SALOMEDS::AttributePixMap_var aPixmap = SALOMEDS::AttributePixMap::_narrow( anAttr );

  CORBA::String_var aPixMapName = aPixmap->GetPixMap();
  if(strcmp(GetIconName(), aPixMapName.in()) != 0)
    aPixmap->SetPixMap(GetIconName());

  // Update Object Browser
  //rnv: to fix issue 0020167 (crash then AddMeshOnGroup called from 
  //python script). Solution: update object browser using UpdateObjBrowser event.
  /*
  SUIT_Session* aSession = SUIT_Session::session();
  QList<SUIT_Application*> anApplications = aSession->applications();
  QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
  while( anIter != anApplications.end() ){
    SUIT_Application* aSApp = *anIter;
    if(SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aSApp)){
      if(SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(anApp->activeStudy())){
	if(_PTR(Study) aCStudy = aStudy->studyDS()){
	  if(aStudyDocument->StudyId() == aCStudy->StudyId()){
	    anApp->updateObjectBrowser();
	    break;
	  }
	}
      }
    }
    anIter++;
  }
  */
  bool done = true;
  ProcessVoidEvent(new TUpdateObjBrowser(aStudyDocument->StudyId(),&done));
}

//----------------------------------------------------------------------------
VISU::ScalarMap_i::
ScalarMap_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  myShowBar(true)
{}

//----------------------------------------------------------------------------
VISU::ScalarMap_i
::~ScalarMap_i()
{}


//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const ScalarMap_i* aPrs3d = dynamic_cast<const ScalarMap_i*>(theOrigin)){
    ScalarMap_i* anOrigin = const_cast<ScalarMap_i*>(aPrs3d);
 
    SetScaling(anOrigin->GetScaling());
    SetGaussMetric(anOrigin->GetGaussMetric());
    SetBarVisible(anOrigin->IsBarVisible());
    Update();
  }
}


//----------------------------------------------------------------------------
/**
 * Creates Scalar Map and initialises it from resources
 */
VISU::Storable* 
VISU::ScalarMap_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  if( aResourceMgr->booleanValue("VISU", "scalar_bar_logarithmic", false) )
    SetScaling(VISU::LOGARITHMIC);
  else
    SetScaling(VISU::LINEAR);

  QColor aColor = aResourceMgr->colorValue( "VISU", "edge_color", QColor( 255, 255, 255 ) );
  SALOMEDS::Color aLinkColor;
  aLinkColor.R = aColor.red()/255.;
  aLinkColor.G = aColor.green()/255.;
  aLinkColor.B = aColor.blue()/255.;
  SetLinkColor(aLinkColor);

  int aGaussMetric = aResourceMgr->integerValue("VISU", "scalar_gauss_metric", 0);
  SetGaussMetric((VISU::GaussMetric)aGaussMetric);

  myShowBar = true;
  return this;
}

//----------------------------------------------------------------------------
CORBA::Float
VISU::ScalarMap_i
::GetMemorySize()
{
  return TSuperClass::GetMemorySize();
}

//----------------------------------------------------------------------------
VISU::Storable* 
VISU::ScalarMap_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetScaling(VISU::Scaling(VISU::Storable::FindValue(theMap,"myScaling").toInt()));

  QString aVal = VISU::Storable::FindValue(theMap,"myShowBar", "1");
  SetBarVisible((aVal.toInt() == 1)? true : false);

  SetGaussMetric(VISU::GaussMetric(VISU::Storable::FindValue(theMap,"myGaussMetric").toInt()));

  SALOMEDS::Color aLinkColor;
  aLinkColor.R = VISU::Storable::FindValue(theMap,"myLinkColor.R").toDouble();
  aLinkColor.G = VISU::Storable::FindValue(theMap,"myLinkColor.G").toDouble();
  aLinkColor.B = VISU::Storable::FindValue(theMap,"myLinkColor.B").toDouble();
  SetLinkColor(aLinkColor);

  // Check if the icon needs to be updated, update if necessary
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  SALOMEDS::StudyBuilder_var aStudyBuilder = aStudy->NewBuilder();
  SALOMEDS::GenericAttribute_var anAttr = 
    aStudyBuilder->FindOrCreateAttribute(theSObject, "AttributePixMap");
  SALOMEDS::AttributePixMap_var aPixmap = SALOMEDS::AttributePixMap::_narrow(anAttr);

  CORBA::String_var aPixMapName = aPixmap->GetPixMap();
  if(strcmp(GetIconName(), aPixMapName.in()) != 0)
    aPixmap->SetPixMap(GetIconName());

  return this;
}

//----------------------------------------------------------------------------
void 
VISU::ScalarMap_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myScaling",        GetScaling() );
  Storable::DataToStream( theStr, "myShowBar",        (IsBarVisible()? 1:0) );
  Storable::DataToStream( theStr, "myGaussMetric",    int(GetGaussMetric()) );

  SALOMEDS::Color aLinkColor = GetLinkColor();
  Storable::DataToStream( theStr, "myLinkColor.R",    aLinkColor.R );
  Storable::DataToStream( theStr, "myLinkColor.G",    aLinkColor.G );
  Storable::DataToStream( theStr, "myLinkColor.B",    aLinkColor.B );
}


//----------------------------------------------------------------------------
VISU::Scaling 
VISU::ScalarMap_i
::GetScaling()
{
  return VISU::Scaling(GetSpecificPL()->GetScaling());
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::SetScaling(VISU::Scaling theScaling)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ScalarMapPL, int>
		   (GetSpecificPL(), &VISU_ColoredPL::SetScaling, theScaling));
}

//----------------------------------------------------------------------------
void 
VISU::ScalarMap_i
::SetLinkColor(const SALOMEDS::Color& theColor) 
{ 
  if(IsSameColor(myLinkColor, theColor))
    return;

  VISU::TSetModified aModified(this);
  
  myLinkColor = theColor;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
SALOMEDS::Color 
VISU::ScalarMap_i
::GetLinkColor() 
{ 
  return myLinkColor;
}

//----------------------------------------------------------------------------
VISU::GaussMetric 
VISU::ScalarMap_i
::GetGaussMetric()
{
  return VISU::GaussMetric(GetSpecificPL()->GetGaussMetric());
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::SetGaussMetric(VISU::GaussMetric theGaussMetric)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ScalarMapPL, VISU::TGaussMetric>
		   (GetSpecificPL(), &VISU_ScalarMapPL::SetGaussMetric, (VISU::TGaussMetric)theGaussMetric));
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::SetSourceGeometry()
{
  int aNbGroups = myGroupNames.size();
  if(aNbGroups != 0){
    GetSpecificPL()->SetSourceGeometry();
    myGroupNames.clear();
    UpdateIcon();
  }
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::AddMeshOnGroup(const char* theGroupName)
{
  VISU::Result_i::PInput anInput = GetCResult()->GetInput();
  VISU::PUnstructuredGridIDMapper anIDMapper = anInput->GetMeshOnGroup(GetCMeshName(), theGroupName);
  if(anIDMapper){
    int aNbGroups  = myGroupNames.size();
    if(myGroupNames.find(theGroupName) == myGroupNames.end()){
      GetSpecificPL()->AddGeometry(anIDMapper->GetOutput(), theGroupName);
      myGroupNames.insert(theGroupName);
      if(aNbGroups == 0)
        UpdateIcon();
      // To update scalar range according to the new input (IPAL21305)
      if(!IsRangeFixed())
        SetSourceRange();
    }
  }
}


//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::RemoveAllGeom()
{
  int aNbGroups  = myGroupNames.size();
  GetSpecificPL()->ClearGeometry();
  myGroupNames.clear();
  if(aNbGroups != 0)
    UpdateIcon();
}


//----------------------------------------------------------------------------
void 
VISU::ScalarMap_i
::DoSetInput(bool theIsInitilizePipe, bool theReInit)
{
  VISU::Result_i::PInput anInput = GetCResult()->GetInput(GetCMeshName(),
							  GetEntity(),
							  GetCFieldName(),
							  GetTimeStampNumber());
  if(!anInput)
    throw std::runtime_error("Mesh_i::Build - GetCResult()->GetInput() == NULL !!!");

  SetField(anInput->GetField(GetCMeshName(),GetTEntity(),GetCFieldName()));
  if(!GetField())
    throw std::runtime_error("There is no Field with the parameters !!!");

  VISU::PUnstructuredGridIDMapper anIDMapper =
    anInput->GetTimeStampOnMesh(GetCMeshName(),GetTEntity(),GetCFieldName(),GetTimeStampNumber());

  if(!anIDMapper) 
    throw std::runtime_error("There is no TimeStamp with the parameters !!!");

  GetSpecificPL()->SetUnstructuredGridIDMapper(anIDMapper);
}


//----------------------------------------------------------------------------
bool
VISU::ScalarMap_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

void VISU::ScalarMap_i::SetBarVisible(CORBA::Boolean theVisible) 
{ 
  if (myShowBar == theVisible)
    return;
  VISU::TSetModified aModified(this);
  myShowBar = theVisible; 
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(MYDEBUG) MESSAGE("ScalarMap_i::CreatePipeLine() - "<<thePipeLine);
  if(!thePipeLine){
    myScalarMapPL = VISU_ScalarMapPL::New();
    myScalarMapPL->GetMapper()->SetScalarVisibility(1);
  }else
    myScalarMapPL = dynamic_cast<VISU_ScalarMapPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myScalarMapPL);
}

//----------------------------------------------------------------------------
VISU_Actor* 
VISU::ScalarMap_i
::CreateActor(bool toSupressShrinking)
{
  VISU_ScalarMapAct* anActor = VISU_ScalarMapAct::New();
  try{
    TSuperClass::CreateActor(anActor);
    anActor->SetBarVisibility(myShowBar); 
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int  aDispMode = aResourceMgr->integerValue("VISU", "scalar_map_represent", 2);
    bool toShrink  = aResourceMgr->booleanValue("VISU", "scalar_map_shrink", false);
    bool toUseShading = aResourceMgr->booleanValue("VISU", "represent_shading", false);
    anActor->SetRepresentation(aDispMode);
    if (toShrink && !toSupressShrinking) anActor->SetShrink();
    anActor->SetShading(toUseShading);

    anActor->SetFeatureEdgesAngle( aResourceMgr->doubleValue("VISU", "feature_edges_angle", 0.0) );
    anActor->SetFeatureEdgesFlags( aResourceMgr->booleanValue("VISU", "show_feature_edges", false),
				   aResourceMgr->booleanValue("VISU", "show_boundary_edges", false),
				   aResourceMgr->booleanValue("VISU", "show_manifold_edges", false),
				   aResourceMgr->booleanValue("VISU", "show_non_manifold_edges", false) );
    anActor->SetFeatureEdgesColoring( aResourceMgr->booleanValue("VISU", "feature_edges_coloring", false) );

    anActor->SetQuadratic2DRepresentation(VISU_Actor::EQuadratic2DRepresentation(aResourceMgr->integerValue( "VISU", 
													     "quadratic_mode", 
													     0)));

    UpdateActor(anActor);
  }catch(...){
    anActor->Delete();
    throw;
  }
  return anActor;
}

//----------------------------------------------------------------------------
VISU_Actor* 
VISU::ScalarMap_i
::CreateActor()
{
  return CreateActor(false);
}

//----------------------------------------------------------------------------
void
VISU::ScalarMap_i
::UpdateActor(VISU_ActorBase* theActor) 
{
  if(VISU_ScalarMapAct* anActor = dynamic_cast<VISU_ScalarMapAct*>(theActor)){
    VISU_LookupTable * aLookupTable = GetSpecificPL()->GetBarTable();

    bool anIsScalarFilterUsed = IsScalarFilterUsed();
    if ( anIsScalarFilterUsed || aLookupTable->HasMarkedValues() )
      aLookupTable->ForceBuild();

    if ( anIsScalarFilterUsed ) {
      static unsigned char MARK_COLOR[] = { 255, 255, 255 };
      aLookupTable->MarkValueByColor( GetScalarFilterMin(), MARK_COLOR );
      aLookupTable->MarkValueByColor( GetScalarFilterMax(), MARK_COLOR );
    }

    VISU_ScalarBarActor *aScalarBar = anActor->GetScalarBar();
    aScalarBar->SetLookupTable( aLookupTable );

    aScalarBar->SetDistribution( GetSpecificPL()->GetDistribution() );
    aScalarBar->SetDistributionVisibility( GetIsDistributionVisible() );

    aScalarBar->SetTitle(GetScalarBarTitle().c_str());
    aScalarBar->SetOrientation(GetBarOrientation());
    aScalarBar->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
    aScalarBar->GetPositionCoordinate()->SetValue(GetPosX(),GetPosY());
    aScalarBar->SetWidth(GetWidth());
    aScalarBar->SetHeight(GetHeight());
    aScalarBar->SetRatios(GetTitleSize(), GetLabelSize(), 
			  GetBarWidth(), GetBarHeight());
    aScalarBar->SetNumberOfLabels(GetLabels());
    aScalarBar->SetLabelFormat(GetLabelsFormat());

    double anRGB[3];

    vtkTextProperty* aTitleProp = aScalarBar->GetTitleTextProperty();
    aTitleProp->SetFontFamily(GetTitFontType());

    GetTitleColor(anRGB[0],anRGB[1],anRGB[2]);
    aTitleProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);

    IsBoldTitle()? aTitleProp->BoldOn() : aTitleProp->BoldOff();
    IsItalicTitle()? aTitleProp->ItalicOn() : aTitleProp->ItalicOff();
    IsShadowTitle()? aTitleProp->ShadowOn() : aTitleProp->ShadowOff();

    vtkTextProperty* aLabelProp = aScalarBar->GetLabelTextProperty();
    aLabelProp->SetFontFamily(GetLblFontType());

    GetLabelColor(anRGB[0],anRGB[1],anRGB[2]);
    aLabelProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);

    IsBoldLabel()? aLabelProp->BoldOn() : aLabelProp->BoldOff();
    IsItalicLabel()? aLabelProp->ItalicOn() : aLabelProp->ItalicOff();
    IsShadowLabel()? aLabelProp->ShadowOn() : aLabelProp->ShadowOff();

    anActor->SetBarVisibility(myShowBar); 

    aScalarBar->Modified();
    
    // Update values labels

    vtkTextProperty* aProp = anActor->GetsValLabelsProps();
    if ( aProp )
    {
      aProp->SetFontFamily( GetValLblFontType() );
      aProp->SetFontSize( GetValLblFontSize() );
      aProp->SetBold( IsBoldValLbl() );
      aProp->SetItalic( IsItalicValLbl() );
      aProp->SetShadow( IsShadowValLbl() );

      double anRGB[ 3 ];
      GetValLblFontColor( anRGB[ 0 ], anRGB[ 1 ], anRGB[ 2 ] );
      aProp->SetColor( anRGB[ 0 ], anRGB[ 1 ], anRGB[ 2 ] );
    }

    // Update edge property
    SALOMEDS::Color aLinkColor = GetLinkColor();
    anActor->GetEdgeProperty()->SetColor(aLinkColor.R, aLinkColor.G, aLinkColor.B);
  }
  TSuperClass::UpdateActor(theActor);
}

//----------------------------------------------------------------------------
struct TGetComponentMin: public SALOME_Event
{
  VISU::ColoredPrs3d_i* myColoredPrs3d;
  vtkIdType myCompID;
  
  typedef CORBA::Double TResult;
  TResult myResult;
  
  TGetComponentMin( VISU::ColoredPrs3d_i* theColoredPrs3d,
		    vtkIdType theCompID ):
    myColoredPrs3d( theColoredPrs3d ),
    myCompID( theCompID )
  {}
    
  virtual
  void
  Execute()
  {
    VISU::TNames aGroupNames;
    VISU::GaussMetric aGaussMetric = VISU::AVERAGE;
    if(VISU::ScalarMap_i* aPrs3d = dynamic_cast<VISU::ScalarMap_i*>(myColoredPrs3d)) {
      aGroupNames = aPrs3d->GetSpecificPL()->GetGeometryNames();
      aGaussMetric = aPrs3d->GetGaussMetric();
    }

    VISU::PMinMaxController aMinMaxController = myColoredPrs3d->GetMinMaxController();
    if ( aMinMaxController ) {
      myResult = aMinMaxController->GetComponentMin( myCompID );
    } else {
      VISU::TMinMax aTMinMax;
      if(myColoredPrs3d->GetScalarField()->myIsELNO)
	aTMinMax = myColoredPrs3d->GetScalarField()->GetMinMax( myCompID, aGroupNames, (VISU::TGaussMetric)aGaussMetric );
      else
	aTMinMax = myColoredPrs3d->GetScalarField()->GetAverageMinMax( myCompID, aGroupNames, (VISU::TGaussMetric)aGaussMetric );
      myResult = aTMinMax.first;
    }
  }
};


//----------------------------------------------------------------------------
double 
VISU::ScalarMap_i
::GetComponentMin(vtkIdType theCompID)
{
  return ProcessEvent( new TGetComponentMin( this, theCompID ) );
}

//----------------------------------------------------------------------------
struct TGetComponentMax: public SALOME_Event
{
  VISU::ColoredPrs3d_i* myColoredPrs3d;
  vtkIdType myCompID;

  typedef CORBA::Double TResult;
  TResult myResult;
  
  TGetComponentMax( VISU::ColoredPrs3d_i* theColoredPrs3d,
		    vtkIdType theCompID ):
    myColoredPrs3d( theColoredPrs3d ),
    myCompID( theCompID )
  {}
    
  virtual
  void
  Execute()
  {
    VISU::TNames aGroupNames;
    VISU::GaussMetric aGaussMetric = VISU::AVERAGE;
    if(VISU::ScalarMap_i* aPrs3d = dynamic_cast<VISU::ScalarMap_i*>(myColoredPrs3d)) {
      aGroupNames = aPrs3d->GetSpecificPL()->GetGeometryNames();
      aGaussMetric = aPrs3d->GetGaussMetric();
    }

    VISU::PMinMaxController aMinMaxController = myColoredPrs3d->GetMinMaxController();
    if ( aMinMaxController ) {
      myResult = aMinMaxController->GetComponentMax( myCompID );
    } else {
      VISU::TMinMax aTMinMax;
      if(myColoredPrs3d->GetScalarField()->myIsELNO)
	aTMinMax = myColoredPrs3d->GetScalarField()->GetMinMax( myCompID, aGroupNames, (VISU::TGaussMetric)aGaussMetric );
      else
	aTMinMax = myColoredPrs3d->GetScalarField()->GetAverageMinMax( myCompID, aGroupNames, (VISU::TGaussMetric)aGaussMetric );
      myResult = aTMinMax.second;
    }
  }
};


//----------------------------------------------------------------------------
double 
VISU::ScalarMap_i
::GetComponentMax(vtkIdType theCompID)
{
  return ProcessEvent( new TGetComponentMax( this, theCompID ) );
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ColoredPrs3dHolder_i.hxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_ColoredPrs3dHolder_i_HeaderFile
#define VISU_ColoredPrs3dHolder_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_PrsObject_i.hh"

#include "SALOME_GenericObj_i.hh"

namespace VISU
{
  class ColoredPrs3d_i;
  class ColoredPrs3dCache_i;

  /*!
   * Interface of 3d presentation's holder, which represents colored 3d presentations,
   * created on fields. It is publishing in the object browser in a separate folder
   * and can be controled by viewer's slider.
   */
  class VISU_I_EXPORT ColoredPrs3dHolder_i : public virtual POA_VISU::ColoredPrs3dHolder,
			       public virtual SALOME::GenericObj_i,
			       public virtual PrsObject_i
  {
    ColoredPrs3dHolder_i();
    ColoredPrs3dHolder_i(const ColoredPrs3dHolder_i&);

    friend class ColoredPrs3dCache_i;
  public:
    //----------------------------------------------------------------------------
    //! A constructor to create an instance of the class
    explicit
    ColoredPrs3dHolder_i(VISU::ColoredPrs3dCache_i& theCache);

    virtual
    ~ColoredPrs3dHolder_i();

    //----------------------------------------------------------------------------
    //! Apply input parameters to last visited presentation in the cache.
    virtual
    CORBA::Boolean 
    Apply(VISU::ColoredPrs3d_ptr thePrs3d,
	  const VISU::ColoredPrs3dHolder::BasicInput& theInput,
	  VISU::View3D_ptr theView3D);

    //----------------------------------------------------------------------------
    //! Gets the last visited presentation in the cache.
    VISU::ColoredPrs3d_i*
    GetPrs3dDevice();

    //! Gets the last visited presentation in the cache.
    virtual
    VISU::ColoredPrs3d_ptr
    GetDevice();

    //----------------------------------------------------------------------------
    //! Gets TimeStampsRange information from the last visited presentation.
    virtual
    VISU::ColoredPrs3dHolder::TimeStampsRange*
    GetTimeStampsRange();

    //----------------------------------------------------------------------------
    //! Gets input parameters of the last visited presentation.
    VISU::ColoredPrs3dHolder::BasicInput*
    GetBasicInput();

    //----------------------------------------------------------------------------
    //! Gets a ColoredPrs3dCache, to which the holder belongs
    VISU::ColoredPrs3dCache_ptr
    GetCache();

    //----------------------------------------------------------------------------
    //! Gets memory size actually used by the holder (Mb).
    virtual
    CORBA::Float
    GetMemorySize();

    //----------------------------------------------------------------------------
    virtual 
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TCOLOREDPRS3DHOLDER; 
    }

    virtual 
    VISU::VISUType
    GetPrsType();

    //----------------------------------------------------------------------------
    virtual
    void
    RemoveFromStudy();

    virtual
    const char*
    GetComment() const;

    virtual
    void
    ToStream(std::ostringstream&);

    static
    Storable*
    StorableEngine(SALOMEDS::SObject_ptr theSObject,
		   const Storable::TRestoringMap& theMap,
		   const std::string& thePrefix,
		   CORBA::Boolean theIsMultiFile);

    virtual
    QString 
    GenerateName();

  public:
    static const std::string myComment;

    //----------------------------------------------------------------------------
  private:
    void
    PublishInStudy(const std::string& theName, 
		   const std::string& theIconName, 
		   const std::string& theComment);

    VISU::ColoredPrs3dCache_i& myCache;
    static int myNbHolders;
  };
}

#endif

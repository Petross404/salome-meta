// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Prs3dUtils.hh
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef __VISU_PRS3D_UTILS_H__
#define __VISU_PRS3D_UTILS_H__

#include "VISU_PrsObject_i.hh"

#include <vtkTimeStamp.h>

namespace VISU
{
  //----------------------------------------------------------------------------
  struct TSetModified: vtkTimeStamp
  {
    VISU::PrsObject_i* myPrsObject;
    
    TSetModified(VISU::PrsObject_i* thePrsObject);
    
    ~TSetModified();
  };
  
  VISU_I_EXPORT std::string  ToFormat( const int thePrec );
  VISU_I_EXPORT int ToPrecision( const char* theFormat );
  

  //----------------------------------------------------------------------------
}


#endif // __VISU_PRS3D_UTILS_H__

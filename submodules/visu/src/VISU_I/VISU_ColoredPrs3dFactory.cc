// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ColoredPrs3dCache_i.cc
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_ColoredPrs3dCache_i.hh"

#if (OCC_VERSION_MAJOR << 16 | OCC_VERSION_MINOR << 8 | OCC_VERSION_MAINTENANCE) > 0x060100
#define NO_CAS_CATCH
#endif

#include <Standard_Failure.hxx>

#ifdef NO_CAS_CATCH
#include <Standard_ErrorHandler.hxx>
#endif

#ifdef _DEBUG_
//static int MYDEBUG = 0;
//#define _DEXCEPT_
#else
//static int MYDEBUG = 0;
#endif

using namespace std;

namespace VISU
{
  //----------------------------------------------------------------------------
  bool
  CreatColoredPrs3d(ColoredPrs3d_i* theColoredPrs3d,
		    Result_i* theResult,
		    const std::string& theMeshName, 
		    VISU::Entity theEntity,
		    const std::string& theFieldName, 
		    CORBA::Long theIteration)
  {
#ifndef _DEXCEPT_
    try{
#ifdef NO_CAS_CATCH
      OCC_CATCH_SIGNALS;
#endif
#endif
      theColoredPrs3d->SetCResult(theResult);
      theColoredPrs3d->SetMeshName(theMeshName.c_str());
      theColoredPrs3d->SetEntity(theEntity);
      theColoredPrs3d->SetFieldName(theFieldName.c_str());
      theColoredPrs3d->SetTimeStampNumber(theIteration);
      if(theColoredPrs3d->Apply(false))
	return true;
#ifndef _DEXCEPT_
    }catch(Standard_Failure) {
      Handle(Standard_Failure) aFail = Standard_Failure::Caught();
      INFOS("Follow signal was occured :\n"<<aFail->GetMessageString());
    }catch(std::exception& exc){
      INFOS("Follow exception was occured :\n"<<exc.what());
    }catch(...){
      INFOS("Unknown exception was occured!");
    }
#endif
    return false;
  }


  //----------------------------------------------------------------------------
  VISU::ColoredPrs3d_i*
  CreatePrs3d_i(VISUType theType,
		SALOMEDS::Study_ptr theStudy,
		ColoredPrs3d_i::EPublishInStudyMode thePublishInStudyMode)
  {
    switch(theType){
    case TSCALARMAP:
      return VISU::CreatePrs3dByEnum<TSCALARMAP>(theStudy, thePublishInStudyMode);
    case TGAUSSPOINTS:
      return VISU::CreatePrs3dByEnum<TGAUSSPOINTS>(theStudy, thePublishInStudyMode);
    case TDEFORMEDSHAPE:
      return VISU::CreatePrs3dByEnum<TDEFORMEDSHAPE>(theStudy, thePublishInStudyMode);
    case TSCALARMAPONDEFORMEDSHAPE:
    case TDEFORMEDSHAPEANDSCALARMAP:
      return VISU::CreatePrs3dByEnum<TDEFORMEDSHAPEANDSCALARMAP>(theStudy, thePublishInStudyMode);
    case TISOSURFACES:
      return VISU::CreatePrs3dByEnum<TISOSURFACES>(theStudy, thePublishInStudyMode);
    case TSTREAMLINES:
      return VISU::CreatePrs3dByEnum<TSTREAMLINES>(theStudy, thePublishInStudyMode);
    case TPLOT3D:
      return VISU::CreatePrs3dByEnum<TPLOT3D>(theStudy, thePublishInStudyMode);
    case TCUTPLANES:
      return VISU::CreatePrs3dByEnum<TCUTPLANES>(theStudy, thePublishInStudyMode);
    case TCUTLINES:
      return VISU::CreatePrs3dByEnum<TCUTLINES>(theStudy, thePublishInStudyMode);
    case TCUTSEGMENT:
      return VISU::CreatePrs3dByEnum<TCUTSEGMENT>(theStudy, thePublishInStudyMode);
    case TVECTORS:
      return VISU::CreatePrs3dByEnum<TVECTORS>(theStudy, thePublishInStudyMode);
    }
    return NULL;
  }


  //----------------------------------------------------------------------------
  VISU::ColoredPrs3dCache::EnlargeType
  GetRequiredCacheMemory(VISU::VISUType theType,
			 VISU::Result_ptr theResult, 
			 const std::string& theMeshName, 
			 VISU::Entity theEntity,
			 const std::string& theFieldName, 
			 CORBA::Long theIteration,
			 CORBA::Float& theUsedMemory,
			 CORBA::Float& theRequiredMemory)
  {
    VISU::ColoredPrs3dCache::EnlargeType anEnlargeType = VISU::ColoredPrs3dCache::NO_ENLARGE;
    if(Result_i* aResult = dynamic_cast<Result_i*>(GetServant(theResult).in())){
      VISU::ColoredPrs3dHolder::BasicInput anInput;
      anInput.myResult = VISU::Result::_duplicate(theResult);
      anInput.myMeshName = theMeshName.c_str();
      anInput.myEntity = theEntity;
      anInput.myFieldName = theFieldName.c_str();
      anInput.myTimeStampNumber = theIteration;
      
      SALOMEDS::Study_var aStudy = aResult->GetStudyDocument();
      VISU::ColoredPrs3dCache_var aCache = ColoredPrs3dCache_i::GetInstance(aStudy);

      theUsedMemory = aCache->GetMemorySize();
      anEnlargeType = aCache->GetRequiredMemory(theType, anInput, theRequiredMemory);
    }
    return anEnlargeType;
  }


  //----------------------------------------------------------------------------
  VISU::ColoredPrs3d_i*
  CreateHolder2GetDeviceByEnum(VISU::VISUType theType,
			       VISU::Result_ptr theResult, 
			       const std::string& theMeshName, 
			       VISU::Entity theEntity,
			       const std::string& theFieldName, 
			       CORBA::Long theIteration,
			       VISU::ColoredPrs3dCache::EnlargeType theEnlargeType,
			       CORBA::Float theRequiredMemory)
  {
    VISU::ColoredPrs3d_i* aColoredPrs3d = NULL;
    if(Result_i* aResult = dynamic_cast<Result_i*>(GetServant(theResult).in())){
      VISU::ColoredPrs3dHolder::BasicInput anInput;
      anInput.myResult = VISU::Result::_duplicate(theResult);
      anInput.myMeshName = theMeshName.c_str();
      anInput.myEntity = theEntity;
      anInput.myFieldName = theFieldName.c_str();
      anInput.myTimeStampNumber = theIteration;
      
      SALOMEDS::Study_var aStudy = aResult->GetStudyDocument();
      VISU::ColoredPrs3dCache_var aCache = ColoredPrs3dCache_i::GetInstance(aStudy);

      if( theEnlargeType == VISU::ColoredPrs3dCache::ENLARGE )
	aCache->SetLimitedMemory( theRequiredMemory );

      VISU::ColoredPrs3dHolder_var aHolder = aCache->CreateHolder(theType, anInput);
      
      if( CORBA::is_nil(aHolder) )
	return NULL;
      
      VISU::ColoredPrs3d_var aPrs3d = aHolder->GetDevice();
      aColoredPrs3d = dynamic_cast<VISU::ColoredPrs3d_i*>(GetServant(aPrs3d).in());
    }
    return aColoredPrs3d;
  }
  
  
  //----------------------------------------------------------------------------
  size_t
  CheckIsPossible(VISU::VISUType theType,
		  const VISU::ColoredPrs3dHolder::BasicInput& theInput,
		  bool theMemoryCheck)
  {
    size_t aMemory = 0;
    switch(theType){
    case TSCALARMAP:
      aMemory = CheckIsPossible<TSCALARMAP>(theInput, theMemoryCheck);
      break;
    case TGAUSSPOINTS:
      aMemory = CheckIsPossible<TGAUSSPOINTS>(theInput, theMemoryCheck);
      break;
    case TDEFORMEDSHAPE:
      aMemory = CheckIsPossible<TDEFORMEDSHAPE>(theInput, theMemoryCheck);
      break;
    case TSCALARMAPONDEFORMEDSHAPE:
    case TDEFORMEDSHAPEANDSCALARMAP:
      aMemory = CheckIsPossible<TDEFORMEDSHAPEANDSCALARMAP>(theInput, theMemoryCheck);
      break;
    case TISOSURFACES:
      aMemory = CheckIsPossible<TISOSURFACES>(theInput, theMemoryCheck);
      break;
    case TSTREAMLINES:
      aMemory = CheckIsPossible<TSTREAMLINES>(theInput, theMemoryCheck);
      break;
    case TPLOT3D:
      aMemory = CheckIsPossible<TPLOT3D>(theInput, theMemoryCheck);
      break;
    case TCUTPLANES:
      aMemory = CheckIsPossible<TCUTPLANES>(theInput, theMemoryCheck);
      break;
    case TCUTLINES:
      aMemory = CheckIsPossible<TCUTLINES>(theInput, theMemoryCheck);
      break;
    case TCUTSEGMENT:
      aMemory = CheckIsPossible<TCUTSEGMENT>(theInput, theMemoryCheck);
      break;
    case TVECTORS:
      aMemory = CheckIsPossible<TVECTORS>(theInput, theMemoryCheck);
      break;
    }
    return aMemory;
  }


  //----------------------------------------------------------------------------
}

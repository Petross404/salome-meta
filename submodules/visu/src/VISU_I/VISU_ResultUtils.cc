// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ResultUtils.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_ResultUtils.hh"

#include "SUIT_Session.h"
#include "SALOME_Event.h"
#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"

#include "VISU_Convertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <QFileInfo>
#include <QFile>
#include <QDir>

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

#ifdef _DEBUG_
static int MYTIMEDEBUG = 0;
#else
static int MYTIMEDEBUG = 0;
#endif

using namespace std;

namespace VISU
{
  //----------------------------------------------------------------------------
  typedef boost::recursive_mutex TMutex;
  typedef TMutex::scoped_lock TLock;

  static TMutex myMutex;

  //----------------------------------------------------------------------------
  TResultManager
  ::TResultManager(Result_i* theResult):
    myResult(theResult)
  {
    myResult->Register();
  }


  //----------------------------------------------------------------------------
  TResultManager
  ::~TResultManager()
  {
    myResult->UnRegister();
  }


  //----------------------------------------------------------------------------
  TTransactionManager
  ::TTransactionManager(_PTR(Study) theStudyDocument):
    myStudyBuilder(theStudyDocument->NewBuilder())
  {
    TLock aLock(myMutex);
    myStudyBuilder->NewCommand();
  }


  //----------------------------------------------------------------------------
  TTransactionManager
  ::~TTransactionManager()
  {
    TLock aLock(myMutex);
    myStudyBuilder->CommitCommand();
  }


  //----------------------------------------------------------------------------
  TUpdateObjBrowser
  ::TUpdateObjBrowser(const int theStudyId, CORBA::Boolean* theIsDone):
    myStudyId(theStudyId),
    myIsDone(theIsDone)
  {}
    
  
  //----------------------------------------------------------------------------
  void
  TUpdateObjBrowser
  ::Execute()
  {
    TLock aLock(myMutex);
    SUIT_Session* aSession = SUIT_Session::session();
    QList<SUIT_Application*> anApplications = aSession->applications();
    QListIterator<SUIT_Application*> anIter (anApplications);
    while (anIter.hasNext()) {
      SUIT_Application* aSApp = anIter.next();
      if(SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aSApp)){
	if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(anApp->activeStudy())) {
	  if (_PTR(Study) aCStudy = aStudy->studyDS()) {
	    if (myStudyId == aCStudy->StudyId()) {
	      TTimerLog aTimerLog(MYTIMEDEBUG,"Result_i::updateObjectBrowser");
	      anApp->updateObjectBrowser();
	      *myIsDone = true;
	      break;
	    }
	  }
	}
      }
    }
  }


  //----------------------------------------------------------------------------
  QString
  GenerateName(const std::string& theName)
  {
    TLock aLock(myMutex);

    typedef std::map<std::string, int> TNameMap;
    static TNameMap aMap;

    TNameMap::const_iterator anIter = aMap.find(theName);
    QString aResult;
    if (anIter == aMap.end()) {
      aMap[theName] = 0;
      aResult = theName.c_str();
    } else {
      aResult = GenerateName(theName,++aMap[theName]);
    }
    return aResult;
  }
  

  //----------------------------------------------------------------------------
  bool
  SplitName(const std::string& theName,
	    std::string& theBase,
	    std::string& theSuffix,
	    char theDelimeter)
  {
    size_t aPosition = theName.rfind(theDelimeter);
    if(aPosition == std::string::npos){
      theBase = theName;
      theSuffix = "";
      return false;
    }

    theBase = theName.substr(0, aPosition);
    theSuffix = theName.substr(aPosition);
    return true;
  }
  

  //----------------------------------------------------------------------------
  std::string
  MakeFileName(const std::string& theName,
	       const void* thePointer)
  {
    std::ostringstream aStream;    
    aStream<<theName<<"_"<<thePointer;
    return aStream.str();
  }
  

  //----------------------------------------------------------------------------
  QString 
  GenerateFieldName(const std::string& theName, 
		    const std::string& theUnits)
  {
    QString aName;
    const string tmp (theUnits.size(),' ');
    if (theUnits == "" || theUnits == tmp)
      aName = QString("%1, -").arg(theName.c_str());
    else
      aName = QString("%1, %2").arg(theName.c_str()).arg(theUnits.c_str());
    aName = aName.simplified();
    return aName;
  }


  //----------------------------------------------------------------------------
  void
  CreateReference(_PTR(Study) theStudyDocument,
		  const std::string& theFatherEntry, 
		  const std::string& theRefEntry)
  {
    _PTR(StudyBuilder) aStudyBuilder = theStudyDocument->NewBuilder();
    _PTR(SObject) aFather = theStudyDocument->FindObjectID(theFatherEntry);
    _PTR(SObject) aNewObj = aStudyBuilder->NewObject(aFather);
    _PTR(SObject) aRefSObj = theStudyDocument->FindObjectID(theRefEntry);
    aStudyBuilder->Addreference(aNewObj,aRefSObj);
  }


  //----------------------------------------------------------------------------
  void 
  RemoveSObject(_PTR(Study) theStudyDocument,
		const string& theEntry)
  {
    _PTR(StudyBuilder) aStudyBuilder = theStudyDocument->NewBuilder();
    _PTR(SObject) aSObject = theStudyDocument->FindObjectID(theEntry);
    aStudyBuilder->RemoveObject(aSObject);
  }


  //----------------------------------------------------------------------------
  void
  BuildEntities(Result_i* theResult,
		Result_i::PInput theInput,
		CORBA::Boolean* theIsDone,
		const std::string& theResultEntry,
		CORBA::Boolean theIsAtOnce,
		CORBA::Boolean theIsBuildGroups,
		CORBA::Boolean theIsBuildFields,
		CORBA::Boolean theIsBuildParts,
		_PTR(Study) theStudy)
  {
    if(*theIsDone)
      return;

    TTimerLog aTimerLog(MYTIMEDEBUG,"Result_i::BuildEntities");
    TResultManager aResultManager(theResult);
    TTransactionManager aTransactionManager(theStudy);

    {
      TTimerLog aTimerLog(MYTIMEDEBUG,"theInput->BuildEntities");
      theInput->BuildEntities();
    }

    QString aComment,aTmp;
    const TMeshMap& aMeshMap = theInput->GetMeshMap();
    TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
    for(; aMeshMapIter != aMeshMap.end(); aMeshMapIter++){
      const string& aMeshName = aMeshMapIter->first;
      const PMesh& aMesh = aMeshMapIter->second;
      const TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      if(aMeshOnEntityMap.empty()) 
	continue;
      aComment = QString("myComment=MESH;myName=%1;myDim=%2");
      aComment = aComment.arg(aMeshName.c_str());
      aComment = aComment.arg(aMesh->myDim);
      aMesh->myEntry = 
	CreateAttributes(theStudy,
			 theResultEntry,
			 NO_ICON,
			 NO_IOR,
			 aMeshName,
			 NO_PERFSITENT_REF,
			 aComment.toLatin1().data(),
			 true);
    
      aComment = QString("myComment=FAMILIES;myMeshName=%1").arg(aMeshName.c_str());
      std::string aSubMeshesEntry = 
	CreateAttributes(theStudy,
			 aMesh->myEntry,
			 NO_ICON,
			 NO_IOR,
			 "Families",
			 NO_PERFSITENT_REF,
			 aComment.toLatin1().data(),
			 true);

      if(theIsBuildGroups){
	aMesh->myGroupsEntry =
	  CreateAttributes(theStudy,
			   aMesh->myEntry,
			   NO_ICON,
			   NO_IOR,
			   NO_NAME,
			   NO_PERFSITENT_REF,
			   NO_COMMENT,
			   true);
      }
	
      if(theIsBuildFields){
	aMesh->myFieldsEntry =
	  CreateAttributes(theStudy,
			   aMesh->myEntry,
			   NO_ICON,
			   NO_IOR,
			   NO_NAME,
			   NO_PERFSITENT_REF,
			   NO_COMMENT,
			   true);
      }

      if(theIsBuildParts){
	aMesh->myPartsEntry =
	  CreateAttributes(theStudy,
			   aMesh->myEntry,
			   NO_ICON,
			   NO_IOR,
			   NO_NAME,
			   NO_PERFSITENT_REF,
			   NO_COMMENT,
			   true);
      }

      //Import entities
      TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
	const TEntity& anEntity = aMeshOnEntityMapIter->first;
	const PMeshOnEntity& aMeshOnEntity = aMeshOnEntityMapIter->second;
	
	string anEntityName;
	switch(anEntity){
	case NODE_ENTITY: 
	  anEntityName = "onNodes"; 
	  break;
	case EDGE_ENTITY: 
	  anEntityName = "onEdges"; 
	  break;
	case FACE_ENTITY: 
	  anEntityName = "onFaces"; 
	  break;
	case CELL_ENTITY: 
	  anEntityName = "onCells"; 
	  break;
	default:
	continue;
	}
	
	aComment = QString("myComment=ENTITY;myMeshName=%1;myId=%2");
        aComment = aComment.arg(aMeshName.c_str());
	aComment = aComment.arg(anEntity);
	
	aMeshOnEntity->myEntry = 
	  CreateAttributes(theStudy, 
			   aSubMeshesEntry, 
			   NO_ICON,
			   NO_IOR,
			   anEntityName.c_str(),
			   NO_PERFSITENT_REF,
			   aComment.toLatin1().data(), 
			   true);
      }
    }
    
    ProcessVoidEvent(new TUpdateObjBrowser(theStudy->StudyId(),theIsDone));
  }


  //----------------------------------------------------------------------------
  void
  BuildGroups(Result_i* theResult,
              Result_i::PInput theInput,
              CORBA::Boolean* theIsDone,
              CORBA::Boolean theIsBuild,
              CORBA::Boolean theIsAtOnce,
              _PTR(Study) theStudy)
  {
    if(!theIsBuild || *theIsDone)
      return;

    TTimerLog aTimerLog(MYTIMEDEBUG,"Result_i::BuildGroups");
    TResultManager aResultManager(theResult);
    TTransactionManager aTransactionManager(theStudy);
    
    {
      TTimerLog aTimerLog(MYTIMEDEBUG,"theInput->BuildGroups");
      theInput->BuildGroups();
    }

    QString aComment,aTmp;
    const TMeshMap& aMeshMap = theInput->GetMeshMap();
    TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
    for(; aMeshMapIter != aMeshMap.end(); aMeshMapIter++){
      const string& aMeshName = aMeshMapIter->first;
      const PMesh& aMesh = aMeshMapIter->second;
      
      const TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      if(aMeshOnEntityMap.empty()) 
        continue;
      
      TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
        const TEntity& anEntity = aMeshOnEntityMapIter->first;
        const PMeshOnEntity& aMeshOnEntity = aMeshOnEntityMapIter->second;
        const TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
        TFamilyMap::const_iterator aFamilyMapIter = aFamilyMap.begin();
        for(; aFamilyMapIter != aFamilyMap.end(); aFamilyMapIter++){
          const string& aFamilyName = aFamilyMapIter->first;
          const PFamily& aFamily = aFamilyMapIter->second;
          aComment=QString("myComment=FAMILY;myMeshName=%1;myEntityId=%2;myName=%3");
          aComment=aComment.arg(aMeshName.c_str());
          aComment=aComment.arg(anEntity);
          aComment=aComment.arg(aFamilyName.c_str());
          aFamily->myEntry = CreateAttributes(theStudy,
                                              aMeshOnEntity->myEntry,
                                              NO_ICON,
                                              NO_IOR,
                                              aFamilyName,
                                              NO_PERFSITENT_REF,
                                              aComment.toLatin1().data(),
                                              true);
        }
      }
      //Importing groups
      const TGroupMap& aGroupMap = aMesh->myGroupMap;
      if(!aGroupMap.empty()){
        aComment = QString("myComment=GROUPS;myMeshName=%1").arg(aMeshName.c_str());
        
        CreateAttributes(theStudy,
                         aMesh->myGroupsEntry,
                         NO_ICON,
                         NO_IOR,
                         "Groups",
                         NO_PERFSITENT_REF,
                         aComment.toLatin1().data(),
                         false);
                         
        TGroupMap::const_iterator aGroupMapIter = aGroupMap.begin();
        for(; aGroupMapIter != aGroupMap.end(); aGroupMapIter++){
          const string& aGroupName = aGroupMapIter->first;
          const PGroup& aGroup = aGroupMapIter->second;
          aComment = QString("myComment=GROUP;myMeshName=%1;myName=%2").arg(aMeshName.c_str()).arg(aGroupName.c_str());
          aGroup->myEntry = CreateAttributes(theStudy,
                                             aMesh->myGroupsEntry,
                                             NO_ICON,
                                             NO_IOR,
                                             aGroupName,
                                             NO_PERFSITENT_REF,
                                             aComment.toLatin1().data(),
                                             true);
          const TFamilySet& aFamilySet = aGroup->myFamilySet;
          TFamilySet::const_iterator aFamilyIter = aFamilySet.begin();
          for(; aFamilyIter != aFamilySet.end(); aFamilyIter++){
            const PFamily& aFamily = (*aFamilyIter).second;
            CreateReference(theStudy,
                            aGroup->myEntry,
                            aFamily->myEntry);
          }
        }
      }else if(!theIsAtOnce)
        RemoveSObject(theStudy,
                      aMesh->myGroupsEntry);
    }
    
    ProcessVoidEvent(new TUpdateObjBrowser(theStudy->StudyId(),theIsDone));
  }


  //----------------------------------------------------------------------------
  void
  BuildFields(Result_i* theResult,
	      Result_i::PInput theInput,
	      CORBA::Boolean* theIsDone,
	      CORBA::Boolean theIsBuild,
	      CORBA::Boolean theIsAtOnce,
	      _PTR(Study) theStudy)
  {
    if(!theIsBuild || *theIsDone)
      return;

    TTimerLog aTimerLog(MYTIMEDEBUG,"Result_i::BuildFields");
    TResultManager aResultManager(theResult);
    TTransactionManager aTransactionManager(theStudy);

    {
      TTimerLog aTimerLog(MYTIMEDEBUG,"theInput->BuildFields");
      theInput->BuildFields();
    }

    QString aComment,aTmp;
    const TMeshMap& aMeshMap = theInput->GetMeshMap();
    TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();

    for(; aMeshMapIter != aMeshMap.end(); aMeshMapIter++)
    {
      const string& aMeshName = aMeshMapIter->first;
      const PMesh& aMesh = aMeshMapIter->second;

      const TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      if(aMeshOnEntityMap.empty()) 
        continue;

      //Import fields
      bool anIsFieldsEntryUpdated = false;
      TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter = aMeshOnEntityMap.begin();

      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++)
      {
        const TEntity& anEntity = aMeshOnEntityMapIter->first;
        const PMeshOnEntity& aMeshOnEntity = aMeshOnEntityMapIter->second;
        const TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
        TFieldMap::const_iterator aFieldMapIter = aFieldMap.begin();
        
        for(; aFieldMapIter != aFieldMap.end(); aFieldMapIter++)
        {
          if(!anIsFieldsEntryUpdated)
          {
            aComment = "";
            aComment.append("myComment=FIELDS;");
            aComment.append("myMeshName=");aComment.append(aMeshName.c_str());
            
            CreateAttributes(theStudy,
                             aMesh->myFieldsEntry,
                             NO_ICON,
                             NO_IOR,
                             "Fields",
                             NO_PERFSITENT_REF,
                             aComment.toLatin1().data(),
                             false);
            anIsFieldsEntryUpdated = true;
          }
        
          const string& aFieldName = aFieldMapIter->first;
          const PField& aField = aFieldMapIter->second;
          const TValField& aValField = aField->myValField;
          QString aFieldNameWithUnit = GenerateFieldName(aFieldName,aField->myUnitNames[0]);
          aComment = QString("myComment=FIELD;myMeshName=%1;myEntityId=%2;myName=%3;myNbTimeStamps=%4;myNumComponent=%5");
          aComment = aComment.arg(aMeshName.c_str());
          aComment = aComment.arg(anEntity);
          aComment = aComment.arg(aFieldName.c_str());
          aComment = aComment.arg(aValField.size());
          aComment = aComment.arg(aField->myNbComp);
          
          aField->myEntry = CreateAttributes(theStudy,
                                            aMesh->myFieldsEntry,
                                            NO_ICON,
                                            NO_IOR,
                                            aFieldNameWithUnit.toLatin1().data(),
                                            NO_PERFSITENT_REF,
                                            aComment.toLatin1().data(),
                                            true);

          CreateReference(theStudy,
                          aField->myEntry,
                          aMeshOnEntity->myEntry);

          TValField::const_iterator aValFieldIter = aValField.begin();
          
          for(; aValFieldIter != aValField.end(); aValFieldIter++)
          {
            int aTimeStamp = aValFieldIter->first;
            const PValForTime& aValForTime = aValFieldIter->second;
            aComment = QString("myComment=TIMESTAMP;myMeshName=%1;myEntityId=%2;myFieldName=%3;myTimeStampId=%4;myNumComponent=%5");
            aComment = aComment.arg(aMeshName.c_str());
            aComment = aComment.arg(anEntity);
            aComment = aComment.arg(aFieldName.c_str());
            aComment = aComment.arg(aTimeStamp);
            aComment = aComment.arg(aField->myNbComp);
            
            string aTimeStampId = VISU_Convertor::GenerateName(aValForTime->myTime);
            
            aValForTime->myEntry = CreateAttributes(theStudy,
                                                    aField->myEntry,
                                                    NO_ICON,
                                                    NO_IOR,
                                                    aTimeStampId,
                                                    NO_PERFSITENT_REF,
                                                    aComment.toLatin1().data(),
                                                    true);
          }
        }
      }

      if(!anIsFieldsEntryUpdated && !theIsAtOnce)
        RemoveSObject(theStudy, aMesh->myFieldsEntry);
    }

    ProcessVoidEvent(new TUpdateObjBrowser(theStudy->StudyId(),theIsDone));
  }


  //----------------------------------------------------------------------------
  void
  BuildMinMax(Result_i* theResult,
              Result_i::PInput theInput,
              CORBA::Boolean* theIsDone,
              CORBA::Boolean theIsBuild)
  {
    if(!theIsBuild || *theIsDone)
      return;

    TTimerLog aTimerLog(MYTIMEDEBUG,"Result_i::BuildMinMax");
    TResultManager aResultManager(theResult);
    
    theInput->BuildMinMax();

    *theIsDone = true;

    theResult->UpdateObservers();
  }


  //----------------------------------------------------------------------------
  void
  BuildFieldDataTree(Result_i* theResult,
                     Result_i::PInput theInput,
                     CORBA::Boolean* theIsFieldsDone,
                     CORBA::Boolean theIsBuildFields,
                     CORBA::Boolean* theIsMinMaxDone,
                     CORBA::Boolean theIsBuildMinMax,
                     _PTR(Study) theStudy)
  {
    BuildFields(theResult,
                theInput,
                theIsFieldsDone,
                theIsBuildFields,
                false,
                theStudy);

    BuildMinMax(theResult,
		theInput,
		theIsMinMaxDone,
		theIsBuildMinMax);
  }


  //----------------------------------------------------------------------------
  bool
  RemoveFile(const std::string& theFileName,
	     bool theRemoveEmptyDir)
  {
    QFileInfo aFileInfo(theFileName.c_str());
    QFile(aFileInfo.absoluteFilePath()).remove();
    
    if(theRemoveEmptyDir)
      QDir().rmdir(aFileInfo.absolutePath());

    return aFileInfo.exists();
  }


  //----------------------------------------------------------------------------
  bool
  CopyFile(const std::string& theSourceFileName,
	   const std::string& theTargetFileName)
  {
    QString aSourcePath = theSourceFileName.c_str();
#ifdef WNT
    aSourcePath.replace( QString("/"), QString("\\") );
    int prevSlash = 0;
    for ( int ind = 0; ind < aSourcePath.length(); ind ++ )
    {
      if ( aSourcePath.at( ind ) == '\\' )
        prevSlash = ind;
      if ( aSourcePath.at( ind ) == ' ' )
      {
        int nextSlash = aSourcePath.indexOf( '\\', ind);
        if ( aSourcePath.at( nextSlash - 1 ) != '"' )
        {
          aSourcePath.insert( nextSlash, '"');
          aSourcePath.insert( prevSlash + 1, '"');
          ind++;
        }
      }
    }
#endif

    QFileInfo aSourceFileInfo( aSourcePath );
    QFileInfo aTargetFileInfo( theTargetFileName.c_str() );
    if(aSourceFileInfo.absoluteFilePath() == aTargetFileInfo.absoluteFilePath())
      return true;

    QString aCommand;
    aCommand.sprintf("%s %s %s", COPY_COMMAND,
                     aSourcePath.toLatin1().data(),
                     theTargetFileName.c_str());

    return system(aCommand.toLatin1().data()) == 0;
  }


  //----------------------------------------------------------------------------
}

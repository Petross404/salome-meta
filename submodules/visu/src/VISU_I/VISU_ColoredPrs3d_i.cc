// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_Prs3dUtils.hh"

#include "VISU_Result_i.hh"
#include "VISU_ColoredPL.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <sstream>
#include <vtkMapper.h>
#include <vtkDataSet.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
//#define _DEXCEPT_
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
namespace
{
  std::string
  FindOrCreate3DPresentationsFolder(SALOMEDS::Study_ptr theStudy)
  {
    static char aFolderName[] = "3D Presentations";
    CORBA::String_var anEntry;
    SALOMEDS::SObject_var aSObject = theStudy->FindObject(aFolderName);
    if(!CORBA::is_nil(aSObject) && aSObject->Depth() == 3){
      anEntry = aSObject->GetID();
      return anEntry.in();
    }
    SALOMEDS::SComponent_var aSComponent = VISU::FindOrCreateVisuComponent(theStudy);
    CORBA::String_var aFatherEntry = aSComponent->GetID();
    anEntry = VISU::CreateAttributes(theStudy,
				     aFatherEntry.in(),
				     "",
				     "",
				     aFolderName,
				     "",
				     "",
				     true).c_str();
    return anEntry.in();
  }
}

//---------------------------------------------------------------
namespace VISU
{
  //---------------------------------------------------------------
  inline
  TMinMax 
  GetMinMax(VISU::Result_i* theResult,
	    VISU::PField theField,
	    vtkIdType theCompID)
  {
    if(!theResult->IsMinMaxDone())
      theResult->GetInput()->BuildMinMax();
    if(theField->myIsELNO)
      return theField->GetMinMax(theCompID, VISU::TNames());
    else
      return theField->GetAverageMinMax(theCompID, VISU::TNames());
  }

  //---------------------------------------------------------------
  void
  TMinMaxController
  ::UpdateReference(ColoredPrs3d_i* theFromPrs3, ColoredPrs3d_i* theToPrs3d)
  {}


  //---------------------------------------------------------------
  double
  TMinMaxController
  ::GetComponentMin(vtkIdType theCompID)
  {
    return VTK_LARGE_FLOAT;
  }


  //---------------------------------------------------------------
  double
  TMinMaxController
  ::GetComponentMax(vtkIdType theCompID)
  {
    return -VTK_LARGE_FLOAT;
  }


  //---------------------------------------------------------------
  struct TSimpleMinMaxController: virtual TVTKMinMaxController
  {
    VISU::Result_i* myResult;
    VISU::ColoredPrs3d_i* myColoredPrs3d;
    
    TSimpleMinMaxController(VISU::ColoredPrs3d_i* theColoredPrs3d):
      myResult(theColoredPrs3d->GetCResult()),
      myColoredPrs3d(theColoredPrs3d)
    {}
    
    ~TSimpleMinMaxController()
    {}

    virtual
    double
    GetComponentMin(vtkIdType theCompID)
    {
      if ( VISU::PField aField = myColoredPrs3d->GetScalarField() ) {
	TMinMax aMinMax = GetMinMax(myResult, aField, theCompID);
	return aMinMax.first;
      }
      return TMinMaxController::GetComponentMin(theCompID);
    }

    virtual
    double
    GetComponentMax(vtkIdType theCompID)
    {
      if ( VISU::PField aField = myColoredPrs3d->GetScalarField() ) {
	TMinMax aMinMax = GetMinMax(myResult, aField, theCompID);
	return aMinMax.second;
      }
      return TMinMaxController::GetComponentMax(theCompID);
    }
  };


  //---------------------------------------------------------------
  PMinMaxController
  CreateDefaultMinMaxController(VISU::ColoredPrs3d_i* theColoredPrs3d)
  {
    return PMinMaxController(new TSimpleMinMaxController(theColoredPrs3d));
  }


  //---------------------------------------------------------------
}


//---------------------------------------------------------------
VISU::ColoredPrs3d_i::
ColoredPrs3d_i(EPublishInStudyMode thePublishInStudyMode) :
  myIsRestored(true),
  myRestoringSObject( SALOMEDS::SObject::_nil() ),
  myEntity( VISU::Entity( -1 ) ),
  myPreviousEntity( VISU::Entity( -1 ) ),
  myTimeStampNumber( -1 ),
  myPreviousTimeStampNumber( -1 ),
  myIsTimeStampFixed(thePublishInStudyMode == EPublishUnderTimeStamp),
  myPublishInStudyMode(thePublishInStudyMode),
  myIsUnits( true ),
  myNumberOfLabels( -1 ),
  myOrientation( VISU::ColoredPrs3dBase::HORIZONTAL ),
  //myPosition[2], myWidth, myHeight, myTitleWidth, myTitleHeight,
  //myLabelWidth, myLabelHeight, myBarWidth, myBarHeight,
  //myIsBoldTitle, myIsItalicTitle, myIsShadowTitle, myTitFontType, myTitleColor[3],
  //myIsBoldLabel, myIsItalicLabel, myIsShadowLabel, myLblFontType, myLabelColor[3],
  myColoredPL(NULL),
  myIsFixedRange(false),
  myIsDistributionVisible(false)
{}

//---------------------------------------------------------------
VISU::ColoredPrs3d_i
::~ColoredPrs3d_i()
{}

//---------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::Register()
{
  TSuperClass::Register();
}

//---------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::UnRegister()
{
  TSuperClass::UnRegister();
}

//---------------------------------------------------------------
void 
VISU::ColoredPrs3d_i
::RemoveFromStudy()
{
  struct TEvent: public SALOME_Event
  {
    VISU::ColoredPrs3d_i* myRemovable;

    TEvent(VISU::ColoredPrs3d_i* theRemovable):
      myRemovable(theRemovable)
    {}
    
    virtual
    void
    Execute()
    {
      SALOMEDS::SObject_var aSObject = myRemovable->GetSObject();

      if(!CORBA::is_nil(aSObject.in()))
	VISU::RemoveFromStudy(aSObject,false);

      myRemovable->TSuperClass::RemoveFromStudy();
    }
  };

  ProcessVoidEvent(new TEvent(this));
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::UpdateFromResult(Result_i* theResult)
{
  struct TEvent: public SALOME_Event
  {
    VISU::ColoredPrs3d_i* myColoredPrs3d;

    TEvent(VISU::ColoredPrs3d_i* theColoredPrs3d):
      myColoredPrs3d(theColoredPrs3d)
    {}
    
    virtual
    void
    Execute()
    {
      try{
	myColoredPrs3d->DoSetInput(false, false);
	myColoredPrs3d->UpdateActors();
      }catch(std::exception& exc){
	INFOS("Follow exception was occured :\n"<<exc.what());
      }catch(...){
	INFOS("Unknown exception was occured!");
      }
    }
  };

  if ( theResult == GetCResult() ) 
    ProcessVoidEvent(new TEvent(this));
}


//---------------------------------------------------------------
bool 
VISU::ColoredPrs3d_i
::SetInput(bool theReInit)
{
  try{
    if(TSuperClass::SetInput(theReInit)){
      if(CheckIsPossible()){
	if(OnSetInput(theReInit)){
	  if(Create(GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber()))
	    return true;
	}else
	  return true;
      }
    }
  }catch(...){
  }

  return false;
}


//----------------------------------------------------------------------------
void 
VISU::ColoredPrs3d_i
::OnRestoreInput()
{
  TSuperClass::OnRestoreInput();
  myEntity = myPreviousEntity;
  myFieldName = myPreviousFieldName;
  myTimeStampNumber = myPreviousTimeStampNumber;
}


//---------------------------------------------------------------
bool 
VISU::ColoredPrs3d_i
::OnSetInput(bool theReInit)
{
  bool anIsCreatNew = !IsPipeLineExists();
  if(anIsCreatNew)
    CreatePipeLine(NULL); // to create proper pipeline

  try{
    DoSetInput(anIsCreatNew, theReInit);

    if(anIsCreatNew)
      GetSpecificPL()->Init();

    // To update scalar range according to the new input
    if(!IsRangeFixed() && theReInit)
      SetSourceRange();

    if(GetPublishInStudyMode() != EDoNotPublish)
      GetCResult()->ConnectObserver(this, myResultConnection);

    myPreviousEntity = myEntity;
    myPreviousFieldName = myFieldName;
    myPreviousTimeStampNumber = myTimeStampNumber;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
    OnRestoreInput();
    throw;
  }catch(...){
    INFOS("Unknown exception was occured!");
    OnRestoreInput();
    throw;
  }

  // To update title according to the new input
  if(theReInit)
    SetTitle(GetCFieldName().c_str());

  return anIsCreatNew;
}


//---------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetEntity(VISU::Entity theEntity)
{
  if ( myEntity == theEntity )
    return;

  VISU::TSetModified aModified(this);
  
  myEntity = theEntity;
  myParamsTime.Modified();
}


//---------------------------------------------------------------
VISU::Entity
VISU::ColoredPrs3d_i
::GetEntity()
{
  return myEntity;
}


//----------------------------------------------------------------------------
VISU::TEntity
VISU::ColoredPrs3d_i
::GetTEntity() const 
{ 
  return VISU::TEntity(int(myEntity));
}


//----------------------------------------------------------------------------
void 
VISU::ColoredPrs3d_i
::SetFieldName(const char* theFieldName)
{
  if(myFieldName == theFieldName)
    return;

  VISU::TSetModified aModified(this);
  
  myFieldName = theFieldName;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
char*
VISU::ColoredPrs3d_i
::GetFieldName()
{
  return CORBA::string_dup(myFieldName.c_str());
}


//----------------------------------------------------------------------------
std::string
VISU::ColoredPrs3d_i
::GetCFieldName() const
{
  return myFieldName;
}


//----------------------------------------------------------------------------
VISU::PField
VISU::ColoredPrs3d_i
::GetField()
{ 
  return myField;
}


//---------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetField(VISU::PField theField)
{
  myField = theField;
}


//----------------------------------------------------------------------------
void 
VISU::ColoredPrs3d_i
::SetTimeStampNumber(CORBA::Long theTimeStampNumber)
{
  if ( myTimeStampNumber == theTimeStampNumber )
    return;

  VISU::TSetModified aModified(this);
  
  myTimeStampNumber = theTimeStampNumber;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetTimeStampNumber()
{
  return myTimeStampNumber;
}


//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetTimeStampNumberByIndex( CORBA::Long theIndex )
{
  VISU::ColoredPrs3dHolder::TimeStampsRange_var aTimeStampsRange = GetTimeStampsRange();
  CORBA::Long aLength = aTimeStampsRange->length();

  if( theIndex >= 0 && theIndex < aLength )
  {
    VISU::ColoredPrs3dHolder::TimeStampInfo anInfo = aTimeStampsRange[ theIndex ];
    return anInfo.myNumber;
  }

  return -1;
}


//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetTimeStampIndexByNumber( CORBA::Long theNumber )
{
  VISU::ColoredPrs3dHolder::TimeStampsRange_var aTimeStampsRange = GetTimeStampsRange();
  CORBA::Long aLength = aTimeStampsRange->length();

  for( int index = 0; index < aLength; index++ )
  {
    VISU::ColoredPrs3dHolder::TimeStampInfo anInfo = aTimeStampsRange[ index ];

    if( anInfo.myNumber == theNumber )
      return index;
  }

  return -1;
}


//----------------------------------------------------------------------------
VISU::PField
VISU::ColoredPrs3d_i
::GetScalarField()
{
  return myField;
}


//----------------------------------------------------------------------------
VISU::PMinMaxController
VISU::ColoredPrs3d_i
::GetMinMaxController()
{
  return myMinMaxController;
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetMinMaxController( const VISU::PMinMaxController& theController )
{
  myMinMaxController = theController;

  if(!IsRangeFixed())
    SetSourceRange();
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetMinMaxController( VISU::ColoredPrs3d_i* theOrigin )
{
  VISU::PMinMaxController aController = theOrigin->GetMinMaxController();

  if ( aController )
    aController->UpdateReference( theOrigin, this );
  
  SetMinMaxController( aController );
}


//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetScalarTimeStampNumber() const
{
  return myTimeStampNumber;
}


//----------------------------------------------------------------------------
VISU::ColoredPrs3dHolder::TimeStampsRange* 
VISU::ColoredPrs3d_i
::GetTimeStampsRange()
{
  VISU::ColoredPrs3dHolder::TimeStampsRange_var aTimeStampsRange =
    new VISU::ColoredPrs3dHolder::TimeStampsRange();
  VISU::TValField& aValField = GetField()->myValField;
  if(IsTimeStampFixed()){
    aTimeStampsRange->length(1);
    PValForTime& aValForTime = aValField[GetTimeStampNumber()];
    std::string aTime = VISU_Convertor::GenerateName(aValForTime->myTime);
    VISU::ColoredPrs3dHolder::TimeStampInfo anInfo;
    anInfo.myNumber = GetTimeStampNumber();
    anInfo.myTime = aTime.c_str();
    aTimeStampsRange[0] = anInfo;
    return aTimeStampsRange._retn();    
  }

  // To exclude timstamps with repeated time
  typedef std::map<std::string, long> TTimeStampsRange;
  TTimeStampsRange aRange;
  {
    VISU::TValField::const_iterator anIter = aValField.begin();
    for(; anIter != aValField.end(); anIter++){
      vtkIdType aTimeStampNumber = anIter->first;
      const PValForTime& aValForTime = anIter->second;
      std::string aTime = VISU_Convertor::GenerateName(aValForTime->myTime);
      aRange[aTime] = aTimeStampNumber;
    }
  }

  // To sort timestamps according to their timestamp number
  typedef std::map<long, std::string> TTimeStampsSortedRange;
  TTimeStampsSortedRange aSortedRange;
  {
    TTimeStampsRange::const_iterator anIter = aRange.begin();
    for(size_t aCounter = 0; anIter != aRange.end(); anIter++, aCounter++){
      vtkIdType aTimeStampNumber = anIter->second;
      const std::string& aTime = anIter->first;
      aSortedRange[aTimeStampNumber] = aTime;
    }
  }

  // To map the C++ data structures to the corresponding CORBA ones
  {
    aTimeStampsRange->length(aRange.size());
    TTimeStampsSortedRange::const_iterator anIter = aSortedRange.begin();
    for(size_t aCounter = 0; anIter != aSortedRange.end(); anIter++, aCounter++){
      vtkIdType aTimeStampNumber = anIter->first;
      const std::string& aTime = anIter->second;
      VISU::ColoredPrs3dHolder::TimeStampInfo anInfo;
      anInfo.myNumber = aTimeStampNumber;
      anInfo.myTime = aTime.c_str();
      aTimeStampsRange[aCounter] = anInfo;
    }
  }
  return aTimeStampsRange._retn();
}

CORBA::Float
VISU::ColoredPrs3d_i
::GetMemorySize()
{
  return TSuperClass::GetMemorySize();
}

VISU::ColoredPrs3dHolder::BasicInput*
VISU::ColoredPrs3d_i
::GetBasicInput()
{
  VISU::ColoredPrs3dHolder::BasicInput* aBasicInput = new VISU::ColoredPrs3dHolder::BasicInput();
  aBasicInput->myResult = GetResultObject();
  aBasicInput->myMeshName = GetMeshName();
  aBasicInput->myEntity = GetEntity();
  aBasicInput->myFieldName = GetFieldName();
  aBasicInput->myTimeStampNumber = GetTimeStampNumber();

  return aBasicInput;
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetHolderEntry(const std::string& theEntry)
{
  myHolderEntry = theEntry;
}


//----------------------------------------------------------------------------
std::string
VISU::ColoredPrs3d_i
::GetHolderEntry()
{
  return myHolderEntry;
}


//----------------------------------------------------------------------------
std::string
VISU::ColoredPrs3d_i
::GetActorEntry()
{
  if(myHolderEntry != "")
    return myHolderEntry;
  return TSuperClass::GetActorEntry();
}


//----------------------------------------------------------------------------
CORBA::Boolean 
VISU::ColoredPrs3d_i
::IsTimeStampFixed()
{
  return myIsTimeStampFixed;
}

//----------------------------------------------------------------------------
VISU::ColoredPrs3d_i::EPublishInStudyMode 
VISU::ColoredPrs3d_i
::GetPublishInStudyMode()
{
  return myPublishInStudyMode;
}

//---------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SameAs(const Prs3d_i* theOrigin)
{
  if(const ColoredPrs3d_i* aPrs3d = dynamic_cast<const ColoredPrs3d_i*>(theOrigin)){
    ColoredPrs3d_i* anOrigin = const_cast<ColoredPrs3d_i*>(aPrs3d);

    bool anIsCreatNew = OnSetInput(false);
    if(anIsCreatNew)
      Build(ESameAs);
    
    TSuperClass::SameAs(theOrigin);
    
    CORBA::Long aNbComp = GetScalarField()->myNbComp;
    CORBA::Long anOriginNbComp = anOrigin->GetScalarField()->myNbComp;
    if(anOriginNbComp < aNbComp)
      SetScalarMode(0);
    else
      SetScalarMode(anOrigin->GetScalarMode());

    SetNbColors(anOrigin->GetNbColors());
    
    SetUnitsVisible(anOrigin->IsUnitsVisible());
    SetIsDistributionVisible(anOrigin->GetIsDistributionVisible());
    SetLabelsFormat( anOrigin->GetLabelsFormat() );
    
    SetBarOrientation(anOrigin->GetBarOrientation());
    
    SetMinMaxController( anOrigin );

    if ( anOrigin->IsRangeFixed() )
      SetRange( anOrigin->GetMin(), anOrigin->GetMax() );
    else
      SetSourceRange();

    SetScalarFilterRange( anOrigin->GetScalarFilterMin(), anOrigin->GetScalarFilterMax() );
    UseScalarFiltering( anOrigin->IsScalarFilterUsed() );

    SetPosition(anOrigin->GetPosX(), anOrigin->GetPosY());
    SetSize(anOrigin->GetWidth(), anOrigin->GetHeight());
    SetRatios(anOrigin->GetTitleSize(), anOrigin->GetLabelSize(),
	      anOrigin->GetBarWidth(), anOrigin->GetBarHeight());
    SetLabels(anOrigin->GetLabels());

    SetTitle(anOrigin->GetTitle());   
    SetBoldTitle(anOrigin->IsBoldTitle());
    SetItalicTitle(anOrigin->IsItalicTitle());
    SetShadowTitle(anOrigin->IsShadowTitle());
    SetTitFontType(anOrigin->GetTitFontType());
    double r,g,b;
    anOrigin->GetTitleColor(r,g,b);
    SetTitleColor(r,g,b);
    
    SetBoldLabel(anOrigin->IsBoldLabel());
    SetItalicLabel(anOrigin->IsItalicLabel());
    SetShadowLabel(anOrigin->IsShadowLabel());
    SetLblFontType(anOrigin->GetLblFontType());
    anOrigin->GetLabelColor(r,g,b);
    SetLabelColor(r,g,b);

    myIsTimeStampFixed = anOrigin->IsTimeStampFixed();

    SetHolderEntry( anOrigin->GetHolderEntry() );

    myGroupNames = anOrigin->GetGroupNames();
  }
}
  
//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::UpdateActor(VISU_ActorBase* theActor) 
{
  bool isOnGroups = myGroupNames.size() > 0;
  if ( isOnGroups )
  {
    QStringList aList;
    TGroupNames::const_iterator anIter = myGroupNames.begin();
    for ( ; anIter != myGroupNames.end(); anIter++ )
    {
      const std::string aGroupName = *anIter;
      aList << QString( aGroupName.c_str() );
    }
    theActor->SetNameActorText( aList.join( "\n" ).toLatin1().constData() );
  }
  theActor->SetIsDisplayNameActor( isOnGroups );

  TSuperClass::UpdateActor(theActor);
}

//----------------------------------------------------------------------------
CORBA::Long 
VISU::ColoredPrs3d_i
::GetScalarMode()
{
  return myColoredPL->GetScalarMode();
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetScalarMode(CORBA::Long theScalarMode) 
{
  CORBA::Long aNbComp = GetScalarField()->myNbComp;
  if(aNbComp == 1)
    theScalarMode = 1;
  else if(theScalarMode > aNbComp)
    theScalarMode = 0;

  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ColoredPL, int>
		   (GetSpecificPL(), &VISU_ColoredPL::SetScalarMode, theScalarMode));
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::ColoredPrs3d_i
::GetMin()
{
  return myColoredPL->GetScalarRange()[0];
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::ColoredPrs3d_i
::GetMax()
{
  return myColoredPL->GetScalarRange()[1];
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetRange( CORBA::Double theMin, CORBA::Double theMax )
{
  VISU::TSetModified aModified(this);

  double aScalarRange[2] = {theMin, theMax};
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ColoredPL, double*>
		   (GetSpecificPL(), &VISU_ColoredPL::SetScalarRange, aScalarRange));
  UseFixedRange(true);
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetScalarFilterRange( CORBA::Double theMin, CORBA::Double theMax )
{
  VISU::TSetModified aModified(this);

  double aScalarRange[ 2 ] = { theMin, theMax };
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ColoredPL, double*>
		   (GetSpecificPL(), &VISU_ColoredPL::SetScalarFilterRange, aScalarRange) );
}


//----------------------------------------------------------------------------
CORBA::Double
VISU::ColoredPrs3d_i
::GetScalarFilterMin()
{
  double aScalarRange[ 2 ];
  GetSpecificPL()->GetScalarFilterRange( aScalarRange );

  return aScalarRange[ 0 ];
}


//----------------------------------------------------------------------------
CORBA::Double
VISU::ColoredPrs3d_i
::GetScalarFilterMax()
{
  double aScalarRange[ 2 ];
  GetSpecificPL()->GetScalarFilterRange( aScalarRange );

  return aScalarRange[ 1 ];
}


//----------------------------------------------------------------------------
CORBA::Boolean
VISU::ColoredPrs3d_i
::IsScalarFilterUsed()
{
  return myColoredPL->IsScalarFilterUsed();
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::UseScalarFiltering( CORBA::Boolean theUseScalarFilter )
{
  return myColoredPL->UseScalarFiltering( theUseScalarFilter );
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetSourceRange()
{
  VISU::TSetModified aModified(this);

  if(IsTimeStampFixed()){
    GetSpecificPL()->SetSourceRange();
    ProcessVoidEvent(new TVoidMemFunEvent<VISU_ColoredPL>
		     (GetSpecificPL(), &VISU_ColoredPL::SetSourceRange));
  }else{
    double aScalarRange[ 2 ] = {GetSourceMin(), GetSourceMax()};
    ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ColoredPL, double*>
		     (GetSpecificPL(), &VISU_ColoredPL::SetScalarRange, aScalarRange));
  }
  UseFixedRange(false);
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::ColoredPrs3d_i
::GetSourceMin()
{
  if(IsTimeStampFixed()){
    double aRange[2];
    GetSpecificPL()->GetSourceRange(aRange);
    return aRange[0];
  }
  return GetComponentMin(GetScalarMode());
}

//----------------------------------------------------------------------------
CORBA::Double 
VISU::ColoredPrs3d_i
::GetSourceMax()
{
  if(IsTimeStampFixed()){
    double aRange[2];
    GetSpecificPL()->GetSourceRange(aRange);
    return aRange[1];
  }
  return GetComponentMax(GetScalarMode());
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetNbColors(CORBA::Long theNbColors)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ColoredPL, int>
		   (GetSpecificPL(), &VISU_ColoredPL::SetNbColors, theNbColors));
}

//----------------------------------------------------------------------------
CORBA::Long 
VISU::ColoredPrs3d_i
::GetNbColors()
{
  return GetSpecificPL()->GetNbColors();
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetBarOrientation(VISU::ColoredPrs3dBase::Orientation theOrientation)
{
  if ( myOrientation == theOrientation )
    return;

  VISU::TSetModified aModified(this);
  
  myOrientation = theOrientation;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
VISU::ColoredPrs3dBase::Orientation 
VISU::ColoredPrs3d_i
::GetBarOrientation() 
{
  return myOrientation;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetPosition(CORBA::Double theX, CORBA::Double theY) 
{ 
  bool anIsSameValue = VISU::CheckIsSameValue(myPosition[0], theX);
  anIsSameValue &= VISU::CheckIsSameValue(myPosition[1], theY);
  if(anIsSameValue)
    return;

  VISU::TSetModified aModified(this);

  myPosition[0] = theX; 
  myPosition[1] = theY;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Double
VISU::ColoredPrs3d_i
::GetPosX() 
{
  return myPosition[0];
}

//----------------------------------------------------------------------------
CORBA::Double
VISU::ColoredPrs3d_i
::GetPosY() 
{ 
  return myPosition[1];
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetSize(CORBA::Double theWidth, CORBA::Double theHeight) 
{
  bool anIsSameValue = VISU::CheckIsSameValue(myWidth, theWidth);
  anIsSameValue &= VISU::CheckIsSameValue(myHeight, theHeight);
  if(anIsSameValue)
    return;

  VISU::TSetModified aModified(this);

  myWidth = theWidth; 
  myHeight = theHeight;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Double
VISU::ColoredPrs3d_i
::GetWidth() 
{
  return myWidth;
}

//----------------------------------------------------------------------------
CORBA::Double
VISU::ColoredPrs3d_i
::GetHeight() 
{ 
  return myHeight;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetRatios(CORBA::Long theTitleSize, CORBA::Long theLabelSize, 
	    CORBA::Long theBarWidth, CORBA::Long theBarHeight) 
{
  bool anIsSameValue = VISU::CheckIsSameValue(myTitleSize, theTitleSize);
  anIsSameValue &= VISU::CheckIsSameValue(myLabelSize, theLabelSize);
  anIsSameValue &= VISU::CheckIsSameValue(myBarWidth, theBarWidth);
  anIsSameValue &= VISU::CheckIsSameValue(myBarHeight, theBarHeight);
  if(anIsSameValue)
    return;

  VISU::TSetModified aModified(this);

  myTitleSize = theTitleSize; 
  myLabelSize = theLabelSize; 
  myBarWidth = theBarWidth; 
  myBarHeight = theBarHeight;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetTitleSize() 
{
  return myTitleSize;
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetLabelSize() 
{
  return myLabelSize;
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetBarWidth() 
{
  return myBarWidth;
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetBarHeight() 
{ 
  return myBarHeight;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetLabels(CORBA::Long theNbLabels)
{
  if(myNumberOfLabels == theNbLabels)
    return;

  VISU::TSetModified aModified(this);

  myNumberOfLabels = theNbLabels;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Long
VISU::ColoredPrs3d_i
::GetLabels() 
{ 
  return myNumberOfLabels;
}

//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetLabelsFormat(const char* theFormat)
{
  if( myLabelsFormat != theFormat ){
    VISU::TSetModified aModified(this);
    myLabelsFormat = theFormat;
    myParamsTime.Modified();
  }
}


//----------------------------------------------------------------------------
char* 
VISU::ColoredPrs3d_i
::GetLabelsFormat() 
{ 
  return CORBA::string_dup(myLabelsFormat.c_str());
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetTitle(const char* theTitle) 
{
  VISU::PValForTime aValForTime;
  VISU::TValField& aValField = GetScalarField()->myValField;
  VISU::TValField::iterator anIter = aValField.find(GetScalarTimeStampNumber());
  if (anIter != aValField.end())
    aValForTime = anIter->second;

  if (aValForTime) {
    std::stringstream aStream;
    const VISU::TTime& aTime = aValForTime->myTime;
    aStream<<theTitle<<" ";
    if(IsUnitsVisible()) {
      aStream << VISU_Convertor::GenerateName(aTime);
    } else {
      QString aName;
      aName.sprintf("%g", aTime.first);
      aStream << aName.toLatin1().data();
    }
    aStream <<std::ends;
    std::string aScalarBarTitle = aStream.str();
    if(myTitle != theTitle || myScalarBarTitle != aScalarBarTitle){
      VISU::TSetModified aModified(this);

      myScalarBarTitle = aScalarBarTitle;
      myTitle = theTitle;
      myParamsTime.Modified();
    }
  }
}

//----------------------------------------------------------------------------
char* 
VISU::ColoredPrs3d_i
::GetTitle() 
{ 
  return CORBA::string_dup(myTitle.c_str());
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetUnitsVisible(CORBA::Boolean theIsVisible)
{
  if ( myIsUnits == theIsVisible ) 
    return;

  VISU::TSetModified aModified( this );

  myIsUnits = theIsVisible;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Boolean
VISU::ColoredPrs3d_i
::IsUnitsVisible()
{
  return myIsUnits;
}

//----------------------------------------------------------------------------
std::string 
VISU::ColoredPrs3d_i
::GetCTitle() 
{ 
  return myTitle;
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsBoldTitle() 
{ 
  return myIsBoldTitle;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetBoldTitle(bool theIsBoldTitle)
{
  if(myIsBoldTitle == theIsBoldTitle)
    return;

  VISU::TSetModified aModified(this);

  myIsBoldTitle = theIsBoldTitle;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsItalicTitle() 
{ 
  return myIsItalicTitle;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetItalicTitle(bool theIsItalicTitle)
{ 
  if(myIsItalicTitle == theIsItalicTitle)
    return;

  VISU::TSetModified aModified(this);

  myIsItalicTitle = theIsItalicTitle;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsShadowTitle() 
{ 
  return myIsShadowTitle;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetShadowTitle(bool theIsShadowTitle)
{ 
  if(myIsShadowTitle == theIsShadowTitle)
    return;

  VISU::TSetModified aModified(this);

  myIsShadowTitle = theIsShadowTitle;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
int
VISU::ColoredPrs3d_i
::GetTitFontType()
{
  return myTitFontType;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetTitFontType(int theTitFontType)
{
  if(myTitFontType == theTitFontType)
    return;

  VISU::TSetModified aModified(this);

  myTitFontType = theTitFontType;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::GetTitleColor(double& theR, 
		double& theG, 
		double& theB)
{
  theR = myTitleColor[0]; 
  theG = myTitleColor[1]; 
  theB = myTitleColor[2];
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetTitleColor(double theR, 
		double theG, 
		double theB)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myTitleColor[0], theR);
  anIsSameValue &= VISU::CheckIsSameValue(myTitleColor[1], theG);
  anIsSameValue &= VISU::CheckIsSameValue(myTitleColor[2], theB);
  if(anIsSameValue)
    return;

  VISU::TSetModified aModified(this);

  myTitleColor[0] = theR; 
  myTitleColor[1] = theG; 
  myTitleColor[2] = theB; 
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsBoldLabel()
{
  return myIsBoldLabel;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetBoldLabel(bool theIsBoldLabel) 
{
  if(myIsBoldLabel == theIsBoldLabel)
    return;

  VISU::TSetModified aModified(this);

  myIsBoldLabel = theIsBoldLabel;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsItalicLabel() 
{
  return myIsItalicLabel;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetItalicLabel(bool theIsItalicLabel)
{
  if(myIsItalicLabel == theIsItalicLabel)
    return;

  VISU::TSetModified aModified(this);

  myIsItalicLabel = theIsItalicLabel;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsShadowLabel() 
{
  return myIsShadowLabel;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetShadowLabel(bool theIsShadowLabel)
{
  if(myIsShadowLabel == theIsShadowLabel)
    return;

  VISU::TSetModified aModified(this);

  myIsShadowLabel = theIsShadowLabel;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
int
VISU::ColoredPrs3d_i
::GetLblFontType()
{
  return myLblFontType;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetLblFontType(int theLblFontType)
{
  if(myLblFontType == theLblFontType)
    return;

  VISU::TSetModified aModified(this);

  myLblFontType = theLblFontType;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::GetLabelColor(double& theR, 
		double& theG, 
		double& theB)
{
  theR = myLabelColor[0]; 
  theG = myLabelColor[1]; 
  theB = myLabelColor[2];
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetLabelColor(double theR, 
		double theG, 
		double theB)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myLabelColor[0], theR);
  anIsSameValue &= VISU::CheckIsSameValue(myLabelColor[1], theG);
  anIsSameValue &= VISU::CheckIsSameValue(myLabelColor[2], theB);
  if(anIsSameValue)
    return;

  VISU::TSetModified aModified(this);

  myLabelColor[0] = theR; 
  myLabelColor[1] = theG; 
  myLabelColor[2] = theB; 
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
CORBA::Boolean
VISU::ColoredPrs3d_i
::IsRangeFixed() 
{ 
  return myIsFixedRange; 
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::UseFixedRange(bool theUseFixedRange)
{
  if(myIsFixedRange == theUseFixedRange)
    return;

  myIsFixedRange = theUseFixedRange;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
std::string
VISU::ColoredPrs3d_i
::GetScalarBarTitle()
{
  return myScalarBarTitle;
}

//----------------------------------------------------------------------------
/**
 * Creates ColoredPrs3d and initialises it from resources
 */
VISU::Storable* 
VISU::ColoredPrs3d_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  SetMeshName(theMeshName.c_str());
  SetEntity(theEntity);
  SetFieldName(theFieldName.c_str());
  SetTimeStampNumber(theTimeStampNumber);
  OnSetInput(false);

  Build(ECreateNew); // to get corresponding input from result and initilize the pipeline

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  int aScalarMode = aResourceMgr->integerValue("VISU", "scalar_bar_mode", 0);
  SetScalarMode(aScalarMode);

  // Scalar Range
  int aRangeType = aResourceMgr->integerValue("VISU" , "scalar_range_type", 0);
  if ( aRangeType == 1 ) {
    float aMin = aResourceMgr->doubleValue("VISU", "scalar_range_min", 0);
    float aMax = aResourceMgr->doubleValue("VISU", "scalar_range_max", 0);
    SetRange( aMin, aMax );
  }
  UseFixedRange( aRangeType == 1 );

  bool isFiltered = aResourceMgr->booleanValue("VISU", "scalar_bar_filter_by_scalars", false);
  UseScalarFiltering( isFiltered );

  int aNumberOfColors = aResourceMgr->integerValue( "VISU", "scalar_bar_num_colors", 64 );
  SetNbColors(aNumberOfColors);

  bool isUnits = aResourceMgr->booleanValue( "VISU", "scalar_bar_display_units", true );
  SetUnitsVisible(isUnits);

  SetIsDistributionVisible( aResourceMgr->booleanValue("VISU", "scalar_bar_show_distribution", false) ); // RKV
  
  int lp = aResourceMgr->integerValue( "VISU", "scalar_bar_label_precision", 3 );
  SetLabelsFormat( VISU::ToFormat( lp ).c_str() );

  // Orientation
  int anOrientation = aResourceMgr->integerValue("VISU", "scalar_bar_orientation", 0);
  if(anOrientation == 1)
    SetBarOrientation(VISU::ColoredPrs3dBase::HORIZONTAL);
  else
    SetBarOrientation(VISU::ColoredPrs3dBase::VERTICAL);

  // Scalar Bar origin
  QString propertyName = QString( "scalar_bar_%1_" ).arg( anOrientation == 0 ? "vertical" : "horizontal" );

  double aXorigin = (myOrientation == VISU::ColoredPrs3dBase::VERTICAL) ? 0.01 : 0.1;
  aXorigin = aResourceMgr->doubleValue("VISU", propertyName + "x", aXorigin);
  myPosition[0] = aXorigin;

  double aYorigin = (myOrientation == VISU::ColoredPrs3dBase::VERTICAL) ? 0.1 : 0.01;
  aYorigin = aResourceMgr->doubleValue("VISU", propertyName + "y", aYorigin);
  myPosition[1] = aYorigin;

  // Scalar Bar size
  myWidth = (myOrientation == VISU::ColoredPrs3dBase::VERTICAL)? 0.08 : 0.8;
  myWidth = aResourceMgr->doubleValue("VISU", propertyName + "width", myWidth);

  myHeight = (myOrientation == VISU::ColoredPrs3dBase::VERTICAL)? 0.8:0.08;
  myHeight = aResourceMgr->doubleValue("VISU", propertyName + "height", myHeight);

  myTitleSize = 0;
  myTitleSize = aResourceMgr->integerValue("VISU", propertyName + "title_size", myTitleSize);

  myLabelSize = 0;
  myLabelSize = aResourceMgr->integerValue("VISU", propertyName + "label_size", myLabelSize);

  myBarWidth = 0;
  myBarWidth = aResourceMgr->integerValue("VISU", propertyName + "bar_width", myBarWidth);

  myBarHeight = 0;
  myBarHeight = aResourceMgr->integerValue("VISU", propertyName + "bar_height", myBarHeight);

  // scalar bar default position
  bool anIsArrangeBar = aResourceMgr->booleanValue("VISU", "scalar_bars_default_position", 0);
  int aPlace = 1;
  if (anIsArrangeBar){
    aPlace = aResourceMgr->integerValue("VISU", "scalar_bar_position_num",0);
  }
  if(myOrientation == VISU::ColoredPrs3dBase::HORIZONTAL){
    myPosition[1] += myHeight*(aPlace-1);
  } else {
    myPosition[0] += myWidth*(aPlace-1);
  }

  // Nb of Labels
  myNumberOfLabels = aResourceMgr->integerValue( "VISU", "scalar_bar_num_labels", 5 );

  // Fonts properties definition
  myIsBoldTitle = myIsItalicTitle = myIsShadowTitle = true;
  myTitFontType = VTK_ARIAL;

  if(aResourceMgr->hasValue( "VISU", "scalar_bar_title_font" )){
    QFont f = aResourceMgr->fontValue( "VISU", "scalar_bar_title_font" );
    if ( f.family() == "Arial" )
      myTitFontType = VTK_ARIAL;
    else if ( f.family() == "Courier" )
      myTitFontType = VTK_COURIER;
    else if ( f.family() == "Times" )
      myTitFontType = VTK_TIMES;
    
    myIsBoldTitle   = f.bold();
    myIsItalicTitle = f.italic();
    myIsShadowTitle =  f.overline();
  }

  QColor aTextColor = aResourceMgr->colorValue( "VISU", "scalar_bar_title_color", QColor( 255, 255, 255 ) );

  SetTitleColor(aTextColor.red()/255., aTextColor.green()/255., aTextColor.blue()/255.);
		
  myTitleColor[0] = aTextColor.red()   / 255.;
  myTitleColor[1] = aTextColor.green() / 255.;
  myTitleColor[2] = aTextColor.blue()  / 255.;

  myIsBoldLabel = myIsItalicLabel = myIsShadowLabel = true;
  myLblFontType = VTK_ARIAL;

  if( aResourceMgr->hasValue( "VISU", "scalar_bar_label_font" )){
    QFont f = aResourceMgr->fontValue( "VISU", "scalar_bar_label_font" );
    if ( f.family() == "Arial" )
      myLblFontType = VTK_ARIAL;
    else if ( f.family() == "Courier" )
      myLblFontType = VTK_COURIER;
    else if ( f.family() == "Times" )
      myLblFontType = VTK_TIMES;
    
    myIsBoldLabel   = f.bold();
    myIsItalicLabel = f.italic();
    myIsShadowLabel =  f.overline();
  }

  QColor aLabelColor = aResourceMgr->colorValue( "VISU", "scalar_bar_label_color", QColor( 255, 255, 255 ) );

  SetLabelColor(aLabelColor.red()/255., aLabelColor.green()/255., aLabelColor.blue()/255.);

  myLabelColor[0] = aLabelColor.red()   / 255.;
  myLabelColor[1] = aLabelColor.green() / 255.;
  myLabelColor[2] = aLabelColor.blue()  / 255.;
  
  // Parameters of labels displaed field values

  myValLblFontType = VTK_ARIAL;
  myIsBoldValLbl = true;
  myIsItalicValLbl = myIsShadowValLbl = false;
  myValLblFontSize = 12;
  if( aResourceMgr->hasValue( "VISU", "values_labeling_font" ) )
  {
    // family
    QFont f = aResourceMgr->fontValue( "VISU", "values_labeling_font" );
    if ( f.family() == "Arial" )
      myValLblFontType = VTK_ARIAL;
    else if ( f.family() == "Courier" )
      myValLblFontType = VTK_COURIER;
    else if ( f.family() == "Times" )
      myValLblFontType = VTK_TIMES;

    // size
    if ( f.pointSize() > -1 )
      myValLblFontSize = f.pointSize();

    // color
    QColor aColor = aResourceMgr->colorValue( 
      "VISU", "values_labeling_color", QColor( 255, 255, 255 ) );
    myValLblFontColor[ 0 ] = aColor.red() / 255.;
    myValLblFontColor[ 1 ] = aColor.green() / 255.;
    myValLblFontColor[ 2 ] = aColor.blue()/ 255.;

    // bold, italic, shadow
    myIsBoldValLbl = f.bold();
    myIsItalicValLbl = f.italic();
    myIsShadowValLbl =  f.overline();
  }
  
  return this;
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SaveRestoringState(SALOMEDS::SObject_ptr theSObject,
		     const Storable::TRestoringMap& theMap)
{
  myRestoringSObject = SALOMEDS::SObject::_duplicate(theSObject);
  myRestoringMap = theMap;
  myIsRestored = false;
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::InitFromRestoringState()
{
  if(!myIsRestored){
    Restore(myRestoringSObject, myRestoringMap);
    myIsRestored = true;
  }
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::ColoredPrs3d_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetEntity((VISU::Entity)VISU::Storable::FindValue(theMap,"myEntity").toInt());
  SetFieldName(VISU::Storable::FindValue(theMap,"myFieldName").toLatin1().data());
  SetTimeStampNumber(VISU::Storable::FindValue(theMap,"myIteration").toInt());
  myIsTimeStampFixed = VISU::Storable::FindValue(theMap,"myIsTimeStampFixed", "1").toInt();
  OnSetInput(false);

  Build(ERestore);

  SetScalarMode(VISU::Storable::FindValue( theMap,"myScalarMode" ).toInt() );
  {
    float aMin = VISU::Storable::FindValue( theMap, "myScalarRange[0]" ).toDouble();
    float aMax = VISU::Storable::FindValue( theMap, "myScalarRange[1]" ).toDouble();
    SetRange( aMin, aMax );
  }
  {
    bool isFiltered = VISU::Storable::FindValue( theMap, "myIsFilteredByScalars", "0" ).toInt();
    float aMin = VISU::Storable::FindValue( theMap, "myScalarFilterRange[0]" ).toDouble();
    float aMax = VISU::Storable::FindValue( theMap, "myScalarFilterRange[1]" ).toDouble();
    SetScalarFilterRange( aMin, aMax );
    UseScalarFiltering( isFiltered );
  }
  SetIsDistributionVisible(VISU::Storable::FindValue(theMap,"myIsDistributionVisible", "0").toInt());

  UseFixedRange(VISU::Storable::FindValue(theMap,"myIsFixedRange", "0").toInt());

  SetNbColors(VISU::Storable::FindValue(theMap,"myNumberOfColors").toInt());
  SetUnitsVisible(VISU::Storable::FindValue(theMap,"myUnitsVisible", "1").toInt());
  SetLabelsFormat(VISU::Storable::FindValue(theMap,"myLabelsFormat", "%-#6.3g").toLatin1().data());
  SetBarOrientation((VISU::ColoredPrs3dBase::Orientation)VISU::Storable::FindValue(theMap,"myOrientation").toInt());
  
  SetTitle(VISU::Storable::FindValue(theMap,"myTitle").toLatin1().data());
  myNumberOfLabels = VISU::Storable::FindValue(theMap,"myNumberOfLabels").toInt();
  myPosition[0] = VISU::Storable::FindValue(theMap,"myPosition[0]").toDouble();
  myPosition[1] = VISU::Storable::FindValue(theMap,"myPosition[1]").toDouble();
  myWidth = VISU::Storable::FindValue(theMap,"myWidth").toDouble();
  myHeight = VISU::Storable::FindValue(theMap,"myHeight").toDouble();
  myTitleSize = VISU::Storable::FindValue(theMap,"myTitleSize").toInt();
  myLabelSize = VISU::Storable::FindValue(theMap,"myLabelSize").toInt();
  myBarWidth = VISU::Storable::FindValue(theMap,"myBarWidth").toInt();
  myBarHeight = VISU::Storable::FindValue(theMap,"myBarHeight").toInt();

  myTitFontType = VISU::Storable::FindValue(theMap,"myTitFontType").toInt();
  myIsBoldTitle = VISU::Storable::FindValue(theMap,"myIsBoldTitle").toInt();
  myIsItalicTitle = VISU::Storable::FindValue(theMap,"myIsItalicTitle").toInt();
  myIsShadowTitle = VISU::Storable::FindValue(theMap,"myIsShadowTitle").toInt();
  myTitleColor[0] = VISU::Storable::FindValue(theMap,"myTitleColor[0]").toFloat();
  myTitleColor[1] = VISU::Storable::FindValue(theMap,"myTitleColor[1]").toFloat();
  myTitleColor[2] = VISU::Storable::FindValue(theMap,"myTitleColor[2]").toFloat();

  myLblFontType = VISU::Storable::FindValue(theMap,"myLblFontType").toInt();
  myIsBoldLabel = VISU::Storable::FindValue(theMap,"myIsBoldLabel").toInt();
  myIsItalicLabel = VISU::Storable::FindValue(theMap,"myIsItalicLabel").toInt();
  myIsShadowLabel = VISU::Storable::FindValue(theMap,"myIsShadowLabel").toInt();
  myLabelColor[0] = VISU::Storable::FindValue(theMap,"myLabelColor[0]").toFloat();
  myLabelColor[1] = VISU::Storable::FindValue(theMap,"myLabelColor[1]").toFloat();
  myLabelColor[2] = VISU::Storable::FindValue(theMap,"myLabelColor[2]").toFloat();

  bool anIsExists;
  QString aGeomNames = VISU::Storable::FindValue(theMap, "myGeomNameList", &anIsExists);
  if(anIsExists){
    QStringList aGeomNameList = aGeomNames.split("|", QString::SkipEmptyParts);
    int aNbOfGroups = aGeomNameList.count();
    if(aNbOfGroups > 0){
      RemoveAllGeom();
      for(int aGroupNum = 0; aGroupNum < aNbOfGroups; aGroupNum++){
	QString aGroupName = aGeomNameList[aGroupNum];
	AddMeshOnGroup(aGroupName.toLatin1().data());
      }
    }
  }
  
  // Parameters of labels displayed field values

  myValLblFontType = VISU::Storable::FindValue( theMap, "myValLblFontType", "0"  ).toInt();
  myIsBoldValLbl = VISU::Storable::FindValue( theMap, "myIsBoldValLbl", "1" ).toInt();
  myIsItalicValLbl = VISU::Storable::FindValue( theMap, "myIsItalicValLbl", "0" ).toInt();
  myIsShadowValLbl = VISU::Storable::FindValue( theMap, "myIsShadowValLbl", "0" ).toInt();
  myValLblFontSize = VISU::Storable::FindValue( theMap, "myValLblFontSize", "12" ).toDouble();
  myValLblFontColor[ 0 ] = VISU::Storable::FindValue( theMap, "myValLblFontColor[0]", "1" ).toFloat();
  myValLblFontColor[ 1 ] = VISU::Storable::FindValue( theMap, "myValLblFontColor[1]", "1" ).toFloat();
  myValLblFontColor[ 2 ] = VISU::Storable::FindValue( theMap, "myValLblFontColor[2]", "1" ).toFloat();
  
  return this;
}

//----------------------------------------------------------------------------
void 
VISU::ColoredPrs3d_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myEntity",         GetEntity() );
  Storable::DataToStream( theStr, "myFieldName",      GetCFieldName().c_str() );
  Storable::DataToStream( theStr, "myIteration",      int(GetTimeStampNumber()) );
  Storable::DataToStream( theStr, "myIsTimeStampFixed", int(myIsTimeStampFixed) );

  Storable::DataToStream( theStr, "myScalarMode",     int(GetScalarMode()) );
  Storable::DataToStream( theStr, "myScalarRange[0]", GetMin() );
  Storable::DataToStream( theStr, "myScalarRange[1]", GetMax() );
  Storable::DataToStream( theStr, "myIsFixedRange",   IsRangeFixed() );
  
  Storable::DataToStream( theStr, "myIsFilteredByScalars",  IsScalarFilterUsed() );
  Storable::DataToStream( theStr, "myScalarFilterRange[0]", GetScalarFilterMin() );
  Storable::DataToStream( theStr, "myScalarFilterRange[1]", GetScalarFilterMax() );

  Storable::DataToStream( theStr, "myIsDistributionVisible", GetIsDistributionVisible() ); // RKV

  Storable::DataToStream( theStr, "myNumberOfColors", int(GetNbColors()) );
  Storable::DataToStream( theStr, "myOrientation",    myOrientation );

  Storable::DataToStream( theStr, "myTitle",          myTitle.c_str() );
  Storable::DataToStream( theStr, "myUnitsVisible",   myIsUnits );
  Storable::DataToStream( theStr, "myNumberOfLabels", myNumberOfLabels );

  Storable::DataToStream( theStr, "myLabelsFormat",   myLabelsFormat.c_str() );

  Storable::DataToStream( theStr, "myPosition[0]",    myPosition[0] );
  Storable::DataToStream( theStr, "myPosition[1]",    myPosition[1] );
  Storable::DataToStream( theStr, "myWidth",          myWidth );
  Storable::DataToStream( theStr, "myHeight",         myHeight );
  Storable::DataToStream( theStr, "myTitleSize",     myTitleSize );
  Storable::DataToStream( theStr, "myLabelSize",     myLabelSize );
  Storable::DataToStream( theStr, "myBarWidth",       myBarWidth );
  Storable::DataToStream( theStr, "myBarHeight",      myBarHeight );

  Storable::DataToStream( theStr, "myTitFontType",    myTitFontType );
  Storable::DataToStream( theStr, "myIsBoldTitle",    myIsBoldTitle );
  Storable::DataToStream( theStr, "myIsItalicTitle",  myIsItalicTitle );
  Storable::DataToStream( theStr, "myIsShadowTitle",  myIsShadowTitle );
  Storable::DataToStream( theStr, "myTitleColor[0]",  myTitleColor[0] );
  Storable::DataToStream( theStr, "myTitleColor[1]",  myTitleColor[1] );
  Storable::DataToStream( theStr, "myTitleColor[2]",  myTitleColor[2] );

  Storable::DataToStream( theStr, "myLblFontType",    myLblFontType );
  Storable::DataToStream( theStr, "myIsBoldLabel",    myIsBoldLabel );
  Storable::DataToStream( theStr, "myIsItalicLabel",  myIsItalicLabel );
  Storable::DataToStream( theStr, "myIsShadowLabel",  myIsShadowLabel );
  Storable::DataToStream( theStr, "myLabelColor[0]",  myLabelColor[0] );
  Storable::DataToStream( theStr, "myLabelColor[1]",  myLabelColor[1] );
  Storable::DataToStream( theStr, "myLabelColor[2]",  myLabelColor[2] );
  
  // Parameters of labels displayed field values

  Storable::DataToStream( theStr, "myValLblFontType", myValLblFontType );
  Storable::DataToStream( theStr, "myIsBoldValLbl", myIsBoldValLbl );
  Storable::DataToStream( theStr, "myIsItalicValLbl", myIsItalicValLbl );
  Storable::DataToStream( theStr, "myIsShadowValLbl", myIsShadowValLbl );
  Storable::DataToStream( theStr, "myValLblFontSize", myValLblFontSize );
  Storable::DataToStream( theStr, "myValLblFontColor[0]", myValLblFontColor[ 0 ] );
  Storable::DataToStream( theStr, "myValLblFontColor[1]", myValLblFontColor[ 1 ] );
  Storable::DataToStream( theStr, "myValLblFontColor[2]", myValLblFontColor[ 2 ] );


  std::ostringstream aGeomNameList;
  std::string aMeshName = GetCMeshName();
  const TGroupNames& aGroupNames = GetGroupNames();
  TGroupNames::const_iterator anIter = aGroupNames.begin();
  for(; anIter != aGroupNames.end(); anIter++){
    const std::string& aGroupName = *anIter;
    aGeomNameList<<aGroupName<<"|";   
  }
  Storable::DataToStream(theStr, "myGeomNameList",  aGeomNameList.str().c_str());
}

//----------------------------------------------------------------------------
const VISU::ColoredPrs3d_i
::TGroupNames&
VISU::ColoredPrs3d_i
::GetGroupNames()
{
  return myGroupNames;
}

//----------------------------------------------------------------------------
bool
VISU::ColoredPrs3d_i
::IsGroupsUsed()
{
  return !myGroupNames.empty();
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetMapScale(double theMapScale)
{
  GetSpecificPL()->SetMapScale(theMapScale);
}


//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(MYDEBUG) MESSAGE("ColoredPrs3d_i::CreatePipeLine() - "<<thePipeLine);
  myColoredPL = dynamic_cast<VISU_ColoredPL*>(thePipeLine);

  SetPipeLine(myColoredPL);
}

//----------------------------------------------------------------------------


VISU::Storable* 
VISU::ColoredPrs3d_i
::Build(EBuildMode theBuildMode)
{
  if(MYDEBUG)
    MESSAGE("ColoredPrs3d_i::Build - "<<myFieldName<<"; theBuildMode = "<<theBuildMode);
  SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
  bool anIsPublishInStudy = (myPublishInStudyMode == EPublishUnderTimeStamp || myPublishInStudyMode == EPublishIndependently);
  if(anIsPublishInStudy) 
    aStudyBuilder->NewCommand();  // There is a transaction
#ifndef _DEXCEPT_
  try{
#endif
    QString aComment;
    SetName("NoName", false);
    if(theBuildMode == ECreateNew || theBuildMode == ESameAs){
      if(!IsRangeFixed()) 
	SetSourceRange();
      if(theBuildMode == ECreateNew) 
	SetTitle(GetCFieldName().c_str());
    }
    if(myPublishInStudyMode == EPublishUnderTimeStamp){
      SetName(GenerateName().toLatin1().data(), false);
      VISU::Storable::TRestoringMap aRestoringMap;
      aRestoringMap["myComment"] = "TIMESTAMP";
      aRestoringMap["myMeshName"] = GetCMeshName().c_str();
      aRestoringMap["myEntityId"] = QString::number(GetEntity());
      aRestoringMap["myFieldName"] = GetCFieldName().c_str();
      aRestoringMap["myTimeStampId"] = QString::number(GetTimeStampNumber());
      aRestoringMap["myNumComponent"] = QString::number(GetScalarField()->myNbComp);
      std::string anEntry = GetCResult()->GetEntry(aRestoringMap);
      if(anEntry == "") 
	throw std::runtime_error("There is no Entry for binding the presentation !!!");
      aComment.sprintf("myComment=%s;myMeshName=%s;myEntityId=%d;myFieldName=%s;myTimeStampId=%d;myNumComponent=%d",
		       GetComment(),
		       GetCMeshName().c_str(),
		       GetEntity(),
		       GetCFieldName().c_str(),
		       GetTimeStampNumber(),
		       GetScalarField()->myNbComp);
      CORBA::String_var anIOR = GetID();
      CreateAttributes(GetStudyDocument(),
		       anEntry,
		       GetIconName(),
		       anIOR.in(),
		       GetName(),
		       "",
		       aComment.toLatin1().data(),
		       true);
    }else if(myPublishInStudyMode == EPublishIndependently){
      SetName(GenerateName().toLatin1().data(), false);
      CORBA::String_var anIOR = GetID();
      std::string aFatherEntry = ::FindOrCreate3DPresentationsFolder(GetStudyDocument());
      aComment.sprintf("myComment=%s",
		       GetComment());
      CreateAttributes(GetStudyDocument(),
		       aFatherEntry,
		       GetIconName(),
		       anIOR.in(),
		       GetName(),
		       "",
		       aComment.toLatin1().data(),
		       true);
    }
#ifndef _DEXCEPT_
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
    throw;
  }catch(...){
    INFOS("Unknown exception was occured!");
    throw;
  }
#endif

  if(anIsPublishInStudy) 
    aStudyBuilder->CommitCommand();
  return this;
}

//----------------------------------------------------------------------------
void
VISU::ColoredPrs3d_i
::SetIsDistributionVisible(CORBA::Boolean isVisible)
{
  if( myIsDistributionVisible != isVisible ){
    VISU::TSetModified aModified(this);
    myIsDistributionVisible = isVisible;
    myParamsTime.Modified();
  }
}

//----------------------------------------------------------------------------
CORBA::Boolean
VISU::ColoredPrs3d_i
::GetIsDistributionVisible()
{
  return myIsDistributionVisible;
}

//----------------------------------------------------------------------------


int
VISU::ColoredPrs3d_i
::GetValLblFontType() const
{
  return myValLblFontType;
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::SetValLblFontType( const int theType )
{
  if ( myValLblFontType == theType )
    return;

  VISU::TSetModified aModified( this );

  myValLblFontType = theType;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------

double
VISU::ColoredPrs3d_i
::GetValLblFontSize() const
{
  return myValLblFontSize;
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::SetValLblFontSize( const double theSize )
{
  if ( VISU::CheckIsSameValue( myValLblFontSize, theSize ) )
    return;

  VISU::TSetModified aModified( this );

  myValLblFontSize = theSize;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::GetValLblFontColor( double& theR, 
                      double& theG, 
                      double& theB ) const
{
  theR = myValLblFontColor[ 0 ];
  theG = myValLblFontColor[ 1 ];
  theB = myValLblFontColor[ 2 ];
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::SetValLblFontColor( const double theR, 
                      const double theG, 
                      const double theB )
{
  if ( VISU::CheckIsSameValue( myValLblFontColor[ 0 ], theR ) &&
       VISU::CheckIsSameValue( myValLblFontColor[ 1 ], theG ) &&
       VISU::CheckIsSameValue (myValLblFontColor[ 2 ], theB ) )
    return;

  VISU::TSetModified aModified(this);

  myValLblFontColor[ 0 ] = theR; 
  myValLblFontColor[ 1 ] = theG; 
  myValLblFontColor[ 2 ] = theB; 
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------

bool
VISU::ColoredPrs3d_i
::IsBoldValLbl() const
{
  return myIsBoldValLbl;
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::SetBoldValLbl( const bool theVal )
{
  if ( myIsBoldValLbl == theVal )
    return;

  VISU::TSetModified aModified( this );

  myIsBoldValLbl =  theVal;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------

bool
VISU::ColoredPrs3d_i
::IsItalicValLbl() const
{
  return myIsItalicValLbl;
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::SetItalicValLbl( const bool theVal )
{
  if ( myIsItalicValLbl == theVal )
    return;

  VISU::TSetModified aModified( this );

  myIsItalicValLbl =  theVal;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------

bool
VISU::ColoredPrs3d_i
::IsShadowValLbl() const
{
  return myIsShadowValLbl;
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::SetShadowValLbl( const bool theVal )
{
  if ( myIsShadowValLbl == theVal )
    return;

  VISU::TSetModified aModified( this );

  myIsShadowValLbl =  theVal;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------

void
VISU::ColoredPrs3d_i
::UpdateMapperLookupTable()
{
  ProcessVoidEvent(new TVoidMemFunEvent<VISU_ColoredPL>
    (GetSpecificPL(), &VISU_ColoredPL::UpdateMapperLookupTable));
}

//----------------------------------------------------------------------------^

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MonoColorPrs_i.cxx
//  Author : Vitaly Smetannikov
//  Module : VISU
//
#include "VISU_MonoColorPrs_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_ScalarMapAct.h"

#include "VISU_PipeLine.hxx"

#include <vtkDataSetMapper.h>
#include <vtkProperty.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//---------------------------------------------------------------
VISU::MonoColorPrs_i
::MonoColorPrs_i(EPublishInStudyMode thePublishInStudyMode):
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode)
{
  if(MYDEBUG) MESSAGE("MonoColorPrs_i::MonoColorPrs_i");
}

//---------------------------------------------------------------
void 
VISU::MonoColorPrs_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const MonoColorPrs_i* aPrs3d = dynamic_cast<const MonoColorPrs_i*>(theOrigin)){
    MonoColorPrs_i* anOrigin = const_cast<MonoColorPrs_i*>(aPrs3d);
    SetColor(anOrigin->GetColor());
    ShowColored(anOrigin->IsColored());
  }
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::MonoColorPrs_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  myIsColored = false;
  myColor.R = myColor.G = myColor.B = 0.5;
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}

//---------------------------------------------------------------
VISU::Storable* 
VISU::MonoColorPrs_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  myIsColored = VISU::Storable::FindValue(theMap,"myIsColored").toInt();
  myColor.R = VISU::Storable::FindValue(theMap,"myColor.R").toDouble();
  myColor.G = VISU::Storable::FindValue(theMap,"myColor.G").toDouble();
  myColor.B = VISU::Storable::FindValue(theMap,"myColor.B").toDouble();

  return this;
}

//---------------------------------------------------------------
void
VISU::MonoColorPrs_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myIsColored", myIsColored );
  Storable::DataToStream( theStr, "myColor.R", myColor.R );
  Storable::DataToStream( theStr, "myColor.G", myColor.G );
  Storable::DataToStream( theStr, "myColor.B", myColor.B );
}


//---------------------------------------------------------------
VISU::MonoColorPrs_i::~MonoColorPrs_i()
{
  if(MYDEBUG) MESSAGE("MonoColorPrs_i::~MonoColorPrs_i()");
}

//---------------------------------------------------------------
CORBA::Boolean VISU::MonoColorPrs_i::IsColored()
{
  return myIsColored;
}



//---------------------------------------------------------------
void
VISU::MonoColorPrs_i::ShowColored(CORBA::Boolean theColored)
{
  if(myIsColored == theColored)
    return;
  myIsColored = theColored; 
  myParamsTime.Modified();
}

//---------------------------------------------------------------
SALOMEDS::Color VISU::MonoColorPrs_i::GetColor()
{
  return myColor;
}

//---------------------------------------------------------------
void VISU::MonoColorPrs_i::SetColor(const SALOMEDS::Color& theColor)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myColor.R, theColor.R);
  anIsSameValue &= VISU::CheckIsSameValue(myColor.G, theColor.G);
  anIsSameValue &= VISU::CheckIsSameValue(myColor.B, theColor.B);
  if(anIsSameValue)
    return;

  VISU::TSetModified aModified(this);
  
  myColor = theColor;
  myParamsTime.Modified();
}

//---------------------------------------------------------------
VISU_Actor* VISU::MonoColorPrs_i::CreateActor(bool toSupressShrinking) 
{
  VISU_Actor* anActor = TSuperClass::CreateActor(toSupressShrinking);
  anActor->GetProperty()->SetColor(myColor.R,myColor.G,myColor.B);
  return anActor;
}

//---------------------------------------------------------------
VISU_Actor* VISU::MonoColorPrs_i::CreateActor()
{
  return CreateActor(false);
}

//---------------------------------------------------------------
void VISU::MonoColorPrs_i::UpdateActor(VISU_ActorBase* theActor) 
{
  if(VISU_ScalarMapAct* anActor = dynamic_cast<VISU_ScalarMapAct*>(theActor)){
    if(IsColored()){
      GetPipeLine()->GetMapper()->SetScalarVisibility(1);
    }else{
      GetPipeLine()->GetMapper()->SetScalarVisibility(0);
      anActor->GetProperty()->SetColor(myColor.R,myColor.G,myColor.B);
    }
    TSuperClass::UpdateActor(theActor);

    // this method should be called after TSuperClass::UpdateActor()
    anActor->SetBarVisibility( IsBarVisible() && IsColored() );
  }
}


// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_View_i.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_View_i.hh"
#include "VISU_Tools.h"

#include "VISU_Gen_i.hh"
#include "VISU_Prs3d_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_PointMap3d_i.hh"
#include "VISU_ScalarMap_i.hh"
#include "VISU_ViewManager_i.hh"
#include "VISU_TableDlg.h"

#include "VISU_Actor.h"
#include "VISU_ScalarMapAct.h"
#include "VISU_PointMap3dActor.h"
#include "VISU_DeformedGridPL.hxx"

#include "SALOME_Event.h"

#include "SUIT_ViewWindow.h"
#include "SUIT_ViewManager.h"
#include "SUIT_Tools.h"

#include "STD_MDIDesktop.h"
#include "STD_TabDesktop.h"

#include "SVTK_ViewWindow.h"
#include "SVTK_ViewModel.h"
#include "VTKViewer_Algorithm.h"

#include "SPlot2d_ViewModel.h"
#include "Plot2d_ViewFrame.h"

#include "SalomeApp_Application.h"
#include "SalomeApp_Study.h"

#include <QtxWorkstack.h>
#include <QtxWorkspace.h>

#include <QWorkspace>
#include <QImage>

#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkActor.h>

using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

namespace VISU {

  typedef TVoidMemFunEvent<SUIT_ViewWindow> TFrameActionEvent;

  struct TNewViewEvent: public SALOME_Event
  {
    SalomeApp_Application* myApplication;
    typedef QWidget* TResult;
    TResult myResult;

    TNewViewEvent (SalomeApp_Application* theApplication):
      myApplication(theApplication),
      myResult(NULL)
    {}

    virtual
    void
    Execute()
    {
      if (STD_MDIDesktop* aDesktop = dynamic_cast<STD_MDIDesktop*>(myApplication->desktop())) {
        myResult = aDesktop->workspace();
      }
    }
  };

  View_i::
  View_i (SalomeApp_Application *theApplication,
	  SUIT_ViewManager* theViewManager):
    myApplication(theApplication),
    myWorkspace(NULL),
    myViewManager(theViewManager)
  {
    if(MYDEBUG) MESSAGE("View_i::View_i - "<<this);
    myWorkspace = ProcessEvent(new TNewViewEvent(theApplication));
  }

  View_i::
  ~View_i()
  {
    if(MYDEBUG) MESSAGE("View_i::~View_i - "<<this);
  }


  struct TApplicationEvent: public SALOME_Event
  {
    SalomeApp_Application* myApplication;

    TApplicationEvent(SalomeApp_Application* theApplication):
      myApplication(theApplication)
    {}
  };

  void
  View_i::
  ShowPart (VISU::View::ViewRepresentation theViewRepresentation,
	    CORBA::Boolean theState)
  {
    struct TEvent: public TApplicationEvent
    {
      VISU::View::ViewRepresentation myViewRepresentation;
      CORBA::Boolean myState;

      TEvent(SalomeApp_Application* theApplication,
	     VISU::View::ViewRepresentation theViewRepresentation,
	     CORBA::Boolean theState):
	TApplicationEvent(theApplication),
	myViewRepresentation(theViewRepresentation),
	myState(theState)
      {}

      virtual
      void
      Execute()
      {
	switch(myViewRepresentation){
	case VISU::View::OBJECTBROWSER:
	  myApplication->getWindow( SalomeApp_Application::WT_ObjectBrowser )->setVisible( myState );
	  break;
	case VISU::View::PYTHON:
	  myApplication->getWindow( SalomeApp_Application::WT_PyConsole )->setVisible( myState );
	  break;
	case VISU::View::MESSAGES:
	  myApplication->getWindow( SalomeApp_Application::WT_LogWindow )->setVisible( myState );
	  break;
	case VISU::View::VIEWER:
	{
	  ViewManagerList mgrs = myApplication->viewManagers();
	  ViewManagerList::const_iterator anIt = mgrs.begin(), aLast = mgrs.end();
	  for( ; anIt!=aLast; anIt++ )
	  {
	    const QVector<SUIT_ViewWindow*>& views = (*anIt)->getViews();
	    for( int i=0; i<views.count(); i++ )
	      views[i]->setShown( myState );
	  }
	  break;
	}}
      }
    };

    ProcessVoidEvent(new TEvent(myApplication,theViewRepresentation,theState));
  }

  struct TPartShownEvent: public TApplicationEvent
  {
    VISU::View::ViewRepresentation myViewRepresentation;
    typedef bool TResult;
    TResult myResult;

    TPartShownEvent(SalomeApp_Application* theApplication,
		    VISU::View::ViewRepresentation theViewRepresentation):
      TApplicationEvent(theApplication),
      myViewRepresentation(theViewRepresentation),
      myResult(false)
    {}

    virtual
    void
    Execute()
    {
      switch(myViewRepresentation){
      case VISU::View::OBJECTBROWSER:
	myResult = myApplication->dockWindow( SalomeApp_Application::WT_ObjectBrowser )->isVisible();
	break;
      case VISU::View::PYTHON:
	myResult = myApplication->dockWindow( SalomeApp_Application::WT_PyConsole )->isVisible();
	break;
      case VISU::View::MESSAGES:
	myResult = myApplication->dockWindow( SalomeApp_Application::WT_LogWindow )->isVisible();
	break;
      case VISU::View::VIEWER:{
	ViewManagerList aViewManagerList = myApplication->viewManagers();
	ViewManagerList::const_iterator anIt = aViewManagerList.begin();
	for( ; anIt != aViewManagerList.end(); anIt++ )
	{
	  const QVector<SUIT_ViewWindow*>& views = (*anIt)->getViews();
	  for( int i=0; i<views.count(); i++ ) {
	    myResult = views[i]->isVisible();
	    return;
	  }
	}
	myResult = false;
	break;
      }}
    }
  };

  CORBA::Boolean
  View_i::
  IsPartShown (VISU::View::ViewRepresentation theViewRepresentation)
  {
    return ProcessEvent(new TPartShownEvent(myApplication,theViewRepresentation));
  }

  // Begin: New methods for view parameters management
  struct TSplitEvent: public SALOME_Event
  {
    SalomeApp_Application * myApplication;
    SUIT_ViewWindow       * myVW;
    Qt::Orientation         myOrientation;
    QtxWorkstack::SplitType mySplitType;

  public:
    TSplitEvent (SalomeApp_Application * theApplication,
		 SUIT_ViewWindow       * theViewWindow,
		 Qt::Orientation         theOrientation,
		 QtxWorkstack::SplitType theSplitType):
      myApplication(theApplication),
      myVW(theViewWindow),
      myOrientation(theOrientation),
      mySplitType(theSplitType)
    {}

    virtual void Execute()
    {
      SUIT_Desktop* desk = myApplication->desktop();
      STD_TabDesktop* tabDesk = dynamic_cast<STD_TabDesktop*>(desk);
      if (tabDesk) {
        QtxWorkstack* workstack = tabDesk->workstack();
        if (workstack) {
	  workstack->Split(myVW, myOrientation, mySplitType);
        }
      }
    }
  };

  void
  View_i::
  SplitRight()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSplitEvent(myApplication, aVW,
				       Qt::Horizontal, QtxWorkstack::SplitMove));
  }

  void
  View_i::
  SplitLeft()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSplitEvent(myApplication, aVW,
				       Qt::Horizontal, QtxWorkstack::SplitStay));
  }

  void
  View_i::
  SplitBottom()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSplitEvent(myApplication, aVW,
				       Qt::Vertical, QtxWorkstack::SplitMove));
  }

  void
  View_i::
  SplitTop()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSplitEvent(myApplication, aVW,
				       Qt::Vertical, QtxWorkstack::SplitStay));
  }

  void
  View_i::
  OnTop()
  {
    struct TOnTopEvent: public SALOME_Event
    {
      SUIT_ViewWindow * myVW;

    public:
      TOnTopEvent (SUIT_ViewWindow * theViewWindow):
        myVW(theViewWindow)
      {}

      virtual void Execute()
      {
	if (myVW) {
	  myVW->setFocus();
        }
      }
    };

    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TOnTopEvent(aVW));
  }

  struct TAttractEvent: public SALOME_Event
  {
    SalomeApp_Application * myApplication;
    SUIT_ViewWindow       * myVW1;
    SUIT_ViewWindow       * myVW2;
    bool                    myAttractAll;

  public:
    TAttractEvent (SalomeApp_Application * theApplication,
		   SUIT_ViewWindow       * theViewWindow1,
		   SUIT_ViewWindow       * theViewWindow2,
		   bool                    theAttractAll):
      myApplication(theApplication),
      myVW1(theViewWindow1),
      myVW2(theViewWindow2),
      myAttractAll(theAttractAll)
    {}

    virtual void Execute()
    {
      SUIT_Desktop* desk = myApplication->desktop();
      STD_TabDesktop* tabDesk = dynamic_cast<STD_TabDesktop*>(desk);
      if (tabDesk) {
        QtxWorkstack* workstack = tabDesk->workstack();
        if (workstack) {
          workstack->Attract(myVW1, myVW2, myAttractAll);
        }
      }
    }
  };

  void
  View_i::
  Attract (VISU::View_ptr theView)
  {
    if (!CORBA::is_nil(theView)) {
      if (VISU::View_i* pView = dynamic_cast<VISU::View_i*>(VISU::GetServant(theView).in())) {
	if (pView) {
	  SUIT_ViewWindow* aVW1 = GetViewWindow();
	  SUIT_ViewWindow* aVW2 = pView->GetViewWindow();
	  if (aVW1 && aVW2)
	    ProcessVoidEvent(new TAttractEvent(myApplication, aVW1, aVW2, false));
	}
      }
    }
  }

  void
  View_i::
  AttractAll (VISU::View_ptr theView)
  {
    if (!CORBA::is_nil(theView)) {
      if (VISU::View_i* pView = dynamic_cast<VISU::View_i*>(VISU::GetServant(theView).in())) {
	if (pView) {
	  SUIT_ViewWindow* aVW1 = GetViewWindow();
	  SUIT_ViewWindow* aVW2 = pView->GetViewWindow();
	  if (aVW1 && aVW2)
	    ProcessVoidEvent(new TAttractEvent(myApplication, aVW1, aVW2, true));
	}
      }
    }
  }

  struct TSizePositionEvent: public SALOME_Event
  {
    SalomeApp_Application * myApplication;
    SUIT_ViewWindow       * myVW;
    double                  myValue;
    bool                    myIsSize;

  public:
    TSizePositionEvent (SalomeApp_Application * theApplication,
			SUIT_ViewWindow       * theViewWindow,
			double                  theValue,
			bool                    theIsSize):
      myApplication(theApplication),
      myVW(theViewWindow),
      myValue(theValue),
      myIsSize(theIsSize)
    {}

    virtual void Execute()
    {
      MESSAGE("TSizePositionEvent::Execute()");
      SUIT_Desktop* desk = myApplication->desktop();
      STD_TabDesktop* tabDesk = dynamic_cast<STD_TabDesktop*>(desk);
      if (tabDesk) {
        QtxWorkstack* workstack = tabDesk->workstack();
        if (workstack) {
          if (myIsSize) {
            //jfa to do:workstack->SetRelativeSizeInSplitter(myVW, myValue);
          } else {
            workstack->SetRelativePositionInSplitter(myVW, myValue);
          }
        }
      }
    }
  };

  void
  View_i::
  SetRelativePositionInSplitter (CORBA::Double thePosition)
  {
    if (thePosition < 0.0 || 1.0 < thePosition) {
      return;
    }
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSizePositionEvent(myApplication, aVW, thePosition, false));
  }

  void
  View_i::
  SetRelativeSizeInSplitter (CORBA::Double theSize)
  {
    if (theSize < 0.0 || 1.0 < theSize) {
      return;
    }
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSizePositionEvent(myApplication, aVW, theSize, true));
  }

  struct TWSSizePositionEvent: public SALOME_Event
  {
    SalomeApp_Application * myApplication;
    SUIT_ViewWindow       * myVW;
    Qt::Orientation         myOrientation;
    double                  myValue;
    bool                    myIsSize;

  public:
    TWSSizePositionEvent (SalomeApp_Application * theApplication,
			  SUIT_ViewWindow       * theViewWindow,
			  Qt::Orientation         theOrientation,
			  double                  theValue,
			  bool                    theIsSize):
      myApplication(theApplication),
      myVW(theViewWindow),
      myOrientation(theOrientation),
      myValue(theValue),
      myIsSize(theIsSize)
    {}

    virtual void Execute()
    {
      MESSAGE("TWSSizePositionEvent::Execute()");
      SUIT_Desktop* desk = myApplication->desktop();
      STD_TabDesktop* tabDesk = dynamic_cast<STD_TabDesktop*>(desk);
      if (tabDesk) {
        QtxWorkstack* workstack = tabDesk->workstack();
        if (workstack) {
          if (myIsSize) {
            //jfa to do:workstack->SetRelativeSize(myVW, myOrientation, myValue);
          } else {
            workstack->SetRelativePosition(myVW, myOrientation, myValue);
          }
        }
      }
    }
  };

  void
  View_i::
  SetRelativePositionX (CORBA::Double thePosition)
  {
    if (thePosition < 0.0 || 1.0 < thePosition) {
      return;
    }
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TWSSizePositionEvent(myApplication, aVW,
						Qt::Horizontal, thePosition, false));
  }

  void
  View_i::
  SetRelativePositionY (CORBA::Double thePosition)
  {
    if (thePosition < 0.0 || 1.0 < thePosition) {
      return;
    }
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TWSSizePositionEvent(myApplication, aVW,
						Qt::Vertical, thePosition, false));
  }

  void
  View_i::
  SetRelativeSizeX (CORBA::Double theSize)
  {
    if (theSize < 0.0 || 1.0 < theSize) {
      return;
    }
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TWSSizePositionEvent(myApplication, aVW,
						Qt::Horizontal, theSize, true));
  }

  void
  View_i::
  SetRelativeSizeY (CORBA::Double theSize)
  {
    if (theSize < 0.0 || 1.0 < theSize) {
      return;
    }
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TWSSizePositionEvent(myApplication, aVW,
						Qt::Vertical, theSize, true));
  }
  // End: New methods for view parameters management

  // Begin: Old methods for view parameters management, they don't work now
  struct TSetViewSize: public SALOME_Event
  {
    SUIT_ViewWindow* myVW;
    typedef void (QRect::* TAction)(int);
    TAction myAction;
    CORBA::Long mySize;

  public:
    TSetViewSize(SUIT_ViewWindow* theViewWindow,
		 TAction theAction,
		 CORBA::Long theSize):
      myVW(theViewWindow),
      myAction(theAction),
      mySize(theSize)
    {}

    virtual
    void
    Execute()
    {
      if (QWidget* aParent = myVW->parentWidget()) {
	QRect aQRect = aParent->frameGeometry();
	(aQRect.*myAction)(mySize);
	aParent->setGeometry(aQRect);
      }
    }
  };

  void
  View_i::
  SetViewWidth(CORBA::Long theWidth)
  {
    ProcessVoidEvent(new TSetViewSize(myViewWindow,&QRect::setWidth,theWidth));
  }

  void
  View_i::
  SetViewHeight(CORBA::Long theHeight)
  {
    ProcessVoidEvent(new TSetViewSize(myViewWindow,&QRect::setHeight,theHeight));
  }


  struct TGetViewSize: public SALOME_Event
  {
    SUIT_ViewWindow* myVW;

    typedef int (QRect::* TAction)() const;
    TAction myAction;

    typedef CORBA::Long TResult;
    TResult myResult;

  public:
    TGetViewSize(SUIT_ViewWindow* theViewWindow,
		 TAction theAction):
      myVW(theViewWindow),
      myAction(theAction)
    {}

    virtual
    void
    Execute()
    {
      if(QWidget* aParent = myVW->parentWidget()){
	QRect aQRect = aParent->frameGeometry();
	myResult = (aQRect.*myAction)();
      }
    }
  };


  CORBA::Long
  View_i::
  GetViewWidth()
  {
    return ProcessEvent(new TGetViewSize(myViewWindow,&QRect::width));
  }

  CORBA::Long
  View_i::
  GetViewHeight()
  {
    return ProcessEvent(new TGetViewSize(myViewWindow,&QRect::height));
  }


  class TWorkspaceEvent: public SALOME_Event
  {
  protected:
    QWidget* myVW;
    QWidget* myWorkspace;

  public:
    TWorkspaceEvent(QWidget* theViewWindow,
		    QWidget* theWorkspace):
      myVW(theViewWindow),
      myWorkspace(theWorkspace)
    {}
  };


  class TAlignEvent: public TWorkspaceEvent
  {
  protected:
    Qt::AlignmentFlag myAligment;

  public:
    TAlignEvent(QWidget* theViewWindow,
		QWidget* theWorkspace,
		Qt::AlignmentFlag theAligment):
      TWorkspaceEvent(theViewWindow,theWorkspace),
      myAligment(theAligment)
    {}

    virtual
    void
    Execute()
    {
      if(QWidget* aParent = myVW->parentWidget())
	SUIT_Tools::alignWidget(aParent,myWorkspace,myAligment);
    }
  };


  void
  View_i::
  SetViewPositionHorizontal (VISU::View::ViewPosition theViewPosition)
  {
    if (myWorkspace) {
      switch (theViewPosition) {
      case VISU::View::LEFT:
	ProcessVoidEvent(new TAlignEvent(myViewWindow,myWorkspace,Qt::AlignLeft));
	break;
      case VISU::View::CENTER:
	ProcessVoidEvent(new TAlignEvent(myViewWindow,myWorkspace,Qt::AlignHCenter));
	break;
      case VISU::View::RIGHT:
	ProcessVoidEvent(new TAlignEvent(myViewWindow,myWorkspace,Qt::AlignRight));
	break;
      }
    }
  }


  void
  View_i::
  SetViewPositionVertical (VISU::View::ViewPosition theViewPosition)
  {
    if (myWorkspace) {
      switch (theViewPosition) {
      case VISU::View::TOP:
	ProcessVoidEvent(new TAlignEvent(myViewWindow,myWorkspace,Qt::AlignTop));
	break;
      case VISU::View::CENTER:
	ProcessVoidEvent(new TAlignEvent(myViewWindow,myWorkspace,Qt::AlignVCenter));
	break;
      case VISU::View::BOTTOM:
	ProcessVoidEvent(new TAlignEvent(myViewWindow,myWorkspace,Qt::AlignBottom));
	break;
      }
    }
  }


  class TXYEvent: public TWorkspaceEvent
  {
  protected:
    CORBA::Double myX, myY;

  public:
    TXYEvent(QWidget* theViewWindow,
	     QWidget* theWorkspace,
	     CORBA::Double theX,
	     CORBA::Double theY):
      TWorkspaceEvent(theViewWindow,theWorkspace),
      myX(theX),
      myY(theY)
    {}
  };


  void
  View_i::
  SetRelativePosition (CORBA::Double theX, CORBA::Double theY)
  {
    struct TEvent: public TXYEvent
    {
    public:
      TEvent(QWidget* theViewWindow,
	     QWidget* theWorkspace,
	     CORBA::Double theX,
	     CORBA::Double theY):
	TXYEvent(theViewWindow,theWorkspace,theX,theY)
      {}

      virtual
      void
      Execute()
      {
	if(QWidget* aParent = myVW->parentWidget())
	  aParent->move(int(myX*myWorkspace->width()),int(myY*myWorkspace->height()));
      }
    };

    if (myWorkspace)
      ProcessVoidEvent(new TEvent(myViewWindow,myWorkspace,theX,theY));
  }

  void
  View_i::
  SetRelativeSize (CORBA::Double theX, CORBA::Double theY)
  {
    struct TEvent: public TXYEvent
    {
    public:
      TEvent(QWidget* theViewWindow,
	     QWidget* theWorkspace,
	     CORBA::Double theX,
	     CORBA::Double theY):
	TXYEvent(theViewWindow,theWorkspace,theX,theY)
      {}

      virtual
      void
      Execute()
      {
	if(QWidget* aParent = myVW->parentWidget())
	  aParent->setGeometry(aParent->x(),
			       aParent->y(),
			       int(myX*myWorkspace->width()),
			       int(myY*myWorkspace->height()));
      }
    };

    if (myWorkspace)
      ProcessVoidEvent(new TEvent(myViewWindow,myWorkspace,theX,theY));
  }
  // End: Old methods for view parameters management, they don't work now

  void
  View_i::
  SetBackground (const SALOMEDS::Color& theColor)
  {
    struct TEvent: public SALOME_Event
    {
      SVTK_ViewWindow* myVW;
      SALOMEDS::Color myColor;
    public:
      TEvent(SVTK_ViewWindow* theViewWindow,
	     const SALOMEDS::Color& theColor):
	myVW(theViewWindow),
	myColor(theColor)
      {}
      virtual void Execute(){
	int aColor[3];
	aColor[0] = int(255.0*myColor.R);
	aColor[1] = int(255.0*myColor.G);
	aColor[2] = int(255.0*myColor.B);
	QColor aNewColor (aColor[0],aColor[1],aColor[2]);
	myVW->setBackgroundColor(aNewColor);
      }
    };
	
    SVTK_ViewWindow* aVW = dynamic_cast<SVTK_ViewWindow*>(GetViewWindow());
    if (aVW)
      ProcessVoidEvent(new TEvent(aVW, theColor));
  }


  struct TBackgroundEvent: public SALOME_Event
  {
    SVTK_ViewWindow* myVW;
    typedef SALOMEDS::Color TResult;
    TResult myResult;

    TBackgroundEvent(SVTK_ViewWindow* theViewWindow):
      myVW(theViewWindow)
    {}

    virtual
    void
    Execute()
    {
      double backint[3];
      GetRenderer(myVW)->GetBackground(backint);
      myResult.R = backint[0];
      myResult.G = backint[1];
      myResult.B = backint[2];
    }
  };

  SALOMEDS::Color
  View_i::
  GetBackground()
  {
    SVTK_ViewWindow* aVW = dynamic_cast<SVTK_ViewWindow*>(GetViewWindow());
    if (aVW)
      return ProcessEvent(new TBackgroundEvent(aVW));

    SALOMEDS::Color aBlack;
    aBlack.R = 0.0;
    aBlack.G = 0.0;
    aBlack.B = 0.0;
    return aBlack;
  }

  void
  View_i::
  Minimize()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TFrameActionEvent(aVW, &SUIT_ViewWindow::showMinimized));
  }

  void
  View_i::
  Restore()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TFrameActionEvent(aVW, &SUIT_ViewWindow::showNormal));
  }

  void
  View_i::
  Maximize()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TFrameActionEvent(aVW, &SUIT_ViewWindow::showMaximized));
  }

  void
  View_i::
  EraseAll()
  {}

  void
  View_i::
  DisplayAll()
  {}

  void
  View_i::
  Erase (PrsObject_ptr thePrsObj)
  {}

  void
  View_i::
  Display (PrsObject_ptr thePrsObj)
  {}

  void
  View_i::
  DisplayOnly (PrsObject_ptr thePrsObj)
  {}

  void
  View_i::
  Update()
  {}

  class TSavePictureEvent: public SALOME_Event
  {
    SUIT_ViewWindow * myVW;
    const char      * myFileName;
  public:
    typedef CORBA::Boolean TResult;
    TResult myResult;
    TSavePictureEvent (SUIT_ViewWindow * theViewWindow, const char * theFileName)
      : myVW(theViewWindow),
        myFileName(theFileName),
        myResult(false)
    {
    }

    virtual void Execute()
    {
      if (myVW) {
        QImage img = myVW->dumpView();
	if (!img.isNull()) {
          QString fileName (myFileName);
          if (!fileName.isEmpty()) {
            QString fmt = SUIT_Tools::extension(fileName).toUpper();
	    if (fmt.isEmpty()) fmt = QString("BMP"); // default format
	    if (fmt == "JPG" ) fmt = "JPEG";
	    //QApplication::setOverrideCursor(Qt::waitCursor);
	    img.save(fileName, fmt.toLatin1().data());
	    myResult = true;
	    //QApplication::restoreOverrideCursor();
          }
        }
      }
    }
  };

  CORBA::Boolean
  View_i::SavePicture(const char* theFileName)
  {
    return ProcessEvent(new TSavePictureEvent (GetViewWindow(), theFileName));
  }

  const char*
  View_i::GetComment() const
  {
    return "";
  }

  void
  View_i::
  ToStream (std::ostringstream& theStr)
  {
  }

  SUIT_ViewWindow* View_i::GetViewWindow()
  {
    if (myViewWindow) {
      QVector<SUIT_ViewWindow*> aViews = myViewManager->getViews();
      int aPos = aViews.indexOf(myViewWindow);
      if (aPos < 0)
	myViewWindow = NULL;
    }
    return myViewWindow;
  }

  void View_i::SetViewWindow(SUIT_ViewWindow* theViewWindow)
  {
    myViewWindow = theViewWindow;
  }


  //===========================================================================
  XYPlot_i::XYPlot_i (SalomeApp_Application* theApplication)
    : View_i(theApplication, NULL)
  {
    if (MYDEBUG) MESSAGE("XYPlot_i::XYPlot_i");
  }

  struct TCreatePlot2dViewEvent: public SALOME_Event
  {
    XYPlot_i * myPlot2dView;
    int        myNew;

    typedef Plot2d_ViewFrame* TResult;
    TResult myResult;

    TCreatePlot2dViewEvent (XYPlot_i * thePlot2dView,
			    const int  theNew):
      myPlot2dView(thePlot2dView),
      myNew(theNew),
      myResult(NULL)
    {}

    virtual
    void
    Execute()
    {
      if (!myPlot2dView->myApplication)
	return;

      SUIT_ViewManager* aViewMgr =
	myPlot2dView->myApplication->getViewManager(SPlot2d_Viewer::Type(), false);
      SUIT_ViewWindow* aVW = NULL;

      if (aViewMgr) {
	if (myNew)
	  aVW = aViewMgr->createViewWindow();
	else
	  aVW = aViewMgr->getActiveView();
      } else {
	if (myNew) {
	  aViewMgr = myPlot2dView->myApplication->getViewManager(SPlot2d_Viewer::Type(), true);
	  aVW = aViewMgr->getActiveView();
	} else {
	  return; // there are no current Plot2d view
	}
      }

      if (aVW) {
	Plot2d_ViewWindow* aPlot2dVW = dynamic_cast<Plot2d_ViewWindow*>(aVW);
	if (aPlot2dVW) {
	  myResult = aPlot2dVW->getViewFrame();
	  if (myResult) {
	    myResult->Repaint();
	  }

	  myPlot2dView->myViewManager = aViewMgr;
	  myPlot2dView->SetViewWindow(aVW);
	}
      }
    }
  };

  Storable* XYPlot_i::Create (int theNew)
  {
    myView = ProcessEvent(new TCreatePlot2dViewEvent(this, theNew));
    if (myView)
      return this;
    return NULL;
  }

  void XYPlot_i::Update()
  {
    ProcessVoidEvent(new TVoidMemFunEvent<Plot2d_ViewFrame>(myView,&Plot2d_ViewFrame::Repaint));
  }

  struct TCloseViewEvent: public SALOME_Event
  {
    SUIT_ViewWindow* myView;
    TCloseViewEvent (SUIT_ViewWindow* theView):
      myView(theView)
    {}
    
    virtual void Execute()
    {
      if ( myView ) myView->close();
    }
  };

  void XYPlot_i::Close()
  {
    ProcessVoidEvent(new TCloseViewEvent(GetViewWindow()));
    QApplication::sendPostedEvents();
  }

  XYPlot_i::~XYPlot_i() {
    if(MYDEBUG) MESSAGE("XYPlot_i::~XYPlot_i");
  }

  void XYPlot_i::SetTitle (const char* theTitle)
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TVoidMemFun1ArgEvent<SUIT_ViewWindow,const QString&,QString>
		       (aVW, &SUIT_ViewWindow::setWindowTitle, QString(theTitle)));
  }
  char* XYPlot_i::GetTitle()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      return CORBA::string_dup( aVW->windowTitle().toLatin1().data());
    return CORBA::string_dup("");
  }

  void XYPlot_i::SetSubTitle (const char* theTitle)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TVoidMemFun1ArgEvent<Plot2d_ViewFrame,const QString&,QString>
		       (myView, &Plot2d_ViewFrame::setTitle, QString(theTitle)));
  }

  char* XYPlot_i::GetSubTitle()
  {
    if (GetViewWindow())
      return CORBA::string_dup(myView->getTitle().toLatin1().data());
    return CORBA::string_dup("");
  }

  void XYPlot_i::SetCurveType (VISU::XYPlot::CurveType theType)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TVoidMemFun2ArgEvent<Plot2d_ViewFrame,int,bool>
		       (myView,&Plot2d_ViewFrame::setCurveType,theType,true));
  }

  VISU::XYPlot::CurveType XYPlot_i::GetCurveType()
  {
    if (GetViewWindow())
      return (VISU::XYPlot::CurveType)myView->getCurveType();
    return VISU::XYPlot::POINTS;
  }

  void XYPlot_i::SetMarkerSize (CORBA::Long theSize)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TVoidMemFun2ArgEvent<Plot2d_ViewFrame,int,bool>
		       (myView,&Plot2d_ViewFrame::setMarkerSize,theSize,true));
  }

  CORBA::Long XYPlot_i::GetMarkerSize()
  {
    if (GetViewWindow())
      return myView->getMarkerSize();
    return -1;
  }

  void XYPlot_i::EnableXGrid (CORBA::Boolean theMajor, CORBA::Long theNumMajor,
			      CORBA::Boolean theMinor, CORBA::Long theNumMinor)
  {
    class TEvent: public SALOME_Event
    {
    public:
      TEvent (Plot2d_ViewFrame* theView, 
	      CORBA::Boolean theMajor, 
	      CORBA::Long theNumMajor,
	      CORBA::Boolean theMinor, 
	      CORBA::Long theNumMinor):
	myView(theView),
	myMajor(theMajor), 
	myNumMajor(theNumMajor),
	myMinor(theMinor), 
	myNumMinor(theNumMinor)
      {}
      
      virtual void Execute()
      {
	myView->setXGrid( myMajor, myNumMajor, myMinor, myNumMinor );
      }
    protected:
      Plot2d_ViewFrame* myView;
      CORBA::Boolean myMajor, myMinor;
      CORBA::Long myNumMajor, myNumMinor;
    };

    if ( GetViewWindow() )
      ProcessVoidEvent( new TEvent( myView, theMajor, theNumMajor, theMinor, theNumMinor ) );
  }
  void XYPlot_i::EnableYGrid(CORBA::Boolean theMajor, 
			     CORBA::Long theNumMajor,
			     CORBA::Boolean theMinor, 
			     CORBA::Long theNumMinor)
  {
    this->EnableYGrid( theMajor, theNumMajor, theMinor, theNumMinor, false, 0, false, 0 );
  }

  void XYPlot_i::EnableYGrid(CORBA::Boolean theMajor,
			     CORBA::Long theNumMajor,
			     CORBA::Boolean theMinor, 
			     CORBA::Long theNumMinor,
			     CORBA::Boolean the2Major, 
			     CORBA::Long the2NumMajor,
			     CORBA::Boolean the2Minor, 
			     CORBA::Long the2NumMinor)
  {
    class TEvent: public SALOME_Event
    {
    public:
      TEvent (Plot2d_ViewFrame* theView, 
	      CORBA::Boolean theMajor,
	      CORBA::Long theNumMajor,
	      CORBA::Boolean theMinor, 
	      CORBA::Long theNumMinor,
	      CORBA::Boolean the2Major, 
	      CORBA::Long the2NumMajor,
	      CORBA::Boolean the2Minor, 
	      CORBA::Long the2NumMinor):
	myView(theView),
	myMajor(theMajor), 
	myNumMajor(theNumMajor),
	myMinor(theMinor), 
	myNumMinor(theNumMinor),
	my2Major(the2Major), 
	my2NumMajor(the2NumMajor),
	my2Minor(the2Minor), 
	my2NumMinor(the2NumMinor)
      {}
      
      virtual void Execute()
      {
	myView->setYGrid( myMajor, myNumMajor, myMinor, myNumMinor,
			  my2Major, my2NumMajor, my2Minor, my2NumMinor);
      }
    protected:
      Plot2d_ViewFrame* myView;
      CORBA::Boolean myMajor, myMinor, my2Major, my2Minor;
      CORBA::Long myNumMajor, myNumMinor, my2NumMajor, my2NumMinor;
    };

    if (GetViewWindow())
      ProcessVoidEvent( new TEvent( myView, theMajor, theNumMajor, theMinor, theNumMinor, 
				    the2Major, the2NumMajor, the2Minor, the2NumMinor ) );
  }
  
  class TSetScaleModeEvent: public SALOME_Event
  {
  public:
    typedef void (Plot2d_ViewFrame::* TFun)(const int, bool);
    TSetScaleModeEvent (Plot2d_ViewFrame* theView, TFun theFun, int theScaling):
      myView(theView), myFun(theFun), myScaling(theScaling)
    {}

    virtual void Execute()
    {
      (myView->*myFun)(myScaling,true);
    }
  protected:
    Plot2d_ViewFrame* myView;
    TFun myFun;
    int myScaling;
  };

  void XYPlot_i::SetHorScaling (VISU::Scaling theScaling)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TSetScaleModeEvent(myView,&Plot2d_ViewFrame::setHorScaleMode,
					      theScaling == VISU::LOGARITHMIC));
  }

  VISU::Scaling XYPlot_i::GetHorScaling()
  {
    //jfa: may be need to be enclosed in SALOME_Event?
    if (GetViewWindow())
      return (VISU::Scaling)myView->getHorScaleMode();
    return VISU::LINEAR;
  }

  void XYPlot_i::SetVerScaling(VISU::Scaling theScaling)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TSetScaleModeEvent(myView,&Plot2d_ViewFrame::setVerScaleMode,
					      theScaling == VISU::LOGARITHMIC));
  }

  VISU::Scaling XYPlot_i::GetVerScaling()
  {
    if (GetViewWindow())
      return (VISU::Scaling)myView->getVerScaleMode();
    return VISU::LINEAR;
  }

  class TSetTitleEvent: public SALOME_Event
  {
  public:
    TSetTitleEvent (Plot2d_ViewFrame* theView, Plot2d_ViewFrame::ObjectType theType, const char* theTitle):
      myView(theView), myType(theType), myTitle(theTitle)
    {}
    virtual void Execute()
    {
      myView->setTitle(true,myTitle,myType);
    }
  protected:
    Plot2d_ViewFrame* myView;
    Plot2d_ViewFrame::ObjectType myType;
    const char* myTitle;
  };

  void XYPlot_i::SetXTitle (const char* theTitle)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TSetTitleEvent(myView, Plot2d_ViewFrame::XTitle, theTitle));
  }
  char* XYPlot_i::GetXTitle()
  {
    if (GetViewWindow())
      return CORBA::string_dup(myView->getTitle(Plot2d_ViewFrame::XTitle).toLatin1().data());
    return CORBA::string_dup("");
  }

  void XYPlot_i::SetYTitle (const char* theTitle)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TSetTitleEvent(myView, Plot2d_ViewFrame::YTitle, theTitle));
  }
  char* XYPlot_i::GetYTitle()
  {
    if (GetViewWindow())
      return CORBA::string_dup(myView->getTitle(Plot2d_ViewFrame::YTitle).toLatin1().data());
    return CORBA::string_dup("");
  }

  void XYPlot_i::ShowLegend (CORBA::Boolean theShowing)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TVoidMemFun2ArgEvent<Plot2d_ViewFrame,bool,bool>
		       (myView,&Plot2d_ViewFrame::showLegend,theShowing,true));
  }

  class TXYPlotViewEvent: public SALOME_Event
  {
    SalomeApp_Application* myApplication;
    Plot2d_ViewFrame*      myView;
    PrsObject_ptr          myPrsObj;
    int                    myDisplaing;

  public:
    TXYPlotViewEvent(SalomeApp_Application* theApplication,
		     Plot2d_ViewFrame*      theView,
		     PrsObject_ptr          thePrsObj,
		     int                    theDisplaing)
      : myApplication(theApplication),
      myView(theView),
      myPrsObj(thePrsObj),
      myDisplaing(theDisplaing)
    {
    }

    virtual void Execute()
    {
      Qtx::VisibilityState state = (myDisplaing == eDisplay ||myDisplaing == eDisplayOnly) ?
	Qtx::ShownState : Qtx::HiddenState;
      // is it Curve ?
      if (Curve_i* aCurve = dynamic_cast<Curve_i*>(VISU::GetServant(myPrsObj).in())) {
	UpdatePlot2d(aCurve,myDisplaing,myView);
	VISU::SetVisibilityState(aCurve->GetEntry(),state);
	VISU::CurveVisibilityChanged(aCurve, myDisplaing , false, true, true);
      }
      // is it Container ?
      if (Container_i* aContainer = dynamic_cast<Container_i*>(VISU::GetServant(myPrsObj).in())) {
	VISU::SetVisibilityState(aContainer->GetEntry(),state);
	int nbCurves = aContainer->GetNbCurves();
	for ( int i = 1; i <= nbCurves; i++ ) {
	  VISU::Curve_i* aCurve = aContainer->GetCurve( i );
	  if ( aCurve && aCurve->IsValid() ) {
	    UpdatePlot2d(aCurve,myDisplaing,myView);
	    VISU::CurveVisibilityChanged(aCurve, myDisplaing , true, true, true);
	  }
	}
	myView->Repaint();
      }
      // is it Table ?
      if (Table_i* aTable = dynamic_cast<Table_i*>(VISU::GetServant(myPrsObj).in())) {
	_PTR(Study) aCStudy;
        if (SUIT_Study* aSStudy = myApplication->activeStudy()) {
          if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
            aCStudy = aStudy->studyDS();
          }
        }
        if (!aCStudy) return;
	_PTR(SObject) TableSO = aCStudy->FindObjectID(aTable->GetEntry());
	if (TableSO) {
	  _PTR(ChildIterator) Iter = aCStudy->NewChildIterator(TableSO);
	  for (; Iter->More(); Iter->Next()) {
	    CORBA::Object_var childObject = VISU::ClientSObjectToObject(Iter->Value());
	    if (!CORBA::is_nil(childObject)) {
	      CORBA::Object_ptr aCurve = VISU::Curve::_narrow(childObject);
	      if (!CORBA::is_nil(aCurve)) {
		Curve_i* crv = dynamic_cast<VISU::Curve_i*>(VISU::GetServant(aCurve).in());
		UpdatePlot2d(crv, myDisplaing, myView);
		VISU::CurveVisibilityChanged(crv, myDisplaing , true, false, true);
	      }
	    }
	  }
	  myView->Repaint();
	  VISU::SetVisibilityState(aTable->GetEntry(),state);
	}
      }
    }
  };

  void XYPlot_i::Display (PrsObject_ptr thePrsObj)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TXYPlotViewEvent (myApplication,myView,thePrsObj,eDisplay));
  }

  void XYPlot_i::Erase (PrsObject_ptr thePrsObj)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TXYPlotViewEvent (myApplication,myView,thePrsObj,eErase));
  }

  void XYPlot_i::DisplayOnly (PrsObject_ptr thePrsObj)
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TXYPlotViewEvent (myApplication,myView,thePrsObj,eDisplayOnly));
  }

  void XYPlot_i::EraseAll()
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TVoidMemFunEvent<Plot2d_ViewFrame> (myView,&Plot2d_ViewFrame::EraseAll));
  }

  void XYPlot_i::FitAll()
  {
    if (GetViewWindow())
      ProcessVoidEvent(new TVoidMemFunEvent<Plot2d_ViewFrame> (myView,&Plot2d_ViewFrame::fitAll));
  }

  class TFitRangeEvent: public SALOME_Event
  {
  public:
    TFitRangeEvent (Plot2d_ViewFrame* theView,
		    const int mode,
		    const double xMin, const double xMax,
		    const double yMin, const double yMax):
      myView(theView), 
      myMode(mode),
      myXMin(xMin),myXMax(xMax),
      myYMin(yMin),myYMax(yMax)
    {}
    virtual void Execute()
    {
      myView->fitData(myMode,myXMin,myXMax,myYMin,myYMax);
    }
  protected:
    const int myMode;
    const double myXMin;
    const double myXMax;
    const double myYMin;
    const double myYMax;
    Plot2d_ViewFrame* myView;
  };
  
  void XYPlot_i::FitXRange(const CORBA::Double xMin,const CORBA::Double xMax)
  {
    const CORBA::Long mode = 1;
    const CORBA::Double yMin = 0.0;
    const CORBA::Double yMax = 0.0;
    ProcessVoidEvent(new TFitRangeEvent(myView,mode,xMin,xMax,yMin,yMax));
  }

  void XYPlot_i::FitYRange(const CORBA::Double yMin,const CORBA::Double yMax)
  {
    const CORBA::Long mode = 2;
    const CORBA::Double xMin = 0.0;
    const CORBA::Double xMax = 0.0;
    ProcessVoidEvent(new TFitRangeEvent(myView,mode,xMin,xMax,yMin,yMax));
  }

  void XYPlot_i::FitRange(const CORBA::Double xMin,const CORBA::Double xMax,
			  const CORBA::Double yMin,const CORBA::Double yMax)
  {
    const CORBA::Long mode = 0;
    ProcessVoidEvent(new TFitRangeEvent(myView,mode,xMin,xMax,yMin,yMax));
  }
  
  void XYPlot_i::GetFitRanges(double& xMin, double& xMax, double& yMin, double& yMax)
  {
    double y2Min,y2Max;
    myView->getFitRanges(xMin,xMax,yMin,yMax,y2Min,y2Max);
  }

  //===========================================================================
  TableView_i::TableView_i (SalomeApp_Application* theApplication)
    : View_i(theApplication, NULL)
  {
  }

  struct TCreateTableViewEvent: public SALOME_Event
  {
    SalomeApp_Application* myApplication;
    VISU::Table_i*         myTable;

    typedef VISU_TableDlg* TResult;
    TResult myResult;

    TCreateTableViewEvent (SalomeApp_Application* theApplication,
			   VISU::Table_i*         theTable):
      myApplication(theApplication),
      myTable(theTable),
      myResult(NULL)
    {}

    virtual
    void
    Execute()
    {
      if (myTable != NULL) {
	if (SUIT_Study* aSStudy = myApplication->activeStudy()) {
	  if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
	    if (_PTR(Study) aCStudy = aStudy->studyDS()) {
              myResult = new VISU_TableDlg (myApplication->desktop(),
                                            aCStudy,
                                            myTable,
                                            false,
                                            VISU_TableDlg::ttAuto,
                                            Qt::Vertical);
              myResult->show();
	    }
	  }
	}
      }
    }
  };

  Storable* TableView_i::Create (VISU::Table_ptr theTable)
  {
    if (MYDEBUG) MESSAGE("TableView_i::Create - " << (!theTable->_is_nil()));
    if (!theTable->_is_nil()) {
      VISU::Table_i* table =
        dynamic_cast<VISU::Table_i*>(VISU::GetServant(theTable).in());
      if (MYDEBUG) MESSAGE("TableView_i::Create - dynamic_cast = " << table);

      myView = ProcessEvent(new TCreateTableViewEvent(myApplication, table));
      if (myView)
        return this;
    }
    return NULL;
  }

  TableView_i::~TableView_i()
  {
    if(MYDEBUG) MESSAGE("TableView_i::~TableView_i");
    delete myView;
  }

  void TableView_i::SetTitle (const char* theTitle)
  {
    ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_TableDlg,const QString&,QString>
                     (myView, &VISU_TableDlg::setWindowTitle, QString(theTitle)));
  }

  char* TableView_i::GetTitle()
  {
    //jfa: may be need to be enclosed in SALOME_Event?
    return CORBA::string_dup(myView->windowTitle().toLatin1().data());
  }

  void TableView_i::Close()
  {
    //jfa: may be need to be enclosed in SALOME_Event?
    myView->close();
  }


  //===========================================================================
  int View3D_i::myNbViewParams = 0;
  const string View3D_i::myComment = "VIEW3D";
  const char* View3D_i::GetComment() const { return myComment.c_str();}

  QString View3D_i::GenerateViewParamsName()
  {
    return VISU::GenerateName("ViewParams", ++myNbViewParams);
  }

  View3D_i::View3D_i (SalomeApp_Application* theApplication)
    : View_i(theApplication, NULL)
  {
    if (MYDEBUG) MESSAGE("View3D_i::View3D_i");
  }

  struct TCreateView3dEvent: public SALOME_Event
  {
    SalomeApp_Application *myApplication;
    View3D_i * myView3D;
    int        myNew;

    typedef SUIT_ViewWindow* TResult;
    TResult myResult;

    TCreateView3dEvent (SalomeApp_Application *theApplication,
			View3D_i * theView3D,
			const int  theNew):
      myApplication(theApplication),
      myView3D(theView3D),
      myNew(theNew),
      myResult(NULL)
    {}

    virtual
    void
    Execute()
    {
      if (!myApplication)
	return;

      if(myNew){
	if(SUIT_ViewManager* aViewManager = myApplication->getViewManager(SVTK_Viewer::Type(),false)){
	  myResult = aViewManager->createViewWindow();
	  myView3D->myViewManager = aViewManager;
	}else{
	  if(SUIT_ViewManager* aViewManager = myApplication->getViewManager(SVTK_Viewer::Type(),true)){
	    myResult = aViewManager->getActiveView();
	    myView3D->myViewManager = aViewManager;
	  }
	}
      }else{
	if(SUIT_ViewManager* aViewManager = myApplication->activeViewManager()){
	  if(SUIT_ViewWindow* aView = aViewManager->getActiveView()){
	    if(dynamic_cast<SVTK_ViewWindow*>(aView)){
	      myView3D->myViewManager = aViewManager;
	      myResult = aViewManager->getActiveView();
	    }
	  }
	}else{
	  if(SUIT_ViewManager* aViewManager = myApplication->getViewManager(SVTK_Viewer::Type(),false)){
	    myView3D->myViewManager = aViewManager;
	    myResult = aViewManager->getActiveView();
	  }
	}
      }
    }
  };

  Storable* View3D_i::Create (int theNew)
  {
    if (MYDEBUG) MESSAGE("View3D_i::Create");
    SUIT_ViewWindow* aVW = ProcessEvent(new TCreateView3dEvent(myApplication, this, theNew));
    if (aVW) {
      SetViewWindow(aVW);
      return this;
    }
    return NULL;
  }

  void View3D_i::Update()
  {
    class TEvent: public SALOME_Event
    {
      SUIT_ViewWindow* myViewWindow;
    public:
      TEvent(SUIT_ViewWindow* theViewWindow):
	myViewWindow(theViewWindow)
      {}
      virtual void Execute()
      {
	SVTK_ViewWindow* aViewWindow = dynamic_cast<SVTK_ViewWindow*>(myViewWindow);
	vtkRenderer* aRenderer = aViewWindow->getRenderer();
	VTK::ActorCollectionCopy aCopy(aRenderer->GetActors());
	vtkActorCollection* anActors = aCopy.GetActors();
	anActors->InitTraversal();
	while (vtkActor *anAct = anActors->GetNextActor()) {
	  if (VISU_ActorBase* anActor = dynamic_cast<VISU_ActorBase*>(anAct)) {
	    if (anActor->GetVisibility()) 
	      anActor->UpdateFromFactory();
	  }
	}
	RepaintView(myViewWindow);
      }
    };

    if (SUIT_ViewWindow* aViewWindow = GetViewWindow())
      ProcessVoidEvent(new TEvent(aViewWindow));
  }

  bool View3D_i::SaveViewParams (SUIT_ViewManager* theViewManager, const std::string& theName)
  {
    MESSAGE("View3D_i::SaveViewParams()");

    if (!theViewManager || theName == "")
      return false;

    _PTR(Study) aCStudy;
    if (SUIT_Study* aSStudy = theViewManager->study()) {
      if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
        aCStudy = aStudy->studyDS();
      }
    }
    if (!aCStudy)
      return false;

    if (theName != "") {
      std::vector<_PTR(SObject)> aList = aCStudy->FindObjectByName(theName.c_str(), "VISU");
      _PTR(GenericAttribute) anAttr;
      int iEnd = aList.size();
      for (int i = 0; i < iEnd; i++) {
	_PTR(SObject) anObj = aList[i];
	string anEntry = anObj->GetID();
	if(MYDEBUG) MESSAGE("View3D_i::SaveViewParams - anEntry = " << anEntry);
	if (anObj->FindAttribute(anAttr, "AttributeString")) {
	  _PTR(AttributeString) aCmnt (anAttr);
	  string aComm (aCmnt->Value());
	  if (MYDEBUG) MESSAGE("View3D_i::SaveViewPoint - aComm = " << aComm);
	  if (aComm.compare(View3D_i::myComment) >= 0) {
	    aCmnt->SetValue(ToString(theViewManager->getActiveView()).c_str());
	    return true;
	  }
	}
      }
    }
    _PTR(SComponent) aSComponent = ClientFindOrCreateVisuComponent(aCStudy);
    string aSComponentEntry = aSComponent->GetID();
    string anEntry = CreateAttributes(aCStudy, 
				      aSComponentEntry.c_str(), 
				      "", 
				      "", 
				      theName, 
				      "",
				      ToString(theViewManager->getActiveView()).c_str());
    return true;
  }

  class TSaveViewParamsEvent: public SALOME_Event
  {
    SUIT_ViewManager* myViewMgr;
    std::string       myName;
  public:
    TSaveViewParamsEvent (SUIT_ViewManager* theViewManager,
			  const std::string& theName):
      myViewMgr(theViewManager),
      myName(theName)
    {}

    virtual void Execute()
    {
      myResult = View3D_i::SaveViewParams(myViewMgr, myName);
    }
    typedef CORBA::Boolean TResult;
    TResult myResult;
  };

  CORBA::Boolean View3D_i::SaveViewParams (const char* theViewParamsName)
  {
    return ProcessEvent(new TSaveViewParamsEvent (myViewManager, theViewParamsName));
  }

  bool View3D_i::RestoreViewParams (SUIT_ViewManager* theViewManager, const std::string& theName)
  {
    _PTR(Study) aCStudy;
    if (SUIT_Study* aSStudy = theViewManager->study()) {
      if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
        aCStudy = aStudy->studyDS();
      }
    }
    if (!aCStudy)
      return false;

    std::vector<_PTR(SObject)> aList = aCStudy->FindObjectByName(theName, "VISU");
    _PTR(GenericAttribute) anAttr;
    int iEnd = aList.size();
    if (MYDEBUG) MESSAGE("View3D_i::RestoreViewPoint - iEnd = " << iEnd);
    for (int i = 0; i < iEnd; i++) {
      _PTR(SObject) anObj = aList[i];
      string anEntry = anObj->GetID();
      if (MYDEBUG) MESSAGE("View3D_i::RestoreViewPoint - anEntry = " << anEntry);
      Storable::TRestoringMap aMap = Storable::GetStorableMap(anObj);
      if (Storable::FindValue(aMap, "myComment") == View3D_i::myComment.c_str()) {
	Restore(theViewManager->getActiveView(), aMap);
	return true;
      }
    }
    return false;
  }

  class TRestoreViewParamsEvent: public SALOME_Event
  {
    SUIT_ViewManager* myViewMgr;
    std::string       myName;
  public:
    TRestoreViewParamsEvent (SUIT_ViewManager* theViewManager,
			     const std::string& theName):
      myViewMgr(theViewManager),
      myName(theName)
    {}

    virtual void Execute()
    {
      myResult = View3D_i::RestoreViewParams(myViewMgr, myName);
    }
    typedef CORBA::Boolean TResult;
    TResult myResult;
  };

  CORBA::Boolean View3D_i::RestoreViewParams (const char* theViewParamsName)
  {
    return ProcessEvent(new TRestoreViewParamsEvent (myViewManager, theViewParamsName));
  }

  void View3D_i::Restore (SUIT_ViewWindow* theViewWindow,
			  const Storable::TRestoringMap& theMap)
  {
    SALOMEDS::Color aColor;
    aColor.R = VISU::Storable::FindValue(theMap,"myColor.R").toDouble();
    aColor.G = VISU::Storable::FindValue(theMap,"myColor.G").toDouble();
    aColor.B = VISU::Storable::FindValue(theMap,"myColor.B").toDouble();

    double aPosition[3];
    aPosition[0] = VISU::Storable::FindValue(theMap,"myPosition[0]").toDouble();
    aPosition[1] = VISU::Storable::FindValue(theMap,"myPosition[1]").toDouble();
    aPosition[2] = VISU::Storable::FindValue(theMap,"myPosition[2]").toDouble();

    double aFocalPnt[3];
    aFocalPnt[0] = VISU::Storable::FindValue(theMap,"myFocalPnt[0]").toDouble();
    aFocalPnt[1] = VISU::Storable::FindValue(theMap,"myFocalPnt[1]").toDouble();
    aFocalPnt[2] = VISU::Storable::FindValue(theMap,"myFocalPnt[2]").toDouble();

    double aViewUp[3];
    aViewUp[0] = VISU::Storable::FindValue(theMap,"myViewUp[0]").toDouble();
    aViewUp[1] = VISU::Storable::FindValue(theMap,"myViewUp[1]").toDouble();
    aViewUp[2] = VISU::Storable::FindValue(theMap,"myViewUp[2]").toDouble();

    double aParallelScale = VISU::Storable::FindValue(theMap,"myParallelScale").toDouble();

    double aScaleFactor[3];
    aScaleFactor[0] = VISU::Storable::FindValue(theMap,"myScaleFactor[0]").toDouble();
    aScaleFactor[1] = VISU::Storable::FindValue(theMap,"myScaleFactor[1]").toDouble();
    aScaleFactor[2] = VISU::Storable::FindValue(theMap,"myScaleFactor[2]").toDouble();

    SetBackground(theViewWindow,aColor);
    SetPointOfView(theViewWindow,aPosition);
    SetViewUp(theViewWindow,aViewUp);
    SetFocalPoint(theViewWindow,aFocalPnt);
    SetParallelScale(theViewWindow,aParallelScale);
    ScaleView(theViewWindow,VISU::View3D::XAxis,aScaleFactor[0]);
    ScaleView(theViewWindow,VISU::View3D::YAxis,aScaleFactor[1]);
    ScaleView(theViewWindow,VISU::View3D::ZAxis,aScaleFactor[2]);
  }


  string View3D_i::ToString (SUIT_ViewWindow* theViewWindow)
  {
    ostringstream strOut;
    Storable::DataToStream(strOut, "myComment", myComment.c_str());
    ToStream(theViewWindow, strOut);
    strOut << ends;
    if(MYDEBUG) MESSAGE("View3D_i::ToString - " << strOut.str());
    return strOut.str();
  }

  void View3D_i::ToStream (SUIT_ViewWindow* theViewWindow, std::ostringstream& theStr)
  {
    double backint[3];
    GetRenderer(theViewWindow)->GetBackground(backint);
    Storable::DataToStream(theStr,"myColor.R",backint[0]);
    Storable::DataToStream(theStr,"myColor.G",backint[1]);
    Storable::DataToStream(theStr,"myColor.B",backint[2]);

    double aPosition[3];
    GetPointOfView(theViewWindow,aPosition);
    Storable::DataToStream(theStr,"myPosition[0]",aPosition[0]);
    Storable::DataToStream(theStr,"myPosition[1]",aPosition[1]);
    Storable::DataToStream(theStr,"myPosition[2]",aPosition[2]);

    double aFocalPnt[3];
    GetFocalPoint(theViewWindow,aFocalPnt);
    Storable::DataToStream(theStr,"myFocalPnt[0]",aFocalPnt[0]);
    Storable::DataToStream(theStr,"myFocalPnt[1]",aFocalPnt[1]);
    Storable::DataToStream(theStr,"myFocalPnt[2]",aFocalPnt[2]);

    double aViewUp[3];
    GetCamera(theViewWindow)->GetViewUp(aViewUp);
    Storable::DataToStream(theStr,"myViewUp[0]",aViewUp[0]);
    Storable::DataToStream(theStr,"myViewUp[1]",aViewUp[1]);
    Storable::DataToStream(theStr,"myViewUp[2]",aViewUp[2]);

    Storable::DataToStream(theStr,"myParallelScale",GetParallelScale(theViewWindow));

    double aScaleFactor[3];
    (dynamic_cast<SVTK_ViewWindow*>(theViewWindow))->GetScale(aScaleFactor);
    Storable::DataToStream(theStr,"myScaleFactor[0]",aScaleFactor[0]);
    Storable::DataToStream(theStr,"myScaleFactor[1]",aScaleFactor[1]);
    Storable::DataToStream(theStr,"myScaleFactor[2]",aScaleFactor[2]);
  }

  void View3D_i::ToStream(std::ostringstream& theStr)
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ToStream(aVW, theStr);
  }

  void View3D_i::Close()
  {
    ProcessVoidEvent(new TCloseViewEvent(GetViewWindow()));
    QApplication::sendPostedEvents();
  }
  
  View3D_i::~View3D_i()
  {
    if(MYDEBUG) MESSAGE("View3D_i::~View3D_i");
  }

  void View3D_i::SetTitle (const char* theTitle)
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TVoidMemFun1ArgEvent<SUIT_ViewWindow,const QString&,QString>
		       (aVW,&SUIT_ViewWindow::setWindowTitle,QString(theTitle)));
  }

  char* View3D_i::GetTitle()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      return CORBA::string_dup(aVW->windowTitle().toLatin1().data());
    return CORBA::string_dup("");
  }

  void View3D_i::SetBackground (SUIT_ViewWindow* theViewWindow,
				const SALOMEDS::Color& theColor)
  {
    //jfa: may be need to be enclosed in SALOME_Event?
    int aColor[3];
    aColor[0] = int(255.0*theColor.R);
    aColor[1] = int(255.0*theColor.G);
    aColor[2] = int(255.0*theColor.B);
    QColor aNewColor (aColor[0],aColor[1],aColor[2]);
    (dynamic_cast<SVTK_ViewWindow*>(theViewWindow))->setBackgroundColor(aNewColor);
  }

  SALOMEDS::Color View3D_i::GetBackground (SUIT_ViewWindow* theViewWindow)
  {
    SALOMEDS::Color aColor;
    double backint[3];
    GetRenderer(theViewWindow)->GetBackground(backint);
    aColor.R = backint[0];  aColor.G = backint[1];  aColor.B = backint[2];
    return aColor;
  }

  class TUpdateViewerEvent: public SALOME_Event
  {
    SUIT_ViewWindow* myVW;
    Prs3d_i* myPrs3d;
    int myDisplaing;
  public:
    TUpdateViewerEvent(SUIT_ViewWindow* theViewWindow,
		       Prs3d_i* thePrs3d,
		       int theDisplaing):
      myVW(theViewWindow),
      myPrs3d(thePrs3d),
      myDisplaing(theDisplaing)
    {}
    virtual void Execute(){
      UpdateViewer(myVW, myDisplaing, myPrs3d);
    }
  };

  class TDisplayTableEvent: public SALOME_Event
  {
    SUIT_ViewWindow* myVW;
    PointMap3d_i* myTable;
    int myDisplaying;
  public:
    TDisplayTableEvent( SUIT_ViewWindow* theView,
			PointMap3d_i* theTable,
			int theDisplaying ) :
      myVW (theView),
      myTable (theTable),
      myDisplaying (theDisplaying)
    {}
    virtual void Execute() {
      SVTK_ViewWindow* aViewWindow = dynamic_cast<SVTK_ViewWindow*>(myVW);
      VISU_PointMap3dActor* anActor = myTable->CreateActor();
      myTable->UpdateActor( anActor );
      aViewWindow->AddActor( anActor );
    }
  };

  void View3D_i::EraseAll()
  {
    if(MYDEBUG) MESSAGE("View3D_i::EraseAll");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TUpdateViewerEvent(aVW,NULL,eEraseAll));
  }

  void View3D_i::DisplayAll()
  {
    if(MYDEBUG) MESSAGE("View3D_i::DisplayAll");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TUpdateViewerEvent(aVW,NULL,eDisplayAll));
  }

  void View3D_i::Erase (PrsObject_ptr thePrsObj)
  {
    if(MYDEBUG) MESSAGE("View3D_i::Erase");
    SUIT_ViewWindow* aVW = GetViewWindow();
    CORBA::Object_ptr anObj = thePrsObj;
    Prs3d_i* aPrs = dynamic_cast<Prs3d_i*>(VISU::GetServant(anObj).in());
    if (aVW && aPrs)
      ProcessVoidEvent(new TUpdateViewerEvent(aVW,aPrs,eErase));
  }

  void View3D_i::Display (PrsObject_ptr thePrsObj)
  {
    if(MYDEBUG) MESSAGE("View3D_i::Display");
    SUIT_ViewWindow* aVW = GetViewWindow();
    CORBA::Object_ptr anObj = thePrsObj;
    Prs3d_i* aPrs = dynamic_cast<Prs3d_i*>(VISU::GetServant(anObj).in());
    if (aVW && aPrs)
      ProcessVoidEvent(new TUpdateViewerEvent(aVW,aPrs,eDisplay));
    else if (aVW && !aPrs) {
      PointMap3d_i* aTable = dynamic_cast<PointMap3d_i*>(VISU::GetServant(anObj).in());
      if (aTable)
	ProcessVoidEvent(new TDisplayTableEvent(aVW, aTable, eDisplay));
    }
  }

  void View3D_i::DisplayOnly (PrsObject_ptr thePrsObj)
  {
    if(MYDEBUG) MESSAGE("View3D_i::DisplayOnly");
    SUIT_ViewWindow* aVW = GetViewWindow();
    CORBA::Object_ptr anObj = thePrsObj;
    Prs3d_i* aPrs =  dynamic_cast<Prs3d_i*>(VISU::GetServant(anObj).in());
    if (aVW && aPrs)
      ProcessVoidEvent(new TUpdateViewerEvent(aVW,aPrs,eDisplayOnly));
    else if (aVW && !aPrs) {
      PointMap3d_i* aTable = dynamic_cast<PointMap3d_i*>(VISU::GetServant(anObj).in());
      if (aTable)
	ProcessVoidEvent(new TDisplayTableEvent(aVW, aTable, eDisplayOnly));
    }
  }

  void View3D_i::FitAll()
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW) {
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW), &SVTK_ViewWindow::onFitAll));
      Update();
    }
  }

  void View3D_i::SetView (VISU::View3D::ViewType theType)
  {
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (!aVW)
      return;

    switch (theType) {
    case VISU::View3D::FRONT :
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW), &SVTK_ViewWindow::onFrontView));
      break;
    case VISU::View3D::BACK :
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW), &SVTK_ViewWindow::onBackView));
      break;
    case VISU::View3D::LEFT :
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW),&SVTK_ViewWindow::onLeftView));
      break;
    case VISU::View3D::RIGHT :
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW),&SVTK_ViewWindow::onRightView));
      break;
    case VISU::View3D::TOP :
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW),&SVTK_ViewWindow::onTopView));
      break;
    case VISU::View3D::BOTTOM :
      ProcessVoidEvent(new TVoidMemFunEvent<SVTK_ViewWindow>
		       (dynamic_cast<SVTK_ViewWindow*>(aVW),&SVTK_ViewWindow::onBottomView));
      break;
    }
    Update();
  }

  class TSet3DViewParamEvent: public SALOME_Event
  {
  public:
    typedef void (*TFun)(SUIT_ViewWindow* theViewWindow, const CORBA::Double theParam[3]);
    TSet3DViewParamEvent (TFun theFun,
			  SUIT_ViewWindow* theViewWindow,
			  const CORBA::Double theParam[3]):
      myFun(theFun),
      myVW(theViewWindow),
      myParam(theParam)
    {}
    virtual void Execute() {
      myFun(myVW,myParam);
    }
  private:
    TFun myFun;
    SUIT_ViewWindow* myVW;
    const CORBA::Double* myParam;
  };

  void View3D_i::SetPointOfView (SUIT_ViewWindow* theViewWindow,
				 const CORBA::Double thePosition[3])
  {
    //jfa: may be need to be enclosed in SALOME_Event?
    GetCamera(theViewWindow)->SetPosition(thePosition);
  }

  void View3D_i::SetPointOfView (const VISU::View3D::XYZ thePosition)
  {
    if(MYDEBUG) MESSAGE("View3D_i::SetPointOfView");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSet3DViewParamEvent(&View3D_i::SetPointOfView,aVW,thePosition));
  }

  void View3D_i::GetPointOfView (SUIT_ViewWindow* theViewWindow,
				 CORBA::Double thePosition[3])
  {
    GetCamera(theViewWindow)->GetPosition(thePosition);
  }

  VISU::View3D::XYZ_slice* View3D_i::GetPointOfView()
  {
    if(MYDEBUG) MESSAGE("View3D_i::GetPointOfView");
    CORBA::Double aPosition[3];
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      GetPointOfView(aVW,aPosition);
    return VISU::View3D::XYZ_dup(aPosition);
  }

  void View3D_i::SetViewUp (SUIT_ViewWindow* theViewWindow,
			    const CORBA::Double theViewUp[3])
  {
    GetCamera(theViewWindow)->SetViewUp(theViewUp);
  }

  void View3D_i::SetViewUp (const VISU::View3D::XYZ theViewUp)
  {
    if(MYDEBUG) MESSAGE("View3D_i::SetViewUp");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSet3DViewParamEvent(&View3D_i::SetViewUp,aVW,theViewUp));
  }

  void View3D_i::GetViewUp (SUIT_ViewWindow* theViewWindow,
			    CORBA::Double theViewUp[3])
  {
    GetCamera(theViewWindow)->GetViewUp(theViewUp);
  }

  VISU::View3D::XYZ_slice* View3D_i::GetViewUp()
  {
    if(MYDEBUG) MESSAGE("View3D_i::GetViewUp");
    CORBA::Double aViewUp[3];
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      GetCamera(aVW)->GetViewUp(aViewUp);
    return VISU::View3D::XYZ_dup(aViewUp);
  }

  void View3D_i::SetFocalPoint (SUIT_ViewWindow* theViewWindow,
				const CORBA::Double theFocalPnt[3])
  {
    GetCamera(theViewWindow)->SetFocalPoint(theFocalPnt);
  }

  void View3D_i::SetFocalPoint (const VISU::View3D::XYZ theCoord)
  {
    if(MYDEBUG) MESSAGE("View3D_i::SetFocalPoint");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSet3DViewParamEvent(&View3D_i::SetFocalPoint,aVW,theCoord));
  }


  void View3D_i::GetFocalPoint (SUIT_ViewWindow* theViewWindow,
				CORBA::Double theFocalPnt[3])
  {
    GetCamera(theViewWindow)->GetFocalPoint(theFocalPnt);
  }

  VISU::View3D::XYZ_slice* View3D_i::GetFocalPoint()
  {
    if(MYDEBUG) MESSAGE("View3D_i::GetFocalPoint");
    CORBA::Double aFocalPnt[3];
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      GetFocalPoint(aVW,aFocalPnt);
    return VISU::View3D::XYZ_dup(aFocalPnt);
  }

  class TSetViewParamEvent: public SALOME_Event
  {
  public:
    typedef void (*TFun)(SUIT_ViewWindow* theViewWindow, CORBA::Double theParam);
    TSetViewParamEvent (TFun theFun,
			SUIT_ViewWindow* theViewWindow,
			CORBA::Double theParam):
      myFun(theFun),
      myVW(theViewWindow),
      myParam(theParam)
    {}
    virtual void Execute()
    {
      myFun(myVW,myParam);
    }
  private:
    TFun myFun;
    SUIT_ViewWindow* myVW;
    CORBA::Double myParam;
  };

  void View3D_i::SetParallelScale (SUIT_ViewWindow* theViewWindow,
				   CORBA::Double theScale)
  {
    GetCamera(theViewWindow)->SetParallelScale(theScale);
  }

  void View3D_i::SetParallelScale (CORBA::Double theScale)
  {
    if(MYDEBUG) MESSAGE("View3D_i::SetParallelScale");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      ProcessVoidEvent(new TSetViewParamEvent(&View3D_i::SetParallelScale,aVW,theScale));
  }

  CORBA::Double View3D_i::GetParallelScale (SUIT_ViewWindow* theViewWindow)
  {
    return GetCamera(theViewWindow)->GetParallelScale();
  }

  CORBA::Double View3D_i::GetParallelScale()
  {
    if(MYDEBUG) MESSAGE("View3D_i::GetParallelScale");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW)
      return GetParallelScale(aVW);
    return 1.0;
  }

  void View3D_i::ScaleView (SUIT_ViewWindow* theViewWindow,
			    VISU::View3D::Axis theAxis, CORBA::Double theParam)
  {
    SVTK_ViewWindow* aViewWindow = dynamic_cast<SVTK_ViewWindow*>(theViewWindow);
    double aScaleFactor[3];
    aViewWindow->GetScale(aScaleFactor);
    aScaleFactor[theAxis] = theParam;
    aViewWindow->SetScale(aScaleFactor);
  }

  void SetScaleView (SUIT_ViewWindow* theViewWindow, const CORBA::Double theScale[3])
  {
    double aScale[3] = {theScale[0], theScale[1], theScale[2]};
    (dynamic_cast<SVTK_ViewWindow*>(theViewWindow))->SetScale(aScale);
  }

  void View3D_i::ScaleView(VISU::View3D::Axis theAxis, CORBA::Double theParam)
  {
    if(MYDEBUG) MESSAGE("View3D_i::ScaleView");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW) {
      double aScale[3];
      (dynamic_cast<SVTK_ViewWindow*>(aVW))->GetScale(aScale);
      aScale[theAxis] = theParam;
      ProcessVoidEvent(new TSet3DViewParamEvent(&SetScaleView,aVW,aScale));
    }
  }

  void View3D_i::RemoveScale()
  {
    if(MYDEBUG) MESSAGE("View3D_i::RemoveScale");
    SUIT_ViewWindow* aVW = GetViewWindow();
    if (aVW) {
      double aScale[3] = {1.0, 1.0, 1.0};
      ProcessVoidEvent(new TSet3DViewParamEvent(&SetScaleView,aVW,aScale));
    }
  }

  //================================================
  // Certain presentation view parameters management
  //================================================
  class TPrsManageEvent: public SALOME_Event
  {
  protected:
    View3D_i* myView3D;
    ScalarMap_ptr myPrs;
    string myError;

    TPrsManageEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      myView3D(theView3D),
      myPrs(ScalarMap::_duplicate(thePrs)),
      myError("Unknown error occured")
    {}
    VISU_Actor* GetMyActor()
    {
      SUIT_ViewWindow* aVW = myView3D->GetViewWindow();
      SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(aVW);
      if (!aVW) {
	myError = "Corrupted view window";
      } else {
	ScalarMap_i* aPrs = dynamic_cast<ScalarMap_i*>(VISU::GetServant(myPrs).in());
	if (!aPrs) {
	  myError = "Corrupted presentation";
	} else {
	  VISU_Actor* anActor = VISU::FindActor(vw, aPrs);
	  if (!anActor) {
	    myError = "No actor found. Display the presentation at first.";
	  } else {
	    myError = "";
	    return anActor;
	  }
	}
      }
      return NULL;
    }
  };

  // Get
  class TGetPrsTypeEvent: public TPrsManageEvent {
  public:
    typedef VISU::PresentationType TResult;
    TResult myResult;
    TGetPrsTypeEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      TPrsManageEvent(theView3D, thePrs), myResult(VISU::SHRINK) {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor())
	myResult = (VISU::PresentationType)anActor->GetRepresentation();
    }
  };

  class TGetShrinkedEvent: public TPrsManageEvent {
  public:
    typedef bool TResult;
    TResult myResult;
    TGetShrinkedEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      TPrsManageEvent(theView3D, thePrs), myResult(false) {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor()) {
	if (anActor->IsShrunkable())
	  myResult = anActor->IsShrunk();
	else
	  myResult = false;
      }
    }
  };

  class TGetShadedEvent: public TPrsManageEvent {
  public:
    typedef bool TResult;
    TResult myResult;
    TGetShadedEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      TPrsManageEvent(theView3D, thePrs), myResult(false) {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor())
	if (VISU_ScalarMapAct* aScalarMapActor = dynamic_cast<VISU_ScalarMapAct*>(anActor))
	  myResult = aScalarMapActor->IsShading();
    }
  };

  class TGetOpacityEvent: public TPrsManageEvent {
  public:
    typedef double TResult;
    TResult myResult;
    TGetOpacityEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      TPrsManageEvent(theView3D, thePrs), myResult(-1.0) {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor()) {
	double oldvalue = anActor->GetOpacity();
	myResult = (double)oldvalue;
      }
    }
  };

  class TGetLineWidthEvent: public TPrsManageEvent {
  public:
    typedef double TResult;
    TResult myResult;
    TGetLineWidthEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      TPrsManageEvent(theView3D, thePrs), myResult(-1.0) {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor()) {
	double oldvalue = anActor->GetLineWidth();
	myResult = (double)oldvalue;
      }
    }
  };

  // Set
  class TSetPrsTypeEvent: public TPrsManageEvent {
  private:
    PresentationType myPrsType;
  public:
    typedef string TResult;
    TResult myResult;
    TSetPrsTypeEvent(View3D_i* theView3D, ScalarMap_ptr thePrs, PresentationType thePrsType):
      TPrsManageEvent(theView3D, thePrs), myPrsType(thePrsType),
      myResult("Unknown error occured")
    {}
    virtual void Execute() {
      VISU::VISUType aType = myPrs->GetType();
      switch (myPrsType) {
      case VISU::INSIDEFRAME:
	{
	  if (aType != VISU::TSCALARMAP &&
	      aType != VISU::TDEFORMEDSHAPE &&
	      aType != VISU::TSCALARMAPONDEFORMEDSHAPE &&
	      aType != VISU::TDEFORMEDSHAPEANDSCALARMAP) {
	    myResult = "Insideframe representation is not available for this type of presentations.";
	    return;
	  }
	}
	break;
      case VISU::SURFACEFRAME:
        {
	  if (aType != VISU::TSCALARMAP &&
	      aType != VISU::TDEFORMEDSHAPE &&
	      aType != VISU::TSCALARMAPONDEFORMEDSHAPE &&
	      aType != VISU::TDEFORMEDSHAPEANDSCALARMAP &&
	      aType != VISU::TCUTPLANES &&
	      aType != VISU::TISOSURFACES &&
	      aType != VISU::TPLOT3D) {
	    myResult = "Surfaceframe representation is not available for this type of presentations.";
	    return;
	  }
        }
	break;
      case VISU::SHRINK:
	myResult = "Use SetShrinked() method to shrink/unshrink presentation.";
	return;
      default:
	break;
      }
      if (VISU_Actor* anActor = GetMyActor()) {
	anActor->SetRepresentation((int)myPrsType);
	SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(myView3D->GetViewWindow());
	vw->Repaint();
	myResult = "";
      } else {
	myResult = myError;
      }
    }
  };

  class TSetShrinkedEvent: public TPrsManageEvent {
  private:
    bool myIsOn;
  public:
    typedef string TResult;
    TResult myResult;
    TSetShrinkedEvent(View3D_i* theView3D, ScalarMap_ptr thePrs, bool isOn):
      TPrsManageEvent(theView3D, thePrs), myIsOn(isOn),
      myResult("Unknown error occured")
    {}
    virtual void Execute() {
      VISU::VISUType aType = myPrs->GetType();
      if (aType == VISU::TVECTORS || aType == VISU::TSTREAMLINES) {
	myResult = "Shrinked representation is not available for this type of presentations.";
      } else {
	if (VISU_Actor* anActor = GetMyActor()) {
	  if (anActor->IsShrunkable()) {
	    if (myIsOn) anActor->SetShrink();
	    else        anActor->UnShrink();
	    SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(myView3D->GetViewWindow());
	    vw->Repaint();
	    myResult = "";
	  } else {
	    myResult = "This presentation is not shrunkable.";
	  }
	} else {
	  myResult = myError;
	}
      }
    }
  };

  class TSetShadedEvent: public TPrsManageEvent {
  private:
    bool myIsOn;
  public:
    typedef string TResult;
    TResult myResult;
    TSetShadedEvent(View3D_i* theView3D, ScalarMap_ptr thePrs, bool isOn):
      TPrsManageEvent(theView3D, thePrs), myIsOn(isOn),
      myResult("Unknown error occured")
    {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor()) {
	if (VISU_ScalarMapAct* aScalarMapActor =
	    dynamic_cast<VISU_ScalarMapAct*>(anActor)) {
	  aScalarMapActor->SetShading(myIsOn);
	  SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(myView3D->GetViewWindow());
	  vw->Repaint();
	  myResult = "";
	} else {
	  myResult = "Corrupted actor";
	}
      } else {
	myResult = myError;
      }
    }
  };

  class TSetOpacityEvent: public TPrsManageEvent {
  private:
    double myOpacity;
  public:
    typedef string TResult;
    TResult myResult;
    TSetOpacityEvent(View3D_i* theView3D, ScalarMap_ptr thePrs, double theOpacity):
      TPrsManageEvent(theView3D, thePrs), myOpacity(theOpacity),
      myResult("Unknown error occured")
    {}
    virtual void Execute() {
      VISU::VISUType aType = myPrs->GetType();
      if (aType == VISU::TVECTORS || aType == VISU::TSTREAMLINES) {
	myResult = "Opacity is meaningless for this type of presentations.";
	return;
      }
      if (VISU_Actor* anActor = GetMyActor()) {
	anActor->SetOpacity((double)myOpacity);
	SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(myView3D->GetViewWindow());
	vw->Repaint();
	myResult = "";
      } else {
	myResult = myError;
      }
    }
  };

  class TSetLineWidthEvent: public TPrsManageEvent {
  private:
    double myLineWidth;
  public:
    typedef string TResult;
    TResult myResult;
    TSetLineWidthEvent(View3D_i* theView3D, ScalarMap_ptr thePrs, double theLineWidth):
      TPrsManageEvent(theView3D, thePrs), myLineWidth(theLineWidth),
      myResult("Unknown error occured")
    {}
    virtual void Execute() {
      if (myPrs->GetType() == VISU::TVECTORS) {
	myResult = "Line Width is meaningless for Vectors presentation.";
	return;
      }
      if (VISU_Actor* anActor = GetMyActor()) {
	anActor->SetLineWidth((double)myLineWidth);
	SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(myView3D->GetViewWindow());
	vw->Repaint();
	myResult = "";
      } else {
	myResult = myError;
      }
    }
  };

  class TSetQuadratic2DPresentationTypeEvent: public TPrsManageEvent {
  private:
    Quadratic2DPresentationType myPrsType;
  public:
    typedef string TResult;
    TResult myResult;
    TSetQuadratic2DPresentationTypeEvent(View3D_i* theView3D,
					 ScalarMap_ptr thePrs,
					 Quadratic2DPresentationType theType):
      TPrsManageEvent(theView3D, thePrs), myPrsType(theType),
      myResult("Unknown error occured")
    {}
    virtual void Execute() 
    {
      VISU::VISUType aType = myPrs->GetType();
      if (aType != VISU::TSCALARMAP &&
	  aType != VISU::TDEFORMEDSHAPE &&
	  aType != VISU::TSCALARMAPONDEFORMEDSHAPE &&
	  aType != VISU::TDEFORMEDSHAPEANDSCALARMAP) {
	myResult = "2D Quadratic element representation is not available for this type of presentations.";
	return;
      }
      if (VISU_Actor* anActor = GetMyActor()) {
	if(myPrsType == VISU::LINES)
	  anActor->SetQuadratic2DRepresentation(VISU_Actor::eLines);
	else if(myPrsType == VISU::ARCS)
	  anActor->SetQuadratic2DRepresentation(VISU_Actor::eArcs);
	
	SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(myView3D->GetViewWindow());
	vw->Repaint();
	myResult = "";
      } else {
	myResult = myError;
      }
    }
  };

  class TGetQuadratic2DPresentationTypeEvent: public TPrsManageEvent {
  public:
    typedef VISU::Quadratic2DPresentationType TResult;
    TResult myResult;
    TGetQuadratic2DPresentationTypeEvent(View3D_i* theView3D, ScalarMap_ptr thePrs):
      TPrsManageEvent(theView3D, thePrs), myResult(VISU::LINES) {}
    virtual void Execute() {
      if (VISU_Actor* anActor = GetMyActor()) {
	if(anActor->GetQuadratic2DRepresentation() == VISU_Actor::eLines)
	  myResult = VISU::LINES;
	else if(anActor->GetQuadratic2DRepresentation() == VISU_Actor::eArcs)
	  myResult = VISU::ARCS;
      }
    }
  };

  PresentationType View3D_i::GetPresentationType(ScalarMap_ptr thePrs)
  {
    return ProcessEvent(new TGetPrsTypeEvent(this,thePrs));
  }
  CORBA::Boolean View3D_i::IsShrinked(ScalarMap_ptr thePrs)
  {
    return ProcessEvent(new TGetShrinkedEvent(this,thePrs));
  }
  CORBA::Boolean View3D_i::IsShaded(ScalarMap_ptr thePrs)
  {
    return ProcessEvent(new TGetShadedEvent(this,thePrs));
  }
  CORBA::Double View3D_i::GetOpacity(ScalarMap_ptr thePrs)
  {
    return ProcessEvent(new TGetOpacityEvent(this,thePrs));
  }
  CORBA::Double View3D_i::GetLineWidth(ScalarMap_ptr thePrs)
  {
    return ProcessEvent(new TGetLineWidthEvent(this,thePrs));
  }

  Quadratic2DPresentationType View3D_i::GetQuadratic2DPresentationType(ScalarMap_ptr thePrs)
  {
    return ProcessEvent(new TGetQuadratic2DPresentationTypeEvent(this,thePrs));
  }
  
  char* View3D_i::SetPresentationType(ScalarMap_ptr thePrs, PresentationType thePrsType)
  {
    string aRet = ProcessEvent(new TSetPrsTypeEvent(this,thePrs,thePrsType));
    return CORBA::string_dup(aRet.c_str());
  }
  char* View3D_i::SetShrinked(ScalarMap_ptr thePrs, CORBA::Boolean isShrinked)
  {
    string aRet = ProcessEvent(new TSetShrinkedEvent(this,thePrs,isShrinked));
    return CORBA::string_dup(aRet.c_str());
  }
  char* View3D_i::SetShaded(ScalarMap_ptr thePrs, CORBA::Boolean isShaded)
  {
    string aRet = ProcessEvent(new TSetShadedEvent(this,thePrs,isShaded));
    return CORBA::string_dup(aRet.c_str());
  }
  char* View3D_i::SetOpacity(ScalarMap_ptr thePrs, CORBA::Double theOpacity)
  {
    string aRet = ProcessEvent(new TSetOpacityEvent(this,thePrs,theOpacity));
    return CORBA::string_dup(aRet.c_str());
  }
  char* View3D_i::SetLineWidth(ScalarMap_ptr thePrs, CORBA::Double theLineWidth)
  {
    string aRet = ProcessEvent(new TSetLineWidthEvent(this,thePrs,theLineWidth));
    return CORBA::string_dup(aRet.c_str());
  }
  
  char* View3D_i::SetQuadratic2DPresentationType(ScalarMap_ptr thePrs, Quadratic2DPresentationType theType)
  {
    string aRet = ProcessEvent(new TSetQuadratic2DPresentationTypeEvent(this,thePrs,theType));
    return CORBA::string_dup(aRet.c_str());
  }
}



// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Gen_i.cc
//  Author : Alexey PETROV
//  Module : VISU

#include "VISU_version.h"
#include "VISU_Gen_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_PrsObject_i.hh"
#include "VISU_ViewManager_i.hh"
#include "VISU_ResultUtils.hh"

#include "VISU_Prs3d_i.hh"
#include "VISU_Mesh_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_PointMap3d_i.hh"
#include "VISU_TimeAnimation.h"
#include "VISU_Evolution.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_ColoredPrs3dCache_i.hh"
#include "VISU_ColoredPrs3dHolder_i.hh"

#include <VISU_Vtk2MedConvertor.hxx>

#include "VISU_Actor.h"

#include "HDFOI.hxx"
#include "HDFascii.hxx"
#include "SALOMEDS_Tool.hxx"

#include "SALOMEDSClient_AttributeName.hxx"
#include "SALOMEDSClient_AttributePixMap.hxx"

#include "SUIT_Session.h"
#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"
#include "LightApp_SelectionMgr.h"
#include "VTKViewer_MarkerUtils.h"
#include "SVTK_ViewModel.h"
#include "SVTK_ViewWindow.h"
#include "SALOME_Event.h"
#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"

#include "utilities.h"

// IDL Headers
#include <omnithread.h>
#include CORBA_SERVER_HEADER(SALOME_Session)
#include CORBA_SERVER_HEADER(SALOME_ModuleCatalog)
#ifdef WITH_MEDGEN
#include CORBA_SERVER_HEADER(MED_Gen)
#endif // WITH_MEDGEN

// QT Includes
#include <QDir>
#include <QFileInfo>

// VTK Includes
#include <vtkRenderer.h>
#include <vtkActorCollection.h>

// OCCT Includes
#include <TCollection_AsciiString.hxx>
#include <TColStd_SequenceOfAsciiString.hxx>

#include "Utils_ExceptHandlers.hxx"
UNEXPECT_CATCH(SalomeException, SALOME::SALOME_Exception);

// STL Includes
#include <sstream>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
namespace filesystem = boost::filesystem;


#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

extern "C"
VISU_I_EXPORT VISU::VISU_Gen_ptr
GetImpl(CORBA::ORB_ptr theORB,
        PortableServer::POA_ptr thePOA,
        SALOME_NamingService* theNamingService,
        QMutex* theMutex)
{
  if(MYDEBUG) MESSAGE("extern 'C' GetImpl");
  VISU::VISU_Gen_i *aVISU_Gen = new VISU::VISU_Gen_i(theORB,thePOA,theNamingService,theMutex);
  //return VISU::VISU_Gen::_duplicate(aVISU_Gen->_this());
  return aVISU_Gen->_this();
}

namespace VISU
{
  //----------------------------------------------------------------------------
  static std::string VISU_TMP_DIR;

  static CORBA::Boolean myIsMultiFile;
  const CORBA::Boolean IsMultiFile()
  {
    return myIsMultiFile;
  }

  //----------------------------------------------------------------------------
  _PTR(SComponent)
  ClientFindOrCreateVisuComponent (_PTR(Study) theStudyDocument)
  {
    _PTR(SComponent) aSComponent = theStudyDocument->FindComponent("VISU");
    if (!aSComponent) {
      _PTR(StudyBuilder) aStudyBuilder = theStudyDocument->NewBuilder();
      aStudyBuilder->NewCommand();
      int aLocked = theStudyDocument->GetProperties()->IsLocked();
      if (aLocked) theStudyDocument->GetProperties()->SetLocked(false);
      aSComponent = aStudyBuilder->NewComponent("VISU");
      _PTR(GenericAttribute) anAttr =
        aStudyBuilder->FindOrCreateAttribute(aSComponent, "AttributeName");
      _PTR(AttributeName) aName (anAttr);

      CORBA::ORB_var anORB = Base_i::GetORB();
      SALOME_NamingService *NamingService = new SALOME_NamingService( anORB );
      CORBA::Object_var objVarN = NamingService->Resolve("/Kernel/ModulCatalog");
      SALOME_ModuleCatalog::ModuleCatalog_var Catalogue =
        SALOME_ModuleCatalog::ModuleCatalog::_narrow(objVarN);
      SALOME_ModuleCatalog::Acomponent_var Comp = Catalogue->GetComponent( "VISU" );
      if (!Comp->_is_nil()) {
        aName->SetValue(Comp->componentusername());
      }

      anAttr = aStudyBuilder->FindOrCreateAttribute(aSComponent, "AttributePixMap");
      _PTR(AttributePixMap) aPixmap (anAttr);
      aPixmap->SetPixMap( "ICON_OBJBROWSER_Visu" );

      VISU_Gen_var aVisuGen = Base_i::GetVisuGenImpl()->_this();
      aStudyBuilder->DefineComponentInstance(aSComponent, aVisuGen->GetID());
      if (aLocked) theStudyDocument->GetProperties()->SetLocked(true);
      aStudyBuilder->CommitCommand();
    }
    return aSComponent;
  }


  //----------------------------------------------------------------------------
  SALOMEDS::SComponent_var
  FindOrCreateVisuComponent(SALOMEDS::Study_ptr theStudyDocument)
  {
    SALOMEDS::SComponent_var aSComponent = theStudyDocument->FindComponent("VISU");
    if (aSComponent->_is_nil()) {
      SALOMEDS::StudyBuilder_var aStudyBuilder = theStudyDocument->NewBuilder();
      aStudyBuilder->NewCommand();
      int aLocked = theStudyDocument->GetProperties()->IsLocked();
      if (aLocked) theStudyDocument->GetProperties()->SetLocked(false);
      aSComponent = aStudyBuilder->NewComponent("VISU");
      SALOMEDS::GenericAttribute_var anAttr = aStudyBuilder->FindOrCreateAttribute(aSComponent, "AttributeName");
      SALOMEDS::AttributeName_var aName = SALOMEDS::AttributeName::_narrow(anAttr);

      //NRI      aName->SetValue("Visu");
      CORBA::ORB_var anORB = Base_i::GetORB();
      SALOME_NamingService *NamingService = new SALOME_NamingService( anORB );
      CORBA::Object_var objVarN = NamingService->Resolve("/Kernel/ModulCatalog");
      SALOME_ModuleCatalog::ModuleCatalog_var Catalogue  = SALOME_ModuleCatalog::ModuleCatalog::_narrow(objVarN);
      SALOME_ModuleCatalog::Acomponent_var Comp = Catalogue->GetComponent( "VISU" );
      if ( !Comp->_is_nil() ) {
        aName->SetValue( Comp->componentusername() );
      }

      anAttr = aStudyBuilder->FindOrCreateAttribute(aSComponent, "AttributePixMap");
      SALOMEDS::AttributePixMap_var aPixmap = SALOMEDS::AttributePixMap::_narrow(anAttr);
      aPixmap->SetPixMap( "ICON_OBJBROWSER_Visu" );

      VISU_Gen_var aVisuGen = Base_i::GetVisuGenImpl()->_this();
      aStudyBuilder->DefineComponentInstance(aSComponent,aVisuGen);
      if (aLocked) theStudyDocument->GetProperties()->SetLocked(true);
      aStudyBuilder->CommitCommand();
    }
    return aSComponent;
  }


  //----------------------------------------------------------------------------
  void
  RegistryStorable()
  {
    Storable::RegistryStorableEngine(Result_i::myComment.c_str(),&(Result_i::StorableEngine));

    Storable::RegistryStorableEngine(Mesh_i::myComment.c_str(),&(Mesh_i::StorableEngine));

    Storable::RegistryStorableEngine(ScalarMap_i::myComment.c_str(),&(StorableEngine<ScalarMap_i>));
    Storable::RegistryStorableEngine(GaussPoints_i::myComment.c_str(),&(StorableEngine<GaussPoints_i>));
    Storable::RegistryStorableEngine(DeformedShape_i::myComment.c_str(),&(StorableEngine<DeformedShape_i>));
    Storable::RegistryStorableEngine(CutPlanes_i::myComment.c_str(),&(StorableEngine<CutPlanes_i>));
    Storable::RegistryStorableEngine(CutLines_i::myComment.c_str(),&(StorableEngine<CutLines_i>));
    Storable::RegistryStorableEngine(CutSegment_i::myComment.c_str(),&(StorableEngine<CutSegment_i>));
    Storable::RegistryStorableEngine(IsoSurfaces_i::myComment.c_str(),&(StorableEngine<IsoSurfaces_i>));
    Storable::RegistryStorableEngine(StreamLines_i::myComment.c_str(),&(StorableEngine<StreamLines_i>));
    Storable::RegistryStorableEngine(Plot3D_i::myComment.c_str(),&(StorableEngine<Plot3D_i>));
    Storable::RegistryStorableEngine(Vectors_i::myComment.c_str(),&(StorableEngine<Vectors_i>));
    Storable::RegistryStorableEngine(DeformedShapeAndScalarMap_i::myComment.c_str(),&(StorableEngine<DeformedShapeAndScalarMap_i>));

    Storable::RegistryStorableEngine(ColoredPrs3dHolder_i::myComment.c_str(),&(ColoredPrs3dHolder_i::StorableEngine));
    Storable::RegistryStorableEngine(ColoredPrs3dCache_i::myComment.c_str(),&(ColoredPrs3dCache_i::StorableEngine));

    Storable::RegistryStorableEngine(PointMap3d_i::myComment.c_str(),&(PointMap3d_i::StorableEngine));
    Storable::RegistryStorableEngine(Table_i::myComment.c_str(),&(Table_i::StorableEngine));
    Storable::RegistryStorableEngine(Curve_i::myComment.c_str(),&(Curve_i::StorableEngine));
    Storable::RegistryStorableEngine(Container_i::myComment.c_str(),&(Container_i::StorableEngine));
  }


  //----------------------------------------------------------------------------
  SALOMEDS::ListOfFileNames*
  GetListOfFileNames(const Result_i::TFileNames& theFileNames)
  {
    SALOMEDS::ListOfFileNames_var aListOfFileNames = new SALOMEDS::ListOfFileNames;
    if(!theFileNames.empty()){
      aListOfFileNames->length(theFileNames.size());
      for(int aCounter = theFileNames.size(); aCounter > 0; aCounter--)
        aListOfFileNames[aCounter-1] = theFileNames[aCounter-1].c_str();
    }
    return aListOfFileNames._retn();
  }


  //----------------------------------------------------------------------------
  bool
  LoadMarkerMap(SALOMEDS::Study_ptr theStudy,
                const char* theURL,
                bool theIsMultiFile,
                bool theIsASCII,
                StudyId2MarkerMap& theStudyId2MarkerMap,
                std::string& theMarkerMapFileName,
                std::string& theMarkerMapFile)
  {
    std::string aPrefix;
    if( theIsMultiFile ) {
      CORBA::String_var anURL = theStudy->URL();
      aPrefix = SALOMEDS_Tool::GetNameFromPath(anURL.in());
    }

    theMarkerMapFileName = aPrefix + "_textures";
    theMarkerMapFile = VISU_TMP_DIR + theMarkerMapFileName;

    if( theIsASCII && !HDFascii::ConvertFromASCIIToHDF( const_cast<char*>( theMarkerMapFile.c_str() ), true ) )
      return false;

    HDFfile*    aFile;
    HDFdataset* aDataset;
    HDFgroup*   aTopGroup;
    HDFgroup*   aGroup;
    HDFgroup*   aSubGroup;
    HDFgroup*   aSubSubGroup;
    int         aSize;

    aFile = new HDFfile( (char*)theMarkerMapFile.c_str() );
    try {
      aFile->OpenOnDisk( HDF_RDONLY );
    }
    catch ( HDFexception ) {
      INFOS( "Load(): " << theMarkerMapFile << " not found!" );
      return false;
    }

    VTK::MarkerMap& aMarkerMap = theStudyId2MarkerMap[ theStudy->StudyId() ];

    for( int i = 0, n = aFile->nInternalObjects(); i < n; i++ ) {
      char markerGrpName[ HDF_NAME_MAX_LEN+1 ];
      aFile->InternalObjectIndentify( i, markerGrpName );

      int aMarkerId = 0;
      std::string aMarkerFile;
      VTK::MarkerTexture aMarkerTexture;

      if( string( markerGrpName ).substr( 0, 6 ) == string( "Marker" ) ) {
        aTopGroup = new HDFgroup( markerGrpName, aFile );
        aTopGroup->OpenOnDisk();

        aMarkerId = atoi( string( markerGrpName ).substr( 6 ).c_str() );
        if( aMarkerId < 1 )
          continue;

        if( aTopGroup->ExistInternalObject( "File" ) ) {
          aDataset = new HDFdataset( "File", aTopGroup );
          aDataset->OpenOnDisk();
          aSize = aDataset->GetSize();
          char* aFileName = new char[ aSize ];
          aDataset->ReadFromDisk( aFileName );
          aMarkerFile = aFileName;
          delete [] aFileName;
          aDataset->CloseOnDisk();
        }

        if( aTopGroup->ExistInternalObject( "Texture" ) ) {
          aDataset = new HDFdataset( "Texture", aTopGroup );
          aDataset->OpenOnDisk();
          aSize = aDataset->GetSize();
          int* aTextureData = new int[ aSize ];
          aDataset->ReadFromDisk( aTextureData );
          for( int j = 0; j < aSize; j++ )
            aMarkerTexture.push_back( aTextureData[j] );
          delete [] aTextureData;
          aDataset->CloseOnDisk();
        }

        aTopGroup->CloseOnDisk();
      }

      if( aMarkerId > 0 )
        aMarkerMap[ aMarkerId ] = VTK::MarkerData( aMarkerFile, aMarkerTexture );
    }

    aFile->CloseOnDisk();
    delete aFile;

    return true;
  }

  //----------------------------------------------------------------------------
  bool
  SaveMarkerMap(SALOMEDS::Study_ptr theStudy,
                const char* theURL,
                bool theIsMultiFile,
                bool theIsASCII,
                const StudyId2MarkerMap& theStudyId2MarkerMap,
                std::string& theMarkerMapFileName,
                std::string& theMarkerMapFile)
  {
    VISU::StudyId2MarkerMap::const_iterator aMainIter = theStudyId2MarkerMap.find( theStudy->StudyId() );
    if( aMainIter == theStudyId2MarkerMap.end() )
      return false;

    const VTK::MarkerMap& aMarkerMap = aMainIter->second;
    if( aMarkerMap.empty() )
      return false;

    std::string aPrefix;
    if( theIsMultiFile ) {
      CORBA::String_var anURL = theStudy->URL();
      aPrefix = SALOMEDS_Tool::GetNameFromPath(anURL.in());
    }

    theMarkerMapFileName = aPrefix + "_textures";
    theMarkerMapFile = string( theURL ) + theMarkerMapFileName;

    HDFfile*    aFile;
    HDFdataset* aDataset;
    HDFgroup*   aTopGroup;
    HDFgroup*   aGroup;
    HDFgroup*   aSubGroup;
    HDFgroup*   aSubSubGroup;
    hdf_size    aSize[ 1 ];

    aFile = new HDFfile( (char*)theMarkerMapFile.c_str() );
    aFile->CreateOnDisk();

    VTK::MarkerMap::const_iterator aMarkerIter = aMarkerMap.begin();
    for( ; aMarkerIter != aMarkerMap.end(); aMarkerIter++ ) {
      int aMarkerId = aMarkerIter->first;
      const VTK::MarkerData& aMarkerData = aMarkerIter->second;
      std::string aMarkerFile = aMarkerData.first;
      VTK::MarkerTexture aMarkerTexture = aMarkerData.second;

      char markerGrpName[30];
      sprintf( markerGrpName, "Marker %d", aMarkerId );
      aTopGroup = new HDFgroup( markerGrpName, aFile );

      aTopGroup->CreateOnDisk();

      aSize[ 0 ] = aMarkerFile.length() + 1;
      aDataset = new HDFdataset( "File", aTopGroup, HDF_STRING, aSize, 1 );
      aDataset->CreateOnDisk();
      aDataset->WriteOnDisk( ( char* )( aMarkerFile.c_str() ) );
      aDataset->CloseOnDisk();

      int* aTextureData = new int[ aMarkerTexture.size() ];
      VTK::MarkerTexture::const_iterator anIter = aMarkerTexture.begin();
      for( int i = 0; anIter != aMarkerTexture.end(); anIter++, i++ )
        aTextureData[i] = *anIter;

      aSize[0] = aMarkerTexture.size();
      aDataset = new HDFdataset( "Texture", aTopGroup, HDF_INT32, aSize, 1 );
      aDataset->CreateOnDisk();
      aDataset->WriteOnDisk( aTextureData );
      aDataset->CloseOnDisk();
      delete [] aTextureData;

      aTopGroup->CloseOnDisk();
    }

    aFile->CloseOnDisk();
    delete aFile;

    if( theIsASCII && !HDFascii::ConvertFromHDFToASCII( const_cast<char*>( theMarkerMapFile.c_str() ), true ) )
      return false;

    return true;
  }


  //----------------------------------------------------------------------------
  VISU_Gen_i
  ::VISU_Gen_i(CORBA::ORB_ptr theORB, PortableServer::POA_ptr thePOA,
               SALOME_NamingService* theNamingService, QMutex* theMutex) :
    Engines_Component_i()
  {
    if(MYDEBUG) MESSAGE("VISU_Gen_i::VISU_Gen_i : "<<theMutex);
    Base_i::myMutex = theMutex;  //apo
    Base_i::myOrb = CORBA::ORB::_duplicate(theORB);
    Base_i::myPoa = PortableServer::POA::_duplicate(thePOA);
    Base_i::myNamingService = theNamingService;
    static SALOME_LifeCycleCORBA aEnginesLifeCycle(theNamingService);
    Base_i::myEnginesLifeCycle = &aEnginesLifeCycle;
    Base_i::myVisuGenImpl = this;
    RegistryStorable();

    CORBA::Object_var anObj = myNamingService->Resolve("/myStudyManager");
    SALOMEDS::StudyManager_var aStudyManager = SALOMEDS::StudyManager::_narrow(anObj);
    SALOMEDS::ListOfOpenStudies_var aListOfOpenStudies = aStudyManager->GetOpenStudies();
    if(aListOfOpenStudies->length() > 0) {
      CORBA::String_var aStudyName = aListOfOpenStudies[0];
      //aFileInfo.setFile(aStudyName.in());
      myStudyDocument = aStudyManager->GetStudyByName(aStudyName/*aFileInfo.baseName()*/);
      myClippingPlaneMgr.SetStudy(GetStudy(myStudyDocument));
    }else
      if(MYDEBUG) MESSAGE("VISU_Gen_i::VISU_Gen_i : there is no opened study in StudyManager !!!");
  }


  //----------------------------------------------------------------------------
  Prs3d_ptr
  VISU_Gen_i
  ::CreatePrs3d(VISUType theType,
                SALOMEDS::Study_ptr theStudy)
  {
    if(ColoredPrs3d_i* aPrs3d = CreatePrs3d_i(theType, theStudy, ColoredPrs3d_i::EPublishIndependently))
      return aPrs3d->_this();
    return Prs3d::_nil();
  }


  //----------------------------------------------------------------------------
  VISU_Gen_i
  ::~VISU_Gen_i()
  {
    if(MYDEBUG) MESSAGE("VISU_Gen_i::~VISU_Gen_i");
  }


  //----------------------------------------------------------------------------
  void
  CorrectSObjectType(SALOMEDS::SObject_ptr theSObject,
                     SALOMEDS::StudyBuilder_ptr theBuilder)
  {
    SALOMEDS::GenericAttribute_var anAttr;
    bool isAttrStringFound = false;

    if( theSObject->FindAttribute(anAttr, "AttributeComment") ) {
      //SRN: Replace an AttributeComment with AttributeString
      SALOMEDS::AttributeComment_var aComment = SALOMEDS::AttributeComment::_narrow(anAttr);
      string aValue = aComment->Value();
      theBuilder->RemoveAttribute(theSObject, "AttributeComment");
      anAttr = theBuilder->FindOrCreateAttribute(theSObject, "AttributeString");
      SALOMEDS::AttributeString_var aStringAttr = SALOMEDS::AttributeString::_narrow(anAttr);
      aStringAttr->SetValue(aValue.c_str());
      isAttrStringFound = true;
    }

    if ( isAttrStringFound || theSObject->FindAttribute(anAttr, "AttributeString") ) {
      SALOMEDS::AttributeString_var aAttComment = SALOMEDS::AttributeString::_narrow(anAttr);
      if ( aAttComment ) {
        CORBA::String_var aValue = aAttComment->Value();
        std::string aString = Storable::CorrectPersistentString(aValue.in());
        aAttComment->SetValue( aString.c_str() );
      }
    }
  }


  //----------------------------------------------------------------------------
  CORBA::Boolean
  LoadWithMarkerMap(SALOMEDS::SComponent_ptr theComponent,
                    const SALOMEDS::TMPFile & theStream,
                    const char* theURL,
                    CORBA::Boolean theIsMultiFile,
                    CORBA::Boolean theIsASCII,
                    StudyId2MarkerMap& theStudyId2MarkerMap)
  {
    SALOMEDS::Study_var aStudy = theComponent->GetStudy();

    SALOMEDS::ChildIterator_var anIter = aStudy->NewChildIterator(theComponent);
    SALOMEDS::StudyBuilder_var  aStudyBuilder = aStudy->NewBuilder();

    for (anIter->InitEx(true); anIter->More(); anIter->Next()) {
      SALOMEDS::SObject_var aSObject = anIter->Value();
      CorrectSObjectType(aSObject, aStudyBuilder);
    }

    VISU_TMP_DIR = theIsMultiFile ? theURL : SALOMEDS_Tool::GetTmpDir();
    SALOMEDS::ListOfFileNames_var aSeq =
      SALOMEDS_Tool::PutStreamToFiles(theStream, VISU_TMP_DIR, theIsMultiFile);
    myIsMultiFile = theIsMultiFile;

    // load textures of custom point markers
    Result_i::TFileNames aTMPFileNames;
    std::string aMarkerMapFileName, aMarkerMapFile;
    if( LoadMarkerMap( aStudy, theURL, theIsMultiFile, theIsASCII,
                       theStudyId2MarkerMap, aMarkerMapFileName, aMarkerMapFile ) ) {
      aTMPFileNames.push_back( aMarkerMapFileName );
    }

    if(!theIsMultiFile && !aTMPFileNames.empty()) {
      SALOMEDS::ListOfFileNames_var aListOfTMPFileNames = GetListOfFileNames(aTMPFileNames);
      SALOMEDS_Tool::RemoveTemporaryFiles(VISU_TMP_DIR, aListOfTMPFileNames, true );
    }

    return true;
  }


  //----------------------------------------------------------------------------
  CORBA::Boolean
  VISU_Gen_i
  ::Load(SALOMEDS::SComponent_ptr theComponent,
         const SALOMEDS::TMPFile & theStream,
         const char* theURL,
         CORBA::Boolean theIsMultiFile)
  {
    Mutex mt(myMutex);
    return LoadWithMarkerMap(theComponent, theStream, theURL, theIsMultiFile, false, myMarkerMap);
  }


  //----------------------------------------------------------------------------
  CORBA::Boolean
  VISU_Gen_i
  ::LoadASCII(SALOMEDS::SComponent_ptr theComponent,
              const SALOMEDS::TMPFile & theStream,
              const char* theURL,
              bool theIsMultiFile)
  {
    Mutex mt(myMutex);
    return LoadWithMarkerMap(theComponent, theStream, theURL, theIsMultiFile, true, myMarkerMap);
  }


  //----------------------------------------------------------------------------
  char*
  VISU_Gen_i
  ::LocalPersistentIDToIOR(SALOMEDS::SObject_ptr theSObject,
                           const char* theLocalPersistentID,
                           CORBA::Boolean theIsMultiFile,
                           CORBA::Boolean theIsASCII)
  {
    CORBA::String_var aString("");
    if(strcmp(theLocalPersistentID,"") != 0) {
      Storable* aStorable = Storable::Create(theSObject,
                                             theLocalPersistentID,
                                             VISU_TMP_DIR,
                                             theIsMultiFile);
      if(aStorable != NULL)
        aString = aStorable->GetID();
    }
    return aString._retn();
  }


  //----------------------------------------------------------------------------
  SALOMEDS::TMPFile*
  VISU_Gen_i
  ::Save(SALOMEDS::SComponent_ptr theComponent,
         const char* theURL,
         bool theIsMultiFile)
  {
    if(MYDEBUG) MESSAGE("VISU_Gen_i::Save - theURL = '"<<theURL<<"'");

    Result_i::TFileNames aFileNames;
    Result_i::TFileNames aFiles;

    SALOMEDS::Study_var aStudy = theComponent->GetStudy();
    SALOMEDS::ChildIterator_var anIter = aStudy->NewChildIterator(theComponent);
    for (; anIter->More(); anIter->Next()) {
      SALOMEDS::SObject_var aSObject = anIter->Value();
      CORBA::Object_var anObj = SObjectToObject(aSObject);
      if(Result_i* aResult = dynamic_cast<Result_i*>(GetServant(anObj).in())){
        aResult->Save(theComponent,
                      theURL,
                      theIsMultiFile,
                      false,
                      aFileNames,
                      aFiles);
      }
    }
    if(MYDEBUG) MESSAGE("VISU_Gen_i::Save - aFileNames.size() - "<<aFileNames.size());

    // save textures of custom point markers
    Result_i::TFileNames aTMPFileNames;
    std::string aMarkerMapFileName, aMarkerMapFile;
    if( SaveMarkerMap( aStudy, theURL, theIsMultiFile, false,
                       myMarkerMap, aMarkerMapFileName, aMarkerMapFile ) ) {
      aTMPFileNames.push_back( aMarkerMapFileName );
      aFileNames.push_back( aMarkerMapFileName );
      aFiles.push_back( aMarkerMapFile );
    }

    SALOMEDS::TMPFile_var aStreamFile = new SALOMEDS::TMPFile(0);
    if(aFileNames.empty())
      return aStreamFile._retn();

    SALOMEDS::ListOfFileNames_var aListOfFileNames = GetListOfFileNames(aFileNames);
    SALOMEDS::ListOfFileNames_var aListOfFiles = GetListOfFileNames(aFiles);

    if(theIsMultiFile)
      aStreamFile = SALOMEDS_Tool::PutFilesToStream(theURL, aListOfFiles.in(), theIsMultiFile);
    else
      aStreamFile = SALOMEDS_Tool::PutFilesToStream(aListOfFiles.in(), aListOfFileNames.in());

    if(!theIsMultiFile && !aTMPFileNames.empty()) {
      SALOMEDS::ListOfFileNames_var aListOfTMPFileNames = GetListOfFileNames(aTMPFileNames);
      SALOMEDS_Tool::RemoveTemporaryFiles(theURL, aListOfTMPFileNames, true);
    }

    return aStreamFile._retn();
  }


  //----------------------------------------------------------------------------
  SALOMEDS::TMPFile*
  VISU_Gen_i
  ::SaveASCII(SALOMEDS::SComponent_ptr theComponent,
              const char* theURL,
              bool theIsMultiFile)
  {
    std::string anURL = theIsMultiFile ? theURL : SALOMEDS_Tool::GetTmpDir();
    if(MYDEBUG) MESSAGE("VISU_Gen_i::SaveASCII - "<<anURL);

    Result_i::TFileNames aFileNames;
    Result_i::TFileNames aFiles;

    SALOMEDS::Study_var aStudy = theComponent->GetStudy();
    SALOMEDS::ChildIterator_var anIter = aStudy->NewChildIterator(theComponent);
    for (; anIter->More(); anIter->Next()) {
      SALOMEDS::SObject_var aSObject = anIter->Value();
      CORBA::Object_var anObj = SObjectToObject(aSObject);
      if(Result_i* aResult = dynamic_cast<Result_i*>(GetServant(anObj).in())){
        aResult->Save(theComponent,
                      anURL,
                      theIsMultiFile,
                      true,
                      aFileNames,
                      aFiles);
      }
    }
    if(MYDEBUG) MESSAGE("VISU_Gen_i::SaveASCII - aFileNames.size() - "<<aFileNames.size());

    // save textures of custom point markers
    std::string aMarkerMapFileName, aMarkerMapFile;
    if( SaveMarkerMap( aStudy, anURL.c_str(), theIsMultiFile, true,
                       myMarkerMap, aMarkerMapFileName, aMarkerMapFile ) ) {
      aFileNames.push_back( aMarkerMapFileName );
      aFiles.push_back( aMarkerMapFile );
    }

    SALOMEDS::TMPFile_var aStreamFile = new SALOMEDS::TMPFile(0);
    if(aFileNames.empty())
      return aStreamFile._retn();

    SALOMEDS::ListOfFileNames_var aListOfFileNames = GetListOfFileNames(aFileNames);
    aStreamFile = SALOMEDS_Tool::PutFilesToStream(anURL, aListOfFileNames.in(), theIsMultiFile);

    if(!theIsMultiFile)
      SALOMEDS_Tool::RemoveTemporaryFiles(anURL, aListOfFileNames, true);

    return aStreamFile._retn();
  }


  //----------------------------------------------------------------------------
  char*
  VISU_Gen_i
  ::IORToLocalPersistentID(SALOMEDS::SObject_ptr theSObject,
                           const char* theIORString,
                           CORBA::Boolean theIsMultiFile,
                           CORBA::Boolean theIsASCII)
  {
    CORBA::String_var aString("");
    if(strcmp(theIORString, "") != 0){
      CORBA::Object_var anObj = GetORB()->string_to_object(theIORString);
      if(Storable* aStorable = dynamic_cast<Storable*>(GetServant(anObj).in())){
        aString = aStorable->ToString().c_str();
        return aString._retn();
      }
    }
    return aString._retn();
  }


  //----------------------------------------------------------------------------
  char*
  VISU_Gen_i
  ::GetID()
  {
    return Base_i::GetID();
  }


  //----------------------------------------------------------------------------
  void
  VISU_Gen_i
  ::SetCurrentStudy(SALOMEDS::Study_ptr theStudy)
  {
    class TEvent: public SALOME_Event {
      std::string myStudyName;
    public:
      TEvent(const std::string theStudyName):myStudyName(theStudyName)
        {}
      virtual void Execute()
        {
          bool isActive = false;
          SUIT_Session* aSession = SUIT_Session::session();
          QList<SUIT_Application*> anApplications = aSession->applications();
          QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
          SUIT_Application* aFirstApp = *anIter;
          while (anIter != anApplications.end()) {
            SUIT_Application* anApp = *anIter;
            if (SUIT_Study* aSStudy = anApp->activeStudy()) {
              if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
                if (_PTR(Study) aCStudy = aStudy->studyDS()) {
                  if(MYDEBUG) MESSAGE("There is an application with active study : StudyId = "
                                      << aCStudy->StudyId() << "; Name = '" << aCStudy->Name() << "'");
                  if (myStudyName == aCStudy->Name()) {
                    isActive = true;
                    break;
                  }
                }
              }
            }
            anIter++;
          }
          if (!isActive) {
            MESSAGE("!!! anApp->onLoadDoc(myStudyName) !!!");
            // Has to be loaded in an empty or in a new application
            SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(aFirstApp);
            anApp->onLoadDoc(myStudyName.c_str());
          }
        }
    };

    if (!CORBA::is_nil(theStudy))
    {
      CORBA::String_var aName = theStudy->Name();
      std::string aStudyName (aName.in());
      if(MYDEBUG) MESSAGE("StudyId = " << theStudy->StudyId() << "; Name = '" << aName.in() << "'");
      myStudyDocument = SALOMEDS::Study::_duplicate(theStudy);
      myClippingPlaneMgr.SetStudy(GetStudy(myStudyDocument));

      ProcessVoidEvent(new TEvent(aStudyName));

#ifdef WITH_MEDGEN
      // Load MED component if necessary
      if (!myStudyDocument->FindComponent("MED")->_is_nil())
      {
	SALOME_LifeCycleCORBA aLCC(SalomeApp_Application::namingService());
	Engines::EngineComponent_var aComponent = aLCC.FindOrLoad_Component("FactoryServer","MED");
	SALOME_MED::MED_Gen_var aMedEngine = SALOME_MED::MED_Gen::_narrow(aComponent);
	
	if(!CORBA::is_nil(aMedEngine))
	{
	  SALOMEDS::StudyBuilder_var aStudyBuilder = myStudyDocument->NewBuilder();
	  try {
	    aStudyBuilder->LoadWith( myStudyDocument->FindComponent( "MED" ), aMedEngine );
	  }
	  catch( const SALOME::SALOME_Exception& ) {
	    // Oops, something went wrong while loading
	    // See also SalomeApp_Study::openDataModel()
	  }
	}
      }
#endif // WITH_MEDGEN
    } else {
      INFOS("CORBA::is_nil(theStudy)");
    }
  }


  //----------------------------------------------------------------------------
  SALOMEDS::Study_ptr
  VISU_Gen_i
  ::GetCurrentStudy()
  {
    return SALOMEDS::Study::_duplicate(myStudyDocument);
  }


  //----------------------------------------------------------------------------
  ViewManager_ptr
  VISU_Gen_i
  ::GetViewManager()
  {
    Mutex mt(myMutex);
    ViewManager_i * aViewManager = new ViewManager_i(myStudyDocument);
    return ViewManager::_duplicate(aViewManager->_this());
  }


  //----------------------------------------------------------------------------
  SALOMEDS::SObject_ptr
  VISU_Gen_i
  ::ImportTables(const char* theFileName, bool theFirstStrAsTitle)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return SALOMEDS::SObject::_nil();

    SALOMEDS::SObject_var aRes = VISU::ImportTables(theFileName,myStudyDocument,
                                                    theFirstStrAsTitle);

    SALOMEDS::Study_var aStudy = aRes->GetStudy();
    SALOMEDS::ChildIterator_var anIter = aStudy->NewChildIterator(aRes);
    for (; anIter->More(); anIter->Next()) {
      SALOMEDS::SObject_var SO = anIter->Value();
      CreateTable( SO->GetID() );
    }

    bool isDone;
    ProcessVoidEvent(new TUpdateObjBrowser(aStudy->StudyId(),&isDone));

    return aRes._retn();
  }


  //----------------------------------------------------------------------------
  CORBA::Boolean
  VISU_Gen_i
  ::ExportTableToFile(SALOMEDS::SObject_ptr theTable,
                      const char* theFileName)
  {
    return VISU::ExportTableToFile(theTable, theFileName);
  }


  //----------------------------------------------------------------------------
  Result_ptr
  VISU_Gen_i
  ::ImportFile(const char* theFileName)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Result::_nil();

    Result_i* aResult = Result_i::New(myStudyDocument,
                                      Result_i::eFile,
                                      Result_i::eImportFile,
                                      true,
                                      true,
                                      true,
                                      true);

    if(aResult->Create(theFileName) != NULL)
      return aResult->_this();
    else
      aResult->_remove_ref();

    return VISU::Result::_nil();
  }


  //----------------------------------------------------------------------------
  Result_ptr
  VISU_Gen_i
  ::CreateResult(const char* theFileName)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Result::_nil();

    Result_i* aResult = Result_i::New(myStudyDocument,
                                      Result_i::eFile,
                                      Result_i::eImportFile,
                                      false,
                                      true,
                                      true,
                                      true);

    if(aResult->Create(theFileName) != NULL)
      return aResult->_this();
    else
      aResult->_remove_ref();

    return VISU::Result::_nil();
  }


  //----------------------------------------------------------------------------
  Result_ptr
  VISU_Gen_i
  ::CopyAndImportFile(const char* theFileName)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Result::_nil();

    Result_i* aResult = Result_i::New(myStudyDocument,
                                      Result_i::eRestoredFile,
                                      Result_i::eCopyAndImportFile,
                                      true,
                                      true,
                                      true,
                                      true);
    if(aResult->Create(theFileName) != NULL)
      return aResult->_this();
    else
      aResult->_remove_ref();

    return VISU::Result::_nil();
  }


  //----------------------------------------------------------------------------
  Result_ptr VISU_Gen_i::ImportMed(SALOMEDS::SObject_ptr theMedSObject)
  {
    if (myStudyDocument->GetProperties()->IsLocked())
      return Result::_nil();

    Result_i* aResult = Result_i::New(myStudyDocument,
                                      Result_i::eComponent,
                                      Result_i::eImportMed,
                                      true,
                                      true,
                                      true,
                                      true);
    if (aResult->Create(theMedSObject) != NULL)
    {
      return aResult->_this();
    }
    else
      aResult->_remove_ref();

    return VISU::Result::_nil();
  }


  //----------------------------------------------------------------------------
  Result_ptr
  VISU_Gen_i
  ::ImportMedField (SALOME_MED::FIELD_ptr theField)
  {
    if (myStudyDocument->GetProperties()->IsLocked())
      return Result::_nil();

    Result_i* aResult = Result_i::New(myStudyDocument,
                                      Result_i::eComponent,
                                      Result_i::eImportMedField,
                                      true,
                                      true,
                                      true,
                                      true);

    if (aResult->Create(theField) != NULL)
      return aResult->_this();
    else
      aResult->_remove_ref();

    return VISU::Result::_nil();
  }

  void
  VISU_Gen_i
  ::RenameMeshInStudy(Result_ptr theResult,
                      const std::string& theMeshName,
                      int theEntity, // -1 for group indication
                      const std::string& theSubMeshName, // Family or Group name
                      const std::string& theNewName)
  {
    Result_i* aResult = dynamic_cast<Result_i*>(GetServant(theResult).in());
    if (!aResult)
      return;

    SALOMEDS::Study_var aStudyDocument = aResult->GetStudyDocument();
    if (aStudyDocument->GetProperties()->IsLocked())
      return;


    QString aComment;
    VISU::VISUType aType;
    if (theEntity >= 0)
      if (theSubMeshName == "")
        aType = VISU::TENTITY;
      else
        aType = VISU::TFAMILY;
    else
      aType = VISU::TGROUP;

    VISU::Storable::TRestoringMap aRestoringMap;
    aRestoringMap["myMeshName"] = theMeshName.c_str();

    switch (aType) {
    case VISU::TENTITY:
      aRestoringMap["myComment"] = "ENTITY";
      aRestoringMap["myId"] = QString::number(theEntity);
      break;
    case VISU::TFAMILY:
      aRestoringMap["myComment"] = "FAMILY";
      aRestoringMap["myEntityId"] = QString::number(theEntity);
      aRestoringMap["myName"] = theSubMeshName.c_str();
      break;
    case VISU::TGROUP:
      aRestoringMap["myComment"] = "GROUP";
      aRestoringMap["myName"] = theSubMeshName.c_str();
      break;
    }

    string anEntry = aResult->GetEntry(aRestoringMap);
    if (anEntry == "")
      return;

    SALOMEDS::SObject_ptr aSObject = aStudyDocument->FindObjectID(anEntry.c_str());

    SALOMEDS::StudyBuilder_var aStudyBuilder = aStudyDocument->NewBuilder();
    aStudyBuilder->NewCommand(); // There is a transaction

    SALOMEDS::GenericAttribute_var anAttr =
      aStudyBuilder->FindOrCreateAttribute(aSObject,"AttributeName");
    SALOMEDS::AttributeName_var aNameAttr = SALOMEDS::AttributeName::_narrow(anAttr);
    aNameAttr->SetValue(theNewName.c_str());

    aStudyBuilder->CommitCommand();
  }


  //----------------------------------------------------------------------------
  void
  VISU_Gen_i
  ::RenameEntityInStudy(Result_ptr   theResult,
                        const char*  theMeshName,
                        VISU::Entity theEntity,
                        const char*  theNewName)
  {
    RenameMeshInStudy(theResult, theMeshName, (int)theEntity, "", theNewName);
  }


  //----------------------------------------------------------------------------
  void
  VISU_Gen_i
  ::RenameFamilyInStudy(Result_ptr   theResult,
                        const char*  theMeshName,
                        VISU::Entity theEntity,
                        const char*  theFamilyName,
                        const char*  theNewName)
  {
    RenameMeshInStudy(theResult, theMeshName, (int)theEntity, theFamilyName, theNewName);
  }


  //----------------------------------------------------------------------------
  void
  VISU_Gen_i
  ::RenameGroupInStudy(Result_ptr  theResult,
                       const char*  theMeshName,
                       const char* theGroupName,
                       const char* theNewName)
  {
    RenameMeshInStudy(theResult, theMeshName, -1, theGroupName, theNewName);
  }


  //----------------------------------------------------------------------------
  Mesh_ptr
  VISU_Gen_i
  ::MeshOnEntity(Result_ptr theResult,
                 const char* theMeshName,
                 VISU::Entity theEntity)
  {
    Result_i* aResult = dynamic_cast<Result_i*>(GetServant(theResult).in());
    if (!aResult)
      return VISU::Mesh::_nil();

    SALOMEDS::Study_var aStudyDocument = aResult->GetStudyDocument();
    if (aStudyDocument->GetProperties()->IsLocked())
      return VISU::Mesh::_nil();

    Mesh_i* aPresent = new Mesh_i();
    if(aPresent->Create(aResult, theMeshName, theEntity))
      return aPresent->_this();
    else
      aPresent->_remove_ref();

    return VISU::Mesh::_nil();
  }


  //----------------------------------------------------------------------------
  Mesh_ptr
  VISU_Gen_i
  ::FamilyMeshOnEntity(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFamilyName)
  {
    Result_i* aResult = dynamic_cast<Result_i*>(GetServant(theResult).in());
    if (!aResult)
      return VISU::Mesh::_nil();

    SALOMEDS::Study_var aStudyDocument = aResult->GetStudyDocument();
    if (aStudyDocument->GetProperties()->IsLocked())
      return VISU::Mesh::_nil();

    Mesh_i* aPresent = new Mesh_i();
    if(aPresent->Create(aResult, theMeshName, theEntity, theFamilyName))
      return aPresent->_this();
    else
      aPresent->_remove_ref();

    return VISU::Mesh::_nil();
  }


  //----------------------------------------------------------------------------
  Mesh_ptr
  VISU_Gen_i
  ::GroupMesh(Result_ptr theResult,
              const char* theMeshName,
              const char* theGroupName)
  {
    Result_i* aResult = dynamic_cast<Result_i*>(GetServant(theResult).in());
    if (!aResult)
      return VISU::Mesh::_nil();

    SALOMEDS::Study_var aStudyDocument = aResult->GetStudyDocument();
    if (aStudyDocument->GetProperties()->IsLocked())
      return VISU::Mesh::_nil();

    Mesh_i* aPresent = new Mesh_i();
    if(aPresent->Create(aResult, theMeshName, theGroupName))
      return aPresent->_this();
    else
      aPresent->_remove_ref();

    return VISU::Mesh::_nil();
  }


  //----------------------------------------------------------------------------
  ScalarMap_ptr
  VISU_Gen_i
  ::ScalarMapOnField(Result_ptr theResult,
                     const char* theMeshName,
                     VISU::Entity theEntity,
                     const char* theFieldName,
                     CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::ScalarMap_i>(theResult,
                                           theMeshName,
                                           theEntity,
                                           theFieldName,
                                           theIteration)._retn();
  }


  //----------------------------------------------------------------------------
  GaussPoints_ptr
  VISU_Gen_i
  ::GaussPointsOnField(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFieldName,
                       CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::GaussPoints_i>(theResult,
                                             theMeshName,
                                             theEntity,
                                             theFieldName,
                                             theIteration)._retn();
  }


  //---------------------------------------------------------------
  DeformedShape_ptr
  VISU_Gen_i
  ::DeformedShapeOnField(Result_ptr theResult,
                         const char* theMeshName,
                         VISU::Entity theEntity,
                         const char* theFieldName,
                         CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::DeformedShape_i>(theResult,
                                               theMeshName,
                                               theEntity,
                                               theFieldName,
                                               theIteration)._retn();
  }


  //---------------------------------------------------------------
  DeformedShapeAndScalarMap_ptr
  VISU_Gen_i
  ::ScalarMapOnDeformedShapeOnField(Result_ptr theResult,
                                    const char* theMeshName,
                                    VISU::Entity theEntity,
                                    const char* theFieldName,
                                    CORBA::Long theIteration)
  {
    return DeformedShapeAndScalarMapOnField(theResult,
                                            theMeshName,
                                            theEntity,
                                            theFieldName,
                                            theIteration);
  }


//---------------------------------------------------------------
  DeformedShapeAndScalarMap_ptr
  VISU_Gen_i
  ::DeformedShapeAndScalarMapOnField(Result_ptr theResult,
                                     const char* theMeshName,
                                     VISU::Entity theEntity,
                                     const char* theFieldName,
                                     CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::DeformedShapeAndScalarMap_i>(theResult,
                                                           theMeshName,
                                                           theEntity,
                                                           theFieldName,
                                                           theIteration)._retn();
  }


  //---------------------------------------------------------------
  Vectors_ptr
  VISU_Gen_i
  ::VectorsOnField(Result_ptr theResult,
                   const char* theMeshName,
                   VISU::Entity theEntity,
                   const char* theFieldName,
                   CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::Vectors_i>(theResult,
                                         theMeshName,
                                         theEntity,
                                         theFieldName,
                                         theIteration)._retn();
  }


  //---------------------------------------------------------------
  IsoSurfaces_ptr
  VISU_Gen_i
  ::IsoSurfacesOnField(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFieldName,
                       CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::IsoSurfaces_i>(theResult,
                                             theMeshName,
                                             theEntity,
                                             theFieldName,
                                             theIteration)._retn();
  }


  //---------------------------------------------------------------
  StreamLines_ptr
  VISU_Gen_i
  ::StreamLinesOnField(Result_ptr theResult,
                       const char* theMeshName,
                       VISU::Entity theEntity,
                       const char* theFieldName,
                       CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::StreamLines_i>(theResult,
                                             theMeshName,
                                             theEntity,
                                             theFieldName,
                                             theIteration)._retn();
  }


  //---------------------------------------------------------------
  Plot3D_ptr
  VISU_Gen_i
  ::Plot3DOnField(Result_ptr theResult,
                  const char* theMeshName,
                  VISU::Entity theEntity,
                  const char* theFieldName,
                  CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::Plot3D_i>(theResult,
                                        theMeshName,
                                        theEntity,
                                        theFieldName,
                                        theIteration)._retn();
  }


  //---------------------------------------------------------------
  CutPlanes_ptr
  VISU_Gen_i
  ::CutPlanesOnField(Result_ptr theResult,
                     const char* theMeshName,
                     VISU::Entity theEntity,
                     const char* theFieldName,
                     CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::CutPlanes_i>(theResult,
                                           theMeshName,
                                           theEntity,
                                           theFieldName,
                                           theIteration)._retn();
  }


  //---------------------------------------------------------------
  CutLines_ptr
  VISU_Gen_i
  ::CutLinesOnField(Result_ptr theResult,
                    const char* theMeshName,
                    VISU::Entity theEntity,
                    const char* theFieldName,
                    CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::CutLines_i>(theResult,
                                          theMeshName,
                                          theEntity,
                                          theFieldName,
                                          theIteration)._retn();
  }

  //---------------------------------------------------------------
  CutSegment_ptr
  VISU_Gen_i
  ::CutSegmentOnField(Result_ptr theResult,
                      const char* theMeshName,
                      VISU::Entity theEntity,
                      const char* theFieldName,
                      CORBA::Long theIteration)
  {
    return Prs3dOnField<VISU::CutSegment_i>(theResult,
                                            theMeshName,
                                            theEntity,
                                            theFieldName,
                                            theIteration)._retn();
  }

  //---------------------------------------------------------------
  struct CreateTableEvent: public SALOME_Event
  {
    SALOMEDS::Study_var myStudyDocument;
    const char* myTableEntry;
    typedef Table_ptr TResult;
    TResult myResult;

    CreateTableEvent(SALOMEDS::Study_var theStudy, const char* theTableEntry)
    {
      myStudyDocument = theStudy;
      myTableEntry = theTableEntry;
      myResult = Table::_nil();
    }

    virtual
    void
    Execute()
    {
      SALOMEDS::SObject_var SO = myStudyDocument->FindObjectID(myTableEntry);
      SALOMEDS::GenericAttribute_var anAttr;
      if ( SO->FindAttribute(anAttr, "AttributeTableOfReal") ) {
        SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);
        if ( isSparseMatrix( aTableOfReal ) ) {
          PointMap3d_i* pPresent = new PointMap3d_i(myStudyDocument,myTableEntry);
          if(pPresent->Create() != NULL)
            myResult = pPresent->_this();
          else {
            pPresent->_remove_ref();
            myResult = VISU::Table::_nil();
          }
          return;
        }
      }
      Table_i* pPresent = new Table_i(myStudyDocument,myTableEntry);
      if(pPresent->Create() != NULL)
        myResult = pPresent->_this();
      else {
        pPresent->_remove_ref();
        myResult = VISU::Table::_nil();
      }
    }

    virtual
    bool
    isSparseMatrix(SALOMEDS::AttributeTableOfReal_var theTableOfReal)
    {
      int aCols = theTableOfReal->GetNbColumns();
      int aRows = theTableOfReal->GetNbRows();

      for (int i=1; i<=aCols; i++) {
        for (int j=1; j<=aRows; j++) {
          if ( !(theTableOfReal->HasValue(j, i)) )
            return false;
        }
      }
      return true;
    }
  };

  //---------------------------------------------------------------
  Table_ptr
  VISU_Gen_i
  ::CreateTable(const char* theTableEntry)
  {
    TCollection_AsciiString tmp( (char*)theTableEntry ); // 11.06.2008 IPAL18844
    if( myStudyDocument->GetProperties()->IsLocked() ||
        tmp.Length()==0 )
      return Table::_nil();

    return ProcessEvent(new CreateTableEvent(myStudyDocument, theTableEntry));
  }

  //---------------------------------------------------------------
  Curve_ptr
  VISU_Gen_i
  ::CreateCurve(Table_ptr theTable,
                CORBA::Long theHRow,
                CORBA::Long theVRow)
  {
    return CreateCurveWithZExt( theTable, theHRow, theVRow, 0, false );
  }


  //---------------------------------------------------------------
  Curve_ptr
  VISU_Gen_i
  ::CreateCurveWithZ(Table_ptr theTable,
                     CORBA::Long theHRow,
                     CORBA::Long theVRow,
                     CORBA::Long theZRow)
  {
    return CreateCurveWithZExt( theTable, theHRow, theVRow, theZRow, false );
  }


  //---------------------------------------------------------------
  Curve_ptr
  VISU_Gen_i
  ::CreateCurveWithZExt(Table_ptr theTable,
                        CORBA::Long theHRow,
                        CORBA::Long theVRow,
                        CORBA::Long theZRow,
                        CORBA::Boolean theIsV2)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Curve::_nil();
    Mutex mt(myMutex);
    PortableServer::POA_ptr aPOA = GetPOA();
    Table_i* pTable = dynamic_cast<Table_i*>(aPOA->reference_to_servant(theTable));
    Curve_i* pPresent = new Curve_i(myStudyDocument,pTable,theHRow,theVRow,theZRow,theIsV2);
    if(pPresent->Create() != NULL)
      return pPresent->_this();
    else{
      pPresent->_remove_ref();
      return VISU::Curve::_nil();
    }
  }


  //---------------------------------------------------------------
  Container_ptr
  VISU_Gen_i
  ::CreateContainer()
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Container::_nil();
    Mutex mt(myMutex);
    Container_i* pPresent = new Container_i(myStudyDocument);
    if(pPresent->Create() != NULL)
      return pPresent->_this();
    else{
      pPresent->_remove_ref();
      return VISU::Container::_nil();
    }
  }


  //---------------------------------------------------------------
  Animation_ptr
  VISU_Gen_i
  ::CreateAnimation(View3D_ptr theView3D)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Animation::_nil();
    Mutex mt(myMutex);
    if(VISU_TimeAnimation_i* anAnim = new VISU_TimeAnimation_i(myStudyDocument,theView3D)){
      return anAnim->_this();
    }else
      return VISU::Animation::_nil();
  }


  //---------------------------------------------------------------
  Evolution_ptr
  VISU_Gen_i
  ::CreateEvolution(XYPlot_ptr theXYPlot)
  {
    if(myStudyDocument->GetProperties()->IsLocked())
      return Evolution::_nil();
    Mutex mt(myMutex);
    if(VISU_Evolution_i* anEvolution = new VISU_Evolution_i(myStudyDocument,theXYPlot)){
      return anEvolution->_this();
    }else
      return VISU::Evolution::_nil();
  }


  //---------------------------------------------------------------
  void
  VISU_Gen_i
  ::DeleteResult (Result_ptr theResult)
  {
    theResult->RemoveFromStudy();
  }


  //---------------------------------------------------------------
  void
  VISU_Gen_i
  ::DeletePrs3d(Prs3d_ptr thePrs3d)
  {
    thePrs3d->RemoveFromStudy();
  }


  //---------------------------------------------------------------
  void
  VISU_Gen_i
  ::Close(SALOMEDS::SComponent_ptr theComponent)
  {
    if ( !CORBA::is_nil( theComponent ) ) {
      SALOMEDS::Study_var aStudy = theComponent->GetStudy();
      // 1. Decrement reference counter for published GenericObj-based servants
      SALOMEDS::ChildIterator_var anIter = aStudy->NewChildIterator( theComponent );
      anIter->InitEx( true );
      for ( ; anIter->More(); anIter->Next() ) {
	SALOMEDS::SObject_var aSObject = anIter->Value();
	if ( CORBA::is_nil( aSObject ) ) continue;
	CORBA::Object_var anObject = aSObject->GetObject();
	if ( CORBA::is_nil( anObject ) ) continue;
	SALOME::GenericObj_var aGenericObj = SALOME::GenericObj::_narrow( anObject );
	if ( !CORBA::is_nil( aGenericObj ) ) aGenericObj->UnRegister();
      }
 
      if ( !CORBA::is_nil( myStudyDocument ) && !CORBA::is_nil( aStudy ) && 
	   myStudyDocument->StudyId() == aStudy->StudyId() )
	myStudyDocument = SALOMEDS::Study::_nil();
    }
  }


  //---------------------------------------------------------------
  char*
  VISU_Gen_i
  ::ComponentDataType()
  {
    return CORBA::string_dup("VISU");
  }


  //---------------------------------------------------------------
  bool
  VISU_Gen_i
  ::CanPublishInStudy(CORBA::Object_ptr theIOR)
  {
    Result_var aResultObj = Result::_narrow(theIOR);
    return !(aResultObj->_is_nil());
  }


  //---------------------------------------------------------------
  SALOMEDS::SObject_ptr
  VISU_Gen_i
  ::PublishInStudy(SALOMEDS::Study_ptr theStudy,
                   SALOMEDS::SObject_ptr theSObject,
                   CORBA::Object_ptr theObject,
                   const char* theName)
    throw (SALOME::SALOME_Exception)
  {
    Unexpect aCatch(SalomeException);
    if(MYDEBUG) MESSAGE("VISU_Gen_i::PublishInStudy : "<<myMutex);
    Mutex mt(myMutex);
    SALOMEDS::SObject_var aResultSO;
    Result_i* aResultObj = dynamic_cast<Result_i*>(GetServant(theObject).in());
    if (!aResultObj)
      return aResultSO._retn();
    const QFileInfo& aFileInfo = aResultObj->GetFileInfo();
    CORBA::String_var anEntry = aResultObj->Create((const char*)aFileInfo.absoluteFilePath().toLatin1())->GetID();
    aResultSO = theStudy->FindObjectID(anEntry);
    return aResultSO._retn();
  }


  //---------------------------------------------------------------
  CORBA::Boolean
  VISU_Gen_i
  ::CanCopy(SALOMEDS::SObject_ptr theObject)
  {
    CORBA::Object_var anObj = SObjectToObject(theObject);
    if (Storable* aStorable = dynamic_cast<Storable*>(GetServant(anObj).in()))
      return aStorable->CanCopy(theObject);

    return false;
  }


  //---------------------------------------------------------------
  SALOMEDS::TMPFile*
  VISU_Gen_i
  ::CopyFrom(SALOMEDS::SObject_ptr theObject,
             CORBA::Long& theObjectID)
  {
    theObjectID = 0;
    SALOMEDS::TMPFile_var aStreamFile = new SALOMEDS::TMPFile;

    CORBA::Object_var anObj = SObjectToObject(theObject);
    if (CORBA::is_nil(anObj)) {
      aStreamFile->length(1);
      aStreamFile[0] = CORBA::string_dup("E")[0];
    } else if (Storable* aStorable = dynamic_cast<Storable*>(GetServant(anObj).in())) {
      std::string aTmpDir = SALOMEDS_Tool::GetTmpDir();
      Storable::TFileNames aFileNames;
      bool anIsDone = aStorable->CopyFrom(theObject, theObjectID, aTmpDir, aFileNames);

      SALOMEDS::ListOfFileNames_var aListOfFileNames = new SALOMEDS::ListOfFileNames;
      aListOfFileNames->length(aFileNames.size());
      for(size_t anId = 0; anId < aFileNames.size(); anId++)
        aListOfFileNames[anId] = aFileNames[anId].c_str();

      if(anIsDone)
        aStreamFile = SALOMEDS_Tool::PutFilesToStream(aTmpDir, aListOfFileNames.in(), false);

      SALOMEDS_Tool::RemoveTemporaryFiles(aTmpDir.c_str(), aListOfFileNames.in(), true);
    }
    return aStreamFile._retn();
  }


  //---------------------------------------------------------------
  CORBA::Boolean
  VISU_Gen_i
  ::CanPaste(const char* theComponentName, CORBA::Long theObjectID) {
    // The VISU component can paste only objects copied by VISU component
    CORBA::String_var aString = ComponentDataType();
    if (strcmp(theComponentName, aString.in()) == 0 && theObjectID == 1)
      return true;
    return false;
  }


  //---------------------------------------------------------------
  SALOMEDS::SObject_ptr
  VISU_Gen_i
  ::PasteInto(const SALOMEDS::TMPFile& theStream,
              CORBA::Long theObjectID,
              SALOMEDS::SObject_ptr theSObject)
  {
    if (theObjectID != 1)
      return SALOMEDS::SObject::_nil();

    SALOMEDS::SComponent_var aComponent = theSObject->GetFatherComponent();
    SALOMEDS::Study_var aStudy = theSObject->GetStudy();
    SALOMEDS::StudyBuilder_var aStudyBuilder = aStudy->NewBuilder();
    CORBA::String_var aComponentID(aComponent->GetID());
    CORBA::String_var aSObjectID(theSObject->GetID());

    SALOMEDS::SObject_var aSObject;
    if (strcmp(aComponentID, aSObjectID) == 0) //create the new result SObject
      aSObject = aStudyBuilder->NewObject(aComponent);
    else
      aSObject = SALOMEDS::SObject::_duplicate(theSObject);

    std::string aTmpDir = SALOMEDS_Tool::GetTmpDir();
    SALOMEDS::ListOfFileNames_var aListOfFileNames =
      SALOMEDS_Tool::PutStreamToFiles(theStream, aTmpDir, false);
    if(MYDEBUG) MESSAGE("Result_i::PasteInto - aListOfFileNames->length() = "<<aListOfFileNames->length());

    std::ostringstream aLocalPersistentID;
    {
      std::string aCopyPersist =  aTmpDir + "copy_persistent";
      std::ifstream anInputFileStream( aCopyPersist.c_str() );
      anInputFileStream >> aLocalPersistentID.rdbuf();
    }

    //Just for Result::Restore to find the Comment attribute :(
    SALOMEDS::GenericAttribute_var anAttr = aStudyBuilder->FindOrCreateAttribute(aSObject, "AttributeString");

    std::string aFileName(aTmpDir);
    if(aListOfFileNames->length() > 1)
      aFileName += aListOfFileNames[1].in();
    Storable* aStorable = Storable::Create(aSObject, aLocalPersistentID.str(), aFileName, false);

    SALOMEDS::ListOfFileNames_var aFilesToRemove = new SALOMEDS::ListOfFileNames;
    aFilesToRemove->length(1);
    aFilesToRemove[0] = aListOfFileNames[0];
    SALOMEDS_Tool::RemoveTemporaryFiles(aTmpDir, aFilesToRemove.in(), true);

    anAttr = aStudyBuilder->FindOrCreateAttribute(aSObject, "AttributeIOR");
    SALOMEDS::AttributeIOR_var anIOR = SALOMEDS::AttributeIOR::_narrow(anAttr);
    CORBA::String_var anIORValue(aStorable->GetID());
    anIOR->SetValue(anIORValue);

    return aSObject._retn();
  }


  //---------------------------------------------------------------
  VISU::ColoredPrs3dCache_ptr
  VISU_Gen_i
  ::GetColoredPrs3dCache(SALOMEDS::Study_ptr theStudy)
  {
    return ColoredPrs3dCache_i::GetInstance(theStudy);
  }


  CORBA::Long VISU_Gen_i::CreateClippingPlane(CORBA::Double X,CORBA::Double  Y, CORBA::Double Z,
                                              CORBA::Double dX, CORBA::Double dY, CORBA::Double dZ,
                                              CORBA::Boolean isAuto, const char* name)
  {
    return myClippingPlaneMgr.CreateClippingPlane(X, Y, Z, dX, dY, dZ, isAuto, name);
  }


  void VISU_Gen_i::EditClippingPlane(CORBA::Long id, CORBA::Double X,CORBA::Double  Y, CORBA::Double Z,
                                     CORBA::Double dX, CORBA::Double dY, CORBA::Double dZ,
                                     CORBA::Boolean isAuto, const char* name)
  {
    myClippingPlaneMgr.EditClippingPlane(id, X, Y, Z, dX, dY, dZ, isAuto, name);
  }

    /* Returns clipping plane by its Id */
  VISU::ClippingPlane* VISU_Gen_i::GetClippingPlane(CORBA::Long id)
  {
    VISU_CutPlaneFunction* aPlane = myClippingPlaneMgr.GetClippingPlane(id);

    if (aPlane != NULL) {
      double aOrigin[3];
      double aDir[3];
      aPlane->GetOrigin(aOrigin);
      aPlane->GetNormal(aDir);

      VISU::ClippingPlane* aRetPlane = new VISU::ClippingPlane;
      aRetPlane->X = aOrigin[0];
      aRetPlane->Y = aOrigin[1];
      aRetPlane->Z = aOrigin[2];
      aRetPlane->dX = aDir[0];
      aRetPlane->dY = aDir[1];
      aRetPlane->dZ = aDir[2];
      aRetPlane->isAuto = aPlane->isAuto();

      aRetPlane->name = aPlane->getName().c_str();
      return aRetPlane;
    }
    return NULL;
  }

    /* Deletes clipping plane by its Id */
  CORBA::Boolean VISU_Gen_i::DeleteClippingPlane(CORBA::Long id)
  {
    return myClippingPlaneMgr.DeleteClippingPlane(id);
  }

    /* Applyes a clipping plane with Id to presentation thePrs */
  CORBA::Boolean VISU_Gen_i::ApplyClippingPlane(Prs3d_ptr thePrs, CORBA::Long id)
  {
    VISU::Prs3d_i* aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(thePrs).in());
    return myClippingPlaneMgr.ApplyClippingPlane(aPrs, id);
  }

    /* Detaches a clipping plane with Id from presentation thePrs */
  CORBA::Boolean VISU_Gen_i::DetachClippingPlane(Prs3d_ptr thePrs, CORBA::Long id)
  {
    VISU::Prs3d_i* aPrs = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(thePrs).in());
    return myClippingPlaneMgr.DetachClippingPlane(aPrs, id);
  }

    /* Get number of clipping planes */
  CORBA::Long VISU_Gen_i::GetClippingPlanesNb()
  {
    return myClippingPlaneMgr.GetClippingPlanesNb();
  }

  /*!
  \brief Converts set of VTK files to the one MED-file
  \param theVTKFiles sequence of VTK files
  \param theMEDFile MED-file
  \param theMeshName mesh name. This parameter can be empty. In this case name
         of mesh is equal vtk2med
  \param theTStamps values of time stamps. This array can be empty, in
         this case values of time stamps will be generated automatically ( 0, 1, 2 ... )
  \return TRUE if operation has been completed successfully, FALSE otherwise
  */
  CORBA::Boolean
  VISU_Gen_i
  ::VTK2MED( const VISU::string_array& theVTKFiles,
             const char* theMEDFile,
             const char* theMeshName,
             const VISU::double_array& theTStamps )
  {
    if ( !theMEDFile || !theVTKFiles.length() )
      return false;

    VISU_Vtk2MedConvertor aConvertor;
    aConvertor.setMEDFileName( theMEDFile );
    // std::string aFirstFile = theVTKFiles[ 0 ];
    char* aFirstFile = CORBA::string_dup( theVTKFiles[ 0 ] );
    aConvertor.setFirstVTKFileName( aFirstFile );
    if ( theVTKFiles.length() > 1 )
    {
      VISU_Vtk2MedConvertor::TVectorString aFiles( theVTKFiles.length() - 1 );
      for ( int i = 1, n = theVTKFiles.length(); i < n; i++ )
        aFiles[ i - 1 ] = theVTKFiles[ i ];
      aConvertor.setDataVTKFileNames( aFiles );
    }
    if ( theTStamps.length() > 0 )
    {
      VISU_Vtk2MedConvertor::TVectorDouble aTStamps( theTStamps.length() );
      for ( int i = 0, n = theTStamps.length(); i < n; i++ )
        aTStamps[ i ] = theTStamps[ i ];
      aConvertor.setTimeStamps( aTStamps );
    }

    if ( theMeshName && strlen( theMeshName ) > 0 )
      aConvertor.setMeshName( theMeshName );

    aConvertor.addToIgnoringFieldList("cellID");
    aConvertor.setCellDataFieldNameIDS("cellID");

    int res = aConvertor.Execute();

    return res == 0;
  }

  CORBA::Long
  VISU_Gen_i
  ::LoadTexture(const char* theTextureFile)
  {
    if( CORBA::is_nil( myStudyDocument ) )
      return 0;

    int aStudyId = myStudyDocument->StudyId();

    VTK::MarkerTexture aMarkerTexture;
    if( !VTK::LoadTextureData( theTextureFile, VTK::MS_NONE, aMarkerTexture ) )
      return 0;

    VTK::MarkerMap& aMarkerMap = myMarkerMap[ aStudyId ];
    int aMarkerId = VTK::GetUniqueId( aMarkerMap );

    VTK::MarkerData& aMarkerData = aMarkerMap[ aMarkerId ];
    aMarkerData.first = theTextureFile;
    aMarkerData.second = aMarkerTexture;

    return aMarkerId;
  }

  // Version information
  char* VISU_Gen_i::getVersion()
  {
#if VISU_DEVELOPMENT
    return CORBA::string_dup( VISU_VERSION_STR"dev" );
#else
    return CORBA::string_dup( VISU_VERSION_STR );
#endif
  }
}

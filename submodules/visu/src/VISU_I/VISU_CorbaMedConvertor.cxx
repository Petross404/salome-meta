// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_CorbaMedConvertor.cxx
//  Author : Alexey PETROV
//  Module : VISU
//  $Header$
//
#include "VISU_CorbaMedConvertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <vtkCellType.h>

#include <boost/tuple/tuple.hpp>

#include "ReceiverFactory.hxx"
#include "MED_SliceArray.hxx"

#include "utilities.h"

#define USER_INTERLACE MED_FULL_INTERLACE

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

extern "C" {
  VISU_Convertor* 
  CreateMEDConvertor(SALOMEDS::SObject_ptr theMedSObject) 
  {
    return new VISU_MEDConvertor(theMedSObject);
  }

  VISU_Convertor* 
  CreateMEDFieldConvertor(SALOME_MED::FIELD_ptr theField) 
  {
    return new VISU_MEDFieldConvertor(theField);
  }
}

namespace
{
  const int MED_NBR_GEOMETRIE_MAILLE = 17;
  
  SALOME_MED::medGeometryElement 
  CELLGEOM[MED_NBR_GEOMETRIE_MAILLE] = {
    SALOME_MED::MED_POINT1,
    SALOME_MED::MED_SEG2,
    SALOME_MED::MED_SEG3,
    SALOME_MED::MED_TRIA3,
    SALOME_MED::MED_QUAD4,
    SALOME_MED::MED_TRIA6,
    SALOME_MED::MED_QUAD8,
    SALOME_MED::MED_TETRA4,
    SALOME_MED::MED_PYRA5,
    SALOME_MED::MED_PENTA6,
    SALOME_MED::MED_HEXA8,
    SALOME_MED::MED_TETRA10,
    SALOME_MED::MED_PYRA13,
    SALOME_MED::MED_PENTA15,
    SALOME_MED::MED_HEXA20,
    SALOME_MED::MED_POLYGON,
    SALOME_MED::MED_POLYHEDRA
  };
  
  const int MED_NBR_GEOMETRIE_FACE = 5;
  
  SALOME_MED::medGeometryElement
  FACEGEOM[MED_NBR_GEOMETRIE_FACE] = {
    SALOME_MED::MED_TRIA3,
    SALOME_MED::MED_QUAD4,
    SALOME_MED::MED_TRIA6,
    SALOME_MED::MED_QUAD8,
    SALOME_MED::MED_POLYGON
  };
  
  const int MED_NBR_GEOMETRIE_ARETE = 2;
  
  SALOME_MED::medGeometryElement
  EDGEGEOM[MED_NBR_GEOMETRIE_ARETE] = {
    SALOME_MED::MED_SEG2,
    SALOME_MED::MED_SEG3
  };
  
  const int MED_NBR_GEOMETRIE_NODE = 1;
  
  SALOME_MED::medGeometryElement
  NODEGEOM[MED_NBR_GEOMETRIE_NODE] = {
    SALOME_MED::MED_POINT1,
  };
  

  //---------------------------------------------------------------
  int
  GetEntity2Geom(const VISU::TEntity& theEntity, 
                 SALOME_MED::medGeometryElement*& theVector)
  {
    switch(theEntity){
    case VISU::CELL_ENTITY: 
      theVector = CELLGEOM; 
      return MED_NBR_GEOMETRIE_MAILLE; 
    case VISU::FACE_ENTITY: 
      theVector = FACEGEOM; 
      return MED_NBR_GEOMETRIE_FACE; 
    case VISU::EDGE_ENTITY: 
      theVector = EDGEGEOM; 
      return MED_NBR_GEOMETRIE_ARETE; 
    case VISU::NODE_ENTITY: 
      theVector = NODEGEOM; 
      return MED_NBR_GEOMETRIE_NODE; 
    }
    return -1;
  }
  

  //---------------------------------------------------------------
  int
  MEDGeom2NbNodes(int theMEDGeomType)
  { 
    switch(theMEDGeomType){
    case SALOME_MED::MED_NONE: 
      return 0;
    case SALOME_MED::MED_POINT1: 
      return 1;
    case SALOME_MED::MED_SEG2: 
      return 2;
    case SALOME_MED::MED_SEG3: 
      return 3;
    case SALOME_MED::MED_TRIA3: 
      return 3;
    case SALOME_MED::MED_TRIA6: 
      return 6;
    case SALOME_MED::MED_QUAD4: 
      return 4;
    case SALOME_MED::MED_QUAD8: 
      return 8;
    case SALOME_MED::MED_TETRA4: 
      return 4;
    case SALOME_MED::MED_TETRA10: 
      return 10;
    case SALOME_MED::MED_HEXA8: 
      return 8;
    case SALOME_MED::MED_HEXA20: 
      return 20;
    case SALOME_MED::MED_PENTA6: 
      return 6;
    case SALOME_MED::MED_PENTA15: 
      return 15;
    case SALOME_MED::MED_PYRA5: 
      return 5;
    case SALOME_MED::MED_PYRA13: 
      return 13;
    }
    return -1;
  }
  

  //---------------------------------------------------------------
  VISU::EGeometry
  MEDGeom2VISU(SALOME_MED::medGeometryElement theGeom)
  { 
    switch(theGeom){
    case SALOME_MED::MED_POINT1:
      return VISU::ePOINT1;
    case SALOME_MED::MED_SEG2: 
      return VISU::eSEG2;
    case SALOME_MED::MED_SEG3: 
      return VISU::eSEG3;
    case SALOME_MED::MED_TRIA3: 
      return VISU::eTRIA3;
    case SALOME_MED::MED_TRIA6: 
      return VISU::eTRIA6;
    case SALOME_MED::MED_QUAD4: 
      return VISU::eQUAD4;
    case SALOME_MED::MED_QUAD8: 
      return VISU::eQUAD8;
    case SALOME_MED::MED_TETRA4: 
      return VISU::eTETRA4;
    case SALOME_MED::MED_TETRA10: 
      return VISU::eTETRA10;
    case SALOME_MED::MED_HEXA8: 
      return VISU::eHEXA8;
    case SALOME_MED::MED_HEXA20: 
      return VISU::eHEXA20;
    case SALOME_MED::MED_PENTA6: 
      return VISU::ePENTA6;
    case SALOME_MED::MED_PENTA15: 
      return VISU::ePENTA15;
    case SALOME_MED::MED_PYRA5: 
      return VISU::ePYRA5;
    case SALOME_MED::MED_PYRA13: 
      return VISU::ePYRA13;
    case SALOME_MED::MED_POLYGON: 
      return VISU::ePOLYGONE;
    case SALOME_MED::MED_POLYHEDRA: 
      return VISU::ePOLYEDRE;
    }
    return VISU::eNONE;
  }
  
  //---------------------------------------------------------------
  SALOME_MED::medGeometryElement 
  VISUGeomToMED(int theGeom)
  { 
    switch(theGeom){
    case VISU::ePOINT1: 
      return SALOME_MED::MED_POINT1;
    case VISU::eSEG2: 
      return SALOME_MED::MED_SEG2;
    case VISU::eTRIA3: 
      return SALOME_MED::MED_TRIA3;
    case VISU::eQUAD4: 
      return SALOME_MED::MED_QUAD4;
    case VISU::eTETRA4: 
      return SALOME_MED::MED_TETRA4;
    case VISU::eHEXA8: 
      return SALOME_MED::MED_HEXA8;
    case VISU::ePENTA6: 
      return SALOME_MED::MED_PENTA6;
    case VISU::ePYRA5: 
      return SALOME_MED::MED_PYRA5;

    case VISU::eSEG3: 
      return SALOME_MED::MED_SEG3;
    case VISU::eTRIA6: 
      return SALOME_MED::MED_TRIA6;
    case VISU::eQUAD8: 
      return SALOME_MED::MED_QUAD8;
    case VISU::eTETRA10: 
      return SALOME_MED::MED_TETRA10;
    case VISU::eHEXA20: 
      return SALOME_MED::MED_HEXA20;
    case VISU::ePENTA15: 
      return SALOME_MED::MED_PENTA15;
    case VISU::ePYRA13: 
      return SALOME_MED::MED_PYRA13;
    case VISU::ePOLYGONE: 
      return SALOME_MED::MED_POLYGON;
    case VISU::ePOLYEDRE: 
      return  SALOME_MED::MED_POLYHEDRA;
    }
    return SALOME_MED::medGeometryElement(-1);
  }

  //---------------------------------------------------------------
  SALOME_MED::medGeometryElement 
  VTKGeomToMED(int theVTKGeomType)
  { 
    switch(theVTKGeomType){
    case VTK_VERTEX: 
      return SALOME_MED::MED_POINT1;
    case VTK_LINE: 
      return SALOME_MED::MED_SEG2;
    case VTK_TRIANGLE:
      return SALOME_MED::MED_TRIA3;
    case VTK_QUAD: 
      return SALOME_MED::MED_QUAD4;
    case VTK_TETRA: 
      return SALOME_MED::MED_TETRA4;
    case VTK_HEXAHEDRON: 
      return SALOME_MED::MED_HEXA8;
    case VTK_WEDGE:
      return SALOME_MED::MED_PENTA6;
    case VTK_PYRAMID: 
      return SALOME_MED::MED_PYRA5;
    case VTK_POLYGON: 
      return SALOME_MED::MED_POLYGON;
    }
    return SALOME_MED::medGeometryElement(-1);
  }
  
  //---------------------------------------------------------------
  VISU::TEntity
  MEDEntityToVTK(SALOME_MED::medEntityMesh theMEDEntity)
  {
    switch(theMEDEntity){
    case SALOME_MED::MED_NODE: 
      return VISU::NODE_ENTITY;
    case SALOME_MED::MED_EDGE: 
      return VISU::EDGE_ENTITY;
    case SALOME_MED::MED_FACE: 
      return VISU::FACE_ENTITY;
    case SALOME_MED::MED_CELL: 
      return VISU::CELL_ENTITY;
    }
    return VISU::TEntity(-1);
  }
  
  //---------------------------------------------------------------
  SALOME_MED::medEntityMesh 
  VTKEntityToMED(VISU::TEntity theVTKEntity)
  {
    switch(theVTKEntity){
    case VISU::NODE_ENTITY: 
      return SALOME_MED::MED_NODE;
    case VISU::EDGE_ENTITY: 
      return SALOME_MED::MED_EDGE;
    case VISU::FACE_ENTITY: 
      return SALOME_MED::MED_FACE;
    case VISU::CELL_ENTITY: 
      return SALOME_MED::MED_CELL;
    }
    return SALOME_MED::medEntityMesh(-1);
  }

  
  //---------------------------------------------------------------
  std::string 
  GetSObjectName(SALOMEDS::SObject_ptr aSObject)
  {
    SALOMEDS::GenericAttribute_var anAttr;
    if (aSObject->FindAttribute(anAttr,"AttributeName")) {
      SALOMEDS::AttributeName_var aName = SALOMEDS::AttributeName::_narrow(anAttr);
      CORBA::String_var aString = aName->Value();
      return aString.in();
    }
    return "";
  }
  

  //---------------------------------------------------------------
  void 
  GetCellsSize(vtkIdType& theNbCells, 
               vtkIdType& theCellsSize,
               SALOME_MED::MESH_ptr theMEDMesh,
               const VISU::TEntity& theVEntity)
  {
    theNbCells = theCellsSize = 0;
    if(MYDEBUG) MESSAGE("GetCellsSize - theVEntity = "<<theVEntity);
    const SALOME_MED::medEntityMesh& aMEntity = VTKEntityToMED(theVEntity);
    SALOME_MED::MESH::connectivityInfos_var connInfo=theMEDMesh->getConnectGlobal(aMEntity);
    int iGeomEnd = connInfo->meshTypes.length();
    for(int iGeom = 0; iGeom < iGeomEnd; iGeom++){
      int iNumElemEnd = connInfo->numberOfElements[iGeom];
      if(iNumElemEnd > 0){
        if(MYDEBUG) MESSAGE("GetCellsSize - iNumElemEnd = "<<iNumElemEnd);
        theCellsSize += iNumElemEnd + connInfo->nodalConnectivityLength[iGeom];
        theNbCells += iNumElemEnd;
      }
    }
  }
  
  
  //---------------------------------------------------------------
  void 
  GetCellsSize(vtkIdType& theNbCells, 
               vtkIdType& theCellsSize,
               SALOME_MED::FAMILY_ptr theMEDFamily)
  {
    theNbCells = theCellsSize = 0;
    SALOME_MED::SUPPORT::supportInfos_var suppInfo=theMEDFamily->getSupportGlobal();
    int iGeomEnd = suppInfo->types.length();
    if(MYDEBUG) MESSAGE("GetCellsSize - iGeomEnd = "<<iGeomEnd);
    for(int iGeom = 0; iGeom < iGeomEnd; iGeom++) {
      int iNumElemEnd = suppInfo->nbEltTypes[iGeom];
      if(iNumElemEnd > 0){
        if(MYDEBUG) MESSAGE("GetCellsSize - iNumElemEnd = "<<iNumElemEnd);
        theNbCells += iNumElemEnd;
        theCellsSize += iNumElemEnd + suppInfo->nodalConnectivityLength[iGeom];
      }
    }
  }
  
  
  //---------------------------------------------------------------
  void
  GetCellsSize(VISU::PCMesh theMesh, 
               SALOME_MED::MESH_ptr theMEDMesh, 
               const VISU::TEntity& theEntity)
  {
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;
    VISU::PCMeshOnEntity aMeshOnEntity = aMeshOnEntityMap[theEntity];
    if(theEntity == VISU::NODE_ENTITY){
      aMeshOnEntity->myNbCells = theMesh->myNbPoints;
      aMeshOnEntity->myCellsSize = 2*theMesh->myNbPoints;
    }else{
      GetCellsSize(aMeshOnEntity->myNbCells,aMeshOnEntity->myCellsSize,theMEDMesh,theEntity);
    }
  }
 

  //---------------------------------------------------------------
  VISU::PCMeshOnEntity 
  InitMeshOnEntity(const VISU::PCMesh& theMesh,
                   const VISU::TEntity& theEntity,
                   const VISU::PCMeshOnEntity& theMeshOnEntity)
  {
    VISU::PCMeshOnEntity aMeshOnEntity;
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;
    VISU::TMeshOnEntityMap::const_iterator anIter = aMeshOnEntityMap.find(theEntity);
    if(anIter == aMeshOnEntityMap.end()){
      aMeshOnEntity.reset(new VISU::TCMeshOnEntity());
      *aMeshOnEntity = *theMeshOnEntity;
      aMeshOnEntity->myEntity = theEntity;
      aMeshOnEntityMap[theEntity] = aMeshOnEntity;
    }else
      aMeshOnEntity = anIter->second;

    GetCellsSize(theMesh,theMesh->myMesh,theEntity);

    return aMeshOnEntity;
  }

  
  VISU::PCSubProfile
  CrSubProfile(const VISU::PCMesh theMesh,
               const VISU::PCField theField,
               const VISU::TCMeshOnEntity& theMeshOnEntity,
               SALOME_MED::medGeometryElement theMGeom,
               int theNbElems)
  {
    if (MYDEBUG) MESSAGE("CrSubProfile");
    VISU::EGeometry aEGeom = MEDGeom2VISU(theMGeom);
    vtkIdType aVNbNodes = VISUGeom2NbNodes(aEGeom);

    VISU::PCSubProfile aSubProfile(new VISU::TCSubProfile());
    aSubProfile->myGeom = aEGeom;
    aSubProfile->myMGeom = theMGeom;
    aSubProfile->myStatus = VISU::eAddAll;
    if(MYDEBUG) MESSAGE("theMGeom = "<<theMGeom);
    const VISU::TCellsFirstIndex& aCellsFirstIndex = theMeshOnEntity.myCellsFirstIndex;
    VISU::TCellsFirstIndex::const_iterator aTimeStampIter = aCellsFirstIndex.find(theMGeom);
    if(aTimeStampIter == (theMeshOnEntity.myCellsFirstIndex).end() && 
       theMGeom != SALOME_MED::MED_POINT1)
      aSubProfile->myStatus = VISU::eRemoveAll;
    else if(aTimeStampIter == aCellsFirstIndex.end() && theMGeom == SALOME_MED::MED_POINT1){
      if(theNbElems > 0){
        aSubProfile->myName = "";
        aSubProfile->myStatus = VISU::eAddPart;
        
        aSubProfile->myNbCells = theNbElems;
        aSubProfile->myCellsSize = aSubProfile->myNbCells;
      }
    }else{
        if(theNbElems > 0){
          aSubProfile->myName = "";
          aSubProfile->myStatus = VISU::eAddPart;
          
          aSubProfile->myNbCells = theNbElems;
          aSubProfile->myCellsSize = aSubProfile->myNbCells*aVNbNodes;
        }
      }
    
    if (MYDEBUG) MESSAGE("CrSubProfile done");
    return aSubProfile;
  }

  VISU::TProfileKey
  GetProfileKey(const VISU::PCMesh theMesh,
                const VISU::PCField theField,
                const VISU::PCValForTime theValForTime,
                const VISU::TCMeshOnEntity& theMeshOnEntity)
  {
    if (MYDEBUG) MESSAGE("GetProfileKey");

    VISU::TProfileKey aProfileKey;

    const VISU::TCellsFirstIndex& aFirstIndex = theMeshOnEntity.myCellsFirstIndex;
    VISU::TCellsFirstIndex::const_iterator anIter = aFirstIndex.begin();
    SALOME_MED::SUPPORT_var aSupport = theValForTime->myField->getSupport();
    int aNbElems = 0;
    if(anIter == aFirstIndex.end() && aSupport->getEntity() == SALOME_MED::MED_NODE){
      SALOME_MED::medGeometryElement aMGeom = SALOME_MED::MED_POINT1;
      try{
        aNbElems = aSupport->getNumberOfElements(SALOME_MED::MED_NONE);
        if(MYDEBUG)MESSAGE("aMGeom="<<aMGeom<<"   aNbElems="<<aNbElems);
      }catch(...){
        MESSAGE("Error in theValForTime->myField->getSupport()->getNumberOfElements(aMGeom);");
      }
        
      VISU::PCSubProfile aSubProfile = CrSubProfile(theMesh,
                                                    theField,
                                                    theMeshOnEntity,
                                                    aMGeom,
                                                    aNbElems);
      aProfileKey.insert(aSubProfile);
    }
    
    for(; anIter != aFirstIndex.end(); anIter++){
      SALOME_MED::medGeometryElement aMGeom = anIter->first;
      try{
        aNbElems = aSupport->getNumberOfElements(aMGeom);
        if(MYDEBUG)MESSAGE("aMGeom="<<aMGeom<<"   aNbElems="<<aNbElems);
      } catch(...){
        MESSAGE("Error in theValForTime->myField->getSupport()->getNumberOfElements(aMGeom);");
        continue;
      }
      VISU::PCSubProfile aSubProfile = CrSubProfile(theMesh,
                                                    theField,
                                                    theMeshOnEntity,
                                                    aMGeom,
                                                    aNbElems);
      aProfileKey.insert(aSubProfile);
    }

    if (MYDEBUG) MESSAGE("GetProfileKey done");
    return aProfileKey;
  }
  
  void
  InitProfile(VISU::PCMesh theMesh,
              VISU::PCField theField,
              VISU::PCValForTime theValForTime,
              VISU::TCMeshOnEntity& theMeshOnEntity)
  {
    if (MYDEBUG) MESSAGE("InitProfile");
    
    VISU::TProfileMap& aProfileMap = theMeshOnEntity.myProfileMap;

    VISU::TProfileKey aProfileKey = GetProfileKey(theMesh,
                                                  theField,
                                                  theValForTime,
                                                  theMeshOnEntity);
    
    VISU::TProfileMap::const_iterator anIter = aProfileMap.find(aProfileKey);
    if(anIter != aProfileMap.end()){
      theValForTime->myProfile = anIter->second;
      if(MYDEBUG) MESSAGE("aProfileMap.find(aProfileKey) aProfile->myGeom=");
    }else{
      VISU::PCProfile aProfile(new VISU::TCProfile());
      VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;
      
      VISU::TProfileKey::const_iterator anIter = aProfileKey.begin();
      for(; anIter != aProfileKey.end(); anIter++){
        VISU::PCSubProfile aSubProfile(*anIter);
        
        if(aProfile->myIsAll && aSubProfile->myStatus != VISU::eAddAll)
          aProfile->myIsAll = false;
        
        VISU::EGeometry aEGeom = aSubProfile->myGeom;
        aGeom2SubProfile[aEGeom] = aSubProfile;
      }
      
      aProfileMap[aProfileKey] = aProfile;
      theValForTime->myProfile = aProfile;
    }
    if (MYDEBUG) MESSAGE("InitProfile done");
  }

  void
  LoadProfile(VISU::PCMesh theMesh,
              VISU::PCField theField,
              VISU::PCValForTime theValForTime,
              VISU::PCMeshOnEntity theMeshOnEntity)
  {
    VISU::PCProfile aProfile = theValForTime->myProfile;
    if (MYDEBUG) MESSAGE("LoadProfile aProfile->myIsDone="<<aProfile->myIsDone);
    if(aProfile->myIsDone)
      return;
    
    const VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;
    VISU::TGeom2SubProfile::const_iterator aGeom2SubProfileIter = aGeom2SubProfile.begin();
    if(aGeom2SubProfileIter == aGeom2SubProfile.end()){
      MESSAGE("Warning! No geom 2 sub profile");
    }
    SALOME_MED::SUPPORT_var aSupport = theValForTime->myField->getSupport();
    for(; aGeom2SubProfileIter != aGeom2SubProfile.end(); aGeom2SubProfileIter++){
      VISU::EGeometry aEGeom = aGeom2SubProfileIter->first;
      SALOME_MED::medGeometryElement aMGeom = VISUGeomToMED(aEGeom);
      VISU::PCSubProfile aSubProfile = aGeom2SubProfileIter->second;
      SALOME_TYPES::ListOfLong_var aGeom2ProfileIds;
      std::vector<int> aGeom2Profile;
      if(!aSupport->isOnAllElements()){
        try{
          if(aMGeom == SALOME_MED::MED_POINT1)
            aGeom2ProfileIds = aSupport->getNumberFromFile(SALOME_MED::MED_NONE);
          else
            aGeom2ProfileIds = aSupport->getNumberFromFile(aMGeom);
          int aLen = aGeom2ProfileIds->length();
          if(MYDEBUG) MESSAGE_BEGIN(" - aMGeom="<<aMGeom<<"; aNbCells="<<aLen);
          for(int i = 0; i < aLen; i++){
            int anId = aGeom2ProfileIds[i];
            aGeom2Profile.push_back(anId);
            if(MYDEBUG) MESSAGE_ADD(std::endl << "------------------------------->" << anId);
          }
          if(MYDEBUG) MESSAGE_END(" ");
        } catch(...) {
          continue;
        }
      } else {
        SALOME_MED::medEntityMesh aMEntity = aSupport->getEntity();
        int aNbElems = theMesh->myMesh->getNumberOfElements(aMEntity,aMGeom);
        for(int i = 0; i < aNbElems; i++) 
          aGeom2Profile.push_back(i+1);
      }
      if(aGeom2Profile.size()>0){
        VISU::TSubMeshID& aSubMeshID = aSubProfile->mySubMeshID;
        int aSize = aGeom2Profile.size();
        aSubMeshID.resize(aSize);
        for(int anId = 0; anId < aSize; anId++){
          aSubMeshID[anId] = aGeom2Profile[anId] - 1;
        }
      }
    }
    
    aProfile->myIsDone = true;
    if (MYDEBUG) MESSAGE("LoadProfile done");
  }
}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_MEDFieldConvertor
::Build()
{
  if(myField->_is_nil()) 
    throw std::runtime_error("VISU_MEDFieldConvertor::Build >> myField->_is_nil() !!!");
  
  SALOME_MED::SUPPORT_var aMEDSupport = myField->getSupport();
  if(aMEDSupport->_is_nil()) 
    throw std::runtime_error("VISU_MEDFieldConvertor::Build >> aMEDSupport->_is_nil() !!!");

  SALOME_MED::medEntityMesh aMEntity = aMEDSupport->getEntity();
  VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
  SALOME_MED::MESH_var aMEDMesh = aMEDSupport->getMesh()->convertInMESH();
  if(aMEDMesh->_is_nil()) 
    throw std::runtime_error("VISU_MEDFieldConvertor::Build >> aMEDMesh->_is_nil() !!!");

  CORBA::String_var aMeshName = aMEDMesh->getName();
  CORBA::String_var aFieldName = myField->getName();

  VISU::PCMesh aMesh = myMeshMap[aMeshName.in()](new VISU::TCMesh());
  aMesh->myNamedPointCoords(new VISU::TNamedPointCoords());
  aMesh->myNbPoints = aMEDMesh->getNumberOfNodes();
  aMesh->myDim = aMEDMesh->getSpaceDimension();
  aMesh->myName = aMeshName.in();
  aMesh->myMesh = aMEDMesh;

  if(MYDEBUG) MESSAGE("VISU_MEDFieldConvertor::Build - aMeshName = "<<aMeshName<<"; myDim = "<<aMesh->myDim);

  VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  VISU::PCMeshOnEntity aMeshOnEntity = aMeshOnEntityMap[aVEntity](new VISU::TCMeshOnEntity());
  aMeshOnEntity->myEntity = aVEntity;
  aMeshOnEntity->myMeshName = aMeshName.in();
  aMeshOnEntity->mySupport = aMEDSupport;

  if(aVEntity == VISU::NODE_ENTITY)
    ::InitMeshOnEntity(aMesh, VISU::CELL_ENTITY, aMeshOnEntity);
  else
    ::InitMeshOnEntity(aMesh, VISU::NODE_ENTITY, aMeshOnEntity);

  ::GetCellsSize(aMesh, aMEDMesh, aVEntity);

  VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
  VISU::PCField aField = aFieldMap[aFieldName.in()](new VISU::TCField());
  aField->myId = myField->getOrderNumber();
  aField->myName = aFieldName.in();
  aField->myEntity = aVEntity;
  aField->myMeshName = aMeshName.in();
  aField->myDataSize = aMeshOnEntity->myNbCells * aField->myNbComp;

  vtkIdType aDataType = VTK_DOUBLE;
  SALOME_MED::FIELDDOUBLE_ptr aFieldDouble = SALOME_MED::FIELDDOUBLE::_narrow(myField);
  if(aFieldDouble->_is_nil()) {
    // PAL18313: Mandriva 64 porting: CRASH at creating presentation on "Import Structure".
    if (sizeof(long) == 4 ) // Size of CORBA::Long is always 4 (see CORBA_basetypes.h)
      aDataType = VTK_LONG;
    else if (sizeof(int) == 4)
      aDataType = VTK_INT;
    else {
      throw std::runtime_error("Can't map CORBA::Long to a VTK type");
    }
  }
  aField->Init(myField->getNumberOfComponents(), aDataType);

  if(MYDEBUG) MESSAGE("VISU_MEDFieldConvertor::Build - aMeshName = "<<aMeshName<<"; myDim = "<<aMesh->myDim);

  VISU::TValField& aValField = aField->myValField;
  int anId = myField->getIterationNumber();
  VISU::PCValForTime aValForTime = aValField[anId](new VISU::TCValForTime());
  aValForTime->myId = anId;
  CORBA::Double aDT = myField->getTime();
  aValForTime->myTime = VISU::TTime(aDT,"");
  aValForTime->myField = myField;

  if(MYDEBUG) 
    MESSAGE("VISU_MEDFieldConvertor::Build - aFieldName = '"<<aFieldName<<
            "'; myId = "<<anId<<"; myTime = "<<aDT);

  return this;
}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_MEDConvertor
::Build() 
{
  if(mySObject->_is_nil()) 
    throw std::runtime_error("VISU_MEDConvertor::Build >> mySObject->_is_nil() !!!");
  SALOMEDS::Study_var aStudy = mySObject->GetStudy();

  CORBA::Object_var aMedObject = VISU::SObjectToObject(mySObject);
  if(!CORBA::is_nil(aMedObject)){
    SALOME_MED::MED_var aMED = SALOME_MED::MED::_narrow(aMedObject);
    return Build(aMED);
  }

  SALOMEDS::ChildIterator_var aTimeStampIterator = aStudy->NewChildIterator(mySObject);
  return Build(aTimeStampIterator);
}


namespace
{
  using namespace boost;

  //---------------------------------------------------------------
  struct TSObjectByName
  {
    std::string myName;
    typedef tuple<SALOMEDS::SObject_var> TRet;

    TSObjectByName(const std::string& theName):
      myName(theName)
    {}

    TRet operator()(SALOMEDS::SObject_ptr theSObj, bool& theIsSuccess)
    {
      SALOMEDS::GenericAttribute_var anAttr;
      if(theSObj->FindAttribute(anAttr,"AttributeName")){
        SALOMEDS::AttributeName_var aName = SALOMEDS::AttributeName::_narrow(anAttr);
        CORBA::String_var aValue = aName->Value();
        theIsSuccess = (myName == aValue.in());
        if(theIsSuccess)
          return TRet(SALOMEDS::SObject::_duplicate(theSObj));
      }
      return TRet();
    }

  };


  //---------------------------------------------------------------
  struct TMeshByName
  {
    std::string myName;
    typedef tuple<SALOME_MED::MESH_var,SALOMEDS::SObject_var> TRet;

    TMeshByName(const std::string& theName):
      myName(theName)
    {}

    TRet operator()(SALOMEDS::SObject_ptr theSObj, bool& theIsSuccess)
    {
      CORBA::Object_var anObj = VISU::SObjectToObject(theSObj);
      if(!CORBA::is_nil(anObj)){
        SALOME_MED::MESH_var aMesh = SALOME_MED::MESH::_narrow(anObj);
        if(!CORBA::is_nil(aMesh)){
          CORBA::String_var aName = aMesh->getName();
          theIsSuccess = (myName == aName.in());
          if(theIsSuccess)
            return TRet(aMesh,SALOMEDS::SObject::_duplicate(theSObj));
        }
      }
      return TRet();
    }
  };


  //---------------------------------------------------------------
  template<typename TFun>
  typename TFun::TRet
  Find(SALOMEDS::SObject_ptr theStartSObj, 
       SALOMEDS::Study_ptr theStudy,
       TFun theFun,
       bool& theIsSuccess,
       bool theIsAllLevels = true)
  {
    SALOMEDS::ChildIterator_var anIter = theStudy->NewChildIterator(theStartSObj);
    anIter->InitEx(theIsAllLevels);
    for(; anIter->More(); anIter->Next()){
      SALOMEDS::SObject_var aSObj = anIter->Value();
      typename TFun::TRet aRet = theFun(aSObj,theIsSuccess);
      if(theIsSuccess)
        return aRet;
    }
    return typename TFun::TRet();
  }

}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_MEDConvertor
::Build(SALOME_MED::MED_ptr theMED)
{
  if(CORBA::is_nil(theMED)) 
    return NULL;

  CORBA::Long aNbMeshes = theMED->getNumberOfMeshes();
  SALOME_TYPES::ListOfString_var aMeshNames = theMED->getMeshNames();
  if(MYDEBUG) MESSAGE("VISU_MEDConvertor::Build - aNbMeshes = "<<aNbMeshes);

  SALOMEDS::Study_var aStudy = mySObject->GetStudy();
  SALOMEDS::SObject_var aMedCompSObj = mySObject->GetFather();

  bool anIsSuccess = false;
  TSObjectByName::TRet aSObjectByNameRet = 
    Find(aMedCompSObj,aStudy,TSObjectByName("MEDMESH"),anIsSuccess);
  if(MYDEBUG) 
    MESSAGE("VISU_MEDConvertor::Build - Find ('"<<"MEDMESH"<<"') = "<<anIsSuccess);
  if(anIsSuccess){
    SALOMEDS::SObject_var aMeshesSObj = boost::get<0>(aSObjectByNameRet);
    for(int iMesh = 0; iMesh < aNbMeshes; iMesh++){
      anIsSuccess = false;
      CORBA::String_var aMeshName = aMeshNames[iMesh];
      TMeshByName::TRet aMeshByNameRet = 
        Find(aMeshesSObj,aStudy,TMeshByName(aMeshName.in()),anIsSuccess);
      if(MYDEBUG) 
        MESSAGE("VISU_MEDConvertor::Build - Find aMeshName('"<<aMeshName.in()<<"') = "<<anIsSuccess);
      if(!anIsSuccess)
        continue;

      VISU::PCMesh aMesh = myMeshMap[aMeshName.in()](new VISU::TCMesh());
      SALOME_MED::MESH_var aMEDMesh = boost::get<0>(aMeshByNameRet);
      aMesh->myNamedPointCoords(new VISU::TNamedPointCoords());
      aMesh->myNbPoints = aMEDMesh->getNumberOfNodes();
      aMesh->myDim = aMEDMesh->getSpaceDimension();
      aMesh->myName = aMeshName.in();
      aMesh->myMesh = aMEDMesh;

      if(MYDEBUG) 
        MESSAGE("VISU_MEDConvertor::Build - aMeshName = "<<aMeshName<<"; myDim = "<<aMesh->myDim);

      std::string aName = aMeshName.in();
      std::replace(aName.begin(),aName.end(),' ','_');

      anIsSuccess = false;
      std::ostringstream aStream;
      aStream<<"MEDSUPPORTS_OF_"<<aName;
      std::string aSupportsName(aStream.str());
      TSObjectByName::TRet aSObjectByNameRet = 
        Find(aMeshesSObj,aStudy,TSObjectByName(aSupportsName.c_str()),anIsSuccess);
      if(MYDEBUG) 
        MESSAGE("VISU_MEDConvertor::Build - Find aSupportsName('"<<aSupportsName<<"') = "<<anIsSuccess);
      if(!anIsSuccess)
        continue;

      VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      SALOMEDS::SObject_var aSupportsSObj = boost::get<0>(aSObjectByNameRet);
      SALOMEDS::ChildIterator_var aSupportIterator = aStudy->NewChildIterator(aSupportsSObj);

      // Fill all MeshOnEntity
      aSupportIterator->InitEx(true);
      for(; aSupportIterator->More(); aSupportIterator->Next()){
        SALOMEDS::SObject_var aSupportSObj = aSupportIterator->Value();
        
        CORBA::Object_var aMedSupport = VISU::SObjectToObject(aSupportSObj);
        if(CORBA::is_nil(aMedSupport)) 
          continue;
        
        SALOME_MED::SUPPORT_var aMEDSupport = SALOME_MED::SUPPORT::_narrow(aMedSupport); 
        if(aMEDSupport->_is_nil()) 
          continue;
        
        SALOME_MED::MESH_var aMeshOnSupport = aMEDSupport->getMesh()->convertInMESH();
        SALOME_MED::medEntityMesh aMEntity = aMEDSupport->getEntity();
        VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
        CORBA::String_var aSupportName = aMEDSupport->getName();
        
        if(aMEDSupport->isOnAllElements() && strcmp(aSupportName.in(),"SupportOnAll_MED_") > 0){
          if(MYDEBUG) 
            MESSAGE("VISU_MEDConvertor::Build - Support isOnAllElements = '"<<aSupportName<<
                    "' aVEntity = "<<aVEntity);
          vtkIdType aNbCells, aCellsSize;
          //Check, if there is any data on the support?
          if(aVEntity == VISU::NODE_ENTITY){
            aMesh->myNbPoints = aMeshOnSupport->getNumberOfNodes();
            aNbCells = aMesh->myNbPoints;
            aCellsSize = 2*aMesh->myNbPoints;
          }else
            ::GetCellsSize(aNbCells,aCellsSize,aMeshOnSupport,aVEntity);
          
          if(aNbCells > 0){
            if(aMeshOnEntityMap.find(aVEntity) == aMeshOnEntityMap.end()){
              VISU::PCMeshOnEntity aMeshOnEntity(new VISU::TCMeshOnEntity());
              aMeshOnEntity->myMeshName = aMeshName.in();
              aMeshOnEntity->myEntity = aVEntity;
              aMeshOnEntity->myNbCells = aNbCells;
              aMeshOnEntity->myCellsSize = aCellsSize;
              aMeshOnEntity->mySupport = aMEDSupport;
              aMeshOnEntityMap[aVEntity] = aMeshOnEntity;
            }
          }
        }
      }

      // Fill all Family
      aSupportIterator->InitEx(true);
      for(; aSupportIterator->More(); aSupportIterator->Next()){
        SALOMEDS::SObject_var aSupportSObj = aSupportIterator->Value();
        
        CORBA::Object_var aMedSupport = VISU::SObjectToObject(aSupportSObj);
        if(CORBA::is_nil(aMedSupport)) 
          continue;
        
        SALOME_MED::SUPPORT_var aMEDSupport = SALOME_MED::SUPPORT::_narrow(aMedSupport); 
        if(aMEDSupport->_is_nil()) 
          continue;
        
        SALOME_MED::MESH_var aMeshOnSupport = aMEDSupport->getMesh()->convertInMESH();
        SALOME_MED::medEntityMesh aMEntity = aMEDSupport->getEntity();
        VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
        CORBA::String_var aSupportName = aMEDSupport->getName();
        
        SALOME_MED::FAMILY_var aMEDFamily = SALOME_MED::FAMILY::_narrow(aMedSupport);
        if(!aMEDFamily->_is_nil()) {
          VISU::TMeshOnEntityMap::iterator aMeshOnEntityMapIter = aMeshOnEntityMap.find(aVEntity);
          if(aMeshOnEntityMapIter == aMeshOnEntityMap.end())
            continue;
          VISU::PCMeshOnEntity aMeshOnEntity = aMeshOnEntityMapIter->second;

          vtkIdType aNbCells = aMeshOnEntity->myNbCells, aCellsSize = aMeshOnEntity->myCellsSize;
          CORBA::Boolean anIsOnAllElements = aMEDSupport->isOnAllElements();
          if(!anIsOnAllElements)
            ::GetCellsSize(aNbCells,aCellsSize,aMEDFamily);

          if(MYDEBUG) 
            MESSAGE("VISU_MEDConvertor::Build "<<
                    "- aFamily = '"<<aSupportName<<"'"<<
                    "; anIsOnAllElements = "<<anIsOnAllElements<<
                    "; aVEntity = "<<aVEntity<<
                    "; aNbCells = "<<aNbCells);

          if(aNbCells > 0){
            VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
            VISU::TFamilyMap::const_iterator aFamilyMapIter = aFamilyMap.find(aSupportName.in());
            if(aFamilyMapIter == aFamilyMap.end()){
              VISU::PCFamily aFamily(new VISU::TCFamily());
              aFamily->myEntity = aVEntity;
              aFamily->myNbCells = aNbCells;
              aFamily->myCellsSize = aCellsSize;
              aFamily->myId = aMEDFamily->getIdentifier();
              aFamily->myName = aSupportName.in();
              aFamily->myFamily = aMEDFamily;
              aFamilyMap[aSupportName.in()] = aFamily;
            }
          }
        }
      }
        
      // Fill all Groups
      aSupportIterator->InitEx(true);
      for(; aSupportIterator->More(); aSupportIterator->Next()){
        SALOMEDS::SObject_var aSupportSObj = aSupportIterator->Value();
        
        CORBA::Object_var aMedSupport = VISU::SObjectToObject(aSupportSObj);
        if(CORBA::is_nil(aMedSupport)) 
          continue;
        
        SALOME_MED::SUPPORT_var aMEDSupport = SALOME_MED::SUPPORT::_narrow(aMedSupport); 
        if(aMEDSupport->_is_nil()) 
          continue;
        
        SALOME_MED::MESH_var aMeshOnSupport = aMEDSupport->getMesh()->convertInMESH();
        SALOME_MED::medEntityMesh aMEntity = aMEDSupport->getEntity();
        VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
        CORBA::String_var aSupportName = aMEDSupport->getName();
        
        SALOME_MED::GROUP_var aMEDGroup = SALOME_MED::GROUP::_narrow(aMedSupport);
        if(!aMEDGroup->_is_nil()){
          CORBA::Boolean anIsOnAllElements = aMEDSupport->isOnAllElements();

          if(MYDEBUG) 
            MESSAGE("VISU_MEDConvertor::Build "<<
                    "- aGroup = '"<<aSupportName<<"'"<<
                    "; anIsOnAllElements = "<<anIsOnAllElements<<
                    "; aVEntity = "<<aVEntity);

          VISU::PCGroup aGroup(new VISU::TCGroup());
          aGroup->myGroup = aMEDGroup;
          VISU::TFamilySet& aFamilySet = aGroup->myFamilySet;
          
          SALOME_MED::Family_array_var aFamilies = aMEDGroup->getFamilies();
          int iFamilyEnd = aFamilies->length();
          for(int iFamaily = 0; iFamaily < iFamilyEnd; iFamaily++){
            SALOME_MED::FAMILY_var aMEDFamily = aFamilies[iFamaily];
            CORBA::String_var aFamilyName = aMEDFamily->getName();
            TFindFamilyOnEntity aFindFamilyOnEntity = 
              FindFamilyOnEntity(aMeshName.in(), aVEntity, aFamilyName.in());
            VISU::PCFamily aFamily = boost::get<2>(aFindFamilyOnEntity);
            if(MYDEBUG) MESSAGE("VISU_MEDConvertor::Build - aGroup - aFamilyName = '"<<aFamilyName.in()<<"' = "<<bool(aFamily));
            if(aFamily){
              aFamilySet.insert(VISU::TEnity2Family(aVEntity, aFamily));
            }
          }
          
          if(!aFamilySet.empty()){
            VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
            aGroupMap[aSupportName.in()] = aGroup;
          }

        }
      }
    }
  }

  anIsSuccess = false;
  aSObjectByNameRet = Find(aMedCompSObj,aStudy,TSObjectByName("MEDFIELD"),anIsSuccess);
  if(anIsSuccess){
    SALOMEDS::SObject_var aFieldsSObj = boost::get<0>(aSObjectByNameRet);
    if(MYDEBUG) MESSAGE("VISU_MEDConvertor::Build - MEDFIELD found.");
    SALOMEDS::ChildIterator_var aFieldIterator = aStudy->NewChildIterator(aFieldsSObj);
    for(int iField = 0; aFieldIterator->More(); aFieldIterator->Next(), iField++){
      SALOMEDS::SObject_var aFieldSObj = aFieldIterator->Value();
      if(MYDEBUG) MESSAGE("VISU_MEDConvertor::Build - aFieldName = '"<<GetSObjectName(aFieldSObj)<<"'");
      SALOMEDS::ChildIterator_var aTimeStampIterator = aStudy->NewChildIterator(aFieldSObj);
      for(; aTimeStampIterator->More(); aTimeStampIterator->Next()){
        SALOMEDS::SObject_var aTimeStampSObj = aTimeStampIterator->Value();
        if(MYDEBUG) MESSAGE("VISU_MEDConvertor::Build - aTimeStampSObj = '"<<GetSObjectName(aTimeStampSObj)<<"'");
        CORBA::Object_var aMedField = VISU::SObjectToObject(aTimeStampSObj);
        if(CORBA::is_nil(aMedField)) 
          continue;

        SALOME_MED::FIELD_var aMEDField = SALOME_MED::FIELD::_narrow(aMedField);
        if(aMEDField->_is_nil()) 
          continue;

        SALOME_MED::SUPPORT_var aMEDSupport = aMEDField->getSupport();
        if(aMEDSupport->_is_nil()) 
          continue;

        SALOME_MED::medEntityMesh aMEntity = aMEDSupport->getEntity();
        VISU::TEntity anEntity = MEDEntityToVTK(aMEntity);
        SALOME_MED::MESH_var aMEDMesh = aMEDSupport->getMesh()->convertInMESH();
        if(aMEDMesh->_is_nil()) 
          continue;

        CORBA::String_var aMeshName = aMEDMesh->getName();
        CORBA::String_var aFieldName = aMEDField->getName();
        
        VISU::TMeshMap::iterator aMeshMapIter = myMeshMap.find(aMeshName.in());
        if(aMeshMapIter == myMeshMap.end())
          continue;

        VISU::PCMesh aMesh = aMeshMapIter->second;
        VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
        VISU::TMeshOnEntityMap::iterator aMeshOnEntityMapIter = aMeshOnEntityMap.find(anEntity);
        if(aMeshOnEntityMapIter == aMeshOnEntityMap.end())
          continue;

        VISU::PCMeshOnEntity aMeshOnEntity = aMeshOnEntityMapIter->second;
        VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
        VISU::TFieldMap::iterator aFieldMapIter = aFieldMap.find(aFieldName.in());
        VISU::PCField aField;
        if(aFieldMapIter == aFieldMap.end()){
          aField = aFieldMap[aFieldName.in()](new VISU::TCField());
          aField->myId = iField;
          aField->myName = aFieldName.in();
          aField->myEntity = anEntity;
          aField->myMeshName = aMeshName.in();
          aField->myDataSize = aMeshOnEntity->myNbCells * aField->myNbComp;

          vtkIdType aDataType = VTK_DOUBLE;
          SALOME_MED::FIELDDOUBLE_ptr aFieldDouble = SALOME_MED::FIELDDOUBLE::_narrow(aMEDField);
          if(aFieldDouble->_is_nil()) {
            // PAL18313: Mandriva 64 porting: CRASH at creating presentation on "Import Structure".
            if (sizeof(long) == 4 ) // Size of CORBA::Long is always 4 (see CORBA_basetypes.h)
              aDataType = VTK_LONG;
            else if (sizeof(int) == 4)
              aDataType = VTK_INT;
            else {
              MESSAGE("Can't map CORBA::Long to a VTK type, for Field " << aFieldName);
              continue;
            }
          }
          aField->Init(aMEDField->getNumberOfComponents(), aDataType);

          if(MYDEBUG) 
            MESSAGE("VISU_MEDConvertor::Build - aMeshOnEntity->myNbCells = "<<aMeshOnEntity->myNbCells);
        }else
          aField = aFieldMapIter->second;

        VISU::TValField& aValField = aField->myValField;
        int anId = aMEDField->getIterationNumber();
        VISU::PCValForTime aValForTime = aValField[anId](new VISU::TCValForTime());
        aValForTime->myId = anId;
        CORBA::Double aDT = aMEDField->getTime();
        aValForTime->myTime = VISU::TTime(aDT,"");
        aValForTime->myField = aMEDField;
        if(MYDEBUG) 
          MESSAGE("VISU_MEDConvertor::Build - aMeshName = '"<<aMeshName<<
                  "'; myEntity = "<<anEntity<<"; myTime = "<<aDT);
      }      
    }
  }
  return this; 
}
 

//---------------------------------------------------------------
VISU_Convertor* 
VISU_MEDConvertor
::Build(SALOMEDS::ChildIterator_ptr theTimeStampIterator)
{
  if(theTimeStampIterator->_is_nil()) return NULL;
  for(; theTimeStampIterator->More(); theTimeStampIterator->Next()){
    SALOMEDS::SObject_var aTimeStampSObj = theTimeStampIterator->Value();
    if(MYDEBUG) MESSAGE("VISU_MEDConvertor::Build - aTimeStampSObj = '"<<GetSObjectName(aTimeStampSObj)<<"'");

    CORBA::Object_var aMedField = VISU::SObjectToObject(aTimeStampSObj);
    if(CORBA::is_nil(aMedField)) 
      continue;

    SALOME_MED::FIELD_var aMEDField = SALOME_MED::FIELD::_narrow(aMedField);
    if(aMEDField->_is_nil()) 
      continue;

    SALOME_MED::SUPPORT_var aMEDSupport = aMEDField->getSupport();
    if(aMEDSupport->_is_nil()) 
      continue;

    SALOME_MED::medEntityMesh aMEntity = aMEDSupport->getEntity();
    VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
    SALOME_MED::MESH_var aMEDMesh = aMEDSupport->getMesh()->convertInMESH();
    if(aMEDMesh->_is_nil()) continue;
    CORBA::String_var aMeshName = aMEDMesh->getName();
    CORBA::String_var aFieldName = aMEDField->getName();

    VISU::PCMesh aMesh;
    VISU::TMeshMap::const_iterator aMeshMapIter = myMeshMap.find(aMeshName.in());
    if(aMeshMapIter == myMeshMap.end()){
      aMesh.reset(new VISU::TCMesh());
      aMesh->myNamedPointCoords(new VISU::TNamedPointCoords());
      aMesh->myNbPoints = aMEDMesh->getNumberOfNodes();
      aMesh->myDim = aMEDMesh->getSpaceDimension();
      aMesh->myName = aMeshName.in();
      aMesh->myMesh = aMEDMesh;
      
      myMeshMap[aMeshName.in()] = aMesh;

      if(MYDEBUG) 
        MESSAGE("VISU_MEDConvertor::Build "<<
                "- aMeshName = '"<<aMeshName<<"'"<<
                "; aDim = "<<aMesh->myDim);
    }else
      aMesh = aMeshMapIter->second;

    VISU::PCMeshOnEntity aMeshOnEntity;
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
    VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter = aMeshOnEntityMap.find(aVEntity);
    if(aMeshOnEntityMapIter == aMeshOnEntityMap.end()){
      aMeshOnEntity.reset(new VISU::TCMeshOnEntity());
      aMeshOnEntity->myEntity = aVEntity;
      aMeshOnEntity->myMeshName = aMeshName.in();
      aMeshOnEntity->mySupport = aMEDSupport;
      aMeshOnEntityMap[aVEntity] = aMeshOnEntity;
    }else
      aMeshOnEntity = aMeshOnEntityMapIter->second;

    if(aVEntity == VISU::NODE_ENTITY)
      ::InitMeshOnEntity(aMesh,VISU::CELL_ENTITY,aMeshOnEntity);
    else
      ::InitMeshOnEntity(aMesh,VISU::NODE_ENTITY,aMeshOnEntity);

    ::GetCellsSize(aMesh,aMEDMesh,aVEntity);

    VISU::PCField aField;
    VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
    VISU::TFieldMap::const_iterator aFieldMapIter = aFieldMap.find(aFieldName.in());
    if(aFieldMapIter == aFieldMap.end()){
      aField.reset(new VISU::TCField());
      aField->myId = mySObject->Tag();
      aField->myName = aFieldName.in();
      aField->myEntity = aVEntity;
      aField->myMeshName = aMeshName.in();
      aField->myDataSize = aMeshOnEntity->myNbCells * aField->myNbComp;
      
      vtkIdType aDataType = VTK_DOUBLE;
      SALOME_MED::FIELDDOUBLE_ptr aFieldDouble = SALOME_MED::FIELDDOUBLE::_narrow(aMEDField);
      if(aFieldDouble->_is_nil()){
        if (sizeof(long) == 4 ) // Size of CORBA::Long is always 4 (see CORBA_basetypes.h)
          //Fix for IPAL20325 Crash on create presentation for imported field 
          aDataType = VTK_LONG;
        else if (sizeof(int) == 4)
          aDataType = VTK_INT;
        else {
          throw std::runtime_error("Can't map CORBA::Long to a VTK type");
        }
      }
      aField->Init(aMEDField->getNumberOfComponents(), aDataType);

      aFieldMap[aFieldName.in()] = aField;

      if(MYDEBUG) 
        MESSAGE("VISU_MEDConvertor::Build - aMeshOnEntity->myNbCells = "<<aMeshOnEntity->myNbCells);
    }else
      aField = aFieldMapIter->second;

    VISU::TValField& aValField = aField->myValField;
    int anId = aMEDField->getIterationNumber();
    VISU::PCValForTime aValForTime = aValField[anId](new VISU::TCValForTime());
    aValForTime->myId = anId;
    CORBA::Double aDT = aMEDField->getTime();
    aValForTime->myTime = VISU::TTime(aDT,"");
    aValForTime->myField = aMEDField;
    if(MYDEBUG) 
      MESSAGE("VISU_MEDConvertor::Build "<<
              "- aMeshName = '"<<aMeshName<<"'"<<
              "; myEntity = "<<aVEntity<<
              "; myTime = "<<aDT<<
              "; anId = "<<anId);
  }
  return this; 
}


//---------------------------------------------------------------
int
VISU_MEDConvertor
::LoadMeshOnEntity(VISU::PMeshImpl theMesh,
                   VISU::PMeshOnEntityImpl theMeshOnEntity)
{
  int anIsUpdated = LoadPoints(theMesh);
  const VISU::TEntity& aVEntity = theMeshOnEntity->myEntity;
  if(aVEntity != VISU::NODE_ENTITY)
    anIsUpdated |= LoadCellsOnEntity(theMesh,theMeshOnEntity);

  return anIsUpdated;
}
 
 
//---------------------------------------------------------------
int
VISU_MEDConvertor
::LoadFamilyOnEntity(VISU::PMeshImpl theMesh,
                     VISU::PMeshOnEntityImpl theMeshOnEntity, 
                     VISU::PFamilyImpl theFamily)
{
  int anIsUpdated = LoadPoints(theMesh);
  const VISU::TEntity& anEntity = theMeshOnEntity->myEntity;
  if(anEntity == VISU::NODE_ENTITY){
    anIsUpdated |= LoadPointsOnFamily(theMesh,theFamily);
  }else{
    anIsUpdated |= LoadCellsOnEntity(theMesh,theMeshOnEntity);
    anIsUpdated |= LoadCellsOnFamily(theMesh,theMeshOnEntity,theFamily);
  }

  return anIsUpdated;
}


//---------------------------------------------------------------
int 
VISU_MEDConvertor
::LoadMeshOnGroup(VISU::PMeshImpl theMesh, 
                  const VISU::TFamilySet& theFamilySet)
{
  //Main part of code
  int anIsUpdated = LoadPoints(theMesh);
  VISU::TFamilySet::const_iterator aFamilyIter = theFamilySet.begin();
  for(; aFamilyIter != theFamilySet.end(); aFamilyIter++){
    VISU::PCFamily aFamily = (*aFamilyIter).second;
    const VISU::TEntity& aVEntity = aFamily->myEntity;
    VISU::PCMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[aVEntity];
    if(aVEntity == VISU::NODE_ENTITY){
      anIsUpdated |= LoadPointsOnFamily(theMesh,aFamily);
    }else{
      anIsUpdated |= LoadCellsOnEntity(theMesh,aMeshOnEntity);
      anIsUpdated |= LoadCellsOnFamily(theMesh,aMeshOnEntity,aFamily);
    }
  }

  return anIsUpdated;
}


//---------------------------------------------------------------
int 
VISU_MEDConvertor
::LoadValForTimeOnMesh(VISU::PMeshImpl theMesh, 
                       VISU::PMeshOnEntityImpl theMeshOnEntity, 
                       VISU::PFieldImpl theField, 
                       VISU::PValForTimeImpl theValForTime)
{
  //Main part of code
  int anIsUpdated = LoadPoints(theMesh);
  const VISU::TEntity& aVEntity = theMeshOnEntity->myEntity;
  if(aVEntity != VISU::NODE_ENTITY)
    anIsUpdated |= LoadCellsOnEntity(theMesh,theMeshOnEntity);

  anIsUpdated |= LoadField(theMesh,theMeshOnEntity,theField,theValForTime);
  
  return anIsUpdated;
}


//---------------------------------------------------------------
int 
VISU_MEDConvertor
::LoadPoints(VISU::PCMesh theMesh)
{
  //Check on existing family
  VISU::PCMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[VISU::NODE_ENTITY];
  
  //Check on loading already done
  if(theMesh->myIsDone) 
    return 0;
  
  SALOME_MED::MESH_var& aMedMesh = theMesh->myMesh;
  vtkIdType aDim = theMesh->GetDim();
  vtkIdType aNbElem = theMesh->GetNbPoints();

  if(MYDEBUG) MESSAGE("LoadPoints - aNbElem = "<<aNbElem);

  if(aNbElem <= 0) 
    throw std::runtime_error("LoadPoints >> There is no points in the mesh !!!");

  SALOME_TYPES::ListOfDouble_var aCCoord = aMedMesh->getCoordinates(SALOME_MED::MED_FULL_INTERLACE);
  VISU::TCMEDCoordHolder* aCoordHolder = new VISU::TCMEDCoordHolder();
  aCoordHolder->Init(aNbElem, aDim, aCCoord);

  VISU::TNamedPointCoords& aCoords = theMesh->myNamedPointCoords;
  aCoords.Init(VISU::PCoordHolder(aCoordHolder));
  
  if(MYDEBUG) MESSAGE("LoadPoints - Filling aMeshOnEntity with type NODE_ENTITY");
  
  VISU::TGeom2SubMesh& aGeom2SubMesh = aMeshOnEntity->myGeom2SubMesh;
  VISU::PSubMeshImpl aSubMesh = aGeom2SubMesh[VISU::ePOINT1](new VISU::TCSubMesh());

  aSubMesh->myNbCells = theMesh->myNbPoints;
  aSubMesh->myCellsSize = 2*theMesh->myNbPoints;

  VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
  aCell2Connect.resize(aNbElem);
  if(MYDEBUG) MESSAGE("LoadPoints - aNbElem="<<aNbElem);
  for(int iElem = 0; iElem < aNbElem; iElem++)
    aCell2Connect[iElem] = VISU::TConnect(1,iElem);
  
  theMesh->myIsDone = true;

  return 1;
}


//---------------------------------------------------------------
int 
VISU_MEDConvertor
::LoadPointsOnFamily(VISU::PCMesh theMesh, 
                     VISU::PCFamily theFamily)
{
  VISU::PCMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[VISU::NODE_ENTITY];

  if(theFamily->myIsDone) 
    return 0;

  vtkIdType aNbElem = theMesh->GetNbPoints();
  SALOME_MED::FAMILY_var aMedFamily = theFamily->myFamily;
  CORBA::Boolean anIsOnAllElements = aMedFamily->isOnAllElements();
  VISU::TSubMeshID& aSubMeshID = theFamily->myGeom2SubMeshID[VISU::ePOINT1];
  
  if(!anIsOnAllElements){
    SALOME_MED::medGeometryElement_array_var aGeom = aMedFamily->getTypes();
    SALOME_TYPES::ListOfLong_var aCellNumForType = aMedFamily->getNumber(aGeom[0]);
    int aSize = aNbElem;
    aNbElem = aCellNumForType->length();
    for(int iElem = 0; iElem < aNbElem; iElem++){
      int anID = aCellNumForType[iElem] - 1;
      if(0 > anID || anID >= aSize){
        static QString aString;
        aString.sprintf("LoadPointsOnFamily - aSize(%d) <= aCellNumForType[%d] = %d < 0",aSize,iElem,anID);
        throw std::runtime_error((const char*)aString.toLatin1());
      }
      aSubMeshID.push_back(anID);
    }
  }else{
    for(int iElem = 0; iElem < aNbElem; iElem++){
      aSubMeshID.push_back(iElem);
    }
  }
  
  theFamily->myIsDone = true;
  
  return 1;
}

//---------------------------------------------------------------
namespace 
{
  typedef MED::TCSlice<int> TA;
  //---------------------------------------------------------------
  typedef std::set<int> TConnSet;
  typedef std::vector<int> TIntArray;
  typedef MED::TCSlice<int> TConnSlice;

  //---------------------------------------------------------------
  class MEDPolygonConnectivity //! retriver of polygon connectivity
  {
    TIntArray myConn;
    TIntArray myConnIndex;
  public:

    MEDPolygonConnectivity(SALOME_MED::MESH_var theMesh,
                           SALOME_MED::medEntityMesh theEntity) 
    {
      {
        SALOME::SenderInt_var aSender = 
          theMesh->getSenderForConnectivity(SALOME_MED::MED_NODAL, theEntity, SALOME_MED::MED_POLYGON);
        long int aSize;
        int* aValuePtr = ReceiverFactory::getValue(aSender.in(), aSize);
        myConn.assign(aValuePtr, aValuePtr + aSize);
      }
      {
        SALOME::SenderInt_var aSender = 
          theMesh->getSenderForConnectivityIndex(SALOME_MED::MED_NODAL, theEntity, SALOME_MED::MED_POLYGON);
        long int aSize;
        int* aValuePtr = ReceiverFactory::getValue(aSender, aSize);
        myConnIndex.assign(aValuePtr, aValuePtr + aSize);

        // since the connectivity indices for polygones are stored together with other elements
        // we should cut the indices by a value of the first one
        if( aSize > 0 )
        {
          int anOffset = myConnIndex.front() - 1;
          for( int anIndex = 0; anIndex < aSize; anIndex++ )
            myConnIndex[ anIndex ] -= anOffset;
        }
      }
    }

    TConnSlice 
    GetConn(int theId) const 
    {
      int anOffSet = myConnIndex[theId] - 1;
      int aSize = myConnIndex[theId + 1] - myConnIndex[ theId ];
      return TConnSlice(&myConn[0], myConn.size(), std::slice(anOffSet, aSize, 1));
    }

    int 
    GetNbElem() const 
    {
      return myConnIndex.size() - 1;
    }

    int
    GetCellSize() const
    {
      return myConn.size() + GetNbElem();
    }
  };


  //---------------------------------------------------------------
  class MEDPolyhedraConnectivity //! retriver of polyhedron connectivity
  {
    TIntArray myConn;
    TIntArray myConnIndex;
    TIntArray myFaceIndex;
  public:

    MEDPolyhedraConnectivity(SALOME_MED::MESH_var theMesh,
                             SALOME_MED::medEntityMesh theEntity)
    {
      {
        SALOME::SenderInt_var aSender = 
          theMesh->getSenderForConnectivity(SALOME_MED::MED_NODAL, theEntity, SALOME_MED::MED_POLYHEDRA);
        long int aSize;
        int* pt = ReceiverFactory::getValue(aSender, aSize);
        //
        SALOME::SenderInt_var aSender2 = 
          theMesh->getSenderForConnectivityIndex(SALOME_MED::MED_NODAL, theEntity, SALOME_MED::MED_POLYHEDRA);
        long int aSizeI;
        int* ptI = ReceiverFactory::getValue(aSender, aSize);
        //
        int nbOfPolyHCells=aSizeI-1;
        std::vector<int> polh1v(aSizeI);
        polh1v[0]=1;
        std::vector<int> polh2v,polh3v;
        for(int i=0;i<nbOfPolyHCells;i++)
          {
            int nbOfFaces=std::count(pt+ptI[i]-1,pt+ptI[i+1]-1,-1)+1;
            polh1v[i+1]=polh1v[i]+nbOfFaces;
            int *work=pt+ptI[i]-1;
            for(int j=0;j<nbOfFaces;j++)
              {
                int *work2=std::find(work,pt+ptI[i+1]-1,-1);
                int connLgthOfFace=std::distance(work,work2);
                polh2v.push_back(connLgthOfFace);
                polh3v.insert(polh3v.end(),work,work2);
                work=work2+1;
              }
          }
        //
        myConn.assign(polh3v.begin(),polh3v.end());
        myFaceIndex.assign(polh2v.begin(),polh2v.end());
        myConnIndex.assign(polh1v.begin(),polh1v.end());
      }
    }

    int
    GetUniqueConn(int theId, 
                  TConnSet& theConnSet) const 
    {
      theConnSet.clear();
      int aStartFaceId = myConnIndex[theId] - 1;
      int anEndFaceId = myConnIndex[theId + 1] - 2;
      int aStartConnId = myFaceIndex[aStartFaceId] - 1;
      int anEndConnId = myFaceIndex[anEndFaceId + 1] - 1;
      for(int aConnId = aStartConnId; aConnId < anEndConnId; aConnId++)
        theConnSet.insert(myConn[aConnId]);
      return theConnSet.size();
    }

    int
    GetNbElem() const
    {
      return myConnIndex.size() - 1;
    }

    int
    GetCellSize() const
    {
      TConnSet aConnSet;
      int aCellSize = 0;
      for(int anElemId = 0; anElemId < GetNbElem(); anElemId++)
        aCellSize += GetUniqueConn(anElemId, aConnSet);
      return aCellSize;
    }
  };
}

//---------------------------------------------------------------
int 
VISU_MEDConvertor
::LoadCellsOnEntity(VISU::PCMesh theMesh,
                    VISU::PCMeshOnEntity theMeshOnEntity)
{
  if(theMeshOnEntity->myIsDone) 
    return 0;

  SALOME_MED::SUPPORT_var& aMedSupport = theMeshOnEntity->mySupport;
  SALOME_MED::MESH_var aMedMesh = aMedSupport->getMesh()->convertInMESH();

  //Main part of code
  const VISU::TEntity& aVEntity = theMeshOnEntity->myEntity;
  const SALOME_MED::medEntityMesh& aMEntity = VTKEntityToMED(aVEntity);
  VISU::TCellsFirstIndex& aFirstIndex = theMeshOnEntity->myCellsFirstIndex;

  SALOME_MED::MESH::connectivityInfos_var anInfo = aMedMesh->getConnectGlobal(aMEntity);
  int iGeomEnd = anInfo->meshTypes.length();

  VISU::TGeom2SubMesh& aGeom2SubMesh = theMeshOnEntity->myGeom2SubMesh;
  vtkIdType aNbPoints = theMesh->GetNbPoints();

  for(int iGeom = 0, aCounter = 0; iGeom < iGeomEnd; iGeom++) {
    SALOME_MED::medGeometryElement aMGeom = anInfo->meshTypes[iGeom];
    VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
    switch (aMGeom) {
    case SALOME_MED::MED_POLYGON:
    {
      MEDPolygonConnectivity aConn(aMedMesh, aMEntity);
      int aNbElem = aConn.GetNbElem();
      if (aNbElem > 0) {
        VISU::PSubMeshImpl aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TCSubMesh());
        aSubMesh->myNbCells   = aNbElem;
        aSubMesh->myCellsSize = aConn.GetCellSize();

        VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
        aCell2Connect.resize(aNbElem);

        for(int iElem = 0; iElem < aNbElem; iElem++) {
          TConnSlice aConnSlice = aConn.GetConn(iElem);
          VISU::TConnect& anArray = aCell2Connect[iElem];
          anArray.resize(aConnSlice.size());
          for(int iConn = 0; iConn < aConnSlice.size(); iConn++)
            anArray[iConn] = aConnSlice[iConn] - 1;
        }
      }
    }
    break;
    case SALOME_MED::MED_POLYHEDRA:
    {
      MEDPolyhedraConnectivity aConn( aMedMesh, aMEntity );
      int aNbElem = aConn.GetNbElem();
      if (aNbElem > 0) {
        VISU::PSubMeshImpl aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TCSubMesh());
        aSubMesh->myNbCells = aNbElem;
        aSubMesh->myCellsSize = aConn.GetCellSize();

        VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
        aCell2Connect.resize(aNbElem);

        TConnSet aConnectSet;
        for(int iElem = 0; iElem < aNbElem; iElem++){
          if(aConn.GetUniqueConn(iElem, aConnectSet)){
            int aNbConn = aConnectSet.size();
            VISU::TConnect& anArray = aCell2Connect[iElem];
            anArray.resize(aNbConn);
            std::set<int>::iterator anIter = aConnectSet.begin();
            for(int i = 0; anIter != aConnectSet.end(); anIter++, i++)
              anArray[i] = *anIter - 1;
          }
        }
      }
    }
    break;
    default:
    {        
      int aMNbNodes = MEDGeom2NbNodes(aMGeom);
      int aVNbNodes = VISUGeom2NbNodes(aEGeom);
      int aNbElem = anInfo->numberOfElements[iGeom];
      if (aNbElem > 0) {
        SALOME_TYPES::ListOfLong_var aConn = 
          aMedMesh->getConnectivity(SALOME_MED::MED_NODAL,
                                    aMEntity,
                                    aMGeom);
        VISU::PSubMeshImpl aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TCSubMesh());

        aSubMesh->myNbCells = aNbElem;
        aSubMesh->myCellsSize = aNbElem*(aVNbNodes+1);

        VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
        std::vector<int> aConnect(aMNbNodes);
        int aNbConnForElem = aConn->length() / aNbElem;

        if(MYDEBUG) MESSAGE("LoadCellsOnEntity - aMGeom = "<<aMGeom<<
                            "; aNbElem = "<<aNbElem<<
                            "; aMNbNodes = "<<aMNbNodes<<
                            "; aVNbNodes = "<<aVNbNodes<<
                            "; aNbConnForElem = "<<aNbConnForElem);

        for(int iElem = 0; iElem < aNbElem; iElem++) {
          VISU::TConnect anArray(aVNbNodes);
          for(int k = 0, kj = iElem*aNbConnForElem; k < aMNbNodes; k++)
            aConnect[k] = aConn[kj+k] - 1;

          switch(aMGeom){
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
          case SALOME_MED::MED_SEG3:
            anArray[0] = aConnect[0];
            anArray[2] = aConnect[1];  

            anArray[1] = aConnect[2];
            break;
#endif
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
          case SALOME_MED::MED_TRIA6:
            anArray[0] = aConnect[0];
            anArray[2] = aConnect[1];  
            anArray[4] = aConnect[2];  

            anArray[1] = aConnect[3];
            anArray[3] = aConnect[4];  
            anArray[5] = aConnect[5];  
            break;
#endif
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
          case SALOME_MED::MED_QUAD8:
            anArray[0] = aConnect[0];
            anArray[2] = aConnect[1];  
            anArray[4] = aConnect[2];  
            anArray[6] = aConnect[3];  

            anArray[1] = aConnect[4];
            anArray[3] = aConnect[5];  
            anArray[5] = aConnect[6];  
            anArray[7] = aConnect[7];  
            break;
#endif
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
          case SALOME_MED::MED_TETRA10 :
#endif
          case SALOME_MED::MED_TETRA4 :
            anArray[0] = aConnect[0];
            anArray[1] = aConnect[1];
            anArray[2] = aConnect[3];  
            anArray[3] = aConnect[2];  
            break;
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
          case SALOME_MED::MED_PYRA13:
#endif
          case SALOME_MED::MED_PYRA5 :
            anArray[0] = aConnect[0];
            anArray[1] = aConnect[3];  
            anArray[2] = aConnect[2];
            anArray[3] = aConnect[1];  
            anArray[4] = aConnect[4];
            break;
          default:
            for (int k = 0; k < aVNbNodes; k++) 
              anArray[k] = aConnect[k];
          }
          for (int k = 0; k < aVNbNodes; k++) 
            if(anArray[k] < 0 || aNbPoints <= anArray[k]){
              static QString aString;
              aString.sprintf("LoadCellsOnEntity >> aNbPoints(%d) <= anArray[%d][%d]=%d < 0 !!!",int(aNbPoints),iElem,k,anArray[k]);
              throw std::runtime_error((const char*)aString.toLatin1());
            }
          aCell2Connect.push_back(anArray);
        } // loop on elements
      }
    }} // switch( aMGeom )
    VISU::TGeom2SubMesh::iterator anIter = aGeom2SubMesh.find(aEGeom);
    if(anIter != aGeom2SubMesh.end()){
      const VISU::PSubMeshImpl& aSubMesh = anIter->second;
      const VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
      int aSize = aCell2Connect.size();
      if(MYDEBUG) MESSAGE("LoadCellsOnEntity - aCounter = "<<aCounter<<"; aSize = "<<aSize<<"; aMGeom = "<<aMGeom);
      aFirstIndex[aMGeom] = VISU::TIndexAndSize(aCounter, aSize);
      aCounter += aSize;
    }
  } //loop on types

  // Dump result connectivity
// #ifdef _DEBUG_
//   TGeom2SubMesh::iterator geom_sm = aGeom2SubMesh.begin();
//   for ( ; geom_sm!=aGeom2SubMesh.end(); ++geom_sm ) {
//     cout << "TYPE: " << geom_sm->first << endl;
//     PSubMeshImpl aSubMesh = geom_sm->second;
//     TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
//     TCell2Connect::iterator id_conn = aCell2Connect.begin();
//     for ( int i = 0; id_conn !=aCell2Connect.end(); ++id_conn ) {
//       cout << "\t" << i++ << ": [";
//       TConnect& anArray = *id_conn;
//       TConnect::iterator n = anArray.begin();
//       for ( ; n != anArray.end(); ++n )
//         cout << " " << *n + 1;
//       cout << " ]"<< endl;
//     }
//   }
// #endif

  theMeshOnEntity->myIsDone = true;

  return 1;
}


//---------------------------------------------------------------
int 
VISU_MEDConvertor
::LoadCellsOnFamily(VISU::PCMesh theMesh,
                    VISU::PCMeshOnEntity theMeshOnEntity, 
                    VISU::PCFamily theFamily)
{
  if(theFamily->myIsDone) 
    return 0;

  SALOME_MED::FAMILY_var aMedFamily = theFamily->myFamily;
  CORBA::Boolean anIsOnAllElements = aMedFamily->isOnAllElements();
  if(!anIsOnAllElements){
    SALOME_MED::medGeometryElement_array_var aGeoms = aMedFamily->getTypes();
    int iGeomEnd = aGeoms->length();
    if(MYDEBUG) MESSAGE("LoadCellsOnFamily - iGeomEnd = "<<iGeomEnd);
    for(int iGeom = 0; iGeom < iGeomEnd; iGeom++){
      SALOME_MED::medGeometryElement aMGeom = aGeoms[iGeom];
      SALOME_TYPES::ListOfLong_var aCellNumForType = aMedFamily->getNumber(aMGeom);
      VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);

      int aNbElem = aCellNumForType->length();
      int aCounter = theMeshOnEntity->myCellsFirstIndex[aMGeom].first;
      int aSize = theMeshOnEntity->myCellsFirstIndex[aMGeom].second;
      VISU::TSubMeshID& aSubMeshID = theFamily->myGeom2SubMeshID[aEGeom]; 
      
      if(MYDEBUG) 
        MESSAGE("LoadCellsOnFamily "<<
                "- aMGeom = "<<aMGeom<<
                "; aNbElem = "<<aNbElem<<
                "; aSize = "<<aSize<<
                "; aCounter = "<<aCounter);
      
      for(int iElem = 0; iElem < aNbElem; iElem++){
        int anID = aCellNumForType[iElem] - aCounter - 1;
        if(0 > anID || anID >= aSize){
          static QString aString;
          aString.sprintf("LoadCellsOnFamily - aNbElem(%d) <= aCellNumForType[%d] = %d < 0 !!!",aNbElem,iElem,anID);
          throw std::runtime_error((const char*)aString.toLatin1());
        }
        aSubMeshID.push_back(anID);
      }
    }
  }else{
    const VISU::TGeom2SubMesh& aGeom2SubMesh = theMeshOnEntity->myGeom2SubMesh;
    VISU::TGeom2SubMesh::const_iterator anIter = aGeom2SubMesh.begin();
    for(; anIter != aGeom2SubMesh.end(); anIter++){
      VISU::EGeometry aEGeom = anIter->first;
      const VISU::TSubMeshImpl& aSubMesh = anIter->second;
      const VISU::TCell2Connect& aCell2Connect = aSubMesh.myCell2Connect;
      VISU::TSubMeshID& aSubMeshID = theFamily->myGeom2SubMeshID[aEGeom];
      int iNumElemEnd = aCell2Connect.size();
      for(int iNumElem = 0; iNumElem < iNumElemEnd; iNumElem++)
        aSubMeshID.push_back(iNumElem);
    }
  }
  
  theFamily->myIsDone = true;

  return 1;
}


template<class TValueType,
         class TContainerType> 
void 
ImportField(TContainerType& theContainer, 
            VISU::PCMesh theMesh,
            VISU::PCField theField,
            VISU::PCValForTime theValForTime,
            VISU::PCMeshOnEntity theMeshOnEntity)
{
  typedef VISU::TTCMEDMeshValue<TValueType, TContainerType> TVMeshValue;
  vtkIdType aNbComp = theField->myNbComp;
  if(theField->myEntity == VISU::NODE_ENTITY){
    VISU::EGeometry aEGeom = VISU::ePOINT1;
    vtkIdType aNbGauss = theValForTime->GetNbGauss(aEGeom);
    vtkIdType aNbElem = theMesh->GetNbPoints();

    if(MYDEBUG) MESSAGE("ImportField - aNbElem = "<<aNbElem);

    VISU::PMeshValue& aVMeshValue = theValForTime->GetMeshValue(VISU::ePOINT1);
    TVMeshValue* aMeshValue = new TVMeshValue();
    aMeshValue->Init(aNbElem, aNbGauss, aNbComp, theContainer, 0);
    aVMeshValue.reset(aMeshValue);
  }else{
    SALOME_MED::medGeometryElement* aGeomElems;
    const VISU::TEntity& aVEntity = theField->myEntity;
    int iGeomEnd = GetEntity2Geom(aVEntity,aGeomElems);
    for(int iGeom = 0; iGeom < iGeomEnd; iGeom++){
      SALOME_MED::medGeometryElement aMGeom = aGeomElems[iGeom];
      VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
      vtkIdType aNbGauss = theValForTime->GetNbGauss(aEGeom);
      const VISU::TCellsFirstIndex& aCellsFirstIndex = theMeshOnEntity->myCellsFirstIndex;
      VISU::TCellsFirstIndex::const_iterator aCellsFirstIndexIter = aCellsFirstIndex.find(aMGeom);
      if(aCellsFirstIndexIter != aCellsFirstIndex.end()){
        const VISU::TIndexAndSize& aIndexAndSize = aCellsFirstIndexIter->second;
        if(MYDEBUG) 
          MESSAGE("ImportField - aMGeom = "<<aMGeom<<
                  "; aIndexAndSize = {"<<aIndexAndSize.first<<
                  ","<<aIndexAndSize.second<<"}");

        vtkIdType aNbElem = aIndexAndSize.second;
        vtkIdType aStart = aIndexAndSize.first * aNbComp;
        VISU::PMeshValue& aVMeshValue = theValForTime->GetMeshValue(aEGeom);
        TVMeshValue* aMeshValue = new TVMeshValue();
        aMeshValue->Init(aNbElem, aNbGauss, aNbComp, theContainer, aStart);
        aVMeshValue.reset(aMeshValue);
      }
    }
  }
}

int
VISU_MEDConvertor
::LoadField(VISU::PCMesh theMesh,
            VISU::PCMeshOnEntity theMeshOnEntity,
            VISU::PField theField, 
            VISU::PCValForTime theValForTime)
{
  MESSAGE("VISU_MEDConvertor::LoadField");
  //Check on loading already done
  VISU::PUnstructuredGridIDMapperImpl anUnstructuredGridIDMapper = theValForTime->myUnstructuredGridIDMapper;
  if(anUnstructuredGridIDMapper->myIsVTKDone) 
    return 0;
  
  VISU::PCProfile aProfile(new VISU::TCProfile());
  aProfile->myIsAll = true;
  theValForTime->myProfile = aProfile;

  SALOME_MED::FIELD_var aMEDField = theValForTime->myField;

  SALOME_MED::SUPPORT_var aMEDSupport = aMEDField->getSupport();

  if(aMEDSupport->isOnAllElements()) aProfile->myIsDone = true;
  
  SALOME_MED::FIELDDOUBLE_ptr aFieldDouble = SALOME_MED::FIELDDOUBLE::_narrow(aMEDField);
  if(!aFieldDouble->_is_nil()){
    SALOME_TYPES::ListOfDouble_var anArray = aFieldDouble->getValue(SALOME_MED::MED_FULL_INTERLACE);
    if(MYDEBUG) MESSAGE("VISU_MEDConvertor::LoadField - There is FIELDDOUBLE = "<<anArray->length());
    ImportField<CORBA::Double>(anArray,
                               theMesh,
                               theField,
                               theValForTime,
                               theMeshOnEntity);
  }

  SALOME_MED::FIELDINT_ptr aFieldInt = SALOME_MED::FIELDINT::_narrow(aMEDField);
  if(!aFieldInt->_is_nil()){
    SALOME_TYPES::ListOfLong_var anArray = aFieldInt->getValue(SALOME_MED::MED_FULL_INTERLACE);
    if(MYDEBUG) MESSAGE("VISU_MEDConvertor::LoadField - There is FIELDINT = "<<anArray->length());
    ImportField<CORBA::Long>(anArray,
                             theMesh,
                             theField,
                             theValForTime,
                             theMeshOnEntity);
  }

  anUnstructuredGridIDMapper->myIsVTKDone = true;

  MESSAGE("VISU_MEDConvertor::LoadField done");
  return 1;
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_DeformedShapeAndScalarMap_i.hh
// Author:  Eugeny Nikolaev
// Module : VISU
//
#ifndef VISU_DeformedShapeAndScalarMap_i_HeaderFile
#define VISU_DeformedShapeAndScalarMap_i_HeaderFile

#include "VISU_ScalarMap_i.hh"

class VISU_DeformedShapeAndScalarMapPL;

namespace VISU
{
  //----------------------------------------------------------------------------  
  //! Class of Scalar Map on Deformed Shape presentation.
  class VISU_I_EXPORT DeformedShapeAndScalarMap_i : public virtual POA_VISU::DeformedShapeAndScalarMap,
						    public virtual ScalarMap_i
  {
    static int myNbPresent;
    DeformedShapeAndScalarMap_i(const DeformedShapeAndScalarMap_i&);
    
  public:
    //----------------------------------------------------------------------------     
    typedef ScalarMap_i TSuperClass;
    typedef VISU::DeformedShapeAndScalarMap TInterface;
        
    explicit
    DeformedShapeAndScalarMap_i(EPublishInStudyMode thePublishInStudyModep);
    
    virtual
    ~DeformedShapeAndScalarMap_i();

    virtual
    VISU::VISUType
    GetType()
    { 
      return VISU::TDEFORMEDSHAPEANDSCALARMAP;
    }
    
    virtual 
    void
    SetScale(CORBA::Double theScale);

    virtual
    CORBA::Double 
    GetScale();

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    VISU_DeformedShapeAndScalarMapPL* 
    GetSpecificPL()
    { 
      return myDeformedShapeAndScalarMapPL;
    }
   
  protected:
    //! Redefines VISU_ColoredPrs3d_i::SetField
    virtual
    void
    SetField(VISU::PField theField);

    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

  public:
    //! Redefines VISU_ColoredPrs3d_i::IsPossible
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Redefines VISU_ColoredPrs3d_i::IsPossible
    virtual
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    //! Redefines VISU_ColoredPrs3d_i::ToStream
    virtual
    void
    ToStream(std::ostringstream& theStr);

    //! Redefines VISU_ColoredPrs3d_i::Restore
    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);
    
    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString 
    GenerateName();

    virtual
    const char* 
    GetIconName();
    
    virtual
    VISU_Actor* 
    CreateActor();

    virtual
    void
    UpdateActor(VISU_Actor* theActor) ;

    virtual 
    void
    SetScalarField(VISU::Entity theEntity,
		   const char* theFieldName,
		   CORBA::Long theTimeStampNumber);

    virtual
    VISU::Entity
    GetScalarEntity();

    virtual
    char* 
    GetScalarFieldName();

    virtual
    CORBA::Long
    GetScalarTimeStampNumber();

    virtual 
    VISU::PField
    GetScalarField();

  private:
    VISU_DeformedShapeAndScalarMapPL *myDeformedShapeAndScalarMapPL;

    PField myScalarField;
    VISU::Entity myScalarEntity;
    std::string myScalarFieldName;
    CORBA::Long myScalarTimeStampNumber;
  };
}
#endif

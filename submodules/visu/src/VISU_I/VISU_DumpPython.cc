// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_DumpPython.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Gen_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_PrsObject_i.hh"

#include "VISU_Prs3d_i.hh"
#include "VISU_Mesh_i.hh"
#include "VISU_ScalarMap_i.hh"
#include "VISU_IsoSurfaces_i.hh"
#include "VISU_DeformedShape_i.hh"
#include "VISU_CutPlanes_i.hh"
#include "VISU_CutLines_i.hh"
#include "VISU_CutSegment_i.hh"
#include "VISU_Vectors_i.hh"
#include "VISU_StreamLines_i.hh"
#include "VISU_Plot3D_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_PointMap3d_i.hh"
#include "VISU_GaussPoints_i.hh"
#include "VISU_DeformedShapeAndScalarMap_i.hh"
#include "VISU_ColoredPrs3dCache_i.hh"
#include "VISU_ColoredPrs3dHolder_i.hh"

#include "utilities.h"

#include <cctype>
#include <sstream>
#include <functional>

#include <QString>
#include <QFileInfo>

//#define COUT

using namespace std;

namespace VISU
{
  //---------------------------------------------------------------------------
  static std::string PREFIX("  ");
  
  typedef std::map<std::string,std::string> TName2EntryMap;
  typedef std::map<std::string,std::string> TEntry2NameMap;
  
  
  //---------------------------------------------------------------------------
  inline
  std::string
  GenerateName(std::string theName,
               SALOMEDS::SObject_ptr theSObject,
               TName2EntryMap& theName2EntryMap,
               TEntry2NameMap& theEntry2NameMap,
               char theSuffix)
  {
    if(theName2EntryMap.find(theName) != theName2EntryMap.end()){
      theName = GenerateName(theName + theSuffix, theSObject, theName2EntryMap, theEntry2NameMap, theSuffix);
    }else{
      CORBA::String_var anID = theSObject->GetID();
      theName2EntryMap[theName] = anID.in();
      theEntry2NameMap[anID.in()] = theName;
      //cout<<"GenerateName - "<<theName<<" => "<<anID.in()<<endl;
    }
    
    return theName;
  }
  
  
  //---------------------------------------------------------------------------
  struct TReplacePredicate
  {
    bool operator()(char theChar) const
    {
      return !(isdigit(theChar) || isalpha(theChar) || theChar == '_');
    }
  };
  
  
  //---------------------------------------------------------------------------
  inline
  std::string
  GetName(SALOMEDS::SObject_ptr theSObject)
  {
    CORBA::String_var aString = theSObject->GetName();
    
    std::string aName = QString(aString.in()).simplified().toLatin1().data();
    
    //replace_if(aName.begin(),aName.end(),not1(ptr_fun(isxdigit)),'_');
    replace_if(aName.begin(),aName.end(),TReplacePredicate(),'_');
    
    if ( isdigit( aName[0] ))
      aName.insert( 0, 1, 'a' );
    
    return aName;
  }
  
  
  //---------------------------------------------------------------------------
  inline
  std::string
  GenerateName(SALOMEDS::SObject_ptr theSObject,
               TName2EntryMap& theName2EntryMap,
               TEntry2NameMap& theEntry2NameMap)
  {
    std::string aName = GetName(theSObject);
    
    return GenerateName(aName, theSObject, theName2EntryMap, theEntry2NameMap, 'X');
  }
  
  
  //---------------------------------------------------------------------------
  inline
  std::string
  GetBoolean(bool theArgument)
  {
    if(theArgument)
      return "True";
    
    return "False";
  }
  
  
  //---------------------------------------------------------------------------
  inline
  std::string
  GetColor(const SALOMEDS::Color& theColor)
  {
    std::ostringstream aStream;
    aStream<<"SALOMEDS.Color("<<theColor.R<<", "<<theColor.G<<", "<<theColor.B<<")";
    return aStream.str();
  }
  
  
  //---------------------------------------------------------------------------
  typedef void (*TDumpToPython)(SALOMEDS::Study_ptr theStudy,
                                CORBA::Boolean theIsPublished,
                                CORBA::Boolean& theIsValidScript,
                                SALOMEDS::SObject_ptr theSObject,
                                std::ostream& theStr,
                                TName2EntryMap& theName2EntryMap,
                                TEntry2NameMap& theEntry2NameMap,
                                std::string theArgumentName,
                                std::string thePrefix);
  
  
  void
  DumpToPython(SALOMEDS::Study_ptr theStudy,
               CORBA::Boolean theIsPublished,
               CORBA::Boolean& theIsValidScript,
               SALOMEDS::SObject_ptr theSObject,
               std::ostream& theStr,
               TName2EntryMap& theName2EntryMap,
               TEntry2NameMap& theEntry2NameMap,
               std::string theArgumentName,
               std::string thePrefix);
  
  
  //---------------------------------------------------------------------------
  void SetClippingPlane(Prs3d_i* thePrs, string theName,
                        std::ostream& theStr,
                        std::string thePrefix) 
  {
    VISU_CutPlaneFunction* aPlane;
    VISU_Gen_i* aGen = VISU_Gen_i::GetVisuGenImpl();
    VISU_ClippingPlaneMgr& aMgr = aGen->GetClippingPlaneMgr();
    int aId;
    for (int i = 0; i < thePrs->GetNumberOfClippingPlanes(); i++) {
      aPlane = dynamic_cast<VISU_CutPlaneFunction*>(thePrs->GetClippingPlane(i));
      if (aPlane) {
        if (!aPlane->isAuto()) {
          aId = aMgr.GetPlaneId(aPlane);
          if (aId > -1) {
            theStr<<thePrefix<<"aVisu.ApplyClippingPlane("<<theName<<", "<<aId<<")"<<endl;
          }
        }
      }
    }
  }
  
  
  void
  Prs3dToPython(VISU::Prs3d_i* theServant,
                std::ostream& theStr,
                const std::string& theName,
                std::string thePrefix)
  {
    float x, y, z;
    theServant->GetOffset(x,y,z);
    theStr<<thePrefix<<theName<<".SetOffset("<<x<<", "<<y<<", "<<z<<")"<<endl;
    SetClippingPlane(theServant, theName, theStr, thePrefix);
    
    VISU::MarkerType aMarkerType = theServant->GetMarkerType();
    if( aMarkerType != VISU::MT_NONE ) {
      if( aMarkerType != VISU::MT_USER ) {
        VISU::MarkerScale aMarkerScale = theServant->GetMarkerScale();
        std::string aParam1, aParam2;
        switch( aMarkerType ) {
          case MT_POINT:   aParam1 = "MT_POINT"; break;
          case MT_PLUS:    aParam1 = "MT_PLUS"; break;
          case MT_STAR:    aParam1 = "MT_STAR"; break;
          case MT_O:       aParam1 = "MT_O"; break;
          case MT_X:       aParam1 = "MT_X"; break;
          case MT_O_POINT: aParam1 = "MT_O_POINT"; break;
          case MT_O_PLUS:  aParam1 = "MT_O_PLUS"; break;
          case MT_O_STAR:  aParam1 = "MT_O_STAR"; break;
          case MT_O_X:     aParam1 = "MT_O_X"; break;
          case MT_POINT_SPRITE: aParam1 = "MT_POINT_SPRITE"; break;
          default:         aParam1 = "MT_NONE"; break;
        }
        switch( aMarkerScale ) {
          case MS_10:      aParam2 = "MS_10"; break;
          case MS_15:      aParam2 = "MS_15"; break;
          case MS_20:      aParam2 = "MS_20"; break;
          case MS_25:      aParam2 = "MS_25"; break;
          case MS_30:      aParam2 = "MS_30"; break;
          case MS_35:      aParam2 = "MS_35"; break;
          case MS_40:      aParam2 = "MS_40"; break;
          case MS_45:      aParam2 = "MS_45"; break;
          case MS_50:      aParam2 = "MS_50"; break;
          case MS_55:      aParam2 = "MS_55"; break;
          case MS_60:      aParam2 = "MS_60"; break;
          case MS_65:      aParam2 = "MS_65"; break;
          case MS_70:      aParam2 = "MS_70"; break;
          default:         aParam2 = "MS_NONE"; break;
        }
        if( aMarkerType == MT_POINT_SPRITE )
          aParam2 = "MS_NONE";
        theStr<<thePrefix<<theName<<".SetMarkerStd(VISU."<<aParam1<<", VISU."<<aParam2<<")"<<endl;
      }
      else {
        int aMarkerTexture = theServant->GetMarkerTexture();
        if( aMarkerTexture >= 0 )
          theStr<<thePrefix<<theName<<".SetMarkerTexture(texture_map["<<aMarkerTexture<<"])"<<endl;
      }
    }
  }
  
  //---------------------------------------------------------------------------
  struct TColoredPrs3dFactory
  {
    virtual
    std::string
    operator()(std::ostream& theStr,
               std::string thePrefix)
    {
      
      return thePrefix;
    }
  };
  
  
  struct TCreateFromResult: TColoredPrs3dFactory
  {
    VISU::ColoredPrs3d_i* myServant;
    std::string myConstructorName;
    std::string myArgumentName;
    
    SALOMEDS::SObject_var mySObject;
    std::string myName;
    
    TCreateFromResult(const SALOMEDS::SObject_ptr theSObject,
                      VISU::ColoredPrs3d_i* theServant,
                      const std::string& theName,
                      const std::string& theConstructorName,
                      const std::string& theArgumentName):
                      myServant(theServant),
                      myConstructorName(theConstructorName),
                      myArgumentName(theArgumentName),
                      myName(theName)
    {
      mySObject = SALOMEDS::SObject::_duplicate(theSObject);
    }

    virtual
    std::string
    operator()(std::ostream& theStr,
                std::string thePrefix)
    {
      std::string aParam;
      switch(myServant->GetEntity()){
        case NODE:
          aParam = "VISU.NODE";
          break;
        case EDGE:
          aParam = "VISU.EDGE";
          break;
        case FACE:
          aParam = "VISU.FACE";
          break;
        case CELL:
          aParam = "VISU.CELL";
          break;
      }
      
      theStr<<thePrefix<<myName<<" = aVisu."<<myConstructorName<<"("<<myArgumentName<<
      ", '"<<myServant->GetCMeshName()<<"'"<<
      ", "<<aParam<<
      ", '"<<myServant->GetCFieldName()<<"'"<<
      ", "<<myServant->GetTimeStampNumber()<<
      ")"<<endl;
      
      theStr<<thePrefix<<"if "<<myName<<" != None:"<<endl;
      thePrefix += PREFIX;
      
      // Add to Name->Object map
      theStr<<thePrefix<<"aName2ObjectMap['"<<myName<<"'] = "<<myName<<endl;
      
      // Set name (as this object could be renamed by user)
      CORBA::String_var aNameInStudy = mySObject->GetName();
      theStr<<thePrefix<<"visu.SetName("<<myName<<", '"<<aNameInStudy.in()<<"')"<<endl;
      
      return thePrefix;
    }
  };
  
  
  //---------------------------------------------------------------------------
  std::string
  ColoredPrs3dToPython(SALOMEDS::SObject_ptr theSObject,
                       VISU::ColoredPrs3d_i* theServant,
                       std::ostream& theStr,
                       const std::string& theName,
                       TColoredPrs3dFactory& thePrsFactory,
                       std::string thePrefix)
  {
    thePrefix = thePrsFactory(theStr, thePrefix);
    
    // Set parameters common for all Prs3d objects (offset values)
    Prs3dToPython(theServant, theStr, theName, thePrefix);
    
    theStr<<thePrefix<<theName<<".SetPosition("<<theServant->GetPosX()<<", "<<theServant->GetPosY()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetSize("<<theServant->GetWidth()<<", "<<theServant->GetHeight()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetNbColors("<<theServant->GetNbColors()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetLabels("<<theServant->GetLabels()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetTitle('"<<theServant->GetTitle()<<"')"<<endl;
    
    std::string aParam;
    switch(theServant->GetBarOrientation()){
      case ColoredPrs3dBase::HORIZONTAL:
        aParam = "VISU.ColoredPrs3d.HORIZONTAL";
        break;
      case ColoredPrs3dBase::VERTICAL:
        aParam = "VISU.ColoredPrs3d.VERTICAL";
        break;
    }
    theStr<<thePrefix<<theName<<".SetBarOrientation("<<aParam<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetScalarMode("<<theServant->GetScalarMode()<<")"<<endl;
    
    if(theServant->IsRangeFixed())
      theStr<<thePrefix<<theName<<".SetRange("<<theServant->GetMin()<<", "<<theServant->GetMax()<<")"<<endl;
    else
      theStr<<thePrefix<<theName<<".SetSourceRange()"<<endl;
    
    const VISU::ColoredPrs3d_i::TGroupNames aGroupNames = theServant->GetGroupNames();
    VISU::ColoredPrs3d_i::TGroupNames::const_iterator anIter = aGroupNames.begin();
    for(; anIter != aGroupNames.end(); anIter++){
      const std::string& aGroupName = *anIter;
      theStr<<thePrefix<<theName<<".AddMeshOnGroup('"<<aGroupName<<"')"<<endl;
    }
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  GaussPointsToPython(SALOMEDS::SObject_ptr theSObject,
                      VISU::GaussPoints_i* theServant,
                      std::ostream& theStr,
                      const std::string& theName,
                      TColoredPrs3dFactory& thePrsFactory,
                      std::string thePrefix)
  {
    thePrefix = ColoredPrs3dToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    bool aBoolean = theServant->GetIsActiveLocalScalarBar();
    theStr<<thePrefix<<theName<<".SetIsActiveLocalScalarBar("<<GetBoolean(aBoolean)<<")"<<endl;
    
    if(aBoolean){
      aBoolean = theServant->GetIsDispGlobalScalarBar();
      theStr<<thePrefix<<theName<<".SetIsDispGlobalScalarBar("<<GetBoolean(aBoolean)<<")"<<endl;
    }
    
    theStr<<thePrefix<<theName<<".SetSpacing("<<theServant->GetSpacing()<<")"<<endl;
    
    aBoolean = theServant->GetBiColor();
    theStr<<thePrefix<<theName<<".SetBiColor("<<GetBoolean(aBoolean)<<")"<<endl;
    
    aBoolean = theServant->GetIsDeformed();
    theStr<<thePrefix<<theName<<".SetIsDeformed("<<GetBoolean(aBoolean)<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetScaleFactor("<<theServant->GetScaleFactor()<<")"<<endl;
    
    std::string aParam;
    switch(theServant->GetPrimitiveType()){
      case VISU::GaussPoints::SPRITE :
        aParam = "VISU.GaussPoints.SPRITE";
        break;
      case VISU::GaussPoints::POINT :
        aParam = "VISU.GaussPoints.POINT";
        break;
      case VISU::GaussPoints::SPHERE :
        aParam = "VISU.GaussPoints.SPHERE";
        break;
    }
    theStr<<thePrefix<<theName<<".SetPrimitiveType("<<aParam<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetClamp("<<theServant->GetClamp()<<")"<<endl;
    
    QString aMainTexture = theServant->GetQMainTexture();
    QString anAlphaTexture = theServant->GetQAlphaTexture();
    theStr<<thePrefix<<theName<<".SetTextures('"<<aMainTexture.toLatin1().data()<<"', '"<<anAlphaTexture.toLatin1().data()<<"')"<<endl;
    
    theStr<<thePrefix<<theName<<".SetAlphaThreshold("<<theServant->GetAlphaThreshold()<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetResolution("<<theServant->GetResolution()<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetFaceLimit("<<theServant->GetFaceLimit()<<")"<<endl;
    
    aBoolean = theServant->GetIsColored();
    theStr<<thePrefix<<theName<<".SetIsColored("<<GetBoolean(aBoolean)<<")"<<endl;
    
    if(aBoolean){
      theStr<<thePrefix<<theName<<".SetMinSize("<<theServant->GetMinSize()<<")"<<endl;
      theStr<<thePrefix<<theName<<".SetMaxSize("<<theServant->GetMaxSize()<<")"<<endl;
    }else{
      theStr<<thePrefix<<theName<<".SetColor("<<GetColor(theServant->GetColor())<<")"<<endl;
      theStr<<thePrefix<<theName<<".SetGeomSize("<<theServant->GetGeomSize()<<")"<<endl;
    }
    
    theStr<<thePrefix<<theName<<".SetMagnification("<<theServant->GetMagnification()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetMagnificationIncrement("<<theServant->GetMagnificationIncrement()<<")"<<endl;
    
    std::string aVisible = theServant->IsBarVisible()? "True" : "False";
    theStr<<thePrefix<<theName<<".SetBarVisible("<<aVisible<<")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  ScalarMapToPython(SALOMEDS::SObject_ptr theSObject,
                    VISU::ScalarMap_i* theServant,
                    std::ostream& theStr,
                    const std::string& theName,
                    TColoredPrs3dFactory& thePrsFactory,
                    std::string thePrefix)
  {
    thePrefix = ColoredPrs3dToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    std::string aParam;
    switch(theServant->GetScaling()){
      case LINEAR:
        aParam = "VISU.LINEAR";
        break;
      case LOGARITHMIC:
        aParam = "VISU.LOGARITHMIC";
        break;
    }
    std::string aVisible = theServant->IsBarVisible()? "True" : "False";
    theStr<<thePrefix<<theName<<".SetScaling("<<aParam<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetBarVisible("<<aVisible<<")"<<endl;
    
    switch(theServant->GetGaussMetric()){
      case AVERAGE:
        aParam = "VISU.AVERAGE";
        break;
      case MINIMUM:
        aParam = "VISU.MINIMUM";
        break;
      case MAXIMUM:
        aParam = "VISU.MAXIMUM";
        break;
    }
    theStr<<thePrefix<<theName<<".SetGaussMetric("<<aParam<<")"<<endl;
    
    SALOMEDS::Color aColor = theServant->GetLinkColor();
    theStr<<thePrefix<<theName<<".SetLinkColor(SALOMEDS.Color("<<
    aColor.R<<", "<<aColor.G<<", "<<aColor.B<<"))"<<endl;
    
    if(theServant->IsRangeFixed())
      theStr<<thePrefix<<theName<<".SetRange("<<theServant->GetMin()<<", "<<theServant->GetMax()<<")"<<endl;
    else
      theStr<<thePrefix<<theName<<".SetSourceRange()"<<endl;
    
    return thePrefix;
  }
  
  //---------------------------------------------------------------------------
  std::string
  MonoColorPrsToPython(SALOMEDS::SObject_ptr theSObject,
                       VISU::MonoColorPrs_i* theServant,
                       std::ostream& theStr,
                       const std::string& theName,
                       TColoredPrs3dFactory& thePrsFactory,
                       std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    theStr<<thePrefix<<theName<<".ShowColored("<<GetBoolean(theServant->IsColored())<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetColor("<<GetColor(theServant->GetColor())<<")"<<endl;
    return thePrefix;
  }
  
  //---------------------------------------------------------------------------
  std::string
  DeformedShapeToPython(SALOMEDS::SObject_ptr theSObject,
                        VISU::DeformedShape_i* theServant,
                        std::ostream& theStr,
                        const std::string& theName,
                        TColoredPrs3dFactory& thePrsFactory,
                        std::string thePrefix)
  {
    thePrefix = MonoColorPrsToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    theStr<<thePrefix<<theName<<".SetScale("<<theServant->GetScale()<<")"<<endl;
    //     theStr<<thePrefix<<theName<<".ShowColored("<<GetBoolean(theServant->IsColored())<<")"<<endl;
    //     theStr<<thePrefix<<theName<<".SetColor("<<GetColor(theServant->GetColor())<<")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  StreamLinesToPython(SALOMEDS::SObject_ptr theSObject,
                      VISU::StreamLines_i* theServant,
                      std::ostream& theStr,
                      TEntry2NameMap& theEntry2NameMap,
                      const std::string& theName,
                      TColoredPrs3dFactory& thePrsFactory,
                      std::string thePrefix)
  {
    thePrefix = MonoColorPrsToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    //    thePrefix = DeformedShapeToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    std::string aParam;
    switch(theServant->GetDirection()){
      case StreamLines::FORWARD:
        aParam = "VISU.StreamLines.FORWARD";
        break;
      case StreamLines::BACKWARD:
        aParam = "VISU.StreamLines.BACKWARD";
        break;
      case StreamLines::BOTH:
        aParam = "VISU.StreamLines.BOTH";
        break;
    }
    
    theStr<<thePrefix<<"aPrs3d = None"<<endl;
    VISU::Prs3d_var aPrs3d = theServant->GetSource();
    if(!CORBA::is_nil(aPrs3d)){
      if(Prs3d_i* aServant3d = dynamic_cast<Prs3d_i*>(GetServant(aPrs3d).in())){
        SALOMEDS::SObject_var aSObject = aServant3d->GetSObject();
        CORBA::String_var anID = aSObject->GetID();
        std::string anArg = theEntry2NameMap[anID.in()];
        theStr<<thePrefix<<"if aName2ObjectMap.has_key('"<<anArg<<"'):"<<endl;
        thePrefix += PREFIX;
        theStr<<thePrefix<<"aPrs3d = aName2ObjectMap['"<<anArg<<"']"<<endl;
      }
    }
    
    theStr<<thePrefix<<theName<<".SetParams("<<
    theServant->GetIntegrationStep()<<", "<<
    theServant->GetPropagationTime()<<", "<<
    theServant->GetStepLength()<<", "<<
    "aPrs3d"<<", "<<
    theServant->GetUsedPoints()<<", "<<
    aParam<<
    ")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  DeformedShapeAndScalarMapToPython(SALOMEDS::SObject_ptr theSObject,
                                    VISU::DeformedShapeAndScalarMap_i* theServant,
                                    std::ostream& theStr,
                                    const std::string& theName,
                                    TColoredPrs3dFactory& thePrsFactory,
                                    std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    theStr<<thePrefix<<theName<<".SetRange("<<theServant->GetMin()<<", "<<theServant->GetMax()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetScale("<<theServant->GetScale()<<")"<<endl;
    
    std::string aParam;
    VISU::Entity anEntity = theServant->GetScalarEntity();
    switch(anEntity){
      case NODE:
        aParam = "VISU.NODE";
        break;
      case EDGE:
        aParam = "VISU.EDGE";
        break;
      case FACE:
        aParam = "VISU.FACE";
        break;
      case CELL:
        aParam = "VISU.CELL";
        break;
    }
    
    CORBA::String_var aFieldName = theServant->GetScalarFieldName();
    CORBA::Long aTimeStampNumber = theServant->GetScalarTimeStampNumber();
    
    theStr<<thePrefix<<theName<<".SetScalarField("<<
    aParam<<", "<<
    "'"<<aFieldName<<"', "<<
    aTimeStampNumber<<
    ")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  VectorsToPython(SALOMEDS::SObject_ptr theSObject,
                  VISU::Vectors_i* theServant,
                  std::ostream& theStr,
                  const std::string& theName,
                  TColoredPrs3dFactory& thePrsFactory,
                  std::string thePrefix)
  {
    thePrefix = DeformedShapeToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    theStr<<thePrefix<<theName<<".SetLineWidth("<<theServant->GetLineWidth()<<")"<<endl;
    
    std::string aParam;
    switch(theServant->GetGlyphType()){
      case Vectors::ARROW:
        aParam = "VISU.Vectors.ARROW";
        break;
      case Vectors::CONE2:
        aParam = "VISU.Vectors.CONE2";
        break;
      case Vectors::CONE6:
        aParam = "VISU.Vectors.CONE6";
        break;
      case Vectors::NONE:
        aParam = "VISU.Vectors.NONE";
        break;
    }
    theStr<<thePrefix<<theName<<".SetGlyphType("<<aParam<<")"<<endl;
    
    switch(theServant->GetGlyphPos()){
      case Vectors::CENTER:
        aParam = "VISU.Vectors.CENTER";
        break;
      case Vectors::TAIL:
        aParam = "VISU.Vectors.TAIL";
        break;
      case Vectors::HEAD:
        aParam = "VISU.Vectors.HEAD";
        break;
    }
    theStr<<thePrefix<<theName<<".SetGlyphPos("<<aParam<<")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  IsoSurfacesToPython(SALOMEDS::SObject_ptr theSObject,
                      VISU::IsoSurfaces_i* theServant,
                      std::ostream& theStr,
                      const std::string& theName,
                      TColoredPrs3dFactory& thePrsFactory,
                      std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    theStr<<thePrefix<<theName<<".SetNbSurfaces("<<theServant->GetNbSurfaces()<<")"<<endl;
    theStr<<thePrefix<<theName<<".ShowLabels("<<theServant->IsLabeled()<<","<<theServant->GetNbLabels()<<")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  CutPlanesToPython(SALOMEDS::SObject_ptr theSObject,
                    VISU::CutPlanes_i* theServant,
                    std::ostream& theStr,
                    const std::string& theName,
                    TColoredPrs3dFactory& thePrsFactory,
                    std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    std::string aParam;
    switch(theServant->GetOrientationType()){
      case CutPlanes::XY:
        aParam = "VISU.CutPlanes.XY";
        break;
      case CutPlanes::YZ:
        aParam = "VISU.CutPlanes.YZ";
        break;
      case CutPlanes::ZX:
        aParam = "VISU.CutPlanes.ZX";
        break;
    }
    theStr<<thePrefix<<theName<<".SetOrientation("<<aParam<<", "<<theServant->GetRotateX()<<", "<<theServant->GetRotateY()<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetDisplacement("<<theServant->GetDisplacement()<<")"<<endl;
    CORBA::Long aNbPlanes = theServant->GetNbPlanes();
    theStr<<thePrefix<<theName<<".SetNbPlanes("<<aNbPlanes<<")"<<endl;
    
    for(CORBA::Long anId = 0; anId < aNbPlanes; anId++){
      if(!theServant->IsDefault(anId))
        theStr<<thePrefix<<theName<<".SetPlanePosition("<<anId<<", "<<theServant->GetPlanePosition(anId)<<")"<<endl;
    }
    
    theStr<<thePrefix<<theName<<".UseDeformation("<<GetBoolean(theServant->IsDeformed())<<")"<<endl;
    if(theServant->IsDeformed()){
      theStr<< thePrefix << theName << ".SetScale(" << theServant->GetScale()<<")"<<endl;
      std::string aStringEntity;
      VISU::Entity anEntity = theServant->GetVectorialFieldEntity();
      switch(anEntity){
        case NODE:
          aStringEntity = "VISU.NODE";
          break;
        case EDGE:
          aStringEntity = "VISU.EDGE";
          break;
        case FACE:
          aStringEntity = "VISU.FACE";
          break;
        case CELL:
          aStringEntity = "VISU.CELL";
          break;
      }
      theStr<< thePrefix << theName << ".SetVectorialField("<<aStringEntity<<", '" << theServant->GetVectorialFieldName() <<"')"<<endl;
    }
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  CutLinesToPython(SALOMEDS::SObject_ptr theSObject,
                   VISU::CutLines_i* theServant,
                   std::ostream& theStr,
                   const std::string& theName,
                   TColoredPrs3dFactory& thePrsFactory,
                   std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    std::string aParam;
    switch(theServant->GetOrientationType()){
      case CutPlanes::XY:
        aParam = "VISU.CutPlanes.XY";
        break;
      case CutPlanes::YZ:
        aParam = "VISU.CutPlanes.YZ";
        break;
      case CutPlanes::ZX:
        aParam = "VISU.CutPlanes.ZX";
        break;
    }
    theStr<<thePrefix<<theName<<".SetOrientation("<<aParam<<", "<<theServant->GetRotateX()<<", "<<theServant->GetRotateY()<<")"<<endl;
    
    switch(theServant->GetOrientationType2()){
      case CutPlanes::XY:
        aParam = "VISU.CutPlanes.XY";
        break;
      case CutPlanes::YZ:
        aParam = "VISU.CutPlanes.YZ";
        break;
      case CutPlanes::ZX:
        aParam = "VISU.CutPlanes.ZX";
        break;
    }
    theStr<<thePrefix<<theName<<".SetOrientation2("<<aParam<<", "<<theServant->GetRotateX2()<<", "<<theServant->GetRotateY2()<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetDisplacement("<<theServant->GetDisplacement()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetDisplacement2("<<theServant->GetDisplacement2()<<")"<<endl;
    
    if(!theServant->IsDefault())
      theStr<<thePrefix<<theName<<".SetBasePlanePosition("<<theServant->GetBasePlanePosition()<<")"<<endl;
    
    CORBA::Boolean aUseAbsLength = theServant->IsUseAbsoluteLength();
    theStr<<thePrefix<<theName<<".SetUseAbsoluteLength("<<aUseAbsLength<<")"<<endl;
    
    CORBA::Long aNbLines = theServant->GetNbLines();
    theStr<<thePrefix<<theName<<".SetNbLines("<<aNbLines<<")"<<endl;
    for(CORBA::Long anId = 0; anId < aNbLines; anId++){
      if(!theServant->IsDefaultPosition(anId))
        theStr<<thePrefix<<theName<<".SetLinePosition("<<anId<<", "<<theServant->GetLinePosition(anId)<<")"<<endl;
    }
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  CutSegmentToPython(SALOMEDS::SObject_ptr theSObject,
                     VISU::CutSegment_i* theServant,
                     std::ostream& theStr,
                     const std::string& theName,
                     TColoredPrs3dFactory& thePrsFactory,
                     std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    double x1, y1, z1, x2, y2, z2;
    theServant->GetPoint1(x1, y1, z1);
    theServant->GetPoint2(x2, y2, z2);
    theStr<<thePrefix<<theName<<".SetPoint1("<<x1<<", "<<y1<<", "<<z1<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetPoint2("<<x2<<", "<<y2<<", "<<z2<<")"<<endl;
    
    CORBA::Boolean aUseAbsLength = theServant->IsUseAbsoluteLength();
    theStr<<thePrefix<<theName<<".SetUseAbsoluteLength("<<aUseAbsLength<<")"<<endl;
    
    return thePrefix;
  }
  
  
  //---------------------------------------------------------------------------
  std::string
  Plot3DToPython(SALOMEDS::SObject_ptr theSObject,
                 VISU::Plot3D_i* theServant,
                 std::ostream& theStr,
                 const std::string& theName,
                 TColoredPrs3dFactory& thePrsFactory,
                 std::string thePrefix)
  {
    thePrefix = ScalarMapToPython(theSObject, theServant, theStr, theName, thePrsFactory, thePrefix);
    theStr<<thePrefix<<endl;
    
    std::string aParam;
    switch(theServant->GetOrientationType()){
      case CutPlanes::XY: 
        aParam = "VISU.Plot3D.XY"; 
        break;
      case CutPlanes::YZ: 
        aParam = "VISU.Plot3D.YZ"; 
        break;
      case CutPlanes::ZX: 
        aParam = "VISU.Plot3D.ZX"; 
        break;
    }
    theStr<<thePrefix<<theName<<".SetOrientation("<<aParam<<", "<<theServant->GetRotateX()<<", "<<theServant->GetRotateY()<<")"<<endl;
    
    theStr<<thePrefix<<theName<<".SetPlanePosition("<<theServant->GetPlanePosition()<<", "<<theServant->IsPositionRelative()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetScaleFactor("<<theServant->GetScaleFactor()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetContourPrs("<<theServant->GetIsContourPrs()<<")"<<endl;
    theStr<<thePrefix<<theName<<".SetNbOfContours("<<theServant->GetNbOfContours()<<")"<<endl;
    
    return thePrefix;
  }
  
  //---------------------------------------------------------------------------
  // declaration
  void DumpChildrenToPython(SALOMEDS::Study_ptr theStudy,
                            CORBA::Boolean theIsPublished,
                            CORBA::Boolean& theIsValidScript,
                            SALOMEDS::SObject_ptr theSObject,
                            std::ostream& theStr,
                            TName2EntryMap& theName2EntryMap,
                            TEntry2NameMap& theEntry2NameMap,
                            std::string theArgumentName,
                            std::string thePrefix);
  
  //---------------------------------------------------------------------------
  template<class TTableAttr>
  void
  TableAttrToPython(SALOMEDS::Study_ptr theStudy,
                    CORBA::Boolean theIsPublished,
                    CORBA::Boolean& theIsValidScript,
                    SALOMEDS::SObject_ptr theSObject,
                    TTableAttr theTableAttr,
                    const std::string& theAttrName,
                    std::ostream& theStr,
                    TName2EntryMap& theName2EntryMap,
                    TEntry2NameMap& theEntry2NameMap,
                    std::string theArgumentName,
                    std::string thePrefix)
  {
    SALOMEDS::GenericAttribute_var anAttr;
    SALOMEDS::SObject_var aFatherSObject = theSObject->GetFather();
    if(aFatherSObject->FindAttribute(anAttr,"AttributeString")){
      SALOMEDS::AttributeString_var aComment =
      SALOMEDS::AttributeString::_narrow(anAttr);
      CORBA::String_var aValue = aComment->Value();
      Storable::TRestoringMap aMap;
      Storable::StringToMap(aValue.in(),aMap);
      bool anIsExist;
      QString aMethodName = VISU::Storable::FindValue(aMap,"myComment",&anIsExist);
      if(anIsExist){
        if(strcmp(aMethodName.toLatin1().data(),"ImportTables") == 0){
          return;
        }
      }
    }
    
    std::string aSObjectName = GetName(theSObject);
    if(theArgumentName.empty())
      theArgumentName = "aSComponent";
    theStr<<thePrefix<<aSObjectName<<" = aBuilder.NewObject("<<theArgumentName<<")"<<endl;
    theStr<<thePrefix<<"if "<<aSObjectName<<":"<<endl;
    thePrefix += PREFIX;
    
    theStr<<thePrefix<<"aBuilder.SetName("<<aSObjectName<<", \""<<theSObject->GetName()<<"\")"<<endl; // Fix for IPAL13165
    std::string aName = "aTableAttr";
    theStr<<thePrefix<<aName<<" = aBuilder.FindOrCreateAttribute("<<
    aSObjectName<<", '"<<theAttrName<<"')"<<endl;
    
    theStr<<thePrefix<<"if "<<aName<<":"<<endl;
    std::string aPrefix = thePrefix;
    thePrefix += PREFIX;
    
    CORBA::String_var aString = theTableAttr->GetTitle();
    theStr<<thePrefix<<aName<<".SetTitle('"<<aString.in()<<"')"<<endl;
    
    CORBA::Long aNbColumns = theTableAttr->GetNbColumns();
    theStr<<thePrefix<<aName<<".SetNbColumns("<<aNbColumns<<")"<<endl;
    
    CORBA::Long aNbRows = theTableAttr->GetNbRows();
    
    // push values and their indices into streams
    stringstream values, rows, columns;
    string comma = "";
    for(CORBA::Long i = 1; i <= aNbColumns; i++){
      for(CORBA::Long j = aNbRows; j > 0; j--){
        if(theTableAttr->HasValue(j,i)){
          values  << comma << theTableAttr->GetValue(j,i);
          rows    << comma << j;
          columns << comma << i;
          if ( comma.empty() )
            comma = ",";
        }
      }
    }
    // push titles and units into streams
    stringstream rowUnits, rowTitles, colTitles;
    SALOMEDS::StringSeq_var aRowUnits = theTableAttr->GetRowUnits();
    SALOMEDS::StringSeq_var aRowTitles = theTableAttr->GetRowTitles();
    comma = "";
    for(CORBA::Long j = 1; j <= aNbRows; j++){
      rowUnits  << comma << "'" << aRowUnits [ j - 1 ] << "'";
      rowTitles << comma << "'" << aRowTitles[ j - 1 ] << "'";
      if ( comma.empty() )
        comma = ",";
    }
    SALOMEDS::StringSeq_var aColumnTitles = theTableAttr->GetColumnTitles();
    comma = "";
    for(CORBA::Long j = 1; j <= aNbColumns; j++){
      colTitles << comma << "'" << aColumnTitles[ j - 1 ] << "'";
      if ( comma.empty() )
        comma = ",";
    }
    /*
    values    << '\0';
    rows      << '\0';
    columns   << '\0';
    rowUnits  << '\0';
    rowTitles << '\0';
    colTitles << '\0';
     */
    // write FillTable command
    theStr<< thePrefix << aName << "_values  = [" << values.str()  << "]" << endl;
    theStr<< thePrefix << aName << "_rows    = [" << rows.str()    << "]" << endl;
    theStr<< thePrefix << aName << "_columns = [" << columns.str() << "]" << endl;
    theStr<< thePrefix << aName << "_rUnits  = [" << rowUnits.str()  << "]" << endl;
    theStr<< thePrefix << aName << "_rTitles = [" << rowTitles.str() << "]" << endl;
    theStr<< thePrefix << aName << "_cTitles = [" << colTitles.str() << "]" << endl;
    theStr<< thePrefix << "visu.FillTable( "
                       << aName << ", "
                       << aName << "_values, "
                       << aName << "_rows, "
                       << aName << "_columns, "
                       << aName << "_rTitles, "
                       << aName << "_rUnits, "
                       << aName << "_cTitles )" << endl;
    
    if(theSObject->FindAttribute(anAttr,"AttributeIOR")){
      theStr<<thePrefix<<endl;
      std::string aName = "aTable";
      theStr<<thePrefix<<"anID = "<<aSObjectName<<".GetID()"<<endl;
      theStr<<thePrefix<<aName<<" = aVisu.CreateTable(anID)"<<endl;
      theArgumentName = aName;
      
      theStr<<thePrefix<<"if "<<aName<<":"<<endl;
      std::string aPrefix2 = thePrefix + PREFIX;
      
      // Set name (as this object could be renamed by user)
      CORBA::String_var aNameInStudy = theSObject->GetName();
      theStr<<aPrefix2<<aName<<".SetTitle('"<<aNameInStudy.in()<<"') # 1"<<endl;
      
      DumpChildrenToPython(theStudy,
                           theIsPublished,
                           theIsValidScript,
                           theSObject,
                           theStr,
                           theName2EntryMap,
                           theEntry2NameMap,
                           theArgumentName,
                           aPrefix2);
      
      theStr<<aPrefix2<<"pass"<<endl<<endl;
    }
    
    theStr<<thePrefix<<"pass"<<endl<<endl;
    theStr<<aPrefix<<"pass"<<endl<<endl;
  }
  
  
  //---------------------------------------------------------------------------
  void
  DumpChildrenToPython(SALOMEDS::Study_ptr theStudy,
                       CORBA::Boolean theIsPublished,
                       CORBA::Boolean& theIsValidScript,
                       SALOMEDS::SObject_ptr theSObject,
                       std::ostream& theStr,
                       TName2EntryMap& theName2EntryMap,
                       TEntry2NameMap& theEntry2NameMap,
                       std::string theArgumentName,
                       std::string thePrefix)
  {
    SALOMEDS::ChildIterator_var aChildItet = theStudy->NewChildIterator(theSObject);
    for(aChildItet->InitEx(false); aChildItet->More(); aChildItet->Next()){
      SALOMEDS::SObject_var aSObject = aChildItet->Value();
      DumpToPython(theStudy,
                   theIsPublished,
                   theIsValidScript,
                   aSObject,
                   theStr,
                   theName2EntryMap,
                   theEntry2NameMap,
                   theArgumentName,
                   thePrefix);
    }
  }
  
  
  //---------------------------------------------------------------------------
  void
  DumpTableAttrToPython(SALOMEDS::Study_ptr theStudy,
                        CORBA::Boolean theIsPublished,
                        CORBA::Boolean& theIsValidScript,
                        SALOMEDS::SObject_ptr theSObject,
                        std::ostream& theStr,
                        TName2EntryMap& theName2EntryMap,
                        TEntry2NameMap& theEntry2NameMap,
                        std::string theArgumentName,
                        std::string thePrefix)
  {
    SALOMEDS::GenericAttribute_var anAttr;
    if(theSObject->FindAttribute(anAttr,"AttributeTableOfInteger")){
      SALOMEDS::AttributeTableOfInteger_var aTableAttr =
      SALOMEDS::AttributeTableOfInteger::_narrow(anAttr);
      
      TableAttrToPython(theStudy,
                        theIsPublished,
                        theIsValidScript,
                        theSObject,
                        aTableAttr,
                        "AttributeTableOfInteger",
                        theStr,
                        theName2EntryMap,
                        theEntry2NameMap,
                        theArgumentName,
                        thePrefix);
      
    }else if(theSObject->FindAttribute(anAttr,"AttributeTableOfReal")){
      SALOMEDS::AttributeTableOfReal_var aTableAttr =
      SALOMEDS::AttributeTableOfReal::_narrow(anAttr);
      
      TableAttrToPython(theStudy,
                        theIsPublished,
                        theIsValidScript,
                        theSObject,
                        aTableAttr,
                        "AttributeTableOfReal",
                        theStr,
                        theName2EntryMap,
                        theEntry2NameMap,
                        theArgumentName,
                        thePrefix);
    }
  }
  
  
  
  //---------------------------------------------------------------------------
  void
  DumpToPython(SALOMEDS::Study_ptr theStudy,
               CORBA::Boolean theIsPublished,
               CORBA::Boolean& theIsValidScript,
               SALOMEDS::SObject_ptr theSObject,
               std::ostream& theStr,
               TName2EntryMap& theName2EntryMap,
               TEntry2NameMap& theEntry2NameMap,
               std::string theArgumentName,
               std::string thePrefix)
  {
    std::string aName = GetName(theSObject);
    if (aName == "")
      return;
    
    CORBA::String_var anID = theSObject->GetID();
    CORBA::String_var aNameInStudy = theSObject->GetName();
    
    CORBA::Object_var anObj = SObjectToObject(theSObject);
    if (!CORBA::is_nil(anObj)) {
      VISU::Base_var aBase = VISU::Base::_narrow(anObj);
      if(!CORBA::is_nil(aBase)){
        std::string aName = GenerateName(theSObject, theName2EntryMap, theEntry2NameMap);
        
        VISU::VISUType aType = aBase->GetType();
        switch(aType){
          case VISU::TRESULT:
            if(Result_i* aServant = dynamic_cast<Result_i*>(GetServant(anObj).in())){
              std::string aFileName = aServant->GetInitFileName();
              Result_i::ECreationId anId = aServant->GetCreationId();
              if(anId == Result_i::eImportFile || anId == Result_i::eCopyAndImportFile){
                switch(anId){
                  case Result_i::eImportFile:
                    theStr<<thePrefix<<aName<<" = aVisu.CreateResult('"<<aFileName<<"')"<<endl;
                    
                    theStr<<thePrefix<<aName<<".SetBuildGroups("<<
                    GetBoolean(aServant->IsGroupsDone())<<")"<<
                    endl;
                    
                    theStr<<thePrefix<<aName<<".SetBuildFields("<<
                    GetBoolean(aServant->IsFieldsDone())<<", "<<
                    GetBoolean(aServant->IsMinMaxDone())<<")"<<
                    endl;
                    
                    theStr<<thePrefix<<aName<<".Build(False, True)"<<endl;
                    
                    theStr<<thePrefix<<"if "<<aName<<".IsDone() :"<<endl;
                    break;
                  case Result_i::eCopyAndImportFile:
                    theStr<<thePrefix<<aName<<" = aVisu.CopyAndImportFile('"<<aFileName<<"')"<<endl;
                    theStr<<thePrefix<<"if "<<aName<<":"<<endl;
                    break;
                }
                
                thePrefix += PREFIX;
                {
                  VISU::Result::EntityNames_var aMeshNames = aServant->GetMeshNames();
                  if (aMeshNames->length() > 0) {
                    for(size_t aMeshId = 0; aMeshId < aMeshNames->length(); aMeshId++){
                      CORBA::String_var aMeshName = aMeshNames[aMeshId];
                      VISU::Result::EntityNames_var aParts = aServant->GetPartNames(aMeshName);
                      if (aParts->length() > 0) {
                        for(size_t aPartId = 0; aPartId < aParts->length(); aPartId++){
                          CORBA::String_var aPart = aParts[aPartId];
                          VISU::Result::Resolution aResolution = aServant->GetResolution(aMeshName, aPart);
                          std::string aParam;
                          switch(aResolution){
                            case VISU::Result::FULL:
                              aParam = "VISU.Result.FULL";
                              break;
                            case VISU::Result::MEDIUM:
                              aParam = "VISU.Result.MEDIUM";
                              break;
                            case VISU::Result::LOW:
                              aParam = "VISU.Result.LOW";
                              break;
                            case VISU::Result::HIDDEN:
                              aParam = "VISU.Result.HIDDEN";
                              break;
                          }
                          theStr<<thePrefix<<aName<<".SetResolution('"<<aMeshName.in()<<"', '"<<aPart.in()<<"', "<<aParam<<")"<<endl;
                        }
                        theStr<<thePrefix<<endl;
                      }
                    }
                  }
                }
                
                theArgumentName = aName;
                DumpChildrenToPython(theStudy,
                                     theIsPublished,
                                     theIsValidScript,
                                     theSObject,
                                     theStr,
                                     theName2EntryMap,
                                     theEntry2NameMap,
                                     theArgumentName,
                                     thePrefix);
                
                theStr<<thePrefix<<"pass"<<endl<<endl;
              }else{
                SALOMEDS::SObject_var aRefSObj;
                if(theSObject->FindSubObject(1,aRefSObj)){
                  SALOMEDS::SObject_var aTargetRefSObj;
                  if(aRefSObj->ReferencedObject(aTargetRefSObj)){
                    CORBA::String_var aString = aTargetRefSObj->GetName();
                    theStr<<thePrefix<<"aSObject = theStudy.FindObject('"<<aString.in()<<"')"<<endl;
                    theStr<<thePrefix<<"if aSObject:"<<endl;
                    thePrefix += PREFIX;
                    theStr<<thePrefix<<"anObject = aSObject.GetObject()"<<endl;
                    theStr<<thePrefix<<"if anObject:"<<endl;
                    std::string aPrefix1 = thePrefix;
                    thePrefix += PREFIX;
                    
                    switch(anId){
                      case Result_i::eImportMed:
                        theStr<<thePrefix<<aName<<" = aVisu.ImportMed(aSObject)"<<endl;
                        break;
                      case Result_i::eImportMedField:
                        theStr<<thePrefix<<aName<<" = aVisu.ImportMedField(anObject)"<<endl;
                        break;
                    }
                    
                    theStr<<thePrefix<<"if "<<aName<<":"<<endl;
                    std::string aPrefix2 = thePrefix;
                    thePrefix += PREFIX;
                    
                    theArgumentName = aName;
                    DumpChildrenToPython(theStudy,
                                         theIsPublished,
                                         theIsValidScript,
                                         theSObject,
                                         theStr,
                                         theName2EntryMap,
                                         theEntry2NameMap,
                                         theArgumentName,
                                         thePrefix);
                    
                    theStr<<thePrefix<<"pass"<<endl<<endl;
                    theStr<<aPrefix2<<"pass"<<endl<<endl;
                    theStr<<aPrefix1<<"pass"<<endl<<endl;
                  }
                }
              }
            }
            return;
          case VISU::TMESH:
            if(Mesh_i* aServant = dynamic_cast<Mesh_i*>(GetServant(anObj).in())){
              VISU::Entity anEntity = aServant->GetEntity();
              const std::string& aSubMeshName = aServant->GetSubMeshName();
              if(anEntity >= 0){
                std::string aParam;
                switch(anEntity){
                  case NODE:
                    aParam = "VISU.NODE";
                    break;
                  case EDGE:
                    aParam = "VISU.EDGE";
                    break;
                  case FACE:
                    aParam = "VISU.FACE";
                    break;
                  case CELL:
                    aParam = "VISU.CELL";
                    break;
                }
                
                if(aSubMeshName == "")
                  theStr<<thePrefix<<aName<<" = aVisu.MeshOnEntity("<<theArgumentName<<
                                            ", '"<<aServant->GetCMeshName()<<"'"<<
                                            ", "<<aParam<<
                                            ")"<<endl;
                else
                  theStr<<thePrefix<<aName<<" = aVisu.FamilyMeshOnEntity("<<theArgumentName<<
                                            ", '"<<aServant->GetCMeshName()<<"'"<<
                                            ", "<<aParam<<
                                            ", '"<<aSubMeshName<<"'"<<
                                            ")"<<endl;
              }else
                theStr<<thePrefix<<aName<<" = aVisu.GroupMesh("<<theArgumentName<<
                                          ", '"<<aServant->GetCMeshName()<<"'"<<
                                          ", '"<<aSubMeshName<<"'"<<
                                          ")"<<endl;
              
              theStr<<thePrefix<<"if "<<aName<<":"<<endl;
              thePrefix += PREFIX;
              
              // Add to Name->Object map
              theStr<<thePrefix<<"aName2ObjectMap['"<<aName<<"'] = "<<aName<<endl;
              
              // Set name (as this object could be renamed by user)
              theStr<<thePrefix<<"visu.SetName("<<aName<<", '"<<aNameInStudy.in()<<"')"<<endl;
              
              // Set parameters common for all Prs3d objects (offset values)
              Prs3dToPython(aServant,theStr,aName,thePrefix);
              
              // Set presentation parameters
              SALOMEDS::Color aColor;
              aColor = aServant->GetCellColor();
              theStr<<thePrefix<<aName<<".SetCellColor(SALOMEDS.Color("<<
              aColor.R<<", "<<aColor.G<<", "<<aColor.B<<"))"<<endl;
              
              aColor = aServant->GetNodeColor();
              theStr<<thePrefix<<aName<<".SetNodeColor(SALOMEDS.Color("<<
              aColor.R<<", "<<aColor.G<<", "<<aColor.B<<"))"<<endl;
              
              aColor = aServant->GetLinkColor();
              theStr<<thePrefix<<aName<<".SetLinkColor(SALOMEDS.Color("<<
              aColor.R<<", "<<aColor.G<<", "<<aColor.B<<"))"<<endl;
              
              std::string aParam;
              switch(aServant->GetPresentationType()){
                case POINT:
                  aParam = "VISU.POINT";
                  break;
                case WIREFRAME:
                  aParam = "VISU.WIREFRAME";
                  break;
                case SHADED:
                  aParam = "VISU.SHADED";
                  break;
                case INSIDEFRAME:
                  aParam = "VISU.INSIDEFRAME";
                  break;
                case SURFACEFRAME:
                  aParam = "VISU.SURFACEFRAME";
                  break;
                case SHRINK:
                  aParam = "VISU.SHRINK";
                  break;
              }
              theStr<<thePrefix<<aName<<".SetPresentationType("<<aParam<<")"<<endl;
              theStr<<thePrefix<<aName<<".SetShrink("<<(aServant->IsShrank()? "True" : "False")<<")"<<endl;
              theStr<<thePrefix<<endl;
              
              std::string aQuad2DPresent;
              switch(aServant->GetQuadratic2DPresentationType()){
                case LINES:
                  aQuad2DPresent = "VISU.LINES";
                  break;
                case ARCS:
                  aQuad2DPresent = "VISU.ARCS";
                  break;
              }
              
              theStr<<thePrefix<<aName<<".SetQuadratic2DPresentationType("<<aQuad2DPresent<<")"<<endl;
              
              DumpChildrenToPython(theStudy,
                                    theIsPublished,
                                    theIsValidScript,
                                    theSObject,
                                    theStr,
                                    theName2EntryMap,
                                    theEntry2NameMap,
                                    theArgumentName,
                                    thePrefix);
              
              theStr<<thePrefix<<"pass"<<endl<<endl;
              return;
            }
            break;
          case VISU::TSCALARMAP:
            if(ScalarMap_i* aServant = dynamic_cast<ScalarMap_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "ScalarMapOnField", theArgumentName);
              thePrefix = ScalarMapToPython(theSObject, aServant, theStr,aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TDEFORMEDSHAPE:
            if(DeformedShape_i* aServant = dynamic_cast<DeformedShape_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "DeformedShapeOnField", theArgumentName);
              thePrefix = DeformedShapeToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TSTREAMLINES:
            if(StreamLines_i* aServant = dynamic_cast<StreamLines_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "StreamLinesOnField", theArgumentName);
              thePrefix = StreamLinesToPython(theSObject, aServant, theStr, theEntry2NameMap, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TSCALARMAPONDEFORMEDSHAPE:
          case VISU::TDEFORMEDSHAPEANDSCALARMAP:
            if(DeformedShapeAndScalarMap_i* aServant = dynamic_cast<DeformedShapeAndScalarMap_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "DeformedShapeAndScalarMapOnField", theArgumentName);
              thePrefix = DeformedShapeAndScalarMapToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TVECTORS:
            if(Vectors_i* aServant = dynamic_cast<Vectors_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "VectorsOnField", theArgumentName);
              thePrefix = VectorsToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TISOSURFACES:
            if(IsoSurfaces_i* aServant = dynamic_cast<IsoSurfaces_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "IsoSurfacesOnField", theArgumentName);
              thePrefix = IsoSurfacesToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TCUTPLANES:
            if(CutPlanes_i* aServant = dynamic_cast<CutPlanes_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "CutPlanesOnField", theArgumentName);
              thePrefix = CutPlanesToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TCUTLINES:
            if(CutLines_i* aServant = dynamic_cast<CutLines_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "CutLinesOnField", theArgumentName);
              thePrefix = CutLinesToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              
              theArgumentName = aName;
              DumpChildrenToPython(theStudy,
                                    theIsPublished,
                                    theIsValidScript,
                                    theSObject,
                                    theStr,
                                    theName2EntryMap,
                                    theEntry2NameMap,
                                    theArgumentName,
                                    thePrefix);
              
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TCUTSEGMENT:
            if(CutSegment_i* aServant = dynamic_cast<CutSegment_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "CutSegmentOnField", theArgumentName);
              thePrefix = CutSegmentToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              
              theArgumentName = aName;
              DumpChildrenToPython(theStudy,
                                    theIsPublished,
                                    theIsValidScript,
                                    theSObject,
                                    theStr,
                                    theName2EntryMap,
                                    theEntry2NameMap,
                                    theArgumentName,
                                    thePrefix);
              
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TPLOT3D:
            if (Plot3D_i* aServant = dynamic_cast<Plot3D_i*>(GetServant(anObj).in())) {
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "Plot3DOnField", theArgumentName);
              thePrefix = Plot3DToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TPOINTMAP3D:
            if (PointMap3d_i* aServant = dynamic_cast<PointMap3d_i*>(GetServant(anObj).in())) {
              
              SALOMEDS::GenericAttribute_var anAttr;
              if(theSObject->FindAttribute(anAttr,"AttributeString")){
                using namespace SALOMEDS;
                AttributeString_var aComment = AttributeString::_narrow(anAttr);
                CORBA::String_var aValue = aComment->Value();
                Storable::TRestoringMap aMap;
                Storable::StringToMap(aValue.in(),aMap);
                bool anIsExist;
                QString aSourceId = VISU::Storable::FindValue(aMap,"mySourceId",&anIsExist);
                if(anIsExist) {
                  if( aSourceId == "TableAttr" ) {
                    DumpTableAttrToPython(theStudy,
                                          theIsPublished,
                                          theIsValidScript,
                                          theSObject,
                                          theStr,
                                          theName2EntryMap,
                                          theEntry2NameMap,
                                          theArgumentName,
                                          thePrefix);
                  }
                }
              }
              
              CORBA::Short aTag = theSObject->Tag();
              theStr<<thePrefix<<"anIsFound, aSObject = "<<(theArgumentName.empty() ? "aSComponent" : theArgumentName)<<".FindSubObject("<<aTag<<")"<<endl;
              theStr<<thePrefix<<"if anIsFound:"<<endl;
              thePrefix += PREFIX;
              
              theStr<<thePrefix<<"anID = aSObject.GetID()"<<endl;
              theStr<<thePrefix<<aName<<" = aVisu.CreateTable(anID)"<<endl;
              
              // Set name (as this object could be renamed by user)
              theStr<<thePrefix<<aName<<".SetTitle('"<<aNameInStudy.in()<<"') # 3"<<endl;
              
              // Set PointMap3D Properties
              
              theStr<<thePrefix<<aName<<".SetScaleFactor("<<aServant->GetScaleFactor()<<")"<<endl;
              theStr<<thePrefix<<aName<<".SetContourPrs("<<aServant->GetIsContourPrs()<<")"<<endl;
              theStr<<thePrefix<<aName<<".SetNbOfContours("<<aServant->GetNbOfContours()<<")"<<endl;
              
              std::string aParam;
              switch(aServant->GetScaling()){
                case LINEAR:
                  aParam = "VISU.LINEAR";
                  break;
                case LOGARITHMIC:
                  aParam = "VISU.LOGARITHMIC";
                  break;
              }
              theStr<<thePrefix<<aName<<".SetScaling("<<aParam<<")"<<endl;
              
              theStr<<thePrefix<<aName<<".SetNbColors("<<aServant->GetNbColors()<<")"<<endl;
              theStr<<thePrefix<<aName<<".SetLabels("<<aServant->GetLabels()<<")"<<endl;
              
              switch(aServant->GetBarOrientation()){
                case ColoredPrs3dBase::HORIZONTAL:
                  aParam = "VISU.ColoredPrs3d.HORIZONTAL";
                  break;
                case ColoredPrs3dBase::VERTICAL:
                  aParam = "VISU.ColoredPrs3d.VERTICAL";
                  break;
              }
              theStr<<thePrefix<<aName<<".SetBarOrientation("<<aParam<<")"<<endl;
              
              if(aServant->IsRangeFixed())
                theStr<<thePrefix<<aName<<".SetRange("<<aServant->GetMin()<<", "<<aServant->GetMax()<<")"<<endl;
              else
                theStr<<thePrefix<<aName<<".SetSourceRange()"<<endl;
              
              theStr<<thePrefix<<aName<<".SetPosition("<<aServant->GetPosX()<<", "<<aServant->GetPosY()<<")"<<endl;
              theStr<<thePrefix<<aName<<".SetSize("<<aServant->GetWidth()<<", "<<aServant->GetHeight()<<")"<<endl;
              
              float dx, dy, dz;
              aServant->GetOffset(dx, dy, dz);
              theStr<<thePrefix<<aName<<".SetOffset("<<dx<<", "<<dy<<", "<<dz<<")"<<endl;
              
              
              theStr<<thePrefix<<endl;
              
              theArgumentName = aName;
              DumpChildrenToPython(theStudy,
                                    theIsPublished,
                                    theIsValidScript,
                                    theSObject,
                                    theStr,
                                    theName2EntryMap,
                                    theEntry2NameMap,
                                    theArgumentName,
                                    thePrefix);
              
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TGAUSSPOINTS:
            if(GaussPoints_i* aServant = dynamic_cast<GaussPoints_i*>(GetServant(anObj).in())){
              TCreateFromResult aPrsFactory(theSObject, aServant, aName, "GaussPointsOnField", theArgumentName);
              thePrefix = GaussPointsToPython(theSObject, aServant, theStr, aName, aPrsFactory, thePrefix);
              theStr<<thePrefix<<"pass"<<endl<<endl;
            }
            return;
          case VISU::TCURVE:
            if(Curve_i* aServant = dynamic_cast<Curve_i*>(GetServant(anObj).in()))
            {
              bool withZ = aServant->GetZRow()>0;
              bool isV2 = aServant->GetIsV2();
              
              theStr << thePrefix << aName << " = visu.CreateCurve";
              if( isV2 )
                theStr << "WithZExt";
              else if( withZ )
                theStr << "WithZ";
              theStr << "(" <<
              theArgumentName<< // table
                ", "<<aServant->GetHRow()<< // H row
                ", "<<aServant->GetVRow(); // V row
              if( withZ || isV2 )
                theStr << ", " << aServant->GetZRow(); // Z row
                if( isV2 )
                  theStr << ", " << aServant->GetIsV2(); // right axis
                  
                  theStr << ", '"<<aServant->GetTitle()<<"'"; // title
                  SALOMEDS::Color aColor = aServant->GetColor();
                theStr << ",SALOMEDS.Color("<<
                aColor.R<<", "<<aColor.G<<", "<<aColor.B<<")"; // color
                
                std::string aParam;
              switch(aServant->GetMarker()){
                case Curve::NONE:      aParam = "VISU.Curve.NONE";      break;
                case Curve::CIRCLE:    aParam = "VISU.Curve.CIRCLE";    break;
                case Curve::RECTANGLE: aParam = "VISU.Curve.RECTANGLE"; break;
                case Curve::DIAMOND:   aParam = "VISU.Curve.DIAMOND";   break;
                case Curve::DTRIANGLE: aParam = "VISU.Curve.DTRIANGLE"; break;
                case Curve::UTRIANGLE: aParam = "VISU.Curve.UTRIANGLE"; break;
                case Curve::LTRIANGLE: aParam = "VISU.Curve.LTRIANGLE"; break;
                case Curve::RTRIANGLE: aParam = "VISU.Curve.RTRIANGLE"; break;
                case Curve::CROSS:     aParam = "VISU.Curve.CROSS";     break;
                case Curve::XCROSS:    aParam = "VISU.Curve.XCROSS";    break;
              }
              theStr<<", "<<aParam; // marker
              
              switch(aServant->GetLine()){
                case Curve::VOIDLINE:       aParam = "VISU.Curve.VOIDLINE";       break;
                case Curve::SOLIDLINE:      aParam = "VISU.Curve.SOLIDLINE";      break;
                case Curve::DASHLINE:       aParam = "VISU.Curve.DASHLINE";       break;
                case Curve::DOTLINE:        aParam = "VISU.Curve.DOTLINE";        break;
                case Curve::DASHDOTLINE:    aParam = "VISU.Curve.DASHDOTLINE";    break;
                case Curve::DASHDOTDOTLINE: aParam = "VISU.Curve.DASHDOTDOTLINE"; break;
              }
              theStr<<", "<<aParam<<", "<<aServant->GetLineWidth()<<")"<<endl; // line type,width
	      // Add to Name->Object map
	      theStr << thePrefix << "aName2ObjectMap['" << aName << "'] = " << aName << endl;
            }
            return;
          case VISU::TTABLE:
            if(dynamic_cast<Table_i*>(GetServant(anObj).in())){
              SALOMEDS::GenericAttribute_var anAttr;
              if(theSObject->FindAttribute(anAttr,"AttributeString")){
                using namespace SALOMEDS;
                AttributeString_var aComment = AttributeString::_narrow(anAttr);
                CORBA::String_var aValue = aComment->Value();
                Storable::TRestoringMap aMap;
                Storable::StringToMap(aValue.in(),aMap);
                bool anIsExist;
                QString aSourceId = VISU::Storable::FindValue(aMap,"mySourceId",&anIsExist);
                if(anIsExist){
                  if( aSourceId == "CutLines" ){
                    theStr<<thePrefix<<"if aName2ObjectMap.has_key('"<<theArgumentName<<"'):"<<endl;
                    thePrefix += PREFIX;
                    
                    theStr<<thePrefix<<"anObject = aName2ObjectMap['"<<theArgumentName<<"']"<<endl;
                    theStr<<thePrefix<<"anIOR = anObject.GetID()"<<endl;
                    theStr<<thePrefix<<"aSObject = theStudy.FindObjectIOR(anIOR)"<<endl;
                    theStr<<thePrefix<<"if aSObject:"<<endl;
                    std::string aPrefix = thePrefix;
                    thePrefix += PREFIX;
                    
                    theStr<<thePrefix<<"anID = aSObject.GetID()"<<endl;
                    theStr<<thePrefix<<aName<<" = aVisu.CreateTable(anID)"<<endl;
                    
                    // Set name (as this object could be renamed by user)
                    theStr<<thePrefix<<aName<<".SetTitle('"<<aNameInStudy.in()<<"') # 2"<<endl;
                    
                    theStr<<thePrefix<<endl;
                    
                    theArgumentName = aName;
                    DumpChildrenToPython(theStudy,
                                          theIsPublished,
                                          theIsValidScript,
                                          theSObject,
                                          theStr,
                                          theName2EntryMap,
                                          theEntry2NameMap,
                                          theArgumentName,
                                          thePrefix);
                    
                    theStr<<thePrefix<<"pass"<<endl<<endl;
                    theStr<<aPrefix<<"pass"<<endl<<endl;
                  }else if( aSourceId == "TableFile" ){
                    CORBA::Short aTag = theSObject->Tag();
                    theStr<<thePrefix<<"anIsFound, aSObject = "<<theArgumentName<<".FindSubObject("<<aTag<<")"<<endl;
                    theStr<<thePrefix<<"if anIsFound:"<<endl;
                    thePrefix += PREFIX;
                    
                    theStr<<thePrefix<<"anID = aSObject.GetID()"<<endl;
                    theStr<<thePrefix<<aName<<" = aVisu.CreateTable(anID)"<<endl;
                    
                    // Set name (as this object could be renamed by user)
                    theStr<<thePrefix<<aName<<".SetTitle('"<<aNameInStudy.in()<<"') # 3"<<endl;
                    
                    theStr<<thePrefix<<endl;
                    
                    theArgumentName = aName;
                    DumpChildrenToPython(theStudy,
                                          theIsPublished,
                                          theIsValidScript,
                                          theSObject,
                                          theStr,
                                          theName2EntryMap,
                                          theEntry2NameMap,
                                          theArgumentName,
                                          thePrefix);
                    
                    theStr<<thePrefix<<"pass"<<endl<<endl;
                  }else if( aSourceId == "TableAttr" ){
                    //theArgumentName = aName;
                    DumpTableAttrToPython(theStudy,
                                          theIsPublished,
                                          theIsValidScript,
                                          theSObject,
                                          theStr,
                                          theName2EntryMap,
                                          theEntry2NameMap,
                                          theArgumentName,
                                          thePrefix);
                  }
                }
              }
            }
            return;
        }
      }
    } else { /*if(!CORBA::is_nil(anObj))*/
      SALOMEDS::GenericAttribute_var anAttr;
      if (theSObject->FindAttribute(anAttr,"AttributeString")) {
        SALOMEDS::AttributeString_var aComment =
        SALOMEDS::AttributeString::_narrow(anAttr);
        CORBA::String_var aValue = aComment->Value();
        Storable::TRestoringMap aMap;
        Storable::StringToMap(aValue.in(),aMap);
        bool anIsExist;
        QString aTypeName = VISU::Storable::FindValue(aMap,"myComment",&anIsExist);
        if (anIsExist) {
          if (aTypeName == "ImportTables") {
            QString aFileName = VISU::Storable::FindValue(aMap,"myFileName",&anIsExist);
            if(anIsExist){
              std::string aName =
              GenerateName(theSObject,theName2EntryMap,theEntry2NameMap);
              QString aFirstStrAsTitle =
              VISU::Storable::FindValue(aMap,"myFirstStrAsTitle",&anIsExist).trimmed().toLower();
              if ( aFirstStrAsTitle == "1" || aFirstStrAsTitle == "true" )
                aFirstStrAsTitle = "True";
              else
                aFirstStrAsTitle = "False";
              theStr<<thePrefix<<aName<<" = aVisu.ImportTables('"
                    <<aFileName.toLatin1().data()<<"',"
                    <<aFirstStrAsTitle.toLatin1().data()<<")"<<endl;
              theStr<<thePrefix<<"if "<<aName<<":"<<endl;
              thePrefix += PREFIX;
              
              theArgumentName = aName;
              DumpChildrenToPython(theStudy,
                                   theIsPublished,
                                   theIsValidScript,
                                   theSObject,
                                   theStr,
                                   theName2EntryMap,
                                   theEntry2NameMap,
                                   theArgumentName,
                                   thePrefix);
              
              theStr<<thePrefix<<"pass"<<endl<<endl;
              return;
            }
          } else if (aTypeName == "VIEW3D") {
            std::string aName = GetName(theSObject);
            theStr<<thePrefix<<aName<<" = aBuilder.NewObject(aSComponent)"<<endl;
            
            theStr<<thePrefix<<"if "<<aName<<":"<<endl;
            thePrefix += PREFIX;
            
            theStr<<thePrefix<<"anAttr = aBuilder.FindOrCreateAttribute("<<aName<<",'AttributeName')"<<endl;
            theStr<<thePrefix<<"anAttr.SetValue('"<<aName<<"')"<<endl;
            
            theStr<<thePrefix<<"anAttr = aBuilder.FindOrCreateAttribute("<<aName<<",'AttributeString')"<<endl;
            theStr<<thePrefix<<"anAttr.SetValue('"<<aValue.in()<<"')"<<endl;
            
            theStr<<thePrefix<<"pass"<<endl<<endl;
            return;
          } else if (aTypeName == "ENTITY" || aTypeName == "FAMILY" || aTypeName == "GROUP") {
            // Set name (as this object could be renamed by user)
            string aMeshName = VISU::Storable::FindValue(aMap,"myMeshName").toLatin1().data();
            string aSubMeshName = VISU::Storable::FindValue(aMap,"myName").toLatin1().data();
            string anEntityTypeKey = "myEntityId";
            if (aTypeName == "ENTITY") anEntityTypeKey = "myId";
            int anEntity = VISU::Storable::FindValue(aMap,anEntityTypeKey,"0").toInt();
            std::string anEntityType;
            switch ((TEntity)anEntity) {
              case NODE_ENTITY: anEntityType = "VISU.NODE"; break;
              case EDGE_ENTITY: anEntityType = "VISU.EDGE"; break;
              case FACE_ENTITY: anEntityType = "VISU.FACE"; break;
              case CELL_ENTITY: anEntityType = "VISU.CELL"; break;
            }
            
            if (aTypeName == "ENTITY" ) {
              theStr<<thePrefix<<"aVisu.RenameEntityInStudy("<<theArgumentName<<", '"<<aMeshName
              <<"', "<<anEntityType<<", '"<<aNameInStudy.in()<<"')"<<endl;
            }
            else if (aTypeName == "FAMILY") {
              if (aSubMeshName != aNameInStudy.in()) {
                theStr<<thePrefix<<"aVisu.RenameFamilyInStudy("<<theArgumentName<<", '"<<aMeshName
                <<"', "<<anEntityType<<", '"<<aSubMeshName<<"', '"<<aNameInStudy.in()<<"')"<<endl;
              }
            }
            else { // "GROUP"
              if (aSubMeshName != aNameInStudy.in()) {
                theStr<<thePrefix<<"aVisu.RenameGroupInStudy("<<theArgumentName<<", '"<<aMeshName
                <<"', '"<<aSubMeshName<<"', '"<<aNameInStudy.in()<<"')"<<endl;
              }
            }
          }
        }
      } else {
        DumpTableAttrToPython(theStudy,
                              theIsPublished,
                              theIsValidScript,
                              theSObject,
                              theStr,
                              theName2EntryMap,
                              theEntry2NameMap,
                              theArgumentName,
                              thePrefix);
      }
    }
    
    DumpChildrenToPython(theStudy,
                        theIsPublished,
                        theIsValidScript,
                        theSObject,
                        theStr,
                        theName2EntryMap,
                        theEntry2NameMap,
                        theArgumentName,
                        thePrefix);
  }


  //---------------------------------------------------------------------------
  void
  DumpCurveToPython(SALOMEDS::Study_ptr theStudy,
                    CORBA::Boolean theIsPublished,
                    CORBA::Boolean& theIsValidScript,
                    SALOMEDS::SObject_ptr theSObject,
                    std::ostream& theStr,
                    TName2EntryMap& theName2EntryMap,
                    TEntry2NameMap& theEntry2NameMap,
                    std::string theArgumentName,
                    std::string thePrefix)
  {
    SALOMEDS::SObject_var aTargetRefSObj;
    if(theSObject->ReferencedObject(aTargetRefSObj)){
      CORBA::Object_var anObj = SObjectToObject(aTargetRefSObj);
      if(CORBA::is_nil(anObj))
        return;
      
      VISU::Base_var aBase = VISU::Base::_narrow(anObj);
      if(CORBA::is_nil(aBase))
        return;
      
      if(aBase->GetType() == VISU::TCURVE){
        CORBA::String_var anID = aTargetRefSObj->GetID();
        std::string anArg = theEntry2NameMap[anID.in()];
        theStr<<thePrefix<<"if aName2ObjectMap.has_key('"<<anArg<<"'):"<<endl;
        thePrefix += PREFIX;
        theStr<<thePrefix<<"anObject = aName2ObjectMap['"<<anArg<<"']"<<endl;
        theStr<<thePrefix<<"if anObject: " <<theArgumentName<<".AddCurve(anObject)"<<endl;
        theStr<<thePrefix<<"pass"<<endl<<endl;
      }
    }
  }


  //---------------------------------------------------------------------------
  void
  DumpContainersToPython(SALOMEDS::Study_ptr theStudy,
                        CORBA::Boolean theIsPublished,
                        CORBA::Boolean& theIsValidScript,
                        SALOMEDS::SObject_ptr theSObject,
                        std::ostream& theStr,
                        TName2EntryMap& theName2EntryMap,
                        TEntry2NameMap& theEntry2NameMap,
                        std::string theArgumentName,
                        std::string thePrefix)
  {
    SALOMEDS::ChildIterator_var aChildItet = theStudy->NewChildIterator(theSObject);
    for(aChildItet->InitEx(false); aChildItet->More(); aChildItet->Next()){
      SALOMEDS::SObject_var aSObject = aChildItet->Value();
      CORBA::Object_var anObj = SObjectToObject(aSObject);
      if(CORBA::is_nil(anObj))
        continue;
      
      VISU::Base_var aBase = VISU::Base::_narrow(anObj);
      if(CORBA::is_nil(aBase))
        continue;
      
      if(aBase->GetType() == VISU::TCONTAINER){
        theStr<<thePrefix<<endl;
        std::string aName = GenerateName(aSObject,theName2EntryMap,theEntry2NameMap);
        theStr<<thePrefix<<aName<<" = aVisu.CreateContainer()"<<endl;
        theStr<<thePrefix<<"if "<<aName<<":"<<endl;
        std::string aPrefix = thePrefix + PREFIX;
        theArgumentName = aName;
        
        // Set name (as this object could be renamed by user)
        CORBA::String_var aNameInStudy = aSObject->GetName();
        theStr<<aPrefix<<"visu.SetName("<<aName<<", '"<<aNameInStudy.in()<<"')"<<endl;
        
        SALOMEDS::ChildIterator_var aCurveIter = theStudy->NewChildIterator(aSObject);
        for(aCurveIter->InitEx(false); aCurveIter->More(); aCurveIter->Next()){
          SALOMEDS::SObject_var aRefSObj = aCurveIter->Value();
          DumpCurveToPython(theStudy,theIsPublished,theIsValidScript,aRefSObj,theStr,theName2EntryMap,theEntry2NameMap,theArgumentName,aPrefix);
        }
        
        theStr<<aPrefix<<"pass"<<endl<<endl;
      }
    }
  }


  //---------------------------------------------------------------------------
  void
  DumpPrs3dCacheToPython(SALOMEDS::Study_ptr theStudy,
                        CORBA::Boolean theIsPublished,
                        CORBA::Boolean& theIsValidScript,
                        SALOMEDS::SObject_ptr theSObject,
                        std::ostream& theStr,
                        TName2EntryMap& theName2EntryMap,
                        TEntry2NameMap& theEntry2NameMap,
                        std::string theArgumentName,
                        std::string thePrefix)
  {
    
    SALOMEDS::ChildIterator_var anIter = theStudy->NewChildIterator(theSObject);
    for(anIter->InitEx(false); anIter->More(); anIter->Next()){
      SALOMEDS::SObject_var aSObject = anIter->Value();
      CORBA::Object_var anObj = SObjectToObject(aSObject);
      if(CORBA::is_nil(anObj))
        continue;
      
      VISU::Base_var aBase = VISU::Base::_narrow(anObj);
      if(CORBA::is_nil(aBase))
        continue;
      
      if(aBase->GetType() == VISU::TCOLOREDPRS3DCACHE){
        ColoredPrs3dCache_i* aCache = dynamic_cast<ColoredPrs3dCache_i*>(GetServant(aBase).in());
        theStr<<thePrefix<<"aCache = aVisu.GetColoredPrs3dCache(aVisu.GetCurrentStudy())"<<endl;
        theStr<<thePrefix<<"if aCache != None:"<<endl;
        {
          std::string aPrefix = thePrefix + PREFIX;
          std::string anArgument;
          VISU::ColoredPrs3dCache::MemoryMode aMode = aCache->GetMemoryMode();
          switch(aMode){
            case VISU::ColoredPrs3dCache::MINIMAL : anArgument = "VISU.ColoredPrs3dCache.MINIMAL"; break;
            case VISU::ColoredPrs3dCache::LIMITED : anArgument = "VISU.ColoredPrs3dCache.LIMITED"; break;
          }
          theStr<<aPrefix<<"aCache.SetMemoryMode("<<anArgument<<")"<<endl;
          
          if(aMode == VISU::ColoredPrs3dCache::LIMITED)
            theStr<<aPrefix<<"aCache.SetLimitedMemory("<<aCache->GetLimitedMemory()<<") # (Mb)"<<endl;
          
          SALOMEDS::ChildIterator_var aChildIter = theStudy->NewChildIterator(aSObject);
          for(aChildIter->InitEx(false); aChildIter->More(); aChildIter->Next()){
            SALOMEDS::SObject_var aSObject = aChildIter->Value();
            CORBA::Object_var anObject = SObjectToObject(aSObject);
            if (CORBA::is_nil(anObject))
              continue;
            
            ColoredPrs3dHolder_i* aServant = dynamic_cast<ColoredPrs3dHolder_i*>(GetServant(anObject).in());
            if(!aServant)
              continue;
            
            ColoredPrs3d_i* aDevice = aServant->GetPrs3dDevice();
            if(!aDevice)
              continue;
            
            Result_i* aResult = aDevice->GetCResult();
	    if (!aResult)
	      continue;

            std::string aResultEntry = aResult->GetEntry();
            std::string aResultName = theEntry2NameMap[aResultEntry];
            
            ColoredPrs3dHolder::BasicInput_var anInput = aServant->GetBasicInput();
            std::string anEntity;
            switch(anInput->myEntity){
              case VISU::NODE : anEntity = "VISU.NODE"; break;
              case VISU::EDGE : anEntity = "VISU.EDGE"; break;
              case VISU::FACE : anEntity = "VISU.FACE"; break;
              case VISU::CELL : anEntity = "VISU.CELL"; break;
            }
            
            
            theStr<<aPrefix<<"anInput = VISU.ColoredPrs3dHolder.BasicInput("<<
            aResultName<<", '"<<
              anInput->myMeshName<<"', "<<
              anEntity<<", '"<<
              anInput->myFieldName<<"', "<<
              anInput->myTimeStampNumber<<")"<<
              endl;
            
            std::string aComment = aDevice->GetComment();
            theStr<<aPrefix<<"aHolder = aCache.CreateHolder(VISU.T"<<aComment<<", anInput)"<<endl;
            theStr<<aPrefix<<"if aHolder != None:"<<endl;
            {
              std::string aPrefix2 = aPrefix + PREFIX;
              CORBA::String_var aNameInStudy = aSObject->GetName();
              theStr<<aPrefix2<<"visu.SetName(aHolder, '"<<aNameInStudy.in()<<"')"<<endl;
              theStr<<aPrefix2<<"aDevice = aHolder.GetDevice()"<<endl;
              theStr<<aPrefix2<<"if aDevice != None:"<<endl;
              {
                std::string aPrefix3 = aPrefix2 + PREFIX;
                TColoredPrs3dFactory aPrsFactory;
                switch(aDevice->GetType()){
                  case VISU::TSCALARMAP:
                    if(ScalarMap_i* aServant = dynamic_cast<ScalarMap_i*>(aDevice)){
                      ScalarMapToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TDEFORMEDSHAPE:
                    if(DeformedShape_i* aServant = dynamic_cast<DeformedShape_i*>(aDevice)){
                      DeformedShapeToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TSTREAMLINES:
                    if(StreamLines_i* aServant = dynamic_cast<StreamLines_i*>(aDevice)){
                      StreamLinesToPython(aSObject, aServant, theStr, theEntry2NameMap, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TSCALARMAPONDEFORMEDSHAPE:
                  case VISU::TDEFORMEDSHAPEANDSCALARMAP:
                    if(DeformedShapeAndScalarMap_i* aServant = dynamic_cast<DeformedShapeAndScalarMap_i*>(aDevice)){
                      DeformedShapeAndScalarMapToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TVECTORS:
                    if(Vectors_i* aServant = dynamic_cast<Vectors_i*>(aDevice)){
                      VectorsToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TISOSURFACES:
                    if(IsoSurfaces_i* aServant = dynamic_cast<IsoSurfaces_i*>(aDevice)){
                      IsoSurfacesToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TCUTPLANES:
                    if(CutPlanes_i* aServant = dynamic_cast<CutPlanes_i*>(aDevice)){
                      CutPlanesToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TCUTLINES:
                    if(CutLines_i* aServant = dynamic_cast<CutLines_i*>(aDevice)){
                      CutLinesToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TCUTSEGMENT:
                    if(CutSegment_i* aServant = dynamic_cast<CutSegment_i*>(aDevice)){
                      CutSegmentToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TPLOT3D:
                    if(Plot3D_i* aServant = dynamic_cast<Plot3D_i*>(aDevice)){
                      Plot3DToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                  case VISU::TGAUSSPOINTS:
                    if(GaussPoints_i* aServant = dynamic_cast<GaussPoints_i*>(aDevice)){
                      GaussPointsToPython(aSObject, aServant, theStr, "aDevice", aPrsFactory, aPrefix3);
                      break;
                    }
                }
                theStr<<aPrefix3<<"pass"<<endl;
              }
              theStr<<aPrefix2<<"pass"<<endl<<endl;
            }
          }
          theStr<<aPrefix<<"pass"<<endl;
        }
      }
    }
  }


  //---------------------------------------------------------------------------
  void
  DumpEvolutionsToPython(SALOMEDS::Study_ptr theStudy,
                        CORBA::Boolean theIsPublished,
                        CORBA::Boolean& theIsValidScript,
                        SALOMEDS::SObject_ptr theSObject,
                        std::ostream& theStr,
                        TName2EntryMap& theName2EntryMap,
                        TEntry2NameMap& theEntry2NameMap,
                        std::string thePrefix)
  {
    if(!theIsPublished) return;
    
    SALOMEDS::ChildIterator_var aChildItet = theStudy->NewChildIterator(theSObject);
    for(aChildItet->InitEx(false); aChildItet->More(); aChildItet->Next()){
      SALOMEDS::SObject_var aSObject = aChildItet->Value();
      
      SALOMEDS::GenericAttribute_var anAttr;
      if (!aSObject->FindAttribute(anAttr, "AttributeString")) continue;
                                  
                                  SALOMEDS::AttributeString_var aStringAttr = SALOMEDS::AttributeString::_narrow(anAttr);
      QString value (aStringAttr->Value());
      if(value.isEmpty()) continue;
                                  
                                  VISU::Storable::TRestoringMap aMap;
      VISU::Storable::StringToMap(value, aMap);
      bool isExist;
      
      QString aTypeName = VISU::Storable::FindValue(aMap,"myComment",&isExist);
      if(!isExist || aTypeName != "EVOLUTION") continue;
        
        //EVOLUTION
      theStr<<thePrefix<<endl;
      QString evolutionName = aSObject->GetName();
      theStr<<thePrefix<<"#Evolution: "<<evolutionName.toLatin1().data()<<endl;
      theStr<<thePrefix<<endl;
      theStr<<thePrefix<<"evolutionSO = aBuilder.NewObject(aSComponent)"<<endl;
      theStr<<thePrefix<<"aBuilder.SetName(evolutionSO, '"<<evolutionName.toLatin1().data()<<"')"<< endl;
      theStr<<thePrefix<<"strAttr = aBuilder.FindOrCreateAttribute(evolutionSO, 'AttributeString')"<< endl;
      theStr<<thePrefix<<"strAttr.SetValue('"<<value.toLatin1().data()<<"')"<< endl;
      
      SALOMEDS::ChildIterator_var anIter = theStudy->NewChildIterator(aSObject);
      for (anIter->Init(); anIter->More(); anIter->Next()) {
        SALOMEDS::SObject_var anObj = anIter->Value();
        
        //FIELD
        theStr<<thePrefix<<"fieldSO = aBuilder.NewObject(evolutionSO)"<<endl;
        if (anObj->FindAttribute(anAttr, "AttributeString")) {
          aStringAttr = SALOMEDS::AttributeString::_narrow(anAttr);
          theStr<<thePrefix<<"strAttr = aBuilder.FindOrCreateAttribute(fieldSO, 'AttributeString')"<< endl;
          theStr<<thePrefix<<"strAttr.SetValue('"<<aStringAttr->Value()<<"')"<< endl;
        }
        
        SALOMEDS::SObject_var refObj;
        if(anObj->ReferencedObject(refObj)) {
          SALOMEDS::SObject_var father = refObj->GetFather();
          value = refObj->GetName();
          QString path(theStudy->GetObjectPath(father));
          //The following code requierd as a field name can contain '/' character
          theStr<<thePrefix<<"aBuilder.Addreference(fieldSO,visu.getSObjectByFatherPathAndName(theStudy, '"<<
          path.toLatin1().data()<<"', '"<<value.toLatin1().data()<<"'))"<<endl;
        }
        value = anObj->GetName();
        if(!value.isEmpty()) theStr<<thePrefix<<"aBuilder.SetName(fieldSO, '"<<value.toLatin1().data()<<"')"<< endl;
        
        //TABLE
        SALOMEDS::ChildIterator_var aChildItet = theStudy->NewChildIterator(anObj);
        for(aChildItet->InitEx(false); aChildItet->More(); aChildItet->Next()){
          SALOMEDS::SObject_var aSObject = aChildItet->Value();
          
          std::string anArgumentName = "fieldSO";
          DumpTableAttrToPython(theStudy,
                                theIsPublished,
                                theIsValidScript,
                                aSObject,
                                theStr,
                                theName2EntryMap,
                                theEntry2NameMap,
                                anArgumentName,
                                thePrefix);
        }
      }
      
    }
    
    theStr<<thePrefix<<endl;
  }


  //---------------------------------------------------------------------------
  void
  DumpAnimationsToPython(SALOMEDS::Study_ptr theStudy,
                        CORBA::Boolean theIsPublished,
                        CORBA::Boolean& theIsValidScript,
                        SALOMEDS::SObject_ptr theSObject,
                        std::ostream& theStr,
                        std::string thePrefix)
  {
    
    if(!theIsPublished) return;
        
    SALOMEDS::ChildIterator_var aChildItet = theStudy->NewChildIterator(theSObject);
    for(aChildItet->InitEx(false); aChildItet->More(); aChildItet->Next()){
      SALOMEDS::SObject_var aSObject = aChildItet->Value();
      
      SALOMEDS::GenericAttribute_var anAttr;
      if (!aSObject->FindAttribute(anAttr, "AttributeString")) continue;
                                  
                                  SALOMEDS::AttributeString_var aStringAttr = SALOMEDS::AttributeString::_narrow(anAttr);
      QString value (aStringAttr->Value());
      if(value.isEmpty()) continue;
                                  
                                  VISU::Storable::TRestoringMap aMap;
      VISU::Storable::StringToMap(value, aMap);
      bool isExist;
      
      QString aTypeName = VISU::Storable::FindValue(aMap,"myComment",&isExist);
      if(!isExist || aTypeName != "ANIMATION") continue;
        
        //ANIMATION
      theStr<<thePrefix<<endl;
      QString animName = aSObject->GetName();
      theStr<<thePrefix<<"#Animation: "<<animName.toLatin1().data()<<endl;
      theStr<<thePrefix<<endl;
      theStr<<thePrefix<<"animSO = aBuilder.NewObject(aSComponent)"<<endl;
      theStr<<thePrefix<<"aBuilder.SetName(animSO, '"<<animName.toLatin1().data()<<"')"<< endl;
      theStr<<thePrefix<<"strAttr = aBuilder.FindOrCreateAttribute(animSO, 'AttributeString')"<< endl;
      theStr<<thePrefix<<"strAttr.SetValue('"<<value.toLatin1().data()<<"')"<< endl;
      
      
      
      SALOMEDS::ChildIterator_var anIter = theStudy->NewChildIterator(aSObject);
      for (anIter->Init(); anIter->More(); anIter->Next()) {
        SALOMEDS::SObject_var anObj = anIter->Value();
        
        //FIELD
        theStr<<thePrefix<<"fieldSO = aBuilder.NewObject(animSO)"<<endl;
        if (anObj->FindAttribute(anAttr, "AttributeString")) {
          aStringAttr = SALOMEDS::AttributeString::_narrow(anAttr);
          theStr<<thePrefix<<"strAttr = aBuilder.FindOrCreateAttribute(fieldSO, 'AttributeString')"<< endl;
          theStr<<thePrefix<<"strAttr.SetValue('"<<aStringAttr->Value()<<"')"<< endl;
        }
        
        SALOMEDS::SObject_var refObj;
        if(anObj->ReferencedObject(refObj)) {
          SALOMEDS::SObject_var father = refObj->GetFather();
          value = refObj->GetName();
          QString path(theStudy->GetObjectPath(father));
          //The following code requierd as a field name can contain '/' character
          theStr<<thePrefix<<"aBuilder.Addreference(fieldSO,visu.getSObjectByFatherPathAndName(theStudy, '"<<
          path.toLatin1().data()<<"', '"<<value.toLatin1().data()<<"'))"<<endl;
        }
        value = anObj->GetName();
        if(!value.isEmpty()) theStr<<thePrefix<<"aBuilder.SetName(fieldSO, '"<<value.toLatin1().data()<<"')"<< endl;
          
          //SCALARMAP,...
        SALOMEDS::ChildIterator_var aSubIter = theStudy->NewChildIterator(anObj);
        for (aSubIter->Init(); aSubIter->More(); aSubIter->Next()) {
          SALOMEDS::SObject_var aSubObj = aSubIter->Value();
          
          theStr<<thePrefix<<"subSO = aBuilder.NewObject(fieldSO)"<<endl;
          value = aSubObj->GetName();
          if(!value.isEmpty()) theStr<<thePrefix<<"aBuilder.SetName(subSO, '"<<value.toLatin1().data()<<"')"<< endl;
          if (aSubObj->FindAttribute(anAttr, "AttributeString")) {
            aStringAttr = SALOMEDS::AttributeString::_narrow(anAttr);
            theStr<<thePrefix<<"strAttr = aBuilder.FindOrCreateAttribute(subSO, 'AttributeString')"<< endl;
            theStr<<thePrefix<<"strAttr.SetValue('"<<aStringAttr->Value()<<"')"<< endl;
          }
        }
        
      }
      
    }
    
    theStr<<thePrefix<<endl;
  }


  void
  DumpClippingPlanesToPython(SALOMEDS::Study_ptr theStudy,
                            CORBA::Boolean theIsPublished,
                            CORBA::Boolean& theIsValidScript,
                            SALOMEDS::SObject_ptr theSObject,
                            std::ostream& theStr,
                            std::string thePrefix,
                            VISU_ClippingPlaneMgr& thePlaneMgr)
  {
    
    if(!theIsPublished) return;
    
    VISU_CutPlaneFunction* aPlane;
    double aOrigin[3], aDir[3];
    for (int i = 0; i < thePlaneMgr.GetClippingPlanesNb(); i++) {
      aPlane = thePlaneMgr.GetClippingPlane(i);
      aPlane->GetOrigin(aOrigin);
      aPlane->GetNormal(aDir);
      
      theStr<<thePrefix<<"aVisu.CreateClippingPlane("<<
        aOrigin[0]<<","<<aOrigin[1]<<","<<aOrigin[2]<<","<<
        aDir[0]<<","<<aDir[1]<<","<<aDir[2]<<","<<
        aPlane->isAuto()<<",\""<<aPlane->getName()<<"\")"<<endl;      
    }
    theStr<<endl;
  }  


  void
  DumpTextureMapToPython(SALOMEDS::Study_ptr theStudy,
                        CORBA::Boolean theIsPublished,
                        CORBA::Boolean& theIsValidScript,
                        SALOMEDS::SObject_ptr theSObject,
                        std::ostream& theStr,
                        std::string thePrefix,
                        const StudyId2MarkerMap& theMarkerMap)
  {
    if(!theIsPublished)
      return;
    
    if(CORBA::is_nil(theStudy))
      return;
    
    StudyId2MarkerMap::const_iterator anIter = theMarkerMap.find(theStudy->StudyId());
    if(anIter == theMarkerMap.end())
      return;
    
    theStr<<thePrefix<<"texture_map = {}"<<endl<<endl;
    
    const VTK::MarkerMap& aMarkerMap = anIter->second;
    VTK::MarkerMap::const_iterator aMarkerIter = aMarkerMap.begin();
    for(; aMarkerIter != aMarkerMap.end(); aMarkerIter++) {
      int aMarkerId = aMarkerIter->first;
      std::string aMarkerTexture = aMarkerIter->second.first;
      theStr<<thePrefix<<"texture_map["<<aMarkerId<<"] = aVisu.LoadTexture(\""<<aMarkerTexture<<"\")"<<endl;
    }
    theStr<<endl;
  }  


  //---------------------------------------------------------------------------
  Engines::TMPFile*
  VISU_Gen_i::
  DumpPython(CORBA::Object_ptr theStudy,
            CORBA::Boolean theIsPublished,
            CORBA::Boolean theIsMultiFile,
            CORBA::Boolean& theIsValidScript)
  {
    theIsValidScript = false;
    
    SALOMEDS::Study_var aStudy = SALOMEDS::Study::_narrow(theStudy);
    if(CORBA::is_nil(aStudy))
      return new Engines::TMPFile(0);
    
    TName2EntryMap aName2EntryMap;
    TEntry2NameMap aEntry2NameMap;
    
#ifndef COUT
    ostringstream aStr;
#else
#define aStr cout
#endif
    
    std::string aPrefix = theIsMultiFile ? PREFIX : "";
    if( theIsMultiFile )
      aStr<<"def RebuildData(theStudy):"<<endl;
    aStr<<aPrefix<<"from batchmode_salome import orb, naming_service, lcc, myStudyManager"<<endl;
    aStr<<aPrefix<<"import SALOME_MED"<<endl;
    aStr<<aPrefix<<"import SALOMEDS"<<endl;
    aStr<<aPrefix<<"import VISU"<<endl;
    aStr<<aPrefix<<"import visu"<<endl;
    aStr<<endl;
    aStr<<aPrefix<<"aVisu = visu.Initialize(orb,naming_service,lcc,myStudyManager,theStudy,0)"<<endl;
    aStr<<aPrefix<<"aSComponent = visu.PublishComponent(theStudy)"<<endl;
    aStr<<aPrefix<<"aMed = lcc.FindOrLoadComponent('FactoryServer','MED')"<<endl;
    aStr<<aPrefix<<"aBuilder = theStudy.NewBuilder()"<<endl;
    aStr<<aPrefix<<"aName2ObjectMap = {}"<<endl;
    aStr<<endl;
    
    SALOMEDS::SComponent_var aComponent = FindOrCreateVisuComponent(aStudy);
    
    VISU::DumpClippingPlanesToPython(aStudy,
                                    theIsPublished,
                                    theIsValidScript,
                                    aComponent.in(),
                                    aStr,
                                    aPrefix, 
                                    myClippingPlaneMgr);
    
    VISU::DumpTextureMapToPython(aStudy,
                                theIsPublished,
                                theIsValidScript,
                                aComponent.in(),
                                aStr,
                                aPrefix,
                                myMarkerMap);
    
    VISU::DumpChildrenToPython(aStudy,
                              theIsPublished,
                              theIsValidScript,
                              aComponent.in(),
                              aStr,
                              aName2EntryMap,
                              aEntry2NameMap,
                              "",
                              aPrefix);
    
    VISU::DumpEvolutionsToPython(aStudy,
                                theIsPublished,
                                theIsValidScript,
                                aComponent.in(),
                                aStr,
                                aName2EntryMap,
                                aEntry2NameMap,
                                aPrefix);
    
    VISU::DumpContainersToPython(aStudy,
                                theIsPublished,
                                theIsValidScript,
                                aComponent.in(),
                                aStr,
                                aName2EntryMap,
                                aEntry2NameMap,
                                "",
                                aPrefix);
    
    VISU::DumpPrs3dCacheToPython(aStudy,
                                theIsPublished,
                                theIsValidScript,
                                aComponent.in(),
                                aStr,
                                aName2EntryMap,
                                aEntry2NameMap,
                                "",
                                aPrefix);
    
    VISU::DumpAnimationsToPython(aStudy,
                                theIsPublished,
                                theIsValidScript,
                                aComponent.in(),
                                aStr,
                                aPrefix);
    
    if (theIsPublished) {
      SALOMEDS::AttributeParameter_var ap = aStudy->GetModuleParameters("Interface Applicative", "Post-Pro", -1);
      if (!CORBA::is_nil(ap)) {
	std::map<std::string,std::string>::iterator it = aEntry2NameMap.begin();
	for ( ;it != aEntry2NameMap.end(); it++ ) {
	  //1. Encode entry
	  if ( ((*it).first).length() < 7 ) continue;
	  std::string tail( (*it).first, 6 );
	  std::string newEntry(ComponentDataType());
	  newEntry = newEntry + "_" + tail;
	  
	  CORBA::String_var anEntry = CORBA::string_dup( newEntry.c_str() );

	  if ( ap->IsSet( anEntry.in(), 6 ) ) { //6 Means string array, see SALOMEDS_Attributes.idl AttributeParameter interface
	    std::string idCommand = std::string("visu.getObjectID(") + (*it).second + std::string(")");
	    SALOMEDS::StringSeq_var aSeq= ap->GetStrArray(anEntry);
	    int oldLenght = aSeq->length();	
	    aSeq->length(oldLenght+2);
	    aSeq[oldLenght] = CORBA::string_dup("_PT_OBJECT_ID_");
	    aSeq[oldLenght + 1] = CORBA::string_dup(idCommand.c_str());
	    ap->SetStrArray( anEntry, aSeq );
	  }
	}
      }
    }
    
    //Output the script that sets up the visul parameters.
    if(theIsPublished) {
      char* script = aStudy->GetDefaultScript("Post-Pro", aPrefix.c_str());
      if(script && strlen(script) > 0) {
        aStr << script;
        CORBA::string_free(script);
      }
    }
    
    if( theIsMultiFile )
      aStr<<aPrefix<<"pass"<<endl;
    
    // theIsValidScript currently is not used by internal dump methods (DumpChildrenToPython(), etc.)
    // If the situation changes, then the following line should be removed, and theIsValidScript
    // should be set properly by those internal methods
    theIsValidScript = true;
    
#ifndef COUT
    std::string aResult = aStr.str();
    //ofstream anFStream("/tmp/dump.py");
    //anFStream<<aResult<<endl;
    
    CORBA::ULong aSize = aResult.size() + 1;
    char* aBuffer = new char[aSize];
    strcpy(aBuffer,&aResult[0]);
    return new Engines::TMPFile(aSize,aSize,(CORBA::Octet*)aBuffer,1);
#else
#undef aStr
    return new Engines::TMPFile(0);
#endif
  }
}

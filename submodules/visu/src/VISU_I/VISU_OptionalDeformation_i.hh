// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Deformation_i.hxx
//  Author : 
//  Module : VISU
//
#ifndef VISU_OptionalDeformation_i_HeaderFile
#define VISU_OptionalDeformation_i_HeaderFile

#include "VISU_I.hxx"
#include "VISU_Deformation_i.hh"
#include "VISU_OptionalDeformationPL.hxx"

namespace VISU {
  class VISU_I_EXPORT OptionalDeformation_i : public virtual POA_VISU::OptionalDeformation,
					      public virtual Deformation_i
  {
    OptionalDeformation_i(const OptionalDeformation_i&);
  public:
    typedef VISU::OptionalDeformation TInterface;
    typedef Deformation_i TSuperClass;

    OptionalDeformation_i(VISU::ColoredPrs3d_i* theModifiedEngine);
    virtual ~OptionalDeformation_i();
    
    virtual void UseDeformation(CORBA::Boolean theFlag);
    virtual CORBA::Boolean IsDeformed();


    virtual
    void
    DeformationToStream(std::ostringstream& theStr);

    virtual
    void
    RestoreDeformation(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void 
    SameAsDeformation(const Deformation_i *aDeformedPrs);

  protected:
    virtual
    void InitDeformedPipeLine(VISU_DeformationPL* theDeformedPipeLine);

    VISU_OptionalDeformationPL*
    GetSpecificDeformedPL() const
    { 
      return myOptionalDeformationPL;
    }
    
  private:
    VISU_OptionalDeformationPL* myOptionalDeformationPL;
    
  };
}
#endif

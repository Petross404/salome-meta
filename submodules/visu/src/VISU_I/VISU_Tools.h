// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Tools.h
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_TOOLS_H
#define VISU_TOOLS_H

#include "VISUConfig.hh"
#include "VISU_I.hxx"

#include <Qtx.h>

#include <SALOMEDSClient_Study.hxx>

class SPlot2d_Curve;
class Plot2d_ViewFrame;
class SalomeApp_Study;
class SalomeApp_Application;
class VISU_ActorBase;
class SALOME_Actor;

namespace VISU 
{
  class Gen_i;
  class Table_i;
  class Curve_i;
  class Container_i;

  /*! Display/Erase/Update a curve presentation.
   *  Parameter \a frame may be NULL, in this case there is only update without display/erase
   */
  VISU_I_EXPORT void                                 UpdateCurve( VISU::Curve_i*,
                                                                  Plot2d_ViewFrame*,
                                                                  SPlot2d_Curve*,
                                                                  int theDisplaying );

  VISU_I_EXPORT void                                 PlotTable( SalomeApp_Study*,
                                                                Plot2d_ViewFrame*,
                                                                VISU::Table_i*,
                                                                int theDisplaying );

  VISU_I_EXPORT void                                 PlotCurve( Plot2d_ViewFrame*,
                                                                VISU::Curve_i*,
                                                                int theDisplaying );

  VISU_I_EXPORT void                                 PlotRemoveCurve(SalomeApp_Application*,
                                                                     VISU::Curve_i* );

  VISU_I_EXPORT void                                 PlotContainer( Plot2d_ViewFrame*,
                                                                    VISU::Container_i*,
                                                                    int theDisplaying );

  VISU_I_EXPORT void                                 CreatePlot( VISU_Gen_i*,
                                                                 Plot2d_ViewFrame*,
                                                                 _PTR(SObject) theTableSO );

  VISU_I_EXPORT void                                 SetVisibilityState(std::string entry, Qtx::VisibilityState state);
  VISU_I_EXPORT void                                 SetVisibilityState(SALOME_Actor *theActor, Qtx::VisibilityState state);

  VISU_I_EXPORT void                                 CurveVisibilityChanged(VISU::Curve_i* theCurve, 
									    int theDisplaying,
									    bool updateCurve,
									    bool updateTable,
									    bool updateContainers);

  VISU_I_EXPORT Qtx::VisibilityState                 GetStateByDisplaying(int theDisplaying);
  VISU_I_EXPORT void                                 updateContainerVisibility(VISU::Container_i* theContainer);
}

#endif

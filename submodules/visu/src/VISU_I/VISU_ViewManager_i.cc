// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ViewManager_i.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_ViewManager_i.hh"
#include "VISU_View_i.hh"
#include "VISU_Prs3d_i.hh"
#include "VISU_Table_i.hh"
#include "VISU_Tools.h"

#include "VISU_Actor.h"
#include "VISU_ActorFactory.h"

#include "SUIT_Tools.h"
#include "SUIT_Session.h"
#include "SUIT_ViewWindow.h"
#include "SUIT_ViewManager.h"

#include "SVTK_ViewWindow.h"
#include "SVTK_ViewModel.h"
#include "VTKViewer_Algorithm.h"
#include "SPlot2d_Curve.h"
#include "SPlot2d_ViewModel.h"
#include "Plot2d_ViewFrame.h"
#include "Plot2d_ViewWindow.h"
#include "Plot2d_ViewModel.h"
#include "Plot2d_ViewManager.h"

#include "SalomeApp_Study.h"
#include "SalomeApp_Application.h"
#include "LightApp_SelectionMgr.h"

#include "SALOME_Event.h"
#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"

#include "VTKViewer_Algorithm.h"
#include "SVTK_Functor.h"

#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

#include <QApplication>

using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

namespace VISU {

  struct TNewViewManagerEvent: public SALOME_Event
  {
    int myStudyId;
    typedef SalomeApp_Application* TResult;
    TResult myResult;

    TNewViewManagerEvent (const int theStudyId):
      myStudyId(theStudyId),
      myResult(NULL)
    {}

    virtual
    void
    Execute()
    {
      MESSAGE("Find application for study with id = : " << myStudyId);
      SUIT_Session* aSession = SUIT_Session::session();
      QList<SUIT_Application*> anApplications = aSession->applications();
      QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
      while ( anIter != anApplications.end() ) {
        SUIT_Application* anApp = *anIter;
        if (SUIT_Study* aSStudy = anApp->activeStudy()) {
          if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
            if (_PTR(Study) aCStudy = aStudy->studyDS()) {
              //if (myStudyName == aCStudy->Name()) {
              if (myStudyId == aCStudy->StudyId()) {
                myResult = dynamic_cast<SalomeApp_Application*>(anApp);
                break;
              }
            }
          }
        }
        anIter++;
      }
      if (!myResult) {
        //MESSAGE("Error: application is not found for study : " << myStudyName);
        MESSAGE("Error: application is not found for study with id = : " << myStudyId);
      }
    }
  };

  //===========================================================================
  ViewManager_i::ViewManager_i(SALOMEDS::Study_ptr theStudy)
  {
    if(MYDEBUG) MESSAGE("ViewManager_i::ViewManager_i - "<<this);

    //CORBA::String_var aStudyName = theStudy->Name();
    //myApplication = ProcessEvent(new TNewViewManagerEvent(aStudyName.in()));
    int aStudyID = theStudy->StudyId();
    myApplication = ProcessEvent(new TNewViewManagerEvent(aStudyID));
  }


  ViewManager_i::~ViewManager_i()
  {
    if(MYDEBUG) MESSAGE("ViewManager_i::~ViewManager_i - "<<this);
  }


  struct TCurrentViewEvent: public SALOME_Event
  {
    SalomeApp_Application* myApplication;
    typedef VISU::View_ptr TResult;
    TResult myResult;

    TCurrentViewEvent(SalomeApp_Application* theApplication):
      myApplication(theApplication),
      myResult(VISU::View::_nil())
    {}

    virtual
    void
    Execute()
    {
      VISU::View3D_i* aView = new View3D_i (myApplication);
      if (aView->Create(0))
        myResult = aView->_this();
    }
  };

  VISU::View_ptr ViewManager_i::GetCurrentView(){
    return ProcessEvent(new TCurrentViewEvent(myApplication));
  }


  template<class TViewFrame>
  struct TCreateViewFrameEvent: public SALOME_Event
  {
    SalomeApp_Application* myApplication;
    typedef typename TViewFrame::TInterface TInterface;
    typedef typename TInterface::_ptr_type TResult;
    TResult myResult;

    TCreateViewFrameEvent (SalomeApp_Application* theApplication):
      myApplication(theApplication),
      myResult(TInterface::_nil())
    {}

    virtual
    void
    Execute()
    {
      //if (CheckStudy(myStudyDocument)) {
      if (myApplication) {
        TViewFrame* pView = new TViewFrame (myApplication);
        if (pView->Create(1)) {
          myResult = pView->_this();
          qApp->processEvents(); // Fix for bug 9929
        }
      }
    }
  };


  VISU::View3D_ptr ViewManager_i::Create3DView()
  {
    if (MYDEBUG) MESSAGE("ViewManager_i::Create3DView");
    return ProcessEvent(new TCreateViewFrameEvent<View3D_i>(myApplication));
  }

  VISU::XYPlot_ptr ViewManager_i::CreateXYPlot()
  {
    if (MYDEBUG) MESSAGE("ViewManager_i::CreateXYPlot");
    return ProcessEvent(new TCreateViewFrameEvent<XYPlot_i>(myApplication));
  }

  class TCreateViewEvent: public SALOME_Event
  {
  public:
    TCreateViewEvent (SalomeApp_Application* theApplication)
      : myApplication(theApplication)
    {}
  protected:
    SalomeApp_Application* myApplication;
  };

  class TCreateTableViewFrameEvent: public TCreateViewEvent
  {
    Table_ptr myTable;
  public:
    TCreateTableViewFrameEvent (SalomeApp_Application* theApplication,
                                Table_ptr theTable):
      TCreateViewEvent(theApplication),
      myTable(theTable),
      myResult(VISU::TableView::_nil())
    {}

    virtual void Execute()
    {
      //if (CheckStudy(myStudyDocument)) {
        VISU::TableView_i* pView = new TableView_i (myApplication);
        if (pView->Create(myTable) != NULL)
          myResult = pView->_this();
      //}
    }
    typedef VISU::TableView_ptr TResult;
    TResult myResult;
  };

  VISU::TableView_ptr ViewManager_i::CreateTableView (VISU::Table_ptr theTable)
  {
    if (MYDEBUG) MESSAGE("ViewManager_i::CreateTableView");
    //return ProcessEvent(new TCreateViewFrameEvent<TableView_i>(myApplication));
    return ProcessEvent(new TCreateTableViewFrameEvent (myApplication, theTable));
  }

  void ViewManager_i::Destroy (View_ptr theView)
  {
    class TEvent: public SALOME_Event {
      View_ptr myView;
    public:
      TEvent(View_ptr theView):
        myView(theView)
      {}
      virtual void Execute(){
        if (!CORBA::is_nil(myView)) {
          if (VISU::View_i* pView = dynamic_cast<VISU::View_i*>(VISU::GetServant(myView).in())) {
            pView->Close();
            pView->_remove_ref();
          }
        }
      }
    };

    if (MYDEBUG) MESSAGE("ViewManager_i::Destroy - " << theView->_is_nil());
    ProcessVoidEvent(new TEvent(theView));
  }

  //===========================================================================
  // VISU namespace functions
  //===========================================================================
  vtkRenderer* GetRenderer (SUIT_ViewWindow* theViewWindow)
  {
    if (SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(theViewWindow))
      return vw->getRenderer();
    return NULL;
  }

  vtkCamera* GetCamera (SUIT_ViewWindow* theViewWindow)
  {
    return GetRenderer(theViewWindow)->GetActiveCamera();
  }

  void RepaintView (SUIT_ViewWindow* theViewWindow)
  {
    if (SVTK_ViewWindow* vf = dynamic_cast<SVTK_ViewWindow*>(theViewWindow)) {
      vf->getRenderer()->ResetCameraClippingRange();
      vf->getRenderWindow()->Render();
    }
  }

  VISU_Actor* UpdateViewer (SUIT_ViewWindow* theViewWindow, int theDisplaing, Prs3d_i* thePrs)
  {
    SVTK_ViewWindow* vf = dynamic_cast<SVTK_ViewWindow*>(theViewWindow);
    if (!vf) return NULL;
    if(MYDEBUG) MESSAGE("UpdateViewer - theDisplaing = "<<theDisplaing<<"; thePrs = "<<thePrs);
    vtkRenderer *aRen = vf->getRenderer();
    VTK::ActorCollectionCopy aCopy(aRen->GetActors());
    vtkActorCollection* anActColl = aCopy.GetActors();
    vtkActor *anActor;
    VISU_Actor *anVISUActor = NULL, *aResActor = NULL;
    for(anActColl->InitTraversal(); (anActor = anActColl->GetNextActor()) != NULL;) {
      if( anActor->IsA("VISU_Actor") ) {
        anVISUActor = VISU_Actor::SafeDownCast(anActor);
        if (thePrs == anVISUActor->GetPrs3d()) {
          aResActor = anVISUActor;
          if(theDisplaing < eErase) {
            aResActor->VisibilityOn();
	    VISU::SetVisibilityState(aResActor, Qtx::ShownState);
	  }
          else {
            aResActor->VisibilityOff();
	    VISU::SetVisibilityState(aResActor, Qtx::HiddenState);
	  }
        } else {
          if(theDisplaing == eEraseAll || theDisplaing == eDisplayOnly) {
            anVISUActor->VisibilityOff();
	    VISU::SetVisibilityState(anVISUActor, Qtx::HiddenState);
	  }
          else if ( theDisplaing == eDisplayAll ) {
            anVISUActor->VisibilityOn();
	    VISU::SetVisibilityState(anVISUActor, Qtx::ShownState);
	  }
        }
      } else if ( anActor->IsA("SALOME_Actor") && (theDisplaing == eEraseAll || theDisplaing == eDisplayOnly) ) {
	// rnv : fix for the 21254: EDF 1861 VISU: Eye symbol and VISU presentations.
	//       Take into account presentations created in other modules.
	SALOME_Actor* aSActor = SALOME_Actor::SafeDownCast(anActor);
	if( aSActor ) {
	  aSActor->VisibilityOff();
	  VISU::SetVisibilityState(aSActor, Qtx::HiddenState);
	}
      }
    }
    
    if (aResActor) {
      RepaintView(theViewWindow);
      return aResActor;
    }
    if(thePrs != NULL && theDisplaing < eErase) {
      try{
        anVISUActor = thePrs->CreateActor();
        vf->AddActor(anVISUActor);
	VISU::SetVisibilityState(anVISUActor, Qtx::ShownState);
      }catch(std::exception& exc){
        if(MYDEBUG) INFOS(exc.what());
        return NULL;
      }catch(...){
        if(MYDEBUG) INFOS("Unknown exception was occured!!!");
        return NULL;
      }
    }
    RepaintView(theViewWindow);
    return anVISUActor;
  }

  struct TUpdatePlot2dEvent: public SALOME_Event
  {
    Curve_i*          myCurve;
    int               myDisplaying;

    TUpdatePlot2dEvent (Curve_i* theCurve, const int theDisplaying):
      myCurve(theCurve),
      myDisplaying(theDisplaying)
    {}

    virtual void Execute()
    {
      SalomeApp_Application* anApp = NULL;
      CORBA::String_var studyName = myCurve->GetStudyDocument()->Name();
      std::string aStudyName = studyName.in();
      SUIT_Session* aSession = SUIT_Session::session();
      QList<SUIT_Application*> anApplications = aSession->applications();
      QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
      while (anIter != anApplications.end()) {
        SUIT_Application* aSUITApp = *anIter;
        if (SUIT_Study* aSStudy = aSUITApp->activeStudy()) {
          if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
            if (_PTR(Study) aCStudy = aStudy->studyDS()) {
              if (aStudyName == aCStudy->Name()) {
                anApp = dynamic_cast<SalomeApp_Application*>(aSUITApp);
                break;
              }
            }
          }
        }
        anIter++;
      }
      if (!anApp)
        return;
      
      ViewManagerList aViewManagerList;
      anApp->viewManagers(SPlot2d_Viewer::Type(), aViewManagerList);
      SUIT_ViewManager* aViewManager;
      foreach( aViewManager, aViewManagerList ) {
        if (Plot2d_ViewManager* aManager = dynamic_cast<Plot2d_ViewManager*>(aViewManager)) {
          if (SPlot2d_Viewer* aViewer = dynamic_cast<SPlot2d_Viewer*>(aManager->getViewModel())) {
            if (Plot2d_ViewFrame* aViewFrame = aViewer->getActiveViewFrame()) {
              UpdatePlot2d(myCurve, myDisplaying, aViewFrame);
            }
          }            
        }
      }
    }
  };

  void UpdatePlot2d (Curve_i* theCurve, int theDisplaying, Plot2d_ViewFrame* theView)
  {
    if(MYDEBUG) MESSAGE("UpdatePlot2d - theDisplaying = " << theDisplaying);
    if (!theView) {
      // update all views
      ProcessVoidEvent(new TUpdatePlot2dEvent(theCurve, theDisplaying));
      return;
    }
    QList<Plot2d_Curve*> clist;
    theView->getCurves(clist);
    if (theDisplaying == eEraseAll) {
      for (int i = 0; i < clist.count(); i++) {
        if(MYDEBUG) MESSAGE("UpdatePlot2d - erasing all : curve - " << clist.at(i));
        theView->eraseCurve(clist.at(i));
      }
    } else if (theDisplaying == eErase) {
      if (theCurve) {
        for (int i = 0; i < clist.count(); i++) {
          SPlot2d_Curve* aSPlot2dC = dynamic_cast<SPlot2d_Curve*>(clist.at(i));
          if (aSPlot2dC->hasIO() &&
              !strcmp(theCurve->GetEntry().c_str(), aSPlot2dC->getIO()->getEntry())) {
            if(MYDEBUG) MESSAGE("UpdatePlot2d - erasing : curve - " << aSPlot2dC);
            theView->eraseCurve(aSPlot2dC);
          }
        }
      }
    } else if (theDisplaying == eDisplay ||
               theDisplaying == eDisplayOnly ||
               theDisplaying == eUpdateData) {
      if (theCurve) {
        bool bFound = false;
        for (int i = 0; i < clist.count(); i++) {
          SPlot2d_Curve* aSPlot2dC = dynamic_cast<SPlot2d_Curve*>(clist.at(i));
          if (aSPlot2dC->hasIO() &&
              !strcmp(theCurve->GetEntry().c_str(), aSPlot2dC->getIO()->getEntry())) {
            if (theDisplaying == eUpdateData) {
              if(MYDEBUG) MESSAGE("UpdatePlot2d - updating data : curve - " << aSPlot2dC);
              aSPlot2dC->setScale(theCurve->GetScale());
            }
            else {
              if(MYDEBUG) MESSAGE("UpdatePlot2d - displaying : curve - " << aSPlot2dC);
            }
            double* xList = 0;
            double* yList = 0;
            QStringList zList;
            int     nbPoints = theCurve->GetData( xList, yList, zList );
            if (nbPoints > 0 && xList && yList) {
              aSPlot2dC->setData( xList, yList, nbPoints, zList );
            }
            if (theDisplaying == eUpdateData) {
              theView->updateCurve(aSPlot2dC, true);
            } else {
              aSPlot2dC->setHorTitle( theCurve->GetHorTitle().c_str() );
              aSPlot2dC->setVerTitle( theCurve->GetVerTitle().c_str() );
              aSPlot2dC->setHorUnits( theCurve->GetHorUnits().c_str() );
              aSPlot2dC->setVerUnits( theCurve->GetVerUnits().c_str() );
              if (!theCurve->IsAuto()) {
                aSPlot2dC->setLine((Plot2d::LineType)theCurve->GetLine(),
                                   theCurve->GetLineWidth());
                aSPlot2dC->setMarker((Plot2d::MarkerType)theCurve->GetMarker());
                SALOMEDS::Color color = theCurve->GetColor();
                aSPlot2dC->setColor(QColor((int)(color.R*255.),
                                           (int)(color.G*255.),
                                           (int)(color.B*255.)));
              }
              aSPlot2dC->setAutoAssign(theCurve->IsAuto());
              theView->displayCurve(aSPlot2dC);
              bFound = true;
            }
          } else if (theDisplaying == eDisplayOnly) {
            theView->eraseCurve(aSPlot2dC);
          }
        }
        if (!bFound && theDisplaying != eUpdateData) {
          Plot2d_Curve* crv = theCurve->CreatePresentation();
          if(MYDEBUG) MESSAGE("UpdatePlot2d - displaying : curve (new) - "<<crv );
          if (crv) {
            theView->displayCurve( crv );
            theCurve->SetLine( (VISU::Curve::LineType)crv->getLine(), crv->getLineWidth() );
            theCurve->SetMarker( (VISU::Curve::MarkerType)crv->getMarker());
            SALOMEDS::Color newColor;
            newColor.R = crv->getColor().red()/255.;
            newColor.G = crv->getColor().green()/255.;
            newColor.B = crv->getColor().blue()/255.;
            theCurve->SetColor( newColor );
            crv->setAutoAssign( theCurve->IsAuto() );
          }
        }
      }
    }
  }


  //----------------------------------------------------------------------------
  struct TIsSamePrs3d
  {
    VISU::Prs3d_i* myPrs3d;

    TIsSamePrs3d(VISU::Prs3d_i* thePrs3d):
      myPrs3d(thePrs3d)
    {}
    
    bool
    operator()(VISU_Actor* theActor) 
    {
      return theActor->GetPrs3d() == myPrs3d;
    }
  };


  //----------------------------------------------------------------------------
  VISU_Actor* 
  FindActor(SVTK_ViewWindow* theViewWindow, VISU::Prs3d_i* thePrs3d)
  {
    if(!thePrs3d)
      return NULL;

    vtkRenderer* aRenderer = theViewWindow->getRenderer();
    VTK::ActorCollectionCopy aCopy(aRenderer->GetActors());
    vtkActorCollection* anActors = aCopy.GetActors();
    return SVTK::Find<VISU_Actor>(anActors, VISU::TIsSamePrs3d(thePrs3d));
  }


  //----------------------------------------------------------------------------
  struct TIsSameActor
  {
    VISU::TActorFactory* myActor;

    TIsSameActor(VISU::TActorFactory* theActor):
      myActor(theActor)
    {}
    
    bool
    operator()(VISU_ActorBase* theActor) 
    {
      return theActor->GetFactory() == myActor;
    }
  };

  //----------------------------------------------------------------------------
  VISU_ActorBase* 
  FindActorBase(SVTK_ViewWindow* theViewWindow, VISU::TActorFactory* theActor)
  {
    if(!theActor)
      return NULL;

    vtkRenderer* aRenderer = theViewWindow->getRenderer();
    VTK::ActorCollectionCopy aCopy(aRenderer->GetActors());
    vtkActorCollection* anActors = aCopy.GetActors();
    return SVTK::Find<VISU_ActorBase>(anActors, VISU::TIsSameActor(theActor));
  }


  //----------------------------------------------------------------------------
  struct TDeleteActorsEvent: public SALOME_Event
  {
    VISU::Curve_i* myPrs;

    TDeleteActorsEvent (VISU::Curve_i* thePrs):
      myPrs(thePrs)
    {}

    virtual
    void
    Execute()
    {
      if (!myPrs) return;

      // 1. Find appropriate application (code like in TNewViewManagerEvent::Execute())
      SALOMEDS::Study_var myStudyDocument = myPrs->GetStudyDocument();
      SalomeApp_Application* anApp = NULL;
      CORBA::String_var studyName = myStudyDocument->Name();
      std::string aStudyName = studyName.in();
      SUIT_Session* aSession = SUIT_Session::session();
      QList<SUIT_Application*> anApplications = aSession->applications();
      QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
      while ( anIter != anApplications.end() ) {
        SUIT_Application* aSUITApp = *anIter;
        if (SUIT_Study* aSStudy = aSUITApp->activeStudy()) {
          if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
            if (_PTR(Study) aCStudy = aStudy->studyDS()) {
              if (aStudyName == aCStudy->Name()) {
                anApp = dynamic_cast<SalomeApp_Application*>(aSUITApp);
                break;
              }
            }
          }
        }
        anIter++;
      }
      if (!anApp)
        return;

      // 2. Remove corresponding IO from selection
      SALOMEDS::SObject_var aSObject = myPrs->GetSObject();
      CORBA::String_var anEntry = aSObject->GetID();

      LightApp_SelectionMgr* aSelectionMgr = anApp->selectionMgr();
      SALOME_ListIO aListIO, aNewListIO;
      aSelectionMgr->selectedObjects(aListIO);

      for (SALOME_ListIteratorOfListIO it (aListIO); it.More(); it.Next()) {
        if (it.Value()->hasEntry()) {
          std::string aCurEntry (it.Value()->getEntry());
          if (aCurEntry != std::string( anEntry.in() ) ) {
            aNewListIO.Append(it.Value());
          }
        }
      }

      aSelectionMgr->setSelectedObjects(aNewListIO);

      // 3. Remove Actors
      ViewManagerList aViewManagerList;
      anApp->viewManagers(SVTK_Viewer::Type(), aViewManagerList);
      QList<SUIT_ViewManager*>::Iterator anVMIter = aViewManagerList.begin();
      for (; anVMIter != aViewManagerList.end(); anVMIter++ ) {
        SUIT_ViewManager* aViewManager = *anVMIter;
        QVector<SUIT_ViewWindow*> aViews = aViewManager->getViews();
        for (int i = 0, iEnd = aViews.size(); i < iEnd; i++) {
          if (SUIT_ViewWindow* aViewWindow = aViews.at(i)) {
            if (Plot2d_ViewWindow* vw = dynamic_cast<Plot2d_ViewWindow*>(aViewWindow)) {
              Plot2d_ViewFrame* vf = vw->getViewFrame();
              QList<Plot2d_Curve*> clist;
              vf->getCurves(clist);
              for (int i = 0; i < clist.count(); i++) {
                if (SPlot2d_Curve* cu = dynamic_cast<SPlot2d_Curve*>(clist.at(i))) {
                  if (cu->hasIO() &&
                      strcmp(myPrs->GetEntry().c_str(), cu->getIO()->getEntry())) {
                    vf->eraseCurve(cu);
                  }
                }
              }
              vf->Repaint();
              //jfa tmp:aViewFrame->unHighlightAll();
            }
          }
        }
      }
    }
  };

  void DeleteActors (VISU::Curve_i* thePrs)
  {
    if (!thePrs) return;
    ProcessVoidEvent(new TDeleteActorsEvent (thePrs));

/*    // 1. Find appropriate application (code like in TNewViewManagerEvent::Execute())
    SALOMEDS::Study_var myStudyDocument = thePrs->GetStudyDocument();
    SalomeApp_Application* anApp = NULL;
    CORBA::String_var studyName = myStudyDocument->Name();
    std::string aStudyName = studyName.in();
    SUIT_Session* aSession = SUIT_Session::session();
    QPtrList<SUIT_Application> anApplications = aSession->applications();
    QPtrListIterator<SUIT_Application> anIter (anApplications);
    while (SUIT_Application* aSUITApp = anIter.current()) {
      ++anIter;
      if (SUIT_Study* aSStudy = aSUITApp->activeStudy()) {
        if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
          if (_PTR(Study) aCStudy = aStudy->studyDS()) {
            if (aStudyName == aCStudy->Name()) {
              anApp = dynamic_cast<SalomeApp_Application*>(aSUITApp);
              break;
            }
          }
        }
      }
    }
    if (!anApp)
      return;

    // 2. Remove corresponding IO from selection
    SALOMEDS::SObject_var aSObject = thePrs->GetSObject();
    CORBA::String_var anEntry = aSObject->GetID();

    LightApp_SelectionMgr* aSelectionMgr = anApp->selectionMgr();
    SALOME_ListIO aListIO, aNewListIO;
    aSelectionMgr->selectedObjects(aListIO);

    for (SALOME_ListIteratorOfListIO it (aListIO); it.More(); it.Next()) {
      if (it.Value()->hasEntry()) {
        std::string aCurEntry (it.Value()->getEntry());
        if (aCurEntry != std::string( anEntry.in() ) ) {
          aNewListIO.Append(it.Value());
        }
      }
    }

    aSelectionMgr->setSelectedObjects(aNewListIO);

    // 3. Remove Actors
    ViewManagerList aViewManagerList;
    anApp->viewManagers(SVTK_Viewer::Type(), aViewManagerList);
    QPtrListIterator<SUIT_ViewManager> anVMIter (aViewManagerList);
    for (; anVMIter.current(); ++anVMIter) {
      SUIT_ViewManager* aViewManager = anVMIter.current();
      QPtrVector<SUIT_ViewWindow> aViews = aViewManager->getViews();
      for (int i = 0, iEnd = aViews.size(); i < iEnd; i++) {
        if (SUIT_ViewWindow* aViewWindow = aViews.at(i)) {
          if (Plot2d_ViewWindow* vw = dynamic_cast<Plot2d_ViewWindow*>(aViewWindow)) {
            Plot2d_ViewFrame* vf = vw->getViewFrame();
            QList<Plot2d_Curve> clist;
            vf->getCurves(clist);
            for (int i = 0; i < clist.count(); i++) {
              if (SPlot2d_Curve* cu = dynamic_cast<SPlot2d_Curve*>(clist.at(i))) {
                if (cu->hasIO() &&
                    strcmp(cu->getIO()->getEntry(), thePrs->GetEntry()) == 0) {
                  vf->eraseCurve(cu);
                }
              }
            }
            vf->Repaint();
            //jfa tmp:aViewFrame->unHighlightAll();
          }
        }
      }
}*/
  }

  void DeleteActors (VISU::Prs3d_i* thePrs)
  {
    if (!thePrs) return;

    // 1. Find appropriate application (code like in TNewViewManagerEvent::Execute())
    SALOMEDS::Study_var myStudyDocument = thePrs->GetStudyDocument();
    SalomeApp_Application* anApp = NULL;
    CORBA::String_var studyName = myStudyDocument->Name();
    std::string aStudyName = studyName.in();
    SUIT_Session* aSession = SUIT_Session::session();
    QList<SUIT_Application*> anApplications = aSession->applications();
    QList<SUIT_Application*>::Iterator anIter = anApplications.begin();
    while (anIter != anApplications.end()) {
      SUIT_Application* aSUITApp = *anIter;
      if (SUIT_Study* aSStudy = aSUITApp->activeStudy()) {
        if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
          if (_PTR(Study) aCStudy = aStudy->studyDS()) {
            if (aStudyName == aCStudy->Name()) {
              anApp = dynamic_cast<SalomeApp_Application*>(aSUITApp);
              break;
            }
          }
        }
      }
      anIter++;
    }
    if (!anApp)
      return;

    // 2. Remove corresponding IO from selection
    SALOMEDS::SObject_var aSObject = thePrs->GetSObject();
    CORBA::String_var anEntry = aSObject->GetID();

    LightApp_SelectionMgr* aSelectionMgr = anApp->selectionMgr();
    SALOME_ListIO aListIO, aNewListIO;
    aSelectionMgr->selectedObjects(aListIO);

    for (SALOME_ListIteratorOfListIO it (aListIO); it.More(); it.Next()) {
      if (it.Value()->hasEntry()) {
        std::string aCurEntry (it.Value()->getEntry());
        if (aCurEntry != std::string( anEntry.in() ) ) {
          aNewListIO.Append(it.Value());
        }
      }
    }

    aSelectionMgr->setSelectedObjects(aNewListIO);

    // 3. Remove Actors
    ViewManagerList aViewManagerList;
    anApp->viewManagers(SVTK_Viewer::Type(), aViewManagerList);
    QList<SUIT_ViewManager*>::Iterator anVMIter = aViewManagerList.begin();
    for (; anVMIter != aViewManagerList.end(); anVMIter++ ) {
      SUIT_ViewManager* aViewManager = *anVMIter;
      QVector<SUIT_ViewWindow*> aViews = aViewManager->getViews();
      for (int i = 0, iEnd = aViews.size(); i < iEnd; i++) {
        if (SUIT_ViewWindow* aViewWindow = aViews.at(i)) {
          if (SVTK_ViewWindow* vw = dynamic_cast<SVTK_ViewWindow*>(aViewWindow)) {
            VISU_Actor* anActor = NULL;
            VTK::ActorCollectionCopy aCopy(vw->getRenderer()->GetActors());
            vtkActorCollection *anActColl = aCopy.GetActors();
            anActColl->InitTraversal();
            vtkActor *aVTKActor = anActColl->GetNextActor();
            for (; !anActor && aVTKActor; aVTKActor = anActColl->GetNextActor()) {
              if (VISU_Actor* anVISUActor = dynamic_cast<VISU_Actor*>(aVTKActor)) {
                if (thePrs == anVISUActor->GetPrs3d()) {
                  anActor = anVISUActor;
                }
              }
            }
            if (anActor) {
              vw->RemoveActor(anActor);
            }
          }
        }
      }
    }
  }
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_CutLines_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_Actor.h"
#include "VISU_CutLinesPL.hxx"
#include "VISU_Convertor.hxx"
#include "VISU_PipeLineUtils.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <vtkAppendPolyData.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
size_t
VISU::CutLines_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity, 
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber, 
	     bool theIsMemoryCheck)
{
  return TSuperClass::IsPossible(theResult,
				 theMeshName,
				 theEntity,
				 theFieldName,
				 theTimeStampNumber,
				 theIsMemoryCheck);
}

//---------------------------------------------------------------
int VISU::CutLines_i::myNbPresent = 0;

//---------------------------------------------------------------
QString 
VISU::CutLines_i::GenerateName() 
{ 
  return VISU::GenerateName("CutLines",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::CutLines_i::myComment = "CUTLINES";

//---------------------------------------------------------------
const char* 
VISU::CutLines_i
::GetComment() const 
{ 
  return myComment.c_str();
}

//----------------------------------------------------------------------------
const char*
VISU::CutLines_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_CUT_LINES";
  else
    return "ICON_TREE_CUT_LINES_GROUPS";
}

//---------------------------------------------------------------
VISU::CutLines_i::
CutLines_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  CutLinesBase_i(thePublishInStudyMode),
  myCutLinesPL(NULL)
{}


//---------------------------------------------------------------
void
VISU::CutLines_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::CutLines_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::CutLines_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetDisplacement(VISU::Storable::FindValue(theMap,"myDisplacement[0]").toDouble());
  SetDisplacement2(VISU::Storable::FindValue(theMap,"myDisplacement[1]").toDouble());
  SetOrientation(CutPlanes::Orientation(VISU::Storable::FindValue(theMap,"myBasePlane[0]").toInt()),
		 Storable::FindValue(theMap,"aRot[0][0]").toDouble(),
		 Storable::FindValue(theMap,"aRot[0][1]").toDouble());
  SetOrientation2(CutPlanes::Orientation(VISU::Storable::FindValue(theMap,"myBasePlane[1]").toInt()),
		  Storable::FindValue(theMap,"aRot[1][0]").toDouble(),
		  Storable::FindValue(theMap,"aRot[1][1]").toDouble());

  if (VISU::Storable::FindValue(theMap,"myBasePlaneCondition").toInt())
    SetDefault();
  else
    SetBasePlanePosition(VISU::Storable::FindValue(theMap,"myBasePlanePosition").toDouble());
  
  QStringList aPosList = VISU::Storable::FindValue(theMap,"myLinePosition").split("|", QString::SkipEmptyParts );
  QStringList aCondList = VISU::Storable::FindValue(theMap,"myLineCondition").split("|",QString::SkipEmptyParts );
  for(int i = 0, iEnd = GetNbLines(); i < iEnd; i++)
    if(aCondList[i].toInt() == 0)
      SetLinePosition(i,aPosList[i].toDouble());

  return this;
}


//---------------------------------------------------------------
void
VISU::CutLines_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myDisplacement[0]", GetDisplacement() );
  Storable::DataToStream( theStr, "myDisplacement[1]", GetDisplacement2() );
  Storable::DataToStream( theStr, "myBasePlane[0]",    int(GetOrientationType()) );
  Storable::DataToStream( theStr, "myBasePlane[1]",    int(GetOrientationType2()) );
  Storable::DataToStream( theStr, "aRot[0][0]",        GetRotateX() );
  Storable::DataToStream( theStr, "aRot[0][1]",        GetRotateY() );
  Storable::DataToStream( theStr, "aRot[1][0]",        GetRotateX2() );
  Storable::DataToStream( theStr, "aRot[1][1]",        GetRotateY2() );
  Storable::DataToStream( theStr, "myBasePlanePosition", GetBasePlanePosition() );
  Storable::DataToStream( theStr, "myBasePlaneCondition", IsDefault() );

  QString aStrPos, aStrCon;
  for(int i = 0, iEnd = GetNbLines(); i < iEnd; i++){
    aStrPos.append(QString::number(GetLinePosition(i)) + "|");
    aStrCon.append(QString::number(IsDefaultPosition(i)) + "|");
  }

  Storable::DataToStream( theStr, "myLinePosition",  (const char*)aStrPos.toLatin1());
  Storable::DataToStream( theStr, "myLineCondition", (const char*)aStrCon.toLatin1());
}


//---------------------------------------------------------------
VISU::CutLines_i
::~CutLines_i()
{
  if(MYDEBUG) MESSAGE("CutLines_i::~CutLines_i()");
}


//---------------------------------------------------------------
void
VISU::CutLines_i
::SetOrientation(VISU::CutPlanes::Orientation theOrient,
		 CORBA::Double theXAngle, 
		 CORBA::Double theYAngle)
{
  struct TEvent: public SALOME_Event 
  {
    VISU_CutLinesPL* myCutLinesPL;
    CORBA::Double myXAngle, myYAngle;
    VISU::CutPlanes::Orientation myOrient;
    TEvent(VISU_CutLinesPL* theCutLines, 
	   VISU::CutPlanes::Orientation theOrient,
	   CORBA::Double theXAngle, 
	   CORBA::Double theYAngle):
      myCutLinesPL(theCutLines), 
      myOrient(theOrient), 
      myXAngle(theXAngle), 
      myYAngle(theYAngle)
    {}

    virtual
    void
    Execute()
    {
      myCutLinesPL->SetOrientation(VISU_CutPlanesPL::PlaneOrientation(myOrient),
				   myXAngle, 
				   myYAngle);
    }
  };

  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TEvent(myCutLinesPL, theOrient, theXAngle, theYAngle));
}

//---------------------------------------------------------------
void 
VISU::CutLines_i
::SetOrientation2(VISU::CutPlanes::Orientation theOrient,
		  CORBA::Double theXAngle, 
		  CORBA::Double theYAngle)
{
  struct TEvent: public SALOME_Event 
  {
    VISU_CutLinesPL* myCutLinesPL;
    CORBA::Double myXAngle, myYAngle;
    VISU::CutPlanes::Orientation myOrient;
    TEvent(VISU_CutLinesPL* theCutLines, 
	   VISU::CutPlanes::Orientation theOrient,
	   CORBA::Double theXAngle, 
	   CORBA::Double theYAngle):
      myCutLinesPL(theCutLines), 
      myOrient(theOrient), 
      myXAngle(theXAngle), 
      myYAngle(theYAngle)
    {}

    virtual
    void
    Execute()
    {
      myCutLinesPL->SetOrientation(VISU_CutPlanesPL::PlaneOrientation(myOrient),
				   myXAngle, 
				   myYAngle,
				   1);
    }
  };

  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TEvent(myCutLinesPL, theOrient, theXAngle, theYAngle));
}

//---------------------------------------------------------------
VISU::CutPlanes::Orientation 
VISU::CutLines_i
::GetOrientationType() 
{ 
  return VISU::CutPlanes::Orientation(myCutLinesPL->GetPlaneOrientation());
}

//---------------------------------------------------------------
VISU::CutPlanes::Orientation 
VISU::CutLines_i
::GetOrientationType2() 
{ 
  return VISU::CutPlanes::Orientation(myCutLinesPL->GetPlaneOrientation(1));
}

//---------------------------------------------------------------
CORBA::Double
VISU::CutLines_i
::GetRotateX()
{
  return myCutLinesPL->GetRotateX();
}

//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetRotateY()
{
  return myCutLinesPL->GetRotateY();
}


//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetRotateX2()
{
  return myCutLinesPL->GetRotateX(1);
}

//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetRotateY2()
{
  return myCutLinesPL->GetRotateY(1);
}


//---------------------------------------------------------------
void
VISU::CutLines_i
::SetDisplacement(CORBA::Double theDisp) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_CutLinesPL, double, int>
		   (GetSpecificPL(), &VISU_CutLinesPL::SetDisplacement, theDisp, 0));
}

//---------------------------------------------------------------
void
VISU::CutLines_i
::SetDisplacement2(CORBA::Double theDisp) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_CutLinesPL, double, int>
		   (GetSpecificPL(), &VISU_CutLinesPL::SetDisplacement, theDisp, 1));
}


//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetDisplacement() 
{ 
  return myCutLinesPL->GetDisplacement();
}

//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetDisplacement2() 
{ 
  return myCutLinesPL->GetDisplacement(1);
}


//---------------------------------------------------------------
void 
VISU::CutLines_i
::SetBasePlanePosition(CORBA::Double thePlanePosition)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_CutLinesPL, double>
		   (GetSpecificPL(), &VISU_CutLinesPL::SetPosition, thePlanePosition));
}

//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetBasePlanePosition()
{ 
  return myCutLinesPL->GetPosition();
}

//---------------------------------------------------------------
void 
VISU::CutLines_i
::SetLinePosition(CORBA::Long thePlaneNumber, 
		  CORBA::Double thePlanePosition)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_CutLinesPL, int, double>
		   (GetSpecificPL(), &VISU_CutLinesPL::SetPartPosition, thePlaneNumber, thePlanePosition));
}

//---------------------------------------------------------------
CORBA::Double 
VISU::CutLines_i
::GetLinePosition(CORBA::Long thePlaneNumber)
{ 
  return myCutLinesPL->GetPartPosition(thePlaneNumber,1);
}


//---------------------------------------------------------------
void
VISU::CutLines_i
::SetDefault()
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFunEvent<VISU_CutLinesPL>
		   (GetSpecificPL(), &VISU_CutLinesPL::SetDefault));
}

//---------------------------------------------------------------
CORBA::Boolean
VISU::CutLines_i
::IsDefault()
{ 
  return myCutLinesPL->IsDefault();
}

//---------------------------------------------------------------
void
VISU::CutLines_i
::SetDefaultPosition(CORBA::Long thePlaneNumber)
{
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_CutLinesPL, int>
		   (GetSpecificPL(), &VISU_CutLinesPL::SetPartDefault, thePlaneNumber));
}

//---------------------------------------------------------------
CORBA::Boolean
VISU::CutLines_i
::IsDefaultPosition(CORBA::Long thePlaneNumber)
{ 
  return myCutLinesPL->IsPartDefault(thePlaneNumber);
}

//---------------------------------------------------------------
void
VISU::CutLines_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myCutLinesPL = VISU_CutLinesPL::New();
  }else
    myCutLinesPL = dynamic_cast<VISU_CutLinesPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myCutLinesPL);
}


//----------------------------------------------------------------------------
bool
VISU::CutLines_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}


//---------------------------------------------------------------
VISU_Actor* 
VISU::CutLines_i
::CreateActor()
{
  if(VISU_Actor* anActor = TSuperClass::CreateActor()){
    anActor->SetVTKMapping(true);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aDispMode = aResourceMgr->integerValue("VISU" , "cut_lines_represent", 2);
    anActor->SetRepresentation(aDispMode);
    return anActor;
  }
  return NULL;
}

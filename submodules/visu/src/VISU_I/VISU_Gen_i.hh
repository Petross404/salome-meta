// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Gen_i.hh
//  Author : Alexey Petrov
//  Module : VISU
//
#ifndef __VISU_GEN_I_H__
#define __VISU_GEN_I_H__

#include "VISUConfig.hh"

#include "VISU_I.hxx"
#include "SALOME_Component_i.hxx"
#include "SALOME_NamingService.hxx"
#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_ClippingPlaneMgr.hxx"

#include <VTKViewer_MarkerDef.h>

#include <map>
#include <string>

namespace VISU
{
  //----------------------------------------------------------------------------
  typedef std::map<int, VTK::MarkerMap> StudyId2MarkerMap;

  //----------------------------------------------------------------------------
  class VISU_Gen_i : public virtual POA_VISU::VISU_Gen,
		     public virtual ::Engines_Component_i,
		     public virtual Base_i
  {
    SALOMEDS::Study_var myStudyDocument;
    VISU_ClippingPlaneMgr myClippingPlaneMgr;

    StudyId2MarkerMap myMarkerMap;

    VISU_Gen_i(const VISU::VISU_Gen_i &);
  public:
    VISU_Gen_i(CORBA::ORB_ptr theORB,
	       PortableServer::POA_ptr thePOA,
	       SALOME_NamingService* theNamingService,
	       QMutex* theMutex);

    virtual 
    ~VISU_Gen_i();

    virtual 
    char* 
    GetID();

    virtual
    VISU::VISUType 
    GetType() { return VISU::TVISUGEN;};

    virtual
    void
    SetCurrentStudy(SALOMEDS::Study_ptr theStudy);

    virtual
    SALOMEDS::Study_ptr
    GetCurrentStudy();

    virtual
    ViewManager_ptr
    GetViewManager();

    virtual
    SALOMEDS::SObject_ptr
    ImportTables(const char* theFileName, bool theFirstStrAsTitle = false);

    virtual
    CORBA::Boolean
    ExportTableToFile(SALOMEDS::SObject_ptr theTable, 
		      const char* theFileName );

    //Create Result
    virtual
    Result_ptr
    ImportFile(const char* theFileName);

    virtual
    Result_ptr
    CreateResult(const char* theFileName);

    virtual
    Result_ptr
    CopyAndImportFile(const char* theFileName);

    virtual
    Result_ptr
    ImportMed(SALOMEDS::SObject_ptr theMedSObject);

    virtual
    Result_ptr
    ImportMedField(SALOME_MED::FIELD_ptr theField);


    //Rename Presentation Of Submeshes
    void
    RenameMeshInStudy(Result_ptr theResult,
		      const std::string& theMeshName,
		      int theEntity, // -1 for group indication
		      const std::string& theSubMeshName, // Family or Group name
		      const std::string& theNewName);
    virtual
    void
    RenameEntityInStudy(Result_ptr   theResult,
			const char*  theMeshName,
			VISU::Entity theEntity,
			const char*  theNewName);
    virtual
    void
    RenameFamilyInStudy(Result_ptr   theResult,
			const char*  theMeshName,
			VISU::Entity theEntity,
			const char*  theFamilyName,
			const char*  theNewName);
    virtual
    void
    RenameGroupInStudy(Result_ptr  theResult,
		       const char*  theMeshName,
		       const char* theGroupName,
		       const char* theNewName);

    //Create Presentation Of Submeshes
    virtual
    Mesh_ptr
    MeshOnEntity(Result_ptr theResult, 
		 const char* theMeshName, 
		 VISU::Entity theEntity);

    virtual
    Mesh_ptr
    FamilyMeshOnEntity(Result_ptr theResult, 
		       const char* theMeshName,
		       VISU::Entity theEntity, 
		       const char* theFamilyName);

    virtual
    Mesh_ptr
    GroupMesh(Result_ptr theResult, 
	      const char* theMeshName, 
	      const char* theGroupName);

    Prs3d_ptr
    CreatePrs3d(VISUType theType,
		SALOMEDS::Study_ptr theStudy);

    virtual
    ScalarMap_ptr
    ScalarMapOnField(Result_ptr theResult,
		     const char* theMeshName, 
		     VISU::Entity theEntity,
		     const char* theFieldName, 
		     CORBA::Long theIteration);

    virtual
    GaussPoints_ptr
    GaussPointsOnField(Result_ptr theResult,
		       const char* theMeshName, 
		       VISU::Entity theEntity,
		       const char* theFieldName, 
		       CORBA::Long theIteration);

    virtual
    DeformedShape_ptr
    DeformedShapeOnField(Result_ptr theResult,
			 const char* theMeshName, 
			 VISU::Entity theEntity,
			 const char* theFieldName, 
			 CORBA::Long theIteration);

    //This function is obsolete. Use DeformedShapeAndScalarMapOnField instead.
    virtual
    DeformedShapeAndScalarMap_ptr
    ScalarMapOnDeformedShapeOnField(Result_ptr theResult,
				    const char* theMeshName, 
				    VISU::Entity theEntity,
				    const char* theFieldName, 
				    CORBA::Long theIteration);

    virtual
    DeformedShapeAndScalarMap_ptr
    DeformedShapeAndScalarMapOnField(Result_ptr theResult,
				     const char* theMeshName, 
				     VISU::Entity theEntity,
				     const char* theFieldName, 
				     CORBA::Long theIteration);

    virtual
    Vectors_ptr
    VectorsOnField(Result_ptr theResult,
		   const char* theMeshName, 
		   VISU::Entity theEntity,
		   const char* theFieldName, 
		   CORBA::Long theIteration);

    virtual
    IsoSurfaces_ptr
    IsoSurfacesOnField(Result_ptr theResult,
		       const char* theMeshName, 
		       VISU::Entity theEntity,
		       const char* theFieldName, 
		       CORBA::Long theIteration);

    virtual
    CutPlanes_ptr
    CutPlanesOnField(Result_ptr theResult,
		     const char* theMeshName, 
		     VISU::Entity theEntity,
		     const char* theFieldName, 
		     CORBA::Long theIteration);

    virtual
    CutLines_ptr
    CutLinesOnField(Result_ptr theResult,
		    const char* theMeshName, 
		    VISU::Entity theEntity,
		    const char* theFieldName, 
		    CORBA::Long theIteration);

    virtual
    CutSegment_ptr
    CutSegmentOnField(Result_ptr theResult,
		      const char* theMeshName, 
		      VISU::Entity theEntity,
		      const char* theFieldName, 
		      CORBA::Long theIteration);

    virtual
    StreamLines_ptr
    StreamLinesOnField(Result_ptr theResult,
		       const char* theMeshName, 
		       VISU::Entity theEntity,
		       const char* theFieldName, 
		       CORBA::Long theIteration);

    virtual
    Plot3D_ptr
    Plot3DOnField(Result_ptr theResult,
		  const char* theMeshName, 
		  VISU::Entity theEntity,
		  const char* theFieldName, 
		  CORBA::Long theIteration);

    //Create Digital Presentation
    virtual 
    Table_ptr
    CreateTable(const char* theTableEntry);

    virtual
    Curve_ptr
    CreateCurve(Table_ptr theTable, 
		CORBA::Long theHRow, 
		CORBA::Long theVRow);

    virtual
    Curve_ptr
    CreateCurveWithZ(Table_ptr theTable, 
                     CORBA::Long theHRow, 
                     CORBA::Long theVRow,
                     CORBA::Long theZRow);

    virtual
    Curve_ptr
    CreateCurveWithZExt(Table_ptr theTable, 
                        CORBA::Long theHRow, 
                        CORBA::Long theVRow,
                        CORBA::Long theZRow,
                        CORBA::Boolean theIsV2);

    virtual
    Container_ptr 
    CreateContainer();

    virtual
    Animation_ptr
    CreateAnimation(View3D_ptr theView3d);

    virtual
    Evolution_ptr
    CreateEvolution(XYPlot_ptr theXYPLot);

    virtual
    void 
    DeleteResult(Result_ptr theResult);

    virtual
    void 
    DeletePrs3d(Prs3d_ptr thePrs3d);

    // inherited methods from Engines::Component
    virtual
    Engines::TMPFile*
    DumpPython(CORBA::Object_ptr theStudy,
	       CORBA::Boolean theIsPublished,
	       CORBA::Boolean theIsMultiFile,
	       CORBA::Boolean& theIsValidScript);

    // inherited methods from SALOMEDS::Driver
    virtual
    SALOMEDS::TMPFile* 
    Save(SALOMEDS::SComponent_ptr theComponent,
	 const char* theURL,
	 bool isMultiFile);

    virtual
    SALOMEDS::TMPFile* 
    SaveASCII(SALOMEDS::SComponent_ptr theComponent,
	      const char* theURL,
	      bool isMultiFile);

    virtual
    bool
    Load(SALOMEDS::SComponent_ptr,
	 const SALOMEDS::TMPFile &,
	 const char* theURL,
	 bool isMultiFile);

    virtual
    bool
    LoadASCII(SALOMEDS::SComponent_ptr,
	      const SALOMEDS::TMPFile &,
	      const char* theURL,
	      bool isMultiFile);
    
    virtual
    void
    Close(SALOMEDS::SComponent_ptr IORSComponent);

    virtual
    char* 
    ComponentDataType();

    virtual
    char* 
    IORToLocalPersistentID(SALOMEDS::SObject_ptr theSObject,
			   const char* IORString,
			   CORBA::Boolean isMultiFile,
			   CORBA::Boolean isASCII);

    virtual
    char* 
    LocalPersistentIDToIOR(SALOMEDS::SObject_ptr theSObject,
			   const char* aLocalPersistentID,
			   CORBA::Boolean isMultiFile,
			   CORBA::Boolean isASCII);

    virtual 
    bool
    CanPublishInStudy(CORBA::Object_ptr theIOR);

    virtual
    SALOMEDS::SObject_ptr
    PublishInStudy(SALOMEDS::Study_ptr theStudy,
		   SALOMEDS::SObject_ptr theSObject,
		   CORBA::Object_ptr theObject,
		   const char* theName) throw (SALOME::SALOME_Exception);

    CORBA::Boolean 
    CanCopy(SALOMEDS::SObject_ptr theObject);

    SALOMEDS::TMPFile* 
    CopyFrom(SALOMEDS::SObject_ptr theObject, 
	     CORBA::Long& theObjectID);

    CORBA::Boolean 
    CanPaste(const char* theComponentName, 
	     CORBA::Long theObjectID);

    SALOMEDS::SObject_ptr
    PasteInto(const SALOMEDS::TMPFile& theStream,
	      CORBA::Long theObjectID,
	      SALOMEDS::SObject_ptr theObject);


    char* getVersion();

    virtual
    VISU::ColoredPrs3dCache_ptr
    GetColoredPrs3dCache(SALOMEDS::Study_ptr theStudy);

    
    virtual CORBA::Long CreateClippingPlane(CORBA::Double X,CORBA::Double  Y, CORBA::Double Z, 
					    CORBA::Double dX, CORBA::Double dY, CORBA::Double dZ, 
					    CORBA::Boolean isAuto, const char* name);

    virtual void EditClippingPlane(CORBA::Long id, CORBA::Double X,CORBA::Double  Y, CORBA::Double Z, 
				     CORBA::Double dX, CORBA::Double dY, CORBA::Double dZ, 
				     CORBA::Boolean isAuto, const char* name);

    /* Returns clipping plane by its Id */
    virtual VISU::ClippingPlane* GetClippingPlane(CORBA::Long id);
    
    /* Deletes clipping plane by its Id */
    virtual CORBA::Boolean DeleteClippingPlane(CORBA::Long id);
    
    /* Applyes a clipping plane with Id to presentation thePrs */
    virtual CORBA::Boolean ApplyClippingPlane(Prs3d_ptr thePrs, CORBA::Long id);
    
    /* Detaches a clipping plane with Id from presentation thePrs */
    virtual CORBA::Boolean DetachClippingPlane(Prs3d_ptr thePrs, CORBA::Long id);
    
    /* Get number of clipping planes */
    virtual CORBA::Long GetClippingPlanesNb();

    VISU_ClippingPlaneMgr& GetClippingPlaneMgr() { return myClippingPlaneMgr; }

    /* Convert a set of VTK files to the MED file */
    virtual
    CORBA::Boolean
    VTK2MED(const VISU::string_array& theVTKFiles,
	    const char* theMEDFile,
	    const char* theMeshName,
	    const VISU::double_array& theTStamps);

    /* Load texture from file */
    virtual CORBA::Long LoadTexture(const char* theTextureFile);

    StudyId2MarkerMap& GetMarkerMap() { return myMarkerMap; }
  };
}

#endif

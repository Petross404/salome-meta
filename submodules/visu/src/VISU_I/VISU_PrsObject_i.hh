// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_PrsObject_i_HeaderFile
#define VISU_PrsObject_i_HeaderFile

#include "VISUConfig.hh"

namespace VISU
{
  //----------------------------------------------------------------------------
  class VISU_I_EXPORT RemovableObject_i : public virtual POA_VISU::RemovableObject,
					  public virtual Storable
  {
  public:
    typedef Storable TSuperClass;

    virtual 
    ~RemovableObject_i();

    SALOMEDS::Study_var
    GetStudyDocument() const;

    SalomeApp_Study* 
    GetGUIStudy() const;

    virtual
    std::string
    GetEntry();

    virtual 
    std::string
    GetName() const;

    virtual 
    void
    SetName(const std::string& theName,
	    bool theIsUpdateStudyAttr);

  protected:
    RemovableObject_i();

    void
    SetStudyDocument(SALOMEDS::Study_ptr theStudy);

  private:
    std::string myName;
    SalomeApp_Study* myGUIStudy;
    SALOMEDS::Study_var myStudyDocument;

    RemovableObject_i(const RemovableObject_i&);
  };


  //----------------------------------------------------------------------------
  class VISU_I_EXPORT PrsObject_i : public virtual POA_VISU::PrsObject,
				    public virtual RemovableObject_i
  {
    PrsObject_i(const PrsObject_i&);

  public:
    typedef RemovableObject_i TSuperClass;

    PrsObject_i(SALOMEDS::Study_ptr theStudy = SALOMEDS::Study::_nil());
  };
}

#endif

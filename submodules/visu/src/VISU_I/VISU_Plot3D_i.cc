// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VISU_Plot3D_i.hh"
#include "VISU_Prs3dUtils.hh"

#include "VISU_Result_i.hh"
#include "VISU_Plot3DPL.hxx"
#include "VISU_Actor.h"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
size_t 
VISU::Plot3D_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  return TSuperClass::IsPossible(theResult,
				 theMeshName,
				 theEntity,
				 theFieldName,
				 theTimeStampNumber,
				 theIsMemoryCheck);
}

//---------------------------------------------------------------
int VISU::Plot3D_i::myNbPresent = 0;

//---------------------------------------------------------------
QString 
VISU::Plot3D_i
::GenerateName() 
{ 
  return VISU::GenerateName("Plot3D",myNbPresent++); 
}

//---------------------------------------------------------------
const string VISU::Plot3D_i::myComment = "PLOT3D";

//---------------------------------------------------------------
const char* 
VISU::Plot3D_i
::GetComment() const 
{ 
  return myComment.c_str(); 
}

//---------------------------------------------------------------
const char*
VISU::Plot3D_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_PLOT_3D";
  else
    return "ICON_TREE_PLOT_3D_GROUPS";
}

//---------------------------------------------------------------
VISU::Plot3D_i
::Plot3D_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  myPlot3DPL(NULL)
{}


//---------------------------------------------------------------
VISU::Storable* 
VISU::Plot3D_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::Plot3D_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetOrientation(VISU::Plot3D::Orientation(VISU::Storable::FindValue(theMap,"myBasePlane").toInt()),
		 Storable::FindValue(theMap,"aRot[0]").toDouble(),
		 Storable::FindValue(theMap,"aRot[1]").toDouble());
  SetPlanePosition(VISU::Storable::FindValue(theMap,"myPlanePosition").toDouble(),
		   VISU::Storable::FindValue(theMap,"myPlaneCondition").toInt());
  SetScaleFactor(VISU::Storable::FindValue(theMap,"myScaleFactor").toDouble());
  SetContourPrs(VISU::Storable::FindValue(theMap,"myContourPrs").toInt());
  SetNbOfContours(VISU::Storable::FindValue(theMap,"myNbOfContours").toInt());

  return this;
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream(theStr, "myBasePlane", int(GetOrientationType()));
  Storable::DataToStream(theStr, "aRot[0]", GetRotateX());
  Storable::DataToStream(theStr, "aRot[1]", GetRotateY());
  Storable::DataToStream(theStr, "myPlanePosition", GetPlanePosition());
  Storable::DataToStream(theStr, "myPlaneCondition", int(IsPositionRelative()));
  Storable::DataToStream(theStr, "myScaleFactor", GetScaleFactor());
  Storable::DataToStream(theStr, "myContourPrs", int(GetIsContourPrs()));
  Storable::DataToStream(theStr, "myNbOfContours", int(GetNbOfContours()));
}


//---------------------------------------------------------------
VISU::Plot3D_i
::~Plot3D_i()
{
  if(MYDEBUG) MESSAGE("Plot3D_i::~Plot3D_i()");
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::SetOrientation(VISU::Plot3D::Orientation theOrient,
		 CORBA::Double theXAngle, 
		 CORBA::Double theYAngle)
{
  struct TEvent: public SALOME_Event 
  {
    VISU_Plot3DPL* myPipeLine;
    VISU_CutPlanesPL::PlaneOrientation myOrient;
    CORBA::Double myXAngle;
    CORBA::Double myYAngle;

    TEvent(VISU_Plot3DPL* thePipeLine,
	   VISU_CutPlanesPL::PlaneOrientation theOrient,
	   CORBA::Double theXAngle, 
	   CORBA::Double theYAngle):
      myPipeLine(thePipeLine),
      myOrient(theOrient),
      myXAngle(theXAngle),
      myYAngle(theYAngle)
    {}

    virtual
    void
    Execute()
    {
      myPipeLine->SetOrientation(myOrient,
				 myXAngle,
				 myYAngle);
    }
  };

  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TEvent(GetSpecificPL(),
			      VISU_CutPlanesPL::PlaneOrientation(theOrient),
			      theXAngle,
			      theYAngle));
}


//---------------------------------------------------------------
VISU::Plot3D::Orientation
VISU::Plot3D_i
::GetOrientationType()
{
  return VISU::Plot3D::Orientation(myPlot3DPL->GetPlaneOrientation());
}


//---------------------------------------------------------------
CORBA::Double
VISU::Plot3D_i
::GetRotateX()
{
  return myPlot3DPL->GetRotateX();
}


//---------------------------------------------------------------
CORBA::Double 
VISU::Plot3D_i
::GetRotateY()
{
  return myPlot3DPL->GetRotateY();
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::SetPlanePosition(CORBA::Double  thePlanePosition,
		   CORBA::Boolean theIsRelative)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_Plot3DPL, double, bool>
		   (GetSpecificPL(), &VISU_Plot3DPL::SetPlanePosition, thePlanePosition, theIsRelative));
}


//---------------------------------------------------------------
CORBA::Double
VISU::Plot3D_i
::GetPlanePosition()
{
  return myPlot3DPL->GetPlanePosition();
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::Plot3D_i
::IsPositionRelative()
{
  return myPlot3DPL->IsPositionRelative();
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::SetScaleFactor(CORBA::Double theScaleFactor)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_Plot3DPL, double>
		   (GetSpecificPL(), &VISU_Plot3DPL::SetScaleFactor, theScaleFactor));
}


//---------------------------------------------------------------
CORBA::Double
VISU::Plot3D_i
::GetScaleFactor()
{
  return myPlot3DPL->GetScaleFactor();
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::SetNbOfContours(CORBA::Long theNb)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_Plot3DPL, int>
		   (GetSpecificPL(), &VISU_Plot3DPL::SetNumberOfContours, theNb));
}


//---------------------------------------------------------------
CORBA::Long
VISU::Plot3D_i
::GetNbOfContours()
{
  return myPlot3DPL->GetNumberOfContours();
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::SetContourPrs(CORBA::Boolean theIsContourPrs)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_Plot3DPL, bool>
		   (GetSpecificPL(), &VISU_Plot3DPL::SetContourPrs, theIsContourPrs));
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::Plot3D_i
::GetIsContourPrs()
{
  return myPlot3DPL->GetIsContourPrs();
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myPlot3DPL = VISU_Plot3DPL::New();
  }else
    myPlot3DPL = dynamic_cast<VISU_Plot3DPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myPlot3DPL);
}


//---------------------------------------------------------------
bool
VISU::Plot3D_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

//---------------------------------------------------------------
VISU_Actor* 
VISU::Plot3D_i
::CreateActor()
{
  if(VISU_Actor* anActor = TSuperClass::CreateActor()){
    anActor->SetVTKMapping(true);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int aDispMode = aResourceMgr->integerValue("VISU", "plot3d_represent", 2);
    anActor->SetRepresentation(aDispMode);
    return anActor;
  }
  return NULL;
}


//---------------------------------------------------------------
void
VISU::Plot3D_i
::SetMapScale(double theMapScale)
{
  myPlot3DPL->SetMapScale(theMapScale);
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Mesh_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Tools.h"

#include "VISU_Result_i.hh"

#include "VISU_MeshAct.h"
#include "VISU_MeshPL.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_Convertor.hxx"

#include "SALOME_Event.h"
#include <SUIT_ResourceMgr.h>


#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

static int INCMEMORY = 4;

using namespace std;

//----------------------------------------------------------------------------
namespace
{
  bool
  IsSameColor(const SALOMEDS::Color& theLeft,
	      const SALOMEDS::Color& theRight)
  {
    return 
      VISU::CheckIsSameValue(theLeft.R, theRight.R) &&
      VISU::CheckIsSameValue(theLeft.G, theRight.G) &&
      VISU::CheckIsSameValue(theLeft.B, theRight.B);
  }
}

//----------------------------------------------------------------------------
int VISU::Mesh_i::myNbPresent = 0;

//----------------------------------------------------------------------------
QString
VISU::Mesh_i
::GenerateName() 
{ 
  return VISU::GenerateName("Mesh", myNbPresent++);
}

//----------------------------------------------------------------------------
const string VISU::Mesh_i::myComment = "MESH";

//----------------------------------------------------------------------------
const char* 
VISU::Mesh_i
::GetComment() const
{ 
  return myComment.c_str();
}

//----------------------------------------------------------------------------
VISU::Mesh_i
::Mesh_i():
  myMeshPL(VISU_MeshPL::New())
{
  SetPipeLine(myMeshPL);
}


//----------------------------------------------------------------------------
size_t
VISU::Mesh_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName,
	     Entity theEntity, 
	     const std::string& theFamilyName)
{
  try{
    MESSAGE("Mesh_i::IsPossible - theMeshName = '"<<theMeshName<<"'; theEntity = "<<theEntity<<"; theFamilyName = '"<<theFamilyName<<"'");
    size_t aSize = INCMEMORY*
      theResult->GetInput()->GetMeshOnEntitySize(theMeshName,(VISU::TEntity)theEntity);
    size_t aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
    MESSAGE("Mesh_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<aResult);
    return aResult;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return 0;
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::Mesh_i
::Create(VISU::Result_i* theResult, 
	 const std::string& theMeshName, 
	 VISU::Entity theEntity, 
	 const std::string& theFamilyName)
{
  SetCResult(theResult);
  SetMeshName(theMeshName.c_str());
  myEntity = int(theEntity);//jfa IPAL9284
  mySubMeshName = theFamilyName;
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  myIsShrank = aResourceMgr->booleanValue("VISU", "mesh_shrink", false);
  return Build(false);
}


//----------------------------------------------------------------------------
size_t 
VISU::Mesh_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     const std::string& theGroupName)
{
  try{
    MESSAGE("Mesh_i::IsPossible - theMeshName = '"<<theMeshName<<"'; theGroupName = '"<<theGroupName<<"'");
    size_t aSize = INCMEMORY*
      theResult->GetInput()->GetMeshOnGroupSize(theMeshName,theGroupName);
    size_t aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
    INFOS("Mesh_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<aResult);
    return aResult;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return 0;
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::Mesh_i
::Create(VISU::Result_i* theResult, 
	 const std::string& theMeshName, 
	 const std::string& theGroupName)
{
  SetCResult(theResult);
  SetMeshName(theMeshName.c_str());
  myEntity = -1;//jfa IPAL9284
  mySubMeshName = theGroupName;
  return Build(false);
}


//----------------------------------------------------------------------------
VISU::Mesh_i
::~Mesh_i()
{
  if(MYDEBUG) MESSAGE("Mesh_i::~Mesh_i()");
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::Mesh_i
::Build(int theRestoring)
{
  SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
  aStudyBuilder->NewCommand();  // There is a transaction
  try {
    if (GetCResult()->GetInput() == NULL)
      throw std::runtime_error("Mesh_i::Build - GetCResult()->GetInput() == NULL !!!");
    if (!theRestoring) {
      myPresentType =  VISU::PresentationType(VISU::GetResourceMgr()->integerValue("VISU" , "mesh_represent", 2));
      myIsShrank |= (myPresentType == VISU::SHRINK);

      if(myEntity == VISU::NODE_ENTITY)
	myPresentType = VISU::POINT;
      SetName((const char*)GenerateName().toLatin1(), false);
      myCellColor.R = 0.0;  myCellColor.G = myCellColor.B = 1.0;
      myNodeColor.R = myNodeColor.G = 1.0;  myNodeColor.B = 1.0;

      QColor aColor = VISU::GetResourceMgr()->colorValue( "VISU", "edge_color", QColor( 255, 255, 255 ) );
      SALOMEDS::Color aLinkColor;
      aLinkColor.R = aColor.red()/255.;
      aLinkColor.G = aColor.green()/255.;
      aLinkColor.B = aColor.blue()/255.;
      SetLinkColor(aLinkColor);

      my2DQuadPrsType = VISU::Quadratic2DPresentationType(VISU::GetResourceMgr()->integerValue( "VISU", "quadratic_mode", 0));
    }

    if(myEntity >= 0)
      if(mySubMeshName == "")
	myType = VISU::TENTITY;
      else
	myType = VISU::TFAMILY;
    else
      myType = VISU::TGROUP;

    if(MYDEBUG) MESSAGE("Mesh_i::Build - myType = "<<myType);
    QString aComment,aTmp;

    std::string aMeshName = GetCMeshName();
    VISU::TEntity anEntity = VISU::TEntity(myEntity);

    VISU::Storable::TRestoringMap aRestoringMap;
    aRestoringMap["myMeshName"] = GetCMeshName().c_str();

    VISU::PUnstructuredGridIDMapper anIDMapper;
    VISU::Result_i::PInput anInput = GetCResult()->GetInput();
    switch(myType){
    case VISU::TENTITY :
      anIDMapper = anInput->GetMeshOnEntity(aMeshName,
					    anEntity);//jfa IPAL9284
      aRestoringMap["myComment"] = "ENTITY";
      aRestoringMap["myId"] = QString::number(anEntity);
      break;
    case VISU::TFAMILY :
      anIDMapper = anInput->GetFamilyOnEntity(aMeshName,
					      anEntity,
					      mySubMeshName);
      aRestoringMap["myComment"] = "FAMILY";
      aRestoringMap["myEntityId"] = QString::number(anEntity);
      aRestoringMap["myName"] = mySubMeshName.c_str();
      break;
    case VISU::TGROUP :
      anIDMapper = anInput->GetMeshOnGroup(GetCMeshName(),
					   mySubMeshName);
      aRestoringMap["myComment"] = "GROUP";
      aRestoringMap["myName"] = mySubMeshName.c_str();
      break;
    }
    if(!anIDMapper) 
      throw std::runtime_error("Mesh_i::Build - !anIDMapper !!!");

    myMeshPL->SetUnstructuredGridIDMapper(anIDMapper);

    string anEntry = GetCResult()->GetEntry(aRestoringMap);
    if(anEntry == "") 
      throw std::runtime_error("Mesh_i::Build - anEntry was not found !!!");
    SALOMEDS::SObject_var aSObject = GetStudyDocument()->FindObjectID(anEntry.c_str());

    if(!theRestoring) { //Setting IOR on the label
      myMeshPL->Init();
      string aResultEntry = GetCResult()->GetEntry();
      SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
      SALOMEDS::GenericAttribute_var anAttr = aStudyBuilder->FindOrCreateAttribute(aSObject,"AttributeIOR");
      SALOMEDS::AttributeIOR_var anIOR = SALOMEDS::AttributeIOR::_narrow(anAttr);
      CORBA::String_var aString = GetID();
      anIOR->SetValue(aString);
    }

    // Set icon
    SALOMEDS::GenericAttribute_var anAttr;
    SALOMEDS::AttributePixMap_var  aPixmap;
    
    anAttr  = aStudyBuilder->FindOrCreateAttribute(aSObject, "AttributePixMap");
    aPixmap = SALOMEDS::AttributePixMap::_narrow( anAttr );
    aPixmap->SetPixMap("ICON_TREE_MESH");

  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
    return NULL;
  }catch(...){
    INFOS("Unknown exception was occured!");
    return NULL;
  }
  aStudyBuilder->CommitCommand();
  return this;
}


//---------------------------------------------------------------
void
VISU::Mesh_i
::SameAs(const VISU::Prs3d_i* theOrigin)
{
  return; // "SameAs" command for mesh is not valid in current architecture
}


//---------------------------------------------------------------
void
VISU::Mesh_i
::RemoveFromStudy()
{
  struct TRemoveFromStudy: public SALOME_Event
  {
    VISU::Mesh_i* myRemovable;
    TRemoveFromStudy(VISU::Mesh_i* theRemovable):
      myRemovable(theRemovable)
    {}
    
    virtual
    void
    Execute()
    {
      VISU::RemoveFromStudy(myRemovable->GetSObject());
      myRemovable->TSuperClass::RemoveFromStudy();
    }
  };

  // Unset icon
  SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
  aStudyBuilder->RemoveAttribute(GetSObject(), "AttributePixMap");

  //Set visibility state to Qtx::UnpresentableState
  VISU::SetVisibilityState(GetEntry(), Qtx::UnpresentableState);
	




  ProcessVoidEvent(new TRemoveFromStudy(this));
}


//----------------------------------------------------------------------------
void 
VISU::Mesh_i
::SetCellColor(const SALOMEDS::Color& theColor) 
{ 
  if(IsSameColor(myCellColor, theColor))
    return;

  VISU::TSetModified aModified(this);
  
  myCellColor = theColor;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
SALOMEDS::Color 
VISU::Mesh_i
::GetCellColor() 
{ 
  return myCellColor;
}


//----------------------------------------------------------------------------
void 
VISU::Mesh_i
::SetNodeColor(const SALOMEDS::Color& theColor) 
{ 
  if(IsSameColor(myNodeColor, theColor))
    return;

  VISU::TSetModified aModified(this);
  
  myNodeColor = theColor;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
SALOMEDS::Color 
VISU::Mesh_i
::GetNodeColor() 
{ 
  return myNodeColor;
}


//----------------------------------------------------------------------------
void 
VISU::Mesh_i
::SetLinkColor(const SALOMEDS::Color& theColor) 
{ 
  if(IsSameColor(myLinkColor, theColor))
    return;

  VISU::TSetModified aModified(this);
  
  myLinkColor = theColor;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
SALOMEDS::Color 
VISU::Mesh_i
::GetLinkColor() 
{ 
  return myLinkColor;
}


//----------------------------------------------------------------------------
void 
VISU::Mesh_i
::SetPresentationType(VISU::PresentationType theType) 
{ 
  if(myPresentType == theType)
    return;

  VISU::TSetModified aModified(this);
  if (theType == VISU::SHRINK) {
    myIsShrank = true;
  }
  myPresentType = theType;
  myParamsTime.Modified();
}


void
VISU::Mesh_i
::SetQuadratic2DPresentationType(VISU::Quadratic2DPresentationType theType)
{
  if(my2DQuadPrsType == theType)
    return;
  
  VISU::TSetModified aModified(this);
  my2DQuadPrsType = theType;
  myParamsTime.Modified();
}


//----------------------------------------------------------------------------
VISU::PresentationType 
VISU::Mesh_i
::GetPresentationType() 
{ 
  return myPresentType;
}


//----------------------------------------------------------------------------
VISU::Quadratic2DPresentationType
VISU::Mesh_i
::GetQuadratic2DPresentationType()
{
  return my2DQuadPrsType;
}




//----------------------------------------------------------------------------
VISU::Entity 
VISU::Mesh_i
::GetEntity() const
{ 
  return VISU::Entity(myEntity);
}


//----------------------------------------------------------------------------
const std::string&
VISU::Mesh_i
::GetSubMeshName() const
{ 
  return mySubMeshName;
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::Mesh_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  myEntity = VISU::Storable::FindValue(theMap,"myEntity").toInt();//jfa IPAL9284
  mySubMeshName = (const char*)VISU::Storable::FindValue(theMap,"mySubMeshName").toLatin1();

  myPresentType = VISU::PresentationType(VISU::Storable::FindValue(theMap,"myPresentType").toInt());

  my2DQuadPrsType = VISU::Quadratic2DPresentationType(VISU::Storable::FindValue(theMap,"my2DQuadPrsType").toInt());

  myIsShrank = (VISU::Storable::FindValue(theMap,"myIsShrank", "0").toInt() == 1)? true: false;

  myCellColor.R = VISU::Storable::FindValue(theMap,"myCellColor.R").toDouble();
  myCellColor.G = VISU::Storable::FindValue(theMap,"myCellColor.G").toDouble();
  myCellColor.B = VISU::Storable::FindValue(theMap,"myCellColor.B").toDouble();

  myNodeColor.R = VISU::Storable::FindValue(theMap,"myNodeColor.R").toDouble();
  myNodeColor.G = VISU::Storable::FindValue(theMap,"myNodeColor.G").toDouble();
  myNodeColor.B = VISU::Storable::FindValue(theMap,"myNodeColor.B").toDouble();

  myLinkColor.R = VISU::Storable::FindValue(theMap,"myLinkColor.R").toDouble();
  myLinkColor.G = VISU::Storable::FindValue(theMap,"myLinkColor.G").toDouble();
  myLinkColor.B = VISU::Storable::FindValue(theMap,"myLinkColor.B").toDouble();

  return Build(true);
}


//----------------------------------------------------------------------------
VISU::Storable*
VISU::Mesh_i
::StorableEngine(SALOMEDS::SObject_ptr theSObject,
		 const Storable::TRestoringMap& theMap,
		 const std::string& thePrefix,
		 CORBA::Boolean theIsMultiFile)
{
  Mesh_i* aMesh = new Mesh_i();
  return aMesh->Restore(theSObject, theMap);
}


//----------------------------------------------------------------------------
void 
VISU::Mesh_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myEntity", myEntity );
  Storable::DataToStream( theStr, "mySubMeshName", mySubMeshName.c_str() );

  Storable::DataToStream( theStr, "myPresentType", int(myPresentType) );

  Storable::DataToStream( theStr, "my2DQuadPrsType", int(my2DQuadPrsType) );

  Storable::DataToStream( theStr, "myIsShrank",  (myIsShrank? "1":"0"));

  Storable::DataToStream( theStr, "myCellColor.R", myCellColor.R );
  Storable::DataToStream( theStr, "myCellColor.G", myCellColor.G );
  Storable::DataToStream( theStr, "myCellColor.B", myCellColor.B );

  Storable::DataToStream( theStr, "myNodeColor.R", myNodeColor.R );
  Storable::DataToStream( theStr, "myNodeColor.G", myNodeColor.G );
  Storable::DataToStream( theStr, "myNodeColor.B", myNodeColor.B );

  Storable::DataToStream( theStr, "myLinkColor.R", myLinkColor.R );
  Storable::DataToStream( theStr, "myLinkColor.G", myLinkColor.G );
  Storable::DataToStream( theStr, "myLinkColor.B", myLinkColor.B );
}


//----------------------------------------------------------------------------
VISU_Actor* 
VISU::Mesh_i
::CreateActor()
{
  VISU_MeshAct* anActor = VISU_MeshAct::New();
  try{
    VISU::Prs3d_i::CreateActor(anActor);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    //bool toShrink  = aResourceMgr->booleanValue("VISU", "mesh_shrink", false);
    //if (toShrink) anActor->SetShrink();
    if (anActor->IsShrunkable()) {
      if (myIsShrank)
	anActor->SetShrink();
      else
	anActor->UnShrink();
    }
    anActor->SetFeatureEdgesAngle( aResourceMgr->doubleValue("VISU", "feature_edges_angle", 0.0) );
    anActor->SetFeatureEdgesFlags( aResourceMgr->booleanValue("VISU", "show_feature_edges", false),
				   aResourceMgr->booleanValue("VISU", "show_boundary_edges", false),
				   aResourceMgr->booleanValue("VISU", "show_manifold_edges", false),
				   aResourceMgr->booleanValue("VISU", "show_non_manifold_edges", false) );
    anActor->SetFeatureEdgesColoring( aResourceMgr->booleanValue("VISU", "feature_edges_coloring", false) );

    if (myType == VISU::TGROUP )
    {
      anActor->SetIsDisplayNameActor( true );
      anActor->SetNameActorText( GetSubMeshName().c_str() );
    }

    UpdateActor(anActor);
  }catch (...) {
    anActor->Delete();
    throw ;
  }
  return anActor;
}


//----------------------------------------------------------------------------
void
VISU::Mesh_i
::UpdateActor(VISU_ActorBase* theActor) 
{
  if(VISU_MeshAct* anActor = dynamic_cast<VISU_MeshAct*>(theActor)){
    if(MYDEBUG) MESSAGE("Mesh_i::UpdateActor");
    VISU::Prs3d_i::UpdateActor(anActor);
    anActor->SetRepresentation(myPresentType);
    if (anActor->IsShrunkable()) {
      if (myIsShrank)
	anActor->SetShrink();
      else
	anActor->UnShrink();
    }
    if(my2DQuadPrsType == VISU::LINES)
      anActor->SetQuadratic2DRepresentation(VISU_Actor::eLines);
    else{
      anActor->SetQuadratic2DRepresentation(VISU_Actor::eArcs);
    }
    anActor->GetSurfaceProperty()->SetColor(myCellColor.R, myCellColor.G, myCellColor.B);
    anActor->GetEdgeProperty()->SetColor(myLinkColor.R, myLinkColor.G, myLinkColor.B);
    anActor->GetNodeProperty()->SetColor(myNodeColor.R, myNodeColor.G, myNodeColor.B);
  }
}

//----------------------------------------------------------------------------


void VISU::Mesh_i::SetShrink(CORBA::Boolean toShrink)
{
  if(toShrink == myIsShrank)
    return;
  VISU::TSetModified aModified(this);
  myIsShrank = toShrink;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Boolean VISU::Mesh_i::IsShrank()
{
  return myIsShrank;
}


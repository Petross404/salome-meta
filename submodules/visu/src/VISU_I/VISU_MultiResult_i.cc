// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MultiResult_i.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#ifdef ENABLE_MULTIPR

#include "VISU_MultiResult_i.hh"
#include "VISU_ResultUtils.hh"

#include "VISU_Convertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "MULTIPR_Obj.hxx"
#include "MULTIPR_API.hxx"
#include "MULTIPR_Exceptions.hxx"

#include "SALOMEDS_Tool.hxx"
#include "HDFascii.hxx"

#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

#include <QStringList>

#include <sstream>

#ifdef _DEBUG_
static int MYDEBUG = 0;
static int MYTIMEDEBUG = 0;
#else
static int MYDEBUG = 0;
static int MYTIMEDEBUG = 0;
#endif


namespace VISU
{
  //---------------------------------------------------------------
  class TSubString: public std::string
  {
  public:
    typedef std::string::size_type size_type;

    TSubString(std::string& theSource, 
	       const std::string& theTarget):
      mySource(theSource),
      myPosition(theSource.find(theTarget)),
      mySize(theTarget.length())
    {}

    TSubString&
    operator = (const std::string& theTarget)
    {
      if(myPosition != std::string::npos)
	mySource.replace(myPosition, mySize, theTarget);
      return *this;
    }

  private:
    std::string& mySource;
    size_type myPosition;
    size_type mySize;
  };


  //---------------------------------------------------------------
  inline
  std::ostream& 
  operator<<(std::ostream& theStream,
	     const MultiResult_i::TPartInfo& thePartInfo)
  {
    theStream<<(thePartInfo.myMeshName)<<" ";
    theStream<<(thePartInfo.myPartID)<<" ";
    theStream<<(thePartInfo.myName)<<" ";
    theStream<<(thePartInfo.myPath)<<" ";
    theStream<<(thePartInfo.myFileName);
    return theStream;
  }


  //---------------------------------------------------------------
  inline
  std::istream& 
  operator>>(std::istream& theStream,
	     MultiResult_i::TPartInfo& thePartInfo)
  {
    theStream>>(thePartInfo.myMeshName);
    theStream>>(thePartInfo.myPartID);
    theStream>>(thePartInfo.myName);
    theStream>>(thePartInfo.myPath);
    theStream>>(thePartInfo.myFileName);
    return theStream;
  }


  //---------------------------------------------------------------
  inline
  MultiResult_i::TPartInfo
  GetPartInfo(const std::string theInfoString)
  {
    MultiResult_i::TPartInfo aPartInfo;
    std::istringstream anOutputStream(theInfoString.c_str());
    anOutputStream>>aPartInfo;
    return aPartInfo;
  }


  //---------------------------------------------------------------
  inline
  MultiResult_i::TPartInfo
  GetPartInfo(multipr::Obj& theMultiprObj,
	      const MultiResult_i::TPartName& thePartName)
  {
    return GetPartInfo(theMultiprObj.getPartInfo(thePartName.c_str()));
  }


  //---------------------------------------------------------------
  inline
  std::string
  ExtractMainPart(const std::string& thePartName)
  {
    size_t aPos = thePartName.rfind('_');
    if(aPos == std::string::npos)
      return thePartName;

    std::string aSuffix = thePartName.substr(aPos);
    if(aSuffix == "_MED" || aSuffix == "_LOW")
      return thePartName.substr(0, aPos);

    return thePartName;
  }


  //---------------------------------------------------------------
  inline
  bool
  IsSubString(const std::string& theSource,
	      const std::string& theSubString)
  {
    return theSource.rfind(theSubString) != std::string::npos;
  }


  //---------------------------------------------------------------
  inline
  bool
  IsMediumResolution(const std::string& thePartName)
  {
    return IsSubString(thePartName, "_MED");
  }


  //---------------------------------------------------------------
  inline
  bool
  IsLowResolution(const std::string& thePartName)
  {
    return IsSubString(thePartName, "_LOW");
  }


  //---------------------------------------------------------------
  inline
  bool
  IsFullResolution(const std::string& thePartName)
  {
    return !IsMediumResolution(thePartName)  && !IsLowResolution(thePartName);
  }


  //---------------------------------------------------------------
  inline
  MultiResult_i::TResolutions
  GetResolutions(const MultiResult_i::TMainPart2SubPartNames& theMainPart2SubPartNames,
		 const std::string& thePartName)
  {
    MultiResult_i::TResolutions aResolutions;
    aResolutions.insert(VISU::Result::FULL);

    MultiResult_i::TPartName aMainPart = ExtractMainPart(thePartName);
    MultiResult_i::TMainPart2SubPartNames::const_iterator anIter = theMainPart2SubPartNames.find(aMainPart);
    if(anIter != theMainPart2SubPartNames.end()){
      const MultiResult_i::TSubPartNames& aSubPartNames = anIter->second;

      if(aSubPartNames.find(aMainPart + "_LOW") != aSubPartNames.end())
	aResolutions.insert(VISU::Result::LOW);

      if(aSubPartNames.find(aMainPart + "_MED") != aSubPartNames.end())
	aResolutions.insert(VISU::Result::MEDIUM);
    }
    return aResolutions;
  }


  std::string
  Resolutions2String(const MultiResult_i::TResolutions& theResolutions)
  {
    std::string aResult;
    MultiResult_i::TResolutions::const_iterator anIter = theResolutions.begin();
    for(; anIter != theResolutions.end(); anIter++){
      VISU::Result::Resolution aResolution = *anIter;
      if(aResolution == VISU::Result::FULL)
	aResult += "F";
      if(aResolution == VISU::Result::LOW)
	aResult += "L";
      if(aResolution == VISU::Result::MEDIUM)
	aResult += "M";
    }
    return aResult;
  }


  //---------------------------------------------------------------
  inline
  VISU::Result::Resolution
  GetResolution(const MultiResult_i::TMainPart2SubPartNames& theMainPart2SubPartNames,
		  const std::string& thePartName)
  {
    MultiResult_i::TResolutions aResolutions = GetResolutions(theMainPart2SubPartNames, thePartName);

    if(aResolutions.find(VISU::Result::LOW) != aResolutions.end())
      return VISU::Result::LOW;

    if(aResolutions.find(VISU::Result::MEDIUM) != aResolutions.end())
      return VISU::Result::MEDIUM;

    return VISU::Result::FULL;
  }


  //---------------------------------------------------------------
  inline
  std::string
  GetIconName(const MultiResult_i::TMainPart2SubPartNames& theMainPart2SubPartNames,
	      const std::string& thePartName)
  {
    VISU::Result::Resolution aResolution = GetResolution(theMainPart2SubPartNames,
							 thePartName);
    if(aResolution == VISU::Result::LOW)
      return "ICON_MULTIPR_VIEW_LOW";

    if(aResolution == VISU::Result::MEDIUM)
      return "ICON_MULTIPR_VIEW_MEDIUM";

    return "ICON_MULTIPR_VIEW_FULL";
  }


  //----------------------------------------------------------------------------
  void 
  BuildParts(Result_i* theResult,
	     Result_i::PInput theInput,
	     multipr::Obj* theMultiprObj,
	     MultiResult_i::TPartInfos* thePartInfos,
	     MultiResult_i::TPartName2FileName* thePartName2FileName,
	     MultiResult_i::TPartName2Resolution* thePartName2Resolution,
	     MultiResult_i::TMainPart2SubPartNames* theMainPart2SubPartNames,
	     CORBA::Boolean* theIsDone,
	     CORBA::Boolean theIsBuild,
	     _PTR(Study) theStudy,
	     bool thePublishInStudy)
  {
    if(!theIsBuild || *theIsDone)
      return;
    
    INITMSG(MYDEBUG, "BuildParts\n");
    TTimerLog aTimerLog(MYTIMEDEBUG, "BuildParts");
    TResultManager aResultManager(theResult);
    TTransactionManager aTransactionManager(theStudy);
    
    try {
      multipr::Obj& aMultiprObj = *theMultiprObj;
      const VISU::TMeshMap& aMeshMap = theInput->GetMeshMap();
      VISU::TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
      const VISU::PMesh& aMesh = aMeshMapIter->second;
      
      MultiResult_i::TPartNames aPartNames = aMultiprObj.getParts();
      BEGMSG(MYDEBUG, "aPartNames.size() = "<<aPartNames.size()<<"\n");
      
      if(thePublishInStudy){
	QString aComment = "Sub-parts: #";
	aComment += QString::number(aPartNames.size());
	
	CreateAttributes(theStudy,
			 aMesh->myPartsEntry,
			 NO_ICON,
			 NO_IOR,
			 "Parts",
			 NO_PERFSITENT_REF,
			 aComment.toLatin1().data(),
			 false);
      }
      
      MultiResult_i::TPartInfos& aPartInfos = *thePartInfos;
      MultiResult_i::TPartName2FileName& aPartName2FileName = *thePartName2FileName;
      MultiResult_i::TPartName2Resolution& aPartName2Resolution = *thePartName2Resolution;
      
      MultiResult_i::TMainPart2SubPartNames& aMainPart2SubPartNames = *theMainPart2SubPartNames;
      
      for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
	const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
	MultiResult_i::TPartName aMainPart = ExtractMainPart(aPartName);
	aMainPart2SubPartNames[aMainPart].insert(aPartName);
	BEGMSG(MYDEBUG, "aMainPart2SubPartNames['"<<aMainPart<<"'].insert('"<<aPartName<<"')\n");
      }
      
      for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
	const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
	MultiResult_i::TPartInfo aPartInfo = GetPartInfo(aMultiprObj, aPartName);

	QFileInfo aFileInfo(aPartInfo.myFileName.c_str());
	aPartInfos[aFileInfo.fileName().toLatin1().data()] = aPartInfo;
	
	aPartName2FileName[aPartInfo.myName] = aPartInfo.myFileName;
	BEGMSG(MYDEBUG, "aPartName2FileName['"<<aPartInfo.myName<<"'] = '"<<aPartInfo.myFileName<<"'\n");
	
	if(!thePublishInStudy)
	  continue;
	
	QString aComment = "";
	MultiResult_i::TResolutions aResoltutions = GetResolutions(aMainPart2SubPartNames, aPartInfo.myName);
	std::string aResoltutionsString = Resolutions2String(aResoltutions);
	if ( IsFullResolution(aPartInfo.myName) ) {
	  std::string anIconName = GetIconName(aMainPart2SubPartNames, aPartInfo.myName);
	  VISU::Result::Resolution aResolution = GetResolution(aMainPart2SubPartNames, aPartInfo.myName);
	  aComment.sprintf("myComment=PART;myMeshName=%s;myName=%s;myResolutions=%s;myState=%d", 
			   aPartInfo.myMeshName.c_str(), aPartInfo.myName.c_str(), aResoltutionsString.c_str(), aResolution); 
	  CreateAttributes(theStudy,
			   aMesh->myPartsEntry,
			   anIconName,
			   NO_IOR,
			   aPartInfo.myName,
			   NO_PERFSITENT_REF,
			   aComment.toLatin1().data(),
			   true);
	  aPartName2Resolution[aPartInfo.myName] = aResolution;
	  BEGMSG(MYDEBUG, "aPartName2Resolution['"<<aPartInfo.myName<<"'] = '"<<aResoltutionsString<<"'\n");
	}
      }
      
      *theIsDone = true;
    }catch(std::exception& exc){
      INFOS("Follow exception was occured :\n"<<exc.what());
    }catch(...){
      INFOS("Unknown exception was occured!");
    }
    
    ProcessVoidEvent(new TUpdateObjBrowser(theStudy->StudyId(), theIsDone));
  }


  //---------------------------------------------------------------
  struct TBuildPartsArgs
  {
    Result_i* myResult;
    Result_i::PInput myInput;
    multipr::Obj* myMultiprObj;
    MultiResult_i::TPartInfos* myPartInfos;
    MultiResult_i::TPartName2FileName* myPartName2FileName;
    MultiResult_i::TPartName2Resolution* myPartName2Resolution;
    MultiResult_i::TMainPart2SubPartNames* myMainPart2SubPartNames;
    CORBA::Boolean* myIsDone;
    CORBA::Boolean myIsBuild;
    _PTR(Study) myStudy;
    bool myPublishInStudy;

    TBuildPartsArgs(Result_i* theResult,
		    Result_i::PInput theInput,
		    multipr::Obj* theMultiprObj,
		    MultiResult_i::TPartInfos* thePartInfos,
		    MultiResult_i::TPartName2FileName* thePartName2FileName,
		    MultiResult_i::TPartName2Resolution* thePartName2Resolution,
		    MultiResult_i::TMainPart2SubPartNames* theMainPart2SubPartNames,
		    CORBA::Boolean* theIsDone,
		    CORBA::Boolean theIsBuild,
		    _PTR(Study) theStudy,
		    bool thePublishInStudy):
      myResult(theResult),
      myInput(theInput),
      myMultiprObj(theMultiprObj),
      myPartInfos(thePartInfos),
      myPartName2FileName(thePartName2FileName),
      myPartName2Resolution(thePartName2Resolution),
      myMainPart2SubPartNames(theMainPart2SubPartNames),
      myIsDone(theIsDone),
      myIsBuild(theIsBuild),
      myStudy(theStudy),
      myPublishInStudy(thePublishInStudy)
    {}
  };


  //----------------------------------------------------------------------------
  void 
  BuildParts(const TBuildPartsArgs& theArgs)
  {
    BuildParts(theArgs.myResult,
	       theArgs.myInput,
	       theArgs.myMultiprObj,
	       theArgs.myPartInfos,
	       theArgs.myPartName2FileName,
	       theArgs.myPartName2Resolution,
	       theArgs.myMainPart2SubPartNames,
	       theArgs.myIsDone,
	       theArgs.myIsBuild,
	       theArgs.myStudy,
	       theArgs.myPublishInStudy);
  }


  //---------------------------------------------------------------
}


//---------------------------------------------------------------
VISU::MultiResult_i
::MultiResult_i(SALOMEDS::Study_ptr theStudy,
	        const ESourceId& theSourceId,
	        const ECreationId& theCreationId,
	        CORBA::Boolean theIsBuildImmediately,
	        CORBA::Boolean theIsBuildFields,
	        CORBA::Boolean theIsBuildMinMax,
	        CORBA::Boolean theIsBuildGroups):
  Result_i(theStudy,
	   theSourceId,
	   theCreationId,
	   theIsBuildImmediately,
	   theIsBuildFields,
	   theIsBuildMinMax,
	   theIsBuildGroups)
{}


//---------------------------------------------------------------
VISU::MultiResult_i
::MultiResult_i()
{}


//---------------------------------------------------------------
size_t
VISU::MultiResult_i
::IsPossible()
{
  return TSuperClass::IsPossible();
}


//---------------------------------------------------------------
VISU::Storable*
VISU::MultiResult_i
::Build(SALOMEDS::SObject_ptr theSObject,
	CORBA::Boolean theIsAtOnce)
{
  if(!TSuperClass::Build(theSObject, theIsAtOnce))
    return NULL;

  if(IsDone())
    return this;

  if(theIsAtOnce){
    BuildParts(this,
	       GetInput(),
	       &myMultiprObj,
	       &myPartInfos,
	       &myPartName2FileName,
	       &myPartName2Resolution,
	       &myMainPart2SubPartNames,
	       &myIsPartsDone,
	       myIsBuildParts,
	       myStudy,
	       true);
  }

  return this;
}


//---------------------------------------------------------------
void
VISU::MultiResult_i
::BuildDataTree(const std::string& theResultEntry)
{
  BuildEntities(this,
		GetInput(),
		&myIsEntitiesDone,
		theResultEntry,
		false,
		myIsBuildGroups,
		myIsBuildFields,
		myIsBuildParts,
		myStudy);
  {
    TBuildPartsArgs anArgs(this,
			   GetInput(),
			   &myMultiprObj,
			   &myPartInfos,
			   &myPartName2FileName,
			   &myPartName2Resolution,
			   &myMainPart2SubPartNames,
			   &myIsPartsDone,
			   myIsBuildParts,
			   myStudy,
			   true);

    boost::thread aThread(boost::bind(&BuildParts, anArgs));
  }
  {
    boost::thread aThread(boost::bind(&BuildGroups,
				      this,
				      GetInput(),
				      &myIsGroupsDone,
				      myIsBuildGroups,
				      false,
				      myStudy));
  }
  {
    boost::thread aThread(boost::bind(&BuildFieldDataTree,
				      this,
				      GetInput(),
				      &myIsFieldsDone,
				      myIsBuildFields,
				      &myIsMinMaxDone,
				      myIsBuildMinMax,
				      myStudy));
  }
}


//---------------------------------------------------------------
VISU::Storable*
VISU::MultiResult_i
::Create(const char* theFileName)
{
  QFileInfo aFileInfo(theFileName);
  QString aTargetFileName = aFileInfo.filePath();
  if(aTargetFileName.endsWith("_maitre.med")){
    try {
      myMultiprObj.create(theFileName);
      if ( myMultiprObj.isValidDistributedMEDFile() ) {
	aTargetFileName = myMultiprObj.getSequentialMEDFilename().c_str();
	SetInitFileName(aFileInfo.filePath().toLatin1().data());
	SetName(VISU::GenerateName(aFileInfo.fileName().toLatin1().data()).toLatin1().data(), false);
	myIsBuildParts = true;
      }
    }catch(std::exception& exc){
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
    }catch(multipr::RuntimeException& exc){
      std::ostringstream aStream;
      exc.dump(aStream);
      aStream<<ends;
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<aStream.str());
    }catch(...){
      MSG(MYDEBUG,"Unknown exception !!!");
    }
  }

  return TSuperClass::Create(aTargetFileName.toLatin1().data());
}


//---------------------------------------------------------------
bool
VISU::MultiResult_i
::Save(SALOMEDS::SComponent_ptr theComponent,
       const std::string& theURL,
       bool theIsMultiFile,
       bool theIsASCII,
       TFileNames& theFileNames,
       TFileNames& theFiles)
{
  bool anIsDone = Result_i::Save(theComponent, 
				 theURL, 
				 theIsMultiFile,
				 theIsASCII, 
				 theFileNames, 
				 theFiles);
  if(!anIsDone)
    return false;

  if(!myMultiprObj.isValidDistributedMEDFile())
    return true;

  INITMSG(MYDEBUG, "MultiResult_i::Save - this = "<<this<<"\n");
  INITMSGA(MYDEBUG, 0, "theIsMultiFile = "<<theIsMultiFile<<"; theIsASCII = "<<theIsASCII<<"\n");

  // To generate an unique prefix for the set of multi sub files
  std::string aPrefix;
  if (theIsMultiFile) {
    CORBA::String_var anURL = GetStudyDocument()->URL();
    aPrefix = SALOMEDS_Tool::GetNameFromPath(anURL.in());
  }

  std::string aBase, aSuffix;
  SplitName(GetFileName(), aBase, aSuffix);
  BEGMSG(MYDEBUG, "aBase = '"<<aBase<<"'; aSuffix = '"<<aSuffix<<"'\n");

  aPrefix = aPrefix + "_" + aBase;
  VISU::TSubString(aPrefix, ".med") = "";

  BEGMSG(MYDEBUG, "aPrefix = '"<<aPrefix<<"'\n");

  // To get a common prefix used in the multi file
  QFileInfo aFileInfo(myMultiprObj.getSequentialMEDFilename().c_str());
  std::string aFilePrefix = aFileInfo.completeBaseName().toLatin1().data();

  MultiResult_i::TPartNames aPartNames = myMultiprObj.getParts();
  for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
    const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
    MultiResult_i::TPartInfo aPartInfo = GetPartInfo(myMultiprObj, aPartName);

    QFileInfo aFileInfo(aPartInfo.myFileName.c_str());
    std::string aFile = aFileInfo.absoluteFilePath().toLatin1().data();

    std::string aFileName = aFileInfo.fileName().toLatin1().data();
    VISU::TSubString(aFileName, aFilePrefix) = aPrefix;
    VISU::TSubString(aFileName, aSuffix) = "";
    aFileName = aFileName + aSuffix;
    INITMSG(MYDEBUG, "aFileName = '"<<aFileName<<"'\n");

    if(theIsMultiFile || theIsASCII){
      std::string aPathToCopy(theURL + aFileName);
      BEGMSG(MYDEBUG, "aPathToCopy = '"<<aPathToCopy<<"'\n");
      
      if(!VISU::CopyFile(aFile, aPathToCopy))
	return false;

      if(theIsASCII)
	HDFascii::ConvertFromHDFToASCII(const_cast<char*>(aPathToCopy.c_str()), true);
    }

    theFileNames.push_back(aFileName);
    theFiles.push_back(aFile);
  }
  
  return true;
}


//---------------------------------------------------------------
CORBA::Boolean 
VISU::MultiResult_i
::CanCopy(SALOMEDS::SObject_ptr theObject) 
{
  if(!myIsPartsDone)
    return Result_i::CanCopy(theObject);

  return false;
}


//---------------------------------------------------------------
void 
VISU::MultiResult_i
::ToStream(std::ostringstream& theStr)
{
  INITMSG(MYDEBUG, "MultiResult_i::ToStream - this = "<<this<<"\n");

  TSuperClass::ToStream(theStr);

  Storable::DataToStream(theStr,"myIsBuildParts", myIsPartsDone);
  if(!myIsPartsDone)
    return;

  {
    std::ostringstream aPartNames, aResolutions;
    TPartName2Resolution::const_iterator anIter = myPartName2Resolution.begin();
    for ( ; anIter != myPartName2Resolution.end() ; anIter++) {
      const TPartName& aPartName = anIter->first;
      aPartNames<<aPartName<<"|";
      const VISU::Result::Resolution& aResolution = anIter->second;
      aResolutions<<aResolution<<"|";
    }
    
    Storable::DataToStream(theStr, "myPartNames",  aPartNames.str().c_str());   
    Storable::DataToStream(theStr, "myResolutions", aResolutions.str().c_str());
  }

  {
    std::string aBase, aSuffix;
    VISU::SplitName(GetFileName(), aBase, aSuffix);
    INITMSG(MYDEBUG, "aBase = '"<<aBase<<"'; aSuffix = '"<<aSuffix<<"'\n");

    QFileInfo aFileInfo(myMultiprObj.getSequentialMEDFilename().c_str());
    std::string aFilePrefix = aFileInfo.completeBaseName().toLatin1().data();
    BEGMSG(MYDEBUG, "aFilePrefix = '"<<aFilePrefix<<"'\n");

    std::ostringstream aPartInfos;
    MultiResult_i::TPartNames aPartNames = myMultiprObj.getParts();
    for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
      const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
      MultiResult_i::TPartInfo aPartInfo = GetPartInfo(myMultiprObj, aPartName);

      QFileInfo aFileInfo(aPartInfo.myFileName.c_str());
      std::string aFileName = aFileInfo.fileName().toLatin1().data();
      VISU::TSubString(aFileName, aFilePrefix + "_") = "";      
      VISU::TSubString(aFileName, aSuffix) = "";
      aPartInfo.myFileName = aFileName + aSuffix;
      aPartInfos<<aPartInfo<<"|";

      INITMSG(MYDEBUG, "aFileName = '"<<aPartInfo.myFileName<<"'\n");
    }

    Storable::DataToStream(theStr, "myPartInfos", aPartInfos.str().c_str());
  }
}


//---------------------------------------------------------------
VISU::Storable*
VISU::MultiResult_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap,
	  const std::string& thePrefix,
	  CORBA::Boolean theIsMultiFile)
{
  INITMSG(MYDEBUG, "MultiResult_i::Restore - this = "<<this<<"\n");

  if(!TSuperClass::Restore(theSObject, theMap, thePrefix, theIsMultiFile))
    return NULL;

  myIsBuildParts = Storable::FindValue(theMap, "myIsBuildParts", "0").toInt();
  if(!myIsBuildParts)
    return this;
  
  QStringList aResolutions = VISU::Storable::FindValue(theMap, "myResolutions").split("|", QString::SkipEmptyParts);
  QStringList aPartNames = VISU::Storable::FindValue(theMap, "myPartNames").split("|", QString::SkipEmptyParts);
  for(size_t anId = 0, anEnd = aPartNames.size(); anId < anEnd; anId++){
    const QString& aPartName = aPartNames[anId];
    VISU::Result::Resolution aResolution = VISU::Result::Resolution(aResolutions[anId].toInt());
    myPartName2Resolution[aPartName.toLatin1().data()] = aResolution;
    INITMSG(MYDEBUG, "aPartName = '"<<aPartName.toLatin1().data()<<"' = "<<aResolution<<"\n");
  }
  
  std::string aBase, aSuffix;
  SplitName(GetFileName(), aBase, aSuffix);
  INITMSGA(MYDEBUG, 0, "aBase = '"<<aBase<<"'; aSuffix = '"<<aSuffix<<"'\n");

  std::string aSourceFileName = GetFileInfo().absoluteFilePath().toLatin1().data();
  BEGMSG(MYDEBUG, "aSourceFileName = '"<<aSourceFileName<<"'\n");    
  
  std::string aPrefix = aSourceFileName;
  VISU::TSubString(aPrefix, ".med") = "";
  VISU::TSubString(aPrefix, aSuffix) = "";
  BEGMSG(MYDEBUG, "aPrefix = '"<<aPrefix<<"'\n");    
  
  std::string aMultiFileName(aPrefix + "_maitre.med" + aSuffix);
  BEGMSG(MYDEBUG, "aMultiFileName = '"<<aMultiFileName<<"'\n");    

  {
    std::ofstream aMultiFileStream(aMultiFileName.c_str());
    aMultiFileStream<<"# MED file v2.3 - Master file created by VISU\n";
    aMultiFileStream<<"#\n";
    aMultiFileStream<<"# [SOURCE]="<<aSourceFileName<<"\n";
    aMultiFileStream<<"#\n";

    QStringList aPartInfos = VISU::Storable::FindValue(theMap, "myPartInfos").split("|", QString::SkipEmptyParts);
    aMultiFileStream<<aPartInfos.size()<<"\n";

    for(size_t anId = 0, anEnd = aPartInfos.size(); anId < anEnd; anId++){
      MultiResult_i::TPartInfo aPartInfo = GetPartInfo(aPartInfos[anId].toLatin1().data());
      aPartInfo.myFileName = aPrefix + "_" + aPartInfo.myFileName;
      INITMSG(MYDEBUG, "aPartInfo.myFileName = '"<<aPartInfo.myFileName<<"'\n");    
      aMultiFileStream<<aPartInfo<<"\n";
    }
  }

  {
    myMultiprObj.create(aMultiFileName.c_str());
    BuildParts(this,
	       GetInput(),
	       &myMultiprObj,
	       &myPartInfos,
	       &myPartName2FileName,
	       &myPartName2Resolution,
	       &myMainPart2SubPartNames,
	       &myIsPartsDone,
	       myIsBuildParts,
	       myStudy,
	       false);
  }

  return this;
}


//---------------------------------------------------------------
CORBA::Boolean 
VISU::MultiResult_i
::IsDone() 
{
  return TSuperClass::IsDone() && 
    (myIsBuildParts? myIsPartsDone: true);
}


//---------------------------------------------------------------
VISU::Result::EntityNames* 
VISU::MultiResult_i
::GetPartNames(const char* theMeshName)
{
  VISU::Result::EntityNames_var aResult = new VISU::Result::EntityNames();
  if(!myIsBuildParts)
    return aResult._retn();
    
  MultiResult_i::TPartNames aMeshParts;
  MultiResult_i::TPartNames aPartNames = myMultiprObj.getParts();
  for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
    const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
    if(!IsFullResolution(aPartName))
      continue;

    MultiResult_i::TPartInfo aPartInfo = GetPartInfo(myMultiprObj, aPartName);
    if(true || aPartInfo.myMeshName == theMeshName) // To ignore theMeshName input parameter
      aMeshParts.push_back(aPartName);
  }

  if(aMeshParts.empty())
    return aResult._retn();

  aResult->length(aMeshParts.size());
  for (size_t aPartID = 0 ; aPartID < aMeshParts.size() ; aPartID++) {
    const MultiResult_i::TPartName& aPartName = aMeshParts[aPartID];
    aResult[aPartID] = aPartName.c_str();
  }

  return aResult._retn();
}

VISU::Result::Resolutions* 
VISU::MultiResult_i
::GetResolutions(const char* theMeshName, 
		 const char* thePartName)
{
  VISU::Result::Resolutions_var aResult = new VISU::Result::Resolutions();
  if(!myIsBuildParts)
    return aResult._retn();

  MultiResult_i::TPartNames aPartNames = myMultiprObj.getParts();
  for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
    const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
    MultiResult_i::TPartName aMainPart = ExtractMainPart(thePartName);
    if(aMainPart != thePartName)
      continue;

    MultiResult_i::TPartInfo aPartInfo = GetPartInfo(myMultiprObj, aPartName);
    if(false || aPartInfo.myMeshName != theMeshName) // To ignore theMeshName input parameter
      continue;

    MultiResult_i::TResolutions aResolutions = VISU::GetResolutions(myMainPart2SubPartNames, thePartName);
    if(aResolutions.empty())
      return aResult._retn();

    aResult->length(aResolutions.size());
    MultiResult_i::TResolutions::const_iterator anIter = aResolutions.end();
    for(size_t anId = 0; anIter != aResolutions.end(); anIter++, anId++){
      const VISU::Result::Resolution& aResolution = *anIter;
      aResult[anId] = aResolution;
    }

    break;
  }

  return aResult._retn();
}

VISU::Result::Resolution
VISU::MultiResult_i
::GetResolution(const char* theMeshName, 
		const char* thePartName)
{
  TPartName2Resolution::iterator anIter = myPartName2Resolution.find(thePartName);
  if(anIter == myPartName2Resolution.end())
    return VISU::Result::HIDDEN;

  const VISU::Result::Resolution& aResolution = anIter->second;
  return aResolution;
}

void 
VISU::MultiResult_i
::SetResolution(const char* theMeshName, 
		const char* thePartName, 
		VISU::Result::Resolution theResolution)
{
  if(!IsFullResolution(thePartName))
    return;

  TPartName2Resolution::iterator anIter = myPartName2Resolution.find(thePartName);
  if(anIter == myPartName2Resolution.end())
    return;

  VISU::Result::Resolution& aResolution = anIter->second;
  if(aResolution == theResolution)
    return;

  if(theResolution != VISU::Result::HIDDEN){
    MultiResult_i::TResolutions aResolutions = VISU::GetResolutions(myMainPart2SubPartNames, thePartName);
    MultiResult_i::TResolutions::iterator anIter = aResolutions.find(theResolution);
    if(anIter == aResolutions.end())
      return;
  }

  VISU::Storable::TRestoringMap aRestoringMap;
  aRestoringMap["myComment"] = "PART";
  //aRestoringMap["myMeshName"] = theMeshName; // To ignore theMeshName input parameter
  aRestoringMap["myName"] = thePartName;

  const VISU::TMeshMap& aMeshMap = Result_i::GetInput()->GetMeshMap();
  //VISU::TMeshMap::const_iterator aMeshIter = aMeshMap.find(theMeshName); // To ignore theMeshName input parameter
  VISU::TMeshMap::const_iterator aMeshIter = aMeshMap.begin();
  if(aMeshIter == aMeshMap.end())
    return;

  std::string aFatherEntry;
  const VISU::PMesh& aMesh = aMeshIter->second;
  if(aMesh->myPartsEntry != "")
    aFatherEntry = Storable::FindEntry(GetStudyDocument(),
				       aMesh->myPartsEntry,
				       aRestoringMap);
  else
    aFatherEntry = Result_i::GetEntry(aRestoringMap);
  
  if ( aFatherEntry == "" )
    return;

  std::string anIconName = "ICON_MULTIPR_VIEW_HIDE";
  if(theResolution == VISU::Result::FULL)
    anIconName = "ICON_MULTIPR_VIEW_FULL";
  else if(theResolution == VISU::Result::MEDIUM)
    anIconName = "ICON_MULTIPR_VIEW_MEDIUM";
  else if(theResolution == VISU::Result::LOW)
    anIconName = "ICON_MULTIPR_VIEW_LOW";

  _PTR(Study) aStudy = GetStudy();
  _PTR(SObject) aSObject = aStudy->FindObjectID(aFatherEntry);
  aRestoringMap = Storable::GetStorableMap(aSObject);

  std::ostringstream anOutputStream;
  anOutputStream<<"myComment=PART;";
  anOutputStream<<"myName="<<thePartName<<";";
  anOutputStream<<"myMeshName="<<theMeshName<<";";
  anOutputStream<<"myResolutions="<<aRestoringMap["myResolutions"].toLatin1().data()<<";";
  anOutputStream<<"myState="<<theResolution;
  anOutputStream<<ends;

  CreateAttributes(aStudy,
		   aFatherEntry,
		   anIconName,
		   NO_IOR,
		   NO_NAME,
		   NO_PERFSITENT_REF,
		   anOutputStream.str(),
		   false);

  aResolution = theResolution;
}


//---------------------------------------------------------------
VISU::MultiResult_i
::~MultiResult_i()
{
  INITMSG(MYDEBUG, "MultiResult_i::~MultiResult_i - this = "<<this<<"\n");
  if(myIsBuildParts){
    TRepresentation2Input::iterator anIter = myRepresentation2Input.begin();
    for ( ; anIter != myRepresentation2Input.end() ; anIter++) {
      const PInput& anInput = anIter->second;
      std::string aFileName = anInput->GetName();
      INITMSG(MYDEBUG, "RemoveFile - aFileName = '"<<aFileName<<"'\n");
      VISU::RemoveFile(aFileName);
    }
    if (GetSourceId() == eRestoredFile) {
      INITMSG(MYDEBUG, "RemoveFile - myMultiprObj.getMEDFilename = '"<<myMultiprObj.getMEDFilename()<<"'\n");
      VISU::RemoveFile(myMultiprObj.getMEDFilename());
      MultiResult_i::TPartNames aPartNames = myMultiprObj.getParts();
      for (size_t aPartID = 0 ; aPartID < aPartNames.size() ; aPartID++) {
	const MultiResult_i::TPartName& aPartName = aPartNames[aPartID];
	MultiResult_i::TPartInfo aPartInfo = GetPartInfo(myMultiprObj, aPartName);
	INITMSG(MYDEBUG, "RemoveFile - aPartInfo.myFileName = '"<<aPartInfo.myFileName<<"'\n");
	VISU::RemoveFile(aPartInfo.myFileName);
      }
    }
  }
}


namespace VISU
{
  //---------------------------------------------------------------
  inline
  void
  UpdateRepresentationKey(const MultiResult_i::TPartName2FileName& thePartName2FileName,
			  MultiResult_i::TRepresentationKey& theRepresentationKey,
			  const std::string& thePartName)
  {
    MultiResult_i::TPartName2FileName::const_iterator anIterator = thePartName2FileName.find(thePartName);
    if (anIterator != thePartName2FileName.end()){
      const MultiResult_i::TFileName& aFileName = anIterator->second;
      theRepresentationKey.insert(aFileName);
    }
  }


  //---------------------------------------------------------------
  MultiResult_i::TRepresentationKey
  GetRepresentation(const MultiResult_i::TPartName2FileName& thePartName2FileName,
		    const MultiResult_i::TPartName2Resolution& thePartName2Resolution)
  {
    // name of selected parts
    MultiResult_i::TRepresentationKey aRepresentationKey;

    // for each part of the mesh
    MultiResult_i::TPartName2Resolution::const_iterator anIter = thePartName2Resolution.begin();
    for ( ; anIter != thePartName2Resolution.end() ; anIter++) {
      const MultiResult_i::TPartName& aPartName = anIter->first;
      if(IsFullResolution(aPartName)){
	const VISU::Result::Resolution& aResolution = anIter->second;
	if(aResolution == VISU::Result::FULL) 
	  UpdateRepresentationKey(thePartName2FileName, aRepresentationKey, aPartName);
	if(aResolution == VISU::Result::MEDIUM) 
	  UpdateRepresentationKey(thePartName2FileName, aRepresentationKey, aPartName + "_MED");
	else if(aResolution == VISU::Result::LOW) 
	  UpdateRepresentationKey(thePartName2FileName, aRepresentationKey, aPartName + "_LOW");
      }
    }
    return aRepresentationKey;
  }


  //---------------------------------------------------------------
  bool 
  UseInitialInput(const MultiResult_i::TPartName2FileName& thePartName2FileName,
		  const MultiResult_i::TPartName2Resolution& thePartName2Resolution)
  {
    bool aResult = true;
    MultiResult_i::TPartName2FileName::const_iterator anIter = thePartName2FileName.begin();
    for ( ; anIter != thePartName2FileName.end() ; anIter++) {
      const MultiResult_i::TPartName& aPartName = anIter->first;
      if(IsFullResolution(aPartName)){
	MultiResult_i::TPartName2Resolution::const_iterator anIter2 = thePartName2Resolution.find(aPartName);
	if(anIter2 != thePartName2Resolution.end()){
	  const VISU::Result::Resolution& aResolution = anIter2->second;
	  if(aResolution == VISU::Result::FULL)
	    continue;
	}
	aResult = false;
	break;
      }
    }
    return aResult;
  }
}


//---------------------------------------------------------------
VISU::Result_i::PInput
VISU::MultiResult_i
::GetInput(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber) 
{
  if(theEntity == NONE) // If client wants use initial behaviour
    return TSuperClass::GetInput();

  if(!myIsPartsDone) // If corresponding data is not ready yet
    return TSuperClass::GetInput();

  //if(UseInitialInput(myPartName2FileName, myPartName2Resolution)) 
  //  return TSuperClass::GetInput();

  MultiResult_i::TRepresentationKey aRepresentationKey = 
    GetRepresentation(myPartName2FileName, myPartName2Resolution);

  typedef std::vector<TFileName> TFileNames;
  TFileNames aFileNames(aRepresentationKey.begin(), aRepresentationKey.end());

  aRepresentationKey.insert( theMeshName );
  aRepresentationKey.insert( theFieldName );

  TRepresentation2Input::iterator anIter = myRepresentation2Input.find(aRepresentationKey);
  if(anIter == myRepresentation2Input.end()){
    INITMSG(MYDEBUG, "MultiResult_i::GetInput - this = "<<this<<"\n");
    std::string aFileName = SALOMEDS_Tool::GetTmpDir() + "multipr_merge.med";
    if(MYDEBUG){
      INITMSG(MYDEBUG, "aFileName = '"<<aFileName<<"':\n");
      TFileNames::const_iterator anIter = aFileNames.begin();
      for ( ; anIter != aFileNames.end() ; anIter++) {
	const MultiResult_i::TFileName& aFileName = *anIter;
	INITMSG(MYDEBUG, "'"<<aFileName<<"'\n");
      }
    }
    int aRes = false;
    std::string anErrorMessage("empty mesh");
    try {
      INITMSG(MYDEBUG, "theMeshName = '"<<theMeshName<<"'; theFieldName = '"<<theFieldName<<"'\n");
      aRes = multipr::merge(aFileNames, theMeshName.c_str(), theFieldName.c_str(), aFileName.c_str());
    }catch(std::exception& exc){
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
      anErrorMessage = exc.what();
    }catch(multipr::RuntimeException& exc){
      std::ostringstream aStream;
      exc.dump(aStream);
      aStream<<ends;
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<aStream.str());
      anErrorMessage = aStream.str();
    }catch(...){
      MSG(MYDEBUG,"Unknown exception !!!");
    }
    INITMSGA(MYDEBUG, 0, "aRes = "<<aRes<<"\n");
    if (aRes == 0) {
      VISU::RemoveFile(aFileName);
      throw std::runtime_error(anErrorMessage); 
    }
    PInput anInput(CreateConvertor(aFileName));
    anInput->BuildFields();
    if(myIsBuildMinMax)
      anInput->BuildMinMax();
    myRepresentation2Input[aRepresentationKey] = anInput;
    return anInput;
  }
  
  return anIter->second;
}


#endif // ENABLE_MULTIPR

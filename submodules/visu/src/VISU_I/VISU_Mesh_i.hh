// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_Mesh_i_HeaderFile
#define VISU_Mesh_i_HeaderFile

#include "VISU_Prs3d_i.hh"

class VISU_MeshPL;

namespace VISU
{
  class VISU_I_EXPORT Mesh_i : public virtual POA_VISU::Mesh,
		 public virtual Prs3d_i
  {
    static int myNbPresent;
    Mesh_i(const Mesh_i&);

  public:
    typedef Prs3d_i TSuperClass;
    typedef VISU::Mesh TInterface;

    Mesh_i();

    virtual
    ~Mesh_i();

    virtual
    void
    SameAs(const Prs3d_i* theOrigin);

    virtual
    void
    RemoveFromStudy();

    virtual
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TMESH;
    }

    virtual 
    void 
    SetCellColor(const SALOMEDS::Color& theColor);

    virtual 
    SALOMEDS::Color 
    GetCellColor();

    virtual 
    void 
    SetNodeColor(const SALOMEDS::Color& theColor);

    virtual 
    SALOMEDS::Color 
    GetNodeColor();

    virtual 
    void 
    SetLinkColor(const SALOMEDS::Color& theColor);

    virtual 
    SALOMEDS::Color 
    GetLinkColor();

    virtual 
    void
    SetPresentationType(VISU::PresentationType theType);

    virtual 
    void
    SetQuadratic2DPresentationType(VISU::Quadratic2DPresentationType theType);

    virtual 
    VISU::Quadratic2DPresentationType
    GetQuadratic2DPresentationType();

    virtual
    VISU::PresentationType 
    GetPresentationType();


    virtual void SetShrink(CORBA::Boolean toShrink);

    virtual CORBA::Boolean IsShrank();

    VISU_MeshPL* GetSpecificPL() const
    { 
      return myMeshPL;
    }

  protected:
    Storable* 
    Build(int theRestoring);

    VISU_MeshPL* myMeshPL;

    int myEntity;//jfa IPAL9284: TEntity myEntity;
    std::string mySubMeshName;
    VISU::VISUType myType;

    VISU::PresentationType              myPresentType;
    VISU::Quadratic2DPresentationType   my2DQuadPrsType;
    SALOMEDS::Color myCellColor;
    SALOMEDS::Color myNodeColor;
    SALOMEDS::Color myLinkColor;
    bool myIsShrank;

  public:
    static 
    size_t
    IsPossible(VISU::Result_i* theResult, 
	       const std::string& theMeshName,
	       VISU::Entity theEntity, 
	       const std::string& theFamilyName = "");
    virtual 
    Storable* 
    Create(VISU::Result_i* theResult, 
	   const std::string& theMeshName, 
	   VISU::Entity theEntity, 
	   const std::string& theFamilyName = "");

    static 
    size_t
    IsPossible(VISU::Result_i* theResult, 
	       const std::string& theMeshName, 
	       const std::string& theGroupName);
    virtual 
    Storable* 
    Create(VISU::Result_i* theResult, 
	   const std::string& theMeshName, 
	   const std::string& theGroupName);

    VISU::Entity
    GetEntity() const;

    const std::string& 
    GetSubMeshName() const;

    virtual
    void
    ToStream(std::ostringstream& theStr);

    virtual
    const char* 
    GetComment() const;

    static const std::string myComment;

    virtual
    QString
    GenerateName();

    virtual
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    static 
    Storable* 
    StorableEngine(SALOMEDS::SObject_ptr theSObject,
		   const Storable::TRestoringMap& theMap,
		   const std::string& thePrefix,
		   CORBA::Boolean theIsMultiFile);
    
    virtual
    VISU_Actor* 
    CreateActor();

    virtual
    void 
    UpdateActor(VISU_ActorBase* theActor);
  };
}

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_TableDlg.cxx
//  Author : Vadim SANDLER, Open CASCADE S.A.S. (vadim.sandler@opencascade.com)
//
#include "VISU_TableDlg.h"

#include "VISU_Table_i.hh"

#include "SUIT_Tools.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include "LightApp_Application.h"

#include "CAM_Module.h"

#include "SALOMEDSClient_Study.hxx"
#include "SALOMEDSClient_GenericAttribute.hxx"
#include "SALOMEDSClient_AttributeTableOfInteger.hxx"
#include "SALOMEDSClient_AttributeTableOfReal.hxx"
#include "SALOMEDSClient_StudyBuilder.hxx"

#include <QLayout>
#include <QTableWidget>
#include <QTabWidget>
#include <QList>
#include <QInputDialog>
#include <QLabel>
#include <QIntValidator>
#include <QDoubleValidator>
#include <QKeyEvent>
#include <QHeaderView>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>

#include "utilities.h"

const int MARGIN_SIZE      = 11;
const int SPACING_SIZE     = 6;
const int MIN_TABLE_WIDTH  = 200;
const int MIN_TABLE_HEIGHT = 200;

NumDelegateItem::NumDelegateItem( QObject* parent, NumValidator mode )
  : QItemDelegate( parent ),
    myMode( mode )
{
}

NumDelegateItem::~NumDelegateItem()
{
}

QWidget* NumDelegateItem::createEditor( QWidget* parent,
                                        const QStyleOptionViewItem& option,
                                        const QModelIndex& index ) const
{
  QLineEdit* editor = new QLineEdit( parent );
  switch ( myMode )
  {
  case NV_Int:
    editor->setValidator( new QIntValidator( editor ) );
    break;
  case NV_Real:
    editor->setValidator( new QDoubleValidator( editor ) );
    break;
  default:
    break;
  }
  return editor;
}

void NumDelegateItem::setEditorData( QWidget* editor,
                                     const QModelIndex& index ) const
{
  QLineEdit* aLE = qobject_cast<QLineEdit*>( editor );
  if ( !aLE )
    return;
  
  switch ( myMode )
  {
  case NV_Int:
    {
      int value = index.model()->data( index, Qt::DisplayRole ).toInt();
      aLE->setText( QString("%1").arg( value ) );
      break;
    }
  case NV_Real:
    {
      double value = index.model()->data(index, Qt::DisplayRole).toDouble();
      aLE->setText( QString("%1").arg( value ) );
      break;
    }
  default:
    aLE->setText( index.model()->data( index, Qt::DisplayRole ).toString() );
    break;
  }
}

class VISU_TableDlg::WidgetCointainer : public QWidget
{
public:
  WidgetCointainer( QWidget* parent, Qt::Orientation o, int lm, int tm, int rm, int bm, int s )
  : QWidget( parent )
  {
    QLayout* l = o == Qt::Horizontal ? (QLayout*)( new QHBoxLayout( this ) ) : ( QLayout* )( new QVBoxLayout( this ) );
    l->setContentsMargins( lm, tm, rm, bm ); 
    l->setSpacing( s );
    setLayout( l );
  }
  void addWidget( QWidget* w )
  {
    if ( w ) {
      QHBoxLayout* hl = qobject_cast<QHBoxLayout*>( layout() );
      QVBoxLayout* wl = qobject_cast<QVBoxLayout*>( layout() );
      if      ( hl ) hl->addWidget( w );
      else if ( wl ) wl->addWidget( w );
    }
  }
  void addSpacing( int s )
  {
    if ( s > 0 ) {
      QHBoxLayout* hl = qobject_cast<QHBoxLayout*>( layout() );
      QVBoxLayout* wl = qobject_cast<QVBoxLayout*>( layout() );
      if      ( hl ) hl->addSpacing( s );
      else if ( wl ) wl->addSpacing( s );
    }
  }
  void addStretch( int s )
  {
    if ( s >= 0 ) {
      QHBoxLayout* hl = qobject_cast<QHBoxLayout*>( layout() );
      QVBoxLayout* wl = qobject_cast<QVBoxLayout*>( layout() );
      if      ( hl ) hl->addStretch( s );
      else if ( wl ) wl->addStretch( s );
    }
  }
};

/*class VISU_Table : public QTableWidget {
public:
  VISU_Table( Orientation orient, QWidget* parent = 0 ) 
    : QTableWidget( parent ), myValidator( 0 ), myOrientation( orient ) {}
  VISU_Table( Orientation orient, int numRows, int numCols, QWidget* parent = 0 )
    : QTableWidget( numRows, numCols, parent ), myValidator( 0 ), myOrientation( orient ) {}
  
  void setValidator( QValidator* v = 0 ) { myValidator = v;  }
  bool isEditing() const { return QTable::isEditing(); }
  
protected:
  QWidget* createEditor ( int row, int col, bool initFromCell ) const
    {
      bool testUnits = ( myOrientation == Qt::Horizontal && col == 0 ) || ( myOrientation == Qt::Vertical && row == 0 );
      QWidget* wg = QTable::createEditor( row, col, initFromCell );
      if ( wg && wg->inherits("QLineEdit") && myValidator && !testUnits ) 
        (( QLineEdit*)wg)->setValidator( myValidator );
      return wg;
    }

protected:
  QValidator* myValidator;
  Orientation myOrientation;
  };*/

QString VISU_TableDlg::tableTitle( int type )
{
  QString tlt;
  switch ( type ) {
  case ttInt:  tlt = tr( "TABLE_OF_INTEGER_TLT" ); break;
  case ttReal: tlt = tr( "TABLE_OF_REAL_TLT" );    break;
  default:     tlt = tr( "TABLE_UNKNOWN_TLT" );    break;
  }
  return tlt;
}
/*!
  Constructor
*/
VISU_TableDlg::VISU_TableDlg( QWidget* parent, 
                              _PTR(Study) study, 
                              VISU::Table_i* table,
                              bool allowEdition,
                              int which,
                              Qt::Orientation orient,
                              bool showColumnTitles )
  : QDialog( parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint ), 
    myStudy( study ), myTable( table ), myEditCheck( 0 ), myUpdateCheck( 0 )
{
  allowEdition = true; //////////////////////////////////
  setWindowTitle( tr( "VIEW_TABLE_TLT" ) );
  setSizeGripEnabled( true );

  bool bHasIntTable = false;
  bool bHasRealTable = false;
  if ( myStudy && myTable ) {
    _PTR(SObject) aSObject = myStudy->FindObjectID( myTable->GetObjectEntry() );
    if ( aSObject ) {
      _PTR(GenericAttribute) anAttr;
      bHasIntTable  = aSObject->FindAttribute( anAttr, "AttributeTableOfInteger" );
      bHasRealTable = aSObject->FindAttribute( anAttr, "AttributeTableOfReal" );
    }
  }

  if ( allowEdition ) {
    myEditCheck = new QCheckBox( tr( "ALLOW_EDIT_CHECK" ), this );
    myUpdateCheck = new QCheckBox( tr( "DO_UPDATE_CHECK" ), this );
    connect( myEditCheck, SIGNAL( toggled( bool ) ), myUpdateCheck, SLOT( setEnabled( bool ) ) );
    myUpdateCheck->setEnabled( false );
    myUpdateCheck->hide(); // the feature has been temporarily disabled
  }

  QWidget* top = 0;

  if ( which == ttInt  || which == ttAll || which == ttAuto && bHasIntTable ) {
    myTableMap[ ttInt ]  = new TableWidget( this, orient );
    myTableMap[ ttInt ]->initialize( myStudy, myTable, ttInt );
    myTableMap[ ttInt ]->setEditEnabled( false );
    myTableMap[ ttInt ]->showColumnTitles( showColumnTitles );
    if ( myEditCheck )
      connect( myEditCheck, SIGNAL( toggled( bool ) ), myTableMap[ ttInt ], SLOT( setEditEnabled( bool ) ) );
  }
  if ( which == ttReal || which == ttAll || which == ttAuto && bHasRealTable ) {
    myTableMap[ ttReal ] = new TableWidget( this, orient );
    myTableMap[ ttReal ]->initialize( myStudy, myTable, ttReal );
    myTableMap[ ttReal ]->setEditEnabled( false );
    myTableMap[ ttReal ]->showColumnTitles( showColumnTitles );
    if ( myEditCheck )
      connect( myEditCheck, SIGNAL( toggled( bool ) ), myTableMap[ ttReal ], SLOT( setEditEnabled( bool ) ) );
  }
  
  if ( myTableMap.count() > 1 ) {
    QTabWidget* tw = new QTabWidget( this );
    for ( int i = ttInt; i < ttAll; i++ ) {
      if ( myTableMap.contains( i ) ) {
        tw->addTab( myTableMap[ i ], tableTitle( i ) );
        myTableMap[ i ]->layout()->setMargin( MARGIN_SIZE );
      }
    }
    top = tw;
  }
  else if ( myTableMap.count() == 1 ) {
    top = myTableMap[myTableMap.keys().first()];
  }
  else {
    QLabel* dumbLabel = new QLabel( tr( "ERR_TABLE_NOT_AVAILABLE" ), this );
    dumbLabel->setAlignment( Qt::AlignCenter );
    top = dumbLabel;
  }

  myOKBtn   = new QPushButton( tr( "BUT_OK" ), this );
  myHelpBtn = new QPushButton( tr( "BUT_HELP" ), this );
  
  QHBoxLayout* btnLayout = new QHBoxLayout; 
  btnLayout->setMargin( 0 );
  btnLayout->setSpacing( SPACING_SIZE );
  
  btnLayout->addWidget( myOKBtn );
  btnLayout->addStretch( 20 );
  btnLayout->addWidget( myHelpBtn );
  connect( myOKBtn,   SIGNAL( clicked() ), this, SLOT( close() ) );
  connect( myHelpBtn, SIGNAL( clicked() ), this, SLOT( help()  ) );

  QVBoxLayout* mainLayout = new QVBoxLayout( this );
  mainLayout->setMargin( MARGIN_SIZE );
  mainLayout->setSpacing( SPACING_SIZE );

  if ( myEditCheck ) {
    QHBoxLayout* checkLayout = new QHBoxLayout; 
    checkLayout->setMargin( 0 );
    checkLayout->setSpacing( SPACING_SIZE );
    checkLayout->addWidget( myEditCheck );
    checkLayout->addWidget( myUpdateCheck );
    mainLayout->addLayout( checkLayout );

    if ( LightApp_Application* app = ( LightApp_Application* )SUIT_Session::session()->activeApplication() ) {
      int anEnableEditing = app->resourceMgr()->booleanValue( "VISU", "tables_enable_editing", false );
      myEditCheck->setChecked( anEnableEditing );
    }
  }
  mainLayout->addWidget( top );
  mainLayout->addLayout( btnLayout );

  resize( 500, 400 );
  SUIT_Tools::centerWidget( this, parent );
}

/*!
  Destructor
*/
VISU_TableDlg::~VISU_TableDlg()
{
}

/*!
  <OK> button slot, saves table(s)
  Called only in create/edit mode ( <edit> parameter for constructor is true )
*/
/*
void VISU_TableDlg::onOK()
{
  myOKBtn->setFocus(); // accept possible changes
  bool done = true;

  if ( myObject ) {
    _PTR(Study) study = myObject->GetStudy();
    _PTR(AttributeTableOfInteger) tblIntAttr;
    _PTR(AttributeTableOfReal)    tblRealAttr;

    if ( study ) {
      _PTR(StudyBuilder) builder = study->NewBuilder();
      builder->NewCommand(); // start transaction !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      try {
        if ( myTableMap.contains( ttInt ) ) {
          builder->RemoveAttribute( myObject, "AttributeTableOfInteger" );
          tblIntAttr = builder->FindOrCreateAttribute( myObject, "AttributeTableOfInteger" );

          int i;
          int nbRows  = myTableMap[ ttInt ]->getNumRows();
          int nbCols  = myTableMap[ ttInt ]->getNumCols();
          QString tlt = myTableMap[ ttInt ]->getTableTitle();
          QStringList rowTitles, colTitles, units;
          myTableMap[ ttInt ]->getRowTitles( rowTitles );
          myTableMap[ ttInt ]->getColTitles( colTitles );
          myTableMap[ ttInt ]->getUnits( units );
          
          if ( nbRows > 0) {
            // data
            int nRow = 0;
            tblIntAttr->SetNbColumns( nbCols );
            for ( i = 0; i < nbRows; i++ ) {
              QStringList data;
              myTableMap[ ttInt ]->getRowData( i, data );
              bool bEmptyRow = true;
              for ( int j = 0; j < data.count(); j++ ) {
                if ( !data[ j ].isNull() ) {
                  tblIntAttr->PutValue( data[ j ].toInt(), nRow+1, j+1 );
                  bEmptyRow = false;
                }
              }
              if ( !bEmptyRow ) {  // Skip rows with no data !!!
                // set row title
                tblIntAttr->SetRowTitle( nRow+1, rowTitles[ i ].isNull() ? "" : (const char*)rowTitles[ i ].toLatin1() ); 
                // set row unit
                tblIntAttr->SetRowUnit( nRow+1, units[ i ].isNull() ? "" : (const char*)units[ i ].toLatin1() ); 
                nRow++;
              }
            }
            if ( nRow > 0 ) { // Set columns only if table is not empty, otherwise exception is raised !!!
              // column titles
              for ( i = 0; i < colTitles.count(); i++ )
                tblIntAttr->SetColumnTitle( i+1, colTitles[ i ].isNull() ? "" : (const char*)colTitles[ i ].toLatin1() );
            }
          }
          // title
          tblIntAttr->SetTitle( (const char*)myTableMap[ ttInt ]->getTableTitle().toLatin1() );
        }
        if ( myTableMap.contains( ttReal ) ) {
          builder->RemoveAttribute( myObject, "AttributeTableOfReal" );
          tblRealAttr = builder->FindOrCreateAttribute( myObject, "AttributeTableOfReal" );

          int i;
          int nbRows  = myTableMap[ ttReal ]->getNumRows();
          int nbCols  = myTableMap[ ttReal ]->getNumCols();
          QString tlt = myTableMap[ ttReal ]->getTableTitle();
          QStringList rowTitles, colTitles, units;
          myTableMap[ ttReal ]->getRowTitles( rowTitles );
          myTableMap[ ttReal ]->getColTitles( colTitles );
          myTableMap[ ttReal ]->getUnits( units );
          
          if ( nbRows > 0) {
            // data
            int nRow = 0;
            tblRealAttr->SetNbColumns( nbCols );
            for ( i = 0; i < nbRows; i++ ) {
              QStringList data;
              myTableMap[ ttReal ]->getRowData( i, data );
              bool bEmptyRow = true;
              for ( int j = 0; j < data.count(); j++ ) {
                if ( !data[ j ].isNull() ) {
                  tblRealAttr->PutValue( data[ j ].toDouble(), nRow+1, j+1 );
                  bEmptyRow = false;
                }
              }
              if ( !bEmptyRow ) {  // Skip rows with no data !!!
                // set row title
                tblRealAttr->SetRowTitle( nRow+1, rowTitles[ i ].isNull() ? "" : (const char*)rowTitles[ i ].toLatin1() ); 
                // set row unit
                tblRealAttr->SetRowUnit( nRow+1, units[ i ].isNull() ? "" : (const char*)units[ i ].toLatin1() );
                nRow++;
              }
            }
            if ( nRow > 0 ) { // Set columns only if table is not empty, otherwise exception is raised !!!
              // column titles
              for ( i = 0; i < colTitles.count(); i++ )
                tblRealAttr->SetColumnTitle( i+1, colTitles[ i ].isNull() ? "" : (const char*)colTitles[ i ].toLatin1() );
            }
          }
          // title
          tblRealAttr->SetTitle( (const char*)myTableMap[ ttReal ]->getTableTitle().toLatin1() );
        }
        if ( myTableMap.contains( ttInt ) || myTableMap.contains( ttReal ) )
          builder->CommitCommand(); // commit transaction !!!!!!!!!!!!!!!!!!!!!!!!!!!
        else
          builder->AbortCommand();  // abort transaction  !!!!!!!!!!!!!!!!!!!!!!!!!!!
      }
      catch( ... ) {
        MESSAGE("VISU_TableDlg::onOK : Exception has been caught !!!");
        builder->AbortCommand();  // abort transaction  !!!!!!!!!!!!!!!!!!!!!!!!!!!
        done = false;
        SUIT_MessageBox::critical ( this, tr("ERR_ERROR"), tr("ERR_APP_EXCEPTION") );
      }
    }
  }
  if ( done ) 
    accept();
}
*/

/*!
  <Help> button slot, shows corresponding help page
*/
void VISU_TableDlg::help()
{
  QString aHelpFileName = "table_presentations_page.html";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(app->activeModule() ? app->moduleName(app->activeModule()->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning( this,
                              QObject::tr("WRN_WARNING"),
                              QObject::tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                              arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName) );
  }
}

/*!
  Provides help on F1 button click
*/
void VISU_TableDlg::keyPressEvent( QKeyEvent* e )
{
  QDialog::keyPressEvent( e );
  if ( e->isAccepted() )
    return;

  if ( e->key() == Qt::Key_F1 ) {
    e->accept();
    help();
  }
}

/*!
  Constructor
*/
VISU_TableDlg::TableWidget::TableWidget( QWidget* parent, 
                                         Qt::Orientation orientation )
  : QWidget( parent ), myOrientation( orientation )
{
  myTitleEdit = new QLineEdit( this );

  myTable = new QTableWidget( 5, 5, this );
  myTable->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding ) );
  myTable->setMinimumSize( MIN_TABLE_WIDTH, MIN_TABLE_HEIGHT );
  myTable->setSelectionMode( QAbstractItemView::SingleSelection );
  myTable->setShowGrid( true );
  myTable->horizontalHeader()->setMovable( false );
  myTable->verticalHeader()->setMovable( false );
  myTable->setDragEnabled( false );

  myAdjustBtn    = new QPushButton( VISU_TableDlg::tr( "ADJUST_CELLS_BTN" ),  this );
  myAddRowBtn    = new QPushButton( VISU_TableDlg::tr( "ADD_ROW_BTN" ),       this );
  myDelRowBtn    = new QPushButton( VISU_TableDlg::tr( "REMOVE_ROW_BTN" ),    this );
  myAddColBtn    = new QPushButton( VISU_TableDlg::tr( "ADD_COLUMN_BTN" ),    this );
  myDelColBtn    = new QPushButton( VISU_TableDlg::tr( "REMOVE_COLUMN_BTN" ), this );
  mySelectAllBtn = new QPushButton( VISU_TableDlg::tr( "SELECT_ALL_BTN" ),    this );
  myClearBtn     = new QPushButton( VISU_TableDlg::tr( "CLEAR_BTN" ),         this );

  mySortPolicyLabel = new QLabel( VISU_TableDlg::tr( "VISU_TABLES_SORT_POLICY" ), this );
  mySortPolicyCombo = new QComboBox( this );
  mySortPolicyCombo->insertItems( 0, QStringList() <<
                                  VISU_TableDlg::tr( "VISU_TABLES_EMPTY_LOWEST" ) <<
                                  VISU_TableDlg::tr( "VISU_TABLES_EMPTY_HIGHEST" ) <<
                                  VISU_TableDlg::tr( "VISU_TABLES_EMPTY_FIRST" ) <<
                                  VISU_TableDlg::tr( "VISU_TABLES_EMPTY_LAST" ) <<
                                  VISU_TableDlg::tr( "VISU_TABLES_EMPTY_IGNORE" ) );

  if ( LightApp_Application* app = ( LightApp_Application* )SUIT_Session::session()->activeApplication() ) {
    int aSortPolicy = app->resourceMgr()->integerValue( "VISU", "tables_sort_policy", 3 );
    mySortPolicyCombo->setCurrentIndex( aSortPolicy );
  }

  // the features has been temporarily disabled
  myAddRowBtn->hide();
  myDelRowBtn->hide();
  myAddColBtn->hide();
  myDelColBtn->hide();
  mySelectAllBtn->hide();
  myClearBtn->hide();

  QVBoxLayout* btnLayout = new QVBoxLayout;
  btnLayout->setMargin( 0 );
  btnLayout->setSpacing( SPACING_SIZE );
  btnLayout->addWidget( myAdjustBtn );
  btnLayout->addStretch( 20 );
  btnLayout->addWidget( myAddRowBtn );
  btnLayout->addWidget( myDelRowBtn );
  btnLayout->addWidget( myAddColBtn );
  btnLayout->addWidget( myDelColBtn );
  btnLayout->addStretch( 20 );
  btnLayout->addWidget( mySelectAllBtn );
  btnLayout->addWidget( myClearBtn );

  QHBoxLayout* sortLayout = new QHBoxLayout;
  sortLayout->setMargin( 0 );
  sortLayout->setSpacing( SPACING_SIZE );
  sortLayout->addWidget( mySortPolicyLabel );
  sortLayout->addWidget( mySortPolicyCombo );
  sortLayout->addStretch( 1 );

  QGridLayout* mainLayout = new QGridLayout( this );
  mainLayout->setMargin( 0 );
  mainLayout->setSpacing( SPACING_SIZE );
  mainLayout->addWidget( myTitleEdit, 0, 0, 1, 2 );
  mainLayout->addWidget( myTable,     1, 0 );
  mainLayout->addLayout( btnLayout,   1, 1 );
  mainLayout->addLayout( sortLayout,  2, 0, 1, 2 );

  connect( myTable, SIGNAL( itemSelectionChanged() ),
           this, SLOT( updateButtonsState() ) );
  connect( myTable, SIGNAL( currentItemChanged( QTableWidgetItem*, QTableWidgetItem* ) ),
           this, SLOT( updateButtonsState() ) );
  connect( myAddRowBtn,    SIGNAL( clicked() ),   this, SLOT( addRow() ) );
  connect( myAddColBtn,    SIGNAL( clicked() ),   this, SLOT( addCol() ) );
  connect( myDelRowBtn,    SIGNAL( clicked() ),   this, SLOT( delRow() ) );
  connect( myDelColBtn,    SIGNAL( clicked() ),   this, SLOT( delCol() ) );
  connect( myAdjustBtn,    SIGNAL( clicked() ),   this, SLOT( adjustTable() ) );
  connect( mySelectAllBtn, SIGNAL( clicked() ),   this, SLOT( selectAll() ) );
  connect( myClearBtn,     SIGNAL( clicked() ),   this, SLOT( clearTable() ) );
  connect( myTable->horizontalHeader(), SIGNAL( sectionClicked( int ) ),
           this, SLOT( columnClicked( int ) ) );
  connect( myTable->verticalHeader(), SIGNAL( sectionClicked( int ) ),
           this, SLOT( rowClicked( int ) ) );
  myTable->horizontalHeader()->installEventFilter( this );
  myTable->verticalHeader()->installEventFilter( this );
  myTable->installEventFilter( this );

  updateButtonsState();
}

/*!
  Destructor
*/
VISU_TableDlg::TableWidget::~TableWidget()
{
}

/*!
  Initialize widget
*/
void VISU_TableDlg::TableWidget::initialize( _PTR(Study) study, VISU::Table_i* table, int type )
{
  myStudy = study;
  myTableObj = table;
  myType = type;

  setUnitsTitle( VISU_TableDlg::tr( "UNITS_TLT" ) );
  setEditEnabled( false );
  showColumnTitles( false );

  updateTableFromServant( true );
  updateButtonsState();
}

void VISU_TableDlg::TableWidget::updateTableFromServant( bool adjust )
{
  _PTR(SObject) aSObject = myStudy->FindObjectID( myTableObj->GetObjectEntry() );
  if ( aSObject ) {
    int i, j;
    switch ( myType ) {
    case ttInt:
      myTable->setItemDelegate( new NumDelegateItem( this, NumDelegateItem::NV_Int ) );
      if ( aSObject->FindAttribute( myAttr, "AttributeTableOfInteger" ) ) {
        _PTR(AttributeTableOfInteger) intAttr  = myAttr;
        try {
          // title
          setTableTitle( intAttr->GetTitle().c_str() );
          // nb of rows & cols
          int nbRows = intAttr->GetNbRows() ; 
          int nbCols = intAttr->GetNbColumns();
          setNumRows( nbRows );
          setNumCols( nbCols );
          // rows titles
          QStringList strlist;
          std::vector<std::string> titles = intAttr->GetRowTitles();
          for ( i = 0; i < nbRows; i++ ) {
            if ( titles.size() > 0 )
              strlist.append( titles[i].c_str() );
            else
              strlist.append( "" );
          }
          setRowTitles( strlist );
          // columns titles
          strlist.clear();
          titles = intAttr->GetColumnTitles();
          for ( i = 0; i < nbCols; i++ ) {
            if ( titles.size() > 0 )
              strlist.append( titles[i].c_str() );
            else
              strlist.append( "" );
          }
          setColTitles( strlist );
          // units
          strlist.clear();
          titles = intAttr->GetRowUnits();
          if ( titles.size() > 0 ) {
            for ( i = 0; i < nbRows; i++ )
              strlist.append( titles[i].c_str() );
            setUnits( strlist );
          }
          // data
          for ( i = 1; i <= nbRows; i++ ) {
            strlist.clear();
            for ( j = 1; j <= nbCols; j++ ) {
              if ( intAttr->HasValue( i, j ) )
                strlist.append( QString::number( intAttr->GetValue( i, j ) ) );
              else
                strlist.append( QString::null );
            }
            setRowData( i-1, strlist );
          }
          if ( adjust ) adjustTable();
        }
        catch( ... ) {
          MESSAGE("VISU_TableDlg::TableWidget::initialize : Exception has been caught !!!");
        }
      }
      break;
    case ttReal:
      myTable->setItemDelegate( new NumDelegateItem( this, NumDelegateItem::NV_Real ) );
      if ( aSObject->FindAttribute( myAttr, "AttributeTableOfReal" ) ) {
        _PTR(AttributeTableOfReal) realAttr = myAttr;
        try {
          // title
          setTableTitle( realAttr->GetTitle().c_str() );
          // nb of rows & cols
          int nbRows = realAttr->GetNbRows() ; 
          int nbCols = realAttr->GetNbColumns();
          setNumRows( nbRows );
          setNumCols( nbCols );
          // rows titles
          QStringList strlist;
          std::vector<std::string> titles = realAttr->GetRowTitles();
          for ( i = 0; i < nbRows; i++ ) {
            if ( titles.size() > 0 )
              strlist.append( titles[i].c_str() );
            else
              strlist.append( "" );
          }
          setRowTitles( strlist );
          // columns titles
          strlist.clear();
          titles = realAttr->GetColumnTitles();
          for ( i = 0; i < nbCols; i++ ) {
            if ( titles.size() > 0 )
              strlist.append( titles[i].c_str() );
            else
              strlist.append( "" );
          }
          setColTitles( strlist );
          // units
          strlist.clear();
          titles = realAttr->GetRowUnits();
          if ( titles.size() > 0 ) {
            for ( i = 0; i < nbRows; i++ )
              strlist.append( titles[i].c_str() );
            setUnits( strlist );
          }
          // data
          for ( i = 1; i <= nbRows; i++ ) {
            strlist.clear();
            for ( j = 1; j <= nbCols; j++ ) {
              if ( realAttr->HasValue( i, j ) )
                strlist.append( QString::number( realAttr->GetValue( i, j ) ) );
              else
                strlist.append( QString::null );
            }
            setRowData( i-1, strlist );
          }
          if ( adjust ) adjustTable();
        }
        catch( ... ) {
          MESSAGE("VISU_TableDlg::TableWidget::initialize : Exception has been caught !!!");
        }
      }
      break;
    default:
      break;
    }
  }
}

/*!
  Enable / disable editing
*/
void VISU_TableDlg::TableWidget::setEditEnabled( bool enable )
{
  if( !enable ) {
    myTable->horizontalHeader()->setSortIndicatorShown( false );
    myTable->verticalHeader()->setSortIndicatorShown( false );
    //adjustTable();
  }

  mySortPolicyLabel->setEnabled( enable );
  mySortPolicyCombo->setEnabled( enable );

  // the rest features have been temporarily disabled
  enable = false;

  myTitleEdit->setReadOnly( !enable );
  myTable->setEditTriggers( enable ? 
                            QAbstractItemView::DoubleClicked   | 
                            QAbstractItemView::SelectedClicked | 
                            QAbstractItemView::EditKeyPressed  :
                            QAbstractItemView::NoEditTriggers );
  myAddRowBtn->setVisible( enable );
  myDelRowBtn->setVisible( enable );
  myAddColBtn->setVisible( enable );
  myDelColBtn->setVisible( enable );
  mySelectAllBtn->setVisible( enable );
  myClearBtn->setVisible( enable );
}

/*!
  Show / hide column titles
*/
void VISU_TableDlg::TableWidget::showColumnTitles( bool showTitles )
{
  if ( myOrientation == Qt::Horizontal )
    myTable->horizontalHeader()->setVisible( showTitles );
  else
    myTable->verticalHeader()->setVisible( showTitles );
}
  
/*!
  Sets table title
*/
void VISU_TableDlg::TableWidget::setTableTitle( const QString& title )
{
  myTitleEdit->setText( title );
}

/*!
  Gets table title
*/
QString VISU_TableDlg::TableWidget::getTableTitle()
{
  return myTitleEdit->text();
}

/*!
  Sets total number of rows
*/
void VISU_TableDlg::TableWidget::setNumRows( const int num )
{
  myOrientation == Qt::Horizontal ? myTable->setRowCount( num ) : myTable->setColumnCount( num );
}

/*!
  Gets total number of rows
*/
int VISU_TableDlg::TableWidget::getNumRows()
{
  return myOrientation == Qt::Horizontal ? myTable->rowCount() : myTable->columnCount();
}

/*!
  Sets total number of columns
*/
void VISU_TableDlg::TableWidget::setNumCols( const int num )
{
  // !!! first column contains units !!!
  myOrientation == Qt::Horizontal ? myTable->setColumnCount( num+1 ) : myTable->setRowCount( num+1 );
//  myOrientation == Qt::Horizontal ? myTable->setColumnReadOnly( 0, true ) : myTable->setRowReadOnly( 0, true );
}

/*!
  Gets total number of columns
*/
int VISU_TableDlg::TableWidget::getNumCols()
{
  // !!! first column contains units !!!
  return myOrientation == Qt::Horizontal ? myTable->columnCount()-1 : myTable->rowCount()-1;
}
/*!
  Sets rows titles
*/
void VISU_TableDlg::TableWidget::setRowTitles( QStringList& tlts )
{
  QStringList aLabels;
  for ( int i = 0; i < tlts.count(); i++ )
    tlts[i].isNull() ? aLabels.append("") : aLabels.append( tlts[i] );

  myOrientation == Qt::Horizontal ?
    myTable->setVerticalHeaderLabels( aLabels ) :
    myTable->setHorizontalHeaderLabels( aLabels );
    
    //  myTable->verticalHeader()->setLabel( i, tlts[i] ) : 
    //myTable->horizontalHeader()->setLabel( i, tlts[i] );
  //}
}
/*!
  Gets rows titles
*/
void VISU_TableDlg::TableWidget::getRowTitles( QStringList& tlts )
{
  tlts.clear();
  if ( myOrientation == Qt::Horizontal ) {
    for ( int i = 0; i < myTable->rowCount(); i++ ) {
      tlts.append( myTable->verticalHeaderItem(i) ? myTable->verticalHeaderItem(i)->text() : "" );
    }
  }
  else {
    for ( int i = 0; i < myTable->columnCount(); i++ ) {
      tlts.append( myTable->horizontalHeaderItem(i) ? myTable->horizontalHeaderItem(i)->text() : "" );
    }
  }
}
/*!
  Sets columns titles
*/
void VISU_TableDlg::TableWidget::setColTitles( QStringList& tlts )
{
  QStringList aLabels;

  // !!! first column contains units !!!
  aLabels.append(""); // it'll be initialized below - in setUnitsTitle() method

  for ( int i = 0; i < tlts.count(); i++ )
    tlts[i].isNull() ? aLabels.append("") : aLabels.append( tlts[i] );

  myOrientation == Qt::Horizontal ?
    myTable->setHorizontalHeaderLabels( aLabels ) :
    myTable->setVerticalHeaderLabels( aLabels );
  
  setUnitsTitle( VISU_TableDlg::tr( "UNITS_TLT" ) );
}
/*!
  Sets columns titles
*/
void VISU_TableDlg::TableWidget::getColTitles( QStringList& tlts )
{
  // !!! first column contains units !!!
  tlts.clear();
  if ( myOrientation == Qt::Horizontal ) {
    for ( int i = 1; i < myTable->columnCount(); i++ ) {
      tlts.append( myTable->horizontalHeaderItem(i) ? myTable->horizontalHeaderItem(i)->text() : "" );
    }    
  }
  else {
    for ( int i = 1; i < myTable->rowCount(); i++ ) {
      tlts.append( myTable->verticalHeaderItem(i) ? myTable->verticalHeaderItem(i)->text() : "" );
    }
  }
}
/*!
  Sets units title
*/
void VISU_TableDlg::TableWidget::setUnitsTitle( const QString& tlt ) {
  // !!! first column contains units !!!
  myTable->model()->setHeaderData( 0, myOrientation, QVariant(tlt.isNull() ? "" : tlt), Qt::DisplayRole );
}
/*!
  Sets units
*/
void VISU_TableDlg::TableWidget::setUnits( QStringList& units )
{
  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myTable->model() );
  if ( aModel )
  {
    QModelIndex anIndex;
    for ( int i = 0; i < units.count(); i++ )
    {
      myOrientation == Qt::Horizontal ?
        anIndex = aModel->index( i, 0 ) :
        anIndex = aModel->index( 0, i );

      aModel->setData( anIndex, QVariant( units[i].isNull() ? "" : units[i] ) );      
    }
  }
}
/*!
  Gets units
*/
void VISU_TableDlg::TableWidget::getUnits( QStringList& units )
{
  units.clear();
  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myTable->model() );
  if ( aModel )
  {
    if ( myOrientation == Qt::Horizontal )
    {
      for ( int i = 0; i < myTable->rowCount(); i++ )
        units.append( aModel->index( i, 0 ).data().toString() );
    }
    else {
      for ( int i = 0; i < myTable->columnCount(); i++ )
        units.append( aModel->index( 0, i ).data().toString() );
    }
  }
}
/*!
  Sets row data
*/
void VISU_TableDlg::TableWidget::setRowData( int row, QStringList& data )
{
  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myTable->model() );
  if ( aModel )
  {
    QModelIndex anIndex; 
    if ( row >= 0 && row < getNumRows() ) {
      for ( int i = 0; i < data.count(); i++ )
      {
        myOrientation == Qt::Horizontal ? anIndex = aModel->index( row, i+1 ) :
                                          anIndex = aModel->index( i+1, row );
        aModel->setData( anIndex, QVariant( data[i] ) );
          
      }
    }
  }
}
/*!
  Gets row data
*/
void VISU_TableDlg::TableWidget::getRowData( int row, QStringList& data )
{
  data.clear();
  QAbstractTableModel* aModel = qobject_cast<QAbstractTableModel*>( myTable->model() );
  if ( aModel )
  {
    if ( row >= 0 && row < getNumRows() )
    {
      if ( myOrientation == Qt::Horizontal )
      {
        for ( int i = 1; i < myTable->columnCount(); i++ )
          data.append( aModel->index( row, i ).data().toString() );
      }
      else {
        for ( int i = 1; i < myTable->rowCount(); i++ )
          data.append( aModel->index( i, row ).data().toString() );
      }
    }
  }
}
/*!
  Adjusts table cell to see contents, <Adjust Cells> button slot
*/
void VISU_TableDlg::TableWidget::adjustTable()
{
  myTable->resizeRowsToContents();
  myTable->resizeColumnsToContents();
}
/*!
  Called when selection changed in table
*/
void VISU_TableDlg::TableWidget::updateButtonsState()
{
  if ( myTable->editTriggers() == QAbstractItemView::NoEditTriggers )
    return;
  bool bDR = false; // <Delete Row(s)>
  bool bDC = false; // <Delete Column(s)>
  bool bSA = false; // <Select All>
  bool bCT = false; // <Clear>
  int i;
  //TO DO column/row selection check
  /*int c = myOrientation == Qt::Horizontal ? 0 : 1;
  for ( i = c; i < myTable->rowCount(); i++ ) {
    
    if ( myTable->isRowSelected( i, true ) )
      bDR = true;
    else 
      bSA = true;
  }
  c = myOrientation == Qt::Horizontal ? 1 : 0;
  for ( i = c; i < myTable->columnCount(); i++ ) {
    if ( myTable->isColumnSelected( i, true ) )
      bDC = true;
    else 
      bSA = true;
      }*/
  /*int nbSel = myTable->numSelections();
  for ( i = 0; i < nbSel; i++ ) {
    QTableSelection ts = myTable->selection( i );
    for ( int j = ts.topRow(); j < ts.bottomRow()+1; j++) {
      for ( int k = ts.leftCol(); k < ts.rightCol()+1; k++) {
        if ( myTable->item( j, k ) )
          bCT = true;
      }
    }
    }*/
  QList<QTableWidgetItem*> aSelection = myTable->selectedItems();
  QList<QTableWidgetItem*>::ConstIterator anIt = aSelection.constBegin(),
    anEndIt = aSelection.constEnd();
  for ( ; anIt !=  anEndIt; anIt++ )
  {
    if( *anIt )
    {
      bCT = true;
      break;
    }
  }
  
  if ( myTable->item( myTable->currentRow(), myTable->currentColumn() ) )
    bCT = true;
  myDelRowBtn->setEnabled( bDR );
  myDelColBtn->setEnabled( bDC );
  mySelectAllBtn->setEnabled( bSA );
  myClearBtn->setEnabled( bCT );
}
/*!
  <Add row> button slot
*/
void VISU_TableDlg::TableWidget::addRow()
{
  myTable->insertRow( myTable->rowCount() );
  updateButtonsState();
}
/*!
  <Add column> button slot
*/
void VISU_TableDlg::TableWidget::addCol()
{
  myTable->insertColumn( myTable->columnCount() );
  updateButtonsState();
}
/*!
  <Delete row(s)> button slot
*/
void VISU_TableDlg::TableWidget::delRow()
{
  //TODO
  /*int c = myOrientation == Qt::Horizontal ? 0 : 1;
  QList<int> il;
  int i;
  for ( i = c; i < myTable->rowCount(); i++ )
    if ( myTable->isRowSelected( i, true ) )
      il.append( i );
  if ( il.count() > 0 ) {
    QMemArray<int> ildel( il.count() );
    for ( i = 0; i < il.count(); i++ )
      ildel[ i ] = il[ i ];
    myTable->removeRows( ildel );
    }*/
  
  updateButtonsState();
}
/*!
  <Delete column(s)> button slot
*/
void VISU_TableDlg::TableWidget::delCol()
{
  //TODO
  /*int c = myOrientation == Qt::Horizontal ? 1 : 0;
  QValueList<int> il;
  int i;
  for ( i = c; i < myTable->numCols(); i++ )
    if ( myTable->isColumnSelected( i, true ) )
      il.append( i );
  if ( il.count() > 0 ) {
    QMemArray<int> ildel( il.count() );
    for ( i = 0; i < il.count(); i++ )
      ildel[ i ] = il[ i ];
    myTable->removeColumns( ildel );
    }*/
  updateButtonsState();
}
/*!
  <Select All> button slot
*/
void VISU_TableDlg::TableWidget::selectAll()
{
  /*myTable->clearSelection();
  QTableSelection ts;
  ts.init( 0, 0 ); ts.expandTo( myTable->numRows()-1, myTable->numCols()-1 );
  myTable->addSelection( ts );*/
  myTable->selectAll();
  updateButtonsState();
}
/*!
  <Clear> button slot
*/
void VISU_TableDlg::TableWidget::clearTable()
{
  /*int nbSel = myTable->numSelections();
  for ( int i = 0; i < nbSel; i++ ) {
    QTableSelection ts = myTable->selection( i );
    for ( int j = ts.topRow(); j < ts.bottomRow()+1; j++) {
      if ( myOrientation == Qt::Vertical && j == 0 ) {
//      continue;      // UNITS
      }
      for ( int k = ts.leftCol(); k < ts.rightCol()+1; k++) {
        if ( myOrientation == Qt::Horizontal && k == 0 ) {
//        continue;   // UNITS
        }
        myTable->clearCell( j, k );
      }
    }
  }
  if ( nbSel == 0 )
    myTable->clearCell( myTable->currentRow(), myTable->currentColumn() );
    myTable->clearSelection();*/
  myTable->clearContents();
  updateButtonsState();
}
/*!
  Column clicked slot
*/
void VISU_TableDlg::TableWidget::columnClicked( int column )
{
  if ( myTableObj && mySortPolicyCombo->isEnabled() ) {
    myTableObj->SortByRow( column + 1,
                           ( VISU::SortOrder )myTable->horizontalHeader()->sortIndicatorOrder(),
                           ( VISU::SortPolicy )mySortPolicyCombo->currentIndex() );
    myTable->horizontalHeader()->setSortIndicatorShown( true );
    myTable->verticalHeader()->setSortIndicatorShown( false );
    updateTableFromServant( false );
  }
}
/*!
  Row clicked slot
*/
void VISU_TableDlg::TableWidget::rowClicked( int row )
{
  /* the feature has been temporarily disabled
  if ( myTableObj && mySortPolicyCombo->isEnabled() && row > 0 ) { // first row contains units
    myTableObj->SortByColumn( row,
                              ( VISU::SortOrder )myTable->verticalHeader()->sortIndicatorOrder(),
                              ( VISU::SortPolicy )mySortPolicyCombo->currentIndex() );
    myTable->horizontalHeader()->setSortIndicatorShown( false );
    myTable->verticalHeader()->setSortIndicatorShown( true );
    updateTableFromServant( false );
  }
  */
}
/*!
  Event filter - handles titles editing
*/
bool VISU_TableDlg::TableWidget::eventFilter( QObject* o, QEvent* e )
{
  if ( e->type() == QEvent::MouseButtonDblClick) {
    //TODO
    /*QMouseEvent* me = ( QMouseEvent* )e;
    if ( me->button() == Qt::LeftButton && (myTable->editTriggers() != QAbstractItemView::NoEditTriggers ) ) {
      if ( o == myTable->horizontalHeader() ) {
        for ( int i = 0; i < myTable->horizontalHeader()->count(); i++ ) {
          QRect rect = myTable->horizontalHeader()->sectionRect( i );
          rect.addCoords( 1, 1, -1, -1 );
          if ( rect.contains( myTable->horizontalHeader()->mapFromGlobal( me->globalPos() ) ) ) {
            if ( myOrientation == Qt::Vertical || i != 0 ) {
              bool bOk;
              QString tlt = QInputDialog::getText( tr( "SET_TITLE_TLT" ), 
                                                   tr( "TITLE_LBL" ),
                                                   QLineEdit::Normal,
                                                   myTable->horizontalHeader()->label( i ),
                                                   &bOk,
                                                   this );
              if ( bOk && !tlt.isNull() )
                myTable->horizontalHeader()->setLabel( i, tlt );
              break;
            }
          }
        }
      }
      if ( o == myTable->verticalHeader() ) {
        for ( int i = 0; i < myTable->verticalHeader()->count(); i++ ) {
          QRect rect = myTable->verticalHeader()->sectionRect( i );
          rect.addCoords( 1, 1, -1, -1 );
          if ( rect.contains( myTable->verticalHeader()->mapFromGlobal( me->globalPos() ) ) ) {
            if ( myOrientation == Qt::Horizontal || i != 0 ) {
              bool bOk;
              QString tlt = QInputDialog::getText( tr( "SET_TITLE_TLT" ), 
                                                   tr( "TITLE_LBL" ),
                                                   QLineEdit::Normal,
                                                   myTable->verticalHeader()->label( i ),
                                                   &bOk,
                                                   this );
              if ( bOk && !tlt.isNull() )
                myTable->verticalHeader()->setLabel( i, tlt );
              break;
            }
          }
        }
      }
      }*/    
  }     
  else if ( e->type() == QEvent::KeyRelease && o == myTable ) {
    QKeyEvent* ke = (QKeyEvent*)e;
    if ( ke->key() == Qt::Key_Delete && (myTable->editTriggers() != QAbstractItemView::NoEditTriggers) ) {
      clearTable();
    }
    else if ( ke->key() == Qt::Key_Backspace && (myTable->editTriggers() != QAbstractItemView::NoEditTriggers) ) {
      clearTable();
      int i = myTable->currentRow();
      int j = myTable->currentColumn() - 1;
      if ( j < 0 ) { j = myTable->columnCount()-1; i--; }
      if ( i >= 0 && j >= 0 )
        myTable->setCurrentCell( i, j );
    }
  }
  return QWidget::eventFilter( o, e );
}

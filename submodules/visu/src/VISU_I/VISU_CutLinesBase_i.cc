// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_CutLinesBase_i.cc
//  Author : Oleg UVAROV
//  Module : VISU
//
#include "VISU_CutLinesBase_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_Actor.h"
#include "VISU_ScalarMapAct.h"
#include "VISU_CutLinesBasePL.hxx"
#include "VISU_Convertor.hxx"
#include "VISU_PipeLineUtils.hxx"

#include "SUIT_ResourceMgr.h"

#include <vtkAppendPolyData.h>

static double EPS_machine = 1.0E-7;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
VISU::CutLinesBase_i::
CutLinesBase_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  myUseAbsLength( false ),
  myCutLinesBasePL(NULL)
{}


//---------------------------------------------------------------
void
VISU::CutLinesBase_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const CutLinesBase_i* aPrs3d = dynamic_cast<const CutLinesBase_i*>(theOrigin)){
    CutLinesBase_i* anOrigin = const_cast<CutLinesBase_i*>(aPrs3d);
    myUseAbsLength = anOrigin->IsUseAbsoluteLength();
    CopyCurvesInverted(anOrigin->GetCurvesInverted());
  }
}


//---------------------------------------------------------------
/*! Copy map to /a myMapCurvesInverted.
 * \param theCurves - map
 */
void 
VISU::CutLinesBase_i
::CopyCurvesInverted(const TCurvesInv& theCurves)
{
  myMapCurvesInverted = theCurves;
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::CutLinesBase_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  SetUseAbsoluteLength( aResourceMgr->booleanValue( "VISU", "use_absolute_length", false ) );
  SetAllCurvesInverted( aResourceMgr->booleanValue( "VISU", "invert_all_curves", false ) );
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::CutLinesBase_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetNbLines(VISU::Storable::FindValue(theMap,"myNbLines").toInt());

  SetUseAbsoluteLength(VISU::Storable::FindValue(theMap,"myUseAbsLength").toInt());

  // Restoring the map - \a myMapCurvesInverted
  QStringList aMapCurvesInverted = VISU::Storable::FindValue(theMap,"myMapCurvesInverted").split("|",QString::SkipEmptyParts );
  if (aMapCurvesInverted.count() == GetNbLines()){
    for(int i = 0, iEnd = GetNbLines(); i < iEnd ; i++){
      if(aMapCurvesInverted[i].toInt())
	SetCurveInverted(i,true);
      else
	SetCurveInverted(i,false);
    }
  } else {
    for(int i = 0, iEnd = GetNbLines(); i < iEnd ; i++)
      SetCurveInverted(i,false);
  }
      
  return this;
}


//---------------------------------------------------------------
void
VISU::CutLinesBase_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myNbLines",      GetNbLines() );

  Storable::DataToStream( theStr, "myUseAbsLength", IsUseAbsoluteLength());

  // Storing the map - \a myMapCurvesInverted
  QString aStrCurvesInverted;
  for(int i = 0, iEnd = GetNbLines(); i < iEnd; i++) 
    aStrCurvesInverted.append(QString::number(IsCurveInverted(i)) + "|");
  Storable::DataToStream( theStr, "myMapCurvesInverted", (const char*)aStrCurvesInverted.toLatin1());
}


//---------------------------------------------------------------
VISU::CutLinesBase_i
::~CutLinesBase_i()
{
  if(MYDEBUG) MESSAGE("CutLinesBase_i::~CutLinesBase_i()");
}


//----------------------------------------------------------------------------^
VISU_Actor*
VISU::CutLinesBase_i
::CreateActor()
{
  VISU_Actor* anActor = TSuperClass::CreateActor();
  if(VISU_ScalarMapAct* aScalarMapAct = dynamic_cast<VISU_ScalarMapAct*>(anActor)) {
    //rnv: To fix the bug IPAL22007 Cut Lines in VTK viewer are shown with square points on ends
    //rnv: set small size of the vertices for the "Cut Lines" and "Cut Segment" presentations.
    aScalarMapAct->Set0DElemSize(1.0);
  }
  return anActor;
}



//---------------------------------------------------------------
void
VISU::CutLinesBase_i
::SetNbLines(CORBA::Long theNb) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_CutLinesBasePL, int>
		   (GetSpecificPL(), &VISU_CutLinesBasePL::SetNbParts, theNb));
}

//---------------------------------------------------------------
CORBA::Long
VISU::CutLinesBase_i
::GetNbLines() 
{ 
  return myCutLinesBasePL->GetNbParts();
}

//---------------------------------------------------------------
/*! Invert all curves of corresponding table
 * see void VISU::CutLinesBase_i::SetCurveInverted(CORBA::Long theCurveNumber,CORBA::Boolean theInvert)
 * \param theInvert - Invert all curves, if value is TRUE, else not.
 */
void
VISU::CutLinesBase_i
::SetAllCurvesInverted(CORBA::Boolean theInvert)
{
  for(int i=0; i < GetNbLines(); i++)
    SetCurveInverted(i, theInvert);
}

//---------------------------------------------------------------
/*! Checks the orientation of all curves
 * \retval TRUE - if all curves are inverted, else FALSE
 */
CORBA::Boolean 
VISU::CutLinesBase_i
::IsAllCurvesInverted()
{
  for (int i=0; i<GetNbLines(); i++)
    if (!IsCurveInverted(i)) return false;
  return true;
}

//---------------------------------------------------------------
/*! Sets orientation of curve
 * \param theCurveNumber - integer value, number of cut line.
 * \param theInvert      - boolean value, TRUE or false.
 */
void
VISU::CutLinesBase_i
::SetCurveInverted(CORBA::Long theCurveNumber, CORBA::Boolean theInvert)
{
  if(myMapCurvesInverted[theCurveNumber] == theInvert)
    return;

  VISU::TSetModified aModified(this);

  myMapCurvesInverted[theCurveNumber] = theInvert;
  myParamsTime.Modified();
}

//---------------------------------------------------------------
/*! Checks orientation of curve.
 * \param theCurveNumber - integer value, number of cut line.
 * \retval TRUE - if line in the table is inverted, else FALSE.
 */
CORBA::Boolean
VISU::CutLinesBase_i
::IsCurveInverted(CORBA::Long theCurveNumber)
{
  return myMapCurvesInverted[theCurveNumber];
}

//---------------------------------------------------------------
/*! It control the way the length of cutlines is shown: using aboslute or relative values
* \param theAbsLength - boolean value, TRUE or false.
*/
void
VISU::CutLinesBase_i
::SetUseAbsoluteLength(CORBA::Boolean theAbsLength)
{
  if ( myUseAbsLength == theAbsLength )
    return;

  VISU::TSetModified aModified(this);

  myUseAbsLength = theAbsLength;
  myParamsTime.Modified();
}

//---------------------------------------------------------------
CORBA::Boolean
VISU::CutLinesBase_i
::IsUseAbsoluteLength()
{
  return myUseAbsLength;
}


//---------------------------------------------------------------
void
VISU::CutLinesBase_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  // temporarily commented (see note 0006576 of the external issue 0020468)
  //myCutLinesBasePL = dynamic_cast<VISU_CutLinesBasePL*>(thePipeLine);
  myCutLinesBasePL = (VISU_CutLinesBasePL*)thePipeLine;

  TSuperClass::CreatePipeLine(myCutLinesBasePL);
}


//---------------------------------------------------------------
void
VISU::CutLinesBase_i
::BuildTableOfReal(SALOMEDS::SObject_var theSObject, bool theIsCutSegment)
{
  try{
    if(MYDEBUG) MESSAGE("CutPlanes_i::BuildTableOfReal");
    Update();
    SALOMEDS::GenericAttribute_var anAttr;
    SALOMEDS::StudyBuilder_var aStudyBuilder = GetStudyDocument()->NewBuilder();
    anAttr = aStudyBuilder->FindOrCreateAttribute(theSObject, "AttributeTableOfReal");
    SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);
    
    typedef set<double> TXCont;
    typedef map<double,double> TXYMap;
    typedef map<int,TXYMap> TXYMapCont;
    typedef map<long,long> TLineIdCont;
    
    QString aTitle( GetScalarBarTitle().c_str() );
    aTitle = aTitle.simplified();
    aTableOfReal->SetTitle( aTitle.toLatin1().data() );
    
    int iLineEnd = myCutLinesBasePL->GetAppendPolyData()->GetNumberOfInputConnections(0);
    if(MYDEBUG) MESSAGE("CutPlanes_i::BuildTableOfReal iLineEnd = "<<iLineEnd);
    TXCont aXCont;
    TXYMapCont aXYMapCont; 
    TLineIdCont aLineIdCont;  // Define internal numeration of lines
    const double *aDirLn = myCutLinesBasePL->GetDirLn();
    const double *aBasePnt = myCutLinesBasePL->GetBasePnt();
    const double *aBoundPrjLn = myCutLinesBasePL->GetBoundPrjLn();
    for(int iLine = 0, jLine = 0; iLine < iLineEnd; iLine++){
      vtkDataSet *aDataSet = myCutLinesBasePL->GetAppendPolyData()->GetInput(iLine);
      myCutLinesBasePL->GetAppendPolyData()->Update();
      int aNbPoints = aDataSet->GetNumberOfPoints();
      if(!aNbPoints) continue;
      vtkPointData *aPointData = aDataSet->GetPointData();
      vtkDataArray *aScalars = aPointData->GetScalars();
      VISU_CellDataToPointData *aFilter = NULL;
      if(!aScalars) {
        aFilter = VISU_CellDataToPointData::New();
        aFilter->SetInputData(aDataSet);
        aFilter->PassCellDataOn();
        aDataSet = aFilter->GetOutput();
        aFilter->Update();
      }
      aPointData = aDataSet->GetPointData();
      aScalars = aPointData->GetScalars();
      if(!aScalars) continue;
      if(MYDEBUG) MESSAGE("CutPlanes_i::BuildTableOfReal iLine = "<<iLine<<"; aNbPoints = "<<aNbPoints);
      aLineIdCont[iLine] = jLine++;
      TXYMap& aXYMap = aXYMapCont[iLine];
      double aPnt[3], aVect[3], aDist, aTopBnd, aDivide;
      aTopBnd = aBoundPrjLn[2];
      aDivide = aBoundPrjLn[2];
      if( !IsUseAbsoluteLength() ){
        aTopBnd = 1.0;
      }
      else aDivide = 1.0;

      for(int i = 0; i < aNbPoints; i++){
        aDataSet->GetPoint(i,aPnt);
        for(int j = 0; j < 3; j++)
          aVect[j] = aPnt[j] - aBasePnt[j];
        //VISU::Sub(aPnt,aBasePnt,aVect);
        if ( fabs(aBoundPrjLn[2]) < EPS_machine )
          aDist = 0.5;
        else
        {
          aDist = vtkMath::Dot(aVect,aDirLn)/aDivide; 
          // the workaround
          if(aDist < 0.0) aDist = 0.0; 
          if(aDist > aTopBnd) aDist = aTopBnd;
        }
        aXYMap[aDist] = aScalars->GetTuple1(i);
      }
      if(aFilter)
        aFilter->Delete();
    }
    if(aXYMapCont.size() == 0)
      throw std::runtime_error("CutPlanes_i::BuildTableOfReal aXYMapCont.size() == 0 !!!");

    {
      // Invertion all curves in the table, which has inversion flag is TRUE (see \a myMapCurvesInverted)
      for(int iLine=0; iLine < iLineEnd; iLine++){
        if (!IsCurveInverted(iLine)) continue;
        TXYMap aNewXYMap;
        TXYMap& aXYMap = aXYMapCont[iLine];
        TXYMap::const_iterator aXYMapIter = aXYMap.begin();
        std::list<double> XKeys;
        for (;aXYMapIter != aXYMap.end() ; aXYMapIter++) XKeys.push_back(aXYMapIter->first);
        XKeys.sort();
        if (XKeys.size() > 1) {
          double a_first_indx = XKeys.front();
          double a_last_indx = XKeys.back();
          if (a_first_indx > a_last_indx){
            XKeys.reverse();
            double tmp = a_first_indx;
            a_first_indx = a_last_indx;
            a_last_indx = tmp;
          }
          std::list<double>::const_iterator aIter = XKeys.begin();
          for (int k=0;k<XKeys.size() && aIter != XKeys.end();k++,aIter++){
            // Warning: value '1.0' come from workaround:
            // see also aDist = vtkMath::Dot(aVect,aDirLn) / aBoundPrjLn[2];
            // aDist >= 0 and aDist<=1.0
            double aTopBnd = aBoundPrjLn[2];
            if( !IsUseAbsoluteLength() ){
              aTopBnd = 1.0;
            }
            aNewXYMap[aTopBnd - *aIter] = aXYMap[*aIter];
          }
          TXYMap::const_iterator aNewXYMapIter = aNewXYMap.begin();
          aXYMap.clear();
          for (;aNewXYMapIter != aNewXYMap.end();aNewXYMapIter++) {
            aXYMap[aNewXYMapIter->first] = aNewXYMapIter->second;
          }
        }
      }
    }
    //Resorting of theXYMap
    TXYMapCont::iterator aXYMapContIter = aXYMapCont.begin();
    for(; aXYMapContIter != aXYMapCont.end(); aXYMapContIter++){
      TXYMap& aXYMap = aXYMapContIter->second, aNewXYMap;
      if(aXYMap.size() > 2){  
        // Try to smooth the values of the line by applying linear approximation
        TXYMap::const_iterator aXYMapIter[2] = {aXYMap.begin(), ++aXYMap.begin()};
        aNewXYMap[aXYMapIter[0]->first] = aXYMapIter[0]->second;
        aXCont.insert(aXYMapIter[0]->first);
        for(; aXYMapIter[1] != aXYMap.end(); aXYMapIter[0]++, aXYMapIter[1]++){
          double aY[3] = {aXYMapIter[0]->second, aXYMapIter[1]->second, 0.0};
          aY[2] = (aY[0] + aY[1])/2.0;
          double aX[3] = {aXYMapIter[0]->first, aXYMapIter[1]->first, 0.0};
          aX[2] = (aX[0] + aX[1])/2.0;
          aNewXYMap[aX[2]] = aY[2];
          aXCont.insert(aX[2]);
        }
        aNewXYMap[aXYMapIter[0]->first] = aXYMapIter[0]->second;
        aXCont.insert(aXYMapIter[0]->first);
        aXYMap = aNewXYMap;
      }else{
        TXYMap::const_iterator aXYMapIter = aXYMap.begin();
        for(; aXYMapIter != aXYMap.end(); aXYMapIter++)
          aXCont.insert(aXYMapIter->first);
      }
    }
    if(aXCont.size() == 0)
      throw std::runtime_error("CutPlanes_i::BuildTableOfReal aXCont.size() == 0 !!!");
    QString aString;
    int iEnd = aXCont.size();
    aTableOfReal->SetNbColumns(iEnd);
    TXCont::const_iterator aXContIter = aXCont.begin();
    double aMinDist = *aXContIter;
    aXContIter = aXCont.end();
    aXContIter--;
    double aMaxDist = *aXContIter;
    if(aMaxDist < aMinDist)
      throw std::runtime_error("CutPlanes_i::BuildTableOfReal aMaxDist < aMinDist !!!");
    aXContIter = aXCont.begin();
    for(long i = 0; aXContIter != aXCont.end(); aXContIter++, i++){
      double aDist = *aXContIter; 
      double aXVal = aDist;
      if(theIsCutSegment){
        aXVal -= aMinDist;
        if(!IsUseAbsoluteLength() && fabs(aMaxDist - aMinDist) > EPS_machine)
          aXVal /= (aMaxDist - aMinDist);
      }
      aTableOfReal->PutValue(aXVal,1,i+1);
      aString.sprintf("%d",i);
      aTableOfReal->SetColumnTitle(i+1,(const char*)aString.toLatin1());
      if(MYDEBUG) MESSAGE("CutPlanes_i::BuildTableOfReal aDist = "<<aDist<<" aXVal = "<<aXVal);
      TXYMapCont::const_iterator aXYMapContIter = aXYMapCont.begin();
      for(; aXYMapContIter != aXYMapCont.end(); aXYMapContIter++){
        long iLine = aXYMapContIter->first;
        long iLineId = aLineIdCont[iLine];
        const TXYMap& aXYMap = aXYMapCont[iLine];
        TXYMap::const_iterator aXYMapIter = aXYMap.find(aDist);
        // Can we find some value that belong to the line and have the same X coordinate?
        if(aXYMapIter == aXYMap.end()) continue;
        double aVal = aXYMapIter->second;
        aTableOfReal->PutValue(aVal,iLineId+2,i+1);
      }
    }
    {
      aTableOfReal->SetRowTitle(1,"X");
      aTableOfReal->SetRowUnit(1,"-");
      QString aUnitName = GetField()->myUnitNames[0].c_str();
      int aScalarMode = myCutLinesBasePL->GetScalarMode();
      if(aScalarMode != 0) 
        aUnitName = GetField()->myUnitNames[aScalarMode-1].c_str();
      aUnitName = aUnitName.simplified();
      if(aUnitName.isEmpty()) aUnitName = "-";
      TXYMapCont::const_iterator aXYMapContIter = aXYMapCont.begin();
      for(; aXYMapContIter != aXYMapCont.end(); aXYMapContIter++){
        long iLine = aXYMapContIter->first;
        long iLineId = aLineIdCont[iLine];
        aString.sprintf("Y%d",iLine);
        if(MYDEBUG) 
          MESSAGE("CutPlanes_i::BuildTableOfReal - SetRowTitle("<<iLineId+2<<",'"<<(const char*)aString.toLatin1()<<"')");
        aTableOfReal->SetRowTitle(iLineId+2,(const char*)aString.toLatin1());
        aTableOfReal->SetRowUnit(iLineId+2,(const char*)aUnitName.toLatin1());
      }
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch (...){
    INFOS("Unknown exception was occured !!!");
  }
}

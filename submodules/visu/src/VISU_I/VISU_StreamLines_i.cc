// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_StreamLines_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_Actor.h"
#include "VISU_StreamLinesPL.hxx"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <vtkDataSetMapper.h>
#include <vtkAppendFilter.h>
#include <vtkUnstructuredGrid.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
size_t
VISU::StreamLines_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  try{
    if(!TSuperClass::IsPossible(theResult, theMeshName, theEntity, theFieldName, theTimeStampNumber, false)) 
      return 0;

    VISU::Result_i::PInput anInput = theResult->GetInput(theMeshName,
							 theEntity,
							 theFieldName,
							 theTimeStampNumber);
    VISU::PUnstructuredGridIDMapper anIDMapper = 
      anInput->GetTimeStampOnMesh(theMeshName,
				  VISU::TEntity(theEntity),
				  theFieldName,
				  theTimeStampNumber);

    vtkUnstructuredGrid* aDataSet = anIDMapper->GetUnstructuredGridOutput();
    size_t aResult = VISU_StreamLinesPL::IsPossible(aDataSet);
    MESSAGE("StreamLines_i::IsPossible - aResult = "<<aResult);
    return aResult;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return 0;
}

//---------------------------------------------------------------
int VISU::StreamLines_i::myNbPresent = 0;

//---------------------------------------------------------------
QString
VISU::StreamLines_i
::GenerateName() 
{
  return VISU::GenerateName("StreamLines",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::StreamLines_i::myComment = "STREAMLINES";

//---------------------------------------------------------------
const char* 
VISU::StreamLines_i
::GetComment() const 
{ 
  return myComment.c_str();
}

//---------------------------------------------------------------
const char*
VISU::StreamLines_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_STREAM_LINES";
  else
    return "ICON_TREE_STREAM_LINES_GROUPS";
}

//---------------------------------------------------------------
VISU::StreamLines_i
::StreamLines_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  MonoColorPrs_i(thePublishInStudyMode),
  myStreamLinesPL(NULL),
  myAppendFilter(vtkAppendFilter::New())
{}


//---------------------------------------------------------------
void
VISU::StreamLines_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const StreamLines_i* aPrs3d = dynamic_cast<const StreamLines_i*>(theOrigin)) {
    StreamLines_i* anOrigin = const_cast<StreamLines_i*>(aPrs3d);
    SetSource(anOrigin->GetSource());
  }
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::StreamLines_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::StreamLines_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  double anIntegrationStep = VISU::Storable::FindValue(theMap,"myIntegrationStep").toDouble();
  double aPropagationTime = VISU::Storable::FindValue(theMap,"myPropagationTime").toDouble();
  double aStepLength = VISU::Storable::FindValue(theMap,"myStepLength").toDouble();
  int aDirection = VISU::StreamLines::Direction(VISU::Storable::FindValue(theMap,"myDirection").toInt());
  double aPercents = VISU::Storable::FindValue(theMap,"myPercents").toDouble();
  SetParams(anIntegrationStep,
	    aPropagationTime,
	    aStepLength,
	    VISU::Prs3d::_nil(),
	    aPercents,
	    VISU::StreamLines::Direction(aDirection));
  mySourceEntry = (const char*)VISU::Storable::FindValue(theMap,"mySourceEntry").toLatin1();

  return this;
}


//---------------------------------------------------------------
void
VISU::StreamLines_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myIntegrationStep", GetIntegrationStep());
  Storable::DataToStream( theStr, "myPropagationTime", GetPropagationTime());
  Storable::DataToStream( theStr, "myStepLength", GetStepLength());

  Storable::DataToStream( theStr, "myDirection", int(GetDirection()));
  Storable::DataToStream( theStr, "myPercents", GetUsedPoints());

  Storable::DataToStream( theStr, "mySourceEntry", mySourceEntry.c_str());
}


//---------------------------------------------------------------
VISU::StreamLines_i
::~StreamLines_i()
{
  if(MYDEBUG) MESSAGE("StreamLines_i::~StreamLines_i()");
  myAppendFilter->Delete();
}


//---------------------------------------------------------------
CORBA::Boolean
VISU::StreamLines_i
::SetParams(CORBA::Double theIntStep,
	    CORBA::Double thePropogationTime,
	    CORBA::Double theStepLength,
	    VISU::Prs3d_ptr thePrs3d,
	    CORBA::Double thePercents,
	    VISU::StreamLines::Direction theDirection)
{
  struct TEvent: public SALOME_Event 
  {
    CORBA::Double myIntStep;
    CORBA::Double myPropogationTime;
    CORBA::Double myStepLength;
    VISU::Prs3d_ptr myPrs3d;
    CORBA::Double myPercents;
    VISU::StreamLines::Direction myDirection;
    
    VISU_StreamLinesPL* myPipeLine;
    VISU::Prs3d_i*& myPrs3dServant;
    vtkAppendFilter* myAppendFilter;
    size_t& myIsAccepted;
    
    TEvent(CORBA::Double theIntStep,
	   CORBA::Double thePropogationTime,
	   CORBA::Double theStepLength,
	   VISU::Prs3d_ptr thePrs3d,
	   CORBA::Double thePercents,
	   VISU::StreamLines::Direction theDirection,
	   VISU_StreamLinesPL* thePipeLine,
	   VISU::Prs3d_i*& thePrs3dServant,
	   vtkAppendFilter* theAppendFilter,
	   size_t& theIsAccepted):
      myIntStep(theIntStep),
      myPropogationTime(thePropogationTime),
      myStepLength(theStepLength),
      myPrs3d(thePrs3d),
      myPercents(thePercents),
      myDirection(theDirection),
      myPipeLine(thePipeLine),
      myPrs3dServant(thePrs3dServant),
      myAppendFilter(theAppendFilter),
      myIsAccepted(theIsAccepted)
    {}

    virtual
    void
    Execute()
    {
      myPrs3dServant = NULL;
      vtkPointSet* aSource = NULL;
      if (!CORBA::is_nil(myPrs3d)){
	myPrs3dServant = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(myPrs3d).in());
	if(myPrs3dServant){
	  myAppendFilter->RemoveAllInputs();
	  myAppendFilter->AddInputData(myPrs3dServant->GetPipeLine()->GetMapper()->GetInput());
	  aSource = myAppendFilter->GetOutput();
          myAppendFilter->Update();
	}
      }
      myIsAccepted = myPipeLine->SetParams(myIntStep,
					   myPropogationTime,
					   myStepLength,
					   aSource,
					   myPercents,
					   myDirection);
    }
  };

  size_t anIsAccepted = false;
  VISU::Prs3d_i* aPrs3dServant = NULL;
  ProcessVoidEvent(new TEvent(theIntStep,
			      thePropogationTime,
			      theStepLength,
			      thePrs3d,
			      thePercents,
			      theDirection,
			      GetSpecificPL(),
			      aPrs3dServant,
			      myAppendFilter,
			      anIsAccepted));
  if(anIsAccepted)
    SetSource(aPrs3dServant);

  return anIsAccepted;
}


//---------------------------------------------------------------
void
VISU::StreamLines_i
::SetSource(VISU::Prs3d_ptr thePrs3d)
{
  if(!thePrs3d->_is_nil()){
    VISU::Prs3d_i* aPrs3di = dynamic_cast<VISU::Prs3d_i*>(VISU::GetServant(thePrs3d).in());
    SetSource(aPrs3di);
  }
}

//---------------------------------------------------------------
void
VISU::StreamLines_i
::SetSource(VISU::Prs3d_i* thePrs3d)
{
  mySourceEntry = "";
  if(!thePrs3d)
    return;

  SALOMEDS::SObject_var aSObject = thePrs3d->GetSObject();
  CORBA::String_var aString = aSObject->GetID();
  if(mySourceEntry == aString.in())
    return;
  
  VISU::TSetModified aModified(this);
  
  mySourceEntry = aString.in();
  myParamsTime.Modified();
}

//---------------------------------------------------------------
void
VISU::StreamLines_i
::SetSource()
{
  if(!myStreamLinesPL->GetSource() && mySourceEntry == "") 
    return;

  if(myStreamLinesPL->GetSource() == myAppendFilter->GetOutput()) 
    return;

  VISU::Prs3d_var aPrs3d = GetSource();
  SetParams(GetIntegrationStep(),
	    GetPropagationTime(),
	    GetStepLength(),
	    aPrs3d,
	    GetUsedPoints(),
	    GetDirection());
}


//---------------------------------------------------------------
CORBA::Double 
VISU::StreamLines_i
::GetIntegrationStep() 
{ 
  return myStreamLinesPL->GetIntegrationStep();
}

//---------------------------------------------------------------
CORBA::Double
VISU::StreamLines_i
::GetPropagationTime() 
{ 
  return myStreamLinesPL->GetPropagationTime();
}

//---------------------------------------------------------------
CORBA::Double
VISU::StreamLines_i
::GetStepLength() 
{ 
  return myStreamLinesPL->GetStepLength();
}

//---------------------------------------------------------------
VISU::StreamLines::Direction
VISU::StreamLines_i
::GetDirection() 
{ 
  return VISU::StreamLines::Direction(myStreamLinesPL->GetDirection());
}


//---------------------------------------------------------------
VISU::Prs3d_ptr
VISU::StreamLines_i
::GetSource()
{
  VISU::Prs3d_var aPrs3d;
  if(MYDEBUG) MESSAGE("StreamLines_i::GetSource() mySourceEntry = '"<<mySourceEntry<<"'");
  if(mySourceEntry != ""){
    SALOMEDS::SObject_var aSObject = GetStudyDocument()->FindObjectID(mySourceEntry.c_str());
    CORBA::Object_var anObj = SObjectToObject(aSObject);
    if(!CORBA::is_nil(anObj)) aPrs3d = VISU::Prs3d::_narrow(anObj);
  }
  return aPrs3d._retn();
}

//---------------------------------------------------------------
CORBA::Double 
VISU::StreamLines_i
::GetUsedPoints() 
{ 
  return myStreamLinesPL->GetUsedPoints();
}


//---------------------------------------------------------------
void 
VISU::StreamLines_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myStreamLinesPL = VISU_StreamLinesPL::New();
  }else
    myStreamLinesPL = dynamic_cast<VISU_StreamLinesPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myStreamLinesPL);
}


//----------------------------------------------------------------------------
bool
VISU::StreamLines_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

//---------------------------------------------------------------
void
VISU::StreamLines_i
::Update() 
{
  SetSource();
  TSuperClass::Update();
}


//---------------------------------------------------------------
VISU_Actor* 
VISU::StreamLines_i
::CreateActor() 
{
  if(VISU_Actor* anActor = TSuperClass::CreateActor(true)){
    anActor->SetVTKMapping(true);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int  aDispMode = aResourceMgr->integerValue("VISU", "stream_lines_represent", 1);
    anActor->SetRepresentation(aDispMode);
    return anActor;
  }
  return NULL;
}


//---------------------------------------------------------------
void
VISU::StreamLines_i
::UpdateActor(VISU_Actor* theActor) 
{
  TSuperClass::UpdateActor(theActor);
}

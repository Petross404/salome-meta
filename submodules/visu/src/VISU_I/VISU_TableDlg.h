// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_TableDlg.h
//  Author : Vadim SANDLER
//  Module : VISU
//
#ifndef VISU_TABLEDLG_H
#define VISU_TABLEDLG_H

#include "VISU_I.hxx"

#include <SALOMEDSClient_Study.hxx>

#include <QDialog>
#include <QItemDelegate>
#include <QMap>

class QLabel;
class QLineEdit;
class QPushButton;
class QCheckBox;
class QComboBox;
class QTableWidget;

namespace VISU
{
  class Table_i;
}

class VISU_I_EXPORT VISU_TableDlg : public QDialog
{ 
  Q_OBJECT

  class TableWidget;
  class WidgetCointainer;

  enum { tcTitle, tcTable, tcControls, tcButtons };

public:

  enum { ttNone, ttInt, ttReal, ttAll, ttAuto };
  enum { teNone      = 0x00,
         teAddRemove = 0x01,
         teEditData  = 0x02,
         teRowColumn = 0x04,
         teAll       = teAddRemove | teEditData | teRowColumn
  };

  VISU_TableDlg( QWidget* parent, 
                 _PTR(Study) study, 
                 VISU::Table_i* table,
                 bool allowEdition = false,
                 int which = ttAuto, 
                 Qt::Orientation orient = Qt::Horizontal,
                 bool showColumnTitles = true );
  ~VISU_TableDlg();

protected:
  void keyPressEvent( QKeyEvent* e );

private slots:
  void help(); 

private:
  QString tableTitle( int type );

private:
  typedef QMap<int, TableWidget*>      TableMap;
  typedef QMap<int, WidgetCointainer*> ContMap;

  TableMap               myTableMap;
  ContMap                myContMap;
  QCheckBox*             myEditCheck;
  QCheckBox*             myUpdateCheck;
  QPushButton*           myOKBtn;
  QPushButton*           myHelpBtn;

  _PTR(Study)            myStudy;
  VISU::Table_i*         myTable;
};
 
class VISU_I_EXPORT VISU_TableDlg::TableWidget : public QWidget
{
  Q_OBJECT

public:
  TableWidget( QWidget* parent = 0, Qt::Orientation orientation = Qt::Horizontal );
  ~TableWidget();

  void    initialize( _PTR(Study) study, VISU::Table_i* table, int type );

  void    setTableTitle( const QString& title );
  QString getTableTitle();
  void    setNumRows( const int num );
  int     getNumRows();
  void    setNumCols( const int num );
  int     getNumCols();
  void    setRowTitles( QStringList& tlts );
  void    getRowTitles( QStringList& tlts );
  void    setColTitles( QStringList& tlts );
  void    getColTitles( QStringList& tlts );
  void    setUnitsTitle( const QString& tlt );
  void    setUnits( QStringList& units );
  void    getUnits( QStringList& units );
  void    setRowData( int row, QStringList& data );
  void    getRowData( int row, QStringList& data );

  bool    eventFilter( QObject* o, QEvent* e);

public slots:
  void    setEditEnabled( bool enable );
  void    showColumnTitles( bool showTitles );

  void    updateButtonsState();
  void    addRow();
  void    addCol();
  void    delRow();
  void    delCol();
  void    adjustTable();
  void    selectAll();
  void    clearTable();

  void    columnClicked( int );
  void    rowClicked( int );

protected:
  void    updateTableFromServant( bool adjust );

private:
  _PTR(Study)            myStudy;
  VISU::Table_i*         myTableObj;
  _PTR(GenericAttribute) myAttr;
  int                    myType;

  QLineEdit*       myTitleEdit;
  QTableWidget*    myTable;
  QPushButton*     myAddRowBtn;
  QPushButton*     myAddColBtn;
  QPushButton*     myDelRowBtn;
  QPushButton*     myDelColBtn;
  QPushButton*     myAdjustBtn;
  QPushButton*     mySelectAllBtn;
  QPushButton*     myClearBtn;
  QLabel*          mySortPolicyLabel;
  QComboBox*       mySortPolicyCombo;
  Qt::Orientation  myOrientation;
};

class VISU_I_EXPORT NumDelegateItem: public QItemDelegate
{
public:
  enum NumValidator{ NV_Int, NV_Real };

  NumDelegateItem( QObject* parent, NumValidator mode = NV_Int );
  virtual ~NumDelegateItem();

  virtual QWidget* createEditor( QWidget* parent,
                                 const QStyleOptionViewItem& option,
                                 const QModelIndex& index ) const;
  virtual void     setEditorData( QWidget* editor,
                                  const QModelIndex& index ) const;
private:
  int myMode;
};

#endif // VISU_TABLEDLG_H


// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_ResultUtils.hh
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef __VISU_RESULT_UTILS_H__
#define __VISU_RESULT_UTILS_H__

#include "VISU_Result_i.hh"
#include "SALOME_Event.h"

#include <set>

namespace VISU
{
  //----------------------------------------------------------------------------
  struct TUpdateObjBrowser: public SALOME_Event
  {
    int myStudyId;
    CORBA::Boolean* myIsDone;
    TUpdateObjBrowser(const int theStudyId, CORBA::Boolean* theIsDone);

    virtual
    void
    Execute();
  };


  //----------------------------------------------------------------------------
  struct TResultManager
  {
    Result_i* myResult;

    TResultManager(Result_i* theResult);

    ~TResultManager();
  };


  //----------------------------------------------------------------------------
  struct TTransactionManager
  {
    _PTR(StudyBuilder) myStudyBuilder;

    TTransactionManager(_PTR(Study) theStudyDocument);

    ~TTransactionManager();
  };


  //----------------------------------------------------------------------------
  QString
  GenerateName(const std::string& theName);


  //----------------------------------------------------------------------------
  bool
  SplitName(const std::string& theName,
	    std::string& theBase,
	    std::string& theSuffix,
	    char theDelimeter = ':');


  //----------------------------------------------------------------------------
  std::string
  MakeFileName(const std::string& theName,
	       const void* thePointer);
    
    
  //----------------------------------------------------------------------------
  void
  BuildEntities(Result_i* theResult,
		Result_i::PInput theInput,
		CORBA::Boolean* theIsDone,
		const std::string& theResultEntry,
		CORBA::Boolean theIsAtOnce,
		CORBA::Boolean theIsBuildGroups,
		CORBA::Boolean theIsBuildFields,
		CORBA::Boolean theIsBuildParts,
		_PTR(Study) theStudy);

  
  //----------------------------------------------------------------------------
  void
  BuildGroups(Result_i* theResult,
	      Result_i::PInput theInput,
	      CORBA::Boolean* theIsDone,
	      CORBA::Boolean theIsBuild,
	      CORBA::Boolean theIsAtOnce,
	      _PTR(Study) theStudy);

  
  //----------------------------------------------------------------------------
  void
  BuildFields(Result_i* theResult,
	      Result_i::PInput theInput,
	      CORBA::Boolean* theIsDone,
	      CORBA::Boolean theIsBuild,
	      CORBA::Boolean theIsAtOnce,
	      _PTR(Study) theStudy);

  
  //----------------------------------------------------------------------------
  void
  BuildMinMax(Result_i* theResult,
	      Result_i::PInput theInput,
	      CORBA::Boolean* theIsDone,
	      CORBA::Boolean theIsBuild);


  //----------------------------------------------------------------------------
  void
  BuildFieldDataTree(Result_i* theResult,
		     Result_i::PInput theInput,
		     CORBA::Boolean* theIsFieldsDone,
		     CORBA::Boolean theIsBuildFields,
		     CORBA::Boolean* theIsMinMaxDone,
		     CORBA::Boolean theIsBuildMinMax,
		     _PTR(Study) theStudy);

  //----------------------------------------------------------------------------
  bool
  RemoveFile(const std::string& theFileName,
	     bool theRemoveEmptyDir = true);

  //----------------------------------------------------------------------------
  bool
  CopyFile(const std::string& theSourceFileName,
	   const std::string& theTargetFileName);

  //----------------------------------------------------------------------------
}


#endif // __VISU_RESULT_UTILS_H__

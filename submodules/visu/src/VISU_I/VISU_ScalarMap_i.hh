// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_ScalarMap_i_HeaderFile
#define VISU_ScalarMap_i_HeaderFile

#include "VISU_ColoredPrs3d_i.hh"

#include <string>
#include <set>

class VISU_ScalarMapPL;

namespace VISU
{
  //============================================================================
  class VISU_I_EXPORT ScalarMap_i : public virtual POA_VISU::ScalarMap,
				    public virtual ColoredPrs3d_i
  {
    static int myNbPresent;
    ScalarMap_i(const ScalarMap_i&);

  public:
	//----------------------------------------------------------------------------
    typedef ColoredPrs3d_i TSuperClass;
    typedef VISU::ScalarMap TInterface;

    explicit
    ScalarMap_i(EPublishInStudyMode thePublishInStudyModep);

    virtual
    ~ScalarMap_i();

    virtual
    VISU::VISUType 
    GetType() 
    { 
      return VISU::TSCALARMAP;
    }

    //----------------------------------------------------------------------------
    //! Gets memory size actually used by the presentation (Mb).
    virtual
    CORBA::Float
    GetMemorySize();

    //----------------------------------------------------------------------------
    virtual 
    VISU::Scaling 
    GetScaling();

    virtual
    void
    SetScaling(VISU::Scaling theScaling);

    //----------------------------------------------------------------------------
    virtual 
    void 
    SetLinkColor(const SALOMEDS::Color& theColor);

    virtual 
    SALOMEDS::Color 
    GetLinkColor();

    //----------------------------------------------------------------------------
    virtual 
    VISU::GaussMetric 
    GetGaussMetric();

    virtual
    void
    SetGaussMetric(VISU::GaussMetric theGaussMetric);

    //----------------------------------------------------------------------------
    //! Sets initial source geometry
    virtual
    void
    SetSourceGeometry();

    //! Add geometry of mesh as group. \retval the id of added group.
    virtual 
    void
    AddMeshOnGroup(const char* theGroupName);
    
    //! Removes all geometries.
    virtual
    void
    RemoveAllGeom();

    //----------------------------------------------------------------------------
    VISU_ScalarMapPL* 
    GetSpecificPL() const
    { 
      return myScalarMapPL; 
    }

    virtual CORBA::Boolean IsBarVisible() { return myShowBar; }

    virtual void SetBarVisible(CORBA::Boolean theVisible);
    
  protected:
    //! Redefines VISU_ColoredPrs3d_i::DoSetInput
    virtual 
    void
    DoSetInput(bool theIsInitilizePipe, bool theReInit);

    //! Redefines VISU_ColoredPrs3d_i::CreatePipeLine
    virtual
    void
    CreatePipeLine(VISU_PipeLine* thePipeLine);

    //! Redefines VISU_ColoredPrs3d_i::CheckIsPossible
    virtual 
    bool 
    CheckIsPossible();

  public:
    /*!
      Checks staticaly, whether it is possible to create presentation 
      with the given basic parameters or not.
    */
    static
    size_t
    IsPossible(Result_i* theResult, 
	       const std::string& theMeshName, 
	       VISU::Entity theEntity,
	       const std::string& theFieldName, 
	       CORBA::Long theTimeStampNumber,
	       bool theIsMemoryCheck);

    //! Redefines VISU_ColoredPrs3d_i::Create
    virtual
    Storable* 
    Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber);

    virtual 
    void
    ToStream(std::ostringstream& theStr);

    static const std::string myComment;

    virtual
    const char* 
    GetComment() const;

    virtual
    QString
    GenerateName();

    virtual
    const char* 
    GetIconName();

    void
    UpdateIcon();

    virtual 
    Storable* 
    Restore(SALOMEDS::SObject_ptr theSObject,
	    const Storable::TRestoringMap& theMap);

    virtual 
    void
    SameAs(const Prs3d_i* theOrigin);

    virtual 
    VISU_Actor* 
    CreateActor();
    
    virtual 
    VISU_Actor* 
    CreateActor(bool toSupressShrinking);

    virtual
    void
    UpdateActor(VISU_ActorBase* theActor);

    virtual
    double
    GetComponentMin(vtkIdType theCompID);

    virtual
    double
    GetComponentMax(vtkIdType theCompID);

  private:
    VISU_ScalarMapPL* myScalarMapPL;
    SALOMEDS::Color myLinkColor;
    bool myShowBar;
  };
}

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_CutLinesPL.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VTKViewer_GeometryFilter.h"

#include <vtkAppendPolyData.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_CutLinesPL);


//----------------------------------------------------------------------------
VISU_CutLinesPL
::VISU_CutLinesPL()
{
  myCondition = 1;
  myPosition = 0;
}


//----------------------------------------------------------------------------
void
VISU_CutLinesPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_CutLinesPL *aPipeLine = dynamic_cast<VISU_CutLinesPL*>(thePipeLine)){
    SetOrientation(aPipeLine->GetPlaneOrientation(1),
                   aPipeLine->GetRotateX(1),aPipeLine->GetRotateY(1),1);
    SetDisplacement(aPipeLine->GetDisplacement(1),1);
    if (aPipeLine->IsDefault())
      SetDefault();
    else
      SetPosition(aPipeLine->GetPosition());
  }
}


//----------------------------------------------------------------------------
void
VISU_CutLinesPL
::Init()
{
  Superclass::Init();

  myBasePlane[0] = XY;
  myBasePlane[1] = YZ;
  myDisplacement[0] = myDisplacement[1] = 0.5;
  myAng[0][0] = myAng[0][1] = myAng[0][2] = 0.0;
  myAng[1][0] = myAng[1][1] = myAng[1][2] = 0.0;

  myCondition = 1;
  myPosition = 0;
}


//----------------------------------------------------------------------------
void
VISU_CutLinesPL
::SetPosition(double thePosition)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myPosition, thePosition);
  anIsSameValue &= (myCondition == 0);
  if(anIsSameValue)
    return;

  myPosition = thePosition;
  myCondition = 0;
  Modified();
}

vtkAlgorithmOutput* 
VISU_CutLinesPL
::InsertCustomPL()
{
  return myAppendPolyData->GetOutputPort();
}

//----------------------------------------------------------------------------
double 
VISU_CutLinesPL
::GetPosition()
{
  double aPosition = myPosition;
  if(myCondition){
      double aBounds[6];
      GetMergedInput()->GetBounds(aBounds);

      double aDir[3];
      GetDir(aDir,
             myAng[0],
             myBasePlane[0]);

      double aBoundPrj[3];
      GetBoundProject(aBoundPrj,
                      aBounds,
                      aDir);

      aPosition = aBoundPrj[0] + aBoundPrj[2] * myDisplacement[0];
  }
  return aPosition;
}


//----------------------------------------------------------------------------
void
VISU_CutLinesPL
::SetDefault()
{
  if(myCondition == 1)
    return;

  myCondition = 1;
  Modified();
}


//----------------------------------------------------------------------------
int
VISU_CutLinesPL
::IsDefault()
{
  return myCondition;
}


//----------------------------------------------------------------------------
void
VISU_CutLinesPL
::Update()
{
  vtkDataSet* aMergedInput = GetMergedInput();
  if(VISU::IsQuadraticData(aMergedInput)) // Bug 0020123, note 0005343
    throw std::runtime_error("Impossible to build presentation");

  ClearAppendPolyData(myAppendPolyData);

  SetPartPosition(1);

  vtkAppendPolyData *anAppendPolyData = vtkAppendPolyData::New();

  //Build base plane
  double aBaseBounds[6];
  GetMergedInput()->GetBounds(aBaseBounds);

  double aDir[2][3];
  GetDir(aDir[0],
         myAng[0],
         myBasePlane[0]);

  CutWithPlanes(anAppendPolyData,
                GetMergedInput(),
                1,
                aDir[0],
                aBaseBounds,
                myPosition,
                myCondition,
                myDisplacement[0]);
  //Build lines
  vtkDataSet *aDataSet = anAppendPolyData->GetOutput();
  anAppendPolyData->Update();

  if(aDataSet->GetNumberOfCells() == 0)
    aDataSet = GetMergedInput();

  double aBounds[6];
  aDataSet->GetBounds(aBounds);

  GetDir(aDir[1],
         myAng[1],
         myBasePlane[1]);

  VISU_CutPlanesPL::CutWithPlanes(myAppendPolyData,
                                  aDataSet,
                                  GetNbParts(),
                                  aDir[1],
                                  aBounds,
                                  myPartPosition,
                                  myPartCondition,
                                  myDisplacement[1]);
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myAppendPolyData.vtk";
  //  VISU::WriteToFile(myAppendPolyData->GetOutput(), aFileName);
  //}
  anAppendPolyData->Delete();

  //Calculate values for building of table
  vtkMath::Cross(aDir[0],aDir[1],myDirLn);
  for (int i = 0; i<3 ; i++) {
    myRealDirLn[i] = myDirLn[i];
    if(myDirLn[i] < 0.0) 
      myDirLn[i] = -1.0*myDirLn[i];//enk:: correction of bug Bug PAL10401
  }

  GetBoundProject(myBoundPrjLn, 
                  aBaseBounds, 
                  myDirLn);

  VISU::Mul(myDirLn,
            myBoundPrjLn[0],
            myBasePnt);
  
  CorrectPnt(myBasePnt,
             aBaseBounds);

  VISU_ScalarMapPL::Update();
}


//----------------------------------------------------------------------------
void
VISU_CutLinesPL
::CutWithPlanes(vtkAppendPolyData* theAppendPolyData, 
                vtkDataSet* theDataSet,
                int theNbPlanes, 
                double theDir[3], 
                double theBounds[6],
                double thePartPosition, 
                int thePartCondition,
                double theDisplacement)
{
  std::vector<double> aPartPosition(1,thePartPosition);
  std::vector<int> aPartCondition(1,thePartCondition);
  VISU_CutPlanesPL::CutWithPlanes(theAppendPolyData,
                                  theDataSet,
                                  theNbPlanes,
                                  theDir,
                                  theBounds,
                                  aPartPosition,
                                  aPartCondition,
                                  theDisplacement);
}


//----------------------------------------------------------------------------

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : VISU_PlanesWidget.h
//  Author : Peter KURNEV
//  Module : SALOME
//  $Header$
//
#ifndef __VISU_ImplicitFunctionWidget_h
#define __VISU_ImplicitFunctionWidget_h

#include <vtkFollower.h>

#include "VISUPipeline.hxx"

#include "VTKViewer.h"

class VISU_UnScaledActor: public vtkFollower
{
  
public:
  
  vtkTypeMacro(VISU_UnScaledActor,vtkFollower);
  static VISU_UnScaledActor *New();
  
  void SetCenter(double *);
  virtual void SetSize(int theSize);
  virtual void Render(vtkRenderer *theRenderer);
  virtual double *GetBounds();

protected:
  VISU_UnScaledActor();
  ~VISU_UnScaledActor(){}

  double myCenter[3];
  int mySize;
};

#include <vtk3DWidget.h>

class vtkImplicitFunction;

class VISU_PIPELINE_EXPORT VISU_ImplicitFunctionWidget : public vtk3DWidget
{
public:
  vtkTypeMacro(VISU_ImplicitFunctionWidget,vtk3DWidget);

  virtual vtkImplicitFunction* ImplicitFunction()=0;
  
protected:
  VISU_ImplicitFunctionWidget();
  ~VISU_ImplicitFunctionWidget();

private:
  VISU_ImplicitFunctionWidget(const VISU_ImplicitFunctionWidget&);  //Not implemented
  void operator=(const VISU_ImplicitFunctionWidget&);  //Not implemented
};

#endif

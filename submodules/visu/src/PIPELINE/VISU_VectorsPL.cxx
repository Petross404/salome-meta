// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_VectorsPL.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_UsedPointsFilter.hxx"
#include "VTKViewer_TransformFilter.h"
#include "VTKViewer_Transform.h"
#include "VTKViewer_CellCenters.h"

#include <vtkGlyph3D.h>
#include <vtkConeSource.h>
#include <vtkLineSource.h>
#include <vtkGlyphSource2D.h>
#include <vtkPolyData.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_VectorsPL);

//----------------------------------------------------------------------------
template<class TOutputFilter>
void ToCellCenters( TOutputFilter *theOutputFilter, 
                    VTKViewer_CellCenters *theCellCenters,
                    vtkDataSet* theDataSet,
                    VISU_UsedPointsFilter* theUsedPointsFilter )
{
  if ( VISU::IsDataOnCells( theDataSet ) ) {
    theCellCenters->SetInputData( theDataSet );
    theCellCenters->VertexCellsOn();
    theOutputFilter->SetInputConnection( theCellCenters->GetOutputPort() );
  }else {
    theUsedPointsFilter->SetInputData( theDataSet );
    theOutputFilter->SetInputConnection( theUsedPointsFilter->GetOutputPort() );
  }
}


VISU_VectorsPL
::VISU_VectorsPL()
{
  SetIsShrinkable(false);
  SetIsFeatureEdgesAllowed(false);

  myGlyph = vtkGlyph3D::New();

  myGlyphSource = vtkGlyphSource2D::New();
  myConeSource = vtkConeSource::New();
  myLineSource = vtkLineSource::New();

  myCenters = VTKViewer_CellCenters::New();
  myTransformFilter = VTKViewer_TransformFilter::New();

  myUsedPointsFilter = VISU_UsedPointsFilter::New();
}


//----------------------------------------------------------------------------
VISU_VectorsPL
::~VISU_VectorsPL()
{
  myGlyph->Delete();

  myCenters->Delete();

  myGlyphSource->Delete();

  myConeSource->Delete();

  myLineSource->Delete();

  myTransformFilter->Delete();
  
  myUsedPointsFilter->Delete();
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_VectorsPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myGlyph->GetMTime());
  aTime = std::max(aTime, myCenters->GetMTime());
  aTime = std::max(aTime, myGlyphSource->GetMTime());
  aTime = std::max(aTime, myConeSource->GetMTime());
  aTime = std::max(aTime, myLineSource->GetMTime());
  aTime = std::max(aTime, myTransformFilter->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_VectorsPL *aPipeLine = dynamic_cast<VISU_VectorsPL*>(thePipeLine)){
    SetGlyphType(aPipeLine->GetGlyphType());
    SetGlyphPos(aPipeLine->GetGlyphPos());
  }
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::SetTransform( VTKViewer_Transform* theTransform )
{
  myTransformFilter->SetTransform( theTransform );
  myTransformFilter->Modified();
}


//----------------------------------------------------------------------------
VTKViewer_Transform* 
VISU_VectorsPL
::GetTransform()
{
  return static_cast< VTKViewer_Transform* >( myTransformFilter->GetTransform() );
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::SetScale(double theScale) 
{
  if ( VISU::CheckIsSameValue( myScaleFactor, theScale ) )
    return;

  myGlyph->SetScaleFactor( theScale );

  myScaleFactor = theScale;

  Modified();
}


//----------------------------------------------------------------------------
double
VISU_VectorsPL
::GetScale() 
{
  return myGlyph->GetScaleFactor();
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::SetGlyphType(VISU_VectorsPL::GlyphType theType) 
{
  if(myTypeGlyph == theType)
    return;

  myTypeGlyph = theType;
  Modified();
}


//----------------------------------------------------------------------------
VISU_VectorsPL::GlyphType
VISU_VectorsPL
::GetGlyphType() const
{
  return myTypeGlyph;
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::SetGlyphPos(VISU_VectorsPL::GlyphPos thePos) 
{
  if(myPosGlyph == thePos)
    return;

  myPosGlyph = thePos;
  Modified();
}


//----------------------------------------------------------------------------
VISU_VectorsPL::GlyphPos
VISU_VectorsPL
::GetGlyphPos() const
{
  return myPosGlyph;
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::Init()
{
  Superclass::Init();

  SetGlyphType(ARROW);
  SetGlyphPos(TAIL);
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::Build()
{
  Superclass::Build();
  
  ToCellCenters( myTransformFilter,
                 myCenters,
                 GetMergedInput(),
                 myUsedPointsFilter );

  myGlyph->SetInputConnection( myTransformFilter->GetOutputPort() );
  myGlyph->SetVectorModeToUseVector();
  myGlyph->SetScaleModeToScaleByVector();
  myGlyph->SetColorModeToColorByScalar();
}


//----------------------------------------------------------------------------
vtkAlgorithmOutput* 
VISU_VectorsPL
::InsertCustomPL()
{
  return myGlyph->GetOutputPort();
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::Update()
{
  switch (myTypeGlyph) {
  case ARROW: {
    myGlyphSource->SetGlyphTypeToArrow();
    myGlyphSource->SetFilled(0);
    switch (myPosGlyph) {
    case TAIL:
      myGlyphSource->SetCenter(0.5, 0.0, 0.0);
      break;
    case HEAD:
      myGlyphSource->SetCenter(-0.5, 0.0, 0.0);
      break;
    case CENTER:
      myGlyphSource->SetCenter(0.0, 0.0, 0.0);
    }
    myGlyph->SetSourceConnection(myGlyphSource->GetOutputPort());
  }
    break;
  case CONE2:
  case CONE6: {
    if (myTypeGlyph == CONE2)
      myConeSource->SetResolution(3);
    else
      myConeSource->SetResolution(7);
    myConeSource->SetHeight(1.0);
    myConeSource->SetRadius(.1);

    switch (myPosGlyph) {
    case TAIL:
      myConeSource->SetCenter(0.5, 0.0, 0.0);
      break;
    case HEAD:
      myConeSource->SetCenter(-0.5, 0.0, 0.0);
      break;
    case CENTER:
      myConeSource->SetCenter(0.0, 0.0, 0.0);
    }
    myGlyph->SetSourceConnection(myConeSource->GetOutputPort());
  }
    break;
  case NONE:
  default: {
    myGlyph->SetSourceConnection(myLineSource->GetOutputPort());
  }
  }

  Superclass::Update();
}


//----------------------------------------------------------------------------
unsigned long int
VISU_VectorsPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataSet* aDataSet = myGlyph->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;
  
  if(vtkDataSet* aDataSet = myCenters->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
vtkDataSet* 
VISU_VectorsPL
::GetOutput()
{
  myGlyph->Update();

  return myGlyph->GetOutput();
}


//----------------------------------------------------------------------------
void
VISU_VectorsPL
::SetMapScale(double theMapScale)
{
  VISU_ScalarMapPL::SetMapScale(theMapScale);
  myMapScaleFactor = theMapScale;

  myGlyph->SetScaleFactor( myScaleFactor*theMapScale );

  Modified();
}


//----------------------------------------------------------------------------

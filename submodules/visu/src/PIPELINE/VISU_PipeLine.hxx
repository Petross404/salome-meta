// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_PipeLine_HeaderFile
#define VISU_PipeLine_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_IDMapper.hxx"

#include <vtkObject.h>
#include <vtkSmartPointer.h>

class vtkCell;
class vtkPlane;
class vtkMapper;
class vtkDataSet;
class vtkPointSet;
class vtkImplicitFunction;
class vtkTimeStamp;

class VISU_MapperHolder;

//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_PipeLine : public vtkObject
{
  friend class VISU_MapperHolder;

public:
  vtkTypeMacro(VISU_PipeLine, vtkObject);

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  void
  ShallowCopy(VISU_PipeLine *thePipeLine,
              bool theIsCopyInput);

  virtual
  void
  SameAs(VISU_PipeLine *thePipeLine);

  //----------------------------------------------------------------------------
  void 
  SetMapperHolder(VISU_MapperHolder* theHolder);

  VISU_MapperHolder* 
  GetMapperHolder();

  const VISU::PIDMapper&  
  GetIDMapper();

  virtual
  vtkDataSet* 
  GetInput();

  virtual 
  vtkMapper* 
  GetMapper();

  virtual
  vtkDataSet* 
  GetOutput();

  //----------------------------------------------------------------------------
  virtual
  void
  Init();

  virtual
  void
  Update();

  //----------------------------------------------------------------------------
  virtual
  vtkIdType
  GetNodeObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetNodeVTKID(vtkIdType theID);

  virtual
  double* 
  GetNodeCoord(vtkIdType theObjID);

  virtual
  vtkIdType
  GetElemObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetElemVTKID(vtkIdType theID);

  virtual
  vtkCell*
  GetElemCell(vtkIdType theObjID);

  //----------------------------------------------------------------------------
  bool
  IsPlanarInput();

  bool 
  IsShrinkable();

  bool 
  IsFeatureEdgesAllowed();

  //----------------------------------------------------------------------------
  void
  SetImplicitFunction(vtkImplicitFunction *theFunction);

  vtkImplicitFunction* 
  GetImplicitFunction();

  void
  SetExtractInside(bool theMode);

  void
  SetExtractBoundaryCells(bool theMode);

  //----------------------------------------------------------------------------
  virtual
  void 
  RemoveAllClippingPlanes();

  vtkIdType
  GetNumberOfClippingPlanes();

  virtual
  bool
  AddClippingPlane(vtkPlane* thePlane);

  virtual
  vtkPlane* 
  GetClippingPlane(vtkIdType theID);

  virtual void RemoveClippingPlane(vtkIdType theID);

  virtual
  void
  SetPlaneParam(double theDir[3], 
                double theDist, 
                vtkPlane* thePlane);

  virtual
  void
  GetPlaneParam(double theDir[3], 
                double& theDist, 
                vtkPlane* thePlane);

  void                
  GetVisibleBounds(double theBounds[6]);

  //----------------------------------------------------------------------------
  static
  size_t
  CheckAvailableMemory(double theSize);

  static
  size_t
  GetAvailableMemory(double theSize,
                     double theMinSize = 1024*1024);

protected:
  //----------------------------------------------------------------------------
  VISU_PipeLine();

  virtual
  ~VISU_PipeLine();

  //----------------------------------------------------------------------------
  virtual
  void
  Build() = 0;

  virtual
  void
  OnCreateMapperHolder() = 0;

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);
                
  virtual
  void
  ComputeVisibleBounds();

  //----------------------------------------------------------------------------
  vtkDataSet* 
  GetClippedInput();

  vtkAlgorithmOutput* 
  GetClippedInputPort();

  void 
  SetIsShrinkable(bool theIsShrinkable);

  void 
  SetIsFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed);
  
  //Visible bounds xmin, xmax, ymin, ymax, zmin, zmax
  double myVisibleBounds[6];
  vtkTimeStamp myVisibleComputeTime;       // Time at which visible bounds computed

private:
  //----------------------------------------------------------------------------
  vtkSmartPointer<VISU_MapperHolder> myMapperHolder;
  bool myIsShrinkable;
  bool myIsFeatureEdgesAllowed;
};

#endif

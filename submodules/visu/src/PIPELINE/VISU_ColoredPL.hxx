// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_ColoredPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_ColoredPL_HeaderFile
#define VISU_ColoredPL_HeaderFile

#include "VISU_PipeLine.hxx"

#include <vtkSmartPointer.h>

class VISU_Extractor;
class VISU_FieldTransform;
class VISU_LookupTable;

class vtkPassThroughFilter;
class vtkDoubleArray;
class vtkThreshold;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_ColoredPL : public VISU_PipeLine
{
public:
  vtkTypeMacro(VISU_ColoredPL, VISU_PipeLine);

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  int
  GetScalarMode();

  virtual
  void
  SetScalarMode(int theScalarMode = 0);
  
  virtual
  double* 
  GetScalarRange();

  virtual
  void
  SetScalarRange( double theRange[2] );

  void
  SetScalarFilterRange( double theRange[2] );

  void
  GetScalarFilterRange( double theRange[2] );

  double* 
  GetScalarFilterRange();

  bool
  IsScalarFilterUsed();

  void
  UseScalarFiltering( bool theUseScalarFilter );

  virtual
  void
  SetScaling(int theScaling);
  
  virtual
  int
  GetScaling();
  
  virtual
  void
  SetNbColors(int theNbColors);

  virtual
  int
  GetNbColors();
  
  vtkDoubleArray* GetDistribution();


public:
  //----------------------------------------------------------------------------
  virtual
  void
  Init();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  VISU_LookupTable*
  GetMapperTable();

  virtual 
  VISU_LookupTable*
  GetBarTable();

  virtual 
  void
  SetMapScale(double theMapScale = 1.0);

  virtual
  double
  GetMapScale();

  virtual
  void
  GetSourceRange(double theRange[2]);

  virtual
  void
  SetSourceRange();

  virtual
  void
  UpdateMapperLookupTable();
  

protected:
  //----------------------------------------------------------------------------
  VISU_ColoredPL();
  VISU_ColoredPL(const VISU_ColoredPL&) {};

  virtual
  ~VISU_ColoredPL();

  //----------------------------------------------------------------------------
  virtual
  void
  Build();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  //----------------------------------------------------------------------------
  VISU_Extractor* 
  GetExtractorFilter();

  VISU_FieldTransform* 
  GetFieldTransformFilter();

  //----------------------------------------------------------------------------
  virtual 
  vtkPointSet* 
  GetClippedInput();

  virtual
  vtkAlgorithmOutput* 
  GetClippedInputPort();

  //----------------------------------------------------------------------------
  static
  void
  SetScalarMode(int theScalarMode,
                vtkDataSet *theInput,
                VISU_Extractor* theExtractor);

private:
  vtkSmartPointer< VISU_LookupTable > myMapperTable;
  vtkSmartPointer< VISU_LookupTable > myBarTable;
  vtkSmartPointer< VISU_Extractor > myExtractor;
  vtkSmartPointer< VISU_FieldTransform > myFieldTransform;
  vtkSmartPointer< vtkThreshold > myThreshold;
  vtkSmartPointer< vtkPassThroughFilter > myPassFilter;
  vtkSmartPointer< vtkDoubleArray > myDistribution;
};
  
#endif

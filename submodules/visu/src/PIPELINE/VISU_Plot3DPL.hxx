// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_CutPlanesPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_Plot3DPL_HeaderFile
#define VISU_Plot3DPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_CutPlanesPL.hxx"

class vtkWarpScalar;
class vtkContourFilter;
class vtkGeometryFilter;
class VISU_CellDataToPointData;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_Plot3DPL : public VISU_ScalarMapPL
{
public:
  vtkTypeMacro(VISU_Plot3DPL, VISU_ScalarMapPL);

  static
  VISU_Plot3DPL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  VISU_CutPlanesPL::PlaneOrientation
  GetPlaneOrientation();

  double
  GetRotateX();

  double
  GetRotateY();

  void
  SetOrientation(VISU_CutPlanesPL::PlaneOrientation theOrientation,
                 double theXAngle = 0.0,
                 double theYAngle = 0.0);

  double
  GetPlanePosition();

  bool
  IsPositionRelative();

  void
  SetPlanePosition(double thePosition,
                   bool theIsRelative);

  void
  SetScaleFactor(double theScaleFactor);

  double
  GetScaleFactor();

  void
  SetContourPrs(bool theIsContourPrs );

  bool
  GetIsContourPrs();

  void
  SetNumberOfContours(int theNumber);

  int
  GetNumberOfContours();

  void
  GetBasePlane (double theOrigin[3],
                double theNormal[3],
                bool  theCenterOrigine = false );
  
  void
  GetMinMaxPosition( double& minPos, 
                     double& maxPos );
  
public:
  virtual
  void
  Init();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  static
  VISU_CutPlanesPL::PlaneOrientation
  GetOrientation(vtkDataSet* theDataSet);

  static
  double
  GetScaleFactor( VISU_ColoredPL* theColoredPL,
                  vtkDataSet* theDataSet );

  void 
  SetMapScale(double theMapScale);

protected:
  VISU_Plot3DPL();

  virtual 
  ~VISU_Plot3DPL();

  virtual
  vtkAlgorithmOutput* 
  InsertCustomPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  double myAngle[3];
  bool myIsRelative, myIsContour;
  double myPosition, myScaleFactor, myMapScaleFactor;
  VISU_CutPlanesPL::PlaneOrientation myOrientation;

  vtkSmartPointer<VISU_CellDataToPointData> myCellDataToPointData;
  vtkSmartPointer<vtkAppendPolyData> myAppendPolyData;
  vtkSmartPointer<vtkGeometryFilter> myGeometryFilter;
  vtkSmartPointer<vtkContourFilter> myContourFilter;
  vtkSmartPointer<vtkWarpScalar> myWarpScalar;

private:
  VISU_Plot3DPL(const VISU_Plot3DPL&);;  // Not implemented.
  void operator=(const VISU_Plot3DPL&);  // Not implemented.
};

#endif

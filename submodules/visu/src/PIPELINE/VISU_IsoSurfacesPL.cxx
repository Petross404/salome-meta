// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_IsoSurfacesPL.hxx"
#include "VISU_LookupTable.hxx"

#include "VISU_PipeLineUtils.hxx"
#include "VISU_LabelPointsFilter.hxx"

#include <vtkContourFilter.h>

#define GAP_COEFFICIENT 0.0001


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_IsoSurfacesPL);


//----------------------------------------------------------------------------
VISU_IsoSurfacesPL
::VISU_IsoSurfacesPL()
{
  SetIsShrinkable(false);
  SetIsFeatureEdgesAllowed(false);

  SetElnoDisassembleState( true );

  myContourFilter = vtkContourFilter::New();

  myCellDataToPointData = VISU_CellDataToPointData::New();
}


//----------------------------------------------------------------------------
VISU_IsoSurfacesPL
::~VISU_IsoSurfacesPL()
{
  myContourFilter->Delete();
  myContourFilter = NULL;

  myCellDataToPointData->Delete();
  myCellDataToPointData = NULL;
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_IsoSurfacesPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myCellDataToPointData->GetMTime());
  aTime = std::max(aTime, myContourFilter->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_IsoSurfacesPL *aPipeLine = dynamic_cast<VISU_IsoSurfacesPL*>(thePipeLine)){
    SetNbParts(aPipeLine->GetNbParts());
    double aRange[2] = {aPipeLine->GetMin(), aPipeLine->GetMax()};
    SetRange(aRange);
    SetRangeFixed(aPipeLine->IsRangeFixed());
  }
}


//----------------------------------------------------------------------------
int
VISU_IsoSurfacesPL
::GetNbParts() 
{
  return myContourFilter->GetNumberOfContours();
}

//----------------------------------------------------------------------------
double
VISU_IsoSurfacesPL
::GetValue(int i) 
{
  return myContourFilter->GetValue(i);
}


//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::SetNbParts(int theNb) 
{
  myContourFilter->SetNumberOfContours(theNb);
}


//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::SetScalarRange( double theRange[2] ) 
{
  Superclass::SetScalarRange( theRange );
  SetRange(myRange);
}


//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::SetRange(double theRange[2], bool theIsForced)
{
  if(VISU::CheckIsSameRange(myRange, theRange) && !theIsForced)
    return;

  if(theRange[0] <= theRange[1]){
    myRange[0] = theRange[0];  
    myRange[1] = theRange[1];
    double aRange[2] = {theRange[0], theRange[1]};
    if( IsRangeFixed() ) {
      double aDelta = fabs( aRange[1] - aRange[0] ) * GAP_COEFFICIENT;
      aRange[0] += aDelta;
      aRange[1] -= aDelta;
    }
    if(GetScaling() == VTK_SCALE_LOG10)
      VISU_LookupTable::ComputeLogRange(theRange, aRange);
    myContourFilter->GenerateValues(GetNbParts(), aRange);
  }
}


//----------------------------------------------------------------------------
double
VISU_IsoSurfacesPL
::GetMin() 
{
  return myRange[0];
}


//----------------------------------------------------------------------------
double
VISU_IsoSurfacesPL
::GetMax() 
{
  return myRange[1];
}


//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::SetRangeFixed(bool theIsFixed)
{
  myIsRangeFixed = theIsFixed;
  SetRange( myRange, true );
}


//----------------------------------------------------------------------------
bool
VISU_IsoSurfacesPL
::IsRangeFixed()
{
  return myIsRangeFixed;
}


void
//----------------------------------------------------------------------------
VISU_IsoSurfacesPL
::Init()
{
  Superclass::Init();

  SetNbParts(10);

  double aScalarRange[2];
  GetSourceRange(aScalarRange);
  SetRange(aScalarRange);

  SetRangeFixed(true);
}

//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::Build()
{
  Superclass::Build();

  VISU::CellDataToPoint(myContourFilter,
                        myCellDataToPointData,
                        GetMergedInput(),
                        GetMergedInputPort());

}


//----------------------------------------------------------------------------

vtkAlgorithmOutput* 
VISU_IsoSurfacesPL
::InsertCustomPL()
{
  return myContourFilter->GetOutputPort();
}


//----------------------------------------------------------------------------
unsigned long int
VISU_IsoSurfacesPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataSet* aDataSet = myContourFilter->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;
  
  if(myCellDataToPointData->GetInput())
    if(vtkDataSet* aDataSet = myCellDataToPointData->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_IsoSurfacesPL
::SetMapScale(double theMapScale)
{
  Superclass::SetMapScale(theMapScale);

  double aRange[2] = {GetMax() - theMapScale*(GetMax()-GetMin()), GetMax()};
  double aNewRange[2] = {aRange[0], aRange[1]};
  if( IsRangeFixed() ) {
    double aDelta = fabs( aNewRange[1] - aNewRange[0] ) * GAP_COEFFICIENT;
    aNewRange[0] += aDelta;
    aNewRange[1] -= aDelta;
  }
  if(GetScaling() == VTK_SCALE_LOG10)
    VISU_LookupTable::ComputeLogRange(aRange,aNewRange);
  myContourFilter->GenerateValues(GetNbParts(), aNewRange);
}


//----------------------------------------------------------------------------

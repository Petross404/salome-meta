// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_ColoredPL.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_ColoredPL.hxx"
#include "VISU_Extractor.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_LookupTable.hxx"
#include "VISU_MapperHolder.hxx"

#include "VISU_PipeLineUtils.hxx"

#include <vtkThreshold.h>
#include <vtkPassThroughFilter.h>
#include <vtkDoubleArray.h>

#ifdef WNT
#include <float.h>
#define isnan _isnan
#endif

//----------------------------------------------------------------------------
VISU_ColoredPL
::VISU_ColoredPL():
  myMapperTable( VISU_LookupTable::New() ),
  myBarTable( VISU_LookupTable::New() ),
  myExtractor( VISU_Extractor::New() ),
  myFieldTransform( VISU_FieldTransform::New() ),
  myThreshold ( vtkThreshold::New() ),
  myPassFilter( vtkPassThroughFilter::New() ),
  myDistribution( vtkDoubleArray::New() )
{
  myMapperTable->Delete();
  myMapperTable->SetScale(VTK_SCALE_LINEAR);
  myMapperTable->SetHueRange(0.667, 0.0);

  myBarTable->Delete();
  myBarTable->SetScale(VTK_SCALE_LINEAR);
  myBarTable->SetHueRange(0.667, 0.0);

  myExtractor->Delete();

  myFieldTransform->Delete();

  myThreshold->AllScalarsOff(); 
  myThreshold->Delete();
  myPassFilter->Delete();
  myDistribution->Delete();
}


//----------------------------------------------------------------------------
VISU_ColoredPL
::~VISU_ColoredPL()
{}


//----------------------------------------------------------------------------
unsigned long int 
VISU_ColoredPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myMapperTable->GetMTime());
  aTime = std::max(aTime, myBarTable->GetMTime());
  aTime = std::max(aTime, myExtractor->GetMTime());
  aTime = std::max(aTime, myFieldTransform->GetMTime());
  aTime = std::max(aTime, myThreshold->GetMTime());
  aTime = std::max(aTime, myPassFilter->GetMTime());
  aTime = std::max(aTime, myDistribution->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_ColoredPL *aPipeLine = dynamic_cast<VISU_ColoredPL*>(thePipeLine)){
    if ( theIsCopyInput ) {
      SetScalarRange( aPipeLine->GetScalarRange() );
      if ( this->IsScalarFilterUsed() )
        SetScalarFilterRange( aPipeLine->GetScalarFilterRange() );
    }

    SetScalarMode(aPipeLine->GetScalarMode());
    SetNbColors(aPipeLine->GetNbColors());
    SetScaling(aPipeLine->GetScaling());
    SetMapScale(aPipeLine->GetMapScale());
  }
}


//----------------------------------------------------------------------------
int
VISU_ColoredPL
::GetScalarMode()
{
  return myExtractor->GetScalarMode();
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::SetScalarMode(int theScalarMode,
                vtkDataSet *theInput,
                VISU_Extractor* theExtractor)
{
  if(theInput){
    if(VISU::IsDataOnPoints(theInput)){
      vtkPointData *aPointData = theInput->GetPointData();
      if(!aPointData->GetAttribute(vtkDataSetAttributes::VECTORS)) {
        if(theScalarMode == 0){
          return;
        }
      }
    } else {
      vtkCellData *aCellData = theInput->GetCellData();
      if(!aCellData->GetAttribute(vtkDataSetAttributes::VECTORS)){
        if(theScalarMode == 0){
          return;
        }
      }
    }
  }

  theExtractor->SetScalarMode(theScalarMode);
}

//----------------------------------------------------------------------------
void
VISU_ColoredPL
::SetScalarMode(int theScalarMode)
{
  SetScalarMode(theScalarMode, GetInput(), myExtractor);
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::SetScalarRange( double theRange[2] )
{
  if (isnan(theRange[0]) || isnan(theRange[1]))
    throw std::runtime_error("NAN values in the presentation");

  if ( theRange[0] > theRange[1] ) 
    return;
  
  if (VISU::CheckIsSameRange( GetScalarRange(), theRange) )
    return;

  myFieldTransform->SetScalarRange( theRange );
  myBarTable->SetRange( theRange );
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::SetScalarFilterRange( double theRange[2] )
{
  double aRange[ 2 ];
  this->GetScalarFilterRange( aRange );

  if ( VISU::CheckIsSameRange( aRange, theRange) )
    return;

  myThreshold->ThresholdBetween( theRange[0], theRange[1] );
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::GetScalarFilterRange( double theRange[2] )
{
  theRange[ 0 ] = myThreshold->GetLowerThreshold();
  theRange[ 1 ] = myThreshold->GetUpperThreshold();
}


//----------------------------------------------------------------------------
double*
VISU_ColoredPL
::GetScalarFilterRange()
{
  static double aRange[ 2 ];

  this->GetScalarFilterRange( aRange );

  return aRange;
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::UseScalarFiltering( bool theUseScalarFilter )
{
  if ( theUseScalarFilter ) {
    // Include threshold filter between the transform and the pass filters. 
    myPassFilter->SetInputConnection( myThreshold->GetOutputPort() );
  } else {
    // Exclude threshold filter before the pass filter. 
    myPassFilter->SetInputConnection( myFieldTransform->GetOutputPort() );
  }
}


//----------------------------------------------------------------------------
bool
VISU_ColoredPL
::IsScalarFilterUsed()
{
  return myThreshold->GetOutput() == myPassFilter->GetInput();
}


//----------------------------------------------------------------------------
vtkDoubleArray* 
VISU_ColoredPL
::GetDistribution() 
{
  unsigned long int aTime = this->GetMTime();
  // If modified then update the distribution array
  if (aTime > myDistribution->GetMTime()) {
        // Set number of colors for the distribution
    int nbColors = this->GetNbColors();
        this->myDistribution->SetNumberOfValues(nbColors);
        // Initialize numbers of colored cells with zero
        this->myDistribution->FillComponent(0, 0);
        // Create a lookup table to compute a color of a cell
    VISU_LookupTable* lut = GetMapperTable();
    double aMapScale = lut->GetMapScale();
    // Get scalar values from the input data to calculate their distribution within cells
    vtkDataArray* dataArr;
    // Dtermine where we have to take scalars from: cells data or points data. 
    if(VISU::IsDataOnCells(this->GetOutput())) {
        dataArr = this->GetOutput()->GetCellData()->GetScalars();
    } else {
        dataArr = this->GetOutput()->GetPointData()->GetScalars();
    }
    // If scalars data array is not defined then create an empty one to avoid exceptions
    if (dataArr == NULL) {
        dataArr = vtkDoubleArray::New();
    }
    
    // Get range of scalars values
//    double aRange[2];
//    dataArr->GetRange(aRange);

    // Build the lookup table with the found range
    // Get number of scalar values
    int aNbVals = dataArr->GetNumberOfTuples();
    if (aNbVals > 0) {
      // Count the number of scalar values for each color in the input data
      int idx = 0;
      double cnt = 0;
      // For each scalar value
      for(vtkIdType aValId = 0; aValId < aNbVals; aValId++){
        // Find the color index for this scalar value
        idx = lut->GetIndex(*(dataArr->GetTuple(aValId)) * aMapScale);
        // Increment the distribution value for this color index
        cnt = this->myDistribution->GetValue(idx);
        this->myDistribution->SetValue(idx, cnt + 1);
      }
      // Compute relative values when 1 is according to the total number of scalar values
      for(vtkIdType aValId = 0; aValId < nbColors; aValId++){
        cnt = this->myDistribution->GetValue(aValId);
        this->myDistribution->SetValue(aValId, cnt / aNbVals);
      }
    }
    this->myDistribution->Modified();
        
  }
  
  return myDistribution;
}
//----------------------------------------------------------------------------
  // RKV : End

//----------------------------------------------------------------------------
double* 
VISU_ColoredPL
::GetScalarRange() 
{
  return myFieldTransform->GetScalarRange();
}

//----------------------------------------------------------------------------
void
VISU_ColoredPL
::SetScaling(int theScaling) 
{
  if(GetScaling() == theScaling)
    return;

  myBarTable->SetScale(theScaling);

  if(theScaling == VTK_SCALE_LOG10)
    myFieldTransform->SetScalarTransform(&(VISU_FieldTransform::Log10));
  else
    myFieldTransform->SetScalarTransform(&(VISU_FieldTransform::Ident));
}

//----------------------------------------------------------------------------
int
VISU_ColoredPL
::GetScaling() 
{
  return myBarTable->GetScale();
}

//----------------------------------------------------------------------------
void
VISU_ColoredPL
::SetNbColors(int theNbColors) 
{
  myMapperTable->SetNumberOfColors(theNbColors);
  myBarTable->SetNumberOfColors(theNbColors);
}

int
VISU_ColoredPL
::GetNbColors() 
{
  return myMapperTable->GetNumberOfColors();
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::Init()
{
  SetScalarMode(0);

  double aRange[2];
  GetSourceRange( aRange );

  SetScalarRange( aRange );
  SetScalarFilterRange( aRange );
}

//----------------------------------------------------------------------------
vtkPointSet* 
VISU_ColoredPL
::GetClippedInput()
{
  if(myPassFilter->GetInput())
    myPassFilter->Update();
  return myPassFilter->GetUnstructuredGridOutput();
}

//----------------------------------------------------------------------------
vtkAlgorithmOutput* 
VISU_ColoredPL
::GetClippedInputPort()
{
  return myPassFilter->GetOutputPort();
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::Build() 
{
  myExtractor->SetInputConnection( Superclass::GetClippedInputPort() );
  myFieldTransform->SetInputConnection(myExtractor->GetOutputPort());

  myThreshold->SetInputConnection( myFieldTransform->GetOutputPort() );
  // The pass filter is used here for possibility to include/exclude 
  // threshold filter before it.
  myPassFilter->SetInputConnection( myFieldTransform->GetOutputPort() );

  GetMapperHolder()->SetLookupTable(GetMapperTable());
  //GetMapper()->InterpolateScalarsBeforeMappingOn();
  GetMapper()->SetUseLookupTableScalarRange( true );
  GetMapper()->SetColorModeToMapScalars();
  GetMapper()->ScalarVisibilityOn();
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::Update() 
{ 
  double *aRange = GetScalarRange();
  double aScalarRange[2] = {aRange[0], aRange[1]};
  if(myBarTable->GetScale() == VTK_SCALE_LOG10)
    VISU_LookupTable::ComputeLogRange(aRange, aScalarRange);

  if(!VISU::CheckIsSameRange(myMapperTable->GetRange(), aScalarRange))
    myMapperTable->SetRange(aScalarRange);
  
  myMapperTable->Build();
  myBarTable->Build();

  Superclass::Update();
}


//----------------------------------------------------------------------------
unsigned long int
VISU_ColoredPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataObject* aDataObject = myExtractor->GetInput())
    aSize = aDataObject->GetActualMemorySize() * 1024;
  
  if(vtkDataObject* aDataObject = myFieldTransform->GetInput())
    aSize += aDataObject->GetActualMemorySize() * 1024;
  
  return aSize;
}


//----------------------------------------------------------------------------
VISU_LookupTable *
VISU_ColoredPL
::GetMapperTable()
{ 
  return myMapperTable.GetPointer();
}


//----------------------------------------------------------------------------
VISU_LookupTable*
VISU_ColoredPL
::GetBarTable()
{
  return myBarTable.GetPointer();
}


//----------------------------------------------------------------------------
VISU_Extractor*
VISU_ColoredPL
::GetExtractorFilter()
{
  return myExtractor.GetPointer();
}


//----------------------------------------------------------------------------
VISU_FieldTransform*
VISU_ColoredPL
::GetFieldTransformFilter()
{
  return myFieldTransform.GetPointer();
}


//----------------------------------------------------------------------------
void 
VISU_ColoredPL
::SetMapScale(double theMapScale)
{
  if(!VISU::CheckIsSameValue(myMapperTable->GetMapScale(), theMapScale)){
    myMapperTable->SetMapScale(theMapScale);
    myMapperTable->Build();
  }
}

double
VISU_ColoredPL
::GetMapScale()
{
  return myMapperTable->GetMapScale();
}


//----------------------------------------------------------------------------
void
VISU_ColoredPL
::GetSourceRange(double theRange[2])
{
  myExtractor->Update();
  myExtractor->GetOutput()->GetScalarRange( theRange );
  
  if (isnan(theRange[0]) || isnan(theRange[1]))
    throw std::runtime_error("NAN values in the presentation");
}

void
VISU_ColoredPL
::SetSourceRange()
{
  double aRange[2];
  GetSourceRange( aRange );
  SetScalarRange( aRange );
}

//----------------------------------------------------------------------------
void
VISU_ColoredPL
::UpdateMapperLookupTable()
{
  //rnv: This method update pointer to the myMapperTable in the MapperHolder
  GetMapperHolder()->SetLookupTable(GetMapperTable());
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
// Salome includes
//
#include "VISU_Convertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "VISU_MeshPL.hxx"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_IsoSurfacesPL.hxx"
#include "VISU_CutPlanesPL.hxx"
#include "VISU_CutLinesPL.hxx"
#include "VISU_ColoredPL.hxx"
#include "VISU_DeformedShapePL.hxx"
#include "VISU_VectorsPL.hxx"
#include "VISU_StreamLinesPL.hxx"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_DeformedShapeAndScalarMapPL.hxx"
#include "VISU_Plot3DPL.hxx"

#include "VISU_ScalarBarActor.hxx"
#include "VISU_OpenGLPointSpriteMapper.hxx"
#include "VTKViewer_GeometryFilter.h"

#include "VISU_ElnoDisassembleFilter.hxx"
#include "VISU_ElnoAssembleFilter.hxx"

//typedef VISU_CutPlanesPL TPresent;
//typedef VISU_ScalarMapPL TPresent;
typedef VISU_CutLinesPL TPresent;

// VTK includes
#include <vtkShrinkFilter.h>
#include <vtkPointLocator.h>
#include <vtkEDFCutter.h>
#include <vtkPlane.h>
#include <vtkWarpVector.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarWidget.h>
#include <vtkMaskPoints.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkImageData.h>
#include <vtkCellData.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtk3DWidget.h>
#include <vtkProperty.h>

//RKV:Begin
#include <vtkLookupTable.h>
#include <vtkIntArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPointSet.h>
#include <vtkPolyData.h>
#include <vtkXYPlotActor.h>
#include <vtkProperty2D.h>
#include <vtkTextProperty.h>

#include <vtkXYPlotWidget.h>
#include <vtkScalarBarWidget.h>
#include <vtkScalarBarActor.h>
#include <vtkMatrix4x4.h>

/*#include <vtkBarChartActor.h>
#include <vtkFloatArray.h>
#include <vtkDataObject.h>
#include <vtkFieldData.h>
#include <vtkMath.h>
#include <vtkTextProperty.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkIdList.h>
#include <vtkProperty2D.h>
#include <vtkLegendBoxActor.h>
#include <vtkTestUtilities.h>
#include <vtkRegressionTestImage.h>
*/
//RKV:End

#include "utilities.h"

static int isOnlyMesh = false;

void PrintMissing(){
  MESSAGE(std::endl << "VISUPipeLine : missing operand after `VISUPipeLine'" <<
          std::endl << "VISUPipeLine : Try `VISUPipeLine -h' for more information.");
}

void PrintHelp(){
  MESSAGE_ADD(std::endl << " Usage: VISUPipeLine filename meshname fieldname [timestamp] [component] " <<
              std::endl << "filename   - Name of the med file.                                       " <<
              std::endl << "meshname   - Name of the mesh in the med file.                           " <<
              std::endl << "fieldname  - Name of the mesh field.                                     " <<
              std::endl << "timestamp  - Number of the time stamp in the field 1,2,3 ... (optionally)" <<
              std::endl << "             by default used first time stamp.                           " <<
              std::endl << "component  - Number of the component in the field 0,1,2,3 ...(optionally)" <<
              std::endl << "             0 - modulus, 1 - first component, 2 - second component, ... " <<
              std::endl << "             by default used modulus.                                    ");
}

void PrintErr(){
  MESSAGE("***************************ERROR*****************************************");
}
//#define _DEBUG_ID_MAPPING_

//RKV:Begin
/*
vtkActor* barChartActor()
{
  int numTuples = 6;

  vtkFloatArray *bitter = vtkFloatArray::New();
  bitter->SetNumberOfTuples(numTuples);
  
  for (int i=0; i<numTuples; i++)
    {
    bitter->SetTuple1(i, vtkMath::Random(7,100));
    }

  vtkDataObject *dobj = vtkDataObject::New();
  dobj->GetFieldData()->AddArray(bitter);
  
  vtkBarChartActor *actor = vtkBarChartActor::New();
  actor->SetInput(dobj);
  actor->SetTitle("Bar Chart");
  actor->GetPositionCoordinate()->SetValue(0.05,0.05,0.0);
  actor->GetPosition2Coordinate()->SetValue(0.95,0.85,0.0);
  actor->GetProperty()->SetColor(1,1,1);
  actor->GetLegendActor()->SetNumberOfEntries(numTuples);
  for (int i=0; i<numTuples; i++)
    {
    double red=vtkMath::Random(0,1);
    double green=vtkMath::Random(0,1);
    double blue=vtkMath::Random(0,1);
    actor->SetBarColor(i,red,green,blue);
    }
  actor->SetBarLabel(0,"oil");
  actor->SetBarLabel(1,"gas");
  actor->SetBarLabel(2,"water");
  actor->SetBarLabel(3,"snake oil");
  actor->SetBarLabel(4,"tequila");
  actor->SetBarLabel(5,"beer");
  actor->LegendVisibilityOn();

  // Set text colors (same as actor for backward compat with test)
  actor->GetTitleTextProperty()->SetColor(1,1,0);
  actor->GetLabelTextProperty()->SetColor(1,0,0);
  return actor;
}
*/
//RKV:End
//----------------------------------------------------------------------------
template<class TPipeLine>
VISU_ColoredPL*
CreateColoredPL(VISU_Convertor* theConvertor,
                const std::string& theMeshName,
                const VISU::TEntity& theEntity,
                const std::string& theFieldName,
                int theTimeStampNumber);


//----------------------------------------------------------------------------
template<>
VISU_ColoredPL*
CreateColoredPL<VISU_GaussPointsPL>(VISU_Convertor* theConvertor,
                                    const std::string& theMeshName,
                                    const VISU::TEntity& theEntity,
                                    const std::string& theFieldName,
                                    int theTimeStampNumber)
{
  VISU_GaussPointsPL* aPresent = VISU_GaussPointsPL::New();
  VISU::PGaussPtsIDMapper aGaussPtsIDMapper = 
    theConvertor->GetTimeStampOnGaussPts(theMeshName,
                                         theEntity,
                                         theFieldName,
                                         theTimeStampNumber);
  aPresent->SetGaussPtsIDMapper(aGaussPtsIDMapper);

  char aMainTexture[80];
  strcpy( aMainTexture, getenv( "VISU_ROOT_DIR" ) );
  strcat( aMainTexture, "/share/salome/resources/visu/sprite_texture.bmp" );
  
  char anAlphaTexture[80];
  strcpy( anAlphaTexture, getenv( "VISU_ROOT_DIR" ) );
  strcat( anAlphaTexture, "/share/salome/resources/visu/sprite_alpha.bmp" );
  
  vtkSmartPointer<vtkImageData> aTextureValue = 
    VISU_GaussPointsPL::MakeTexture( aMainTexture, anAlphaTexture );
  aPresent->SetImageData( aTextureValue.GetPointer() );

  aPresent->Update();

#ifdef _DEBUG_ID_MAPPING_
  vtkDataSet* aDataSet = aPresent->GetOutput();
  aDataSet->Update();
  int aNbCells = aDataSet->GetNumberOfCells();
  cout<<"aNbCells = "<<aNbCells<<endl;
  for(int anCellId = 0; anCellId < aNbCells; anCellId++){
    vtkIdType anObjID = aPresent->GetNodeObjID(anCellId);
    vtkIdType aVtkID = aPresent->GetNodeVTKID(anObjID);
    cout<<anObjID<<"; "<<aVtkID<<"; - ";
    double* aCoord = aPresent->GetNodeCoord(anObjID);
    cout<<aCoord[0]<<"; "<<aCoord[1]<<"; "<<aCoord[2]<<endl;
  }
#endif
  return aPresent;
}

//----------------------------------------------------------------------------
template<class TPipeLine>
VISU_ColoredPL*
CreateColoredPL(VISU_Convertor* theConvertor,
                const std::string& theMeshName,
                const VISU::TEntity& theEntity,
                const std::string& theFieldName,
                int theTimeStampNumber)
{
  TPipeLine* aPresent = TPipeLine::New();
  VISU::PUnstructuredGridIDMapper anUnstructuredGridIDMapper = 
    theConvertor->GetTimeStampOnMesh(theMeshName,
                                     theEntity,
                                     theFieldName,
                                     theTimeStampNumber);
  aPresent->SetUnstructuredGridIDMapper(anUnstructuredGridIDMapper);

  double aRange[] = { 4, 5 };
cout << "before filter limits set" << endl;
      vtkObject::GlobalWarningDisplayOn();
      aPresent->DebugOn();
  aPresent->SetScalarFilterRange( aRange );
  aPresent->UseScalarFiltering( true );
cout << "after filter set" << endl;

  //aPresent->ClearGeometry();
  //{
  //  VISU::PUnstructuredGridIDMapper anIDMapper = 
  //    theConvertor->GetMeshOnGroup(theMeshName,
  //                               "groupe1");
  //  aPresent->AddGeometry(anIDMapper->GetOutput());
  //}
  //{
  //  VISU::PUnstructuredGridIDMapper anIDMapper = 
  //    theConvertor->GetMeshOnGroup(theMeshName,
  //                               "TUBEH                                                                           ");
  //  aPresent->AddGeometry(anIDMapper->GetOutput());
  //}
  //{
  //  VISU::PUnstructuredGridIDMapper anIDMapper = 
  //    theConvertor->GetMeshOnGroup(theMeshName,
  //                               "STRI                                                                            ");
  //  aPresent->AddGeometry(anIDMapper->GetOutput());
  //}
  aPresent->Update();
cout << "after update" << endl;
  return aPresent;
}


//----------------------------------------------------------------------------
int
main(int argc, char** argv)
{
  try{
    if(argc == 1){
      PrintMissing();
      return 1;
    }
    if(argc == 2 && !strcmp(argv[1],"-h")) {
      PrintHelp();
      return 1;
    }
    
    char* aFileName =0;
    char* aMeshName =0;
    char* aFieldName =0;
    int aTimeStampNumber = 1;
    int aComponentNumber  = 0;
    bool allInit = false;
    bool isMeshOk = false;
    bool isFieldOk = false;
    bool isTimeStampOk = false;
    bool isComponentOk = false;

    if(argc == 4 ){
      aFileName = new char[static_cast<int>(strlen(argv[1])) + 1];
      aMeshName = new char[static_cast<int>(strlen(argv[2])) + 1];
      aFieldName = new char[static_cast<int>(strlen(argv[3])) + 1];
      
      strcpy(aFileName,argv[1]);
      strcpy(aMeshName,argv[2]);
      strcpy(aFieldName,argv[3]);
      
      allInit = true;
    }

    if(argc == 5) {
      aFileName = new char[static_cast<int>(strlen(argv[1])) + 1];
      aMeshName = new char[static_cast<int>(strlen(argv[2])) + 1];
      aFieldName = new char[static_cast<int>(strlen(argv[3])) + 1];
      
      strcpy(aFileName,argv[1]);
      strcpy(aMeshName,argv[2]);
      strcpy(aFieldName,argv[3]);

      aTimeStampNumber = atoi(argv[4]);
      allInit = true;
    }
    
    if(argc == 6) {
      aFileName = new char[static_cast<int>(strlen(argv[1])) + 1];
      aMeshName = new char[static_cast<int>(strlen(argv[2])) + 1];
      aFieldName = new char[static_cast<int>(strlen(argv[3])) + 1];
      
      strcpy(aFileName,argv[1]);
      strcpy(aMeshName,argv[2]);
      strcpy(aFieldName,argv[3]);

      aTimeStampNumber = atoi(argv[4]);
      aComponentNumber = atoi(argv[5]);
      allInit = true;
    }
    


    if(allInit){
            
      vtkRenderWindow *aWindow = vtkRenderWindow::New();
      vtkRenderer *aRenderer = vtkRenderer::New();

      aWindow->AddRenderer(aRenderer);
      aRenderer->GetActiveCamera()->ParallelProjectionOn();

      vtkRenderWindowInteractor *anInteractor = vtkRenderWindowInteractor::New();
      anInteractor->SetRenderWindow(aWindow);
      aWindow->Delete();

      vtkInteractorStyle* aStyle = vtkInteractorStyleTrackballCamera::New();
      anInteractor->SetInteractorStyle(aStyle);
      aStyle->Delete();

      VISU_Convertor* aConvertor = CreateConvertor(aFileName);
      aConvertor->BuildEntities();
      aConvertor->BuildGroups();
      aConvertor->BuildFields();
      aConvertor->BuildMinMax();
      const VISU::TMeshMap& aMeshMap = aConvertor->GetMeshMap();
      VISU::TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
      if(aMeshMapIter == aMeshMap.end()) return 0;
      const std::string& aMeshName = aMeshMapIter->first;
      const VISU::PMesh aMesh = aMeshMapIter->second;
      const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter;
      if(isOnlyMesh){
        const VISU::TEntity& anEntity = VISU::CELL_ENTITY;
        aMeshOnEntityMapIter = aMeshOnEntityMap.find(anEntity);

        VISU::PNamedIDMapper anIDMapper = 
          aConvertor->GetMeshOnEntity(aMeshName,anEntity);

        VISU_MeshPL* aPresent = VISU_MeshPL::New();
        aPresent->SetUnstructuredGridIDMapper(anIDMapper);

        vtkActor* aActor = vtkActor::New();
        aActor->SetMapper(aPresent->GetMapper());
        aActor->GetProperty()->SetRepresentation(VTK_WIREFRAME);
        //aRenderer->ResetCameraClippingRange();

        aRenderer->AddActor(aActor);

        aWindow->Render();
        anInteractor->Start();
        return 0;
      }
      //Import fields
      aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++) {
        const VISU::TEntity& anEntity = aMeshOnEntityMapIter->first;
        const VISU::PMeshOnEntity aMeshOnEntity = aMeshOnEntityMapIter->second;
        const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
        VISU::TFieldMap::const_iterator aFieldMapIter = aFieldMap.begin();
        for(; aFieldMapIter != aFieldMap.end(); aFieldMapIter++){
          const VISU::PField aField = aFieldMapIter->second;
//        if(aField->myNbComp == 1) 
//          continue;
          const std::string& aFieldName = aFieldMapIter->first;
          const VISU::TValField& aValField = aField->myValField;
          VISU::TValField::const_reverse_iterator aValFieldIter = aValField.rbegin();
          if(aValFieldIter == aValField.rend()) return 0;
          int aTimeStamp = aValFieldIter->first;

          vtkActor* anActor = vtkActor::New();
          VISU_ColoredPL* aPresent = NULL;
          if(anEntity == VISU::NODE_ENTITY){
            aPresent = CreateColoredPL<TPresent>(aConvertor,
                                                 aMeshName,
                                                 anEntity,
                                                 aFieldName,
                                                 aTimeStamp);

            anActor->SetMapper(aPresent->GetMapper());
          }else{
            continue;
            aPresent = CreateColoredPL<TPresent>(aConvertor,
                                                 aMeshName,
                                                 anEntity,
                                                 aFieldName,
                                                 aTimeStamp);

            VTKViewer_GeometryFilter* aGeometryFilter = VTKViewer_GeometryFilter::New();
            aGeometryFilter->SetInputData(aPresent->GetOutput());
            aGeometryFilter->SetInside(true);

            vtkMaskPoints* aMaskPoints = vtkMaskPoints::New();
            aMaskPoints->SetInputConnection(aGeometryFilter->GetOutputPort());
            aMaskPoints->SetGenerateVertices(true);
            aMaskPoints->SetOnRatio(1);

            VISU_OpenGLPointSpriteMapper* aMapper = VISU_OpenGLPointSpriteMapper::New();
            aMapper->SetAverageCellSize( VISU_DeformedShapePL::GetScaleFactor( aPresent->GetOutput() ) );
  
            char aMainTexture[80];
            strcpy( aMainTexture, getenv( "VISU_ROOT_DIR" ) );
            strcat( aMainTexture, "/share/salome/resources/visu/sprite_texture.vti" );
            
            char anAlphaTexture[80];
            strcpy( anAlphaTexture, getenv( "VISU_ROOT_DIR" ) );
            strcat( anAlphaTexture, "/share/salome/resources/visu/sprite_alpha.vti" );
            
            vtkSmartPointer<vtkImageData> aTextureValue = 
              VISU_GaussPointsPL::MakeTexture( aMainTexture, anAlphaTexture );
            aMapper->SetImageData( aTextureValue.GetPointer() );

            //vtkPolyDataMapper* aMapper = vtkPolyDataMapper::New();
            aMapper->SetLookupTable(aPresent->GetMapperTable());
            aMapper->SetUseLookupTableScalarRange(true);
            aMapper->SetColorModeToMapScalars();
            aMapper->SetScalarVisibility(true);

            aMapper->SetInputConnection(aMaskPoints->GetOutputPort());
            aGeometryFilter->Delete();

            anActor->SetMapper(aMapper);
            aMapper->Delete();
          }





//RKV:Begin
  vtkLookupTable* lut = vtkLookupTable::New();
  int nbColors = aPresent->GetNbColors();
  lut->SetNumberOfTableValues(nbColors);
  vtkDataArray* dataArr;
    if(VISU::IsDataOnCells(aPresent->GetInput())) {
        dataArr = aPresent->GetInput()->GetCellData()->GetScalars();
    } else {
        dataArr = aPresent->GetInput()->GetPointData()->GetScalars();
    }
  double aRange[2];
  dataArr->GetRange(aRange);
  MSG(true, "Range[0]: "<<aRange[0]);
  MSG(true, "Range[1]: "<<aRange[1]);

  lut->SetTableRange(aRange);
  lut->Build();
    MSG(true, "1\n");
  vtkIntArray* distr = vtkIntArray::New();
  distr->SetNumberOfValues(nbColors);
    MSG(true, "Number of colors: "<<nbColors);
  distr->FillComponent(0, 0.);
    MSG(true, "2\n");
  int aNbVals = dataArr->GetNumberOfTuples();
  int idx = 0, cnt = 0;
  
    MSG(true, "3\n");
  for(vtkIdType aValId = 0; aValId < aNbVals; aValId++){
//    MSG(true, "Value: "<< *(dataArr->GetTuple(aValId)));
        idx = lut->GetIndex(*(dataArr->GetTuple(aValId)));
//    MSG(true, "Value index "<<idx);
        cnt = distr->GetValue(idx);
        distr->SetValue(idx, cnt + 1);
  }
    MSG(true, "4\n");
  distr->Modified();
  double range[2];
  distr->GetRange(range);
  MSG(true, "DistrRange[0]: " << range[0]);
  MSG(true, "DistrRange[1]: " << range[1]);
    MSG(true, "5\n");
//  vtkPoints* points = vtkPoints::New();
//  aNbVals = distr->GetNumberOfTuples();
//  points->Allocate(aNbVals);
//  double x[3] = {0., 0., 0.};
/*  for(vtkIdType aValId = 0; aValId < aNbVals; aValId++){
        x[0] = aValId;
        points->InsertPoint(aValId, x);
*///    MSG(true, "Inserted point "<<aValId);
//    MSG(true, ": "<<distr->GetValue(aValId));
//  }
//  points->SetData(distr);
  
/*  vtkPointSet* pset = vtkPolyData::New();
  pset->SetPoints(points);
  pset->GetPointData()->SetScalars(distr);
*/    
  vtkDataObject *dobj = vtkDataObject::New();
  dobj->GetFieldData()->AddArray(distr);
  
  vtkXYPlotActor* xyplot = vtkXYPlotActor::New();
//  xyplot->AddInput(pset);
  xyplot->AddDataObjectInput(dobj);
  xyplot->GetPositionCoordinate()->SetValue(0.0, 0.67, 0);
  xyplot->GetPosition2Coordinate()->SetValue(1.0, 0.33, 0); // #relative to Position
  xyplot->SetXValuesToIndex();
//  xyplot->SetXValuesToNormalizedArcLength();
//  xyplot->SetXValuesToArcLength();
//  xyplot->SetNumberOfXLabels(0);
  xyplot->SetTitle("");
  xyplot->SetXTitle("");
  xyplot->SetYTitle("Distribution");
//  xyplot->ReverseYAxisOn();
//  xyplot->SetDataObjectPlotModeToRows();
  xyplot->SetLabelFormat("%.0f");
  xyplot->SetXRange(0, nbColors-1);
  xyplot->SetYRange(range[0], range[1]);
/*  int ny = floor(fabs(range[1] - range[0]));
    MSG(true, "ny = "<<ny);
    MSG(true, "nbYLabels = "<<xyplot->GetNumberOfYLabels());
  if (ny < xyplot->GetNumberOfYLabels()) {
    MSG(true, "5.1");
        xyplot->SetNumberOfYLabels(ny);
    MSG(true, "nbYLabels = "<<xyplot->GetNumberOfYLabels());
  }
*/  xyplot->GetProperty()->SetColor(1, 1, 1);
  xyplot->GetProperty()->SetLineWidth(2);
  xyplot->GetProperty()->SetDisplayLocationToForeground();
/*  vtkMatrix4x4 *m = vtkMatrix4x4::New();
    MSG(true, "5.1\n");
  m->Zero();
    MSG(true, "5.2\n");
  m->SetElement(0, 1, 1);
  m->SetElement(1, 0, -1);
  m->SetElement(2, 2, 1);
  m->SetElement(3, 3, 1);
    MSG(true, "5.3\n");
  xyplot->PokeMatrix(m);
*/  //# Set text prop color (same color for backward compat with test)
  //# Assign same object to all text props
  vtkTextProperty* tprop;
  tprop = xyplot->GetTitleTextProperty();
  tprop->SetColor(xyplot->GetProperty()->GetColor());
  xyplot->SetAxisTitleTextProperty(tprop);
  xyplot->SetAxisLabelTextProperty(tprop);
  
//RKV:End  
  

    MSG(true, "6\n");

/*{
  vtkXYPlotWidget* aWidget = vtkXYPlotWidget::New();
  aWidget->SetInteractor( anInteractor );
  aWidget->SetXYPlotActor( xyplot );
  aWidget->SetEnabled( 1 );
}*/


      vtkObject::GlobalWarningDisplayOn();

          VISU_ScalarBarActor * aScalarBar = VISU_ScalarBarActor::New();
          //vtkScalarBarActor * aScalarBar = vtkScalarBarActor::New();
          aPresent->SetNbColors(5);
          aPresent->DebugOn();
          aPresent->Update();
          aScalarBar->SetLookupTable(aPresent->GetBarTable());
          aScalarBar->DebugOn();
          aScalarBar->SetDistribution(aPresent->GetDistribution());
          aScalarBar->DistributionVisibilityOn();
//        aScalarBar->SetOrientationToHorizontal();
//        aScalarBar->GetPositionCoordinate()->SetValue(.1, .6, 0);
//        aScalarBar->GetPosition2Coordinate()->SetValue(.8, .3, 0);

/*{
  vtkScalarBarWidget* aWidget = vtkScalarBarWidget::New();
  aWidget->SetInteractor( anInteractor );
  aWidget->SetScalarBarActor( aScalarBar );
  aWidget->SetEnabled( 1 );
}
*/
          aRenderer->AddActor(anActor);
//        aRenderer->AddActor2D(xyplot); //RKV
          aRenderer->AddActor2D(aScalarBar);
          
          //aRenderer->AddActor2D(barChartActor()); //RKV

          aWindow->Render();
          aRenderer->ResetCamera();

          anInteractor->Start();
          return 0;
        }
      }
    }
    else{
      PrintMissing();
      return 1;
    }

    if(!isMeshOk) {
      PrintErr();
      MESSAGE("Can not find mesh with name `"<<aMeshName<<"' in the file `"<<aFileName<<"'");
      PrintErr();
      return 1;
    }
    if(!isFieldOk) {
      PrintErr();
      MESSAGE("Can not find field with name `"<<aFieldName<<"' on the mesh `"<<aMeshName<<"'");
      PrintErr();
      return 1; 
    }
    if(!isComponentOk) {
      PrintErr();
      MESSAGE("Field field with name `"<<aFieldName<<"' containt less then `"<<aComponentNumber<<"' component(s).");
      PrintErr();
      return 1;
    }
    if(!isTimeStampOk) {
      PrintErr();
      MESSAGE("Can not find time stamp with number `"<<aTimeStampNumber<<"' on the field `"<<aFieldName<<"'");
      PrintErr();
    }
  }catch(std::exception& exc){
    MSG(true, "Follow exception was occured :\n"<<exc.what());
  }catch(...){
    MSG(true, "Unknown exception was occured in VISU_Convertor_impl");
  }
  return 1;
}

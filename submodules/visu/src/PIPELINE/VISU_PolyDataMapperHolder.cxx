// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PolyDataMapperHolder.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_PolyDataMapperHolder.hxx"
#include "SALOME_ExtractPolyDataGeometry.h"
#include "VISU_LookupTable.hxx"

#include "VISU_PipeLineUtils.hxx"

#include <vtkPolyDataMapper.h>
#include <vtkPolyData.h>

#include <vtkPlane.h>
#include <vtkImplicitBoolean.h>
#include <vtkImplicitFunction.h>
#include <vtkImplicitFunctionCollection.h>
#include <vtkMath.h>
//#include <vtkExtractPolyDataGeometry.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_PolyDataMapperHolder);


//----------------------------------------------------------------------------
VISU_PolyDataMapperHolder
::VISU_PolyDataMapperHolder():
  myExtractPolyDataGeometry(SALOME_ExtractPolyDataGeometry::New())
{
  if(MYDEBUG) MESSAGE("VISU_PolyDataMapperHolder::VISU_PolyDataMapperHolder - "<<this);

  // Clipping functionality
  myExtractPolyDataGeometry->Delete();
  myExtractPolyDataGeometry->SetStoreMapping(true);

  vtkImplicitBoolean* anImplicitBoolean = vtkImplicitBoolean::New();
  myExtractPolyDataGeometry->SetImplicitFunction(anImplicitBoolean);
  anImplicitBoolean->SetOperationTypeToIntersection();
  anImplicitBoolean->Delete();
  //myExtractPolyDataGeometry->ExtractInsideOn();
}


//----------------------------------------------------------------------------
VISU_PolyDataMapperHolder
::~VISU_PolyDataMapperHolder()
{
  if(MYDEBUG)
    MESSAGE("VISU_PolyDataMapperHolder::~VISU_PolyDataMapperHolder - "<<this);
}


//----------------------------------------------------------------------------
void 
VISU_PolyDataMapperHolder
::ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput)
{
  if(VISU_PolyDataMapperHolder* aMapperHolder = dynamic_cast<VISU_PolyDataMapperHolder*>(theMapperHolder)){
    if(theIsCopyInput)
      SetPolyDataIDMapper(aMapperHolder->GetPolyDataIDMapper());
    
    VISU::CopyPolyDataMapper(GetPolyDataMapper(), 
                             aMapperHolder->GetPolyDataMapper(), 
                             theIsCopyInput);
    myExtractPolyDataGeometry->SetImplicitFunction(aMapperHolder->GetImplicitFunction());
  }
}


//----------------------------------------------------------------------------
unsigned long int
VISU_PolyDataMapperHolder
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(myExtractPolyDataGeometry->GetInput())
    if(vtkDataSet* aDataSet = myExtractPolyDataGeometry->GetOutput())
      aSize = aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::SetPolyDataIDMapper(const VISU::PPolyDataIDMapper& theIDMapper)
{
  myExtractPolyDataGeometry->SetInputConnection(theIDMapper->GetOutputPort());
  myPolyDataIDMapper = theIDMapper;
  SetIDMapper(theIDMapper);
}


//----------------------------------------------------------------------------
const VISU::PPolyDataIDMapper&  
VISU_PolyDataMapperHolder
::GetPolyDataIDMapper()
{
  return myPolyDataIDMapper;
}


//----------------------------------------------------------------------------
vtkPolyData* 
VISU_PolyDataMapperHolder
::GetPolyDataInput()
{
  if(myPolyDataIDMapper)
    return myPolyDataIDMapper->GetPolyDataOutput();

  return NULL;
}


//----------------------------------------------------------------------------
vtkPointSet* 
VISU_PolyDataMapperHolder
::GetClippedInput()
{
  if(myExtractPolyDataGeometry->GetInput())
    myExtractPolyDataGeometry->Update();
  return myExtractPolyDataGeometry->GetOutput();
}

//----------------------------------------------------------------------------
vtkAlgorithmOutput* 
VISU_PolyDataMapperHolder
::GetClippedInputPort()
{
  return myExtractPolyDataGeometry->GetOutputPort();
}


//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::OnCreateMapper()
{
  myPolyDataMapper = vtkPolyDataMapper::New();
  myPolyDataMapper->Delete();
  SetMapper(myPolyDataMapper.GetPointer());
}


//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::SetPolyDataMapper(vtkPolyDataMapper* theMapper)
{
  myPolyDataMapper = theMapper;
  SetMapper(myPolyDataMapper.GetPointer());
}


//----------------------------------------------------------------------------
vtkPolyDataMapper* 
VISU_PolyDataMapperHolder
::GetPolyDataMapper()
{
  GetMapper();
  return myPolyDataMapper.GetPointer();
}


//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::SetLookupTable(VISU_LookupTable* theLookupTable)
{
  myPolyDataMapper->SetLookupTable(theLookupTable);
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_PolyDataMapperHolder
::GetNodeObjID(vtkIdType theID)
{
  vtkIdType anID = myExtractPolyDataGeometry->GetNodeObjId(theID);
  return Superclass::GetNodeObjID(anID);
}

//----------------------------------------------------------------------------
vtkIdType 
VISU_PolyDataMapperHolder
::GetNodeVTKID(vtkIdType theID)
{
  vtkIdType anID = Superclass::GetNodeVTKID(theID);
  return myExtractPolyDataGeometry->GetNodeVTKId(anID);
}

//----------------------------------------------------------------------------
double* 
VISU_PolyDataMapperHolder
::GetNodeCoord(vtkIdType theObjID)
{
  return Superclass::GetNodeCoord(theObjID);
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_PolyDataMapperHolder
::GetElemObjID(vtkIdType theID)
{
  vtkIdType anID = myExtractPolyDataGeometry->GetElemObjId(theID);
  return Superclass::GetElemObjID(anID);
}

//----------------------------------------------------------------------------
vtkIdType
VISU_PolyDataMapperHolder
::GetElemVTKID(vtkIdType theID)
{
  vtkIdType anID = Superclass::GetElemVTKID(theID);
  return myExtractPolyDataGeometry->GetElemVTKId(anID);
}

//----------------------------------------------------------------------------
vtkCell* 
VISU_PolyDataMapperHolder
::GetElemCell(vtkIdType  theObjID)
{
  return Superclass::GetElemCell(theObjID);
}


//------------------------ Clipping planes -----------------------------------
bool 
VISU_PolyDataMapperHolder
::AddClippingPlane(vtkPlane* thePlane)
{
  if (thePlane) {
    if (vtkImplicitBoolean* aBoolean = myExtractPolyDataGeometry->GetImplicitBoolean()) {
      vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
      aFunction->AddItem(thePlane);
      aBoolean->Modified();
      // Check, that at least one cell present after clipping.
      // This check was introduced because of bug IPAL8849.
      vtkDataSet* aClippedDataSet = GetClippedInput();
      if(aClippedDataSet->GetNumberOfCells() < 1)
        return false;
    }
  }
  return true;
}

//----------------------------------------------------------------------------
vtkPlane* 
VISU_PolyDataMapperHolder
::GetClippingPlane(vtkIdType theID)
{
  vtkPlane* aPlane = NULL;
  if(theID >= 0 && theID < GetNumberOfClippingPlanes()){
    if(vtkImplicitBoolean* aBoolean = myExtractPolyDataGeometry->GetImplicitBoolean()){
      vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
      vtkImplicitFunction* aFun = NULL;
      aFunction->InitTraversal();
      for(vtkIdType anID = 0; anID <= theID; anID++)
        aFun = aFunction->GetNextItem();
      aPlane = dynamic_cast<vtkPlane*>(aFun);
    }
  }
  return aPlane;
}

//----------------------------------------------------------------------------
void VISU_PolyDataMapperHolder::RemoveClippingPlane(vtkIdType theID)
{
  if(theID >= 0 && theID < GetNumberOfClippingPlanes()){
    if(vtkImplicitBoolean* aBoolean = myExtractPolyDataGeometry->GetImplicitBoolean()){
      vtkImplicitFunctionCollection* aFunctions = aBoolean->GetFunction();
      aFunctions->RemoveItem(theID);
      aBoolean->Modified();  
    }
  }
}

//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::RemoveAllClippingPlanes()
{
  if(vtkImplicitBoolean* aBoolean = myExtractPolyDataGeometry->GetImplicitBoolean()){
    vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
    aFunction->RemoveAllItems();
    aBoolean->Modified(); // VTK bug
  }
}

//----------------------------------------------------------------------------
vtkIdType
VISU_PolyDataMapperHolder
::GetNumberOfClippingPlanes()
{
  if(vtkImplicitBoolean* aBoolean = myExtractPolyDataGeometry->GetImplicitBoolean()){
    vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
    return aFunction->GetNumberOfItems();
  }
  return 0;
}

//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::SetImplicitFunction(vtkImplicitFunction *theFunction)
{
  myExtractPolyDataGeometry->SetImplicitFunction(theFunction);
} 

//----------------------------------------------------------------------------
vtkImplicitFunction* 
VISU_PolyDataMapperHolder
::GetImplicitFunction()
{
  return myExtractPolyDataGeometry->GetImplicitFunction();
}

//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::SetExtractInside(bool theMode)
{
  myExtractPolyDataGeometry->SetExtractInside(theMode);
}

//----------------------------------------------------------------------------
void
VISU_PolyDataMapperHolder
::SetExtractBoundaryCells(bool theMode)
{
  myExtractPolyDataGeometry->SetExtractBoundaryCells(theMode);
}


//----------------------------------------------------------------------------

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_ScalarMapPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_ScalarMapPL_HeaderFile
#define VISU_ScalarMapPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_MergedPL.hxx"
#include "VISU_ColoredPL.hxx"
#include "VISU_UnstructuredGridPL.hxx"

class VISU_DataSetMapperHolder;
class VISU_ElnoAssembleFilter;
class VISU_AppendFilter;
class VISU_MergeFilter;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_ScalarMapPL : public VISU_MergedPL,
                                              public VISU_ColoredPL,
                                              public VISU_UnstructuredGridPL
{
public:
  vtkTypeMacro(VISU_ScalarMapPL, VISU_ColoredPL);

  static 
  VISU_ScalarMapPL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  void
  Update();

  virtual
  void
  SetSourceGeometry();

  virtual
  int
  AddGeometry(vtkDataSet* theGeometry, const VISU::TName& theGeomName);

  virtual
  vtkDataSet*
  GetGeometry(int theGeomNumber, VISU::TName& theGeomName);

  virtual
  int
  GetNumberOfGeometry();

  bool 
  IsExternalGeometryUsed();

  virtual
  void
  ClearGeometry();

  virtual
  void
  GetSourceRange(double theRange[2]);

  virtual
  void
  SetGaussMetric(VISU::TGaussMetric theGaussMetric);
  
  virtual
  VISU::TGaussMetric
  GetGaussMetric();

  //----------------------------------------------------------------------------
  virtual 
  vtkPointSet* 
  GetMergedInput();

  virtual 
  vtkAlgorithmOutput* 
  GetMergedInputPort();

protected:
  //----------------------------------------------------------------------------
  VISU_ScalarMapPL();
  
  virtual
  ~VISU_ScalarMapPL();

  virtual
  void
  OnCreateMapperHolder();

  virtual
  vtkAlgorithmOutput* 
  InsertCustomPL();

  virtual
  void
  Build();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  void
  SetElnoDisassembleState( bool theIsShrunk );

private:
  VISU_ScalarMapPL(const VISU_ScalarMapPL&);  // Not implemented.
  void operator=(const VISU_ScalarMapPL&);  // Not implemented.

  vtkSmartPointer< VISU_ElnoAssembleFilter > myElnoAssembleFilter;
  vtkSmartPointer< VISU_AppendFilter > myAppendFilter;
  vtkSmartPointer< VISU_MergeFilter > myMergeFilter;
};
  
#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_StreamLine_HeaderFile
#define VISU_StreamLine_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_Streamer.hxx"

class VISU_PIPELINE_EXPORT VISU_StreamLine : public VISU_Streamer
{
public:
  vtkTypeMacro(VISU_StreamLine,VISU_Streamer);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Construct object with step size set to 1.0.
  static VISU_StreamLine *New();

  // Description:
  // Specify the length of a line segment. The length is expressed in terms of
  // elapsed time. Smaller values result in smoother appearing streamlines, but
  // greater numbers of line primitives.
  vtkSetClampMacro(StepLength,double,0.000001,VTK_DOUBLE_MAX);
  vtkGetMacro(StepLength,double);

protected:
  VISU_StreamLine();
  ~VISU_StreamLine() {};

  // Convert streamer array into vtkPolyData
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  // the length of line primitives
  double StepLength;

private:
  VISU_StreamLine(const VISU_StreamLine&);  // Not implemented.
  void operator=(const VISU_StreamLine&);  // Not implemented.
};

#endif

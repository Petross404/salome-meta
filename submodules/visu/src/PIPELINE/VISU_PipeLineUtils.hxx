// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_PipeLineUtils_HeaderFile
#define VISU_PipeLineUtils_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ConvertorUtils.hxx"
#include "VISU_CellDataToPointData.hxx"

#include <vtkProperty.h>
#include <vtkObjectFactory.h>
#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>

#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>

#include <vtkMath.h>

#ifndef MESSAGE
#define MESSAGE(msg) std::cout<<__FILE__<<"["<<__LINE__<<"]::"<<msg<<endl

#undef EXCEPT
#define EXCEPT(msg) QString(QString(__FILE__) + "[" + QString::number(__LINE__) + "]::" + msg)

#undef EXCEPTION
#define EXCEPTION(msg) EXCEPT(msg).latin1()

#endif

class VISU_OpenGLPointSpriteMapper;

namespace VISU
{
  //----------------------------------------------------------------------------
  void
  Mul(const double A[3], 
      double b, 
      double C[3]); // C = A * b
  

  //----------------------------------------------------------------------------
  void
  Sub(const double A[3], 
      const double B[3], 
      double C[3]); // C = A - B


  //----------------------------------------------------------------------------
  template<class TOutputFilter> 
  void
  CellDataToPoint(TOutputFilter* theOutputFilter, 
                  VISU_CellDataToPointData *theCellDataToPointData,
                  vtkDataSet* theDataSet,
                  vtkAlgorithmOutput* theAlgorithmOutput)

  {
    if(VISU::IsDataOnCells(theDataSet)){
      theCellDataToPointData->SetInputConnection(theAlgorithmOutput);
      theCellDataToPointData->PassCellDataOn();
      theOutputFilter->SetInputConnection(theCellDataToPointData->GetOutputPort());
    }else
      theOutputFilter->SetInputConnection(theAlgorithmOutput);
  }

  //----------------------------------------------------------------------------
  //! Checks whether the float values are the same or not
  bool VISU_PIPELINE_EXPORT
  CheckIsSameValue(double theTarget,
                   double theSource);

  //! Checks whether the scalar range is the same or not
  bool VISU_PIPELINE_EXPORT
  CheckIsSameRange(double* theTarget,
                   double* theSource);

  //! Customizes vtkMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyMapper(vtkMapper* theTarget, 
             vtkMapper* theSource,
             bool theIsCopyInput);

  //! Customizes vtkDataSetMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyDataSetMapper(vtkDataSetMapper* theTarget, 
                    vtkDataSetMapper* theSource,
                    bool theIsCopyInput);

  //! Customizes vtkPolyDataMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyPolyDataMapper(vtkPolyDataMapper* theTarget, 
                     vtkPolyDataMapper* theSource,
                     bool theIsCopyInput);

  //! Customizes VISU_OpenGLPointSpriteMapper::ShallowCopy
  void VISU_PIPELINE_EXPORT
  CopyPointSpriteDataMapper(VISU_OpenGLPointSpriteMapper* theTarget, 
                            VISU_OpenGLPointSpriteMapper* theSource,
                            bool theIsCopyInput);


  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  ComputeBoundsParam(double theBounds[6],
                     double theDirection[3], 
                     double theMinPnt[3],
                     double& theMaxBoundPrj, 
                     double& theMinBoundPrj);


  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  DistanceToPosition(double theBounds[6],
                     double theDirection[3], 
                     double theDist, 
                     double thePos[3]);


  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  PositionToDistance(double theBounds[6],
                     double theDirection[3], 
                     double thePos[3], 
                     double& theDist);


  //----------------------------------------------------------------------------
  bool VISU_PIPELINE_EXPORT
  IsQuadraticData(vtkDataSet* theDataSet);

  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  ComputeVisibleBounds(vtkDataSet* theDataSet,
                       double theBounds[6]);

  //----------------------------------------------------------------------------
  void VISU_PIPELINE_EXPORT
  ComputeBoxCenter(double theBounds[6], double theCenter[3]);

  //----------------------------------------------------------------------------
  double VISU_PIPELINE_EXPORT
  ComputeBoxDiagonal(double theBounds[6]);

}

#endif
  

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_LabelPointsFilter.hxx
// Author:  Vitaly Smetannikov
// Module : VISU
//
#ifndef VISU_LabelPointsFilter_HeaderFile
#define VISU_LabelPointsFilter_HeaderFile

#include "VISUPipeline.hxx"
#include <vtkPolyDataAlgorithm.h>

class vtkPolyData;

class VISU_PIPELINE_EXPORT VISU_LabelPointsFilter : public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(VISU_LabelPointsFilter, vtkPolyDataAlgorithm);

  static VISU_LabelPointsFilter* New();

  void SetPointsNb(int theNb);

  int GetPointsNb() const { return myPointsNb; }

protected:
  VISU_LabelPointsFilter();

  virtual ~VISU_LabelPointsFilter();

  virtual int RequestData(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);

  virtual int RequestUpdateExtent(vtkInformation*,
                                  vtkInformationVector**,
                                  vtkInformationVector*);


  void TraverseAndMark (vtkIdList* theWave, 
                        vtkIdList* theWave2, 
                        int* theVisited,
                        vtkIdType* thePointMap,
                        int& theRegionNumber,
                        vtkIdType& thePointNumber,
                        vtkPolyData* theMesh);
  
  int GetRegions(vtkPolyData* theInput, 
                 vtkPolyData* theOutput);

  int myPointsNb;

};


#endif

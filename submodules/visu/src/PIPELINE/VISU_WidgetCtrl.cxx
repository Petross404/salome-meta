// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : VVTK_WidgetCtrl.cxx
//  Author : Peter KURNEV
//  Module : SALOME
//  $Header$
//
#include "VISU_WidgetCtrl.hxx"
//
#include "VISU_ImplicitFunctionWidget.hxx"
#include "VISU_PlanesWidget.hxx"
#include "VISU_SphereWidget.hxx"
//
#include <algorithm>
//
#include <vtkObject.h>
#include <vtkImplicitFunction.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkCommand.h>
//
vtkStandardNewMacro(VISU_WidgetCtrl);

//==================================================================
// function: VISU_WidgetCtrl
// purpose :
//==================================================================
VISU_WidgetCtrl::VISU_WidgetCtrl() 
{
  myDummyWidget=NULL;
  myDisableAll=0;
  myNbWidgets=2;
  myActiveIndex=-1;// 0 - PlanesWidget; 1 - SphereWidget
  myPriority=0.;
  //
  myPlanesWidget=VISU_PlanesWidget::New();
  mySphereWidget=VISU_SphereWidget::New();
  //
  myWidgets[0]=myPlanesWidget;
  myWidgets[1]=mySphereWidget;
  //
  myEventCallbackCommand=vtkCallbackCommand::New();
  myEventCallbackCommand->SetClientData(this); 
  myEventCallbackCommand->SetCallback(VISU_WidgetCtrl::ProcessEvents);
  //
  int i;
  for (i=0; i<myNbWidgets; ++i){
    myWidgets[i]->AddObserver(vtkCommand::EnableEvent,
                              myEventCallbackCommand,
                              myPriority);
    myWidgets[i]->AddObserver(vtkCommand::DisableEvent,
                              myEventCallbackCommand,
                              myPriority);
    myWidgets[i]->AddObserver(vtkCommand::EndInteractionEvent,
                              myEventCallbackCommand,
                              myPriority);
    myWidgets[i]->AddObserver(vtkCommand::StartInteractionEvent,
                              myEventCallbackCommand,
                              myPriority);
    
    myWidgets[i]->AddObserver(vtkCommand::InteractionEvent,
                              myEventCallbackCommand,
                              myPriority);
  }
}
//==================================================================
// function: ~
// purpose :
//==================================================================
VISU_WidgetCtrl::~VISU_WidgetCtrl()
{  
  
  myPlanesWidget->Delete();
  mySphereWidget->Delete();
  //
  for (int i=0; i<myNbWidgets; ++i){
    myWidgets[i]->RemoveObserver(myEventCallbackCommand);
  }
  //
  myEventCallbackCommand->Delete();
}
//==================================================================
// function: GetEnabled 
// purpose :
//==================================================================
int VISU_WidgetCtrl::GetEnabled()
{
  if (HasActiveIndex()) {
    return GetActiveWidget()->GetEnabled();
  }
  return 0;
}
//==================================================================
// function: SetEnabled
// purpose :
//==================================================================
void VISU_WidgetCtrl::SetEnabled(int theFlag)
{
  int iFlag, i;
  //
  myDisableAll=0;
  //
  iFlag=GetEnabled();
  if (iFlag==theFlag) {
    return;
  }
  //
  if (theFlag) {//enabling
    if (HasActiveIndex()) {
      for (i=0; i<myNbWidgets; ++i) {
        iFlag=(i==myActiveIndex) ? 1 : 0;
        myWidgets[i]->SetEnabled(iFlag);
      }
    }
  }
  else {//disabling
    myDisableAll=1;
    for (i=0; i<myNbWidgets; ++i) {
      myWidgets[i]->SetEnabled(0);
    }
  }

  Modified();
}
//==================================================================
// function: ProcessEvents
// purpose :
//==================================================================
void VISU_WidgetCtrl::ProcessEvents(vtkObject* vtkNotUsed(theObject), 
                                    unsigned long theEvent,
                                    void* theClientData, 
                                    void* vtkNotUsed(theCallData))
{
  VISU_WidgetCtrl *aSelf = reinterpret_cast<VISU_WidgetCtrl*>(theClientData);
  switch(theEvent){
  case vtkCommand::DisableEvent:
    if(aSelf->GetDisableAll())
      aSelf->InvokeEvent(theEvent, NULL);
  default:
    aSelf->InvokeEvent(theEvent, NULL);
    break;
  }
}

//==================================================================
// function: GetNbWidgets
// purpose :
//==================================================================
int VISU_WidgetCtrl::GetNbWidgets()const 
{
  return myNbWidgets;
}
//==================================================================
// function: GetDisableAll
// purpose :
//==================================================================
int VISU_WidgetCtrl::GetDisableAll()const
{
  return myDisableAll;
}
//==================================================================
// function: SetActiveIndex
// purpose :
//==================================================================
void VISU_WidgetCtrl::SetActiveIndex(const int theIndex)
{
  myActiveIndex=-1;
  if (theIndex>=0 && theIndex<myNbWidgets){
    myActiveIndex=theIndex;
  }

  Modified();
}
//==================================================================
// function: GetActiveIndex
// purpose :
//==================================================================
int VISU_WidgetCtrl::GetActiveIndex()const
{
  return myActiveIndex;
}
//==================================================================
// function: HasActiveIndex
// purpose :
//==================================================================
bool VISU_WidgetCtrl::HasActiveIndex()const
{
  return (myActiveIndex>=0 && myActiveIndex<myNbWidgets);
}
//==================================================================
// function: IsPlanesActive
// purpose :
//==================================================================
bool VISU_WidgetCtrl::IsPlanesActive()const
{
  return (myActiveIndex==0);
}
//==================================================================
// function: IsSphereActive
// purpose :
//==================================================================
bool VISU_WidgetCtrl::IsSphereActive()const
{
  return (myActiveIndex==1);
}
//==================================================================
// function: GetActiveWidget
// purpose :
//==================================================================
VISU_ImplicitFunctionWidget* VISU_WidgetCtrl::GetActiveWidget()
{
  if (HasActiveIndex()){
    return myWidgets[myActiveIndex];
  }
  return myDummyWidget;
}
//==================================================================
// function: GetWidget
// purpose :
//==================================================================
VISU_ImplicitFunctionWidget* VISU_WidgetCtrl::GetWidget(const int theIndex)
{
  if (theIndex>=0 && theIndex<myNbWidgets) {
    return myWidgets[theIndex];
  }
  return myDummyWidget;
}
//==================================================================
// function: GetPlanesWidget
// purpose :
//==================================================================
VISU_PlanesWidget* VISU_WidgetCtrl::GetPlanesWidget()
{
  return myPlanesWidget;
}
//==================================================================
// function: GetSphereWidget
// purpose :
//==================================================================
VISU_SphereWidget* VISU_WidgetCtrl::GetSphereWidget()
{
  return mySphereWidget;
}
//==================================================================
// function: PlaceWidget
// purpose :
//==================================================================
void VISU_WidgetCtrl::PlaceWidget(double theBounds[6])
{
  for (int i=0; i<myNbWidgets; ++i) { 
    myWidgets[i]->PlaceWidget(theBounds);
  }
}
//==================================================================
// function: SetInteractor
// purpose :
//==================================================================
void VISU_WidgetCtrl::SetInteractor(vtkRenderWindowInteractor* theRWI)
{
  for (int i=0; i<myNbWidgets; ++i) {
    myWidgets[i]->SetInteractor(theRWI);
  }

  Modified();
}
//==================================================================
// function: GetInteractor
// purpose :
//==================================================================
vtkRenderWindowInteractor* VISU_WidgetCtrl::GetInteractor()
{
  return myWidgets[0]->GetInteractor();
}
//==================================================================
// function: SetPlaceFactor
// purpose :
//==================================================================
void VISU_WidgetCtrl::SetPlaceFactor(double theFactor)
{
  for (int i=0; i<myNbWidgets; ++i) {
    myWidgets[i]->SetPlaceFactor(theFactor);
  }

  Modified();
}
//==================================================================
// function: GetPlaceFactor
// purpose :
//==================================================================
double VISU_WidgetCtrl::GetPlaceFactor()
{
  return myWidgets[0]->GetPlaceFactor();
}
//==================================================================
// function: ImplicitFunction
// purpose :
//==================================================================
vtkImplicitFunction* VISU_WidgetCtrl::ImplicitFunction()
{
  return this;
}

//==================================================================
double    
VISU_WidgetCtrl
::EvaluateFunction(double theX[3])
{
  if(VISU_ImplicitFunctionWidget* aWidget = GetActiveWidget()){
    if(vtkImplicitFunction* aFunction = aWidget->ImplicitFunction())
      return aFunction->EvaluateFunction(theX[0],theX[1],theX[2]);
  }
  return 1.0;
}

//==================================================================
void    
VISU_WidgetCtrl
::EvaluateGradient(double theX[3], 
                   double theG[3])
{
  theG[0] = theG[1] = theG[2] = 0.0;
  if(VISU_ImplicitFunctionWidget* aWidget = GetActiveWidget()){
    if(vtkImplicitFunction* aFunction = aWidget->ImplicitFunction())
      aFunction->EvaluateGradient(theX,theG);
  }
}

//==================================================================
unsigned long
VISU_WidgetCtrl
::GetMTime()
{
  unsigned long aTime = Superclass::GetMTime();

  if(vtkImplicitFunction* aFunction = myPlanesWidget->ImplicitFunction())
    aTime = std::max(aTime,aFunction->GetMTime());

  if(vtkImplicitFunction* aFunction = mySphereWidget->ImplicitFunction())
    aTime = std::max(aTime,aFunction->GetMTime());
  
  return aTime;
}

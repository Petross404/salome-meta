// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PointSpriteMapperHolder.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_PointSpriteMapperHolder_HeaderFile
#define VISU_PointSpriteMapperHolder_HeaderFile

#include "VISU_PolyDataMapperHolder.hxx"

class VISU_OpenGLPointSpriteMapper;


//----------------------------------------------------------------------------
class VISU_PointSpriteMapperHolder : public VISU_PolyDataMapperHolder
{
public:
  vtkTypeMacro(VISU_PointSpriteMapperHolder, VISU_PolyDataMapperHolder);

  static 
  VISU_PointSpriteMapperHolder* 
  New();

  //----------------------------------------------------------------------------
  virtual
  void
  ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput);

  //----------------------------------------------------------------------------
  void 
  SetGaussPtsIDMapper(const VISU::PGaussPtsIDMapper& theGaussPtsIDMapper);

  const VISU::PGaussPtsIDMapper&
  GetGaussPtsIDMapper();

  virtual 
  VISU_OpenGLPointSpriteMapper* 
  GetPointSpriteMapper();

protected:
  //----------------------------------------------------------------------------
  VISU_PointSpriteMapperHolder();
  VISU_PointSpriteMapperHolder(const VISU_PointSpriteMapperHolder&);

  virtual
  ~VISU_PointSpriteMapperHolder();

  //----------------------------------------------------------------------------
  virtual
  void
  OnCreateMapper();

private:
  //----------------------------------------------------------------------------
  VISU::PGaussPtsIDMapper myGaussPtsIDMapper;
  vtkSmartPointer<VISU_OpenGLPointSpriteMapper> myPointSpriteMapper;
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_StreamLinesPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_StreamLinesPL_HeaderFile
#define VISU_StreamLinesPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_DeformedShapePL.hxx"

#include <vtkStreamer.h>

class vtkDataSet;
class vtkPointSet;
class VTKViewer_CellCenters;
class VTKViewer_GeometryFilter;
class VISU_MaskPointsFilter;
class vtkStreamLine;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_StreamLinesPL : public VISU_DeformedShapePL
{
public:
  vtkTypeMacro(VISU_StreamLinesPL, VISU_DeformedShapePL);

  static
  VISU_StreamLinesPL*
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  size_t
  SetParams(double theIntStep,
            double thePropogationTime,
            double theStepLength,
            vtkPointSet* theSource,
            double thePercents,
            int theDirection = VTK_INTEGRATE_BOTH_DIRECTIONS);

  virtual
  vtkPointSet*
  GetSource();

  virtual
  double
  GetUsedPoints();

  virtual
  double 
  GetIntegrationStep();

  virtual
  double
  GetPropagationTime();

  virtual
  double
  GetStepLength();

  virtual
  int
  GetDirection();

  virtual
  vtkDataSet* 
  GetStreamerSource();

  virtual
  double 
  GetVelocityCoeff();

  virtual
  double
  GetMaxIntegrationStep();

  virtual
  double
  GetMinIntegrationStep();

  virtual
  double
  GetMinStepLength();

  virtual
  double
  GetMaxStepLength();

  virtual
  double
  GetMinPropagationTime();

  virtual
  double
  GetMaxPropagationTime();

  virtual
  double
  GetBasePropagationTime();

public:
  virtual
  vtkAlgorithmOutput* 
  InsertCustomPL();

  virtual
  void
  Init();

  virtual
  void
  Build();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  void
  SetMapScale(double theMapScale = 1.0);

public:
  static
  double
  GetMaxIntegrationStep(vtkDataSet* theDataSet);

  static
  double
  GetMinIntegrationStep(vtkDataSet* theDataSet, 
                        double thePercents);
  static
  double
  GetBaseIntegrationStep(vtkDataSet* theDataSet, 
                         double thePercents);
  
  static 
  double
  GetMinPropagationTime(vtkDataSet* theDataSet, 
                        double thePercents);

  static
  double
  GetMaxPropagationTime(vtkDataSet* theDataSet);

  static
  double
  GetBasePropagationTime(vtkDataSet* theDataSet);

  static
  double
  GetMinStepLength(vtkDataSet* theDataSet, 
                   double thePercents);

  static
  double
  GetMaxStepLength(vtkDataSet* theDataSet);

  static
  double
  GetBaseStepLength(vtkDataSet* theDataSet, 
                    double thePercents);

  static
  double
  GetVelocityCoeff(vtkDataSet* theDataSet);

  static
  size_t
  IsPossible(vtkPointSet* theDataSet);

protected:
  VISU_StreamLinesPL();

  virtual
  ~VISU_StreamLinesPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  static 
  double
  GetNecasseryMemorySize(vtkIdType theNbOfPoints, 
                         double theStepLength, 
                         double thePropogationTime, 
                         double thePercents);

  static
  size_t
  FindPossibleParams(vtkDataSet* theDataSet, 
                     double& theStepLength, 
                     double& thePropogationTime, 
                     double& thePercents);
  
  static 
  double
  CorrectIntegrationStep(double theStep, 
                         vtkDataSet* theDataSet, 
                         double thePercents);

  static 
  double
  CorrectPropagationTime(double thePropagationTime, 
                         vtkDataSet* theDataSet, 
                         double thePercents);

  static
  double
  CorrectStepLength(double theStep, 
                    vtkDataSet* theDataSet, 
                    double thePercents);

  static
  double
  GetUsedPointsDefault();

  vtkStreamLine* myStream;
  vtkPointSet* mySource;
  VTKViewer_CellCenters* myCenters;
  VTKViewer_GeometryFilter *myGeomFilter;
  VISU_MaskPointsFilter *myPointsFilter;
  double myPercents;

private:
  VISU_StreamLinesPL(const VISU_StreamLinesPL&);  // Not implemented.
  void operator=(const VISU_StreamLinesPL&);  // Not implemented.
};


#endif
 

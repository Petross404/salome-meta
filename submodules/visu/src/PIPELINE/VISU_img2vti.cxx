// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_img2vti.cxx
// Author:  Oleg UVAROV
// Module : VISU
//
#include <vtkImageData.h>
#include <vtkBMPReader.h>
#include <vtkPNGReader.h>
#include <vtkJPEGReader.h>
#include <vtkXMLImageDataWriter.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

int main( int argc, char** argv )
{
  if(MYDEBUG) std::cout << "Converting the image to VTI format" << std::endl;

  if( argc != 4 )
  {
    std::cout << "Usage : VISU_img2vti ImageFormat InputFileName OutputFileName" << std::endl;
    return 1;
  }

  if(MYDEBUG){
    std::cout << argv[1] << std::endl;
    std::cout << argv[2] << std::endl;
    std::cout << argv[3] << std::endl;
  }

  vtkImageReader2* aReader = NULL;
  if( !strcmp( argv[1], "BMP" ) || !strcmp( argv[1], "bmp" ) )
  {
    if(MYDEBUG) std::cout << "Input image format - BMP" << std::endl;
    aReader = vtkBMPReader::New();
  }
  else if( !strcmp( argv[1], "PNG" ) || !strcmp( argv[1], "png" ) )
  {
    if(MYDEBUG) std::cout << "Input image format - PNG" << std::endl;
    aReader = vtkPNGReader::New();
  }
  else if( !strcmp( argv[1], "JPG" ) || !strcmp( argv[1], "jpg" ) )
  {
    if(MYDEBUG) std::cout << "Input image format - JPG" << std::endl;
    aReader = vtkJPEGReader::New();
  }
  else
  {
    std::cout << "Unknown input image format... Must be BMP, PNG or JPG." << std::endl;
    return 1;
  }
  aReader->SetFileName( argv[2] );
  aReader->Update();

  vtkXMLImageDataWriter* aWriter = vtkXMLImageDataWriter::New();
  aWriter->SetInputConnection( aReader->GetOutputPort() );
  aWriter->SetFileName( argv[3] );
  aWriter->Write();

  aWriter->Delete();
  aReader->Delete();

  return 0;
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_ElnoDisassembleFilter_H
#define VISU_ElnoDisassembleFilter_H

#include "VISUPipeline.hxx"

#include <vtkUnstructuredGridAlgorithm.h>

class VISU_PIPELINE_EXPORT VISU_ElnoDisassembleFilter : public vtkUnstructuredGridAlgorithm
{
public:
  typedef vtkUnstructuredGridAlgorithm Superclass;

  static VISU_ElnoDisassembleFilter *New();

  void SetShrinkFactor( double theValue );
  double GetShrinkFactor();

protected:
  VISU_ElnoDisassembleFilter();
  ~VISU_ElnoDisassembleFilter();

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  double myShrinkFactor;

private:
  VISU_ElnoDisassembleFilter(const VISU_ElnoDisassembleFilter&);  // Not implemented.
  void operator=(const VISU_ElnoDisassembleFilter&);  // Not implemented.
};

#endif

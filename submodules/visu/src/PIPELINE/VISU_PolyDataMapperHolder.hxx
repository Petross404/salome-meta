// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PolyDataMapperHolder.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_PolyDataMapperHolder_HeaderFile
#define VISU_PolyDataMapperHolder_HeaderFile

#include "VISU_MapperHolder.hxx"

class vtkPolyDataMapper;
class vtkPolyData;
class SALOME_ExtractPolyDataGeometry;


//----------------------------------------------------------------------------
class VISU_PolyDataMapperHolder : public VISU_MapperHolder
{
public:
  vtkTypeMacro(VISU_PolyDataMapperHolder, VISU_MapperHolder);

  static 
  VISU_PolyDataMapperHolder* 
  New();

  //----------------------------------------------------------------------------
  virtual
  void
  ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput);

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  //----------------------------------------------------------------------------
  void 
  SetPolyDataIDMapper(const VISU::PPolyDataIDMapper& theIDMapper);

  const VISU::PPolyDataIDMapper&
  GetPolyDataIDMapper();

  virtual
  vtkPolyData* 
  GetPolyDataInput();

  virtual 
  vtkPolyDataMapper* 
  GetPolyDataMapper();

  //----------------------------------------------------------------------------
  virtual
  vtkIdType
  GetNodeObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetNodeVTKID(vtkIdType theID);

  virtual
  double* 
  GetNodeCoord(vtkIdType theObjID);

  virtual
  vtkIdType
  GetElemObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetElemVTKID(vtkIdType theID);

  virtual
  vtkCell*
  GetElemCell(vtkIdType theObjID);

  //----------------------------------------------------------------------------
  virtual
  void
  SetImplicitFunction(vtkImplicitFunction *theFunction);

  virtual
  vtkImplicitFunction* 
  GetImplicitFunction();

  virtual
  void
  SetExtractInside(bool theMode);

  virtual
  void
  SetExtractBoundaryCells(bool theMode);

  //----------------------------------------------------------------------------
  // Clipping planes
  virtual
  void 
  RemoveAllClippingPlanes();

  virtual
  vtkIdType
  GetNumberOfClippingPlanes();

  virtual
  bool
  AddClippingPlane(vtkPlane* thePlane);

  virtual
  vtkPlane* 
  GetClippingPlane(vtkIdType theID);
  
  void RemoveClippingPlane(vtkIdType theID);

protected:
  //----------------------------------------------------------------------------
  VISU_PolyDataMapperHolder();
  VISU_PolyDataMapperHolder(const VISU_PolyDataMapperHolder&);

  virtual
  ~VISU_PolyDataMapperHolder();

  //----------------------------------------------------------------------------
  virtual
  void
  OnCreateMapper();

  void 
  SetPolyDataMapper(vtkPolyDataMapper* theMapper);

  //----------------------------------------------------------------------------
  virtual
  void
  SetLookupTable(VISU_LookupTable* theLookupTable);

  virtual
  vtkPointSet* 
  GetClippedInput();

  virtual
  vtkAlgorithmOutput* 
  GetClippedInputPort();

private:
  //----------------------------------------------------------------------------
  VISU::PPolyDataIDMapper myPolyDataIDMapper;
  vtkSmartPointer<vtkPolyDataMapper> myPolyDataMapper;

protected:
  vtkSmartPointer<SALOME_ExtractPolyDataGeometry> myExtractPolyDataGeometry; //!< Clipping
};

#endif

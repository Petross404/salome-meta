// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_LabelPointsFilter.cxx
// Author:  Vitaly Smetannikov
// Module : VISU
//
#include "VISU_LabelPointsFilter.hxx"

#include <vtkPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkFeatureEdges.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkGeometryFilter.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkMath.h>

#include <set>
#include <vector>


#define CONTAINS(SET, PT) (SET.find(PT) != SET.end())

struct ltIdType 
{
  bool operator()(const vtkIdType a1, const vtkIdType a2) const
  {
    return a1 < a2;
  }
};



//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_LabelPointsFilter);


//----------------------------------------------------------------------------
void VISU_LabelPointsFilter::SetPointsNb(int theNb)
{
  if (myPointsNb == theNb) return;
  myPointsNb = (theNb < 1)? 1:theNb;
  Modified();
}

//----------------------------------------------------------------------------
VISU_LabelPointsFilter::VISU_LabelPointsFilter():
  vtkPolyDataAlgorithm(),
  myPointsNb(3)
{
}

//----------------------------------------------------------------------------
VISU_LabelPointsFilter::~VISU_LabelPointsFilter()
{}



//----------------------------------------------------------------------------
int VISU_LabelPointsFilter::RequestData(vtkInformation* vtkNotUsed(request),
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkPolyData* input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData* output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  GetRegions(input, output);

  return 1;
}

int VISU_LabelPointsFilter::RequestUpdateExtent(vtkInformation* vtkNotUsed(request),
                                                vtkInformationVector** inputVector,
                                                vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  vtkPolyData* input = vtkPolyData::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData* output = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  GetRegions(input, output);
  return 1;
}



int VISU_LabelPointsFilter::GetRegions(vtkPolyData* theInput, 
                                       vtkPolyData* theOutput)
{
  vtkIdType cellId, i;
  vtkIdType numPts, numCells;
  vtkPoints *inPts;

  vtkPointData *aInpPD=theInput->GetPointData(), *aOutPD=theOutput->GetPointData();
  vtkCellData *aInpCD=theInput->GetCellData(), *aOutCD=theOutput->GetCellData();
  
  //  Check input/allocate storage
  //
  inPts = theInput->GetPoints();

  if (inPts == NULL)
    return 1;

  numPts = inPts->GetNumberOfPoints();
  numCells = theInput->GetNumberOfCells();

  if ( numPts < 1 || numCells < 1 )
    return 1;

  // Build cell structure
  //
  vtkPolyData* aMesh = vtkPolyData::New();
  aMesh->CopyStructure(theInput);
  aMesh->BuildLinks();

  // Initialize.  Keep track of points and cells visited.
  //
  vtkIdTypeArray* aRegionSizes = vtkIdTypeArray::New();
  int* aVisited = new int[numCells];
  for ( i=0; i < numCells; i++ )
     aVisited[i] = -1;

  vtkIdType* aPointMap = new vtkIdType[numPts];  
  for ( i=0; i < numPts; i++ )
    aPointMap[i] = -1;

  vtkPoints* newPts = vtkPoints::New();
  newPts->Allocate(numPts);

  // Traverse all cells marking those visited.  Each new search
  // starts a new connected region. Connected region grows 
  // using a connected wave propagation.
  //
  vtkIdList* aWave = vtkIdList::New();
  aWave->Allocate(numPts/4+1,numPts);
  vtkIdList* aWave2 = vtkIdList::New();
  aWave2->Allocate(numPts/4+1,numPts);

  vtkIdType aPointNumber = 0;
  int aRegionNumber = 0;

  vtkIdList* aCellIds = vtkIdList::New(); 
  aCellIds->Allocate(8, VTK_CELL_SIZE);
  vtkIdList* aPointIds = vtkIdList::New(); 
  aPointIds->Allocate(8, VTK_CELL_SIZE);

  //  vtkIdType aNumCellsInRegion;

  aOutPD->CopyAllocate(aInpPD);
  aOutCD->CopyAllocate(aInpCD);

  //visit all cells marking with region number
  for (cellId=0; cellId < numCells; cellId++) {
    if ( aVisited[cellId] < 0 ) {
      aWave->InsertNextId(cellId);
      aPointNumber = 0;
      TraverseAndMark(aWave, aWave2, aVisited, aPointMap, 
                      aRegionNumber, aPointNumber, aMesh);
      
      if (aPointNumber >= myPointsNb) {
        std::set<vtkIdType, ltIdType> aIdxSet;
        for (i=0; i < numPts; i++) {
          if ( aPointMap[i] > -1 ) {
            aIdxSet.insert(i);
            aPointMap[i] = -1;
          }
        }
        std::vector<vtkIdType> aIdx(aIdxSet.begin(), aIdxSet.end());
        int aActualPts = aIdx.size();
        int aNewId;
        if (myPointsNb > 2) {
          int k = aActualPts/(myPointsNb - 1);
          int count;
          for (i=0, count = 0; i < aActualPts; i+=k, count++) {
            aNewId = newPts->InsertNextPoint(inPts->GetPoint(aIdx[i]));
            aOutPD->CopyData(aInpPD, aIdx[i], aNewId);
          }
          if (count < myPointsNb) {
            aNewId = newPts->InsertNextPoint(inPts->GetPoint(aIdx[aActualPts - 1]));
            aOutPD->CopyData(aInpPD, aIdx[aActualPts - 1], aNewId);
          }
        } else {          
          aNewId = newPts->InsertNextPoint(inPts->GetPoint(aIdx[0]));
          aOutPD->CopyData(aInpPD, aIdx[0], aNewId);
          if (myPointsNb == 2) {
            aNewId = newPts->InsertNextPoint(inPts->GetPoint(aIdx[aActualPts - 1]));
            aOutPD->CopyData(aInpPD, aIdx[aActualPts - 1], aNewId);
          }
        }
      }
      aWave->Reset();
      aWave2->Reset(); 
    }
  }

  aWave->Delete();
  aWave2->Delete();

  theOutput->SetPoints(newPts);
  newPts->Delete();


  delete [] aVisited;
  delete [] aPointMap;
  aMesh->Delete();
  theOutput->Squeeze();
  aCellIds->Delete();
  aPointIds->Delete();

  return aRegionSizes->GetMaxId() + 1;
}


// Mark current cell as visited and assign region number.  Note:
// traversal occurs across shared vertices.
//
void VISU_LabelPointsFilter::TraverseAndMark (vtkIdList* theWave, 
                                              vtkIdList* theWave2, 
                                              int* theVisited,
                                              vtkIdType* thePointMap,
                                              int& theRegionNumber,
                                              vtkIdType& thePointNumber,
                                              vtkPolyData* theMesh)
{
  vtkIdType cellId, ptId, numIds, i;
  int j, k;
  vtkIdType *pts, *cells, npts;
  vtkIdList *tmpWave;
  unsigned short ncells;
  vtkIdList* aNeighborCellPointIds = vtkIdList::New();


  while ( (numIds=theWave->GetNumberOfIds()) > 0 ) {
    for ( i=0; i < numIds; i++ ) {
      cellId = theWave->GetId(i);
      if ( theVisited[cellId] < 0 ) {
        theVisited[cellId] = theRegionNumber;
        theMesh->GetCellPoints(cellId, npts, pts);
        
        for (j=0; j < npts; j++) {
          if ( thePointMap[ptId=pts[j]] < 0 ) {
            thePointMap[ptId] = thePointNumber++;
          }       
          theMesh->GetPointCells(ptId,ncells,cells);
          
          // check connectivity criterion (geometric + scalar)
          for (k=0; k < ncells; k++) {
            cellId = cells[k];
            theWave2->InsertNextId(cellId);
            //              }
          }//for all cells using this point
        }//for all points of this cell
      }//if cell not yet visited
    }//for all cells in this wave
    
    tmpWave = theWave;
    theWave = theWave2;
    theWave2 = tmpWave;
    tmpWave->Reset();
  } //while wave is not empty
}

/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile$

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME VISU_CellDataToPointData - map cell data to point data
// .SECTION Description
// VISU_CellDataToPointData is a filter that transforms cell data (i.e., data
// specified per cell) into point data (i.e., data specified at cell
// points). The method of transformation is based on averaging the data
// values of all cells using a particular point. Optionally, the input cell
// data can be passed through to the output as well.

// .SECTION Caveats
// This filter is an abstract filter, that is, the output is an abstract type
// (i.e., vtkDataSet). Use the convenience methods (e.g.,
// GetPolyDataOutput(), GetStructuredPointsOutput(), etc.) to get the type
// of output you want.

// .SECTION See Also
// vtkPointData vtkCellData vtkPointDataToCellData

// note from SALOME:
// This file is a part of VTK library
// It has been renamed and modified for SALOME project

#ifndef __VISU_CellDataToPointData_h
#define __VISU_CellDataToPointData_h

#include "VISUPipeline.hxx"
#include <vtkDataSetAlgorithm.h>

class vtkDataSet;

class VISU_PIPELINE_EXPORT VISU_CellDataToPointData : public vtkDataSetAlgorithm
{
public:
  static VISU_CellDataToPointData *New();
  vtkTypeMacro(VISU_CellDataToPointData,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Control whether the input cell data is to be passed to the output. If
  // on, then the input cell data is passed through to the output; otherwise,
  // only generated point data is placed into the output.
  vtkSetMacro(PassCellData,int);
  vtkGetMacro(PassCellData,int);
  vtkBooleanMacro(PassCellData,int);

protected:
  VISU_CellDataToPointData();
  ~VISU_CellDataToPointData() {};

  virtual int RequestData(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);

  int PassCellData;
private:
  VISU_CellDataToPointData(const VISU_CellDataToPointData&);  // Not implemented.
  void operator=(const VISU_CellDataToPointData&);  // Not implemented.
};

#endif



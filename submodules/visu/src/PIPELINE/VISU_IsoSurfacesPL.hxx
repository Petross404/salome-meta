// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_IsoSurfacesPL_HeaderFile
#define VISU_IsoSurfacesPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ScalarMapPL.hxx"

class vtkContourFilter;
class VISU_CellDataToPointData;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_IsoSurfacesPL : public VISU_ScalarMapPL
{
public:
  vtkTypeMacro(VISU_IsoSurfacesPL, VISU_ScalarMapPL);

  static
  VISU_IsoSurfacesPL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  int
  GetNbParts();

  virtual double GetValue(int i);

  virtual
  void
  SetNbParts(int theNb = 10);

  virtual
  void
  SetScalarRange( double theRange[2] );

  virtual
  void
  SetRange(double theRange[2], bool theIsForced = false);

  virtual
  double
  GetMin();

  virtual
  double
  GetMax();

  virtual
  void
  SetRangeFixed(bool theIsFixed);

  virtual
  bool
  IsRangeFixed();
  
public:
  virtual
  void
  Init();

  virtual
  void
  Build();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  vtkAlgorithmOutput* 
  InsertCustomPL();

  virtual
  void
  SetMapScale(double theMapScale = 1.0);

protected:
  VISU_IsoSurfacesPL();

  virtual
  ~VISU_IsoSurfacesPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  int myNbParts;
  double myRange[2];
  bool myIsRangeFixed;
  VISU_CellDataToPointData* myCellDataToPointData;
  vtkContourFilter *myContourFilter;

private:
  VISU_IsoSurfacesPL(const VISU_IsoSurfacesPL&);;  // Not implemented.
  void operator=(const VISU_IsoSurfacesPL&);  // Not implemented.

};


#endif

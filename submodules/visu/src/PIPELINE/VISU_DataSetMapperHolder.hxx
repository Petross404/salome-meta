// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_DataSetMapperHolder.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_DataSetMapperHolder_HeaderFile
#define VISU_DataSetMapperHolder_HeaderFile

#include "VISU_MapperHolder.hxx"

class vtkDataSetMapper;
class vtkUnstructuredGrid;
class SALOME_ExtractGeometry;
class VISU_ElnoDisassembleFilter;


//----------------------------------------------------------------------------
class VISU_DataSetMapperHolder : public VISU_MapperHolder
{
public:
  vtkTypeMacro(VISU_DataSetMapperHolder, VISU_MapperHolder);

  static 
  VISU_DataSetMapperHolder* 
  New();

  //----------------------------------------------------------------------------
  virtual
  void
  ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput);

  void
  SetElnoDisassembleState( bool theIsShrunk );

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  //----------------------------------------------------------------------------
  void 
  SetUnstructuredGridIDMapper(const VISU::PUnstructuredGridIDMapper& theIDMapper);

  const VISU::PUnstructuredGridIDMapper&
  GetUnstructuredGridIDMapper();

  virtual
  vtkUnstructuredGrid* 
  GetUnstructuredGridInput();

  virtual 
  vtkDataSetMapper* 
  GetDataSetMapper();

  //----------------------------------------------------------------------------
  virtual
  vtkIdType
  GetNodeObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetNodeVTKID(vtkIdType theID);

  virtual
  double* 
  GetNodeCoord(vtkIdType theObjID);

  virtual
  vtkIdType
  GetElemObjID(vtkIdType theID);

  virtual
  vtkIdType
  GetElemVTKID(vtkIdType theID);

  virtual
  vtkCell*
  GetElemCell(vtkIdType theObjID);

  //----------------------------------------------------------------------------
  virtual
  void
  SetImplicitFunction(vtkImplicitFunction *theFunction);

  virtual
  vtkImplicitFunction* 
  GetImplicitFunction();

  virtual
  void
  SetExtractInside(bool theMode);

  virtual
  void
  SetExtractBoundaryCells(bool theMode);

  //----------------------------------------------------------------------------
  // Clipping planes
  virtual
  void 
  RemoveAllClippingPlanes();

  virtual
  vtkIdType
  GetNumberOfClippingPlanes();

  virtual
  bool
  AddClippingPlane(vtkPlane* thePlane);

  virtual
  vtkPlane* 
  GetClippingPlane(vtkIdType theID);

  virtual void RemoveClippingPlane(vtkIdType theID);

protected:
  //----------------------------------------------------------------------------
  VISU_DataSetMapperHolder();
  VISU_DataSetMapperHolder(const VISU_DataSetMapperHolder&);

  virtual
  ~VISU_DataSetMapperHolder();

  //----------------------------------------------------------------------------
  virtual
  void
  OnCreateMapper();

  void 
  SetDataSetMapper(vtkDataSetMapper* theMapper);

  //----------------------------------------------------------------------------
  virtual
  void
  SetLookupTable(VISU_LookupTable* theLookupTable);

  virtual
  vtkPointSet* 
  GetClippedInput();

  virtual
  vtkAlgorithmOutput* 
  GetClippedInputPort();

private:
  //----------------------------------------------------------------------------
  VISU::PUnstructuredGridIDMapper myUnstructuredGridIDMapper;
  vtkSmartPointer< VISU_ElnoDisassembleFilter > myElnoDisassembleFilter; //!< Handling ELNO data
  vtkSmartPointer< SALOME_ExtractGeometry > myExtractGeometry; //!< Clipping
  vtkSmartPointer< vtkDataSetMapper > myDataSetMapper;
};

#endif

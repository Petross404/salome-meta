// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_DataSetMapperHolder.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_DataSetMapperHolder.hxx"
#include "SALOME_ExtractGeometry.h"
#include "VISU_ElnoDisassembleFilter.hxx"
#include "VISU_LookupTable.hxx"

#include "VISU_PipeLineUtils.hxx"

#include <vtkDataSetMapper.h>
#include <vtkUnstructuredGrid.h>

#include <vtkPlane.h>
#include <vtkImplicitBoolean.h>
#include <vtkImplicitFunction.h>
#include <vtkImplicitFunctionCollection.h>
#include <vtkMath.h>

#include <cmath>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_DataSetMapperHolder);


//----------------------------------------------------------------------------
VISU_DataSetMapperHolder
::VISU_DataSetMapperHolder():
  myElnoDisassembleFilter( VISU_ElnoDisassembleFilter::New() ),
  myExtractGeometry( SALOME_ExtractGeometry::New() )
{
  if(MYDEBUG) MESSAGE("VISU_DataSetMapperHolder::VISU_DataSetMapperHolder - "<<this);

  myElnoDisassembleFilter->Delete();

  // Clipping functionality
  myExtractGeometry->Delete();
  myExtractGeometry->SetStoreMapping(true);

  vtkImplicitBoolean* anImplicitBoolean = vtkImplicitBoolean::New();
  myExtractGeometry->SetImplicitFunction(anImplicitBoolean);
  anImplicitBoolean->SetOperationTypeToIntersection();
  anImplicitBoolean->Delete();
}


//----------------------------------------------------------------------------
VISU_DataSetMapperHolder
::~VISU_DataSetMapperHolder()
{
  if(MYDEBUG)
    MESSAGE("VISU_DataSetMapperHolder::~VISU_DataSetMapperHolder - "<<this);
}


//----------------------------------------------------------------------------
void 
VISU_DataSetMapperHolder
::ShallowCopy(VISU_MapperHolder *theMapperHolder,
              bool theIsCopyInput)
{
  if(VISU_DataSetMapperHolder* aMapperHolder = dynamic_cast<VISU_DataSetMapperHolder*>(theMapperHolder)){
    if(theIsCopyInput)
      SetUnstructuredGridIDMapper(aMapperHolder->GetUnstructuredGridIDMapper());
    
    VISU::CopyDataSetMapper(GetDataSetMapper(), 
                            aMapperHolder->GetDataSetMapper(), 
                            theIsCopyInput);
    myExtractGeometry->SetImplicitFunction(aMapperHolder->GetImplicitFunction());
  }
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetElnoDisassembleState( bool theIsShrunk )
{
  double aShrinkFactor = std::abs( myElnoDisassembleFilter->GetShrinkFactor() );
  if ( theIsShrunk )
    myElnoDisassembleFilter->SetShrinkFactor( aShrinkFactor );
  else
    myElnoDisassembleFilter->SetShrinkFactor( -aShrinkFactor );
}


//----------------------------------------------------------------------------
unsigned long int
VISU_DataSetMapperHolder
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(myExtractGeometry->GetInput())
    if(vtkDataSet* aDataSet = myExtractGeometry->GetOutput())
      aSize = aDataSet->GetActualMemorySize() * 1024;
  
  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetUnstructuredGridIDMapper(const VISU::PUnstructuredGridIDMapper& theIDMapper)
{
  myElnoDisassembleFilter->SetInputConnection( theIDMapper->GetOutputPort() );
  myExtractGeometry->SetInputConnection( myElnoDisassembleFilter->GetOutputPort() );
  myUnstructuredGridIDMapper = theIDMapper;
  SetIDMapper( theIDMapper );
}


//----------------------------------------------------------------------------
const VISU::PUnstructuredGridIDMapper&  
VISU_DataSetMapperHolder
::GetUnstructuredGridIDMapper()
{
  return myUnstructuredGridIDMapper;
}


//----------------------------------------------------------------------------
vtkUnstructuredGrid* 
VISU_DataSetMapperHolder
::GetUnstructuredGridInput()
{
  if(myUnstructuredGridIDMapper)
    return myUnstructuredGridIDMapper->GetUnstructuredGridOutput();

  return NULL;
}


//----------------------------------------------------------------------------
vtkPointSet* 
VISU_DataSetMapperHolder
::GetClippedInput()
{
  if(myExtractGeometry->GetInput())
    myExtractGeometry->Update();
  return myExtractGeometry->GetOutput();
}

//----------------------------------------------------------------------------
vtkAlgorithmOutput* 
VISU_DataSetMapperHolder
::GetClippedInputPort()
{
  return myExtractGeometry->GetOutputPort();
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::OnCreateMapper()
{
  myDataSetMapper = vtkDataSetMapper::New();
  myDataSetMapper->Delete();
  SetMapper(myDataSetMapper.GetPointer());
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetDataSetMapper(vtkDataSetMapper* theMapper)
{
  myDataSetMapper = theMapper;
  SetMapper(myDataSetMapper.GetPointer());
}


//----------------------------------------------------------------------------
vtkDataSetMapper* 
VISU_DataSetMapperHolder
::GetDataSetMapper()
{
  GetMapper();
  return myDataSetMapper.GetPointer();
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetLookupTable(VISU_LookupTable* theLookupTable)
{
  myDataSetMapper->SetLookupTable(theLookupTable);
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_DataSetMapperHolder
::GetNodeObjID(vtkIdType theID)
{
  vtkIdType anID = myExtractGeometry->GetNodeObjId(theID);
  return Superclass::GetNodeObjID(anID);
}

//----------------------------------------------------------------------------
vtkIdType 
VISU_DataSetMapperHolder
::GetNodeVTKID(vtkIdType theID)
{
  vtkIdType anID = Superclass::GetNodeVTKID(theID);
  return myExtractGeometry->GetNodeVTKId(anID);
}

//----------------------------------------------------------------------------
double* 
VISU_DataSetMapperHolder
::GetNodeCoord(vtkIdType theObjID)
{
  return Superclass::GetNodeCoord(theObjID);
}


//----------------------------------------------------------------------------
vtkIdType 
VISU_DataSetMapperHolder
::GetElemObjID(vtkIdType theID)
{
  vtkIdType anID = myExtractGeometry->GetElemObjId(theID);
  return Superclass::GetElemObjID(anID);
}

//----------------------------------------------------------------------------
vtkIdType
VISU_DataSetMapperHolder
::GetElemVTKID(vtkIdType theID)
{
  vtkIdType anID = Superclass::GetElemVTKID(theID);
  return myExtractGeometry->GetElemVTKId(anID);
}

//----------------------------------------------------------------------------
vtkCell* 
VISU_DataSetMapperHolder
::GetElemCell(vtkIdType  theObjID)
{
  return Superclass::GetElemCell(theObjID);
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetImplicitFunction(vtkImplicitFunction *theFunction)
{
  myExtractGeometry->SetImplicitFunction(theFunction);
} 

//----------------------------------------------------------------------------
vtkImplicitFunction * 
VISU_DataSetMapperHolder
::GetImplicitFunction()
{
  return myExtractGeometry->GetImplicitFunction();
}

//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::RemoveAllClippingPlanes()
{
  if(vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()){
    vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
    aFunction->RemoveAllItems();
    aBoolean->Modified(); // VTK bug
  }
}

//----------------------------------------------------------------------------
vtkIdType
VISU_DataSetMapperHolder
::GetNumberOfClippingPlanes()
{
  if(vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()){
    vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
    return aFunction->GetNumberOfItems();
  }
  return 0;
}

//----------------------------------------------------------------------------
bool 
VISU_DataSetMapperHolder
::AddClippingPlane(vtkPlane* thePlane)
{
  if (thePlane) {
    if (vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()) {
      vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
      aFunction->AddItem(thePlane);
      aBoolean->Modified();
      // Check, that at least one cell present after clipping.
      // This check was introduced because of bug IPAL8849.
      vtkDataSet* aClippedDataSet = GetClippedInput();
      if(aClippedDataSet->GetNumberOfCells() < 1)
        return false;
    }
  }
  return true;
}

//----------------------------------------------------------------------------
vtkPlane* 
VISU_DataSetMapperHolder
::GetClippingPlane(vtkIdType theID)
{
  vtkPlane* aPlane = NULL;
  if(theID >= 0 && theID < GetNumberOfClippingPlanes()){
    if(vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()){
      vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
      vtkImplicitFunction* aFun = NULL;
      aFunction->InitTraversal();
      for(vtkIdType anID = 0; anID <= theID; anID++)
        aFun = aFunction->GetNextItem();
      aPlane = dynamic_cast<vtkPlane*>(aFun);
    }
  }
  return aPlane;
}

//----------------------------------------------------------------------------
void VISU_DataSetMapperHolder::RemoveClippingPlane(vtkIdType theID)
{
  if(theID >= 0 && theID < GetNumberOfClippingPlanes()){
    if(vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()){
      vtkImplicitFunctionCollection* aFunctions = aBoolean->GetFunction();
      aFunctions->RemoveItem(theID);
      aBoolean->Modified();
    }
  }
}


//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetExtractInside(bool theMode)
{
  myExtractGeometry->SetExtractInside(theMode);
}

//----------------------------------------------------------------------------
void
VISU_DataSetMapperHolder
::SetExtractBoundaryCells(bool theMode)
{
  myExtractGeometry->SetExtractBoundaryCells(theMode);
}


//----------------------------------------------------------------------------

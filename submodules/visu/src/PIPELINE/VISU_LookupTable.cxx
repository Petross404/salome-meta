// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_LookupTable.cxx
//  Author : Vitaliy Smetannikov
//  Module : VISU
//
#include "VISU_LookupTable.hxx"

#include <vtkObjectFactory.h>
#include <vtkBitArray.h>
#include <math.h>

using namespace std;


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_LookupTable);


//----------------------------------------------------------------------------
VISU_LookupTable
::VISU_LookupTable(int sze, int ext):
  vtkLookupTable(sze, ext),
  myScale(1.0),
  myBicolor(false),
  myHasMarkedValues(false)
{}

//----------------------------------------------------------------------------
namespace
{
  inline
  void
  CopyColor( unsigned char* theTaget, const unsigned char* theSource )
  {
    theTaget[0] = theSource[0];
    theTaget[1] = theSource[1];
    theTaget[2] = theSource[2];
  }
}


//----------------------------------------------------------------------------
void
VISU_LookupTable
::MarkValueByColor( double theValue,
                    unsigned char* theColor )
{
  vtkIdType anIndex = this->GetIndex( theValue );
  unsigned char *aTablePtr = this->GetPointer( anIndex );
  CopyColor( aTablePtr, theColor );
  myHasMarkedValues = true;
}


//----------------------------------------------------------------------------
void
VISU_LookupTable
::FillByColor( unsigned char* theColor )
{
  vtkIdType aNbColors = this->GetNumberOfColors();
  for(int i = 0; i < aNbColors; i++){
    unsigned char *aTablePtr = this->GetPointer(i);
    CopyColor( aTablePtr, theColor );
  }
}


//----------------------------------------------------------------------------
void
VISU_LookupTable
::MakeBiColor()
{
  unsigned char aRedPtr[3] = {255, 0, 0};
  unsigned char aBluePtr[3] = {0, 0, 255};

  double aRange[2];
  this->GetTableRange(aRange);
  vtkIdType aNbColors = this->GetNumberOfColors();

  double aDelta = (aRange[1]-aRange[0])/aNbColors;
  double aValue = aRange[0]+0.5*aDelta;
  for(int i = 0; i < aNbColors; i++){
    vtkIdType anIndex = this->GetIndex(aValue);
    unsigned char* aTablePtr = this->GetPointer(anIndex);
    if(aValue > 0.0){
      CopyColor(aTablePtr,aRedPtr);
    }else{
      CopyColor(aTablePtr,aBluePtr);
    }
    aValue += aDelta;
  }
}


//----------------------------------------------------------------------------
void
VISU_LookupTable
::SetMapScale(double theScale)
{
  if( myScale != theScale )
  {
    myScale = theScale;
    Modified();
  }
}

void VISU_LookupTable::SetBicolor( bool theBicolor )
{
  if( myBicolor != theBicolor )
  {
    myBicolor = theBicolor;
    Modified();
  }
}


int
VISU_LookupTable
::ComputeLogRange(double inRange[2],
                  double outRange[2])
{
  if(inRange[0] >= inRange[1])
    return -1;
  if(0.0 <= inRange[0] && 0.0 < inRange[1]){
    if(inRange[0] != 0.0)
      outRange[0] = log10((double)inRange[0]);
    else
      outRange[0] = log10((double)inRange[1]*1.0E-6);
    outRange[1] = log10((double)inRange[1]);
    return 0;
  }else if(inRange[0] < 0.0 && inRange[1] <= 0.0){
    outRange[0] = log10((double)-inRange[0]);
    outRange[1] = log10((double)-inRange[1]);
    return 1;
  }else
    return -1;
}

unsigned char*
VISU_LookupTable
::MapValue(double v)
{
  if(GetScale() == VTK_SCALE_LOG10) {
    double aLowBound = log10(this->TableRange[0]);
    v = pow(double(10.0), aLowBound + (v - aLowBound)*myScale);
    return vtkLookupTable::MapValue(v);
  } else if (!myBicolor) {
    v = this->TableRange[0] + (v - this->TableRange[0])*myScale;
    return vtkLookupTable::MapValue(v);
  } else {
    unsigned char* table = this->Table->GetPointer(0);
    int index = v > 0 ? 4*static_cast<int>(this->GetNumberOfColors()-1) : 0;
    return &table[index];
  }
}

void
VISU_LookupTable
::ForceBuild()
{
  Superclass::ForceBuild();
  myHasMarkedValues = false;
}

// Apply log to value, with appropriate constraints.
inline
double
VISU_ApplyLogScale(double v,
                   double range[2],
                   double logRange[2])
{
  // is the range set for negative numbers?
  if (range[0] < 0) {
    if (v < 0) {
      v = log10(-static_cast<double>(v));
    }
    else if (range[0] > range[1]) {
      v = logRange[0];
    }
    else {
      v = logRange[1];
    }
  }
  else {
    if (v > 0) {
      v = log10(static_cast<double>(v));
    }
    else if (range[0] < range[1]) {
      v = logRange[0];
    }
    else {
      v = logRange[1];
    }
  }
  return v;
}

// Apply shift/scale to the scalar value v and do table lookup.
inline
unsigned char *
VISU_LinearLookup(double v,
                  unsigned char *table,
                  double maxIndex,
                  double shift,
                  double scale,
                  bool bicolor)
{
  if( !bicolor )
  {
    double findx = (v + shift)*scale;
    if (findx < 0)
      findx = 0;
    if (findx > maxIndex)
      findx = maxIndex;

    return &table[4*static_cast<int>(findx)];
    // round
    //return &table[4*(int)(findx + 0.5f)];
  }
  else
  {
    int index = v > 0 ? 4*static_cast<int>(maxIndex) : 0;
    return &table[index];
  }
}

// accelerate the mapping by copying the data in 32-bit chunks instead
// of 8-bit chunks
template<class T>
void
VISU_LookupTableMapData(vtkLookupTable *self,
                        T *input,
                        unsigned char *output,
                        int length,
                        int inIncr,
                        int outFormat,
                        double theMapScale,
                        bool bicolor)
{
  int i = length;
  double *range = self->GetTableRange();
  double maxIndex = self->GetNumberOfColors() - 1;
  double shift, scale;
  unsigned char *table = self->GetPointer(0);
  unsigned char *cptr;
  double alpha;

  if ( (alpha=self->GetAlpha()) >= 1.0 ) //no blending required
  {
    if (self->GetScale() == VTK_SCALE_LOG10)
    {
      double val;
      double logRange[2];
      VISU_LookupTable::ComputeLogRange(range, logRange);
      shift = -logRange[0];
      if (logRange[1] <= logRange[0])
      {
        scale = VTK_LARGE_FLOAT;
      }
      else
      {
        scale = (maxIndex + 1)/(logRange[1] - logRange[0]);
      }
      /* correct scale
      scale = maxIndex/(logRange[1] - logRange[0]);
      */
      if (outFormat == VTK_RGBA)
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_RGB)
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_LUMINANCE_ALPHA)
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          *output++ = cptr[3];
          input += inIncr;
        }
      }
      else // outFormat == VTK_LUMINANCE
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          input += inIncr;
        }
      }
    }//if log scale

    else //not log scale
    {
      shift = -range[0];
      if (range[1] <= range[0])
      {
        scale = VTK_LARGE_FLOAT;
      }
      else
      {
        scale = (maxIndex + 1)/(range[1] - range[0]);
      }
      /* correct scale
      scale = maxIndex/(range[1] - range[0]);
      */

      if (outFormat == VTK_RGBA)
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_RGB)
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_LUMINANCE_ALPHA)
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          *output++ = cptr[3];
          input += inIncr;
        }
      }
      else // outFormat == VTK_LUMINANCE
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          input += inIncr;
        }
      }
    }//if not log lookup
  }//if blending not needed

  else //blend with the specified alpha
  {
    if (self->GetScale() == VTK_SCALE_LOG10)
    {
      double val;
      double logRange[2];
      VISU_LookupTable::ComputeLogRange(range, logRange);
      shift = -logRange[0];
      if (logRange[1] <= logRange[0])
      {
        scale = VTK_LARGE_FLOAT;
      }
      else
      {
        scale = (maxIndex + 1)/(logRange[1] - logRange[0]);
      }
      /* correct scale
      scale = maxIndex/(logRange[1] - logRange[0]);
      */
      if (outFormat == VTK_RGBA)
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = static_cast<unsigned char>((*cptr)*alpha); cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_RGB)
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_LUMINANCE_ALPHA)
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          *output++ = static_cast<unsigned char>(alpha*cptr[3]);
          input += inIncr;
        }
      }
      else // outFormat == VTK_LUMINANCE
      {
        while (--i >= 0)
        {
          val = VISU_ApplyLogScale(*input, range, logRange);
          cptr = VISU_LinearLookup(val, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          input += inIncr;
        }
      }
    }//log scale with blending

    else //no log scale with blending
    {
      shift = -range[0];
      if (range[1] <= range[0])
      {
        scale = VTK_LARGE_FLOAT;
      }
      else
      {
        scale = (maxIndex + 1)/(range[1] - range[0]);
      }
      /* correct scale
      scale = maxIndex/(range[1] - range[0]);
      */

      if (outFormat == VTK_RGBA)
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = static_cast<unsigned char>((*cptr)*alpha); cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_RGB)
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = *cptr++;
          *output++ = *cptr++;
          *output++ = *cptr++;
          input += inIncr;
        }
      }
      else if (outFormat == VTK_LUMINANCE_ALPHA)
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          *output++ = static_cast<unsigned char>(cptr[3]*alpha);
          input += inIncr;
        }
      }
      else // outFormat == VTK_LUMINANCE
      {
        while (--i >= 0)
        {
          cptr = VISU_LinearLookup(*input, table, maxIndex, shift, scale*theMapScale, bicolor);
          *output++ = static_cast<unsigned char>(cptr[0]*0.30 + cptr[1]*0.59 +
                                                 cptr[2]*0.11 + 0.5);
          input += inIncr;
        }
      }
    }//no log scale
  }//alpha blending
}

// Although this is a relatively expensive calculation,
// it is only done on the first render. Colors are cached
// for subsequent renders.
template<class T>
void
VISU_LookupTableMapMag(vtkLookupTable *self,
                       T *input,
                       unsigned char *output,
                       int length,
                       int inIncr,
                       int outFormat,
                       double theMapScale,
                       bool bicolor)
{
  double tmp, sum;
  double *mag;
  int i, j;

  mag = new double[length];
  for (i = 0; i < length; ++i)
  {
    sum = 0;
    for (j = 0; j < inIncr; ++j)
    {
      tmp = (double)(*input);
      sum += (tmp * tmp);
      ++input;
    }
    mag[i] = sqrt(sum);
  }

  VISU_LookupTableMapData(self, mag, output, length, 1, outFormat, theMapScale, bicolor);

  delete [] mag;
}


void VISU_LookupTable::MapScalarsThroughTable2(void *input,
                                               unsigned char *output,
                                               int inputDataType,
                                               int numberOfValues,
                                               int inputIncrement,
                                               int outputFormat)
{
  if (this->UseMagnitude && inputIncrement > 1)
  {
    switch (inputDataType)
    {
      case VTK_BIT:
        vtkErrorMacro("Cannot comput magnitude of bit array.");
        break;
      case VTK_CHAR:
        VISU_LookupTableMapMag(this,static_cast<char *>(input),output,
                               numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_UNSIGNED_CHAR:
        VISU_LookupTableMapMag(this,static_cast<unsigned char *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_SHORT:
        VISU_LookupTableMapMag(this,static_cast<short *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_UNSIGNED_SHORT:
        VISU_LookupTableMapMag(this,static_cast<unsigned short *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_INT:
        VISU_LookupTableMapMag(this,static_cast<int *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_UNSIGNED_INT:
        VISU_LookupTableMapMag(this,static_cast<unsigned int *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_LONG:
        VISU_LookupTableMapMag(this,static_cast<long *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_UNSIGNED_LONG:
        VISU_LookupTableMapMag(this,static_cast<unsigned long *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_FLOAT:
        VISU_LookupTableMapMag(this,static_cast<float *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      case VTK_DOUBLE:
        VISU_LookupTableMapMag(this,static_cast<double *>(input),output,
                             numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
        return;
      default:
        vtkErrorMacro(<< "MapImageThroughTable: Unknown input ScalarType");
        return;
    }
  }

  switch (inputDataType)
  {
    case VTK_BIT:
    {
      vtkIdType i, id;
      vtkBitArray *bitArray = vtkBitArray::New();
      bitArray->SetVoidArray(input,numberOfValues,1);
      vtkUnsignedCharArray *newInput = vtkUnsignedCharArray::New();
      newInput->SetNumberOfValues(numberOfValues);
      for (id=i=0; i<numberOfValues; i++, id+=inputIncrement)
      {
        newInput->SetValue(i, bitArray->GetValue(id));
      }
      VISU_LookupTableMapData(this,
                              static_cast<unsigned char*>(newInput->GetPointer(0)),
                              output,numberOfValues,
                              inputIncrement,outputFormat,myScale,myBicolor);
      newInput->Delete();
      bitArray->Delete();
    }
    break;

    case VTK_CHAR:
      VISU_LookupTableMapData(this,static_cast<char *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_UNSIGNED_CHAR:
      VISU_LookupTableMapData(this,static_cast<unsigned char *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_SHORT:
      VISU_LookupTableMapData(this,static_cast<short *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_UNSIGNED_SHORT:
      VISU_LookupTableMapData(this,static_cast<unsigned short *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_INT:
      VISU_LookupTableMapData(this,static_cast<int *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_UNSIGNED_INT:
      VISU_LookupTableMapData(this,static_cast<unsigned int *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_LONG:
      VISU_LookupTableMapData(this,static_cast<long *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_UNSIGNED_LONG:
      VISU_LookupTableMapData(this,static_cast<unsigned long *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_FLOAT:
      VISU_LookupTableMapData(this,static_cast<float *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    case VTK_DOUBLE:
      VISU_LookupTableMapData(this,static_cast<double *>(input),output,
                              numberOfValues,inputIncrement,outputFormat,myScale,myBicolor);
      break;

    default:
      vtkErrorMacro(<< "MapImageThroughTable: Unknown input ScalarType");
      return;
  }
}

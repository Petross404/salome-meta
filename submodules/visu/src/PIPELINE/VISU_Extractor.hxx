// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Extractor.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_Extractor_HeaderFile
#define VISU_Extractor_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ConvertorDef.hxx"
#include <vtkDataSetAlgorithm.h>


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_Extractor : public vtkDataSetAlgorithm
{
public:
  vtkTypeMacro(VISU_Extractor, vtkDataSetAlgorithm);

  static
  VISU_Extractor
  *New();

  virtual
  int
  GetScalarMode();

  virtual
  void
  SetScalarMode(int theScalarMode = 0);

  virtual
  VISU::TGaussMetric
  GetGaussMetric();

  virtual
  void
  SetGaussMetric(VISU::TGaussMetric theGaussMetric = VISU::AVERAGE_METRIC);

protected:
  VISU_Extractor();
  VISU_Extractor(const VISU_Extractor&);

  ~VISU_Extractor();

  virtual
  int
  RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  int myScalarMode;
  VISU::TGaussMetric myGaussMetric;
};

#endif

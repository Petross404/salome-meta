// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_DeformationPL.hxx
//  Author : 
//  Module : SALOME
//
#ifndef VISU_DeformationPL_HeaderFile
#define VISU_DeformationPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_MapperHolder.hxx"
#include <vtkSmartPointer.h>

class vtkDataSet;
class VISU_MergeFilter;
class vtkPassThroughFilter;
class vtkWarpVector;
class VISU_CellDataToPointData;

class VISU_PIPELINE_EXPORT VISU_DeformationPL {
  
public:
  VISU_DeformationPL();
  virtual ~VISU_DeformationPL();

  //-----------------------------------------------------------
  virtual void SetScale(double theScaleFactor);
  virtual void SetMapScale(double theMapScaleFactor);
  virtual double GetScale();

  //-----------------------------------------------------------
  virtual void SetVectorialField(VISU::PUnstructuredGridIDMapper theIdMapper) = 0;
  virtual VISU::PUnstructuredGridIDMapper getVectorialField() = 0;


  //-----------------------------------------------------------
  virtual 
  unsigned 
  long int
  GetMTime();

  static double GetDefaultScaleFactor(VISU_DeformationPL *thePipeLine);

  void SetWarpVectorInput(vtkDataSet *theInput);
  vtkDataSet* GetWarpVectorOutput();
  vtkAlgorithmOutput* GetWarpVectorOutputPort();

  //-----------------------------------------------------------
  void SetMergeFilterInput(vtkDataSet* ScalarInput,
                           vtkDataSet* VectorialInput);

  vtkDataSet* GetMergeFilterOutput();

protected:

  VISU::PUnstructuredGridIDMapper myVectorialField;
  vtkWarpVector *myWarpVector;
  vtkSmartPointer<VISU_MergeFilter> myVectorMergeFilter;
  vtkPassThroughFilter *myInputPassFilter;
  vtkPassThroughFilter *myOutputPassFiler;
  VISU_CellDataToPointData *myCellDataToPointData;

private:
  double myScaleFactor;
  double myMapScaleFactor;

};

#endif

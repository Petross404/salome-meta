// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : VVTK_ImplicitFunctionWidget.cxx
//  Author : Peter KURNEV
//  Module : SALOME
//  $Header$
//
#include "VISU_ImplicitFunctionWidget.hxx"
//
#include <vtkFollower.h>
#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkDataSet.h>
#include <vtkImplicitFunction.h>
#include <vtkMapper.h>
//
vtkStandardNewMacro(VISU_UnScaledActor);

//====================================================================
// function: VISU_UnScaledActor
// purpose:
//====================================================================
VISU_UnScaledActor::VISU_UnScaledActor() 
{
  for (int i=0; i<3; ++i){
    myCenter[i]=0.;
  }
  mySize=24;
}
//====================================================================
// function: SetCenter
// purpose:
//====================================================================
void VISU_UnScaledActor::SetCenter(double *pC)
{
  for (int i=0; i<3; ++i){
    myCenter[i]=pC[i];
  }
}
//====================================================================
// function:GetBounds
// purpose:
//====================================================================
double* VISU_UnScaledActor::GetBounds()
{
  Superclass::GetBounds();
  //
  for (int i=0; i<3; ++i){
    Bounds[2*i]=myCenter[i];
    Bounds[2*i+1]=myCenter[i];
  }
  return Bounds;
}
//====================================================================
// function:Render
// purpose:
//====================================================================
void VISU_UnScaledActor::Render(vtkRenderer *theRenderer)
{
  if(theRenderer){
    double P[2][3] = {{-1.0, -1.0, 0.0},{+1.0, +1.0, 0.0}};
    theRenderer->ViewToWorld(P[0][0],P[0][1],P[0][2]);
    theRenderer->ViewToWorld(P[1][0],P[1][1],P[1][2]);
    double aWorldDiag = sqrt((P[1][0]-P[0][0])*(P[1][0]-P[0][0])+
                                           (P[1][1]-P[0][1])*(P[1][1]-P[0][1])+
                                           (P[1][2]-P[0][2])*(P[1][2]-P[0][2]));
    int* aSize = theRenderer->GetRenderWindow()->GetSize();
    double aWinDiag = sqrt(double(aSize[0]*aSize[0]+aSize[1]*aSize[1]));
    vtkDataSet* aDataSet = GetMapper()->GetInput();
    double aLength = aDataSet->GetLength();
    double aPrecision = 1.e-3;
    double anOldScale = GetScale()[0];
    double aScale = 
      mySize*aWorldDiag/aWinDiag/aLength*sqrt(double(aSize[0])/double(aSize[1]));

    SetOrigin(myCenter);  
    //
    if(fabs(aScale - anOldScale)/aScale > aPrecision){
      SetScale(aScale);
    }
  }
  vtkFollower::Render(theRenderer);
  
}
//====================================================================
// function:SetSize
// purpose:
//====================================================================
void VISU_UnScaledActor::SetSize(int theSize)
{
  mySize = theSize;
}

//==================================================================
// class: VISU_ImplicitFunctionWidget
//
//==================================================================
// function: VISU_ImplicitFunctionWidget
// purpose :
//==================================================================
VISU_ImplicitFunctionWidget::VISU_ImplicitFunctionWidget() 
: 
  vtk3DWidget()
{
}
//==================================================================
// function: ~
// purpose :
//==================================================================
VISU_ImplicitFunctionWidget::~VISU_ImplicitFunctionWidget()
{  
}

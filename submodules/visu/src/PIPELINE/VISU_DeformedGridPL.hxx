// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_ScalarMapPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_DeformedGridPL_HeaderFile
#define VISU_DeformedGridPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_PolyDataPL.hxx"

class vtkWarpScalar;
class vtkContourFilter;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_DeformedGridPL : public VISU_PolyDataPL
{
public:
  vtkTypeMacro(VISU_DeformedGridPL, VISU_PolyDataPL);

  static 
  VISU_DeformedGridPL* 
  New();

  //----------------------------------------------------------------------------
  virtual
  unsigned long int 
  GetMTime();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  //----------------------------------------------------------------------------
  void
  SetScaleFactor( double theScaleFactor );

  double
  GetScaleFactor();

  void
  SetContourPrs( bool theIsContourPrs );

  bool
  GetIsContourPrs();

  void
  SetNumberOfContours( int theNumber );

  int
  GetNumberOfContours();

  virtual
  void
  SetMapScale(double theMapScale = 1.0);

  //----------------------------------------------------------------------------
  virtual
  void
  Init();

  virtual
  void
  Update();

protected:
  //----------------------------------------------------------------------------
  VISU_DeformedGridPL();
  
  virtual
  ~VISU_DeformedGridPL();

  virtual
  void
  Build();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

private:
  VISU_DeformedGridPL(const VISU_DeformedGridPL&);  // Not implemented.
  void operator=(const VISU_DeformedGridPL&);  // Not implemented.

  double myScaleFactor;
  double myMapScaleFactor;
  vtkContourFilter* myContourFilter;
  vtkWarpScalar *myWarpScalar;
  bool myIsContour;
};
  
#endif

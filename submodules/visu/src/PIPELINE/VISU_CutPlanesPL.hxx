// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_CutPlanesPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_CutPlanesPL_HeaderFile
#define VISU_CutPlanesPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_OptionalDeformationPL.hxx"
#include "VISU_MapperHolder.hxx"

#include <vector>

class vtkAppendPolyData;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_CutPlanesPL : public VISU_ScalarMapPL,
                                              public VISU_OptionalDeformationPL
{
public:
  vtkTypeMacro(VISU_CutPlanesPL, VISU_ScalarMapPL);

  static 
  VISU_CutPlanesPL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  enum PlaneOrientation {XY, YZ, ZX};

  virtual 
  void
  SetOrientation(const VISU_CutPlanesPL::PlaneOrientation& theOrient,
                 double theXAng, 
                 double theYAng, 
                 int theNum = 0);
  
  virtual 
  const PlaneOrientation& 
  GetPlaneOrientation(int theNum = 0);

  virtual
  double 
  GetRotateX(int theNum = 0);

  virtual
  double
  GetRotateY(int theNum = 0);

  virtual
  double 
  GetDisplacement(int theNum = 0);

  virtual
  void
  SetDisplacement(double theDisp, 
                  int theNum = 0);

  virtual
  void
  SetPartPosition(int thePartNumber, 
                  double thePartPosition);

  virtual
  double 
  GetPartPosition(int thePartNumber, 
                  int theNum = 0);

  virtual 
  void
  SetPartDefault(int thePartNumber);

  virtual
  int
  IsPartDefault(int thePartNumber);

  virtual
  void
  SetNbParts(int theNb);

  virtual
  int
  GetNbParts();

public:
  virtual
  void
  Init();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  vtkAppendPolyData* 
  GetAppendPolyData() 
  { 
    return myAppendPolyData; 
  }

public:
  static
  double* 
  GetRx(double theRx[3][3], 
        double thaAng);

  static
  double* 
  GetRy(double theRy[3][3], 
        double thaAng);

  static
  double* 
  GetRz(double theRz[3][3], 
        double thaAng);

  static
  void
  CorrectPnt(double thePnt[3], 
             const double BoundPrj[6]);

  static
  void
  GetBoundProject(double BoundPrj[3], 
                  const double BoundBox[6], 
                  const double Dir[3]);

  static
  void
  GetDir(double theDir[3],
         const double theAng[3],
         const PlaneOrientation& theBasePlane);

  static 
  void
  ClearAppendPolyData(vtkAppendPolyData *theAppendPolyData);

  static 
  void
  CutWithPlane(vtkAppendPolyData* theAppendPolyData, 
               vtkDataSet* theDataSet,
               double theDir[3], 
               double theOrig[3]);

  static
  void
  CutWithPlanes(vtkAppendPolyData* theAppendPolyData, 
                vtkDataSet* theDataSet,
                int theNbPlanes, 
                double theDir[3], 
                double theBounds[6],
                const std::vector<double>& thePlanePosition,
                const std::vector<int>& thePlaneCondition,
                double theDisplacement);

  virtual void SetVectorialField(VISU::PUnstructuredGridIDMapper);
  VISU::PUnstructuredGridIDMapper getVectorialField();

  virtual
  void
  SetMapScale(double theMapScale = 1.0);


protected:
  VISU_CutPlanesPL();

  virtual
  ~VISU_CutPlanesPL();

  virtual 
  vtkAlgorithmOutput* 
  InsertCustomPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  void
  SetPartPosition(int theNum = 0);

  int myNbParts;
  PlaneOrientation myBasePlane[2];
  double myAng[2][3], myDisplacement[2];
  vtkAppendPolyData *myAppendPolyData;
  std::vector<double> myPartPosition;
  std::vector<int> myPartCondition;

private:
  VISU_CutPlanesPL(const VISU_CutPlanesPL&);  // Not implemented.
  void operator=(const VISU_CutPlanesPL&);  // Not implemented.
};

#endif

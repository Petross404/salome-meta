// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VISU_ElnoDisassembleFilter.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_ElnoMeshValue.hxx"

#include <vtkCellData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro( VISU_ElnoDisassembleFilter );


//----------------------------------------------------------------------------
VISU_ElnoDisassembleFilter::VISU_ElnoDisassembleFilter()
{
  this->SetInputArrayToProcess( 0, // idx
                                0, // port
                                0, // connection
                                vtkDataObject::FIELD_ASSOCIATION_CELLS, // field association
                                "ELNO_FIELD" ); // name

  this->SetInputArrayToProcess( 1, // idx
                                0, // port
                                0, // connection
                                vtkDataObject::FIELD_ASSOCIATION_CELLS, // field association
                                "ELNO_COMPONENT_MAPPER" ); // name

  this->myShrinkFactor = -0.999;
}


//----------------------------------------------------------------------------
VISU_ElnoDisassembleFilter::~VISU_ElnoDisassembleFilter()
{}


//----------------------------------------------------------------------------
void VISU_ElnoDisassembleFilter::SetShrinkFactor( double theValue )
{
  if ( VISU::CheckIsSameValue( theValue, myShrinkFactor ) )
    return;

  myShrinkFactor = theValue;
  this->Modified();
}


//----------------------------------------------------------------------------
double VISU_ElnoDisassembleFilter::GetShrinkFactor()
{
  return myShrinkFactor;
}


//----------------------------------------------------------------------------
namespace
{
  //----------------------------------------------------------------------------
  template < int points_type, int elno_type >
  struct TExecute2
  {
    vtkUnstructuredGrid *myInput;
    vtkUnstructuredGrid *myOutput;
    vtkDataArray *myElnoDataArray;
    vtkDataArray *myElnoDataMapper;
    double myShrinkFactor;

    typedef typename VISU::TL::TEnum2VTKArrayType< points_type >::TResult TPointsDataArray;
    typedef typename VISU::TL::TEnum2VTKBasicType< points_type >::TResult TPointsDataType;

    typedef typename VISU::TL::TEnum2VTKArrayType< elno_type >::TResult TElnoDataArray;
    typedef typename VISU::TL::TEnum2VTKBasicType< elno_type >::TResult TElnoDataType;

    VISU::TGetElnoNodeData< elno_type > myGetElnoNodeData;
    vtkCellArray *myConnectivity;
    vtkPointData *myInputPointData;
    vtkPointData *myOutputPointData;
    TPointsDataArray *myInputPointsArray;
    TPointsDataArray *myOutputPointsArray;
    TElnoDataArray* myElnoFullDataArray;
    TElnoDataArray* myElnoPartialDataArray;
    TPointsDataArray *myElnoPointCoords;
    vtkIntArray* myInputPointsMapper;
    vtkIntArray* myOutputPointsMapper;

    //----------------------------------------------------------------------------
    TExecute2( vtkUnstructuredGrid *theInput, 
               vtkUnstructuredGrid *theOutput, 
               vtkDataArray *theElnoDataArray,
               vtkDataArray *theElnoDataMapper,
               double theShrinkFactor )
      : myGetElnoNodeData( theElnoDataArray, theElnoDataMapper )
      , myInput( theInput )
      , myOutput( theOutput )
      , myElnoDataArray( theElnoDataArray )
      , myElnoDataMapper( theElnoDataMapper )
      , myShrinkFactor( theShrinkFactor )
    {
      myConnectivity = vtkCellArray::New();
      myConnectivity->DeepCopy( theInput->GetCells() );
    
      vtkPoints *anInputPoints = theInput->GetPoints();
      vtkPoints *aPoints = anInputPoints->New( anInputPoints->GetDataType() );
      vtkIdType aNbCells = myConnectivity->GetNumberOfCells();
      vtkIdType aNbPoints = myConnectivity->GetNumberOfConnectivityEntries() - aNbCells;
      aPoints->Allocate( aNbPoints );
    
      myInputPointsArray = TPointsDataArray::SafeDownCast( anInputPoints->GetData() );
      myOutputPointsArray = TPointsDataArray::SafeDownCast( aPoints->GetData() );
    
      myInputPointData = theInput->GetPointData();
      myOutputPointData = theOutput->GetPointData();
      myOutputPointData->Allocate( aNbPoints );
    
      vtkCellData *anInputCellData = theInput->GetCellData();

      // To create a new copy of initial data for output
      myElnoFullDataArray = TElnoDataArray::New();
      myElnoFullDataArray->SetName( "VISU_FIELD" );
      myElnoFullDataArray->SetNumberOfComponents( myGetElnoNodeData.getNbComp() );
      myElnoFullDataArray->SetNumberOfTuples( aNbPoints );

      // To create a new copy of partial initial data for output
      myElnoPartialDataArray = TElnoDataArray::New();
      // This partial data can be represented as in terms of vectors as scalars
      if ( anInputCellData->GetVectors() != NULL ) 
        myElnoPartialDataArray->SetNumberOfComponents( 3 );
      else
        myElnoPartialDataArray->SetNumberOfComponents( 1 );
      myElnoPartialDataArray->SetNumberOfTuples( aNbPoints );

      myElnoPointCoords = TPointsDataArray::New();
      myElnoPointCoords->SetName( "ELNO_POINT_COORDS" );
      myElnoPointCoords->SetNumberOfComponents( 3 );
      myElnoPointCoords->SetNumberOfTuples( aNbPoints );

      vtkDataArray* anArray = myInputPointData->GetArray( "VISU_POINTS_MAPPER" );
      myInputPointsMapper = vtkIntArray::SafeDownCast( anArray );
      
      myOutputPointsMapper = vtkIntArray::New();
      myOutputPointsMapper->SetName( myInputPointsMapper->GetName() );
      myOutputPointsMapper->SetNumberOfComponents( myInputPointsMapper->GetNumberOfComponents() );
      myOutputPointsMapper->SetNumberOfTuples( aNbPoints );

      if ( theShrinkFactor > 0.0 )
        this->ShrinkExecute();
      else
        this->SimpleExecute();

      theOutput->SetPoints( aPoints );
      
      theOutput->SetCells( theInput->GetCellTypesArray(), 
                           theInput->GetCellLocationsArray(),
                           myConnectivity );

      myConnectivity->Delete();
      
      vtkCellData *anOutputCellData = theOutput->GetCellData();
      anOutputCellData->PassData( anInputCellData );
      
      anOutputCellData->RemoveArray( "ELNO_COMPONENT_MAPPER" );
      anOutputCellData->RemoveArray( "ELNO_FIELD" );
      anOutputCellData->RemoveArray( "VISU_FIELD" );
      anOutputCellData->SetVectors( NULL );
      
      //anOutputPointData->PassData( anInputPointData );
      
      myOutputPointData->AddArray( myElnoFullDataArray );
      myElnoFullDataArray->Delete();
      
      if ( anInputCellData->GetVectors() != NULL ) 
        myOutputPointData->SetVectors( myElnoPartialDataArray );
      else
        myOutputPointData->SetScalars( myElnoPartialDataArray );
      myElnoPartialDataArray->Delete();
      
      myOutputPointData->AddArray( myElnoPointCoords );
      myElnoPointCoords->Delete();
      
      myOutputPointData->AddArray( myOutputPointsMapper );
      myOutputPointsMapper->Delete();
    }

    //----------------------------------------------------------------------------
    void SimpleExecute()
    {
      // To reserve a temproary value holder
      vtkIdType aNbComp = std::max( 3, myGetElnoNodeData.getNbComp() );
      std::vector< TElnoDataType > anElnoDataValues( aNbComp ); 

      std::vector< int > anPointsMapperValues( myInputPointsMapper->GetNumberOfComponents() ); 
   
      myConnectivity->InitTraversal();
      vtkIdType aNbPts = 0, *aPts = 0;
      for ( vtkIdType aCellId = 0; myConnectivity->GetNextCell( aNbPts, aPts ); aCellId++ ) {
        for ( vtkIdType aPntId = 0; aPntId < aNbPts; aPntId++ ) {
          TPointsDataType aCoords[ 3 ];
          vtkIdType aCurrentPntId = aPts[ aPntId ];
          myInputPointsArray->GetTupleValue( aCurrentPntId, aCoords );
          
          aPts[ aPntId ] = myOutputPointsArray->InsertNextTupleValue( aCoords );
          vtkIdType aNewPntId = aPts[ aPntId ];
          
          myElnoPointCoords->SetTupleValue( aNewPntId, aCoords );
          
          myOutputPointData->CopyData( myInputPointData, aCurrentPntId, aNewPntId );
          
          TElnoDataType* anElnoData = myGetElnoNodeData( aCellId, aPntId );
          myElnoFullDataArray->SetTupleValue( aNewPntId,  anElnoData );
          
          myElnoFullDataArray->GetTupleValue( aNewPntId, &anElnoDataValues[ 0 ] );
          myElnoPartialDataArray->SetTupleValue( aNewPntId, &anElnoDataValues[ 0 ] );

          myInputPointsMapper->GetTupleValue( aCurrentPntId, &anPointsMapperValues[ 0 ] );
          myOutputPointsMapper->SetTupleValue( aNewPntId, &anPointsMapperValues[ 0 ] );
        }
      }
    }

    //----------------------------------------------------------------------------
    void ShrinkExecute()
    {
      // To reserve a temproary value holder
      vtkIdType aNbComp = std::max( 3, myGetElnoNodeData.getNbComp() );
      std::vector< TElnoDataType > anElnoDataValues( aNbComp ); 
      
      std::vector< int > anPointsMapperValues( myInputPointsMapper->GetNumberOfComponents() ); 
   
      myConnectivity->InitTraversal();
      vtkIdType aNbPts = 0, *aPts = 0;
      for ( vtkIdType aCellId = 0; myConnectivity->GetNextCell( aNbPts, aPts ); aCellId++ ) {
        
        TPointsDataType aCenter[ 3 ] = { TPointsDataType(), TPointsDataType(), TPointsDataType() };
        
        for ( vtkIdType aPntId = 0; aPntId < aNbPts; aPntId++ ) {
          TPointsDataType aCoords[ 3 ];
          myInputPointsArray->GetTupleValue( aPts[ aPntId ], aCoords );
          
          aCenter[ 0 ] += aCoords[ 0 ];
          aCenter[ 1 ] += aCoords[ 1 ];
          aCenter[ 2 ] += aCoords[ 2 ];
        }
        
        aCenter[ 0 ] /= aNbPts;
        aCenter[ 1 ] /= aNbPts;
        aCenter[ 2 ] /= aNbPts;
        
        for ( vtkIdType aPntId = 0; aPntId < aNbPts; aPntId++ ) {
          TPointsDataType aCoords[ 3 ];
          vtkIdType aCurrentPntId = aPts[ aPntId ];
          myInputPointsArray->GetTupleValue( aCurrentPntId, aCoords );
          
          TPointsDataType aNewCoords[ 3 ];
          
          aNewCoords[ 0 ] = aCenter[ 0 ]  + 
            TPointsDataType( myShrinkFactor * ( aCoords[ 0 ] - aCenter[ 0 ] ) );
          aNewCoords[ 1 ] = aCenter[ 1 ]  + 
            TPointsDataType( myShrinkFactor * ( aCoords[ 1 ] - aCenter[ 1 ] ) );
          aNewCoords[ 2 ] = aCenter[ 2 ]  + 
            TPointsDataType( myShrinkFactor * ( aCoords[ 2 ] - aCenter[ 2 ] ) );
          
          aPts[ aPntId ] = myOutputPointsArray->InsertNextTupleValue( aNewCoords );
          vtkIdType aNewPntId = aPts[ aPntId ];
          
          myElnoPointCoords->SetTupleValue( aNewPntId, aCoords );
          
          myOutputPointData->CopyData( myInputPointData, aCurrentPntId, aNewPntId );
          
          TElnoDataType* anElnoData = myGetElnoNodeData( aCellId, aPntId );
          myElnoFullDataArray->SetTupleValue( aNewPntId, anElnoData );
          
          myElnoFullDataArray->GetTupleValue( aNewPntId, &anElnoDataValues[ 0 ] );
          myElnoPartialDataArray->SetTupleValue( aNewPntId, &anElnoDataValues[ 0 ] );

          myInputPointsMapper->GetTupleValue( aCurrentPntId, &anPointsMapperValues[ 0 ] );
          myOutputPointsMapper->SetTupleValue( aNewPntId, &anPointsMapperValues[ 0 ] );
        }
      }
    }
  };


  //----------------------------------------------------------------------------
  template < int points_type, int elno_type >
  int Execute2( vtkUnstructuredGrid *theInput, 
                vtkUnstructuredGrid *theOutput, 
                vtkDataArray *theElnoDataArray,
                vtkDataArray *theElnoDataMapper,
                double theShrinkFactor )
  {
    TExecute2< points_type, elno_type >( theInput, 
                                         theOutput, 
                                         theElnoDataArray, 
                                         theElnoDataMapper, 
                                         theShrinkFactor );
        
    return 1;
  }
          

  //----------------------------------------------------------------------------
  template < int points_type >
  int Execute( vtkUnstructuredGrid *theInput, 
               vtkUnstructuredGrid *theOutput, 
               vtkDataArray *theElnoDataArray,
               vtkDataArray *theElnoDataMapper,
               double theShrinkFactor )
  {
    switch( theElnoDataArray->GetDataType() ){
    case VTK_DOUBLE:
      return Execute2< points_type, VTK_DOUBLE >
        ( theInput, theOutput, theElnoDataArray, theElnoDataMapper, theShrinkFactor );
    case VTK_FLOAT:
      return Execute2< points_type, VTK_FLOAT >
        ( theInput, theOutput, theElnoDataArray, theElnoDataMapper, theShrinkFactor );
    case VTK_INT:
      return Execute2< points_type, VTK_INT >
        ( theInput, theOutput, theElnoDataArray, theElnoDataMapper, theShrinkFactor );
    case VTK_LONG:
      return Execute2< points_type, VTK_LONG >
        ( theInput, theOutput, theElnoDataArray, theElnoDataMapper, theShrinkFactor );
    default:
      break;
    }
    
    return 0;
  } 


  //----------------------------------------------------------------------------
}


//----------------------------------------------------------------------------
int VISU_ElnoDisassembleFilter::RequestData( vtkInformation *vtkNotUsed(request),
                                             vtkInformationVector **inputVector,
                                             vtkInformationVector *outputVector )
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkUnstructuredGrid *anInput =
    vtkUnstructuredGrid::SafeDownCast( inInfo->Get( vtkDataObject::DATA_OBJECT() ) );
  vtkUnstructuredGrid *anOutput = 
    vtkUnstructuredGrid::SafeDownCast( outInfo->Get( vtkDataObject::DATA_OBJECT() ) );

  vtkDataArray *anElnoDataArray = this->GetInputArrayToProcess( 0, inputVector );
  vtkDataArray *anElnoDataMapper = this->GetInputArrayToProcess( 1, inputVector );

  if ( !anElnoDataArray ) {
    anOutput->ShallowCopy( anInput );
    return 1;
  }

  vtkPoints *aPoints = anInput->GetPoints();
  switch( aPoints->GetDataType() ){
  case VTK_DOUBLE:
    return ::Execute< VTK_DOUBLE >( anInput, anOutput, anElnoDataArray, anElnoDataMapper, myShrinkFactor );
  case VTK_FLOAT:
    return ::Execute< VTK_FLOAT >( anInput, anOutput, anElnoDataArray, anElnoDataMapper, myShrinkFactor );
  case VTK_INT:
    return ::Execute< VTK_INT >( anInput, anOutput, anElnoDataArray, anElnoDataMapper, myShrinkFactor );
  case VTK_LONG:
    return ::Execute< VTK_LONG >( anInput, anOutput, anElnoDataArray, anElnoDataMapper, myShrinkFactor );
  default:
    break;
  }  
  
  return 0;
}


//----------------------------------------------------------------------------

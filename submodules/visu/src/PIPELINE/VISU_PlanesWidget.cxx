// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : VVTK_ImplicitFunctionWidget.cxx
//  Author : Peter KURNEV
//  Module : SALOME
//  $Header$
//
#include "VISU_PlanesWidget.hxx"
#include "VISU_ImplicitFunctionWidget.hxx"

#include <vtkActor.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkCellPicker.h>
#include <vtkConeSource.h>
#include <vtkEDFCutter.h>
#include <vtkFeatureEdges.h>
#include <vtkImageData.h>
#include <vtkLineSource.h>
#include <vtkMath.h>
#include <vtkObjectFactory.h>
#include <vtkOutlineFilter.h>
#include <vtkPlane.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>
#include <vtkTubeFilter.h>
#include <vtkImplicitBoolean.h>
#include <vtkImplicitFunctionCollection.h>
////
#include <vtkFollower.h>
#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkRenderWindow.h>

static
bool 
IsValidPlane2Position(vtkPlane *pPx,
                      vtkDataSet *pDataSet,
                      double aTol=0.003);
static 
void 
GetBndPoints(vtkDataSet *pDataSet, 
             double aPnts[24]);
static 
double
DistanceToPlane(const double x[3], 
                const double n[3], 
                const double p0[3]);

vtkStandardNewMacro(VISU_PlanesWidget);

//==================================================================
// function: VISU_PlanesWidget
// purpose :
//==================================================================
VISU_PlanesWidget::VISU_PlanesWidget() 
: 
  VISU_ImplicitFunctionWidget()
{
  State = VISU_PlanesWidget::Start;
  EventCallbackCommand->SetCallback(VISU_PlanesWidget::ProcessEvents);
  
  NormalToXAxis = 0;
  NormalToYAxis = 0;
  NormalToZAxis = 0;

  HandleMoveEvent = true;
  HandleLeftButtonEvent = true;
  HandleMiddleButtonEvent = false;
  HandleRightButtonEvent = false;

  // Build the representation of the widget
  // 
  myPlane1 = vtkPlane::New();
  myPlane1->SetNormal(0,0,1);
  myPlane1->SetOrigin(0,0,0);
  //
  myDistance = 10.;
  myPlane2 = vtkPlane::New();
  myPlane2->SetNormal(0.,0.,-1.);
  myPlane2->SetOrigin(0,0,myDistance);
  //
  myImplicitFunction = vtkImplicitBoolean::New();
  myImplicitFunction->SetOperationTypeToUnion();  
  //
  myBox = vtkImageData::New();
  myBox->SetDimensions(2,2,2);
  myOutline = vtkOutlineFilter::New();
  myOutline->SetInputData(myBox);
  myOutlineMapper = vtkPolyDataMapper::New();
  myOutlineMapper->SetInputConnection(myOutline->GetOutputPort());
  myOutlineActor = vtkActor::New();
  this->myOutlineActor->SetMapper(this->myOutlineMapper);
  this->myOutlineActor->PickableOff();
  this->OutlineTranslation = 0;
  
  this->myCutter1 = vtkEDFCutter::New();
  this->myCutter1->SetInputData(myBox);
  this->myCutter1->SetCutFunction(myPlane1);
  this->myCutMapper1 = vtkPolyDataMapper::New();
  this->myCutMapper1->SetInputConnection(this->myCutter1->GetOutputPort());
  this->myCutActor1 = vtkActor::New();
  this->myCutActor1->SetMapper(this->myCutMapper1);
  this->myDrawPlane = 1;

  this->myEdges1 = vtkFeatureEdges::New();
  myEdges1->SetColoring(0); 
  this->myEdges1->SetInputConnection(this->myCutter1->GetOutputPort());
  this->myEdgesMapper1 = vtkPolyDataMapper::New();
  this->myEdgesMapper1->SetInputConnection(this->myEdges1->GetOutputPort());
  this->myEdgesActor1 = vtkActor::New();
  this->myEdgesActor1->SetMapper(this->myEdgesMapper1);
  myEdgesActor1->GetProperty()->SetLineWidth(4.); 
  myEdgesActor1->GetProperty()->SetColor(0., .5, .7); 

  this->myCutter2 = vtkEDFCutter::New();
  this->myCutter2->SetInputData(myBox);
  this->myCutter2->SetCutFunction(this->myPlane2);
  this->myCutMapper2 = vtkPolyDataMapper::New();
  this->myCutMapper2->SetInputConnection(this->myCutter2->GetOutputPort());
  this->myCutActor2 = vtkActor::New();
  this->myCutActor2->SetMapper(this->myCutMapper2);

  myEdges2 = vtkFeatureEdges::New();
  myEdges2->SetColoring(0); 
  myEdges2->SetInputConnection(myCutter2->GetOutputPort());
  myEdgesMapper2 = vtkPolyDataMapper::New();
  myEdgesMapper2->SetInputConnection(myEdges2->GetOutputPort());
  myEdgesActor2 = vtkActor::New();
  myEdgesActor2->SetMapper(myEdgesMapper2);
  myEdgesActor2->GetProperty()->SetLineWidth(4.); 
  myEdgesActor2->GetProperty()->SetColor(.7, .0, .0); 

  // Create the + plane normal
  this->LineSource = vtkLineSource::New();
  this->LineSource->SetResolution(1);
  this->LineMapper = vtkPolyDataMapper::New();
  this->LineMapper->SetInputConnection(this->LineSource->GetOutputPort());
  this->LineActor = vtkActor::New();
  this->LineActor->SetMapper(this->LineMapper);

  this->ConeSource = vtkConeSource::New();
  this->ConeSource->SetResolution(12);
  this->ConeSource->SetAngle(20.);
  this->ConeMapper = vtkPolyDataMapper::New();
  this->ConeMapper->SetInputConnection(this->ConeSource->GetOutputPort());
  this->ConeActor = VISU_UnScaledActor::New();
  this->ConeActor->SetMapper(this->ConeMapper);
  ConeActor->SetSize(36);
  ConeActor->SetCenter(ConeSource->GetCenter());

  // Create the - plane normal
  this->LineSource2 = vtkLineSource::New();
  this->LineSource2->SetResolution(1);
  this->LineMapper2 = vtkPolyDataMapper::New();
  this->LineMapper2->SetInputConnection(this->LineSource2->GetOutputPort());
  this->LineActor2 = vtkActor::New();
  this->LineActor2->SetMapper(this->LineMapper2);

  this->ConeSource2 = vtkConeSource::New();
  this->ConeSource2->SetResolution(12);
  this->ConeSource2->SetAngle(20.);
  this->ConeMapper2 = vtkPolyDataMapper::New();
  this->ConeMapper2->SetInputConnection(this->ConeSource2->GetOutputPort());
  this->ConeActor2 = VISU_UnScaledActor::New();
  this->ConeActor2->SetMapper(this->ConeMapper2);
  ConeActor2->SetSize(36);
  ConeActor2->SetCenter(ConeSource2->GetCenter());

  // Create the origin handle
  this->Sphere = vtkSphereSource::New();
  this->Sphere->SetThetaResolution(16);
  this->Sphere->SetPhiResolution(8);
  this->SphereMapper = vtkPolyDataMapper::New();
  this->SphereMapper->SetInputConnection(this->Sphere->GetOutputPort());
  this->SphereActor = VISU_UnScaledActor::New();
  this->SphereActor->SetMapper(this->SphereMapper);
  SphereActor->SetSize(36);
  SphereActor->SetCenter(Sphere->GetCenter());

  this->Transform = vtkTransform::New();

  // Define the point coordinates
  double bounds[6];
  bounds[0] = -0.5;
  bounds[1] = 0.5;
  bounds[2] = -0.5;
  bounds[3] = 0.5;
  bounds[4] = -0.5;
  bounds[5] = 0.5;

  // Initial creation of the widget, serves to initialize it
  this->PlaceWidget(bounds);

  //Manage the picking stuff
  this->Picker = vtkCellPicker::New();
  this->Picker->SetTolerance(0.005);
  this->Picker->AddPickList(this->myCutActor1);
  this->Picker->AddPickList(this->myCutActor2);
  this->Picker->AddPickList(this->LineActor);
  this->Picker->AddPickList(this->ConeActor);
  this->Picker->AddPickList(this->LineActor2);
  this->Picker->AddPickList(this->ConeActor2);
  this->Picker->AddPickList(this->SphereActor);
  this->Picker->AddPickList(this->myOutlineActor);
  this->Picker->PickFromListOn();
  
  // Set up the initial properties
  this->CreateDefaultProperties();
  
}
//==================================================================
// function: ~
// purpose :
//==================================================================
VISU_PlanesWidget::~VISU_PlanesWidget()
{  
  myPlane1->Delete();

  this->myPlane2->Delete();
  this->myImplicitFunction->Delete();

  myBox->Delete();
  this->myOutline->Delete();
  this->myOutlineMapper->Delete();
  this->myOutlineActor->Delete();
  
  this->myCutter1->Delete();
  this->myCutMapper1->Delete();
  this->myCutActor1->Delete();

  this->myEdges1->Delete();
  this->myEdgesMapper1->Delete();
  this->myEdgesActor1->Delete();
  
  myCutter2->Delete();
  myCutMapper2->Delete();
  myCutActor2->Delete();

  myEdges2->Delete();
  myEdgesMapper2->Delete();
  myEdgesActor2->Delete();
  
  this->LineSource->Delete();
  this->LineMapper->Delete();
  this->LineActor->Delete();

  this->ConeSource->Delete();
  this->ConeMapper->Delete();
  this->ConeActor->Delete();

  this->LineSource2->Delete();
  this->LineMapper2->Delete();
  this->LineActor2->Delete();

  this->ConeSource2->Delete();
  this->ConeMapper2->Delete();
  this->ConeActor2->Delete();

  this->Sphere->Delete();
  this->SphereMapper->Delete();
  this->SphereActor->Delete();

  this->Transform->Delete();

  this->Picker->Delete();

  this->NormalProperty->Delete();
  this->SelectedNormalProperty->Delete();
  this->PlaneProperty->Delete();
  this->SelectedPlaneProperty->Delete();
  this->OutlineProperty->Delete();
  this->SelectedOutlineProperty->Delete();
  this->EdgesProperty->Delete();
}
//==================================================================
// function: ImplicitFunction
// purpose :
//==================================================================
vtkImplicitFunction* VISU_PlanesWidget::ImplicitFunction()
{
  return this->myImplicitFunction;
}
//==================================================================
// function: SetDistance
// purpose :
//==================================================================
void 
VISU_PlanesWidget
::SetDistance(const double theDistance)
{
  if( theDistance <= 0.0 || theDistance == myDistance )
    return;

  myDistance=theDistance;
  //
  double *origin, *normal, oNew[3], aN2[3]; 
  origin = myPlane1->GetOrigin();
  normal = myPlane1->GetNormal();
  vtkMath::Normalize(normal);
  oNew[0] = origin[0] + myDistance*normal[0];
  oNew[1] = origin[1] + myDistance*normal[1];
  oNew[2] = origin[2] + myDistance*normal[2];
  myPlane2->SetOrigin(oNew);
  aN2[0] = -normal[0];
  aN2[1] = -normal[1];
  aN2[2] = -normal[2];
  myPlane2->SetNormal(aN2);
}
//==================================================================
// function: Distance
// purpose :
//==================================================================
double
VISU_PlanesWidget
::Distance() const
{
  return myDistance;
}
//==================================================================
// function: SetEnabled
// purpose :
//==================================================================
void VISU_PlanesWidget::SetEnabled(int enabling)
{
  if ( ! this->Interactor )    {
    vtkErrorMacro(<<"The interactor must be set prior to enabling/disabling widget");
    return;
  }

  if ( enabling ) {//------------------------------------------------------------
    vtkDebugMacro(<<"Enabling plane widget");
    
    if ( this->Enabled ){ //already enabled, just return
      return;
    }
    
    if ( ! this->CurrentRenderer ){
      this->CurrentRenderer = this->Interactor->FindPokedRenderer(
        this->Interactor->GetLastEventPosition()[0],
        this->Interactor->GetLastEventPosition()[1]);
      if (this->CurrentRenderer == NULL)        {
        return;
      }
    }
    //
    vtkCamera *pCamera=CurrentRenderer->GetActiveCamera();
    pCamera->SetParallelProjection(1);
    //
    this->myImplicitFunction->AddFunction(myPlane1);
    this->myImplicitFunction->AddFunction(this->myPlane2);

    this->Enabled = 1;

    // listen for the following events
    vtkRenderWindowInteractor *i = this->Interactor;
    if( this->HandleMoveEvent )    {
      i->AddObserver(vtkCommand::MouseMoveEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
    }
    if( this->HandleLeftButtonEvent )    {
      i->AddObserver(vtkCommand::LeftButtonPressEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
      i->AddObserver(vtkCommand::LeftButtonReleaseEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
    }
    if( this->HandleMiddleButtonEvent )    {
      i->AddObserver(vtkCommand::MiddleButtonPressEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
      i->AddObserver(vtkCommand::MiddleButtonReleaseEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
    }
    if( this->HandleRightButtonEvent )    {
      i->AddObserver(vtkCommand::RightButtonPressEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
      i->AddObserver(vtkCommand::RightButtonReleaseEvent, 
                     this->EventCallbackCommand, 
                     this->Priority);
    }
    // add the outline
    this->CurrentRenderer->AddActor(this->myOutlineActor);
    this->myOutlineActor->SetProperty(this->OutlineProperty);

    // add the edges
    this->CurrentRenderer->AddActor(this->myEdgesActor1);
    this->CurrentRenderer->AddActor(myEdgesActor2);

    this->myOutlineActor->SetProperty(this->EdgesProperty);

    // add the normal vector
    this->CurrentRenderer->AddActor(this->LineActor);
    this->LineActor->SetProperty(this->NormalProperty);
    this->CurrentRenderer->AddActor(this->ConeActor);
    this->ConeActor->SetProperty(this->NormalProperty);

    this->CurrentRenderer->AddActor(this->LineActor2);
    this->LineActor2->SetProperty(this->NormalProperty);
    this->CurrentRenderer->AddActor(this->ConeActor2);
    this->ConeActor2->SetProperty(this->NormalProperty);
    
    // add the origin handle
    this->CurrentRenderer->AddActor(this->SphereActor);
    this->SphereActor->SetProperty(this->NormalProperty);

    // add the plane (if desired)
    if ( this->myDrawPlane )      {
      this->CurrentRenderer->AddActor(this->myCutActor1);
      this->CurrentRenderer->AddActor(this->myCutActor2);
    }
    this->myCutActor1->SetProperty(this->PlaneProperty);
    myCutActor2->SetProperty(this->PlaneProperty);
    
    this->UpdateRepresentation();
    //this->SizeHandles();
    this->InvokeEvent(vtkCommand::EnableEvent,NULL);
  }
  
  else {//disabling----------------------------------------------------------
    vtkDebugMacro(<<"Disabling plane widget");

    if ( ! this->Enabled ) {//already disabled, just return
      return;
    }
    
    if(vtkImplicitFunctionCollection* aFunction = this->myImplicitFunction->GetFunction()){
      aFunction->RemoveAllItems();
      this->myImplicitFunction->Modified(); // VTK bug
    }

    this->Enabled = 0;

    // don't listen for events any more
    this->Interactor->RemoveObserver(this->EventCallbackCommand);

    // turn off the various actors
    this->CurrentRenderer->RemoveActor(this->myOutlineActor);
    this->CurrentRenderer->RemoveActor(this->myEdgesActor1);
    this->CurrentRenderer->RemoveActor(myEdgesActor2);
    this->CurrentRenderer->RemoveActor(this->LineActor);
    this->CurrentRenderer->RemoveActor(this->ConeActor);
    this->CurrentRenderer->RemoveActor(this->LineActor2);
    this->CurrentRenderer->RemoveActor(this->ConeActor2);
    this->CurrentRenderer->RemoveActor(this->SphereActor);
    this->CurrentRenderer->RemoveActor(this->myCutActor1);
    this->CurrentRenderer->RemoveActor(myCutActor2);

    this->InvokeEvent(vtkCommand::DisableEvent,NULL);
    this->CurrentRenderer = NULL;
  }
  
  this->Interactor->Render();
}
//==================================================================
// function: ProcessEvents
// purpose :
//==================================================================
void VISU_PlanesWidget::ProcessEvents(vtkObject* vtkNotUsed(object), 
                                                unsigned long event,
                                                void* clientdata, 
                                                void* vtkNotUsed(calldata))
{
  VISU_PlanesWidget* self = 
    reinterpret_cast<VISU_PlanesWidget *>( clientdata );

  //okay, let's do the right thing
  switch(event)
    {
    case vtkCommand::LeftButtonPressEvent:
      self->OnLeftButtonDown();
      break;
    case vtkCommand::LeftButtonReleaseEvent:
      self->OnLeftButtonUp();
      break;
    case vtkCommand::MiddleButtonPressEvent:
      self->OnMiddleButtonDown();
      break;
    case vtkCommand::MiddleButtonReleaseEvent:
      self->OnMiddleButtonUp();
      break;
    case vtkCommand::RightButtonPressEvent:
      self->OnRightButtonDown();
      break;
    case vtkCommand::RightButtonReleaseEvent:
      self->OnRightButtonUp();
      break;
    case vtkCommand::MouseMoveEvent:
      self->OnMouseMove();
      break;
    default:
      break;
    }
}
//==================================================================
// function: HighlightNormal
// purpose :
//==================================================================
void VISU_PlanesWidget::HighlightNormal(int highlight)
{
  if ( highlight ) {
    this->LineActor->SetProperty(this->SelectedNormalProperty);
    this->ConeActor->SetProperty(this->SelectedNormalProperty);
    this->LineActor2->SetProperty(this->SelectedNormalProperty);
    this->ConeActor2->SetProperty(this->SelectedNormalProperty);
    this->SphereActor->SetProperty(this->SelectedNormalProperty);
    }
  else
    {
    this->LineActor->SetProperty(this->NormalProperty);
    this->ConeActor->SetProperty(this->NormalProperty);
    this->LineActor2->SetProperty(this->NormalProperty);
    this->ConeActor2->SetProperty(this->NormalProperty);
    this->SphereActor->SetProperty(this->NormalProperty);
    }
}
//==================================================================
// function: HighlightPlane
// purpose :
//==================================================================
void VISU_PlanesWidget::HighlightPlane(int highlight)
{
  if ( highlight )    {
    this->myCutActor1->SetProperty(this->SelectedPlaneProperty);
    myCutActor2->SetProperty(this->SelectedPlaneProperty);
  }
  else    {
    this->myCutActor1->SetProperty(this->PlaneProperty);
    myCutActor2->SetProperty(this->PlaneProperty);
  }
}
//==================================================================
// function: HighlightOutline
// purpose :
//==================================================================
void VISU_PlanesWidget::HighlightOutline(int highlight)
{
  if (highlight)    {
    this->myOutlineActor->SetProperty(this->SelectedOutlineProperty);
  }
  else    {
    this->myOutlineActor->SetProperty(this->OutlineProperty);
  }
}
//==================================================================
// function: OnLeftButtonDown
// purpose :
//==================================================================
void VISU_PlanesWidget::OnLeftButtonDown()
{
  // We're only here if we are enabled
  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];
  //
  // Okay, we can process this. See if we've picked anything.
  // Make sure it's in the activated renderer
  vtkRenderer *ren = this->Interactor->FindPokedRenderer(X,Y);
  if ( ren != this->CurrentRenderer )    {
    this->State = VISU_PlanesWidget::Outside;
    return;
  }
  
  vtkAssemblyPath *path;
  this->Picker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->Picker->GetPath();

  if ( path == NULL ) {//not picking this widget
    this->HighlightPlane(0);
    this->HighlightNormal(0);
    this->HighlightOutline(0);
    this->State = VISU_PlanesWidget::Outside;
    return;
  }

  vtkProp *prop = path->GetFirstNode()->GetViewProp();
  this->ValidPick = 1;
  this->Picker->GetPickPosition(this->LastPickPosition);
  //
  if ( prop == this->ConeActor || prop == this->LineActor ||
       prop == this->ConeActor2 || prop == this->LineActor2 )  {
    this->HighlightPlane(1);
    this->HighlightNormal(1);
    this->State = VISU_PlanesWidget::Rotating;
  }
  else if ( prop == this->myCutActor1)    {
    this->HighlightPlane(1);
    this->State = VISU_PlanesWidget::Pushing;
  }
  else if ( prop == this->SphereActor )    {
    this->HighlightNormal(1);
    this->State = VISU_PlanesWidget::MovingOrigin;
  }
  else if (prop == myCutActor2)    {
    this->HighlightPlane(1);
    this->State = VISU_PlanesWidget::ChangeDistance;
  }
  else    {
    if ( this->OutlineTranslation )      {
      this->HighlightOutline(1);
      this->State = VISU_PlanesWidget::MovingOutline;
    }
  }
  
  this->EventCallbackCommand->SetAbortFlag(1);
  this->StartInteraction();
  this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
  this->Interactor->Render();
}
//==================================================================
// function: OnLeftButtonUp
// purpose :
//==================================================================
void VISU_PlanesWidget::OnLeftButtonUp()
{
  if ( this->State == VISU_PlanesWidget::Outside )    {
    return;
  }

  this->State = VISU_PlanesWidget::Start;
  this->HighlightPlane(0);
  this->HighlightOutline(0);
  this->HighlightNormal(0);
  //this->SizeHandles();
  
  this->EventCallbackCommand->SetAbortFlag(1);
  this->EndInteraction();
  this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  this->Interactor->Render();
}
//==================================================================
// function: OnMiddleButtonDown
// purpose :
//==================================================================
void VISU_PlanesWidget::OnMiddleButtonDown()
{
  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  // Okay, we can process this. See if we've picked anything.
  // Make sure it's in the activated renderer
  vtkRenderer *ren = this->Interactor->FindPokedRenderer(X,Y);
  if ( ren != this->CurrentRenderer )    {
    this->State = VISU_PlanesWidget::Outside;
    return;
  }
  
  // Okay, we can process this.
  vtkAssemblyPath *path;
  this->Picker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->Picker->GetPath();
  
  if ( path == NULL ) {//nothing picked
    this->State = VISU_PlanesWidget::Outside;
    return;
  }

  this->ValidPick = 1;
  this->Picker->GetPickPosition(this->LastPickPosition);
  this->State = VISU_PlanesWidget::MovingPlane;
  this->HighlightNormal(1);
  this->HighlightPlane(1);
  
  this->EventCallbackCommand->SetAbortFlag(1);
  this->StartInteraction();
  this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
  this->Interactor->Render();
}
//==================================================================
// function: OnMiddleButtonUp
// purpose :
//==================================================================
void VISU_PlanesWidget::OnMiddleButtonUp()
{
  if ( this->State == VISU_PlanesWidget::Outside )    {
    return;
  }

  this->State = VISU_PlanesWidget::Start;
  this->HighlightPlane(0);
  this->HighlightOutline(0);
  this->HighlightNormal(0);
  //this->SizeHandles();
  
  this->EventCallbackCommand->SetAbortFlag(1);
  this->EndInteraction();
  this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  this->Interactor->Render();
}
//==================================================================
// function: OnRightButtonDown
// purpose :
//==================================================================
void VISU_PlanesWidget::OnRightButtonDown()
{
  this->State = VISU_PlanesWidget::Scaling;

  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  // Okay, we can process this. See if we've picked anything.
  // Make sure it's in the activated renderer
  vtkRenderer *ren = this->Interactor->FindPokedRenderer(X,Y);
  if ( ren != this->CurrentRenderer )    {
    this->State = VISU_PlanesWidget::Outside;
    return;
  }
  
  // Okay, we can process this. Try to pick handles first;
  // if no handles picked, then pick the bounding box.
  vtkAssemblyPath *path;
  this->Picker->Pick(X,Y,0.0,this->CurrentRenderer);
  path = this->Picker->GetPath();
  if ( path == NULL ){ //nothing picked
    this->State = VISU_PlanesWidget::Outside;
    return;
  }
  
  this->ValidPick = 1;
  this->Picker->GetPickPosition(this->LastPickPosition);
  this->HighlightPlane(1);
  this->HighlightOutline(1);
  this->HighlightNormal(1);
  
  this->EventCallbackCommand->SetAbortFlag(1);
  this->StartInteraction();
  this->InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
  this->Interactor->Render();
}
//==================================================================
// function: OnRightButtonUp
// purpose :
//==================================================================
void VISU_PlanesWidget::OnRightButtonUp()
{
  if ( this->State == VISU_PlanesWidget::Outside )    {
    return;
  }
  
  this->State = VISU_PlanesWidget::Start;
  this->HighlightPlane(0);
  this->HighlightOutline(0);
  this->HighlightNormal(0);
  //this->SizeHandles();
  
  this->EventCallbackCommand->SetAbortFlag(1);
  this->EndInteraction();
  this->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  this->Interactor->Render();
}
//==================================================================
// function: OnMouseMove
// purpose :
//==================================================================
void VISU_PlanesWidget::OnMouseMove()
{
  //this->SizeHandles();

  // See whether we're active
  if ( this->State == VISU_PlanesWidget::Outside || 
       this->State == VISU_PlanesWidget::Start )    {
    return;
  }
  
  int X = this->Interactor->GetEventPosition()[0];
  int Y = this->Interactor->GetEventPosition()[1];

  // Do different things depending on state
  // Calculations everybody does
  double focalPoint[4], pickPoint[4], prevPickPoint[4];
  double z, vpn[3];

  vtkCamera *camera = this->CurrentRenderer->GetActiveCamera();
  if ( !camera ) {
    return;
  }

  // Compute the two points defining the motion vector
  this->ComputeWorldToDisplay(this->LastPickPosition[0], this->LastPickPosition[1],
                              this->LastPickPosition[2], focalPoint);
  z = focalPoint[2];
  this->ComputeDisplayToWorld(double(this->Interactor->GetLastEventPosition()[0]),
                              double(this->Interactor->GetLastEventPosition()[1]),
                              z, prevPickPoint);
  this->ComputeDisplayToWorld(double(X), double(Y), z, pickPoint);

  // Process the motion
  if ( this->State == VISU_PlanesWidget::MovingPlane )    {
    //this->TranslatePlane(prevPickPoint, pickPoint);
    //printf(" TranslatePlane\n");
  }
  else if ( this->State == VISU_PlanesWidget::MovingOutline )    {
    //this->TranslateOutline(prevPickPoint, pickPoint);
    //printf(" TranslateOutline\n");
  }
  else if ( this->State == VISU_PlanesWidget::MovingOrigin )    {
    this->TranslateOrigin(prevPickPoint, pickPoint);
    //printf(" TranslateOrigin\n");
  }
  else if ( this->State == VISU_PlanesWidget::Pushing )    {
    this->Push(prevPickPoint, pickPoint);
   // printf(" Push\n");
  }
  else if ( this->State == VISU_PlanesWidget::Scaling )    {
    //this->Scale(prevPickPoint, pickPoint, X, Y);
    //printf(" Scale\n");
  }
  else if ( this->State == VISU_PlanesWidget::Rotating )    {
    camera->GetViewPlaneNormal(vpn);
    this->Rotate(X, Y, prevPickPoint, pickPoint, vpn);
    //printf(" Rotate\n");
  }
  else if ( this->State == VISU_PlanesWidget::ChangeDistance )    {
    this->PushDistance(prevPickPoint, pickPoint);
    //printf(" PushDistance\n");
  }
  // Interact, if desired
  this->EventCallbackCommand->SetAbortFlag(1);
  this->InvokeEvent(vtkCommand::InteractionEvent,NULL);
  
  this->Interactor->Render();
}
//==================================================================
// function: Push
// purpose :
//==================================================================
void VISU_PlanesWidget::Push(double *p1, double *p2)
{
  //Get the motion vector
  int i;
  double v[3];
  //
  for (i=0; i<3; ++i){  
    v[i] = p2[i] - p1[i];
  }
  //
  double aOr1[3], aNr1[3], aNr2[3], aD, z1;
  //
  myPlane1->GetOrigin(aOr1);
  myPlane1->GetNormal(aNr1);
  myPlane2->GetNormal(aNr2);
  //
  aD=vtkMath::Dot(v, aNr2);
  z1 = aOr1[2]+aD*aNr2[2];
  if( z1 <= myBox->GetOrigin()[2] ){
    return;
  }
  //  
  aD=vtkMath::Dot(v, aNr1);
  for (i=0; i < 3; ++i) {
    aOr1[i]=aOr1[i]+aD*aNr1[i];
  }
  SetOriginInternal(aOr1);
  this->UpdateRepresentation();
}
//==================================================================
// function: TranslateOrigin
// purpose :
//==================================================================
void VISU_PlanesWidget::TranslateOrigin(double *p1, double *p2)
{
  //Get the motion vector
  int i;
  double v[3];
  //
  for (i=0; i<3; ++i){  
    v[i] = p2[i] - p1[i];
  }
  //
  //Add to the current point, project back down onto plane
  double *o = myPlane1->GetOrigin();
  double *n = myPlane1->GetNormal();
  double newOrigin[3];
  //
  for (i=0; i<3; ++i){
    newOrigin[i]=o[i] + v[i];
  }
  vtkPlane::ProjectPoint(newOrigin, o, n, newOrigin);
  SetOriginInternal(newOrigin); 
  this->UpdateRepresentation();
}
//==================================================================
// function: SetOriginInternal
// purpose : Set the origin of the plane.(for Internal calls)
//==================================================================
void VISU_PlanesWidget::SetOriginInternal(double x[3]) 
{
  double *bounds = this->myOutline->GetOutput()->GetBounds();
  int i, j;
  for (i=0; i<3; ++i)    {
    j=2*i;
    if ( x[i] < bounds[j] ) {
      x[i] = bounds[j];
    }
    else if ( x[i] > bounds[j+1] ) {
      x[i] = bounds[j+1];
    }
  }
  //
  bool bFlag;
  double aOr2[3], aNr2[3], aNr1[3];
  vtkPlane *pPx;
  //
  myPlane1->GetNormal(aNr1);
  myPlane2->GetNormal(aNr2);
  for (i=0; i<3; ++i)    {
    aOr2[i]=x[i]+myDistance*aNr1[i];
  }
  pPx=vtkPlane::New();
  pPx->SetOrigin(aOr2);
  pPx->SetNormal(aNr2);
  bFlag=IsValidPlane2Position(pPx, myBox);
  if (bFlag){
    myPlane1->SetOrigin(x);
    myPlane2->SetOrigin(aOr2);
  }
  pPx->Delete();
}
//==================================================================
// function: Rotate
// purpose :
//==================================================================
void VISU_PlanesWidget::Rotate(int X, int Y, 
                                         double *p1, double *p2, 
                                         double *vpn)
{
  double v[3];    //vector of motion
  double axis[3]; //axis of rotation
  double theta;   //rotation angle
  int i;

  // mouse motion vector in world space
  for (i=0; i<3; ++i){ 
    v[i] = p2[i] - p1[i];
  }
  //
  double *origin = myPlane1->GetOrigin();
  double *normal = myPlane1->GetNormal();

  // Create axis of rotation and angle of rotation
  vtkMath::Cross(vpn,v,axis);
  if ( vtkMath::Normalize(axis) == 0.0 )    {
    return;
  }

  int *size = this->CurrentRenderer->GetSize();
  double l2 = (X-this->Interactor->GetLastEventPosition()[0])*
    (X-this->Interactor->GetLastEventPosition()[0]) + 
    (Y-this->Interactor->GetLastEventPosition()[1])*
    (Y-this->Interactor->GetLastEventPosition()[1]);
  theta = 360.0 * sqrt(l2/((double)size[0]*size[0]+size[1]*size[1]));

  //Manipulate the transform to reflect the rotation
  this->Transform->Identity();
  this->Transform->Translate(origin[0],origin[1],origin[2]);
  this->Transform->RotateWXYZ(theta,axis);
  this->Transform->Translate(-origin[0],-origin[1],-origin[2]);

  //Set the new normal
  double nNew[3], aN2[3], oNew[3];
  this->Transform->TransformNormal(normal,nNew);
  //
  for (i=0; i<3; ++i){  
    aN2[i] = -nNew[i];
  }
  vtkMath::Normalize(nNew);
  for (i=0; i<3; ++i){  
    oNew[i] = origin[i] + myDistance*nNew[i];
  }
  //
  vtkPlane *pPx=vtkPlane::New();
  pPx->SetNormal(aN2); 
  pPx->SetOrigin(oNew);
  //
  bool bFlag=IsValidPlane2Position(pPx, myBox);
  if (bFlag) {
    myPlane1->SetNormal(nNew);
    this->myPlane2->SetNormal(aN2); 
    this->myPlane2->SetOrigin(oNew);
  } 
  pPx->Delete();
  this->UpdateRepresentation();
}
//==================================================================
// function: PushDistance
// purpose :
//==================================================================
void VISU_PlanesWidget::PushDistance(double *p1, double *p2)
{
  int i;
  double v[3],  *anOrigin1, *aN1, *anOrigin2, *aN2, aD;
  //Get the motion vector
  for (i=0; i<3; ++i){ 
    v[i] = p2[i] - p1[i];
  }
  //
  anOrigin1 = myPlane1->GetOrigin();
  aN1 = myPlane1->GetNormal();
  anOrigin2 = myPlane2->GetOrigin();
  aN2 = myPlane2->GetNormal();

  vtkMath::Normalize(aN1);

  double origin[3];
  double distance = vtkMath::Dot( v, aN2 );
  for(i=0; i<3; ++i) {
    origin[i] = anOrigin2[i] + distance * aN2[i];
  }
  double d = DistanceToPlane(origin, aN1, anOrigin1);
  if( d <= 0.0 )
    return;
  //
  bool bFlag;
  double aOr2[3], aNr2[3];
  vtkPlane *pPx;
  //
  myPlane2->GetOrigin(aOr2);
  myPlane2->GetNormal(aNr2);
  pPx=vtkPlane::New();
  pPx->SetNormal(aNr2);
  aD=vtkMath::Dot(v, aNr2);
  for (i=0; i < 3; ++i) {
    aOr2[i]=aOr2[i]+aD*aNr2[i];
  }
  pPx->SetOrigin(aOr2);
  bFlag=IsValidPlane2Position(pPx, myBox);
  if(bFlag) {
    myPlane2->SetOrigin(aOr2);
    myPlane2->Modified();
    aD=DistanceToPlane(aOr2, aN1, anOrigin1);
    //
    myDistance=aD;
  }
  pPx->Delete();
  this->UpdateRepresentation();
}

//==================================================================
// function: TranslatePlane
// purpose : Loop through all points and translate them
//==================================================================
void VISU_PlanesWidget::TranslatePlane(double *p1, double *p2)
{
  //Get the motion vector
  double v[3];
  v[0] = p2[0] - p1[0];
  v[1] = p2[1] - p1[1];
  v[2] = p2[2] - p1[2];
  
  //Translate the plane
  double oNew[3];
  double *origin = myPlane1->GetOrigin();
  oNew[0] = origin[0] + v[0];
  oNew[1] = origin[1] + v[1];
  oNew[2] = origin[2] + v[2];
  myPlane1->SetOrigin(oNew);
  
  origin = this->myPlane2->GetOrigin();
  oNew[0] = origin[0] + v[0];
  oNew[1] = origin[1] + v[1];
  oNew[2] = origin[2] + v[2];
  this->myPlane2->SetOrigin(oNew);

  this->UpdateRepresentation();
}
//==================================================================
// function: TranslateOutline
// purpose :Loop through all points and translate them
//==================================================================
void VISU_PlanesWidget::TranslateOutline(double *p1, double *p2)
{
  //Get the motion vector
  double v[3];
  v[0] = p2[0] - p1[0];
  v[1] = p2[1] - p1[1];
  v[2] = p2[2] - p1[2];
  
  //Translate the bounding box
  double *origin = myBox->GetOrigin();
  double oNew[3];
  oNew[0] = origin[0] + v[0];
  oNew[1] = origin[1] + v[1];
  oNew[2] = origin[2] + v[2];
  myBox->SetOrigin(oNew);

  //Translate the plane
  origin = myPlane1->GetOrigin();
  oNew[0] = origin[0] + v[0];
  oNew[1] = origin[1] + v[1];
  oNew[2] = origin[2] + v[2];
  myPlane1->SetOrigin(oNew);

  origin = this->myPlane2->GetOrigin();
  oNew[0] = origin[0] + v[0];
  oNew[1] = origin[1] + v[1];
  oNew[2] = origin[2] + v[2];
  this->myPlane2->SetOrigin(oNew);

  this->UpdateRepresentation();
}

//==================================================================
// function: Scale
// purpose :
//==================================================================
void VISU_PlanesWidget::Scale(double *p1, double *p2, 
                                        int vtkNotUsed(X), int Y)
{
  //Get the motion vector
  double v[3];
  v[0] = p2[0] - p1[0];
  v[1] = p2[1] - p1[1];
  v[2] = p2[2] - p1[2];

  //int res = this->PlaneSource->GetXResolution();
  double *o = myPlane1->GetOrigin();

  // Compute the scale factor
  double sf = vtkMath::Norm(v) / this->myOutline->GetOutput()->GetLength();
  if ( Y > this->Interactor->GetLastEventPosition()[1] )    {
    sf = 1.0 + sf;
  }
  else    {
    sf = 1.0 - sf;
  }
  
  this->Transform->Identity();
  this->Transform->Translate(o[0],o[1],o[2]);
  this->Transform->Scale(sf,sf,sf);
  this->Transform->Translate(-o[0],-o[1],-o[2]);

  double *origin = myBox->GetOrigin();
  double *spacing = myBox->GetSpacing();
  double oNew[3], p[3], pNew[3];
  p[0] = origin[0] + spacing[0];
  p[1] = origin[1] + spacing[1];
  p[2] = origin[2] + spacing[2];

  this->Transform->TransformPoint(origin,oNew);
  this->Transform->TransformPoint(p,pNew);

  myBox->SetOrigin(oNew);
  myBox->SetSpacing( (pNew[0]-oNew[0]), (pNew[1]-oNew[1]), (pNew[2]-oNew[2]) );

  this->UpdateRepresentation();
}



//==================================================================
// function: CreateDefaultProperties
// purpose :
//==================================================================
void VISU_PlanesWidget::CreateDefaultProperties()
{
  // Normal properties
  this->NormalProperty = vtkProperty::New();
  this->NormalProperty->SetColor(1,1,1);
  this->NormalProperty->SetLineWidth(2);

  this->SelectedNormalProperty = vtkProperty::New();
  this->SelectedNormalProperty->SetColor(1,0,0);
  this->NormalProperty->SetLineWidth(2);

  // Plane properties
  this->PlaneProperty = vtkProperty::New();
  this->PlaneProperty->SetAmbient(1.0);
  this->PlaneProperty->SetAmbientColor(1.0,1.0,1.0);

  this->SelectedPlaneProperty = vtkProperty::New();
  this->SelectedPlaneProperty->SetAmbient(1.0);
  this->SelectedPlaneProperty->SetAmbientColor(0.0,1.0,0.0);
  this->SelectedPlaneProperty->SetOpacity(0.25);

  // Outline properties
  this->OutlineProperty = vtkProperty::New();
  this->OutlineProperty->SetAmbient(1.0);
  this->OutlineProperty->SetAmbientColor(1.0,1.0,1.0);

  this->SelectedOutlineProperty = vtkProperty::New();
  this->SelectedOutlineProperty->SetAmbient(1.0);
  this->SelectedOutlineProperty->SetAmbientColor(0.0,1.0,0.0);

  // Edge property
  this->EdgesProperty = vtkProperty::New();
  this->EdgesProperty->SetAmbient(1.0);
  this->EdgesProperty->SetAmbientColor(1.0,1.0,1.0);
}
//==================================================================
// function: InitialPlaceWidget
// purpose :
//==================================================================
void VISU_PlanesWidget::InitialPlaceWidget(double bds[6])
{
  double bounds[6], origin[3];

  PlaceWidget(bds);
  //
  this->AdjustBounds(bds, bounds, origin);
  this->SetOrigin((bounds[1]+bounds[0])/2.0,
                  (bounds[3]+bounds[2])/2.0,
                  (bounds[5]+bounds[4])/2.0);

  static double DIST_COEFF = 0.1;
  SetDistance(this->InitialLength*DIST_COEFF);
  //
  this->UpdateRepresentation();
}
//==================================================================
// function: PlaceWidget
// purpose :
//==================================================================
void VISU_PlanesWidget::PlaceWidget(double bds[6])
{
  int i;
  double bounds[6], origin[3];

  this->AdjustBounds(bds, bounds, origin);

  // Set up the bounding box
  myBox->SetOrigin(bounds[0],bounds[2],bounds[4]);
  myBox->SetSpacing((bounds[1]-bounds[0]),(bounds[3]-bounds[2]),
                        (bounds[5]-bounds[4]));
  this->myOutline->Update();

  if (this->GetInput() || this->Prop3D)    {
    this->LineSource->SetPoint1(myPlane1->GetOrigin());
    if ( this->NormalToYAxis )      {
      myPlane1->SetNormal(0,1,0);
      myPlane2->SetNormal(0,-1,0);
      this->LineSource->SetPoint2(0,1,0);
    }
    else if ( this->NormalToZAxis )      {
      myPlane1->SetNormal(0,0,1);
      myPlane2->SetNormal(0,0,-1);
      this->LineSource->SetPoint2(0,0,1);
    }
    else{ //default or x-normal
      myPlane1->SetNormal(1,0,0);
      myPlane2->SetNormal(-1,0,0);
      this->LineSource->SetPoint2(1,0,0);
    }
  }
  
  for (i=0; i<6; i++)    {
    this->InitialBounds[i] = bounds[i];
  }

  this->InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
                             (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
                             (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));

  this->UpdateRepresentation();
}
//==================================================================
// function: SetOrigin
// purpose :Set the origin of the plane.(for external calls)
//==================================================================
void VISU_PlanesWidget::SetOrigin(double x, double y, double z) 
{
  double origin[3];
  origin[0] = x;
  origin[1] = y;
  origin[2] = z;
  this->SetOrigin(origin);
}
//==================================================================
// function: SetOrigin 
// purpose : Set the origin of the plane.(for external calls)
//==================================================================
void VISU_PlanesWidget::SetOrigin(double x[3])
{
  double *bounds = this->myOutline->GetOutput()->GetBounds();
  for (int i=0; i<3; i++)    {
    if ( x[i] < bounds[2*i] )      {
      x[i] = bounds[2*i];
    }
    else if ( x[i] > bounds[2*i+1] )      {
      x[i] = bounds[2*i+1];
    }
  }
  myPlane1->SetOrigin(x);
  double *origin, *normal, oNew[3];
  origin = myPlane1->GetOrigin();
  normal = myPlane1->GetNormal();
  vtkMath::Normalize(normal);
  oNew[0] = origin[0] + myDistance*normal[0];
  oNew[1] = origin[1] + myDistance*normal[1];
  oNew[2] = origin[2] + myDistance*normal[2];
  this->myPlane2->SetOrigin(oNew);
  this->UpdateRepresentation();
}
//==================================================================
// function: GetOrigin
// purpose :Get the origin of the plane.
//==================================================================
double* VISU_PlanesWidget::GetOrigin() 
{
  return myPlane1->GetOrigin();
}

void VISU_PlanesWidget::GetOrigin(double xyz[3]) 
{
  myPlane1->GetOrigin(xyz);
}
//==================================================================
// function: SetNormal
// purpose :Set the normal to the plane.
//==================================================================
void VISU_PlanesWidget::SetNormal(double x, double y, double z) 
{
  double n[3];
  n[0] = x;
  n[1] = y;
  n[2] = z;
  vtkMath::Normalize(n);
  myPlane1->SetNormal(n);
  n[0] =- x;
  n[1] =- y;
  n[2] =- z;
  this->myPlane2->SetNormal(n);

  this->UpdateRepresentation();
}

//==================================================================
// function: SetNormal
// purpose :Set the normal to the plane.
//==================================================================
void VISU_PlanesWidget::SetNormal(double n[3]) 
{
  this->SetNormal(n[0], n[1], n[2]);
}
//==================================================================
// function: GetNormal
// purpose :Get the normal to the plane.
//==================================================================
double* VISU_PlanesWidget::GetNormal() 
{
  return myPlane1->GetNormal();
}
//==================================================================
// function: GetNormal
// purpose :Get the normal to the plane.
//==================================================================
void VISU_PlanesWidget::GetNormal(double xyz[3]) 
{
  myPlane1->GetNormal(xyz);
}
//==================================================================
// function: SetDrawPlane
// purpose :
//==================================================================
void VISU_PlanesWidget::SetDrawPlane(int drawPlane)
{
  if ( drawPlane == this->myDrawPlane )    {
    return;
  }
  
  this->Modified();
  this->myDrawPlane = drawPlane;
  if ( this->Enabled ) {
    if ( drawPlane ) {
      this->CurrentRenderer->AddActor(this->myCutActor1);
      this->CurrentRenderer->AddActor(myCutActor2);
    }
    else {
      this->CurrentRenderer->RemoveActor(this->myCutActor1);
      this->CurrentRenderer->RemoveActor(myCutActor2);
    }
    this->Interactor->Render();
  }
}
//==================================================================
// function: SetNormalToXAxis
// purpose :
//==================================================================
void VISU_PlanesWidget::SetNormalToXAxis (int var)
{
  if (this->NormalToXAxis != var)    {
    this->NormalToXAxis = var;
    this->Modified();
  }
  if (var)    {
    this->NormalToYAxisOff();
    this->NormalToZAxisOff();
  }
}
//==================================================================
// function: SetNormalToYAxis
// purpose :
//==================================================================
void VISU_PlanesWidget::SetNormalToYAxis (int var)
{
  if (this->NormalToYAxis != var)    {
    this->NormalToYAxis = var;
    this->Modified();
  }
  if (var)    {
    this->NormalToXAxisOff();
    this->NormalToZAxisOff();
  }
}
//==================================================================
// function: SetNormalToZAxis
// purpose :
//==================================================================
void VISU_PlanesWidget::SetNormalToZAxis (int var)
{
  if (this->NormalToZAxis != var)    {
    this->NormalToZAxis = var;
    this->Modified();
  }
  if (var)    {
    this->NormalToXAxisOff();
    this->NormalToYAxisOff();
  }
}
//==================================================================
// function: GetPolyData
// purpose :
//==================================================================
void VISU_PlanesWidget::GetPolyData(vtkPolyData *pd)
{ 
  pd->ShallowCopy(this->myCutter1->GetOutput()); 
}
//==================================================================
// function: GetPolyDataSource
// purpose :
//==================================================================
/*
vtkPolyDataSource *VISU_PlanesWidget::GetPolyDataSource()
{
  return this->myCutter1;
}
*/
//==================================================================
// function:GetPlane
// purpose :
//==================================================================
void VISU_PlanesWidget::GetPlane(vtkPlane *plane)
{
  if ( plane == NULL )    {
    return;
  }
  
  plane->SetNormal(myPlane1->GetNormal());
  plane->SetOrigin(myPlane1->GetOrigin());
}
//==================================================================
// function:UpdatePlacement
// purpose :
//==================================================================
void VISU_PlanesWidget::UpdatePlacement(void)
{
  this->myOutline->Update();
  this->myCutter1->Update();
  this->myEdges1->Update();
}
//==================================================================
// function:UpdateRepresentation
// purpose :
//==================================================================
void VISU_PlanesWidget::UpdateRepresentation()
{
  if ( ! this->CurrentRenderer )    {
    return;
  }

  double *origin = myPlane1->GetOrigin();
  double *normal = myPlane1->GetNormal();
  double p2[3];

  // Setup the plane normal
  double d = this->myOutline->GetOutput()->GetLength();

  p2[0] = origin[0] + 0.30 * d * normal[0];
  p2[1] = origin[1] + 0.30 * d * normal[1];
  p2[2] = origin[2] + 0.30 * d * normal[2];

  this->LineSource->SetPoint1(origin);
  this->LineSource->SetPoint2(p2);
  this->ConeSource->SetCenter(p2);
  this->ConeSource->SetDirection(normal);
  ConeActor->SetCenter(p2);

  p2[0] = origin[0] - 0.30 * d * normal[0];
  p2[1] = origin[1] - 0.30 * d * normal[1];
  p2[2] = origin[2] - 0.30 * d * normal[2];

  this->LineSource2->SetPoint1(origin);
  this->LineSource2->SetPoint2(p2);
  this->ConeSource2->SetCenter(p2);
  this->ConeSource2->SetDirection(normal);
  ConeActor2->SetCenter(p2);

  // Set up the position handle
  this->Sphere->SetCenter(origin);
  SphereActor->SetCenter(origin);

  this->myEdgesMapper1->SetInputConnection(this->myEdges1->GetOutputPort());
}

//==================================================================
// function:PrintSelf
// purpose :
//==================================================================
void VISU_PlanesWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
//==================================================================
// function: IsValidPlane2Position
// purpose :
//==================================================================
bool IsValidPlane2Position(vtkPlane *pPx,
                           vtkDataSet *pDataSet,
                           double aTol)
{
  bool bRet;
  int i, iFound;
  double aD, aDmax, aPnts[24], aDiagonal;
  double aTol1, aOr[3], aN[3];
  //
  bRet=false;
  aDiagonal=pDataSet->GetLength();
  aTol1=aDiagonal*aTol;
  //
  GetBndPoints(pDataSet, aPnts);
  //
  pPx->GetOrigin(aOr);
  pPx->GetNormal(aN);
  vtkMath::Normalize(aN);
  //
  iFound=0;
  aDmax=0.;
  for (i=0; i<24; i+=3){
    aD=-DistanceToPlane(aPnts+i, aN, aOr);
    if (aD>aDmax){
            aDmax=aD;
            iFound=1;
    }
  }
  if (iFound && aDmax>aTol1) {
    bRet=!bRet;
  }
  return bRet;
}
//==================================================================
// function: GetBndPoints
// purpose :
//==================================================================
void 
GetBndPoints(vtkDataSet *pDataSet, 
             double aPnts[24])
{
  int aIndx[24] = {
    0,2,4,1,2,4,1,3,4,0,3,4,
    0,2,5,1,2,5,1,3,5,0,3,5
  };
  int i;
  double *pBounds=pDataSet->GetBounds();
  //
  for (i=0; i<24; ++i){
    aPnts[i]=pBounds[aIndx[i]];
  }
}
//==================================================================
// function: DistanceToPlane
// purpose :
//==================================================================
double 
DistanceToPlane(const double x[3], 
                const double n[3], 
                const double p0[3])
{
  return ((n[0]*(x[0]-p0[0]) + 
           n[1]*(x[1]-p0[1]) +  
           n[2]*(x[2]-p0[2])));
}
/*
//==================================================================
// function:SizeHandles
// purpose :
//==================================================================
void VISU_PlanesWidget::SizeHandles()
{
  //  double radius = 
  this->vtk3DWidget::SizeHandles(1.35);
}
*/

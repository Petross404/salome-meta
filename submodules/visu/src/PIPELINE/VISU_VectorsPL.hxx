// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_VectorsPL_HeaderFile
#define VISU_VectorsPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_DeformedShapePL.hxx"

class VTKViewer_TransformFilter;
class VTKViewer_Transform;
class VTKViewer_CellCenters;

class vtkGlyphSource2D;
class vtkConeSource;
class vtkLineSource;

class vtkGlyph3D;

class VISU_UsedPointsFilter;

//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_VectorsPL : public VISU_DeformedShapePL
{
public:
  vtkTypeMacro(VISU_VectorsPL, VISU_DeformedShapePL);

  static
  VISU_VectorsPL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  virtual
  void
  SetScale(double theScale);

  virtual
  double
  GetScale();

  enum GlyphType{ ARROW, CONE2, CONE6, NONE};

  virtual
  void
  SetGlyphType(GlyphType theType);

  virtual
  GlyphType
  GetGlyphType() const;
    
  enum GlyphPos{ CENTER, TAIL,HEAD};

  virtual
  void
  SetGlyphPos(GlyphPos thePos);

  virtual
  GlyphPos
  GetGlyphPos() const;

public:
  virtual
  void
  Init();

  virtual
  void
  Build();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  vtkDataSet* 
  GetOutput();

  virtual
  void
  SetTransform(VTKViewer_Transform* theTransform); 

  virtual
  VTKViewer_Transform* 
  GetTransform();
  
  virtual
  void
  SetMapScale(double theMapScale = 1.0);

protected:
  VISU_VectorsPL();

  virtual
  ~VISU_VectorsPL();

  virtual
  vtkAlgorithmOutput* 
  InsertCustomPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  GlyphType myTypeGlyph;
  GlyphPos myPosGlyph;
  vtkGlyph3D *myGlyph;

  vtkGlyphSource2D *myGlyphSource;
  vtkLineSource *myLineSource;
  vtkConeSource *myConeSource;

  VTKViewer_CellCenters* myCenters;
  VTKViewer_TransformFilter *myTransformFilter;

  VISU_UsedPointsFilter* myUsedPointsFilter;

private:
  VISU_VectorsPL(const VISU_VectorsPL&);  // Not implemented.
  void operator=(const VISU_VectorsPL&);  // Not implemented.
};


#endif

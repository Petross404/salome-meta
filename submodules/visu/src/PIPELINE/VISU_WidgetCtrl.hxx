// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : VISU_WidgetCtrl.hxx
//  Author : Peter KURNEV
//  Module : SALOME
//  $Header$
//
#ifndef __VISU_WidgetCtrl_hxx
#define __VISU_WidgetCtrl_hxx

#include "VISUPipeline.hxx"
#include <vtkImplicitFunction.h>

#include "VTKViewer.h"

class vtkImplicitFunction;
class vtkObject;
class vtkRenderWindowInteractor;
class vtkCommand;
class vtkCallbackCommand;

class VISU_PlanesWidget;
class VISU_SphereWidget;
class VISU_ImplicitFunctionWidget;

class VISU_PIPELINE_EXPORT VISU_WidgetCtrl : public vtkImplicitFunction
{
public:
  static VISU_WidgetCtrl *New();
  vtkTypeMacro(VISU_WidgetCtrl,vtkObject);

  vtkImplicitFunction* ImplicitFunction();

  virtual
  double  
  EvaluateFunction(double theX[3]);

  virtual
  void  
  EvaluateGradient(double theX[3], 
                   double theG[3]);

  virtual
  unsigned long
  GetMTime();

  void PlaceWidget(double theBounds[6]);
  void SetEnabled(int theFlag);
  int  GetEnabled();
  void On() {SetEnabled(1);}
  void Off(){SetEnabled(0);}
  
  void SetInteractor(vtkRenderWindowInteractor* theRWI);
  vtkRenderWindowInteractor* GetInteractor();
  //
  void  
  SetPlaceFactor(double theFactor);

  double
  GetPlaceFactor();
  //
  void SetActiveIndex(const int theFlag);
  int  GetActiveIndex()const;
  bool HasActiveIndex()const;
  bool IsPlanesActive()const;
  bool IsSphereActive()const;

  
  VISU_ImplicitFunctionWidget* GetActiveWidget();
  VISU_ImplicitFunctionWidget* GetWidget(const int);

  int GetNbWidgets()const;
  int GetDisableAll()const;
  
  //
  VISU_PlanesWidget * GetPlanesWidget();
  VISU_SphereWidget * GetSphereWidget();
  //
  static void ProcessEvents(vtkObject* theObject, 
                            unsigned long theEvent,
                            void* theClientData, 
                            void* theCalldata);
  //
protected:
  VISU_WidgetCtrl();
  ~VISU_WidgetCtrl();

protected:
  int myNbWidgets;
  int myActiveIndex;
  int myCounter;
  int myDisableAll;
  double myPriority;
  VISU_ImplicitFunctionWidget *myWidgets[2];
  VISU_ImplicitFunctionWidget *myDummyWidget;
  VISU_PlanesWidget *myPlanesWidget;
  VISU_SphereWidget *mySphereWidget;
  vtkCallbackCommand *myEventCallbackCommand;

private:
  VISU_WidgetCtrl(const VISU_WidgetCtrl&);  //Not implemented
  void operator=(const VISU_WidgetCtrl&);   //Not implemented
};

#endif

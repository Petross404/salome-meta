// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_CutLinesPL_HeaderFile
#define VISU_CutLinesPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_CutLinesBasePL.hxx"

class vtkAppendPolyData;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_CutLinesPL : public VISU_CutLinesBasePL
{
public:
  vtkTypeMacro(VISU_CutLinesPL,VISU_CutLinesBasePL);

  static 
  VISU_CutLinesPL* 
  New();

  virtual
  void
  SetPosition(double thePosition);

  virtual
  double 
  GetPosition();

  virtual
  void
  SetDefault();

  virtual
  int
  IsDefault();

public:
  virtual
  void
  Init();

  vtkAlgorithmOutput*
  InsertCustomPL();

  virtual
  void
  Update();

  static
  void
  CutWithPlanes(vtkAppendPolyData* theAppendPolyData, 
                vtkDataSet* theDataSet,
                int theNbPlanes, 
                double theDir[3], 
                double theBounds[6], 
                double thePlanePosition, 
                int thePlaneCondition,
                double theDisplacement);

protected:
  VISU_CutLinesPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  double myPosition;
  int myCondition;

private:
  VISU_CutLinesPL(const VISU_CutLinesPL&);  // Not implemented.
  void operator=(const VISU_CutLinesPL&);  // Not implemented.
};


#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU DeformedShapeAndScalarMapPL
// File:    VISU_DeformedShapeAndScalarMapPL.cxx
// Author:  Eugeny Nikolaev
// Module : VISU
//
#include "VISU_DeformedShapeAndScalarMapPL.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_Extractor.hxx"
#include "VISU_LookupTable.hxx"
#include "VISU_DeformedShapePL.hxx"
#include "VTKViewer_TransformFilter.h"
#include "VTKViewer_Transform.h"
#include "VISU_MergeFilter.hxx"
#include "VISU_ElnoDisassembleFilter.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "SALOME_ExtractGeometry.h"

#include <vtkPlane.h>
#include <vtkWarpVector.h>
#include <vtkImplicitBoolean.h>
#include <vtkImplicitFunction.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointDataToCellData.h>
#include <vtkImplicitFunctionCollection.h>

#ifdef WNT
#include <float.h>
#define isnan _isnan
#endif

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_DeformedShapeAndScalarMapPL)

//----------------------------------------------------------------------------
/*!
 * Constructor. Creating new instances of vtkWarpVector,vtkMergeFilter,vtkUnstructuredGrid
 * Where:
 * \li myDeformVectors is vtkWarpVector  - deformation vectors data
 * \li myScalarsMergeFilter   is vtkMergeFilter - merge filter.
 * Merge filter which unify the deformation and scalars
 * \li myScalars is vtk shared pointer to vtkUnstructuredGrid - scalars data
*/
VISU_DeformedShapeAndScalarMapPL
::VISU_DeformedShapeAndScalarMapPL():
  myScaleFactor(1.0),
  myMapScaleFactor(1.0)
{
  myWarpVector = vtkWarpVector::New();

  myScalarsMergeFilter = VISU_MergeFilter::New();
  myScalarsMergeFilter->SetMergingInputs(true);

  myScalarsExtractor = VISU_Extractor::New();

  myScalarsFieldTransform = VISU_FieldTransform::New();

  myCellDataToPointData = VISU_CellDataToPointData::New();
  myScalarsElnoDisassembleFilter = VISU_ElnoDisassembleFilter::New();

  vtkImplicitBoolean* anImplicitBoolean = vtkImplicitBoolean::New();
  anImplicitBoolean->SetOperationTypeToIntersection();

  myExtractGeometry = SALOME_ExtractGeometry::New();
  myExtractGeometry->SetImplicitFunction(anImplicitBoolean);
}

//----------------------------------------------------------------------------
/*!
 * Destructor.
 * Delete all fields.
*/
VISU_DeformedShapeAndScalarMapPL
::~VISU_DeformedShapeAndScalarMapPL()
{
  myWarpVector->Delete();

  myScalarsMergeFilter->Delete();
  
  myScalarsExtractor->Delete();

  myScalarsFieldTransform->Delete();

  myCellDataToPointData->Delete();
}

//----------------------------------------------------------------------------
/*!
 * Initial method
 */
void
VISU_DeformedShapeAndScalarMapPL
::Init()
{
  Superclass::Init();
  
  SetScale(VISU_DeformedShapePL::GetDefaultScale(this));
}

//----------------------------------------------------------------------------
/*!
 * Build method
 * Building of deformation and puts result to merge filter.
 */
void
VISU_DeformedShapeAndScalarMapPL
::Build()
{
  Superclass::Build();
}


//----------------------------------------------------------------------------
vtkAlgorithmOutput* 
VISU_DeformedShapeAndScalarMapPL
::InsertCustomPL()
{
  GetMapper()->SetColorModeToMapScalars();
  GetMapper()->ScalarVisibilityOn();

  VISU::CellDataToPoint(myWarpVector,
                        myCellDataToPointData,
                        GetMergedInput(),
                        GetMergedInputPort());
  
  myScalars = vtkUnstructuredGrid::SafeDownCast(GetMergedInput());

  UpdateScalars();

  myScalarsFieldTransform->SetInputConnection(myScalarsExtractor->GetOutputPort());

  // Sets geometry for merge filter
  myScalarsMergeFilter->SetGeometryConnection(myWarpVector->GetOutputPort());

  vtkDataSet* aScalarsDataSet = myScalarsFieldTransform->GetOutput();
  myScalarsFieldTransform->Update();

  vtkAlgorithmOutput* aScalarsConnection = myScalarsFieldTransform->GetOutputPort();
  myScalarsMergeFilter->SetScalarsConnection(aScalarsConnection);
  myScalarsMergeFilter->AddField("VISU_CELLS_MAPPER", aScalarsDataSet);
  myScalarsMergeFilter->AddField("VISU_POINTS_MAPPER", aScalarsDataSet);

  return myScalarsMergeFilter->GetOutputPort();
}


//----------------------------------------------------------------------------
/*!
 *  Update method
 */
void
VISU_DeformedShapeAndScalarMapPL
::Update()
{
  Superclass::Update();
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myScalarsExtractor.vtk";
  //  VISU::WriteToFile(myScalarsExtractor->GetUnstructuredGridOutput(), aFileName);
  //}
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myWarpVector.vtk";
  //  VISU::WriteToFile(myWarpVector->GetUnstructuredGridOutput(), aFileName);
  //}
  //{
  //  std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-myScalarsMergeFilter.vtk";
  //  VISU::WriteToFile(myScalarsMergeFilter->GetUnstructuredGridOutput(), aFileName);
  //}
}

//----------------------------------------------------------------------------
unsigned long int
VISU_DeformedShapeAndScalarMapPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataSet* aDataSet = myWarpVector->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;
  
  if(vtkDataSet* aDataSet = myScalarsExtractor->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;

  if(vtkDataSet* aDataSet = myScalarsMergeFilter->GetOutput())
    aSize += aDataSet->GetActualMemorySize() * 1024;

  if(myCellDataToPointData->GetInput())
    if(vtkDataSet* aDataSet = myCellDataToPointData->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}

//----------------------------------------------------------------------------
/*!
 * Update scalars method.
 * Put scalars to merge filter.
 */
void
VISU_DeformedShapeAndScalarMapPL
::UpdateScalars()
{
  vtkDataSet* aScalars = GetScalars();
  myScalarsElnoDisassembleFilter->SetInputData(aScalars);
  myExtractGeometry->SetInputConnection(myScalarsElnoDisassembleFilter->GetOutputPort());
  myScalarsExtractor->SetInputConnection(myExtractGeometry->GetOutputPort());

  vtkDataSet* aDataSet = myScalarsElnoDisassembleFilter->GetOutput();
  myScalarsElnoDisassembleFilter->Update();
  if(VISU::IsDataOnCells(aDataSet))
    GetMapper()->SetScalarModeToUseCellData();
  else
    GetMapper()->SetScalarModeToUsePointData();
}

//----------------------------------------------------------------------------
/*!
 * Copy information about pipline.
 * Copy scale and scalars.
 */
void
VISU_DeformedShapeAndScalarMapPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_DeformedShapeAndScalarMapPL *aPipeLine = dynamic_cast<VISU_DeformedShapeAndScalarMapPL*>(thePipeLine)){
     SetImplicitFunction(aPipeLine->GetImplicitFunction());
     SetScale(aPipeLine->GetScale());
     SetScalars(aPipeLine->GetScalars());
  }
}

//----------------------------------------------------------------------------
/*!
 * Set scalars.
 * Sets vtkDataSet with scalars values to VISU_Extractor filter for scalars extraction.
 */
void
VISU_DeformedShapeAndScalarMapPL
::SetScalars(vtkDataSet *theScalars)
{
  if(GetScalars() == theScalars)
    return;
  
  myScalars = vtkUnstructuredGrid::SafeDownCast(theScalars);
  UpdateScalars();
}

//----------------------------------------------------------------------------
/*!
 * Get pointer to input scalars.
 */
vtkDataSet* 
VISU_DeformedShapeAndScalarMapPL
::GetScalars()
{
  return myScalars.GetPointer();
}

//----------------------------------------------------------------------------
/*!
 * Removes all clipping planes (for myScalars)
 */
void
VISU_DeformedShapeAndScalarMapPL
::RemoveAllClippingPlanes()
{
  if(vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()){
    vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
    aFunction->RemoveAllItems();
    aBoolean->Modified();
  }
  Superclass::RemoveAllClippingPlanes();
}

//----------------------------------------------------------------------------
/*!
 * Removes a clipping plane (for myScalars)
 */
void
VISU_DeformedShapeAndScalarMapPL
::RemoveClippingPlane(vtkIdType theID)
{
  if(vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()){
    vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
    if(theID >= 0 && theID < aFunction->GetNumberOfItems())
      aFunction->RemoveItem(theID);
    aBoolean->Modified();
  }
  Superclass::RemoveClippingPlane(theID);
}

//----------------------------------------------------------------------------
/*!
 * Adds a clipping plane (for myScalars)
 */
bool 
VISU_DeformedShapeAndScalarMapPL
::AddClippingPlane(vtkPlane* thePlane)
{
  if (thePlane) {
    if (vtkImplicitBoolean* aBoolean = myExtractGeometry->GetImplicitBoolean()) {
      vtkImplicitFunctionCollection* aFunction = aBoolean->GetFunction();
      aFunction->AddItem(thePlane);

      // Check, that at least one cell present after clipping.
      // This check was introduced because of bug IPAL8849.
      vtkDataSet* aClippedDataSet = GetClippedInput();
      if(aClippedDataSet->GetNumberOfCells() < 1)
        return false;
    }
  }
  return Superclass::AddClippingPlane(thePlane);
}

//----------------------------------------------------------------------------
/*!
 * Sets implicit function of clipping
 */
void
VISU_DeformedShapeAndScalarMapPL
::SetImplicitFunction(vtkImplicitFunction *theFunction)
{
  myExtractGeometry->SetImplicitFunction(theFunction);
} 

//----------------------------------------------------------------------------
/*!
 * Gets implicit function of clipping
 */
vtkImplicitFunction * 
VISU_DeformedShapeAndScalarMapPL
::GetImplicitFunction()
{
  return myExtractGeometry->GetImplicitFunction();
}

//----------------------------------------------------------------------------
/*!
 * Sets scale for deformed shape
 */
void
VISU_DeformedShapeAndScalarMapPL
::SetScale(double theScale) 
{
  if(VISU::CheckIsSameValue(myScaleFactor, theScale))
    return;

  myScaleFactor = theScale;
  myWarpVector->SetScaleFactor(theScale*myMapScaleFactor);
}

//----------------------------------------------------------------------------
/*!
 * Gets scale of deformed shape.
 */
double
VISU_DeformedShapeAndScalarMapPL
::GetScale() 
{
  return myScaleFactor;
}

//----------------------------------------------------------------------------
/*!
 * Set scale factor of deformation.
 */
void
VISU_DeformedShapeAndScalarMapPL
::SetMapScale(double theMapScale)
{
  myMapScaleFactor = theMapScale;
  Superclass::SetMapScale(theMapScale);
  myWarpVector->SetScaleFactor(myScaleFactor*theMapScale);
}

//----------------------------------------------------------------------------
/*!
 * Gets scalar mode.
 */
int
VISU_DeformedShapeAndScalarMapPL
::GetScalarMode()
{
  return myScalarsExtractor->GetScalarMode();
}

//----------------------------------------------------------------------------
/*!
 * Sets scalar mode.
 */
void
VISU_DeformedShapeAndScalarMapPL
::SetScalarMode(int theScalarMode)
{
  VISU_ScalarMapPL::SetScalarMode(theScalarMode, GetScalars(), myScalarsExtractor);
}

//----------------------------------------------------------------------------
void
VISU_DeformedShapeAndScalarMapPL
::SetScaling(int theScaling) 
{
  if(GetScaling() == theScaling)
    return;

  GetBarTable()->SetScale(theScaling);

  if(theScaling == VTK_SCALE_LOG10)
    myScalarsFieldTransform->SetScalarTransform(&(VISU_FieldTransform::Log10));
  else
    myScalarsFieldTransform->SetScalarTransform(&(VISU_FieldTransform::Ident));
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapeAndScalarMapPL
::SetScalarRange(double theRange[2])
{
  if (isnan(theRange[0]) || isnan(theRange[1]))
    throw std::runtime_error("NAN values in the presentation");

  if(VISU::CheckIsSameRange(theRange, GetScalarRange()))
    return;

  myScalarsFieldTransform->SetScalarRange(theRange);
  GetBarTable()->SetRange(theRange);
}


//----------------------------------------------------------------------------
double* 
VISU_DeformedShapeAndScalarMapPL
::GetScalarRange() 
{
  return myScalarsFieldTransform->GetScalarRange();
}


//----------------------------------------------------------------------------
/*!
 * Gets ranges of extracted scalars
 * \param theRange[2] - output values
 * \li theRange[0] - minimum value
 * \li theRange[1] - maximum value
 */
void 
VISU_DeformedShapeAndScalarMapPL
::GetSourceRange(double theRange[2])
{
  myScalarsExtractor->Update();
  myScalarsExtractor->GetUnstructuredGridOutput()->GetScalarRange(theRange);

  if (isnan(theRange[0]) || isnan(theRange[1]))
    throw std::runtime_error("NAN values in the presentation");
}


//----------------------------------------------------------------------------
void
VISU_DeformedShapeAndScalarMapPL
::SetGaussMetric(VISU::TGaussMetric theGaussMetric) 
{
  if(GetGaussMetric() == theGaussMetric)
    return;

  myScalarsExtractor->SetGaussMetric(theGaussMetric);
}


//----------------------------------------------------------------------------
VISU::TGaussMetric
VISU_DeformedShapeAndScalarMapPL
::GetGaussMetric() 
{
  return myScalarsExtractor->GetGaussMetric();
}

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_OpenGLPointSpriteMapper.cxx
// Author:  
// Module : VISU
//
#include "VISU_OpenGLPointSpriteMapper.hxx"

//#include "SVTK_Extension.h"

#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkCommand.h>
#include <vtkImageData.h>
#include <vtkMatrix4x4.h>
#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolygon.h>
#include <vtkProperty.h>
#include <vtkTimerLog.h>
#include <vtkTriangle.h>

#include <stdio.h>
#include <cmath>
#include <string>

#include "utilities.h"

#ifndef WNT
# ifndef GLX_GLXEXT_LEGACY
#  define GLX_GLXEXT_LEGACY
# endif
# include <GL/glx.h>
# include <dlfcn.h>
#else
# include <wingdi.h>
#endif

#ifndef VTK_IMPLEMENT_MESA_CXX
vtkStandardNewMacro(VISU_OpenGLPointSpriteMapper);
#endif

// some definitions for what the polydata has in it
#define VTK_PDPSM_COLORS             0x0001
#define VTK_PDPSM_CELL_COLORS        0x0002
#define VTK_PDPSM_POINT_TYPE_FLOAT   0x0004
#define VTK_PDPSM_POINT_TYPE_DOUBLE  0x0008
#define VTK_PDPSM_NORMAL_TYPE_FLOAT  0x0010
#define VTK_PDPSM_NORMAL_TYPE_DOUBLE 0x0020
#define VTK_PDPSM_OPAQUE_COLORS      0x0040

#ifndef APIENTRY
#define APIENTRY
#endif
#ifndef APIENTRYP
#define APIENTRYP APIENTRY *
#endif

#ifndef GL_OBJECT_INFO_LOG_LENGTH_ARB
#define GL_OBJECT_INFO_LOG_LENGTH_ARB     0x8B84
#endif

#ifndef GL_VERTEX_SHADER_ARB
#define GL_VERTEX_SHADER_ARB              0x8B31
#endif

#ifndef GL_VERTEX_PROGRAM_POINT_SIZE_ARB
#define GL_VERTEX_PROGRAM_POINT_SIZE_ARB  0x8642
#endif

#ifndef GL_ARB_point_sprite
#define GL_POINT_SPRITE_ARB               0x8861
#define GL_COORD_REPLACE_ARB              0x8862
#endif

#ifndef GL_ARB_shader_objects
typedef char GLcharARB;
#endif

#ifndef GL_ARB_vertex_buffer_object
typedef ptrdiff_t GLsizeiptrARB;

#define GL_ARRAY_BUFFER_ARB               0x8892
#define GL_STATIC_DRAW_ARB                0x88E4
#endif

typedef void (APIENTRYP PFNGLSHADERSOURCEARBPROC) (GLhandleARB shaderObj, GLsizei count, const GLcharARB* *string, const GLint *length);
typedef GLhandleARB (APIENTRYP PFNGLCREATESHADEROBJECTARBPROC) (GLenum shaderType);
typedef void (APIENTRYP PFNGLCOMPILESHADERARBPROC) (GLhandleARB shaderObj);
typedef GLhandleARB (APIENTRYP PFNGLCREATEPROGRAMOBJECTARBPROC) (void);
typedef void (APIENTRYP PFNGLATTACHOBJECTARBPROC) (GLhandleARB containerObj, GLhandleARB obj);
typedef void (APIENTRYP PFNGLLINKPROGRAMARBPROC) (GLhandleARB programObj);
typedef void (APIENTRYP PFNGLUSEPROGRAMOBJECTARBPROC) (GLhandleARB programObj);
typedef void (APIENTRYP PFNGLGETOBJECTPARAMETERIVARBPROC) (GLhandleARB obj, GLenum pname, GLint *params);
typedef void (APIENTRYP PFNGLGETINFOLOGARBPROC) (GLhandleARB obj, GLsizei maxLength, GLsizei *length, GLcharARB *infoLog);
typedef GLint (APIENTRYP PFNGLGETATTRIBLOCATIONARBPROC) (GLhandleARB programObj, const GLcharARB *name);
typedef void (APIENTRYP PFNGLVERTEXATTRIB1FARBPROC) (GLuint index, GLfloat x);

typedef void (APIENTRYP PFNGLBINDBUFFERARBPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRYP PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);
typedef void (APIENTRYP PFNGLGENBUFFERSARBPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRYP PFNGLBUFFERDATAARBPROC) (GLenum target, GLsizeiptrARB size, const GLvoid *data, GLenum usage);

static PFNGLSHADERSOURCEARBPROC             vglShaderSourceARB            = NULL;
static PFNGLCREATESHADEROBJECTARBPROC       vglCreateShaderObjectARB      = NULL;
static PFNGLCOMPILESHADERARBPROC            vglCompileShaderARB           = NULL;
static PFNGLCREATEPROGRAMOBJECTARBPROC      vglCreateProgramObjectARB     = NULL;
static PFNGLATTACHOBJECTARBPROC             vglAttachObjectARB            = NULL;
static PFNGLLINKPROGRAMARBPROC              vglLinkProgramARB             = NULL;
static PFNGLUSEPROGRAMOBJECTARBPROC         vglUseProgramObjectARB        = NULL;
static PFNGLGETOBJECTPARAMETERIVARBPROC     vglGetObjectParameterivARB    = NULL;
static PFNGLGETINFOLOGARBPROC               vglGetInfoLogARB              = NULL;
static PFNGLGETATTRIBLOCATIONARBPROC        vglGetAttribLocationARB       = NULL;
static PFNGLVERTEXATTRIB1FARBPROC           vglVertexAttrib1fARB          = NULL;

static PFNGLGENBUFFERSARBPROC               vglGenBuffersARB              = NULL;
static PFNGLBINDBUFFERARBPROC               vglBindBufferARB              = NULL;
static PFNGLBUFFERDATAARBPROC               vglBufferDataARB              = NULL;
static PFNGLDELETEBUFFERSARBPROC            vglDeleteBuffersARB           = NULL;

#ifndef WNT
#define GL_GetProcAddress( x )   glXGetProcAddressARB( (const GLubyte*)x )
#else
#define GL_GetProcAddress( x )   wglGetProcAddress( (const LPCSTR)x )
#endif

bool InitializeARB()
{
  vglShaderSourceARB = (PFNGLSHADERSOURCEARBPROC)GL_GetProcAddress( "glShaderSourceARB" );
  if( !vglShaderSourceARB )
    return false;

  vglCreateShaderObjectARB = (PFNGLCREATESHADEROBJECTARBPROC)GL_GetProcAddress( "glCreateShaderObjectARB" );
  if( !vglCreateShaderObjectARB )
    return false;

  vglCompileShaderARB = (PFNGLCOMPILESHADERARBPROC)GL_GetProcAddress( "glCompileShaderARB" );
  if( !vglCompileShaderARB )
    return false;

  vglCreateProgramObjectARB = (PFNGLCREATEPROGRAMOBJECTARBPROC)GL_GetProcAddress( "glCreateProgramObjectARB" );
  if( !vglCreateProgramObjectARB )
    return false;

  vglAttachObjectARB = (PFNGLATTACHOBJECTARBPROC)GL_GetProcAddress( "glAttachObjectARB" );
  if( !vglAttachObjectARB )
    return false;

  vglLinkProgramARB = (PFNGLLINKPROGRAMARBPROC)GL_GetProcAddress( "glLinkProgramARB" );
  if( !vglLinkProgramARB )
    return false;

  vglUseProgramObjectARB = (PFNGLUSEPROGRAMOBJECTARBPROC)GL_GetProcAddress( "glUseProgramObjectARB" );
  if( !vglUseProgramObjectARB )
    return false;

  vglGetObjectParameterivARB = (PFNGLGETOBJECTPARAMETERIVARBPROC)GL_GetProcAddress( "glGetObjectParameterivARB" );
  if( !vglGetObjectParameterivARB )
    return false;

  vglGetInfoLogARB = (PFNGLGETINFOLOGARBPROC)GL_GetProcAddress( "glGetInfoLogARB" );
  if( !vglGetInfoLogARB )
    return false;

  vglGetAttribLocationARB = (PFNGLGETATTRIBLOCATIONARBPROC)GL_GetProcAddress( "glGetAttribLocationARB" );
  if( !vglGetAttribLocationARB )
    return false;

  vglVertexAttrib1fARB = (PFNGLVERTEXATTRIB1FARBPROC)GL_GetProcAddress( "glVertexAttrib1fARB" );
  if( !vglVertexAttrib1fARB )
    return false;

  vglGenBuffersARB = (PFNGLGENBUFFERSARBPROC)GL_GetProcAddress( "glGenBuffersARB" );
  if( !vglGenBuffersARB )
    return false;

  vglBindBufferARB = (PFNGLBINDBUFFERARBPROC)GL_GetProcAddress( "glBindBufferARB" );
  if( !vglBindBufferARB )
    return false;

  vglBufferDataARB = (PFNGLBUFFERDATAARBPROC)GL_GetProcAddress( "glBufferDataARB" );
  if( !vglBufferDataARB )
    return false;

  vglDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC)GL_GetProcAddress( "glDeleteBuffersARB" );
  if( !vglDeleteBuffersARB )
    return false;

  return true;
};

static bool IsARBInitialized = InitializeARB();
static double Tolerance = 1.0 / VTK_LARGE_FLOAT;

//-----------------------------------------------------------------------------
// Construct empty object.
VISU_OpenGLPointSpriteMapper::VISU_OpenGLPointSpriteMapper()
{
  this->RenderMode               = VISU_OpenGLPointSpriteMapper::Occlude;

  this->ListId                   = 0;
  this->TotalCells               = 0;
  this->ExtensionsInitialized    = 0;
  this->DefaultPointSize         = 20.0;
  this->AverageCellSize          = 0.0;

  this->UsePointSprites          = true;
  this->UseTextures              = true;
  this->UseShader                = true;

  this->PrimitiveType            = VISU_OpenGLPointSpriteMapper::PointSprite;

  this->PointSpriteMode          = 0;

  this->PointSpriteClamp         = 256.0;
  this->PointSpriteSize          = 0.2;
  this->PointSpriteMinSize       = 0.1;
  this->PointSpriteMaxSize       = 0.3;
  this->PointSpriteMagnification = 1.0;

  this->PointSpriteAlphaThreshold = 0.5;
  this->PointSpriteOpacity       = 1.0;
  this->PointSpriteTexture       = 0;

  this->UseOpenGLMapper          = false;
}
//-----------------------------------------------------------------------------
VISU_OpenGLPointSpriteMapper::~VISU_OpenGLPointSpriteMapper()
{
  if( PointSpriteTexture>0 )
    glDeleteTextures( 1, &PointSpriteTexture );

  if( this->LastWindow )
    this->ReleaseGraphicsResources(this->LastWindow);
}

//-----------------------------------------------------------------------------
char* readFromFile( std::string fileName )
{
  FILE* file = fopen( fileName.c_str(), "r" );

  char* content = NULL;
  int count = 0;

  if( file != NULL )
  {
    fseek( file, 0, SEEK_END );
    count = ftell( file );
    rewind( file );

    if( count > 0 )
    {
      content = ( char* )malloc( sizeof( char ) * ( count + 1 ) );
      count = fread( content, sizeof( char ), count, file );
      content[ count ] = '\0';
    }
    fclose( file );
  }

  return content;
}
//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::PrintInfoLog( GLhandleARB obj )
{
  int infologLength = 0;
  int charsWritten  = 0;
  char* infoLog;

  vglGetObjectParameterivARB( obj, GL_OBJECT_INFO_LOG_LENGTH_ARB, &infologLength );

  if( infologLength > 0 )
  {
    infoLog = ( char* )malloc( infologLength );
    vglGetInfoLogARB( obj, infologLength, &charsWritten, infoLog );
    printf( "%s\n", infoLog );
    free( infoLog );
  }
}
//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::InitShader()
{
  //cout << "Initializing vertex program" << endl;

  std::string fileName = std::string( getenv( "VISU_ROOT_DIR") ) +
                         "/share/salome/resources/visu/Vertex_Program_ARB.txt";

  char* shader = readFromFile( fileName );

  GLhandleARB VertexShader = vglCreateShaderObjectARB( GL_VERTEX_SHADER_ARB );
  vglShaderSourceARB( VertexShader, 1, (const GLcharARB**)&shader, NULL );
  vglCompileShaderARB( VertexShader );
  //this->PrintInfoLog( VertexShader );

  this->VertexProgram = vglCreateProgramObjectARB();
  vglAttachObjectARB( this->VertexProgram, VertexShader );

  vglLinkProgramARB( this->VertexProgram );
  //this->PrintInfoLog( VertexProgram );
  /*
  cout << "Shader from " << fileName << endl;
  for( int i = 0; i < strlen( shader ); i++ )
    cout << shader[i];
  cout << endl;

  if( glGetError() == GL_NO_ERROR )
    cout << "Loading vertex program... ok" << endl << endl;
  else
    cout << "Loading vertex program... failed" << endl << endl;
  */
  free( shader );
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetShaderVariable( const char* variable, float value )
{
  //cout << this->VertexProgram << " ";
  //cout << vglGetAttribLocationARB( this->VertexProgram, variable ) << " ";
  //cout << variable << " " << value << endl;

  vglVertexAttrib1fARB( vglGetAttribLocationARB( this->VertexProgram, variable ), value );
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPrimitiveType( int thePrimitiveType )
{
  if( this->PrimitiveType == thePrimitiveType )
    return;

  this->PrimitiveType = thePrimitiveType;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteMode( int theMode )
{
  if( this->PointSpriteMode == theMode )
    return;

  this->PointSpriteMode = theMode;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteClamp( float theClamp )
{
  if( fabs( this->PointSpriteClamp - theClamp ) < Tolerance )
    return;

  this->PointSpriteClamp = theClamp;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetAverageCellSize(float theSize)
{
  if( fabs( this->AverageCellSize - theSize ) < Tolerance )
    return;

  this->AverageCellSize = theSize;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteSize( float theSize )
{
  if( fabs( this->PointSpriteSize - theSize ) < Tolerance )
    return;

  this->PointSpriteSize = theSize;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteMinSize( float theMinSize )
{
  if( fabs( this->PointSpriteMinSize - theMinSize ) < Tolerance )
    return;

  this->PointSpriteMinSize = theMinSize;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteMaxSize( float theMaxSize )
{
  if( fabs( this->PointSpriteMaxSize - theMaxSize ) < Tolerance )
    return;

  this->PointSpriteMaxSize = theMaxSize;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteMagnification( float theMagnification )
{
  if( fabs( this->PointSpriteMagnification - theMagnification ) < Tolerance )
    return;

  this->PointSpriteMagnification = theMagnification;
  this->Modified();
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::SetPointSpriteAlphaThreshold( float theAlphaThreshold )
{
  if( fabs( this->PointSpriteAlphaThreshold - theAlphaThreshold ) < Tolerance )
    return;

  this->PointSpriteAlphaThreshold = theAlphaThreshold;
  this->Modified();
}

//-----------------------------------------------------------------------------
bool VISU_OpenGLPointSpriteMapper::InitExtensions()
{
  if( this->ExtensionsInitialized )
    return true;

  IsARBInitialized = InitializeARB();

  char* ext = (char*)glGetString( GL_EXTENSIONS );
  //cout << "OpenGL extensions : " << ext << endl;

  if( !IsARBInitialized ||
      strstr( ext, "GL_ARB_point_sprite" ) == NULL ||
      strstr( ext, "GL_ARB_shader_objects" ) == NULL ||
      strstr( ext, "GL_ARB_vertex_buffer_object" ) == NULL )
  {
    vtkWarningMacro(<<"Initializing ARB extensions failed");
    this->UseOpenGLMapper = true;

    return false;
  }

  if( this->UseShader )
    this->InitShader();

  this->ExtensionsInitialized = 1;
  return true;
}

//-----------------------------------------------------------------------------
float ViewToDisplay( vtkRenderer* theRenderer )
{
  double p1[3], p2[3];

  theRenderer->SetViewPoint( 0.0, 0.0, 0.0 );
  theRenderer->ViewToDisplay();
  theRenderer->GetDisplayPoint( p1 );

  theRenderer->SetViewPoint( 1.0, 1.0, 1.0 );
  theRenderer->ViewToDisplay();
  theRenderer->GetDisplayPoint( p2 );

  double coefficient = sqrt( pow( p2[0] - p1[0], 2 ) + pow( p2[1] - p1[1], 2 ) ) / sqrt( 2. );
  //cout << p1[0] << " " << p1[1] << " " << p1[2] << endl;
  //cout << p2[0] << " " << p2[1] << " " << p2[2] << endl;
  //cout << "ZOOM  : " << coefficient << endl;

  return coefficient;
}

//-----------------------------------------------------------------------------
//
// Receives from Actor -> maps data to primitives
//
void VISU_OpenGLPointSpriteMapper::RenderPiece(vtkRenderer *ren, vtkActor *act)
{
  bool isUseThisMapper = this->PrimitiveType != VISU_OpenGLPointSpriteMapper::GeomSphere;

  if( isUseThisMapper )
    this->InitExtensions();

  if( !isUseThisMapper )
  {
    MAPPER_SUPERCLASS::RenderPiece( ren, act );
    return;
  }

  vtkIdType numPts;
  vtkPolyData *input= this->GetInput();

  //
  // make sure that we've been properly initialized
  //
  if (ren->GetRenderWindow()->CheckAbortStatus())
    return;

  if ( input == NULL )
  {
    vtkErrorMacro(<< "No input!");
    return;
  }
  else
  {
    this->InvokeEvent(vtkCommand::StartEvent,NULL);
    this->Update();
    this->InvokeEvent(vtkCommand::EndEvent,NULL);

    numPts = input->GetNumberOfPoints();
  }

  if (numPts == 0)
  {
    vtkDebugMacro(<< "No points!");
    return;
  }

  if ( this->LookupTable == NULL )
    this->CreateDefaultLookupTable();

  // make sure our window is current
  ren->GetRenderWindow()->MakeCurrent();

  if( this->UsePointSprites ) //&& this->PrimitiveType == VISU_OpenGLPointSpriteMapper::PointSprite )
    this->InitPointSprites();

  // Initializing the texture for Point Sprites
  if( this->UseTextures && this->PrimitiveType == VISU_OpenGLPointSpriteMapper::PointSprite )
    this->InitTextures();

  vglUseProgramObjectARB( this->VertexProgram );
  float aViewToDisplay = ViewToDisplay( ren );
  this->SetShaderVariable( "mode",          this->PointSpriteMode );
  this->SetShaderVariable( "clampSize",     this->PointSpriteClamp );
  this->SetShaderVariable( "geomSize",      aViewToDisplay * this->AverageCellSize * this->PointSpriteSize );
  this->SetShaderVariable( "minSize",       aViewToDisplay * this->AverageCellSize * this->PointSpriteMinSize );
  this->SetShaderVariable( "maxSize",       aViewToDisplay * this->AverageCellSize * this->PointSpriteMaxSize );
  this->SetShaderVariable( "magnification", this->PointSpriteMagnification );

  //
  // if something has changed regenerate colors and display lists
  // if required
  //
  int noAbort=1;
  if ( this->GetMTime() > this->BuildTime ||
       input->GetMTime() > this->BuildTime ||
       act->GetProperty()->GetMTime() > this->BuildTime ||
       ren->GetRenderWindow() != this->LastWindow)
  {
#ifdef _DEBUG_RENDERING_PERFORMANCE_
    // To control when the mapper is recalculated
    MESSAGE( "VISU_OpenGLPointSpriteMapper::RenderPiece - "
             <<(this->GetMTime() > this->BuildTime)<<"; "
             <<(input->GetMTime() > this->BuildTime)<<"; "
             <<(act->GetProperty()->GetMTime() > this->BuildTime)<<"; ");
#endif
    // sets this->Colors as side effect
    this->MapScalars( act->GetProperty()->GetOpacity() );

    if (!this->ImmediateModeRendering &&
        !this->GetGlobalImmediateModeRendering())
    {
      this->ReleaseGraphicsResources(ren->GetRenderWindow());
      this->LastWindow = ren->GetRenderWindow();

      // get a unique display list id
      this->ListId = glGenLists(1);
      glNewList(this->ListId,GL_COMPILE);

      noAbort = this->Draw(ren,act);
      glEndList();

      // Time the actual drawing
      this->Timer->StartTimer();
      glCallList(this->ListId);
      this->Timer->StopTimer();
    }
    else
    {
      this->ReleaseGraphicsResources(ren->GetRenderWindow());
      this->LastWindow = ren->GetRenderWindow();
    }
    if (noAbort)
      this->BuildTime.Modified();
  }
  // if nothing changed but we are using display lists, draw it
  else
  {
    if (!this->ImmediateModeRendering &&
        !this->GetGlobalImmediateModeRendering())
    {
      // Time the actual drawing
      this->Timer->StartTimer();
      glCallList(this->ListId);
      this->Timer->StopTimer();
    }
  }

  // if we are in immediate mode rendering we always
  // want to draw the primitives here
  if (this->ImmediateModeRendering ||
      this->GetGlobalImmediateModeRendering())
  {
    // sets this->Colors as side effect
    this->MapScalars( act->GetProperty()->GetOpacity() );

    // Time the actual drawing
    this->Timer->StartTimer();
    this->Draw(ren,act);
    this->Timer->StopTimer();
  }

  this->TimeToDraw = (float)this->Timer->GetElapsedTime();

  // If the timer is not accurate enough, set it to a small
  // time so that it is not zero
  if ( this->TimeToDraw == 0.0 )
    this->TimeToDraw = 0.0001;

  vglUseProgramObjectARB( 0 );

  if( this->UsePointSprites ) //&& this->PrimitiveType == VISU_OpenGLPointSpriteMapper::PointSprite )
    this->CleanupPointSprites();
}

//-----------------------------------------------------------------------------
float VISU_OpenGLPointSpriteMapper::GetMaximumSupportedSize()
{
  float maximumSupportedSize = 512.0;
  //glGetFloatv( GL_POINT_SIZE_MAX_ARB, &maximumSupportedSize );

  return maximumSupportedSize;
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::InitPointSprites()
{
  glEnable( GL_POINT_SPRITE_ARB );
  glEnable( GL_VERTEX_PROGRAM_POINT_SIZE_ARB );

  glPushAttrib( GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_DEPTH_BUFFER_BIT | GL_ENABLE_BIT | GL_LIGHTING_BIT );

  this->RenderMode = this->PointSpriteOpacity < 1.0 ? VISU_OpenGLPointSpriteMapper::Accumulate : VISU_OpenGLPointSpriteMapper::Occlude;

  switch (this->RenderMode)
  {
    case VISU_OpenGLPointSpriteMapper::Accumulate:
    {
      glDepthFunc( GL_LESS );
      glEnable( GL_DEPTH_TEST );

      glEnable( GL_BLEND );
      glBlendFunc( GL_SRC_ALPHA, GL_ONE );

      glEnable( GL_ALPHA_TEST );
      glAlphaFunc( GL_GREATER, this->PointSpriteAlphaThreshold );
      break;
    }

    case VISU_OpenGLPointSpriteMapper::Occlude:
    {
      glDepthFunc( GL_LEQUAL );
      glEnable( GL_DEPTH_TEST );

      glEnable( GL_ALPHA_TEST );
      glAlphaFunc( GL_GREATER, this->PointSpriteAlphaThreshold );

      glDisable( GL_BLEND );
      break;
    }

    default:
    {
      break;
    }
  }
  // Disable Lighting/Shading.
  glDisable( GL_LIGHTING );

  // Disable material properties
  glDisable( GL_COLOR_MATERIAL );
}

//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::CleanupPointSprites()
{
  // Set GL params back to normal to stop other vtkMappers displaying wrongly
  glPopAttrib();

  glDisable( GL_VERTEX_PROGRAM_POINT_SIZE_ARB );
  glDisable( GL_POINT_SPRITE_ARB );
}


//-----------------------------------------------------------------------------
void
VISU_OpenGLPointSpriteMapper
::SetImageData( vtkImageData* theImageData )
{
  if(GetImageData() == theImageData)
    return;
  this->ImageData = theImageData;
  this->Modified();
}

vtkImageData*
VISU_OpenGLPointSpriteMapper
::GetImageData()
{
  return this->ImageData.GetPointer();
}


//-----------------------------------------------------------------------------
void VISU_OpenGLPointSpriteMapper::InitTextures()
{
  //cout << "VISU_OpenGLPointSpriteMapper::InitTextures " << this->GetImageData() << endl;
  if( !this->GetImageData() )
    return;

  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

  int* aSize = GetImageData()->GetDimensions();
  unsigned char* dataPtr = (unsigned char*)GetImageData()->GetScalarPointer();
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, aSize[0], aSize[1], 0,
                GL_RGBA, GL_UNSIGNED_BYTE, dataPtr );

  //glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_TEXTURE_2D );
  glTexEnvf( GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE );
  glBindTexture( GL_TEXTURE_2D, this->PointSpriteTexture );
}


//-----------------------------------------------------------------------------
int ComputeHue( int r, int g, int b )
{
  int h = 0;

  int max = r;
  int whatmax = 0;
  if( g > max ) {
    max = g;
    whatmax = 1;
  }
  if( b > max ) {
    max = b;
    whatmax = 2;
  }

  int min = r;
  if ( g < min ) min = g;
  if ( b < min ) min = b;
  int delta = max-min;

  if( delta == 0 )
    h = 0;
  else if( whatmax == 0 ) {
    if ( g >= b )
      h = (120*(g-b)+delta)/(2*delta);
    else
      h = (120*(g-b+delta)+delta)/(2*delta) + 300;
  }
  else if( whatmax == 1 ) {
    if ( b > r )
      h = 120 + (120*(b-r)+delta)/(2*delta);
    else
      h = 60 + (120*(b-r+delta)+delta)/(2*delta);
  }
  else {
    if ( r > g )
      h = 240 + (120*(r-g)+delta)/(2*delta);
    else
      h = 180 + (120*(r-g+delta)+delta)/(2*delta);
  }

  return h + 1;
}

//-----------------------------------------------------------------------------
struct TVertex
{
  GLfloat r, g, b, hue;
  GLfloat vx, vy, vz;
};


//-----------------------------------------------------------------------------
struct TColorFunctorBase
{
  virtual
  void
  get( TVertex& theVertex, vtkIdType thePointId, vtkIdType theCellId ) = 0;
};


//-----------------------------------------------------------------------------
struct TPropertyColor : TColorFunctorBase
{
  double myColor[3];
  double myHue;

  TPropertyColor( vtkProperty *theProperty )
  {
    theProperty->GetColor( myColor );
    int aRed = int( myColor[0] * 255 );
    int aGreen = int( myColor[1] * 255 );
    int aBlue = int( myColor[2] * 255 );

    myHue = ComputeHue( aRed, aGreen, aBlue );
  }

  virtual
  void
  get( TVertex& theVertex, vtkIdType thePointId, vtkIdType theCellId )
  {
    theVertex.r = myColor[0];
    theVertex.g = myColor[1];
    theVertex.b = myColor[2];

    theVertex.hue = myHue;
  }
};


//-----------------------------------------------------------------------------
struct TColors2Color : TColorFunctorBase
{
  vtkUnsignedCharArray *myColors;

  TColors2Color( vtkUnsignedCharArray *theColors ):
    myColors( theColors )
  {}

  virtual
  void
  get( TVertex& theVertex, vtkIdType thePointId, vtkIdType theCellId )
  {
    vtkIdType aTupleId = GetTupleId( thePointId, theCellId );
    unsigned char *aColor = myColors->GetPointer( aTupleId << 2 );

    theVertex.r = int( aColor[0] ) / 255.0;
    theVertex.g = int( aColor[1] ) / 255.0;
    theVertex.b = int( aColor[2] ) / 255.0;

    theVertex.hue = ComputeHue( aColor[0], aColor[1], aColor[2] );
  }  

  virtual
  vtkIdType
  GetTupleId( vtkIdType thePointId, vtkIdType theCellId ) = 0;
};


//-----------------------------------------------------------------------------
struct TPointColors2Color : TColors2Color
{
  TPointColors2Color( vtkUnsignedCharArray *theColors ):
    TColors2Color( theColors )
  {}

  virtual
  vtkIdType
  GetTupleId( vtkIdType thePointId, vtkIdType theCellId )
  {
    return thePointId;
  }
};


//-----------------------------------------------------------------------------
struct TCellColors2Color : TColors2Color
{
  TCellColors2Color( vtkUnsignedCharArray *theColors ):
    TColors2Color( theColors )
  {}

  virtual
  vtkIdType
  GetTupleId( vtkIdType thePointId, vtkIdType theCellId )
  {
    return theCellId;
  }
};


//-----------------------------------------------------------------------------
template < class TCoordinates >
void DrawPoints( TCoordinates *theStartPoints,
                 vtkCellArray *theCells,
                 TColorFunctorBase* theColorFunctor,
                 TVertex* theVertexArr,
                 vtkIdType &theCellId,
                 vtkIdType &theVertexId )
{
  vtkIdType *ptIds = theCells->GetPointer();
  vtkIdType *endPtIds = ptIds + theCells->GetNumberOfConnectivityEntries();

  while ( ptIds < endPtIds ) {
    vtkIdType nPts = *ptIds;
    ++ptIds;

    while ( nPts > 0 ) {
      TVertex& aVertex = theVertexArr[ theVertexId ];
      vtkIdType aPointId = *ptIds;

      TCoordinates *anOffsetPoints = theStartPoints + 3 * aPointId;
      aVertex.vx = anOffsetPoints[0];
      aVertex.vy = anOffsetPoints[1];
      aVertex.vz = anOffsetPoints[2];

      theColorFunctor->get( aVertex, aPointId, theCellId );

      ++theVertexId;
      ++ptIds; 
      --nPts; 
    }

    ++theCellId;
  }
}


//-----------------------------------------------------------------------------
template < class TCoordinates >
void DrawCellsPoints( vtkPolyData *theInput,
                      vtkPoints* thePoints,
                      TColorFunctorBase* theColorFunctor,
                      TVertex* theVertexArr )
{
  vtkIdType aCellId = 0, aVertexId = 0;

  TCoordinates *aStartPoints = (TCoordinates *) thePoints->GetVoidPointer(0);

  if ( vtkCellArray* aCellArray = theInput->GetVerts() )
    DrawPoints( aStartPoints, aCellArray, theColorFunctor, theVertexArr, aCellId, aVertexId );
  
  if ( vtkCellArray* aCellArray = theInput->GetLines() )
    DrawPoints( aStartPoints, aCellArray, theColorFunctor, theVertexArr, aCellId, aVertexId );
  
  if ( vtkCellArray* aCellArray = theInput->GetPolys() )
    DrawPoints( aStartPoints, aCellArray, theColorFunctor, theVertexArr, aCellId, aVertexId );
  
  if ( vtkCellArray* aCellArray = theInput->GetStrips() )
    DrawPoints( aStartPoints, aCellArray, theColorFunctor, theVertexArr, aCellId, aVertexId ); 
}


//-----------------------------------------------------------------------------
int VISU_OpenGLPointSpriteMapper::Draw(vtkRenderer *theRenderer, vtkActor *theActor)
{

  if( this->PrimitiveType == VISU_OpenGLPointSpriteMapper::GeomSphere )
    return MAPPER_SUPERCLASS::Draw( theRenderer, theActor );

  vtkUnsignedCharArray *colors = NULL;
  vtkPolyData          *input  = this->GetInput();
  vtkPoints            *points;
  int noAbort = 1;
  int cellScalars = 0;

  // if the primitives are invisable then get out of here
  if (this->PointSpriteOpacity <= 0.0)
  {
    return noAbort;
  }

  // and draw the display list
  points = input->GetPoints();

  // are they cell or point scalars
  if ( this->Colors )
  {
    colors = this->Colors;
    if ( (this->ScalarMode == VTK_SCALAR_MODE_USE_CELL_DATA ||
          this->ScalarMode == VTK_SCALAR_MODE_USE_CELL_FIELD_DATA ||
          !input->GetPointData()->GetScalars() )
         && this->ScalarMode != VTK_SCALAR_MODE_USE_POINT_FIELD_DATA)
    {
      cellScalars = 1;
    }
  }

  {
    vtkIdType aTotalConnectivitySize = 0;

    if ( vtkCellArray* aCellArray = input->GetVerts() )
      aTotalConnectivitySize += aCellArray->GetNumberOfConnectivityEntries() - aCellArray->GetNumberOfCells();

    if ( vtkCellArray* aCellArray = input->GetLines() )
      aTotalConnectivitySize += aCellArray->GetNumberOfConnectivityEntries() - aCellArray->GetNumberOfCells();

    if ( vtkCellArray* aCellArray = input->GetPolys() )
      aTotalConnectivitySize += aCellArray->GetNumberOfConnectivityEntries() - aCellArray->GetNumberOfCells();

    if ( vtkCellArray* aCellArray = input->GetStrips() )
      aTotalConnectivitySize += aCellArray->GetNumberOfConnectivityEntries() - aCellArray->GetNumberOfCells();

    if ( aTotalConnectivitySize > 0 ) {
      TVertex* aVertexArr = new TVertex[ aTotalConnectivitySize ];

      double aPropertyColor[3];
      theActor->GetProperty()->GetColor( aPropertyColor );

      glPointSize( this->DefaultPointSize );

      {
        TColorFunctorBase* aColorFunctor = NULL;
        if( colors && this->PointSpriteMode != 1 ) {
          if ( cellScalars )
            aColorFunctor = new TCellColors2Color( colors );
          else
            aColorFunctor = new TPointColors2Color( colors );
        } else {
          aColorFunctor = new TPropertyColor( theActor->GetProperty() );
        }
        if ( points->GetDataType() == VTK_FLOAT )
          ::DrawCellsPoints< float >( input, points, aColorFunctor, aVertexArr );
        else
          ::DrawCellsPoints< double >( input, points, aColorFunctor, aVertexArr );

        delete aColorFunctor;
      }

      if( this->ExtensionsInitialized ) {
        GLuint aBufferObjectID = 0;
        vglGenBuffersARB( 1, &aBufferObjectID );
        vglBindBufferARB( GL_ARRAY_BUFFER_ARB, aBufferObjectID );
        
        int anArrayObjectSize = sizeof( TVertex ) * aTotalConnectivitySize;
        vglBufferDataARB( GL_ARRAY_BUFFER_ARB, anArrayObjectSize, aVertexArr, GL_STATIC_DRAW_ARB );
        
        delete [] aVertexArr;
        
        vglBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
        vglBindBufferARB( GL_ARRAY_BUFFER_ARB, aBufferObjectID );
        
        glColorPointer( 4, GL_FLOAT, sizeof(TVertex), (void*)0 );
        glVertexPointer( 3, GL_FLOAT, sizeof(TVertex), (void*)(4*sizeof(GLfloat)) );
        
        glEnableClientState( GL_VERTEX_ARRAY );
        glEnableClientState( GL_COLOR_ARRAY );
        
        glDrawArrays( GL_POINTS, 0, aTotalConnectivitySize );
        
        glDisableClientState( GL_COLOR_ARRAY );
        glDisableClientState( GL_VERTEX_ARRAY );
        
        vglDeleteBuffersARB( 1, &aBufferObjectID );
      } else { // there are not extensions
        glColorPointer( 4, GL_FLOAT, sizeof(TVertex), aVertexArr );
        glVertexPointer( 3, GL_FLOAT, sizeof(TVertex), 
                         (void*)((GLfloat*)((void*)(aVertexArr)) + 4));

        glEnableClientState( GL_VERTEX_ARRAY );
        glEnableClientState( GL_COLOR_ARRAY );
        
        glDrawArrays( GL_POINTS, 0, aTotalConnectivitySize );
        
        glDisableClientState( GL_COLOR_ARRAY );
        glDisableClientState( GL_VERTEX_ARRAY );

        delete [] aVertexArr;
      }
    }

    input->GetVerts()->GetNumberOfCells() + 
    input->GetLines()->GetNumberOfCells() + 
    input->GetPolys()->GetNumberOfCells() + 
    input->GetStrips()->GetNumberOfCells();
  }


  this->UpdateProgress(1.0);
  return noAbort;
}
//-----------------------------------------------------------------------------
// Release the graphics resources used by this mapper.  In this case, release
// the display list if any.
void VISU_OpenGLPointSpriteMapper::ReleaseGraphicsResources(vtkWindow *win)
{
  this->Superclass::ReleaseGraphicsResources(win);

  if (this->ListId && win)
    {
    win->MakeCurrent();
    glDeleteLists(this->ListId,1);
    this->ListId = 0;
    }
  this->LastWindow = NULL;
}

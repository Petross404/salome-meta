// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_ScalarBarActor.hxx"

#include <vtkPolyDataMapper2D.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkScalarsToColors.h>
#include <vtkTextMapper.h>
#include <vtkTextProperty.h>
#include <vtkViewport.h>
#include <vtkWindow.h>
#include <vtkLogLookupTable.h>
#include <vtkProperty2D.h> // RKV
#include <vtkAxisActor2D.h> // RKV

using namespace std;

vtkCxxSetObjectMacro(VISU_ScalarBarActor,LookupTable,VISU_LookupTable);
vtkCxxSetObjectMacro(VISU_ScalarBarActor,LabelTextProperty,vtkTextProperty);
vtkCxxSetObjectMacro(VISU_ScalarBarActor,TitleTextProperty,vtkTextProperty);

//------------------------------------------------------------------------------
VISU_ScalarBarActor* VISU_ScalarBarActor::New(){
  vtkObject* ret = vtkObjectFactory::CreateInstance("VISU_ScalarBarActor");
  if(ret)
    return (VISU_ScalarBarActor*)ret;
  return new VISU_ScalarBarActor;
}

VISU_ScalarBarActor::VISU_ScalarBarActor()
{
  this->LookupTable = NULL;
  this->Position2Coordinate->SetValue(0.17, 0.8);
  
  this->PositionCoordinate->SetCoordinateSystemToNormalizedViewport();
  this->PositionCoordinate->SetValue(0.82,0.1);
  
  this->MaximumNumberOfColors = 64;
  this->NumberOfLabels = 5;
  this->NumberOfLabelsBuilt = 0;
  this->Orientation = VTK_ORIENT_VERTICAL;
  this->Title = NULL;

  this->LabelTextProperty = vtkTextProperty::New();
  this->LabelTextProperty->SetFontSize(12);
  this->LabelTextProperty->SetBold(1);
  this->LabelTextProperty->SetItalic(1);
  this->LabelTextProperty->SetShadow(1);
  this->LabelTextProperty->SetFontFamilyToArial();

  this->TitleTextProperty = vtkTextProperty::New();
  this->TitleTextProperty->ShallowCopy(this->LabelTextProperty);

  this->LabelFormat = new char[8]; 
  sprintf(this->LabelFormat,"%s","%-#6.3g");

  this->TitleMapper = vtkTextMapper::New();
  this->TitleActor = vtkActor2D::New();
  this->TitleActor->SetMapper(this->TitleMapper);
  this->TitleActor->GetPositionCoordinate()->
    SetReferenceCoordinate(this->PositionCoordinate);
  
  this->TextMappers = NULL;
  this->TextActors = NULL;

  this->ScalarBar = vtkPolyData::New();
  this->ScalarBarMapper = vtkPolyDataMapper2D::New();
  this->ScalarBarMapper->SetInputData(this->ScalarBar);
  this->ScalarBarActor = vtkActor2D::New();
  this->ScalarBarActor->SetMapper(this->ScalarBarMapper);
  this->ScalarBarActor->GetPositionCoordinate()->
    SetReferenceCoordinate(this->PositionCoordinate);
  this->LastOrigin[0] = 0;
  this->LastOrigin[1] = 0;
  this->LastSize[0] = 0;
  this->LastSize[1] = 0;

  this->TitleRatioSize = 0;
  this->LabelRatioWidth = 0;
  this->BarRatioWidth = 0;
  this->BarRatioHeight = 0;
  
  // RKV : Begin
  this->Distribution = vtkDoubleArray::New();
  this->DistributionObj = vtkDataObject::New();
  this->DistributionActor = VISU_XYPlotActor::New();
  this->DistributionActor->SetTitle("");
  this->DistributionActor->SetXTitle("");
  this->DistributionActor->SetYTitle("");
  this->DistributionActor->GetXAxisActor2D()->LabelVisibilityOff();
  this->DistributionActor->GetXAxisActor2D()->TitleVisibilityOff();
  this->DistributionActor->GetXAxisActor2D()->TickVisibilityOff();
//  this->DistributionActor->GetXAxisActor2D()->SetFontFactor(0.);
  this->DistributionActor->SetNumberOfYLabels(1);
//  this->DistributionActor->SetNumberOfXLabels(2);
  this->DistributionActor->GetXAxisActor2D()->AdjustLabelsOff();
  this->DistributionActor->GetYAxisActor2D()->AdjustLabelsOff();
  this->DistributionActor->LegendOff();
  this->DistributionActor->SetLabelFormat("%4.3f");
  this->DistributionActor->SetXValuesToIndex();
//  this->DistributionActor->GetPositionCoordinate()->SetValue(0.0, 0.67, 0);
//  this->DistributionActor->GetPosition2Coordinate()->SetValue(1.0, 0.33, 0); // #relative to Position
  this->DistributionActor->GetPositionCoordinate()->
    SetReferenceCoordinate(this->PositionCoordinate);
  this->DistributionVisibilityOff(); // Don't show the distribution curve by default
  
  // RKV : End
}

void VISU_ScalarBarActor::ReleaseGraphicsResources(vtkWindow *win)
{
  this->TitleActor->ReleaseGraphicsResources(win);
  if (this->TextMappers != NULL )
    {
    for (int i=0; i < this->NumberOfLabelsBuilt; i++)
      {
      this->TextActors[i]->ReleaseGraphicsResources(win);
      }
    }
  this->ScalarBarActor->ReleaseGraphicsResources(win);
  this->DistributionActor->ReleaseGraphicsResources(win); // RKV
}

VISU_ScalarBarActor::~VISU_ScalarBarActor()
{
  if (this->LabelFormat) 
    {
    delete [] this->LabelFormat;
    this->LabelFormat = NULL;
    }

  this->TitleMapper->Delete();
  this->TitleActor->Delete();

  if (this->TextMappers != NULL )
    {
    for (int i=0; i < this->NumberOfLabelsBuilt; i++)
      {
      this->TextMappers[i]->Delete();
      this->TextActors[i]->Delete();
      }
    delete [] this->TextMappers;
    delete [] this->TextActors;
    }

  // RKV : Begin
  this->DistributionActor->Delete();
  this->DistributionObj->Delete();
  this->SetDistribution(NULL);
  // RKV : End

  this->ScalarBar->Delete();
  this->ScalarBarMapper->Delete();
  this->ScalarBarActor->Delete();
  
  if (this->Title)
    {
    delete [] this->Title;
    this->Title = NULL;
    }
  
  this->SetLookupTable(NULL);
  this->SetLabelTextProperty(NULL);
  this->SetTitleTextProperty(NULL);
}

int VISU_ScalarBarActor::RenderOverlay(vtkViewport *viewport)
{
  int renderedSomething = 0;
  int i;
  
  // Everything is built, just have to render
  if (this->Title != NULL)
    {
    renderedSomething += this->TitleActor->RenderOverlay(viewport);
    }
  this->ScalarBarActor->RenderOverlay(viewport);
  // RKV : Begin
  if (this->DistributionVisibility)
    this->DistributionActor->RenderOverlay(viewport);
  // RKV : End
  if( this->TextActors == NULL)
    {
     vtkWarningMacro(<<"Need a mapper to render a scalar bar");
     return renderedSomething;
    }
  
  for (i=0; i<this->NumberOfLabels; i++)
    {
    renderedSomething += this->TextActors[i]->RenderOverlay(viewport);
    }

  renderedSomething = (renderedSomething > 0)?(1):(0);

  return renderedSomething;
}

int VISU_ScalarBarActor::RenderOpaqueGeometry(vtkViewport *viewport)
{
  int renderedSomething = 0;
  int i;
  int size[2];
  
  if (!this->LookupTable)
    {
    vtkWarningMacro(<<"Need a mapper to render a scalar bar");
    return 0;
    }

  if (!this->TitleTextProperty)
    {
    vtkErrorMacro(<<"Need title text property to render a scalar bar");
    return 0;
    }

  if (!this->LabelTextProperty)
    {
    vtkErrorMacro(<<"Need label text property to render a scalar bar");
    return 0;
    }

  // Check to see whether we have to rebuild everything
  int positionsHaveChanged = 0;
  if (viewport->GetMTime() > this->BuildTime || 
      (viewport->GetVTKWindow() && 
       viewport->GetVTKWindow()->GetMTime() > this->BuildTime))
    {
    // if the viewport has changed we may - or may not need
    // to rebuild, it depends on if the projected coords chage
    int *barOrigin;
    barOrigin = this->PositionCoordinate->GetComputedViewportValue(viewport);
    size[0] = 
      this->Position2Coordinate->GetComputedViewportValue(viewport)[0] -
      barOrigin[0];
    size[1] = 
      this->Position2Coordinate->GetComputedViewportValue(viewport)[1] -
      barOrigin[1];
    if (this->LastSize[0] != size[0] || 
        this->LastSize[1] != size[1] ||
        this->LastOrigin[0] != barOrigin[0] || 
        this->LastOrigin[1] != barOrigin[1])
      {
      positionsHaveChanged = 1;
      }
    }
  
  // Check to see whether we have to rebuild everything
  // RKV : Begin
  if (positionsHaveChanged ||
      this->GetMTime() > this->BuildTime || 
      this->LookupTable->GetMTime() > this->BuildTime ||
      this->LabelTextProperty->GetMTime() > this->BuildTime ||
      this->TitleTextProperty->GetMTime() > this->BuildTime ||
      this->Distribution->GetMTime() > this->BuildTime)
  // RKV : End
/* RKV  if (positionsHaveChanged ||
      this->GetMTime() > this->BuildTime || 
      this->LookupTable->GetMTime() > this->BuildTime ||
      this->LabelTextProperty->GetMTime() > this->BuildTime ||
      this->TitleTextProperty->GetMTime() > this->BuildTime)*/
    {

    // Delete previously constructed objects
    //
    if (this->TextMappers != NULL )
      {
      for (i=0; i < this->NumberOfLabelsBuilt; i++)
        {
        this->TextMappers[i]->Delete();
        this->TextActors[i]->Delete();
        }
      delete [] this->TextMappers;
      delete [] this->TextActors;
      }

    // Build scalar bar object; determine its type
    //
    VISU_LookupTable *lut = this->LookupTable; //SALOME specific
    int isLogTable = lut->GetScale() == VTK_SCALE_LOG10;
    
    // we hard code how many steps to display
    int numColors = this->MaximumNumberOfColors;
    double *range = lut->GetRange();

    int numPts = 2*(numColors + 1);
    vtkPoints *pts = vtkPoints::New();
    pts->SetNumberOfPoints(numPts);
    vtkCellArray *polys = vtkCellArray::New();
    polys->Allocate(polys->EstimateSize(numColors,4));
    vtkUnsignedCharArray *colors = vtkUnsignedCharArray::New();
    colors->SetNumberOfComponents(3);
    colors->SetNumberOfTuples(numColors);

// RKV : Begin
    // If the distribution is changed then recalculate the total
    if (this->Distribution->GetMTime() > this->BuildTime) {
          int aNbVals = this->Distribution->GetNumberOfTuples();
      double range[2];
      this->Distribution->GetRange(range);
      this->DistributionActor->SetYRange(0, range[1]);
/*        int total = 0;
          for(vtkIdType aValId = 0; aValId < aNbVals; aValId++){
                total += this->Distribution->GetValue(aValId);
          }
          this->DistributionActor->SetYRange(0, total);
*/
      }
  
    this->DistributionActor->SetProperty(this->GetProperty());
    this->DistributionActor->GetProperty()->SetColor(1, 1, 1);
    this->DistributionActor->GetProperty()->SetLineWidth(2);
    this->DistributionActor->GetProperty()->SetDisplayLocationToForeground();
    vtkTextProperty* tprop;
    tprop = this->DistributionActor->GetTitleTextProperty();
    tprop->SetColor(this->DistributionActor->GetProperty()->GetColor());
//  this->DistributionActor->SetAxisTitleTextProperty(tprop);
//  this->DistributionActor->SetAxisLabelTextProperty(tprop);
    
// RKV : End
    this->ScalarBarActor->SetProperty(this->GetProperty());
    this->ScalarBar->Initialize();
    this->ScalarBar->SetPoints(pts);
    this->ScalarBar->SetPolys(polys);
    this->ScalarBar->GetCellData()->SetScalars(colors);
    pts->Delete(); polys->Delete(); colors->Delete();

    // get the viewport size in display coordinates
    int *barOrigin, barWidth, barHeight;
    barOrigin = this->PositionCoordinate->GetComputedViewportValue(viewport);
    size[0] = 
      this->Position2Coordinate->GetComputedViewportValue(viewport)[0] -
      barOrigin[0];
    size[1] = 
      this->Position2Coordinate->GetComputedViewportValue(viewport)[1] -
      barOrigin[1];
    this->LastOrigin[0] = barOrigin[0];
    this->LastOrigin[1] = barOrigin[1];
    this->LastSize[0] = size[0];
    this->LastSize[1] = size[1];
    
    // Update all the composing objects
    this->TitleActor->SetProperty(this->GetProperty());
    this->TitleMapper->SetInput(this->Title);
    if (this->TitleTextProperty->GetMTime() > this->BuildTime)
      {
      // Shallow copy here so that the size of the title prop is not affected
      // by the automatic adjustment of its text mapper's size (i.e. its
      // mapper's text property is identical except for the font size
      // which will be modified later). This allows text actors to
      // share the same text property, and in that case specifically allows
      // the title and label text prop to be the same.
      this->TitleMapper->GetTextProperty()->ShallowCopy(this->TitleTextProperty);
      this->TitleMapper->GetTextProperty()->SetJustificationToCentered();
      }
    
    // find the best size for the title font
    int titleSize[2];
    this->SizeTitle(titleSize, size, viewport);
    
    // find the best size for the ticks
    int labelSize[2];
    this->AllocateAndSizeLabels(labelSize, size, viewport, range);
    this->NumberOfLabelsBuilt = this->NumberOfLabels;
    
    this->SizeBar(barWidth, barHeight, size, viewport, range);

    // generate points
    double x[3]; x[2] = 0.0;
    double delta, val;
    if ( this->Orientation == VTK_ORIENT_VERTICAL )
      {
      delta=(double)barHeight/numColors;
      for (i=0; i<numPts/2; i++)
        {
        x[0] = 0;
        x[1] = i*delta;
        pts->SetPoint(2*i,x);
        x[0] = barWidth;
        pts->SetPoint(2*i+1,x);
        }
      }
    else
      {
      delta=(double)barWidth/numColors;
      for (i=0; i<numPts/2; i++)
        {
        x[0] = i*delta;
        x[1] = barHeight;
        pts->SetPoint(2*i,x);
        x[1] = 0;
        pts->SetPoint(2*i+1,x);
        }
      }

    //polygons & cell colors
    unsigned char *rgba, *rgb;
    vtkIdType ptIds[4];
    for (i=0; i<numColors; i++)
      {
      ptIds[0] = 2*i;
      ptIds[1] = ptIds[0] + 1;
      ptIds[2] = ptIds[1] + 2;
      ptIds[3] = ptIds[0] + 2;
      polys->InsertNextCell(4,ptIds);

      if ( isLogTable ){ //SALOME specific
        double rgbval = log10(range[0]) + 
          i*(log10(range[1])-log10(range[0]))/(numColors -1);
        rgba = lut->MapValue(rgbval);
      }else{
        rgba = lut->MapValue(range[0] + (range[1] - range[0])*
                             ((double)i /(numColors-1.0)));
      }

      rgb = colors->GetPointer(3*i); //write into array directly
      rgb[0] = rgba[0];
      rgb[1] = rgba[1];
      rgb[2] = rgba[2];
    }

    // Now position everything properly
    //
    if (this->Orientation == VTK_ORIENT_VERTICAL)
      {
      int sizeTextData[2];
      
      // center the title
      this->TitleActor->SetPosition(size[0]/2, 0.9*size[1]);
      
      for (i=0; i < this->NumberOfLabels; i++)
        {
        val = (double)i/(this->NumberOfLabels-1) *barHeight;
        this->TextMappers[i]->GetSize(viewport,sizeTextData);
        this->TextMappers[i]->GetTextProperty()->SetJustificationToLeft();
        this->TextActors[i]->SetPosition(barWidth+3,
                                         val - sizeTextData[1]/2);
        }
        
      }
    else
      {
      this->TitleActor->SetPosition(size[0]/2, 
                                    barHeight + labelSize[1] + 0.1*size[1]);
      for (i=0; i < this->NumberOfLabels; i++)
        {
        this->TextMappers[i]->GetTextProperty()->SetJustificationToCentered();
        val = (double)i/(this->NumberOfLabels-1) * barWidth;
        this->TextActors[i]->SetPosition(val, barHeight + 0.05*size[1]);
        }
        
      }
      
    // Compute the position of the distribution curve
    this->PlaceDistribution(viewport, barWidth, barHeight); // RKV

    this->BuildTime.Modified();
    }

  // Everything is built, just have to render
  if (this->Title != NULL)
    {
    renderedSomething += this->TitleActor->RenderOpaqueGeometry(viewport);
    }
  this->ScalarBarActor->RenderOpaqueGeometry(viewport);
  // RKV : Begin
  if (this->DistributionVisibility)
    this->DistributionActor->RenderOpaqueGeometry(viewport);
  // RKV : End
  for (i=0; i<this->NumberOfLabels; i++)
    {
    renderedSomething += this->TextActors[i]->RenderOpaqueGeometry(viewport);
    }

  renderedSomething = (renderedSomething > 0)?(1):(0);

  return renderedSomething;
}

void VISU_ScalarBarActor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  if ( this->LookupTable )
    {
    os << indent << "Lookup Table:\n";
    this->LookupTable->PrintSelf(os,indent.GetNextIndent());
    }
  else
    {
    os << indent << "Lookup Table: (none)\n";
    }

  if (this->TitleTextProperty)
    {
    os << indent << "Title Text Property:\n";
    this->TitleTextProperty->PrintSelf(os,indent.GetNextIndent());
    }
  else
    {
    os << indent << "Title Text Property: (none)\n";
    }

  if (this->LabelTextProperty)
    {
    os << indent << "Label Text Property:\n";
    this->LabelTextProperty->PrintSelf(os,indent.GetNextIndent());
    }
  else
    {
    os << indent << "Label Text Property: (none)\n";
    }

  os << indent << "Title: " << (this->Title ? this->Title : "(none)") << "\n";
  os << indent << "Maximum Number Of Colors: " 
     << this->MaximumNumberOfColors << "\n";
  os << indent << "Number Of Labels: " << this->NumberOfLabels << "\n";
  os << indent << "Number Of Labels Built: " << this->NumberOfLabelsBuilt << "\n";

  os << indent << "Orientation: ";
  if ( this->Orientation == VTK_ORIENT_HORIZONTAL )
    {
    os << "Horizontal\n";
    }
  else
    {
    os << "Vertical\n";
    }

  os << indent << "Label Format: " << this->LabelFormat << "\n";
}

void VISU_ScalarBarActor::ShallowCopy(vtkProp *prop)
{
  VISU_ScalarBarActor *a = VISU_ScalarBarActor::SafeDownCast(prop);
  if ( a != NULL )
    {
    this->SetPosition2(a->GetPosition2());
    this->SetLookupTable(a->GetLookupTable());
    this->SetMaximumNumberOfColors(a->GetMaximumNumberOfColors());
    this->SetOrientation(a->GetOrientation());
    this->SetLabelTextProperty(a->GetLabelTextProperty());
    this->SetTitleTextProperty(a->GetTitleTextProperty());
    this->SetLabelFormat(a->GetLabelFormat());
    this->SetTitle(a->GetTitle());
    this->GetPositionCoordinate()->SetCoordinateSystem(
      a->GetPositionCoordinate()->GetCoordinateSystem());    
    this->GetPositionCoordinate()->SetValue(
      a->GetPositionCoordinate()->GetValue());
    this->GetPosition2Coordinate()->SetCoordinateSystem(
      a->GetPosition2Coordinate()->GetCoordinateSystem());    
    this->GetPosition2Coordinate()->SetValue(
      a->GetPosition2Coordinate()->GetValue());
    }

  // Now do superclass
  this->vtkActor2D::ShallowCopy(prop);
}

void VISU_ScalarBarActor::AllocateAndSizeLabels(int *labelSize, int *size,
                                              vtkViewport *viewport,
                                              double *range)
{
  labelSize[0] = labelSize[1] = 0;

  this->TextMappers = new vtkTextMapper * [this->NumberOfLabels];
  this->TextActors = new vtkActor2D * [this->NumberOfLabels];

  char string[512];

  double val;
  int i;
  
  // TODO: this should be optimized, maybe by keeping a list of
  // allocated mappers, in order to avoid creation/destruction of
  // their underlying text properties (i.e. each time a mapper is
  // created, text properties are created and shallow-assigned a font size
  // which value might be "far" from the target font size).

  VISU_LookupTable *lut = this->LookupTable; //SALOME specific
  int isLogTable = lut->GetScale() == VTK_SCALE_LOG10;

  for (i=0; i < this->NumberOfLabels; i++)
    {
    this->TextMappers[i] = vtkTextMapper::New();

    if(isLogTable && 0 < i && i < this->NumberOfLabels - 1){ // SALOME specific
      double lval = log10(range[0]) + (double)i/(this->NumberOfLabels-1) *
        (log10(range[1])-log10(range[0]));
      val = pow((double)10,(double)lval);
    }else{
      val = range[0] + (double)i/(this->NumberOfLabels-1) * (range[1]-range[0]);
    }
    sprintf(string, this->LabelFormat, val);
    this->TextMappers[i]->SetInput(string);

    // Shallow copy here so that the size of the label prop is not affected
    // by the automatic adjustment of its text mapper's size (i.e. its
    // mapper's text property is identical except for the font size
    // which will be modified later). This allows text actors to
    // share the same text property, and in that case specifically allows
    // the title and label text prop to be the same.
    this->TextMappers[i]->GetTextProperty()->ShallowCopy(
      this->LabelTextProperty);

    this->TextActors[i] = vtkActor2D::New();
    this->TextActors[i]->SetMapper(this->TextMappers[i]);
    this->TextActors[i]->SetProperty(this->GetProperty());
    this->TextActors[i]->GetPositionCoordinate()->
      SetReferenceCoordinate(this->PositionCoordinate);
    }

  if (this->NumberOfLabels)
    {
    int targetWidth, targetHeight;

    if(LabelRatioWidth == 0)
      if ( this->Orientation == VTK_ORIENT_VERTICAL )
          targetWidth = (int)(0.6*size[0]);
        else
          targetWidth = (int)(size[0]*0.8/this->NumberOfLabels);
    else
      targetWidth = (int)(0.01*LabelRatioWidth*size[0]);

    if ( this->Orientation == VTK_ORIENT_VERTICAL )
      targetHeight = (int)(0.86*size[1]/this->NumberOfLabels);
    else
      targetHeight = (int)(0.25*size[1]);

    vtkTextMapper::SetMultipleConstrainedFontSize(viewport, 
                                                  targetWidth, 
                                                  targetHeight,
                                                  this->TextMappers,
                                                  this->NumberOfLabels,
                                                  labelSize);
    }
}

void VISU_ScalarBarActor::SizeTitle(int *titleSize, int *size, 
                                  vtkViewport *viewport)
{
  titleSize[0] = titleSize[1] = 0;

  if (this->Title == NULL || !strlen(this->Title))
    {
    return;
    }

  int targetWidth, targetHeight;
  
  if(TitleRatioSize == 0)
    targetWidth = size[0];
  else
    targetWidth = (int)(0.01*TitleRatioSize*size[0]);

  if ( this->Orientation == VTK_ORIENT_VERTICAL )
    targetHeight = (int)(0.1*size[1]);
  else
    targetHeight = (int)(0.25*size[1]);

  this->TitleMapper->SetConstrainedFontSize(
    viewport, targetWidth, targetHeight);

  this->TitleMapper->GetSize(viewport, titleSize);
}

// RKV : Begin
void VISU_ScalarBarActor::SetDistributionVisibility(int v) 
{
  this->DistributionVisibility = v;
  if (v) {
        this->DistributionActor->VisibilityOn();
  } else {
        this->DistributionActor->VisibilityOff();
  }
}

void VISU_ScalarBarActor::SetDistribution(vtkDoubleArray *distr) 
{
  this->Distribution = distr;
  if (distr == NULL) return;
  
  this->DistributionObj->Initialize();
  this->DistributionObj->GetFieldData()->AddArray(this->Distribution);
  this->DistributionActor->AddDataObjectInput(this->DistributionObj);
  // Set ranges of axes for the distribution curve
  this->DistributionActor->SetXRange(0, this->Distribution->GetNumberOfTuples()-1);
  double range[2];
  this->Distribution->GetRange(range);
  int aNbVals = this->Distribution->GetNumberOfTuples();
//  int total = 0;
  if (this->GetDebug()) {
  for(vtkIdType aValId = 0; aValId < aNbVals; aValId++){
//      if (this->GetDebug()) {
                if (this->Distribution->GetValue(aValId) > 0)
              vtkDebugMacro(<< "D(" << aValId << ") = " << this->Distribution->GetValue(aValId));
//      }
//      total += this->Distribution->GetValue(aValId);
  }
  }
//  this->DistributionActor->SetYRange(0, total);
  this->DistributionActor->SetYRange(0, range[1]);
  vtkDebugMacro(<< "max X = " << this->Distribution->GetNumberOfTuples());
  vtkDebugMacro(<< "Y = (" << range[0] << ", " << range[1] << ")");
//  vtkDebugMacro(<< "total = " << total);
}

void VISU_ScalarBarActor::DebugOn() {
        this->DistributionActor->DebugOn();
        Superclass::DebugOn();
} 

void VISU_ScalarBarActor::DebugOff() {
        this->DistributionActor->DebugOff();
        Superclass::DebugOff();
} 
// RKV : End

void VISU_ScalarBarActor::SetRatios(int titleRatioSize, int labelRatioWidth, 
                                    int barRatioWidth, int barRatioHeight)
{
  TitleRatioSize=titleRatioSize;
  if(TitleRatioSize>100)
    TitleRatioSize=100;
  else if(TitleRatioSize<0)
    TitleRatioSize=0;

  LabelRatioWidth=labelRatioWidth;
  if(LabelRatioWidth>100)
    LabelRatioWidth=100;
  else if(LabelRatioWidth<0)
    LabelRatioWidth=0;

  BarRatioWidth=barRatioWidth;
  if(BarRatioWidth>100)
    BarRatioWidth=100;
  else if(BarRatioWidth<0)
    BarRatioWidth=0;

  BarRatioHeight=barRatioHeight;
  if(BarRatioHeight>100)
    BarRatioHeight=100;
  else if(BarRatioHeight<0)
    BarRatioHeight=0;
}

void VISU_ScalarBarActor::GetRatios(int& titleRatioSize, int& labelRatioWidth, 
                                    int& barRatioWidth, int& barRatioHeight)
{
  titleRatioSize=TitleRatioSize;
  labelRatioWidth=LabelRatioWidth;
  barRatioWidth=BarRatioWidth;
  barRatioHeight=BarRatioHeight;
}

void VISU_ScalarBarActor::SizeBar(int& barSizeWidth, int& barSizeHeight, int *size,
                                  vtkViewport *viewport, double *range)
{
  if(BarRatioWidth == 0)
    if ( this->Orientation == VTK_ORIENT_VERTICAL )
      {
        int labelSize[2];
        this->AllocateAndSizeLabels(labelSize, size, viewport,range);
        barSizeWidth = size[0] - 4 - labelSize[0];
      } else
        barSizeWidth = size[0];
  else
    barSizeWidth = (int)(0.01*BarRatioWidth*size[0]);

  if(BarRatioHeight == 0)
    if ( this->Orientation == VTK_ORIENT_VERTICAL )
      barSizeHeight = (int)(0.86*size[1]);
    else
      barSizeHeight = (int)(0.4*size[1]);
  else
    barSizeHeight = (int)(0.01*BarRatioHeight*size[1]);
}
// RKV : Begin
//------------------------------------------------------------------------------
/** Place the distribution plot actor in the viewport according to the 
 * scalar bar location and orientation */
void VISU_ScalarBarActor::PlaceDistribution(vtkViewport *viewport, const int barWidth, const int barHeight) {
    vtkDebugMacro(<< "barOrigin[0]=" << this->LastOrigin[0] << "; barOrigin[1]=" << this->LastOrigin[1]);
    // Detect the side of the viewport where the curve should be placed by the bar origin.
    double u = (double)(this->LastOrigin[0]), v = (double)(this->LastOrigin[1]), z=0;
    viewport->ViewportToNormalizedViewport(u, v);
    
    if ( this->Orientation == VTK_ORIENT_VERTICAL ) {
      // Position the distribution curve vertically
      if (u > 0.5) {
        // X - UP, Y - TO THE LEFT
        this->DistributionActor->SetPlotLocation(VISU_XYPLOT_RIGHT);
        // Curve to be placed on the left side of the bar
        vtkDebugMacro(<< "Curve to be placed on the left side of the bar");
        // relative to the bar origin
        u = 0;
        v = 0;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro(<< "u=" << u << "; v=" << v);
        this->DistributionActor->GetPositionCoordinate()->SetValue(u, v, 0);
        // relative to Position
        u = - barWidth;
        v = barHeight;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro("u2=" << u << "; v2=" << v);
        this->DistributionActor->GetPosition2Coordinate()->SetValue(u, v, 0);
      } else {
        // X - UP, Y - TO THE RIGHT
        this->DistributionActor->SetPlotLocation(VISU_XYPLOT_LEFT);
        // Curve to be placed on the right side of the bar
        vtkDebugMacro(<< "Curve to be placed on the right side of the bar");
        // relative to the bar origin
        u = barWidth;
        v = 0;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro(<< "u=" << u << "; v=" << v);
        this->DistributionActor->GetPositionCoordinate()->SetValue(u, v, 0);
        // relative to Position
        u = barWidth;
        v = barHeight;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro("u2=" << u << "; v2=" << v);
        this->DistributionActor->GetPosition2Coordinate()->SetValue(u, v, 0);
      }
    } else {
      // Position the distribution curve horizontally
      if (v > 0.5) {
        // X - TO THE LEFT, Y - DOWN
        this->DistributionActor->SetPlotLocation(VISU_XYPLOT_TOP);
        // Curve to be placed below the bar
        vtkDebugMacro(<< "Curve to be placed below the bar");
        // relative to the bar origin
        u = 0;
        v = 0;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro(<< "u=" << u << "; v=" << v);
        this->DistributionActor->GetPositionCoordinate()->SetValue(u, v, 0);
        // relative to Position
        u = barWidth;
        v = - barHeight;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro("u2=" << u << "; v2=" << v);
        this->DistributionActor->GetPosition2Coordinate()->SetValue(u, v, 0);
      } else {
        // X - TO THE RIGHT, Y - UP
        this->DistributionActor->SetPlotLocation(VISU_XYPLOT_BOTTOM);
        // Curve to be placed on the top of the bar
        vtkDebugMacro(<< "Curve to be placed on the top of the bar");
        // relative to the bar origin
        u = 0;
        v = barHeight;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro(<< "u=" << u << "; v=" << v);
        this->DistributionActor->GetPositionCoordinate()->SetValue(u, v, 0);
        // relative to Position
        u = barWidth;
        v = barHeight;
        viewport->ViewportToNormalizedViewport(u, v);
        vtkDebugMacro("u2=" << u << "; v2=" << v);
        this->DistributionActor->GetPosition2Coordinate()->SetValue(u, v, 0);
      }
    }
}
// RKV : End


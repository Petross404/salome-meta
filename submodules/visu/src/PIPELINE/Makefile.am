# Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com

#  VISU OBJECT : interactive object for VISU entities implementation
#  File   : Makefile.in
#  Module : VISU

include $(top_srcdir)/adm_local/unix/make_common_starter.am

lib_LTLIBRARIES= libVisuPipeLine.la

salomeinclude_HEADERS= \
	VISU_MapperHolder.hxx \
	VISU_DataSetMapperHolder.hxx \
	VISU_PolyDataMapperHolder.hxx \
	VISU_PointSpriteMapperHolder.hxx \
	VISU_PipeLineUtils.hxx \
	VISU_PipeLine.hxx \
	VISU_ColoredPL.hxx \
	VISU_MergedPL.hxx \
	VISU_UnstructuredGridPL.hxx \
	VISU_MeshPL.hxx \
	VISU_ScalarMapPL.hxx \
	VISU_CutPlanesPL.hxx \
	VISU_CutLinesBasePL.hxx \
	VISU_CutLinesPL.hxx \
	VISU_CutSegmentPL.hxx \
	VISU_IsoSurfacesPL.hxx \
	VISU_DeformedShapePL.hxx \
	VISU_VectorsPL.hxx \
	VISU_StreamLinesPL.hxx \
	VISU_LookupTable.hxx \
	VISU_ScalarBarActor.hxx \
	VISU_Extractor.hxx \
	VISU_FieldTransform.hxx \
	VISU_MaskPointsFilter.hxx \
	VISU_PolyDataPL.hxx \
	VISU_GaussPointsPL.hxx \
	VISU_Plot3DPL.hxx \
	VISU_DeformedGridPL.hxx \
	VISU_OpenGLPointSpriteMapper.hxx \
	VISU_ImplicitFunctionWidget.hxx \
	SALOME_ExtractGeometry.h \
	SALOME_ExtractPolyDataGeometry.h \
	VISU_ScalarBarCtrl.hxx \
	VISU_PlanesWidget.hxx \
	VISU_SphereWidget.hxx \
	VISU_WidgetCtrl.hxx \
	VISU_DeformedShapeAndScalarMapPL.hxx \
	VISUPipeline.hxx \
	VISU_LabelPointsFilter.hxx \
	VISU_ElnoDisassembleFilter.hxx \
	VISU_ElnoAssembleFilter.hxx \
	VISU_DeformationPL.hxx \
	VISU_OptionalDeformationPL.hxx \
	VISU_XYPlotActor.hxx \
	VISU_CellDataToPointData.hxx

dist_libVisuPipeLine_la_SOURCES= \
	VISU_MapperHolder.cxx \
	VISU_DataSetMapperHolder.cxx \
	VISU_PolyDataMapperHolder.cxx \
	VISU_PointSpriteMapperHolder.cxx \
	VISU_PipeLineUtils.cxx \
	VISU_PipeLine.cxx \
	VISU_ColoredPL.cxx \
	VISU_MergedPL.cxx \
	VISU_UnstructuredGridPL.cxx \
	VISU_MeshPL.cxx \
	VISU_ScalarMapPL.cxx \
	VISU_CutPlanesPL.cxx \
	VISU_CutLinesBasePL.cxx \
	VISU_CutLinesPL.cxx \
	VISU_CutSegmentPL.cxx \
	VISU_IsoSurfacesPL.cxx \
	VISU_DeformedShapePL.cxx \
	VISU_VectorsPL.cxx \
	VISU_StreamLinesPL.cxx \
	VISU_LookupTable.cxx \
	VISU_ScalarBarActor.cxx \
	VISU_Extractor.cxx \
	VISU_FieldTransform.cxx \
	VISU_MaskPointsFilter.cxx \
	VISU_PolyDataPL.cxx \
	VISU_GaussPointsPL.cxx \
	VISU_Plot3DPL.cxx \
	VISU_DeformedGridPL.cxx \
	SALOME_ExtractGeometry.cxx \
	SALOME_ExtractPolyDataGeometry.cxx \
	VISU_OpenGLPointSpriteMapper.cxx \
	VISU_ImplicitFunctionWidget.cxx \
	VISU_PlanesWidget.cxx \
	VISU_SphereWidget.cxx \
	VISU_WidgetCtrl.cxx \
	VISU_ScalarBarCtrl.cxx \
	VISU_DeformedShapeAndScalarMapPL.cxx \
	VISU_LabelPointsFilter.cxx \
	VISU_ElnoDisassembleFilter.cxx \
	VISU_ElnoAssembleFilter.cxx \
	VISU_DeformationPL.cxx \
	VISU_OptionalDeformationPL.cxx\
	VISU_XYPlotActor.cxx \
	VISU_CellDataToPointData.cxx

libVisuPipeLine_la_CPPFLAGS= \
	$(VTK_INCLUDES) \
	$(QT_INCLUDES) \
	$(KERNEL_CXXFLAGS) \
	$(GUI_CXXFLAGS) \
	$(MED_CXXFLAGS) \
	$(HDF5_INCLUDES) $(BOOST_CPPFLAGS) \
	-I$(srcdir)/../CONVERTOR

libVisuPipeLine_la_LDFLAGS= \
	$(VTK_LIBS) -lVTKViewer \
	$(KERNEL_LDFLAGS) -lSALOMELocalTrace \
	$(GUI_LDFLAGS) -lqtx -lsuit -lvtkTools \
	$(MED_LDFLAGS) \
	../CONVERTOR/libVisuConvertor.la \
	$(MED3_LIBS_C_ONLY) \
	$(QT_LIBS) \
	$(OGL_LIBS)


# Executables targets
bin_PROGRAMS= VISUPipeLine VISU_img2vti
dist_VISUPipeLine_SOURCES= VISUPipeLine.cxx
dist_VISU_img2vti_SOURCES= VISU_img2vti.cxx

AM_CPPFLAGS+=$(libVisuPipeLine_la_CPPFLAGS)
LDADD=$(libVisuPipeLine_la_LDFLAGS) libVisuPipeLine.la \
	-lOpUtil -lMEDWrapper -lMEDWrapper_V2_2 -lMEDWrapperBase -lsuit -lqtx -lSALOMEBasics -lvtkIOImage$(VTK_SUFFIX)

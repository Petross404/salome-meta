// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:    VISU_ScalarMapPL.cxx
// Author:  Roman NIKOLAEV
// Module : VISU
//Salome includes
//
#include "VISU_DeformationPL.hxx"
#include "VISU_MergeFilter.hxx"
#include "VISU_DeformedShapePL.hxx"
#include "VISU_PipeLineUtils.hxx"

//VTK includes
#include <vtkDataSet.h>
#include <vtkPassThroughFilter.h>
#include <vtkWarpVector.h>
#include <vtkUnstructuredGrid.h>
#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//----------------------------------------------------------------------------
VISU_DeformationPL::VISU_DeformationPL():
  myScaleFactor(1.0),
  myMapScaleFactor(1.0)
{
  if(MYDEBUG) MESSAGE("VISU_DeformationPL()::VISU_DeformationPL() - "<<this);
  myWarpVector = vtkWarpVector::New();
  myWarpVector->SetScaleFactor(myScaleFactor);
  
  myVectorMergeFilter = VISU_MergeFilter::New();
  myVectorMergeFilter->SetMergingInputs(true);
  myInputPassFilter = vtkPassThroughFilter::New();
  myOutputPassFiler = vtkPassThroughFilter::New();
  myCellDataToPointData = VISU_CellDataToPointData::New();
  myCellDataToPointData->PassCellDataOn();

  myInputPassFilter->SetInputData(vtkUnstructuredGrid::New());

  myCellDataToPointData->SetInputConnection(myInputPassFilter->GetOutputPort());

  myWarpVector->SetInputConnection(myCellDataToPointData->GetOutputPort());
  
  myOutputPassFiler->SetInputConnection(myWarpVector->GetOutputPort());
}

//----------------------------------------------------------------------------
VISU_DeformationPL::~VISU_DeformationPL()
{
  if(MYDEBUG) MESSAGE("VISU_DeformationPL()::~VISU_DeformationPL() - "<<this);
  myWarpVector->Delete();
  myVectorMergeFilter->Delete();
  myInputPassFilter->Delete();
  myOutputPassFiler->Delete();
  myCellDataToPointData->Delete();
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_DeformationPL::GetMTime(){
  unsigned long int aTime = std::max(myWarpVector->GetMTime(), 
                                     myVectorMergeFilter->GetMTime());

  aTime = std::max(aTime,myInputPassFilter->GetMTime());
  aTime = std::max(aTime,myOutputPassFiler->GetMTime());
  aTime = std::max(aTime,myCellDataToPointData->GetMTime());
  return 0;
}

//----------------------------------------------------------------------------
void VISU_DeformationPL::SetScale(double theScaleFactor)
{
  if(myScaleFactor == theScaleFactor)
    return;
  myScaleFactor = theScaleFactor;
  myWarpVector->SetScaleFactor(myScaleFactor*myMapScaleFactor);
}

void VISU_DeformationPL::SetMapScale(double theMapScaleFactor)
{
  if(myMapScaleFactor == theMapScaleFactor)
    return;
  myMapScaleFactor = theMapScaleFactor;
  
  myWarpVector->SetScaleFactor(myScaleFactor*myMapScaleFactor);
}


double VISU_DeformationPL::GetScale()
{
  return myScaleFactor;
}

//----------------------------------------------------------------------------
void VISU_DeformationPL::SetWarpVectorInput(vtkDataSet *theInput)
{
  myInputPassFilter->SetInputData(theInput);
}

//----------------------------------------------------------------------------
vtkDataSet* VISU_DeformationPL::GetWarpVectorOutput()
{
  vtkDataSet* aDataSet = myOutputPassFiler->GetOutput();
  myOutputPassFiler->Update();
  return aDataSet;
}

//----------------------------------------------------------------------------
vtkAlgorithmOutput* VISU_DeformationPL::GetWarpVectorOutputPort()
{
  return myOutputPassFiler->GetOutputPort();
}

//----------------------------------------------------------------------------
void VISU_DeformationPL::SetMergeFilterInput(vtkDataSet* ScalarInput,
                         vtkDataSet* VectorialInput)
{
  myVectorMergeFilter->SetScalarsData(ScalarInput);
  myVectorMergeFilter->AddField("VISU_CELLS_MAPPER",ScalarInput);
  myVectorMergeFilter->AddField("VISU_POINTS_MAPPER",ScalarInput);
  
  myVectorMergeFilter->SetGeometryData(VectorialInput);
  myVectorMergeFilter->SetVectorsData(VectorialInput);
}

//----------------------------------------------------------------------------
vtkDataSet* VISU_DeformationPL::GetMergeFilterOutput(){
  vtkDataSet* aDataSet = myVectorMergeFilter->GetOutput();
  myVectorMergeFilter->Update();
  return aDataSet;
}

//----------------------------------------------------------------------------
double VISU_DeformationPL::GetDefaultScaleFactor(VISU_DeformationPL *thePipeLine)
{
  if(!thePipeLine || !thePipeLine->GetMergeFilterOutput())
    return 0.0;
  
  double aSourceRange[2];
  thePipeLine->GetMergeFilterOutput()->GetScalarRange(aSourceRange);
  
  static double EPS = 1.0 / VTK_LARGE_FLOAT;
  if(fabs(aSourceRange[1]) > EPS){
    vtkDataSet* aDataSet = thePipeLine->GetMergeFilterOutput();
    double aScaleFactor = VISU_DeformedShapePL::GetScaleFactor(aDataSet);
    return aScaleFactor / aSourceRange[1];
  }
  return 0.0;
}


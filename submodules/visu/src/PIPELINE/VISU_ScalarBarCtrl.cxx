// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_ScalarBarCtrl.cxx
// Author:  Peter KURNEV
// Module : VISU
//
#include "VISU_ScalarBarCtrl.hxx"
#include "VISU_LookupTable.hxx"
#include "VISU_ScalarBarActor.hxx"

#include <vtkObjectFactory.h>
#include <vtkActor2D.h> 
#include <vtkCoordinate.h>
#include <vtkRenderer.h>
#include <vtkScalarsToColors.h>
#include <vtkTextProperty.h>
#include <vtkType.h>

#include <string.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_ScalarBarCtrl);

//----------------------------------------------------------------------------
VISU_ScalarBarCtrl
::VISU_ScalarBarCtrl():
  myGlobalRangeIsDefined(false)
{
  myDistance=0.02;
  myPosition[0]=0.15;
  myPosition[1]=0.01;
  myBicolor=false;
  myMarked=false;
  myMarkedValue=99.;
  //
  SetMode(eSimple);

  // Initilize global scalar bar
  myGlobalLookupTable = VISU_LookupTable::New();
  myGlobalLookupTable->SetHueRange(0.667,0.0);

  myGlobalScalarBar = VISU_ScalarBarActor::New();
  myGlobalScalarBar->SetLookupTable(myGlobalLookupTable);
  myGlobalLookupTable->Delete();

  // Initilize local scalar bar
  myLocalLookupTable = VISU_LookupTable::New();
  myLocalLookupTable->SetHueRange(0.667,0.0);

  myLocalScalarBar = VISU_ScalarBarActor::New();
  myLocalScalarBar->SetLookupTable(myLocalLookupTable);
  myLocalLookupTable->Delete();

  myBlack[0] = myBlack[1] = myBlack[2] = 0;
  myGrey[0] = myGrey[1] = myGrey[2] = 192;
  //
  myCtrlVisibility = 1;
  SetVisibility(1);
}


//----------------------------------------------------------------------------
VISU_ScalarBarCtrl
::~VISU_ScalarBarCtrl()
{
  myGlobalScalarBar->Delete();
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetMode(VISU_ScalarBarCtrl::EMode theMode)
{
  myMode = theMode;
}

VISU_ScalarBarCtrl::EMode
VISU_ScalarBarCtrl
::GetMode() const
{
  return myMode;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetVisibility(int theVisibility)
{
  myGlobalScalarBar->SetVisibility(false);
  myLocalScalarBar->SetVisibility(false);
  int aVisibility = (myCtrlVisibility && theVisibility);
  if(aVisibility){
    if(myMode != eSimple)
      myGlobalScalarBar->SetVisibility(aVisibility);
    myLocalScalarBar->SetVisibility(aVisibility);
  }
}

int
VISU_ScalarBarCtrl
::GetVisibility() const
{
  return myGlobalScalarBar->GetVisibility() || myLocalScalarBar->GetVisibility();
}

void
VISU_ScalarBarCtrl
::SetCtrlVisibility(int theVisibility)
{
  myCtrlVisibility = theVisibility;
}

int
VISU_ScalarBarCtrl
::GetCtrlVisibility() const
{
  return myCtrlVisibility;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetRangeLocal(double *theRange)
{
  myLocalLookupTable->SetTableRange(theRange);
}

void
VISU_ScalarBarCtrl
::SetRangeLocal(double theMin,
                double theMax)
{
  myLocalLookupTable->SetTableRange(theMin,theMax);
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetRangeGlobal(double *theRange)
{
  myGlobalLookupTable->SetTableRange(theRange);
}

void
VISU_ScalarBarCtrl
::SetRangeGlobal(double theMin,
                 double theMax)
{
  myGlobalLookupTable->SetTableRange(theMin,theMax);
}

void
VISU_ScalarBarCtrl
::SetGlobalRangeIsDefined(bool theIsDefined)
{
  myGlobalRangeIsDefined = theIsDefined;
}


//----------------------------------------------------------------------------
VISU_ScalarBarActor* 
VISU_ScalarBarCtrl
::GetLocalBar() 
{
  return myLocalScalarBar;
}

VISU_ScalarBarActor* 
VISU_ScalarBarCtrl
::GetGlobalBar() 
{
  return myGlobalScalarBar;
}


//----------------------------------------------------------------------------
VISU_LookupTable* 
VISU_ScalarBarCtrl
::GetLocalTable() 
{
  return myLocalLookupTable;
}

VISU_LookupTable* 
VISU_ScalarBarCtrl
::GetGlobalTable() 
{
  return myGlobalLookupTable;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::AddToRender(vtkRenderer* theRenderer)
{
  theRenderer->AddActor2D(myGlobalScalarBar);
  theRenderer->AddActor2D(myLocalScalarBar);
  Update();
}

void
VISU_ScalarBarCtrl
::RemoveFromRender(vtkRenderer* theRenderer)
{
  theRenderer->RemoveActor2D(myGlobalScalarBar);
  theRenderer->RemoveActor2D(myLocalScalarBar);
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetWidth(double theWidth)
{
  myGlobalScalarBar->SetWidth(theWidth);
  myLocalScalarBar->SetWidth(theWidth);
}

double
VISU_ScalarBarCtrl
::GetWidth() const
{
  return myGlobalScalarBar->GetWidth();
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetHeight(double theHeight)
{
  myGlobalScalarBar->SetHeight(theHeight);
  myLocalScalarBar->SetHeight(theHeight);
}


double
VISU_ScalarBarCtrl
::GetHeight() const
{
  return myGlobalScalarBar->GetHeight();
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetPosition(const double* thePosition)
{
  myPosition[0] = thePosition[0];
  myPosition[1] = thePosition[1];
}

const double* 
VISU_ScalarBarCtrl::GetPosition() const
{
  return myPosition;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetSpacing(const double theSpacing)
{
  myDistance = theSpacing;
}

double
VISU_ScalarBarCtrl
::GetSpacing() const
{
  return myDistance;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetBicolor(const bool theBicolor)
{
  myBicolor = theBicolor;
}

bool
VISU_ScalarBarCtrl
::GetBicolor() const
{
  return myBicolor;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetMarkValue(const double theValue) 
{
  myMarkedValue = theValue;
}

double
VISU_ScalarBarCtrl
::GetMarkValue() const
{
  return myMarkedValue;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::SetIsMarked(const bool theFlag) 
{
  myMarked = theFlag;
}

bool
VISU_ScalarBarCtrl
::GetIsMarked() const
{
  return myMarked;
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::Update()
{
  SetVisibility(GetVisibility());
  //
  PrepareTables();
  //
  if(myBicolor)
    UpdateForBicolor();
  else
    UpdateForColor();
  //
  UpdateMarkValue();
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::UpdateMarkValue()
{
  if(myMarked){
    if(myMode == eGlobal){
      myGlobalLookupTable->MarkValueByColor( myMarkedValue, myBlack );
    }else{
      myLocalLookupTable->MarkValueByColor( myMarkedValue, myBlack );
    }
  }
  if(myGlobalRangeIsDefined){
    double aLocalRange[2];
    myLocalLookupTable->GetTableRange(aLocalRange);
    myGlobalLookupTable->MarkValueByColor( aLocalRange[0], myBlack );
    myGlobalLookupTable->MarkValueByColor( aLocalRange[1], myBlack );
  }
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::PrepareTables(VISU_ScalarBarActor* theScalarBarActor,
                VISU_LookupTable *theLookupTable,
                vtkIdType theId)
{
  vtkCoordinate * aCoordinate = theScalarBarActor->GetPositionCoordinate();
  aCoordinate->SetCoordinateSystemToNormalizedViewport();
  if(theScalarBarActor->GetOrientation() == VTK_ORIENT_VERTICAL){
    double aWidth = theScalarBarActor->GetWidth();
    aCoordinate->SetValue(myPosition[0]+theId*(aWidth+myDistance), myPosition[1]);
  }else{ 
    double aHeight = theScalarBarActor->GetHeight();
    aCoordinate->SetValue(myPosition[0], myPosition[1]+theId*(aHeight+myDistance));
  }
  // Initialize Lookup Tables and Scalar Bars 
  theLookupTable->Modified();
  theLookupTable->Build();
}


void
VISU_ScalarBarCtrl
::PrepareTables()
{
  if(myMode != eSimple){
    PrepareTables(myGlobalScalarBar,myGlobalLookupTable,0);
    PrepareTables(myLocalScalarBar,myLocalLookupTable,1);
  }else{
    PrepareTables(myLocalScalarBar,myLocalLookupTable,0);
  }
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::UpdateForColor()
{
  if(myMode == eGlobal){ 
    myLocalLookupTable->FillByColor( myGrey );
  }else if(myMode == eLocal){
    myGlobalLookupTable->FillByColor( myGrey );
  }
}


//----------------------------------------------------------------------------
void
VISU_ScalarBarCtrl
::UpdateForBicolor()
{
  myLocalLookupTable->Modified();
  myLocalLookupTable->Build();

  if(myMode == eSimple){
    myLocalLookupTable->MakeBiColor();
    return;
  }

  if(myMode == eGlobal){
    myGlobalLookupTable->MakeBiColor();
    myLocalLookupTable->FillByColor( myGrey );
  }else if(myMode == eLocal){
    myLocalLookupTable->MakeBiColor();
    myGlobalLookupTable->FillByColor( myGrey );
  }
}

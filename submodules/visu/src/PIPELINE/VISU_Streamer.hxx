// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_Streamer_HeaderFile
#define VISU_Streamer_HeaderFile

#include "VISUPipeline.hxx"

#include <vtkStreamer.h>

class VISU_PIPELINE_EXPORT VISU_Streamer : public vtkStreamer
{
public:
  vtkTypeMacro(VISU_Streamer,vtkStreamer);

protected:
  // Description:
  // Construct object to start from position (0,0,0); integrate forward;
  // terminal speed 0.0; vorticity computation off; integrations step length
  // 0.2; and maximum propagation time 100.0.
  VISU_Streamer();
  ~VISU_Streamer();

  // Integrate data
  void Integrate(vtkDataSet *input, vtkDataSet *source);

  static VTK_THREAD_RETURN_TYPE ThreadedIntegrate( void *arg );

private:
  VISU_Streamer(const VISU_Streamer&);  // Not implemented.
  void operator=(const VISU_Streamer&);  // Not implemented.
};

#endif

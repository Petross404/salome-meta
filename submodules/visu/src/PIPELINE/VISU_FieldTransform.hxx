// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_FieldTransform.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_FieldTransform_HeaderFile
#define VISU_FieldTransform_HeaderFile

#include <vtkDataSetAlgorithm.h>

#include "VISUPipeline.hxx"

class VTKViewer_Transform;

class VISU_PIPELINE_EXPORT VISU_FieldTransform : public vtkDataSetAlgorithm
{
public:
  vtkTypeMacro(VISU_FieldTransform, vtkDataSetAlgorithm);

  static 
  VISU_FieldTransform
  *New();

  virtual
  unsigned long 
  GetMTime();
  
  static
  double
  Ident(double theArg);

  static
  double
  Log10(double theArg);

  typedef double (*TTransformFun)(double);

  void
  SetScalarTransform(TTransformFun theFunction);

  TTransformFun
  GetScalarTransform() 
  {
    return myFunction;
  }

  void
  SetSpaceTransform(VTKViewer_Transform* theTransform);

  VTKViewer_Transform* 
  GetSpaceTransform() 
  {
    return myTransform;
  }

  double* 
  GetScalarRange()
  {
    return myScalarRange; 
  }
  
  void
  SetScalarRange(double theScalarRange[2]);

  void
  SetScalarMin(double theValue);

  void
  SetScalarMax(double theValue);

protected:
  VISU_FieldTransform();

  virtual
  ~VISU_FieldTransform();

  virtual
  int
  RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  VTKViewer_Transform *myTransform;
  TTransformFun myFunction;
  double myScalarRange[2];
  
private:
  VISU_FieldTransform(const VISU_FieldTransform&);
  void operator=(const VISU_FieldTransform&);
};

#endif

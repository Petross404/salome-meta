// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header$
//
#ifndef VISU_MergeFilter_H
#define VISU_MergeFilter_H

#include "VISU_Convertor.hxx"

//#include <vtkDataSetAlgorithm.h>
#include <vtkPointSetAlgorithm.h>

namespace VISU
{
  class TFieldList;
}


//------------------------------------------------------------------------------
class VISU_CONVERTOR_EXPORT VISU_MergeFilter : public vtkPointSetAlgorithm
{
public:
  static VISU_MergeFilter *New();
  vtkTypeMacro(VISU_MergeFilter, vtkPointSetAlgorithm);

  // Description:
  // Specify object from which to extract geometry information.
  void SetGeometryConnection(vtkAlgorithmOutput *input);
  void SetGeometryData(vtkDataSet *);

  // Description:
  // Specify object from which to extract scalar information.
  void SetScalarsConnection(vtkAlgorithmOutput *);
  void SetScalarsData(vtkDataSet *);

  // Description:
  // Specify object from which to extract vector information.
  void SetVectorsConnection(vtkAlgorithmOutput *);
  void SetVectorsData(vtkDataSet *);

  // Description:
  // Specify object from which to extract normal information.
  void SetNormals(vtkAlgorithmOutput *);
  
  // Description:
  // Specify object from which to extract texture coordinates
  // information.
  void SetTCoords(vtkAlgorithmOutput *);

  // Description:
  // Specify object from which to extract tensor data.
  void SetTensors(vtkAlgorithmOutput *);

  // Description:
  // Set the object from which to extract a field and the name
  // of the field
  void AddField(const char* name, vtkDataSet* input);

  // Description:
  // Removes all previously added fields
  void RemoveFields();

  // Description:
  // Defines whether to perform merging of data with the geometry according to
  // the ids of the cell or not
  void
  SetMergingInputs(bool theIsMergingInputs);
  
  bool
  IsMergingInputs();
  
protected:
  VISU_MergeFilter();
  ~VISU_MergeFilter();

  virtual
  int
  RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  int
  RequestUpdateExtent(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  int
  FillInputPortInformation(int port, vtkInformation *info);

  VISU::TFieldList* FieldList;
  bool myIsMergingInputs;

private:
  VISU_MergeFilter(const VISU_MergeFilter&);  // Not implemented.
  void operator=(const VISU_MergeFilter&);  // Not implemented.
};

#endif



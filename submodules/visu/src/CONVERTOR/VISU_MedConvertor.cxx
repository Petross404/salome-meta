// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MedConvertor.cxx
//  Author : Alexey PETROV
//  Module : VISU

#include "VISU_MedConvertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "MED_Factory.hxx"
#include "MED_Algorithm.hxx"
#include "MED_GaussUtils.hxx"
#include "MED_Utilities.hxx"

#include "utilities.h"

#include "SALOMEconfig.h"

#if (OCC_VERSION_MAJOR << 16 | OCC_VERSION_MINOR << 8 | OCC_VERSION_MAINTENANCE) > 0x060100
#define NO_CAS_CATCH
#endif

#include <NCollection_Map.hxx>

#include <Standard_Failure.hxx>

#ifdef NO_CAS_CATCH
#include <Standard_ErrorHandler.hxx>
#endif

#include <vtkCellType.h>

#ifdef WNT
#include <float.h>
#define isnan _isnan
#endif

using MED::TInt;
using MED::TFloat;
using MED::EBooleen;

#ifdef _DEBUG_
static int MYDEBUG = 0;
static int MYVALUEDEBUG = 0;
static int MY_FAMILY_DEBUG = 0;
static int MY_GROUP_DEBUG = 0;
//#define _DEXCEPT_
#else
static int MYDEBUG = 0;
static int MYVALUEDEBUG = 0;
static int MY_FAMILY_DEBUG = 0;
static int MY_GROUP_DEBUG = 0;
#endif

#define _LOAD_FAMILIES_
#define _EDF_NODE_IDS_



namespace
{
  //---------------------------------------------------------------
  vtkIdType
  MEDGeom2NbNodes(MED::EGeometrieElement theMEDGeomType)
  { 
    return theMEDGeomType % 100;
  }


  //---------------------------------------------------------------
  VISU::EGeometry
  MEDGeom2VISU(MED::EGeometrieElement theGeom)
  { 
    switch(theGeom){
    case MED::ePOINT1: return VISU::ePOINT1;
    case MED::eSEG2: return VISU::eSEG2;
    case MED::eTRIA3: return VISU::eTRIA3;
    case MED::eQUAD4: return VISU::eQUAD4;
    case MED::eTETRA4: return VISU::eTETRA4;
    case MED::eHEXA8: return VISU::eHEXA8;
    case MED::ePENTA6: return VISU::ePENTA6;
    case MED::ePYRA5: return VISU::ePYRA5;

    case MED::eSEG3: return VISU::eSEG3;
    case MED::eTRIA6: return VISU::eTRIA6;
    case MED::eQUAD8: return VISU::eQUAD8;
    case MED::eQUAD9: return VISU::eQUAD9;
    case MED::eOCTA12: return VISU::eOCTA12;
    case MED::eTETRA10: return VISU::eTETRA10;
    case MED::eHEXA20: return VISU::eHEXA20;
    case MED::eHEXA27: return VISU::eHEXA27;
    case MED::ePENTA15: return VISU::ePENTA15;
    case MED::ePYRA13: return VISU::ePYRA13;

    case MED::ePOLYGONE: return VISU::ePOLYGONE;
    case MED::ePOLYEDRE: return VISU::ePOLYEDRE;
    }
    return VISU::eNONE;
  }


  //---------------------------------------------------------------
  MED::EGeometrieElement 
  VISUGeom2MED(int theGeom)
  { 
    switch(theGeom){
    case VISU::ePOINT1: return MED::ePOINT1;
    case VISU::eSEG2: return MED::eSEG2;
    case VISU::eTRIA3: return MED::eTRIA3;
    case VISU::eQUAD4: return MED::eQUAD4;
    case VISU::eTETRA4: return MED::eTETRA4;
    case VISU::eHEXA8: return MED::eHEXA8;
    case VISU::ePENTA6: return MED::ePENTA6;
    case VISU::ePYRA5: return MED::ePYRA5;

    case VISU::eSEG3: return MED::eSEG3;
    case VISU::eTRIA6: return MED::eTRIA6;
    case VISU::eQUAD8: return MED::eQUAD8;
    case VISU::eQUAD9: return MED::eQUAD9;
    case VISU::eOCTA12: return MED::eOCTA12;
    case VISU::eTETRA10: return MED::eTETRA10;
    case VISU::eHEXA20: return MED::eHEXA20;
    case VISU::eHEXA27: return MED::eHEXA27;
    case VISU::ePENTA15: return MED::ePENTA15;
    case VISU::ePYRA13: return MED::ePYRA13;

    case VISU::ePOLYGONE: return MED::ePOLYGONE;
    case VISU::ePOLYEDRE: return MED::ePOLYEDRE;
    }
    return MED::EGeometrieElement(-1);
  }


  //---------------------------------------------------------------
  VISU::TEntity
  MEDEntityToVTK( MED::EEntiteMaillage theMEDEntity )
  {
    VISU::TEntity anEntity = VISU::TEntity( -1 );

    switch(theMEDEntity){
    case MED::eNOEUD :
      anEntity = VISU::NODE_ENTITY;
      break;
    case MED::eARETE : 
      anEntity = VISU::EDGE_ENTITY;
      break;
    case MED::eFACE : 
      anEntity = VISU::FACE_ENTITY;
      break;
    case MED::eMAILLE : 
      anEntity = VISU::CELL_ENTITY;
      break;
    case MED::eNOEUD_ELEMENT :
      anEntity = VISU::CELL_ENTITY;
      break;
    }

    return anEntity;
  }


  //---------------------------------------------------------------
  MED::EEntiteMaillage 
  VTKEntityToMED(VISU::TEntity theVTKEntity)
  {
    switch(theVTKEntity){
    case VISU::NODE_ENTITY: return MED::eNOEUD;
    case VISU::EDGE_ENTITY: return MED::eARETE;
    case VISU::FACE_ENTITY: return MED::eFACE;
    case VISU::CELL_ENTITY: return MED::eMAILLE;
    }
    return MED::EEntiteMaillage(-1);
  }


  //---------------------------------------------------------------
  VISU::PMEDSubProfile
  CrSubProfile(const MED::PWrapper& theMEDWrapper,
               const MED::PMeshInfo& theMeshInfo,
               MED::EEntiteMaillage theMEntity,
               MED::EGeometrieElement theMGeom,
               const MED::TGeom2Size& theGeom2Size,
               const MED::TGeom2Profile& theGeom2Profile)
  {
    VISU::EGeometry aEGeom = MEDGeom2VISU(theMGeom);
    vtkIdType aVNbNodes = VISUGeom2NbNodes(aEGeom);

    VISU::PMEDSubProfile aSubProfile(new VISU::TMEDSubProfile());
    aSubProfile->myGeom = aEGeom;
    aSubProfile->myMGeom = theMGeom;
    aSubProfile->myStatus = VISU::eAddAll;

    MED::TGeom2Size::const_iterator aTimeStampIter = theGeom2Size.find(theMGeom);
    if(aTimeStampIter == theGeom2Size.end())
      aSubProfile->myStatus = VISU::eRemoveAll;
    else{
      MED::TGeom2Profile::const_iterator aProfileIter = theGeom2Profile.find(theMGeom);
      if(aProfileIter != theGeom2Profile.end()){
        MED::PProfileInfo aProfileInfo = aProfileIter->second;

        aSubProfile->myName = aProfileInfo->GetName();
        aSubProfile->myStatus = VISU::eAddPart;

        const MED::TElemNum& anElemNum = aProfileInfo->myElemNum;
        TInt aNbElem = anElemNum.size();
        aSubProfile->myNbCells = aNbElem;
        switch(theMGeom){
        case MED::ePOLYGONE: {
          MED::PPolygoneInfo aPolygoneInfo = 
            theMEDWrapper->GetPPolygoneInfo(theMeshInfo,theMEntity,theMGeom);
          for(TInt anElemId = 0; anElemId < aNbElem ; anElemId++){
            TInt aNbConn = aPolygoneInfo->GetNbConn( anElemNum[ anElemId ] - 1 );
            aSubProfile->myCellsSize += aNbConn;
          }
          break;
        }
        case MED::ePOLYEDRE: {
          MED::PPolyedreInfo aPolyedreInfo = 
            theMEDWrapper->GetPPolyedreInfo(theMeshInfo,theMEntity,theMGeom);
          for(TInt anElemId = 0; anElemId < aNbElem ; anElemId++){
            MED::TCConnSliceArr aConnSliceArr = 
              aPolyedreInfo->GetConnSliceArr( anElemNum[ anElemId ] - 1 );
            TInt aNbFaces = aConnSliceArr.size();
            TInt aCellSize = 0;
            for(TInt iFace = 0; iFace < aNbFaces; iFace++){
              MED::TCConnSlice aConnSlice = aConnSliceArr[iFace];
              TInt aNbConn = aConnSlice.size();
              aCellSize += aNbConn;
            }
            aSubProfile->myCellsSize += aCellSize;
          }
          break;
        }
        default: {
          aSubProfile->myCellsSize = aSubProfile->myNbCells*aVNbNodes;
        }}
      }else{
        TInt aNbElem = aTimeStampIter->second;
        aSubProfile->myNbCells = aNbElem;
        switch(theMGeom){
        case MED::ePOLYGONE: {
          MED::PPolygoneInfo aPolygoneInfo = 
            theMEDWrapper->GetPPolygoneInfo(theMeshInfo,theMEntity,theMGeom);
          for(TInt anElemId = 0; anElemId < aNbElem ; anElemId++){
            TInt aNbConn = aPolygoneInfo->GetNbConn(anElemId);
            aSubProfile->myCellsSize += aNbConn;
          }
          break;
        }
        case MED::ePOLYEDRE: {
          MED::PPolyedreInfo aPolyedreInfo = 
            theMEDWrapper->GetPPolyedreInfo(theMeshInfo,theMEntity,theMGeom);
          for(TInt anElemId = 0; anElemId < aNbElem ; anElemId++){
            MED::TCConnSliceArr aConnSliceArr = 
              aPolyedreInfo->GetConnSliceArr(anElemId);
            TInt aNbFaces = aConnSliceArr.size();
            TInt aCellSize = 0;
            for(TInt iFace = 0; iFace < aNbFaces; iFace++){
              MED::TCConnSlice aConnSlice = aConnSliceArr[iFace];
              TInt aNbConn = aConnSlice.size();
              aCellSize += aNbConn;
            }
            aSubProfile->myCellsSize += aCellSize;
          }
          break;
        }
        default: {
          aSubProfile->myCellsSize = aSubProfile->myNbCells*aVNbNodes;
        }}
      }
    }
    INITMSG(MYDEBUG,
            "- aMGeom = "<<theMGeom<<
            "; aEGeom = "<<aEGeom<<
            "; aName = '"<<aSubProfile->myName<<"'"<<
            "; aStatus = "<<aSubProfile->myStatus<<
            "; aNbCells = "<<aSubProfile->myNbCells<<
            "; aCellsSize = "<<aSubProfile->myCellsSize<<
            endl);
    
    return aSubProfile;
  }


  //---------------------------------------------------------------
  VISU::TProfileKey
  GetProfileKey(const MED::PWrapper& theMEDWrapper,
                const MED::PMeshInfo& theMeshInfo,
                const MED::PTimeStampValueBase& theTimeStampValue,
                const VISU::TMEDMeshOnEntity& theMeshOnEntity,
                MED::EEntiteMaillage theMEntity,
                const MED::TGeom2Size& theGeom2Size)
  {
    INITMSG(MYDEBUG,"GetProfileKey"<<endl);
    
    VISU::TProfileKey aProfileKey;
    const MED::TGeom2Profile& aGeom2Profile = theTimeStampValue->GetGeom2Profile();

    const MED::TGeom2Size& aGeom2Size = theMeshOnEntity.myGeom2Size;
    MED::TGeom2Size::const_iterator anIter = aGeom2Size.begin();
    for(; anIter != aGeom2Size.end(); anIter++){
      MED::EGeometrieElement aMGeom = anIter->first;
      VISU::PSubProfile aSubProfile = CrSubProfile(theMEDWrapper,
                                                   theMeshInfo,
                                                   theMEntity,
                                                   aMGeom,
                                                   theGeom2Size,
                                                   aGeom2Profile);
      aProfileKey.insert(aSubProfile);
    }
    
    return aProfileKey;
  }
  

  //---------------------------------------------------------------
  void
  InitProfile(const MED::PWrapper& theMEDWrapper,
              const MED::PMeshInfo& theMeshInfo,
              MED::PTimeStampValueBase& theTimeStampValue,
              VISU::TMEDMeshOnEntity& theMeshOnEntity,
              MED::EEntiteMaillage theMEntity,
              const MED::TGeom2Size& theGeom2Size,
              VISU::TMEDValForTime& theValForTime)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"InitProfile");
    INITMSG(MYDEBUG,"InitProfile"<<endl);
    
    VISU::TProfileMap& aProfileMap = theMeshOnEntity.myProfileMap;
    
    VISU::TProfileKey aProfileKey = GetProfileKey(theMEDWrapper,
                                            theMeshInfo,
                                            theTimeStampValue,
                                            theMeshOnEntity,
                                            theMEntity,
                                            theGeom2Size);
    
    VISU::TProfileMap::const_iterator anIter = aProfileMap.find(aProfileKey);
    if(anIter != aProfileMap.end()){
      theValForTime.myProfile = anIter->second;
      INITMSG(MYDEBUG,"aProfileMap.find(aProfileKey)"<<endl);
    }else{
      VISU::PMEDProfile aProfile(new VISU::TMEDProfile());
      VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;
      
      VISU::TProfileKey::const_iterator anIter = aProfileKey.begin();
      for(; anIter != aProfileKey.end(); anIter++){
        VISU::PMEDSubProfile aSubProfile(*anIter);

        if(aProfile->myIsAll && aSubProfile->myStatus != VISU::eAddAll)
          aProfile->myIsAll = false;

        VISU::EGeometry aEGeom = aSubProfile->myGeom;
        aGeom2SubProfile[aEGeom] = aSubProfile;
      }

      aProfileMap[aProfileKey] = aProfile;
      theValForTime.myProfile = aProfile;
    }
  }


  //---------------------------------------------------------------
  VISU::TGaussKey
  GetGaussKey(const MED::PTimeStampValueBase& theTimeStampValue,
              const VISU::TMEDMeshOnEntity& theMeshOnEntity,
              const MED::TGeom2Size& theGeom2Size,
              VISU::TMEDValForTime& theValForTime)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"GetGaussKey");
    INITMSG(MYDEBUG,"GetGaussKey"<<endl);

    VISU::TGaussKey aGaussKey;
    VISU::PMEDProfile aProfile = theValForTime.myProfile;
    VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;

    const MED::TTimeStampInfo& aTimeStampInfo = theTimeStampValue->GetTimeStampInfo();
    const MED::TGeom2Gauss& aGeom2Gauss = aTimeStampInfo.GetGeom2Gauss();

    const MED::TGeom2Size& aGeom2Size = theMeshOnEntity.myGeom2Size;
    MED::TGeom2Size::const_iterator anIter = aGeom2Size.begin();
    for(; anIter != aGeom2Size.end(); anIter++){
      MED::EGeometrieElement aMGeom = anIter->first;
      VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);

      VISU::TGeom2SubProfile::iterator anIter2 = aGeom2SubProfile.find(aEGeom);
      if(anIter2 == aGeom2SubProfile.end()){
        INITMSG(MYDEBUG,"anIter2 == aGeom2SubProfile.end!!"<<endl);
        continue;
      }
      VISU::PMEDSubProfile aSubProfile = anIter2->second;

      MED::TGeom2Size::const_iterator aTimeStampIter = theGeom2Size.find(aMGeom);
      if(aTimeStampIter != theGeom2Size.end()){
        TInt aNbCells = aTimeStampIter->second;
        if(aSubProfile->myStatus == VISU::eAddPart)
          aNbCells = aSubProfile->myNbCells;

        VISU::PMEDGaussSubMesh aGaussSubMesh(new VISU::TMEDGaussSubMesh());
        aGaussSubMesh->mySubProfile = aSubProfile;
        aGaussSubMesh->myStatus = aSubProfile->myStatus;
          
        VISU::PMEDGauss aGauss(new VISU::TMEDGauss());
        aGaussSubMesh->myGauss = aGauss;
        aGauss->myGeom = aEGeom;
        aGauss->myNbPoints = 1;

        MED::TGeom2Gauss::const_iterator aGaussIter = aGeom2Gauss.find(aMGeom);
        if(aGaussIter != aGeom2Gauss.end()){
          MED::PGaussInfo aGaussInfo = aGaussIter->second;
          aGauss->myGaussInfo = aGaussInfo;
          aGauss->myName = aGaussInfo->GetName();
          aGauss->myNbPoints = aGaussInfo->GetNbGauss();
        } else if ( aTimeStampInfo.GetNbGauss(aMGeom) > 1 )
          EXCEPTION( std::runtime_error, "aGaussIter == aGeom2Gauss.end()"
                     " && aTimeStampInfo.GetNbGauss(aMGeom) > 1 !!!" );
        
        aGaussSubMesh->myNbCells = aNbCells*aGauss->myNbPoints;
        aGaussSubMesh->myCellsSize = aGaussSubMesh->myNbCells*2;

        aGaussKey.insert(aGaussSubMesh);

        INITMSGA(MYDEBUG,0,
                 "- aEGeom = "<<aGauss->myGeom<<
                 "; aName = '"<<aGauss->myName<<"'"<<
                 "; aNbGauss = "<<aGauss->myNbPoints<<
                 "; aStatus = "<<aGaussSubMesh->myStatus<<
                 "; aNbCells = "<<aGaussSubMesh->myNbCells<<
                 "; aCellsSize = "<<aGaussSubMesh->myCellsSize<<
                 endl);

        //break;
      }
    }

    return aGaussKey;
  }


  //---------------------------------------------------------------
  void
  InitGaussMesh(MED::PTimeStampValueBase& theTimeStampValue,
                VISU::TMEDMeshOnEntity& theMeshOnEntity,
                const MED::TGeom2Size& theGeom2Size,
                VISU::TMEDValForTime& theValForTime)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"InitGaussMesh");
    INITMSG(MYDEBUG,"InitGaussMesh"<<endl);

    if(theMeshOnEntity.myEntity == VISU::NODE_ENTITY)
      return;

    VISU::TGaussMeshMap& aGaussMeshMap = theMeshOnEntity.myGaussMeshMap;
    
    VISU::TGaussKey aGaussKey = GetGaussKey(theTimeStampValue,
                                            theMeshOnEntity,
                                            theGeom2Size,
                                            theValForTime);

    VISU::TGaussMeshMap::const_iterator anIter = aGaussMeshMap.find(aGaussKey);
    if(anIter != aGaussMeshMap.end()){
      theValForTime.myGaussMesh = anIter->second;
      INITMSG(MYDEBUG,"aGaussMeshMap.find(aGaussKey)"<<endl);
    }else{
      VISU::PMEDGaussMesh aGaussMesh(new VISU::TMEDGaussMesh());
      VISU::TGeom2GaussSubMesh& aGeom2GaussSubMesh = aGaussMesh->myGeom2GaussSubMesh;
      {
        VISU::TGaussKey::const_iterator anIter = aGaussKey.begin();
        for(; anIter != aGaussKey.end(); anIter++){
          VISU::PMEDGaussSubMesh aGaussSubMesh(*anIter);
          VISU::PMEDGauss aGauss = aGaussSubMesh->myGauss;
          VISU::EGeometry aEGeom = aGauss->myGeom;
          aGeom2GaussSubMesh[aEGeom] = aGaussSubMesh;
        }
      }
      {
        VISU::TGaussSubMeshArr& aGaussSubMeshArr = aGaussMesh->myGaussSubMeshArr;
        aGaussSubMeshArr.resize(aGeom2GaussSubMesh.size());
        VISU::TGeom2GaussSubMesh::const_iterator anIter = aGeom2GaussSubMesh.begin();
        for(TInt anID = 0; anIter != aGeom2GaussSubMesh.end(); anIter++, anID++){
          const VISU::PGaussSubMeshImpl& aGaussSubMesh = anIter->second;
          aGaussSubMeshArr[anID] = aGaussSubMesh;
        }
      }
      INITMSG(MYDEBUG,"aGaussMeshMap[aGaussKey] = aGaussMesh"<<std::endl);
      aGaussMeshMap[aGaussKey] = aGaussMesh;
      theValForTime.myGaussMesh = aGaussMesh;
    }
  }

  //---------------------------------------------------------------
  void
  BuildMeshOnEntityMap(VISU::PMEDMesh theMesh,
                       const MED::TEntityInfo& theEntityInfo,
                       const MED::PNodeInfo& theNodeInfo,
                       const MED::PWrapper& theMEDWrapper)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildMeshOnEntityMap");
    INITMSG(MYDEBUG,"BuildMeshOnEntityMap"<<endl);

    MED::PMeshInfo aMeshInfo = theMesh->myMeshInfo;
    const std::string& aMeshName = theMesh->myName;
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;
    MED::TEntityInfo::const_iterator anEntityIter = theEntityInfo.begin();
    for(; anEntityIter != theEntityInfo.end(); anEntityIter++){
      const MED::EEntiteMaillage& aMEntity = anEntityIter->first;
      const MED::TGeom2Size& aGeom2Size = anEntityIter->second;
      
      VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
      VISU::PMEDMeshOnEntity aMeshOnEntity = 
        aMeshOnEntityMap[aVEntity](new VISU::TMEDMeshOnEntity());
      aMeshOnEntity->myEntity = aVEntity;
      aMeshOnEntity->myMeshName = aMeshName;
      aMeshOnEntity->myGeom2Size = aGeom2Size;
      VISU::TFamilyID2CellsSize& aFamilyID2CellsSize = aMeshOnEntity->myFamilyID2CellsSize;
      VISU::TGeom2ElemID2FamilyID& aGeom2ElemID2FamilyID = aMeshOnEntity->myGeom2ElemID2FamilyID;

      INITMSG(MYDEBUG,
              "- aMEntity = "<<aMEntity<<
              "; aVEntity = "<<aVEntity<<
              endl);
      
      if(aMEntity == MED::eNOEUD){
        aMeshOnEntity->myNbCells = theMesh->myNbPoints;
        aMeshOnEntity->myCellsSize = 2*theMesh->myNbPoints;
        
        VISU::TElemID2FamilyID& anElemID2FamilyID = aGeom2ElemID2FamilyID[MED::ePOINT1];

        for(TInt iElem = 0; iElem < theMesh->myNbPoints; iElem++){
          TInt aFamId = theNodeInfo->GetFamNum(iElem);
          if(aFamId != 0)
            aFamilyID2CellsSize[aFamId] += 2;
          anElemID2FamilyID[iElem] = aFamId;
        }
        
        INITMSG(MYDEBUG,
                "- myNbCells = "<<aMeshOnEntity->myNbCells<<
                "; myCellsSize = "<<aMeshOnEntity->myCellsSize<<
                std::endl);
        
      }else{
        MED::TGeom2Size::const_iterator aGeom2SizeIter = aGeom2Size.begin();
        aMeshOnEntity->myNbCells = 0;
        aMeshOnEntity->myCellsSize = 0;
        for(; aGeom2SizeIter != aGeom2Size.end(); aGeom2SizeIter++){
          const MED::EGeometrieElement& aMGeom = aGeom2SizeIter->first;
          
          VISU::TElemID2FamilyID& anElemID2FamilyID = aGeom2ElemID2FamilyID[aMGeom];
          switch(aMGeom){
          case MED::ePOLYGONE: {
            MED::PPolygoneInfo aPolygoneInfo = theMEDWrapper->GetPPolygoneInfo(aMeshInfo,aMEntity,aMGeom);
            TInt aNbElem = aPolygoneInfo->GetNbElem();
            
            INITMSG(MYDEBUG,
                    "- aMGeom = "<<aMGeom<<
                    "; aNbElem = "<<aNbElem<<
                    endl);
            
            aMeshOnEntity->myNbCells += aNbElem;      
            for(TInt anElemId = 0; anElemId < aNbElem ; anElemId++){
              TInt aNbConn = aPolygoneInfo->GetNbConn(anElemId);
              aMeshOnEntity->myCellsSize += aNbConn;
              TInt aFamId = aPolygoneInfo->GetFamNum(anElemId);
              if(aFamId != 0)
                aFamilyID2CellsSize[aFamId] += aNbConn + 1;
              anElemID2FamilyID[anElemId] = aFamId;
            }
            break;
          }
          case MED::ePOLYEDRE: {
            MED::PPolyedreInfo aPolyedreInfo = theMEDWrapper->GetPPolyedreInfo(aMeshInfo,aMEntity,aMGeom);
            TInt aNbElem = aPolyedreInfo->GetNbElem();
            
            INITMSG(MYDEBUG,
                    "- aMGeom = "<<aMGeom<<
                    "; aNbElem = "<<aNbElem<<
                    endl);
            
            aMeshOnEntity->myNbCells += aNbElem;
            for(TInt anElemId = 0; anElemId < aNbElem ; anElemId++){
              MED::TCConnSliceArr aConnSliceArr = aPolyedreInfo->GetConnSliceArr(anElemId);
              TInt aNbFaces = aConnSliceArr.size();
              TInt aCellSize = 0;
              for(TInt iFace = 0; iFace < aNbFaces; iFace++){
                MED::TCConnSlice aConnSlice = aConnSliceArr[iFace];
                TInt aNbConn = aConnSlice.size();
                aCellSize += aNbConn;
              }
              aMeshOnEntity->myCellsSize += aCellSize;
              TInt aFamId = aPolyedreInfo->GetFamNum(anElemId);
              if(aFamId != 0)
                aFamilyID2CellsSize[aFamId] += aCellSize + 1;
              anElemID2FamilyID[anElemId] = aFamId;
            }
            break;
          }
          default: {
            VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
            vtkIdType aVNbNodes = VISUGeom2NbNodes(aEGeom);
            MED::PCellInfo aCellInfo = theMEDWrapper->GetPCellInfo(aMeshInfo,aMEntity,aMGeom);
            TInt aNbElem = aCellInfo->GetNbElem();
            aMeshOnEntity->myNbCells += aNbElem;
            aMeshOnEntity->myCellsSize += aNbElem*(aVNbNodes+1);
            
            INITMSG(MYDEBUG,
                    "- aMGeom = "<<aMGeom<<
                    "; aNbElem = "<<aNbElem<<
                    "; myNbCells = "<<aMeshOnEntity->myNbCells<<
                    "; myCellsSize = "<<aMeshOnEntity->myCellsSize<<
                    endl);
            
            for(TInt iElem = 0; iElem < aNbElem; iElem++){
              TInt aFamId = aCellInfo->GetFamNum(iElem);
              if(aFamId != 0)
                aFamilyID2CellsSize[aFamId] += aVNbNodes + 1;
              anElemID2FamilyID[iElem] = aFamId;
            }
          }} // end switch(...)
        }
      }
    }
  }

  //---------------------------------------------------------------
  void
  BuildMeshGrilleOnEntityMap(VISU::PMEDMesh theMesh,
                             const MED::TEntityInfo& theEntityInfo,
                             const MED::PGrilleInfo& theGrilleInfo,
                             const MED::PWrapper& theMEDWrapper)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildMeshGrilleOnEntityMap");
    INITMSG(MYDEBUG,"BuildMeshGrilleOnEntityMap"<<endl);
    
    MED::PMeshInfo aMeshInfo = theMesh->myMeshInfo;
    const std::string& aMeshName = theMesh->myName;
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;
    MED::TEntityInfo::const_iterator anEntityIter = theEntityInfo.begin();
    
    VISU::TStructuredId& aGrilleStructure = theMesh->myGrilleStructure;
    if ( theGrilleInfo->GetGrilleType() != MED::eGRILLE_STANDARD )
      for ( int aDimId = 0; aDimId < theMesh->myDim; aDimId++ )
        aGrilleStructure[aDimId] = theGrilleInfo->GetNbIndexes(aDimId);
    else {
      MED::TIntVector aVector = theGrilleInfo->GetGrilleStructure();
      for ( int aDimId = 0; aDimId < aVector.size(); aDimId++ )
        aGrilleStructure[aDimId] = aVector[aDimId];
    }

    int kMax = aGrilleStructure[2];
    int jMax = aGrilleStructure[1];
    int iMax = aGrilleStructure[0];
    
    int iii = 0;
    VISU::TObj2StructuredId& aObj2StructuredId = theMesh->myObj2StructuredId;
    switch ( theMesh->myDim ) {
    case 1:
      for ( int i = 1; i <= iMax; i++ )
        aObj2StructuredId[i-1][0] = i;
      break;
    case 2:
      for ( int j = 1; j <= jMax; j++ )
        for ( int i = 1; i <= iMax; i++ ) {
          aObj2StructuredId[iii][0] = i;
          aObj2StructuredId[iii][1] = j;
          iii++;
        }
      break;
    case 3:
      for ( int k = 1; k <= kMax; k++ )
        for ( int j = 1; j <= jMax; j++ )
          for ( int i = 1; i <= iMax; i++ ) {
            aObj2StructuredId[iii][0] = i;
            aObj2StructuredId[iii][1] = j;
            aObj2StructuredId[iii][2] = k;
            iii++;
          }
    }
    
    for(; anEntityIter != theEntityInfo.end(); anEntityIter++){
      const MED::EEntiteMaillage& aMEntity = anEntityIter->first;
      const MED::TGeom2Size& aGeom2Size = anEntityIter->second;
      
      VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
      VISU::PMEDMeshOnEntity aMeshOnEntity = 
        aMeshOnEntityMap[aVEntity](new VISU::TMEDMeshOnEntity());
      aMeshOnEntity->myEntity = aVEntity;
      aMeshOnEntity->myMeshName = aMeshName;
      aMeshOnEntity->myGeom2Size = aGeom2Size;
      VISU::TFamilyID2CellsSize& aFamilyID2CellsSize = aMeshOnEntity->myFamilyID2CellsSize;
      VISU::TGeom2ElemID2FamilyID& aGeom2ElemID2FamilyID = aMeshOnEntity->myGeom2ElemID2FamilyID;

      INITMSG(MYDEBUG,
              "- aMEntity = "<<aMEntity<<
              "; aVEntity = "<<aVEntity<<
              endl);

      if(aMEntity == MED::eNOEUD){
        aMeshOnEntity->myNbCells = theGrilleInfo->GetNbNodes();
        aMeshOnEntity->myCellsSize = 2*theGrilleInfo->GetNbNodes();

        VISU::TElemID2FamilyID& anElemID2FamilyID = aGeom2ElemID2FamilyID[MED::ePOINT1];

        if((theGrilleInfo->myFamNumNode).size() > 0)
          for(TInt iElem = 0; iElem < theGrilleInfo->GetNbNodes(); iElem++){
            TInt aFamId = theGrilleInfo->GetFamNumNode(iElem);
            if(aFamId != 0)
              aFamilyID2CellsSize[aFamId] += 2;
            anElemID2FamilyID[iElem] = aFamId;
          }

        INITMSG(MYDEBUG,
                "- myNbCells = "<<aMeshOnEntity->myNbCells<<
                "; myCellsSize = "<<aMeshOnEntity->myCellsSize<<
                endl);;

      } else {
        MED::TGeom2Size::const_iterator aGeom2SizeIter = aGeom2Size.begin();
        aMeshOnEntity->myNbCells = 0;
        aMeshOnEntity->myCellsSize = 0;
        for(; aGeom2SizeIter != aGeom2Size.end(); aGeom2SizeIter++){
          const MED::EGeometrieElement& aMGeom = aGeom2SizeIter->first;
          VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
          vtkIdType aVNbNodes = VISUGeom2NbNodes(aEGeom);
          TInt aNbElem = aGeom2SizeIter->second;//theGrilleInfo->GetNbCells();
          aMeshOnEntity->myNbCells += aNbElem;
          aMeshOnEntity->myCellsSize += aNbElem*(aVNbNodes+1);

          VISU::TElemID2FamilyID& anElemID2FamilyID = aGeom2ElemID2FamilyID[aMGeom];

          INITMSG(MYDEBUG,
                  "- aMGeom = "<<aMGeom<<
                  "; aNbElem = "<<aNbElem<<
                  "; myNbCells = "<<aMeshOnEntity->myNbCells<<
                  "; myCellsSize = "<<aMeshOnEntity->myCellsSize<<
                  endl);

          MED::TElemNum& famNums =
            (aMEntity == MED::eMAILLE) ? theGrilleInfo->myFamNum : theGrilleInfo->myFamSubNum;
          if (famNums.size() == aNbElem)
            for(TInt iElem = 0; iElem < aNbElem; iElem++){
              TInt aFamId = famNums[iElem];
              if(aFamId != 0)
                aFamilyID2CellsSize[aFamId] += aVNbNodes + 1;
              anElemID2FamilyID[iElem] = aFamId;
            }
        }
      }
    }
  }


  //---------------------------------------------------------------
  void
  BuildFieldMap(VISU::PMEDMesh theMesh,
                const MED::TEntityInfo& theEntityInfo,
                MED::PWrapper theMEDWrapper)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildFieldMap");
    TInt aNbFields = theMEDWrapper->GetNbFields(); 
    MED::PMeshInfo aMeshInfo = theMesh->myMeshInfo;
    const std::string& aMeshName = theMesh->myName;
    INITMSG(MYDEBUG,"BuildFieldMap: aNbFields = "<<aNbFields<<"\n");
    for(TInt iField = 1; iField <= aNbFields; iField++){
      VISU::TTimerLog aTimerLog(MYDEBUG,"GetPFieldInfo");
      
      MED::TErr anError = 0;
      MED::PFieldInfo aFieldInfo = theMEDWrapper->GetPFieldInfo(aMeshInfo, iField, &anError);
      if ( anError < 0 )
        continue;

      TInt aNbComp = aFieldInfo->GetNbComp();
      std::string aFieldName = aFieldInfo->GetName();
      
      MED::TGeom2Size aGeom2Size;
      MED::EEntiteMaillage aMEntity;
      TInt aNbTimeStamps = theMEDWrapper->GetNbTimeStamps(aFieldInfo,
                                                          theEntityInfo,
                                                          aMEntity,
                                                          aGeom2Size);
      if(aNbTimeStamps < 1)
        continue;
      
      VISU::TEntity aVEntity = MEDEntityToVTK( aMEntity );
      vtkIdType aDataType = VTK_DOUBLE;
      if ( aFieldInfo->GetType() != MED::eFLOAT64 ) {
#if defined(HAVE_F77INT64)
        aDataType = VTK_LONG;
#else   
        aDataType = VTK_INT;
#endif
      }
      VISU::PMEDMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[aVEntity];
      VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
      VISU::PMEDField aField = aFieldMap[aFieldName](new VISU::TMEDField());
      aField->myId = iField;
      aField->Init(aNbComp, aDataType);
      aField->myEntity = aVEntity;
      aField->myName = aFieldName;
      aField->myMeshName = aMeshName;
      aField->myDataSize = aMeshOnEntity->myNbCells * aNbComp;
      aField->myIsELNO = ( aMEntity == MED::eNOEUD_ELEMENT );

      INITMSG(MYDEBUG,"myName = '"<<aField->myName<<"'"<<
              "; myId = "<<aField->myId<<
              "; myEntity = "<<aField->myEntity<<
              "; myDataSize = "<<aField->myDataSize<<
              "; myNbComp = "<<aField->myNbComp<<"\n");
      
      for(TInt iComp = 0; iComp < aNbComp; iComp++){
        aField->myCompNames[iComp] = aFieldInfo->GetCompName(iComp);
        aField->myUnitNames[iComp] = aFieldInfo->GetUnitName(iComp);
      }
      
      for(TInt iTimeStamp = 1; iTimeStamp <= aNbTimeStamps; iTimeStamp++){
        VISU::TTimerLog aTimerLog(MYDEBUG,"GetPTimeStampInfo");

        MED::TErr anError = 0;
        MED::PTimeStampInfo aTimeStampInfo = theMEDWrapper->GetPTimeStampInfo(aFieldInfo,
                                                                              aMEntity,
                                                                              aGeom2Size,
                                                                              iTimeStamp,
                                                                              &anError);
        if ( anError < 0 )
          continue;

        TFloat aDt = aTimeStampInfo->GetDt();
        std::string anUnitDt = aTimeStampInfo->GetUnitDt();

        VISU::TValField& aValField = aField->myValField;
        VISU::PMEDValForTime aValForTime = aValField[iTimeStamp](new VISU::TMEDValForTime());
        aValForTime->myId = iTimeStamp;
        aValForTime->myFieldName = aField->myName;
        aValForTime->myEntity = aField->myEntity;
        aValForTime->myMeshName = aField->myMeshName;
        aValForTime->myTime = VISU::TTime(aDt,anUnitDt);
        INITMSG(MYDEBUG,"aDt = '"<<aDt<<", "<<anUnitDt<<"'\n");
        
        VISU::TGeom2NbGauss& aVGeom2NbGauss = aValForTime->myGeom2NbGauss;
        const MED::TGeom2NbGauss& aMGeom2NbGauss = aTimeStampInfo->myGeom2NbGauss;
        MED::TGeom2NbGauss::const_iterator anIter = aMGeom2NbGauss.begin();
        for(; anIter != aMGeom2NbGauss.end(); anIter++){
          const MED::EGeometrieElement& aMGeom = anIter->first;
          VISU::EGeometry aEGeom = MEDGeom2VISU( aMGeom );
          TInt aNbGauss = anIter->second;
          aVGeom2NbGauss[ aEGeom ] = aNbGauss;

          // ELNO data should satisfy the following condition ( implicitly )
          vtkIdType aNbNodes = MEDGeom2NbNodes( aMGeom );
          aField->myIsELNO &= ( aNbGauss == aNbNodes );
        }
      }
    }
  }


  //---------------------------------------------------------------
  void
  BuildFamilyMap(VISU::PMEDMesh theMesh,
                 const MED::TEntityInfo& theEntityInfo,
                 const MED::TEntity2TGeom2ElemInfo& theEntity2TGeom2ElemInfo,
                 const MED::TFamilyInfoSet& theFamilyInfoSet,
                 MED::PWrapper theMEDWrapper)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildFamilyMap");
    INITMSG(MYDEBUG,"BuildFamilyMap\n");

    MED::PMeshInfo aMeshInfo = theMesh->myMeshInfo;
    MED::TEntity2FamilySet aEntity2FamilySet = MED::GetEntity2FamilySet(theMEDWrapper,theEntity2TGeom2ElemInfo,theFamilyInfoSet);
    MED::TEntity2FamilySet::const_iterator aEntity2FamilySetIter = aEntity2FamilySet.begin();
    for(; aEntity2FamilySetIter != aEntity2FamilySet.end(); aEntity2FamilySetIter++){
      const MED::EEntiteMaillage& aMEntity = aEntity2FamilySetIter->first;
      const MED::TFamilyTSizeSet& aFamilyTSizeSet = aEntity2FamilySetIter->second;
      
      VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
      VISU::PMEDMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[aVEntity];
      const VISU::TFamilyID2CellsSize& aFamilyID2CellsSize = aMeshOnEntity->myFamilyID2CellsSize;
      VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
      VISU::TFamilyIDMap& aFamilyIDMap = aMeshOnEntity->myFamilyIDMap;
        
      if(aFamilyTSizeSet.empty())
        continue;
        
      INITMSG(MY_FAMILY_DEBUG,
              "- aMEntity = "<<aMEntity<<
              "; aVEntity = "<<aVEntity<<
              endl);

      MED::TFamilyTSizeSet::const_iterator aFamilyTSizeSetIter = aFamilyTSizeSet.begin();
      for(; aFamilyTSizeSetIter != aFamilyTSizeSet.end(); aFamilyTSizeSetIter++){
        const MED::TFamilyTSize& aFamilyTSize = *aFamilyTSizeSetIter;
        const MED::PFamilyInfo& aFamilyInfo = boost::get<0>(aFamilyTSize);
        TInt aSize = boost::get<1>(aFamilyTSize);
        TInt anId = aFamilyInfo->GetId();
        if(anId == 0) 
          continue;
        
        std::string aFamilyName = aFamilyInfo->GetName();

        VISU::PMEDFamily aFamily = aFamilyMap[aFamilyName](new VISU::TMEDFamily());
        aFamily->myId = anId;
        aFamily->myEntity = aVEntity;
        aFamily->myName = aFamilyName;
        aFamily->myNbCells = aSize;

        aFamily->myCellsSize = 0;
        VISU::TFamilyID2CellsSize::const_iterator anIter = aFamilyID2CellsSize.find(anId);
        if(anIter != aFamilyID2CellsSize.end())
          aFamily->myCellsSize = anIter->second;

        const TInt aNbGroup = aFamilyInfo->GetNbGroup();
        VISU::TNames& aGroupNames = aFamily->myGroupNames;
        aGroupNames.resize(aNbGroup);
        for(TInt i = 0; i < aNbGroup; i++){
          std::string aGroupName = aFamilyInfo->GetGroupName(i);
          aGroupNames[i] = aGroupName;
        }

        aFamilyIDMap[anId] = aFamily;

        INITMSG(MY_FAMILY_DEBUG,
                "- aFamilyName = '"<<aFamilyName<<"'"<<
                "; myId = "<<aFamily->myId<<"; "<<
                "; aNbAttr = "<<aFamilyInfo->GetNbAttr()<<
                "; aNbGroup = "<<aFamilyInfo->GetNbGroup()<<
                "; aVEntity = "<<aVEntity<<
                "; myNbCells = "<<aFamily->myNbCells<<
                "; myCellsSize = "<<aFamily->myCellsSize<<
                endl);
#ifdef _DEBUG_  
        for(TInt i = 0; i < aNbGroup; i++){
          std::string aGroupName = aFamilyInfo->GetGroupName(i);
          INITMSG(MY_FAMILY_DEBUG,"- aGroupName = '"<<aGroupName<<"'\n");
        }
#endif

      }
    }
  }

  
  /*!
   * Build grille family map
   */
  void
  BuildGrilleFamilyMap(VISU::PMEDMesh theMesh,
                       const MED::TEntityInfo& theEntityInfo,
                       const MED::TFamilyInfoSet& theFamilyInfoSet,
                       MED::PWrapper theMEDWrapper)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildGrilleFamilyMap");
    INITMSG(MYDEBUG,"BuildGrilleFamilyMap\n");
    
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;

    MED::TEntityInfo::const_iterator aEntityIter = theEntityInfo.begin();

    const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;
    MED::PGrilleInfo aGrilleInfo = theMEDWrapper->GetPGrilleInfo(aMeshInfo);

    const MED::TFamilyID2NbCells& aFam2NbCells = MED::GetFamilyID2NbCells(aGrilleInfo);

    MED::TFamilyInfoSet::const_iterator aFamInter = theFamilyInfoSet.begin();
    for(; aFamInter != theFamilyInfoSet.end(); aFamInter++){
      const MED::PFamilyInfo& aFamilyInfo = *aFamInter;
      TInt anId = aFamilyInfo->GetId();

      if(anId == 0)
        continue;

      std::string aFamilyName = aFamilyInfo->GetName();
      const MED::EEntiteMaillage& aMEntity = MED::GetEntityByFamilyId(aGrilleInfo,
                                                                      anId);
      VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);

      VISU::PMEDMeshOnEntity aMeshOnEntity;
      VISU::TMeshOnEntityMap::iterator aMeshOnEntityIter = aMeshOnEntityMap.find(aVEntity);
      if(aMeshOnEntityIter != aMeshOnEntityMap.end())
        aMeshOnEntity = aMeshOnEntityIter->second;

      VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
      VISU::TFamilyIDMap& aFamilyIDMap = aMeshOnEntity->myFamilyIDMap;

      VISU::PMEDFamily aFamily = aFamilyMap[aFamilyName](new VISU::TMEDFamily());
      aFamily->myId = anId;
      aFamily->myEntity = aVEntity;
      aFamily->myName = aFamilyName;
      aFamily->myNbCells = 0;
      aFamily->myCellsSize = 0;

      const VISU::TFamilyID2CellsSize& aFamilyID2CellsSize = aMeshOnEntity->myFamilyID2CellsSize;
      VISU::TFamilyID2CellsSize::const_iterator aFamilyid2CellsSizeIter = aFamilyID2CellsSize.find(anId);
      if(aFamilyid2CellsSizeIter != (aMeshOnEntity->myFamilyID2CellsSize).end())
        aFamily->myCellsSize = aFamilyid2CellsSizeIter->second;
      MED::TFamilyID2NbCells::const_iterator aFam2NbCellsIter = aFam2NbCells.find(anId);
      if(aFam2NbCellsIter != aFam2NbCells.end())
        aFamily->myNbCells = aFam2NbCellsIter->second;

      const TInt aNbGroup = aFamilyInfo->GetNbGroup();
      VISU::TNames& aGroupNames = aFamily->myGroupNames;
      aGroupNames.resize(aNbGroup);
      for(TInt i = 0; i < aNbGroup; i++){
        std::string aGroupName = aFamilyInfo->GetGroupName(i);
        aGroupNames[i] = aGroupName;
      }

      aFamilyIDMap[anId] = aFamily;

      INITMSG(MY_FAMILY_DEBUG,
              "- aFamilyName =|"<<aFamily->myName<<"|"
              << "; myId = "<<aFamily->myId
              << "; aNbAttr = "<<aFamilyInfo->GetNbAttr()
              << "; aNbGroup = "<<aFamilyInfo->GetNbGroup()
              << "; aVEntity = "<<aVEntity
              << "; myNbCells = "<<aFamily->myNbCells
              << "; myCellsSize = "<<aFamily->myCellsSize
              <<endl);


    }
  }

  //---------------------------------------------------------------
  void
  BuildGroupMap(VISU::PMEDMesh theMesh,
                const MED::TFamilyInfoSet& theFamilyInfoSet)
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildGroupMap");
    INITMSG(MYDEBUG,"BuildGroupMap\n");

    VISU::TGroupMap& aGroupMap = theMesh->myGroupMap;
    MED::TGroupInfo aGroupInfo = MED::GetGroupInfo(theFamilyInfoSet);
    MED::TGroupInfo::const_iterator aGroupInfoIter = aGroupInfo.begin();
    for(; aGroupInfoIter != aGroupInfo.end(); aGroupInfoIter++){
      const std::string& aGroupName = aGroupInfoIter->first;
      INITMSG(MY_GROUP_DEBUG,"aGroupName = '"<<aGroupName<<"'\n");

      VISU::PMEDGroup aGroup(new VISU::TMEDGroup());
      VISU::TFamilySet& aFamilySet = aGroup->myFamilySet;

      const MED::TFamilyInfoSet& aFamilyInfoSet = aGroupInfoIter->second;
      MED::TFamilyInfoSet::const_iterator aFamilyIter = aFamilyInfoSet.begin();
      for(; aFamilyIter != aFamilyInfoSet.end(); aFamilyIter++){
        const MED::PFamilyInfo& aFamilyInfo = *aFamilyIter;
        std::string aFamilyName = aFamilyInfo->GetName();
        
        VISU::TEntity aVEntity = VISU::TEntity(-1);
        VISU::PMEDFamily aFamily;
        
        // Find aVisuEntity
        const VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;
        VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityIter = aMeshOnEntityMap.begin();
        for(; aMeshOnEntityIter != aMeshOnEntityMap.end(); aMeshOnEntityIter++){
          const VISU::PMeshOnEntity& aMeshOnEntity = aMeshOnEntityIter->second;
          const VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
          VISU::TFamilyMap::const_iterator aFamilyMapIter = aFamilyMap.begin();
          for(; aFamilyMapIter != aFamilyMap.end(); aFamilyMapIter++){
            const std::string& aName = aFamilyMapIter->first;
            if(aName == aFamilyName){
              aFamily = aFamilyMapIter->second;
              aVEntity = aFamily->myEntity;
              if(aFamily && aVEntity >= 0){
                aFamilySet.insert(VISU::TEnity2Family(aVEntity, aFamily));
                INITMSG(MY_GROUP_DEBUG,
                        "- aFamilyName = '"<<aFamilyName<<"'"<<
                        "; aVEntity = "<<aVEntity<<
                        "\n");
              }
            }
          }
        } 
      }
      if(!aFamilySet.empty())
        aGroupMap.insert(VISU::TGroupMap::value_type(aGroupName,aGroup));
    }
  }


  //---------------------------------------------------------------
  struct TSetIsDone
  {
    bool& myIsDone;
    TSetIsDone(bool& theIsDone):
      myIsDone(theIsDone)
    {}

    ~TSetIsDone()
    {
      myIsDone = true;
    }

  };


  //---------------------------------------------------------------
}

namespace VISU
{
  //---------------------------------------------------------------
  void
  TMEDNamedPointCoords
  ::Init(const MED::PNodeInfo& theNodeInfo,
         MED::EVersion theVersion)
  {
    TMEDCoordHolder* aCoordHolder = new TMEDCoordHolder();
    TInt aNbElem = theNodeInfo->GetNbElem();
    TInt aDim = theNodeInfo->GetMeshInfo()->GetSpaceDim();
    aCoordHolder->Init(aNbElem, aDim, theNodeInfo->myCoord);
    TNamedPointCoords::Init(PCoordHolder(aCoordHolder));
    myVersion = theVersion;

    for(TInt iDim = 0; iDim < aDim; iDim++)
      myPointsDim[iDim] = theNodeInfo->GetCoordName(iDim);
      
    myIsElemNum = theNodeInfo->IsElemNum();
    if(theNodeInfo->IsElemNum())
      myElemNum = theNodeInfo->myElemNum;

    myIsElemNames = theNodeInfo->IsElemNames();
    if(theNodeInfo->IsElemNames())
      myElemNames = theNodeInfo->myElemNames;
  }

  void
  TMEDNamedPointCoords
  ::Init(const MED::PGrilleInfo& theGrilleInfo)
  {
    TMEDCoordHolder* aCoordHolder = new TMEDCoordHolder();
    TInt aNbElem = theGrilleInfo->GetNbNodes();
    TInt aDim = theGrilleInfo->GetMeshInfo()->GetSpaceDim();
    MED::PNodeCoord aCoord(new MED::TNodeCoord(aNbElem * aDim));
    aCoordHolder->Init(aNbElem, aDim, aCoord);
    TNamedPointCoords::Init(PCoordHolder(aCoordHolder));

    for(TInt iDim = 0; iDim < aDim; iDim++)
      myPointsDim[iDim] = theGrilleInfo->GetCoordName(iDim);

    for(TInt iElem = 0; iElem < aNbElem; iElem++){
      VISU::TCoordSlice aVCoordSlice = GetCoordSlice(iElem);
      MED::TNodeCoord aMCoord = theGrilleInfo->GetCoord(iElem);
      for(TInt iDim = 0; iDim < aDim; iDim++){
        aVCoordSlice[iDim] = aMCoord[iDim];
      }
    }
  }

  vtkIdType
  TMEDNamedPointCoords
  ::GetObjID(vtkIdType theID) const
  {
    if(myIsElemNum)
      return (*myElemNum)[theID];
    return TNamedPointCoords::GetObjID(theID);
  }


  vtkIdType
  TMEDNamedPointCoords
  ::GetVTKID(vtkIdType theID) const
  {
    if(myIsElemNum){
      // To prepare corresponding mapper engine
      if(myObj2VTKID.empty()){
        vtkIdType anEnd = myElemNum->size();
        for(vtkIdType anID = 0; anID < anEnd; anID++)
          myObj2VTKID[(*myElemNum)[anID]] = anID;
      }
      TObj2VTKID::const_iterator anIter = myObj2VTKID.find(theID);
      if(anIter != myObj2VTKID.end())
        return anIter->second;
      return -1;
    }
    return TNamedPointCoords::GetVTKID(theID);
  }

  MED::TInt
  GetPNOMLength(MED::EVersion theVersion)
  {
    //if(theVersion == MED::eV2_1)
    //  return MED::GetPNOMLength<MED::eV2_1>();
    return MED::GetPNOMLength<MED::eV2_2>();
  }

  std::string 
  TMEDNamedPointCoords
  ::GetNodeName(vtkIdType theObjID) const
  {
    if(myIsElemNames)
      return GetString(theObjID, GetPNOMLength(myVersion), *myElemNames);
    return TNamedPointCoords::GetNodeName(theObjID);
  }

  unsigned long int
  TMEDNamedPointCoords
  ::GetMemorySize()
  {
    size_t aSize = TNamedPointCoords::GetMemorySize();

    if(myElemNum){
      aSize += myObj2VTKID.size() * sizeof(vtkIdType) * 2;
      aSize += myElemNum->size() * sizeof(MED::TInt);
    }

    if(myIsElemNames)
      aSize += myElemNames->size() * sizeof(char);
    return aSize;
  }


  //---------------------------------------------------------------
  vtkIdType
  TMEDSubProfile
  ::GetElemObjID(vtkIdType theID) const
  {
    if ( !mySubMeshID.empty() )
      theID = mySubMeshID[theID];

    if ( myIsElemNum )
      return (*myElemNum)[theID];
    else
      return theID;
  }
  

  //---------------------------------------------------------------
  vtkIdType
  TMEDSubProfile
  ::GetElemVTKID(vtkIdType theID) const
  {
    if ( myIsElemNum )
      for ( size_t anId = 0; anId < (*myElemNum).size(); anId++ ) 
        if ( (*myElemNum)[ anId ] == theID ) {
          theID = anId;
          break;
        }

    return TSubProfileImpl::GetElemVTKID( theID );
  }


  //----------------------------------------------------------------
  unsigned long int
  TMEDSubProfile
  ::GetMemorySize()
  {
    size_t aSize = TSubProfileImpl::GetMemorySize();
    if(myIsElemNum)
      aSize += myElemNum->size() * sizeof(MED::TInt);
    return aSize;
  }


  //---------------------------------------------------------------
  void
  TMEDGauss
  ::LessThan(const PGaussImpl& theGauss,
             bool& theResult) const
  {
    TGaussImpl::LessThan(theGauss,theResult);
    if(myGaussInfo){
      if(PMEDGauss aGauss = theGauss){
        const MED::TGaussInfo& aLeft = myGaussInfo;
        const MED::TGaussInfo& aReight = aGauss->myGaussInfo;
        theResult = MED::TGaussInfo::TLess()(aLeft,aReight);
      }
    }
  }


  //---------------------------------------------------------------
  TGaussPointID
  TMEDGaussSubMesh
  ::GetObjID(vtkIdType theID) const
  {
    vtkIdType aNbPoints = myGauss->myNbPoints;
    TCellID aCellID = theID / aNbPoints;
    TLocalPntID aLocalPntID = theID % aNbPoints;

    if ( myIsElemNum )
      aCellID = GetElemObjID(aCellID);
    else
      aCellID += myStartID;

    return TGaussPointID(aCellID, aLocalPntID);
  }
  
  
  //---------------------------------------------------------------
  vtkIdType
  TMEDGaussSubMesh
  ::GetVTKID( const TGaussPointID& theID ) const
  {
    vtkIdType aResult = -1;

    TCellID aCellID = theID.first;
    TLocalPntID aLocalPntID = theID.second;
    
    vtkIdType aNbPoints = myGauss->myNbPoints;
    if ( aLocalPntID >= aNbPoints )
      return aResult;
    
    if ( myIsElemNum ) {
      aCellID = GetElemVTKID( aCellID );
    } else
      aCellID -= myStartID;

    return aCellID * aNbPoints + aLocalPntID + myStartID;
  }


  //---------------------------------------------------------------
  unsigned long int
  TMEDGaussSubMesh
  ::GetMemorySize()
  {
    size_t aSize = TGaussSubMeshImpl::GetMemorySize();
    if(myIsElemNum)
      aSize += myElemNum->size() * sizeof(MED::TInt);
    return aSize;
  }


  //---------------------------------------------------------------
  void
  TMEDSubMesh
  ::Init(const MED::PElemInfo& theElemInfo,
         MED::EVersion theVersion)
  {
    myVersion = theVersion;
    myIsElemNum = theElemInfo->IsElemNum();
    if(myIsElemNum)
      myElemNum = theElemInfo->myElemNum;

    myIsElemNames = theElemInfo->IsElemNames();
    if(theElemInfo->IsElemNames())
      myElemNames = theElemInfo->myElemNames;
  }


  void
  TMEDSubMesh
  ::Init(const MED::PGrilleInfo& theGrilleInfo)
  {}

  vtkIdType
  TMEDSubMesh
  ::GetElemObjID(vtkIdType theID) const
  {
    if(myIsElemNum)
      return (*myElemNum)[theID];
    else
      return TSubMeshImpl::GetElemObjID(theID);
  }

  std::string 
  TMEDSubMesh
  ::GetElemName(vtkIdType theObjID) const
  {
    if(myIsElemNames)
      return GetString(theObjID, GetPNOMLength(myVersion), *myElemNames);
    return TSubMeshImpl::GetElemName(theObjID);
  }

  unsigned long int
  TMEDSubMesh
  ::GetMemorySize()
  {
    size_t aSize = TSubMeshImpl::GetMemorySize();

    if(myIsElemNum)
      aSize += myElemNum->size() * sizeof(MED::TInt);

    if(myIsElemNames)
      aSize += myElemNames->size() * sizeof(char);

    return aSize;
  }


  //---------------------------------------------------------------
}


//---------------------------------------------------------------
extern "C"
VISU_Convertor* 
CreateConvertor(const std::string& theFileName)
{
  if(MED::PWrapper aMed = MED::CrWrapper(theFileName,true))
    return new VISU_MedConvertor(theFileName, aMed);
  return NULL;
}

VISU_MedConvertor
::VISU_MedConvertor(const std::string& theFileName, MED::PWrapper theMed):
  myIsEntitiesDone(false),
  myIsFieldsDone(false),
  myIsGroupsDone(false),
  myIsMinMaxDone(false),
  myMed(theMed)
{
  myName = theFileName;
}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_MedConvertor
::BuildEntities()
{
  if(myIsEntitiesDone)
    return this;

  TSetIsDone aSetIsDone(myIsEntitiesDone);
  VISU::TTimerLog aTimerLog(MYDEBUG,"BuildEntities");

  TInt aNbMeshes = myMed->GetNbMeshes();
  VISU::TMeshMap& aMeshMap = myMeshMap;

  bool isDescConn = false; // Mantis issue 0020483

  INITMSG(MYDEBUG,"BuildEntities aNbMeshes = "<<aNbMeshes<<"\n");

  for (TInt iMesh = 1; iMesh <= aNbMeshes; iMesh++) {
#ifndef _DEXCEPT_
    try{
#endif
      VISU::TTimerLog aTimerLog(MYDEBUG,"GetPMeshInfo");

      MED::PMeshInfo aMeshInfo = myMed->GetPMeshInfo(iMesh);
      std::string aMeshName = aMeshInfo->GetName();
      TInt aDim = aMeshInfo->GetDim();
      MED::EMaillage aType = aMeshInfo->GetType();
        
      // creating TMesh structure and TMeshOnEntityMap         
      VISU::PMEDMesh aMesh = aMeshMap[aMeshName](new VISU::TMEDMesh());
      
      aMesh->myDim = aDim;
      aMesh->myName = aMeshName;
      aMesh->myMeshInfo = aMeshInfo;
      aMesh->myNamedPointCoords(new VISU::TMEDNamedPointCoords());

      INITMSG(MYDEBUG,"aMeshName = '"<<aMeshName<<
              "; aDim = "<<aDim<<"\n");

      if (aType == MED::eNON_STRUCTURE) {

        if (MED::PNodeInfo aNodeInfo = myMed->GetPNodeInfo(aMeshInfo)) {
          MED::TEntityInfo anEntityInfo = myMed->GetEntityInfo(aMeshInfo, MED::eNOD);

          aMesh->myNbPoints = aNodeInfo->GetNbElem();
          aMesh->myEntityInfo = anEntityInfo;

          // Check connectivity (Mantis issue 0020483)
          if (!isDescConn) {
            NCollection_Map<MED::EEntiteMaillage> aDescendingEntitiesMap;

            MED::TEntityInfo aEntityInfoDesc = myMed->GetEntityInfo(aMeshInfo, MED::eDESC);
            MED::TEntityInfo::iterator anEntityIterDesc = aEntityInfoDesc.begin();
            //for (; anEntityIterDesc != aEntityInfoDesc.end() && !isDescConn; anEntityIterDesc++) {
            for (; anEntityIterDesc != aEntityInfoDesc.end(); anEntityIterDesc++) {
              const MED::EEntiteMaillage& anEntity = anEntityIterDesc->first;
              aDescendingEntitiesMap.Add(anEntity);
              //if (anEntity != MED::eNOEUD) isDescConn = true;
            }
            MED::TEntityInfo aEntityInfo = myMed->GetEntityInfo(aMeshInfo, MED::eNOD);
            MED::TEntityInfo::iterator anEntityIter = aEntityInfo.begin();
            for (; anEntityIter != aEntityInfo.end(); anEntityIter++) {
              const MED::EEntiteMaillage& anEntity = anEntityIter->first;
              aDescendingEntitiesMap.Remove(anEntity);
            }

            if (aDescendingEntitiesMap.Extent()) isDescConn = true;
          }

#ifndef _DEXCEPT_
          try{
#endif
            INITMSG(MYDEBUG,"myNbPoints = "<<aMesh->myNbPoints<<
                    "; anEntityInfo.size() = "<<anEntityInfo.size()<<
                    "\n");

            BuildMeshOnEntityMap(aMesh,
                                 anEntityInfo,
                                 aNodeInfo,
                                 myMed);

#ifndef _DEXCEPT_
          }catch(std::exception& exc){
            MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
          }catch(...){
            MSG(MYDEBUG,"Unknown exception !!!");
          }
#endif
        }
      } // NON STRUCTURED MESH
      else {
        MED::PGrilleInfo aGrilleInfo = myMed->GetPGrilleInfo(aMeshInfo);

        MED::TEntityInfo anEntityInfo;
        anEntityInfo[MED::eNOEUD][MED::ePOINT1] = aGrilleInfo->GetNbNodes();
        anEntityInfo[aGrilleInfo->GetEntity()][aGrilleInfo->GetGeom()] = aGrilleInfo->GetNbCells();
        anEntityInfo[aGrilleInfo->GetSubEntity()][aGrilleInfo->GetSubGeom()] = aGrilleInfo->GetNbSubCells();
        
        aMesh->myNbPoints   = aGrilleInfo->GetNbNodes();
        aMesh->myEntityInfo = anEntityInfo;

        aMesh->myIsPolarType = (MED::eGRILLE_POLAIRE == aGrilleInfo->GetGrilleType());
        aMesh->myIsStructured = true;
#ifndef _DEXCEPT_
        try{
#endif
          BEGMSG(MYDEBUG,"anEntityInfo.size() = "<<anEntityInfo.size()<<"\n");
          
          BuildMeshGrilleOnEntityMap(aMesh,
                                     anEntityInfo,
                                     aGrilleInfo,
                                     myMed);
          
#ifndef _DEXCEPT_
        }catch(std::exception& exc){
          MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
        }catch(...){
          MSG(MYDEBUG,"Unknown exception !!!");
        }
#endif

      }

#ifndef _DEXCEPT_
    }catch(std::exception& exc){
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
    }catch(...){
      MSG(MYDEBUG,"Unknown exception !!!");
    }
#endif
  }

  // Mantis issue 0020483
  if (isDescConn)
    INFOS("There are some elements in descending connectivity in med file. They were not read !!!");

  return this; 
}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_MedConvertor
::BuildFields()
{
  if(myIsFieldsDone)
    return this;

  VISU_Convertor_impl::BuildFields();

  TSetIsDone aSetIsDone(myIsFieldsDone);
  VISU::TTimerLog aTimerLog(MYDEBUG,"BuildFields");

  TInt aNbMeshes = myMed->GetNbMeshes();
  VISU::TMeshMap& aMeshMap = myMeshMap;

  INITMSG(MYDEBUG,"BuildFields - aNbMeshes = "<<aNbMeshes<<"\n");

  for(TInt iMesh = 1; iMesh <= aNbMeshes; iMesh++){
#ifndef _DEXCEPT_
    try{
#endif
      VISU::TTimerLog aTimerLog(MYDEBUG,"GetPMeshInfo");

      MED::PMeshInfo aMeshInfo = myMed->GetPMeshInfo(iMesh);
      std::string aMeshName = aMeshInfo->GetName();
      
      VISU::TMeshMap::const_iterator anIter = aMeshMap.find(aMeshName);
      if(anIter == aMeshMap.end())
        continue;
      VISU::PMEDMesh aMesh = anIter->second;

      INITMSG(MYDEBUG,"aMeshName = '"<<aMeshName<<"'\n");
#ifndef _DEXCEPT_
      try{
#endif
        MED::TEntityInfo anEntityInfo = myMed->GetEntityInfo(aMeshInfo);

        BuildFieldMap(aMesh,
                      anEntityInfo,
                      myMed);
#ifndef _DEXCEPT_
      }catch(std::exception& exc){
        MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
      }catch(...){
        MSG(MYDEBUG,"Unknown exception !!!");
      }
#endif

#ifndef _DEXCEPT_
    }catch(std::exception& exc){
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
    }catch(...){
      MSG(MYDEBUG,"Unknown exception !!!");
    }
#endif
  } 

  return this; 
}


//---------------------------------------------------------------
template<class TimeStampValueType>
void
BuildTimeStampMinMax(MED::SharedPtr<TimeStampValueType> theTimeStampValue,
                     const VISU::PMEDMeshOnEntity theMeshOnEntity,
                     const MED::TGeom2Gauss& theGeom2Gauss,
                     VISU::TMetric2Comp2MinMax& theMetric2Comp2MinMax,
                     VISU::TMetric2Comp2MinMax& theMetric2Comp2AverageMinMax,
                     VISU::TMetric2Comp2Group2MinMax& theMetric2Comp2Group2MinMax,
                     VISU::TMetric2Comp2Group2MinMax& theMetric2Comp2Group2AverageMinMax,
                     TInt theNbComp,
                     TInt theNbComp2)
{
  const VISU::TFamilyIDMap& aFamilyIDMap = theMeshOnEntity->myFamilyIDMap;
  VISU::TGeom2ElemID2FamilyID aGeom2ElemID2FamilyID = theMeshOnEntity->myGeom2ElemID2FamilyID;

  VISU::TVector<double> aBaseAverageValues;
  aBaseAverageValues.resize(3);
  aBaseAverageValues[VISU::AVERAGE_METRIC] = 0.0;
  aBaseAverageValues[VISU::MINIMUM_METRIC] = VTK_LARGE_FLOAT;
  aBaseAverageValues[VISU::MAXIMUM_METRIC] = -VTK_LARGE_FLOAT;

  const typename TimeStampValueType::TTGeom2Value& aGeom2Value = theTimeStampValue->myGeom2Value;
  typename TimeStampValueType::TTGeom2Value::const_iterator anIter = aGeom2Value.begin();
  for(; anIter != aGeom2Value.end(); anIter++){
    const typename TimeStampValueType::TTMeshValue& aMMeshValue = anIter->second;
    MED::EGeometrieElement aMGeom = anIter->first;
                  
    TInt aNbElem = aMMeshValue.myNbElem;
    TInt aNbGauss = aMMeshValue.myNbGauss;
                  
// AKL: fix of 0021175 issue. Non-regression test is GVIEW10561(import zzzz121b.med).
//     MED::TGeom2Gauss::const_iterator aGaussIter = theGeom2Gauss.find(aMGeom);
//     if(aGaussIter == theGeom2Gauss.end())
//       aNbGauss = 1;
                  
    INITMSG(MYDEBUG,
            "- aMGeom = "<<aMGeom<<
            "; aNbElem = "<<aNbElem<<
            "; aNbGauss = "<<aNbGauss<<
            endl);

    VISU::TElemID2FamilyID anElemID2FamilyID = aGeom2ElemID2FamilyID[aMGeom];
    
    bool isAverageByGaussPoints = (aNbGauss > 1);
    // To calculate min/max per components
    for(TInt iElem = 0; iElem < aNbElem; iElem++){
      typename TimeStampValueType::TTMeshValue::TCValueSliceArr aMValueSliceArr = aMMeshValue.GetCompValueSliceArr(iElem);

      for(int aGaussMetric = (int)VISU::AVERAGE_METRIC; aGaussMetric <= (int)VISU::MAXIMUM_METRIC; aGaussMetric++){
        VISU::TComp2MinMax& aComp2MinMax = theMetric2Comp2MinMax[aGaussMetric];
        VISU::TComp2MinMax& aComp2AverageMinMax = theMetric2Comp2AverageMinMax[aGaussMetric];
        VISU::TComp2Group2MinMax& aComp2Group2MinMax = theMetric2Comp2Group2MinMax[aGaussMetric];
        VISU::TComp2Group2MinMax& aComp2Group2AverageMinMax = theMetric2Comp2Group2AverageMinMax[aGaussMetric];

        for(TInt iComp = 0; iComp < theNbComp; iComp++){
          const typename TimeStampValueType::TTMeshValue::TCValueSlice& aMValueSlice = aMValueSliceArr[iComp];

          VISU::TMinMax& aMinMax = aComp2MinMax[iComp+1];
          double& aMin = aMinMax.first;
          double& aMax = aMinMax.second;
          VISU::TMinMax& anAverageMinMax = aComp2AverageMinMax[iComp+1];
          double& anAverageMin = anAverageMinMax.first;
          double& anAverageMax = anAverageMinMax.second;
          double anAverageValue = aBaseAverageValues[aGaussMetric];

          VISU::TGroup2MinMax& aGroup2MinMax = aComp2Group2MinMax[iComp+1];
          VISU::TGroup2MinMax& aGroup2AverageMinMax = aComp2Group2AverageMinMax[iComp+1];
          std::map<VISU::TName,double> aGroup2AverageValue;

          // get names of groups, to which the element belongs
          VISU::TNames aGroupNames;
          VISU::TElemID2FamilyID::const_iterator anIter = anElemID2FamilyID.find(iElem);
          if(anIter != anElemID2FamilyID.end()){
            int aFamilyId = anIter->second;
            if(aFamilyId != 0){
              VISU::TFamilyIDMap::const_iterator aFamilyIter = aFamilyIDMap.find(aFamilyId);
              if(aFamilyIter != aFamilyIDMap.end()){
                VISU::PMEDFamily aFamily = aFamilyIter->second;
                aGroupNames = aFamily->myGroupNames;
                VISU::TNames::const_iterator aGroupIter = aGroupNames.begin();
                for(; aGroupIter != aGroupNames.end(); aGroupIter++)
                  aGroup2AverageValue[*aGroupIter] = aBaseAverageValues[aGaussMetric];
              }
            }
          }

          for(TInt iGauss = 0; iGauss < aNbGauss; iGauss++){
            const double& aVal = aMValueSlice[iGauss];
            aMin = isnan(aVal) ? aVal : std::min(aMin,aVal);
            aMax = isnan(aVal) ? aVal : std::max(aMax,aVal);          
            if(isAverageByGaussPoints){
              switch(aGaussMetric) {
                case VISU::AVERAGE_METRIC: anAverageValue += aVal; break;
                case VISU::MINIMUM_METRIC: anAverageValue = std::min(anAverageValue, aVal); break;
                case VISU::MAXIMUM_METRIC: anAverageValue = std::max(anAverageValue, aVal); break;
              }
            }
            else {
              anAverageMin = aMin;
              anAverageMax = aMax;
            }

            // additional calculation for each group, to which the element belongs
            VISU::TNames::const_iterator aGroupIter = aGroupNames.begin();
            for(; aGroupIter != aGroupNames.end(); aGroupIter++){
              VISU::TName aGroupName = *aGroupIter;
              bool isGroupFound = aGroup2MinMax.find(aGroupName) != aGroup2MinMax.end();
              VISU::TMinMax& aGroupMinMax = aGroup2MinMax[aGroupName];
              double& aGroupMin = aGroupMinMax.first;
              double& aGroupMax = aGroupMinMax.second;
              aGroupMin = isGroupFound ? std::min(aGroupMin,aVal) : aVal;
              aGroupMax = isGroupFound ? std::max(aGroupMax,aVal) : aVal;          

              if(isAverageByGaussPoints){
                switch(aGaussMetric) {
                  case VISU::AVERAGE_METRIC: aGroup2AverageValue[aGroupName] = aGroup2AverageValue[aGroupName] + aVal; break;
                  case VISU::MINIMUM_METRIC: aGroup2AverageValue[aGroupName] = std::min(aGroup2AverageValue[aGroupName], aVal); break;
                  case VISU::MAXIMUM_METRIC: aGroup2AverageValue[aGroupName] = std::max(aGroup2AverageValue[aGroupName], aVal); break;
                }
              }
              else {
                VISU::TMinMax& aGroupAverageMinMax = aGroup2AverageMinMax[aGroupName];
                double& aGroupAverageMin = aGroupAverageMinMax.first;
                double& aGroupAverageMax = aGroupAverageMinMax.second;
                aGroupAverageMin = aGroupMin;
                aGroupAverageMax = aGroupMax;
              }
            }
          }
          if(isAverageByGaussPoints){
            if(aGaussMetric == VISU::AVERAGE_METRIC)
              anAverageValue /= aNbGauss;
            anAverageMin = std::min(anAverageMin,anAverageValue);
            anAverageMax = std::max(anAverageMax,anAverageValue);

            VISU::TNames::const_iterator aGroupIter = aGroupNames.begin();
            for(; aGroupIter != aGroupNames.end(); aGroupIter++){
              VISU::TName aGroupName = *aGroupIter;
              double aGroupAverageValue = aGroup2AverageValue[aGroupName];
              if(aGaussMetric == VISU::AVERAGE_METRIC)
                aGroupAverageValue /= aNbGauss;
              bool isGroupFound = aGroup2AverageMinMax.find(aGroupName) != aGroup2AverageMinMax.end();
              VISU::TMinMax& aGroupAverageMinMax = aGroup2AverageMinMax[aGroupName];
              double& aGroupAverageMin = aGroupAverageMinMax.first;
              double& aGroupAverageMax = aGroupAverageMinMax.second;
              aGroupAverageMin = isGroupFound ? std::min(aGroupAverageMin,aGroupAverageValue) : aGroupAverageValue;
              aGroupAverageMax = isGroupFound ? std::max(aGroupAverageMax,aGroupAverageValue) : aGroupAverageValue;
            }
          }
        }
      }
    }

    // To calculate min/max per vector modulus
    for(int aGaussMetric = (int)VISU::AVERAGE_METRIC; aGaussMetric <= (int)VISU::MAXIMUM_METRIC; aGaussMetric++){
      VISU::TComp2MinMax& aComp2MinMax = theMetric2Comp2MinMax[aGaussMetric];
      VISU::TComp2MinMax& aComp2AverageMinMax = theMetric2Comp2AverageMinMax[aGaussMetric];
      VISU::TComp2Group2MinMax& aComp2Group2MinMax = theMetric2Comp2Group2MinMax[aGaussMetric];
      VISU::TComp2Group2MinMax& aComp2Group2AverageMinMax = theMetric2Comp2Group2AverageMinMax[aGaussMetric];

      VISU::TMinMax& aMinMax = aComp2MinMax[0];
      double& aMin = aMinMax.first;
      double& aMax = aMinMax.second;
      VISU::TMinMax& anAverageMinMax = aComp2AverageMinMax[0];
      double& anAverageMin = anAverageMinMax.first;
      double& anAverageMax = anAverageMinMax.second;

      VISU::TGroup2MinMax& aGroup2MinMax = aComp2Group2MinMax[0];
      VISU::TGroup2MinMax& aGroup2AverageMinMax = aComp2Group2AverageMinMax[0];

      for(TInt iElem = 0; iElem < aNbElem; iElem++){
        typename TimeStampValueType::TTMeshValue::TCValueSliceArr aMValueSliceArr = aMMeshValue.GetGaussValueSliceArr(iElem);

        // get names of groups, to which the element belongs
        VISU::TNames aGroupNames;
        VISU::TElemID2FamilyID::const_iterator anIter = anElemID2FamilyID.find(iElem);
        if(anIter != anElemID2FamilyID.end()){
          int aFamilyId = anIter->second;
          if(aFamilyId != 0){
            VISU::TFamilyIDMap::const_iterator aFamilyIter = aFamilyIDMap.find(aFamilyId);
            if(aFamilyIter != aFamilyIDMap.end()){
              VISU::PMEDFamily aFamily = aFamilyIter->second;
              aGroupNames = aFamily->myGroupNames;
            }
          }
        }

        for(TInt iGauss = 0; iGauss < aNbGauss; iGauss++){
          const typename TimeStampValueType::TTMeshValue::TCValueSlice& aMValueSlice = aMValueSliceArr[iGauss];
          double aValue = 0.0;
          for(TInt iComp = 0; iComp < theNbComp2; iComp++){
            double aVal = aMValueSlice[iComp];
            aValue += aVal*aVal;
          }
          aValue = sqrt(aValue);
          aMin = isnan(aValue) ? aValue : std::min(aMin,aValue);
          aMax = isnan(aValue) ? aValue : std::max(aMax,aValue);

          // additional calculation for each group, to which the element belongs
          VISU::TNames::const_iterator aGroupIter = aGroupNames.begin();
          for(; aGroupIter != aGroupNames.end(); aGroupIter++){
            VISU::TName aGroupName = *aGroupIter;
            bool isGroupFound = aGroup2MinMax.find(aGroupName) != aGroup2MinMax.end();
            VISU::TMinMax& aGroupMinMax = aGroup2MinMax[aGroupName];
            double& aGroupMin = aGroupMinMax.first;
            double& aGroupMax = aGroupMinMax.second;
            aGroupMin = isGroupFound ? std::min(aGroupMin,aValue) : aValue;
            aGroupMax = isGroupFound ? std::max(aGroupMax,aValue) : aValue;
          }
        }
        if(isAverageByGaussPoints){
          typename TimeStampValueType::TTMeshValue::TCValueSliceArr aMCompValueSliceArr = aMMeshValue.GetCompValueSliceArr(iElem);
          double aValue = 0.0;
          for(TInt iComp = 0; iComp < theNbComp2; iComp++){
            const typename TimeStampValueType::TTMeshValue::TCValueSlice& aMCompValueSlice = aMCompValueSliceArr[iComp];
            double anAverageValue = aBaseAverageValues[aGaussMetric];
            for(TInt iGauss = 0; iGauss < aNbGauss; iGauss++){
              const double& aVal = aMCompValueSlice[iGauss];
              switch(aGaussMetric) {
                case VISU::AVERAGE_METRIC: anAverageValue += aVal; break;
                case VISU::MINIMUM_METRIC: anAverageValue = std::min(anAverageValue, aVal); break;
                case VISU::MAXIMUM_METRIC: anAverageValue = std::max(anAverageValue, aVal); break;
              }
            }
            if(aGaussMetric == VISU::AVERAGE_METRIC)
              anAverageValue /= aNbGauss;
            aValue += anAverageValue*anAverageValue;
          }
          aValue = sqrt(aValue);
          anAverageMin = std::min(anAverageMin,aValue);
          anAverageMax = std::max(anAverageMax,aValue);

          // additional calculation for each group, to which the element belongs
          VISU::TNames::const_iterator aGroupIter = aGroupNames.begin();
          for(; aGroupIter != aGroupNames.end(); aGroupIter++){
            VISU::TName aGroupName = *aGroupIter;
            bool isGroupFound = aGroup2AverageMinMax.find(aGroupName) != aGroup2AverageMinMax.end();
            VISU::TMinMax& aGroupAverageMinMax = aGroup2AverageMinMax[aGroupName];
            double& aGroupAverageMin = aGroupAverageMinMax.first;
            double& aGroupAverageMax = aGroupAverageMinMax.second;
            aGroupAverageMin = isGroupFound ? std::min(aGroupAverageMin,aValue) : aValue;
            aGroupAverageMax = isGroupFound ? std::max(aGroupAverageMax,aValue) : aValue;
          }
        }
        else {
          anAverageMin = aMin;
          anAverageMax = aMax;

          // additional calculation for each group, to which the element belongs
          VISU::TNames::const_iterator aGroupIter = aGroupNames.begin();
          for(; aGroupIter != aGroupNames.end(); aGroupIter++){
            VISU::TName aGroupName = *aGroupIter;
            VISU::TMinMax& aGroupAverageMinMax = aGroup2AverageMinMax[aGroupName];
            double& aGroupAverageMin = aGroupAverageMinMax.first;
            double& aGroupAverageMax = aGroupAverageMinMax.second;
            aGroupAverageMin = aGroup2MinMax[aGroupName].first;
            aGroupAverageMax = aGroup2MinMax[aGroupName].second;
          }
        }
      }
    }
  }
}

  
//---------------------------------------------------------------
VISU_Convertor* 
VISU_MedConvertor
::BuildMinMax()
{
  if(myIsMinMaxDone)
    return this;

  VISU_Convertor_impl::BuildMinMax();

  TSetIsDone aSetIsDone(myIsMinMaxDone);
  VISU::TTimerLog aTimerLog(MYDEBUG,"BuildMinMax");

  MED::TKey2Gauss aKey2Gauss = MED::GetKey2Gauss(myMed);
  MED::TMKey2Profile aMKey2Profile = MED::GetMKey2Profile(myMed);

  TInt aNbMeshes = myMed->GetNbMeshes();
  VISU::TMeshMap& aMeshMap = myMeshMap;

  INITMSG(MYDEBUG,"BuildMinMax - aNbMeshes = "<<aNbMeshes<<"\n");

  for(TInt iMesh = 1; iMesh <= aNbMeshes; iMesh++){
#ifndef _DEXCEPT_
    try{
#endif
      VISU::TTimerLog aTimerLog(MYDEBUG,"BuildMinMax - GetPMeshInfo");

      MED::PMeshInfo aMeshInfo = myMed->GetPMeshInfo(iMesh);
      std::string aMeshName = aMeshInfo->GetName();
      
      VISU::TMeshMap::const_iterator anIter = aMeshMap.find(aMeshName);
      if(anIter == aMeshMap.end())
        continue;
      VISU::PMEDMesh aMesh = anIter->second;

#ifndef _DEXCEPT_
      try{
#endif
        TInt aNbFields = myMed->GetNbFields(); 

        INITMSG(MYDEBUG,
                "- aMeshName = '"<<aMeshName<<"'"<<
                "; aNbFields = "<<aNbFields<<"\n");

        MED::TEntityInfo anEntityInfo = myMed->GetEntityInfo(aMeshInfo);

        for(TInt iField = 1; iField <= aNbFields; iField++)
        {
          VISU::TTimerLog aTimerLog(MYDEBUG,"BuildMinMax - GetPFieldInfo()");

          MED::TErr anError = 0;
          MED::PFieldInfo aFieldInfo = myMed->GetPFieldInfo(aMeshInfo, iField, &anError);
          if ( anError < 0 )
            continue;

          std::string aFieldName = aFieldInfo->GetName();
          INITMSG(MYDEBUG,"- aFieldName = '"<<aFieldName<<"'\n");

          MED::TGeom2Size aGeom2Size;
          MED::EEntiteMaillage aMEntity;
          TInt aNbTimeStamps = myMed->GetNbTimeStamps(aFieldInfo,
                                                     anEntityInfo,
                                                     aMEntity,
                                                     aGeom2Size);
          if(aNbTimeStamps < 1)
            continue;
        
          VISU::TEntity aVEntity = MEDEntityToVTK(aMEntity);
          VISU::PMEDMeshOnEntity aMeshOnEntity = aMesh->myMeshOnEntityMap[aVEntity];
          VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
          VISU::PMEDField aField = aFieldMap[aFieldName];
        
          TInt aNbComp = aField->myNbComp;
          int aNbComp2 = aNbComp;
          if(aNbComp == 2 || aNbComp == 4)
            aNbComp2 = 2;
          else if(aNbComp > 4)
            aNbComp2 = 3;

          VISU::TMetric2Comp2MinMax& aMetric2Comp2MinMax = aField->myMetric2Comp2MinMax;
          VISU::TMetric2Comp2MinMax& aMetric2Comp2AverageMinMax = aField->myMetric2Comp2AverageMinMax;
          VISU::TMetric2Comp2Group2MinMax& aMetric2Comp2Group2MinMax = aField->myMetric2Comp2Group2MinMax;
          VISU::TMetric2Comp2Group2MinMax& aMetric2Comp2Group2AverageMinMax = aField->myMetric2Comp2Group2AverageMinMax;
          TSetIsDone aSetIsDone(aField->myIsMinMaxInitilized);
          for(TInt iTimeStamp = aNbTimeStamps; iTimeStamp >= 1; iTimeStamp--){
            VISU::TTimerLog aTimerLog(MYDEBUG,"BuildMinMax - GetPTimeStampInfo()");
            INITMSG(MYDEBUG,"- iTimeStamp = "<<iTimeStamp<<endl);
            
#ifndef _DEXCEPT_
            try {
#ifdef NO_CAS_CATCH
              OCC_CATCH_SIGNALS;
#endif
#endif
              MED::PTimeStampInfo aTimeStampInfo = 
                myMed->GetPTimeStampInfo(aFieldInfo,
                                         aMEntity,
                                         aGeom2Size,
                                         iTimeStamp);

              MED::PTimeStampValueBase aTimeStampValue = 
                myMed->GetPTimeStampValue(aTimeStampInfo,
                                          aMKey2Profile,
                                          aKey2Gauss);

              const MED::TGeom2Gauss& aGeom2Gauss = aTimeStampInfo->GetGeom2Gauss();

              if(aFieldInfo->GetType() == MED::eFLOAT64)
                BuildTimeStampMinMax<MED::TFloatTimeStampValue>(CastToFloatTimeStampValue(aTimeStampValue),
                                                                aMeshOnEntity,
                                                                aGeom2Gauss,
                                                                aMetric2Comp2MinMax,
                                                                aMetric2Comp2AverageMinMax,
                                                                aMetric2Comp2Group2MinMax,
                                                                aMetric2Comp2Group2AverageMinMax,
                                                                aNbComp,
                                                                aNbComp2);

              else
                BuildTimeStampMinMax<MED::TIntTimeStampValue>(CastToIntTimeStampValue(aTimeStampValue),
                                                              aMeshOnEntity,
                                                              aGeom2Gauss,
                                                              aMetric2Comp2MinMax,
                                                              aMetric2Comp2AverageMinMax,
                                                              aMetric2Comp2Group2MinMax,
                                                              aMetric2Comp2Group2AverageMinMax,
                                                              aNbComp,
                                                              aNbComp2);

#ifndef _DEXCEPT_
            }catch(Standard_Failure){
              Handle(Standard_Failure) aFail = Standard_Failure::Caught();          
              MSG(MYDEBUG,"Follow signal was occured in:\n"<<aFail->GetMessageString());
            }catch(std::exception& exc){
              MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
            }catch(...){
              MSG(MYDEBUG,"Unknown exception !!!");
            }
#endif
          }
          for(TInt iComp = 0; iComp <= aNbComp; iComp++){
            VISU::TMinMax aMinMax = aField->GetMinMax(iComp, VISU::TNames());
            INITMSG(MYDEBUG,"- "<<iComp<<": "<<aMinMax.first<<"; "<<aMinMax.second<<endl);
          }
        }
#ifndef _DEXCEPT_
      }catch(std::exception& exc){
        MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
      }catch(...){
        MSG(MYDEBUG,"Unknown exception !!!");
      }
#endif

#ifndef _DEXCEPT_
    }catch(std::exception& exc){
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
    }catch(...){
      MSG(MYDEBUG,"Unknown exception !!!");
    }
#endif
  }

  return this; 
}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_MedConvertor
::BuildGroups()
{
  if(myIsGroupsDone)
    return this;

  TSetIsDone aSetIsDone(myIsGroupsDone);
  VISU::TTimerLog aTimerLog(MYDEBUG,"BuildGroups");

  TInt aNbMeshes = myMed->GetNbMeshes();
  VISU::TMeshMap& aMeshMap = myMeshMap;

  INITMSG(MYDEBUG,"BuildGroups - aNbMeshes = "<<aNbMeshes<<"\n");

  for(TInt iMesh = 1; iMesh <= aNbMeshes; iMesh++){
#ifndef _DEXCEPT_
    try{
#endif
      VISU::TTimerLog aTimerLog(MYDEBUG,"GetPMeshInfo");

      MED::PMeshInfo aMeshInfo = myMed->GetPMeshInfo(iMesh);
      std::string aMeshName = aMeshInfo->GetName();

      VISU::TMeshMap::const_iterator anIter = aMeshMap.find(aMeshName);
      if(anIter == aMeshMap.end())
        continue;
      VISU::PMEDMesh aMesh = anIter->second;

      INITMSG(MYDEBUG,"aMeshName = '"<<aMeshName<<"'\n");

      MED::EMaillage aType = aMeshInfo->GetType();
      
      MED::TEntityInfo anEntityInfo = myMed->GetEntityInfo(aMeshInfo);
      
      MED::TEntity2TGeom2ElemInfo anEntity2TGeom2ElemInfo = 
        MED::GetEntity2TGeom2ElemInfo(myMed, aMeshInfo, anEntityInfo);
      
#ifndef _DEXCEPT_
      try{
#endif
        MED::TFamilyInfoSet aFamilyInfoSet = MED::GetFamilyInfoSet(myMed, aMeshInfo);
        
        if(aType == MED::eNON_STRUCTURE)
          BuildFamilyMap(aMesh,
                         anEntityInfo,
                         anEntity2TGeom2ElemInfo,
                         aFamilyInfoSet,
                         myMed);
        else
          BuildGrilleFamilyMap(aMesh,
                               anEntityInfo,
                               aFamilyInfoSet,
                               myMed);
        
        BuildGroupMap(aMesh,
                      aFamilyInfoSet);
#ifndef _DEXCEPT_
      }catch(std::exception& exc){
        MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
      }catch(...){
          MSG(MYDEBUG,"Unknown exception !!!");
      }
#endif

#ifndef _DEXCEPT_
    }catch(std::exception& exc){
      MSG(MYDEBUG,"Follow exception was occured in:\n"<<exc.what());
    }catch(...){
      MSG(MYDEBUG,"Unknown exception !!!");
    }
#endif
  } 

  return this; 
}


//---------------------------------------------------------------
int
VISU_MedConvertor
::LoadMeshOnEntity(VISU::PMeshImpl theMesh,
                   VISU::PMeshOnEntityImpl theMeshOnEntity)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadMeshOnEntity");
  INITMSG(MYDEBUG,"LoadMeshOnEntity"<<endl);

  const VISU::TEntity& anEntity = theMeshOnEntity->myEntity;

  int isPointsUpdated = 0, isCellsOnEntityUpdated = 0;
  if(anEntity == VISU::NODE_ENTITY){
    isPointsUpdated += LoadPoints(myMed, theMesh);
  }else{
    isPointsUpdated += LoadPoints(myMed, theMesh);
    isCellsOnEntityUpdated += LoadCellsOnEntity(myMed, theMesh, theMeshOnEntity);
  }

  return (isPointsUpdated || isCellsOnEntityUpdated);
}


//---------------------------------------------------------------
int
VISU_MedConvertor
::LoadFamilyOnEntity(VISU::PMeshImpl theMesh,
                     VISU::PMeshOnEntityImpl theMeshOnEntity, 
                     VISU::PFamilyImpl theFamily)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadFamilyOnEntity");
  INITMSG(MYDEBUG,"LoadFamilyOnEntity"<<endl);

  const VISU::TEntity& anEntity = theMeshOnEntity->myEntity;

  int isPointsUpdated = 0, isCellsOnEntityUpdated = 0;
  if(anEntity == VISU::NODE_ENTITY){
    isPointsUpdated += LoadPointsOnFamily(myMed, theMesh, theFamily);
  }else{
    isPointsUpdated += LoadPoints(myMed, theMesh);
    isCellsOnEntityUpdated += LoadCellsOnFamily(myMed, theMesh, theMeshOnEntity, theFamily);
  }

  return (isPointsUpdated || isCellsOnEntityUpdated);
}


//---------------------------------------------------------------
int
VISU_MedConvertor
::LoadMeshOnGroup(VISU::PMeshImpl theMesh, 
                  const VISU::TFamilySet& theFamilySet)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadMeshOnGroup");
  INITMSG(MYDEBUG,"LoadMeshOnGroup"<<endl);

  int isPointsUpdated = 0, isCellsOnEntityUpdated = 0;
  VISU::TFamilySet::const_iterator aFamilyIter = theFamilySet.begin();
  for(; aFamilyIter != theFamilySet.end(); aFamilyIter++){
    VISU::PMEDFamily aFamily = (*aFamilyIter).second;
    const VISU::TEntity& anEntity = aFamily->myEntity;
    const VISU::PMEDMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[anEntity];
    isPointsUpdated += LoadPoints(myMed, theMesh);
    if(anEntity == VISU::NODE_ENTITY){
      isPointsUpdated += LoadPointsOnFamily(myMed, theMesh, aFamily);
    }else{
      isCellsOnEntityUpdated += LoadCellsOnFamily(myMed,theMesh,aMeshOnEntity,aFamily);
    }
  }

  return (isPointsUpdated || isCellsOnEntityUpdated);
}


//---------------------------------------------------------------
int
VISU_MedConvertor
::LoadValForTimeOnMesh(VISU::PMeshImpl theMesh, 
                       VISU::PMeshOnEntityImpl theMeshOnEntity, 
                       VISU::PFieldImpl theField, 
                       VISU::PValForTimeImpl theValForTime)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadValForTimeOnMesh");
  INITMSG(MYDEBUG,"LoadValForTimeOnMesh"<<endl);

  const VISU::TEntity& anEntity = theMeshOnEntity->myEntity;
  int isPointsUpdated = 0, isCellsOnEntityUpdated = 0;
  isPointsUpdated += LoadPoints(myMed,theMesh);
  if(anEntity != VISU::NODE_ENTITY)
    isCellsOnEntityUpdated += LoadCellsOnEntity(myMed,theMesh,theMeshOnEntity);
  int isFieldUpdated = LoadValForTimeOnMesh(myMed,theMesh,theMeshOnEntity,theField,theValForTime);
  
  return (isPointsUpdated || isCellsOnEntityUpdated || isFieldUpdated);
}


//---------------------------------------------------------------
int
VISU_MedConvertor
::LoadValForTimeOnGaussPts(VISU::PMeshImpl theMesh, 
                           VISU::PMeshOnEntityImpl theMeshOnEntity, 
                           VISU::PFieldImpl theField, 
                           VISU::PValForTimeImpl theValForTime)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadValForTimeOnGaussPts");
  INITMSG(MYDEBUG,"LoadValForTimeOnGaussPts"<<endl);

  const VISU::TEntity& anEntity = theMeshOnEntity->myEntity;
  int isPointsUpdated = 0, isCellsOnEntityUpdated = 0;
  if(anEntity != VISU::NODE_ENTITY)
    isCellsOnEntityUpdated += LoadCellsOnEntity(myMed,theMesh,theMeshOnEntity);
  int isFieldUpdated = LoadValForTimeOnGaussPts(myMed,theMesh,theMeshOnEntity,theField,theValForTime);
  
  return (isPointsUpdated || isCellsOnEntityUpdated || isFieldUpdated);
}


//---------------------------------------------------------------
int 
VISU_MedConvertor
::LoadPoints(const MED::PWrapper& theMed,
             VISU::PMEDMesh theMesh) 
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadPoints");
  try{
    //Check on existing family
    VISU::PMEDMeshOnEntity aMeshOnEntity = theMesh->myMeshOnEntityMap[VISU::NODE_ENTITY];
    aMeshOnEntity->myMeshName = theMesh->myName;
    aMeshOnEntity->myEntity = VISU::NODE_ENTITY;

    INITMSG(MYDEBUG,"LoadPoints - theMesh->myIsDone = "<<theMesh->myIsDone<<"'\n");

    //Check on loading already done
    if(theMesh->myIsDone)
      return 0;

    MED::EMaillage aType = theMesh->myMeshInfo->GetType();
    
    if(aType == MED::eNON_STRUCTURE){
      //Main part of code
      MED::PNodeInfo aNodeInfo = theMed->GetPNodeInfo(theMesh->myMeshInfo);
      TInt aNbElem = aNodeInfo->GetNbElem();
      
      VISU::PMEDNamedPointCoords aNamedPointCoords = theMesh->myNamedPointCoords;
      aNamedPointCoords->Init(aNodeInfo, theMed->GetVersion());
      
      VISU::TGeom2SubMesh& aGeom2SubMesh = aMeshOnEntity->myGeom2SubMesh;
      VISU::PMEDSubMesh aSubMesh = aGeom2SubMesh[VISU::ePOINT1](new VISU::TMEDSubMesh());
      
      aSubMesh->Init(MED::PElemInfo(aNodeInfo), theMed->GetVersion()); 
      aSubMesh->myNbCells = theMesh->myNbPoints;
      aSubMesh->myCellsSize = 2*theMesh->myNbPoints;
      
      VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
      aCell2Connect.resize(aNbElem);
      for (int iElem = 0; iElem < aNbElem; iElem++)
        aCell2Connect[iElem] = VISU::TConnect(1,iElem);
    } // END NON_STRUCTURED
    else { // STRUCTURED
      //Main part of code
      MED::PGrilleInfo aGrilleInfo = theMed->GetPGrilleInfo(theMesh->myMeshInfo);
      TInt aNbElem = aGrilleInfo->GetNbNodes();
      
      VISU::PMEDNamedPointCoords aNamedPointCoords = theMesh->myNamedPointCoords;
      aNamedPointCoords->Init(aGrilleInfo);
      
      VISU::TGeom2SubMesh& aGeom2SubMesh = aMeshOnEntity->myGeom2SubMesh;
      VISU::PMEDSubMesh aSubMesh = aGeom2SubMesh[VISU::ePOINT1](new VISU::TMEDSubMesh());
      
      aSubMesh->Init(aGrilleInfo); 
      aSubMesh->myNbCells = theMesh->myNbPoints;
      aSubMesh->myCellsSize = 2*theMesh->myNbPoints;
      
      VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
      aCell2Connect.resize(aNbElem);
      for(int iElem = 0; iElem < aNbElem; iElem++)
        aCell2Connect[iElem] = VISU::TConnect(1,iElem);
      
    }
    
    theMesh->myIsDone = true;
    
    return 1;

  }catch(std::exception& exc){
    throw;
  }catch(...){
    EXCEPTION(std::runtime_error,"Unknown exception !!!");
  }

  return 0;
}


//---------------------------------------------------------------
int 
VISU_MedConvertor
::LoadPointsOnFamily(const MED::PWrapper& theMed,
                     const VISU::PMEDMesh theMesh, 
                     const VISU::PMEDFamily theFamily) 
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadPointsOnFamily");
  try{
    if(theFamily->myIsDone) 
      return 0;

    const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;
    MED::EMaillage aType = aMeshInfo->GetType();

    //Main part of code
    if(aType == MED::eNON_STRUCTURE){
      MED::PNodeInfo aNodeInfo = theMed->GetPNodeInfo(theMesh->myMeshInfo);
      TInt aNbElem = aNodeInfo->GetNbElem();
      
      if(aNbElem > 0){
        TInt anId = theFamily->myId;
        VISU::TSubMeshID& aSubMeshID = theFamily->myGeom2SubMeshID[VISU::ePOINT1];
        for(TInt iElem = 0; iElem < aNbElem; iElem++) 
          if(aNodeInfo->GetFamNum(iElem) == anId)
            aSubMeshID.push_back(iElem);
      }
    }
    else {
      MED::PGrilleInfo aGrilleInfo = theMed->GetPGrilleInfo(aMeshInfo);
      TInt aNbElem = aGrilleInfo->GetNbNodes();
      
      if(aNbElem > 0){
        TInt anId = theFamily->myId;
        VISU::TSubMeshID& aSubMeshID = theFamily->myGeom2SubMeshID[VISU::ePOINT1];
        for(TInt iElem = 0; iElem < aNbElem; iElem++) 
          if(aGrilleInfo->GetFamNumNode(iElem) == anId)
            aSubMeshID.push_back(iElem);
      }
    }

    theFamily->myIsDone = true;

    return 1;

  }catch(std::exception& exc){
    throw;
  }catch(...){
    EXCEPTION(std::runtime_error,"Unknown exception !!!");
  }

  return 0;
}


//---------------------------------------------------------------
int 
VISU_MedConvertor
::LoadCellsOnEntity(const MED::PWrapper& theMed,
                    const VISU::PMEDMesh theMesh,
                    const VISU::PMEDMeshOnEntity theMeshOnEntity)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadCellsOnEntity");
#ifndef _DEXCEPT_
  try{
#endif
    const VISU::TEntity& aVEntity = theMeshOnEntity->myEntity;
    const MED::EEntiteMaillage& aMEntity = VTKEntityToMED(aVEntity);

    INITMSG(MYDEBUG,"LoadCellsOnEntity - aVEntity = "<<aVEntity<<"\n");

    if(theMeshOnEntity->myIsDone)
      return 0;

    const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;

    MED::EMaillage aType = aMeshInfo->GetType();

    if(aType == MED::eSTRUCTURE){
      
      MED::PGrilleInfo aGrilleInfo = theMed->GetPGrilleInfo(aMeshInfo);
      TInt aNbPoints = aGrilleInfo->GetNbNodes();
      
      std::map<TInt,TInt> aNodeIdMap;
      EBooleen anIsNodeNum = MED::eFAUX;
      
      const MED::TGeom2Size& aGeom2Size = theMeshOnEntity->myGeom2Size;
      MED::TGeom2Size::const_iterator aGeom2SizeIter = aGeom2Size.begin();
      VISU::TGeom2SubMesh& aGeom2SubMesh = theMeshOnEntity->myGeom2SubMesh;
      
      for(; aGeom2SizeIter != aGeom2Size.end(); aGeom2SizeIter++){
        const MED::EGeometrieElement& aMGeom = aGeom2SizeIter->first;
        VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
        INITMSG(MYDEBUG,"aMGeom = "<<aMGeom<<"; aEGeom = "<<aEGeom<<"\n");
        {
          vtkIdType aVNbNodes = VISUGeom2NbNodes(aEGeom);
          INITMSG(MYDEBUG,"aVNbNodes = "<<aVNbNodes<<"\n");
        
          TInt aNbElem = aGeom2SizeIter->second;//aGrilleInfo->GetNbCells();
          
          if(aNbElem > 0){
            VISU::PMEDSubMesh aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TMEDSubMesh());
            
            aSubMesh->Init(aGrilleInfo); 
            aSubMesh->myNbCells = aNbElem;      
            aSubMesh->myCellsSize = aNbElem*(aVNbNodes+1);
            
            VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
            aCell2Connect.resize(aNbElem);
            
            TInt aMNbNodes = MEDGeom2NbNodes(aMGeom);
            VISU::TVector<TInt> aConnect(aMNbNodes);
            
            for(TInt iElem = 0; iElem < aNbElem; iElem++) {
              MED::TIntVector aConn = aGrilleInfo->GetConn(iElem, theMeshOnEntity->myEntity != VISU::CELL_ENTITY);
              VISU::TConnect& anArray = aCell2Connect[iElem];
              anArray.resize(aVNbNodes);
              
              if(anIsNodeNum){
                for(TInt iConn = 0; iConn < aMNbNodes; iConn++){
                  aConnect[iConn] = aNodeIdMap[aConn[iConn] - 1];
                }
              }else{
                for(int iConn = 0; iConn < aMNbNodes; ++iConn){
                  aConnect[iConn] = aConn[iConn];
                }
              }
              
              switch(aMGeom){
              case MED::eQUAD4:
                anArray[0] = aConnect[0];
                anArray[1] = aConnect[2];
                anArray[2] = aConnect[3];
                anArray[3] = aConnect[1];
                break;
              case MED::eHEXA8:
                anArray[0] = aConnect[0];
                anArray[1] = aConnect[2];
                anArray[2] = aConnect[3];
                anArray[3] = aConnect[1];
                anArray[4] = aConnect[4];
                anArray[5] = aConnect[6];
                anArray[6] = aConnect[7];
                anArray[7] = aConnect[5];
                break;
              default:
                for(int iNode = 0; iNode < aVNbNodes; iNode++) 
                  anArray[iNode] = aConnect[iNode];
              }
              
              for(int iNode = 0; iNode < aVNbNodes; iNode++) 
                if(anArray[iNode] < 0 || aNbPoints <= anArray[iNode])
                  EXCEPTION(std::runtime_error,"LoadCellsOnEntity - "<<
                            " aNbPoints("<<aNbPoints<<") "<<
                            "<= anArray["<<iElem<<"]"<<
                            "["<<iNode<<"]"<<
                            "("<<anArray[iNode]<<") < 0");
            }
          }
        }
      }

      theMeshOnEntity->myIsDone = true;
      
      return 1;
      
    }
    
    MED::PNodeInfo aNodeInfo = theMed->GetPNodeInfo(aMeshInfo);
    TInt aNbPoints = aNodeInfo->GetNbElem();

    std::map<TInt,TInt> aNodeIdMap;
#ifdef _EDF_NODE_IDS_
    EBooleen anIsNodeNum = MED::eFAUX;
#else
    EBooleen anIsNodeNum = aNodeInfo->IsElemNum();
    if(anIsNodeNum){
      for(TInt i = 0; i < aNbPoints; i++){
        aNodeIdMap[aNodeInfo->GetElemNum(i)-1] = i;
      }
    }
#endif

    const MED::TGeom2Size& aGeom2Size = theMeshOnEntity->myGeom2Size;
    MED::TGeom2Size::const_iterator aGeom2SizeIter = aGeom2Size.begin();
    VISU::TGeom2SubMesh& aGeom2SubMesh = theMeshOnEntity->myGeom2SubMesh;

    for(; aGeom2SizeIter != aGeom2Size.end(); aGeom2SizeIter++){
      const MED::EGeometrieElement& aMGeom = aGeom2SizeIter->first;
      VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
      INITMSG(MYDEBUG,"aMGeom = "<<aMGeom<<"; aEGeom = "<<aEGeom<<"\n");
      switch(aMGeom){
      case MED::ePOLYGONE: {
        MED::PPolygoneInfo aPolygoneInfo = theMed->GetPPolygoneInfo(aMeshInfo,aMEntity,aMGeom);
        TInt aNbElem = aPolygoneInfo->GetNbElem();
        if(aNbElem > 0){
          VISU::PMEDSubMesh aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TMEDSubMesh());
          
          aSubMesh->Init(MED::PElemInfo(aPolygoneInfo),theMed->GetVersion()); 
          aSubMesh->myNbCells = aNbElem;      

          VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
          aCell2Connect.resize(aNbElem);
          
          for(TInt iElem = 0; iElem < aNbElem; iElem++) {
            MED::TCConnSlice aConnSlice = aPolygoneInfo->GetConnSlice(iElem);
            TInt aNbConn = aPolygoneInfo->GetNbConn(iElem);
            aSubMesh->myCellsSize += aNbConn;
            VISU::TConnect& anArray = aCell2Connect[iElem];
            anArray.resize(aNbConn);
            for(TInt iConn = 0; iConn < aNbConn; iConn++)
              anArray[iConn] = aConnSlice[iConn] - 1;
          }
        }
        break;
      }
      case MED::ePOLYEDRE: {
        MED::PPolyedreInfo aPolyedreInfo = theMed->GetPPolyedreInfo(aMeshInfo,aMEntity,aMGeom);
        TInt aNbElem = aPolyedreInfo->GetNbElem();
        
        if(aNbElem > 0){
          VISU::PMEDSubMesh aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TMEDSubMesh());
          
          aSubMesh->Init(MED::PElemInfo(aPolyedreInfo),theMed->GetVersion()); 
          aSubMesh->myNbCells = aNbElem;      

          VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
          aCell2Connect.resize(aNbElem);
          
          for(TInt iElem = 0; iElem < aNbElem; iElem++){
            MED::TCConnSliceArr aConnSliceArr = aPolyedreInfo->GetConnSliceArr(iElem);
            VISU::TConnect& anArray = aCell2Connect[iElem];
            typedef std::set<TInt> TConnectSet;
            TConnectSet aConnectSet;
            TInt aNbFaces = aConnSliceArr.size();
            for(TInt iFace = 0; iFace < aNbFaces; iFace++){
              MED::TCConnSlice aConnSlice = aConnSliceArr[iFace];
              TInt aNbConn = aConnSlice.size();
              aSubMesh->myCellsSize += aNbConn;
              for(TInt iConn = 0; iConn < aNbConn; iConn++){
                aConnectSet.insert(aConnSlice[iConn]);
              }
            }
          
            int aNbConn = aConnectSet.size();
            anArray.resize(aNbConn);
            TConnectSet::iterator anIter = aConnectSet.begin();
            for(int i = 0; anIter != aConnectSet.end(); anIter++, i++){
              TInt anId = *anIter;
              anArray[i] = anId - 1;
            }
          }
        }

        break;
      }
      default: {
        vtkIdType aVNbNodes = VISUGeom2NbNodes(aEGeom);
        INITMSG(MYDEBUG,"aVNbNodes = "<<aVNbNodes<<"\n");
        
        MED::PCellInfo aCellInfo = theMed->GetPCellInfo(aMeshInfo,aMEntity,aMGeom);
        TInt aNbElem = aCellInfo->GetNbElem();
        
        if(aNbElem > 0){
          VISU::PMEDSubMesh aSubMesh = aGeom2SubMesh[aEGeom](new VISU::TMEDSubMesh());
          
          aSubMesh->Init(MED::PElemInfo(aCellInfo), theMed->GetVersion()); 
          aSubMesh->myNbCells = aNbElem;      
          aSubMesh->myCellsSize = aNbElem*(aVNbNodes+1);

          VISU::TCell2Connect& aCell2Connect = aSubMesh->myCell2Connect;
          aCell2Connect.resize(aNbElem);
        
          TInt aMNbNodes = MEDGeom2NbNodes(aMGeom);
          VISU::TVector<TInt> aConnect(aMNbNodes);
        
          for(TInt iElem = 0; iElem < aNbElem; iElem++) {
            MED::TCConnSlice aConnSlice = aCellInfo->GetConnSlice(iElem);
            VISU::TConnect& anArray = aCell2Connect[iElem];
            anArray.resize(aVNbNodes);
            
            if(anIsNodeNum){
              for(TInt iConn = 0; iConn < aMNbNodes; iConn++){
                aConnect[iConn] = aNodeIdMap[aConnSlice[iConn] - 1];
              }
            }else{
              for(int iConn = 0; iConn < aMNbNodes; iConn++){
                aConnect[iConn] = aConnSlice[iConn] - 1;
              }
            }
            
            switch(aMGeom){
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
            case MED::eSEG3:
              anArray[0] = aConnect[0];
              anArray[2] = aConnect[1];  

              anArray[1] = aConnect[2];
              break;
#endif
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
            case MED::eTRIA6:
              anArray[0] = aConnect[0];
              anArray[2] = aConnect[1];  
              anArray[4] = aConnect[2];  

              anArray[1] = aConnect[3];
              anArray[3] = aConnect[4];  
              anArray[5] = aConnect[5];  
              break;
#endif
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
            case MED::eQUAD8:
              anArray[0] = aConnect[0];
              anArray[2] = aConnect[1];  
              anArray[4] = aConnect[2];  
              anArray[6] = aConnect[3];  

              anArray[1] = aConnect[4];
              anArray[3] = aConnect[5];  
              anArray[5] = aConnect[6];  
              anArray[7] = aConnect[7];  
              break;
#endif
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
            case MED::eQUAD9:
              anArray[0] = aConnect[0];
              anArray[2] = aConnect[1];  
              anArray[4] = aConnect[2];  
              anArray[6] = aConnect[3];  

              anArray[1] = aConnect[4];
              anArray[3] = aConnect[5];  
              anArray[5] = aConnect[6];  
              anArray[7] = aConnect[7];  
			  anArray[8] = aConnect[8];
              break;
#endif
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
            case MED::eTETRA10:
              anArray[0] = aConnect[0];
              anArray[1] = aConnect[2];
              anArray[2] = aConnect[1];  
              anArray[3] = aConnect[3];  

              anArray[4] = aConnect[6];
              anArray[5] = aConnect[5];
              anArray[6] = aConnect[4];  

              anArray[7] = aConnect[7];  
              anArray[8] = aConnect[9];  
              anArray[9] = aConnect[8];  
              break;
#endif
            case MED::eTETRA4:
              anArray[0] = aConnect[0];
              anArray[1] = aConnect[2];
              anArray[2] = aConnect[1];  
              anArray[3] = aConnect[3];  
              break;
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
            case MED::ePYRA13:
              anArray[0] = aConnect[0];

              anArray[1] = aConnect[3];
              anArray[2] = aConnect[2];  
              anArray[3] = aConnect[1];  
              anArray[4] = aConnect[4];

              anArray[5] = aConnect[8];
              anArray[6] = aConnect[7];  
              anArray[7] = aConnect[6];  
              anArray[8] = aConnect[5];  

              anArray[9] = aConnect[9];  
              anArray[10] = aConnect[12];  
              anArray[11] = aConnect[11];  
              anArray[12] = aConnect[10];  
              break;
#endif
            case MED::ePYRA5:
              anArray[0] = aConnect[0];
              anArray[1] = aConnect[3];  
              anArray[2] = aConnect[2];
              anArray[3] = aConnect[1];  
              anArray[4] = aConnect[4];
              break;
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
			case MED::eHEXA27:
              anArray[0] = aConnect[0];
              anArray[1] = aConnect[3];
              anArray[2] = aConnect[2];  
              anArray[3] = aConnect[1];  

              anArray[4] = aConnect[4];
              anArray[5] = aConnect[7];
              anArray[6] = aConnect[6];
              anArray[7] = aConnect[5];

			  anArray[8] = aConnect[11];	
			  anArray[9] = aConnect[10];  
              anArray[10] = aConnect[9];  
			  anArray[11] = aConnect[8];

			  anArray[12] = aConnect[15];	
			  anArray[13] = aConnect[14];
              anArray[14] = aConnect[13];
			  anArray[15] = aConnect[12];

			  anArray[16] = aConnect[16];	
			  anArray[17] = aConnect[19];
              anArray[18] = aConnect[18];
			  anArray[19] = aConnect[17];

			  anArray[20] = aConnect[21];
     		  anArray[21] = aConnect[23];
              anArray[22] = aConnect[24];
			  anArray[23] = aConnect[22];

  			  anArray[24] = aConnect[20];
     		  anArray[25] = aConnect[25];
              anArray[26] = aConnect[26];
              break;
#endif
            default:
              for(int iNode = 0; iNode < aVNbNodes; iNode++) 
                anArray[iNode] = aConnect[iNode];
            }
            for(int iNode = 0; iNode < aVNbNodes; iNode++) 
              if(anArray[iNode] < 0 || aNbPoints <= anArray[iNode])
                EXCEPTION(std::runtime_error,"LoadCellsOnEntity - "<<
                          " aNbPoints("<<aNbPoints<<") "<<
                          "<= anArray["<<iElem<<"]"<<
                          "["<<iNode<<"]"<<
                          "("<<anArray[iNode]<<") < 0");
          }
        }
      }}
    }

    // END NON_STRUCTURED

    theMeshOnEntity->myIsDone = true;

    return 1;

#ifndef _DEXCEPT_
  }catch(std::exception& exc){
    throw;
  }catch(...){
    EXCEPTION(std::runtime_error,"Unknown exception !!!");
  }
#endif

  return 0;
}


//---------------------------------------------------------------
int 
VISU_MedConvertor
::LoadCellsOnFamily(const MED::PWrapper& theMed,
                    const VISU::PMEDMesh theMesh,
                    const VISU::PMEDMeshOnEntity theMeshOnEntity,
                    const VISU::PMEDFamily theFamily)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadCellsOnFamily");
#ifndef _DEXCEPT_
  try{
#endif
    const VISU::TEntity& aVEntity = theMeshOnEntity->myEntity;
    const MED::EEntiteMaillage& aMEntity = VTKEntityToMED(aVEntity);

    INITMSG(MYDEBUG,"LoadCellsOnFamily - aVEntity = "<<aVEntity<<"\n");

    if(theFamily->myIsDone)
      return 0;

    TInt anId = theFamily->myId;

    const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;
    VISU::TGeom2SubMeshID& aGeom2SubMeshID = theFamily->myGeom2SubMeshID;
    MED::EMaillage aType = aMeshInfo->GetType();

    const MED::TGeom2Size& aGeom2Size = theMeshOnEntity->myGeom2Size;
    MED::TGeom2Size::const_iterator aGeom2SizeIter = aGeom2Size.begin();
    for(; aGeom2SizeIter != aGeom2Size.end(); aGeom2SizeIter++){
      const MED::EGeometrieElement& aMGeom = aGeom2SizeIter->first;
      if(aType == MED::eNON_STRUCTURE){
        MED::PElemInfo anElemInfo;
        switch(aMGeom){
        case MED::ePOLYGONE: {
          anElemInfo = theMed->GetPPolygoneInfo(aMeshInfo,aMEntity,aMGeom);
          break;
        }
        case MED::ePOLYEDRE: {
          anElemInfo = theMed->GetPPolyedreInfo(aMeshInfo,aMEntity,aMGeom);
          break;
        }
        default: {
          anElemInfo = theMed->GetPCellInfo(aMeshInfo,aMEntity,aMGeom);
          break;
        }}
        if(anElemInfo){
          if(TInt aNbElem = anElemInfo->GetNbElem()){
            VISU::TSubMeshID aSubMeshID;
            for(TInt iElem = 0; iElem < aNbElem; iElem++) 
              if(anElemInfo->GetFamNum(iElem) == anId)
                aSubMeshID.push_back(iElem);
            if(!aSubMeshID.empty()){
              VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
              INITMSG(MYDEBUG,"aMGeom = "<<aMGeom<<"\n");
              aGeom2SubMeshID.insert(VISU::TGeom2SubMeshID::value_type(aEGeom,aSubMeshID));
            }
          }
        }
      } else {
        MED::PGrilleInfo anElemInfo = theMed->GetPGrilleInfo(aMeshInfo);
        if(anElemInfo){
          TInt aNbElem = anElemInfo->GetNbCells();
          if(aNbElem>0 && (aMGeom == anElemInfo->GetGeom()) ){
            VISU::TSubMeshID aSubMeshID;
            for(TInt iElem = 0; iElem < aNbElem; iElem++) 
              if(anElemInfo->GetFamNum(iElem) == anId)
                aSubMeshID.push_back(iElem);
            if(!aSubMeshID.empty()){
              VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);
              INITMSG(MYDEBUG,"aMGeom = "<<aMGeom<<"\n");
              aGeom2SubMeshID.insert(VISU::TGeom2SubMeshID::value_type(aEGeom,aSubMeshID));
            }
          }
        }
      }
    }

    theFamily->myIsDone = true;

    return 1;

#ifndef _DEXCEPT_
  }catch(std::exception& exc){
    throw;
  }catch(...){
    EXCEPTION(std::runtime_error,"Unknown exception !!!");
  }
#endif

  return 0;
}


//---------------------------------------------------------------
void
LoadProfile(const MED::PWrapper& theMed,
            VISU::PMEDMesh theMesh,
            MED::PTimeStampValueBase theTimeStampValue,
            VISU::TMEDValForTime& theValForTime,
            VISU::TMEDMeshOnEntity& theMeshOnEntity)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadProfile");
  INITMSG(MYDEBUG,"LoadProfile"<<endl);

  VISU::PMEDProfile aProfile = theValForTime.myProfile;
  if(aProfile->myIsDone)
    return;

  const VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;
  const MED::TGeom2Profile& aGeom2Profile = theTimeStampValue->GetGeom2Profile();
  MED::TGeom2Profile::const_iterator anIter = aGeom2Profile.begin();
  for(; anIter != aGeom2Profile.end(); anIter++){
    MED::PProfileInfo aProfileInfo = anIter->second;
    MED::EGeometrieElement aMGeom = anIter->first;
    VISU::EGeometry aEGeom = MEDGeom2VISU(aMGeom);

    VISU::TGeom2SubProfile::const_iterator anIter2 = aGeom2SubProfile.find(aEGeom);
    if(anIter2 != aGeom2SubProfile.end()){
      VISU::PMEDSubProfile aSubProfile = anIter2->second;

      MED::TElemNum& anElemNum = aProfileInfo->myElemNum;
      if(!anElemNum.empty()){
        VISU::TSubMeshID& aSubMeshID = aSubProfile->mySubMeshID;
        TInt aSize = anElemNum.size();
        aSubMeshID.resize(aSize);
        for(TInt anId = 0; anId < aSize; anId++)
          aSubMeshID[anId] = anElemNum[anId] - 1;
      }

      INITMSG(MYDEBUG,
              "- aEGeom = "<<aEGeom<<
              "; aNbCells = "<<aSubProfile->mySubMeshID.size()<<
              endl);
    }
  }
  {
    const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;
    
    VISU::TEntity aVEntity = theMeshOnEntity.myEntity;
    MED::EEntiteMaillage aMEntity = VTKEntityToMED(aVEntity);
    
    const VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;
    VISU::TGeom2SubProfile::const_iterator anIter = aGeom2SubProfile.begin();
    for(; anIter != aGeom2SubProfile.end(); anIter++){
      const VISU::PMEDSubProfile& aSubProfile = anIter->second;
      MED::EGeometrieElement aMGeom = aSubProfile->myMGeom;
      MED::PElemInfo anElemInfo;
      anElemInfo = theMed->GetPElemInfo(aMeshInfo,aMEntity,aMGeom);
      aSubProfile->myIsElemNum = anElemInfo->IsElemNum();
      aSubProfile->myElemNum = anElemInfo->myElemNum;
    }
  }

  aProfile->myIsDone = true;
}


//---------------------------------------------------------------
void
LoadGaussMesh(const MED::PWrapper& theMed,
              VISU::PMEDMesh theMesh,
              MED::PTimeStampValueBase theTimeStampValue,
              VISU::TMEDValForTime& theValForTime,
              VISU::TMEDMeshOnEntity& theMeshOnEntity)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadGaussMesh");
  INITMSG(MYDEBUG,"LoadGaussMesh"<<endl);

  // this part of code must be reimplemented in connection with GRILLE structures
  if(theMesh->myMeshInfo->GetType() == MED::eSTRUCTURE)
    EXCEPTION(std::runtime_error,"LoadGaussMesh - Gauss Points localization error!!!");
  
  VISU::PMEDGaussMesh aGaussMesh = theValForTime.myGaussMesh;
  if(!aGaussMesh || aGaussMesh->myIsDone)
    return;

  const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;
  MED::PNodeInfo aNodeInfo = theMed->GetPNodeInfo(aMeshInfo);

  VISU::TEntity aVEntity = theMeshOnEntity.myEntity;
  MED::EEntiteMaillage aMEntity = VTKEntityToMED(aVEntity);

  const VISU::TGeom2GaussSubMesh& aGeom2GaussSubMesh = aGaussMesh->myGeom2GaussSubMesh;
  const MED::TTimeStampInfo& aTimeStampInfo = theTimeStampValue->GetTimeStampInfo();
  const MED::TGeom2Gauss& aGeom2Gauss = aTimeStampInfo.GetGeom2Gauss();

  VISU::TGeom2GaussSubMesh::const_iterator aSubMeshIter = aGeom2GaussSubMesh.begin();
  for(; aSubMeshIter != aGeom2GaussSubMesh.end(); aSubMeshIter++){
    VISU::PMEDGaussSubMesh aGaussSubMesh = aSubMeshIter->second;
    VISU::EGeometry aEGeom = aSubMeshIter->first;
    
    if(aGaussSubMesh->myIsDone)
      continue;

    VISU::PMEDSubProfile aSubProfile = aGaussSubMesh->mySubProfile;
    const VISU::TSubMeshID& aSubMeshID = aSubProfile->mySubMeshID;
    MED::EGeometrieElement aMGeom = aSubProfile->myMGeom;

    MED::PPolygoneInfo aPolygoneInfo;
    MED::PPolyedreInfo aPolyedreInfo;
    MED::PCellInfo aCellInfo;
    MED::PElemInfo anElemInfo;

    switch(aMGeom){
    case MED::ePOLYGONE: 
      aPolygoneInfo = theMed->GetPPolygoneInfo(aMeshInfo,
                                               aMEntity,
                                               aMGeom);
      anElemInfo = aPolygoneInfo;
      break;
    case MED::ePOLYEDRE: 
      aPolyedreInfo = theMed->GetPPolyedreInfo(aMeshInfo,
                                               aMEntity,
                                               aMGeom);
      anElemInfo = aPolyedreInfo;
      break;
    default:
      aCellInfo = theMed->GetPCellInfo(aMeshInfo,
                                       aMEntity, 
                                       aMGeom);
      anElemInfo = aCellInfo;
    }
    aGaussSubMesh->myIsElemNum = anElemInfo->IsElemNum();
    aGaussSubMesh->myElemNum = anElemInfo->myElemNum;

    std::string aName;
    bool anIsGaussCoord3D = false;
    MED::PGaussCoord aGaussCoordPtr(new MED::TGaussCoord());
    MED::TGaussCoord& aGaussCoord = *aGaussCoordPtr;

    MED::TGeom2Gauss::const_iterator aGaussIter = aGeom2Gauss.find(aMGeom);
    if(aGaussIter != aGeom2Gauss.end()){
      VISU::PMEDGauss aGauss = aGaussSubMesh->myGauss;
      MED::PGaussInfo aGaussInfo = aGauss->myGaussInfo;
      if(aGaussInfo){
        aName = aGaussInfo->GetName();
        if(!aSubMeshID.empty()){
          const std::string& aProfileName = aSubProfile->myName;
          MED::PProfileInfo aProfileInfo = MED::GetProfileInfo(theMed,
                                                               aProfileName);
          if(aProfileInfo){
            const MED::TElemNum& anElemNum = aProfileInfo->myElemNum;
            anIsGaussCoord3D = MED::GetGaussCoord3D(aGaussInfo,
                                                    aCellInfo,
                                                    aNodeInfo,
                                                    aGaussCoord,
                                                    anElemNum);
          }
        }else
          anIsGaussCoord3D = MED::GetGaussCoord3D(aGaussInfo,
                                                  aCellInfo,
                                                  aNodeInfo,
                                                  aGaussCoord);
      }
    }else{
      if(!aSubMeshID.empty()){
        const std::string& aProfileName = aSubProfile->myName;
        MED::PProfileInfo aProfileInfo = MED::GetProfileInfo(theMed,
                                                             aProfileName);
        if(aProfileInfo){
          const MED::TElemNum& anElemNum = aProfileInfo->myElemNum;
          switch(aMGeom){
          case MED::ePOLYGONE: 
            anIsGaussCoord3D = MED::GetBaryCenter(aPolygoneInfo,
                                                  aNodeInfo,
                                                  aGaussCoord,
                                                  anElemNum);
            break;
          case MED::ePOLYEDRE: 
            anIsGaussCoord3D = MED::GetBaryCenter(aPolyedreInfo,
                                                  aNodeInfo,
                                                  aGaussCoord,
                                                  anElemNum);
            break;
          default:
            anIsGaussCoord3D = MED::GetBaryCenter(aCellInfo,
                                                  aNodeInfo,
                                                  aGaussCoord,
                                                  anElemNum);
          }
        }
      }else
        switch(aMGeom){
        case MED::ePOLYGONE: 
          anIsGaussCoord3D = MED::GetBaryCenter(aPolygoneInfo,
                                                aNodeInfo,
                                                aGaussCoord);
          break;
        case MED::ePOLYEDRE: 
          anIsGaussCoord3D = MED::GetBaryCenter(aPolyedreInfo,
                                                aNodeInfo,
                                                aGaussCoord);
          break;
        default:
          anIsGaussCoord3D = MED::GetBaryCenter(aCellInfo,
                                                aNodeInfo,
                                                aGaussCoord);
        }
    }
    
    if(anIsGaussCoord3D){
      VISU::TPointCoords& aCoords = aGaussSubMesh->myPointCoords;
      VISU::TMEDGaussCoordHolder* aCoordHolder = new VISU::TMEDGaussCoordHolder();
      aCoordHolder->Init(aGaussCoordPtr);
      aCoords.Init(VISU::PCoordHolder(aCoordHolder));
      
      aGaussSubMesh->myIsDone = true;
      
      TInt aNbGauss = aGaussCoord.GetNbGauss();
      TInt aNbElem = aGaussCoord.GetNbElem();
      TInt aNbCells = aNbElem*aNbGauss;

      INITMSG(MYDEBUG,
              "- aEGeom = "<<aEGeom<<
              "; aName = '"<<aName<<"'"<<
              "; aStatus = "<<aGaussSubMesh->myStatus<<
              "; aNbElem = "<<aNbElem<<
              "; aNbGauss = "<<aNbGauss<<
              "; aNbCells = "<<aNbCells<<
              endl);
    }else
      EXCEPTION(std::runtime_error,"LoadGaussMesh - Gauss Points localization error!!!");
  }
  
  aGaussMesh->myIsDone = true;
}


//---------------------------------------------------------------
template<class TimeStampValueType>
void
FillValForTime(MED::SharedPtr<TimeStampValueType> theTimeStampValue,
               const VISU::TGeom2SubProfile& theGeom2SubProfile,
               VISU::PMEDValForTime theValForTime,
               VISU::PMEDField theField)
{
  if(theValForTime->myIsFilled) 
    return;
  theField->myDataSize = 0;
  TInt aNbComp = theField->myNbComp;

  VISU::TGeom2SubProfile::const_iterator anIter = theGeom2SubProfile.begin();
  for(; anIter != theGeom2SubProfile.end(); anIter++) {
    VISU::EGeometry aEGeom = anIter->first;
    VISU::PMEDSubProfile aSubProfile(anIter->second);

    TInt aNbElem = aSubProfile->myNbCells;
    theField->myDataSize += aNbElem * aNbComp;

    if(aSubProfile->myStatus != VISU::eRemoveAll){
      TInt aNbGauss = theValForTime->GetNbGauss(aEGeom);

      INITMSG(MYDEBUG,
              "- aEGeom = "<<aEGeom<<
              "; aNbElem = "<<aNbElem<<
              "; aNbGauss = "<<aNbGauss<<
              endl);
      
      VISU::PMeshValue& aVMeshValue = theValForTime->GetMeshValue(aEGeom);
      typedef typename TimeStampValueType::TElement TElement;
      typedef VISU::TTMEDMeshValue<TElement> TVMeshValue;
      TVMeshValue* aMeshValue = new TVMeshValue();

      MED::EGeometrieElement aMGeom = aSubProfile->myMGeom;
      typedef typename TimeStampValueType::PTMeshValue PTMeshValue;
      PTMeshValue aMeshValuePtr = theTimeStampValue->GetMeshValuePtr(aMGeom);
      aMeshValue->Init(aNbElem, aNbGauss, aNbComp, aMeshValuePtr);
      aVMeshValue.reset(aMeshValue);
    }
  }
  theValForTime->myIsFilled = true;
}

//---------------------------------------------------------------
int 
LoadValForTime(const MED::PWrapper& theMed,
               VISU::PMEDMesh theMesh,
               VISU::PMEDMeshOnEntity theMeshOnEntity,
               VISU::PMEDField theField, 
               VISU::PMEDValForTime theValForTime,
               bool theIsGauss,
               bool& theIsDone)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"LoadValForTime");
  INITMSG(MYDEBUG,"LoadValForTime - theIsGauss = "<<theIsGauss<<endl);

  //Check on loading already done
  if(theIsDone) 
    return 0;

  //Main part of code
  const std::string& aMeshName = theMeshOnEntity->myMeshName;
  const MED::PMeshInfo& aMeshInfo = theMesh->myMeshInfo;
  MED::PFieldInfo aFieldInfo = theMed->GetPFieldInfo(aMeshInfo,
                                                     theField->myId);

  MED::TGeom2Size aGeom2Size;
  MED::EEntiteMaillage aMEntity;
  theMed->GetNbTimeStamps(aFieldInfo,
                          theMesh->myEntityInfo,
                          aMEntity,
                          aGeom2Size);

  MED::PTimeStampInfo aTimeStampInfo = 
    theMed->GetPTimeStampInfo(aFieldInfo,
                              aMEntity,
                              aGeom2Size,
                              theValForTime->myId);

  MED::TKey2Gauss aKey2Gauss = GetKey2Gauss(theMed);
  MED::TMKey2Profile aMKey2Profile = GetMKey2Profile(theMed);

  MED::PTimeStampValueBase aTimeStampValue = 
    theMed->GetPTimeStampValue(aTimeStampInfo,
                               aMKey2Profile,
                               aKey2Gauss);
  
  InitProfile(theMed,
              aMeshInfo,
              aTimeStampValue,
              theMeshOnEntity,
              aMEntity,
              aGeom2Size,
              theValForTime);

  LoadProfile(theMed,
              theMesh,
              aTimeStampValue,
              theValForTime,
              theMeshOnEntity);
  
  if(theIsGauss) {
    InitGaussMesh(aTimeStampValue,
                  theMeshOnEntity,
                  aGeom2Size,
                  theValForTime);

    LoadGaussMesh(theMed,
                  theMesh,
                  aTimeStampValue,
                  theValForTime,
                  theMeshOnEntity);
  }
  
  VISU::PMEDProfile aProfile = theValForTime->myProfile;
  VISU::TGeom2SubProfile& aGeom2SubProfile = aProfile->myGeom2SubProfile;

  INITMSGA(MYDEBUG,0,
           "- aMeshName = '"<<aMeshName<<"'"<<
           "; aFieldName = '"<<aFieldInfo->GetName()<<"'"<<
           "; aMEntity = "<<aMEntity<<
           "; aNbComp = "<<theField->myNbComp<<
           endl);

  if(aFieldInfo->GetType() == MED::eFLOAT64)
    FillValForTime<MED::TFloatTimeStampValue>(CastToFloatTimeStampValue(aTimeStampValue),
                                              aGeom2SubProfile,
                                              theValForTime,
                                              theField);
  else
    FillValForTime<MED::TIntTimeStampValue>(CastToIntTimeStampValue(aTimeStampValue),
                                            aGeom2SubProfile,
                                            theValForTime,
                                            theField);
  theIsDone = true;

  return 1; 
}


//---------------------------------------------------------------
int 
VISU_MedConvertor
::LoadValForTimeOnMesh(const MED::PWrapper& theMed,
                       VISU::PMEDMesh theMesh,
                       VISU::PMEDMeshOnEntity theMeshOnEntity,
                       VISU::PMEDField theField, 
                       VISU::PMEDValForTime theValForTime)
{
  VISU::PUnstructuredGridIDMapperImpl anUnstructuredGridIDMapper = theValForTime->myUnstructuredGridIDMapper;
  return LoadValForTime(theMed,
                        theMesh,
                        theMeshOnEntity,
                        theField,
                        theValForTime,
                        false,
                        anUnstructuredGridIDMapper->myIsVTKDone);
}


//---------------------------------------------------------------
int 
VISU_MedConvertor
::LoadValForTimeOnGaussPts(const MED::PWrapper& theMed,
                           VISU::PMEDMesh theMesh,
                           VISU::PMEDMeshOnEntity theMeshOnEntity,
                           VISU::PMEDField theField, 
                           VISU::PMEDValForTime theValForTime)
{
  VISU::PGaussPtsIDFilter aGaussPtsIDFilter = theValForTime->myGaussPtsIDFilter;
  return LoadValForTime(theMed,
                        theMesh,
                        theMeshOnEntity,
                        theField,
                        theValForTime,
                        true,
                        aGaussPtsIDFilter->myIsVTKDone);
}

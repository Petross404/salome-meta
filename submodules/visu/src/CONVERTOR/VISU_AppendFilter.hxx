// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef VISU_APPENDFILTER_H
#define VISU_APPENDFILTER_H

#include "VISUConvertor.hxx"
#include "VISU_AppendFilterUtilities.hxx"

#include <vtkAppendFilter.h>

/*! \brief This class used same as vtkAppendFilter. See documentation on VTK for more information.
 */
class VISU_CONVERTOR_EXPORT VISU_AppendFilter : public vtkAppendFilter,
                                                public VISU::TAppendFilterHelper
{
public:
  /*! \fn static VISU_AppendFilter *New()
   */
  static VISU_AppendFilter *New();
  
  /*! \fn vtkTypeMacro(VISU_AppendFilter, vtkAppendFilter)
   *  \brief VTK type revision macros.
   */
  vtkTypeMacro(VISU_AppendFilter, vtkAppendFilter);

protected:
  /*! \fn VISU_AppendFilter();
   * \brief Constructor
   */
  VISU_AppendFilter();

  /*! \fn ~VISU_AppendFilter();
   * \brief Destructor.
   */
  ~VISU_AppendFilter();

  virtual
  int
  RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
};

#endif

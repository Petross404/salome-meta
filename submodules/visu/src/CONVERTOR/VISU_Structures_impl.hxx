// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : 
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_Structures_impl_HeaderFile
#define VISU_Structures_impl_HeaderFile

/*! 
  \file VISU_Structures_impl.hxx
  \brief The file contains definitions for basic classes of the implementation of VISU CONVERTER package
*/

#include "VISUConvertor.hxx"
#include "VISU_Structures.hxx"
#include "VISU_ConvertorDef_impl.hxx"

class vtkPointSet;


namespace VISU
{
  //---------------------------------------------------------------
  //! Define an utility base class which allow to keep calculated number of cells and their size
  struct VISU_CONVERTOR_EXPORT TSizeCounter: virtual TIsVTKDone
  {
    TSizeCounter();
    vtkIdType myNbCells; //!< Number of cells contained into corresponding sublclass
    vtkIdType myCellsSize; //!< Size of cells contained into corresponding sublclass
  };


  //---------------------------------------------------------------
  //! Define a container for VTK representation
  class TPolyDataHolder: public virtual TSizeCounter
  {
  protected:
    mutable PPolyData mySource;
  public:
    TPolyDataHolder();

    //! This method allow to create corresponding VTK data set by demand (not at once)
    const PPolyData& 
    GetSource() const;

    virtual
    vtkPolyData* 
    GetPolyDataOutput();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();
  };


  //---------------------------------------------------------------
  //! Define a container for VTK representation
  class VISU_CONVERTOR_EXPORT TUnstructuredGridHolder: public virtual TSizeCounter
  {
  public:
    TUnstructuredGridHolder();

    //! This method allow to create corresponding VTK data set by demand (not at once)
    const PUnstructuredGrid& 
    GetSource() const;

    //! This method allow to create corresponding VTK filter by demand (not at once)
    const PPassThroughFilter& 
    GetFilter() const;

    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

  protected:
    mutable PUnstructuredGrid mySource;
    mutable PPassThroughFilter myFilter;
  };


  //---------------------------------------------------------------
  //! Define an intermediate class which unifies memory size calculation
  struct VISU_CONVERTOR_EXPORT TMemoryCheckIDMapper: public virtual TIsVTKDone,
                               public virtual TIDMapper
  {
    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();
  };


  //---------------------------------------------------------------
  //! Define a container for VTK representation
  /*!
    This container allow to combine other VTK representation into single one.
  */
  class VISU_CONVERTOR_EXPORT TAppendFilterHolder: public virtual TMemoryCheckIDMapper
  {
  protected:
    mutable PAppendFilter myFilter;
  public:
    TAppendFilterHolder();

    //! This method allow to create corresponding VTK filter by demand (not at once)
    const PAppendFilter& 
    GetFilter() const;

    //! Reimplement the TNamedIDMapper::GetUnstructuredGridOutput
    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    //! Reimplement the TNamedIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();
  };

  /*!
    This container allow to combine other VTK representation into single one.
  */
  //---------------------------------------------------------------
  //! Define a container for VTK representation
  /*!
    This container allow to combine other VTK representation into single one.
  */
  class TAppendPolyDataHolder: public virtual TMemoryCheckIDMapper
  {
  protected:
    mutable PAppendPolyData myFilter;
  public:
    TAppendPolyDataHolder();

    //! This method allow to create corresponding VTK filter by demand (not at once)
    const PAppendPolyData& 
    GetFilter() const;

    //! Reimplement the TGaussPtsIDMapper::GetPolyDataOutput
    virtual
    vtkPolyData* 
    GetPolyDataOutput();

    //! Reimplement the TGaussPtsIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();
  };


  //---------------------------------------------------------------
  //! Define a container for VTK representation
  /*!
    This container allow to assign data to mesh and represent them into single VTK representation
  */
  class TMergeFilterHolder: public virtual TMemoryCheckIDMapper
  {
  protected:
    mutable PMergeFilter myFilter;
  public:
    TMergeFilterHolder();

    //! This method allow to create corresponding VTK filter by demand (not at once)
    const PMergeFilter& 
    GetFilter() const;

    //! Gets output of the filter as vtkDataSet
    virtual
    vtkDataSet*
    GetOutput();

    //! Gets output port of the filter
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();
  };


  //---------------------------------------------------------------
  //! Specialize TMesh to provide VTK mapping for nodes
  struct VISU_CONVERTOR_EXPORT TMeshImpl: virtual TMesh, 
                    virtual TIsVTKDone
  {
    PNamedPointCoords myNamedPointCoords; //!< Keeps intermediate representation of the nodes
    vtkIdType myNbPoints; //!< Keeps number of the nodes

    TMeshImpl();

    vtkIdType
    GetNbPoints() const;

    vtkIdType
    GetDim() const;

    vtkPointSet*
    GetPointSet(); //!< Gets initialized corresponding VTK structure
  };


  //---------------------------------------------------------------
  typedef TVector<vtkIdType> TSubMeshID;
  typedef enum {eRemoveAll, eAddAll, eAddPart, eNone} ESubMeshStatus; 

  //! Specialize TSubProfile to provide VTK mapping
  struct VISU_CONVERTOR_EXPORT TSubProfileImpl: virtual TSubProfile, 
                          virtual TUnstructuredGridHolder
  {
    TSubProfileImpl();

    EGeometry myGeom; //!< Defines to what geometrical type the MED PROFILE belong to
    std::string myName; //!< Keeps its name

    //! Get object number of mesh cell by its VTK one
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theVtkI) const;

    //! Get cell VTK ID for corresponding object ID
    virtual
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;
    
    //! Return true in case if it is default profile,
    //! i.e myName == "" (MED_NOPFL, see med.h)
    virtual
    bool 
    isDefault() const;

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    //! Keeps status of the structure
    /*!
      In some cases MED file does not use MED PROFILES, but at VISU creates corresponding data strucutre
      in order to construct mesh for MED TIEMSTAMPS in uniform way.
    */
    ESubMeshStatus myStatus; 
    TSubMeshID mySubMeshID; //!< Keeps numbers of mesh cell which contain the MED PROFILE
  };


  //---------------------------------------------------------------
  typedef std::map<vtkIdType,vtkIdType> TID2ID;
  typedef TVector<PSubProfileImpl> TSubProfileArr;
  typedef std::map<EGeometry,PSubProfileImpl> TGeom2SubProfile;

  //! Specialize TProfile to provide VTK mapping for MED TIMESTAMP mesh
  struct VISU_CONVERTOR_EXPORT TProfileImpl: virtual TProfile, 
                       virtual TAppendFilterHolder
  {
    TProfileImpl();
    bool myIsAll; //!< Say, whether the MED TIMESTAMP defined on all MED ENTITY or not
   
    //! Reimplement the TIDMapper::GetNodeObjID
    virtual 
    vtkIdType 
    GetNodeObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeVTKID
    virtual 
    vtkIdType 
    GetNodeVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeCoord
    virtual 
    double*  
    GetNodeCoord(vtkIdType theObjID);

    //! Reimplement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemVTKID
    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemCell
    virtual
    vtkCell* 
    GetElemCell(vtkIdType theObjID);
    
    //! Reimplement the TNamedIDMapper::GetUnstructuredGridOutput
    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    //! Reimplement the TNamedIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    //! Reimplement the TNamedIDMapper::GetNodeName
    virtual
    std::string 
    GetNodeName(vtkIdType theObjID) const;

    //! Reimplement the TNamedIDMapper::GetElemName
    virtual
    std::string 
    GetElemName(vtkIdType theObjID) const;

    TID2ID myElemObj2VTKID; //!< Keeps object to VTK numeration mapping
    TSubProfileArr mySubProfileArr; //!< Keeps sequence of TSubProfiles as they were added into TAppendFilterHolder
    PNamedPointCoords myNamedPointCoords; //!< Keeps reference on the same TNamedPointCoords as TMesh
    TMeshOnEntityImpl* myMeshOnEntity; //<! Keeps backward reference to corresponding MED ENTITY mesh

    TGeom2SubProfile myGeom2SubProfile; //!< Keeps TSubProfiles according to their geometrical type
  };


  //---------------------------------------------------------------
  //! Specialize TIDMapper to provide VTK mapping for MED TIMESTAMP mesh
  struct TUnstructuredGridIDMapperImpl: virtual TMergeFilterHolder,
                                        virtual TUnstructuredGridIDMapper
  {
    PAppendFilterHolder myIDMapper; //!< Responsible for numbering
    PCommonCellsFilter myCommonCellsFilter;

    TUnstructuredGridIDMapperImpl();
    
    //! Reimplement the TIDMapper::GetNodeObjID
    virtual 
    vtkIdType 
    GetNodeObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeVTKID
    virtual 
    vtkIdType 
    GetNodeVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeCoord
    virtual 
    double*  
    GetNodeCoord(vtkIdType theObjID);

    //! Reimplement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemVTKID
    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemCell
    virtual
    vtkCell* 
    GetElemCell(vtkIdType theObjID);
    
    //! Reimplement the TUnstructuredGridIDMapper::GetUnstructuredGridOutput
    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    //! Reimplement the TIDMapper::GetOutput
    virtual
    vtkDataSet* 
    GetOutput();

    //! Reimplement the TUnstructuredGridIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput*
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();
    
    void 
    SetReferencedMesh( const PNamedIDMapper& theNamedIDMapper );

    PUnstructuredGrid 
    GetSource();

  protected:
    void Build();

    TUnstructuredGridHolder mySource; //!< Keeps assigned data
  };

  //---------------------------------------------------------------
  //! Specialize TIDMapper to provide VTK mapping for MED TIMESTAMP mesh
  struct TPolyDataIDMapperImpl: virtual TMergeFilterHolder,
                                virtual TPolyDataIDMapper
  {
    PAppendPolyDataHolder myIDMapper; //!< Responsible for numbering

    //! Reimplement the TIDMapper::GetNodeObjID
    virtual 
    vtkIdType 
    GetNodeObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeVTKID
    virtual 
    vtkIdType 
    GetNodeVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeCoord
    virtual 
    double*  
    GetNodeCoord(vtkIdType theObjID);

    //! Reimplement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemVTKID
    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemCell
    virtual
    vtkCell* 
    GetElemCell(vtkIdType theObjID);
    
    //! Reimplement the TPolyDataIDMapper::GetPolyDataOutput
    virtual
    vtkPolyData* 
    GetPolyDataOutput();

    //! Reimplement the TIDMapper::GetOutput
    virtual
    vtkDataSet* 
    GetOutput();

    //! Reimplement the TPolyDataIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput*
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    PPolyData 
    GetSource();

  protected:
    void Build();

    TPolyDataHolder mySource; //!< Keeps assigned data
  };


  //---------------------------------------------------------------
  //! Specialize TGauss to provide more detail information of the MED GAUSS entity for VTK mapping
  struct VISU_CONVERTOR_EXPORT TGaussImpl: virtual TGauss
  {
    EGeometry myGeom; //!< Define, to which geometrical type the MED GAUSS entity belongs
    std::string myName; //!< Keeps name of the MED GAUSS entity
    vtkIdType myNbPoints; //<! Keeps number of points for the MED GAUSS entity

    TGaussImpl();

    //! To define a way to implement more detail comparision of the TGaussSubMesh instances
    virtual
    void
    LessThan(const PGaussImpl& theGauss,
             bool& theResult) const;
  };


  //---------------------------------------------------------------
  //! Specialize TGaussSubMesh to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TGaussSubMeshImpl: virtual TGaussSubMesh, 
                            virtual TPolyDataHolder
  {
    TGaussSubMeshImpl();

    //! To implement the TGaussPtsIDMapper::GetObjID
    virtual
    TGaussPointID
    GetObjID(vtkIdType theID) const;

    //! To implement the TGaussPtsIDMapper::GetVTKID
    virtual
    vtkIdType
    GetVTKID(const TGaussPointID& theID) const;
    
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;
    
    virtual
    vtkIdType
    GetGlobalID(vtkIdType theID) const;
    
    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    PGaussImpl myGauss; //<! Keep reference to corresponding TGauss structure

    //! Keeps status of the structure
    /*!
      In some cases MED file does not use MED GAUSS, but at VISU creates corresponding data strucutre
      in order to construct mesh for MED TIEMSTAMPS in uniform way.
    */
    ESubMeshStatus myStatus;

    vtkIdType myStartID;
    PPointCoords myPointCoords; //!< Keeps coordinates of Gauss Points
  };


  //---------------------------------------------------------------
  typedef TVector<PGaussSubMeshImpl> TGaussSubMeshArr;
  typedef std::map<EGeometry, PGaussSubMeshImpl> TGeom2GaussSubMesh;

  //! Specialize TGaussMesh to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TGaussMeshImpl: virtual TGaussMesh, 
                         virtual TAppendPolyDataHolder
  {
    TGaussMeshImpl();

    //! Reimplement the TGaussPtsIDMapper::GetObjID
    virtual
    TGaussPointID
    GetObjID(vtkIdType theID) const;

    //! Reimplements the TGaussPtsIDMapper::GetVTKID
    virtual
    vtkIdType 
    GetVTKID(const TGaussPointID& theID) const;

    //! Reimplement the TGaussPtsIDMapper::GetPolyDataOutput
    virtual
    vtkPolyData* 
    GetPolyDataOutput();

    //! Reimplement the TGaussPtsIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    //! Reimplement the TGaussPtsIDMapper::GetParent
    virtual 
    TNamedIDMapper*
    GetParent() const;

    TNamedIDMapper* myParent; //!< Refer to parent mesh
    TGaussSubMeshArr myGaussSubMeshArr; //!< Keeps sequence of TGaussSubMesh as they were added into TAppendFilterHolder
    TGeom2GaussSubMesh myGeom2GaussSubMesh; //!< Keeps TGaussSubMesh according to their geometrical type
  };


  //---------------------------------------------------------------
  //! Specialize TGaussPtsIDMapper to provide VTK mapping for MED TIMESTAMP mesh
  struct VISU_CONVERTOR_EXPORT TGaussPtsIDFilter: virtual TPolyDataIDMapperImpl,
                            virtual TGaussPtsIDMapper
  { 
    PGaussPtsIDMapper myGaussPtsIDMapper;

    //! Reimplement the TGaussPtsIDMapper::GetObjID
    virtual 
    TGaussPointID 
    GetObjID(vtkIdType theID) const;

    //! Reimplements the TGaussPtsIDMapper::GetVTKID
    virtual 
    vtkIdType 
    GetVTKID(const TGaussPointID& theID) const;

    //! Reimplements the TGaussPtsIDMapper::GetParent
    //! Reimplement the TGaussPtsIDMapper::GetParent
    virtual 
    TNamedIDMapper*
    GetParent() const;

    //! Reimplement the TNamedIDMapper::GetUnstructuredGridOutput
    virtual
    vtkPolyData* 
    GetPolyDataOutput();

    //! Reimplement the TIDMapper::GetOutput
    virtual
    vtkDataSet*
    GetOutput();
  };


  //---------------------------------------------------------------
  typedef TVector<vtkIdType> TConnect;
  typedef TVector<TConnect> TCell2Connect;

  //! The class is responsible for mapping of cells of defined geometrical type  
  struct VISU_CONVERTOR_EXPORT TSubMeshImpl: virtual TUnstructuredGridHolder
  {
    TSubMeshImpl();

    //! Reimplements the TStructured::CopyStructure
    virtual
    void
    CopyStructure( PStructured theStructured );

    //! To implement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! To implement the TNamedIDMapper::GetElemName
    virtual
    std::string 
    GetElemName(vtkIdType theObjID) const;

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    vtkIdType myStartID;
    TCell2Connect myCell2Connect; //!< Contains connectivity for the cells
  };


  //---------------------------------------------------------------
  typedef std::map<EGeometry,PSubMeshImpl> TGeom2SubMesh;
  typedef TVector<PSubMeshImpl> TSubMeshArr;

  //! Specialize TMeshOnEntity to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TMeshOnEntityImpl: virtual TMeshOnEntity, 
                            virtual TAppendFilterHolder, 
                            virtual TSizeCounter
  {
    TMeshOnEntityImpl();

    //! Reimplements the TStructured::CopyStructure
    virtual
    void
    CopyStructure( PStructured theStructured );

    //! Reimplement the TIDMapper::GetNodeVTKID
    virtual 
    vtkIdType 
    GetNodeVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeObjID
    virtual 
    vtkIdType 
    GetNodeObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemVTKID
    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! Reimplement the TNamedIDMapper::GetNodeName
    virtual
    std::string 
    GetNodeName(vtkIdType theObjID) const;

    //! Reimplement the TNamedIDMapper::GetElemName
    virtual
    std::string 
    GetElemName(vtkIdType theObjID) const;

    //! Reimplement the TNamedIDMapper::GetUnstructuredGridOutput
    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    //! Reimplement the TNamedIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    TID2ID myElemObj2VTKID; //!< To support object to VTK number mapping 
    TSubMeshArr mySubMeshArr; //!< Keeps sequence of TSubMeshImpl as they were added into TAppendFilterHolder
    PNamedPointCoords myNamedPointCoords; //!< Share the same instance with TMesh to implement nodal mapping

    TGeom2SubMesh myGeom2SubMesh; //!< Keeps TSubMeshImpl according to their geometrical type
  };


  //---------------------------------------------------------------
  typedef std::map<EGeometry,TSubMeshID> TGeom2SubMeshID;

  //! Specialize TFamily to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TFamilyImpl: virtual TFamily, 
                      virtual TUnstructuredGridHolder
  {
    //! Reimplements the TStructured::CopyStructure
    virtual
    void
    CopyStructure( PStructured theStructured );

    //! Reimplement the TIDMapper::GetNodeObjID
    vtkIdType 
    GetNodeObjID(vtkIdType theID) const ;

    //! Reimplement the TIDMapper::GetNodeVTKID
    virtual 
    vtkIdType 
    GetNodeVTKID(vtkIdType theID) const ;

    //! Reimplement the TIDMapper::GetElemVTKID
    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! Reimplement the TUnstructuredGridIDMapper::GetUnstructuredGridOutput
    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    //! Reimplement the TUnstructuredGridIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    PNamedPointCoords myNamedPointCoords;  //!< Share the same instance with TMesh to implement nodal mapping
    TID2ID myElemObj2VTKID; //!< To support object to VTK number mapping
    TSubMeshID myMeshID; //!< Keeps numbers of mesh elements that belongs to the MED FAMILY

    TGeom2SubMeshID myGeom2SubMeshID; //!< Keeps TSubMeshID according to their geometrical type

    TNames myGroupNames; //!< Keeps names of groups that refer to the family
  };


  //---------------------------------------------------------------
  typedef std::pair<vtkIdType,vtkIdType> TNbASizeCells;
  typedef TVector<PFamilyImpl> TFamilyArr;

  //! Specialize TGroup to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TGroupImpl: virtual TGroup, 
                     virtual TAppendFilterHolder
  {
    //! Reimplements the TStructured::CopyStructure
    virtual
    void
    CopyStructure( PStructured theStructured );

    //! Calculate pair of values - number of cells and its size
    TNbASizeCells 
    GetNbASizeCells() const;

    //! Reimplement the TIDMapper::GetElemVTKID
    virtual 
    vtkIdType 
    GetElemVTKID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetElemObjID
    virtual 
    vtkIdType 
    GetElemObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeObjID
    virtual 
    vtkIdType 
    GetNodeObjID(vtkIdType theID) const;

    //! Reimplement the TIDMapper::GetNodeVTKID
    virtual 
    vtkIdType 
    GetNodeVTKID(vtkIdType theID) const;

    //! Reimplement the TUnstructuredGridIDMapper::GetUnstructuredGridOutput
    virtual
    vtkUnstructuredGrid* 
    GetUnstructuredGridOutput();

    //! Reimplement the TUnstructuredGridIDMapper::GetOutputPort
    virtual
    vtkAlgorithmOutput* 
    GetOutputPort();

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();

    TID2ID myElemObj2VTKID; //!< To support object to VTK number mapping
    TFamilyArr myFamilyArr; //!< Keeps sequence of TFamily as they were added into TAppendFilterHolder
    PNamedPointCoords myNamedPointCoords; //!< Share the same instance with TMesh to implement nodal mapping
  };


  //---------------------------------------------------------------
  typedef            TVector<TMinMax>              TComp2MinMax;
  typedef       TVector<TComp2MinMax>       TMetric2Comp2MinMax;

  typedef     std::map<TName,TMinMax>             TGroup2MinMax;
  typedef      TVector<TGroup2MinMax>        TComp2Group2MinMax;
  typedef TVector<TComp2Group2MinMax> TMetric2Comp2Group2MinMax;

  //! Specialize TField to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TFieldImpl: virtual TField
  {
    TFieldImpl();

    //! To initialize the data structure    
    void 
    Init(vtkIdType theNbComp,
         vtkIdType theDataType);

    //! Gets type idetificator of the mesh data.
    vtkIdType
    GetDataType() const;

    //! Implement the TField::GetMinMax
    virtual
    TMinMax 
    GetMinMax(vtkIdType theCompID, const TNames& theGroupNames, TGaussMetric theGaussMetric = VISU::AVERAGE_METRIC);

    //! Implement the TField::GetAverageMinMax
    virtual
    TMinMax 
    GetAverageMinMax(vtkIdType theCompID, const TNames& theGroupNames, TGaussMetric theGaussMetric = VISU::AVERAGE_METRIC);

    vtkIdType myDataSize; //!< Keeps size of the assigned data
    vtkIdType myDataType; //!< Keeps type idetificator of the mesh data
    TMetric2Comp2MinMax myMetric2Comp2MinMax; //!< Keeps min/max values for each component of the MED FIELD
    TMetric2Comp2MinMax myMetric2Comp2AverageMinMax; //!< Keeps average by Gauss Points min/max values for each component of the MED FIELD
                                                     //!< If Nb of Gauss Points <=1 myAverageMinMaxArr equal myMinMaxArr

    TMetric2Comp2Group2MinMax myMetric2Comp2Group2MinMax;
    TMetric2Comp2Group2MinMax myMetric2Comp2Group2AverageMinMax;
  };


  //---------------------------------------------------------------
  typedef std::map<EGeometry, PMeshValue> TGeom2MeshValue;

  class TGeom2Value: public virtual TBaseStructure
  {
    TGeom2MeshValue myGeom2MeshValue;
  public:

    //! Gets mesh data for defined geometrical type (constant version)
    const PMeshValue& 
    GetMeshValue(EGeometry theGeom) const;

    //! Gets mesh data for defined geometrical type
    PMeshValue& 
    GetMeshValue(EGeometry theGeom);

    //! Gets container of the whole mesh data
    TGeom2MeshValue& 
    GetGeom2MeshValue();
    
    //! Gets container of the whole mesh data (constant version)
    const TGeom2MeshValue& 
    GetGeom2MeshValue() const;

    //! Gets mesh data for the first geometry
    PMeshValue
    GetFirstMeshValue() const;
  };


  //---------------------------------------------------------------
  typedef std::map<EGeometry,vtkIdType> TGeom2NbGauss;

  //! Specialize TValForTime to provide VTK mapping for the entity
  struct VISU_CONVERTOR_EXPORT TValForTimeImpl: virtual TValForTime
  {
    PGaussPtsIDFilter myGaussPtsIDFilter; //!< Keep VTK representation for mesh and data on Gauss Points
    PUnstructuredGridIDMapperImpl myUnstructuredGridIDMapper; //!< Keep VTK representation for ordinary mesh and data
    TGeom2Value myGeom2Value; //!< Keep value that is assigned to the mesh
    TGeom2NbGauss myGeom2NbGauss; //!< Keep number of Gauss Points
    bool myIsFilled;              //!< Keep the status of the TValForTime (true - already filled, false - not filled);
   
    TValForTimeImpl();

    TGeom2MeshValue& 
    GetGeom2MeshValue();
    
    const TGeom2MeshValue& 
    GetGeom2MeshValue() const;
    
    //! Get mesh data for defined geometrical type (constant version)
    const PMeshValue& 
    GetMeshValue(EGeometry theGeom) const;

    //! Get mesh data for defined geometrical type
    PMeshValue& 
    GetMeshValue(EGeometry theGeom);

    //! Gets mesh data for the first geometry
    PMeshValue
    GetFirstMeshValue() const;

    //! Get number of Gauss Points for defined geometrical type
    virtual
    int
    GetNbGauss(EGeometry theGeom) const;

    //! Get maximum number of Gauss Points among all geometrical types (provided for convenience)
    virtual
    int
    GetMaxNbGauss() const;

    //! Gets memory size used by the instance (bytes).
    virtual
    unsigned long int
    GetMemorySize();
  };


  //---------------------------------------------------------------
}


#endif

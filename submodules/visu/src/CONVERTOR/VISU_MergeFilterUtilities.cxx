// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header$
//
#include "VISU_MergeFilterUtilities.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkRectilinearGrid.h>
#include <vtkStructuredGrid.h>
#include <vtkStructuredPoints.h>
#include <vtkUnstructuredGrid.h>

#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkVersion.h>

#include <algorithm>
#include <vector>
#include <set>
#include <map>

#if !defined(VTK_XVERSION)
#define VTK_XVERSION (VTK_MAJOR_VERSION<<16)+(VTK_MINOR_VERSION<<8)+(VTK_BUILD_VERSION)
#endif

namespace
{

  using namespace VISU;
  
  void CopyVectorsOnCells(vtkDataSet *theVectorsDataSet,
                          vtkDataSet *theOutput)
  {
    vtkDataArray *anInputVectors = theVectorsDataSet->GetCellData()->GetVectors();
    vtkDataArray *anOutputVectors = vtkDataArray::CreateDataArray(anInputVectors->GetDataType());
    
    //Clear output vector data
    theOutput->GetCellData()->SetVectors(NULL);
    
    //Copy vectors data
    vtkIntArray* anOutputIDMapper = GetIDMapper(theOutput,
                                                TGetCellData(),
                                                "VISU_CELLS_MAPPER");
    
    vtkIntArray* anInputIDMapper = GetIDMapper(theVectorsDataSet,
                                               TGetCellData(),
                                               "VISU_CELLS_MAPPER");
    
    TObjectIdArray anIntersection;
    GetIntersection(anOutputIDMapper,
                    anInputIDMapper,
                    anIntersection);

    vtkIdType aNbTuples = anIntersection.size();
    anOutputVectors->SetNumberOfComponents(anInputVectors->GetNumberOfComponents());
    anOutputVectors->SetNumberOfTuples(aNbTuples);
    theOutput->GetCellData()->SetVectors(anOutputVectors);
    anOutputVectors->Delete();
    
    TObjectId2TupleIdMap anOutputObjectId2TupleIdMap;
    GetObjectId2TupleIdMap(anOutputIDMapper, anOutputObjectId2TupleIdMap);
    
    TObjectId2TupleIdMap anInputObjectId2TupleIdMap;
    GetObjectId2TupleIdMap(anInputIDMapper, anInputObjectId2TupleIdMap);

    for(vtkIdType iTupleId = 0; iTupleId < aNbTuples; iTupleId++ ){
      TObjectId &anObjectId = anIntersection[iTupleId];
      vtkIdType anOutputCellId  = anOutputObjectId2TupleIdMap[anObjectId];
      vtkIdType anInputCellId = anInputObjectId2TupleIdMap[anObjectId];
      anOutputVectors->SetTuple(anOutputCellId,anInputVectors->GetTuple(anInputCellId));
    }
  }
  
  void CopyVectorsOnPoints(vtkDataSet *theVectorsDataSet,
                          vtkDataSet *theOutput)
  {
    vtkDataArray *anInputVectors = theVectorsDataSet->GetPointData()->GetVectors();
    
    //Clear output vector data
    theOutput->GetPointData()->SetVectors(NULL);
    
    vtkDataArray *anOutputVectors = vtkDataArray::CreateDataArray(anInputVectors->GetDataType());
    
    //Copy vectors data
    vtkIntArray* anOutputIDMapper = GetIDMapper(theOutput,
                                                TGetPointData(),
                                                "VISU_POINTS_MAPPER");
    
    vtkIntArray* anInputIDMapper = GetIDMapper(theVectorsDataSet,
                                               TGetPointData(),
                                               "VISU_POINTS_MAPPER");
    TObjectIdArray anIntersection;

    GetIntersection(anOutputIDMapper,
                    anInputIDMapper,
                    anIntersection);
    
    vtkIdType aNbTuples = anIntersection.size();  
    anOutputVectors->SetNumberOfComponents(anInputVectors->GetNumberOfComponents());
    anOutputVectors->SetNumberOfTuples(aNbTuples);
    
    

    TObjectId2TupleIdMap anOutputObjectId2TupleIdMap;
    GetObjectId2TupleIdMap(anOutputIDMapper, anOutputObjectId2TupleIdMap);
    
    TObjectId2TupleIdMap anInputObjectId2TupleIdMap;
    GetObjectId2TupleIdMap(anInputIDMapper, anInputObjectId2TupleIdMap);
    
    for(vtkIdType iTupleId = 0; iTupleId < aNbTuples; iTupleId++ ){
      TObjectId& anObjectId = anIntersection[iTupleId];
      vtkIdType anOutputPointId  = anOutputObjectId2TupleIdMap[anObjectId];
      vtkIdType anInputPointId = anInputObjectId2TupleIdMap[anObjectId];
      anOutputVectors->SetTuple(anOutputPointId,anInputVectors->GetTuple(anInputPointId));
    }
    
    theOutput->GetPointData()->SetVectors(anOutputVectors);
    anOutputVectors->Delete();
  }
  
  
  //---------------------------------------------------------------
  template<class TDataSet>
  void 
  CopyDataOnCells(TDataSet *theInput,
                  vtkIntArray *theGeometryCellMapper,
                  vtkIntArray *theDataCellMapper,
                  vtkDataSet* theScalarsDataSet,
                  vtkDataSet* theVectorsDataSet,
                  vtkDataSet* theNormalsDataSet,
                  vtkDataSet* theTCoordsDataSet,
                  vtkDataSet* theTensorsDataSet,
                  VISU::TFieldList* theFieldList,
                  TDataSet *theOutput)
  {
    if(IsDifferent(theGeometryCellMapper, theDataCellMapper)){
      TObjectIdArray anIntersection;
      GetIntersection(theGeometryCellMapper,
                      theDataCellMapper,
                      anIntersection);
    
      TObjectId2TupleIdMap aGeomObjectId2TupleIdMap;
      GetObjectId2TupleIdMap(theGeometryCellMapper, aGeomObjectId2TupleIdMap);
      
      TObjectId2TupleIdMap aDataObjectId2TupleIdMap;
      GetObjectId2TupleIdMap(theDataCellMapper, aDataObjectId2TupleIdMap);
      
      vtkCellData *aCellData = theScalarsDataSet->GetCellData();
      vtkCellData *anOutputCellData = theOutput->GetCellData();
      anOutputCellData->CopyAllocate(aCellData);

      if(theVectorsDataSet && theVectorsDataSet != theScalarsDataSet)
        anOutputCellData->CopyVectorsOff();

      vtkIdType aNbTuples = anIntersection.size();
      theOutput->Allocate(aNbTuples);
      vtkIdList *aCellIds = vtkIdList::New();
      for(int aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
        TObjectId& anObjectId = anIntersection[aTupleId];
        vtkIdType aCellId = aGeomObjectId2TupleIdMap[anObjectId];
        vtkCell *aCell = theInput->GetCell(aCellId);
        aCellIds->Reset();
        vtkIdType aNbPointIds = aCell->PointIds->GetNumberOfIds();
        for(vtkIdType anId = 0; anId < aNbPointIds; anId++)
          aCellIds->InsertNextId(aCell->GetPointIds()->GetId(anId));
        vtkIdType aCellType = theInput->GetCellType(aCellId);
        vtkIdType aNewCellId = theOutput->InsertNextCell(aCellType, aCellIds);
        vtkIdType aDataCellId = aDataObjectId2TupleIdMap[anObjectId];
        anOutputCellData->CopyData(aCellData, aDataCellId, aNewCellId);
      }
      aCellIds->Delete();

      theOutput->SetPoints(theInput->GetPoints());
      
    }else{
      theOutput->CopyStructure(theInput);
      theOutput->GetCellData()->ShallowCopy(theScalarsDataSet->GetCellData());
    }
    theOutput->GetPointData()->ShallowCopy(theInput->GetPointData());
    
    //If need, copy vectors data.
    if(theVectorsDataSet && theVectorsDataSet != theScalarsDataSet){
      bool isVectorsOnCells = theVectorsDataSet->GetCellData()->GetVectors() != NULL;
      bool isVectorsDataOnPoints = theVectorsDataSet->GetPointData()->GetVectors() != NULL;
      if(isVectorsOnCells) {
        CopyVectorsOnCells(theVectorsDataSet,theOutput);
      }
      else if(isVectorsDataOnPoints){
        CopyVectorsOnPoints(theVectorsDataSet,theOutput);
      }
    }
  }
  
  //---------------------------------------------------------------
  template<class TDataSet>
  void 
  CopyDataOnPoints(TDataSet *theInput,
                   vtkIntArray *theGeometryPointMapper,
                   vtkIntArray *theDataPointMapper,
                   vtkDataSet* theScalarsDataSet,
                   vtkDataSet* theVectorsDataSet,
                   vtkDataSet* theNormalsDataSet,
                   vtkDataSet* theTCoordsDataSet,
                   vtkDataSet* theTensorsDataSet,
                   VISU::TFieldList* theFieldList,
                   TDataSet *theOutput)
  {
    if(!theGeometryPointMapper || !theDataPointMapper)
      return;
    if(IsDifferent(theGeometryPointMapper, theDataPointMapper)){
      TObjectId2TupleIdMap aDataObjectId2PointIdMap;
      GetObjectId2TupleIdMap(theDataPointMapper, aDataObjectId2PointIdMap);

      vtkCellData *aCellData = theInput->GetCellData();
      vtkCellData *anOutputCellData = theOutput->GetCellData();
      anOutputCellData->CopyAllocate(aCellData);

      vtkIdList *aCellIds = vtkIdList::New();
      int aNbCells = theInput->GetNumberOfCells();
      theOutput->Allocate(aNbCells);
	  if(!VISU::IsElnoData(theScalarsDataSet)) {
		  for(int aCellId = 0; aCellId < aNbCells; aCellId++){
			aCellIds->Reset();
			vtkCell *aCell = theInput->GetCell(aCellId);
			vtkIdType aNbPointIds = aCell->PointIds->GetNumberOfIds();
			for(vtkIdType anId = 0; anId < aNbPointIds; anId++){
			  vtkIdType aPointId = aCell->GetPointIds()->GetId(anId);
			  int* aPointer = theGeometryPointMapper->GetPointer(aPointId * 2);
			  TCellId aCellId = *aPointer;
			  TEntityId anEntityId = *(aPointer + 1);
			  TObjectId anObjectId(aCellId, anEntityId);
			  TObjectId2TupleIdMap::iterator anIter = aDataObjectId2PointIdMap.find(anObjectId);
			  if(anIter != aDataObjectId2PointIdMap.end()){
				aPointId = anIter->second;
				aCellIds->InsertNextId(aPointId);
			  }else
				goto PASS_INSERT_NEXT_CELL;
			}
			{
			  vtkIdType aCellType = theInput->GetCellType(aCellId);
			  vtkIdType aNewCellId = theOutput->InsertNextCell(aCellType, aCellIds);
			  anOutputCellData->CopyData(aCellData, aCellId, aNewCellId);
			}
		  PASS_INSERT_NEXT_CELL:
			continue;
		  }
		  aCellIds->Delete();
	  } else {
		vtkIntArray* anInputCellIDMapper = GetIDMapper(theInput,
													   TGetCellData(),
				                                       "VISU_CELLS_MAPPER");

		vtkIntArray* anScalarsCellIDMapper = GetIDMapper(theScalarsDataSet,
													     TGetCellData(),
				                                         "VISU_CELLS_MAPPER");

		vtkIntArray *anInputArray = dynamic_cast<vtkIntArray*>(anInputCellIDMapper);
		int aNbComp = anInputArray->GetNumberOfComponents();
		TCellId inCellObjId = -1;
		TEntityId inEntityId = -1;
		TCellId scalarCellVtkId = -1;
		vtkCell* inputCell = NULL;
		vtkCell* scalarCell = NULL;
				
		for(int aCellId = 0; aCellId < aNbCells; aCellId++) {
			aCellIds->Reset();
			int* anInPointer = anInputArray->GetPointer(aCellId*aNbComp);
			inCellObjId = *anInPointer;
			inEntityId = *(anInPointer+1);
			if(inCellObjId == -1) continue;
			scalarCellVtkId = VISU::GetVTKID(anScalarsCellIDMapper,inCellObjId,inEntityId);
			if(scalarCellVtkId == -1) continue;
			scalarCell = theScalarsDataSet->GetCell(scalarCellVtkId);
			inputCell = theInput->GetCell(aCellId);
			vtkIdType aCellType = inputCell->GetCellType();
			if(scalarCell->GetCellType() == aCellType) {
				aCellIds->DeepCopy(scalarCell->PointIds);							  
				vtkIdType aNewCellId = theOutput->InsertNextCell(aCellType, aCellIds);
				anOutputCellData->CopyData(aCellData, aCellId, aNewCellId);
			}				
		}
	  }
      
      // Copy geometry points
      // 1. Create vtkPoints instance of the same data type
      vtkPointSet* aScalarsDataSet = dynamic_cast<vtkPointSet*>(theScalarsDataSet);
      vtkPoints* anGeometryPoints = theInput->GetPoints();
      vtkPoints* aDataPoints = aScalarsDataSet->GetPoints();
      vtkPoints* anOutputPoints = vtkPoints::New(aDataPoints->GetDataType());
      theOutput->SetPoints(anOutputPoints);
      anOutputPoints->Delete();

      // 2. Perform mapping of geometry points
      TObjectId2TupleIdMap aGeomObjectId2TupleIdMap;
      GetObjectId2TupleIdMap(theGeometryPointMapper, aGeomObjectId2TupleIdMap);

      // 3. Loop over all data points
      int aNbDataPoints = theDataPointMapper->GetNumberOfTuples();
      anOutputPoints->SetNumberOfPoints(aNbDataPoints);
      for(int aPointId = 0; aPointId < aNbDataPoints; aPointId++){
        int* aPointer = theDataPointMapper->GetPointer(aPointId * 2);
        TCellId aCellId = *aPointer;
        TEntityId anEntityId = *(aPointer + 1);
        TObjectId anObjectId(aCellId, anEntityId);
        TObjectId2TupleIdMap::iterator anIter = aGeomObjectId2TupleIdMap.find(anObjectId);
        if(anIter != aGeomObjectId2TupleIdMap.end()){
          // If the point exists in the geometry put it to output
          int aGeometryPointId = anIter->second;
          double aCoords[3];
          anGeometryPoints->GetPoint(aGeometryPointId, aCoords);
          anOutputPoints->SetPoint(aPointId, aCoords);
        }else{
          // If no, the point from data should be used
          double aCoords[3];
          aDataPoints->GetPoint(aPointId, aCoords);
          anOutputPoints->SetPoint(aPointId, aCoords);
        }
      }
    }else{
      theOutput->CopyStructure(theInput);
      theOutput->GetCellData()->ShallowCopy(theInput->GetCellData());
    }
    theOutput->GetPointData()->ShallowCopy(theScalarsDataSet->GetPointData());
    
    //If need, copy vectors data.
    if(theVectorsDataSet && theVectorsDataSet != theScalarsDataSet){
      bool isVectorsOnCells = theVectorsDataSet->GetCellData()->GetVectors() != NULL;
      bool isVectorsDataOnPoints = theVectorsDataSet->GetPointData()->GetVectors() != NULL;

      //Merge cells if need
      //rnv
      if(!IsDifferent(theGeometryPointMapper, theDataPointMapper)){
        vtkIntArray* theGeometryCellMapper = GetIDMapper(theVectorsDataSet,
                                                         TGetCellData(),
                                                         "VISU_CELLS_MAPPER");
        
        vtkIntArray* theDataCellMapper = GetIDMapper(theScalarsDataSet,
                                                     TGetCellData(),
                                                     "VISU_CELLS_MAPPER");
        
          
        if(IsDifferent(theGeometryCellMapper, theDataCellMapper)){
          TObjectIdArray anIntersection;
          
          GetIntersection(theGeometryCellMapper,
                          theDataCellMapper,
                          anIntersection);
    
          TObjectId2TupleIdMap aGeomObjectId2TupleIdMap;
          GetObjectId2TupleIdMap(theGeometryCellMapper, aGeomObjectId2TupleIdMap);
      
          TObjectId2TupleIdMap aDataObjectId2TupleIdMap;
          GetObjectId2TupleIdMap(theDataCellMapper, aDataObjectId2TupleIdMap);
      
          vtkCellData *aCellData = theScalarsDataSet->GetCellData();
          vtkCellData *anOutputCellData = theOutput->GetCellData();
          anOutputCellData->CopyAllocate(aCellData);

          vtkIdType aNbTuples = anIntersection.size();
          theOutput->Allocate(aNbTuples);
          vtkIdList *aCellIds = vtkIdList::New();
          for(int aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
            TObjectId& anObjectId = anIntersection[aTupleId];
            vtkIdType aCellId = aGeomObjectId2TupleIdMap[anObjectId];
            vtkCell *aCell = theInput->GetCell(aCellId);
            aCellIds->Reset();
            vtkIdType aNbPointIds = aCell->PointIds->GetNumberOfIds();
            for(vtkIdType anId = 0; anId < aNbPointIds; anId++)
              aCellIds->InsertNextId(aCell->GetPointIds()->GetId(anId));
            vtkIdType aCellType = theInput->GetCellType(aCellId);
            vtkIdType aNewCellId = theOutput->InsertNextCell(aCellType, aCellIds);
            vtkIdType aDataCellId = aDataObjectId2TupleIdMap[anObjectId];
            anOutputCellData->CopyData(aCellData, aDataCellId, aNewCellId);
          }
          aCellIds->Delete();
          
        }
      }
        
      if(isVectorsOnCells) {
        CopyVectorsOnCells(theVectorsDataSet,theOutput);
      }
      else if(isVectorsDataOnPoints){
        CopyVectorsOnPoints(theVectorsDataSet,theOutput);
      }
    }
  }

  //---------------------------------------------------------------
  typedef vtkDataArray* (vtkDataSetAttributes::* TGetAttribute)();
#if (VTK_XVERSION < 0x050100)
  typedef int (vtkDataSetAttributes::* TSetAttribute)(vtkDataArray*);
#else
  typedef int (vtkDataSetAttributes::* TSetAttribute)(vtkAbstractArray*);
  typedef int (vtkDataSetAttributes::* TSetDataAttribute)(vtkDataArray*);
#endif


#if (VTK_XVERSION >= 0x050100)
  inline
  void
  CopyArray(vtkDataArray* theDataArray,
            vtkDataSetAttributes* theOutput, 
            TSetDataAttribute theSetAttribute,
            vtkIdType theFixedNbTuples)
  {
    if(theDataArray){
      vtkIdType aNbTuples = theDataArray->GetNumberOfTuples();
      if(theFixedNbTuples == aNbTuples)
        (theOutput->*theSetAttribute)(theDataArray);
    }
  }
#endif

  inline
  void
  CopyArray(vtkDataArray* theDataArray,
            vtkDataSetAttributes* theOutput, 
            TSetAttribute theSetAttribute,
            vtkIdType theFixedNbTuples)
  {
    if(theDataArray){
      vtkIdType aNbTuples = theDataArray->GetNumberOfTuples();
      if(theFixedNbTuples == aNbTuples)
        (theOutput->*theSetAttribute)(theDataArray);
    }
  }


  //---------------------------------------------------------------
  inline
  void
  CopyAttribute(vtkDataSetAttributes* theInput, 
                TGetAttribute theGetAttribute,
                vtkDataSetAttributes* theOutput, 
#if (VTK_XVERSION < 0x050100)
                TSetAttribute theSetAttribute,
#else
                TSetDataAttribute theSetAttribute,
#endif
                vtkIdType theFixedNbTuples)
  {
    CopyArray((theInput->*theGetAttribute)(),
              theOutput, theSetAttribute,
              theFixedNbTuples);
  }


  //---------------------------------------------------------------
  inline
  void
  CopyDataSetAttribute(vtkDataSet *theInput,
                       TGetAttribute theGetAttribute,
                       vtkDataSet *theOutput, 
#if (VTK_XVERSION < 0x050100)
                       TSetAttribute theSetAttribute,
#else
                       TSetDataAttribute theSetAttribute,
#endif
                       vtkIdType theNbPoints, 
                       vtkIdType theNbCells)
  {
    CopyAttribute(theInput->GetPointData(), 
                  theGetAttribute,
                  theOutput->GetPointData(), 
                  theSetAttribute,
                  theNbPoints);
    CopyAttribute(theInput->GetCellData(), 
                  theGetAttribute,
                  theOutput->GetCellData(), 
                  theSetAttribute,
                  theNbCells);
  }


  //---------------------------------------------------------------
  inline
  void
  CopyField(vtkDataSetAttributes* theInput, 
            const char* theFieldName, 
            vtkDataSetAttributes* theOutput,
            vtkIdType theFixedNbTuples)
  {
    CopyArray(theInput->GetArray(theFieldName),
              theOutput,
#if (VTK_XVERSION < 0x050100)
        &vtkDataSetAttributes::AddArray,
#else
              &vtkFieldData::AddArray,
#endif
              theFixedNbTuples);
  }


  //---------------------------------------------------------------
  inline
  void
  CopyDataSetField(vtkDataSet* theInput, 
                   const char* theFieldName, 
                   vtkDataSet* theOutput,
                   vtkIdType theNbPoints, 
                   vtkIdType theNbCells)
  {
    if(theInput){
      CopyField(theInput->GetPointData(), 
                theFieldName, 
                theOutput->GetPointData(), 
                theNbPoints);
      CopyField(theInput->GetCellData(), 
                theFieldName, 
                theOutput->GetCellData(), 
                theNbCells);
    }
  }

  //---------------------------------------------------------------
  void
  BasicExecute(vtkDataSet *theInput,
               vtkDataSet* theScalarsDataSet,
               vtkDataSet* theVectorsDataSet,
               vtkDataSet* theNormalsDataSet,
               vtkDataSet* theTCoordsDataSet,
               vtkDataSet* theTensorsDataSet,
               VISU::TFieldList* theFieldList,
               vtkDataSet *theOutput)
  {
    theOutput->CopyStructure(theInput);

    vtkIdType aNbPoints = theInput->GetNumberOfPoints();
    vtkIdType aNbCells = theInput->GetNumberOfCells();
  
    // merge data only if it is consistent
    if(theScalarsDataSet)
      CopyDataSetAttribute(theScalarsDataSet, 
                           &vtkDataSetAttributes::GetScalars,
                           theOutput,
                           &vtkDataSetAttributes::SetScalars,
                           aNbPoints,
                           aNbCells);

    if(theVectorsDataSet)
      CopyDataSetAttribute(theVectorsDataSet, 
                           &vtkDataSetAttributes::GetVectors,
                           theOutput, 
                           &vtkDataSetAttributes::SetVectors,
                           aNbPoints, 
                           aNbCells);

    if(theNormalsDataSet)
      CopyDataSetAttribute(theNormalsDataSet,
                           &vtkDataSetAttributes::GetNormals,
                           theOutput,
                           &vtkDataSetAttributes::SetNormals,
                           aNbPoints,
                           aNbCells);

    if(theTCoordsDataSet)
      CopyDataSetAttribute(theTCoordsDataSet,
                           &vtkDataSetAttributes::GetTCoords,
                           theOutput,
                           &vtkDataSetAttributes::SetTCoords,
                           aNbPoints, 
                           aNbCells);
    
    if(theTensorsDataSet)
      CopyDataSetAttribute(theTensorsDataSet, 
                           &vtkDataSetAttributes::GetTensors,
                           theOutput, 
                           &vtkDataSetAttributes::SetTensors,
                           aNbPoints, 
                           aNbCells);

    VISU::TFieldListIterator anIter(theFieldList);
    for(anIter.Begin(); !anIter.End() ; anIter.Next()){
      vtkDataSet *aDataSet = anIter.Get()->Ptr;
      const char* aFieldName = anIter.Get()->GetName();
      CopyDataSetField(aDataSet, 
                       aFieldName, 
                       theOutput, 
                       aNbPoints, 
                       aNbCells);
    }
  }


  //---------------------------------------------------------------
  template<class TDataSet>
  bool 
  Execute(TDataSet *theInput,
          vtkDataSet* theScalarsDataSet,
          vtkDataSet* theVectorsDataSet,
          vtkDataSet* theNormalsDataSet,
          vtkDataSet* theTCoordsDataSet,
          vtkDataSet* theTensorsDataSet,
          VISU::TFieldList* theFieldList,
          bool theIsMergingInputs,
          TDataSet *theOutput)
  {
    if(theIsMergingInputs){
      vtkCellData *aCellData = theInput->GetCellData();
      if(vtkDataArray *aCellMapper = aCellData->GetArray("VISU_CELLS_MAPPER")){
        bool anIsDataOnCells = false;
        if(vtkDataSet* aDataSet = theScalarsDataSet)
          if(vtkCellData* aCellData1 = aDataSet->GetCellData())
            anIsDataOnCells = (aCellData1->GetArray("VISU_FIELD") != NULL);
        if(anIsDataOnCells){
          vtkIntArray *aGeometryCellMapper = dynamic_cast<vtkIntArray*>(aCellMapper);
          vtkIntArray* aDataCellMapper = GetIDMapper(theFieldList,
                                                     TGetCellData(),
                                                     "VISU_CELLS_MAPPER");
          CopyDataOnCells(theInput,
                          aGeometryCellMapper,
                          aDataCellMapper,
                          theScalarsDataSet,
                          theVectorsDataSet,
                          theNormalsDataSet,
                          theTCoordsDataSet,
                          theTensorsDataSet,
                          theFieldList,
                          theOutput);
        }else{
          vtkPointData *aPointData = theInput->GetPointData();
          vtkDataArray *aPointMapper = aPointData->GetArray("VISU_POINTS_MAPPER");
          vtkIntArray *aGeometryPointMapper = dynamic_cast<vtkIntArray*>(aPointMapper);
          vtkIntArray* aDataPointMapper = GetIDMapper(theFieldList,
                                                      TGetPointData(),
                                                      "VISU_POINTS_MAPPER");
          CopyDataOnPoints(theInput,
                           aGeometryPointMapper,
                           aDataPointMapper,
                           theScalarsDataSet,
                           theVectorsDataSet,
                           theNormalsDataSet,
                           theTCoordsDataSet,
                           theTensorsDataSet,
                           theFieldList,
                           theOutput);
        }
      }
    }else{
      BasicExecute(theInput,
                   theScalarsDataSet,
                   theVectorsDataSet,
                   theNormalsDataSet,
                   theTCoordsDataSet,
                   theTensorsDataSet,
                   theFieldList,
                   theOutput);
    }
    return true;
  }
}

namespace VISU
{

  //---------------------------------------------------------------
  void
  GetObjectIdSet(vtkIntArray *theArray, 
                 TObjectIdSet& theObjectIdSet)
  {
    theObjectIdSet.clear();
    int aMaxId = theArray->GetMaxId();
    int* aPointer = theArray->GetPointer(0);
    int* anEndPointer = theArray->GetPointer(aMaxId + 1);
    for(; aPointer != anEndPointer; aPointer += 2){
      TCellId aCellId = *aPointer;
      TEntityId anEntityId = *(aPointer + 1);
      TObjectId anObjectId(aCellId, anEntityId);
      theObjectIdSet.insert(anObjectId);
    }
  }


  //---------------------------------------------------------------
  void
  GetObjectId2TupleIdMap(vtkIntArray *theArray, 
                        TObjectId2TupleIdMap& theObjectId2TupleIdMap)
  {
    theObjectId2TupleIdMap.clear();
    int* aPointer = theArray->GetPointer(0);
    int aNbTuples = theArray->GetNumberOfTuples();
    for(vtkIdType aTupleId = 0; aTupleId < aNbTuples; aTupleId++, aPointer += 2){
      TCellId aCellId = *aPointer;
      TEntityId anEntityId = *(aPointer + 1);
      TObjectId anObjectId(aCellId, anEntityId);
      theObjectId2TupleIdMap[anObjectId] = aTupleId;
    }
  }

  //---------------------------------------------------------------
  void 
  GetObjectId2TupleGaussIdArray(vtkIntArray *theArray, 
                                TObjectId2TupleGaussIdMap& theObjectId2TupleGaussIdMap)
  {
    theObjectId2TupleGaussIdMap.clear();
    int* aPointer = theArray->GetPointer(0);
    int aNbTuples = theArray->GetNumberOfTuples();
    for(vtkIdType aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
      int aCellId = *aPointer;
      TObjectId2TupleGaussIdMap::iterator it = theObjectId2TupleGaussIdMap.find(aCellId);
      if(it == theObjectId2TupleGaussIdMap.end()){
        TCellIdArray anIdArray;
        anIdArray.push_back(aTupleId);
        theObjectId2TupleGaussIdMap.insert(make_pair(aCellId,anIdArray));
      }
      else{
        (*it).second.push_back(aTupleId);
      }
      aPointer += 2;
    }
  }
  
  //---------------------------------------------------------------
  template<class TGetFieldData>
  vtkIntArray*
  GetIDMapper(VISU::TFieldList* theFieldList,
              TGetFieldData theGetFieldData,
              const char* theFieldName)
  {
    VISU::TFieldListIterator anIter(theFieldList);
    for(anIter.Begin(); !anIter.End() ; anIter.Next()){
      const char* aFieldName = anIter.Get()->GetName();
      if(strcmp(aFieldName, theFieldName) == 0){
        vtkDataSet* aDataSet = anIter.Get()->Ptr;
        vtkFieldData *aFieldData = theGetFieldData(aDataSet);
        vtkDataArray *anIDMapper = aFieldData->GetArray(theFieldName);
        return dynamic_cast<vtkIntArray*>(anIDMapper);
      }
    }
    return NULL;
  }
  // Explicit symbol export when compiling with g++ and optimizations,
  // needed by VISUConvertor during linking
  #if (__GNUG__ && __OPTIMIZE__)
  template vtkIntArray*
  GetIDMapper<TGetPointData>(TFieldList*, TGetPointData, const char* );
  #endif

  //---------------------------------------------------------------
  template<class TGetFieldData>
  vtkIntArray*
  GetIDMapper(vtkDataSet* theIDMapperDataSet,
              TGetFieldData theGetFieldData,
              const char* theFieldName)
  {
    vtkFieldData *aFieldData = theGetFieldData(theIDMapperDataSet);
    vtkDataArray *anIDMapper = aFieldData->GetArray(theFieldName);
    return dynamic_cast<vtkIntArray*>(anIDMapper);
  }


  //---------------------------------------------------------------
  bool
  IsDifferent(vtkIntArray *theFirstIDMapper,
              vtkIntArray *theSecondIDMapper)
  {
    vtkIdType aFirstNbTuples = theFirstIDMapper->GetNumberOfTuples();
    vtkIdType aSecondNbTuples = theSecondIDMapper->GetNumberOfTuples();
    if(aFirstNbTuples != aSecondNbTuples)
      return true;

    int aMaxId = theFirstIDMapper->GetMaxId();
    int* aFirstPointer = theFirstIDMapper->GetPointer(0);
    int* aSecondPointer = theSecondIDMapper->GetPointer(0);
    for(int anId = 0; anId <= aMaxId; anId++){
      if(*aFirstPointer++ != *aSecondPointer++)
        return true;
    }
    
    return false;
  }


  //---------------------------------------------------------------
  void
  GetIntersection(vtkIntArray *theFirstIDMapper,
                  vtkIntArray *theSecondIDMapper,
                  TObjectIdArray& theResult)
  {
    TObjectIdSet aFirstObjectIdSet;
    GetObjectIdSet(theFirstIDMapper, aFirstObjectIdSet);
    
    TObjectIdSet aSecondObjectIdSet;
    GetObjectIdSet(theSecondIDMapper, aSecondObjectIdSet);

    size_t aMaxLength = std::max(aFirstObjectIdSet.size(), aSecondObjectIdSet.size());
    theResult.resize(aMaxLength);
    TObjectIdArray::iterator anArrayIter = theResult.begin();
    anArrayIter = std::set_intersection(aFirstObjectIdSet.begin(),
                                        aFirstObjectIdSet.end(),
                                        aSecondObjectIdSet.begin(),
                                        aSecondObjectIdSet.end(),
                                        anArrayIter);
    theResult.erase(anArrayIter, theResult.end());
  }

  //---------------------------------------------------------------
  bool
  Execute(vtkUnstructuredGrid *theInput,
          vtkUnstructuredGrid *theOutput,
          vtkDataSet* theScalarsDataSet,
          vtkDataSet* theVectorsDataSet,
          vtkDataSet* theNormalsDataSet,
          vtkDataSet* theTCoordsDataSet,
          vtkDataSet* theTensorsDataSet,
          TFieldList* theFieldList,
          bool theIsMergingInputs)
  {
    return ::Execute(theInput,
                     theScalarsDataSet,
                     theVectorsDataSet,
                     theNormalsDataSet,
                     theTCoordsDataSet,
                     theTensorsDataSet,
                     theFieldList,
                     theIsMergingInputs,
                     theOutput);
  }


  //---------------------------------------------------------------
  bool
  Execute(vtkPolyData *theInput,
          vtkPolyData *theOutput,
          vtkDataSet* theScalarsDataSet,
          vtkDataSet* theVectorsDataSet,
          vtkDataSet* theNormalsDataSet,
          vtkDataSet* theTCoordsDataSet,
          vtkDataSet* theTensorsDataSet,
          TFieldList* theFieldList,
          bool theIsMergingInputs)
  {
    return ::Execute(theInput,
                     theScalarsDataSet,
                     theVectorsDataSet,
                     theNormalsDataSet,
                     theTCoordsDataSet,
                     theTensorsDataSet,
                     theFieldList,
                     theIsMergingInputs,
                     theOutput);
  }
}

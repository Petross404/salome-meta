// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:
//  Author:
//  Module : VISU
//
#include "VISU_IDMapper.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkDataSet.h>

namespace VISU
{
  //---------------------------------------------------------------
  TStructuredId
  ::TStructuredId()
  {
    myData[0] = myData[1] = myData[2] = -1;
  }

  vtkIdType&
  TStructuredId
  ::operator [] ( size_t theId )
  {
    return myData[ theId ];
  }

  const vtkIdType&
  TStructuredId
  ::operator [] ( size_t theId ) const
  {
    return myData[ theId ];
  }

  bool
  TStructuredId
  ::operator == ( const TStructuredId& theId ) const
  {
    return 
      theId[0] == myData[0] &&
      theId[1] == myData[1] &&
      theId[2] == myData[2];
  }

  //---------------------------------------------------------------
  void
  TStructured
  ::CopyStructure(PStructured theStructured)
  {
    myIsPolarType = theStructured->myIsPolarType;
    myIsStructured = theStructured->myIsStructured;
    myGrilleStructure = theStructured->myGrilleStructure;
    myObj2StructuredId = theStructured->myObj2StructuredId;    
  }

  bool
  TStructured
  ::IsStructured() const
  {
    return myIsStructured;
  }
  
  TStructuredId
  TStructured
  ::GetStructure()
  {
    return myGrilleStructure;
  }

  vtkIdType
  TStructured
  ::GetStructureDim()
  {
    return 
      vtkIdType( myGrilleStructure[0] >= 0) +
      vtkIdType( myGrilleStructure[1] >= 0) +
      vtkIdType( myGrilleStructure[2] >= 0);
  }

  TStructuredId
  TStructured
  ::GetIndexesOfNode(vtkIdType theNodeId)
  {
    if ( IsStructured() ) {
      TObj2StructuredId::const_iterator aIter = myObj2StructuredId.find( theNodeId );
      if ( aIter != myObj2StructuredId.end() )
        return aIter->second;
    }

    return TStructuredId();
  }
  
  vtkIdType
  TStructured
  ::GetObjectIDByIndexes(TStructuredId theVec)
  {
    TObj2StructuredId::const_iterator aIter = myObj2StructuredId.begin();
    for (; aIter != myObj2StructuredId.end(); aIter++ ) {
      if ( theVec == aIter->second )
        return aIter->first;
    }

    return -1;
  }


  //---------------------------------------------------------------
  double*  
  TIDMapper
  ::GetNodeCoord(vtkIdType theObjID)
  {
    vtkIdType aVTKID = GetNodeVTKID(theObjID);
    return GetOutput()->GetPoint(aVTKID);
  }

  vtkIdType
  TIDMapper
  ::GetNodeVTKID(vtkIdType theID) const
  {
    return theID;
  }

  vtkIdType
  TIDMapper
  ::GetNodeObjID(vtkIdType theID) const
  {
    return theID;
  }

  vtkCell* 
  TIDMapper
  ::GetElemCell(vtkIdType theObjID)
  {
    vtkIdType aVtkID = GetElemVTKID(theObjID);
    return GetOutput()->GetCell(aVtkID);
  }

  vtkIdType
  TIDMapper
  ::GetElemVTKID(vtkIdType theID) const
  {
    return theID;
  }

  vtkIdType
  TIDMapper
  ::GetElemObjID(vtkIdType theID) const
  {
    return theID;
  }  
  //---------------------------------------------------------------


  vtkDataSet* 
  TUnstructuredGridIDMapper
  ::GetOutput()
  {
    return GetUnstructuredGridOutput();
  }
  //---------------------------------------------------------------


  vtkDataSet* 
  TPolyDataIDMapper
  ::GetOutput()
  {
    return GetPolyDataOutput();
  }


  //---------------------------------------------------------------
}

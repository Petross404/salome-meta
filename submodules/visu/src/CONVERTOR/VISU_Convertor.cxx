// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:    VISU_Convertor.cxx
//  Author:  Alexey PETROV
//  Module : VISU
//
#include "VISU_Convertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <QString>

#include <utility>


//---------------------------------------------------------------
VISU_Convertor
::VISU_Convertor():
  myIsDone(false)
{}


//---------------------------------------------------------------
const std::string& 
VISU_Convertor
::GetName()
{
  return myName;
}


//---------------------------------------------------------------
int
VISU_Convertor
::IsDone() const 
{
  return myIsDone; 
}

//---------------------------------------------------------------
const VISU::TMeshMap& 
VISU_Convertor
::GetMeshMap() 
{ 
  return myMeshMap;
}


//---------------------------------------------------------------
std::string
VISU_Convertor
::GenerateName(const VISU::TTime& aTime)
{
  static QString aName;
  const std::string aUnits = aTime.second, tmp(aUnits.size(), ' ');
  if(aUnits == "" || aUnits == tmp)
    aName.sprintf("%g, -", aTime.first);
  else
    aName.sprintf("%g, %s", aTime.first, aTime.second.c_str());
  aName = aName.simplified();
  return (const char*)aName.toLatin1();
}


//---------------------------------------------------------------
std::string
VISU_Convertor
::GenerateName(const std::string& theName, 
               unsigned int theTimeId) 
{
  static QString aName;
  aName = QString(theName.c_str()).simplified();
  int iEnd = strlen((const char*)aName.toLatin1());
  static int VtkHighLevelLength = 12; //25
  if(iEnd > VtkHighLevelLength) iEnd = VtkHighLevelLength;
  char* aNewName = new char[iEnd+1];
  aNewName[iEnd] = '\0';
  strncpy(aNewName, (const char*)aName.toLatin1(), iEnd);
  std::replace(aNewName, aNewName + iEnd, ' ', '_');
  if(true || theTimeId == 0)
    aName = aNewName;
  else
    aName.sprintf("%s_%d",aNewName,theTimeId);
  delete[] aNewName;
  return (const char*)aName.toLatin1();
}


//---------------------------------------------------------------

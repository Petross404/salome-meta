// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header$
//
#include "VISU_GaussMergeFilter.hxx"
#include "VISU_MergeFilterUtilities.hxx"

#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkFloatArray.h>

#include <vtkExecutive.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkStreamingDemandDrivenPipeline.h>


//------------------------------------------------------------------------------
vtkStandardNewMacro(VISU_GaussMergeFilter);

//------------------------------------------------------------------------------
VISU_GaussMergeFilter
::VISU_GaussMergeFilter():
  myIsMergingInputs(false)
{
  this->FieldList = new VISU::TFieldList;
  this->SetNumberOfInputPorts(6);
}

//------------------------------------------------------------------------------
VISU_GaussMergeFilter::~VISU_GaussMergeFilter()
{
  delete this->FieldList;
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetGeometryConnection(vtkAlgorithmOutput *input)
{
  this->Superclass::SetInputConnection(input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetGeometryData(vtkDataSet *input)
{
  this->Superclass::SetInputData(input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetScalarsData(vtkDataSet *input)
{
  this->SetInputData(1, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetScalarsConnection(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(1, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetVectorsData(vtkDataSet *input)
{
  this->SetInputData(2, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetVectorsConnection(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(2, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetNormals(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(3, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetTCoords(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(4, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::SetTensors(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(5, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::AddField(const char* name, vtkDataSet* input)
{
  this->FieldList->Add(name, input);
}

//------------------------------------------------------------------------------
void VISU_GaussMergeFilter::RemoveFields()
{
  delete this->FieldList;
  this->FieldList = new VISU::TFieldList;
}


//---------------------------------------------------------------
void
VISU_GaussMergeFilter
::SetMergingInputs(bool theIsMergingInputs)
{
  if(myIsMergingInputs == theIsMergingInputs)
    return;

  myIsMergingInputs = theIsMergingInputs;
  Modified();
}

  
//---------------------------------------------------------------
bool
VISU_GaussMergeFilter
::IsMergingInputs()
{
  return myIsMergingInputs;
}
  

//---------------------------------------------------------------
int
VISU_GaussMergeFilter
::RequestData(vtkInformation *theRequest,
              vtkInformationVector **theInputVector,
              vtkInformationVector *theOutputVector)
{
  vtkInformation *inInfo0 = theInputVector[0]->GetInformationObject(0);
  vtkInformation *inInfo1 = theInputVector[1]->GetInformationObject(0);
  vtkInformation *outInfo = theOutputVector->GetInformationObject(0);

  // get the input and output
  vtkDataSet *anInput = vtkDataSet::SafeDownCast(inInfo0->Get(vtkDataObject::DATA_OBJECT()));
  vtkDataSet *aScalars = theInputVector[1]->GetNumberOfInformationObjects() > 0 ?
    vtkDataSet::SafeDownCast(inInfo1->Get(vtkDataObject::DATA_OBJECT())) : NULL;
  vtkDataSet *anOutput = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if(vtkUnstructuredGrid *anInputUG = dynamic_cast<vtkUnstructuredGrid*>(this->GetInput())){
    vtkPolyData *anOutputPD = dynamic_cast<vtkPolyData*>(this->GetOutput());
    return ExecuteGauss(anInputUG,
                        aScalars,
                        anOutputPD);
  }

  return Superclass::RequestData(theRequest,
                                 theInputVector,
                                 theOutputVector);
}

//----------------------------------------------------------------------------
//  Trick:  Abstract data types that may or may not be the same type
// (structured/unstructured), but the points/cells match up.
// Output/Geometry may be structured while ScalarInput may be 
// unstructured (but really have same triagulation/topology as geometry).
// Just request all the input. Always generate all of the output (todo).
int
VISU_GaussMergeFilter
::RequestUpdateExtent(vtkInformation *vtkNotUsed(request),
                      vtkInformationVector **inputVector,
                      vtkInformationVector *vtkNotUsed(outputVector))
{
  vtkInformation *inputInfo;
  int idx;
  
  for (idx = 0; idx < 6; ++idx)
    {
    inputInfo = inputVector[idx]->GetInformationObject(0);
    if (inputInfo)
      {
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
                     1);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(), 1);
      }
    }
  return 1;
}


//----------------------------------------------------------------------------
int
VISU_GaussMergeFilter
::FillInputPortInformation(int port, vtkInformation *info)
{
  int retval = this->Superclass::FillInputPortInformation(port, info);
  if (port > 0)
    {
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
    }
  return retval;
}

//----------------------------------------------------------------------------
int
VISU_GaussMergeFilter
::FillOutputPortInformation(int port, vtkInformation *info)
{
 info->Set(vtkDataObject::DATA_TYPE_NAME(),"vtkPolyData");
 return 1;
}

void
VISU_GaussMergeFilter
::SetGaussPtsIDMapper(const VISU::PGaussPtsIDMapper& theIDMapper)
{
  myGaussPtsIDMapper = theIDMapper;
}


const VISU::PGaussPtsIDMapper&  
VISU_GaussMergeFilter
::GetGaussPtsIDMapper()
{
  return myGaussPtsIDMapper;
}

bool 
VISU_GaussMergeFilter
::ExecuteGauss(vtkUnstructuredGrid* theInput,
               vtkDataSet*          theScalarsDataSet,
               vtkPolyData*         theOutput)
{
  if(IsMergingInputs()){
    vtkCellData *aCellData = theInput->GetCellData();
    if(vtkDataArray *aCellMapper = aCellData->GetArray("VISU_CELLS_MAPPER")){
      vtkIntArray *aGeometryCellMapper = dynamic_cast<vtkIntArray*>(aCellMapper);
      
      vtkIntArray* aDataPointMapper = GetIDMapper(FieldList,
                                                  VISU::TGetPointData(),
                                                  "VISU_POINTS_MAPPER");

      vtkIntArray* aDataCellIds = vtkIntArray::New();

      int nbPoints = aDataPointMapper->GetNumberOfTuples();
      aDataCellIds->SetNumberOfComponents(2);
      aDataCellIds->SetNumberOfTuples(nbPoints);
      int* aDataCellPointer = aDataCellIds->GetPointer(0);
      {
        int nbPoints = aDataPointMapper->GetNumberOfTuples();
        for(int i=0;i<nbPoints;i++,aDataCellPointer++){
          VISU::TGaussPointID aGPID = myGaussPtsIDMapper->GetObjID(i);
          vtkIdType aCellId = aGPID.first;
          *aDataCellPointer = aCellId;
          aDataCellPointer++;
          *aDataCellPointer = 3; // it's a entity CELL
        }
      }
      /*
      vtkIntArray* anCellArr = GetIDMapper(FieldList,
                                           VISU::TGetCellData(),
                                           "VISU_CELLS_MAPPER");
      vtkIntArray* anPMArr = GetIDMapper(FieldList,
                                         VISU::TGetPointData(),
                                         "VISU_POINTS_MAPPER");

      vtkDataArray* anFArr = GetIDMapper(FieldList,
                                         VISU::TGetPointData(),
                                         "VISU_FIELD");
      */
      if(VISU::IsDifferent(aDataCellIds, aGeometryCellMapper)){
        VISU::TObjectIdArray anIntersection;
        VISU::GetIntersection(aDataCellIds,
                              aGeometryCellMapper,
                              anIntersection);

        VISU::TObjectId2TupleGaussIdMap aDataCellId2TupleGaussIdMap;
        VISU::GetObjectId2TupleGaussIdArray(aDataCellIds, aDataCellId2TupleGaussIdMap);

        vtkIdType aNbTuples = 0;
        for(vtkIdType i = 0;i < anIntersection.size();i++)
          aNbTuples += aDataCellId2TupleGaussIdMap[anIntersection[i].first].size();
        
        vtkPointSet* aScalarsDataSet = dynamic_cast<vtkPointSet*>(theScalarsDataSet);
        vtkPoints* aDataPoints = aScalarsDataSet->GetPoints();
        vtkPoints* anOutputPoints = vtkPoints::New(aDataPoints->GetDataType());
        
        anOutputPoints->SetNumberOfPoints(aNbTuples);
        theOutput->SetPoints(anOutputPoints);
        anOutputPoints->Delete();
        
        vtkCellData*   anInputCellData  = aScalarsDataSet->GetCellData();
        vtkPointData* anInputPointData = aScalarsDataSet->GetPointData();

        theOutput->Allocate(aNbTuples);
        vtkCellData*  anOutputCellData  = theOutput->GetCellData();
        vtkPointData* anOutputPointData = theOutput->GetPointData();

        anOutputCellData->CopyAllocate(anInputCellData,aNbTuples);
        anOutputPointData->CopyAllocate(anInputPointData,aNbTuples);
        
        vtkIdList *aCellIds = vtkIdList::New();
        double aCoords[3];
        for(int aTupleId=0, aNewTupleId=0; aTupleId<anIntersection.size(); aTupleId++){
          VISU::TObjectId& anObjectId = anIntersection[aTupleId];
          VISU::TCellIdArray aCellIdArray = aDataCellId2TupleGaussIdMap[anObjectId.first];
          
          for(vtkIdType i = 0; i < aCellIdArray.size();i++) {
            vtkIdType aCellId = aCellIdArray[i];
            vtkCell *aCell = theScalarsDataSet->GetCell(aCellId);
            
            aCellIds->Reset();
            aCellIds->InsertNextId(aNewTupleId);
            aNewTupleId++;
          
            vtkIdType aCellType = theScalarsDataSet->GetCellType(aCellId);
            vtkIdType aNewCellId = theOutput->InsertNextCell(aCellType, aCellIds);
          
            anOutputCellData->CopyData(anInputCellData, aCellId, aNewCellId);
            anOutputPointData->CopyData(anInputPointData, aCellId, aNewCellId);

            aDataPoints->GetPoint(aCellId, aCoords);
            anOutputPoints->SetPoint(aNewCellId, aCoords);
          }
        }
      }
    }
  }
  return true;
}

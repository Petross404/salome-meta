// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header$
//
#include "VISU_MergeFilter.hxx"
#include "VISU_MergeFilterUtilities.hxx"

#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>

#include <vtkExecutive.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkStreamingDemandDrivenPipeline.h>


//------------------------------------------------------------------------------
vtkStandardNewMacro(VISU_MergeFilter);

//------------------------------------------------------------------------------
VISU_MergeFilter
::VISU_MergeFilter():
  myIsMergingInputs(false)
{
  this->FieldList = new VISU::TFieldList;
  this->SetNumberOfInputPorts(6);
}

//------------------------------------------------------------------------------
VISU_MergeFilter::~VISU_MergeFilter()
{
  delete this->FieldList;
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetGeometryConnection(vtkAlgorithmOutput *input)
{
  this->Superclass::SetInputConnection(input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetGeometryData(vtkDataSet *input)
{
  this->Superclass::SetInputData(input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetScalarsData(vtkDataSet *input)
{
  this->SetInputData(1, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetScalarsConnection(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(1, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetVectorsData(vtkDataSet *input)
{
  this->SetInputData(2, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetVectorsConnection(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(2, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetNormals(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(3, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetTCoords(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(4, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetTensors(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(5, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::AddField(const char* name, vtkDataSet* input)
{
  this->FieldList->Add(name, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::RemoveFields()
{
  delete this->FieldList;
  this->FieldList = new VISU::TFieldList;
}


//---------------------------------------------------------------
void
VISU_MergeFilter
::SetMergingInputs(bool theIsMergingInputs)
{
  if(myIsMergingInputs == theIsMergingInputs)
    return;

  myIsMergingInputs = theIsMergingInputs;
  Modified();
}

  
//---------------------------------------------------------------
bool
VISU_MergeFilter
::IsMergingInputs()
{
  return myIsMergingInputs;
}
  

//---------------------------------------------------------------
int
VISU_MergeFilter
::RequestData(vtkInformation *theRequest,
              vtkInformationVector **theInputVector,
              vtkInformationVector *theOutputVector)
{
  vtkInformation *inInfo0 = theInputVector[0]->GetInformationObject(0);
  vtkInformation *inInfo1 = theInputVector[1]->GetInformationObject(0);
  vtkInformation *inInfo2 = theInputVector[2]->GetInformationObject(0);
  vtkInformation *inInfo3 = theInputVector[3]->GetInformationObject(0);
  vtkInformation *inInfo4 = theInputVector[4]->GetInformationObject(0);
  vtkInformation *inInfo5 = theInputVector[5]->GetInformationObject(0);
  vtkInformation *outInfo = theOutputVector->GetInformationObject(0);

  // get the input and output
  vtkDataSet *anInput = vtkDataSet::SafeDownCast(inInfo0->Get(vtkDataObject::DATA_OBJECT()));
  vtkDataSet *aScalars = theInputVector[1]->GetNumberOfInformationObjects() > 0 ?
    vtkDataSet::SafeDownCast(inInfo1->Get(vtkDataObject::DATA_OBJECT())) : NULL;
  vtkDataSet *aVectors = theInputVector[2]->GetNumberOfInformationObjects() > 0 ?
    vtkDataSet::SafeDownCast(inInfo2->Get(vtkDataObject::DATA_OBJECT())) : NULL;
  vtkDataSet *aNormals = theInputVector[3]->GetNumberOfInformationObjects() > 0 ?
    vtkDataSet::SafeDownCast(inInfo3->Get(vtkDataObject::DATA_OBJECT())) : NULL;
  vtkDataSet *aTCoords = theInputVector[4]->GetNumberOfInformationObjects() > 0 ?
    vtkDataSet::SafeDownCast(inInfo4->Get(vtkDataObject::DATA_OBJECT())) : NULL;
  vtkDataSet *aTensors = theInputVector[5]->GetNumberOfInformationObjects() > 0 ?
    vtkDataSet::SafeDownCast(inInfo5->Get(vtkDataObject::DATA_OBJECT())) : NULL;
  vtkDataSet *anOutput = vtkDataSet::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if(vtkUnstructuredGrid *anInputUG = dynamic_cast<vtkUnstructuredGrid*>(anInput)){
    vtkUnstructuredGrid *anOutputUG = dynamic_cast<vtkUnstructuredGrid*>(anOutput);
    return VISU::Execute(anInputUG,
                         anOutputUG,
                         aScalars,
                         aVectors,
                         aNormals,
                         aTCoords,
                         aTensors,
                         this->FieldList,
                         IsMergingInputs());
  }else if(vtkPolyData *anInputPD = dynamic_cast<vtkPolyData*>(anInput)){
    vtkPolyData *anOutputPD =  dynamic_cast<vtkPolyData*>(anOutput);
    return VISU::Execute(anInputPD,
                         anOutputPD,
                         aScalars,
                         aVectors,
                         aNormals,
                         aTCoords,
                         aTensors,
                         this->FieldList,
                         IsMergingInputs());
  }

  return Superclass::RequestData(theRequest,
                                 theInputVector,
                                 theOutputVector);
}

//----------------------------------------------------------------------------
//  Trick:  Abstract data types that may or may not be the same type
// (structured/unstructured), but the points/cells match up.
// Output/Geometry may be structured while ScalarInput may be 
// unstructured (but really have same triagulation/topology as geometry).
// Just request all the input. Always generate all of the output (todo).
int
VISU_MergeFilter
::RequestUpdateExtent(vtkInformation *vtkNotUsed(request),
                      vtkInformationVector **inputVector,
                      vtkInformationVector *vtkNotUsed(outputVector))
{
  vtkInformation *inputInfo;
  int idx;
  
  for (idx = 0; idx < 6; ++idx)
    {
    inputInfo = inputVector[idx]->GetInformationObject(0);
    if (inputInfo)
      {
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
                     1);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(), 1);
      }
    }
  return 1;
}


//----------------------------------------------------------------------------
int
VISU_MergeFilter
::FillInputPortInformation(int port, vtkInformation *info)
{
  int retval = this->Superclass::FillInputPortInformation(port, info);
  if (port > 0)
    {
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
    }
  return retval;
}


// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : VISU_UsedPointsFilter.cxx
//  Author : 
//  Module : VISU
//
#include "VISU_UsedPointsFilter.hxx"

#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCell.h>
#include <vtkIdList.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

#include <map>

vtkStandardNewMacro(VISU_UsedPointsFilter);


VISU_UsedPointsFilter::VISU_UsedPointsFilter()
{
}

VISU_UsedPointsFilter::~VISU_UsedPointsFilter()
{
}


int VISU_UsedPointsFilter::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkDataSet *anInput = vtkDataSet::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkUnstructuredGrid *anOutput = vtkUnstructuredGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  typedef std::map<vtkIdType, vtkIdType> TId2IdMap;
  TId2IdMap aId2IdMap;

  vtkPointData *aPointData = anOutput->GetPointData();
  aPointData->CopyAllocate(anInput->GetPointData());

  vtkPoints* aUsedPoints = vtkPoints::New();
  vtkIdList *anIdList = vtkIdList::New();
  vtkIdType iEnd = anInput->GetNumberOfPoints();
  for(vtkIdType aPointId = 0; aPointId < iEnd; aPointId++){
    anInput->GetPointCells(aPointId,anIdList);
    if(anIdList->GetNumberOfIds() > 0){
      vtkIdType aNewPointId = aUsedPoints->InsertNextPoint(anInput->GetPoint(aPointId));
      aPointData->CopyData(anInput->GetPointData(), aPointId, aNewPointId);
      aId2IdMap[aPointId] = aNewPointId;
    }
  }
  aPointData->Squeeze();
  anOutput->SetPoints(aUsedPoints);
  aUsedPoints->Delete();
  anIdList->Delete();

  vtkCellData *aCellData = anOutput->GetCellData();
  aCellData->CopyAllocate(anInput->GetCellData());

  anOutput->Allocate(anInput->GetNumberOfCells()); 
  vtkIdList *anOldPointsIds = vtkIdList::New();
  vtkIdList *aNewPointsIds = vtkIdList::New();
  aNewPointsIds->Allocate(VTK_CELL_SIZE);
  iEnd = anInput->GetNumberOfCells();
  for(vtkIdType aCellId = 0; aCellId < iEnd; aCellId++){
    anInput->GetCellPoints(aCellId, anOldPointsIds);
    vtkIdType aNbPointsInCell = anOldPointsIds->GetNumberOfIds();
    aNewPointsIds->Reset();
    for(vtkIdType i = 0; i < aNbPointsInCell; i++){
      vtkIdType anOldId = anOldPointsIds->GetId(i);
      TId2IdMap::iterator anIter = aId2IdMap.find(anOldId);
      if(anIter == aId2IdMap.end())
        goto NEXT_CELL;
      vtkIdType aNewId = anIter->second;
      aNewPointsIds->InsertNextId(aNewId);
    }
    {
      vtkIdType aNewCellId = anOutput->InsertNextCell(anInput->GetCellType(aCellId), aNewPointsIds);
      aCellData->CopyData(anInput->GetCellData(), aCellId, aNewCellId);
    }
  NEXT_CELL:
    continue;
  }
  aCellData->Squeeze();
  anOldPointsIds->Delete();
  aNewPointsIds->Delete();
  anOutput->Squeeze();
  return 1;
}


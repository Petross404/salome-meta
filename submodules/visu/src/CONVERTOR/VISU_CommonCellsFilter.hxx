// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File      : VISU_CommonCellsFilter.hxx
// Created   : Tue Apr  3 16:16:53 2007
// Author    : Eugeny NIKOLAEV (enk)
//
#ifndef VISU_CommonCellsFilter_HeaderFile
#define VISU_CommonCellsFilter_HeaderFile

#include <vtkUnstructuredGridAlgorithm.h>

class VISU_CommonCellsFilter: public vtkUnstructuredGridAlgorithm
{
public:
  static VISU_CommonCellsFilter *New();
  vtkTypeMacro(VISU_CommonCellsFilter,vtkUnstructuredGridAlgorithm);

  // Description:
  // Specify the Unstructured Grid which overview
  // nodal profile.
  void SetProfileUG(vtkAlgorithmOutput *input);

  // Description:
  // Specify the Unstructured Grid which overview
  // cells data on CELL_ENTITY.
  void SetCellsUG(vtkAlgorithmOutput *input);

protected:
  VISU_CommonCellsFilter();
  ~VISU_CommonCellsFilter();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *); //generate output data

private:
  VISU_CommonCellsFilter(const VISU_CommonCellsFilter&); // Lock copy
  void operator=(const VISU_CommonCellsFilter&); // Lock copy
};

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  File   : VISU_Vtk2MedConvertor.hxx
//  Author : Eugeny NIKOLAEV, Open CASCADE SAS
//
#ifndef VISU_VTK2MEDCONVERTOR_H
#define VISU_VTK2MEDCONVERTOR_H

#include "VISUConvertor.hxx"

#include <MED_Common.hxx>
#include <vtkIntArray.h>
#include <vtkDataSet.h>

#include <vector>
#include <map>
#include <string>

class vtkUnstructuredGrid;
class vtkIntArray;

class VISU_CONVERTOR_EXPORT VISU_Vtk2MedConvertor
{
public:

  typedef std::vector<std::string>                  TVectorString;
  typedef std::vector<double>                       TVectorDouble;
  typedef std::vector<int>                          TCellIds;
  typedef std::map<MED::EGeometrieElement,TCellIds> TGeom2CellIds;
  
  VISU_Vtk2MedConvertor();
  
  VISU_Vtk2MedConvertor( const std::string theMEDFileName,
                         const std::string theFirstVTKFileName );
  
  VISU_Vtk2MedConvertor( const std::string theMEDFileName,
                         const std::string theFirstVTKFileName,
                         const TVectorString theDataVTKFileNames );
  
  
  void               setMEDFileName( const std::string theFileName );
  std::string        getMEDFileName() const;
  
  void               setFirstVTKFileName( const std::string theFileName );
  std::string        getFirstVTKFileName() const;
  
  void               setDataVTKFileNames( const TVectorString theFileNames );
  void               getDataVTKFileNames( TVectorString& ) const;

  void               setVersion( const MED::EVersion theVersion );
  MED::EVersion      getVersion() const;

  void               setMeshName( const std::string theMeshName );
  std::string        getMeshName() const;

  void               addToIgnoringFieldList( const std::string& theFieldName );
  void               eraseFromIgnoringFieldList( const std::string& theFieldName );
  const std::set<std::string>& getIgnoringFieldList() const;

  void               setCellDataFieldNameIDS( const std::string& theFieldName );
  const std::string& getCellDataFieldNameIDS() const;
    
  void               setPointDataFieldNameIDS( const std::string& theFieldName );
  const std::string& getPointDataFieldNameIDS() const;

  void               setTimeStamps( const TVectorDouble& theTStamps );
  void               getTimeStamps( TVectorDouble& theTStamps ) const;
    
  int                Execute();
    
protected:
  
  TVectorString         myDataVTKFileNames; // vtk files with data
  std::string           myMEDFileName; // output MED file name.
  std::string           myFirstVTKFileName;  // vtk file with geometry,data from this file also used.
  MED::EVersion         myVersion;
  std::string           myMeshName;
  std::set<std::string> myIgnoringFieldList;
  std::string           myCellDataFieldNameIDS;
  std::string           myPointDataFieldNameIDS;
  TVectorDouble         myTStamps;
  
private:

  // Fill container with indices of cells which match given type.
  void
  GetIdsOfCellsOfType( vtkDataSet* theInput, // input
                       const int type, // input
                       vtkIntArray *array ); // output

  
  // ret value 0 - OK
  // ret value 1 - ERROR
  int
  Geometry2MED( vtkDataSet* aInput,
                MED::PWrapper myMed,
                MED::PMeshInfo aMeshInfo,
                TGeom2CellIds& outGeom2CellIdMap );

  // ret value 0 - OK
  // ret value 1 - ERROR
  int
  Data2MED( std::vector<vtkDataSet*> theListForAdd,
            MED::PWrapper myMed,
            MED::PMeshInfo theMeshInfo,
            TGeom2CellIds& theGeom2CellIdMap );

  // ret value 0 - OK
  // ret value 1 - ERROR
  int
  CreateElements( vtkDataSet* theInput,
                  MED::PMeshInfo theMeshInfo,
                  MED::PWrapper  theMed,
                  vtkIntArray* theCellsMapper,
                  MED::EEntiteMaillage theEntity,
                  int theVTKGeom,
                  int nbPointsInGeom,
                  std::vector<int>& theNumberingConvertor,
                  TGeom2CellIds& theGeom2CellIdMap );

  // ret value 0 - OK
  // ret value 1 - ERROR
  int
  CreatePolygons( vtkDataSet* theInput,
                  MED::PMeshInfo theMeshInfo,
                  MED::PWrapper  theMed,
                  vtkIntArray* theCellsMapper,
                  MED::EEntiteMaillage theEntity,
                  TGeom2CellIds& theGeom2CellIdMap );

  // ret value 0 - OK
  // ret value 1 - ERROR
  int
  CreatePolyedres( vtkDataSet* theInput,
                   MED::PMeshInfo theMeshInfo,
                   MED::PWrapper  theMed,
                   vtkIntArray* theCellsMapper,
                   MED::EEntiteMaillage theEntity,
                   TGeom2CellIds& theGeom2CellIdMap );
  
};

#endif // VISU_VTK2MEDCONVERTOR_H

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File      : VISU_CommonCellsFilter.cxx
// Created   : Wed Apr  4 08:45:07 2007
// Author    : Eugeny NIKOLAEV (enk)
//
#include "VISU_CommonCellsFilter.hxx"
#include "VISU_ConvertorDef.hxx"

// VTK product headers
#include <vtkUnstructuredGrid.h>
#include <vtkSetGet.h>
#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkCellTypes.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkIdList.h>
#include <vtkFloatArray.h>
#include <vtkCell.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

// STL
#include <algorithm>
#include <vector>
#include <map>
#include <set>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

// TTimerLog
#include "VISU_ConvertorUtils.hxx"

namespace
{
  typedef std::pair<int,int> TPair;// pair first - object id, second - entity
  //
  typedef std::vector<int>  TSortedArrayOne;
  typedef std::set<TPair>   TSortedArrayPair;
  typedef std::set<int>     TIdSet;
  typedef std::map<int,int> TId2IdMap;

  inline
  void
  GetSortedArrayAsPair(vtkIntArray *theArray, 
                       TSortedArrayPair& theSortedArray)
  {
    TSortedArrayPair aSortedArray;
    int nbComp = theArray->GetNumberOfComponents();
    if(nbComp == 2){
      int aMaxId = theArray->GetNumberOfTuples()*theArray->GetNumberOfComponents();
      int* aPointer = theArray->GetPointer(0);
      int* anEndPointer = theArray->GetPointer(aMaxId + 1);
      for(;aPointer<anEndPointer;){
        TPair aPair;
        aPair.first = *aPointer;
        aPointer++;
        aPair.second = *aPointer;
        aPointer++;
        aSortedArray.insert(aPair);
      }
    } else if (nbComp == 1) {
      int aMaxId = theArray->GetNumberOfTuples();
      int* aPointer = theArray->GetPointer(0);
      int* anEndPointer = theArray->GetPointer(aMaxId + 1);
      for(;aPointer<anEndPointer;){
        TPair aPair;
        aPair.first = *aPointer;
        aPointer++;
        aPair.second = (int)VISU::NODE_ENTITY;
        aSortedArray.insert(aPair);
      }
      
    }
    theSortedArray.swap(aSortedArray);
  }

  inline
  void
  GetSortedArrayOne(vtkIntArray *theArray, 
                    TSortedArrayOne& theSortedArray)
  {
    int aMaxId = theArray->GetMaxId();
    int* aPointer = theArray->GetPointer(0);
    int* anEndPointer = theArray->GetPointer(aMaxId + 1);
    TSortedArrayOne aSortedArray(aPointer, anEndPointer);
    std::sort(aSortedArray.begin(), aSortedArray.end());
    theSortedArray.swap(aSortedArray);
  }

  inline
  void
  GetIdsForCopy(vtkUnstructuredGrid *inputUGrid, 
                vtkIntArray* inputPointIds,
                TSortedArrayOne& outputSortedArray)
  {
    if(inputUGrid){
      TSortedArrayOne aSortedPointIds;
      TSortedArrayOne aOutputCellIds;
      TIdSet aMapForSearch;
      int nbTuples = inputPointIds->GetNumberOfTuples();
      int nbComp = inputPointIds->GetNumberOfComponents();
      int * aPtr = inputPointIds->GetPointer(0);
      int * aPtrEnd = inputPointIds->GetPointer(nbTuples*nbComp+1);
      if(nbComp == 1)
        while(aPtr<aPtrEnd){
          aMapForSearch.insert(*aPtr);
          aPtr++;
        }
      else if (nbComp == 2)
        while(aPtr<aPtrEnd){
          aMapForSearch.insert(*aPtr);
          aPtr++;aPtr++;
        }
      int nbInputCells = inputUGrid->GetNumberOfCells();

      for(int idCell=0;idCell<nbInputCells;idCell++){
        vtkCell*   aCell = inputUGrid->GetCell(idCell);
        vtkIdList* ptIds = aCell->GetPointIds();
        int nbPointsInCell = ptIds->GetNumberOfIds();
        bool aGoodCell = true;
        for(int i=0;i<nbPointsInCell;i++){
          int aSearchingId = ptIds->GetId(i);
          TIdSet::iterator aResult = aMapForSearch.find(aSearchingId);
          if(aResult == aMapForSearch.end()){
            aGoodCell = false;
            break;
          }
        }
        if(aGoodCell)
          aOutputCellIds.push_back(idCell);
        else
          continue;
        
      }

      outputSortedArray.swap(aOutputCellIds);
    }
  }

  inline
  void
  CopyElementsToOutput(vtkUnstructuredGrid* theInputUG,
                       int& theNbElements,
                       TSortedArrayOne& theElementIdsForCopy,
                       TId2IdMap& theOldId2NewIdPointsMap,
                       vtkUnstructuredGrid* theOutputUG)
  {
    vtkIntArray* theOuputIDSArray = vtkIntArray::New();
    theOuputIDSArray->SetName("VISU_CELLS_MAPPER");
    theOuputIDSArray->SetNumberOfComponents(2);
    theOuputIDSArray->SetNumberOfTuples(theNbElements);
    int* aOuputIDSPtr = theOuputIDSArray->GetPointer(0);
    
    vtkIntArray* aInputCellsMapper =
      dynamic_cast<vtkIntArray*>(theInputUG->GetCellData()->GetArray("VISU_CELLS_MAPPER"));
    int* aInputCellsMapperPointer = aInputCellsMapper->GetPointer(0);
    for(int aCellIndex=0;aCellIndex<theNbElements;aCellIndex++){
      int aCellId = theElementIdsForCopy[aCellIndex];
      vtkIdList* aOldPointIds = theInputUG->GetCell(aCellId)->GetPointIds();
      vtkIdList* aNewPointIds = vtkIdList::New();
      int nbPointIds = aOldPointIds->GetNumberOfIds();
      aNewPointIds->SetNumberOfIds(nbPointIds);
      for(int j=0;j<nbPointIds;j++){
        int aOldId = aOldPointIds->GetId(j);
        int aNewId = theOldId2NewIdPointsMap[aOldId];
        aNewPointIds->SetId(j,aNewId);
      }
      const int aOldCellId = theElementIdsForCopy[aCellIndex];
      theOutputUG->InsertNextCell(theInputUG->GetCellType(aOldCellId),
                                  aNewPointIds);

      *aOuputIDSPtr = aInputCellsMapperPointer[2*aOldCellId];
      aOuputIDSPtr++;
      *aOuputIDSPtr = aInputCellsMapperPointer[2*aOldCellId+1];
      aOuputIDSPtr++;
      
      aNewPointIds->Delete();
    }

    theOutputUG->GetCellData()->AddArray(theOuputIDSArray);
    
    theOuputIDSArray->Delete();
  }
}

vtkStandardNewMacro(VISU_CommonCellsFilter);

VISU_CommonCellsFilter
::VISU_CommonCellsFilter()
{
  this->SetNumberOfInputPorts(1);
}

VISU_CommonCellsFilter
::~VISU_CommonCellsFilter()
{}

void
VISU_CommonCellsFilter
::SetProfileUG(vtkAlgorithmOutput *input)
{
  this->SetInputConnection(0, input);
}

void
VISU_CommonCellsFilter
::SetCellsUG(vtkAlgorithmOutput *input)
{
  this->SetNumberOfInputPorts(2);
  this->SetInputConnection(1, input);
}

int
VISU_CommonCellsFilter
::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  VISU::TTimerLog aTimerLog(MYDEBUG,"VISU_CommonCellsFilter::Execute");

  vtkInformation *inInfo1 = inputVector[0]->GetInformationObject(0);
  vtkUnstructuredGrid *anInputProfileUG = vtkUnstructuredGrid::SafeDownCast(
    inInfo1->Get(vtkDataObject::DATA_OBJECT()));

  vtkUnstructuredGrid *anInputCellsUG = NULL;
  if( this->GetNumberOfInputPorts() > 1 )
  {
    vtkInformation *inInfo2 = inputVector[1]->GetInformationObject(0);
    anInputCellsUG = vtkUnstructuredGrid::SafeDownCast(
      inInfo2->Get(vtkDataObject::DATA_OBJECT()));
  }

  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkUnstructuredGrid *anOutput = vtkUnstructuredGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  if(anInputCellsUG == NULL){
    anOutput->ShallowCopy(anInputProfileUG);
  }
  else{
    // check if anInputProfileUG already have cells types not equal VTK_VERTEX
    vtkCellTypes* aCellTypes = vtkCellTypes::New();

    anInputProfileUG->GetCellTypes(aCellTypes);
    if(aCellTypes){
      if (aCellTypes->GetNumberOfTypes()!=1 )
        anOutput->ShallowCopy(anInputProfileUG);
      else{
        if(aCellTypes->GetCellType(0) != VTK_VERTEX)
          anOutput->DeepCopy(anInputProfileUG);
        else{

          vtkCellData* aInputCellData = anInputProfileUG->GetCellData();

          //
          // Calculate output points
          //
          vtkIdList* aPointIdsForCopy = vtkIdList::New();
          vtkPoints* aOutputPointSet = vtkPoints::New();
          TId2IdMap  aOldId2NewIdPointsMap;
          
          aOutputPointSet->Reset();
          
          vtkIntArray* aPointIDS =
            dynamic_cast<vtkIntArray*>(aInputCellData->GetArray("VISU_CELLS_MAPPER"));
          if(aPointIDS){
            int* aPtr = aPointIDS->GetPointer(0);
            aPointIdsForCopy->SetNumberOfIds(aPointIDS->GetNumberOfTuples());
            for(int i=0;i<aPointIDS->GetNumberOfTuples();i++){
              aPointIdsForCopy->SetId(i,*aPtr);
              aPtr++;aPtr++;
            }
            aOutputPointSet->SetNumberOfPoints(aPointIdsForCopy->GetNumberOfIds());
            // aOutputPointSet copy points from anInputProfileUG to aOutputPointSet, which
            // in aPointIdsForCopy ids list
            anInputProfileUG->GetPoints()->GetPoints(aPointIdsForCopy,aOutputPointSet);
            for(int i=0;i<aPointIdsForCopy->GetNumberOfIds();i++)
              aOldId2NewIdPointsMap[aPointIdsForCopy->GetId(i)] = i;
            anOutput->SetPoints(aOutputPointSet);
          }
          aOutputPointSet->Delete();
          // applay scalar,vector,normal,tensor ... values
          anOutput->GetPointData()->CopyFieldOff("VISU_CELLS_MAPPER");
          anOutput->GetPointData()->CopyFieldOff("VISU_POINTS_MAPPER");
          anOutput->GetPointData()->PassData(aInputCellData);
          //anOutput->GetPointData()->GetArray("VISU_CELLS_MAPPER")->SetName("VISU_POINTS_MAPPER");

          // apply VISU_POINTS_MAPPER
          int anEntity = int(VISU::NODE_ENTITY);
          vtkIntArray*  aNewPointsIdsArray = vtkIntArray::New();
          aNewPointsIdsArray->SetName("VISU_POINTS_MAPPER");
          aNewPointsIdsArray->SetNumberOfComponents(2);
          aNewPointsIdsArray->SetNumberOfTuples(aPointIdsForCopy->GetNumberOfIds());
          int *aPtr = aNewPointsIdsArray->GetPointer(0);
          for(int i = 0; i < aPointIdsForCopy->GetNumberOfIds(); i++){
            *aPtr++ = aPointIdsForCopy->GetId(i);
            *aPtr++ = anEntity;
          }
          anOutput->GetPointData()->AddArray(aNewPointsIdsArray);

          
            
          aNewPointsIdsArray->Delete();
          
          
          // Calculate output cells
          int nbCells=0;

          TSortedArrayOne aCellIdsForCopy;
          
          GetIdsForCopy(anInputCellsUG,aPointIDS,aCellIdsForCopy);
          nbCells = aCellIdsForCopy.size();

          // copy cells to output
          int aAllocMem = nbCells;
          anOutput->Allocate(aAllocMem);

          if(nbCells>0 && anInputCellsUG)
            CopyElementsToOutput(anInputCellsUG,
                                 nbCells,
                                 aCellIdsForCopy,
                                 aOldId2NewIdPointsMap,
                                 anOutput);
          
          
          aPointIdsForCopy->Delete();
        }
      }
      
    }
    else
      anOutput->ShallowCopy(anInputProfileUG);
  }
  return 1;
}

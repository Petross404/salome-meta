// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:
//  Author:
//  Module : VISU
//
#include "VISU_MeshValue.hxx"
#include "VISU_ElnoMeshValue.hxx"
#include "VISU_Structures_impl.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "VISU_PointCoords.hxx"
#include "VISU_VTKTypeList.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>

#include <vtkPointData.h>
#include <vtkCellData.h>

#include <vtkCharArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkShortArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkIntArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkLongArray.h>
#include <vtkUnsignedLongArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>

#include <string>
#include <algorithm>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

namespace VISU
{
  //---------------------------------------------------------------
  std::string
  GenerateFieldName(const PFieldImpl& theField,
                    const PValForTimeImpl& theValForTime)
  {
    const VISU::TTime& aTime = theValForTime->myTime;
    std::string aFieldName = theField->myMeshName + ", " + theField->myName + ": " +
      VISU_Convertor::GenerateName(aTime);
    return aFieldName;
  }


  //---------------------------------------------------------------
  void
  TMeshValueBase
  ::Init(vtkIdType theNbElem,
         vtkIdType theNbGauss,
         vtkIdType theNbComp)
  {
    myNbElem = theNbElem;
    myNbGauss = theNbGauss;
    myNbComp = theNbComp;
    myStep = theNbComp*theNbGauss;
  }

  vtkIdType
  TMeshValueBase
  ::GetNbElem() const
  {
    return myNbElem;
  }

  vtkIdType
  TMeshValueBase
  ::GetNbComp() const
  {
    return myNbComp;
  }

  vtkIdType
  TMeshValueBase
  ::GetNbGauss() const
  {
    return myNbGauss;
  }

  size_t
  TMeshValueBase
  ::size() const
  {
    return myNbElem * myStep;
  }


  //----------------------------------------------------------------------------
  template<int EDataType>
  void
  InitTimeStampOnProfile(const PUnstructuredGrid& theSource,
                         const PFieldImpl& theField,
                         const PValForTimeImpl& theValForTime,
                         const VISU::TEntity& theEntity);


  //----------------------------------------------------------------------------
  void
  GetTimeStampOnProfile(const PUnstructuredGrid& theSource,
                        const PFieldImpl& theField,
                        const PValForTimeImpl& theValForTime,
                        const VISU::TEntity& theEntity)
  {
    vtkIdType aDataType = theField->GetDataType();
    switch(aDataType){
    case VTK_DOUBLE:
      InitTimeStampOnProfile<VTK_DOUBLE>(theSource, theField, theValForTime, theEntity);
      break;
    case VTK_FLOAT:
      InitTimeStampOnProfile<VTK_FLOAT>(theSource, theField, theValForTime, theEntity);
      break;
    case VTK_INT:
      InitTimeStampOnProfile<VTK_INT>(theSource, theField, theValForTime, theEntity);
      break;
    case VTK_LONG:
      InitTimeStampOnProfile<VTK_LONG>(theSource, theField, theValForTime, theEntity);
      break;
    default:
      EXCEPTION(std::runtime_error,
                "GetTimeStampOnProfile - handling unsupported data type - "<<aDataType);
    }
  }


  //----------------------------------------------------------------------------
  template<int EDataType>
  struct TDataArrayHolder
  {
    typedef typename TL::TEnum2VTKArrayType<EDataType>::TResult TVTKDataArray;
    typedef typename TL::TEnum2VTKBasicType<EDataType>::TResult TVTKBasicType;
    TVTKDataArray* myDataArray;

    TDataArrayHolder(TVTKDataArray* theDataArray):
      myDataArray(theDataArray)
    {}

    void
    WritePointer(TVTKDataArray* theDataArray,
                 vtkIdType theTupleId,
                 TVTKBasicType* thePointer)
    {
      vtkIdType aNumberOfComponents = theDataArray->GetNumberOfComponents();
      vtkIdType aPosition = theTupleId * aNumberOfComponents;
      TVTKBasicType *aPtr = theDataArray->WritePointer(aPosition, aNumberOfComponents);
      for(vtkIdType anId = 0; anId < aNumberOfComponents; anId++)
        *aPtr++ = *thePointer++;
    }

    virtual
    void
    SetTuple(vtkIdType theTupleId,
             TVTKBasicType* thePointer)
    {
      this->WritePointer(myDataArray, theTupleId, thePointer);
    }
  };


  //----------------------------------------------------------------------------
  template<int EDataType>
  struct TDataArrayHolder2: TDataArrayHolder<EDataType>
  {
    typedef TDataArrayHolder<EDataType> TSuperClass;
    typedef typename TSuperClass::TVTKDataArray TVTKDataArray;
    typedef typename TSuperClass::TVTKBasicType TVTKBasicType;
    TVTKDataArray* myDataArray2;

    TDataArrayHolder2(TVTKDataArray* theDataArray,
                      TVTKDataArray* theDataArray2):
      TSuperClass(theDataArray),
      myDataArray2(theDataArray2)
    {}

    virtual
    void
    SetTuple(vtkIdType theTupleId,
             TVTKBasicType* thePointer)
    {
      this->WritePointer(this->myDataArray, theTupleId, thePointer);
      this->WritePointer(this->myDataArray2, theTupleId, thePointer);
    }
  };


  //----------------------------------------------------------------------------
  template<int EDataType>
  struct TTimeStampOnProfileInitArray
  {
    typedef typename TL::TEnum2VTKArrayType<EDataType>::TResult TVTKDataArray;
    typedef typename TL::TEnum2VTKBasicType<EDataType>::TResult TVTKBasicType;
    typedef TTMeshValue<TVTKBasicType> TMeshValue;
    typedef MED::SharedPtr<TMeshValue> TMeshValuePtr;

    typedef TDataArrayHolder<EDataType> TTDataArrayHolder;
    typedef MED::SharedPtr<TTDataArrayHolder> PDataArrayHolder;
    PDataArrayHolder myDataArrayHolder;

    TTimeStampOnProfileInitArray(const PDataArrayHolder& theDataArrayHolder):
      myDataArrayHolder(theDataArrayHolder)
    {}

    void
    Execute(const PFieldImpl& theField,
            const PValForTimeImpl& theValForTime,
            const TGaussMetric theGaussMetric = VISU::AVERAGE_METRIC)
    {
      vtkIdType aNbComp = theField->myNbComp;
      vtkIdType aSize = std::max(vtkIdType(3), aNbComp);
      TVector<TVTKBasicType> aDataValues (aSize);

      const TGeom2MeshValue& aGeom2MeshValue = theValForTime->GetGeom2MeshValue();
      TGeom2MeshValue::const_iterator anIter = aGeom2MeshValue.begin();
      for (int aTupleId = 0; anIter != aGeom2MeshValue.end(); anIter++) {
        EGeometry aEGeom = anIter->first;
        const TMeshValuePtr aMeshValue = anIter->second;

        vtkIdType aNbElem = aMeshValue->GetNbElem();
        vtkIdType aNbGauss = aMeshValue->GetNbGauss();

        INITMSG(MYDEBUG,
                "- aEGeom = "<<aEGeom<<
                "; aNbElem = "<<aNbElem<<
                "; aNbGauss = "<<aNbGauss<<
                std::endl);

        for (vtkIdType iElem = 0; iElem < aNbElem; iElem++, aTupleId++) {
          typename TMeshValue::TCValueSliceArr aValueSliceArr = aMeshValue->GetCompValueSliceArr(iElem);

          for (vtkIdType iComp = 0; iComp < aNbComp; iComp++) {
            const typename TMeshValue::TCValueSlice& aValueSlice = aValueSliceArr[iComp];
            //jfa//aDataValues[iComp] = TVTKBasicType();
            for (vtkIdType iGauss = 0; iGauss < aNbGauss; iGauss++) {
              TVTKBasicType aValue = aValueSlice[iGauss];
              //jfa//if (iGauss == 0 && theGaussMetric != VISU::AVERAGE_METRIC) {
              if (iGauss == 0) {
                aDataValues[iComp] = aValue; // init
              }
              else {
                switch (theGaussMetric) {
                  case VISU::AVERAGE_METRIC:
                    aDataValues[iComp] += aValue; break;
                  case VISU::MINIMUM_METRIC:
                    aDataValues[iComp] = std::min( aValue, aDataValues[iComp] ); break;
                  case VISU::MAXIMUM_METRIC:
                    aDataValues[iComp] = std::max( aValue, aDataValues[iComp] ); break;
                }
              }
            }
            if (theGaussMetric == VISU::AVERAGE_METRIC)
              aDataValues[iComp] /= aNbGauss;
          }

          this->myDataArrayHolder->SetTuple(aTupleId, &aDataValues[0]);
        }
      }
    }
  };

  template<int EDataType>
  struct TTimeStampOnProfileInitModulus
  {
    typedef typename TL::TEnum2VTKArrayType<EDataType>::TResult TVTKDataArray;
    typedef typename TL::TEnum2VTKBasicType<EDataType>::TResult TVTKBasicType;
    typedef TTMeshValue<TVTKBasicType> TMeshValue;
    typedef MED::SharedPtr<TMeshValue> TMeshValuePtr;

    typedef TDataArrayHolder<EDataType> TTDataArrayHolder;
    typedef MED::SharedPtr<TTDataArrayHolder> PDataArrayHolder;
    PDataArrayHolder myDataArrayHolder;

    TTimeStampOnProfileInitModulus(const PDataArrayHolder& theDataArrayHolder):
      myDataArrayHolder(theDataArrayHolder)
    {}

    void
    Execute(const PFieldImpl& theField, const PValForTimeImpl& theValForTime)
    {
      vtkIdType aNbComp = theField->myNbComp;
      vtkIdType aSize = vtkIdType(3); // Minimum, Maximum and Average modulus
      TVector<TVTKBasicType> aDataValues (aSize);

      const TGeom2MeshValue& aGeom2MeshValue = theValForTime->GetGeom2MeshValue();
      TGeom2MeshValue::const_iterator anIter = aGeom2MeshValue.begin();
      for (int aTupleId = 0; anIter != aGeom2MeshValue.end(); anIter++) {
        EGeometry aEGeom = anIter->first;
        const TMeshValuePtr aMeshValue = anIter->second;

        vtkIdType aNbElem = aMeshValue->GetNbElem();
        vtkIdType aNbGauss = aMeshValue->GetNbGauss();

        INITMSG(MYDEBUG,
                "- aEGeom = "<<aEGeom<<
                "; aNbElem = "<<aNbElem<<
                "; aNbGauss = "<<aNbGauss<<
                std::endl);

        for (vtkIdType iElem = 0; iElem < aNbElem; iElem++, aTupleId++) {
          typename TMeshValue::TCValueSliceArr aValueSliceArr = aMeshValue->GetCompValueSliceArr(iElem);

          // modules of all gauss points
          TVector<TVTKBasicType> aModules (aNbGauss);

          for (vtkIdType iComp = 0; iComp < aNbComp; iComp++) {
            const typename TMeshValue::TCValueSlice& aValueSlice = aValueSliceArr[iComp];
            for (vtkIdType iGauss = 0; iGauss < aNbGauss; iGauss++) {
              TVTKBasicType aValue = aValueSlice[iGauss];

              // modules of all gauss points
              if (iComp == 0)
                aModules[iGauss] = aValue * aValue; // init
              else
                aModules[iGauss] += aValue * aValue;
            }
          }

          TVTKBasicType aModule = (TVTKBasicType)sqrt((double)aModules[0]);
          aDataValues[0] = aModule; // init Min
          aDataValues[1] = aModule; // init Max
          aDataValues[2] = 0.0;     // init Average

          for (vtkIdType ig = 0; ig < aNbGauss; ig++) {
            aModule = (TVTKBasicType)sqrt((double)aModules[ig]);

            aDataValues[0] = std::min(TVTKBasicType(aModule), aDataValues[0]); // Min
            aDataValues[1] = std::max(TVTKBasicType(aModule), aDataValues[1]); // Max
            aDataValues[2] += aModule;                                         // Average
          }
          aDataValues[2] /= aNbGauss; // Average

          this->myDataArrayHolder->SetTuple(aTupleId, &aDataValues[0]);
        }
      }
    }
  };


  //----------------------------------------------------------------------------
  template<int EDataType>
  void
  InitTimeStampOnProfile(const PUnstructuredGrid& theSource,
                         const PFieldImpl& theField,
                         const PValForTimeImpl& theValForTime,
                         const VISU::TEntity& theEntity)
  {
    vtkIdType aNbTuples = theField->myDataSize / theField->myNbComp;
    std::string aFieldName = VISU::GenerateFieldName(theField, theValForTime);

    vtkDataSetAttributes* aDataSetAttributes;
    switch ( theEntity ) {
    case VISU::NODE_ENTITY :
      aDataSetAttributes = theSource->GetPointData();
      break;
    default:
      aDataSetAttributes = theSource->GetCellData();
    }

    typedef typename TL::TEnum2VTKArrayType<EDataType>::TResult TVTKDataArray;
    TVTKDataArray *aSelectedDataArray = TVTKDataArray::New();
    vtkIdType aNbComp = theField->myNbComp;

    switch ( aNbComp ) {
    case 1:
      aSelectedDataArray->SetNumberOfComponents( 1 );
      aDataSetAttributes->SetScalars( aSelectedDataArray );
      break;
    default:
      aSelectedDataArray->SetNumberOfComponents( 3 );
      aDataSetAttributes->SetVectors( aSelectedDataArray );
    }
    aSelectedDataArray->SetNumberOfTuples( aNbTuples );
    aSelectedDataArray->SetName( aFieldName.c_str() );

    TVTKDataArray *aFullDataArray = TVTKDataArray::New();
    aFullDataArray->SetNumberOfComponents( aNbComp );
    aFullDataArray->SetNumberOfTuples( aNbTuples );
    aFullDataArray->SetName( "VISU_FIELD" );
    aDataSetAttributes->AddArray( aFullDataArray );

    INITMSG(MYDEBUG,"InitTimeStampOnProfile "<<
            "- theEntity = "<<theEntity<<
            "; aNbTuples = "<<aNbTuples<<
            "; aNbComp = "<<aNbComp<<
            std::endl);

    TTimerLog aTimerLog(MYDEBUG,"InitTimeStampOnProfile");

    const TGeom2MeshValue& aGeom2MeshValue = theValForTime->GetGeom2MeshValue();
    typedef typename TL::TEnum2VTKBasicType< EDataType >::TResult TVTKBasicType;
    typedef TTMeshValue< TVTKBasicType > TMeshValue;
    typedef MED::SharedPtr< TMeshValue > TMeshValuePtr;

    typedef TDataArrayHolder< EDataType > TTDataArrayHolder;
    typedef MED::SharedPtr< TTDataArrayHolder > PDataArrayHolder;

    TMeshValuePtr aMeshValue = theValForTime->GetFirstMeshValue();
    if ( aGeom2MeshValue.size() == 1 && aMeshValue->GetNbGauss() == 1 ) {
      aFullDataArray->SetVoidArray(aMeshValue->GetPointer(),
                                   aMeshValue->size(),
                                   true);
      INITMSG(MYDEBUG,"InitTimeStampOnProfile - aFullDataArray->SetVoidArray()"<<std::endl);
      if ( aNbComp == 1 ) {
        aSelectedDataArray->SetVoidArray( aMeshValue->GetPointer(),
                                          aMeshValue->size(),
                                          true );
        INITMSG(MYDEBUG,"InitTimeStampOnProfile - aSelectedDataArray->SetVoidArray()"<<std::endl);
      }else{
        PDataArrayHolder aDataArrayHolder(new TTDataArrayHolder(aSelectedDataArray));
        TTimeStampOnProfileInitArray<EDataType>(aDataArrayHolder).Execute(theField, theValForTime);
      }
    }
    else {
      typedef TDataArrayHolder2<EDataType> TTDataArrayHolder2;
      PDataArrayHolder aDataArrayHolder(new TTDataArrayHolder2(aSelectedDataArray, aFullDataArray));
      TTimeStampOnProfileInitArray<EDataType>(aDataArrayHolder).Execute(theField, theValForTime);

      if ( theValForTime->GetMaxNbGauss() > 1 ) { // at least one of geometry contains multiple gauss points
        TVTKDataArray *aGaussMinDataArray = TVTKDataArray::New();
        aGaussMinDataArray->SetNumberOfComponents( aNbComp );
        aGaussMinDataArray->SetNumberOfTuples( aNbTuples );
        aGaussMinDataArray->SetName( "VISU_FIELD_GAUSS_MIN" );
        aDataSetAttributes->AddArray( aGaussMinDataArray );

        PDataArrayHolder aGaussMinDataArrayHolder(new TTDataArrayHolder(aGaussMinDataArray));
        TTimeStampOnProfileInitArray<EDataType>(aGaussMinDataArrayHolder).Execute
          (theField, theValForTime, VISU::MINIMUM_METRIC);
        aGaussMinDataArray->Delete();

        TVTKDataArray *aGaussMaxDataArray = TVTKDataArray::New();
        aGaussMaxDataArray->SetNumberOfComponents( aNbComp );
        aGaussMaxDataArray->SetNumberOfTuples( aNbTuples );
        aGaussMaxDataArray->SetName( "VISU_FIELD_GAUSS_MAX" );
        aDataSetAttributes->AddArray( aGaussMaxDataArray );

        PDataArrayHolder aGaussMaxDataArrayHolder(new TTDataArrayHolder(aGaussMaxDataArray));
        TTimeStampOnProfileInitArray<EDataType>(aGaussMaxDataArrayHolder).Execute
          (theField, theValForTime, VISU::MAXIMUM_METRIC);
        aGaussMaxDataArray->Delete();

        TVTKDataArray *aGaussModulusDataArray = TVTKDataArray::New();
        aGaussModulusDataArray->SetNumberOfComponents( 3 ); // Min, Max and Average
        aGaussModulusDataArray->SetNumberOfTuples( aNbTuples );
        aGaussModulusDataArray->SetName( "VISU_FIELD_GAUSS_MOD" );
        aDataSetAttributes->AddArray( aGaussModulusDataArray );

        PDataArrayHolder aGaussModulusDataArrayHolder(new TTDataArrayHolder(aGaussModulusDataArray));
        TTimeStampOnProfileInitModulus<EDataType>(aGaussModulusDataArrayHolder).Execute
          (theField, theValForTime);
        aGaussModulusDataArray->Delete();
      }
    }

    aSelectedDataArray->Delete();
    aFullDataArray->Delete();

    // Process the case for ELNO data
    //-------------------------------
    if ( theField->myIsELNO ) {
      // To calculate effective number of components for the VTK compatibel ELNO data representation
      vtkIdType aEffectNbTuples = 0;
      TGeom2MeshValue::const_iterator anIter = aGeom2MeshValue.begin();
      for ( ; anIter != aGeom2MeshValue.end(); anIter++ ) {
        const PMeshValue& aMeshValue = anIter->second;
        aEffectNbTuples += aMeshValue->GetNbElem() * aMeshValue->GetNbGauss();
      }

      vtkIdType anEffectNbComp = ( aEffectNbTuples * aNbComp ) / aNbTuples + 1;

      // To create corresponding VTK representation for the ELNO data
      TSetElnoNodeData< EDataType > aSetElnoNodeData( anEffectNbComp,
                                                      aNbComp,
                                                      aNbTuples,
                                                      "ELNO_FIELD",
                                                      "ELNO_COMPONENT_MAPPER" );

      std::vector< TVTKBasicType > aDataValues( aNbComp ); // To reserve a temproary value holder

      // To initilize these VTK representation for the ELNO data from the MED
      anIter = aGeom2MeshValue.begin();
      for ( ; anIter != aGeom2MeshValue.end(); anIter++ ) {
        EGeometry aEGeom = anIter->first;
        const TMeshValuePtr aMeshValue = anIter->second;

        vtkIdType aNbElem = aMeshValue->GetNbElem();
        vtkIdType aNbGauss = aMeshValue->GetNbGauss();

        INITMSG(MYDEBUG,
                "- aEGeom = "<<aEGeom<<
                "; aNbElem = "<<aNbElem<<
                "; aNbGauss = "<<aNbGauss<<
                std::endl);
        std::vector<int> med2visu(aNbGauss);
        InitMed2VisuArray(med2visu,aEGeom);
        for ( vtkIdType iElem = 0; iElem < aNbElem; iElem++ ) {
          const typename TMeshValue::TValueSliceArr& aValueSliceArr =
            aMeshValue->GetGaussValueSliceArr( iElem );

          for( vtkIdType iGauss = 0; iGauss < aNbGauss; iGauss++ ) {
            const typename TMeshValue::TCValueSlice& aValueSlice = aValueSliceArr[ med2visu[iGauss] ];

            for( vtkIdType iComp = 0; iComp < aNbComp; iComp++ ) {
              aDataValues[ iComp ] = aValueSlice[ iComp ];
            }

            aSetElnoNodeData.AddNextPointData( &aDataValues[ 0 ] );
          }

          aSetElnoNodeData.InsertNextCellData();
        }
      }

      // Assign the ELNO data on the corresponding VTK data set attribute
      aSetElnoNodeData.AddData( aDataSetAttributes );
    }
    //-------------------------------
  }


  //----------------------------------------------------------------------------
  template<int EDataType>
  void
  InitTimeStampOnGaussMesh(const PPolyData& theSource,
                           const PFieldImpl& theField,
                           const PValForTimeImpl& theValForTime);

  void
  GetTimeStampOnGaussMesh(const PPolyData& theSource,
                          const PFieldImpl& theField,
                          const PValForTimeImpl& theValForTime)
  {
    vtkIdType aDataType = theField->GetDataType();
    switch(aDataType){
    case VTK_DOUBLE:
      InitTimeStampOnGaussMesh<VTK_DOUBLE>(theSource, theField, theValForTime);
      break;
    case VTK_FLOAT:
      InitTimeStampOnGaussMesh<VTK_FLOAT>(theSource, theField, theValForTime);
      break;
    case VTK_INT:
      InitTimeStampOnGaussMesh<VTK_INT>(theSource, theField, theValForTime);
      break;
    case VTK_LONG:
      InitTimeStampOnGaussMesh<VTK_LONG>(theSource, theField, theValForTime);
      break;
    default:
      EXCEPTION(std::runtime_error,
                "GetTimeStampOnGaussMesh - handling unsupported data type - "<<aDataType);
    }
  }

  //----------------------------------------------------------------------------
  template<int EDataType>
  struct TTimeStampOnGaussMeshInitArray
  {
    typedef typename TL::TEnum2VTKArrayType<EDataType>::TResult TVTKDataArray;
    typedef typename TL::TEnum2VTKBasicType<EDataType>::TResult TVTKBasicType;
    typedef TTMeshValue<TVTKBasicType> TMeshValue;
    typedef MED::SharedPtr<TMeshValue> TMeshValuePtr;

    typedef TDataArrayHolder<EDataType> TTDataArrayHolder;
    typedef MED::SharedPtr<TTDataArrayHolder> PDataArrayHolder;
    PDataArrayHolder myDataArrayHolder;

    TTimeStampOnGaussMeshInitArray(const PDataArrayHolder& theDataArrayHolder):
      myDataArrayHolder(theDataArrayHolder)
    {}

    void
    Execute(const PFieldImpl& theField,
            const PValForTimeImpl& theValForTime)
    {
      vtkIdType aNbComp = theField->myNbComp;
      vtkIdType aSize = std::max(vtkIdType(3), aNbComp);
      TVector<TVTKBasicType> aDataValues(aSize);

      const TGeom2MeshValue& aGeom2MeshValue = theValForTime->GetGeom2MeshValue();

      PGaussMeshImpl aGaussMesh = theValForTime->myGaussMesh;
      const TGeom2GaussSubMesh& aGeom2GaussSubMesh = aGaussMesh->myGeom2GaussSubMesh;
      TGeom2GaussSubMesh::const_iterator anIter = aGeom2GaussSubMesh.begin();
      for(int aTupleId = 0; anIter != aGeom2GaussSubMesh.end(); anIter++){
        EGeometry aEGeom = anIter->first;

        PGaussSubMeshImpl aGaussSubMesh = anIter->second;
        if(!aGaussSubMesh->myIsDone)
          continue;

        TGeom2MeshValue::const_iterator anIter2 = aGeom2MeshValue.find(aEGeom);
        if(anIter2 == aGeom2MeshValue.end()){
          EXCEPTION(std::runtime_error,
                    "TTimeStampOnGaussMeshInitArray >> Can't find values for corresponding Gauss Points SubMesh");
        }
        TMeshValuePtr aMeshValue = anIter2->second;
        vtkIdType aNbGauss = aMeshValue->GetNbGauss();
        vtkIdType aNbElem = aMeshValue->GetNbElem();

        if(aNbGauss < 1)
          continue;

        const TPointCoords& aCoords = aGaussSubMesh->myPointCoords;

        INITMSG(MYDEBUG,
                "- aEGeom = "<<aEGeom<<
                "; aNbElem = "<<aNbElem<<
                "; aNbGauss = "<<aNbGauss<<
                "; aCoords.GetNbPoints() = "<<aCoords.GetNbPoints()<<
                std::endl);

        if(aCoords.GetNbPoints() == aNbElem*aNbGauss){
          for(int iElem = 0; iElem < aNbElem; iElem++){
            typename TMeshValue::TCValueSliceArr aValueSliceArr = aMeshValue->GetGaussValueSliceArr(iElem);
            for(int iGauss = 0; iGauss < aNbGauss; iGauss++, aTupleId++){
              const typename TMeshValue::TCValueSlice& aValueSlice = aValueSliceArr[iGauss];
              for(int iComp = 0; iComp < aNbComp; iComp++){
                aDataValues[iComp] = aValueSlice[iComp];
              }
              this->myDataArrayHolder->SetTuple(aTupleId, &aDataValues[0]);
            }
          }
        }else{
          for(int iElem = 0; iElem < aNbElem; iElem++, aTupleId++){
            typename TMeshValue::TCValueSliceArr aValueSliceArr = aMeshValue->GetCompValueSliceArr(iElem);
            for(int iComp = 0; iComp < aNbComp; iComp++){
              const typename TMeshValue::TCValueSlice& aValueSlice = aValueSliceArr[iComp];
              aDataValues[iComp] = TVTKBasicType();
              for(int iGauss = 0; iGauss < aNbGauss; iGauss++){
                aDataValues[iComp] += aValueSlice[iGauss];
              }
              aDataValues[iComp] /= aNbGauss;
            }
            this->myDataArrayHolder->SetTuple(aTupleId, &aDataValues[0]);
          }
        }
      }
    }
  };


  template<int EDataType>
  void
  InitTimeStampOnGaussMesh(const PPolyData& theSource,
                           const PFieldImpl& theField,
                           const PValForTimeImpl& theValForTime)
  {
    vtkIdType aNbTuples = theSource->GetNumberOfPoints();
    std::string aFieldName = VISU::GenerateFieldName(theField, theValForTime);

    vtkDataSetAttributes* aDataSetAttributes = theSource->GetPointData();

    typedef typename TL::TEnum2VTKArrayType<EDataType>::TResult TVTKDataArray;
    TVTKDataArray *aSelectedDataArray = TVTKDataArray::New();
    vtkIdType aNbComp = theField->myNbComp;
    switch(aNbComp){
    case 1:
      aSelectedDataArray->SetNumberOfComponents(1);
      aDataSetAttributes->SetScalars(aSelectedDataArray);
      break;
    default:
      aSelectedDataArray->SetNumberOfComponents(3);
      aDataSetAttributes->SetVectors(aSelectedDataArray);
    }
    aSelectedDataArray->SetNumberOfTuples(aNbTuples);
    aSelectedDataArray->SetName(aFieldName.c_str());

    TVTKDataArray *aFullDataArray = TVTKDataArray::New();
    aFullDataArray->SetNumberOfComponents(aNbComp);
    aFullDataArray->SetNumberOfTuples(aNbTuples);
    aFullDataArray->SetName("VISU_FIELD");
    aDataSetAttributes->AddArray(aFullDataArray);

    INITMSG(MYDEBUG,"InitTimeStampOnGaussMesh "<<
            "- aNbTuples = "<<aNbTuples<<
            "; aNbComp = "<<aNbComp<<
            std::endl);
    TTimerLog aTimerLog(MYDEBUG,"InitTimeStampOnGaussMesh");

    const TGeom2MeshValue& aGeom2MeshValue = theValForTime->GetGeom2MeshValue();
    typedef typename TL::TEnum2VTKBasicType<EDataType>::TResult TVTKBasicType;
    typedef TTMeshValue<TVTKBasicType> TMeshValue;
    typedef MED::SharedPtr<TMeshValue> TMeshValuePtr;

    typedef TDataArrayHolder<EDataType> TTDataArrayHolder;
    typedef MED::SharedPtr<TTDataArrayHolder> PDataArrayHolder;

    TMeshValuePtr aMeshValue = theValForTime->GetFirstMeshValue();
    if(aGeom2MeshValue.size() == 1){
      aFullDataArray->SetVoidArray(aMeshValue->GetPointer(),
                                   aMeshValue->size(),
                                   true);
      INITMSG(MYDEBUG,"InitTimeStampOnGaussMesh - aFullDataArray->SetVoidArray()"<<std::endl);
      if(aNbComp == 1 || aNbComp == 3){
        aSelectedDataArray->SetVoidArray(aMeshValue->GetPointer(),
                                         aMeshValue->size(),
                                         true);
        INITMSG(MYDEBUG,"InitTimeStampOnGaussMesh - aSelectedDataArray->SetVoidArray()"<<std::endl);
      }else{
        PDataArrayHolder aDataArrayHolder(new TTDataArrayHolder(aSelectedDataArray));
        TTimeStampOnGaussMeshInitArray<EDataType>(aDataArrayHolder).Execute(theField, theValForTime);
      }
    }else{
      typedef TDataArrayHolder2<EDataType> TTDataArrayHolder2;
      PDataArrayHolder aDataArrayHolder(new TTDataArrayHolder2(aSelectedDataArray, aFullDataArray));
      TTimeStampOnGaussMeshInitArray<EDataType>(aDataArrayHolder).Execute(theField, theValForTime);
    }

    aSelectedDataArray->Delete();
    aFullDataArray->Delete();
  }


  //----------------------------------------------------------------------------

  void InitMed2VisuArray(std::vector<int>& anArray, EGeometry aEGeom){
    switch(aEGeom){
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
    case eSEG3:
      anArray[0] = 0;
      anArray[2] = 1;
      anArray[1] = 2;
      break;
#endif

#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
    case eTRIA6:
      anArray[0] = 0;
      anArray[2] = 1;
      anArray[4] = 2;

      anArray[1] = 3;
      anArray[3] = 4;
      anArray[5] = 5;
      break;
#endif

#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
    case eQUAD8:
      anArray[0] = 0;
      anArray[2] = 1;
      anArray[4] = 2;
      anArray[6] = 3;

      anArray[1] = 4;
      anArray[3] = 5;
      anArray[5] = 6;
      anArray[7] = 7;
      break;
#endif
#if !(defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
    case eQUAD9:
      anArray[0] = 0;
      anArray[2] = 1;
      anArray[4] = 2;
      anArray[6] = 3;

      anArray[1] = 4;
      anArray[3] = 5;
      anArray[5] = 6;
      anArray[7] = 7;
	  anArray[8] = 8;
      break;
#endif
    case eTETRA4:
      anArray[0] = 0;
      anArray[1] = 2;
      anArray[2] = 1;
      anArray[3] = 3;
      break;
    case ePYRA5:
      anArray[0] = 0;
      anArray[1] = 3;
      anArray[2] = 2;
      anArray[3] = 1;
      anArray[4] = 4;
      break;
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
    case eTETRA10:
      anArray[0] = 0;
      anArray[1] = 2;
      anArray[2] = 1;
      anArray[3] = 3;

      anArray[4] = 6;
      anArray[5] = 5;
      anArray[6] = 4;

      anArray[7] = 7;
      anArray[8] = 9;
      anArray[9] = 8;
      break;
#endif

#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
    case ePYRA13:
      anArray[0] = 0;
      anArray[1] = 3;
      anArray[2] = 2;
      anArray[3] = 1;
      anArray[4] = 4;

      anArray[5] = 8;
      anArray[6] = 7;
      anArray[7] = 6;
      anArray[8] = 5;

      anArray[9] = 9;
      anArray[10] = 12;
      anArray[11] = 11;
      anArray[12] = 10;
      break;
#endif
#if (defined(VISU_USE_VTK_QUADRATIC)) && defined(VISU_ENABLE_QUADRATIC)
	case eHEXA27:
      anArray[0] = 0;
      anArray[1] = 3;
      anArray[2] = 2; 
      anArray[3] = 1; 

      anArray[4] = 4;
      anArray[5] = 7;
      anArray[6] = 6;
      anArray[7] = 5;

	  anArray[8] = 11;
	  anArray[9] = 10;
      anArray[10] = 9;
	  anArray[11] = 8;

	  anArray[12] = 15;
	  anArray[13] = 14;
      anArray[14] = 13;
	  anArray[15] = 12;

	  anArray[16] = 16;
	  anArray[17] = 19;
      anArray[18] = 18;
	  anArray[19] = 17;

	  anArray[20] = 21;
	  anArray[21] = 23;
      anArray[22] = 24;
	  anArray[23] = 22;

	  anArray[24] = 20;
	  anArray[25] = 25;
      anArray[26] = 26;
      break;
#endif
    default:
      for(int i=0;i<anArray.size();i++){
        anArray[i] = i;
      }
      break;
    }
  }
}

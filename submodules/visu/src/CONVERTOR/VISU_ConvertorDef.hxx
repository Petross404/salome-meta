// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : 
//  Author : 
//  Module : 
//
#ifndef VISU_ConvertorDef_HeaderFile
#define VISU_ConvertorDef_HeaderFile

/*! 
  \file VISU_ConvertorDef.hxx
  \brief The file contains predeclarations for basic classes of the VISU CONVERTER package
*/

#include "MED_SharedPtr.hxx"

namespace VISU
{
  //---------------------------------------------------------------
  enum  TEntity {NODE_ENTITY, EDGE_ENTITY, FACE_ENTITY, CELL_ENTITY};


  //---------------------------------------------------------------
  //! Defines VISU enumeration of geometrical types
  enum EGeometry {ePOINT1=1, eSEG2=102, eSEG3=103, eTRIA3=203,
                  eQUAD4=204, eTRIA6=206,eQUAD8=208, eQUAD9=209, eTETRA4=304,
                  ePYRA5=305, ePENTA6=306, eHEXA8=308, eOCTA12=312, eTETRA10=310, 
                  ePYRA13=313, ePENTA15=315, eHEXA20=320, eHEXA27=327,
                  ePOLYGONE=400, ePOLYEDRE=500, eNONE=-1};


  //---------------------------------------------------------------
  //! Enumeration used to extract different kinds of values from the data on gauss points
  enum TGaussMetric { AVERAGE_METRIC = 0, MINIMUM_METRIC, MAXIMUM_METRIC };


  //---------------------------------------------------------------
  struct TMesh;
  typedef MED::SharedPtr<TMesh> PMesh;


  //---------------------------------------------------------------
  struct TGaussSubMesh;
  typedef MED::SharedPtr<TGaussSubMesh> PGaussSubMesh;


  //---------------------------------------------------------------
  struct TGaussMesh;
  typedef MED::SharedPtr<TGaussMesh> PGaussMesh;


  //---------------------------------------------------------------
  struct TSubProfile;
  typedef MED::SharedPtr<TSubProfile> PSubProfile;


  //---------------------------------------------------------------
  struct TProfile;
  typedef MED::SharedPtr<TProfile> PProfile;


  //---------------------------------------------------------------
  struct TMeshOnEntity;
  typedef MED::SharedPtr<TMeshOnEntity> PMeshOnEntity;


  //---------------------------------------------------------------
  struct TFamily;
  typedef MED::SharedPtr<TFamily> PFamily;


  //---------------------------------------------------------------
  struct TGroup;
  typedef MED::SharedPtr<TGroup> PGroup;


  //---------------------------------------------------------------
  struct TField;
  typedef MED::SharedPtr<TField> PField;


  //---------------------------------------------------------------
  struct TGauss;
  typedef MED::SharedPtr<TGauss> PGauss;


  //---------------------------------------------------------------
  struct TValForTime;
  typedef MED::SharedPtr<TValForTime> PValForTime;
  

  //---------------------------------------------------------------
  struct TGrille;
  typedef MED::SharedPtr<TGrille> PGrille;


  //---------------------------------------------------------------
}

#endif

// Copyright (C) 2007-2013  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:    VISUConvertor.cxx
//  Author:  Alexey PETROV
//  Module : VISU
//
#include "VISU_Convertor.hxx"
#include "VISU_ConvertorUtils.hxx"

#include "VISU_Convertor_impl.hxx"
#include "VISU_TableReader.hxx"

#include <fstream>      
#include <sstream>
#include <memory>       

#include <vtkCellType.h>
#include <vtkUnstructuredGrid.h>

#include <QDir>
#include <QFileInfo>
#include <QStringList>


#include "utilities.h"

using namespace std;

#ifdef DEBUG
static int MYDEBUG = 1;
#else
static int MYDEBUG = 0;
#endif

//#define _DEBUG_ID_MAPPING_
#define _DEXCEPT_

typedef vtkUnstructuredGrid TOutput;

void ParseMEDFile(const char* theFileName) 
{
#ifndef _DEXCEPT_
  try{
#endif
    MSG(MYDEBUG,"'"<<theFileName<<"'...");
    //theFileName = "Apointe.med";
    auto_ptr<VISU_Convertor> aCon(CreateConvertor(theFileName));
    //aCon->GetSize();
    //return;
    aCon->BuildEntities();
    aCon->BuildGroups();
    aCon->BuildFields();
    aCon->BuildMinMax();
    const VISU::TMeshMap& aMeshMap = aCon->GetMeshMap();
    //return;
    VISU::TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
    for(; aMeshMapIter != aMeshMap.end(); aMeshMapIter++){
      //continue;

      const string& aMeshName = aMeshMapIter->first;
      const VISU::PMesh& aMesh = aMeshMapIter->second;
      const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
      VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter;

      //Import fields
      aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
        const VISU::TEntity& anEntity = aMeshOnEntityMapIter->first;
        const VISU::PMeshOnEntity& aMeshOnEntity = aMeshOnEntityMapIter->second;
        const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
        VISU::TFieldMap::const_reverse_iterator aFieldMapIter = aFieldMap.rbegin();
        for(; aFieldMapIter != aFieldMap.rend(); aFieldMapIter++){
          const string& aFieldName = aFieldMapIter->first;
          const VISU::PField& aField = aFieldMapIter->second;
          const VISU::TValField& aValField = aField->myValField;
          VISU::TValField::const_iterator aValFieldIter = aValField.begin();
          for(; aValFieldIter != aValField.end(); aValFieldIter++){
            int aTimeStamp = aValFieldIter->first;

            if(anEntity != VISU::NODE_ENTITY){
              VISU::PGaussPtsIDMapper aGaussMesh = 
                aCon->GetTimeStampOnGaussPts(aMeshName,anEntity,aFieldName,aTimeStamp);
#ifdef _DEBUG_ID_MAPPING_
              vtkDataSet* aDataSet = aGaussMesh->GetOutput();
              aDataSet->Update();
              int aNbCells = aDataSet->GetNumberOfCells();
              MESSAGE("aNbCells = "<<aNbCells);
              for(int anCellId = 0; anCellId < aNbCells; anCellId++){
                VISU::TGaussPointID anObjID = aGaussMesh->GetObjID(anCellId);
                cout<<anObjID.first<<"; "<<anObjID.second<<"; "<<aGaussMesh->GetNodeVTKID(anObjID.first)<<endl;
                double* aCoord = aGaussMesh->GetNodeCoord(anCellId);
                cout<<aCoord[0]<<"; "<<aCoord[1]<<"; "<<aCoord[2]<<endl;
              }
#endif
            }else{
              //continue;
              VISU::PIDMapper anIDMapper = 
                aCon->GetTimeStampOnMesh(aMeshName,anEntity,aFieldName,aTimeStamp);
#ifdef _DEBUG_ID_MAPPING_
              vtkDataSet* aDataSet = anIDMapper->GetOutput();
              aDataSet->Update();
              int aNbCells = aDataSet->GetNumberOfCells();
              for(int anCellId = 0; anCellId < aNbCells; anCellId++){
                int anObjID = anIDMapper->GetElemObjID(anCellId);
                int aVTKID  = anIDMapper->GetElemVTKID(anObjID);
                cout<<anObjID<<"; "<<aVTKID<<endl;
              }
#endif
            }
            //goto OK;
          }
        }
      }

      //Importing groups
      const VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
      VISU::TGroupMap::const_iterator aGroupMapIter = aGroupMap.begin();
      for(; aGroupMapIter != aGroupMap.end(); aGroupMapIter++){
        const string& aGroupName = aGroupMapIter->first;
        aCon->GetMeshOnGroup(aMeshName,aGroupName);
      }

      //continue;

      //Import mesh on entity
      aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
        const VISU::TEntity& anEntity = aMeshOnEntityMapIter->first;
        VISU::PIDMapper anIDMapper = aCon->GetMeshOnEntity(aMeshName,anEntity);
#ifdef _DEBUG_ID_MAPPING_
        vtkDataSet* aDataSet = anIDMapper->GetOutput();
        int aNbCells, anCellId, anObjID, aVTKID;
        aNbCells = aDataSet->GetNumberOfCells();
        for(anCellId = 0; anCellId < aNbCells; anCellId++){
          anObjID = anIDMapper->GetElemObjID(anCellId);
          aVTKID  = anIDMapper->GetElemVTKID(anObjID);
          MESSAGE(anObjID<<"; "<<aVTKID);
        }
#endif
      }

      //Import families
      aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
      for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
        const VISU::TEntity& anEntity = aMeshOnEntityMapIter->first;
        const VISU::PMeshOnEntity& aMeshOnEntity = aMeshOnEntityMapIter->second;
        //aCon->GetMeshOnEntity(aMeshName,anEntity);
        const VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
        VISU::TFamilyMap::const_iterator aFamilyMapIter = aFamilyMap.begin();
        for(; aFamilyMapIter != aFamilyMap.end(); aFamilyMapIter++){
          const string& aFamilyName = aFamilyMapIter->first;
          aCon->GetFamilyOnEntity(aMeshName,anEntity,aFamilyName);
        }
      }
    }
  OK:
    MSG(MYDEBUG,"OK");
#ifndef _DEXCEPT_
  }catch(std::exception& exc){
    MSG(MYDEBUG,"Follow exception was occured in file:"<<theFileName<<"\n"<<exc.what());
  }catch(...){
    MSG(MYDEBUG,"Unknown exception was occured in VISU_Convertor_impl in file:"<<theFileName);
  } 
#endif
}

void ParseXLSFile(const char* theFileName) 
{
  VISU::TTableContainer aContainer;
  VISU::ImportTables( theFileName, aContainer );
}

int
main( int argc, char** argv )
{ 
  if ( argc > 1 ) {
    QFileInfo aFileInfo( argv[1] );
    for ( int i = 0; i < 1; i++ ) {
      if ( aFileInfo.exists() ) {
        if ( aFileInfo.isDir() ) {
          QDir aDir( aFileInfo.absoluteFilePath() );
          QStringList wildcards; wildcards << "*.med";
          QStringList aStringList = aDir.entryList( wildcards, QDir::Files );
          int jEnd = aStringList.count();
          for ( int j = 0; j < jEnd; j++ ) {
            ParseMEDFile( aDir.filePath( aStringList[j] ).toLatin1().data() );
          }
        } else {
          QString anExtension = aFileInfo.suffix();
          if ( anExtension == "med" )
            ParseMEDFile( argv[1] );
          else if ( anExtension == "xls" || anExtension == "txt" )
            ParseXLSFile( argv[1] );
        }
      }
    }
    return 0;
  }
  return 1;
}

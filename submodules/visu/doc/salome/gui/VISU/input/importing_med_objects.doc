/*!

\page importing_med_objects_page Import/Export MED objects

There are two sources of \ref med_object_page "MED objects":
- MED and other SALOME components publishing them MED objects
- MED files

<em>To load MED objects from other components:</em>
\par
In the Object Browser, use context menu item <b>Import Structure</b>
to import the whole MED structure, <b>Import Field</b> to import a
certain field and <b>Import TimeStamp</b> to import a timestamp.
<br><br>
It is possible to load MED objects directly from the components, where
they are published. Usually this is MED component, but it can also be
Calculator,  which creates  MED objects as a result of execution of
Supervisor graphs.

\note  If you have just opened your study from a hdf, the data of
components is not yet loaded. In this case the items <b>Import
Structure</b> and <b>Import TimeStamp</b> will not be accessible. To
load the data of any component from an opened hdf you can use <b>Load
Component Data</b> command, accessible via the context menu of any
object of the target component in the Object Browser. After the data
of the component is loaded  this item will disappear. The data from
the components possessing their own GUI is loaded during the
activation of the component GUI. The data of MED component is loaded
automatically during the activation of Post-Pro GUI, the objects are
imported from MED to Post-Pro rather often.

<em>To import a MED file:</em>
\par
From the \b File menu choose <b>Import -> MED File</b> or right-click
on the \b Post-Pro object in the Object Browser and select <b>MED
File</b> option. After it you will see a standard dialog box allowing
to search for and choose *.med  files:

\image html importfromfile.png

\par
To confirm your choice click \b Open.
<br><br>
If the option <b>Use Build Progress</b> is checked in <b>Post-Pro
Sweep and MED Import Preferences</b>, you will be able to choose how
the object containing in the file will be built, otherwise the
structure of the imported MED object will be immediately displayed in
the Object Browser and it will be loaded and built as set in the \b Preferences.

\image html buildprogress.png

\par
\b Settings
<ul>
<li><b>Build all</b> - when this option is checked, the application
loads all data necessary for processing of the object.</li>
<li><b>Build at once</b> - when this option is checked, you can't
perform any operations until the MED file is fully loaded and
processed.</li>
<li><b>Close dialog at finish</b> - when this option is checked, the
dialog box is closed after loading of the selected MED file. When
unchecked, it allows to monitor the build process.</li>
</ul>

\par
<b>Import progress</b> - allows to choose what items exactly should be
built at loading. By default everything is checked in.  Note that the
coloured rectangles show the progress of the respective step and
become blue when complete.
<ul>
<li><b>Build entities</b> - displays the rate at which the geometrical
data is loaded.</li>
<li><b>Build fields</b> - when this option is checked, the fields are
built automatically at loading.</li>
<li><b>Build min/max</b> - parses time stamps at loading finding the
highest and the lowest value.</li>
<li><b>Build Groups</b> - builds groups and families at loading.</li>
</ul>
\b Time - shows the <b>Elapsed time</b> spent by the application on processing.

<em>To export a MED file:</em>
\par
Right-click on the top level of MED object in the Object Browser and select <b>Export to MED
File</b> option. When you do so you will see a standard dialog box where you can select the 
path and change the name of the exported MED file:

\image html exportmedfile.png

To complete file export click <b>Save</b>.

<br><b>See Also</b> a sample script of 
\ref tui_import_export_page "Import/Export" of MED objects via TUI.

*/

/*!

\page slider_page Slider

\n \b Slider allows animating Post-Pro field presentations in the
most simple and efficient way. \b Slider works only with field
presentations created on all timestamps of the field. 

\image html fieldpres3.png

The presentations compatible with this
functionality can be synchronized (in terms of timestamps) and moved
along the time axis to reproduce the corresponding physical process.

\b Slider also provides memory management possibilities, which allow the
user to define a suitable compromise between the available computer
resources and the navigation performance.

\n \ref animating_page is an earlier implementation of the same
functionality. However, it provides some extended possibilities, such
as saving presentations into AVI files.

To activate \b Slider, in the Main menu select <b>View -> Windows ->
Slider</b>

\image html slider1.png

\b Navigation tab provides possibilities for managing and running
the animation:
<ul>
<li>The scroll bar allows to navigate through the
timestamps. The values of the first and the last timestamps are
displayed. </li>
<li>The control buttons allow to launch or to end the sweeping, run it forward or backward,
move to the next or the previous frame.</li>
<li>The drop-down field to the left lists the timestamps by their order
numbers from 1 to n. </li>
<li>The drop-down field to the right lists the values
of the timestamps. </li>
<li>Cycled button allows to view the animation in the loop.</li>
</ul>

\image html slider2.png

\b Properties tab allows setting memory management properties.

- \b Minimal radio button commands the application to use a minimum
  amount of memory
- \b Limited radio button allows to set the upper limit for the amount
  of used memory
- \b Used  and \b Free fields allow to set target values for used and
  free memory.
- \b Speed scroll allows to set the speed of the presentation.

*/

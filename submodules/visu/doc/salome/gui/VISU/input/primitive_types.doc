/*!

\page primitive_types_page Primitive Types

\n Gauss Point presentations use primitives to visualize the value of
the results obtained during calculations so that the color and
relative size of the points depend on each component of a vector,
tensor or matrix (e.g. scalar and module for vectors). Of course, the
size of primitives in the Viewer also depends on their location within
the 3D scene. At a given computed size, a primitive will be bigger
when it is close to the camera and smaller at a distance.

\image html image30.gif

There are three Basic primitive types which can be used in Gauss
Points presentations: <b>Point Sprites</b>, <b>Open GL Points</b> and
<b>Geometrical Spheres</b>.

<br><h2>Point Sprites</h2>

\image html image33.jpg

Each <b>Point Sprite</b> is visualised in 3D Viewer with two 16*16
textures: <b>Main texture</b> which defines the shape of the point
sprite and <b>Alpha Channel Texture</b> which defines the transparency
of the point sprite. By default Salome suggests textures defined in
the Preferences, however you can browse for other .bmp, .png or .jpg
files to be used as textures.
\n <b>Alpha Channel Threshold</b> defines the level of transparency
ranging from 0 to 1.
\n The default sizes of point sprites are computed by the system,
therefore, these parameters cannot be defined in the preferences,
however, the <b>Maximum Size</b> of the point sprite is limited
(Clamp) so that the whole scene would remain visible if the user
applies a high zoom factor. <b>Maximum Size</b> corresponds to the
maximum size of a point sprite during rendering whatever is the
magnification of the point or the zoom of the view. Technically, it
corresponds to setting up a clamp in the vertex shader. The Size is
measured in pixels and belongs to the interval from 1 to 512.

<br><h2>OpenGL Points</h2>

\image html image30.gif

<b>OpenGL Points</b> don't use any files as textures, displaying
results as simple OpenGL points, so the only parameter relevant to
them is the <b>Maximum Size</b>.

<br><h2>Geometrical Spheres</h2>

\image html image31.gif

<b>Geometrical Spheres</b> display results using a standard sphere with
a number of faces be defined thought a \b Resolution parameter. By
default the \b Resolution is set to 8. This number corresponds to the
Latitude and Longitude definitions of a VTK sphere. The <b>Number of
Faces</b> parameter is equal to the total number of points in the
scene when no segmentation cursor is used or number of points within
the cursor when a segmentation is in progress, multiplied by the
number of faces of one Sphere in the current resolution. This value
computed by the system using the formula: NbrFaces = 2*Resolution x
[Resolution-2] and displayed to the end-user for information purpose.

In the pictures you can see the spheres with Resolution of <b>8</b>:

\image html image87.gif

and <b>6</b>:

\image html image88.gif

<b>Notify when number of faces exceeds</b> prompts the user by a
dialog to continue or to cancel the display operation if the number of
faces is greater than the number defined in the menu.

\image html warning.png

*/
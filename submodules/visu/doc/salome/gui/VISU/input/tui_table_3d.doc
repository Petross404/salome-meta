/*!

\page tui_table_3d_page Table 3D presentation 

Create and display a Table 3D presentation:

\code
import os

import salome

import visu_gui

# The directory containing data table files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import tables from file
tableFile = os.path.join(datadir,"Tables","table_test.xls")
myTablesSO = myVisu.ImportTables(tableFile, False)

# Create a table presentation
table = None
anIsFound, aTableSO = myTablesSO.FindSubObject(1)
if anIsFound:
      anID = aTableSO.GetID()
      table = myVisu.CreateTable(anID)
      # Set 'Contour' presentation type
      table.SetContourPrs(1)
      # Set number od contours
      table.SetNbOfContours(1024)
      # Set scale factor
      table.SetScaleFactor(0.2)

# Show the table presentation
if (table is not None):
    # Display just created tabel presentation
    myViewManager = myVisu.GetViewManager()
    myView = myViewManager.Create3DView()

    myView.DisplayOnly(table)
    myView.FitAll()

# Update the object browser
salome.sg.updateObjBrowser(1)
\endcode

<br>Please, see \ref VISU.Table "Table interface reference documentation" 
for more details.

*/

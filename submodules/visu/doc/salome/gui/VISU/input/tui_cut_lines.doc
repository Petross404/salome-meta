/*!

\page tui_cut_lines_page Cut Lines

Create <b>Cut Lines</b> presentation on a field of the imported MED file:

\code
import os

import salome

import VISU
import visu_gui

# The directory containing MED files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import a MED file
medFile = os.path.join(datadir,"MedFiles","fra.med")
myResult = myVisu.ImportFile(medFile)

# Create cut lines for the first timestamp of 'VITESSE' field
meshName = 'LE VOLUME'
fieldEntity = VISU.NODE
fieldName = 'VITESSE'
myCutLines = myVisu.CutLinesOnField(myResult, meshName, fieldEntity, fieldName, 1)

# Set the orientation of the base plane (parallel to ZX)
myCutLines.SetOrientation(VISU.CutPlanes.ZX, 0, 0)

# Set the orientation of the cutting planes (parallel to ZX)
myCutLines.SetOrientation2(VISU.CutPlanes.XY, 0, 0)

# Set the number of lines
myCutLines.SetNbLines(12)

# Set the displacement of the base plane
myCutLines.SetDisplacement(0)

# Set a custom position for the first line
newPos = (myCutLines.GetLinePosition(1) - myCutLines.GetLinePosition(0))/2
myCutLines.SetLinePosition(0, newPos)

# Update the object browser
salome.sg.updateObjBrowser(1)

# Display the newly created cut lines
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

myView.DisplayOnly(myCutLines)
myView.FitAll()
\endcode

<br>Please, see \ref VISU.CutLines "CutLines interface reference documentation" 
for more details.

*/

/*!

\page gauss_points_presentations_page Gauss Points Presentations

\image html gauss_points.png "Example of Gauss Points presentation"

Gauss Points presentation presents data on Fields as a set of fixed
points of the renormalisation group flow with Gaussian distribution.

\n <em>To create a Gauss Points  presentation:</em>
\par
&ndash; Right-click on one of the time stamps of the field in the
Object browser and from the pop-up menu choose <b>Gauss Points</b>, or
\n &ndash; Click on one of the time stamps of the field in the Object
browser and select from the main menu <b>Visualization > Gauss
Points</b>, or click <em>"Gauss Points"</em> icon in the
<b>Visualization Toolbar</b>.

\image html image29.gif
<center><em>"Gauss Points" icon</em></center>

\image html image31.jpg

\par
Now it is possible to choose the \subpage types_of_gauss_points_presentations_page
"Type" of your Gauss Points presentation and \subpage primitive_types_page
"Primitives" used for visualisation of the points and set their properties.

To exit the dialog and apply choices press \b OK button, or press \b
CANCEL button to quit.

<br><b>See Also</b> a sample TUI Script of  
\ref tui_gauss_points_page "Gauss Points presentations" creation.

*/

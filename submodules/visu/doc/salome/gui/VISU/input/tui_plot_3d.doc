/*!

\page tui_plot_3d_page Plot 3D

Create <b>Plot 3D</b> presentation on a field of the imported MED file:

\code
import os

import salome

import VISU
import visu_gui

# The directory containing MED files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import a MED file
medFile = os.path.join(datadir,"MedFiles","fra.med")
myResult = myVisu.ImportFile(medFile)

# Create a 3d plot for the first timestamp of 'VITESSE' field
meshName = 'LE VOLUME'
fieldEntity = VISU.NODE
fieldName = 'VITESSE'
myPlot3D = myVisu.Plot3DOnField(myResult, meshName, fieldEntity, fieldName, 1)

# Set the cut plane orientation
myPlot3D.SetOrientation(VISU.Plot3D.ZX, 0, 0)

# Set the relative position of the plane
myPlot3D.SetPlanePosition(0.8, True)

# Set the scale factor
myPlot3D.SetScaleFactor(0.25)

# Set the contour presentation type
myPlot3D.SetContourPrs(True)

# Set the number of contours
myPlot3D.SetNbOfContours(100)

# Update the object browser
salome.sg.updateObjBrowser(1)

# Display the newly created plot 3d presentation
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

myView.DisplayOnly(myPlot3D)
myView.FitAll()
\endcode

<br>Please, see \ref VISU.Plot3D "Plot3D interface reference documentation" 
for more details.

*/

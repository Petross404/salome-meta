.. index:: single: SampleAsnUplet

SampleAsnUplet
  *Liste d'états*. Cette clé décrit les points de calcul sous la forme d'une
  liste de n-uplets, chaque n-uplet étant un état.

  Exemple :
  ``{"SampleAsnUplet":[[0,1,2,3],[4,3,2,1],[-2,3,-4,5]]}`` pour 3 points dans un espace d'état de dimension 4

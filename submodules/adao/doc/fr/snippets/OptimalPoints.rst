.. index:: single: OptimalPoints

OptimalPoints
  *Liste de série d'entiers*. Chaque élément est une série, contenant les
  points idéaux déterminés par la recherche optimale, rangés par ordre de
  préférence décroissante et dans le même ordre que les vecteurs de base
  réduite trouvés itérativement.

  Exemple :
  ``mp = ADD.get("OptimalPoints")[-1]``

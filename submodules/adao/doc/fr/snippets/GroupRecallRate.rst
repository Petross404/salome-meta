.. index:: single: GroupRecallRate

GroupRecallRate
  *Valeur réelle*. Cette clé indique le taux de rappel vers le meilleur insecte
  de l'essaim. C'est une valeur réelle comprise entre 0 et 1. Le défaut est de
  0.5.

  Exemple :
  ``{"GroupRecallRate":0.5}``

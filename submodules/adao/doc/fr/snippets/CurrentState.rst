.. index:: single: CurrentState

CurrentState
  *Liste de vecteurs*. Chaque élément est un vecteur d'état courant utilisé
  au cours du déroulement itératif de l'algorithme utilisé.

  Exemple :
  ``Xs = ADD.get("CurrentState")[:]``

.. index:: single: SwarmVelocity

SwarmVelocity
  *Valeur réelle*. Cette clé indique la part de la vitesse d'insecte qui est
  imposée par l'essaim, dite "vitesse de groupe". C'est une valeur réelle
  positive. Le défaut est de 1.

  Exemple :
  ``{"SwarmVelocity":1.}``

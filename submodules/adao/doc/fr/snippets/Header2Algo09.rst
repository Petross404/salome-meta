Exemples d'utilisation en Python (TUI)
++++++++++++++++++++++++++++++++++++++

Voici un exemple très simple d'usage de l'algorithme proposé et de ses
paramètres, écrit en :ref:`section_tui`, et dont les informations indiquées en
entrée permettent de définir un cas équivalent en interface graphique.

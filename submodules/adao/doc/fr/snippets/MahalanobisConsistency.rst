.. index:: single: MahalanobisConsistency

MahalanobisConsistency
  *Liste de valeurs*. Chaque élément est une valeur de l'indicateur de
  qualité de Mahalanobis.

  Exemple :
  ``m = ADD.get("MahalanobisConsistency")[-1]``

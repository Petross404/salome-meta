.. index:: single: APosterioriCovariance

APosterioriCovariance
  *Liste de matrices*. Chaque élément est une matrice :math:`\mathbf{A}` de
  covariances des erreurs *a posteriori* de l'état optimal.

  Exemple :
  ``A = ADD.get("APosterioriCovariance")[-1]``

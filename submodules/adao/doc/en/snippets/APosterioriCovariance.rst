.. index:: single: APosterioriCovariance

APosterioriCovariance
  *List of matrices*. Each element is an *a posteriori* error covariance
  matrix :math:`\mathbf{A}` of the optimal state.

  Example:
  ``A = ADD.get("APosterioriCovariance")[-1]``

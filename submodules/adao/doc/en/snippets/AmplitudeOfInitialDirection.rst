.. index:: single: AmplitudeOfInitialDirection

AmplitudeOfInitialDirection
  *Real value*. This key indicates the scaling of the initial perturbation
  build as a vector used for the directional derivative around the nominal
  checking point. The default is 1, that means no scaling.

  Example:
  ``{"AmplitudeOfInitialDirection":0.5}``

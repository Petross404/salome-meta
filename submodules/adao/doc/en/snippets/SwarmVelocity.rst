.. index:: single: SwarmVelocity

SwarmVelocity
  *Real value*. This key indicates the part of the insect velocity which is
  imposed by the swarm, named "group velocity". It is a positive floating point
  value. The default value is 1.

  Example :
  ``{"SwarmVelocity":1.}``

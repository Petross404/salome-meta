.. index:: single: GroupRecallRate

GroupRecallRate
  *Real value*. This key indicates the recall rate at the best swarm insect. It
  is a floating point value between 0 and 1. The default value is 0.5.

  Example :
  ``{"GroupRecallRate":0.5}``

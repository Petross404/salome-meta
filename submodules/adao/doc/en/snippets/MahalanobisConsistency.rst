.. index:: single: MahalanobisConsistency

MahalanobisConsistency
  *List of values*. Each element is a value of the Mahalanobis quality
  indicator.

  Example:
  ``m = ADD.get("MahalanobisConsistency")[-1]``

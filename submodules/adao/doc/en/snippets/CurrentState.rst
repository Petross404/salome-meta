.. index:: single: CurrentState

CurrentState
  *List of vectors*. Each element is a usual state vector used during the
  iterative algorithm procedure.

  Example:
  ``Xs = ADD.get("CurrentState")[:]``

.. index:: single: Analysis

Analysis
  *List of vectors*. Each element of this variable is an optimal state
  :math:`\mathbf{x}^*` in optimization or an analysis :math:`\mathbf{x}^a` in
  data assimilation.

  Example:
  ``Xa = ADD.get("Analysis")[-1]``

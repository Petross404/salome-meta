.. index:: single: OptimalPoints

OptimalPoints
  *List of integer series*. Each element is a series, containing the ideal points
  determined by the optimal search, ordered by decreasing preference and in the
  same order as the reduced basis vectors found iteratively.

  Example :
  ``mp = ADD.get("OptimalPoints")[-1]``

#!/bin/bash

# Squish IDE binary
SQUISH_IDE=/PRODUCTS/Linux/squish-5.1.1-qt47x-linux64/bin/squishide

# source environment
source env_squish.sh

# run Squish
${SQUISH_IDE}&

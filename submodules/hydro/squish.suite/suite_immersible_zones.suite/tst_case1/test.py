"""
This test case corresponds to HYDRO-003 (Immersible zone creation/edition and operations with it) scenario
"""

import os
import tempfile
import shutil

def main():
    # Prepare test data
    global TEST_FILES_DIR
    TEST_FILES_DIR = os.getenv("TEST_FILES_DIR")

    global APPSETTINGS
    APPSETTINGS = os.path.join(TEST_FILES_DIR, "SalomeApp.xml")

    source(os.path.join(os.getenv("COMMON_SCRIPT_DIR"), "common_hydro.py"))

    # Run test
    os.system("killSalome.py") #@MZN
    runCase()
    
    # Kill SALOME
    os.system("killSalome.py")

def runCase():
    S1() # Creating a new study
    S2() # Creating immersible zone
    S3() # Show/hide immersible zone
    S4() # Color of immersible zone
    S5() # Delete immersible zone


"""
Creating a new study.
"""
def S1():

    # Title 1: Creating a new study
    
    # 1. Start the application
    startApplication("runSalome.py --modules=HYDRO,GEOM,SMESH -r %s" % APPSETTINGS)
    waitFor("object.exists(':SALOME_STD_TabDesktop')", 100000)
      
    # 2. File - New
    activateMenuItem("File", "New")
    
    # 3. New study window appears including Object Browser and Python console
    checkObjectBrowser()
    checkPythonConsole()
   
    # 4. Select HYDRO in the list of modules, located in the toolbar
    # 5. Graphics scene and OCC scene appear
    activateHYDRO()

    showTrihedron(False) # Hide trihedron


"""
Create immersible zone.
"""
def S2():
    
    # Title 2: Create immersible zone
    
    # 1. Click Ctrl+B
    # 2. "Import bathymetry" panel appears
    # 3. Import "IGN_pts_utiles_L2e_1.xyz" bathymetry file
    bath_file_path = os.path.join(TEST_FILES_DIR, "IGN_pts_utiles_L2e_1.xyz")
      
    importBathymetry(bath_file_path)
   
    # 4. Select IGN_pts_utiles_L2e_1 in Object Browser
    # 5. Call context menu - Show only
    checkOBItem("HYDRO", "BATHYMETRIES", "IGN_pts_utiles_L2e_1")
    activateOBContextMenuItem("HYDRO.BATHYMETRIES", "IGN_pts_utiles_L2e_1", "Show only")
    
    # 6. Click "Fit All" button to observe imported bathymetry
    fitAll(True)
       
    # Check the bathymetry presentation in OCC viewer
    test.vp("VP1")
    
    # Hide bathymetry presentation
    activateOBContextMenuItem("HYDRO.BATHYMETRIES", "IGN_pts_utiles_L2e_1", "Hide")
    resetOCCViewer()
    
    # 7. HYDRO - Create polyline
    activateMenuItem("HYDRO", "Create polyline")
    
    # 8. Create the polyline as follows:
    addSplineSection(True)
    
    addPolylinePoints([(621, 298), (571, 347), (620, 399), (721, 439), (812, 413), (839, 284), (699, 151)])
  
    clickButton(waitForObject(":Create polyline.Apply_QPushButton"))

    # 9. HYDRO - Create immersible zone
    activateMenuItem("HYDRO", "Create immersible zone")
    
    # 10. "Create immersible zone" panel appears
    waitFor("object.exists(':SALOME *.Create immersible zone_HYDROGUI_ImmersibleZoneDlg')", 20000)
    zone_panel = findObject(":SALOME *.Create immersible zone_HYDROGUI_ImmersibleZoneDlg")
    test.compare(zone_panel.visible, True)
    test.compare(zone_panel.windowTitle, "Create immersible zone")
    
    # Check that default name is "Immersible zone_1"    
    test.compare(str(waitForObject(":Zone name.Name_QLineEdit").displayText), "Immersible zone_1")
    
    # 11. As polyline select created in step 8 polyline
    selectComboBoxItem(waitForObject(":Parameters.Polyline_QComboBox"), "Polyline_1")
    
    # 12. As bathymetry select imported in step 3 bathymetry
    selectComboBoxItem(waitForObject(":Bathymetry_QComboBox"), "IGN_pts_utiles_L2e_1")

    # 13. Click "Apply"
    clickButton(waitForObject(":Create immersible zone.Apply_QPushButton"))

    # 14. Check the result
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Show only")
    fitAll(True)
    test.vp("VP2") 
    
    # 15. Import once more bathymetry, for example, inverted_IGN_pts_utiles_L2e_1
    importBathymetry(bath_file_path, "inverted_IGN_pts_utiles_L2e_1", True)
    
    # 16. Create the polyline as follows:
    hideAll()
    
    activateMenuItem("HYDRO", "Create polyline")
    
    addSplineSection(True)
    addPolylinePoints([(827, 188), (650, 191), (495, 283), (441, 397), (515, 528), (755, 442), (769, 367)])
   
    addSplineSection(True)
    addPolylinePoints([(543, 380), (508, 431), (514, 489), (569, 469), (598, 413), (619, 388)])
    
    addSplineSection(True)
    addPolylinePoints([(696, 222), (762, 214), (798, 233), (773, 293), (693, 322)])

    clickButton(waitForObject(":Create polyline.Apply_QPushButton"))

    # 17. Create any open polyline. It has name "Polyline_3"
    activateMenuItem("HYDRO", "Create polyline")
    
    addPolylineSection(False)
    addPolylinePoints([(368, 504), (525, 348), (830, 375), (954, 299)])

    waitFor("object.exists(':Create polyline.Name_QLineEdit')", 20000)
    test.compare(str(findObject(":Create polyline.Name_QLineEdit").displayText), "Polyline_3")
    
    clickButton(waitForObject(":Create polyline.Apply_QPushButton"))
    
    hideAll()
   
    # 18. In Object browser select "NATURAL OBJECTS" item, call its context menu - Create immersible zone
    activateOBContextMenuItem("HYDRO", "NATURAL OBJECTS", "Create immersible zone")
   
    # 19. "Create immersible zone" panel appears
    zone_panel = waitForObject(":SALOME *.Create immersible zone_HYDROGUI_ImmersibleZoneDlg")
    test.compare(zone_panel.visible, True)
    test.compare(zone_panel.windowTitle, "Create immersible zone")
    
    # Check that the name is "Immersible zone_2" now    
    test.compare(str(waitForObject(":Zone name.Name_QLineEdit").displayText), "Immersible zone_2")

    # 20. Check that only "Polyline_1" and "Polyline_2" are shown in the combo box.
    poly_combo = waitForObject(":Parameters.Polyline_QComboBox")
    checkComboBoxContent(poly_combo, ["Polyline_1", "Polyline_2"])  
    
    # As polyline select created in step 16 polyline. 
    selectComboBoxItem(poly_combo, "Polyline_2")
    
    # 21. Check that both bathymetries are shown in the combo box.
    bath_combo = waitForObject(":Bathymetry_QComboBox")
    checkComboBoxContent(bath_combo, ["IGN_pts_utiles_L2e_1", "inverted_IGN_pts_utiles_L2e_1"])
        
    # As bathymetry select imported in step 3 bathymetry.
    selectComboBoxItem(bath_combo, "IGN_pts_utiles_L2e_1")
   
    # 22. Check the preview:
    fitAll()
    test.vp("VP3")   
    
    # 23. Click "Cancel" button
    clickButton(waitForObject(":Create immersible zone.Cancel_QPushButton"))
    
    # 24. No object has been created
    checkOBItemNotExists("HYDRO", "NATURAL OBJECTS", "Immersible zone_2")
    
    # 25. Repeat steps 18-21
    activateOBContextMenuItem("HYDRO", "NATURAL OBJECTS", "Create immersible zone")
    selectComboBoxItem(waitForObject(":Parameters.Polyline_QComboBox"), "Polyline_2")
    selectComboBoxItem(waitForObject(":Bathymetry_QComboBox"), "IGN_pts_utiles_L2e_1")
    
    # 26. Click "Apply"
    clickButton(waitForObject(":Create immersible zone.Apply_QPushButton"))
    
    # 27. Check the result
    fitAll(True)
    test.vp("VP4")
    
    # 28. Create polyline, containing from polyline section and from spline section:
    # 29. End points of sectors should have identical coordinates
    hideAll()
    
    activateMenuItem("HYDRO", "Create polyline")
    
    start_point = (570, 130)
    end_point = (880, 310)
    
    addPolylineSection(False)
    addPolylinePoints([start_point, end_point])
   
    addSplineSection(False)
    addPolylinePoints([start_point, (510, 220), (600, 260), (600, 410), end_point])
        
    clickButton(waitForObject(":Create polyline.Apply_QPushButton"))
    
    # 30. HYDRO - Create immersible zone
    activateMenuItem("HYDRO", "Create immersible zone")
    
    # 31. Select just created "Polyline_4" in "Polyline" combo box 
    selectComboBoxItem(waitForObject(":Parameters.Polyline_QComboBox"), "Polyline_4")
    
    # 32. Select any bathymetry
    selectComboBoxItem(waitForObject(":Bathymetry_QComboBox"), "inverted_IGN_pts_utiles_L2e_1")

    # 33. Click "Apply"
    clickButton(waitForObject(":Create immersible zone.Apply_QPushButton"))
    
    # 34. Check the result
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_3", "Show only")
    fitAll(True)
    test.vp("VP5")

   
"""
Show/hide immersible zone.
"""
def S3():
    
    # Title 3: Show/hide immersible zone.
    
    # 1. Activate OCC 3D view
    
    # Already active: do nothing
    
    # 2. Select "Immersible zone_1" in Object Browser
    # 3. Call context menu - Show Only
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Show only")
    
    # 4. Fit all
    fitAll(True)
    
    # 5. Only "Immersible zone_1" is shown in OCC view
    test.vp("VP2")
    
    # 6. Call context menu - Hide
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Hide")
    
    # 7. "Immersible zone_1" is hidden in OCC view
    test.vp("vp_empty_occ_view")
    
    # 8. Select "Immersible zone_1" and "Immersible zone_2" in Object Browser
    # 9. Call context menu - Show
    zones = ["Immersible zone_1", "Immersible zone_2"]
    activateOBContextMenuItems("HYDRO.NATURAL OBJECTS", zones, "Show")
      
    # 10. Fit all
    fitAll()
    
    # 11. Both immersible zones are shown in OCC 3D view
    test.vp("VP9")
    
    # 12. Call context menu - Hide all
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Hide all")
    
    # 13. Both immersible zones are hidden in OCC 3D view
    test.vp("vp_empty_occ_view")
    
    # 14. Window - New window - Graphics view
    activateMenuItem("Window", "New Window", "Graphics view")
    
    # 15. New Graphics view appears
    waitFor("object.exists(':Graphics scene:2 - viewer:1_TabItem')", 20000)
    test.compare(findObject(":Graphics scene:2 - viewer:1_TabItem").text, "Graphics scene:2 - viewer:1")
    test.compare(findObject(":Graphics scene:2 - viewer:1_TabItem").enabled, True)
    
    # 16. Show Immersible zone_1
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Show")
      
    # 17. Nothing is shown
    test.vp("VP7")
    
    # 18. Window - New window - VTK 3D view
    activateMenuItem("Window", "New Window", "VTK 3D view")

    # 19. New VTK view appears
    waitFor("object.exists(':VTK scene:1 - viewer:1_TabItem')", 20000)
    test.compare(findObject(":VTK scene:1 - viewer:1_TabItem").enabled, True)
    test.compare(findObject(":VTK scene:1 - viewer:1_TabItem").text, "VTK scene:1 - viewer:1")

    # 20. Show Immersible zone_2
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_2", "Show")

    # 21. Fit all
    fitAll()

    # 22. Check the result:
    test.vp("VP8")


"""
Color of immersible zone.
"""
def S4():
    
    # Title 4: Color of immersible zone.
    
    # 1. Activate OCC 3D view
    clickTab(waitForObject(":SALOME *_QtxWorkstackTabBar"), "OCC scene:1 - viewer:1")
       
    # 2. Show Immersible zone_1
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Show")
    
    # 3. Call its context menu - Color:
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Color")
        
    # 4. Click the button to select filling color
    doubleClick(waitForObject(":Set color.Color_HYDROGUI_ColorWidget"), 43, 7, 0, Qt.LeftButton)
     
    # 5. "Select color" dialog appears:
    color_dlg1 = waitForObject(":Select Color_QColorDialog")
    test.compare(color_dlg1.visible, True)
    test.compare(color_dlg1.windowTitle, "Select Color")
    
    # 6. Select green color and click OK
    mouseClick(waitForObject(":Basic colors_QWellArray"), 184, 11, 0, Qt.LeftButton)
    clickButton(waitForObject(":Select Color.OK_QPushButton"))
    
    # 7. Selected color appears in "Filling color" field
    test.vp("VP10")
    
    # 8. Check Border check box
    mouseClick(waitForObject(":Set color.Border_QGroupBox"), 17, 11, 0, Qt.LeftButton)
    
    # 9. Color button to select Border color becomes enabled
    waitFor("object.exists(':Border.Color_HYDROGUI_ColorWidget')", 20000)
    test.compare(findObject(":Border.Color_HYDROGUI_ColorWidget").enabled, True)
        
    # 10. Click the button to select border color
    doubleClick(waitForObject(":Border.Color_HYDROGUI_ColorWidget"), 76, 9, 0, Qt.LeftButton)
    
    # 11. "Select color" dialog appears
    color_dlg2 = waitForObject(":Select Color_QColorDialog")
    test.compare(color_dlg2.visible, True)
    test.compare(color_dlg2.windowTitle, "Select Color")
    
    # 12. Select yellow color and click OK
    mouseClick(waitForObject(":Basic colors_QWellArray"), 214, 89, 0, Qt.LeftButton)
    clickButton(waitForObject(":Select Color.OK_QPushButton"))
    
    # 13. Selected color appears in Border field
    test.vp("VP11")
    
    # 14. Click Cancel button
    clickButton(waitForObject(":Set color.Cancel_QPushButton"))
    
    # 15. No changes with Immersible zone_1 appear
    deselectAll()
    test.vp("VP2") 
    
    # 16. Repeat steps 3-13
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Color")
    doubleClick(waitForObject(":Set color.Color_HYDROGUI_ColorWidget"), 43, 7, 0, Qt.LeftButton)
    mouseClick(waitForObject(":Basic colors_QWellArray"), 184, 11, 0, Qt.LeftButton)
    clickButton(waitForObject(":Select Color.OK_QPushButton"))
    mouseClick(waitForObject(":Set color.Border_QGroupBox"), 17, 11, 0, Qt.LeftButton)
    doubleClick(waitForObject(":Border.Color_HYDROGUI_ColorWidget"), 76, 9, 0, Qt.LeftButton)
    mouseClick(waitForObject(":Basic colors_QWellArray"), 214, 89, 0, Qt.LeftButton)
    clickButton(waitForObject(":Select Color.OK_QPushButton"))
  
    # 17. Click OK in "set color" dialog
    clickButton(waitForObject(":Set color.OK_QPushButton"))
    
    # 18. Check the result:
    deselectAll()
    test.vp("VP12")


"""
Delete immersible zone.
"""
def S5():
    
    # Title 5: Delete immersible zone.
    
    # 1. Select "Immersible zone_1"
    # 2. Call context menu - Delete
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Delete")
  
    # 3. Delete objects" dialog appear
    delete_dlg = waitForObject(":Delete objects_HYDROGUI_DeleteDlg")
    test.compare(str(delete_dlg.windowTitle), "Delete objects")
    objects_list_text = waitForObject(":Delete objects_QTextEdit")
    test.compare(objects_list_text.plainText, "- Immersible zone_1")
    
    # 4. Click No
    clickButton(waitForObject(":Delete objects.No_QPushButton"))
    
    # 5. Nothing happens (Immersible zone_1 is still displayed in the view and presented in Object browser)
    deselectAll()
    test.vp("VP12")
    checkOBItem("HYDRO", "NATURAL OBJECTS", "Immersible zone_1")
        
    # 6. Select Immersible zone_1
    # 7. Call context menu - Delete
    activateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Immersible zone_1", "Delete")
    
    # 8. Delete objects" dialog appear
    waitForObject(":Delete objects_HYDROGUI_DeleteDlg")
    
    # 9. Click Yes
    clickButton(waitForObject(":Delete objects.Yes_QPushButton"))
    
    # 10. Immersible zone_1 disappear as from OCC view as far from Object browser
    checkOBItemNotExists("HYDRO", "NATURAL OBJECTS", "Immersible zone_1")
    test.vp("vp_empty_occ_view")
        
    # 11. Click Undo button
    clickButton(waitForObject(":SALOME *.Undo_QToolButton"))
    deselectAll()
    
    test.vp("VP12")
    checkOBItem("HYDRO", "NATURAL OBJECTS", "Immersible zone_1")
        
    # 12. Select Immersible zone_1 and Immersible zone_2
    # 13. Click Delete button
    selectObjectBrowserItem("Immersible zone_1", "HYDRO.NATURAL OBJECTS")
    selectObjectBrowserItem("Immersible zone_2", "HYDRO.NATURAL OBJECTS", modifier=Qt.ControlModifier)
    type(waitForObject(":SALOME_STD_TabDesktop"), "<Del>")
    
    # 14. Delete objects dialog appears:
    waitForObject(":Delete objects_HYDROGUI_DeleteDlg")
    objects_list_text = waitForObject(":Delete objects_QTextEdit")
    test.compare(objects_list_text.plainText, "- Immersible zone_1\n- Immersible zone_2")
    
    # 15. Click Yes
    clickButton(waitForObject(":Delete objects.Yes_QPushButton"))
    
    # 16. Both immersible zones disappear from OCC view and Object browser
    checkOBItemNotExists("HYDRO", "NATURAL OBJECTS", "Immersible zone_1")
    checkOBItemNotExists("HYDRO", "NATURAL OBJECTS", "Immersible zone_2")
    test.vp("vp_empty_occ_view")
    
    

#!/bin/bash

# SALOME installation directory
SALOME_CURRENT=/dn26/HYDRO/current/2015.04.06

# set tests root dir
if [ ! -n "${ROOT_DIR}" ] ; then
    export ROOT_DIR=`pwd`
    echo "ROOT_DIR has been set to " ${ROOT_DIR}
fi

# set current HYDRO build environment
cd ${SALOME_CURRENT}
source env.sh
cd $ROOT_DIR

unset SALOME_CURRENT

# remove python directory from PYTHONPATH
PYTHONPATH=$(echo "$PYTHONPATH" | sed -e 's/:\/dn47\/SALOME\/PRODUCTS\/7x\/opt\/DEBIAN.6.0.64\/7.5.1\/Python-2.7.3\/lib\/python2.7$//')

# Squish + OpenGL
export SQUISH_GRABWINDOW_CLASSES=QGLWidget,QWSGLWindowSurface,SVTK_RenderWindowInteractor,OCCViewer_ViewPort3d,pqQVTKWidget

# Directory used by tests for writing files
export TEST_DIR=${ROOT_DIR}/TEST_TMP

# Directory for files imported in tests
export TEST_FILES_DIR=${ROOT_DIR}/shared/testdata

# Common script dir
export COMMON_SCRIPT_DIR=${ROOT_DIR}/shared/scripts

"""Common functions"""
import os
import datetime, time


"""
Check that Object Browser is displayed.
"""
def checkObjectBrowser():
    waitFor("object.exists(':Object Browser_QtxTreeView')", 20000)
    test.compare(findObject(":Object Browser_QtxTreeView").visible, True)
    
"""
Check that Python console is displayed.
"""
def checkPythonConsole():
    waitFor("object.exists(':Python Console_PyConsole_EnhEditor')", 20000)
    test.compare(findObject(":Python Console_PyConsole_EnhEditor").visible, True)

"""
Activate menu for any level of depth.
For example:  activateMenuItem("File","New")
"""
def activateMenuItem(*menuPath):
    menu = ":SALOME_QMenuBar"
    parent = "{type='STD_TabDesktop' unnamed='1' visible='1' windowTitle?='SALOME *'}"
    for item in menuPath[:-1]:
        activateItem(waitForObjectItem(menu, item))
        menu = "{title='%s' type='QMenu' unnamed='1' visible='1'}" % (item) 
        parent = menu
    activateItem(waitForObjectItem(menu, menuPath[-1]))
   
"""
Return current data in given format
"""    
def getCurrentDateString(format = "%d/%m/%Y %H:%M"):
    now = datetime.datetime.now()
    return str(now.strftime(format))

"""
Return current user name
"""    
def getCurrentUser():
     user = os.getenv("USER")
     return user

"""
Return difference between given and current date 
""" 
def dateDifferenceToNow(date_str, format="%d/%m/%Y %H:%M"):
    now = datetime.datetime.now()
    date_to_compare = datetime.datetime(*(time.strptime(date_str, format)[0:6]))
    test.log("dateDifferenceToNow","now:"+str(now)+" date_to_compare:"+str(date_to_compare)+"  dif:"+ str(now-date_to_compare))
    return abs(now - date_to_compare)< datetime.timedelta(minutes = 1)


#--------------------------------------------------------------------------
# Menu File
#--------------------------------------------------------------------------
"""
Open given file via menu "File"-"Open"
For example:  openFile("/<Full path to file>")
"""    
def openFile(file_path):
    activateMenuItem("File","Open...")
    test.verify(os.path.exists(file_path))
    realName = "{buddy=':Open File.File name:_QLabel' name='fileNameEdit' type='QLineEdit' visible='1'}"  #:fileNameEdit_QLineEdit_2
    waitForObject(realName).setText(file_path)
    type(waitForObject(realName), "<Return>")
    
"""
Save given file via menu "File"-"Save As"
For example:  saveFile("/<Full path to file>")
"""    
def saveFile(file_path):
    activateMenuItem("File","Save As...")
    lineEditRealName = "{buddy=':Save File.File name:_QLabel' name='fileNameEdit' type='QLineEdit' visible='1'}"
    saveBtnRealName = "{text='Save' type='QPushButton' unnamed='1' visible='1' window=':Save File_SUIT_FileDlg'}"#{text='Save' type='QPushButton' unnamed='1' visible='1' window=':Save File_SUIT_FileDlg'}
    waitFor("object.exists('%s')" % lineEditRealName.replace("'", "\\'"), 2000)
    waitForObject(lineEditRealName).setText(file_path)
    type(waitForObject(lineEditRealName), "<Return>")

"""
Import Python script via SALOME Python console
For example:  importPythonScript("SMESH_test1")
"""
def importPythonScript(fileName):
    mouseClick(waitForObject(":Python Console_PyConsole_EnhEditor"), 50, 10, 0, Qt.LeftButton)
    type(waitForObject(":Python Console_PyConsole_EnhEditor"), "import %s" % fileName)
    type(waitForObject(":Python Console_PyConsole_EnhEditor"), "<Return>")
    
"""
Call File->Close menu
""" 
def closeFile():
    activateMenuItem("File","Close")

"""
Makes dump of the study to given file via menu
File->Dump study
"""
def dumpStudy(file_path, save_gui_state = False ):
    activateMenuItem("File","Dump Study...")
    waitForObject(":Dump study.Save GUI state_QCheckBox").checked = save_gui_state
    mouseClick(waitForObject(":fileNameEdit_QLineEdit_3"), 20, 11, 0, Qt.LeftButton)
    waitForObject(":fileNameEdit_QLineEdit_3").setText(file_path)
    type(waitForObject(":fileNameEdit_QLineEdit_3"), "<Return>")

"""
Opens python script via menu File->Load Script
"""
def loadScript(file_path):
    activateMenuItem("File","Load Script...")
    mouseClick(waitForObject(":fileNameEdit_QLineEdit_4"), 45, 12, 0, Qt.LeftButton)
    waitForObject(":fileNameEdit_QLineEdit_4").setText(file_path)
    clickButton(waitForObject(":Load python script.Open_QPushButton"))     


#--------------------------------------------------------------------------
# Object browser
#--------------------------------------------------------------------------
"""
Deselect all objects selected in Object browser.
"""
def deselectAll():
    objBrowser = waitForObject(":Object Browser_QtxTreeView")
    objBrowser.clearSelection()   

"""
Select item in Object Browser tree under topTreeItem with possible modifier.
The mostly used values  of modifier are Qt.ShiftModifier, Qt.ControlModifier
"""  
def selectObjectBrowserItem(item, topTreeItem = "HYDRO" , modifier =  0 ):
    itemText = getItemText(item, topTreeItem) 
    waitForObjectItem(":Object Browser_QtxTreeView",  itemText)
    clickItem(":Object Browser_QtxTreeView", itemText, 30, 5, modifier, Qt.LeftButton)

"""
Verify that given item exists in Object Browser
For example: checkOBItemNotExists("HYDRO", "NATURAL OBJECTS", "Immersible zone_1")
""" 
def checkOBItem(*parentNames):
    treeObj = waitForObject(":Object Browser_QtxTreeView")
    obj = getModelIndexByName(treeObj, *parentNames)
    if (not obj is None) and obj.isValid(): 
        test.compare(obj.text, parentNames[-1])
    else:
        s=""
        for name in parentNames:
            s +="/"+str(name)
        test.fail("Unexpectedly failed to find the object in OB", s)

"""
Verify that given item doesn't exist in Object Browser
For example: checkOBItemNotExists("HYDRO", "NATURAL OBJECTS", "Immersible zone_2")
""" 
def checkOBItemNotExists(*parentNames):
    treeObj = waitForObject(":Object Browser_QtxTreeView")
    obj = getModelIndexByName(treeObj, *parentNames)
    s=""
    for name in parentNames:
        s +="/"+str(name)
    if (not obj is None) and obj.isValid(): 
        test.fail("Unexpectedly object is found in OB", s)
    else:
        test.passes("Object '%s' wasn't found in OB" % s)    
            
"""
Activate context menu item for the given object in the Object Browser.
For example: aactivateOBContextMenuItem("HYDRO.NATURAL OBJECTS", "Zone_1", "Show")
"""
def activateOBContextMenuItem(parent, item, *menuPath):
    try:
        name = getItemText(item, parent)
        selectObjectBrowserItem(item, parent)
        openItemContextMenu(waitForObject(":Object Browser_QtxTreeView"), name, 10, 1, 0)
        popupItem(*menuPath)
    except LookupError as err:
        test.fail("Unexpectedly failed to find the object in OB", str(err))

"""
Activate context menu item for the given list of objects in the Object Browser.
For example:  activateOBContextMenuItems("HYDRO.NATURAL OBJECTS", ["Zone_1", "Zone_2"], "Show")
"""
def activateOBContextMenuItems(topTreeItem, listItems, *menuPath):
    itemText = ""
    selectObjectBrowserItem(listItems[0], topTreeItem)
    for item in listItems[1:]:
        itemText = getItemText(item, topTreeItem) 
        waitForObjectItem(":Object Browser_QtxTreeView",  itemText)
        clickItem(":Object Browser_QtxTreeView", itemText, 30, 5, Qt.ControlModifier, Qt.LeftButton)
    openItemContextMenu(waitForObject(":Object Browser_QtxTreeView"), itemText, 10, 1, 0)
    popupItem(*menuPath)    

#--------------------------------------------------------------------------
# OCC Viewer
#--------------------------------------------------------------------------
"""
Activate context menu item at the given point of 3D viewer.
For example: activateOCCViewerContextMenuItem(100, 100, "Hide all")
"""   
def activateOCCViewerContextMenuItem(x, y, *menuPath): 
    activateViewerContextMenuItem(":SALOME *.3D View Operations_OCCViewer_ViewPort3d", x, y, *menuPath)

"""
Click "Fit All" button
If isDeselectAll = True all selected objects will be deselected.
""" 
def fitAll(isDeselectAll=False):
    if isDeselectAll:
        deselectAll()
    #@MZN clickButton(waitForObject(":SALOME *.Fit All_QToolButton"))
    btn = waitForObject(":SALOME *.Fit All_QToolButton")
    btn.click()

"""
Click "Reset" button
""" 
def resetOCCViewer():
    clickButton(waitForObject(":SALOME *.Reset_QToolButton"))

"""
Show/Hide trihedron axis in OCC Viewer
"""
def showTrihedron(val):
    btn = waitForObject(":SALOME *.Show/Hide trihedron_QToolButton")
    if btn.checked != val:
        clickButton(btn)
"""
Call Transparency popup menu in OCC Viewer
"""
def setOCCViwerTransparency(value, pos_x = -1, pos_y = -1 ):
    setTransparency(":SALOME *.3D View Operations_OCCViewer_ViewPort3d", value, pos_x, pos_y)
    
"""
Call Isos popup menu in OCC Viewer
"""
def setOCCViwerIsos(u, v, pos_x = -1, pos_y = -1):
    setIsos(":SALOME *.3D View Operations_OCCViewer_ViewPort3d", u, v, pos_x, pos_y)

"""
Mouse click in OCC viewer.
"""
def occMouseClick(x, y):
    mouseClick(waitForObject(":SALOME *.3D View Operations_OCCViewer_ViewPort3d"), x, y, 0, Qt.LeftButton)
    
"""
Multiple mouse clicks in OCC viewer.
"""
def occMouseClicks(points):
    occViewer = waitForObject(":SALOME *.3D View Operations_OCCViewer_ViewPort3d")
    for pnt in points:
        mouseClick(occViewer, pnt[0], pnt[1], 0, Qt.LeftButton)
    
"""
Mouse drag in OCC viewer.
"""
def occMouseDrag(x1, y1, x2, y2):
    mouseDrag(waitForObject(":SALOME *.3D View Operations_OCCViewer_ViewPort3d"), x1, y1, x2, y2, 1, Qt.LeftButton)

#--------------------------------------------------------------------------
# VTK Viewer
#--------------------------------------------------------------------------
    
"""
Activate context menu item at the given point of VTK viewer.
For example:  activateVTKContextMenuItem(50, 100, "Hide")
"""
def activateVTKContextMenuItem(x, y, *menuPath):
    activateViewerContextMenuItem(":SALOME *.SVTK_RenderWindowInteractor_SVTK_RenderWindowInteractor", x, y, *menuPath)

def setVTKIsos(u, v, pos_x = -1, pos_y = -1):
    setIsos(":SALOME *.SVTK_RenderWindowInteractor_SVTK_RenderWindowInteractor", u, v, pos_x, pos_y)

def setVTKTransparency(value, pos_x = -1, pos_y = -1 ):
    setTransparency(":SALOME *.SVTK_RenderWindowInteractor_SVTK_RenderWindowInteractor", value, pos_x, pos_y)
    

#--------------------------------------------------------------------------
# Widgets
#--------------------------------------------------------------------------
"""
Set button checked or unchecked
"""
def setButtonChecked(button, isChecked):
    if (isChecked != button.checked):
        clickButton(button)

    test.verify(button.checked == isChecked)

"""
Select item with the given text in the combo box
"""
def selectComboBoxItem(combo, itemText):
    for index in range(combo.count):
        if combo.itemText(index) == itemText:
            combo.setCurrentIndex(index)
            break
        
    test.verify(combo.currentText == itemText)
    pass

"""
Check that the combo box contains only the given items.
"""
def checkComboBoxContent(combo, items):
    sortItems = list(items)
    sortItems.sort()
    
    comboItems = []
    for index in range(combo.count):
        comboItems.append(str(combo.itemText(index)))
    
    comboItems.sort()
        
    test.compare(comboItems, sortItems)
    pass

"""
Click the object item.
"""
def clickObjectItem(objectName, itemName, modifier=0):
    itemNameFormatted = itemName.replace('_',"\\\\_")
    waitForObjectItem(objectName, itemNameFormatted)
    clickItem(objectName, itemNameFormatted, 10, 5, modifier, Qt.LeftButton)
    
"""
Double click the object item.
"""
def doubleClickObjectItem(objectName, itemName):
    itemNameFormatted = itemName.replace('_',"\\\\_")
    waitForObjectItem(objectName, itemNameFormatted)
    doubleClickItem(objectName, itemNameFormatted, 10, 5, 0, Qt.LeftButton)
    
#--------------------------------------------------------------------------
# Internal methods
#--------------------------------------------------------------------------

"""
Returns the QModelIndex for child with given name and parent index
Warning: may works incorrectly for items with duplicated names   
"""    
def getChildModelIndex(parentIdx, name):
    row=0
    if parentIdx is None:
        return None
    childIdx = parentIdx.child(row, 0)
    while childIdx.isValid():
        if name == str(childIdx.data().toString()):
            return childIdx
        row += 1
        childIdx = childIdx.sibling(row, 0)
    return None    

"""
Returns the QModelIndex form given tree by list of hierarchical names 
For example:  getModelIndexByName(tree, "HYDRO", "NATURAL OBJECTS", "Immersible zone_1")
""" 
def getModelIndexByName(treeObj, *parentNames):
    parentNodeName = parentNames[0].replace('_',"\\\\_")
    waitForObjectItem(treeObj,  parentNodeName)
    # clickItem(treeObj, parentNodeName, 5, 5, 0, Qt.LeftButton)
    
    root = treeObj.indexAt(QPoint(0,0))
     
    for name in parentNames[1:]:
        idx = getChildModelIndex(root, name)
        root = idx
    return root    


    
def getObjectBrowserItem(theItem, theParent=":HYDRO_QModelIndex", theOccurrence = 1):
    return "{column='0' container='%s' occurrence='%s' text='%s' type='QModelIndex'}" % (theParent,  theOccurrence, theItem)  

def getItemText(item, parent):
    if parent is None:
        return item
    else:
        return "%s.%s" % (parent, item.replace('_',"\\\\_"))

def popupItem(*menuPath):
    menu = waitForObject("{type='QtxMenu' unnamed='1' visible='1'}")
    parent = "{type='QtxMenu' unnamed='1' visible='1'}"
    for item in menuPath[:-1]:
        test.log("Element",str(item)+" menu "+str(menu))
        activateItem(waitForObjectItem(menu, item))
        menu = "{title='%s' type='QMenu' unnamed='1' visible='1'}" % (item)
        parent = menu
    activateItem(waitForObjectItem(menu, menuPath[-1]))

def viewerPopupItem(menu_type, *menuPath ):
    menu = waitForObject("{type='%s' unnamed='1' visible='1'}" % menu_type)
        
    for item_name in menuPath[:-1]:
        menu_children = object.children(menu)
        for child in menu_children:
            properties = object.properties(child)
            if properties.get("text", "") == item_name:
                mouseClick(child, 5, 5, 0, Qt.LeftButton)
                menu = child
                break
            
    activateItem(waitForObjectItem(menu, menuPath[-1]))

def activateViewerContextMenuItem(viewer, x, y, *menuPath): 
    vtkViewer = waitForObject(viewer)
    if x < 0:
        x = vtkViewer.width/2
    if y < 0:
        y = vtkViewer.height/2
    
    mouseClick(vtkViewer, x, y, 0, Qt.LeftButton)
    mouseClick(vtkViewer, x, y, 0, Qt.RightButton)
    viewerPopupItem("QtxMenu", *menuPath)
    res = waitFor("object.exists('%s')" % "{type=\\'QtxMenu\\' unnamed=\\'1\\' visible=\\'1\\'}",10000)
    if res:
        mouseClick(vtkViewer, x, y, 0, Qt.LeftButton)


def getFullItemName(itemIndex):
    fullName = str(itemIndex.data().toString())
    parentIndex = itemIndex.parent()
    
    while parentIndex.isValid():
        parentName = str(parentIndex.data().toString())
        fullName = "%s::%s" % (parentName, fullName) 
        
        parentIndex = parentIndex.parent()
        
    return fullName
    
def getChildValues(parentIndex, theIsRecursive = True):
    values = {}
    row = 0
    child_index0 = parentIndex.child(row, 0)
    while child_index0.isValid():
        child_index1 = child_index0.sibling(row, 1)
        if child_index1.isValid():
            key = getFullItemName(child_index0) # str(child_index0.data().toString())
            value = str(child_index1.data().toString())
            values[key] = value
        
        if theIsRecursive:
            values.update(getChildValues(child_index0))
            
        row += 1
        child_index0 = parentIndex.child(row, 0)
        
    return values 
    
def getTreeValues(treeObj):
    root = treeObj.indexAt(QPoint(0,0))
    
    return getChildValues(root)
    
def checkContainsStringValues(values, stringKeys):
    res = True
    
    for key in stringKeys:
        if key not in values:
            res = False
            test.fatal("Map of values doesn't contain %s key" % key)
        elif len(values[key]) < 1:
            res = False
            test.fatal("Value for key %s is empty" % key)
            
    return res
     
def isInteger(value):
    res = True
    
    intValue = cast(value, int)
    res = intValue is not None
        
    return res

def isFloat(value):
    res = True
    
    try:
        float(value)
    except ValueError:
        res = False
        
    return res
     
def checkContainsIntValues(values, intKeys):
    res = True
    
    for key in intKeys:
        if key not in values:
            res = False
            test.fatal("Map of values doesn't contain %s key" % key)
        elif not isInteger(values[key]):
            res = False
            test.fatal("Value for key %s is not an integer ('%s')" % (key, values[key]))
            
    return res
    
def checkContainsListOfIntValues(values, listOfIntKeys, sep):
    res = True
    
    for key in listOfIntKeys:
        if key not in values:
            res = False
            test.fatal("Map of values doesn't contain %s key" % key)
        else:
            listItems = values[key].split(sep)
            
            for item in listItems:
                if not isInteger(item):
                    res = False
                    test.fatal("Value for key %s is not a list of integer ('%s')" % (key, values[key]))
            
    return res
    
def checkContainsFloatValues(values, floatKeys):
    res = True
    
    for key in floatKeys:
        if key not in values:
            res = False
            test.fatal("Map of values doesn't contain %s key" % key)
        elif not isFloat(values[key]):
            res = False
            test.fatal("Value for key %s is not a float ('%s')" % (key, values[key]))
            
    return res
    
"""
Set the given value to the input field.
"""
def setInputFieldValue(field, value):
    type(field, "<Ctrl+A>")
    type(field, value)
    
    
def clearSelection():
    mouseClick(waitForObject(":SALOME *.3D View Operations_OCCViewer_ViewPort3d"), 1, 1, 0, Qt.LeftButton)

def getChildNames(tree, root):
    row = 0
    list=[]
    rootName = str(root.data().toString())
    while root.isValid():
        rootName1 = str(root.data().toString())
        child = root.child(0, 0)
        if child.isValid():
            childName = str(child.data().toString())
            list.extend( getChildNames(tree, child) )
        else:
            if not tree.isRowHidden(row, root.parent()):
                name = getFullItemName(root)
                list.append(name)    
        row += 1
        root = root.sibling(row, 0)
        
    return list

def getTreeNames(tree):
    root = tree.indexAt(QPoint(0,0))
    return getChildNames( tree, root)


        
"""Common functions for HYDRO module"""
import os
import datetime, time

source(os.path.join(os.getenv("COMMON_SCRIPT_DIR"), "common.py"))


#--------------------------------------------------------------------------
# HYDRO module activation
#--------------------------------------------------------------------------
"""
Activate HYDRO module via toolbar and check that OB, Graphics viewer and OCC Viewer are created
"""  
def activateHYDRO():
    clickButton(waitForObject(":SALOME *.HYDRO_QToolButton"))
    waitFor("object.exists(':Object Browser_QtxTreeView')", 20000)
    test.compare(findObject(":Object Browser_QtxTreeView").visible, True)
    waitFor("object.exists(':SALOME *.3D View Operations_OCCViewer_ViewPort3d')", 20000)
    test.compare(findObject(":SALOME *.3D View Operations_OCCViewer_ViewPort3d").visible, True)
    waitForObject(":SALOME_STD_TabDesktop").resize(1916, 930)
    waitForObject(":SALOME *.3D View Operations_OCCViewer_ViewPort3d").resize(1527, 601)
    clickTab(waitForObject(":SALOME *_QtxWorkstackTabBar"), "Graphics scene:1 - viewer:1")
    waitFor("object.exists(':SALOME *_GraphicsView_ViewPort')", 20000)
    test.compare(findObject(":SALOME *_GraphicsView_ViewPort").visible, True)
    clickTab(waitForObject(":SALOME *_QtxWorkstackTabBar"), "OCC scene:1 - viewer:1")

#--------------------------------------------------------------------------
# Show/hide
#--------------------------------------------------------------------------
"""
Hide all objects displayed in the current OCC view.
"""
def occHideAll():
    activateOCCViewerContextMenuItem(100, 100, "Hide all")
    
"""
Show all objects in the current OCC view.
"""
def occShowAll():
    activateOCCViewerContextMenuItem(100, 100, "Show all")
    
"""
Hide all objects displayed in the current VTK view.
"""
def vtkHideAll():
    activateVTKContextMenuItem(100, 100, "Hide all")
    
"""
Show all objects in the current VTK view.
"""
def vtkShowAll():
    activateVTKContextMenuItem(100, 100, "Show all")

#--------------------------------------------------------------------------
# Bathymetry
#--------------------------------------------------------------------------
def importBathymetry(filePath, bathName=None, isToInvert=False):
    # Click Ctrl+B
    type(waitForObject(":SALOME_STD_TabDesktop"), "<Ctrl+B>")
    
    # "Import bathymetry" panel appears
    bath_panel = waitForObject(":SALOME *.Import bathymetry_HYDROGUI_ImportBathymetryDlg")
    test.compare(bath_panel.windowTitle, "Import bathymetry")
    
    # Click "Open file" icon 
    clickButton(waitForObject(":Import bathymetry from file_QToolButton"))
    
    # "Import bathymetry from file" dialog appears
    open_file_dlg = waitForObject(":Import bathymetry from file_SUIT_FileDlg")
    test.compare(str(open_file_dlg.windowTitle), "Import bathymetry from file")
    
    # Set bathymetry file path
    type(waitForObject(":fileNameEdit_QLineEdit"), filePath)
    
    # Click "Open" button
    clickButton(waitForObject(":Import bathymetry.Open_QPushButton"))

    # Set "Invert altitude values" option
    invertCheckBox = waitForObject(":Import bathymetry.Invert altitude values_QCheckBox")
    setButtonChecked(invertCheckBox, isToInvert)
    
    # Set bathymetry name
    if bathName is not None:
        type(waitForObject(":Bathymetry name.Name_QLineEdit"), "<Ctrl+A>")
        type(waitForObject(":Bathymetry name.Name_QLineEdit"), bathName)

    # Click Apply
    clickButton(waitForObject(":Import bathymetry.Apply_QPushButton"))

#--------------------------------------------------------------------------
# Polyline
#--------------------------------------------------------------------------   
"""
Add spline section to the polyline.
"""
def addSection(isSpline, isClosed):
    # New section
    clickButton(waitForObject(":Sections.New section_QToolButton"))
    
    # Choose type
    sectionType = "Polyline"
    if isSpline:
        sectionType = "Spline"   
    
    combo = waitForObject(":Add element.Type_QComboBox")
    selectComboBoxItem(combo, sectionType)

    # Set closed property
    closedCheckBox = waitForObject(":Add element.Closed_QCheckBox")
    setButtonChecked(closedCheckBox, isClosed)
    
    # Click add button
    clickButton(waitForObject(":Add element.Add_QPushButton"))

"""
Add spline section to the polyline.
"""
def addSplineSection(isClosed):
    addSection(True, isClosed)

"""
Add polyline (not spline) section to the polyline.
"""
def addPolylineSection(isClosed):
    addSection(False, isClosed)

"""
Add points to the polyline.
For example: addPolylinePoints([(621, 298), (571, 347), (620, 399)])
"""    
def addPolylinePoints(points):
    # Turn addition mode on
    additionModeButton = waitForObject(":Sections.Addition mode_QToolButton")
    setButtonChecked(additionModeButton, True)
    
    # Add points by clicking in OCC viewer
    occMouseClicks(points)

    
#--------------------------------------------------------------------------
# Coordinate system
#--------------------------------------------------------------------------
"""
Get status bar message.
"""
def getStatusMessage():
    statusBar = waitForObject(":SALOME *_QStatusBar")
    return str(statusBar.currentMessage())
   
"""
Get local coordinates from the status bar as a tuple of floats (x, y).
"""
def getLocalCoordinates():
    x = None
    y = None
    
    msg = getStatusMessage()
    
    if msg.find("Local") > 0:
        lmsg = msg.split("(")[1].split(")")[0]
        (x,y) = msg.split(",")
        (x,y) = (float(x), float(y))
        
    return (x, y)

"""
Get global coordinates from the status bar as a tuple of floats (x, y).
"""
def getGlobalCoordinates():
    x = None
    y = None
    
    msg = getStatusMessage()
    
    if msg.find("Global") > 0:
        lmsg = msg.split("(")[2].split(")")[0]
        (x,y) = msg.split(",")
        (x,y) = (float(x), float(y))
        
    return (x, y)
        
    
    

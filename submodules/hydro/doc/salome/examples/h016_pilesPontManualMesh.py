# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")
if not os.path.isdir(HYDRO_TEST_RESOURCES):
    os.mkdir(HYDRO_TEST_RESOURCES)

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

import sys
import salome

salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import loadImage, GeolocaliseImageCoords, GeolocaliseImageReference
from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy
from salome.hydrotools.hydroGeoMeshUtils import splitShapesAll, mergePolylines

hydro_doc = HYDROData_Document.Document()

offsetX = 430000.
offsetY = 6350000.
hydro_doc.SetLocalCS( offsetX, offsetY )

garonne_ign_01 = loadImage(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_ign_01.png"), "garonne_ign_01", 0)
GeolocaliseImageCoords(garonne_ign_01,
                       QPoint(40, 817), QPoint(1325, 85),
                       QPointF(471562, 6.36775e+06), QPointF(489400, 6.37702e+06))

garonne_ign_02 = loadImage(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_ign_02.png"), "garonne_ign_02", 1)
GeolocaliseImageReference(garonne_ign_02, garonne_ign_01,
                          QPoint(1389, 447), QPoint(784, 481),
                          QPoint(631, 95), QPoint(26, 129))

garonnes = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne.shp"), True, 5)
limites_domaine = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "domaine.shp"), False, 6)
lits_majeur = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "lit_majeur.shp"), True, 7)
pilesPont = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "pilesPont.shp"), False, 8)
zonesPont = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "zonePont.shp"), False, 9)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))
garonne_point_L93 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_point_L93.xyz"))

domaineEtendu = createImmersibleZone(hydro_doc, "domaineEtendu", limites_domaine[0], Cloud_02, True, 2)
litMajeur = createImmersibleZone(hydro_doc, "litMajeur", lits_majeur[0], garonne_point_L93, True, 3)

seq = garonnes + zonesPont
garzones = splitShapesAll(hydro_doc, seq)

garZonesPont = [garzones[0], garzones[2], garzones[4], garzones[6]]
litMineurPont = mergePolylines(hydro_doc, "litMineurPont", garZonesPont)

garonnesAval = [garzones[3], garzones[4]]
litMineurAval = mergePolylines(hydro_doc, "litMineurAval", garonnesAval)

garonnesAmont = [garzones[1], garzones[6]]
litMineurAmont = mergePolylines(hydro_doc, "litMineurAmont", garonnesAmont)

litMineur_aval = createImmersibleZone(hydro_doc, "litMineur_aval", litMineurAval, garonne_point_L93, True, 3)
litMineur_amont = createImmersibleZone(hydro_doc, "litMineur_amont", litMineurAmont, garonne_point_L93, True, 3)
litMineur_pont = createImmersibleZone(hydro_doc, "litMineur_pont", litMineurPont, garonne_point_L93, True, 3)

pile_pont0 = createImmersibleZone(hydro_doc, "pile_pont0", pilesPont[0], None, False, 3)
pile_pont1 = createImmersibleZone(hydro_doc, "pile_pont1", pilesPont[1], None, False, 3)
pile_pont2 = createImmersibleZone(hydro_doc, "pile_pont2", pilesPont[2], None, False, 3)
pile_pont3 = createImmersibleZone(hydro_doc, "pile_pont3", pilesPont[3], None, False, 3)


# Calculation case
garonne_1 = hydro_doc.CreateObject( KIND_CALCULATION )
garonne_1.SetName( "garonne_1" )

garonne_1.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
garonne_1.AddGeometryObject( domaineEtendu )
garonne_1.AddGeometryObject( litMajeur )
garonne_1.AddGeometryObject( litMineur_amont )
garonne_1.AddGeometryObject( litMineur_aval )
garonne_1.AddGeometryObject( litMineur_pont )
garonne_1.AddGeometryObject( pile_pont0 )
garonne_1.AddGeometryObject( pile_pont1 )
garonne_1.AddGeometryObject( pile_pont2 )
garonne_1.AddGeometryObject( pile_pont3 )

case_geom_group = domaineEtendu.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = litMajeur.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = litMineur_amont.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = litMineur_aval.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = litMineur_pont.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = pile_pont0.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = pile_pont1.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = pile_pont2.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = pile_pont3.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
garonne_1.SetBoundaryPolyline( limites_domaine[0] )

# Start the algorithm of the partition and assignment
garonne_1.Update()
garonne_1_riveDroite = hydro_doc.FindObjectByName( "garonne_1_Reg_1" )
garonne_1_Zone_1 = hydro_doc.FindObjectByName( "garonne_1_Zone_1" )
garonne_1_Zone_1.SetColor( QColor( 64, 156, 192 ))
garonne_1_riveDroite.AddZone( garonne_1_Zone_1 )
garonne_1_Zone_2 = hydro_doc.FindObjectByName( "garonne_1_Zone_2" )
garonne_1_Zone_2.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
garonne_1_Zone_2.SetMergeObject( Cloud_02 )
garonne_1_Zone_2.SetColor( QColor( 192, 183, 64 ))
garonne_1_riveDroite.AddZone( garonne_1_Zone_2 )
garonne_1_aval = hydro_doc.FindObjectByName( "garonne_1_Reg_2" )
garonne_1_Zone_3 = hydro_doc.FindObjectByName( "garonne_1_Zone_3" )
garonne_1_Zone_3.SetMergeType( HYDROData_Zone.Merge_Object )
garonne_point_L93 = hydro_doc.FindObjectByName( "garonne_point_L93" )
garonne_1_Zone_3.SetMergeObject( garonne_point_L93 )
garonne_1_Zone_3.SetColor( QColor( 64, 183, 192 ))
garonne_1_aval.AddZone( garonne_1_Zone_3 )
garonne_1_riveGauche = hydro_doc.FindObjectByName( "garonne_1_Reg_3" )
garonne_1_Zone_4 = hydro_doc.FindObjectByName( "garonne_1_Zone_4" )
garonne_1_Zone_4.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
garonne_1_Zone_4.SetMergeObject( Cloud_02 )
garonne_1_Zone_4.SetColor( QColor( 64, 192, 162 ))
garonne_1_riveGauche.AddZone( garonne_1_Zone_4 )
garonne_1_Zone_6 = hydro_doc.FindObjectByName( "garonne_1_Zone_6" )
garonne_1_Zone_6.SetColor( QColor( 64, 192, 90 ))
garonne_1_riveGauche.AddZone( garonne_1_Zone_6 )
garonne_1_pont = hydro_doc.FindObjectByName( "garonne_1_Reg_4" )
garonne_1_Zone_5 = hydro_doc.FindObjectByName( "garonne_1_Zone_5" )
garonne_1_Zone_5.SetMergeType( HYDROData_Zone.Merge_Object )
garonne_point_L93 = hydro_doc.FindObjectByName( "garonne_point_L93" )
garonne_1_Zone_5.SetMergeObject( garonne_point_L93 )
garonne_1_Zone_5.SetColor( QColor( 64, 190, 192 ))
garonne_1_pont.AddZone( garonne_1_Zone_5 )
garonne_1_amont = hydro_doc.FindObjectByName( "garonne_1_Reg_5" )
garonne_1_Zone_7 = hydro_doc.FindObjectByName( "garonne_1_Zone_7" )
garonne_1_Zone_7.SetMergeType( HYDROData_Zone.Merge_Object )
garonne_point_L93 = hydro_doc.FindObjectByName( "garonne_point_L93" )
garonne_1_Zone_7.SetMergeObject( garonne_point_L93 )
garonne_1_Zone_7.SetColor( QColor( 109, 192, 64 ))
garonne_1_amont.AddZone( garonne_1_Zone_7 )
garonne_1_pile1 = hydro_doc.FindObjectByName( "garonne_1_Reg_6" )
garonne_1_Zone_8 = hydro_doc.FindObjectByName( "garonne_1_Zone_8" )
garonne_1_Zone_8.SetMergeType( HYDROData_Zone.Merge_ZMAX )
garonne_1_Zone_8.SetColor( QColor( 192, 179, 64 ))
garonne_1_pile1.AddZone( garonne_1_Zone_8 )
garonne_1_pile2 = hydro_doc.FindObjectByName( "garonne_1_Reg_7" )
garonne_1_Zone_9 = hydro_doc.FindObjectByName( "garonne_1_Zone_9" )
garonne_1_Zone_9.SetMergeType( HYDROData_Zone.Merge_ZMAX )
garonne_1_Zone_9.SetColor( QColor( 192, 75, 64 ))
garonne_1_pile2.AddZone( garonne_1_Zone_9 )
garonne_1_pile3 = hydro_doc.FindObjectByName( "garonne_1_Reg_8" )
garonne_1_Zone_10 = hydro_doc.FindObjectByName( "garonne_1_Zone_10" )
garonne_1_Zone_10.SetMergeType( HYDROData_Zone.Merge_ZMAX )
garonne_1_Zone_10.SetColor( QColor( 162, 192, 64 ))
garonne_1_pile3.AddZone( garonne_1_Zone_10 )
garonne_1_pile4 = hydro_doc.FindObjectByName( "garonne_1_Reg_9" )
garonne_1_Zone_11 = hydro_doc.FindObjectByName( "garonne_1_Zone_11" )
garonne_1_Zone_11.SetMergeType( HYDROData_Zone.Merge_ZMAX )
garonne_1_Zone_11.SetColor( QColor( 64, 192, 81 ))
garonne_1_pile4.AddZone( garonne_1_Zone_11 )
garonne_1_riveDroite.SetName( "garonne_1_riveDroite" )
garonne_1_aval.SetName( "garonne_1_aval" )
garonne_1_riveGauche.SetName( "garonne_1_riveGauche" )
garonne_1_pont.SetName( "garonne_1_pont" )
garonne_1_amont.SetName( "garonne_1_amont" )
garonne_1_pile1.SetName( "garonne_1_pile1" )
garonne_1_pile2.SetName( "garonne_1_pile2" )
garonne_1_pile3.SetName( "garonne_1_pile3" )
garonne_1_pile4.SetName( "garonne_1_pile4" )

# Export of the calculation case
garonne_1_entry = garonne_1.Export()

# Get geometry shape and print debug information
import GEOM
print("Entry:", garonne_1_entry)
HYDRO_garonne_1 = salome.IDToObject( str( garonne_1_entry ) )
print("Geom shape:", HYDRO_garonne_1)
print("Geom shape name:", HYDRO_garonne_1.GetName())


###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

geompy = geomBuilder.New()

children = getChildrenInStudy(HYDRO_garonne_1)
garonne_riveGauche = children["garonne_1_riveGauche"]
garonne_riveDroite = children["garonne_1_riveDroite"]
garonne_aval = children["garonne_1_aval"]
garonne_pont = children["garonne_1_pont"]
garonne_amont = children["garonne_1_amont"]

# --- basic properties control: edges length, surfaces
controlGeomProps(geompy, garonne_riveGauche,  29149.1353799,  35949580.6716)
controlGeomProps(geompy, garonne_aval,   7965.23431497,  935955.786347)
controlGeomProps(geompy, garonne_pont,   962.0745533,  19649.150520)
controlGeomProps(geompy, garonne_amont,   22635.6212065,  2531409.65041)
controlGeomProps(geompy, garonne_riveDroite,  32012.2241814, 25998085.6892)

# --- manual identification of all useful edge groups (boundary conditions)

allEdgesIds = geompy.SubShapeAllIDs(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
print("allEdgesIds", allEdgesIds)

(isDone, ClosedFreeBoundary, OpenFreeBoundary) = geompy.GetFreeBoundary(HYDRO_garonne_1)
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[0], "OuterFreeBoundary")
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[1], "pilesPont1")
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[2], "pilesPont2")
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[3], "pilesPont3")
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[4], "pilesPont4")

freeBoundary = geompy.ExtractShapes(ClosedFreeBoundary[0], geompy.ShapeType["EDGE"], True)
freeBoundaryIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, freeBoundary[i]) for i in range(len(freeBoundary)) ]
print("freeBoundaryIds", freeBoundaryIds)

edgesPile1 = geompy.ExtractShapes(ClosedFreeBoundary[1], geompy.ShapeType["EDGE"], True)
edgesPile2 = geompy.ExtractShapes(ClosedFreeBoundary[2], geompy.ShapeType["EDGE"], True)
edgesPile3 = geompy.ExtractShapes(ClosedFreeBoundary[3], geompy.ShapeType["EDGE"], True)
edgesPile4 = geompy.ExtractShapes(ClosedFreeBoundary[4], geompy.ShapeType["EDGE"], True)
pilesPont1Ids = [ geompy.GetSubShapeID(HYDRO_garonne_1, edgesPile1[i]) for i in range(len(edgesPile1)) ]
pilesPont2Ids = [ geompy.GetSubShapeID(HYDRO_garonne_1, edgesPile2[i]) for i in range(len(edgesPile2)) ]
pilesPont3Ids = [ geompy.GetSubShapeID(HYDRO_garonne_1, edgesPile3[i]) for i in range(len(edgesPile3)) ]
pilesPont4Ids = [ geompy.GetSubShapeID(HYDRO_garonne_1, edgesPile4[i]) for i in range(len(edgesPile4)) ]
pilesPontIds = pilesPont1Ids + pilesPont2Ids + pilesPont3Ids + pilesPont4Ids
print("pilesPontIds", pilesPontIds)

[garonne_aval_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_aval], geompy.ShapeType["EDGE"], True)
[garonne_aval_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_aval], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_garonne_1, garonne_aval_droite, "garonne_aval_droite")
geompy.addToStudyInFather(HYDRO_garonne_1, garonne_aval_gauche, "garonne_aval_gauche")
[garonne_amont_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_amont], geompy.ShapeType["EDGE"], True)
[garonne_amont_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_amont], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_garonne_1, garonne_amont_droite, "garonne_amont_droite")
geompy.addToStudyInFather(HYDRO_garonne_1, garonne_amont_gauche, "garonne_amont_gauche")
[garonne_pont_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_pont], geompy.ShapeType["EDGE"], True)
[garonne_pont_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_pont], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_garonne_1, garonne_pont_droite, "garonne_pont_droite")
geompy.addToStudyInFather(HYDRO_garonne_1, garonne_pont_gauche, "garonne_pont_gauche")
rives = [garonne_aval_droite, garonne_aval_gauche, garonne_amont_droite, garonne_amont_gauche, garonne_pont_droite, garonne_pont_gauche]
rivesIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, rives[i]) for i in range(len(rives)) ]
print("rivesIds", rivesIds)

edges_garonne_aval = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_aval], geompy.ShapeType["EDGE"], True)
edges_garonne_amont = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_amont], geompy.ShapeType["EDGE"], True)
edges_garonne_pont = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_pont], geompy.ShapeType["EDGE"], True)
edges_riveGauche = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_riveGauche], geompy.ShapeType["EDGE"], True)
edges_riveDroite = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_riveDroite], geompy.ShapeType["EDGE"], True)
edges_garonne_avalIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_garonne_aval[i]) for i in range(len(edges_garonne_aval)) ]
edges_garonne_amontIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_garonne_amont[i]) for i in range(len(edges_garonne_amont)) ]
edges_garonne_pontIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_garonne_pont[i]) for i in range(len(edges_garonne_pont)) ]
edges_riveGaucheIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_riveGauche[i]) for i in range(len(edges_riveGauche)) ]
edges_riveDroiteIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_riveDroite[i]) for i in range(len(edges_riveDroite)) ]

print("edges_garonne_amontIds", edges_garonne_amontIds) 
print("edges_garonne_avalIds", edges_garonne_avalIds) 
print("edges_garonne_pontIds", edges_garonne_pontIds) 
print("edges_riveGaucheIds", edges_riveGaucheIds)
print("edges_riveDroiteIds", edges_riveDroiteIds)

edges_litMineurIds = edges_garonne_amontIds
for edge in edges_garonne_avalIds:
  edges_litMineurIds.append(edge)
for edge in edges_garonne_pontIds:
  if edge not in edges_litMineurIds and edge not in pilesPontIds:
    edges_litMineurIds.append(edge)

sectionsIds = [Id for Id in edges_litMineurIds if Id not in rivesIds]
print("sectionsIds", sectionsIds)
SectionsGaronne = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(SectionsGaronne, sectionsIds)
geompy.addToStudyInFather(HYDRO_garonne_1, SectionsGaronne, "SectionsGaronne")

bordGaucheDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveGaucheIds]
bordDroiteDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveDroiteIds]
print("bordGaucheDomaineIds", bordGaucheDomaineIds)
print("bordDroiteDomaineIds", bordDroiteDomaineIds)
bordGaucheDomaine = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordGaucheDomaine, bordGaucheDomaineIds)
geompy.addToStudyInFather(HYDRO_garonne_1, bordGaucheDomaine, "bordGaucheDomaine")
bordDroiteDomaine = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordDroiteDomaine, bordDroiteDomaineIds)
geompy.addToStudyInFather(HYDRO_garonne_1, bordDroiteDomaine, "bordDroiteDomaine")

amont = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(46757.861314, 25833.234752, 0))
aval = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(39078.979127, 32588.627279, 0))
geompy.addToStudyInFather(HYDRO_garonne_1, amont, "amont")
geompy.addToStudyInFather(HYDRO_garonne_1, aval, "aval")
###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats
import tempfile

smesh = smeshBuilder.New()
garonne_1 = smesh.Mesh(HYDRO_garonne_1)
NETGEN_1D_2D = garonne_1.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters_1 = NETGEN_1D_2D.Parameters()
NETGEN_2D_Parameters_1.SetMaxSize( 200 )
NETGEN_2D_Parameters_1.SetSecondOrder( 0 )
NETGEN_2D_Parameters_1.SetOptimize( 1 )
NETGEN_2D_Parameters_1.SetFineness( 4 )
NETGEN_2D_Parameters_1.SetMinSize( 50 )
NETGEN_2D_Parameters_1.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters_1.SetFuseEdges( 1 )
NETGEN_2D_Parameters_1.SetQuadAllowed( 0 )
Regular_1D = garonne_1.Segment(geom=garonne_aval)
garonne_aval_1 = Regular_1D.GetSubMesh()
Number_of_Segments_1 = Regular_1D.NumberOfSegments(100)
QuadFromMedialAxis_1D2D = garonne_1.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=garonne_aval)
Regular_1D_1 = garonne_1.Segment(geom=garonne_amont)
garonne_amont_1 = Regular_1D_1.GetSubMesh()
Number_of_Segments_2 = Regular_1D_1.NumberOfSegments(150)
QuadFromMedialAxis_1D2D_1 = garonne_1.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=garonne_amont)
Regular_1D_2 = garonne_1.Segment(geom=SectionsGaronne)
sections = Regular_1D_2.GetSubMesh()
Number_of_Segments_3 = Regular_1D_2.NumberOfSegments(10)
isDone = garonne_1.SetMeshOrder( [ [ sections, garonne_aval_1, garonne_amont_1 ] ])
NETGEN_1D_2D_1 = garonne_1.Triangle(algo=smeshBuilder.NETGEN_1D2D,geom=garonne_pont)
garonne_pont_1 = NETGEN_1D_2D_1.GetSubMesh()
NETGEN_2D_Parameters_2 = NETGEN_1D_2D_1.Parameters()
NETGEN_2D_Parameters_2.SetMaxSize( 50 )
NETGEN_2D_Parameters_2.SetSecondOrder( 0 )
NETGEN_2D_Parameters_2.SetOptimize( 1 )
NETGEN_2D_Parameters_2.SetFineness( 3 )
NETGEN_2D_Parameters_2.SetMinSize( 10 )
NETGEN_2D_Parameters_2.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters_2.SetFuseEdges( 1 )
NETGEN_2D_Parameters_2.SetQuadAllowed( 0 )
isDone = garonne_1.SetMeshOrder( [ [ sections, garonne_aval_1, garonne_amont_1, garonne_pont_1 ] ])
isDone = garonne_1.Compute()
isDone = garonne_1.SplitQuadObject( garonne_1, 1 )

# --- geometrical groups of faces

garonne_riveDroite_2 = garonne_1.GroupOnGeom(garonne_riveDroite,'garonne_riveDroite',SMESH.FACE)
garonne_aval_2 = garonne_1.GroupOnGeom(garonne_aval,'garonne_aval',SMESH.FACE)
garonne_riveGauche_2 = garonne_1.GroupOnGeom(garonne_riveGauche,'garonne_riveGauche',SMESH.FACE)
garonne_pont_2 = garonne_1.GroupOnGeom(garonne_pont,'garonne_pont',SMESH.FACE)
garonne_amont_2 = garonne_1.GroupOnGeom(garonne_amont,'garonne_amont',SMESH.FACE)

# --- geometrical groups of edges

ClosedFreeBoundary_1 = garonne_1.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.EDGE)
garonne_amont_droite_2 = garonne_1.GroupOnGeom(garonne_amont_droite,'garonne_amont_droite',SMESH.EDGE)
garonne_amont_gauche_2 = garonne_1.GroupOnGeom(garonne_amont_gauche,'garonne_amont_gauche',SMESH.EDGE)
garonne_aval_droite_2 = garonne_1.GroupOnGeom(garonne_aval_droite,'garonne_aval_droite',SMESH.EDGE)
garonne_aval_gauche_2 = garonne_1.GroupOnGeom(garonne_aval_gauche,'garonne_aval_gauche',SMESH.EDGE)
garonne_pont_droite_2 = garonne_1.GroupOnGeom(garonne_pont_droite,'garonne_pont_droite',SMESH.EDGE)
garonne_pont_gauche_2 = garonne_1.GroupOnGeom(garonne_pont_gauche,'garonne_pont_gauche',SMESH.EDGE)
SectionsGaronne_1 = garonne_1.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.EDGE)
bordGaucheDomaine_1 = garonne_1.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.EDGE)
bordDroiteDomaine_1 = garonne_1.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.EDGE)
amont_1 = garonne_1.GroupOnGeom(amont,'amont',SMESH.EDGE)
aval_1 = garonne_1.GroupOnGeom(aval,'aval',SMESH.EDGE)
pilePont1_1 = garonne_1.GroupOnGeom(ClosedFreeBoundary[1],'pilePont1',SMESH.EDGE)
pilePont2_1 = garonne_1.GroupOnGeom(ClosedFreeBoundary[2],'pilePont2',SMESH.EDGE)
pilePont3_1 = garonne_1.GroupOnGeom(ClosedFreeBoundary[3],'pilePont3',SMESH.EDGE)
pilePont4_1 = garonne_1.GroupOnGeom(ClosedFreeBoundary[4],'pilePont4',SMESH.EDGE)

# --- geometrical groups of nodes

garonne_amont_3 = garonne_1.GroupOnGeom(garonne_amont,'garonne_amont',SMESH.NODE)
garonne_aval_3 = garonne_1.GroupOnGeom(garonne_aval,'garonne_aval',SMESH.NODE)
garonne_pont_3 = garonne_1.GroupOnGeom(garonne_pont,'garonne_pont',SMESH.NODE)
garonne_riveDroite_3 = garonne_1.GroupOnGeom(garonne_riveDroite,'garonne_riveDroite',SMESH.NODE)
garonne_riveGauche_3 = garonne_1.GroupOnGeom(garonne_riveGauche,'garonne_riveGauche',SMESH.NODE)
ClosedFreeBoundary_2 = garonne_1.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.NODE)
garonne_amont_droite_2 = garonne_1.GroupOnGeom(garonne_amont_droite,'garonne_amont_droite',SMESH.NODE)
garonne_amont_gauche_2 = garonne_1.GroupOnGeom(garonne_amont_gauche,'garonne_amont_gauche',SMESH.NODE)
garonne_aval_droite_2 = garonne_1.GroupOnGeom(garonne_aval_droite,'garonne_aval_droite',SMESH.NODE)
garonne_aval_gauche_2 = garonne_1.GroupOnGeom(garonne_aval_gauche,'garonne_aval_gauche',SMESH.NODE)
garonne_pont_droite_2 = garonne_1.GroupOnGeom(garonne_pont_droite,'garonne_pont_droite',SMESH.NODE)
garonne_pont_gauche_2 = garonne_1.GroupOnGeom(garonne_pont_gauche,'garonne_pont_gauche',SMESH.NODE)
SectionsGaronne_2 = garonne_1.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.NODE)
bordGaucheDomaine_2 = garonne_1.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.NODE)
bordDroiteDomaine_2 = garonne_1.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.NODE)
amont_2 = garonne_1.GroupOnGeom(amont,'amont',SMESH.NODE)
aval_2 = garonne_1.GroupOnGeom(aval,'aval',SMESH.NODE)
pilePont1_2 = garonne_1.GroupOnGeom(ClosedFreeBoundary[1],'pilePont1',SMESH.NODE)
pilePont2_2 = garonne_1.GroupOnGeom(ClosedFreeBoundary[2],'pilePont2',SMESH.NODE)
pilePont3_2 = garonne_1.GroupOnGeom(ClosedFreeBoundary[3],'pilePont3',SMESH.NODE)
pilePont4_2 = garonne_1.GroupOnGeom(ClosedFreeBoundary[4],'pilePont4',SMESH.NODE)

## Set names of Mesh objects
smesh.SetName(NETGEN_1D_2D.GetAlgorithm(), 'NETGEN 1D-2D')
smesh.SetName(QuadFromMedialAxis_1D2D.GetAlgorithm(), 'QuadFromMedialAxis_1D2D')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Number_of_Segments_1, 'Number of Segments_1')
smesh.SetName(Number_of_Segments_2, 'Number of Segments_2')
smesh.SetName(NETGEN_2D_Parameters_1, 'NETGEN 2D Parameters_1')
smesh.SetName(Number_of_Segments_3, 'Number of Segments_3')
smesh.SetName(NETGEN_2D_Parameters_2, 'NETGEN 2D Parameters_2')

smesh.SetName(garonne_aval_1, 'submesh_garonne_aval')
smesh.SetName(garonne_pont_1, 'submesh_garonne_pont')
smesh.SetName(garonne_amont_1, 'submesh_garonne_amont')
smesh.SetName(garonne_1.GetMesh(), 'submesh_garonne_1')
smesh.SetName(sections, 'submesh_sections')


garonne_1.SetAutoColor( 1 )
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)
fichierMaillage = os.path.join(tmpdir, 'garonne_1.med')
garonne_1.ExportMED(fichierMaillage, 0, SMESH.MED_V2_2, 1, None ,1)

controlMeshStats(garonne_1, 6985, 745, 13761)
controlSubMeshStats(garonne_pont_2, 202)
controlSubMeshStats(garonne_amont_2, 3000)
controlSubMeshStats(garonne_aval_2, 2000)
controlSubMeshStats(garonne_riveDroite_2, 3941)
controlSubMeshStats(garonne_riveGauche_2, 4635)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

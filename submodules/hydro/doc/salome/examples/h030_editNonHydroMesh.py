# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

# --- generated resources

origFileMesh = os.path.join(HYDRO_TEST_RESOURCES, 'garonne_2.med')

# ----------------------------------------------------------------------------------
# --- domain extension

import sys
import salome

salome.salome_init()

from salome.hydrotools.shapesGroups import freeBordersGroup, exploreEdgeGroups, fitShapePointsToMesh

offsetX = 430000.
offsetY = 6350000.

from salome.hydrotools.changeCoords import changeCoords

origFileMeshTrans = os.path.join(tmpdir, 'garonne_2_trs.med')
changeCoords(origFileMesh, origFileMeshTrans, 2154, 2154, offsetX, offsetY, 0, 0)

from salome.hydrotools.cutMesh import cutMesh

cutFileMesh = os.path.join(tmpdir, 'garonne_2_cut.med')
meshFile = cutMesh(origFileMeshTrans,
                   os.path.join(HYDRO_SAMPLES, 'zoneNouvelleDigue.shp'),
                   cutFileMesh, offsetX, offsetY)

ficMeshOut = os.path.join(tmpdir, "garonne_2_cut_brd.med")
fileMeshBrd = freeBordersGroup(cutFileMesh, ficMeshOut)
exploreEdgeGroups(fileMeshBrd, "", offsetX, offsetY)

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()
hydro_doc.SetLocalCS( offsetX, offsetY )

limites_original = importPolylines(hydro_doc, os.path.join(tmpdir, "garonne_2_cut_brd_FreeBorders.shp"), False, 4)
[nouvelle_digue] = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "nouvelleDigue.shp"), True, 4)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))

domaine_nouveau = createImmersibleZone(hydro_doc, "domaine_nouveau", limites_original[2], Cloud_02, True, 0)

Profile_1 = hydro_doc.CreateObject( KIND_PROFILE )
Profile_1.SetName( "Profile_1" )
profile_points = [ gp_XY( -30, -10 ),
                   gp_XY( -15, 5 ),
                   gp_XY( 0, 6 ),
                   gp_XY( 15, 5 ),
                   gp_XY( 30, -10 ) ];
Profile_1.SetParametricPoints( profile_points )
Profile_1.Update()

Polyline3D_1_Profile_1 = hydro_doc.CreateObject( KIND_PROFILE )
Polyline3D_1_Profile_1.SetName( "Polyline3D_1_Profile_1" )
profile_points = [ gp_XY( 0, 19.63 ),
                   gp_XY( 495.678, 20.18 ),
                   gp_XY( 1116.92, 20.39 ),
                   gp_XY( 1635.79, 19.66 ),
                   gp_XY( 2040.12, 20.1 ),
                   gp_XY( 2719.6, 20.76 ),
                   gp_XY( 3184.38, 21.1 ),
                   gp_XY( 4149.72, 21.47 ),
                   gp_XY( 4546.55, 26.79 ) ];
Polyline3D_1_Profile_1.SetParametricPoints( profile_points )
Polyline3D_1_Profile_1.Update()

Polyline3D_1 = hydro_doc.CreateObject( KIND_POLYLINE )
Polyline3D_1.SetName( "Polyline3D_1" )
Polyline3D_1.SetPolylineXY( nouvelle_digue )
Polyline3D_1.SetChildProfileUZ( Polyline3D_1_Profile_1.GetProfileUZ() )
Polyline3D_1.SetAltitudeObject( Cloud_02 )
Polyline3D_1.Update()

Digue_1 = hydro_doc.CreateObject( KIND_DIGUE )
Digue_1.SetName( "Digue_1" )
Digue_1.SetZLevel( 5 )
Digue_1.SetGuideLine( Polyline3D_1 )
Digue_1.SetProfileMode( True )
Digue_1.SetProfile( Profile_1 )
Digue_1.SetEquiDistance( 30 )
Digue_1.Update()

# Calculation case
domaineNouveau = hydro_doc.CreateObject( KIND_CALCULATION )
domaineNouveau.SetName( "domaineNouveau" )

domaineNouveau.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
domaineNouveau.AddGeometryObject( Digue_1 )
domaineNouveau.AddGeometryObject( domaine_nouveau )

case_geom_group = Digue_1.GetGroup( 2 )
domaineNouveau.AddGeometryGroup( case_geom_group )
case_geom_group = Digue_1.GetGroup( 0 )
domaineNouveau.AddGeometryGroup( case_geom_group )
case_geom_group = Digue_1.GetGroup( 3 )
domaineNouveau.AddGeometryGroup( case_geom_group )
case_geom_group = Digue_1.GetGroup( 1 )
domaineNouveau.AddGeometryGroup( case_geom_group )
case_geom_group = domaine_nouveau.GetGroup( 0 )
domaineNouveau.AddGeometryGroup( case_geom_group )

domaineNouveau.SetBoundaryPolyline( limites_original[2] )

# Start the algorithm of the partition and assignment
domaineNouveau.Update()
domaineNouveau_Reg_1 = hydro_doc.FindObjectByName( "domaineNouveau_Reg_1" )
domaineNouveau_Zone_1 = hydro_doc.FindObjectByName( "domaineNouveau_Zone_1" )
domaineNouveau_Zone_1.SetMergeType( HYDROData_Zone.Merge_ZMAX )
domaineNouveau_Zone_1.SetColor( QColor( 28, 168, 228 ))
domaineNouveau_Reg_1.AddZone( domaineNouveau_Zone_1 )
domaineNouveau_Reg_2 = hydro_doc.FindObjectByName( "domaineNouveau_Reg_2" )
domaineNouveau_Zone_2 = hydro_doc.FindObjectByName( "domaineNouveau_Zone_2" )
domaineNouveau_Zone_2.SetColor( QColor( 28, 51, 228 ))
domaineNouveau_Reg_2.AddZone( domaineNouveau_Zone_2 )
domaineNouveau_Reg_1.SetName( "domaineNouveau_Reg_1" )
domaineNouveau_Reg_2.SetName( "domaineNouveau_Reg_2" )

# Export of the calculation case
domaineNouveau_entry = domaineNouveau.Export()

##----------------------
## --- Geometry
##----------------------

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

from salome.hydrotools.hydroGeoMeshUtils import getChildrenInStudy

geompy = geomBuilder.New()

print ("Entry:", domaineNouveau_entry)
HYDRO_domaineNouveau = salome.IDToObject( str( domaineNouveau_entry ) )
print ("Geom shape:", HYDRO_domaineNouveau)
print ("Geom shape name:", HYDRO_domaineNouveau.GetName())

#[reg_new] = geompy.SubShapeAll(HYDRO_domaineNouveau, geompy.ShapeType["FACE"])
children = getChildrenInStudy(HYDRO_domaineNouveau)
sub_digue = children['domaineNouveau_Reg_1']
sub_Reg_2 = children['domaineNouveau_Reg_2']
sub_Digue_1_Left_Bank = children['domaineNouveau_Digue_1_Left_Bank']
sub_Digue_1_Outlet = children['domaineNouveau_Digue_1_Outlet']
sub_Digue_1_Right_Bank = children['domaineNouveau_Digue_1_Right_Bank']
sub_domaine_nouveau_Outer = children['domaineNouveau_domaine_nouveau_Outer']
sub_Digue_1_Inlet = children['domaineNouveau_Digue_1_Inlet']

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
#--- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats

smesh = smeshBuilder.New()

#--- algorithms and hypothesis
domaineRemaille = smesh.Mesh(HYDRO_domaineNouveau)

NETGEN_2D = domaineRemaille.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 5 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

Regular_1D = domaineRemaille.Segment(geom=sub_Digue_1_Outlet)
Number_of_Segments_1 = Regular_1D.NumberOfSegments(4)
Regular_1D_1 = domaineRemaille.Segment(geom=sub_Digue_1_Inlet)
status = domaineRemaille.AddHypothesis(Number_of_Segments_1,sub_Digue_1_Inlet)

Regular_1D_2 = domaineRemaille.Segment(geom=sub_Digue_1_Left_Bank)
Local_Length_1 = Regular_1D_2.LocalLength(50,None,1e-07)
Regular_1D_3 = domaineRemaille.Segment(geom=sub_Digue_1_Right_Bank)
status = domaineRemaille.AddHypothesis(Local_Length_1,sub_Digue_1_Right_Bank)
# Regular_1D_4 = domaineRemaille.Segment(geom=sub_digue)
# status = domaineRemaille.AddHypothesis(Local_Length_1,sub_digue)
QuadFromMedialAxis_1D2D = domaineRemaille.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=sub_digue)

([origMeshBrd], status) = smesh.CreateMeshesFromMED(fileMeshBrd)
FreeBorders = origMeshBrd.GetGroupByName("FreeBorders")
Import_1D = domaineRemaille.UseExisting1DElements(geom=sub_domaine_nouveau_Outer)
Source_Edges_1 = Import_1D.SourceEdges( FreeBorders ,1,1)

isDone = domaineRemaille.Compute()
isDone = domaineRemaille.SplitQuadObject( domaineRemaille, 1 )

controlMeshStats(domaineRemaille, 5179, 769, 10144)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

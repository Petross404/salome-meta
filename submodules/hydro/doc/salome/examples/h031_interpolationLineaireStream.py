# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")

import sys
import salome
salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.controls import controlValues
 
hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0.000, 0.000 )

axe = hydro_doc.CreateObject( KIND_POLYLINEXY )
axe.SetName( "axe" )
axe.SetZLevel( 2 )
axe.SetSectionColor( 0, QColor( 228, 105, 28, 255 ) )
axe.AddSection( "Section_1", 1, 0 )
axe.AddPoint( 0, gp_XY( 9.00, 20.00 ) )
axe.AddPoint( 0, gp_XY( 31.00, 20.00 ) )
axe.AddPoint( 0, gp_XY( 48.84, 26.13 ) )
axe.AddPoint( 0, gp_XY( 64.61, 34.64 ) )
axe.AddPoint( 0, gp_XY( 84.63, 58.56 ) )
axe.AddPoint( 0, gp_XY( 97.38, 72.55 ) )
axe.Update()

riveGauche = hydro_doc.CreateObject( KIND_POLYLINEXY )
riveGauche.SetName( "riveGauche" )
riveGauche.SetZLevel( 0 )
riveGauche.SetSectionColor( 0, QColor( 28, 228, 68, 255 ) )
riveGauche.AddSection( "Section_1", 1, 0 )
riveGauche.AddPoint( 0, gp_XY( 8.01, 26.89 ) )
riveGauche.AddPoint( 0, gp_XY( 31.62, 28.04 ) )
riveGauche.AddPoint( 0, gp_XY( 55.02, 36.84 ) )
riveGauche.AddPoint( 0, gp_XY( 85.29, 74.85 ) )
riveGauche.AddPoint( 0, gp_XY( 86.64, 76.59 ) )
riveGauche.Update()

riveDroite = hydro_doc.CreateObject( KIND_POLYLINEXY )
riveDroite.SetName( "riveDroite" )
riveDroite.SetZLevel( 1 )
riveDroite.SetSectionColor( 0, QColor( 228, 218, 28, 255 ) )
riveDroite.AddSection( "Section_1", 1, 0 )
riveDroite.AddPoint( 0, gp_XY( 8.75, 12.88 ) )
riveDroite.AddPoint( 0, gp_XY( 10.04, 13.03 ) )
riveDroite.AddPoint( 0, gp_XY( 36.87, 13.54 ) )
riveDroite.AddPoint( 0, gp_XY( 60.01, 25.02 ) )
riveDroite.AddPoint( 0, gp_XY( 99.97, 60.03 ) )
riveDroite.AddPoint( 0, gp_XY( 100.35, 60.37 ) )
riveDroite.Update()

Profile_1 = hydro_doc.CreateObject( KIND_PROFILE )
Profile_1.SetName( "Profile_1" )
profile_points = [ gp_XYZ( 10, 13, 10 ),
                   gp_XYZ( 10, 14, 6 ),
                   gp_XYZ( 10, 16, 6 ),
                   gp_XYZ( 10, 17, 5 ),
                   gp_XYZ( 10, 19, 5 ),
                   gp_XYZ( 10, 20, 8 ),
                   gp_XYZ( 10, 22, 8 ),
                   gp_XYZ( 10, 23, 7 ),
                   gp_XYZ( 10, 25, 7 ),
                   gp_XYZ( 10, 27, 11 ) ];
Profile_1.SetProfilePoints( profile_points )
Profile_1.Update()

Profile_2 = hydro_doc.CreateObject( KIND_PROFILE )
Profile_2.SetName( "Profile_2" )
profile_points = [ gp_XYZ( 60, 25, 11 ),
                   gp_XYZ( 59.2857, 26.7143, 9 ),
                   gp_XYZ( 58.5714, 28.4286, 9 ),
                   gp_XYZ( 58.2143, 29.2857, 10 ),
                   gp_XYZ( 57.8571, 30.1429, 10 ),
                   gp_XYZ( 57.5, 31, 6 ),
                   gp_XYZ( 57.1429, 31.8571, 6 ),
                   gp_XYZ( 56.4286, 33.5714, 8 ),
                   gp_XYZ( 55.3571, 36.1429, 8 ),
                   gp_XYZ( 55, 37, 9 ) ];
Profile_2.SetProfilePoints( profile_points )
Profile_2.Update()

Profile_3 = hydro_doc.CreateObject( KIND_PROFILE )
Profile_3.SetName( "Profile_3" )
profile_points = [ gp_XYZ( 100, 60, 11 ),
                   gp_XYZ( 97.75, 62.25, 7 ),
                   gp_XYZ( 96.25, 63.75, 6 ),
                   gp_XYZ( 94, 66, 7 ),
                   gp_XYZ( 91, 69, 6 ),
                   gp_XYZ( 90.25, 69.75, 6 ),
                   gp_XYZ( 88.75, 71.25, 5 ),
                   gp_XYZ( 85, 75, 10 ) ];
Profile_3.SetProfilePoints( profile_points )
Profile_3.Update()

Stream_1 = hydro_doc.CreateObject( KIND_STREAM )
Stream_1.SetName( "Stream_1" )
Stream_1.SetZLevel( 3 )
Stream_1.SetInterpolationMethod( 1 )
Stream_1.SetHydraulicAxis( axe )
Stream_1.AddProfile( Profile_1 )
Stream_1.AddProfile( Profile_2 )
Stream_1.AddProfile( Profile_3 )
Stream_1.SetLeftBank( riveGauche )
Stream_1.SetRightBank( riveDroite )
Stream_1.SetHaxStep( 1.000 )
Stream_1.SetNbProfilePoints( 100 )
Stream_1.Update()

bathy = hydro_doc.FindObjectByName("Stream_1_Altitude_1")
vecX=[40., 44., 48.]
vecY=[35., 43., 51.]
alti = bathy.GetAltitudesForPoints(vecX, vecY)
refs = [9.67, 9.35, 9.09]
controlValues(alti, refs, 0.05)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")

import sys
import salome

salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 430000, 6.35e+06 )

Cloud_inexistant = hydro_doc.CreateObject( KIND_BATHYMETRY )
Cloud_inexistant.SetName( "Cloud_inexistant" )

Cloud_inexistant.SetAltitudesInverted( 0 );
ret = Cloud_inexistant.ImportFromFile( os.path.join(HYDRO_SAMPLES,  "Cloud_inexistant.xyz" ))

if ret:
  raise ValueError('loading problem not detected: return value should be null')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

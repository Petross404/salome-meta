# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")

import sys
import salome

salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0, 0 )

garonne_ign_inexistant = hydro_doc.CreateObject( KIND_IMAGE )
garonne_ign_inexistant.SetName( "garonne_ign_inexistant" )

ret = garonne_ign_inexistant.LoadImage( os.path.join(HYDRO_SAMPLES , "garonne_ign_inexistant.png" ))

if ret:
  raise ValueError('loading problem not detected: return value should be null')

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

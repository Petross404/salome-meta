# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")

import sys
import salome

salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 430000, 6.35e+06 )

Cloud_02 = hydro_doc.CreateObject( KIND_BATHYMETRY )
Cloud_02.SetName( "Cloud_02" )

Cloud_02.SetAltitudesInverted( 0 );
if not(Cloud_02.ImportFromFile( os.path.join(HYDRO_SAMPLES,  "Cloud_02.xyz" ))):
  raise ValueError('problem while loading bathymetry')

Cloud_02.Update()

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

# Copyright (C) 2015  CEA/DEN, EDF R&D
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

SET(SALOME_TEST_DRIVER "$ENV{ABSOLUTE_APPLI_PATH}/bin/salome/appliskel/salome_test_driver.py")
SET(COMPONENT_NAME HYDRO)
SET(TIMEOUT        300)

SET(EXAMPLES_TESTS
  h001_importImage
  h002_importImage
  h003_changeLCS
  h004_importBathyXYZ
  h005_importImageBad
  h006_importBathyXYZBad
  h007_createPolySplineClosed
  h008_simpleCase
  h009_normalCaseManual
  h010_normalCaseManualMesh
  h011_normalCaseManualInterpolZ
  h012_caseDigueManual
  h013_caseDigueManualMesh
  h014_caseDigueManualInterpolZ
  h015_normalCaseManualTelemac
  h016_pilesPontManualMesh
  h017_interpolationLineaire
  h018_streamInterpolation
  h019_normalCaseManualInterpolZStrickler
  h020_normalCaseChangeBathy
  h021_meshChangeBathy
  g022_extensionSimpleComplete
  h022_extensionSimpleComplete
  h023_extensionSimpleAmont
  h024_extensionSimpleAval
  h025_extensionSimpleRiveDroite
  h026_extensionSimpleRiveGauche
  g027_domaineAmont
  h027_regroupMeshes
  g028_domaineAmontChevauchant
  h028_regroupOverlappingMeshes
  h029_regroupMeshesNonEnglobant
  h030_editNonHydroMesh
  h031_interpolationLineaireStream
  h032_completeCaseNoRegions
  h033_completeCaseModifiedRegions
  h034_splitShapes
  )

FOREACH(tfile ${EXAMPLES_TESTS})
  SET(TEST_NAME HYDRO_${tfile})
  ADD_TEST(${TEST_NAME} python ${SALOME_TEST_DRIVER} ${TIMEOUT} ${tfile}.py)
  SET_TESTS_PROPERTIES(${TEST_NAME} PROPERTIES LABELS "${COMPONENT_NAME}")
ENDFOREACH()

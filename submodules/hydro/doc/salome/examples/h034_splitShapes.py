# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

# ----------------------------------------------------------------------------------
# --- domain extension

import sys
import salome

salome.salome_init()

offsetX = 430000.
offsetY = 6350000.

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, splitShapesAll, splitShapeTool

hydro_doc = HYDROData_Document.Document()
hydro_doc.SetLocalCS( offsetX, offsetY )

contours = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "contour.shp"), False, 0)
lmajeurs = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "lmajeur.shp"), True, 0)
lm2s = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "lm2.shp"), True, 0)
openPolys = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "openPoly.shp"), False, 0)
                           
seq = contours + lmajeurs
shapeList = splitShapesAll(hydro_doc, seq)
if len(shapeList) != 8:
    raise ValueError("Bad nunber of shapes after splitShapesAll, found %d instead of 8"%len(shapeList))

seq2 = contours + lm2s
shapeList2 = splitShapesAll(hydro_doc, seq2)
if len(shapeList2) != 16:
    raise ValueError("Bad nunber of shapes after splitShapesAll, found %d instead of 16"%len(shapeList2))

seq3 = contours + lm2s + lmajeurs + openPolys
shapeList3 = splitShapesAll(hydro_doc, seq3)
if len(shapeList3) != 89:
    raise ValueError("Bad nunber of shapes after splitShapesAll, found %d instead of 89"%len(shapeList3))

shapeList4 = splitShapeTool(hydro_doc, lmajeurs[0], contours[0])
if len(shapeList4) != 4:
    raise ValueError("Bad nunber of shapes after splitShapeTool, found %d instead of 4"%len(shapeList4))

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

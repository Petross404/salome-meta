# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")
if not os.path.isdir(HYDRO_TEST_RESOURCES):
    os.mkdir(HYDRO_TEST_RESOURCES)

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

import sys
import salome

salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import loadImage, GeolocaliseImageCoords, GeolocaliseImageReference
from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy
from salome.hydrotools.hydroGeoMeshUtils import createAxis3DDEmbankmentAltiProfile, createAxis3DDEmbankmentBathy
from salome.hydrotools.hydroGeoMeshUtils import createEmbankmentSectionA, createEmbankmentSectionB

hydro_doc = HYDROData_Document.Document()

offsetX = 430000.
offsetY = 6350000.
hydro_doc.SetLocalCS( offsetX, offsetY )

garonne_ign_01 = loadImage(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_ign_01.png"), "garonne_ign_01", 0)
GeolocaliseImageCoords(garonne_ign_01,
                       QPoint(40, 817), QPoint(1325, 85),
                       QPointF(471562, 6.36775e+06), QPointF(489400, 6.37702e+06))

garonne_ign_02 = loadImage(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_ign_02.png"), "garonne_ign_02", 1)
GeolocaliseImageReference(garonne_ign_02, garonne_ign_01,
                          QPoint(1389, 447), QPoint(784, 481),
                          QPoint(631, 95), QPoint(26, 129))

garonnes = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne.shp"), True, 6)
limites_domaine = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "domaine.shp"), False, 7)
lits_majeur = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "lit_majeur.shp"), True, 8)
axesDigue = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "axeDigue.shp"), True, 9)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))
garonne_point_L93 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_point_L93.xyz"))

domaineEtendu = createImmersibleZone(hydro_doc, "domaineEtendu", limites_domaine[0], Cloud_02, True, 2)
litMajeur = createImmersibleZone(hydro_doc, "litMajeur", lits_majeur[0], garonne_point_L93, True, 3)
litMineur = createImmersibleZone(hydro_doc, "litMineur", garonnes[0], garonne_point_L93, True, 4)

altiDiguePts = [(1., 23.0),
                ( 5000., 25.5 )]
altiProfile, axe3D = createAxis3DDEmbankmentAltiProfile(hydro_doc, "axe3DDigue", axesDigue[0], altiDiguePts)
sectionPoints = [(0, 0), (8, -1), (10,-10)]
section, digue = createEmbankmentSectionA(hydro_doc, "digue", axe3D, sectionPoints, 50., 5)

# Calculation case
garonne_1 = hydro_doc.CreateObject( KIND_CALCULATION )
garonne_1.SetName( "garonne_1" )

garonne_1.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
garonne_1.AddGeometryObject( litMineur )
garonne_1.AddGeometryObject( domaineEtendu )
garonne_1.AddGeometryObject( litMajeur )
garonne_1.AddGeometryObject( digue );

case_geom_group = domaineEtendu.GetGroup( 0 );
garonne_1.AddGeometryGroup( case_geom_group );
case_geom_group = litMineur.GetGroup( 0 );
garonne_1.AddGeometryGroup( case_geom_group );
case_geom_group = litMajeur.GetGroup( 0 );
garonne_1.AddGeometryGroup( case_geom_group );
case_geom_group = digue.GetGroup( 2 );
garonne_1.AddGeometryGroup( case_geom_group );
case_geom_group = digue.GetGroup( 0 );
garonne_1.AddGeometryGroup( case_geom_group );
case_geom_group = digue.GetGroup( 3 );
garonne_1.AddGeometryGroup( case_geom_group );
case_geom_group = digue.GetGroup( 1 );
garonne_1.AddGeometryGroup( case_geom_group );
garonne_1.SetBoundaryPolyline( limites_domaine[0] );

# Start the algorithm of the partition and assignment
garonne_1.Update()

garonne_1_litMineur = hydro_doc.FindObjectByName( "garonne_1_Reg_1" )
garonne_1_Zone_1 = hydro_doc.FindObjectByName( "garonne_1_Zone_1" )
garonne_1_Zone_1.SetMergeType( HYDROData_Zone.Merge_ZMIN )
garonne_1_Zone_1.SetColor( QColor( 192, 137, 64 ))
garonne_1_litMineur.AddZone( garonne_1_Zone_1 )

garonne_1_riveDroite = hydro_doc.FindObjectByName( "garonne_1_Reg_2" )
garonne_1_Zone_2 = hydro_doc.FindObjectByName( "garonne_1_Zone_2" )
garonne_1_Zone_2.SetColor( QColor( 64, 192, 98 ))
garonne_1_riveDroite.AddZone( garonne_1_Zone_2 )

garonne_1_Zone_3 = hydro_doc.FindObjectByName( "garonne_1_Zone_3" )
garonne_1_Zone_3.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
garonne_1_Zone_3.SetMergeObject( Cloud_02 )
garonne_1_Zone_3.SetColor( QColor( 64, 66, 192 ))
garonne_1_riveDroite.AddZone( garonne_1_Zone_3 )

garonne_1_riveGauche = hydro_doc.FindObjectByName( "garonne_1_Reg_3" )
garonne_1_Zone_4 = hydro_doc.FindObjectByName( "garonne_1_Zone_4" )
garonne_1_Zone_4.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
garonne_1_Zone_4.SetMergeObject( Cloud_02 )
garonne_1_Zone_4.SetColor( QColor( 64, 192, 164 ))
garonne_1_riveGauche.AddZone( garonne_1_Zone_4 )

garonne_1_Zone_5 = hydro_doc.FindObjectByName( "garonne_1_Zone_5" )
garonne_1_Zone_5.SetColor( QColor( 192, 149, 64 ))
garonne_1_riveGauche.AddZone( garonne_1_Zone_5 )

garonne_1_digue = hydro_doc.FindObjectByName( "garonne_1_Reg_4" )
garonne_1_Zone_6 = hydro_doc.FindObjectByName( "garonne_1_Zone_6" )
garonne_1_Zone_6.SetMergeType( HYDROData_Zone.Merge_ZMAX )
garonne_1_Zone_6.SetColor( QColor( 64, 192, 177 ))
garonne_1_digue.AddZone( garonne_1_Zone_6 )

garonne_1_litMineur.SetName("garonne_1_litMineur")
garonne_1_riveDroite.SetName("garonne_1_riveDroite")
garonne_1_riveGauche.SetName("garonne_1_riveGauche")
garonne_1_digue.SetName("garonne_1_digue")

# Export of the calculation case
garonne_1_entry = garonne_1.Export()

#----------------------
# --- Geometry
#----------------------

# Get geometry shape and print debug information
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

geompy = geomBuilder.New()

print("Entry:", garonne_1_entry)
HYDRO_garonne_1 = salome.IDToObject( str( garonne_1_entry ) )
print("Geom shape:", HYDRO_garonne_1)
print("Geom shape name:", HYDRO_garonne_1.GetName())

children = getChildrenInStudy(HYDRO_garonne_1)
garonne_riveGauche = children["garonne_1_riveGauche"]
garonne_litMineur = children["garonne_1_litMineur"]
garonne_riveDroite = children["garonne_1_riveDroite"]
garonne_digue = children["garonne_1_digue"]

controlGeomProps(geompy, garonne_riveGauche,  39490.835288, 35845737.590926)
controlGeomProps(geompy, garonne_digue,       10341.466108,   103090.760662)
controlGeomProps(geompy, garonne_litMineur,   30337.548492,  3488480.304388)
controlGeomProps(geompy, garonne_riveDroite,  32012.343241, 25998769.23615)

# --- manual identification of all useful edge groups (boundary conditions)

allEdgesIds = geompy.SubShapeAllIDs(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
print("allEdgesIds", allEdgesIds)

(isDone, ClosedFreeBoundary, OpenFreeBoundary) = geompy.GetFreeBoundary(HYDRO_garonne_1)
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[0], "ClosedFreeBoundary")

freeBoundary = geompy.ExtractShapes(ClosedFreeBoundary[0], geompy.ShapeType["EDGE"], True)
freeBoundaryIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, freeBoundary[i]) for i in range(len(freeBoundary)) ]
print("freeBoundaryIds", freeBoundaryIds)

[litMineur_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_litMineur], geompy.ShapeType["EDGE"], True)
[litMineur_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_litMineur], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_garonne_1, litMineur_droite, "litMineur_droite")
geompy.addToStudyInFather(HYDRO_garonne_1, litMineur_gauche, "litMineur_gauche")
rives = [litMineur_droite, litMineur_gauche]
rivesIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, rives[i]) for i in range(len(rives)) ]
print("rivesIds", rivesIds)

edges_litMineur = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_litMineur], geompy.ShapeType["EDGE"], True)
edges_riveGauche = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_riveGauche], geompy.ShapeType["EDGE"], True)
edges_riveDroite = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_riveDroite], geompy.ShapeType["EDGE"], True)
edges_litMineurIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_litMineur[i]) for i in range(len(edges_litMineur)) ]
edges_riveGaucheIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_riveGauche[i]) for i in range(len(edges_riveGauche)) ]
edges_riveDroiteIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_riveDroite[i]) for i in range(len(edges_riveDroite)) ]

print("edges_litMineurIds", edges_litMineurIds) 
print("edges_riveGaucheIds", edges_riveGaucheIds)
print("edges_riveDroiteIds", edges_riveDroiteIds)

sectionsIds = [Id for Id in edges_litMineurIds if Id not in rivesIds]
print("sectionsIds", sectionsIds)
SectionsGaronne = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(SectionsGaronne, sectionsIds)
geompy.addToStudyInFather(HYDRO_garonne_1, SectionsGaronne, "SectionsGaronne")

bordGaucheDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveGaucheIds]
bordDroiteDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveDroiteIds]
print("bordGaucheDomaineIds", bordGaucheDomaineIds)
print("bordDroiteDomaineIds", bordDroiteDomaineIds)
bordGaucheDomaine = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordGaucheDomaine, bordGaucheDomaineIds)
geompy.addToStudyInFather(HYDRO_garonne_1, bordGaucheDomaine, "bordGaucheDomaine")
bordDroiteDomaine = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordDroiteDomaine, bordDroiteDomaineIds)
geompy.addToStudyInFather(HYDRO_garonne_1, bordDroiteDomaine, "bordDroiteDomaine")

amont = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(46757.861314, 25833.234752, 0))
aval = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(39078.979127, 32588.627279, 0))
geompy.addToStudyInFather(HYDRO_garonne_1, amont, "amont")
geompy.addToStudyInFather(HYDRO_garonne_1, aval, "aval")

amontDigue = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(44736.43, 29847.52, 0))
avalDigue = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(40961.39, 31800.95, 0))
geompy.addToStudyInFather(HYDRO_garonne_1, amontDigue, "amontDigue")
geompy.addToStudyInFather(HYDRO_garonne_1, avalDigue, "avalDigue")

SectionsDigue = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(SectionsDigue, [geompy.GetSubShapeID(HYDRO_garonne_1, amontDigue), geompy.GetSubShapeID(HYDRO_garonne_1, avalDigue)])
geompy.addToStudyInFather( HYDRO_garonne_1, SectionsDigue, 'SectionsDigue' )


#----------------------
# --- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats
import tempfile

smesh = smeshBuilder.New()

# --- algorithms and hypothesis
garonne_1 = smesh.Mesh(HYDRO_garonne_1)

NETGEN_2D = garonne_1.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 50 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

algo2D_litMineur = garonne_1.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=garonne_litMineur)
algo1D_litMineur = garonne_1.Segment(geom=garonne_litMineur)
hypo1D_litMineur = algo1D_litMineur.LocalLength(100,None,1e-07)
subMesh_litMineur = algo1D_litMineur.GetSubMesh()
smesh.SetName(subMesh_litMineur, "litMineur")

algo1D_SectionsGaronne = garonne_1.Segment(geom=SectionsGaronne)
hypo1D_SectionsGaronne = algo1D_SectionsGaronne.NumberOfSegments(8)
hypo1D_SectionsGaronne.SetDistrType( 0 )
subMesh_SectionsGaronne = algo1D_SectionsGaronne.GetSubMesh()
smesh.SetName(subMesh_SectionsGaronne, "SectionsGaronne")

algo2D_digue = garonne_1.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=garonne_digue)
algo1D_digue = garonne_1.Segment(geom=garonne_digue)
hypo1D_digue = algo1D_digue.LocalLength(50,None,1e-07)
subMesh_digue = algo1D_digue.GetSubMesh()
smesh.SetName(subMesh_digue, "digue")

algo1D_SectionsDigue = garonne_1.Segment(geom=SectionsDigue)
hypo1D_SectionsDigue = algo1D_SectionsDigue.NumberOfSegments(4)
hypo1D_SectionsDigue.SetDistrType( 0 )
subMesh_SectionsDigue = algo1D_SectionsDigue.GetSubMesh()
smesh.SetName(subMesh_SectionsDigue, "SectionsDigue")

isDone = garonne_1.SetMeshOrder( [ [ subMesh_SectionsGaronne, subMesh_litMineur ], [ subMesh_SectionsDigue, subMesh_digue ]])

# --- compute mesh
isDone = garonne_1.Compute()
isDone = garonne_1.SplitQuadObject( garonne_1, 1 )
isDone = garonne_1.ReorientObject( garonne_1 )

# --- geometrical groups of faces
riveGauche_1 = garonne_1.GroupOnGeom(garonne_riveGauche,'riveGauche',SMESH.FACE)
litMineur_1 = garonne_1.GroupOnGeom(garonne_litMineur,'litMineur',SMESH.FACE)
riveDroite_1 = garonne_1.GroupOnGeom(garonne_riveDroite,'riveDroite',SMESH.FACE)
digue_1 = garonne_1.GroupOnGeom(garonne_digue,'digue',SMESH.FACE)

# --- geometrical groups of edges
ClosedFreeBoundary_1 = garonne_1.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.EDGE)
litMineur_droite_1 = garonne_1.GroupOnGeom(litMineur_droite,'litMineur_droite',SMESH.EDGE)
litMineur_gauche_1 = garonne_1.GroupOnGeom(litMineur_gauche,'litMineur_gauche',SMESH.EDGE)
SectionsGaronne_1 = garonne_1.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.EDGE)
bordGaucheDomaine_1 = garonne_1.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.EDGE)
bordDroiteDomaine_1 = garonne_1.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.EDGE)
amont_1 = garonne_1.GroupOnGeom(amont,'amont',SMESH.EDGE)
aval_1 = garonne_1.GroupOnGeom(aval,'aval',SMESH.EDGE)
SectionsDigue_1 = garonne_1.GroupOnGeom(SectionsDigue,'SectionsDigue',SMESH.EDGE)

# --- geometrical groups of nodes
garonne_1_litMineur_2 = garonne_1.GroupOnGeom(garonne_litMineur,'garonne_1_litMineur',SMESH.NODE)
garonne_1_riveDroite_2 = garonne_1.GroupOnGeom(garonne_riveDroite,'garonne_1_riveDroite',SMESH.NODE)
garonne_1_riveGauche_2 = garonne_1.GroupOnGeom(garonne_riveGauche,'garonne_1_riveGauche',SMESH.NODE)
garonne_1_digue_2 = garonne_1.GroupOnGeom(garonne_digue,'garonne_1_digue',SMESH.FACE)
ClosedFreeBoundary_2 = garonne_1.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.NODE)
litMineur_droite_2 = garonne_1.GroupOnGeom(litMineur_droite,'litMineur_droite',SMESH.NODE)
litMineur_gauche_2 = garonne_1.GroupOnGeom(litMineur_gauche,'litMineur_gauche',SMESH.NODE)
SectionsGaronne_2 = garonne_1.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.NODE)
bordGaucheDomaine_2 = garonne_1.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.NODE)
bordDroiteDomaine_2 = garonne_1.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.NODE)
amont_2 = garonne_1.GroupOnGeom(amont,'amont',SMESH.NODE)
aval_2 = garonne_1.GroupOnGeom(aval,'aval',SMESH.NODE)
SectionsDigue_2 = garonne_1.GroupOnGeom(SectionsDigue,'SectionsDigue',SMESH.NODE)

garonne_1.SetAutoColor( 1 )
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)
fichierMaillage = os.path.join(tmpdir, 'garonne_1.med')
garonne_1.ExportMED(fichierMaillage, 0, SMESH.MED_V2_2, 1, None ,1)

mesures = garonne_1.GetMeshInfo()
d= {}
for key, value in mesures.items():
  d[str(key)] = value
nbTriangles = d['Entity_Triangle']
nbNodes = d['Entity_Node']

controlMeshStats(garonne_1, 6190, 691, 12201)
controlSubMeshStats(litMineur_1, 2384)
controlSubMeshStats(riveDroite_1, 2400)
controlSubMeshStats(riveGauche_1, 6585)
controlSubMeshStats(digue_1, 832)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

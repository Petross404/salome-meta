# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

# --- generated resources

origFileMesh = os.path.join(HYDRO_TEST_RESOURCES, 'garonne_2.med')

# ----------------------------------------------------------------------------------
# --- domain extension

import sys
import salome

salome.salome_init()

from salome.hydrotools.shapesGroups import freeBordersGroup, exploreEdgeGroups, fitShapePointsToMesh

offsetX = 430000.
offsetY = 6350000.

ficMeshOut = os.path.join(tmpdir, "garonne_2_brd.med")
fileMeshBrd = freeBordersGroup(origFileMesh, ficMeshOut)
exploreEdgeGroups(fileMeshBrd, "", offsetX, offsetY)

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone

hydro_doc = HYDROData_Document.Document()
hydro_doc.SetLocalCS( offsetX, offsetY )

limites_original = importPolylines(hydro_doc, os.path.join(tmpdir, "garonne_2_brd_FreeBorders.shp"), True, 4)
limites_domaine  = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "extension_1_1.shp"), False, 2)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))

domaine_original  = createImmersibleZone(hydro_doc, "domaine_original", limites_original[0], Cloud_02, False, 1)
domaine_englobant = createImmersibleZone(hydro_doc, "domaine_englobant", limites_domaine[0], Cloud_02, True, 0)

# Calculation case
extension = hydro_doc.CreateObject( KIND_CALCULATION )
extension.SetName( "extension" )

extension.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
extension.AddGeometryObject( domaine_englobant )
extension.AddGeometryObject( domaine_original )

case_geom_group = domaine_englobant.GetGroup( 0 )
extension.AddGeometryGroup( case_geom_group )
case_geom_group = domaine_original.GetGroup( 0 )
extension.AddGeometryGroup( case_geom_group )

extension.SetBoundaryPolyline( limites_domaine[0] )

# Start the algorithm of the partition and assignment
extension.Update()
reg_extension = hydro_doc.FindObjectByName( "extension_Reg_1" )
extension_Zone_1 = hydro_doc.FindObjectByName( "extension_Zone_1" )
extension_Zone_1.SetColor( QColor( 28, 168, 228 ))
reg_extension.AddZone( extension_Zone_1 )
reg_original = hydro_doc.FindObjectByName( "extension_Reg_2" )
extension_Zone_2 = hydro_doc.FindObjectByName( "extension_Zone_2" )
extension_Zone_2.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
extension_Zone_2.SetMergeObject( Cloud_02 )
extension_Zone_2.SetColor( QColor( 28, 51, 228 ))
reg_original.AddZone( extension_Zone_2 )
reg_extension.SetName( "reg_extension" )
reg_original.SetName( "reg_original" )

# Export of the calculation case
extension_entry = extension.Export()

##----------------------
## --- Geometry
##----------------------

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

from salome.hydrotools.hydroGeoMeshUtils import getChildrenInStudy

geompy = geomBuilder.New()

print ("Entry:", extension_entry)
HYDRO_extension = salome.IDToObject( str( extension_entry ) )
print ("Geom shape:", HYDRO_extension)
print ("Geom shape name:", HYDRO_extension.GetName())

[reg_extension] = geompy.SubShapeAll(HYDRO_extension, geompy.ShapeType["FACE"])
children = getChildrenInStudy(HYDRO_extension)
extension_domaine_original_Outer = children["extension_domaine_original_Outer"]

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
#--- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats

smesh = smeshBuilder.New()

#--- algorithms and hypothesis
extensionEnglobante = smesh.Mesh(HYDRO_extension)

NETGEN_2D = extensionEnglobante.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 5 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

([origMeshBrd], status) = smesh.CreateMeshesFromMED(fileMeshBrd)
FreeBorders = origMeshBrd.GetGroupByName("FreeBorders")

Import_1D = extensionEnglobante.UseExisting1DElements(geom=extension_domaine_original_Outer)
Source_Edges_1 = Import_1D.SourceEdges( FreeBorders ,1,1)

isDone = extensionEnglobante.Compute()

controlMeshStats(extensionEnglobante, 8107, 805, 15885)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

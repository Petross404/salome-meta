# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

# --- generated resources

origFileMesh = os.path.join(HYDRO_TEST_RESOURCES, 'garonne_2.med')
origFileMesh2 = os.path.join(HYDRO_TEST_RESOURCES, 'garonneAmontChevauchant.med')

# ----------------------------------------------------------------------------------
# --- domain extension

import sys
import salome

salome.salome_init()

from salome.hydrotools.shapesGroups import freeBordersGroup, exploreEdgeGroups, fitShapePointsToMesh

offsetX = 430000.
offsetY = 6350000.

ficMeshOut = os.path.join(tmpdir, "garonne_2_brd.med")
fileMeshBrd = freeBordersGroup(origFileMesh, ficMeshOut)
exploreEdgeGroups(fileMeshBrd, "", offsetX, offsetY)

from salome.hydrotools.changeCoords import changeCoords

origFileMesh2Trans = os.path.join(tmpdir, 'garonneAmontChevauchant_trs.med')
changeCoords(origFileMesh2, origFileMesh2Trans, 2154, 2154, offsetX, offsetY, 0, 0)

from salome.hydrotools.cutMesh import cutMesh

cutFileMesh2 = os.path.join(tmpdir, 'garonneAmontChevauchant_cut.med')
meshFile = cutMesh(origFileMesh2Trans,
                   os.path.join(HYDRO_SAMPLES, 'coupeDomaineAmontChevauchant.shp'),
                   cutFileMesh2, offsetX, offsetY)

ficMeshOut2 = os.path.join(tmpdir, "garonneAmont_brd.med")
fileMeshBrd2 = freeBordersGroup(cutFileMesh2, ficMeshOut2)
exploreEdgeGroups(fileMeshBrd2, "", offsetX, offsetY)

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()
hydro_doc.SetLocalCS( offsetX, offsetY )

limites_original = importPolylines(hydro_doc, os.path.join(tmpdir, "garonne_2_brd_FreeBorders.shp"), True, 4)
limites_amont = importPolylines(hydro_doc, os.path.join(tmpdir, "garonneAmont_brd_FreeBorders.shp"), True, 5)
limites_domaine = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "raccord_4_2.shp"), False, 3)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))

domaine_regroup = createImmersibleZone(hydro_doc, "domaine_regroup", limites_domaine[0], Cloud_02, True, 0)
domaine_original  = createImmersibleZone(hydro_doc, "domaine_original", limites_original[0], Cloud_02, False, 1)
domaine_amont  = createImmersibleZone(hydro_doc, "domaine_amont", limites_amont[0], Cloud_02, False, 2)

# Calculation case
domainRegroup = hydro_doc.CreateObject( KIND_CALCULATION )
domainRegroup.SetName( "domainRegroup" )

domainRegroup.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
domainRegroup.AddGeometryObject( domaine_amont )
domainRegroup.AddGeometryObject( domaine_original )
domainRegroup.AddGeometryObject( domaine_regroup )

case_geom_group = domaine_amont.GetGroup( 0 )
domainRegroup.AddGeometryGroup( case_geom_group )
case_geom_group = domaine_original.GetGroup( 0 )
domainRegroup.AddGeometryGroup( case_geom_group )
case_geom_group = domaine_regroup.GetGroup( 0 )
domainRegroup.AddGeometryGroup( case_geom_group )

domainRegroup.SetBoundaryPolyline( limites_domaine[0] )

# Start the algorithm of the partition and assignment
domainRegroup.Update()
domainRegroup_Reg_1 = hydro_doc.FindObjectByName( "domainRegroup_Reg_1" )
domainRegroup_Zone_1 = hydro_doc.FindObjectByName( "domainRegroup_Zone_1" )
domainRegroup_Zone_1.SetColor( QColor( 28, 51, 228 ))
domainRegroup_Reg_1.AddZone( domainRegroup_Zone_1 )
domainRegroup_Reg_2 = hydro_doc.FindObjectByName( "domainRegroup_Reg_2" )
domainRegroup_Zone_2 = hydro_doc.FindObjectByName( "domainRegroup_Zone_2" )
domainRegroup_Zone_2.SetColor( QColor( 28, 155, 228 ))
domainRegroup_Reg_2.AddZone( domainRegroup_Zone_2 )
domainRegroup_Reg_3 = hydro_doc.FindObjectByName( "domainRegroup_Reg_3" )
domainRegroup_Zone_3 = hydro_doc.FindObjectByName( "domainRegroup_Zone_3" )
domainRegroup_Zone_3.SetColor( QColor( 145, 228, 28 ))
domainRegroup_Reg_3.AddZone( domainRegroup_Zone_3 )
domainRegroup_Reg_1.SetName( "domainRegroup_Reg_1" )
domainRegroup_Reg_2.SetName( "domainRegroup_Reg_2" )
domainRegroup_Reg_3.SetName( "domainRegroup_Reg_3" )

# Export of the calculation case
domainRegroup_entry = domainRegroup.Export()

##----------------------
## --- Geometry
##----------------------

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

from salome.hydrotools.hydroGeoMeshUtils import getChildrenInStudy

geompy = geomBuilder.New()

print ("Entry:", domainRegroup_entry)
HYDRO_domainRegroup = salome.IDToObject( str( domainRegroup_entry ) )
print ("Geom shape:", HYDRO_domainRegroup)
print ("Geom shape name:", HYDRO_domainRegroup.GetName())

[reg_extension] = geompy.SubShapeAll(HYDRO_domainRegroup, geompy.ShapeType["FACE"])
children = getChildrenInStudy(HYDRO_domainRegroup)
domaine_original_Outer = children["domainRegroup_domaine_original_Outer"]
domaine_amont_Outer = children["domainRegroup_domaine_amont_Outer"]

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
#--- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats

smesh = smeshBuilder.New()

#--- algorithms and hypothesis
domaineEnglobant = smesh.Mesh(HYDRO_domainRegroup)

NETGEN_2D = domaineEnglobant.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 5 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

([origMeshBrd], status) = smesh.CreateMeshesFromMED(fileMeshBrd)
FreeBorders = origMeshBrd.GetGroupByName("FreeBorders")
Import_1D = domaineEnglobant.UseExisting1DElements(geom=domaine_original_Outer)
Source_Edges_1 = Import_1D.SourceEdges( FreeBorders ,1,1)

([origMeshBrd2], status) = smesh.CreateMeshesFromMED(fileMeshBrd2)
FreeBorders2 = origMeshBrd2.GetGroupByName("FreeBorders")
Import_1D2 = domaineEnglobant.UseExisting1DElements(geom=domaine_amont_Outer)
Source_Edges_2 = Import_1D2.SourceEdges( FreeBorders2 ,1,1)

isDone = domaineEnglobant.Compute()

controlMeshStats(domaineEnglobant, 9703, 1186, 19077)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

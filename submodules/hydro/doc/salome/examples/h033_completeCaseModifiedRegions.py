# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")
if not os.path.isdir(HYDRO_TEST_RESOURCES):
    os.mkdir(HYDRO_TEST_RESOURCES)

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

import sys
import salome

salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()

offsetX = 430000.
offsetY = 6350000.
hydro_doc.SetLocalCS( offsetX, offsetY )

garonne = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne.shp"), True, 2)
lit_majeur = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "lit_majeur.shp"), True, 3)
ile = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "ile.shp"), True, 4)
domaine = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "domaine.shp"), False, 5)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))
garonne_point_L93 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_point_L93.xyz"))

litMineur = createImmersibleZone(hydro_doc, "litMineur", garonne[0], Cloud_02, True, 6)
litMajeur = createImmersibleZone(hydro_doc, "litMajeur", lit_majeur[0], garonne_point_L93, True, 5)
ilot = createImmersibleZone(hydro_doc, "ilot", ile[0], Cloud_02, False, 9)
domaineEtendu = createImmersibleZone(hydro_doc, "domaineEtendu", domaine[0], Cloud_02, True, 9)

# Calculation case
casGaronne = hydro_doc.CreateObject( KIND_CALCULATION )
casGaronne.SetName( "casGaronne" )

casGaronne.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
casGaronne.AddGeometryObject( litMineur )
casGaronne.AddGeometryObject( domaineEtendu )
casGaronne.AddGeometryObject( litMajeur )
casGaronne.AddGeometryObject( ilot )

case_geom_group = domaineEtendu.GetGroup( 0 )
casGaronne.AddGeometryGroup( case_geom_group )
case_geom_group = litMineur.GetGroup( 0 )
casGaronne.AddGeometryGroup( case_geom_group )
case_geom_group = litMajeur.GetGroup( 0 )
casGaronne.AddGeometryGroup( case_geom_group )
casGaronne.SetBoundaryPolyline( domaine[0] )

# Start the algorithm of the partition and assignment
casGaronne.Update()
casGaronne_litMineur = hydro_doc.FindObjectByName( "casGaronne_Reg_1" )
casGaronne_Zone_1 = hydro_doc.FindObjectByName( "casGaronne_Zone_1" )
casGaronne_Zone_1.SetMergeType( HYDROData_Zone.Merge_ZMIN )
casGaronne_Zone_1.SetColor( QColor( 192, 113, 64 ))
casGaronne_litMineur.AddZone( casGaronne_Zone_1 )

casGaronne_riveDroite = hydro_doc.FindObjectByName( "casGaronne_Reg_2" )
casGaronne_Zone_2 = hydro_doc.FindObjectByName( "casGaronne_Zone_2" )
casGaronne_Zone_2.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
casGaronne_Zone_2.SetMergeObject( Cloud_02 )
casGaronne_Zone_2.SetColor( QColor( 141, 192, 64 ))
casGaronne_riveDroite.AddZone( casGaronne_Zone_2 )

casGaronne_Zone_3 = hydro_doc.FindObjectByName( "casGaronne_Zone_3" )
casGaronne_Zone_3.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
casGaronne_Zone_3.SetMergeObject( Cloud_02 )
casGaronne_Zone_3.SetColor( QColor( 64, 192, 77 ))
casGaronne_riveDroite.AddZone( casGaronne_Zone_3 )

casGaronne_riveGauche = hydro_doc.FindObjectByName( "casGaronne_Reg_3" )
casGaronne_Zone_4 = hydro_doc.FindObjectByName( "casGaronne_Zone_4" )
casGaronne_Zone_4.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
casGaronne_Zone_4.SetMergeObject( Cloud_02 )
casGaronne_Zone_4.SetColor( QColor( 64, 75, 192 ))
casGaronne_riveGauche.AddZone( casGaronne_Zone_4 )

casGaronne_Zone_5 = hydro_doc.FindObjectByName( "casGaronne_Zone_5" )
casGaronne_Zone_5.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
casGaronne_Zone_5.SetMergeObject( Cloud_02 )
casGaronne_Zone_5.SetColor( QColor( 64, 192, 77 ))
casGaronne_riveGauche.AddZone( casGaronne_Zone_5 )

casGaronne_ile = hydro_doc.FindObjectByName( "casGaronne_Reg_4" )
casGaronne_Zone_6 = hydro_doc.FindObjectByName( "casGaronne_Zone_6" )
casGaronne_Zone_6.SetMergeType( HYDROData_Zone.Merge_ZMAX )
casGaronne_Zone_6.SetColor( QColor( 228, 145, 28 ))
casGaronne_ile.AddZone( casGaronne_Zone_6 )

casGaronne_litMineur.SetName("casGaronne_litMineur")
casGaronne_riveDroite.SetName("casGaronne_riveDroite")
casGaronne_riveGauche.SetName("casGaronne_riveGauche")
casGaronne_ile.SetName( "casGaronne_ile" )

# Export of the calculation case
casGaronne_entry = casGaronne.Export()

#----------------------
# --- Geometry
#----------------------

# Get geometry shape and print debug information
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

geompy = geomBuilder.New()

print("Entry:", casGaronne_entry)
HYDRO_casGaronne = salome.IDToObject( str( casGaronne_entry ) )
print("Geom shape:", HYDRO_casGaronne)
print("Geom shape name:", HYDRO_casGaronne.GetName())

# --- manual definition: geometrical faces

children = getChildrenInStudy(HYDRO_casGaronne)
garonne_riveGauche = children["casGaronne_riveGauche"]
garonne_litMineur = children["casGaronne_litMineur"]
garonne_riveDroite = children["casGaronne_riveDroite"]

controlGeomProps(geompy, garonne_riveGauche,  33378.8047,   34595667.2876)
controlGeomProps(geompy, garonne_litMineur,   30337.548492,  3488480.304388)
controlGeomProps(geompy, garonne_riveDroite,  32012.343241, 25998769.23615)

# --- manual identification of all useful edge groups (boundary conditions)

allEdgesIds = geompy.SubShapeAllIDs(HYDRO_casGaronne, geompy.ShapeType["EDGE"])
print("allEdgesIds", allEdgesIds)

(isDone, ClosedFreeBoundary, OpenFreeBoundary) = geompy.GetFreeBoundary(HYDRO_casGaronne)
geompy.addToStudyInFather(HYDRO_casGaronne, ClosedFreeBoundary[0], "ClosedFreeBoundary")

freeBoundary = geompy.ExtractShapes(ClosedFreeBoundary[0], geompy.ShapeType["EDGE"], True)
freeBoundaryIds = [ geompy.GetSubShapeID(HYDRO_casGaronne, freeBoundary[i]) for i in range(len(freeBoundary)) ]
print("freeBoundaryIds", freeBoundaryIds)

[litMineur_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_litMineur], geompy.ShapeType["EDGE"], True)
[litMineur_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_litMineur], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_casGaronne, litMineur_droite, "litMineur_droite")
geompy.addToStudyInFather(HYDRO_casGaronne, litMineur_gauche, "litMineur_gauche")
rives = [litMineur_droite, litMineur_gauche]
rivesIds = [ geompy.GetSubShapeID(HYDRO_casGaronne, rives[i]) for i in range(len(rives)) ]
print("rivesIds", rivesIds)

edges_litMineur = geompy.GetSharedShapesMulti([HYDRO_casGaronne, garonne_litMineur], geompy.ShapeType["EDGE"], True)
edges_riveGauche = geompy.GetSharedShapesMulti([HYDRO_casGaronne, garonne_riveGauche], geompy.ShapeType["EDGE"], True)
edges_riveDroite = geompy.GetSharedShapesMulti([HYDRO_casGaronne, garonne_riveDroite], geompy.ShapeType["EDGE"], True)
edges_litMineurIds = [ geompy.GetSubShapeID(HYDRO_casGaronne, edges_litMineur[i]) for i in range(len(edges_litMineur)) ]
edges_riveGaucheIds = [ geompy.GetSubShapeID(HYDRO_casGaronne, edges_riveGauche[i]) for i in range(len(edges_riveGauche)) ]
edges_riveDroiteIds = [ geompy.GetSubShapeID(HYDRO_casGaronne, edges_riveDroite[i]) for i in range(len(edges_riveDroite)) ]

print("edges_litMineurIds", edges_litMineurIds)
print("edges_riveGaucheIds", edges_riveGaucheIds)
print("edges_riveDroiteIds", edges_riveDroiteIds)

sectionsIds = [Id for Id in edges_litMineurIds if Id not in rivesIds]
print("sectionsIds", sectionsIds)
SectionsGaronne = geompy.CreateGroup(HYDRO_casGaronne, geompy.ShapeType["EDGE"])
geompy.UnionIDs(SectionsGaronne, sectionsIds)
geompy.addToStudyInFather(HYDRO_casGaronne, SectionsGaronne, "SectionsGaronne")

bordGaucheDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveGaucheIds]
bordDroiteDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveDroiteIds]
print("bordGaucheDomaineIds", bordGaucheDomaineIds)
print("bordDroiteDomaineIds", bordDroiteDomaineIds)
bordGaucheDomaine = geompy.CreateGroup(HYDRO_casGaronne, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordGaucheDomaine, bordGaucheDomaineIds)
geompy.addToStudyInFather(HYDRO_casGaronne, bordGaucheDomaine, "bordGaucheDomaine")
bordDroiteDomaine = geompy.CreateGroup(HYDRO_casGaronne, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordDroiteDomaine, bordDroiteDomaineIds)
geompy.addToStudyInFather(HYDRO_casGaronne, bordDroiteDomaine, "bordDroiteDomaine")

amont = geompy.GetEdgeNearPoint(HYDRO_casGaronne, geompy.MakeVertex(46757.861314, 25833.234752, 0))
aval = geompy.GetEdgeNearPoint(HYDRO_casGaronne, geompy.MakeVertex(39078.979127, 32588.627279, 0))
geompy.addToStudyInFather(HYDRO_casGaronne, amont, "amont")
geompy.addToStudyInFather(HYDRO_casGaronne, aval, "aval")

#----------------------
# --- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats
import tempfile

smesh = smeshBuilder.New()

# --- algorithms and hypothesis
garonne_2 = smesh.Mesh(HYDRO_casGaronne)

NETGEN_2D = garonne_2.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 50 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

algo2D_litMineur = garonne_2.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=garonne_litMineur)
algo1D_litMineur = garonne_2.Segment(geom=garonne_litMineur)
hypo1D_litMineur = algo1D_litMineur.LocalLength(100,None,1e-07)
subMesh_litMineur = algo1D_litMineur.GetSubMesh()
smesh.SetName(subMesh_litMineur, "litMineur")

algo1D_SectionsGaronne = garonne_2.Segment(geom=SectionsGaronne)
hypo1D_SectionsGaronne = algo1D_SectionsGaronne.NumberOfSegments(8)
hypo1D_SectionsGaronne.SetDistrType( 0 )
subMesh_SectionsGaronne = algo1D_SectionsGaronne.GetSubMesh()
smesh.SetName(subMesh_SectionsGaronne, "SectionsGaronne")

isDone = garonne_2.SetMeshOrder( [ [ subMesh_SectionsGaronne, subMesh_litMineur ] ])

# --- compute mesh
isDone = garonne_2.Compute()
isDone = garonne_2.SplitQuadObject( garonne_2, 1 )
isDone = garonne_2.ReorientObject( garonne_2 )

# --- geometrical groups of faces
riveGauche_1 = garonne_2.GroupOnGeom(garonne_riveGauche,'riveGauche',SMESH.FACE)
litMineur_1 = garonne_2.GroupOnGeom(garonne_litMineur,'litMineur',SMESH.FACE)
riveDroite_1 = garonne_2.GroupOnGeom(garonne_riveDroite,'riveDroite',SMESH.FACE)

# --- geometrical groups of edges

ClosedFreeBoundary_1 = garonne_2.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.EDGE)
litMineur_droite_1 = garonne_2.GroupOnGeom(litMineur_droite,'litMineur_droite',SMESH.EDGE)
litMineur_gauche_1 = garonne_2.GroupOnGeom(litMineur_gauche,'litMineur_gauche',SMESH.EDGE)
Sectionsgaronne_2 = garonne_2.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.EDGE)
bordGaucheDomaine_1 = garonne_2.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.EDGE)
bordDroiteDomaine_1 = garonne_2.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.EDGE)
amont_1 = garonne_2.GroupOnGeom(amont,'amont',SMESH.EDGE)
aval_1 = garonne_2.GroupOnGeom(aval,'aval',SMESH.EDGE)

# --- geometrical groups of nodes

garonne_2_litMineur_2 = garonne_2.GroupOnGeom(garonne_litMineur,'garonne_2_litMineur',SMESH.NODE)
garonne_2_riveDroite_2 = garonne_2.GroupOnGeom(garonne_riveDroite,'garonne_2_riveDroite',SMESH.NODE)
garonne_2_riveGauche_2 = garonne_2.GroupOnGeom(garonne_riveGauche,'garonne_2_riveGauche',SMESH.NODE)
ClosedFreeBoundary_2 = garonne_2.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.NODE)
litMineur_droite_2 = garonne_2.GroupOnGeom(litMineur_droite,'litMineur_droite',SMESH.NODE)
litMineur_gauche_2 = garonne_2.GroupOnGeom(litMineur_gauche,'litMineur_gauche',SMESH.NODE)
SectionsGaronne_2 = garonne_2.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.NODE)
bordGaucheDomaine_2 = garonne_2.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.NODE)
bordDroiteDomaine_2 = garonne_2.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.NODE)
amont_2 = garonne_2.GroupOnGeom(amont,'amont',SMESH.NODE)
aval_2 = garonne_2.GroupOnGeom(aval,'aval',SMESH.NODE)

garonne_2.SetAutoColor( 1 )
fichierMaillage = os.path.join(HYDRO_TEST_RESOURCES, 'garonne_2.med')
garonne_2.ExportMED(fichierMaillage, 0, SMESH.MED_V2_2, 1, None ,1)

controlMeshStats(garonne_2, 3952, 512, 7690)
controlSubMeshStats(litMineur_1, 2384)
controlSubMeshStats(riveDroite_1, 2321)
controlSubMeshStats(riveGauche_1, 2985)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
# --- HYDRO complete case
#----------------------

constraint2 = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "constraint2.shp"), True, 5)
res = HYDROData_CompleteCalcCase.AddObjects(hydro_doc, casGaronne, constraint2, True)
if not res[0]:
    raise ValueError("complete calculation case unsuccessful")
if res[1]:
    raise ValueError("complete calculation case should not have created new regions")

regions = casGaronne.GetRegions()
for r in regions:
    print("region: %s" %r.GetName())
    zones = r.GetZones()
    for z in zones:
        print("  zone: %s mergeType: %s" %(z.GetName(), z.GetMergeType()))
        if z.GetMergeType() == 0:
            Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
            z.SetMergeType( HYDROData_Zone.Merge_Object )
            z.SetMergeObject( Cloud_02 )
        r.AddZone(z)
# casGaronne.Update() # Do not update! Breaks regions!

casGaronne2_entry = casGaronne.Export()

# TODO: test something to check the final result


# -*- coding: utf-8 -*-

import sys
import salome

salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0, 0 )

Polyline_1 = hydro_doc.CreateObject( KIND_POLYLINEXY )
Polyline_1.SetName( "Polyline_1" )

Polyline_1.SetZLevel( 0 )

Polyline_1.SetWireColor( QColor( 0, 0, 0, 255 ) )

Polyline_1.AddSection( "Section_1", 1, 1 )
Polyline_1.AddPoint( 0, gp_XY( -293.23, 276.54 ) )
Polyline_1.AddPoint( 0, gp_XY( -270.98, 163.29 ) )
Polyline_1.AddPoint( 0, gp_XY( -207.28, 251.26 ) )
Polyline_1.AddPoint( 0, gp_XY( -158.75, 160.26 ) )
Polyline_1.AddPoint( 0, gp_XY( -109.20, 131.95 ) )
Polyline_1.AddPoint( 0, gp_XY( -59.66, 126.90 ) )
Polyline_1.AddPoint( 0, gp_XY( -54.60, 187.56 ) )
Polyline_1.AddPoint( 0, gp_XY( -82.91, 220.93 ) )
Polyline_1.AddPoint( 0, gp_XY( 76.84, 251.27 ) )
Polyline_1.AddPoint( 0, gp_XY( -60.67, 285.65 ) )
Polyline_1.AddPoint( 0, gp_XY( -73.81, 330.13 ) )

Polyline_1.Update()



if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")
if not os.path.isdir(HYDRO_TEST_RESOURCES):
    os.mkdir(HYDRO_TEST_RESOURCES)

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

import sys
import salome

salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()

offsetX = 430000.
offsetY = 6350000.
hydro_doc.SetLocalCS( offsetX, offsetY )

limites_amont = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "domaineAmontChevauchant.shp"), False, 4)
garonnes_amont = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonneAmont.shp"), True, 3)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))
garonne_point_L93 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_point_L93.xyz"))

domaine_amont = createImmersibleZone(hydro_doc, "domaine_amont", limites_amont[0], Cloud_02, True, 0)
garonne_amont = createImmersibleZone(hydro_doc, "garonne_amont", garonnes_amont[0], garonne_point_L93, True, 0)

# Calculation case
domaineAmontChevauchant = hydro_doc.CreateObject( KIND_CALCULATION )
domaineAmontChevauchant.SetName( "domaineAmontChevauchant" )

domaineAmontChevauchant.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
domaineAmontChevauchant.AddGeometryObject( domaine_amont )
domaineAmontChevauchant.AddGeometryObject( garonne_amont )

case_geom_group = domaine_amont.GetGroup( 0 )
domaineAmontChevauchant.AddGeometryGroup( case_geom_group )
case_geom_group = garonne_amont.GetGroup( 0 )
domaineAmontChevauchant.AddGeometryGroup( case_geom_group )

domaineAmontChevauchant.SetBoundaryPolyline( limites_amont[0] )

# Start the algorithm of the partition and assignment
domaineAmontChevauchant.Update()
domaineAmontChevauchant_RiveGauche = hydro_doc.FindObjectByName( "domaineAmontChevauchant_Reg_1" )
domaineAmontChevauchant_Zone_1 = hydro_doc.FindObjectByName( "domaineAmontChevauchant_Zone_1" )
domaineAmontChevauchant_Zone_1.SetColor( QColor( 28, 205, 228 ))
domaineAmontChevauchant_RiveGauche.AddZone( domaineAmontChevauchant_Zone_1 )
domaineAmontChevauchant_LitMineur = hydro_doc.FindObjectByName( "domaineAmontChevauchant_Reg_2" )
domaineAmontChevauchant_Zone_2 = hydro_doc.FindObjectByName( "domaineAmontChevauchant_Zone_2" )
domaineAmontChevauchant_Zone_2.SetMergeType( HYDROData_Zone.Merge_ZMIN )
domaineAmontChevauchant_Zone_2.SetColor( QColor( 228, 205, 28 ))
domaineAmontChevauchant_LitMineur.AddZone( domaineAmontChevauchant_Zone_2 )
domaineAmontChevauchant_RiveDroite = hydro_doc.FindObjectByName( "domaineAmontChevauchant_Reg_3" )
domaineAmontChevauchant_Zone_3 = hydro_doc.FindObjectByName( "domaineAmontChevauchant_Zone_3" )
domaineAmontChevauchant_Zone_3.SetColor( QColor( 118, 228, 28 ))
domaineAmontChevauchant_RiveDroite.AddZone( domaineAmontChevauchant_Zone_3 )
domaineAmontChevauchant_RiveGauche.SetName( "domaineAmontChevauchant_RiveGauche" )
domaineAmontChevauchant_LitMineur.SetName( "domaineAmontChevauchant_LitMineur" )
domaineAmontChevauchant_RiveDroite.SetName( "domaineAmontChevauchant_RiveDroite" )

# Export of the calculation case
domaineAmontChevauchant_entry = domaineAmontChevauchant.Export()

#----------------------
# --- Geometry
#----------------------

# Get geometry shape and print debug information
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

geompy = geomBuilder.New()

print ("Entry:", domaineAmontChevauchant_entry)
HYDRO_domaineAmontChevauchant = salome.IDToObject( str( domaineAmontChevauchant_entry ) )
print ("Geom shape:", HYDRO_domaineAmontChevauchant)
print ("Geom shape name:", HYDRO_domaineAmontChevauchant.GetName())

# --- manual definition: geometrical faces

children = getChildrenInStudy(HYDRO_domaineAmontChevauchant)
garonne_riveGauche = children["domaineAmontChevauchant_RiveGauche"]
garonne_litMineur = children["domaineAmontChevauchant_LitMineur"]
garonne_riveDroite = children["domaineAmontChevauchant_RiveDroite"]
#[garonne_riveGauche, garonne_litMineur, garonne_riveDroite] = geompy.SubShapeAll(HYDRO_domaineAmontChevauchant, geompy.ShapeType["FACE"])

controlGeomProps(geompy, garonne_riveGauche,  24116.411482, 15853537.868346)
controlGeomProps(geompy, garonne_litMineur,   24157.055987,  2726363.217489)
controlGeomProps(geompy, garonne_riveDroite,  27118.637078, 32377028.446033)

# --- manual identification of all useful edge groups (boundary conditions)

allEdgesIds = geompy.SubShapeAllIDs(HYDRO_domaineAmontChevauchant, geompy.ShapeType["EDGE"])
print("allEdgesIds", allEdgesIds)

(isDone, ClosedFreeBoundary, OpenFreeBoundary) = geompy.GetFreeBoundary(HYDRO_domaineAmontChevauchant)
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, ClosedFreeBoundary[0], "ClosedFreeBoundary")

freeBoundary = geompy.ExtractShapes(ClosedFreeBoundary[0], geompy.ShapeType["EDGE"], True)
freeBoundaryIds = [ geompy.GetSubShapeID(HYDRO_domaineAmontChevauchant, freeBoundary[i]) for i in range(len(freeBoundary)) ]
print("freeBoundaryIds", freeBoundaryIds)

[litMineur_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_litMineur], geompy.ShapeType["EDGE"], True)
[litMineur_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_litMineur], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, litMineur_droite, "litMineur_droite")
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, litMineur_gauche, "litMineur_gauche")
rives = [litMineur_droite, litMineur_gauche]
rivesIds = [ geompy.GetSubShapeID(HYDRO_domaineAmontChevauchant, rives[i]) for i in range(len(rives)) ]
print("rivesIds", rivesIds)

edges_litMineur = geompy.GetSharedShapesMulti([HYDRO_domaineAmontChevauchant, garonne_litMineur], geompy.ShapeType["EDGE"], True)
edges_riveGauche = geompy.GetSharedShapesMulti([HYDRO_domaineAmontChevauchant, garonne_riveGauche], geompy.ShapeType["EDGE"], True)
edges_riveDroite = geompy.GetSharedShapesMulti([HYDRO_domaineAmontChevauchant, garonne_riveDroite], geompy.ShapeType["EDGE"], True)
edges_litMineurIds = [ geompy.GetSubShapeID(HYDRO_domaineAmontChevauchant, edges_litMineur[i]) for i in range(len(edges_litMineur)) ]
edges_riveGaucheIds = [ geompy.GetSubShapeID(HYDRO_domaineAmontChevauchant, edges_riveGauche[i]) for i in range(len(edges_riveGauche)) ]
edges_riveDroiteIds = [ geompy.GetSubShapeID(HYDRO_domaineAmontChevauchant, edges_riveDroite[i]) for i in range(len(edges_riveDroite)) ]

print("edges_litMineurIds", edges_litMineurIds)
print("edges_riveGaucheIds", edges_riveGaucheIds)
print("edges_riveDroiteIds", edges_riveDroiteIds)

sectionsIds = [Id for Id in edges_litMineurIds if Id not in rivesIds]
print("sectionsIds", sectionsIds)
SectionsGaronne = geompy.CreateGroup(HYDRO_domaineAmontChevauchant, geompy.ShapeType["EDGE"])
geompy.UnionIDs(SectionsGaronne, sectionsIds)
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, SectionsGaronne, "SectionsGaronne")

bordGaucheDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveGaucheIds]
bordDroiteDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveDroiteIds]
print("bordGaucheDomaineIds", bordGaucheDomaineIds)
print("bordDroiteDomaineIds", bordDroiteDomaineIds)
bordGaucheDomaine = geompy.CreateGroup(HYDRO_domaineAmontChevauchant, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordGaucheDomaine, bordGaucheDomaineIds)
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, bordGaucheDomaine, "bordGaucheDomaine")
bordDroiteDomaine = geompy.CreateGroup(HYDRO_domaineAmontChevauchant, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordDroiteDomaine, bordDroiteDomaineIds)
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, bordDroiteDomaine, "bordDroiteDomaine")

amont = geompy.GetEdgeNearPoint(HYDRO_domaineAmontChevauchant, geompy.MakeVertex(54062.101935, 20486.776236, 0))
aval = geompy.GetEdgeNearPoint(HYDRO_domaineAmontChevauchant, geompy.MakeVertex(47800.17745, 25211,491804, 0))
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, amont, "amont")
geompy.addToStudyInFather(HYDRO_domaineAmontChevauchant, aval, "aval")

#----------------------
# --- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats
import tempfile

smesh = smeshBuilder.New()

# --- algorithms and hypothesis
garonneAmont = smesh.Mesh(HYDRO_domaineAmontChevauchant)

NETGEN_2D = garonneAmont.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 50 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

algo2D_litMineur = garonneAmont.Quadrangle(algo=smeshBuilder.QUAD_MA_PROJ,geom=garonne_litMineur)
algo1D_litMineur = garonneAmont.Segment(geom=garonne_litMineur)
hypo1D_litMineur = algo1D_litMineur.LocalLength(100,None,1e-07)
subMesh_litMineur = algo1D_litMineur.GetSubMesh()
smesh.SetName(subMesh_litMineur, "litMineur")

algo1D_SectionsGaronne = garonneAmont.Segment(geom=SectionsGaronne)
hypo1D_SectionsGaronne = algo1D_SectionsGaronne.NumberOfSegments(8)
hypo1D_SectionsGaronne.SetDistrType( 0 )
subMesh_SectionsGaronne = algo1D_SectionsGaronne.GetSubMesh()
smesh.SetName(subMesh_SectionsGaronne, "SectionsGaronne")

isDone = garonneAmont.SetMeshOrder( [ [ subMesh_SectionsGaronne, subMesh_litMineur ] ])

# --- compute mesh
isDone = garonneAmont.Compute()
isDone = garonneAmont.SplitQuadObject( garonneAmont, 1 )
isDone = garonneAmont.ReorientObject( garonneAmont )

# --- geometrical groups of faces
riveGauche_1 = garonneAmont.GroupOnGeom(garonne_riveGauche,'riveGauche',SMESH.FACE)
litMineur_1 = garonneAmont.GroupOnGeom(garonne_litMineur,'litMineur',SMESH.FACE)
riveDroite_1 = garonneAmont.GroupOnGeom(garonne_riveDroite,'riveDroite',SMESH.FACE)

# --- geometrical groups of edges

ClosedFreeBoundary_1 = garonneAmont.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.EDGE)
litMineur_droite_1 = garonneAmont.GroupOnGeom(litMineur_droite,'litMineur_droite',SMESH.EDGE)
litMineur_gauche_1 = garonneAmont.GroupOnGeom(litMineur_gauche,'litMineur_gauche',SMESH.EDGE)
SectionsgaronneAmont = garonneAmont.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.EDGE)
bordGaucheDomaine_1 = garonneAmont.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.EDGE)
bordDroiteDomaine_1 = garonneAmont.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.EDGE)
amont_1 = garonneAmont.GroupOnGeom(amont,'amont',SMESH.EDGE)
aval_1 = garonneAmont.GroupOnGeom(aval,'aval',SMESH.EDGE)

# --- geometrical groups of nodes

garonneAmont_litMineur_2 = garonneAmont.GroupOnGeom(garonne_litMineur,'garonneAmont_litMineur',SMESH.NODE)
garonneAmont_riveDroite_2 = garonneAmont.GroupOnGeom(garonne_riveDroite,'garonneAmont_riveDroite',SMESH.NODE)
garonneAmont_riveGauche_2 = garonneAmont.GroupOnGeom(garonne_riveGauche,'garonneAmont_riveGauche',SMESH.NODE)
ClosedFreeBoundary_2 = garonneAmont.GroupOnGeom(ClosedFreeBoundary[0],'ClosedFreeBoundary',SMESH.NODE)
litMineur_droite_2 = garonneAmont.GroupOnGeom(litMineur_droite,'litMineur_droite',SMESH.NODE)
litMineur_gauche_2 = garonneAmont.GroupOnGeom(litMineur_gauche,'litMineur_gauche',SMESH.NODE)
SectionsgaronneAmont = garonneAmont.GroupOnGeom(SectionsGaronne,'SectionsGaronne',SMESH.NODE)
bordGaucheDomaine_2 = garonneAmont.GroupOnGeom(bordGaucheDomaine,'bordGaucheDomaine',SMESH.NODE)
bordDroiteDomaine_2 = garonneAmont.GroupOnGeom(bordDroiteDomaine,'bordDroiteDomaine',SMESH.NODE)
amont_2 = garonneAmont.GroupOnGeom(amont,'amont',SMESH.NODE)
aval_2 = garonneAmont.GroupOnGeom(aval,'aval',SMESH.NODE)

garonneAmont.SetAutoColor( 1 )
fichierMaillage = os.path.join(HYDRO_TEST_RESOURCES, 'garonneAmontChevauchant.med')
garonneAmont.ExportMED(fichierMaillage, 0, SMESH.MED_V2_2, 1, None ,1)

controlMeshStats(garonneAmont, 3056, 395, 5953)
controlSubMeshStats(litMineur_1, 1904)
controlSubMeshStats(riveDroite_1, 2518)
controlSubMeshStats(riveGauche_1, 1531)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()


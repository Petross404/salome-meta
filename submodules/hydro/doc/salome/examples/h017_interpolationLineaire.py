# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")

import sys
import salome

salome.salome_init()

###
### HYDRO component
###

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 430000.000, 6350000.000 )

contour = hydro_doc.CreateObject( KIND_POLYLINEXY )
contour.SetName( "contour" )

contour.SetZLevel( 0 )

contour.AddSection( "Section_1", 0, 1 )
contour.AddPoint( 0, gp_XY( 56217.97, 14517.47 ) )
contour.AddPoint( 0, gp_XY( 58938.24, 14526.73 ) )
contour.AddPoint( 0, gp_XY( 58947.49, 13277.63 ) )
contour.AddPoint( 0, gp_XY( 56208.72, 13286.88 ) )

contour.Update()


Cloud_02 = hydro_doc.CreateObject( KIND_BATHYMETRY )
Cloud_02.SetName( "Cloud_02" )

Cloud_02.SetAltitudesInverted( 0 )
if not(Cloud_02.ImportFromFile( os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz" ))):
  raise ValueError('problem while loading bathymetry')

Cloud_02.Update()


domaine = hydro_doc.CreateObject( KIND_IMMERSIBLE_ZONE )
domaine.SetName( "domaine" )

domaine.SetZLevel( 1 )

domaine.SetAltitudeObject( Cloud_02 )
domaine.SetPolyline( contour )

domaine.Update()


# Calculation case
etude = hydro_doc.CreateObject( KIND_CALCULATION )
etude.SetName( "etude" )

etude.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
etude.AddGeometryObject( domaine )

case_geom_group = domaine.GetGroup( 0 )
etude.AddGeometryGroup( case_geom_group )
etude.SetBoundaryPolyline( contour )

# Start the algorithm of the partition and assignment
etude.Update()
etude_Reg_1 = hydro_doc.FindObjectByName( "etude_Reg_1" )
etude_Zone_1 = hydro_doc.FindObjectByName( "etude_Zone_1" )
etude_Zone_1.SetColor( QColor( 124, 192, 64 ))
etude_Reg_1.AddZone( etude_Zone_1 )
etude_Reg_1.SetName( "etude_Reg_1" )

# Export of the calculation case
etude_entry = etude.Export()

# Get geometry shape and print debug information
import GEOM
print("Entry:", etude_entry)
HYDRO_etude_1 = salome.IDToObject( str( etude_entry ) )
print("Geom shape:", HYDRO_etude_1)
print("Geom shape name:", HYDRO_etude_1.GetName())


###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

etude_domaine_Outer = geompy.CreateGroup(HYDRO_etude_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(etude_domaine_Outer, [3, 6, 8, 10])
domaine = geompy.CreateGroup(HYDRO_etude_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(domaine, [1])
geompy.addToStudyInFather( HYDRO_etude_1, etude_domaine_Outer, 'etude_domaine_Outer' )
geompy.addToStudyInFather( HYDRO_etude_1, domaine, 'domaine' )

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
relief = smesh.Mesh(HYDRO_etude_1)
NETGEN_2D = relief.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters_1 = NETGEN_2D.Parameters()
NETGEN_2D_Parameters_1.SetMaxSize( 25 )
NETGEN_2D_Parameters_1.SetSecondOrder( 0 )
NETGEN_2D_Parameters_1.SetOptimize( 1 )
NETGEN_2D_Parameters_1.SetFineness( 3 )
NETGEN_2D_Parameters_1.SetMinSize( 10 )
NETGEN_2D_Parameters_1.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters_1.SetFuseEdges( 1 )
NETGEN_2D_Parameters_1.SetQuadAllowed( 0 )
isDone = relief.Compute()
domaine_1 = relief.GroupOnGeom(domaine,'domaine',SMESH.FACE)
domaine_2 = relief.GroupOnGeom(domaine,'domaine',SMESH.NODE)
smesh.SetName(relief, 'relief')

med_file = r'/tmp/relief.med'

try:
  os.remove(med_file)
except OSError:
  pass


try:
  relief.ExportMED( med_file, 0, SMESH.MED_V2_2, 1, None ,1)
except:
  print('ExportToMEDX() failed. Invalid file name?')


## Set names of Mesh objects
smesh.SetName(domaine_1, 'domaine')
smesh.SetName(NETGEN_2D.GetAlgorithm(), 'NETGEN_2D')
smesh.SetName(relief.GetMesh(), 'relief')
smesh.SetName(domaine_2, 'domaine')
smesh.SetName(NETGEN_2D_Parameters_1, 'NETGEN 2D Parameters_1')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
# --- Z interpolation with HYDRO
#----------------------

from salome.hydrotools.interpolZ import interpolZ
from salome.hydrotools.controls import controlStatZ

# --- nom du cas dans HYDRO
nomCas = 'etude'

# --- fichier med 2D(x,y) du cas, produit par SMESH
fichierMaillage = med_file

# --- dictionnaire: (clé = nom de groupe med, valeur= nom de région)
dicoGroupeRegion= dict(domaine  = 'etude_Reg_1',
                      )

# --- value to use for Z when the node is not in a region (used to detect problems)
zUndef = 90
# --- interpolation Method: 0 = nearest point on bathymetry (default), 1 = linear interpolation
interpolMethod = 1
# --- produce a 3D mesh (Z set to its value instead of 0
m3d = True

# --- Z interpolation on the bathymety/altimetry on the mesh nodes
statz = interpolZ(nomCas, fichierMaillage, dicoGroupeRegion, zUndef, interpolMethod, m3d)
#print statz
refstatz = {'domaine': (27.10, 168.28, 91.77, 46.047, 28.637, 161.17, 0.2)}
controlStatZ(statz, refstatz)

# --- add a field on nodes of type double with z values, named "BOTTOM"
#createZfield2(fichierMaillage)

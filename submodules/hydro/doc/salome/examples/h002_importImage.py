# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")

import sys
import salome

salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0, 0 )

garonne_ign_01 = hydro_doc.CreateObject( KIND_IMAGE )
garonne_ign_01.SetName( "garonne_ign_01" )

garonne_ign_01.SetZLevel( 0 )


if not(garonne_ign_01.LoadImage(os.path.join(HYDRO_SAMPLES, "garonne_ign_01.png" ))):
  raise ValueError('problem while loading image')

garonne_ign_01.SetLocalPoints( QPoint( 40, 817 ),
                               QPoint( 1325, 85 ) )

garonne_ign_01.SetGlobalPoints( 1,
                                QPointF( 471562, 6.36775e+06 ),
                                QPointF( 489400, 6.37702e+06 ) )

garonne_ign_01.Update()

garonne_ign_02 = hydro_doc.CreateObject( KIND_IMAGE )
garonne_ign_02.SetName( "garonne_ign_02" )

garonne_ign_02.SetZLevel( 1 )


if not(garonne_ign_02.LoadImage( os.path.join(HYDRO_SAMPLES, "garonne_ign_02.png" ))):
  raise ValueError('problem while loading image')

garonne_ign_02.SetLocalPoints( QPoint( 1389, 447 ),
                               QPoint( 784, 481 ) )

garonne_ign_02.SetGlobalPoints( 3,
                                QPointF( 631, 95 ),
                                QPointF( 26, 129 ) )
garonne_ign_02.SetTrsfReferenceImage( garonne_ign_01 )

garonne_ign_02.Update()

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

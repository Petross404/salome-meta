# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_RESOURCES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "share/salome/resources/hydro")

import sys
import salome
salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

axe = hydro_doc.CreateObject( KIND_POLYLINEXY )
axe.SetName( "axe" )

axe.SetZLevel( 0 )

axe.AddSection( "Section_1", 1, 0 )
axe.AddPoint( 0, gp_XY( 107.09, -10.91 ) )
axe.AddPoint( 0, gp_XY( 102.60, 39.37 ) )
axe.AddPoint( 0, gp_XY( 70.28, 83.06 ) )
axe.AddPoint( 0, gp_XY( 28.39, 106.10 ) )
axe.AddPoint( 0, gp_XY( -14.11, 109.99 ) )

axe.Update()


domain = hydro_doc.CreateObject( KIND_POLYLINEXY )
domain.SetName( "domain" )

domain.SetZLevel( 3 )

domain.AddSection( "Section_1", 0, 1 )
domain.AddPoint( 0, gp_XY( 0.46, 0.37 ) )
domain.AddPoint( 0, gp_XY( 125.71, 0.18 ) )
domain.AddPoint( 0, gp_XY( 109.60, 112.58 ) )
domain.AddPoint( 0, gp_XY( 1.03, 123.51 ) )

domain.Update()


bathyFlat = hydro_doc.CreateObject( KIND_BATHYMETRY )
bathyFlat.SetName( "bathyFlat" )

bathyFlat.SetAltitudesInverted( 0 )
if not(bathyFlat.ImportFromFile( os.path.join(HYDRO_SAMPLES, "bathyFlat.xyz" ))):
  raise ValueError('problem while loading bathymetry')

bathyFlat.Update()

profiles = hydro_doc.CreateObject( KIND_PROFILE )
profiles.SetName( "profiles" )
badProfilesIds=[]
isToProject=True
nbp = profiles.ImportFromFile(hydro_doc, os.path.join(HYDRO_SAMPLES, "profilsStream.xyz"), badProfilesIds, isToProject)
print("nombre profils: " , nbp)
if not(nbp):
  raise ValueError('problem while loading profiles')
#profiles.Update()

nomsprofs = ["Profile_%d"%i for i in range(1,nbp+1)]
seqProfs = hydro_doc.FindObjectsByNames(nomsprofs)


plaine = hydro_doc.CreateObject( KIND_IMMERSIBLE_ZONE )
plaine.SetName( "plaine" )

plaine.SetZLevel( 1 )

plaine.SetFillingColor( QColor( 185, 171, 101, 255 ) )

plaine.SetAltitudeObject( bathyFlat )
plaine.SetPolyline( domain )

plaine.Update()


Stream_1 = hydro_doc.CreateObject( KIND_STREAM )
Stream_1.SetName( "Stream_1" )

Stream_1.SetZLevel( 2 )

Stream_1.SetHydraulicAxis( axe )
for profil in seqProfs:
  Stream_1.AddProfile(profil)

Stream_1.SetDDZ( 0.050 )
Stream_1.SetSpatialStep( 0.5 )

Stream_1.Update()


# Calculation case
Case_1 = hydro_doc.CreateObject( KIND_CALCULATION )
Case_1.SetName( "Case_1" )

Case_1.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
Case_1.AddGeometryObject( plaine )
Case_1.AddGeometryObject( Stream_1 )

case_geom_group = plaine.GetGroup( 0 )
Case_1.AddGeometryGroup( case_geom_group )
case_geom_group = Stream_1.GetGroup( 2 )
Case_1.AddGeometryGroup( case_geom_group )
case_geom_group = Stream_1.GetGroup( 0 )
Case_1.AddGeometryGroup( case_geom_group )
case_geom_group = Stream_1.GetGroup( 3 )
Case_1.AddGeometryGroup( case_geom_group )
case_geom_group = Stream_1.GetGroup( 1 )
Case_1.AddGeometryGroup( case_geom_group )
Case_1.SetBoundaryPolyline( domain )

# Start the algorithm of the partition and assignment
Case_1.Update()
Reg_riveGauche = hydro_doc.FindObjectByName( "Case_1_Reg_1" )
Case_1_Zone_1 = hydro_doc.FindObjectByName( "Case_1_Zone_1" )
Case_1_Zone_1.SetColor( QColor( 156, 192, 64 ))
Reg_riveGauche.AddZone( Case_1_Zone_1 )
Reg_litMineur = hydro_doc.FindObjectByName( "Case_1_Reg_2" )
Case_1_Zone_2 = hydro_doc.FindObjectByName( "Case_1_Zone_2" )
Case_1_Zone_2.SetMergeType( HYDROData_Zone.Merge_Object )
Stream_1_Altitude_1 = hydro_doc.FindObjectByName( "Stream_1_Altitude_1" )
Case_1_Zone_2.SetMergeObject( Stream_1_Altitude_1 )
Case_1_Zone_2.SetColor( QColor( 64, 75, 192 ))
Reg_litMineur.AddZone( Case_1_Zone_2 )
Reg_riveDroite = hydro_doc.FindObjectByName( "Case_1_Reg_3" )
Case_1_Zone_3 = hydro_doc.FindObjectByName( "Case_1_Zone_3" )
Case_1_Zone_3.SetColor( QColor( 192, 109, 64 ))
Reg_riveDroite.AddZone( Case_1_Zone_3 )
Reg_riveGauche.SetName( "Reg_riveGauche" )
Reg_litMineur.SetName( "Reg_litMineur" )
Reg_riveDroite.SetName( "Reg_riveDroite" )

# Export of the calculation case
Case_1_entry = Case_1.Export()

#----------------------
# --- Geometry
#----------------------

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

# Get geometry shape and print debug information
print("Entry:", Case_1_entry)
HYDRO_Case_1 = salome.IDToObject( str( Case_1_entry ) )
print("Geom shape:", HYDRO_Case_1)
print("Geom shape name:", HYDRO_Case_1.GetName())

geompy = geomBuilder.New()

# --- manual definition: geometrical faces
[riveGauche,litMineur,riveDroite] = geompy.SubShapeAll(HYDRO_Case_1, geompy.ShapeType["FACE"])

# --- manual definition: useful groups of edges
Case_1_plaine_Outer = geompy.CreateGroup(HYDRO_Case_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(Case_1_plaine_Outer, [4, 12, 19, 23, 25, 21, 14, 9])

# --- manual identification of all useful edge groups (boundary conditions)

allEdgesIds = geompy.SubShapeAllIDs(HYDRO_Case_1, geompy.ShapeType["EDGE"])
print("allEdgesIds", allEdgesIds)

(isDone, ClosedFreeBoundary, OpenFreeBoundary) = geompy.GetFreeBoundary(HYDRO_Case_1)
geompy.addToStudyInFather(HYDRO_Case_1, ClosedFreeBoundary[0], "ClosedFreeBoundary")

freeBoundary = geompy.ExtractShapes(ClosedFreeBoundary[0], geompy.ShapeType["EDGE"], True)
freeBoundaryIds = [ geompy.GetSubShapeID(HYDRO_Case_1, freeBoundary[i]) for i in range(len(freeBoundary)) ]
print("freeBoundaryIds", freeBoundaryIds)

edges_litMineur = geompy.GetSharedShapesMulti([HYDRO_Case_1, litMineur], geompy.ShapeType["EDGE"], True)
edges_litMineurIds = [ geompy.GetSubShapeID(HYDRO_Case_1, edges_litMineur[i]) for i in range(len(edges_litMineur)) ]
sectionsIds = [Id for Id in edges_litMineurIds if Id in freeBoundaryIds]
print("sectionsIds", sectionsIds)
sections = geompy.CreateGroup(HYDRO_Case_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(sections, sectionsIds)

# --- publish in study
geompy.addToStudyInFather( HYDRO_Case_1, riveGauche, 'riveGauche' )
geompy.addToStudyInFather( HYDRO_Case_1, litMineur, 'litMineur' )
geompy.addToStudyInFather( HYDRO_Case_1, riveDroite, 'riveDroite' )
geompy.addToStudyInFather( HYDRO_Case_1, sections, 'sections' )

# --- basic properties control: edges length, surfaces

controlGeomProps(geompy, riveGauche, 355.239855, 7755.628676)
controlGeomProps(geompy, litMineur,  383.395884, 3432.296956)
controlGeomProps(geompy, riveDroite, 419.214176, 2537.363869)

#----------------------
# --- Meshing
#----------------------

med_file = r'/tmp/plaine.med'
try:
  os.remove(med_file)
except OSError:
  pass

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats

smesh = smeshBuilder.New()

# --- algorithms and hypothesis
plaine = smesh.Mesh(HYDRO_Case_1)

NETGEN_2D = plaine.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters_1 = NETGEN_2D.Parameters()
NETGEN_2D_Parameters_1.SetMaxSize( 10 )
NETGEN_2D_Parameters_1.SetSecondOrder( 0 )
NETGEN_2D_Parameters_1.SetOptimize( 1 )
NETGEN_2D_Parameters_1.SetFineness( 3 )
NETGEN_2D_Parameters_1.SetMinSize( 1 )
NETGEN_2D_Parameters_1.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters_1.SetFuseEdges( 1 )
NETGEN_2D_Parameters_1.SetQuadAllowed( 0 )

algo2D_litMineur = plaine.Quadrangle(algo=smeshBuilder.QUADRANGLE,geom=litMineur)
algo1D_litMineur = plaine.Segment(geom=litMineur)
hypo1D_litMineur = algo1D_litMineur.NumberOfSegments(60)
subMesh_litMineur = algo1D_litMineur.GetSubMesh()
smesh.SetName(subMesh_litMineur, "litMineur")

algo1D_sections = plaine.Segment(geom=sections)
hypo1D_sections = algo1D_sections.NumberOfSegments(20)
hypo1D_sections.SetDistrType( 0 )
subMesh_sections = algo1D_sections.GetSubMesh()
smesh.SetName(subMesh_sections, "sections")

isDone = plaine.SetMeshOrder( [ [ subMesh_sections, subMesh_litMineur ] ])

# --- compute mesh
isDone = plaine.Compute()
isDone = plaine.SplitQuadObject( plaine, 1 )
isDone = plaine.ReorientObject( plaine )

# --- geometrical groups of faces
riveGauche_1 = plaine.GroupOnGeom(riveGauche,'riveGauche',SMESH.FACE)
litMineur_2 = plaine.GroupOnGeom(litMineur,'litMineur',SMESH.FACE)
riveDroite_1 = plaine.GroupOnGeom(riveDroite,'riveDroite',SMESH.FACE)

# --- geometrical groups of edges
Case_1_plaine_Outer_1 = plaine.GroupOnGeom(Case_1_plaine_Outer,'Case_1_plaine_Outer',SMESH.EDGE)
sections_2 = plaine.GroupOnGeom(sections,'sections',SMESH.EDGE)

# --- geometrical groups of nodes
riveGauche_2 = plaine.GroupOnGeom(riveGauche,'riveGauche',SMESH.NODE)
litMineur_3 = plaine.GroupOnGeom(litMineur,'litMineur',SMESH.NODE)
riveDroite_2 = plaine.GroupOnGeom(riveDroite,'riveDroite',SMESH.NODE)
Case_1_plaine_Outer_2 = plaine.GroupOnGeom(Case_1_plaine_Outer,'Case_1_plaine_Outer',SMESH.NODE)
sections_3 = plaine.GroupOnGeom(sections,'sections',SMESH.NODE)

smesh.SetName(plaine, 'plaine')

plaine.SetAutoColor( 1 )
try:
  plaine.ExportMED( med_file, 0, SMESH.MED_V2_2, 1, None ,1)
except:
  print('ExportToMEDX() failed. Invalid file name?')

controlMeshStats(plaine, 1680, 227, 3251)
controlSubMeshStats(litMineur_2, 2400)
controlSubMeshStats(riveDroite_1, 257)
controlSubMeshStats(riveGauche_1, 580)

#----------------------
# --- Z interpolation with HYDRO
#----------------------

from salome.hydrotools.interpolZ import interpolZ
from salome.hydrotools.controls import controlStatZ

# --- case name in HYDRO
nomCas = 'Case_1'

# --- med file 2D(x,y) of the case produced by SMESH
fichierMaillage = med_file

# --- dictionary [med group name] = region name
dicoGroupeRegion= dict(litMineur  = 'Reg_litMineur',
                       riveDroite = 'Reg_riveDroite',
                       riveGauche = 'Reg_riveGauche',
                       )
# --- value to use for Z when the node is not in a region (used to detect problems)
zUndef = 110
# --- interpolation Method: 0 = nearest point on bathymetry (default), 1 = linear interpolation
interpolMethod = 0
# --- produce a 3D mesh (Z set to its value instead of 0
m3d = True

# --- Z interpolation on the bathymety/altimetry on the mesh nodes
statz = interpolZ(nomCas, fichierMaillage, dicoGroupeRegion, zUndef, interpolMethod, m3d)
#print statz
refstatz = {'riveDroite': (100.0, 100.0, 100.0, 0.0, 100.0, 100.0),
            'riveGauche': (100.0, 100.0, 100.0, 0.0, 100.0, 100.0),
            'litMineur': (80.35, 100.0, 92.88, 4.82, 84.44, 100.0)}
controlStatZ(statz, refstatz)

# --- add a field on nodes of type double with z values, named "BOTTOM"
#createZfield2(fichierMaillage)



if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

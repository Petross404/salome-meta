# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")
if not os.path.isdir(HYDRO_TEST_RESOURCES):
    os.mkdir(HYDRO_TEST_RESOURCES)

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

import sys
import salome

salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import loadImage, GeolocaliseImageCoords, GeolocaliseImageReference
from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()

offsetX = 430000.
offsetY = 6350000.
hydro_doc.SetLocalCS( offsetX, offsetY )

garonne_ign_01 = loadImage(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_ign_01.png"), "garonne_ign_01", 0)
GeolocaliseImageCoords(garonne_ign_01,
                       QPoint(40, 817), QPoint(1325, 85),
                       QPointF(471562, 6.36775e+06), QPointF(489400, 6.37702e+06))

garonne_ign_02 = loadImage(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_ign_02.png"), "garonne_ign_02", 1)
GeolocaliseImageReference(garonne_ign_02, garonne_ign_01,
                          QPoint(1389, 447), QPoint(784, 481),
                          QPoint(631, 95), QPoint(26, 129))

garonnes = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne.shp"), True, 5)
limites_domaine = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "domaine.shp"), False, 6)
lits_majeur = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "lit_majeur.shp"), True, 7)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))
garonne_point_L93 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "garonne_point_L93.xyz"))

domaineEtendu = createImmersibleZone(hydro_doc, "domaineEtendu", limites_domaine[0], Cloud_02, True, 2)
litMajeur = createImmersibleZone(hydro_doc, "litMajeur", lits_majeur[0], garonne_point_L93, True, 3)
litMineur = createImmersibleZone(hydro_doc, "litMineur", garonnes[0], garonne_point_L93, True, 4)

# Calculation case
garonne_1 = hydro_doc.CreateObject( KIND_CALCULATION )
garonne_1.SetName( "garonne_1" )

garonne_1.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
garonne_1.AddGeometryObject( litMineur )
garonne_1.AddGeometryObject( domaineEtendu )
garonne_1.AddGeometryObject( litMajeur )

case_geom_group = domaineEtendu.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = litMineur.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
case_geom_group = litMajeur.GetGroup( 0 )
garonne_1.AddGeometryGroup( case_geom_group )
garonne_1.SetBoundaryPolyline( limites_domaine[0] )

# Start the algorithm of the partition and assignment
garonne_1.Update()
garonne_1_litMineur = hydro_doc.FindObjectByName( "garonne_1_Reg_1" )
garonne_1_Zone_1 = hydro_doc.FindObjectByName( "garonne_1_Zone_1" )
garonne_1_Zone_1.SetMergeType( HYDROData_Zone.Merge_ZMIN )
garonne_1_Zone_1.SetColor( QColor( 192, 113, 64 ))
garonne_1_litMineur.AddZone( garonne_1_Zone_1 )

garonne_1_riveDroite = hydro_doc.FindObjectByName( "garonne_1_Reg_2" )
garonne_1_Zone_2 = hydro_doc.FindObjectByName( "garonne_1_Zone_2" )
garonne_1_Zone_2.SetColor( QColor( 141, 192, 64 ))
garonne_1_riveDroite.AddZone( garonne_1_Zone_2 )

garonne_1_Zone_3 = hydro_doc.FindObjectByName( "garonne_1_Zone_3" )
garonne_1_Zone_3.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
garonne_1_Zone_3.SetMergeObject( Cloud_02 )
garonne_1_Zone_3.SetColor( QColor( 64, 192, 77 ))
garonne_1_riveDroite.AddZone( garonne_1_Zone_3 )

garonne_1_riveGauche = hydro_doc.FindObjectByName( "garonne_1_Reg_3" )
garonne_1_Zone_4 = hydro_doc.FindObjectByName( "garonne_1_Zone_4" )
garonne_1_Zone_4.SetMergeType( HYDROData_Zone.Merge_Object )
Cloud_02 = hydro_doc.FindObjectByName( "Cloud_02" )
garonne_1_Zone_4.SetMergeObject( Cloud_02 )
garonne_1_Zone_4.SetColor( QColor( 64, 75, 192 ))
garonne_1_riveGauche.AddZone( garonne_1_Zone_4 )

garonne_1_Zone_5 = hydro_doc.FindObjectByName( "garonne_1_Zone_5" )
garonne_1_Zone_5.SetColor( QColor( 64, 192, 77 ))
garonne_1_riveGauche.AddZone( garonne_1_Zone_5 )

garonne_1_litMineur.SetName( "garonne_1_litMineur" )
garonne_1_riveDroite.SetName( "garonne_1_riveDroite" )
garonne_1_riveGauche.SetName( "garonne_1_riveGauche" )

# Export of the calculation case
garonne_1_entry = garonne_1.Export()

#----------------------
# --- Geometry
#----------------------

# Get geometry shape and print debug information
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

geompy = geomBuilder.New()

print("Entry:", garonne_1_entry)
HYDRO_garonne_1 = salome.IDToObject( str( garonne_1_entry ) )
print("Geom shape:", HYDRO_garonne_1)
print("Geom shape name:", HYDRO_garonne_1.GetName())


children = getChildrenInStudy(HYDRO_garonne_1)
garonne_riveGauche = children["garonne_1_riveGauche"]
garonne_litMineur = children["garonne_1_litMineur"]
garonne_riveDroite = children["garonne_1_riveDroite"]
#[garonne_litMineur,garonne_riveDroite,garonne_riveGauche] = geompy.SubShapeAll(HYDRO_garonne_1, geompy.ShapeType["FACE"])

controlGeomProps(geompy, garonne_riveGauche,  29149.36918,  35948828.352061)
controlGeomProps(geompy, garonne_litMineur,   30337.548492,  3488480.304388)
controlGeomProps(geompy, garonne_riveDroite,  32012.343241, 25998769.23615)

# --- manual identification of all useful edge groups (boundary conditions)

allEdgesIds = geompy.SubShapeAllIDs(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
print("allEdgesIds", allEdgesIds)

(isDone, ClosedFreeBoundary, OpenFreeBoundary) = geompy.GetFreeBoundary(HYDRO_garonne_1)
geompy.addToStudyInFather(HYDRO_garonne_1, ClosedFreeBoundary[0], "ClosedFreeBoundary")

freeBoundary = geompy.ExtractShapes(ClosedFreeBoundary[0], geompy.ShapeType["EDGE"], True)
freeBoundaryIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, freeBoundary[i]) for i in range(len(freeBoundary)) ]
print("freeBoundaryIds", freeBoundaryIds)

[litMineur_droite] = geompy.GetSharedShapesMulti([garonne_riveDroite, garonne_litMineur], geompy.ShapeType["EDGE"], True)
[litMineur_gauche] = geompy.GetSharedShapesMulti([garonne_riveGauche, garonne_litMineur], geompy.ShapeType["EDGE"], True)
geompy.addToStudyInFather(HYDRO_garonne_1, litMineur_droite, "litMineur_droite")
geompy.addToStudyInFather(HYDRO_garonne_1, litMineur_gauche, "litMineur_gauche")
rives = [litMineur_droite, litMineur_gauche]
rivesIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, rives[i]) for i in range(len(rives)) ]
print("rivesIds", rivesIds)

edges_litMineur = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_litMineur], geompy.ShapeType["EDGE"], True)
edges_riveGauche = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_riveGauche], geompy.ShapeType["EDGE"], True)
edges_riveDroite = geompy.GetSharedShapesMulti([HYDRO_garonne_1, garonne_riveDroite], geompy.ShapeType["EDGE"], True)
edges_litMineurIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_litMineur[i]) for i in range(len(edges_litMineur)) ]
edges_riveGaucheIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_riveGauche[i]) for i in range(len(edges_riveGauche)) ]
edges_riveDroiteIds = [ geompy.GetSubShapeID(HYDRO_garonne_1, edges_riveDroite[i]) for i in range(len(edges_riveDroite)) ]

print("edges_litMineurIds", edges_litMineurIds) 
print("edges_riveGaucheIds", edges_riveGaucheIds)
print("edges_riveDroiteIds", edges_riveDroiteIds)

sectionsIds = [Id for Id in edges_litMineurIds if Id not in rivesIds]
print("sectionsIds", sectionsIds)
SectionsGaronne = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(SectionsGaronne, sectionsIds)
geompy.addToStudyInFather(HYDRO_garonne_1, SectionsGaronne, "SectionsGaronne")

bordGaucheDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveGaucheIds]
bordDroiteDomaineIds = [Id for Id in freeBoundaryIds if Id in edges_riveDroiteIds]
print("bordGaucheDomaineIds", bordGaucheDomaineIds)
print("bordDroiteDomaineIds", bordDroiteDomaineIds)
bordGaucheDomaine = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordGaucheDomaine, bordGaucheDomaineIds)
geompy.addToStudyInFather(HYDRO_garonne_1, bordGaucheDomaine, "bordGaucheDomaine")
bordDroiteDomaine = geompy.CreateGroup(HYDRO_garonne_1, geompy.ShapeType["EDGE"])
geompy.UnionIDs(bordDroiteDomaine, bordDroiteDomaineIds)
geompy.addToStudyInFather(HYDRO_garonne_1, bordDroiteDomaine, "bordDroiteDomaine")

amont = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(46757.861314, 25833.234752, 0))
aval = geompy.GetEdgeNearPoint(HYDRO_garonne_1, geompy.MakeVertex(39078.979127, 32588.627279, 0))
geompy.addToStudyInFather(HYDRO_garonne_1, amont, "amont")
geompy.addToStudyInFather(HYDRO_garonne_1, aval, "aval")

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

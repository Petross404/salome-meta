# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")
if not os.path.isdir(HYDRO_TEST_RESOURCES):
    os.mkdir(HYDRO_TEST_RESOURCES)

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

import sys
import salome

salome.salome_init()

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import loadImage, GeolocaliseImageCoords, GeolocaliseImageReference
from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0, 0 )

simplePolylines = importPolylines(hydro_doc, os.path.join(HYDRO_SAMPLES, "simplePolyline.shp"), True, 1)
imZone = createImmersibleZone(hydro_doc, "imZone", simplePolylines[0], None, True, 0)

# Calculation case
Case_1 = hydro_doc.CreateObject( KIND_CALCULATION )
Case_1.SetName( "Case_1" )

Case_1.SetAssignmentMode( HYDROData_CalculationCase.AUTOMATIC )
Case_1.AddGeometryObject( imZone )

case_geom_group = imZone.GetGroup( 0 );
Case_1.AddGeometryGroup( case_geom_group );
Case_1.SetBoundaryPolyline( simplePolylines[0] )

# Start the algorithm of the partition and assignment
Case_1.Update()

# Export of the calculation case
Case_1_entry = Case_1.Export()

#----------------------
# --- Geometry
#----------------------

# Get geometry shape and print debug information
import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS
from salome.hydrotools.controls import controlGeomProps

geompy = geomBuilder.New()

print("Entry:", Case_1_entry)
Case_1_geom = salome.IDToObject( str( Case_1_entry ) )
print("Geom shape:", Case_1_geom)
print("Geom shape name:", Case_1_geom.GetName())

controlGeomProps(geompy, Case_1_geom, 1218.7373973, 49578.1516521)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

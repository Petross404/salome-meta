# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

# --- generated resources

origFileMesh = os.path.join(HYDRO_TEST_RESOURCES, 'garonne_2.med')

# ----------------------------------------------------------------------------------
# --- domain extension

import sys
import salome

salome.salome_init()

from salome.hydrotools.shapesGroups import freeBordersGroup, exploreEdgeGroups, fitShapePointsToMesh

offsetX = 430000.
offsetY = 6350000.

ficMeshOut = os.path.join(tmpdir, "garonne_2_brd.med")
fileMeshBrd = freeBordersGroup(origFileMesh, ficMeshOut)
exploreEdgeGroups(fileMeshBrd, "", offsetX, offsetY)

fitShapePointsToMesh(os.path.join(tmpdir, "garonne_2_brd_FreeBorders.shp"),
                     os.path.join(HYDRO_SAMPLES, 'extension_1_3.shp'),
                     tmpdir, True, True)

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines

hydro_doc = HYDROData_Document.Document()
hydro_doc.SetLocalCS( offsetX, offsetY )

limites_original = importPolylines(hydro_doc, os.path.join(tmpdir, "garonne_2_brd_FreeBorders_split.shp"), True, 4)
limites_domaine  = importPolylines(hydro_doc, os.path.join(tmpdir, "extension_1_3_adj.shp"), False, 2)

limite_extension = mergePolylines(hydro_doc, 'limite_extension', [limites_original[1], limites_domaine[1]])

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))

domaine_extension = createImmersibleZone(hydro_doc, "domaine_extension", limite_extension, Cloud_02, True, 0)

# Calculation case
extension = hydro_doc.CreateObject( KIND_CALCULATION )
extension.SetName( "extension" )
extension.SetAssignmentMode( HYDROData_CalculationCase.AUTOMATIC )
extension.AddGeometryObject( domaine_extension )
extension.SetBoundaryPolyline( limite_extension )
extension.Update()

# # Calculation case
# extension = hydro_doc.CreateObject( KIND_CALCULATION )
# extension.SetName( "extension" )
# extension.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
# extension.AddGeometryObject( domaine_extension )
# case_geom_group = domaine_extension.GetGroup( 0 )
# extension.AddGeometryGroup( case_geom_group )
# extension.SetBoundaryPolyline( limite_extension )
# extension.SetStricklerTable( Strickler_table_1 )
# # Start the algorithm of the partition and assignment
# extension.Update()
# extension_Reg_1 = hydro_doc.FindObjectByName( "extension_Reg_1" )
# extension_Zone_1 = hydro_doc.FindObjectByName( "extension_Zone_1" )
# extension_Zone_1.SetColor( QColor( 105, 228, 28 ))
# extension_Reg_1.AddZone( extension_Zone_1 )
# extension_Reg_1.SetName( "Case_1_Reg_1" )

# Export of the calculation case
extension_entry = extension.Export()

##----------------------
## --- Geometry
##----------------------

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

from salome.hydrotools.hydroGeoMeshUtils import getChildrenInStudy

geompy = geomBuilder.New()

print ("Entry:", extension_entry)
HYDRO_extension = salome.IDToObject( str( extension_entry ) )
print ("Geom shape:", HYDRO_extension)
print ("Geom shape name:", HYDRO_extension.GetName())

[reg_extension] = geompy.SubShapeAll(HYDRO_extension, geompy.ShapeType["FACE"])
#children = getChildrenInStudy(HYDRO_extension)
#extension_domaine_original_Outer = children["extension_domaine_original_Outer"]
#TODO: better identification
extension_domaine_original_Outer = geompy.CreateGroup(HYDRO_extension, geompy.ShapeType["EDGE"])
geompy.UnionIDs(extension_domaine_original_Outer, [3])
geompy.addToStudyInFather( HYDRO_extension, extension_domaine_original_Outer, 'extension_domaine_original_Outer' )

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
#--- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats

smesh = smeshBuilder.New()

#--- algorithms and hypothesis
extensionEnglobante = smesh.Mesh(HYDRO_extension)

NETGEN_2D = extensionEnglobante.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 5 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

([origMeshBrd], status) = smesh.CreateMeshesFromMED(fileMeshBrd)
FreeBorders = origMeshBrd.GetGroupByName("FreeBorders")

Import_1D = extensionEnglobante.UseExisting1DElements(geom=extension_domaine_original_Outer)
Source_Edges_1 = Import_1D.SourceEdges( FreeBorders ,1,1)

isDone = extensionEnglobante.Compute()

controlMeshStats(extensionEnglobante, 4811, 599, 9368)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

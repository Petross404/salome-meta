# -*- coding: utf-8 -*-

import os
HYDRO_SAMPLES = os.path.join( os.environ["HYDRO_ROOT_DIR"], "bin/salome/test/HYDRO")
HYDRO_TEST_RESOURCES = os.path.join(os.environ["HYDRO_DIR"], "bin", "salome", "test", "tmp_test")

import tempfile
tmpdir = tempfile.mkdtemp()
print("tmpdir=",tmpdir)

# --- generated resources

origFileMeshAval = os.path.join(HYDRO_TEST_RESOURCES, 'garonne_2.med')
origFileMeshAmont = os.path.join(HYDRO_TEST_RESOURCES, 'garonneAmont.med')

# ----------------------------------------------------------------------------------
# --- domain extension

import sys
import salome

salome.salome_init()

from salome.hydrotools.shapesGroups import freeBordersGroup, exploreEdgeGroups, fitShapePointsToMesh

offsetX = 430000.
offsetY = 6350000.

ficMeshOutAval = os.path.join(tmpdir, "garonneAval_brd.med")
fileMeshAvalBrd = freeBordersGroup(origFileMeshAval, ficMeshOutAval)
exploreEdgeGroups(fileMeshAvalBrd, "", offsetX, offsetY)

ficMeshOutAmont = os.path.join(tmpdir, "garonneAmont_brd.med")
fileMeshAmontBrd = freeBordersGroup(origFileMeshAmont, ficMeshOutAmont)
exploreEdgeGroups(fileMeshAmontBrd, "", offsetX, offsetY)

#----------------------
# --- HYDRO
#----------------------

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from salome.hydrotools.hydroGeoMeshUtils import importPolylines, importBathymetry, createImmersibleZone, mergePolylines, getChildrenInStudy

hydro_doc = HYDROData_Document.Document()
hydro_doc.SetLocalCS( offsetX, offsetY )

fitShapePointsToMesh(os.path.join(tmpdir, "garonneAval_brd_FreeBorders.shp"),
                     os.path.join(HYDRO_SAMPLES, 'raccord_4_1.shp'),
                     tmpdir, False, False)

# TODO: if limites_aval imported as spline, problem on mesh with 2D triangle algorithm
limites_aval = importPolylines(hydro_doc, os.path.join(tmpdir, "garonneAval_brd_FreeBorders.shp"), False, 4)
limites_domaine1  = importPolylines(hydro_doc, os.path.join(tmpdir, "raccord_4_1_adj.shp"), False, 2)

fitShapePointsToMesh(os.path.join(tmpdir, "garonneAmont_brd_FreeBorders.shp"),
                     os.path.join(tmpdir, 'raccord_4_1_adj.shp'),
                     tmpdir, False, False)


# TODO: if limites_amont imported as spline, problem on mesh with 2D triangle algorithm
limites_amont = importPolylines(hydro_doc, os.path.join(tmpdir, "garonneAmont_brd_FreeBorders.shp"), False, 5)
limites_domaine  = importPolylines(hydro_doc, os.path.join(tmpdir, "raccord_4_1_adj_adj.shp"), False, 2)

Cloud_02 = importBathymetry(hydro_doc, os.path.join(HYDRO_SAMPLES, "Cloud_02.xyz"))

junction = createImmersibleZone(hydro_doc, "junction", limites_domaine[0], Cloud_02, True, 0)
garonneAval = createImmersibleZone(hydro_doc, "garonneAval", limites_aval[0], Cloud_02, False, 0)
garonneAmont = createImmersibleZone(hydro_doc, "garonneAmont", limites_amont[0], Cloud_02, False, 0)

# Calculation case
Junction = hydro_doc.CreateObject( KIND_CALCULATION )
Junction.SetName( "Junction" )

Junction.SetAssignmentMode( HYDROData_CalculationCase.MANUAL )
Junction.AddGeometryObject( garonneAmont )
Junction.AddGeometryObject( garonneAval )
Junction.AddGeometryObject( junction )

case_geom_group = garonneAmont.GetGroup( 0 )
Junction.AddGeometryGroup( case_geom_group )
case_geom_group = junction.GetGroup( 0 )
Junction.AddGeometryGroup( case_geom_group )
case_geom_group = garonneAval.GetGroup( 0 )
Junction.AddGeometryGroup( case_geom_group )

Junction.SetBoundaryPolyline( limites_domaine[0] )

# Start the algorithm of the partition and assignment
Junction.Update()
Junction_Reg_1 = hydro_doc.FindObjectByName( "Junction_Reg_1" )
Junction_Zone_1 = hydro_doc.FindObjectByName( "Junction_Zone_1" )
Junction_Zone_1.SetMergeType( HYDROData_Zone.Merge_ZMAX )
Junction_Zone_1.SetColor( QColor( 228, 111, 28 ))
Junction_Reg_1.AddZone( Junction_Zone_1 )
Junction_Reg_2 = hydro_doc.FindObjectByName( "Junction_Reg_2" )
Junction_Zone_2 = hydro_doc.FindObjectByName( "Junction_Zone_2" )
Junction_Zone_2.SetMergeType( HYDROData_Zone.Merge_ZMAX )
Junction_Zone_2.SetColor( QColor( 185, 228, 28 ))
Junction_Reg_2.AddZone( Junction_Zone_2 )
Junction_Reg_3 = hydro_doc.FindObjectByName( "Junction_Reg_3" )
Junction_Zone_3 = hydro_doc.FindObjectByName( "Junction_Zone_3" )
Junction_Zone_3.SetColor( QColor( 28, 228, 111 ))
Junction_Reg_3.AddZone( Junction_Zone_3 )
Junction_Reg_1.SetName( "Junction_Reg_1" )
Junction_Reg_2.SetName( "Junction_Reg_2" )
Junction_Reg_3.SetName( "Junction_Reg_3" )

# Export of the calculation case
Junction_entry = Junction.Export()

##----------------------
## --- Geometry
##----------------------

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

from salome.hydrotools.hydroGeoMeshUtils import getChildrenInStudy

geompy = geomBuilder.New()

print ("Entry:", Junction_entry)
HYDRO_Junction = salome.IDToObject( str( Junction_entry ) )
print ("Geom shape:", HYDRO_Junction)
print ("Geom shape name:", HYDRO_Junction.GetName())

[reg_Junction] = geompy.SubShapeAll(HYDRO_Junction, geompy.ShapeType["FACE"])

children = getChildrenInStudy(HYDRO_Junction)
Junction_garonneAmont_Outer = children['Junction_garonneAmont_Outer']
Junction_garonneAval_Outer = children['Junction_garonneAval_Outer']

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#----------------------
#--- Meshing
#----------------------

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.hydrotools.controls import controlMeshStats, controlSubMeshStats

smesh = smeshBuilder.New()

#--- algorithms and hypothesis
meshJunction = smesh.Mesh(HYDRO_Junction)

NETGEN_2D = meshJunction.Triangle(algo=smeshBuilder.NETGEN_1D2D)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 200 )
NETGEN_2D_Parameters.SetSecondOrder( 0 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 4 )
NETGEN_2D_Parameters.SetMinSize( 5 )
NETGEN_2D_Parameters.SetUseSurfaceCurvature( 1 )
NETGEN_2D_Parameters.SetFuseEdges( 1 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

([origMeshAvalBrd], status) = smesh.CreateMeshesFromMED(fileMeshAvalBrd)
FreeBordersAval = origMeshAvalBrd.GetGroupByName("FreeBorders")
Import_1Daval = meshJunction.UseExisting1DElements(geom=Junction_garonneAval_Outer)
Source_Edges_1 = Import_1Daval.SourceEdges( FreeBordersAval ,1,1)

([origMeshAmontBrd], status) = smesh.CreateMeshesFromMED(fileMeshAmontBrd)
FreeBordersAmont = origMeshAmontBrd.GetGroupByName("FreeBorders")
Import_1Damont = meshJunction.UseExisting1DElements(geom=Junction_garonneAmont_Outer)
Source_Edges_2 = Import_1Damont.SourceEdges( FreeBordersAmont ,1,1)

isDone = meshJunction.Compute()

controlMeshStats(meshJunction, 7420, 918, 14525)

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

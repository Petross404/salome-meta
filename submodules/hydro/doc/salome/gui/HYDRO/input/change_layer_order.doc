
/**
  @file
  \brief Help for the change layer order
*/

/**
  \page change_layer_order Change layer order
  
  <a href="index.html"><b>Back to Main page.</b></a>

  The HYDROGUI module allows the user to define priority of visibility 2d objects.<br>
  This feature is executed with help of <b>Change layer order</b> dialog box, which can be called via according command in context menu, called in OCC viewer.<br>

<b>Change layer order</b> dialog box looks like: 

  \image html change_layer_order_dialog_box.png "Change layer order dialog box"
  
   Only shown objects are presented in the dialog box.<br>
   
   Controls of the dialog box: <br>  
   
   <b>Move the item(s) on top</b> - button to move selected in the list objects on top, i.e. set for them the highest visibility priority;<br>
   \image html move_item_on_top_button.png "Move the item(s) on top button"
   <b>Move the item(s) up</b> - button to move selected in the list objects up, i.e. increase visibility priority on one point;<br>
   \image html move_item_up_button.png "Move the item(s) up button"
   <b>Move the item(s) down</b> - button to move selected in the list objects down, i.e. decrease visibility priority on one point;<br>
   \image html move_item_down_button.png "Move the item(s) down button"
   <b>Move the item(s) on bottom</b> - button to move selected in the list objects on bottom, i.e. set for them least visibility priority;<br>
   \image html move_item_on_bottom_button.png "Move the item(s) on bottom button"
   <b>All objects</b> - check-box to show all objects(even hidden) in the list;<br>
   <b>Apply</b> - button to apply modifications done;<br>
   <b>Apply and Close</b> - button to apply modifications done and close dialog box;<br>
   <b>Close</b> - button to close dialog box.
   
   Example:
   \image html example_change_layer_order_before.png "Before changing layer order"
   
   \image html example_change_layer_order_after.png "After changing layer order"
   
<a href="index.html"><b>Back to Main page.</b></a>

*/

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Constitution du cas de calcul
#########################################

.. |createCalculationCase1| image:: /_static/createCalculationCase1.png
   :align: middle

.. |createCalculationCase2| image:: /_static/createCalculationCase2.png
   :align: middle

.. |createCalculationCase3| image:: /_static/createCalculationCase3.png
   :align: middle

.. |createCalculationCase4| image:: /_static/createCalculationCase4.png
   :align: middle

.. |createCalculationCase5| image:: /_static/createCalculationCase5.png
   :align: middle

.. |createCalculationCase6| image:: /_static/createCalculationCase6.png
   :align: middle

.. |createCalculationCase7| image:: /_static/createCalculationCase7.png
   :align: middle

.. |createCalculationCase8| image:: /_static/createCalculationCase8.png
   :align: middle

.. |createCalculationCase9| image:: /_static/createCalculationCase9.png
   :align: middle

.. |createCalculationCase10| image:: /_static/createCalculationCase10.png
   :align: middle

Création du cas
===============

Nous allons créer un *Cas de Calcul*, c'est à dire la configuration des objets que nous allons retenir dans notre étude.

Nous avons créé 3 ou 4 objets : le lit mineur, le lit majeur, la digue optionnelle et le domaine d'étude.

Pour créer un *cas de calcul*, nous utilisons le menu contextuel de la rubrique *CALCULATION CASE*.

  |createCalculationCase1|

Dans le dialogue, nous nommons le cas : *garonne_1*.

***Nous choisissons comme limite le domaine**,
nous prenons le **mode manuel** (ce mode sera expliqué plus loin),
nous sélectionnons les 3 objets naturels présents plus la digue optionnelle, pour les inclure dans le cas.

Il faut valider cette étape en appuyant sur le bouton *next*.

  |createCalculationCase2|

Le dialogue propose de sélectionner des groupes à conserver :
il s'agit des contours des différents objets pris en compte dans le cas.
Ces groupes peuvent se révéler utiles par la suite pour définir des conditions aux limites, ou contrôler le maillage.
Le fait de les conserver permet de les retrouver sous leur nom dans les différentes étapes du calcul.

Nous sélectionnons les groupes proposés, pour les garder dans le cas.

Il faut valider cette étape en appuyant sur le bouton *next*.

  |createCalculationCase3|

Le dialogue suivant permet d'affecter des coefficients de frottement à différentes zones du domaine,
à l'aide d'une table de coefficients de Strickler, et d'un découpage en zones `land cover map`.
Il faut avoir préalablement importé et/ou édité cette table de Strickler et ce `land cover map`.
Pour cet exemple, la `land cover map` est optionnelle. Pour l'introduire, on peut se référer au
paragraphe *Land Cover Map* plus bas.

Il faut valider cette étape en appuyant sur le bouton *next*.

  |createCalculationCase4|

Choix du calcul de la Bathymetrie pour les zones de chevauchement
=================================================================

L'étape suivante montre le résultat de la *partition* des différentes zones retenues dans le cas de calcul, sans la digue.
Les surfaces initiales ont été découpées selon les lignes de chevauchement.

Le panneau de droite montre autant de *régions* que de faces découpées : chaque région contient une face découpée ou *zone*.

Certaines zones sont colorées en rouge : ces zones sont les zones de chevauchement des faces initiales.
La liste des objets initiaux apparaît dans la deuxième colonne.

Pour ces zones, plusieurs définitions de bathymétrie sont possibles. Elles sont listées en troisième colonne.
Tant que l'on n'a pas défini quelle règle prendre pour calculer la bathymétrie sur la zone, elle est déclarée *en conflit*.

  |createCalculationCase5|

Il faut sélectionner successivement chaque zone en conflit, pour choisir le mode de calcul de la bathymétrie
pour la zone.
La sélection s'opère dans le menu déroulant associé à la zone.

Les choix possibles sont :

 * une bathymétrie parmi celles proposées

 * le minimum local de l'ensemble des bathymétries listées

 * le maximum local de l'ensemble des bathymétries listées

  |createCalculationCase6|

Quand la résolution des conflits est terminée, il ne doit plus rester de zone rouge.

Il est possible de régénérer les couleurs des zones (bouton *regenerate colors*) si nécessaire.

  |createCalculationCase7|

Regroupement des zones en régions
=================================

Nous regroupons ensuite plusieurs zones dans des régions. Pourquoi ?

Une zone correspond à un mode de calcul de la bathymétrie.
Le contour de cette zone n'est pas forcément intéressant en tant que *ligne de contrainte* du maillage.
Quand on regroupe plusieurs zones contiguës dans une même région, seul le contour de la région sera gardé en
tant que *ligne de contrainte* du maillage.

Nous regroupons donc les deux zones de la rive gauche ensemble, et faisons de même avec les deux zones de la rive Droite,
pour obtenir trois régions.

Pour déplacer une zone, il faut faire un *drag & drop* de cette zone d'une région à une autre.

Il faut valider cette étape en appuyant sur le bouton *next*.

  |createCalculationCase8|

Il faut valider cette dernière étape en appuyant sur le bouton *Finish*.

Le cas est publié dans l'arbre d'étude.Nous sélectionnons chaque région pour lui donner un nom significatif (menu contextuel *Rename*).

Pour identifier la région, il faut s'aider en examinant les zones qu'elle contient.
La vue graphique affiche les zones. En développant l'arbre d'étude au niveau de chaque région,
il est possible de sélectionner les zones pour les mettre en surbrillance.

En principe, il n'est pas nécessaire de renommer les zones elles mêmes, mais seulement les régions.
Ce sont les régions que l'on utilisera par la suite à l'aide de leur nom. Une étude réaliste peut en contenir plusieurs dizaines.

  |createCalculationCase10|
 
.. only:: html
 
  :ref:`ref_exempleInondation`


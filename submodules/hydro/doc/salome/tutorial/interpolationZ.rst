..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Interpolation en Z
#########################################

.. |HYDROSolver| image:: /_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |genereInterpolz| image:: /_static/genereInterpolz.png
   :align: middle
   
.. |BottomImage| image:: /_static/Bottom.png
   :align: middle

.. |Capture_meshZ| image:: /_static/Capture_meshZ.png
   :align: middle


.. |occ_view_scaling| image:: /_static/occ_view_scaling.png
   :align: middle
   :width: 16pt
   :height: 16pt


Le maillage que nous avons généré à l'étape précédente ne contient pas d'information d'altitude.
Pour alimenter le code TELEMAC avec cette information, il faut rajouter au maillage un champ contenant la
coordonnée Z au noeuds du maillage.

Le mode de calcul de la coordonnée Z a été décrit zone par zone dans la définition du cas de calcul,
dans le module HYDRO.

Calcul de l'interpolation en Z aux noeuds du maillage
=====================================================

La constitution du champ d'altitude se fait au moyen d'un script Python, qu'il faut préparer, puis exécuter.

Le script utilise une association des régions du cas de calcul HYDRO aux groupes de faces correspondants dans le maillage.
Une commande du module HYDROSOLVER facilite la définition de cette association.

Il faut maintenant activer le module HYDROSOLVER, via la liste défilante des modules, ou son icône dans le bandeau : |HYDROSolver|.
Nous utilisons la commande *Generate interpolz.py* du menu *Hydro*.

Il faut sélectionner le cas de calcul dans la rubrique *HYDRO / CALCULATION CASE* de l'arbre d'étude.
Son nom apparaît dans la première ligne *Calculation cas* du dialogue.

Le chemin complet du script à créer doit être renseigné dans la rubrique *Output path*.

Il faut désigner le fichier du maillage de départ construit à l'étape précédente dans rubrique *MED file*.

La valeur *Undefined Z* est utilisée quand le module HYDRO ne sait pas calculer l'altitude en un point.
C'est utilisé a postériori pour détecter d'éventuels problèmes de définition des zones de calcul dans le cas de calcul.

L'interpolation sur les nuages de points peut se faire de deux manières, selon que les nuages sont plus
denses que le maillage, ou l'inverse.
Pour des nuages de points très denses, il suffit de prendre l'altitude du point le plus proche du nuage.
Quand le maillage est plus dense que le nuage, il vaut mieux prendre l'altitude linéarisée, obtenue par
une triangulation préalable du nuage de points. Cette dernière méthode est plus précise
mais un peu plus coûteuse.

Il faut sélectionner les bons noms de régions en correspondance avec les noms des groupes de faces,
en laissant la sélection à *None* pour les autres groupes.

  |genereInterpolz|

Le script produit plusieurs fichiers dont le nom se déduit du nom du fichier maillage d'origine
avec des suffixes différents, rangés dans le répertoire du fichier d'origine :

* garonne_1.med  : fichier d'origine (coordonnée z = 0)
* garonne_1.xyz  : fichier xyz (ASCII) des altitudes aux noeuds *(optionnel)*
* garonne_1F.med : coordonnée Z à sa valeur calculée, et champ "BOTTOM" avec la valeur Z aux noeuds

**Remarque** : La modification de la coordonnée Z des noeuds du maillage n'est pas nécessaire à TELEMAC,
mais utile pour une visualisation de contrôle du maillage.

Pour exécuter le script, il faut que le module HYDRO soit bien actif dans l'étude.
**Si l'on reprend une étude précédemment sauvegardée, il faut avoir activé le module HYDRO avant
de lancer le script** (il suffit de sélectionner HYDRO au moins une fois,
pour que les données stockées dans le fichier d'étude soient lues).
Nous exécutons le script avec la commande du menu *File / Load Script...*.
Le script bloque l'interface graphique le temps de son exécution qui dépend de la taille du maillage
et des nuages de point de bathymétrie. Il affiche une trace d'exécution dans la console
Python qui est affichée par défaut dans les modules GEOM et SMESH.

Il est aussi possible d'adapter manuellement le script ci-dessous :

Il faut recopier le script et l'adapter en fonction des noms de régions utilisés dans le cas de calcul
et des noms de groupes de faces dans le maillage.

.. literalinclude:: interpolZ.py
    :lines: 1-

Visualisation de l'interpolation en Z aux noeuds du maillage
============================================================


Visualisation avec le module MED
--------------------------------

Le module MED offre une visualisation simple des champs d'un maillage MED.
Il faut activer le module MED, puis utiliser le menu *File/Add Data Source* ou l'icône équivalente, et retrouver le fichier *garonne_1F.med*.
En dépliant l'objet *garonne_1F.med* dans l'arbre d'étude, nous trouvons le maillage *HYDRO_Garonne_1* et le champ *BOTTOM*.

Il faut sélectionner le champ et utiliser l'icone *scalar map*.

Le champ s'affiche dans la vue 3D. Le menu contextuel de la vue 3D propose la commande *Representation / Surface with Edges*

  |BottomImage|

Visualisation dans le module SMESH
----------------------------------

A la fin de l'exécution du script d'interpolation, le maillage *HYDRO_Garonne_1* est apparu une seconde fois dans l'arbre d'étude,
sous la première instance, avec une icône différente. S'il n'y est pas, le menu contextuel de l'arbre d'étude propose la commande *Refresh*.

Nous affichons ce maillage dans le module SMESH, avec la commande *show*.
Pour mieux voir le relief, il faut modifier l'échelle en Z avec l'icône |occ_view_scaling| de la vue 3D. Ici, il suffit de prendre un facteur 3 pour Z.

*Rappel* : pour manipuler l'objet dans la vue 3D, il faut utiliser la touche <CTRL> et les boutons de la souris, ou la molette pour le zoom.

Voici la vue des groupes correspondant aux régions :

  |Capture_meshZ|

.. only:: html

   :ref:`ref_exempleInondation`

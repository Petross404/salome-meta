..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

Outils complémentaires : qgis et CloudCompare
###############################################

qgis et CloudCompare sont fournis à coté de SALOME-HYDRO.

A partir de SALOME 8, l'installation de qgis et/ou CloudCompare est fournie dans un produit séparé. 
A EDF cela dépend de la plateforme :

* sur Calibre 7 qgis et cloudCompare sont fournis à part,
* sur Calibre 9, cloudCompare est installé avec SALOME, qgis à part.

Connaissant le répertoire d'installation des produits ( *<appli_XXX>* ) ::

  <appli_XXX>/salome shell CloudCompare

ou::

  <appli_XXX>/salome shell qgis


Pour CloudCompare sur Calibre 9, on peut aussi, depuis SALOME, utiliser le menu *tools / plugins / Salome shell session*
pour ouvrir un shell dans l'environnement SALOME.

Dans ce shell, la commande *CloudCompare* lance le logiciel.

 
.. only:: html
 
   :ref:`ref_notionsPrealables`

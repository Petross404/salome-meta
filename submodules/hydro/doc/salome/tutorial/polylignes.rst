..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Création, modification de polylignes
#########################################

.. |contour_garonne| image:: /_static/contourGaronne.png
   :align: middle

.. |contour_lit_majeur| image:: /_static/contourLitMajeur.png
   :align: middle

.. |icone_polyligne_xy| image:: /_static/icon_polyline_xy.png
   :align: middle

.. |createPolyligneDialogue1| image:: /_static/createPolyligneDialogue1.png
   :align: middle

.. |insertNewSection| image:: /_static/insertNewSection.png
   :align: middle

.. |createPolyligneDialogue2| image:: /_static/createPolyligneDialogue2.png
   :align: middle

.. |addition_mode| image:: /_static/additionMode.png
   :align: middle

.. |ajoutPointsPolyligne| image:: /_static/ajoutPointsPolyligne.png
   :align: middle

.. |modification_mode| image:: /_static/modificationMode.png
   :align: middle

.. |undoPoly| image:: /_static/undoPoly.png
   :align: middle

.. |modificationPolyligne2| image:: /_static/modificationPolyligne2.png
   :align: middle

Nous allons créer trois lignes en dessinant sur les cartes et la bathymétrie.

* Un contour fermé représentant une portion de la Garonne
  (débordant du domaine d'étude défini plus loin), représenté par la surface bleu foncé ci-dessous :

  |contour_garonne|

* Un autre contour fermé sur une portion du lit majeur du fleuve
  (toujours débordant du domaine d'étude en amont et en aval),
  correspondant au champ de bathymétrie garonne_point_L93.

* Un contour fermé en lignes brisées qui délimitera notre domaine d'étude.

Le contour du lit majeur et du domaine d'étude :

  |contour_lit_majeur|

Dessin des rives du fleuve
==========================

Pour créer une polyligne, nous utilisons le menu contextuel de la rubrique *POLYLIGNES* dans l'arbre de
l'étude SALOME : |icone_polyligne_xy|

La première partie du dialogue de création permet de nommer la polyligne.
Il est important de donner des noms significatifs aux objets, nous pourrons être amenés à en manipuler un grand nombre.

* **remarque** : attention à ne pas utiliser d'espaces ou d'accents pour les noms d'objets : problèmes potentiels
  à différents niveaux dont les scripts Python.
  
Il est toujours possible de renommer les objets après coup :

  |createPolyligneDialogue1|

Les polylignes peuvent comprendre plusieurs sections. Nous n'en utiliserons qu'une seule, il faut la créer :

  |insertNewSection|

Dans le dialogue relatif à la section, nous choisissons une ligne de type spline, fermée et cliquons sur le bouton add.

  |createPolyligneDialogue2|

 * **remarque** : Les polylignes sont de deux types : 

   * *spline* : une seule ligne continue, a dérivée continue, passant par tous les points.
     **On privilégie ce type de ligne pour toutes les lignes courbes, les points de définition ne 
     seront pas repris dans le maillage**, ce qui donne une plus grande souplesse sur le contrôle
     du raffinement de maillage.
 
   * *polyline* : une seule ligne continue, composée de segments droits. Il faut utiliser
     ce type de ligne pour les objets artificiels composés de segments droits, et chaque fois
     que l'on a besoin de lignes brisées. Les points sont conservés dans le maillage.

Une fois la section créée, il faut ajouter des points en passant en mode addition :

  |addition_mode|

Nous cliquons alors sur la carte, en suivant le contour du fleuve. **Il faut commencer à une extrémité
(amont ou aval du domaine)**. Chaque clic dépose un point, et le contour fermé se dessine progressivement.
Il est possible d'annuler un ou plusieurs des derniers points, avec les boutons undo / redo :

  |undoPoly|

 * **remarque** : **Il est important de choisir ou commence une ligne fermée : le point correspondant est le 
   seul qui reste imposé dans le maillage. Dans le cas d'un fleuve, autant mettre ce point hors du domaine.**

  |ajoutPointsPolyligne|

Nous ajoutons des points jusqu'à revenir au point de départ après avoir décrit les deux rives.
Il est inutile de mettre trop de points, sauf à vouloir capturer les détails d'une rive très découpée.
A titre d'exemple, il faut une soixantaine de points pour la portion de fleuve qui nous intéresse.

Quand la saisie est terminée, il faut valider avec *apply and close*.

modification, édition de polyligne
----------------------------------
 
Si le résultat n'est pas satisfaisant, il est possible de modifier la polyligne en utilisant le menu
contextuel de la polyligne. Dans le dialogue qui s'affiche, il faut sélectionner la section, puis
cliquer sur le mode modification :

  |modification_mode|

Il est alors possible de :

 * sélectionner un seul point en cliquant dessus,

 * sélectionner plusieurs points en les encadrant,

 * créer un point intermédiaire en cliquant sur la ligne, entre deux points.

Le ou les points sélectionnés peuvent être déplacés avec un clic-déplacement
(le bouton gauche et maintenu enfoncé sur le déplacement).

Les points restent sélectionnés tant qu'il n'y a pas d'autre clic ou sélection.

Le bouton Undo permet d'annuler les dernières modifications.

Les coordonnées des points sélectionnés sont affichées dans le dialogue. Elles sont éditables.

  |modificationPolyligne2|

En cours d'édition, on voit deux lignes. La ligne noire, précise, n'est pas recalculée à chaque fois.
La ligne rouge est modifiée à chaque action, elle est moins précise, mais redessinée plus vite
(c'est sensible pour les lignes comprenant un grand nombre de points).

Il est possible de recalculer la ligne sans quitter le dialogue : il faut utiliser le bouton *apply*,
puis, si l'on veut poursuivre les modifications, resélectionner la section avec un double clic,
suivi de *OK* ou *Cancel* 

 * **Remarque** : On notera qu'il est possible de changer le type *spline* ou *polyline*, la fermeture,
   en mode édition, avec un double clic sur la section.  

Dessin du contour du lit majeur et du domaine de calcul
=======================================================

Une fois le fleuve dessiné, nous poursuivons avec le dessin du contour de la bathymétrie du lit majeur, garonne_point_L93,
puis la définition du contour du domaine de calcul, qui doit couper les deux autres lignes en amont et en aval.

**Il est préférable que le domaine de calcul coupe le fleuve perpendiculairement à son axe, de façon à
définir correctement les conditions limites amont et aval. Il faut aussi éviter de créer des angles aigus en limite
du domaine, ce qui risquerait de favoriser l'apparition de triangles surcontraints lors du maillage** (ce qui peut se
corriger par une détection et une inversion de diagonale). 
 
.. only:: html
 
   :ref:`ref_exempleInondation`

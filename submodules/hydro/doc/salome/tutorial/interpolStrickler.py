# -*- coding: utf-8 -*-

# ===== Description du cas, a éditer =====
#=========================================

# --- nom du cas dans HYDRO
nomCas = 'garonne_1'

# --- fichier en entrée :fichier med après interpolation en Z, complété par défaut avec le champ 'BOTTOM FRICTION'
fichierMaillage_in = '/tmp/garonne_1F.med'

from salome.hydrotools.interpolS import assignStrickler

assignStrickler(nomCas, fichierMaillage_in)

"""
# --- il est possible de mettre un fichier différent en sortie, un nom de champ différent du défaut ('BOTTOM FRICTION')
# --- output_file_name : optionnel, fichier en sortie : le même (par défaut) ou différent
fichierMaillage_out = fichierMaillage_in
# --- med_field_name : optionnel, nom du champ des coefficients de Strickler, défaut = 'BOTTOM FRICTION'
nomChamp = 'BOTTOM FRICTION'
assignStrickler(nomCas, fichierMaillage_in, output_file_name=fichierMaillage_out, med_field_name=nomChamp)
"""
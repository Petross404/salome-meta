..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Lancement qgis et CloudCompare
#########################################

A partir de SALOME 8, l'installation de qgis et/ou CloudCompare est fournie dans un produit séparé. 
A EDF cela dépend de la plateforme :

* sur Calibre 7 qgis et cloudCompare sont fournis à part,
* sur Calibre 9, cloudCompare est installé avec SALOME, qgis à part.

Connaissant le répertoire d'installation des produits ( *<appli_XXX>* ) ::

  <appli_XXX>/salome shell CloudCompare

ou::

  <appli_XXX>/salome shell qgis


Pour CloudCompare sur Calibre 9, on peut aussi, depuis SALOME, utiliser le menu *tools / plugins / Salome shell session*
pour ouvrir un shell dans l'environnement SALOME.

Dans ce shell, la commande *CloudCompare* lance le logiciel.

#########################################
Aide sommaire qgis
#########################################

On donne ci-dessous quelques indications d’utilisation de qgis.
Il s’agit de quelques fonctions de base concernant l’affichage et le traitement de lignes ou de polygones. 
Pour des utilisations plus avancées, et les notions de manipulation et d’affichage de données géographiques, l’utilisateur se référera à la documentation officielle de qgis <http://www.qgis.org/fr/docs/index.html>
ou à des documentations existantes spécifiques.

Accès internet depuis qgis (spécifique EDF R&D)
===============================================

L'accès internet est nécessaire dans de nombreux usages de qgis, par exemple pour installer des plugins.
Pour passer le proxy, à EDF R&D, il faut dans le menu *settings / options* de qgis, onglet *Network*, cocher l'option
*Use proxy for web access* et prendre *DefaultProxy* comme *Proxy type*.

Il faut par ailleurs avoir défini le proxy dans son environnement. On peut par exemple ajouter les lignes suivantes dans le fichier
*.bashrc* de son compte Linux :

::

  export http_proxy=http://proxypac.edf.fr:3128
  export https_proxy=http://proxypac.edf.fr:3128
  export no_proxy=localhost,.edf.fr

Ouvrir un projet qgis
=====================

Pour ouvrir ou créer un projet qgis, aller dans le menu Project puis sélectionner Open ou New.
Les données SIG sont constituées de plusieurs types de fichiers.


Afficher des Données dans qgis
==============================

Un « Shapefile » ou « Fichier de forme » est le format de stockage des données vectorielles utilisées 
par la plupart des SIG, il est constitué des fichiers suivants :

 * ***.shp** : stocke les entités géographiques. Il s'agit du shapefile proprement-dit.

 * ***.dbf** (DataBaseFile) : stocke les données attributaires (consultable sous Excel).

 * ***.shx** : stocke les index des enregistrements du fichier ".shp".

 * ***.prj**  (recommandé) : stocke la projection associée.

 * ***.sbn**, ***.sbx** (faculatatifs) : stocke des index n'existant qu'après une requête ou une jointure.

 * ***.xml** (facultatif) : stocke les métadonnées relative au shape.

Un fichier de données **vecteur** est constitué de points ou de contours ou tandis qu’un fichier **raster** 
est constitué de pixels, c'est-à-dire qu’il s’agit d’une image comme un fond de plan ou une photo.

Pour charger des données existantes (par exemple des données CorineLandCover), 
cliquer sur le bouton AddVectorLayer dans la barre des boutons à gauche ou sélectionner AddLayer 
puis Add Vector Layer dans le menu Layer.

Pour visualiser vos données, vous trouverez dans le menu « Vue » ou « View » tous les outils nécessaires
à l’exploration par déplacement ou zoom dans vos cartes.

Les déplacements sur la carte sont également accessibles depuis la barre d’icônes où se trouvent 
les mêmes symboles que dans le menu déroulant.

Le « Zoom Full » ou « Zoom sur l’étendue » permet d’élargir la vue à l’ensemble des données du projet.

Le « Zoom to Layer » zoome sur la couche qui est sélectionnée dans la colonne de gauche.

Créer des lignes ou polygones
=============================

Une couche **Line** permet de dessiner un contour, un tracé, une courbe de niveau... 
avec des caractéristiques propres.

La couche **Polygon** est une surface. Ce polygone apparaitra sur la carte et sera représenté 
aussi sous forme d’une ligne dans la table de la couche, qui aura ces propres éléments exemple: 
colonne nombre, couleur, taille...

Pour créer une nouvelle couche de type Line ou Polygon :

 * dans le menu Layer, sélectionner CreateLayer/new Shapefile Layer

 * Sélectionner le type Line oui Polygon dans la boite de dialogue (ajouter un nouveau champ
   en saisissant son nom dans Field si nécessaire »

 * Cliquer sur OK

 * Nommer et renseigner l’emplacement du nouveau fichier shape, cliquer sur enregistrer

 * Pour créer des lignes, passer en mode édition en cliquant sur l’icône représentant un crayon

 * Cliquer sur le bouton Add Feature dans la barre des taches

 * Terminer la ligne ou le polygone par un clic droit

 * Renseigner l’attribut de la ligne (id) dans la fenêtre qui vient d’apparaître, 
   puis cliquer sur OK

 * Réitérer l’opération pour les lignes suivantes.

 * Terminer la mise à jour de la couche en recliquant sur le bouton représentant un crayon (stop editing)

Extraire des lignes ou polygones d’un fichier shape
===================================================

**Cette opération peut être utile, voire indispensable, lorsque les données sont fournies sur un 
territoire plus important que la zone d’étude, à l’instar des données CorineLandCover 
qui sont fournies par département.**

Dans un premier temps, disposer d’un polygone englobant la zone d’étude ou représentant la zone d’étude : 
à saisir soit dans SALOME-HYDRO, soit dans qgis – exemple (Contour.shp)

SI le contour a été saisi dans SALOME_HYDRO comme une polyligne, il faut l’exporter depuis SALOME-HYDRO 
au format shp (Polyline / Export) (Exemple Contour_Etude.shp).

Cette polyligne doit ensuite être transformée en polygone dans qgis :

 * Add Vector Layer : Contour.shp

 * dans « Vector / Geometry Tools » : lancer « Lines to Polygons »
   et transformer Contour.shp en Contour_polygon.shp

Afficher les zones Corine Land Cover du département en cliquant sur « Add Vector Layer » (exemple : CLC12_D086_RGF.shp) 

Extraire les données à l’intérieur d’un contour :

 * Dans qgis : dans « Vector/ Geoprocessing Tools » : lancer « Intersect » et indiquer
   dans « Input vector « : le fichier CLC (exemple :  CLC12_D086_RGF.shp), 
   et dans « Intersect layer » le contour de l’étude transformé en polygone
   (exemple : Contour_polygon.shp). 
   Donner un nom au fichier résultat (exemple CLC12_D086_Extract.shp)

Attention, le découpage des zones de CorineLandCover par le contour d’étude a pu générer des entités
constituées de deux parties. Pour que SALOME-HYDRO puisse reconnaitre toutes les parties celles-ci 
doivent être dans des entités différentes. 

 * Dans qgis : dans « Vector/ Geometry Tools » : lancer « Multipart to single parts »
   avec pour « Input polygon vector layer » le fichier extrait des CLC (exemple CLC12_D086_Extract.shp))
   et en output shapefile un autre nom (exemple : CLC12_D086_Extract_Single_Parts.shp)

 * C’est ce  fichier shp, (CLC12_D086_Extract_Single_Parts.shp) qui pourra être importé dans SALOME-HYDRO
   en tant que LandCoverMap : « Import Land Cover Map from files »

.. only:: html
 
   :ref:`ref_outilsComplementaires`


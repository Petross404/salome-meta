..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Import des images et fonds de plan
#########################################

.. |Hydro_module_button| image:: /_static/HYDRO.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |Hydro_module_launch| image:: /_static/LaunchHYDRO.png
   :align: middle

.. |import_image| image:: /_static/importImage.png
   :align: middle

.. |import_image2| image:: /_static/importImage2.png
   :align: middle

.. |selection_A| image:: /_static/selectionA.png
   :align: middle

.. |selection_B| image:: /_static/selectionB.png
   :align: middle

.. |selection_B2| image:: /_static/selectionB2.png
   :align: middle

.. |fit_all| image:: /_static/fitall.png
   :align: middle

.. |import_image_B| image:: /_static/importImageB.png
   :align: middle

.. |deux_images| image:: /_static/deuxImages.png
   :align: middle

.. |zoom_style| image:: /_static/zoomStyle.png
   :align: middle

Chargement du module HYDRO
==========================

Pour activer le module HYDRO, il faut le sélectionner dans la liste défilante des modules 
ou cliquer sur son icône dans le bandeau : |Hydro_module_button|.

  |Hydro_module_launch|


Import et géoréférencement d'une première image
===============================================

Pour construire le contour de la rivière, nous allons importer plusieurs images.
Ici il s'agit de cartes que l'on peut télécharger à partir de sites comme `Géoportail`_ 
ou le site de l'IGN. Géoportail offre la possibilité de trouver les coordonnées de points dans
une image, nécessaires au géoréférencement. 

.. _Géoportail: http://www.geoportail.gouv.fr/accueil

Pour importer une image, nous utilisons le menu contextuel (clic droit) de la rubrique *IMAGES* dans l'arbre de
l'étude SALOME: 

  |import_image|
  
La première image se trouve dans le répertoire d'installation de l'application SALOME HYDRO ::

  <appli_xxx>/bin/salome/test/HYDRO/garonne_ign_01.png

L'image est affichée, avec deux points *A* et *B* qui vont être repositionnés à des emplacements
dont on connaît les coordonnées dans le repère Lambert 93, choisis assez éloignés l'un de l'autre
pour garder une bonne précision.

  |import_image2|
 
Voici les deux points particuliers dont on a préalablement repéré les coordonnées avec Géoportail :

  |selection_A|

coordonnées (471562, 6367750): un point en dessous à gauche de la marque *92*.

  |selection_B|

coordonnées (489400, 6377020): le centre de la croix à coté de *Hautevignes*.

 * **remarque** : Pour contrôler le zoom sur la carte, on utilise la touche <Ctrl> avec le bouton gauche 
   de la souris. Pour se déplacer sur la carte, il faut utiliser la touche <Ctrl> avec le bouton du milieu 
   de la souris.

Nous sélectionnons successivement le point A, puis le point B, en utilisant les boutons 
*Activate point A selection* puis *Activate point B selection*.
Après avoir sélectionné les deux points, le dialogue de géoréférencement ressemble à ceci :

  |selection_B2|

Pour les coordonnées x,y de chaque point, la première colonne donne les coordonnées du pixel de l'image, 
la deuxième colonne donne les coordonnées géographiques.

Nous validons le géoréférencement avec le bouton *Apply and Close*.
Il faut s'assurer que l'icône "oeil" à gauche du nom de l'image dans l'arbre est bien active, et recentrer l'image
à l'aide du bouton *fit all* : |fit_all|. 

L'image obtenue est légèrement inclinée vers la droite.

 * **remarque** : dans cette vue, le zoom peut se faire à l'aide de la molette de la souris.
   Le zoom peut être centré sur la position de la souris, ou au centre de l'image. C'est une option accessible dans le menu 
   *File/préférences* sous la rubrique *SALOME*, onglet *3D viewer*, *zooming style* :

  |zoom_style|

Quand on promène la souris sur la carte, les coordonnées du curseur s'affichent dans la barre d'état
en bas à gauche de l'application.

 * **remarque** : il faut prendre l'habitude de **sauver l'étude en cours régulièrement**
   (menu *File/Save* ou *File/Save as*). Il est également conseillé de sauvegarder les différentes
   étapes de l'étude dans des fichiers différents : *il est arrivé, dans certaines circonstances difficiles
   à reproduire, que l'étude soit sauvée de manière incomplète.* Les cas où ce problème est survenu
   correspondent à des rechargements d'études comprenant des données de plusieurs modules, et pour lesquels
   le module HYDRO n'a pas été activé avant la sauvegarde. On se prémunit contre se risque de sauvegarde
   incomplète en activant le module HYDRO avant de sauvegarder l'étude. 

Import d'une deuxième image, géoréférencée à partir la première
===============================================================

La deuxième image est au même endroit que la première ::

  <appli_xxx>/bin/salome/test/HYDRO/garonne_ign_02.png

Pour la géoréférencer, nous utilisons l'option *choose points on the reference image* du dialogue.
Après avoir sélectionné la première image comme référence, nous voyons les deux images l'une en dessus de l'autre.
Nous sélectionnons d'abord un point A au même emplacement sur les deux images, puis un point B.
Il faut prendre deux points les plus éloignés possible, et zoomer suffisamment pour être précis.
Le dialogue ressemble alors à ceci :

  |import_image_B|

Après avoir validé le dialogue, puis affiché les deux images et recentré la vue, nous obtenons :

  |deux_images|

Définition d'un repère local
============================

Le repère local permet la manipulation de petits nombres. **Le changement de repère améliore
beaucoup la précision des étapes de géométrie, de maillage et de calcul**. Notamment, le fait 
de garder des grands nombres peut rendre impossible l'obtention d'un maillage de bonne qualité.

Dans le menu *HYDRO/Change Local CS*, nous saisissons les coordonnées de la nouvelle origine.

Pour notre exemple, nous prenons pour origine le point (430 000, 6 350 000) situé
au sud ouest de notre domaine d'étude.

Après avoir validé le changement de repère, nous constatons que les coordonnées du curseur 
qui s'affichent dans la barre d'état en bas à gauche de l'application sont fournies dans
les deux repères (local et global).

  * **remarque** : ce changement de repère peut être fait plusieurs fois, à différents moments
    de l'étude, car tout ce qui a déjà été importé est transposé par l'application,
    mais il est préférable de le faire assez tôt pour minimiser les risques d'erreur ou d'incohérence,
    et, en tous cas, avant d'exporter des éléments du module HYDRO, notamment la géométrie de calcul.
    **Il est recommandé de fixer le repère local tôt dans l'étude, dès que l'étendue du domaine de calcul
    est connue**.
    
  * Après un changement de repère local, if faut mettre à jour les données de l'arbre qui apparaissent en bleu italique
    avec le menu contextuel *update*, puis recentrer la vue avec la commande *Fit All*.

Dump Python
===========

Toutes les opérations effectuées à l'interface graphique ont leur équivalent en script Python.
On peut faire un dump de l'étude dans un script avec le menu *File/Dump Study*.
Le script Python obtenu permet, en partant d'une étude SALOME vierge, de reconstruire les données
de l'étude. ce script est, bien sûr, éditable.

On peut faire le dump après la définition du repère local, et comparer le fichier obtenu avec ::

  <appli_xxx>/bin/salome/test/HYDRO/h003_changeLCS.py

On pourra vérifier la bonne exécution du dump en repartant d'une étude vierge (redémarrer SALOME,
*new document*, ou seulement *new document*), puis menu *File/Load Script* et activation du module HYDRO.

.. only:: html

   :ref:`ref_exempleInondation`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

####################################
Conseils, pièges à éviter
####################################

Ce paragraphe sera repris en fonction des retours utilisateur.


- chevauchement d'objets ou lignes très proches qui vont conduire à un maillage laid ou trop fin. 

- trop de points dans les splines, quand ils ne sont pas nécessaires. 

- tout ce qui va conduire à une complexité inutile de la partition lors de la création du cas,
  et empêcher un mode automatique de regroupement des zones en régions.

- changement de repère : il est préférable de définir un repère local au plus tôt. cf. tutoriel.

- Les deux systèmes de coordonnées apparaissent, mais il est préférable que la transition de l’un à l’autre
  se fasse simplement (on arrondit les coordonnées de la nouvelle origine).

- cohérence des systèmes de coordonnées des données importées : par exemple tout mettre en Lambert 93 avant import.

- angles aigus aux limites du domaines : risque de création de triangles surcontaints, ce qui implique de les détecter 
  et d'inverser des diagonales.
  
- traiter les nuages de points trop volumineux avant de les importer dans SALOME-HYDRO, par exemple en utilisant 
  l'outil *cloudCompare* pour faire une décimation du nuage selon la courbure de celui-ci.
  
- enregistrer très régulièrement l'étude, en la renommant de façon à conserver des étapes successives.
  Cela peut s'avérer très utile en cas d'erreur intempestive...
  
- ne pas utiliser de caractères spéciaux (espaces, accents...) dans les noms des objets.

- donner des noms significatifs aux différents objets (ils sont vite très nombreux dans une étude réaliste),
  aini qu'aux hypothèses de maillage.
  
- il est important de bien choisir où commence une ligne fermée de type spline :
  Ce premier point reste présent jusqu'au maillage, et peut poser une contrainte inutile si il est mal placé.
  Dans le dessin d'un lit mineur, on fait généralement un contour fermé débortdant du domaine d'étude. Il est judicieux
  de mettre ce premier point hors du domaine.
  
- dans la construction d'un domaine d'étude contenant un fleuve, il faut veiller à couper celui-ci perpendiculairement
  à son axe, pour pouvoir définir des conditions aux limites correctes.

- utilisation du type spline ou du type polyline ?

   * *spline* : une seule ligne continue, a dérivée continue, passant par tous les points.
     On privilégie ce type de ligne pour toutes les lignes courbes, les points de définition ne 
     seront pas repris dans le maillage, ce qui donne une plus grande souplesse sur le contrôle
     du raffinement de maillage.
 
   * *polyline* : une seule ligne continue, composée de segments droits. Il faut utiliser
     ce type de ligne pour les objets artificiels composés de segments droits, et chaque fois
     que l'on a besoin de lignes brisées. Les points sont conservés dans le maillage.
 
   * Les deux types de lignes peuvent être combinés : voir le chapitre sur la manipulation de polylignes.
   
.. only:: html
 
   :ref:`ref_notionsPrealables`

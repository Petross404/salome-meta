rm -rf backup out
mkdir backup
mkdir out
cp -p _static/*.png backup
for file in `ls backup`; do
  convert -units PixelsPerInch -density 226 backup/$file out/$file
  echo $file
done

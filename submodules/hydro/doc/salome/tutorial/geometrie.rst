..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Étape géométrique
#########################################

.. |publieGeom| image:: /_static/publieGeom.png
   :align: middle

.. |activeGeom| image:: /_static/activeGeom.png
   :align: middle

.. |facesGeom| image:: /_static/facesGeom.png
   :align: middle

.. |contoursGeomLitMineurDomaine| image:: /_static/contoursGeomLitMineurDomaine.png
   :align: middle

.. |creeGroupeGeom| image:: /_static/creeGroupeGeom.png
   :align: middle

Quand le cas de calcul est prêt, il faut l'exporter dans le module de géométrie, GEOM,
à l'aide du menu contextuel sur le nom du cas de calcul : *Export Calculation Case*.

L'entrée *HYDRO_Garonne_1* apparaît dans l'arbre de l'étude, sous *Geometry*.

  |publieGeom|

Nous activons le module GEOM, soit dans le menu Déroulant sous la barre de menus,
soit via l'icône de la barre des modules, soit via le menu contextuel *Activate Geometry Module*
sous le nom de l'objet *HYDRO_Garonne_1* dans l'arbre.

  |activeGeom|

En développant l'objet *HYDRO_Garonne_1*, nous retrouvons plusieurs groupes créés automatiquement
lors de la création du cas de calcul: les trois faces correspondant aux régions, de même nom,
ainsi que groupes d'edges, *garonne_1_litMineur_Outer* et *garonne_1_domaineEtendu_Outer*.

Pour voir les groupes de faces, nous les sélectionnons et *show only* dans le menu contextuel.
La commande *auto color* du menu contextuel de *HYDRO_Garonne_1* permet de différentier les faces.

  |facesGeom|

Les deux groupes d'edges représentent les rives du lit mineur et le contour du domaine.

  |contoursGeomLitMineurDomaine|

Pour réaliser le maillage, nous avons besoin d'identifier les deux sections amont et aval du lit mineur,
sous formes de groupes d'edges.
Pour créer des groupes dans un objet, il est préférable de commencer par l'afficher seul (*show only*).

Nous créons pour cela un groupe de type *edges*, de nom *SectionsGaronne*, contenant les deux extrémités du lit Mineur.

Nous utilisons la commande *Create Group* dans le menu contextuel.
La boite de dialogue permet de créer 4 types de groupes : points, edges, faces, volumes.
Nous sélectionnons le type *edges* : 2ème bouton radio dans la rubrique *Shape Type*.

Nous nommons le groupe : *SectionsGaronne*.

Nous sélectionnons les deux extremités du lit mineur dans la vue graphique.
Pour sélectionner plusieurs éléments, il faut utiliser la touche *<Shift>*.
En appuyant sur le bouton *add*, les numéros des  sous géométries (sub-Shape) apparaissent dans le dialogue.

Il faut valider par *Apply and Close*. 

  |creeGroupeGeom|

Ces groupes apparaissent dans l'arbre d'étude, sous l'objet *HYDRO_Garonne_1*.

Nous créons également deux groupes de type *edges*,de nom *aval* et *amont* pour distinguer les deux types de conditions limites,
en utilisant les mêmes sous géométries, séparément.

Il faut de même créer deux groupes d'edges pour fermer le contour. nommés *bordDroiteDomaine* et *bordGaucheDomaine*, contenant
les frontières continues du domaine, coté rive droite et côté rive gauche.

Nous pouvons maintenant réaliser le maillage de cette géométrie.
 
.. only:: html
 
   :ref:`ref_exempleInondation`

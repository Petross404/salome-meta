..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Streams
#########################################

.. |exemple_profil| image:: /_static/exemple_profil.png
   :align: middle

.. |profilsEtLignedo| image:: /_static/profilsEtLignedo.png
   :align: middle

.. |iconeImportSinusX| image:: /_static/iconeImportSinusX.png
   :align: middle

.. |menuImportSinusX| image:: /_static/menuImportSinusX.png
   :align: middle

.. |georeferencementProfiles| image:: /_static/georeferencementProfiles.png
   :align: middle

.. |profilsIgnores| image:: /_static/profilsIgnores.png
   :align: middle

.. |createStream| image:: /_static/createStream.png
   :align: middle

.. |objetStream| image:: /_static/objetStream.png
   :align: middle

Les objets Stream permettent de définir des cours d'eau à l'aide d'une succession de profils.
Ces profils sont des coupes verticales en travers faites à intervalles réguliers le long du cours d'eau.

  |exemple_profil|

Pour reconstituer le cours d'eau, il faut une succession de profils (avec leur géoréférencement), et une
ligne définissant l'axe du cours d'eau. Cette ligne permet d'ordonner les profils.

  |profilsEtLignedo|

Nous importons une série de profils géoréférencés ainsi qu'une polyligne XY définissant la ligne d'eau,
au format SinusX (ou au format xyz).
La polyligne n'a pas besoin d'être très précise, dans la mesure où elle coupe les profils.

L'import de fichiers au format SinusX se fait à partir de la barre de menu *HYDRO*, commande *Import from SinusX*
ou du bouton correspondant dans la barre d'outils du module.

  |iconeImportSinusX|

  |menuImportSinusX|

Nous importons successivement les fichiers *lignedo.sx* et *garonne_profiles.sx*
qui se trouvent dans le répertoire d'installation de l'application SALOME HYDRO ::

  <appli_xxx>/bin/salome/test/HYDRO

La ligne d'eau apparaît dans la rubrique *POLYLINES* et la série de profils se retrouve
dans la rubrique *PROFILES* de l'arbre de l'étude.


Les profils sont nommés individuellement, et éditables.

  |georeferencementProfiles|

Pour créer l'objet Stream, il faut utiliser la commande *Create stream* du menu contextuel de la rubrique
*NATURAL OBJECTS*.

Nous nommons le stream *garonne_stream*, sélectionnons la polyligne *lignedo* et récupérons la série de profils
précédemment importés (il faut les sélectionner dans l'arbre d'étude et utiliser le bouton *Add*
du dialogue *create stream*.

La ligne d'eau ne coupe pas tous les profils, il y en en a en amont et en aval, ils vont être ignorés.

  |profilsIgnores|

Pour permettre le calcul de la bathymétrie, un nuage de points est généré par interpolation entre les profils,
sous forme d'une série de profils intermédiaires constitués de points.

Le paramètre *Stream spatial step* donne la distance entre ces profils intermédiaires.

Le paramètre *Stream ddz* donne le pas d'échantillonage vertical pour la discrétisation des profils.

  |createStream|

Il faut valider avec *Apply and Close*. L'objet Stream apparait dans la vue et dans la rubrique
*NATURAL OBJECTS* sous le nom donné précedemment, *garonne_stream*

  |objetStream|
  
Le nuage de point associé apparaît dans la rubrique *BATHYMETRIES* sous le nom *garonne_stream_Altitude_1*.

.. only:: html
 
   :ref:`ref_casParticuliers`


..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Objets Artificiels
#########################################

.. |axeDigue| image:: /_static/axeDigue.png
   :align: middle

.. |creationDigue_1| image:: /_static/creationDigue_1.png
   :align: middle

.. |modifModeProfile| image:: /_static/modifModeProfile.png
   :align: middle

.. |creationDigue_2| image:: /_static/creationDigue_2.png
   :align: middle

.. |creationDigue_3| image:: /_static/creationDigue_3.png
   :align: middle

.. |altitudeDigue| image:: /_static/altitudeDigue.png
   :align: middle

.. |polyline3D| image:: /_static/polyline3D.png
   :align: middle

.. |artificialDigue| image:: /_static/artificialDigue.png
   :align: middle

.. |zoomDigue| image:: /_static/zoomDigue.png
   :align: middle

Les objets artificiels regroupent les digues et les canaux. Ces deux types d'objets
sont construits sur le même principe :

 * une section verticale constante,

 * un axe 3D, c'est à dire une polyligne 3D selon laquelle la section est extrudée.

L'axe 3D est construit à partir d'une polyligne dans le plan horizontal, à laquelle
on associe une ligne d'altitude définie dans un plan vertical, qui donne la cote Z
en fonction de l'abscisse curviligne de la polyligne horizontale.

La ligne d'altitude est soit construite explicitement, soit obtenue en projetant
la polyligne horizontale sur un champ de bathymétrie.

Lors du calcul de l'altitude aux noeuds du maillage (interpolation en Z), les noeuds situés sur la digue
sont traités spécifiquement : l'altitude du noeud est prise sur la forme géométrique 3D de
la digue telle qu'elle a été définie ci-dessus.

Création d'une digue
====================

Pour définir l'axe horizontal de la digue, nous créons une polyligne non fermée, de type spline,
avec l'éditeur de polyligne (menu contextuel de la rubrique *POLYLIGNES* dans l'arbre de
l'étude SALOME).

Nous nommons cette polyligne *axeDigue*.

  |axeDigue|

La section de la digue se crée avec le menu contextuel *Create profile* de la rubrique *PROFILES* 
dans l'arbre de l'étude SALOME.

Nous nommons cette section *sectionDigue*.

Il faut dessiner à main levée la forme approximative de la section, nous l'éditerons ensuite 
pour mettre des cotes précises.

  |creationDigue_1|

Pour corriger les coordonnées des noeuds, il faut se placer en mode Modification :

  |modifModeProfile|

Les noeuds peuvent être sélectionnés en bloc en les englobant dans un rectangle, dans la vue graphique.

Ils s'affichent dans un tableau au dessus de la vue graphique.

En cliquant sur le titre de la colonne *index*, on réordonne les noeuds.

  |creationDigue_2|

Pour le choix des valeurs Z, il faut savoir que l'altitude finale d'un point de la digue s'obtient
à partir de la section en ce point, en additionnant la cote du point sur la section à la
cote de la section le long du profil d'altitude.
Plus précisément, le calcul est le suivant :

 * le point **A** (x,y,0) dont on veut l'altitude est projeté sur la courbe *axeDigue*
   en un point *P* (x',y',0) tel que la droite *AP* est normale à la tangente en **P**
   à l'axe de la digue. Le point **P** est à une distance horizontale 
   *d* = distance(A,P) de la courbe *axeDigue*.

 * A partir de la coordonnée curviligne de **P** sur *axeDigue*, on récupère une valeur de Z0
   sur la ligne d'altitude. Cette valeur Z0 correspond à la cote 0 de la section.

 * Pour obtenir l'altitude finale en **P**, on calcule la cote Z1 sur la section au point d'abscisse *d*. 
   L'altitude finale est Z = Z0 +Z1.

 * **remarque** : ce mode de calcul suppose une section symétrique par rapport à x=0. 

Ici, la ligne d'altitude que nous allons définir correspond au sommet de la section.
Nous créons donc une section symétrique de 20 mètres de largeur, à flancs assez raides.

  |creationDigue_3|

Il reste à créer la ligne d'altitude de la digue. Nous allons la définir explicitement
avec l'altitude des deux extrémités.

Si l'altitude de la digue est variable, il faut avoir une idée approximative de sa longueur,
pour construire un profil d'altitude précis (il manque une fonction d'affichage de la longueur des polylignes).

Si l'on définit plus de deux points, l'altitude est interpolée linéairement entre deux points,
et, si la courbe est plus longue que la ligne d'altitude, les valeurs de Z au delà du dernier
point sont prises à la cote de ce dernier point.

  |altitudeDigue|

Il faut ensuite créer l'axe 3D de la dique à l'aide du menu contextuel *Create polyline 3D*
de la rubrique *POLYLINES 3D*.

  |polyline3D|

Enfin, on utilise le menu contextuel *Create digue* (in french in the text!)
de la rubrique *ARTIFICIAL OBJECTS*.

  |artificialDigue|

Le paramètre equidistance sert lors de l'extrusion précise de la section le long de l'axe.

 * Si la valeur est trop grande par rapport au rayon de courbure de l'axe, la section est décentrée 
   lors de son extrusion.

 * Si la valeur est trop faible, le temps de calcul devient long.

On peut prendre une valeur de l'ordre de deux ou trois fois la largeur de la digue, en première
approximation.

Pour contrôler le résultat, il faut superposer la vue de la digue et son axe.

  |zoomDigue|

Lors de la constitution du cas de calcul avec la digue, il faut isoler la digue dans une région spécifique,
parce que l'on va la mailler de préférence en quadrangles avec l'algorithme *Quadrangle (Medial Axis projection)*.

Pour le calcul d'altitude, comme la digue se superpose à un terrain naturel, l'option ZMAX est la plus logique dans le cas général.


Création d'un canal
===================

Le canal se construit exactement comme la digue, avec simplement un menu spécifique *Create channel*
dans la rubrique *ARTIFICIAL OBJECTS*. 

La section a une forme de cuvette, et le calcul d'altitude se fait
selon la même logique que pour la digue.

Lors de la constitution du cas de calcul avec le canal, il faut isoler le canal dans une région spécifique,
car il sera maillé de préférence en quadrangles avec l'algorithme *Quadrangle (Medial Axis projection)*.

Pour le calcul d'altitude, le canal s'inscrivant dans un terrain naturel,
l'option ZMIN est la plus logique dans le cas général.

.. only:: html
 
   :ref:`ref_casParticuliers`

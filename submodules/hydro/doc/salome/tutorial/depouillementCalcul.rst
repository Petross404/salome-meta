..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Dépouillement du calcul TELEMAC
#########################################


.. |paravisDansSalome| image:: /_static/paravisDansSalome.png
   :align: middle

.. |hydraulicsFilters| image:: /_static/hydraulicsFilters.png
   :align: middle

.. |paravis02| image:: /_static/paravis02.png
   :align: middle

.. |paravis03| image:: /_static/paravis03.png
   :align: middle

.. |paravis04| image:: /_static/paravis04.png
   :align: middle

.. |paravis05| image:: /_static/paravis05.png
   :align: middle

.. |paravis06| image:: /_static/paravis06.png
   :align: middle

.. |paravis07a| image:: /_static/paravis07a.png
   :align: middle

.. |paravis07| image:: /_static/paravis07.png
   :align: middle

.. |paravis08| image:: /_static/paravis08.png
   :align: middle

.. |paravis09| image:: /_static/paravis09.png
   :align: middle

.. |paravis10| image:: /_static/paravis10.png
   :align: middle

.. |paravis11| image:: /_static/paravis11.png
   :align: middle

.. |paravis12| image:: /_static/paravis12.png
   :align: middle

.. |paravis13| image:: /_static/paravis13.png
   :align: middle

.. |paravis14| image:: /_static/paravis14.png
   :align: middle

.. |paravis15| image:: /_static/paravis15.png
   :align: middle

.. |paravis16| image:: /_static/paravis16.png
   :align: middle

.. |paravis17| image:: /_static/paravis17.png
   :align: middle

.. |paravis18| image:: /_static/paravis18.png
   :align: middle

.. |paravis19| image:: /_static/paravis19.png
   :align: middle

.. |paravis20| image:: /_static/paravis20.png
   :align: middle

.. |paravis21| image:: /_static/paravis21.png
   :align: middle

.. |paravis22| image:: /_static/paravis22.png
   :align: middle

.. |paravis24| image:: /_static/paravis24.png
   :align: middle

.. |paravis25| image:: /_static/paravis25.png
   :align: middle

.. |paravis26| image:: /_static/paravis26.png
   :align: middle

.. |paravis27| image:: /_static/paravis27.png
   :align: middle

.. |paravis28| image:: /_static/paravis28.png
   :align: middle

.. |paravis29| image:: /_static/paravis29.png
   :align: middle

.. |paravis30| image:: /_static/paravis30.png
   :align: middle

.. |paravis31| image:: /_static/paravis31.png
   :align: middle

.. |paravis32| image:: /_static/paravis32.png
   :align: middle

.. |paravis33| image:: /_static/paravis33.png
   :align: middle

.. |paravis34| image:: /_static/paravis34.png
   :align: middle

.. |paravis35| image:: /_static/paravis35.png
   :align: middle

.. |paravis36| image:: /_static/paravis36.png
   :align: middle

Paravis correspond à l'adaptation du produit open source Paraview à SALOME.

Paravis peut être utilisé de plusieur façons. Soit comme module depuis l'interface graphique de SALOME,

  |paravisDansSalome|

soit en tant qu'outil autonome.


Lancement de Paraview en mode autonome
======================================

Installer la dernière version de SALOME. Sur l'intranet EDF : ::

  ftp://ftp.pleiade.edf.fr/projets/salome/Releases/

Une fois SALOME installé, 2 dossiers sont créés :

* appli_xxx
* Salome_xxx

Se placer dans le dossier appli_xxx et Lancer Paraview par le salome shell de SALOME.

La commande est : ::

  <chemin appli_xxx>/salome shell paraview

Exemple ::

  ~/salome-hydro/SALOME_DEV/14-10-2018/appli_DEV/salome shell paraview

Paraview s’ouvre.

Filtres utiles pour l'hydraulique
=================================

Charger un fichier résultat : *File / Open*.

Cliquer sur *Apply*.

Aller dans *Filters / Hydraulics*. C’est là que sont répertoriés les filtres dédiés à l’hydraulique tels que :

* Profil spatial `Filtre Spatial Profile`_
* :ref:Profil temporel `Filtre Temporal on Point`_
* :ref:Calcul de débit à travers une section `Filtre Rate Of Flow Through Section`_
* :ref:Calcul de sédiments érodés / déposés dans une zone `Filtre Sediment Deposit`_
* :ref:Evolution temporelle d’une variable dans une colonne d’eau `Filtre Depth Vs Time On Point`_ (en un x et y fixé,
  évolution d’une variable comme la température, par exemple, en fonction du temps
  en abscisse et de la profondeur en ordonnées)


 |hydraulicsFilters|

Filtre Temporal on Point
------------------------

Charger un fichier résultat.

Sélectionner le filtre Temporal On Point.

Pour l’instant, se placer en 3D. Il existe un bug pour déplacer la croix si on reste en 2D.

Déplacer la croix blanche à l’endroit souhaité. La croix blanche est située initialement au centre de la boîte encadrant le modèle.

  |paravis02|

Jouer sur le Radius (encadré en rouge) pour diminuer le rayon de la sphère en fonction de la taille des mailles.

  |paravis03|

Cliquer sur *Apply*.

Sélectionner la variable à afficher, par exemple Free surface.

  |paravis04|

* **Astuce :** La croix blanche a disparu à ce moment sur la fenêtre de gauche RenderView.
  Pour la faire réapparaître, se placer sur la fenêtre RenderView montrant le fichier résultat.

Filtre Spatial Profile
----------------------

Charger un fichier résultat.

Lancer le filtre Spatial Profile : une ligne jaune apparaît, diagonale de la boîte encadrant le modèle.

  |paravis05|

Déplacer les 2 extrémités de cette ligne à l’endroit où l’on souhaite commencer la polyligne :
pour cela, placer la souris où l’on souhaite déplacer l’extrémité sur le fichier résultat et taper 1 ou Ctrl+1. Faire de même pour la deuxième extrémité en faisant 2 ou Ctrl+2.

  |paravis06|

Adapter l’échelle de couleur si besoin : pour cela se placer sur le fichier résultat et cliquer sur :

  |paravis07a|

  |paravis07|

Se replacer sur le filtre SpatialProfile1.

  |paravis08|

Utiliser les commandes de l’encadré rouge :
on conseille d’utiliser la dernière commande *Alt + Left click* pour ajouter des points à la fin de la polyligne.

  |paravis09|

  |paravis10|

Et ainsi de suite jusqu’à l’obtention de la ligne souhaitée.

Cliquer sur *Apply*.

Sélectionner la variable souhaitée, par exemple *Free Surface*.
Dans **X Axis Parameters**, décocher *Use Index For Axis* et sélectionner *Curv Abscissa* dans *X Array Name*.

  |paravis11|

* **Astuce :** La polyligne a disparu à ce moment sur la fenêtre de gauche RenderView.
  Pour la faire réapparaître, se placer sur la fenêtre RenderView montrant le fichier résultat et cliquer sur *Show Spline*.

  |paravis12|

Penser à **sélectionner l’option Subdivide Input Polyline**. Elle permet d’avoir une interpolation plus ou moins précise entre les points que l’on a cliqués à l’écran. C’est à l’utilisateur de bouger le curseur de façon à ce que la courbe ne change plus. 50 suffit la plupart du temps mais il faut vérifier qu’il n’y a plus de changement pour des valeurs supérieures (dans certains cas c’est 400).

  |paravis28|

* **Remarque :**

Un exemple de comparaison pour un profil en travers d’un cours d’eau si on ne sélectionne pas Subdivide Input Polyline (à gauche) et si on le sélectionne et le met égal à 50 (à droite) :

  |paravis29|

  |paravis30|

  |paravis31|

Poly Line Source
--------------------------------

Les trois filtres qui suivent peuvent être appliqués sur des couches shape, sinusx mais aussi sur des polylines qui seraient dessinées à main levée sur le fichier résultat.

Pour cela, aller dans Sources et sélectionner Poly Line Source.

  |paravis32|

Voici ce qu’on obtient une fois la source Poly Line Source sélectionnée :

  |paravis33|

  |paravis34|

Par défaut la polyline est placée en 0, 0. Ce qui, dans la plupart des cas, sera très loin de notre fichier résultat. Pour ramener la polyline facilement sur le fichier résultat, utiliser les 2 commandes suivantes :

* Déplacer les 2 extrémités de cette ligne à l’endroit où l’on souhaite commencer la polyligne : pour cela, placer la souris où l’on souhaite déplacer l’extrémité de la ligne sur le fichier résultat et taper 1 ou Ctrl+1.
* Faire de même pour la deuxième extrémité en faisant 2 ou Ctrl+2.

Puis, pour dessiner la polyline :

* Sélectionner un des deux points extrémité de la ligne
* Alt + Left Clic permet de rajouter des points à la suite de l’extrémité sélectionnée préalablement.
* Ctrl + Left click permet d’insérer un nouveau point sur la ligne au niveau de la position de la souris.
* Shift + Left Click permet d’enlever un point au niveau de la position de la souris.

  |paravis35|

Cliquer sur *Apply*.

On peut ensuite lui appliquer les filtres Spatial Profile With Source ou Sediment Deposit ou Rate Of Flow Through Section. Pour Sediment Deposit, penser à fermer la polyline en cochant Closed (entouré en rouge sur la Figure ci-dessus).


Filtre Spatial Profile With Source
----------------------------------

Charger un fichier résultat.

Cliquer sur *Apply*.

Charger une polyligne déjà créée. Cette polyligne peut avoir 2 formats : shape ou sinusx (créée dans Qgis, par exemple, au format shape).

Cliquer sur *Apply*.

Si la polyligne est déjà dans le même référentiel local que le modèle, elle s’affiche automatiquement.

  |paravis13|

Penser aux possibles translations qui ont pu être effectuées lors de la construction du modèle.
Si la polyligne est construite dans Qgis sur fond de carte, elle sera dans le référentiel global.
Il va donc falloir la translater dans Paraview.

Pour cela une fois la polyligne chargée, sélectionner le filtre *Transform* et renseigner
la translation dans l’encadré rouge.
Une boîte s’affiche, on peut la faire disparaître en décochant *Show box* (dans l’encadré rouge).

  |paravis14|

Lancer le filtre *Spatial Profil With Source*.

Une fenêtre s’ouvre :

* Renseigner dans un premier temps l’input : il s’agit du fichier résultat.
* Renseigner dans un deuxième temps la source : il s’agit de la polyligne (chargée (ou de sa translation s’il y a lieu) ou dessinée).

  |paravis15| |paravis16|

Cliquer sur *OK*.

Cliquer sur *Apply*.

Sélectionner la variable que l’on souhaite visualiser, exemple ci-dessous la surface libre.

  |paravis17|

* **Remarque :** Comme pour le filtre Spatial Profile, il est important de cocher Subdivide Input Polyline (qui apparaît une fois le filtre activé et sélectionné) afin d’avoir une interpolation précise. La valeur conseillée est 50 (mais ne pas hésiter à jouer sur la valeur pour vérifier que la courbe ne bouge plus). La courbe traçant la variable sera alors plus précise, plus détaillée.

Pour cela, sélectionner la polyligne dans *Pipeline Browser* et cocher *Subdivide Input Polyline*.

  |paravis18|

Cliquer sur *Apply*.

  |paravis19|

L’interpolation est plus précise.

On peut également charger des couches shape comprenant plusieurs polylines, ce qui permettra de comparer des profils en travers par exemple :

  |paravis36|

* **Astuce :** Si vous avez une polyline au format SinusX et qu’elle est chargée sans message d’erreur dans PARAVIEW mais que vous ne la voyez pas, pensez à utiliser la commande suivante : ::

    sed -i 's/\r//g'  nom_du_fichier

Elle permet de Transformer un fichier au format SinusX créé sous Windows en format SinusX lisible sous Linux.


Filtre Rate Of Flow Through Section
-----------------------------------

Charger un fichier résultat.

Cliquer sur *Apply*.

Charger un fichier polyligne décrivant une section (à travers laquelle on souhaite connaître le débit transitant).
Cette polyligne peut avoir 2 formats : shape ou sinusx.

Ou dessiner directement sur le résultat une polyline en utilisant Sources / Poly Line Source.

Cliquer sur *Apply*.

Lancer le filtre *Rate Of Flow Through Section*.

Une fenêtre s’ouvre :

* Renseigner dans un premier temps l’input : il s’agit du fichier résultat.
* Renseigner dans un deuxième temps la source : il s’agit de la polyligne (chargée ou dessinée) décrivant
  la section à travers laquelle on souhaite connaître le débit transitant.

  |paravis20| |paravis21|

Cliquer sur *OK*.

Cliquer sur *Apply*.

  |paravis22|

Filtre Sediment Deposit
-----------------------

Charger un fichier résultat.

Cliquer sur *Apply*

Charger un fichier contour (polygone au niveau duquel on souhaite connaître les évolutions du fond (érosion / dépôt)).
Ce polygone est au format shape.

Ou dessiner directement sur le résultat une polyline en utilisant Sources / Poly Line Source. Penser à la fermer.

Cliquer sur *Apply*.

Lancer le filtre *Sediment Deposit*.

Une fenêtre s’ouvre :

* Renseigner dans un premier temps l’input : il s’agit du fichier résultat.
* Renseigner dans un deuxième temps la source : il s’agit du polygone (chargé ou dessiné) décrivant le contour au niveau
  duquel on souhaite connaître les évolutions du fond.

  |paravis24| |paravis25|

Cliquer sur *OK*.

Cliquer sur *Apply*.

  |paravis26|

On a alors 3 courbes tracées que l’on peut sélectionner comme on le souhaite :

  |paravis27|

* Négatif = érosion
* Positif = Dépôt
* Total = Somme des deux

Filtre Depth Vs Time On Point
-----------------------------

Description à venir.

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tutoriel SALOME HYDRO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. |Hydro_module_button| image:: /_static/HYDRO.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |hydrosolver_module_button| image:: /_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

SALOME-HYDRO est une plateforme métier dédiée aux études d'hydraulique à surface
libre, avec les codes TELEMAC et MASCARET. La plateforme s'appuie sur les modules
génériques de SALOME (géométrie, maillage, supervision de calculs, post traitement...)
auxquels ont été ajoutés deux modules spécifiques du métier :

  * HYDRO |Hydro_module_button| : acquisition des données de terrain (contours, bathymétrie...),
    construction de la géométrie du cas de calcul, identification des zones de
    condition limite...

  * HYDROSolver |hydrosolver_module_button| : définition des paramètres physiques et
    numériques du cas de calcul, pilotage du calcul.

Le tutoriel traite l'ensemble des étapes d'une étude d'inondation liée à une crue
de fleuve. L'exemple est tiré d'un cas réel pour lequel beaucoup de données sont disponibles.

.. _ref_installationLancementSalome:

###################################
Introduction à SALOME
###################################

Dans cette partie, on rappelle comment installer et exécuter SALOME.

.. toctree::
   :numbered:
   :maxdepth: 3

   installationLancementSalome.rst

.. _ref_notionsPrealables:

###################################
Quelques notions préalables
###################################

On décrit ici les différentes étapes d'une étude SALOME-HYDRO, en introduisant les concepts nécessaires.
Cette partie contient également une série de recommandations et de pièges à éviter.

L'exercice décrit plus bas permet de découvrir ces concepts par la manipulation.

.. toctree::
   :numbered:
   :maxdepth: 3

   preliminaires.rst
   piegesAEviter.rst
   donneesPrealables.rst


.. _ref_outilsComplementaires:

###################################
Des outils complémentaires à SALOME
###################################

Les outils qgis et CloudCompare sont fournis à coté de SALOME-HYDRO.

.. toctree::
   :numbered:
   :maxdepth: 3

   introQgis.rst
   introCloudCompare.rst

.. _ref_exempleInondation:

###################################
Un exemple simple d'inondation
###################################

Cet exercice couvre complètement une étude d'inondation sur un problème simple.

.. toctree::
   :numbered:
   :maxdepth: 3

   import.rst
   importBathy.rst
   polylignes.rst
   objetsNaturels.rst
   casCalcul.rst
   geometrie.rst
   maillage.rst
   interpolationZ.rst
   miseEnDonneesTelemac.rst
   lancementCalcul.rst
   depouillementCalcul.rst

.. _ref_casParticuliers:

###################################
Des cas particuliers
###################################

On introduit ici quelques concepts non abordés dans l'exemple simple précédent.

.. toctree::
   :numbered:
   :maxdepth: 3

   streams.rst
   objetsArtificiels.rst
   manipsPolys.rst
   landCoverMap.rst
   completeCase.rst
   copyMesh.rst
   
.. _ref_outilsReprise:

###################################
Des outils pour la reprise d'études
###################################

Il arrive fréquemment que l'on doive reprendre des études, par exemple pour étendre ou modifier un maillage, changer la bathymétrie.
Ces études peuvent avoir été générées avec d'autres outils que SALOME HYDRO.
On suppose que l'on dispose a minima d'un fichier maillage au format MED correspondant à l'étude à reprendre,
contenant éventuellement des groupes (conditions limites ou autres) que l'on souhaite récupérer.

Le script *converter.py* fourni avec Telemac permet de convertir les types de maillages lus par Telemac au format MED.
Il est accessible depuis le shell SALOME. ::

  <appli_XXX>/salome shell
  converter.py --help

.. toctree::
   :numbered:
   :maxdepth: 3

   modifBathy.rst
   changeCoordSystem.rst
   extensionMaillageExistant.rst
   
.. _ref_formatsSpecs:

###################################
Formats et specifications
###################################

On introduit ici quelques formats d'import-export.
La documentation du format SinusX en français correspond à l'usage d'origine du format et donne une transposition pour l'usage SALOME,
**La documentation du format SinusX en anglais donne la spécification précise du format SinusX dans SALOME HYDRO.
C'est cette dernière documentation qui constitue la référence pour SALOME HYDRO.**

.. toctree::
   :numbered:
   :maxdepth: 3

   format_sinusx.rst
   sinusX_Format.rst

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Objets "Naturels", zones immersibles
#########################################

.. |createImmersibleZone| image:: /_static/createImmersibleZone.png
   :align: middle

.. |selectColor| image:: /_static/selectColor.png
   :align: middle

.. |zonesImmersibles| image:: /_static/zonesImmersibles.png
   :align: middle

.. |changeLayerOrder| image:: /_static/changeLayerOrder.png
   :align: middle

.. |zoneSubmersible| image:: /_static/zoneSubmersible.png
   :align: middle


Nous avons créé le contour d'objets dit "naturels", par opposition à des objets "artificiels".
Cette classification correspond au mode de calcul de l'altitude Z en tout point de leur surface.

Les objets naturels correspondent à des surfaces dont la cote Z est déterminée par un champ de bathymétrie/altitude.

Les objets artificiels sont par exemple des digues ou des canaux, qui seront décrits par une section
constante, extrudée le long d'une ligne dont l'altitude pourra éventuellement varier.

Il existe deux sous catégories d'objets naturels, les zones "immersibles" associées à un champ de bathymétrie/altitude
de type nuage de points (.xyz ou .asc) et les zones dites "stream" qui correspondent à des rivières décrites par une série de profils 
en travers. Ces profils constituent un champ de bathymétrie sur lequel on utilisera un mode d'interpolation particulier 
pour calculer la cote Z.

Nous allons créer trois zones immersibles, définies par un contour et un champ de bathymétrie/altitude :

 * le lit mineur, défini par le contour de la *garonne*, et le champ *garonne_point_L93*,

 * le lit majeur, défini par le contour du  champ *garonne_point_L93* et ce même champ,

 * le domaine d'étude, défini par son contour, et le champ *cloud_02*.

Pour créer une zone immersible, nous utilisons le menu contextuel de la rubrique *NATURAL OBJECTS*.

  |createImmersibleZone|

Le dialogue propose de définir le nom de la zone, et de sélectionner le contour et la bathymétrie dans les listes d'objets existant.
Il faut valider avec *apply and close*.

Une fois la zone créée, il est possible de changer sa couleur à l'aide du menu contextuel associé au nom de l'objet.

  |selectColor|

En affichant les trois zones, nous devrions obtenir une vue ressemblant à ceci :

  |zonesImmersibles|

Il faut peut être réordonner l'affichage des différentes couches : dans le menu contextuel de la vue graphique,
il y a une entrée *Change Layer Order* qui permet de définir cet ordre, qui sera mémorisé à la sauvegarde de l'étude :

  |changeLayerOrder|

Dans le menu contextuel associé à chaque zone immersible, il y a un paramètre *Submersible* coché par défaut (icône enfoncée).
Lorsque l'on veut créer une île au sens TELEMAC, c'est à dire une zone non inondable, qui fera un trou dans le maillage,
il faut décocher ce paramètre.

  |zoneSubmersible|

#########################################
Création d'une digue (optionnel)
#########################################

La création de cette digue est optionnelle, le cas est exploitable sans.

Pour créer l'objet *digue*, voir le paragraphe *Objets Artificiels* plus bas.
 
.. only:: html
 
   :ref:`ref_exempleInondation`

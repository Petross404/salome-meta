..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

###############################################
Changement de système de coordonnées, d'origine
###############################################

.. |changes_coords| image:: /_static/changes_coords.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |dialogChangeCoords| image:: /_static/dialogChangeCoords.png
   :align: middle

.. |dialogChangeCoords2| image:: /_static/dialogChangeCoords2.png
   :align: middle


Il peut arriver que l'on récupère un maillage existant dans un système de coordonnées que l'on souhaite changer,
ou simplement avec une origine locale que l'on souhaite décaler.

Dans le module *HYDROSOLVER*, on utilise le dialogue *change Coordinates* accessible depuis le menu *Hydro*
ou l'icone |changes_coords|.

Si on laisse décochée la case *Change coordinates system*, Le dialogue permet une simple translation du maillage,
**sans perte des champs que le fichier peut contenir**. Une fois sélectionné le fichier du maillage d'origine,
le dialogue propose un nom de fichier pour le maillage translaté en ajoutant un suffixe. Il est possible de prendre le nom de son choix.
Il faut indiquer les coordonnées du repère local d'origine, et celles d'arrivée.

  |dialogChangeCoords|

Si l'on coche la case *Change coordinates system*, il est possible de sélectionner les systèmes de coordonnées d'origine et d'arrivée,
dans une liste préétablie. Il est impératif de donner les coordonnées du repère local d'origine pour un fonctionnement correct.

  |dialogChangeCoords2|

.. only:: html
 
   :ref:`ref_outilsReprise`


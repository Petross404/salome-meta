..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#####################################################
Copier un maillage existant sur une autre géométrie 
#####################################################

.. |CopyMesh| image:: /_static/CopyMesh.png
   :align: middle

.. |CopyMesh1| image:: /_static/CopyMesh1.png
   :align: middle

.. |CopyMesh2| image:: /_static/CopyMesh2.png
   :align: middle

.. |CopyMesh3| image:: /_static/CopyMesh3.png
   :align: middle

.. |CopyMesh4| image:: /_static/CopyMesh4.png
   :align: middle

.. |CopyMesh5| image:: /_static/CopyMesh5.png
   :align: middle

.. |CopyMesh6| image:: /_static/CopyMesh6.png
   :align: middle

.. |CopyMesh7| image:: /_static/CopyMesh7.png
   :align: middle

.. |CopyMesh8| image:: /_static/CopyMesh8.png
   :align: middle

On dispose d’une géométrie A et d’un maillage A s’appuyant sur la géométrie A. On souhaite réaliser un maillage B de la géométrie B avec les mêmes hypothèses que le maillage A, sachant que la géométrie B diffère très peu de la A.

Pour avoir la géométrie B, on a utilisé l’option *Complete calculation case* du module *HYDRO*. Une fois le cas de calcul complété, exporté dans GEOM, penser à créer le groupe d’edge correspondant à la polyline dans le module GEOM. La polyline apparaîtra alors dans le maillage dans SMESH et l’on pourra réaliser un sous-maillage s’appuyant dessus. 

  |CopyMesh|

Dans le module SMESH, aller dans *Mesh / Copy Mesh*.

  |CopyMesh1|

Renseigner alors l’interface suivante :

  |CopyMesh2|

Dans Mesh : sélectionner le maillage à copier (maillage A).
Dans New Mesh Name, donner un nom au futur maillage (maillage B).
Dans New Geometry, sélectionner la nouvelle géométrie sur laquelle on souhaite réaliser un nouveau maillage (géométrie B).
Cliquer sur *Apply and Close*.

Voici le résultat que l’on obtient. La partie comprenant la nouvelle polyline, dans notre cas, repose sur une partie de la géométrie qui a été modifiée : elle n’est donc pas maillée immédiatement.

  |CopyMesh3|

Si on souhaite appliquer un sous-maillage à la polyline que l’on a rajoutée, cliquer sur *Create sub-mesh* et renseigner les différents champs (exemple possible de choix sur la figure ci-dessous).

  |CopyMesh4|

On obtient alors le maillage suivant : 

  |CopyMesh5|

On remarque que la face qui vient d’être maillée n’est pas orientée dans le même sens que les autres faces (couleur foncée au lieu de claire).

Pour réorienter la face, aller dans *Modification / Orientation*. Dans Group, sélectionner la face à réorienter en cliquant directement dans l’Object Browser sur le groupe de face concerné. Cliquer sur Add, la face devient jaune. 
Cliquer sur *Apply and Close*.

  |CopyMesh6|

  |CopyMesh7|

Voici le maillage final :

  |CopyMesh8|

.. only:: html
 
   :ref:`ref_casParticuliers`


..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Compléter un cas de calcul
#########################################

.. |CompleteCase| image:: /_static/CompleteCase.png
   :align: middle

.. |CompleteCase1| image:: /_static/CompleteCase1.png
   :align: middle

Une fois que le maillage a été réalisé et la topographie/bathymétrie interpolée, il peut être nécessaire de rajouter des polylignes dans la géométrie (lignes de crête de digue, par exemple). Ces polylignes sont des lignes de contraintes que l’on souhaite intégrer au maillage par la suite.

Il faut, dans un premier temps, rajouter sa polyline dans la rubrique POLYLINES.
Puis sélectionner le cas de calcul à modifier et faire clic droit / Complete calculation case.

  |CompleteCase|

Rajouter alors la polyline dans Included objects et cliquer sur Next jusqu’à la dernière page.

  |CompleteCase1|

Cette option *Complete calculation case* permet de ne pas devoir récréer le cas de calcul de zéro et de ne pas devoir réassembler les zones qui auraient été préalablement déjà regroupées. Seules les zones coupées par la polyline rajoutée devront être regroupées selon le souhait de l'utilisateur.
Cliquer sur Finish et Exporter vers GEOM. On pourra alors appliquer un sous-maillage sur cette polyline.

.. only:: html
 
   :ref:`ref_casParticuliers`


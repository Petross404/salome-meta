..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Modification d'une bathymétrie
#########################################

On souhaite appliquer une nouvelle bathymétrie sur tout ou partie d'un maillage existant, pour lequel l'on ne dispose que d'un maillage MED,
contenant un champ de bathymétrie nommé *'BOTTOM'*.

Supposons par exemple que l'on a une nouvelle bathymétrie pour le lit mineur, identifié dans le maillage par un groupe de mailles *'litMineur'*.
Cette bathymétrie doit être dans le même système de coordonnées que le maillage.

En Python, depuis la console embarquée de SALOME, ou depuis un shell SALOME : ::

  <appli_XXX>/salome shell
  python
  
  from salome.hydrotools.changeBathy import changeBathy
  changeBathy(meshFileIn, meshFileOut, newBathy, 'litMineur', Xoffset, Yoffset)

Ici, 

  * *meshFileIn* : le chemin complet du maillage d'origine avec son champ 'BOTTOM' à modifier, exemple :"/home/user/meshOrig.med"
  * *meshFileOut* : le chemin complet du maillage avec le nouveau champ 'BOTTOM',
  * *newBathy* : le chemin complet du nouveau fichier de bathymétrie,
  * *'litMineur'* : le nom du groupe de mailles dont on modifie la bathymétrie,
  * *Xoffset, Yoffset* : les coordonnées du repère local, optionnelles, si non nulles.


.. only:: html
 
   :ref:`ref_outilsReprise`


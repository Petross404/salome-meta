..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Notions préliminaires
#########################################

 * **Géoréférencement** : Le système de projection à utiliser dépend de
   la localisation géographique de l'étude et doit être choisi et noté
   pour faciliter la reprise de l'étude. En France métropolitaine, la
   projection de référence est Lambert 93.
   **Toutes les données importées dans SALOME-HYDRO doivent être dans le même réferentiel.**
   SALOME-HYDRO ne propose pas de convertisseur.

 * **Repère Local** : Les coordonnées planes, **toujours exprimées en mètres**,
   dans la projection utilisée correspondent souvent à des grands nombres.
   Par exemple, les coordonnées en Lambert 93 d'un point du territoire
   métropolitain sont par exemple de l'ordre de (400 000, 6 500 000).
   Le repère local consiste en un simple décalage de l'origine, permettant
   de manipuler de plus petits nombres.
   Pour améliorer la précision numérique dans les différentes étapes de
   l'étude (géométrie, maillage, calcul...),
   **il est très fortement recommandé de prendre un repère local**.
   SALOME-HYDRO affiche les coordonnées dans les deux repères (local et global)
   et assure les translations automatiquement à l'import et à l'export.

 * **ligne de contrainte** : Ligne sur laquelle le maillage va s'appuyer.
   Dans SALOME, ces lignes sont définies dans la Géométrie (module GEOM) en tant que "edge".
   Toutes les lignes que l'on construit dans le module HYDRO ne sont pas forcément gardées dans la géométrie.

 * **axe hydraulique** : Ligne d'écoulement d'une rivière.
   Souvent obtenue en reliant les points bas d'une succession de profils en travers.

 * **ligne de crête** : Ligne reliant les points les plus hauts d'une digue.

 * **partition** : Il s'agit d'une partition de l'espace en **zones** et **régions**.
   Les concepts de zones et de régions sont introduits par la pratique dans l'exercice plus bas.
   Les frontières d'une région correspondent aux lignes de contrainte du maillage. Les régions peuvent
   être découpées en plusieurs zones. Une zone correspond à un mode de calcul de la bathymétrie.
   
 * **Land Cover Map** : Carte d'occupation des sols, qui donne la nature des terrains, selon une codification
   *Corine Land Cover*.
   
 * **Table de Strickler** : Donne le *coefficient de Stricker* (frottement au sol) par type de Zone *Corine Land Cover*.
   Ces coefficients sont à ajuster au cas par cas, selon le type de calcul.


################################################
Principales étapes d'une étude avec SALOME-HYDRO
################################################

.. |etapesEtude| image:: /_static/etapesEtude.png
   :align: middle

.. |SALOME_Memo| image:: /_static/SALOME_Memo.png
   :align: middle

SALOME contient l’ensemble des modules nécessaires au lancement d’une étude Telemac.

L’application SALOME-HYDRO concentre les modules HYDRO et HYDRO-SOLVER au sein de la plate-forme SALOME
et intègre le système Telemac2D.

La figure ci-dessous résume les étapes générales et les outils utilisés au sein de la plate-forme SALOME.

  |etapesEtude|

L’étude se déroulera en passant par les différents modules de SALOME :

 * HYDRO

 * GEOM

 * MESH

 * HYDROSOLVER

 * PARAVIS

-------------------------------------

**Les prinicpales étapes :**
 
 * HYDRO : on importe des images et/ou des plans

 * HYDRO : on crée ou importe des lignes de contour d'**objets naturels** (rivières, iles...)
   et **artificiels** (digues, canaux, obstacles...),

 * HYDRO : on importe des champs de bathymétrie / altimétrie, ou des séries de profils de rivière,

 * HYDRO : on constitue des objets naturels et artificiels,
 
 * HYDRO : on importe ou crée une carte des occupations des sols (Land Cover Map)
   et une table donnant les coefficients de Strickler par type de zone (frottements au sol),

 * HYDRO : on constitue un **cas de calcul** en choisissant les objets utiles au cas,
   on regroupe les **zones** (une zone correspond à un mode de calcul particulier de la bathymétrie)
   en **régions** (Les frontières des régions correspondent aux lignes de contrainte du maillage,
   ce sont les faces de la géométrie finale SALOME), on associe au cas la Land Cover Map et la table de Strickler,

 * HYDRO : on exporte le cas vers GEOM,

 * GEOM : on reprend le cas dans GEOM, pour compléter l'identification des groupes liés aux différentes régions du domaine
   et des conditions limites,

 * SMESH : on choisit les algorithmes et hypothèses de maillage, on calcule le maillage et exporte le fichier MED du maillage,

 * HYDROSOLVER : on génère et exécute le script Python qui permet le calcul de l'interpolation en Z aux noeuds du maillage,
   à partir du fichier MED et du cas de calcul,

 * HYDROSOLVER : on génère et exécute le script Python qui permet l'affectation des coefficients de Strickler aux noeuds du maillage,
   à partir du fichier MED et du cas de calcul,

 * HYDROSOLVER : on définit les zones de conditions limites (fichier xxx.bcd)

 * HYDROSOLVER : on definit les évolutions des valeurs des conditions limites au cours du temps (fichier xxx.lqd ?)

 * HYDROSOLVER : on définit le paramétrage physico numérique du calcul (fichier cas),

 * HYDROSOLVER : on exécute le calcul,

 * PARAVIS : on dépouille les résultats

Dans le module HYDRO lui-même, la logique d’enchaînement des étapes est la suivante
(voir les différents types d’objets manipulés dans l’arbre de gauche) :

Il s’agit ici de la description d’un déroulement type, des allers-retours entre les différentes étapes
sont tout à fait possibles et se produiront certainement.

 * IMAGES :

L’idée est de partir d’images satellitaires et/ou de cartes de la zone à mailler
pour situer les différents éléments de l’étude., Ces images devront être géoréférencées
dans le même système de coordonnées que l’ensemble des données (Lambert93 par exemple).
Il peut s’agir de capture d’écran du géoportail par exemple.

 * BATHYMETRIE :

Les bathymétries constituées de nuages de points et/ ou de profils qui constituent le modèle de terrain
sur lequel va s’appuyer le maillage sont importées dans le projet.

 * POLYLINES :

Les polylignes (importées et/ou construites dans SALOME) permettent de définir les contours des différents objets
naturels et artificiels qui vont intervenir dans le cas de calcul. Ce sont des lignes fermées dans le cas général.
Certaines de ces lignes constitueront les lignes de contrainte du maillage.
Les polylignes splines permettent de définir des contours sans que le maillage qui
s’y appuiera ne doive utiliser strictement les points de la ligne. Seule la forme générale compte.
On peut tracer les lignes à la souris ou les importer à partir d’un fichier.

 * NATURAL OBJECTS :

Il s’agit des éléments constitués par exemple de l’emprise d’un domaine, d’îles, de lacs...
On sait en général leur associer une bathymétrie.

 * ARTIFICIAL OBJECTS :

Il s’agit de construire des éléments tels que des digues ou des canaux de géométrie connue.

 * CALCULATION CASES :

Lors de la définition du cas de calcul on sélectionne les objets à mailler et on résout les conflits
de recouvrement des bathymétries.

-------------------------------------

**Résumé des étapes :** également `disponible en pdf`__.

.. _SALOME_Memo_pdf: SALOME_Memo.pdf

__ SALOME_Memo_pdf_

  |SALOME_Memo|


données préalables
==================

* Pour les fichiers image : disposer de fichiers images des fonds carto ou photo et avoir repéré dans le système de travail
  les coordonnées de 2 ou 3 points bien répartis sur l’image (suffisament éloignés pour améliorer la précision).
  Avec trois points, on peut faire une transformation affine de l'image, pour le cas improbable ou celle-ci ne
  correspondrait pas à une projection verticale.

* Disposer des données de bathy au format ASCII.

Pour l’instant les fichiers de bathymetrie doivent porter l’extension .xyz (3 colonnes x,y,z) ou .asc
(format de type grille a pas régulier, tel que fourni dans la BD Alti de l'IGN, par exemple).

* Si l'on dispose de profils en travers pour le lit mineur, il peuvent être fournis au format .xyz
  avec une ligne blanche séparant chaque profil, ou au format SinusX décrit plus loin.

Import d'images
================

Il est possible d’importer des plans, cartes, et photos dans le module Hydro dans le dossier Images de l’arbre de l'étude.
L’idée est de partir d’images satellitaires et/ou de cartes de la zone à mailler,
géoréférencées dans le même système de coordonnées que l’ensemble des données (Lambert93 par exemple).
Il est possible de récupérer l’intersection de deux images, de les fusionner, de restreindre une image à partir d’une polyline.

Les manipulations d'images sont introduites dans l'exercice plus bas.

Choix d'un repère local
=======================

**Il est vivement conseillé de changer l’origine du système de coordonnées local**
pour éviter de manipuler de très grands nombres et avoir plus de précisions dans les différents calculs, notamment pour le maillage.

Pour cela il faut utiliser le menu *Hydro/change local CS* et renseigner les coordonnées de la nouvelle origine.

Import de Bathymetries
=======================

Le mode opératoire est expliqué dans l'exercice plus bas.

 * **remarque** : Si les altitudes sont inférieures à 0, la bathymétrie peut être cachée par les cartes ou photos
   (qui sont à Z = 0 par convention). Si l'on a besoin de voir simultanément la bathymétrie et les images,
   on peut, par exemple, soit passer en vue de dessous, soit éditer la bathymetrie (menu contextuel "edit imported bathymetry")
   pour inverser les z, le temps de contrôler la superposition des cartes et de la bathymétrie
   (ne pas oublier d'enlever l'inversion de z après !).

Récupération de données de bathymétrie d'un ancien maillage
-----------------------------------------------------------

Pour les différentes opérations ci-dessous, le mode opératoire précis reste à détailler. Les scripts Python cités
ne sont pas fournis avec cette version, is nécesitent des adaptations au cas par cas.

 * Il est possible de récupérer un ancien maillage d’un cas d’étude, en le transformant  avec un convertisseur
   intégré dans SALOME-HYDRO du format selafin (.slf) au format .med (format dédié pour la plate-forme Salomé en général).

 * A partir de l’ancien maillage, il peut être nécessaire d’appliquer une translation (par exemple +2 000 000)
   à la position y des nœuds pour passer en système de géoréférencement connu (par exemple LambertIIEtendu).

 * Puis la bathymétrie (champ de fond Z) est récupérée à l’aide d’un script Python qui crée un fichier .xyz
   (position x du nœud, position y du nœud, Champ Z associé).

 * Ce fichier .xyz est converti en Lambert93 grâce au logiciel libre Circé (sous Windows).

 * Ensuite les positions des nœuds des bords sont récupérées à l’aide d’un script Python qui parcourt les nœuds,
   constate s'ils sont au bord et crée un fichier dans lequel chaque bord récupéré est mis en forme pour un import direct
   dans le module HYDRO.

 * On peut isoler de cette façon le contour de l’emprise, des piles de ponts, et les îles éventuelles.

Import d'objets de type lignes
==================================

Le format SinusX (ASCII) décrit en annexe permet de décrire plusieurs types de lignes et de profils.
Les fichiers au format SinusX qui respectent les conventions décrites en annexe peuvent être importés
dans SALOME HYDRO.

polylignes
-----------

définition : lignes dans le plan XY, généralement utilisées pour définir des contours, des zones.

profils
----------

Deux types de profils : géoréférencés ou non.

Les profils géoréférencés sont définis dans XYZ, les autres dans XZ (XY).
Utilité : section de digue, de canal, de rivière.

On définit une seule section pour un canal ou une digue, une série de profils pour une rivière.

Pour une digue, la valeur Z=0 correspond à la ligne de crête, pour un canal, c'est la ligne de fond.

On considère uniquement des profils symétriques (par rapport à la ligne de crête ou de fond).


Streams
--------

Rivière décrite par une succession de profils en travers, ordonnés via une ligne amont-aval qui passe par ces profils.
Cette ligne peut être l'axe hydraulique, mais ce n'est pas obligatoire.

Les fichiers de stream peuvent être des fichiers XYZ pour lequels chaque profil est séparé par une ligne vide.


Dessin de lignes
=================

Les contours de type polyligne sont nécessaires à la création de la géométrie.
Ils permettent la construction de l’emprise du modèle, des îles, du lit mineur d’un fleuve, ainsi que des digues, des canaux, des routes...

On peut afficher la bathymetrie ou les cartes lors de la saisie des contours pour se repérer.

polylines
---------

Dans SALOME les polylignes sont de deux types :

 *  polylignes (ligne brisée constituée d'une série de segments droits, pour décrire un objet géométrique)

 *  splines (suite d’arcs qui donne une courbe à dérivée continue,
    pour décrire une courbure naturelle, qui s’adaptera à la finesse de la discrétisation).

L’utilisation de splines permet de définir des contours sans que le maillage qui s’appuie dessus
ne s’accroche à tous les points de la ligne : seule la forme générale compte.

Le mode opératoire est détaillé dans l'exercice ci-dessous.

Il est possible de créer des lignes combinant polylignes et splines.
Voir plus loin le pararaphe de manipulation des polylignes.

profils de digue ou canal
--------------------------

On peut importer ou dessiner ce type de profils.
Le mode opératoire du dessin est détaillé dans l'exercice ci-dessous.

profils de rivière
------------------

Seul l'import de ces profils est prévu.

Création d'objets "naturels" type "zone immersible"
===================================================

Une **zone immersible** est une zone qui sera maillée. Les iles qui ne sont pas submersibles peuvent être exclues du maillage.
Dans SALOME HYDRO, on distingue les îles du reste en désactivant leur attribut **Submersible**.

Créer une zone immersible consiste à créer une face géométrique à partir d’un des contours dessinés précédemment.
On renseigne donc pour cela la polyline (obligatoire) sur laquelle va reposer la face et la bathymétrie (facultative)
que l’on souhaite associer à cette zone géographique.

 * Remarque : la bathymétrie est facultative dans la création des objets naturels, notamment dans le cas des îles.

 * Remarque : Il est  possible de changer l’ordre d’affichage des différents objets naturels et artificiels qui sont tous par convention
   dans le plan z=0, pour remettre "dessus" les petits objets.

Création d'objets "naturels" type "stream"
==========================================

Le mode opératoire est détaillé dans l'exemple plus bas.

Création d'objet "artificiel" type digue ou canal
=================================================

Le mode opératoire est détaillé dans l'exemple plus bas.

Obstacles
============

Objets géométriques complexes (bâtiments...) importés depuis GEOM,
pour constituer des zones non submersibles ("iles" ou assimilés).
Il faut mettre ces objets dans le repère local avant des les importer.

Tables de coefficients de Strickler, Land Cover maps
====================================================

Il est possible définir une carte des coefficients de Strickler (frottements sur le fond) couvrant le domaine d'étude.

Des cartes décrivant la nature des sols (Land cover Map) peuvent être récupérées sur différents sites.
Il est notamment possible de télécharger et d'éditer dans un outil de SIG (Systeme d'Information Géographique)
comme *qgis* les cartes "Corine Land Cover".
Ces cartes s'appuient sur une nomenclature standard des différents types de territoire.

On définit en parallèle une table des coefficients de Strickler qui donne le coefficient pour chaque type défini dans la nomenclature.
Les coefficients de Strickler sont en général ajustés pour une étude donnée, pour recaler le modèle.

Les Land Cover Map peuvent être importées depuis qgis ou créées "from scratch" dans SALOME-HYDRO.
Il est également possible d'éditer ces cartes dans SALOME-HYDRO : ajout, suppression, regroupement, modification de zones...

Constitution d'un cas de calcul
================================

Lors de la constitution d'un cas de calcul, il est possible de ne sélectionner que certains des objets définis précedemment.
A partir d'une même base d'objets, on peut créer plusieurs cas de calculs plus ou moins complexes (prise en compte ou non
de détails comme des piles de ponts, par exemple).

L'emprise du domaine est définie par un contour polygonal particulier.

Le chevauchement des différents objets crée des zones "en conflit" pour lesquelles il faut faire des choix pour le calcul de
la bathymétrie.

Le résultat du découpage en zones des différents objets se chevauchant constitue l'opération dite de **partition**.

On peut regrouper des zones en régions homogènes dans la structure du maillage visée,
pour s’affranchir des contours que l'on ne veut pas garder en tant que lignes de contraintes.

Dans le cas de calcul, il est possible d'identifier certaines lignes qui serviront de support aux conditions limites.

Le resultat est exporté dans le module de géométrie.

Le mode opératoire est détaillé dans l'exemple plus bas.

Géométrie: Module GEOM
======================

Une fois le cas de calcul terminé et exporté il apparaît dans le module GEOM.

Il faut activer ce module pour pouvoir visualiser et modifier le cas exporté.

Il est conseillé de faire un *show only* sur la géométrie :
dans l'arbre, se placer sur le cas de calcul dans la géométrie et menu contextuel clic droit *show only*. 

Dans GEOM, on voit notre cas de calcul sous le nom <nom de cas>_N auquel est attaché le (ou les) contour(s)
choisis au moment au moment de la définition du cas de calcul.

Il se peut qu’on ait besoin d'identifier certaines parties :

 * Faces : pour mailler de façon différentes certaines zones

 * Segments : pour définir les conditions aux limites.

Le mode opératoire est détaillé dans l'exemple plus bas.

 * remarque : Il est possible d'utiliser le module de géométrie pour définir un certain nombre de
   contraintes sur le maillage. par exemple, on peut définir des points fixes de notre maillage
   (qui vont par exemple correspondre à des points de mesure).
   **Toute modification de la géométrie se traduit par la création d'un nouvel objet et la perte des groupes
   définis dans l'objet initial. Il faut donc créer les groupes en dernier, sur la géométrie finale,
   et, si possible éviter les modifications qui font perdre les définitions automatiques du module HYDRO.**

Maillage: Module SMESH
=======================

On se réferera aux formations SALOME pour l'utilisation du module SMESH.

Le mode opératoire pour SALOME-HYDRO est détaillé dans l'exemple plus bas.

Interpolation en Z
===================

principes
---------

En hydrodynamique il est primordial de connaître la valeur de la bathymétrie en chaque nœud de calcul.

Le calcul de la bathymétrie est fait zone par zone, a chaque zone est associé un mode de calcul de la bathymétrie :

 * à partir des nuages de points

 * à partir des profils de rivière

 * à partir de l'axe et de la section des digues et canaux

 * à partir de la CAO des obstacles

Pour les nuages de points, on dispose dans HYDROSOLVER d’un utilitaire générant un script Python
qui permet d’interpoler la bathymétrie sur le maillage.
Ce script utilise un algorithme qui prend soit la valeur du Z du point le plus proche, soit la valeur Z interpolée
sur une triangulation préalable du nuage.

Le mode opératoire est détaillé dans l'exemple plus bas.

Mise en données Physico-numérique pour TELEMAC
===============================================

Cette mise en données fait intervenir le module HYDROSOLVER pour l'assemblage du cas de calcul.

description des conditions limites
----------------------------------

Chaque zone de condition limite correspond à un groupe nommé dans le maillage.
Les types de conditon limites associés à un groupe sont définis dans un fichier.
Dans le module HYDROSOLVER, un outil permet d'associer des types de condition limites aux groupes concernés ans le maillage, 


édition du fichier Cas
----------------------

Les paramètres de calcul sont définis dans le fichier Cas avec la syntaxe TELEMAC 2D
(avec l'editeur EFICAS accessible depuis le module HYDROSOLVER ou avec un éditeur de texte standard).

inventaire des fichiers utilisés par TELEMAC 2D
-----------------------------------------------

A compléter, voir l'exemple ci-dessous.

Lancement et suivi du calcul
============================

Le module HYDROSOLVER permet de lancer TELEMAC 2D.

Le mode opératoire est détaillé dans l'exemple plus bas.

Dépouillement des résultats
===========================

Le module PARAVIS est utilisé pour l'exploitation des résultats.
On se réfèrera aux formations SALOME pour l'utilisation du module PARAVIS.
Certains filtres spécifiques à l’hydraulique sont détaillés dans l’exemple plus bas.
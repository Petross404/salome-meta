..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

######################################
Interprétation du format SinusX
######################################

Structure du fichier et types de lignes
=======================================

Le fichier ASCII est structuré en blocs de données.

Les lignes commençant par *B* sont des délimiteurs de bloc.

Les lignes commençant par *CN* ou *CP* complètent la définition du bloc.

Les autres lignes commençant par *C* sont des commentaires.

Les lignes commençant par des nombres sont les points du bloc.

On peut décrire des semis de points, des courbes xyz, des profils, des courbes de niveau.

Selon le format, les courbes peuvent être reliées ou non, fermées ou non.
En pratique, une courbe non reliée est une sorte de semis de point...

Le module HYDRO de SALOME prend en compte des points xyz, des courbes dans le plan XoY (z=0),
des courbes dans le plan XoZ (profils). 
Pour traiter les courbes dans l'espace (Z quelconque, variable ou non),on peut faire 3 types
de traitements lors de l'import dans le module HYDRO :

* projeter en z=0 pour avoir une courbe reliée dans le plan XoY.
* créer une ligne dans le plan XoZ (abscisse curviligne de la courbe XoY, altitude).
* importer les points x,y,z non reliés comme Bathymétrie/altimétrie.

Le premier import est le plus important et doit être fait systématiquement.

Le second est utile pour des courbes xyz, quand Z est variable.

Le troisième n'est pas facile à utiliser directement dans le module HYDRO, au delà de la simple visualisation
des champs de points de Bathymétrie/altimétrie en 3D. Pour l'interpolation d'altitude au noeuds du maillage,
on a besoin du contour du champ de Bathymetrie/altimétrie dans le plan XoY.
Ce contour n'a pas vraiment de sens pour une ligne.
Par contre, en combinant les points de plusieurs lignes (par exemple une série de courbes de niveau)
dans un même nuage de points, on peut reconstituer une Bathymétrie/altimétrie.

Délimiteur de bloc
==================

::

  B type x1 y1 z1 dx dy dz rapport

type
----

* type = S semis de point    
* type = C courbe xyz
* type = P profil xyz
* type = N courbe de niveau

x1 y1 z1 dx dy dz rapport
-------------------------

Origine et deltas pour convertir les mesures de la table à digitaliser en mètres.

On n'en tient pas compte à l'import.

exemples de lignes B
--------------------

::

  B S +4.290000E+05 +2.420000E+05 +0.000000E+00 +1.500000E+03 +2.000000E+03 +1.000
  B N +0.000000E+00 +0.000000E+00 +0.000000E+00 +1.000000E+00 +1.000000E+00 +1.000000E+00 1
  B C +0.000000E+00 +0.000000E+00 +0.000000E+00 +1.000000E+00 +1.000000E+00 +1.000000E+00 1
  B C +0.000000E+00 +0.000000E+00 +0.000000E+00 +3.200000E+04 +2.400000E+04 +1.000
  B C -3.333333E+03 +4.875000E+04 +0.000 A000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.659154
  B C -3.333330E+03 +4.875000E+04 +0.000 A000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658869

export
------

::

  B type   0.0  0.0  0.0    1.0  1.0  1.0    1.0

ou plutôt, à préférer

::

  B type


Nom du bloc
===========

::

  CN le_nom_du_bloc

Import
------

Remplacer les espaces par '_' (exemple : courbe 1 --> courbe_1)

Export
------

Les noms des objets dans le module HYDRO. Éviter les espaces et caractères accentués dans les noms.

Liaison Fermeture
=================

Définition historique
---------------------

::

  CP fermé relié

Deux booleens indiquant si la courbe est fermée, et si les points sont reliés

*exemple* : CP 0 1 : courbe ouverte, points reliés.

En pratique, on ne doit pas avoir besoin de points non reliés, sauf les semis.

Proposition d'interprétation et export
--------------------------------------

::

  CP fermé spline

Deux booleens indiquant si la courbe est fermée, et si elle est de type spline ou ligne brisée.
Il faudra corriger manuellement les jeux de données existant pour les courbes fermées, et les polygones.

Plan de définition
==================

::

  CP numeroPlan

Le numéro du plan vaut 0, 1 ou 2 (0 : plan XoY, 1 : plan YoZ, 2 : plan XoZ)

La plupart des blocs sont dans le plan XoY : CP 0

On peut utiliser les plans 1 ou 2 pour définir des profils / section dans un plan normal à l'axe hydraulique,
avec deux coordonnées utiles. En pratique, on utilisera systématiquement le plan 2.

Paramètres complémentaires selon le type de blocs
=================================================

type = S semis de point
-----------------------

Sans objet

type = C courbe xyz
-------------------

::

  CP (16 indicateurs réels)

Inutilisé - pas interprété à l'import.

Soit on fait un export compatible::

  CP 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0

Soit on ne met rien : préférable.

type = P profil xyz
-------------------

Définition historique
~~~~~~~~~~~~~~~~~~~~~

::

  CP Zref Xref dz dx

Je n'ai pas compris comment c'était utilisé en altitude...

interprétation proposée pour un profil dans le plan *XoZ*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En pratique profil dans un plan vertical avec la donnée des 2 extrémités géoreférencées.
On suppose que la troisième coordonnée des points est à 0.

::

  CP Xorig Yorig Xfin Yfin

(Xorig, Yorig) et (Xfin, Yfin) extrémités géoréférencées du profil.

On passe d'un point (x,z) au point géoréférencé (ref +x*dx, yref +x*dy, z), avec (dx,dy) vecteur horizontal 
normé calculé à partir des 2 extrémités géoréférencées.

interprétation proposée pour un profil dans le plan *XoY*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On suppose que les points sont déjà géoréférencés, avec leurs 3 coordonnées valides.

On peut regrouper plusieurs profils dans SALOME HYDRO pour créer un objet rivière ('stream')

type = N courbe de niveau
-------------------------

::

  CP altitude

Altitude de la courbe de niveau. On retrouve la valeur dans la coordonnée Z de chaque point.

import : prendre cette valeur en référence,
ignorer la coordonnée Z des points (toujours la même valeur dans les exemples) ?

Lignes de Commentaires
======================

::

  C texte quelconque

ignoré à l'import

on peut mettre en tête de fichier des informations d'export à définir, par exemple : 

C SALOME HYDRO version xxx

C nom de l'étude SALOME

C date 

Lignes de points
================

::

  X Y Z touche texte

touche
------

inutilisé, ignorer

texte
-----

un label associé au point : inutilisé, ignorer

import
------

Pour les courbes de niveau, on peut ignorer Z, il est défini au niveau du bloc.

Quand deux points consécutifs ont les mêmes coordonnées, il faut ignorer le deuxième point.
(cas rencontré dans des jeux de données).

export
------

Selon la nature des courbes, on met à zéro la coordonnée inutile.

remarque : les courbes de niveau importées sont transformées en courbes dans le plan Z=0
et regroupées en semis de points. On perd l'information courbe de niveau dans SALOME et on ne saura pas
les exporter en tant que courbe de niveau avec l'altitude renseignée.

Pour les courbes projetées dans le plan XoY::

  X Y 0.0

Pour les courbes projetées dans le plan XoZ::

  X 0.0 Z 

Pour les profils géoréférencés XoY::

  X Y Z

exemple de courbe XYZ
=====================

Ici, il s'agit en réalité d'une courbe de niveau Z=9.0, mais elle n'est pas définie comme telle.

import
------

::

  B C -3.333330E+03 +4.875000E+04 +0.000000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658992
  CN trait_cote_ile
  CP 1 1 
  CP  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00 
  CP 0 
  C 
     211563.340     133489.165      9.000 A
     211604.013     133517.519      9.000 A
     211645.047     133544.734      9.000 A
     211684.597     133585.430      9.000 A
     211716.859     133584.277      9.000 A
     211777.488     133597.853      9.000 A
     211828.211     133609.896      9.000 A
     211896.886     133618.448      9.000 A
     211945.422     133633.990      9.000 A
     211996.891     133649.326      9.000 A
     212044.447     133622.576      9.000 A
     212067.681     133584.157      9.000 A
     212056.937     133521.369      9.000 A
     212007.716     133482.464      9.000 A
     211959.462     133428.999      9.000 A
     211925.576     133392.362      9.000 A
     211885.762     133376.207      9.000 A
     211817.310     133404.427      9.000 A
     211783.848     133416.811      9.000 A
     211742.545     133413.025      9.000 A
     211681.493     133421.775      9.000 A
     211644.814     133436.616      9.000 A
     211597.633     133447.736      9.000 A
     211569.826     133461.954      9.000 A


``B C -3.3...``
  bloc de type C (courbe xyz), parametres de digitalisation ignorés

``CN ...``
  nom du bloc = trait_cote_ile

``CP 1 1``
  courbe fermée, spline

``CP +0.0...``
  ignoré

``CP 0``
  plan XoY (ça doit toujours être le plan 0)

``C ...``
  les commentaires sont ignorés

``211563.340     133489.165      9.000 A``
  point x, y, z.  Les informations supplémentaires sont ignorées.

Si toutes les valeurs Z du bloc sont à zéro, les valeurs Z sont ignorées.
Si au moins une valeur Z du bloc est différente de zéro, alors les points alimentent un semis
regroupant les différentes courbes du fichier.

export
------

Courbe projetée dans le plan XoY sous forme de courbe de niveau à Z=0.
C'est une courbe fermée, spline.

::

  B N
  CN trait_cote_ile
  CP 1 1 
  CP 0.0
  CP 0
  C
     211563.340     133489.165     0.000
     211604.013     133517.519     0.000
     211645.047     133544.734     0.000
     211684.597     133585.430     0.000
     211716.859     133584.277     0.000
     211777.488     133597.853     0.000
     211828.211     133609.896     0.000
     211896.886     133618.448     0.000
     211945.422     133633.990     0.000
     211996.891     133649.326     0.000
     212044.447     133622.576     0.000
     212067.681     133584.157     0.000
     212056.937     133521.369     0.000
     212007.716     133482.464     0.000
     211959.462     133428.999     0.000
     211925.576     133392.362     0.000
     211885.762     133376.207     0.000
     211817.310     133404.427     0.000
     211783.848     133416.811     0.000
     211742.545     133413.025     0.000
     211681.493     133421.775     0.000
     211644.814     133436.616     0.000
     211597.633     133447.736     0.000
     211569.826     133461.954     0.000


exemple de courbe de niveau
===========================

Ici, on a repris l'exemple précédent en changeant le type de courbe : B N au lieu de B C
et indication de l'altitude dans l'en tête : CP 9.0.

import
------

::

  B N -3.333330E+03 +4.875000E+04 +0.000000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658992
  CN trait_cote_ile
  CP 1 1 
  CP 9.0 
  CP 0 
  C 
     211563.340     133489.165      9.000 A
     211604.013     133517.519      9.000 A
     211645.047     133544.734      9.000 A
     211684.597     133585.430      9.000 A
     211716.859     133584.277      9.000 A
     211777.488     133597.853      9.000 A
     211828.211     133609.896      9.000 A
     211896.886     133618.448      9.000 A
     211945.422     133633.990      9.000 A
     211996.891     133649.326      9.000 A
     212044.447     133622.576      9.000 A
     212067.681     133584.157      9.000 A
     212056.937     133521.369      9.000 A
     212007.716     133482.464      9.000 A
     211959.462     133428.999      9.000 A
     211925.576     133392.362      9.000 A
     211885.762     133376.207      9.000 A
     211817.310     133404.427      9.000 A
     211783.848     133416.811      9.000 A
     211742.545     133413.025      9.000 A
     211681.493     133421.775      9.000 A
     211644.814     133436.616      9.000 A
     211597.633     133447.736      9.000 A
     211569.826     133461.954      9.000 A


``B N -3.3...``
  bloc de type N (courbe de niveau), paramètres de digitalisation ignorés

``CN ...``
  nom du bloc = trait_cote_ile

``CP 1 1``
  courbe fermée, spline

``CP 9.0``
  altitude de la courbe.

``CP 0``
  plan XoY

``C ...``
  les commentaires sont ignorés

``211563.340     133489.165      9.000 A``
  point x, y, z.  Les informations supplémentaires sont ignorées.

Les valeurs Z des points sont ignorées. On alimente le semis de points
avec les points de la courbe avec z forcé à 9.0.

export
------

Courbe projetée dans le plan XoY sous forme de courbe à Z=0.
C'est une courbe fermée, spline.

::

  B N
  CN trait_cote_ile
  CP 1 1 
  CP 0.0
  CP 0
  C
     211563.340     133489.165     0.000
     211604.013     133517.519     0.000
     211645.047     133544.734     0.000
     211684.597     133585.430     0.000
     211716.859     133584.277     0.000
     211777.488     133597.853     0.000
     211828.211     133609.896     0.000
     211896.886     133618.448     0.000
     211945.422     133633.990     0.000
     211996.891     133649.326     0.000
     212044.447     133622.576     0.000
     212067.681     133584.157     0.000
     212056.937     133521.369     0.000
     212007.716     133482.464     0.000
     211959.462     133428.999     0.000
     211925.576     133392.362     0.000
     211885.762     133376.207     0.000
     211817.310     133404.427     0.000
     211783.848     133416.811     0.000
     211742.545     133413.025     0.000
     211681.493     133421.775     0.000
     211644.814     133436.616     0.000
     211597.633     133447.736     0.000
     211569.826     133461.954     0.000


Courbe projetée dans le plan XoZ selon l'abscisse curviligne.
C'est une courbe ouverte de type spline, définie par convention dans le plan XoZ.

::

  B P
  CN altitude_trait_cote_ile
  CP 0 1
  CP 0.0 0.0 0.0 0.0
  CP 2
  C
    0.0 9.0
    ...
    x   9.0

*Les extrémités ne sont pas géoréférencées, on s'appuie sur une autre courbe XoY*.
Pour distinguer les profils XoZ non géoréférencés de ceux qui le sont (par leur extrémités),
on utilise une valeur particulière du champ CP avec des coordonnées nulles::

  CP 0.0 0.0 0.0 0.0

Les valeurs x des abscisses curvilignes sont exprimées en mètre le long de la courbe,
ou sont normalisées entre 0 et 1. **A revoir**

Synthèse
========

Dans le module HYDRO de SALOME, lors de l'importation des fichiers SinusX existant,

- les semis de points sont transformés en Bathymétries.
- les courbes de niveau sont transformées en courbes à Z=0 et profils dans le plan XoZ,
  non géoréférencés (abscisse curviligne, z).
- les courbes xyz sont transformées en courbes à Z=0 et en profils dans le plan XoZ,
  non géoréférencés (abscisse curviligne, z).
- les profils XoZ géoréférencés par les coordonnées des extrémités sont convertis
  en profils où tous les points sont définis par leurs 3 coordonnées X, Y, Z (profils XoY).
- les profils XoZ non géoréférencés sont importés tels que
  (utilisés comme ligne d'altitude pour une autre ligne dans le plan XoY,
  ou comme section pour un canal, une digue...)
- les profils XoY sont conservés tels quels (tous les points sont définis par leurs 3 coordonnées X, Y, Z).
- l'ensemble des points des courbes de niveau (avec Z différent de 0) et des courbes xyz servent
  à créer un champ de Bathymétrie.

Lors de l'export, 

- les polylignes sont exportées en courbes de niveau Z=0, ouvertes ou fermées, spline ou ligne brisée. 
  (Que fait-on des sections ? Utiliser un commentaire de début de section sur le point ?)
- les profils sont exportés en tant que profils (à approfondir)
- Doit on exporter les bathymétries au format SinusX ou au format xyz ? (ne diffère que par la présence d'un en tête).


.. only:: html
 
   :ref:`ref_formatsSpecs`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

##############################################
Format SinusX for SALOME HYDRO: specifications
##############################################

Introduction
============

SinuxX is an ASCII format used at EDF for for free surface flow studies with Telemac.
SinusX format as been defined a long time ago for a digitizing software used for maps, isocontours...
The sofware is not used any more, but there are still parameters related to the software
in the format specification and the data files.
The goal of this specification is provide an editable ASCII format for import and export of SALOME HYDRO module data,
and allow import of existing SinusX data files.
The parameters related to the old SinusX software will be ignored on import in SALOME HYDRO module,
and default values will be provided on export from SALOME HYDRO module, to keep a consistent format between
old and new data.
Some existing files does not follow correctly the format and must be manually corrected. For instance,
closed parameter is not handled correctly in a lot of files.

Contents and general structure of SinusX format, correspondance with HYDRO data types
=====================================================================================

HYDRO data types to import and export
-------------------------------------

HYDRO module data contains Bathymetry fields and 2D or 3D polylines. 2D polylines are either polylines in the XoY plane (Z=0)
or profiles in a vertical plane.

The bathymetry fields are a list of x,y,z points.

The polylines, in the XoY plane, are defined by an ordered list of x,y points. the polylines are either closed or open,
spline or not. Sections are not yet taken into account here.

The profiles are defined in a vertical plane, by an ordered list of u,z points. The profiles are georeferenced or not,
are used to describe variables sections of a river (stream object), constant sections of channel or enbankments,
or altitude lines to be combined with XoY lines in order to build 3D lines.

All the HYDRO module objects are named.

general structure of SinusX format
----------------------------------

The file is structured in blocs of data.
There are several types of blocs:

* Scatter plot: field of Bathymetry or Altimetry, contains a list of x,y,z points
* XYZ profile: for instance a vertical profile on river, defined by a list of x,y,z points (in a vertical plane)
* isocontour: a line at a constant altitude, defined by a list of x,y,z points (with z constant)
* XYZ curve : a line in 3D, defined by a list of x,y,z points

All Blocs begin with a line starting by 'B type ...'.

A bloc contains several parameter lines starting with 'CN name' or 'CP parameters...'.

Comments are lines starting with 'C ...' and are ignored.

A bloc contains several point lines (one x,y,z point per line) after the bloc start and parameter lines.

SinusX lines format
-------------------

Bloc Delimiter
~~~~~~~~~~~~~~

::

  B type several ignored parameters

**type:**

* type = S: Scatter plot    
* type = C: XYZ curve
* type = P: XYZ profile
* type = N: isocontour

There are other types in some old files. These blocs are ignored.
The parameters after the type are ignored.

Bloc name
~~~~~~~~~

::

  CN le_nom_du_bloc

Avoid spaces and special characters (for instance accents) in name.

Closed and spline parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  CP closed spline

Closed and spline are two booleans (0 or 1). 
for instance,
::

  CP 1 1

means a closed line of type spline.

This parameter is significant only for curves, profiles and isocontour (types C, P, N)
and is present but not interpreted for Scatter plot. 

specific curve parameters, depending on curve type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Following the type of curve (C, P, N), this parameter is different:

**type = C: XYZ curve**

::

  CP real1 real2... real16

These 16 real parameters are ignored at import, and can be set to 0.0 at export.

**type = P: XYZ profile**

::

  CP x1 y1 x2 y2

Folowing the plane definition parameter (see below), the values define the start and end points
or are ignored.

If the plane definition is XoY (horizontal), all the point of the profile are fully defined with their 3 coordinates x,y,z.
All the points are supposed to be in the same vertical plane.
The x1,y1 and x2,y2 values are ignored (and set to 0.0 at export).

If the plane definition is XoZ (vertical), the points of the profile are defined with 2 coordinates x,z (or u,z).
The y coordinate of the profile points are set to 0.0.
The two extremities are defined by origin: p1(x1,y1,0) and end: p2(x2,y2,0).
The Points of the profile are in the plane defined by the line p1,p2 and the vertical.
The x or u coordinate represents the abcissa in meters on the line p1,p2 counted from origin p1.

If the plane definition is XoZ (vertical) and x1, y1, x2, y2 are all null, the profile is not georeferenced.

**type = N: isocontour**

::

  CP altitude

Altitude value for isocontour. This value is also in the Z coordinate of each point of the bloc,
but the Z coordinates are ignored, only the altitude parameter is taken into account.

plane definition parameter
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  CP plane

Plane parameter is an integer value: 0 1 ou 2.

* 0: plane XoY
* 1: plane YoZ
* 2: plane XoZ

Plane 0 is used for XYZ curve, XYZ profile and isocontour.

Plane 1 is not used.

Plane 2 is used for XYZ profile.

Plane parameter is present but not interpreted for Scatter plots.

Point line
~~~~~~~~~~

::

  x y z ignored comments

There are always 3 coordinates.
Everything after the coordinates is ignored.

At export, the real values must be with a fixed format with 3 decimals. For instance::

  275982.120   128441.000      -7.500

Coordinates are in meter, in Lambert 93 projection: x, y absolutes values are less then 10000000.000.
Z abolute values are less than 10000.000.

SinusX blocs description and format
===================================

Scatter plot bloc
=================

example of scatter plot bloc
----------------------------

*import from existing data file:*
::

  C 
  B S +0.000000E+00 +0.000000E+00 +0.000000E+00 +1.000000E+00 +1.000000E+00 +1.000000E+00 1
  CN semis_de_points
  CP 0 1
  CP 0
  C an ignored comment
  +2.759820E05 +1.284410E05 -7.500000E00 A
  +2.759970E05 +1.284470E05 -7.500000E00 A
  +2.760120E05 +1.284530E05 -7.500000E00 A
  +2.759760E05 +1.283920E05 +3.000000E00 A
  +2.759900E05 +1.283970E05 -2.450000E00 A
  +2.760030E05 +1.284030E05 -4.000000E00 A
  +2.760160E05 +1.284080E05 -5.450000E00 A
  +2.760300E05 +1.284130E05 -4.250000E00 A
  +2.760440E05 +1.284190E05 -7.500000E-01 A
  +2.760570E05 +1.284240E05 +3.250000E00 A

The only parameters used are the type of bloc and the name. Closed, spline parameter and plane parameter are ignored.
For each point, only the x y z value are used.

*export:*
::

  C 
  B S
  CN semis_de_points
  CP 0 0
  CP 0
  275982.000    128441.000   -7.500
  275997.000    128447.000   -7.500
  276012.000    128453.000   -7.500
  275976.000    128392.000    3.000
  275990.000    128397.000   -2.450
  276003.000    128403.000   -4.000
  276016.000    128408.000   -5.450
  276030.000    128413.000   -4.250
  276044.000    128419.000   -7.500
  276057.000    128424.000    3.250

example of XYZ profile
----------------------

profile defined by two georeferenced extremities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*import from existing data file:*
::

  B P 
  CN profil_with_extremities
  CP 0 1
  CP +2.759820E05 +1.284410E05 +2.760570E05 +1.284240E05
  CP 2
    1.120  0.000 -1.250 
    x y z values lines with y=0.0
    ...

The profile of name *profil_with_extremties* is vertical, is an open line of type spline.
The two extremities are (+2.759820E05 +1.284410E05 0.0) and (+2.760570E05 +1.284240E05 0.0)

*export:*
::

  B P 
  CN profil_with_extremities
  CP 0 1
  CP   275982.000  128441.000    276057.000    128424.000 
  CP 2
    1.120  0.000 -1.250 
    x y z values lines with y=0.0
    ...

profile not georeferenced
~~~~~~~~~~~~~~~~~~~~~~~~~

*import from existing data file:*
::

  B P 
  CN profil_not_georeferenced
  CP 0 0
  CP 0.0 0.0 0.0 0.0
  CP 2
    -10.0  0.0  0.0
     -8.0  0.0 -5.0
      8.0  0.0 -5.0
     10.0  0.0  0.0

The profile of name *profil_not_georeferenced* is vertical, is an open line of type polyline.
The 4 real parameters are null, meaning there are no georeferenced extremities.

*export:*
::

  B P 
  CN profil_not_georeferenced
  CP 0 0
  CP 0.0 0.0 0.0 0.0
  CP 2
    -10.0  0.0  0.0
     -8.0  0.0 -5.0
      8.0  0.0  5.0
     10.0  0.0  0.0

profile with fully georeferenced points
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*import from existing data file:*
::

  B P 
  CN profil_fully_georeferenced
  CP 0 1
  CP +1.00000E+02 +2.00000E+02  +1.00000E+01  +2.00000E+01
  CP 0
    275982.000    128441.000   -7.500
    x y z values lines
    ...

The profile of name *profil_fully_georeferenced* is defined in plane 0 (XoY),
meaning all the points are fully georeferenced (x y z significants).
The 4 real parameters are not used.

*export:*
::

  B P 
  CN profil_fully_georeferenced
  CP 0 1
  CP 0.0 0.0 0.0 0.0
  CP 0
    275982.000    128441.000   -7.500
    x y z values lines
    ...

example of XYZ curve
--------------------

*import from existing data file:*
::

  B C -3.333330E+03 +4.875000E+04 +0.000000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658992
  CN trait_cote_ile
  CP 1 1 
  CP  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00 
  CP 0 
  C 
     211563.340     133489.165      8.750 A
     211604.013     133517.519      9.000 A
     211645.047     133544.734      9.100 A
     211684.597     133585.430      9.150 A
     211716.859     133584.277      9.050 A
     211777.488     133597.853      8.950 A
     211828.211     133609.896      9.000 A
     211896.886     133618.448      9.200 A
     211945.422     133633.990      9.250 A
     211996.891     133649.326      9.150 A
     212044.447     133622.576      9.050 A
     212067.681     133584.157      9.000 A
     212056.937     133521.369      9.000 A
     212007.716     133482.464      9.000 A
     211959.462     133428.999      9.000 A
     211925.576     133392.362      9.000 A
     211885.762     133376.207      9.000 A
     211817.310     133404.427      9.000 A
     211783.848     133416.811      9.000 A
     211742.545     133413.025      9.000 A
     211681.493     133421.775      9.000 A
     211644.814     133436.616      9.000 A
     211597.633     133447.736      9.000 A
     211569.826     133461.954      9.000 A

The curve of name *trait_cote_ile* is closed, of type spline, defined in plane XoY (XoY is the only possible value here).
Other parameters are not used.

This 3D curve is converted in a polyline and a profile in HYDRO module:

* One polyline (with z=0).
* One vertical profile (u,z) representing the altitude along the polyline defined above.

The u parameter of the vertical profile is the curvilign abcissa (in meters) along the isocontour.


*export* 

See isocontour and XYZ profile not georeferenced

example of isocontour
---------------------

*import from existing data file:*
::

  B N -3.333330E+03 +4.875000E+04 +0.000000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658992
  CN trait_cote_ile
  CP 1 1 
  CP 9.0 
  CP 0 
  C 
     211563.340     133489.165      9.000 A
     211604.013     133517.519      9.000 A
     211645.047     133544.734      9.000 A
     211684.597     133585.430      9.000 A
     211716.859     133584.277      9.000 A
     211777.488     133597.853      9.000 A
     211828.211     133609.896      9.000 A
     211896.886     133618.448      9.000 A
     211945.422     133633.990      9.000 A
     211996.891     133649.326      9.000 A
     212044.447     133622.576      9.000 A
     212067.681     133584.157      9.000 A
     212056.937     133521.369      9.000 A
     212007.716     133482.464      9.000 A
     211959.462     133428.999      9.000 A
     211925.576     133392.362      9.000 A
     211885.762     133376.207      9.000 A
     211817.310     133404.427      9.000 A
     211783.848     133416.811      9.000 A
     211742.545     133413.025      9.000 A
     211681.493     133421.775      9.000 A
     211644.814     133436.616      9.000 A
     211597.633     133447.736      9.000 A
     211569.826     133461.954      9.000 A


The isocontour of name *trait_cote_ile* is closed, of type spline, defined in plane XoY
(XoY is the only possible value here).
The constant altitude is 9.0. the Z values of the points are ignored.

This 3D curve is converted in a polyline and a profile in HYDRO module:

* One polyline (with z=0).
* One vertical profile (u,z) representing the altitude along the polyline defined above, with a constant Z.

The u parameter of the vertical profile is the curvilign abcissa (in meters) along the isocontour.

*export of an isocontour with z=0:*
::

  B N
  CN trait_cote_ile
  CP 1 1 
  CP 0.0
  CP 0
  C
     211563.340     133489.165     0.000
     211604.013     133517.519     0.000
     211645.047     133544.734     0.000
     211684.597     133585.430     0.000
     211716.859     133584.277     0.000
     211777.488     133597.853     0.000
     211828.211     133609.896     0.000
     211896.886     133618.448     0.000
     211945.422     133633.990     0.000
     211996.891     133649.326     0.000
     212044.447     133622.576     0.000
     212067.681     133584.157     0.000
     212056.937     133521.369     0.000
     212007.716     133482.464     0.000
     211959.462     133428.999     0.000
     211925.576     133392.362     0.000
     211885.762     133376.207     0.000
     211817.310     133404.427     0.000
     211783.848     133416.811     0.000
     211742.545     133413.025     0.000
     211681.493     133421.775     0.000
     211644.814     133436.616     0.000
     211597.633     133447.736     0.000
     211569.826     133461.954     0.000


See also XYZ profile not georeferenced


Synthesis
=========

In SALOME HYDRO module, when importing SinusX files,

- scatter plots are imported as Bathymetries,
- isocontours are converted as polylines (z=0) and profiles not georeferenced (lines with z=constant),
- XYZ curves are converted as polylines (z=0) and profiles not georeferenced (curvilign abcissa, z),
- XYZ profiles with georeferenced extremities are imported as is (stream profiles for instance),
- XYZ profiles with with fully georeferenced points are converted in profiles with georeferenced extremities (see stream profiles),
- XYZ profiles not georeferenced are imported as is (used for section of a channel or embankment, or altitude line...),
- **all points of XYZ curves and isocontour with z different of 0 are regrouped to build a Bathymetry field**,
  (Isocontour with z=0 are not significant in that case,
- when 2 successive points have the same coordinates, the second point must be removed (there are some old data files with this problem).

When exporting SinusX files, all the polylines and profiles can be exported in a single file:

- polylines are exported as isocontour with z=0, (open or closed, spline or not),
  *(what about sections ? add a comment after the first point of a section?)*
- profiles are exported as is, georeferenced or not...
- **bathymetries are exported on demand (volume may be huge).**


.. only:: html
 
   :ref:`ref_formatsSpecs`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Création du maillage
#########################################

.. |mesh_init| image:: /_static/mesh_init.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |mesh_edit| image:: /_static/mesh_edit.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |mesh_hypo_edit| image:: /_static/mesh_hypo_edit.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |vtk_view_fitall| image:: /_static/vtk_view_fitall.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |vtk_view_top| image:: /_static/vtk_view_top.png
   :align: middle
   :width: 16pt
   :height: 16pt


.. |Capture_CreateMesh| image:: /_static/Capture_CreateMesh.png
   :align: middle

.. |Capture_HypothesisConstruction| image:: /_static/Capture_HypothesisConstruction.png
   :align: middle

.. |Capture_CreateSubMesh| image:: /_static/Capture_CreateSubMesh.png
   :align: middle

.. |Capture_HypothesisLocalLength| image:: /_static/Capture_HypothesisLocalLength.png
   :align: middle

.. |Capture_HypothesisNbSegments| image:: /_static/Capture_HypothesisNbSegments.png
   :align: middle

.. |Capture_WarningOrder| image:: /_static/Capture_WarningOrder.png
   :align: middle

.. |Capture_OrderingSubMeshes| image:: /_static/Capture_OrderingSubMeshes.png
   :align: middle

.. |Capture_MeshComputationSucceed| image:: /_static/Capture_MeshComputationSucceed.png
   :align: middle

.. |Capture_CreateGroupsFromGeometry| image:: /_static/Capture_CreateGroupsFromGeometry.png
   :align: middle


Lorsque la géométrie est prête, nous activons le module de maillage, *SMESH*.

Introduction au fonctionnement de SMESH
=======================================

Pour spécifier le maillage, on définit en général un algorithme par défaut avec son paramétrage :
dans SMESH, on parle des *hypothèses* de l'algorithme.

Cet algorithme et ces hypothèses s'appliquent partout sauf modification portant sur une partie de
la géométrie (*sub shape*).

On peut donc créer des sous maillages sur une face ou un groupe de faces,
une edge ou un groupe d'edges, pour définir des algorithmes et/ou des hypothèses spécifiques.

Lors du maillage, la géométrie de la pièce à mailler est explorée en partant de la dimension 1 (edges),
puis la dimension 2 (faces), et enfin la dimension 3 (volumes). Ici nous n'avons pas de volumes.

Les maillages de dimension 1 sont donc prioritaires sur ceux de dimension 2 et s'imposent à eux.

Certains algorithmes gèrent simultanément plusieurs dimensions (dans notre cas, edges et faces).
Quand ces algorithmes tolèrent que l'on impose le maillage de certaines edges,
la définition de sous maillages de la ou des faces concernées sera prise en compte, sinon non.
De même, suivant l'algorithme choisi pour une face, l'onglet 1D du dialogue de maillage est actif on non.

Il en résulte que, dans certains cas, pour certaines *sub shapes*, plusieurs algorithmes et/ou hypothèses
sont définis, et il faut alors établir des priorités. Ces situations sont détectées automatiquement
et l'utilisateur se voit proposer des choix.

Algorithmes et Hypothèses de base pour la géométrie
====================================================

Ici, nous allons spécifier un maillage triangle par défaut sur l'ensemble du domaine, et particulariser
le maillage du lit mineur, pour obtenir des triangles allongés dans le sens de l'écoulement.

Nous sélectionnons la géométrie *HYDRO_garonne_1* dans l'arbre d'étude, et lançons la définition du maillage :
menu *Mesh/Create Mesh* ou icône |mesh_init|.

Dans le dialogue *Create Mesh*, nous choisissons l'algorithme de maillage *Netgen 1D-2D* qui va s'appliquer
par défaut sur l'ensemble de la géométrie.

  |Capture_CreateMesh|

Il faut préciser les hypothèses de maillage :

Nous cliquons sur le bouton actif dans la ligne *Hypothesis* pour choisir un type d'hypothèses,
et choisissons *Netgen 2D Parameters*.

Nous prenons une taille maximum d'arète de 200 (mètres), et minimale de 50. Pour le taux de progression
de la taille des triangles, *Fineness*, nous prenons *Very Fine*, pour préserver la qualité des triangles.

  |Capture_HypothesisConstruction|

Après avoir validé le dialogue d'hypothèses, nous validons la définition du maillage avec le bouton *Apply and Close*.

Le maillage apparaît sous le nom *Mesh_1* dans l'arbre avec une icône indiquant son statut : *non généré, ou incomplet*.

Il est possible de renommer le maillage, soit directement, soit via le dialogue d'édition, à partir du menu contextuel.

Toujours à partir du dialogue d'édition |mesh_edit|, il est possible changer l'algorithme,
ou de modifier les hypothèses : |mesh_hypo_edit|.

Sous maillages
==============

Nous sélectionnons le maillage dans l'arbre d'étude et créons un sous maillage (menu contextuel *Create Sub Mesh*).

Dans le dialogue qui s'affiche, il faut renseigner la géométrie, en cliquant dans l'arbre d'étude sur la face *litMineur*
contenue dans *HYDRO_garonne_1*. Pour cette sélection, il faut que la flèche du dialogue sur la ligne *Geometry* soit active.
Elle l'est par défaut.

Il est utile de renommer tout de suite le sous-maillage (première ligne du dialogue).

Nous choisissons l'algorithme *Quadrangle (Medial Axis projection)*. Cet algorithme reconstruit un axe hydraulique fictif,
découpe la rivière en quadrangles, normalement à l'axe hydraulique.

  |Capture_CreateSubMesh|

Il reste à définir la longueur des quadrangles, leur nombre dans la section de la rivière.
Dans l'onglet *1D* du dialogue du sous maillage *litMineur*, nous choisissons l'algorithme *Wire Discretisation*
l'hypothèse *Local Length*, et prenons une longueur de 100 (mètres). Il est utile de renommer l'hypothèse à ce stade.

**remarque** : Les hypothèses et algorithmes peuvent être partagés entre plusieurs maillages et sous maillages,
ce qui permet de modifier en un seul endroit tout ce qui doit rester cohérent. du coup, **il est utile d'avoir des noms
significatifs pour les identifier**.

  |Capture_HypothesisLocalLength|

Nous validons la définition du sous maillage avec le bouton *Apply and Close*.

La longueur que nous avons définie s'applique à la fois longitudinalement et transversalement. Pour contrôler le
nombre de mailles transversales, il faut un nouveau sous maillage, appliqué au groupe d'edges *SectionsGaronne*.

Nous créons donc un sous maillage sur *SectionsGaronne*, en prenant cette fois comme algorithme *Wire Discretisation*
et comme hypothèse *Nb. Segments*, avec 8 segments distribués régulièrement.

  |Capture_HypothesisNbSegments|

Nous validons la définition du sous maillage avec le bouton *Apply and Close*.
Il faut établir une priorité entre deux définitions :

  |Capture_WarningOrder|  |Capture_OrderingSubMeshes|

Nous sélectionnons *SectionsGaronne* pour le faire remonter en tête de liste.

Génération du maillage
======================

Après avoir validé, le maillage est prêt pour être généré.
Pour générer le maillage, il faut le sélectionner, et utiliser le menu contextuel *Compute*.
Une boite d'information s'affiche à la fin du calcul et donne des statistiques élémentaires.

|Capture_MeshComputationSucceed|

L'icône du maillage a changé dans l'arbre d'étude, et indique l'état *généré correctement*.

Le maillage n'est pas encore fini pour nos besoins, mais nous pouvons déjà le voir.
Pour l'afficher, *show* puis *FitAll* |vtk_view_fitall| et vue de dessus |vtk_view_top| (*-OZ*)
dans la barre d'icônes du viewer 3D.

Pour découper les quadrangles, nous utilisons le menu *Modification/Cutting of Quadrangles*.
Dans le dialogue, nous cochons *apply to all*, *use diagonal 1-3* puis *preview* :
la modification proposée apparaît,
il est possible de zoomer avec la molette de la souris pour vérifier.
Nous validons par *Apply and Close*.

Contrôle du maillage
====================

Il faut vérifier que le maillage est conforme aux besoins de TELEMAC.

Orientation des mailles
-----------------------

Pour les maillages surfaciques XY, **SALOME peut orienter par défaut les triangles dans le sens contraire à ce qu'attend TELEMAC**,
selon le sens de saisie des différentes lignes. SALOME peut donc orienter les triangles d'une face
avec la normale sortante vers le bas. On peut le vérifier avec la couleur des mailles
(bleu plus foncé sur l'envers que sur l'endroit) ou en utilisant la commande *Orientation of Faces* du menu contextuel
de la vue 3D du maillage. Cette commande dessine une flèche par triangle. Sur l'envers de la face, on ne voit qu'un point.
Pour voir les flèches, il faut changer l'angle de vue.

Pour réorienter les faces, nous utilisons le menu *Modification / Orientation* et cochons l'option *Apply to All*.
Après validation, les faces changent de couleur (bleu  plus clair).

Triangles surcontraints
-----------------------

Il faut éviter deux types de conditions limites différentes sur deux arêtes d'un triangle, et, plus généralement,
éviter que deux arêtes aient des conditions limites imposées (aucun degré de liberté sur le triangle).

Parfois, les mailleurs créent de tels triangles dans les coins du maillage. C'est le cas de Netgen dans les angles aigus.
Quand on peut, il faut éviter de créer des angles aigus aux limites du domaine.

Les triangles surcontraints se détectent avec la commande de menu *Controls / Face Controls / Over-constrained faces*.
Pour corriger cela, il faut utiliser la commande de menu *Modification / Diagonal Inversion* et sélectionner
l'edge interne du triangle en cause.

Création des groupes, Enregistrement du maillage
================================================

Les groupes de noeuds et d'éléments sont utiles pour 'étape d'interpolation de la bathymétrie,
et pour la définition des conditions limites.

Pour définir les groupes dans le maillage, nous utilisons la commande *Create Groups from Geometry*
du menu contextuel du maillage.
Nous sélectionnons tous les groupes et sub Shapes de la géometrie *HYDRO_garonne_2*
et constituons successivement des groupes d'**éléments** et de **noeuds**.

  |Capture_CreateGroupsFromGeometry|

Ces groupes apparaissent dans l'arbre d'étude sous le maillage après validation du dialogue.
Il est possible de le voir avec *show only*. L'option *Auto Color* du menu contextuel du maillage
les met en évidence.

Pour enregistrer le maillage dans un fichier au format MED, après avoir sélectionné le maillage,
nous utilisons la commande du menu *File / Export / MED file*.

.. only:: html

   :ref:`ref_exempleInondation`

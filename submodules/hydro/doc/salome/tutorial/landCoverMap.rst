..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Land Cover Map
#########################################


.. |stricklerTable_1| image:: /_static/stricklerTable_1.png
   :align: middle

.. |importLandCoverMap| image:: /_static/importLandCoverMap.png
   :align: middle

.. |importLandCoverMap_2| image:: /_static/importLandCoverMap_2.png
   :align: middle

.. |importLandCoverMap_3| image:: /_static/importLandCoverMap_3.png
   :align: middle

.. |casLandCoverMap| image:: /_static/casLandCoverMap.png
   :align: middle

.. |BottomFriction| image:: /_static/BottomFriction.png
   :align: middle

.. |GenerateKs| image:: /_static/GenerateKs.png
   :align: middle

.. |InterfaceScriptKs| image:: /_static/InterfaceScriptKs.png
   :align: middle


Import d'une Land Cover Map
===========================
Nous allons reprendre l'exemple précédent "garonne_1" pour ajouter une carte des coefficients de Strickler.
La carte à importer peur être téléchargée et éditée préalablement dans qgis. Les *Corine Land Cover* couvrent généralement 
un grand territoire et sont très détaillées.

**Remarque :** Pour que ces cartes ne soient pas trop lourdes à manipuler dans SALOME HYDRO,
il est préférable de les découper dans qgis de façon à les restreindre autant que possible au domaine d'étude.
On définira un polygone de découpe englobant le domaine d'étude, en vérifiant que l'on ne perd pas de zone lors de la découpe
(Une carte découpée est fournie avec ce tutoriel). La carte comprend une base de données qui donne, pour chaque zone,
le code de type de territoire associé, selon la nomenclature *Corine Land Cover*.

Avant d'importer la carte, il faut disposer d'une table définissant les codes  *Corine Land Cover*, la couleur de représentation 
des types de zones, les coefficients de Strickler associés.

Une table par défaut est proposée dans SALOME-HYDRO, visible dans l'arbre d'étude, dans la rubrique *STRICKLER TABLES*, sous le nom
*Strickler table_1*. Il est possible de l'éditer avec le menu contextuel *Edit Strickler table*.
La plupart des champs sont éditables,
**notamment les coefficients de Strickler qu'il faudra de toutes façon adapter pour une étude réaliste**.

  |stricklerTable_1|
  
Le champ *Attribute Name* doit correspondre au champ de codification des types de zones dans la base de donnée
associée au *Corine Land Cover* importé : *CODE_06* correspond aux cartes de 2006, *CODE_12* aux cartes de 2012.
Ce champ n'est pas éditable dans le dialogue ci-dessus.

Dans l'exemple fourni, la carte date de 2006, il nous faut donc une autre table de Strickler.
Ces tables sont enregistrées dans des fichiers texte éditables.
Nous chargeons une table 2006 fournie dans le répertoire d'installation de l'application SALOME HYDRO
avec la commande *Import Strickler table* du menu contextuel de la rubrique *STRICKLER TABLES* ::

  <appli_xxx>/share/salome/resources/hydro/def_strickler_table_06.txt

Il faut effacer la table précédente *Strickler table_1*, pour que la nouvelle table soit correctement utilisée
lors de l'import de la Land Cover Map (menu contextuel *Delete*). 

Nous pouvons maintenant charger la Land Cover Map.
Nous utilisons pour cela la commande *Import land cover map from file(s)* du menu contextuel de la rubrique
*LAND COVER MAPS* ::
  
  <appli_xxx>/bin/salome/test/HYDRO/HYDRO/CLC_decoupe.shp

La carte comprend déjà un grand nombre de polygones noirs (plus de 600) dont on peut voir le contour en les sélectionnant.
La découpe déborde très largement de la zone d'étude. On voit les zones "oubliées" lors de la découpe dans qgis,
sur les bords. Elles sont hors de la zone d'étude.

  |importLandCoverMap|
  
En pratique, on sélectionne tous les polygones (<crtl> A dans la liste), avant d'appuyer sur le bouton *Next>*.
Il y a plusieurs attributs trouvés dans la base importée. Nous sélectionnons l'attribut qui nous intéresse,
le type de zone : *CODE_06*, avant d'appuyer sur le bouton *Next>*.
Les codes de zones sont correctement associés à leur définition fournie dans la nouvelle table de Strickler.

  |importLandCoverMap_2|

Nous appuyons sur le bouton *Finish*. Le traitement prend quelques dizaines de secondes.
La Land Cover Map ne s'affiche pas automatiquement : commande *show* du menu contextuel de l'objet *CLC_decoupe* 
dans la rubrique *LAND COVER MAPS*. On peut voir le cas de calcul sous la carte, en transparence.

  |importLandCoverMap_3|

Création, édition de Land Cover Map
===================================

Il est possible de créer de toutes pièces une Land Cover Map, à l'aide de polygones créés dans SALOME-HYDRO
ou importés.

Le menu contextuel de la Land Cover Map offre des possibilités d'édition des zones : 
ajouter, enlever, découper, regrouper, changer le type.

Nous ne détaillons pas ces opérations ici.

Utilisation d'une Land Cover Map dans le cas de calcul
======================================================

Pour exploiter la Land Cover Map, il faut éditer le cas de calcul. 
Nous éditons le cas de calcul *garonne_1* avec la commande *edit calculation case* du menu contextuel du cas de calcul.
Il faut avancer jusqu'au panneau *Land cover map* avec les boutons *Next>* et sélectionner le Land Cover Map et la 
table de Strickler

  |casLandCoverMap| 

Nous poursuivons avec les boutons *Next>* et *Finish* pour valider le cas.


Création d'un champ de Strickler aux noeuds du maillage
=======================================================

Telemac exploite un champ des coefficients de Strickler aux noeuds du maillage. Ce champ a pour nom *BOTTOM FRICTION*.
Ce champ est ajouté au fichier MED du maillage, comme le champ d'altitude aux noeuds.

Aller dans HYDROSOLVER / Hydro et cliquer sur Generate assignStrickler.py.

  |GenerateKs|

  |InterfaceScriptKs|

Il faut sélectionner le cas de calcul dans la rubrique HYDRO / CALCULATION CASE de l’arbre d’étude. Son nom apparaît dans la première ligne Calculation case du dialogue.

Le chemin complet du script à créer doit être renseigné dans la rubrique Output path.

Il faut désigner le fichier du maillage de départ construit à l’étape précédente dans rubrique MED file.
   
Nous pouvons voir le champ résultat avec le module MED, comme pour le champ d'altitude.
    
  |BottomFriction|
 
.. only:: html
 
   :ref:`ref_casParticuliers`

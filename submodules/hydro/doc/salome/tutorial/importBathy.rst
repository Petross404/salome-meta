..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#######################
Import de bathymétrie
#######################

.. |import_bathy1| image:: /_static/bathy1.png
   :align: middle

Le module HYDRO permet l'importation de types de fichiers de bathymétrie, correspondant
aux extensions .xyz et .asc. Ce sont des fichiers texte ASCII.
Les fichiers .xyz comprennent un point par ligne, avec les coordonnées x y z.
les fichiers .asc correspondent à un maillage cartésien structuré en x, y. 
Après un entête donnant le nombre de lignes, de colonnes, les coordonnées
du coin sud ouest, le pas en x et en y, on trouve la matrice des altitudes z.
Il est également possible d'importer des bathymétries contenues dans des fichiers au
format SinusX, qui sera décrit plus loin.

Nous allons importer deux fichiers pour les besoins de notre étude.  
Ils se trouvent dans le répertoire d'installation de l'application SALOME HYDRO ::

  <appli_xxx>/bin/salome/test/HYDRO/cloud_02.xyz 

  <appli_xxx>/bin/salome/test/HYDRO/garonne_point_L93.xyz 

Le fichier *cloud_02.xyz* est un extrait de données IGN sur une matrice régulière avec un point tout les 75 m, en accès libre.
Le fichier *garonne_point_L93.xyz* correspond à une étude antérieure, couvre le lit majeur du fleuve sur la région,
et est potentiellement plus précis que le précédent, au moins sur le lit mineur.

Pour importer ces fichiers, nous utilisons le menu contextuel de la rubrique *BATHYMETRIES*
dans l'arbre de l'étude SALOME. Le dialogue d'import permet de sélectionner le fichier à importer
et de changer son nom dans l'arbre d'étude.
Nous validons l'import avec le bouton *Apply and Close*.

pour afficher la bathymétrie, il faut cliquer sur l'oeil en face de son nom, et recentrer l'image
à l'aide du bouton *fit all*.

Les points sont colorés selon leur altitudes, et les plages de couleurs sont ajustées en fonction
du minimum et du maximum de l'ensemble des champs affichés. La légende correspondante est 
systématiquement affichée.

Nous devrions obtenir une vue ressemblant à ceci :

  |import_bathy1|

 
.. only:: html
 
   :ref:`ref_exempleInondation`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

####################################
installation et lancement de SALOME
####################################

.. |fit_all| image:: /_static/fitall.png
   :align: middle

installation
============

Vous devez obtenir un fichier d'installation: (*xxx*.run).
A EDF, on l'obtient via l'intranet :

 * Ouvrir compte  (ex : I88928)

 * Ouvrir navigateur web (bouton bleu en haut à droite ou Applications/Internet/Navigation Web Iceweasel)

 * Taper l’adresse : https://salome.der.edf.fr/

 * Chercher la dernière version de SALOME-HYDRO dans la rubrique : Télécharger.

 * On récupère un fichier *xxx*.run

 * Enregistrer (le fichier est automatiquement sauvé dans /Téléchargements)

 * Ouvrir un terminal  (En haut de l’écran : Accessoires/Terminal)

 * Taper cd Téléchargements

 * Taper ls : on doit voir le *xxx*.run

Pour tout le monde :

 * (si nécessaire le rendre exécutable : chmod +x *xxx*.run)

 * Lancer l’installation en tapant ./*xxx*.run

 * Réponse aux questions d’installation (répertoire : à votre choix, langue :
   **impérativement anglais** (l'option a été masquée, il n'y a pas le choix),
   toutes les autres questions éventuelles : oui par défaut)

Salome-Hydro est installé. Il y a une icône sur le bureau.
Il est possible de glisser-déplacer l'icône dans un éditeur ouvert (gedit ou équivalent) pour remplacer la ligne *Terminal=false* par *Terminal=true*.
Ceci permet d'avoir la trace d'exécution, pour faciliter le diagnostic de certains problèmes. Ce n'est pas obligatoire.
 
L'installation en anglais, parce que la traduction en français est incomplète. 

lancement de SALOME
====================

Double-clic sur l'icône SALOME du bureau, ou, connaissant le répertoire d'installation de SALOME ( *<appli_XXX>* ),
dans un terminal ::

  <appli_XXX>/salome
  
Dans le terminal, il est possible de récupérer les traces d'exécution en cas de problème.

* **remarque** : il est possible de donner des options à la commande salome.
  Par exemple, l'option -k ferme toutes les instances de SALOME précédentes et libère les ressources associées.
  On peut obtenir de l'aide avec l'option -h ::

  <appli_XXX>/salome -h

manipulation des vues dans SALOME
====================================

Pour les personnes ne connaissant pas SALOME, on décrit ici les principales commandes de manipulation des vues 2D et 3D,
ainsi que les techniques de sélection et affichage des objets de l'arbre de l'étude (Object Browser).

Affichage des données dans les fenêtres
---------------------------------------

Par un clic sur le petit œil à côté de l'objet dans la fenêtre Object Browser.

Par un clic droit dans l’arbre de gauche (dans la fenêtre Object Browser) sur l'objet :
on peut utiliser les différentes possibilités ::

  /show/ show only / hide / hide only / hide all /

Zoom
----

Ctrl + clic gauche

Le zoom est centré par défaut.
Activation avec la molette de la souris : pour pouvoir activer le zoom avec la molette de la souris :
file / préférences (sélectionner le module SALOME) / 3D viewer / zooming style / relative au curseur. 
Un zoom relatif au curseur est centré sur la partie autour de la position du curseur  
alors qu’un zoom centré prend en compte toute la zone.

Pour zoomer sur une donnée de l’arbre : 

Sélectionner la donnée de l’arbre, afficher cette donnée (clic droit et *show* ou *show only*)
Dans la barre d’outils en haut choisir le bouton *Fit All* |fit_all|.

Rotation
-----------

Ctrl + clic droit

Translation
-----------

Ctrl + molette

Viewers
-------

SALOME propose différents viewers : 

VTK pour les vues 3D (module MESH) et OCC pour les vues 2D (pour la géométrie).
On les retrouve via les onglets qui apparaissent à l’écran. 

Les différents éléments de l'étude ne sont pas tous affichables indifféremment dans tous les viewers !

   


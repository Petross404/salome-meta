..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Aide sommaire CloudCompare
#########################################

.. |CloudCompare_01| image:: /_static/CloudCompare_01.png
   :align: middle

.. |CloudCompare_02| image:: /_static/CloudCompare_02.png
   :align: middle

.. |CloudCompare_03| image:: /_static/CloudCompare_03.png
   :align: middle

.. |CloudCompare_04| image:: /_static/CloudCompare_04.png
   :align: middle
 
.. |CloudCompare_05| image:: /_static/CloudCompare_05.png
   :align: middle
  
Chargement et visualisation d'un nuage de point
===============================================

A partir du menu *File / open* de CloudCompare, on peut charger des nuages de points à différents formats dont le format xyz
(option *ASCII cloud*, fichier texte ASCII, 3 coordonnées x y z par ligne)
et le format asc (option *RASTER grid*, ASCII grille régulière, fournie par l'IGN notamment).
Deux exemples sont disponibles dans l'installation de SALOME_HYDRO ::

  <appli_xxx>/bin/salome/test/HYDRO/garonne_point_L93.xyz 
  
  <appli_xxx>/bin/salome/test/HYDRO/BDALTIr_2-0_MNT_EXT_0450_6375_LAMB93_IGN69_20110929.asc

On prend le fichier *garonne_point_L93.xyz*. A l'ouverture du fichier, on peut valider le premier dialogue sans modification (*Apply*) :

  |CloudCompare_01|
  
Le deuxième dialogue propose un changement de repère pour améliorer la précision, que l'on peut accepter tel que (*Yes*) :

  |CloudCompare_02|

Le nuage de point s'affiche, mais sans coloration selon la cote Z. On sélectionne la ligne *garone_point_L93 - Cloud* :

  |CloudCompare_03|

Dans le menu *Edit / Scalar fields / Export Coordinate(s) to SF(s)*, on sélectionne la coordonnée Z comme nouveau champ scalaire.

  |CloudCompare_04|

Le nuage de point devient coloré. On peut mettre une légende en cochant l'option *visible* de la rubrique *Color Scale*.

  |CloudCompare_05|

.. only:: html
 
   :ref:`ref_outilsComplementaires`


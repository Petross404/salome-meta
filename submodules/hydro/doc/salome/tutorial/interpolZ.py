# -*- coding: utf-8 -*-

# ===== Description du cas, a éditer =====
#=========================================

# --- nom du cas dans HYDRO
nomCas = 'garonne_1'

# --- fichier med 2D(x,y) du cas, produit par SMESH
fichierMaillage = '/tmp/garonne_1.med'

# --- dictionnaire: (clé = nom de groupe med, valeur= nom de région)
dicoGroupeRegion= dict(litMineur  = 'garonne_1_litMineur',
                      riveDroite = 'garonne_1_riveDroite',
                      riveGauche = 'garonne_1_riveGauche',
                      )

# --- méthode d'interpolation sur les nuages de points de bathymétrie
#     interpolMethod = 0 : interpolation au point le plus proche
#     interpolMethod = 1 : interpolation linéaire de l'altitude par triangulation du nuage de points
interpolMethod = 0

# --- valeur de Z à prendre quand le noeud n'est pas trouvé dans la région (détection de problèmes)                       
zUndef = 90

# ==== Partie Générique du traitement ====
#=========================================

import string
import sys
import salome

salome.salome_init()

from salome.hydrotools.interpolZ import interpolZ

# --- Z interpolation Z sur la bathymetrie/altimetrie aux noeuds du maillage
statz = interpolZ(nomCas, fichierMaillage, dicoGroupeRegion, zUndef, interpolMethod)

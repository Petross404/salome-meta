..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Manipulations de Polylignes
#########################################

.. |deuxSections| image:: /_static/deuxSections.png
   :align: middle

.. |deuxExtremites| image:: /_static/deuxExtremites.png
   :align: middle

.. |bringTogether| image:: /_static/bringTogether.png
   :align: middle

.. |deuxPointsConfondus| image:: /_static/deuxPointsConfondus.png
   :align: middle

.. |contourFermeDeuxSections| image:: /_static/contourFermeDeuxSections.png
   :align: middle

.. |domaineMaritime| image:: /_static/domaineMaritime.png
   :align: middle

.. |zonePont| image:: /_static/zonePont.png
   :align: middle

.. |raffinement| image:: /_static/raffinement.png
   :align: middle

.. |completeSplitDialog| image:: /_static/completeSplitDialog.png
   :align: middle

.. |zonesSplitCreees| image:: /_static/zonesSplitCreees.png
   :align: middle

.. |zonePontSplit| image:: /_static/zonePontSplit.png
   :align: middle

.. |zoneAmontSplit| image:: /_static/zoneAmontSplit.png
   :align: middle

.. |zoneAvalSplit| image:: /_static/zoneAvalSplit.png
   :align: middle

.. |mergeZonesPont| image:: /_static/mergeZonesPont.png
   :align: middle

.. |pilesDePont| image:: /_static/pilesDePont.png
   :align: middle

.. |objetsPont| image:: /_static/objetsPont.png
   :align: middle


Nous n'avons abordé jusqu'à présent que des polylignes à une seule section.
L'utilisation de plusieurs sections dans les polylignes permet de combiner des splines et des
lignes brisées dans un même contour, par exemple. Les opérations sur les polylignes
(merge, split, copy, paste) permettent, entre autres, de créer des contours partageant un bord,
avant la création du cas de calcul.

Limites de domaine maritime
===========================

Un domaine maritime est souvent limité par une ligne de côte, détaillée, et par une ligne brisée
la plus simple possible au large.

La ligne de côte détaillée est saisie sous forme d'une spline, dans une polyligne, non fermée.

La limite en mer est saisie dans une deuxième section de la même polyligne.

Pour créer la deuxième section, il faut éditer la polyligne dans laquelle on a saisi la ligne de côte,
cliquer sur *insert new section* et choisir une ligne de type *polyline* non fermée.
On valide la création de section avec le bouton *Add*.

La section 2 étant sélectionnée, le bouton *Addition mode* permet d'ajouter des points.
On crée la ligne brisée en visant approximativement les extrémités de la ligne de côte.

  |deuxSections|

Il faut maintenant faire correspondre précisément les extrémités des deux sections pour obtenir un
contour fermé.

Pour cela, on sélectionne simultanément les deux sections (touche shift)
et on passe en mode modification (bouton *modification mode*).

On sélectionne simultanément les deux premiers points d'extrémité à rapprocher,
avec une selection par boite englobante à la souris
(en ayant préalablement fait un zoom si nécessaire).

  |deuxExtremites|

Les coordonnées des deux points s'affichent, ainsi que la distance entre les deux points,
dans la colonne distance.

On notera que le bouton de droite est devenu actif (il faut sélectionner deux sections).

  |bringTogether|

En cliquant sur le bouton, les deux points deviennent superposés, leurs coordonnées sont mises à jour,
et la distance affichée est devenue nulle.

  |deuxPointsConfondus|

Il faut procéder de même avec les autres extrémités à rapprocher, puis valider avec *Apply and close*.

La polyligne représente maintenant un contour fermé composé de 2 sections de types différents.

  |contourFermeDeuxSections|

 * **remarque** : Lors de la superposition des points, l'un prend les coordonnées de l'autre.
   Il n'est pas facile de maîtriser quel point reste fixe,
   donc il vaut mieux éviter qu'ils soient trop éloignés pour ne pas déformer
   la ligne de côte.

Il faut ensuite transformer le contour en objet naturel. Nous avons notre domaine maritime.

  |domaineMaritime|

Créer des sections sur un lit mineur : barrages, ponts
======================================================

Le lit mineur d'une rivière peut être découpé en plusieurs sections, pour permettre des
traitement spécifiques. Par exemple :

 * La zone au niveau d'un pont, pour laquelle la description explicite des piles du pont
   va vraisemblablement imposer un maillage spécifique, si le reste du lit mineur est
   maillé en triangles allongés dans le sens de l'écoulement.

 * Un barrage, pour lequel on souhaite par exemple écrire des conditions limites décrivant
   son fonctionnement. Le barrage sera alors défini comme une zone "non inondable"
   ou "insubmersible" en travers du lit mineur.

 * Une zone pour laquelle on veut particulariser le maillage.

Créer une zone spécifique au niveau d'un pont sur le lit mineur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nous reprenons l'exemple précédent sur la Garonne.
Nous commençons par dessiner un rectangle en travers du lit mineur *garonne*, débordant franchement
de part et d'autre des rives. Nous nommons la polyligne correspondante *zonePont*.

  |zonePont|

**La polyligne du lit mineur doit être suffisamment raffinée pour que le découpage soit précis, et
pour éviter des problèmes plus tard à la création du cas de calcul.** Si besoin, il faut éditer la
polyligne du lit mineur pour ajouter des points de part et d'autre des traits de coupe.
**Il faut notamment éviter qu'il n'y ait pas de point entre deux traits de coupe d'une courbe spline**.

  |raffinement|

Nous utilisons alors la commande *Split polylignes* du menu contextuel de la polyligne *garonne*,
et nous prenons l'onglet *Complete split*. Il faut sélectionner la polyligne *zonePont* et cliquer
sur le bouton *include*, pour obtenir les deux polylignes dans la liste du dialogue.

  |completeSplitDialog|

Après validation par *Apply and close*, nous obtenons quatre nouvelles polylines pour décrire
le lit mineur, et quatre pour décrire la zone du pont.

  |zonesSplitCreees|

Nous allons regrouper les polylignes par paquets pour reconstituer les nouvelles zones, avec la
commande *Merge polylines*.

La zone du pont est constituée des quatres polylignes suivantes :

  |zonePontSplit|

La zone du lit mineur en amont est constituée de deux polylignes :

  |zoneAmontSplit|

La zone du lit mineur en aval est constituée de deux polylignes :

  |zoneAvalSplit|

Pour regrouper les polylignes, nous en sélectionnons une et utilisons la commande *Merge polylines*
du menu contextuel. Il faut sélectionner les quatre polylignes, cliquer sur le bouton *include*,
choisir un nom pour la nouvelle polyligne, *litMineurPont* et valider par *Apply and close*.

  |mergeZonesPont|

Nous créons de même les polylignes *litMineurAmont* et *litMineurAval*.
Les trois nouvelles polylignes servent à définir trois zones immersibles,
*litMineur_aval*, *litMineur_amont* et *litMineur_pont*.

Si nous voulons représenter les piles du pont dans le maillage, il faut les représenter en tant que
polylignes et définir des zones non immersibles.

  |pilesDePont|

  |objetsPont|

Créer une zone spécifique au niveau d'un barrage sur le lit mineur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le principe est exactement le même que pour le pont, mais sans les piles.

.. only:: html

   :ref:`ref_casParticuliers`

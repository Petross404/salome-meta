..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Mise en données TELEMAC
#########################################

.. |HYDROSolver| image:: /_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |eficas_04| image:: /_static/eficas_04.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |edit_initial_field_file| image:: /_static/edit_initial_field_file.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |genereCondlim| image:: /_static/genereCondlim.png
   :align: middle
   
.. |eficas_01| image:: /_static/eficas_01.png
   :align: middle
   
.. |eficas_02| image:: /_static/eficas_02.png
   :align: middle
   
.. |eficas_03| image:: /_static/eficas_03.png
   :align: middle
   
.. |eficas_20| image:: /_static/eficas_20.png
   :align: middle
   
.. |eficas_21| image:: /_static/eficas_21.png
   :align: middle
   
.. |eficas_22| image:: /_static/eficas_22.png
   :align: middle
   
.. |eficas_23| image:: /_static/eficas_23.png
   :align: middle

.. |salomeShell| image:: /_static/salomeShell.png
   :align: middle

.. |initialisationHauteurEau| image:: /_static/initialisationHauteurEau.png
   :align: middle

.. |choixCodeEficas| image:: /_static/choixCodeEficas.png
   :align: middle

.. |rechercheEficas| image:: /_static/rechercheEficas.png
   :align: middle

.. |initialFieldDialog| image:: /_static/initialFieldDialog.png
   :align: middle

.. |CheckBoundaryConditions| image:: /_static/CheckBoundaryConditions.png
   :align: middle

.. |CheckBoundaryConditions1| image:: /_static/CheckBoundaryConditions1.png
   :align: middle

.. |CheckBoundaryConditions2| image:: /_static/CheckBoundaryConditions2.png
   :align: middle

.. |CheckBoundaryConditions3| image:: /_static/CheckBoundaryConditions3.png
   :align: middle

Une fois le maillage généré avec l'altimétrie, il reste à définir la nature des zones de conditions limites,
les valeurs des conditions limites de débit et de hauteur d'eau au cours du temps, et l'ensemble des
paramètres physiques et numériques du calcul.
Il faut se reporter au manuel de Telemac pour la définition de ces différents paramètres.

Ces informations sont regroupées dans plusieurs fichiers de texte (ASCII) à générer ou éditer manuellement.

**Ces différents fichiers seront rangés dans le même répertoire que le maillage.**

Il faut activer le module HYDROSOLVER, via la liste défilante des modules, ou son icône dans le bandeau : |HYDROSolver|.
Le module HYDROSOLVER prend en charge la mise en donnée physico-numérique et les calculs pour les codes
Telemac et Mascaret ainsi que leur couplages.

Caractérisation des zones de conditions limites
===============================================

Les zones de conditions limites ont été définies dans les étapes précédentes,
et sont concrétisées par des groupes nommés d'éléments dans le maillage.

Le fichier *condlim.bcd* associe à chaque groupe ses caractéristiques :
entrée, sortie, paroi... Quatre valeurs entières définissent un type de condition limite.

La première ligne indique le nombre de conditions limites définies, il y a ensuite une ligne par condition limite.
Chaque ligne comprend 4 entiers (le type) et le nom du groupe.

Nous pouvons générer le fichier à partir de la commande *Edit boundary conditions file* du module *HYDROSOLVER*.

Il faut définir en entrée le chemin du fichier MED utilisé, et en sortie, le chemin du fichier des zones de conditions limites.
L'entrée *Boundary condition file* ne sert que pour lire un fichier existant.

Il faut sélectionner le type de condition limite sur les zones amont, aval, bord gauche et droit, et ne rien mettre sur les autres groupes.

  |genereCondlim|
  
Le fichier résultat ressemble à ceci :

.. literalinclude:: condlim.bcd
    :lines: 1-

* **Remarque** : Il n'est plus nécessaire de créer le fichier *.cli* prévu dans le manuel de Telemac, 
  qui liste l'ensemble des noeuds de conditions limites avec les types associés. Ce fichier est généré
  automatiquement.

Vérification que le contour est bien fermé et affichage des conditions limites
------------------------------------------------------------------------------

Il est possible de vérifier que le contour du modèle est bien fermé.

Dans *HYDROSOLVER / Hydro*, cliquer sur *Check boundary conditions*.

  |CheckBoundaryConditions|

  |CheckBoundaryConditions1|

Il faut désigner le fichier de maillage MED et le fichier des conditions limites.

Puis on a la possibilité d’afficher les frontières solides et liquides du modèle :

  |CheckBoundaryConditions2|

Ou alors d’afficher les frontières de type Télémac (débit imposé, cote imposée…) :

  |CheckBoundaryConditions3|

Valeurs des conditions limites de débit et hauteur d'eau
========================================================

Le fichier *init.lqd* donne les valeurs des conditions limites de débit et de hauteur d'eau au cours du temps.
Il faut se reporter au manuel de Telemac pour sa définition précise.

.. literalinclude:: init.lqd
    :lines: 1-

Ajout d'un champ initial dans le fichier MED
============================================

Afin d'initialiser le calcul, on peut imposer une hauteur
d'eau initiale ou une cote initiale dans les groupes de faces qui nous intéressent,
en l'occurence, ici, dans le lit mineur.

Avec une interface de saisie
----------------------------

Pour cela, on utilise le dialogue *Initial Field* du module *HYDROSOLVER*
(icone |edit_initial_field_file| ou menu *hydro / edit initial field*).

  |initialFieldDialog|
  
Après avoir sélectionné le fichier MED issu des étapes précédentes, le dialogue propose un nom de fichier MED en sortie,
placé par défaut dans le même répertoire que le fichier d'origine, et avec un suffixe *_ic*. Ce nom est éditable.

Le nom du champ à compléter est éditable, il est mis par défaut à *WATER_DEPTH*, on peut aussi utiliser *WATER ELEVATION* pour saisir une cote.

Le dialogue affiche la liste des groupes de faces trouvés, et leur affecte une valeur de champ par défaut, modifiable.
On peut changer spécifiquement la valeur d'un ou plusieurs groupes de faces.
Ici, par exemple, on a changé la hauteur d'eau initiale dans le lit mineur à 10m.

Ou par script
-------------

Pour information, on peut faire la même initialisation à l'aide d'un script Python :

.. literalinclude:: add_field_h_on_group.py
    :lines: 1-

Il suffit de remplir :

 * le nom du fichier MED,
 * le nom du maillage (dans le module SMESH) à partir duquel il a été construit,
 * les groupes de faces auxquels on souhaite ajouter une cote ou une hauteur d'eau initiale,
 * le nom du champ ajouté (*WATER_DEPTH* ou *WATER ELEVATION*).

Pour lancer le script :

 * ouvrir un SALOME shell.

  |salomeShell|

 * se placer dans le répertoire où l'on a rangé le script *add_field_h_on_group.py*,

 * exécuter le script : *python add_field_h_on_group.py*.

Le résultat
-----------

Le champ *hauteur d'eau* est ajouté au fichier MED. Si on a utilisé le script, il faut penser à enregistrer le fichier précédent
(comprenant Z et Ks) sous un autre nom pour le conserver pour réutilisation en cas de besoin.

  |initialisationHauteurEau|

Paramètres physiques et numériques du calcul
============================================

Le fichier *CasGaronne.cas* liste les autres fichiers, qui doivent être dans le même répertoire.
Il donne ensuite les différents paramètres physico-numériques nécessaires au calcul.
De même que précédemment, il faut se reporter au manuel de Telemac pour la définition de ces paramètres.

Voici la version générée avec EFICAS (voir ci-dessous l'utilisation d'EFICAS)

.. literalinclude:: CasGaronne.cas
    :lines: 1-

Et la version plus légère, ou seuls figurent les mots clefs différents de leur valeur par défaut.

.. literalinclude:: CasGaronne.Lcas
    :lines: 1-

Edition du fichier cas avec EFICAS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il existe 2 méthodes pour réaliser cette action, avec le module *HYDROSOLVER* : 

* **avec les menus :** dans le menu HYDRO, cliquer sur *Edit cas file*

  |eficas_01|

* **avec les icônes :** quand on active le module Hydrosolver, de nouveaux boutons apparaissent dans la barre d'outils.
  Cliquer à droite sur Edit cas file
  
  |eficas_02|
  
Cliquer sur New pour créer un fichier cas. 

Sélectionner le code souhaité : dans notre cas, *telemac2d*.

  |choixCodeEficas|
  
  |eficas_03|
  
Renseigner ce qui est rouge. Quand une sous-rubrique ou rubrique est complète elle passe au vert.
Ce qui est en vert est rempli par défaut mais l'utilisateur a la main dessus.
Penser à enregistrer régulièrement le cas créé. Pour cela, aller dans *File / Save* ou *Save as*, 
ou cliquer sur l'icône |eficas_04| le fichier sera enregistré en *.comm* ou *.jdc*.

Les puces vertes sont de deux couleurs :

 * Quand la valeur renseignée est la valeur par défaut, la puce est vert foncé.
 * Quand la valeur renseignée est différente de la valeur par défaut, la puce est vert clair.

Comment fonctionne EFICAS ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~

**remarque** : Pour chercher où est rangé un mot clé, aller dans le menu *Edit / Find Keyword*.

La fenêtre suivante donne l'emplacement du mot clé ainsi que la documentation associée.

  |rechercheEficas|

Quand on se place dans l'interface centrale, au niveau de la rubrique, dans la partie de droite intitulée Settings *NOM DE RUBRIQUE*,
apparaissent des mots clés facultatifs que l'on peut rajouter dans la sous-rubrique correspondante,
en double cliquant sur le carré devant le mot clé.

**Exemple :** je souhaite rajouter le mot clé *Control_section* dans *Output_Files*. Je double-clique dessus à droite :
  
  |eficas_20|

Il apparaît alors dans la sous-rubrique *Output_Files*. Si je me place dessus dans l'écran central, 
j'ai d'autres mots clés qui se présentent à moi dans la partie de droite sous le titre 
*Control_Section* que je peux rajouter de la même manière.

  |eficas_21|
  
Il est également possible d'avoir l'aide du mot clé en direct. Pour cela, il suffit de se placer sur le mot clé et l'aide apparaît :  

  |eficas_22|

Si on clique sur le mot clé avec la souris l'aide apparaît en bas à gauche :

  |eficas_23|

**Rangement des paramètres par rubriques et sous-rubriques**

Dans *Computation_Environment*, on retrouve par défaut :

 * *Initialization* : concerne les fichiers de données d'entrée comme le fichier de géométrie et le fichier des conditions limites.
   Pour prendre en compte le titre, taper le nom souhaité et faites entrer.
   
 * *Restart* : pour repartir d'un calcul précédent
 
 * *Output_files* : concerne les fichiers résultats, le listing et leurs caractéristiques.

Dans *Hydro*, on retrouve par défaut :

 * *Boundary_Conditions* : concerne les fichiers de condition limites
   (fichier des frontières liquides, fichier des courbes de tarage, cote ou débit imposé...)
   
 * *Physical_Parameters_Hydro* : concerne le frottement. L'utilisateur peut rajouter ce qui concerne les vagues,
   la météorologie, les sources, la qualité d'eau...
   
 * *Numerical_Parameters_Hydro* : concerne les équations utilisées, le traitement du système linéaire.

Dans General_Parameters, on retrouve par défaut :

 * *Debugger* : en mode debugger ou non

 * *Time* : concerne le pas de temps, durée de la simulation...
 
 * *Location* : concerne l'origine des coordonnées...

Dans Numerical_Parameters, on retrouve par défaut :

 * *Solver_Info* : concerne le solveur

 * *Discretizations_Implicitation* : concerne l'implicitation de la hauteur, de la vitesse, la discrétisation en espace...

 * *Propagation_Info*

 * *Advection_Info* : concerne le mass lumping, la compatibilité du gradient de surface libre...

 * *Diffusion* : concerne la diffusion des vitesses, l'option pour la diffusion des vitesses...

 * *Automatic_Differentiation*

 * *Advanced* : concerne le stockage de matrice, le produit vecteur-matrice...

**Développement à venir**

A terme, l'utilisateur pourra choisir parmi des "fichiers cas modèles" pré-remplis. On trouvera parmi ceux-ci : 

 * un fichier cas modèle inondation,

 * un fichier cas modèle maritime,

 * un fichier cas modèle thermique.

.. only:: html

   :ref:`ref_exempleInondation`

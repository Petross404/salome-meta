..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#############################################################
Interpretation of the SinusX format: historic (for old files)
#############################################################

File structure and line types
=============================
The ASCII file is structured in data blocks.

The lines that start with *B* are block limiters.

The lines starting with *CN* or *CP* complete the block definition.

The other lines starting with *C* are comments.

The lines that start with numbers are the block pointers.

It is possible to describe scatter plots, xyz curves, profiles and isocontours.

Depending on the format, the curves may or may not be connected and may be closed or open.
In practice, a curve that is not connected is a type of scatter plot...
The SALOME HYDRO module takes into account xyz points, curves in the XOY plane (z=0)
and curves in the XOZ plane (profiles).
Three types of processing can be used to handle curves in space (any Z, variable or not)
during import in the HYDRO module:

* Projection at z=0 to have a connected curve in the XOY plane.
* Creation of a line in the XOZ plane (curvilinear abscissa for the XOY curve, altitude).
* Import of x,y,z points that are not connected as Bathymetry/altimetry

The first import is the most important and should be done systematically.

The second is useful for xyz curves, where Z is variable.

The third is not easy to use directly in the HYDRO module, beyond the simple visualisation
of the Bathymetry/altimetry point fields in 3D. The contour of the Bathymetry/altimetry
field in the XOY plane is needed for the nodal interpolation of the altitude; however,
it makes no real linear sense. On the other, a Bathymetry/altimetry can be
reconstituted by combining the points of several lines (for example, a series of isocontours)
in a single point cloud.

Block delimiter
===============

::

  B type x1 y1 z1 dx dy dz scale

type
----

* type = S scatterplot
* type = C xyz curve
* type = P xyz profile
* type = N isocontour

x1 y1 z1 dx dy dz scale
-------------------------

Origin and deltas for conversion of measurements to metres in the table to be digitized.

This is ignored on import

examples of B-type lines
------------------------

::

  B S +4.290000E+05 +2.420000E+05 +0.000000E+00 +1.500000E+03 +2.000000E+03 +1.000
  B N +0.000000E+00 +0.000000E+00 +0.000000E+00 +1.000000E+00 +1.000000E+00 +1.000000E+00 1
  B C +0.000000E+00 +0.000000E+00 +0.000000E+00 +1.000000E+00 +1.000000E+00 +1.000000E+00 1
  B C +0.000000E+00 +0.000000E+00 +0.000000E+00 +3.200000E+04 +2.400000E+04 +1.000
  B C -3.333333E+03 +4.875000E+04 +0.000 A000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.659154
  B C -3.333330E+03 +4.875000E+04 +0.000 A000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658869

export
------

::

  B type   0.0  0.0  0.0    1.0  1.0  1.0    1.0

or, preferably, just

::

  B type


Data Block Name
===============

::

  CN the_block_name

Import
------
Replace spaces by an underscore ‘_’ (example: curve 1 -> curve_1)

Export
------

Names of the objects in the HYDRO module.  Avoid the use of spaces and accented characters in the names.

Closure connection
==================

Historical definition
---------------------

::

  CP closed connection

Two booleans indicate whether the curve is closed or not and whether the points are connected

*example*: CP 0 1: open curve, linked points.

In practice, there should be no need for unconnected points, except for the scatter plots

Interpretation proposition and export
-------------------------------------

::

  CP closed spline

Two booleans indicate whether the curve is closed and whether it is the spline or broken-line type.
The existing data sets for the closed curves and the polygons have to be corrected manually.

Definition plane
================

::

  CP PlaneNumber

The plane number is entered as 0, 1 or 2 (0: XOY plane, 1: YOZ plane, 2: XOZ plane).

Most data blocks are in the XOY plane: CP 0

Plane numbers 1 or 2 serve to define profiles / sections in a normal plane to the hydraulic axis,
with two useful coordinates. In practice, plane 2 will be used systematically.

Additional block-type related parameters
========================================

type = S scatterplot
--------------------

Not applicable

type = C xyz curve
------------------

::

  CP (16 real indicators)

Not used – not interpreted on import.

Either a compatible export is made:

::

 CP 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0

Or nothing is input here: preferable.

type = P xyz profile
--------------------

Historical definition
~~~~~~~~~~~~~~~~~~~~~

::

  CP Zref Xref dz dx

TO BE COMPLETED

Proposed interpretation for a profile in the *XOZ* plan
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In practice, this is a profile in the vertical plane with the georeferenced
positions of the two extremities given. It is assumed that the third coordinate of the points is 0.

::

  CP Xorig Yorig Xend Yend

(Xorig, Yorig) and (Xend, Yend) are the georeferenced positions of the profile endpoints.

We go from a point (x, z) to the georeferenced point (ref +x*dx, yref +x*dy, z), with (dx, dy)
the normalized horizontal vector calculated from the two georeferenced end-points.

Proposed interpretation for a profile in the XOY plan
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The points are assumed to be already georeferenced, each with three valid coordinates.

Several profiles can be grouped together in SALOME HYDRO to create a river object ('stream').

type = N isocontour
-------------------

::

  CP altitude

Altitude of the isocontour. This is the Z-coordinate value of each point.

Import: take this value as reference, ignoring the Z coordinate of individual points (FIXME: always the same value in the examples?)

Comment lines
=============

::

  C any text

Ignored on import

Export information that needs to be defined can be put in the file header, for example:

C SALOME HYDRO version xxx

C name of the SALOME study

C date

Point lines
===========

::

  X Y Z text key

key
---

Not used, ignore

text
----

A label associated with the point: not used, ignore

import
------
For isocontours, the Z value can be ignored, as it is defined at block level.

When two consecutive points have the same coordinates (which may occur with
some data sets), the second point should be ignored.

export
------
Depending on the nature of the curves, the unnecessary coordinate is set to zero.
Note: imported isocontours are converted to curves in the Z=0 plane and grouped together in scatter
plots. The isocontour information is lost in SALOME and it is not possible to export them as isocontours with the altitude entered.

For curves projected in the XOY plane::

  X Y 0.0

For curves projected in the XOZ plane::

  X 0.0 Z

For the XOY georeferenced profiles::

  X Y Z

XYZ curve example
=================

This is actually an isocontour with Z=9.0, though not defined as such.

import
------

::

  B C -3.333330E+03 +4.875000E+04 +0.000000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658992
  CN island_shore_line
  CP 1 1
  CP  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00  +0.000000E+00
  CP 0
  C
     211563.340     133489.165      9.000 A
     211604.013     133517.519      9.000 A
     211645.047     133544.734      9.000 A
     211684.597     133585.430      9.000 A
     211716.859     133584.277      9.000 A
     211777.488     133597.853      9.000 A
     211828.211     133609.896      9.000 A
     211896.886     133618.448      9.000 A
     211945.422     133633.990      9.000 A
     211996.891     133649.326      9.000 A
     212044.447     133622.576      9.000 A
     212067.681     133584.157      9.000 A
     212056.937     133521.369      9.000 A
     212007.716     133482.464      9.000 A
     211959.462     133428.999      9.000 A
     211925.576     133392.362      9.000 A
     211885.762     133376.207      9.000 A
     211817.310     133404.427      9.000 A
     211783.848     133416.811      9.000 A
     211742.545     133413.025      9.000 A
     211681.493     133421.775      9.000 A
     211644.814     133436.616      9.000 A
     211597.633     133447.736      9.000 A
     211569.826     133461.954      9.000 A


``B C -3.3...``
  block type C (xyz curve), digitization parameters ignored

``CN ...``
  block name = island_shore_line

``CP 1 1``
  closed curve, spline

``CP +0.0...``
  ignored``CP 0``

``CP 0``
  plan XoY (ça doit toujours être le plan 0)

``C ...``
  the comments are ignored

``211563.340     133489.165      9.000 A``
  x, y, z point data. The additional information is ignored.

If all the Z values of the block are zero, they are ignored.
If at least one Z coordinate of the block has a non-zero value, then
the points supply a scatter plot containing the data points of the different file curves.

export
------
A curve projected in the XOY plane in the form of an isocontour at Z = 0.
It is a closed, spline curve.

::

  B N
  CN island_shore_line
  CP 1 1
  CP 0.0
  CP 0
  C
     211563.340     133489.165     0.000
     211604.013     133517.519     0.000
     211645.047     133544.734     0.000
     211684.597     133585.430     0.000
     211716.859     133584.277     0.000
     211777.488     133597.853     0.000
     211828.211     133609.896     0.000
     211896.886     133618.448     0.000
     211945.422     133633.990     0.000
     211996.891     133649.326     0.000
     212044.447     133622.576     0.000
     212067.681     133584.157     0.000
     212056.937     133521.369     0.000
     212007.716     133482.464     0.000
     211959.462     133428.999     0.000
     211925.576     133392.362     0.000
     211885.762     133376.207     0.000
     211817.310     133404.427     0.000
     211783.848     133416.811     0.000
     211742.545     133413.025     0.000
     211681.493     133421.775     0.000
     211644.814     133436.616     0.000
     211597.633     133447.736     0.000
     211569.826     133461.954     0.000


Isocontour example
==================

Using the data from the example above, the curve type is changed: B N instead of B C
and the altitude is indicated in the header: CP 9.0.

import
------

::

  B N -3.333330E+03 +4.875000E+04 +0.000000E+00 +3.333330E+02 +1.666670E+02 +1.000000E+00 0.658992
  CN island_shore_line
  CP 1 1
  CP 9.0
  CP 0
  C
     211563.340     133489.165      9.000 A
     211604.013     133517.519      9.000 A
     211645.047     133544.734      9.000 A
     211684.597     133585.430      9.000 A
     211716.859     133584.277      9.000 A
     211777.488     133597.853      9.000 A
     211828.211     133609.896      9.000 A
     211896.886     133618.448      9.000 A
     211945.422     133633.990      9.000 A
     211996.891     133649.326      9.000 A
     212044.447     133622.576      9.000 A
     212067.681     133584.157      9.000 A
     212056.937     133521.369      9.000 A
     212007.716     133482.464      9.000 A
     211959.462     133428.999      9.000 A
     211925.576     133392.362      9.000 A
     211885.762     133376.207      9.000 A
     211817.310     133404.427      9.000 A
     211783.848     133416.811      9.000 A
     211742.545     133413.025      9.000 A
     211681.493     133421.775      9.000 A
     211644.814     133436.616      9.000 A
     211597.633     133447.736      9.000 A
     211569.826     133461.954      9.000 A


``B N -3.3...``
  block type N (isocontour), digitization parameters ignored

``CN ...``
  block name = island_shore_line

``CP 1 1``
  closed curve, spline

``CP 9.0``
  curve altitude

``CP 0``
  XoY plane

``C ...``
  the comments are ignored

``211563.340     133489.165      9.000 A``
  x, y, z point data. The additional information is ignored.

The Z point values are ignored. The scatter plots are supplied with the
curve points, with z forced to 9.0.

export
------

A curve, projected in the XOY plane in the form of a curve at Z = 0.
It is a closed, spline curve.

::

  B N
  CN island_shore_line
  CP 1 1
  CP 0.0
  CP 0
  C
     211563.340     133489.165     0.000
     211604.013     133517.519     0.000
     211645.047     133544.734     0.000
     211684.597     133585.430     0.000
     211716.859     133584.277     0.000
     211777.488     133597.853     0.000
     211828.211     133609.896     0.000
     211896.886     133618.448     0.000
     211945.422     133633.990     0.000
     211996.891     133649.326     0.000
     212044.447     133622.576     0.000
     212067.681     133584.157     0.000
     212056.937     133521.369     0.000
     212007.716     133482.464     0.000
     211959.462     133428.999     0.000
     211925.576     133392.362     0.000
     211885.762     133376.207     0.000
     211817.310     133404.427     0.000
     211783.848     133416.811     0.000
     211742.545     133413.025     0.000
     211681.493     133421.775     0.000
     211644.814     133436.616     0.000
     211597.633     133447.736     0.000
     211569.826     133461.954     0.000

Curve projected in the XOZ plane along the curvilinear abscissa.
This is an open, spline-type curve, defined by convention in the XOZ plane.

::

  B P
  CN altitude_island_shore_line
  CP 0 1
  CP 0.0 0.0 0.0 0.0
  CP 2
  C
    0.0 9.0
    ...
    x   9.0

*The ends are not georeferenced; they are defined with respect to another XOY curve.*
To distinguish the XOZ profiles that are not georeferenced from those that
are (by endpoints), a specific value with null coordinates is used for the CP field::

  CP 0.0 0.0 0.0 0.0

The x values of the curvilinear abscissae are expressed in metres along
the curve or normalized between 0 and 1. **To be reviewed**.

Summary
=======

In the SALOME HYDRO module, when existing SinusX files are imported:

- scatter plots are imported as Bathymetries.
- isocontours are converted to curves at Z=0 and non-georeferenced profiles
  profiles in the XOZ plane (curvilinear abscissa, z).
- XYZ curves are converted to curves at Z=0 and profiles that are not
  georeferenced in the XOZ plane (curvilinear abscissa, z).
- XOZ profiles georeferenced by endpoint coordinates are converted into
  profiles whose points are all defined by their X, Y, Z coordinates (XOY profiles).
- XOZ profiles that are not georeferenced are imported as is (used as a line
  of altitude for another line in the XOY plane or as a section for a channel, embankment, etc.)
- the XOY profiles are kept as such (all points are defined by their respective
  X, Y, Z coordinates).
- All the point data of the isocontours (with non-zero Z values) and xyz
  curves are used to create a Bathymetry field.

During file export,

- the polylines are exported as zero-level isocontours, open or closed, spline
  or broken line. (What is done with the sections? Use a beginning of section comment on the point?)
- the profiles are exported as such (to be developed)
- Should bathymetries be exported in SinusX format or in xyz format?
  (only differs by the presence of a header line)
- Dans le module HYDRO de SALOME, lors de l'importation des fichiers SinusX existant,


.. only:: html

   :ref:`ref_english_formatsSpecs`

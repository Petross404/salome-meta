..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Creation / modification of polylines
#########################################

.. |contour_garonne| image:: ../_static/contourGaronne.png
   :align: middle

.. |contour_lit_majeur| image:: ../_static/contourLitMajeur.png
   :align: middle

.. |icone_polyligne_xy| image:: ../_static/icon_polyline_xy.png
   :align: middle

.. |createPolyligneDialogue1| image:: ../_static/createPolyligneDialogue1.png
   :align: middle

.. |insertNewSection| image:: ../_static/insertNewSection.png
   :align: middle

.. |createPolyligneDialogue2| image:: ../_static/createPolyligneDialogue2.png
   :align: middle

.. |addition_mode| image:: ../_static/additionMode.png
   :align: middle

.. |ajoutPointsPolyligne| image:: ../_static/ajoutPointsPolyligne.png
   :align: middle

.. |modification_mode| image:: ../_static/modificationMode.png
   :align: middle

.. |undoPoly| image:: ../_static/undoPoly.png
   :align: middle

.. |modificationPolyligne2| image:: ../_static/modificationPolyligne2.png
   :align: middle

We are going to create three lines by drawing on the maps and the bathymetry.

* A closed contour representing a portion of the Garonne (extending beyond the study domain defined later)
  represented by the dark blue surface in the following image:

  |contour_garonne|

* Another closed contour on a portion of the major bed of the river (still extending beyond the study domain
  both upstream and downstream), corresponding to the bathymetry field garonne_point_L93.

* A closed contour in broken lines that will delineate the boundaries of our study domain.

The contour of the major bed and the study domain:

  |contour_lit_majeur|

Drawing the river banks
=======================

The context menu |icone_polyligne_xy| of the *POLYLINES* folder in the SALOME study tree is used to create a polyline.

The first section of the creation dialog box is used to name the polyline.
It is important to give the objects meaningful names as a large number of them may be need to be handled.

* **remark** : do not use accentuated characters in object names: this may induce problems, fr instance in dump Python scripts.

They can always be renamed later:

  |createPolyligneDialogue1|

The polylines may comprise several sections. We will only be using one, which has to be created:

  |insertNewSection|

In the dialog box related to the section, choose a spline-type, closed line and click on the add button.

  |createPolyligneDialogue2|

 * **Note**: There are two types of polyline:

   * *spline*: a single continuous line, with derivative continuity passing through all the points.
     This type of line is privileged when creating curved lines; the control points will not be reused in the mesh,
     resulting in more flexibility control of the mesh refinement.

   * polyline: a single continuous line, composed of straight segments. This type of line must be used for artificial
     objects comprising straight segments and whenever broken lines are needed. The points are kept in the mesh.

Once the section has been created, points must be added by switching to the addition mode:

  |addition_mode|

Now click on the map, starting at one end **(upstream or downstream of the domain)** and follow the contour of the river.
Each click adds a point and the closed contour is progressively drawn. One or more of the last points can be deleted
with the undo / redo buttons:

  |undoPoly|

 * **Note**: **The choice of where to start a closed line is important: the corresponding point is the only mesh-imposed
   point. In the case of a river, it is best to put this point outside the domain.**

  |ajoutPointsPolyligne|

Continue to add points as needed until you arrive back at the starting point, having represented the two banks.
There is no need to add too many points, unless the riverbank is deeply incised and these details need to be captured.
For example, about sixty points are required along the river portion of interest in this case.

When the entry is finished, confirm the created section by clicking *apply and close*.

Modifying and editing polylines
-------------------------------

If the result is not satisfactory, the polyline can be modified through the associated context menu,
by first selecting the section and afterwards clicking on modification mode:

  |modification_mode|

It is then possible to:

 * Select a single point by clicking on it,

 * Select several points by enclosing them in a selection box,

 * Create an intermediate point by clicking on the line, between two points.

The selected point(s) can be moved using drag-and-drop (left button click and hold down while moving).

They remain selected until another click or selection is made.

The Undo button can be used, if needed, to cancel the latest changes.

The coordinates of the selected points are displayed in the dialog box as editable text.

  |modificationPolyligne2|

Two lines appear when editing: a precise, solid black line that is not recalculated each time an edit is made
and a red line that changes with each modification. The latter is less precise but is redrawn more quickly
(This is an important factor when lines containing a "large" number of points need to be modified).

The line can be recalculated from within the dialog box by clicking the *apply* button.
If you wish to continue the modifications, the section can then be reselected with a double click,
followed by *OK* or *Cancel*.

 * **Note**: In edition mode, double clicking on the section provides options to change the segment type,
   from spline to polyline or vice versa, and the closure.

Drawing the contour of the major bed and the study domain
=========================================================

Once the river has been drawn, the next step is to draw the contour of the major river bed bathymetry,
*garonne_point_L93*, then define the outside contour of the computational domain,
which must cut the other two lines upstream and downstream.

**It is preferable for the computational domain to cut the river perpendicular to its axis,
in order to correctly define the upstream and downstream boundary conditions.
The creation of acute angles at the boundary of the domain should be avoided, as they increase the risk
of over-constrained triangles appearing when the mesh is created**
(this problem that can be corrected by diagonal detection and inversion).

.. only:: html

   :ref:`ref_english_exempleInondation`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Land Cover Map
#########################################


.. |stricklerTable_1| image:: ../_static/stricklerTable_1.png
   :align: middle

.. |importLandCoverMap| image:: ../_static/importLandCoverMap.png
   :align: middle

.. |importLandCoverMap_2| image:: ../_static/importLandCoverMap_2.png
   :align: middle

.. |importLandCoverMap_3| image:: ../_static/importLandCoverMap_3.png
   :align: middle

.. |casLandCoverMap| image:: ../_static/casLandCoverMap.png
   :align: middle

.. |BottomFriction| image:: ../_static/BottomFriction.png
   :align: middle


Import of a Land Cover Map
===========================

The previous example "garonne_1" is taken to add a map of the Strickler coefficients.
The map can be downloaded and modified in qgis. The *Corine Land Cover* overlap a large territory
and are very detailed.

**Note:** To simplify handle of Corine Land Cover maps in SALOME-HYDRO,
It is recomended to cut them into qgis in order to restrict them as much as possible to the field of study.
We will define a cutting polygon covering the field of study, making sure that no zone is lost when cutting
(A cuted map is provided with this tutorial). The map includes a database which gives, for each zone,
the associated land type code, according to the *Corine Land Cover* nomenclature.

Before importing the map, you must have a table defining the *Corine Land Cover* codes, the colour representation
of the zone types and the associated Strickler coefficients.

A default table is available in SALOME-HYDRO, visible in the study tree, in the *STRICKLER TABLES* section, with the name
*Strickler table_1*. The table can be edited with the context menu *Edit Strickler table*.
Most fields are editable,
**in particular the Strickler coefficients that will have to be adapted in any case for a realistic study**.

  |stricklerTable_1|

The *Attribute Name* field must match the field type field in the database associated with
the imported *Corine Land Cover*: *CODE_06* corresponds to the 2006 maps, *CODE_12* to the 2012 maps.
This field is not editable in the above dialog box. FIXME: pas clair

In the example provided, the map is from 2006, another Strickler table is needed.
These tables are saved in editable text files.
We load a 2006 table provided in the installation directory of the SALOME HYDRO application
With the *Import Strickler* command from the context menu of the topic *TABLES STRICKLER* ::

  <appli_xxx>/share/salome/resources/hydro/def_strickler_table_06.txt

You must delete the previous table *Strickler table_1*, so that the new table is correctly used
When importing the Land Cover Map (context menu *Delete*).

We can now load the Land Cover Map.
To do this, we use the *Import land cover map from file (s)* command in the context menu section
*LAND COVER MAPS* ::

  <appli_xxx>/bin/salome/test/HYDRO/HYDRO/CLC_decoupe.shp

The map already contains a large number of black polygons (more than 600) whose outline can be seen by selecting them.
FIXME: La découpe déborde très largement de la zone d'étude. On voit les zones "oubliées" lors de la découpe dans qgis,
sur les bords. Elles sont hors de la zone d'étude.

  |importLandCoverMap|

In practice, we select all polygons (<crtl> A in the list), before pressing the *Next>* button.
There are several attributes found in the imported database. We select the attribute that interests us,
the zone type: *CODE_06*, before pressing the *Next>* button.
The zone codes are well associated with their definition provided in the new Strickler table.

  |importLandCoverMap_2|

We press the *Finish* button. The treatment takes few seconds.
The Land Cover Map is not displayed automatically: *show* command in the context menu of the object *CLC_decoupe*
In the *LAND COVER MAPS* section. One can see the calculation under the card, in transparency.

  |importLandCoverMap_3|

Creation, edition of Land Cover Map
===================================

It is possible to create a Land Cover Map from scratch, using polygons created from SALOME-HYDRO
or imported.

The context menu of the Land Cover Map offers options for editing zones:
adding, removing, cutting, grouping, changing the type.

We do not detail these operations here.

Use of Land Cover Map in calculation case
=========================================

To use the Land Cover Map, the computation case must be edited.
We edit the *garonne_1* calculation case with the *edit calculation case* command in the context menu of the calculation case.
You must go to the *Land Cover map* tab with the *Next>* buttons and select the Land Cover Map and
table of Strickler

  |casLandCoverMap|

We continue with the *Next>* and *Finish* buttons to validate the case.

Creating a Strickler field at mesh nodes
========================================

TELEMAC can use a field of Strickler coefficients at the nodes of the mesh. This field has the name *BOTTOM FRICTION*.
This field is added to the MED file of the mesh, such as the elevation field at the nodes.

You have to manually adapt the script below:

It is necessary to copy the script below and to adapt it according to the case of computation
and file names for input and output.
It is possible to use the same MED file as input and output.

.. literalinclude:: ../interpolStrickler.py
    :lines: 1-

We can see the result field with the MED module, as for the elevation field.

  |BottomFriction|

.. only:: html

   :ref:`ref_english_casParticuliers`

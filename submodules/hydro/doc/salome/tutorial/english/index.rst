..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SALOME HYDRO Tutorial
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. |Hydro_module_button| image:: ../_static/HYDRO.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |hydrosolver_module_button| image:: ../_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

SALOME HYDRO is an open integration platform devoted to hydraulic studies with the TELEMAC and MASCARET
codes in the field of free surface flows. The platform is based on the SALOME generic modules
(geometry and mesh creation, monitoring and automated management of computations, post-processing, etc.)
to which two specific task modules have been added:

  * HYDRO |Hydro_module_button|: acquisition of field data (contours, bathymetry, etc.),
    construction of the geometry for the calculation case, identification of the boundary condition regions, etc

  * HYDROSOLVER |hydrosolver_module_button|: definition of the physical model and numerical parameters of the
    simulation (calculation case) parameters, computational steering.

The tutorial provides a step-by-step guide to a carrying out a case study on river flood inundation.
The practical exercise is drawn from a real example for which plenty of data is available.

.. _ref_english_installationLancementSalome:

###################################
Introduction to SALOME
###################################

This section briefly recalls the procedure for installing and running SALOME.

.. toctree::
   :numbered:
   :maxdepth: 3

   english_installationLancementSalome.rst

.. _ref_english_notionsPrealables:

###################################
A few preliminary concepts
###################################

The different steps involved in a SALOME-HYDRO study are described herein and the necessary concepts introduced.
This section also contains a series of recommendations and pitfalls to be avoided.

This is followed by a simple exercise enabling to discover these concepts through practical experience.

.. toctree::
   :numbered:
   :maxdepth: 3

   english_preliminaires.rst
   english_piegesAEviter.rst
   english_donneesPrealables.rst

.. _ref_english_outilsComplementaires:

###################################
SALOME Complementary tools
###################################

qgis et CloudCompare are provided with SALOME-HYDRO.

.. toctree::
   :numbered:
   :maxdepth: 3

   english_introQgis.rst
   english_introCloudCompare.rst


.. _ref_english_exempleInondation:

###################################
A simple example of flooding
###################################

This exercise covers the full procedure for conducting a flood study on a simple problem.

.. toctree::
   :numbered:
   :maxdepth: 3

   english_import.rst
   english_importBathy.rst
   english_polylignes.rst
   english_objetsNaturels.rst
   english_casCalcul.rst
   english_geometrie.rst
   english_maillage.rst
   english_interpolationZ.rst
   english_miseEnDonneesTelemac.rst
   english_lancementCalcul.rst
   english_depouillementCalcul.rst

.. _ref_english_casParticuliers:

###################################
Some specific cases
###################################

This chapter introduces a few concepts that were not addressed in the previous simple example.

.. toctree::
   :numbered:
   :maxdepth: 3

   english_streams.rst
   english_objetsArtificiels.rst
   english_manipsPolys.rst
   english_landCoverMap.rst

.. _ref_english_formatsSpecs:

###################################
Formats and specifications
###################################

A few import-export formats are introduced herein.
The first SinusX format documentation corresponds to the original use of the format and gives a transposition for SALOME use.
**The second SinusX format documentation provides the precise specification for the SinusX format in SALOME HYDRO. The latter documentation constitutes the reference for SALOME HYDRO.**


.. toctree::
   :numbered:
   :maxdepth: 3

   english_format_sinusx.rst
   english_sinusX_Format.rst


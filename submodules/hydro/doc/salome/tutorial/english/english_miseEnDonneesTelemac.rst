..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
TELEMAC setup
#########################################

.. |HYDROSolver| image:: ../_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |eficas_04| image:: ../_static/eficas_04.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |genereCondlim| image:: ../_static/genereCondlim.png
   :align: middle

.. |eficas_01| image:: ../_static/eficas_01.png
   :align: middle

.. |eficas_02| image:: ../_static/eficas_02.png
   :align: middle

.. |eficas_03| image:: ../_static/eficas_03.png
   :align: middle

.. |eficas_20| image:: ../_static/eficas_20.png
   :align: middle

.. |eficas_21| image:: ../_static/eficas_21.png
   :align: middle

.. |eficas_22| image:: ../_static/eficas_22.png
   :align: middle

.. |eficas_23| image:: ../_static/eficas_23.png
   :align: middle

Once the mesh is generated with the altimetry data, it remains to define the nature of the boundary condition regions,
the values of the flow rate and water depth boundary conditions over time and all the physical and numerical model parameters.
The TELEMAC manual should be consulted for the parameter definitions.

This information is compiled in several text files (ASCII) that are now going to be generated or manually edited.

**The abovementioned files will be stored in the same directory as the mesh.**

You must activate the HYDROSOLVER module, through the scrolling list of imodules, or its icon in the banner: |HYDROSolver|.
The HYDROSOLVER module supports physico-numerical data setup and calculations for codes
TELEMAC and MASCARET and their couplings.

Characterization of boundary regions
====================================

The boundary condition regions were defined in the previous
steps and are incorporated by named groups of mesh elements.

The *condlim.bcd* file associates specific types of boundary condition (inlet, outlet, wall, etc.) to each group.
Four integer values define a boundary condition type.

The first line indicates the number of boundary conditions defined and is followed by four lines:
one per boundary condition. Each line contains four integers (the type) as well as the name of the group.

This file caan be created with de command  *Edit boundary conditions file* from *HYDROSOLVER* module.

The path of the MED file used must be defined as input, and the file path of the boundary condition fields as output.
The *Boundary condition file* entry is used only to read/edit an existing file.

You must select the boundary condition type on the upstream, downstream, left and right bank,
and do not put anything on the other groups.

  |genereCondlim|

The result file looks like this:

.. literalinclude:: ../condlim.bcd
    :lines: 1-

* **Note**: It is no longer necessary to create the .cli file specified in the Telemac manual,
    which lists all the boundary condition nodes with the types associated. This file is automatically generated.

Values of the discharge and water elevation boundary conditions
===============================================================

The *init.lqd* file contains the values of the flow rate and water depth boundary conditions over time.
Reference should be made to the Telemac manual for more precise details on the file definition.

.. literalinclude:: ../init.lqd
    :lines: 1-

Physical and numerical parameters of the simulation
===================================================

The *init.cas* file lists the other files, which must be in the same directory.
It then gives the different physical and numerical parameters required for the simulation (calculation case).
As mentioned before, the Telemac manual should be consulted for the definition of these parameters.

* **Note**: for version 1.0 (March 2016), the presence of a Fortran file is compulsory, even if it only contains one comment line
  (it is necessary to force the recompilation of the TELEMAC executable). In the init.cas file,
  the comment in front of the FORTRAN FILE keyword must be deleted and the corresponding file created.

.. literalinclude:: ../init.cas
    :lines: 1-

Steering file setup through EFICAS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are 2 methods to perform this, with the module *HYDROSOLVER*:

* **with the menus:** in HYDRO menu, click on *Edit cas file*

  |eficas_01|

* **with the icons:** When the Hydrosolver module is activated, new buttons appear in the toolbar.
  Right click on Edit case file

  |eficas_02|

Click New to create a case file.

  |eficas_03|

Complete all red items. When a sub-heading or heading is complete it changes to green.
Some green items are setup with the default value but the user can overwrite them.
Save (regularly) the created case. To do this, go to *File/Save* or *Save as*
or click on the icon |eficas_04|.
The file will be saved as *.comm* or *.jdc*.

How EFICAS works?
~~~~~~~~~~~~~~~~~

In thecentral interface, at the level of the heading, in the right-hand section entitled Settings *NOM DE RUBRIQUE*,
optional keywords appear and can be added to the corresponding sub-heading,
by double-clicking the square box in front of the keyword.

**Example:** To add the keyword *Control_section* in *Output_Files*. Double-click on the right:

  |eficas_20|

It appears in the *Output_Files* sub-section. In the central screen, other keywords appears in the right part under the title
*Control_Section*. I can add them in the same way.

  |eficas_21|

It is also possible to have the help of the keyword. To do this, simply move the mouse over the keyword and the help appears:

|eficas_22|

If you click on the keyword with the mouse the help appears at the bottom left:

  |eficas_23|

**Parameters by section and sub-section**

In *Computation_Environment*, by default, we find:

 * *Initialization*: concerns input data files such as the geometry file and the boundary conditions file.
    To take the title into account, type the desired name and enter.

 * *Restart*: to restart from a previous computation.

 * *Output_files*: result files, listing and their options.

In *Hydro*, by default, we find:

 * *Boundary_Conditions*: boundary condition files
   (files for liquid boundaries, stage-discharge curves, prescribed elevation or flowrate…)

 * *Physical_Parameters_Hydro*: Physical parameters for hydrodynamics. Waves, meteo, source terms…

 * *Numerical_Parameters_Hydro*: Numerical parameters for hydrodynamics, linear system options.

In *General_Parameters*, by default, we find:

 * *Debugger*: debugger mode or not.

 * *Time*: time step, duration…

 * *Location*: coordinates origin…

In *Numerical_Parameters*, by default, we find:

 * *Solver_Info*: solver setup

 * *Discretizations_Implicitation*: implicitation for depth, velocity, space discretization…

 * *Propagation_Info*

 * *Advection_Info*: mass lumping, free surface gradient compatibility…

 * *Diffusion*: velocity diffusion, option for velocity diffusion…

 * *Automatic_Differentiation*

 * *Advanced*: matrix storage, matrix-vector product…

**Developement in progress**

In the end, the user can choose from pre-filled "template case files". These include:

  * A flood model case file,

  * A maritime model case file,

  * A thermal case model file.

.. only:: html

   :ref:`ref_english_exempleInondation`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
"Natural" objects – immersible zones
#########################################

.. |createImmersibleZone| image:: ../_static/createImmersibleZone.png
   :align: middle

.. |selectColor| image:: ../_static/selectColor.png
   :align: middle

.. |zonesImmersibles| image:: ../_static/zonesImmersibles.png
   :align: middle

.. |changeLayerOrder| image:: ../_static/changeLayerOrder.png
   :align: middle

.. |zoneSubmersible| image:: ../_static/zoneSubmersible.png
   :align: middle

The contours created thus far are for "natural" objects, as against "artificial" objects.
The latter classification corresponds to the use of altitude mode for computing the Z-elevation
at all points on their surfaces.

Natural objects correspond to surfaces whose Z-dimension is determined by a bathymetry/altitude field.

Examples of artificial objects are embankments, canals or channels, which will be represented by a constant
cross-section, extruded along a line whose altitude could potentially vary.

There are two sub-categories of natural objects: "immersible" zones associated to a point cloud of the
bathymetry field /altitude type (.xyz or .asc) and "stream" zones which correspond to rivers described
by a series of cross profiles. These profiles constitute a bathymetry field on which a specific interpolation
method will be applied to calculate the Z-dimension.

In this exercise, three immersible zones will be created, defined by a contour and a bathymetry/altitude field:

 * The minor bed, defined by the *garonne* contour and the bathymetry field *garonne_point_L93*,

 * The major bed, defined by the contour of the *garonne_point_L93* field and the field itself,

 * The study domain, defined by its contour, and the *cloud_02* field.

The context menu of the *NATURAL OBJECTS* folder is used to create an immersible zone.

  |createImmersibleZone|

The dialog box proposes defining the zone name and selecting the contour and the bathymetry from the drop-down
lists of existing objects. The choices must be confirmed with *apply and close* to create the zone.

Once created, the colour of the zone can changed using the context menu associated to the object name.

  |selectColor|

Displaying all three zones should provide a view resembling the one below:

  |zonesImmersibles|

It may be necessary to re-order the display layers: the *Change Layer* Order option in the context menu
of the graphic view permits to set the object order. This setting will be remembered when the study is saved:

  |changeLayerOrder|

The context menu associated with each immersible zone contains a *Submersible* parameter that is checked by default
(icon pushed in). If the user wants to create an island in the TELEMAC sense, i.e. a non-floodable zone,
which will create a hole in the mesh, this parameter has to be unchecked.

  |zoneSubmersible|

.. only:: html

   :ref:`ref_english_exempleInondation`

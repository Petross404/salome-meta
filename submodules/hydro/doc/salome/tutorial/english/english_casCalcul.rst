..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Calculation case set-up
#########################################

.. |createCalculationCase1| image:: ../_static/createCalculationCase1.png
   :align: middle

.. |createCalculationCase2| image:: ../_static/createCalculationCase2.png
   :align: middle

.. |createCalculationCase3| image:: ../_static/createCalculationCase3.png
   :align: middle

.. |createCalculationCase4| image:: ../_static/createCalculationCase4.png
   :align: middle

.. |createCalculationCase5| image:: ../_static/createCalculationCase5.png
   :align: middle

.. |createCalculationCase6| image:: ../_static/createCalculationCase6.png
   :align: middle

.. |createCalculationCase7| image:: ../_static/createCalculationCase7.png
   :align: middle

.. |createCalculationCase8| image:: ../_static/createCalculationCase8.png
   :align: middle

.. |createCalculationCase9| image:: ../_static/createCalculationCase9.png
   :align: middle

.. |createCalculationCase10| image:: ../_static/createCalculationCase10.png
   :align: middle

Case creation
=============

This step consists in creating a *calculation case*, i.e. the configuration of objects that will be retained
in the example study.

Three objects have been created: the minor bed, the major bed and the study domain.

To create a *calculation case*, open the context menu of the *CALCULATION CASE* folder
and name the case garonne_1 in the dialog box.

  |createCalculationCase1|

**Select the domain as the limit**, then click on **manual mode**
(this mode will be explained below) and select the three objects present and include them in the case.

This step must be confirmed by pressing the *next* button.

  |createCalculationCase2|

The dialog proposes to select the groups to be preserved: these are the contours of the different objects
taken into account in the case. These groups may prove useful later to define the boundary conditions
or to control the mesh. By keeping them, they can be found under their names in subsequent steps of the calculation.

Select the three available groups and include them in the case.

The *next* button should then be clicked to confirm this step.

  |createCalculationCase3|

The dialog box below enables the assignment of friction coefficients to the different domain zones,
using a Strickler coefficients table and a *land cover map* partitioning the study area into zones,
both of which need to have been previously imported and/or edited.
This step is omitted in the context of the present simple study; it will, however, be expanded upon later.

The instructions must be confirmed by pressing the *next* button.

  |createCalculationCase4|

Choice of Bathymetry calculation for the overlapping zones
==========================================================

The following step shows the result of the *partition* of the different zones retained in the calculation case.
The original surfaces were cut along the overlapping lines.

The panel on the right shows as many cut *regions* as cut faces: each region contains one a cut face or *zone*.

Certain zones are coloured red: these zones are the overlap zones of the original faces.
The list of initial objects appears in the second column.

Several bathymetry definitions are possible for these zones, as listed in the third column.
Until a rule has been defined to calculate the zone bathymetry, this zone is declared to be *in conflict*.

  |createCalculationCase5|

Each conflict zone must be selected, one after the other, to choose the bathymetry mode for the zone.
The selection is made from the drop-down menu associated with the zone.

The following options can be selected:

 * A bathymetry among those listed

 * The local minimum of all listed bathymetries

 * The local maximum of all listed bathymetries

  |createCalculationCase6|

There should be no red zones remaining once the conflicts have been resolved.

The zone colours can be regenerated (*regenerate colors* button), if necessary.

  |createCalculationCase7|

Grouping zones into regions
===========================

The next step is to group several zones into separate regions.

What purpose does this serve? A zone corresponds to a computational mode for the bathymetry.
The contour of this zone is not necessarily of interest as a *mesh constraint line*.
When several adjacent zones are grouped together into one region, only the contour of the region
will be kept as a *mesh constraint line*.

Hence, group the two left bank zones together and do likewise with the two zones on the Right bank,
so as to obtain three regions.

To move a zone, drag and drop the zone name from one region to another (in the right panel).

  |createCalculationCase8|

This last step must be confirmed by clicking on the *Finish* button.

The case now appears in the study tree. Select each region and give it a meaningful name (context menu).

It can be helpful, when renaming a region, to look at the zones it contains, which are displayed in the graphic view.
By expanding the study tree for each region, the zones can be selected and then highlighted.

As a rule, there is no need to rename the zones themselves, just the regions.
This is because the regions, identified by name, will subsequently be used.
A real case study could contain several dozens of regions.

  |createCalculationCase10|

.. only:: html

  :ref:`ref_english_exempleInondation`


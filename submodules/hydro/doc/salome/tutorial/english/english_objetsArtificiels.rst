..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Artificial objects
#########################################

.. |axeDigue| image:: ../_static/axeDigue.png
   :align: middle

.. |creationDigue_1| image:: ../_static/creationDigue_1.png
   :align: middle

.. |modifModeProfile| image:: ../_static/modifModeProfile.png
   :align: middle

.. |creationDigue_2| image:: ../_static/creationDigue_2.png
   :align: middle

.. |creationDigue_3| image:: ../_static/creationDigue_3.png
   :align: middle

.. |altitudeDigue| image:: ../_static/altitudeDigue.png
   :align: middle

.. |polyline3D| image:: ../_static/polyline3D.png
   :align: middle

.. |artificialDigue| image:: ../_static/artificialDigue.png
   :align: middle

.. |zoomDigue| image:: ../_static/zoomDigue.png
   :align: middle

Artificial objects cover embankments and channels. Both object types are constructed on the same principle:

 * A constant vertical section,

 * A 3D axis, this being a 3D polyline from which the section will be extruded.

The 3D axis is constructed from a horizontal polyline, to which will be associated a vertically-defined line
of altitude, which gives the Z-value as a function of the curvilinear abscissa of the polyline’s horizontal path.

The altitude line is either explicitly constructed or obtained by projecting the horizontal polyline
onto a bathymetry field.

When the node altitudes are computed (Z interpolation), a specific treatment is applied to the nodes on the
embankment: the altitude of the node is evaluated on the 3D geometric shape of the embankment, as defined above.

Creating an embankment
======================

To define the embankment's horizontal axis, an open, spline-type polyline is created with the polyline editor
(context menu of the *POLYLINES* folder in the SALOME study tree).

Assign the name axeDigue (embankmentAxis) to this polyline.

  |axeDigue|

The embankment section is created using the *Create profile* context menu of the *PROFILES* folder
in the SALOME study tree.

This section is named *sectionDigue* (embankmentSection).

The approximate shape of the section has to be drawn freehand;
it will later be edited to put in the exact elevation parameters.

  |creationDigue_1|

To correct the node coordinates, you need to switch to Modification mode:

  |modifModeProfile|

The nodes can be block selected by grabbing them in a selection rectangle, in the graphic view.

They are displayed in a table above the graphic view.

The nodes are reordered by clicking on the *Index* column title.

  |creationDigue_2|

For the choice of Z-values, it is essential to know that the final altitude of a point on the embankment is obtained
from the cross-section at this point, by adding the elevation of the point on the section to the elevation of the
section along the altitude profile. To be more specific, the calculation required is as follows:

 * Point **A** (x,y,0), whose altitude is required, is projected onto the *axeDigue* curve at a point **P** (x’,y’,0)
   such that the straight line *AP* is perpendicular to the tangent to the curve of the embankment axis at **P**.
   The point **P** is at a horizontal distance *d* = distance(A,P) from the *axeDigue* curve.

 * From the curvilinear coordinate **P** on *axeDigue*, we get a value of Z0 value on the line of altitude.
   This Z0 value corresponds to the zero-elevation level of the embankment section.

 * To obtain the final altitude in **P**, the elevation Z1 on the section is calculated at the abscissa point *d*.
   The final altitude is Z = Z0 +Z1.

 * **Note:** this calculation method assumes a symmetrical cross-section with respect to x=0.

In this case, the line of altitude that will be defined corresponds to the vertex (i.e. top) of the cross-section. Hence, we create a symmetrical cross-section that is 20 metres wide, with fairly steep slopes.

  |creationDigue_3|

The embankment line of altitude still has to be created.
It will be defined explicitly with the altitude of the two ends.

If the elevation of the embankment is variable, an approximate idea of its length is needed
in order to construct an accurate altitude profile (a display function of the polyline lengths is missing).

If more than two points are defined, the altitude is linearly interpolated between two points and,
if the curve is longer than the altitude line, the Z values beyond the end point are taken
at the elevation of this last point.

  |altitudeDigue|

The 3D axis has to be created next using the *Create  polyline 3D* context menu of the POLYLINES 3D folder.

  |polyline3D|

The last step uses the *Create digue* context menu of the *ARTIFICIAL OBJECTS* folders.

  |artificialDigue|

The equidistance parameter serves to optimise the precision of the section extrusion along the axis.

 * If the value is too high in relation to the radius of axis curvature, the cross-section
   is offset from its centre line during extrusion.

 * If the value is too low, the computational time becomes long.

Taking a value of about two or three times the width of the embankment is a reasonable first approximation.

The views of the embankment and its axis should then be overlaid to verify the result.

  |zoomDigue|

When the calculation case with the embankment is set up, the latter should be isolated in a specific region
because it is preferable to mesh it in quadrangles using the *Quadrangle (Medial Axis projection)* algorithm.

As the embankment is overlaid on natural terrain, the *ZMAX* option is generally the most logical choice for the altitude calculation.

Creating a channel
===================

The canal channel is created in exactly same the same way as the embankment but using a specific menu,
*Create channel*, in the *ARTIFICIAL OBJECTS* folder.

The section is bowl-shaped and the altitude is calculated using the same approach as for the embankment.

When the calculation case with the channel is being set up, the channel should be isolated in a specific region
as it is preferable to mesh it in quadrangles using the Quadrangle (Medial Axis projection) algorithm.

For the altitude calculation, with the channel being in natural terrain,
the *ZMIN* option is the most logical choice in most cases.

.. only:: html

   :ref:`ref_english_casParticuliers`

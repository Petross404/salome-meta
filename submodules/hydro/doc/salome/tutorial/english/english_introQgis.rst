..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Run Qgis and CloudCompare
#########################################

Starting from SALOME 8, the installer of qgis and / or CloudCompare is provided in a separate product.
At EDF the installer depends on the platform:

* On Calibre 7 qgis and CloudCompare are provided separately,
* On Calibre 9, CloudCompare is installed with SALOME, qgis aside.

Knowing the installation directory of the products (*<appli_XXX>*) ::

  <appli_XXX>/salome shell CloudCompare

ou::

  <appli_XXX>/salome shell qgis

For CloudCompare on Calibre 9, you can also use the *tools / plugins / Salome shell session* menu from SALOME
to open a shell in the SALOME environment.

In this shell, the *CloudCompare* command launches the software.

#########################################
Basic help on qgis
#########################################

Some indications of the use of qgis are given below.
These are some basic functions for displaying and processing rows or polygons.
For more advanced uses and geographic data manipulation and visualization, the user can refer to
the official qgis documentation <http://www.qgis.org/fr/docs/index.html> or specific existing documentation.

Setup Internet access from qgis (specific EDF R&D)
==================================================

Internet access is needed in many uses of qgis, for example to install plugins.
To setup EDF R&D proxy configuration, in the *settings / options* menu of qgis,
*Network* tab, check the option *Use proxy for web access* and take *DefaultProxy* as *Proxy type*.

You also must add the following lines to the file *.bashrc* from its Linux account:

::

  export http_proxy=http://proxypac.edf.fr:3128
  export https_proxy=http://proxypac.edf.fr:3128
  export no_proxy=localhost,.edf.fr

Open a qgis projet
==================

To open or create a qgis project, go to the *Project* menu and select *Open or New*.
GIS data consists of several types of files.

Show dada in qgis
=================

A "Shapefile" is the format for storing the vector data used
by most GIS, it is made from the following files:

 * ***.shp**: stores geographic entities. This is the shapefile itself.

 * ***.dbf** (DataBaseFile): stores the attribute data (available in Excel).

 * ***.shx**: stores the record indexes of the ".shp" file.

 * ***.prj**  (recommanded): stores the associated projection.

 * ***.sbn**, ***.sbx** (faculatatifs): FIXME: stocke des index n'existant qu'après une requête ou une jointure.

 * ***.xml** (facultatif): stores metadata for shape.

A **vector** data file is make with points or contours while a **raster** file
is made up of pixels, that means that it is an image or a photography as background.

To load existing data (for example, CorineLandCover data),
Click the *AddVectorLayer* button in the left button bar or select *AddLayer*
then *Add Vector Layer* from the *Layer* menu.

To view data, you will find in the "View" menu all the necessary tools
to explore by moving or zooming in your maps.

Displacements on the map are also accessible from the icon bar where
there are the same symbols as in the scrolling menu.

The "Zoom Full" allows the view to all project data.

The "Zoom to Layer" zooms to the layer that is selected in the left column.

Create des lines or polygones
=============================

A layer **Line** allows to draw an outline, a plot, an isocontour ...
with its own characteristics.

The **Polygon** layer is a surface. FIXME: Ce polygone apparaitra sur la carte et sera représenté
aussi sous forme d’une ligne dans la table de la couche, qui aura ces propres éléments exemple:
colonne nombre, couleur, taille...

To create new layer of type *Line* or *Polygon*:

 * In the layer menu select *CreateLayer/new Shapefile Layer*

 * Select Line or Polygon type in dialog box (add a new field if needed by filling its name)

 * Click OK

 * Name and fill up the path of the new shape file and save

 * To create lines, switch to edit mode by clicking on the icon representing a pencil

 * Click on *Add Feature* in taskbar

 * End the line of polygone with a right click

 * Enter the attribute of the line (id) in the window that just appeared,
   then click OK.

 * Repeat for the next lines

 * Finish the update of the layer by clicking on the button representing a pencil (stop editing)

Extract lines of polygons from a shape file
===========================================

**This operation is needed, when the data cover a larger zone
than the study area, such as the CorineLandCover data
Which are provided by large zones (French department).**

First, have a polygon bounding the study area:
the polygon can be created in SALOME-HYDRO, or in qgis - example (Contour.shp)

If the polygon has been created in SALOME-HYDRO as a polyline, it must be exported from SALOME-HYDRO
in shp (Polyline / Export) format (Example Contour_Etude.shp).

This polyline must then be transformed into a polygon in qgis:

 * Add Vector Layer: Contour.shp

 * In *Vector / Geometry Tools*: run *Lines to Polygons*
   and transform Contour.shp in Contour_polygon.shp

Display the CorineLandCover areas by clicking on *Add Vector Layer* (example: CLC12_D086_RGF.shp)

Extract data within a contour:

 * In qgis: in *Vector / Geoprocessing Tools*: run *Intersect* and complete
   in *Input vector*: the CLC file (example: CLC12_D086_RGF.shp),
   and in *Intersect layer* the contour of the study in polygon
   (example: Contour_polygon.shp).
   Give a name to the result file (example CLC12_D086_Extract.shp)


Caution, the cutting of the CorineLandCover areas by the study outline could generate
2-parts entities. In order that SALOME-HYDRO recognize all the parts, they
must be in different entities.

 * In qgis: in *Vector / Geometry Tools*: run *Multipart to single parts*
   with "Input polygon vector layer" the extracted file from the CLC (example CLC12_D086_Extract.shp))
   and as *output shapefile* another name (example: CLC12_D086_Extract_Single_Parts.shp)

 * This shp file, (CLC12_D086_Extract_Single_Parts.shp) can be imported into SALOME-HYDRO
   as LandCoverMap: *Import Land Cover Map from files*

.. only:: html

   :ref:`ref_english_outilsComplementaires`


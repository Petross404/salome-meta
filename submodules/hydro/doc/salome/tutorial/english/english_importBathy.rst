..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#######################
Bathymetry import
#######################

.. |import_bathy1| image:: ../_static/bathy1.png
   :align: middle

The HYDRO module enables bathymetry file types that have .xyz or .asc extensions to be imported.
These are ASCII text files. The .xyz files contain one point per line, with the x y z coordinates,
whereas the .asc files are formatted in a Cartesian x, y grid structure. The .asc files contain a header
which lists the number of lines and columns, the coordinates of the south western corner and the cell-size in x and in y,
below which is a matrix of the z altitude values. It is also possible to import bathymetries contained
in SinusX-format files, as will be described later.

Two files need to be imported for the purposes of this study.
They are found in the installation directory of the SALOME HYDRO application::

  <appli_xxx>/bin/salome/test/HYDRO/cloud_02.xyz

  <appli_xxx>/bin/salome/test/HYDRO/garonne_point_L93.xyz

The *cloud_02.xyz* file is an extract of IGN data on a regular grid with one point every 75 m, freely available.
The *garonne_point_L93.xyz* file corresponds to an earlier study, covers the major bed of the river over the region
and is potentially more precise than the previous file, at least on the minor bed.

These files are imported using the context menu of the *BATHYMETRIES* folder in the SALOME study tree.
The import dialog box permits the user to select the file to be imported and to change its name in the study tree.
The import instructions are confirmed by clicking on the *Apply and Close* button.

To display the bathymetry, click on the eye opposite its name and re-centre the image using the *fit all* button.

The points are coloured according to their altitudes and the colour ranges are adjusted according to the minimum
and maximum of all the displayed fields. The corresponding legend is always displayed.

You should obtain a view that looks like this:

  |import_bathy1|


.. only:: html

   :ref:`ref_english_exempleInondation`

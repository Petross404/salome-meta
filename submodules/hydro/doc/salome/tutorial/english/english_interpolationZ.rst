..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Z Interpolation
#########################################

.. |HYDROSolver| image:: ../_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |genereInterpolz| image:: ../_static/genereInterpolz.png
   :align: middle

.. |Bottom| image:: ../_static/Bottom.png
   :align: middle

.. |Capture_meshZ| image:: ../_static/Capture_meshZ.png
   :align: middle


.. |occ_view_scaling| image:: ../_static/occ_view_scaling.png
   :align: middle
   :width: 16pt
   :height: 16pt

The mesh generated in the previous step does not contain any information on the altitude.
To supply the TELEMAC code with this data, a field containing the Z-coordinate of the nodes must be added to the mesh.

The method for calculating the Z coordinate was described zone by zone in the calculation case set-up section of the HYDRO module.

Calculation of the Z interpolation at the mesh nodes
====================================================

The altitude field is constituted by means of the following Python script that must be generated and then run.

In the script, the regions of the HYDRO calculation case are associated to the corrresponding faces group of the mesh.
A specific command in HYDROSOLVER module helps to build this association.

The HYDROSOLVER module is activated with a selection in the module list (combo box) or its icon in the toolbar: |HYDROSolver|.
Activate the command *Generate interpolz.py* in the *Hydro* menu.

Select the calculation case in the study tree, under the folder *HYDRO / CALCULATION CASE*.
Its name appears in the first line of the dialog: *Calculation case*.

In the *Output path* field, give the full path of the Python script to create (script name must end with .py).

In the *MED file* field, select the mesh field produced at the previous step.

*Undefined Z* is used when he HYDRO module does not know how to calculate the altitude on a point.
This may be used to help in case of problems in the zones definition in the calculation case.

The interpolation of point clouds can be done in two ways, depending on whether the clouds are denser than the mesh, or vice versa.
For very dense point clouds, it suffices to take the altitude of the closest point in the cloud.
If the mesh is denser than the cloud, it is better to use linearised interpolation, obtained by previous triangulation of the point cloud.
This last method is more accurate but slightly more costly, computationally.

The region names must be selected in correspondance of their group of faces (representing the same geometric face),
and, for the other groups, the selection should be *None*.

  |genereInterpolz|

The script produces several files, whose names are derived from the original file name of the mesh, with different suffixes,
which are stored in the original file's directory:

 * garonne_1.med: original file (z coordinate = 0)
 * garonne_1.xyz: xyz file (ASCII) of the altitudes at the nodes *(optional)*
 * garonne_1F.med: calculated value of the Z coordinate and “BOTTOM” field with the Z-value at each node

**Remark** : The Z coordinate on mesh nodes is not used by TELEMAC, but is useful for visual control in SMESH.

To run the script, the HYDRO module for the study must be active. If resuming work on a study that was previously saved,
the HYDRO module must be activated before running the script
(simply select HYDRO at least once in order for the data stored in the study file to be read).

The script is executed manually with the File / Load Script... menu command.
The script blocks the graphic interface during execution.
It displays an execution trace in the Python console that is displayed by default in the GEOM and SMESH modules.

It is also possible to edit the following script:

The script needs to be copied and modified as needed for the region names used in the calculation case and for the named groups of mesh faces.

.. literalinclude:: ../interpolZ.py
    :lines: 1-

Visualisation of the Z interpolation at the mesh nodes
======================================================

Visualisation with the MED module
---------------------------------

The MED module offers a simple field view of a MED mesh.
The MED module must first be activated, then use the *File/Add Data Source* menu or the equivalent icon and find the *garonne_1F.med* file.
Expanding the *garonne_1F.med* object in the study tree will reveal the *HYDRO_Garonne_1* mesh and the *BOTTOM* field.

Select the field and click on the *scalar map* icon.

The field is displayed in 3D view. The 3D-view context menu contains the *Representation / Surface with Edges* command

  |Bottom|

Visualisation in the SMESH module
---------------------------------

When the interpolation script finishes running, the *HYDRO_Garonne_1* mesh should appear a second time, with a different icon,
underneath the first instance in the study tree. If it is not visible, use the *Refresh* command in the study tree context menu
to update the file list.

The second mesh is displayed in the SMESH module using the *show* command. For a better relief view, the Z scale needs to be modified
with the 3D view icon |occ_view_scaling| . In this case, scaling the Z-axis by a factor of 3 is sufficient.

*Remember*: objects are manipulated in 3D view using the <CTRL> key and the mouse buttons or the mouse wheel for zooming.

The following is a group view corresponding to the study regions:

  |Capture_meshZ|

.. only:: html

   :ref:`ref_english_exempleInondation`

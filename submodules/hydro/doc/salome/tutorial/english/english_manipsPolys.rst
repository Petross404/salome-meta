..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Polyline manipulation
#########################################

.. |deuxSections| image:: ../_static/deuxSections.png
   :align: middle

.. |deuxExtremites| image:: ../_static/deuxExtremites.png
   :align: middle

.. |bringTogether| image:: ../_static/bringTogether.png
   :align: middle

.. |deuxPointsConfondus| image:: ../_static/deuxPointsConfondus.png
   :align: middle

.. |contourFermeDeuxSections| image:: ../_static/contourFermeDeuxSections.png
   :align: middle

.. |domaineMaritime| image:: ../_static/domaineMaritime.png
   :align: middle

.. |zonePont| image:: ../_static/zonePont.png
   :align: middle

.. |raffinement| image:: ../_static/raffinement.png
   :align: middle

.. |completeSplitDialog| image:: ../_static/completeSplitDialog.png
   :align: middle

.. |zonesSplitCreees| image:: ../_static/zonesSplitCreees.png
   :align: middle

.. |zonePontSplit| image:: ../_static/zonePontSplit.png
   :align: middle

.. |zoneAmontSplit| image:: ../_static/zoneAmontSplit.png
   :align: middle

.. |zoneAvalSplit| image:: ../_static/zoneAvalSplit.png
   :align: middle

.. |mergeZonesPont| image:: ../_static/mergeZonesPont.png
   :align: middle

.. |pilesDePont| image:: ../_static/pilesDePont.png
   :align: middle

.. |objetsPont| image:: ../_static/objetsPont.png
   :align: middle

Only single section polylines have thus far been addressed.
The use of several sections in polylines enables, for example, the combination of splines
and broken lines in a single contour.
Polyline operations (merge, split, copy, paste) can be used, for instance,
to create contours that share an edge before the calculation case is created.

Maritime domain boundaries
==========================

A maritime domain is often bounded by a coastline, described in detail,
and by the simplest possible broken line offshore.

Details of the coastline are captured and entered in the form of an open polyline spline.

The sea boundary is captured in a second section of the same polyline.

To create a second section, you need to edit the polyline that represents the coastline:
click on *insert new section* and choose a line of the open *polyline* type,
then use the *Add* button to validate the section creation.

As section 2 is already selected, use the *Addition mode* button to start adding points.
The aim when creating the broken line is to place the first and last points in the approximate
vicinity of the coast line endpoints.

  |deuxSections|

The endpoints of the two sections must now be made to correspond exactly so as to obtain a closed contour.

To do so, select both sections simultaneously (shift key)
and switch to *modification mode* (modification mode button).

Next, in the view window, select both of the first two endpoints to be brought together
by dragging a selection box around them with the mouse (after having zoomed in, if necessary).

  |deuxExtremites|

The coordinates of the two points are displayed, as well as the distance between the two points,
in the distance column.

Note that the right hand button has become active (two sections must be selected).

  |bringTogether|

When the button is clicked, the two points are superimposed, their coordinates are updated
and the displayed distance changes to zero.

  |deuxPointsConfondus|

The same procedure needs to be applied to the other two endpoints, then validate with *Apply and close*.

The polyline now represents a closed contour consisting of 2 sections of different types.

  |contourFermeDeuxSections|

 * **Note**: When the points are superimposed, one moves to the coordinates of the other.
   Controlling which of the points remains fixed is not an easy task;
   therefore it is best to avoid having too much distance between them in order to prevent
   any deformation of the coastline.

The contour must then be converted into a natural object, providing the maritime domain.

  |domaineMaritime|

Creating sections on a minor bed: dams, bridges
===============================================

A minor river bed can be divided into several sections to which specific treatment options can be applied.
For example:

 * The zone around a bridge, where explicit description of the bridge piers is likely to impose a specific mesh,
   if the rest of the minor bed is meshed in stretched triangles in the direction of the flow path.

 * A dam for which one wants to write, for example, the boundary conditions describing its operation.
   The dam will then be defined as a "non floodable" or "insubmersible" zone across the minor bed.

 * A zone for which the user wants to tailor the mesh.

Creating a specific zone around a bridge over the minor bed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Taking the previous example on the Garonne, begin by drawing a rectangle across the *garonne* minor bed,
extending clearly beyond the riverbanks on both sides. Name the polyline rectangle *zonePont* (zoneBridge).

  |zonePont|

**The polyline of the minor bed needs to be sufficiently refined for the cutting to be precise
and to avoid problems when creating the calculation case afterwards.**
If necessary, the polyline of the minor bed should be edited to add points on both sides
of the cross-section lines.
**There must always be at least one point between two cut lines of a spline curve**.

  |raffinement|

Use the *Split polylines* command from the *garonne* polyline context menu and open the *Complete split* tab.
Select the *zonePont* polyline then click on the *include* button to have both polylines in the dialog list view.

  |completeSplitDialog|

After validating with *Apply and close*, four new polylines are obtained to describe the minor bed
and four to describe the bridge zone.

  |zonesSplitCreees|

The new zones will be reconstructed by joining the polylines together, set by set,
with the *Merge polylines* command.

The bridge zone is composed of the following four polylines:

  |zonePontSplit|

The upstream minor bed zone is composed of two polylines:

  |zoneAmontSplit|

The downstream minor bed zone is composed of two polylines:

  |zoneAvalSplit|

To join the polylines, select any one of them and click on the *Merge polylines* command in the context menu.
You now need to select the four polylines, click on the *include* button, assign a name to the new polyline, *litMineurPont* ("minorBedBridge") and confirm the command by clicking on *Apply and close*.

  |mergeZonesPont|

The *litMineurAmont* ("upstreamMinorBed") and *litMineurAval* ("downstreamMinorBed") polylines are created
in the same way. The three new polylines serve to define three immersible zones: *litMineur_aval*,
*litMineur_amont* and *litMineur_pont*.

If one wants to have bridge piers modelled in the mesh, these need to be represented as polylines
and non-submersible zones defined.

  |pilesDePont|

  |objetsPont|

Creating a specific zone around a dam on the minor bed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The principle is exactly the same as for the bridge, but without the piers.

.. only:: html

   :ref:`ref_english_casParticuliers`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Lancement du calcul TELEMAC
#########################################

.. |HYDROSolver| image:: ../_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |case_pytel| image:: ../_static/case_pytel.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |CreateCasePytel| image:: ../_static/CreateCasePytel.png
   :align: middle

.. |SelectCommandPytel| image:: ../_static/SelectCommandPytel.png
   :align: middle

.. |CasPytel| image:: ../_static/CasPytel.png
   :align: middle

.. |CasPytelRepTravail| image:: ../_static/CasPytelRepTravail.png
   :align: middle

.. |CasPytelFichierCas| image:: ../_static/CasPytelFichierCas.png
   :align: middle

.. |CasPytelEntreeMedIncomplete| image:: ../_static/CasPytelEntreeMedIncomplete.png
   :align: middle

.. |CasPytelSave| image:: ../_static/CasPytelSave.png
   :align: middle

.. |CasPytelArbre| image:: ../_static/CasPytelArbre.png
   :align: middle

.. |CasPytelComputeCase| image:: ../_static/CasPytelComputeCase.png
   :align: middle

.. |CasPytelCalcul| image:: ../_static/CasPytelCalcul.png
   :align: middle

.. |CasPytelFinCalcul| image:: ../_static/CasPytelFinCalcul.png
   :align: middle

.. |eficas_05| image:: ../_static/eficas_05.png
   :align: middle

.. |eficas_06| image:: ../_static/eficas_06.png
   :align: middle

.. |eficas_07| image:: ../_static/eficas_07.png
   :align: middle

.. |eficas_08| image:: ../_static/eficas_08.png
   :align: middle

.. |eficas_09| image:: ../_static/eficas_09.png
   :align: middle

.. |eficas_10| image:: ../_static/eficas_10.png
   :align: middle

The HYDROSolver module must now be activated via the module scrolling list or with the |HYDROSolver| icon in the ribbon.
This module is responsible for the Telemac and Mascaret numerical modelling and their couplings.

Creating the PYTEL Calculation Case
===================================

Pytel can be used to launch a simple Telemac code execution.
A Pytel Calculation case is created with the command Create case for Pytel execution |case_pytel| available
in the hydro menu or in a ribbon icon.

* **Note**: The ribbon icons for the module currently in use (at the top right) are not necessarily visible:
  the pop-up menu (right click) in the ribbon shows the groups of icons displayed and those that are not and allows them to be managed.

  |eficas_05|

  |eficas_06|

Use the file previously created with EFICAS.

  |eficas_07|

Save PYTEL case.

  |eficas_08|

The case appears in the study tree.

  |CasPytelArbre|

Launching the PYTEL Calculation Case
====================================

The *Compute Case* command of the Case menu is used to launch the Telemac simulation

  |eficas_09|

While the calculation is running, an onscreen listing of the computation (log file) is displayed in a window.

  |eficas_10|

The window can be closed at the end of the computation.

  |CasPytelFinCalcul|

.. only:: html

   :ref:`ref_english_exempleInondation`

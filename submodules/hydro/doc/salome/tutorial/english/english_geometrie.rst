..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Geometry step
#########################################

.. |publieGeom| image:: ../_static/publieGeom.png
   :align: middle

.. |activeGeom| image:: ../_static/activeGeom.png
   :align: middle

.. |facesGeom| image:: ../_static/facesGeom.png
   :align: middle

.. |contoursGeomLitMineurDomaine| image:: ../_static/contoursGeomLitMineurDomaine.png
   :align: middle

.. |creeGroupeGeom| image:: ../_static/creeGroupeGeom.png
   :align: middle

When the calculation case is ready, it must be exported to the geometry module, GEOM,
using the context menu on the name of the calculation case: *Export Calculation Case*.

The *HYDRO_Garonne_1* entry appears in the study tree, under *Geometry*.

  |publieGeom|

The GEOM module can be activated either in the drop-down list under the menu bar,
through the icon in the module bar or via the *Activate Geometry Module* context menu under
the object name *HYDRO_Garonne_1* in the tree.

  |activeGeom|

Expanding the *HYDRO_Garonne_1* object reveals several groups created automatically
when the calculation case was created:
the three faces corresponding to *regions*, with the same names, and group of edges,
*garonne_1_litMineur_Outer* and *garonne_1_domaineEtendu_Outer*.

To see the group of faces, select them and click on *show only* in the pop-up menu.
The *auto color* context menu under the object name *HYDRO_Garonne_1* allows a better identification
of the faces.

  |facesGeom|

The two groups of edges represent the banks of the minor bed and the outline contour of the domain.
To see them, select them and click on show only in the pop-up menu.

  |contoursGeomLitMineurDomaine|

The upstream and downstream cross-sections of the minor bed still need to be identified, in order to create the mesh.

To create groups in an object, it is best to start by displaying the object alone (*show only*).

Create an *edges* group type, called *SectionsGaronne* ("GaronneSections"),
containing both ends of the Minor bed.

This is done using the Create Group command in the context menu.
The dialog box allows the creation of four types of group: points, edges, faces and volumes.
Select the *edges* type – second radio button in the section Shape Type.

In the name field, type the group name: *SectionsGaronne* ("GaronneSections").

Select the the two sections in the graphic view.
Several elements can be selected by holding down the "Shift" key.
When the add button is pressed, the number of the SubGeometry appears in the Sub-shape list.

Confirm by clicking *Apply and Close*.

  |creeGroupeGeom|

Two groups of the *edges* type, *amont* ("upstream") and *aval* ("downstream"),
are also created to distinguish between the two types of boundary conditions.

Another two edge groups must likewise be created to close the contour;
named *bordDroiteDomaine* ("domainRightBoundary") and *bordGaucheDomaine* ("domainLeftBoundary"),
they contain the continuous boundaries on the right and left bank sides of the domain.

The mesh of this geometry can now be created.

.. only:: html

   :ref:`ref_english_exempleInondation`

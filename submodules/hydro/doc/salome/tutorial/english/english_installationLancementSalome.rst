..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.


##########################################
SALOME installation and start-up procedure 
##########################################

.. |fit_all| image:: ../_static/fitall.png
   :align: middle

Installation
============

You should obtain an installation file for SALOME_HYDRO: *xxx*.run.
Inside EDF, SALOME_HYDRO is available on the EDF intranet:

 * Open an account (e.g.: I88928)

 * Open a web browser (blue button at the top right or Applications/Internet/Navigation Web Iceweasel) 

 * Type the address: https://salome.der.edf.fr/ 

 * Look for the latest version of SALOME_HYDRO in the folder:  Downloads (Téléchargements)

 * Extract the *xxx*.run file

 * Save (the file is automatically saved in /Downloads (Téléchargements)) 

 * Open a terminal (at the top of the screen:  Accessories  (Accessoires)/Terminal) 

 * Type cd Téléchargements (Downloads) 

 * Type ls: you should see the *xxx*.run 

For everybody:

 * (if necessary, make it executable: chmod +x *xxx*.run) 

 * Start the installation by typing ./*xxx*.run 

 * Answer the installation questions (directory: of your choice; **language: must be English**
   (the option has been removed, as there is no choice);
   for all remaining questions, if any:  yes by default) 

Salome-Hydro is now installed and appears as an icon on the desktop.
The icon can be dragged and dropped into an open editor (gedit or equivalent)
to replace the *Terminal=false* line by *Terminal=true*.
This provides an execution trace, facilitating the diagnosis of certain application problems.
It is not a compulsory step. 

The installation is in English because the translation into French is incomplete.

Starting SALOME
===============
 
Double-click the SALOME icon on the desktop, or, knowing the installation folder of SALOME ( *<appli_XXX>* ),
in a terminal ::

  <appli_XXX>/salome

It is possible to get execution log in case of problem, in the terminal.  

* **Note** : the *salome* command accepts options.
  For instance, -k option closes all previous SALOME instances and releases all associated resources.
  To get help, use -h option ::

  <appli_XXX>/salome -h

View handling in SALOME
=======================

For those users unfamiliar with SALOME, the following paragraphs describe the main 2D and 3D view-handling commands, as well as techniques to select and display objects in the study tree (Object Browser).
 
Display data in the view window:
--------------------------------

By clicking the eye icon next to the object in the Object Browser window. 

By right clicking on the object in the tree on the left (in the Object Browser window),
which brings up a list of options::

  /show/ show only / hide / hide only / hide all / 

Zoom
----

Ctrl + left click 

The zoom is centred by default. Activation of zoom using the scroll (mouse) wheel: 
to enable use of the scroll wheel to control the zoom, this option must be activated:
file /preferences (select the SALOME module) /3D viewer /zooming style /relative to the cursor.
A zoom relative to the cursor pans in to the area around the cursor position
while a centred zoom pans out to takes account of the whole zone.
 
To zoom in on a data item in the tree: 

Select the item from the tree. In the toolbar at the top of the window, 
select the Fit All button 

Rotation
--------

Ctrl + right click 

Translation (horizontal/vertical movements)
-------------------------------------------

Ctrl + mouse wheel

Viewers
-------

SALOME proposes several viewers:
 
VTK for the 3D views (MESH module) and OCC for the 2D views (for the geometry).
These can be found via tabs that appear on the screen.

The different elements of the study are not all displayed equally well in all the viewers!


..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Preliminary concepts
#########################################

 * **Georeferencing**: The projection system depends on the geographic location of the study
   and must be selected and stored to facilitate study follow up. The reference projection
   in metropolitan France is Lambert 93.
   **All data imported into SALOME HYDRO must be in the same (unique) reference frame.**
   SALOME HYDRO does not propose a coordinate converter.

 * **Local reference point**: The Cartesian coordinate values, **always expressed in metres**,
   in a given projection system often represent very large numbers.
   The Lambert 93 coordinates of a point in continental France, for example,
   are of the order (400 000, 6 500 000).
   A simple shift of the origin results in a local reference point,
   reducing the size of the numbers to be handled.
   To improve numerical precision in the different study steps (geometry, meshing, simulation, etc.),
   **the use of a local reference point is highly recommended.**
   SALOME HYDRO displays coordinates in both coordinate systems (local and global)
   and automatically applies the translations on import and export.

 * **Constraint line**: Line used to support and structure the mesh.
   In SALOME, these lines are defined in the Geometry (GEOM module) as "edge".
   Not every line created in the HYDRO module will necessarily be kept in the geometry.

 * **Hydraulic axis**: The orientation of river flow.
   Often obtained by connecting the low points of a succession of cross profiles.

 * **Crest line**: Line joining the highest points of an embankment.

 * **Partition**: This is a segmentation of the space into **regions** and **zones**.
   The concepts of zones and regions are introduced in the practice exercise to follow.
   The boundaries of a region correspond to the constraint lines of the mesh.
   The regions can be divided into several zones with each zone corresponding
   to a computational mode for the bathymetry. 
   
 * **Land Cover Map** : A map describing the region of interest, giving the kind of land cover
   (for instance: forest, cultivated area, town...), following the *Corine Land Cover* classification.
   
 * **Strickler table** : Gives the *Stricker coefficient* (friction)
   for each kind of land cover (entry of the *Corine Land Cover* classification).
   These coefficients must be redefined on each case, following the type of calculation.


################################################
Main steps in a study using SALOME-HYDRO
################################################

.. |etapesEtude| image:: ../_static/etapesEtude.png
   :align: middle

SALOME contains all the modules needed to start a Telemac study. 

The SALOME-HYDRO application gathers the HYDRO and HYDRO-SOLVER modules,
which form the heart of the SALOME system, onto a single platform that integrates the Telemac2D system. 

The following figure summarises the general steps and the tools used within the SALOME platform.

  |etapesEtude|

The study will be carried out using the following SALOME modules: 

 * HYDRO 

 * GEOM 

 * SMESH 

 * HYDROSOLVER 

 * PARAVIS 

Functions/Operations: 

 * HYDRO: images and/or plans are imported 

 * HYDRO: contour lines of **natural features** (rivers, islands, etc.) and **artificial features**
   (embankments, channels, obstacles, etc.) are created or imported 

 * HYDRO: bathymetry / altitude fields or series of river profiles are loaded 

 * HYDRO: object elements representing the natural and artificial features are constructed, 
 
 * HYDRO : a Land Cover Map and a Strickler table are created or imported 

 * HYDRO: a **calculation case** is created by selecting the objects relevant to the case;
   the **zones** (each associated to a specific method of bathymetry calculation) are grouped into **regions**
   (the boundaries of which correspond to mesh constraint lines; these are the faces of the final SALOME geometry) 

 * HYDRO: the case is exported to GEOM 

 * GEOM: the case is taken up in GEOM to complete the identification of the groups associated
   to the different regions of the domain and the boundary conditions 

 * SMESH: the mesh assumptions ("hypotheses" in the dialogs) and algorithms are chosen,
   the mesh is calculated and the corresponding MED file is exported 

 * HYDROSOLVER: the Z interpolation Python script giving z coordinates on mesh nodes
   from the MED file and the calculation case is generated and executed

 * HYDROSOLVER: the Strickler Python script giving Strickler coefficients on mesh nodes
   from the MED file and the calculation case is generated and executed

 * HYDROSOLVER: the physical and numerical parameters of the simulation are defined (case file) 

 * HYDROSOLVER: the calculation is executed 

 * PARAVIS: the results are analysed

The sequential logic of the operating steps in the HYDRO module itself is as follows
(see the different types of manipulated objects in the tree on the left): 

 * IMAGES:

Satellite imagery and/or maps of the study area are used to locate the absolute and relative positions of the elements to be represented in the mesh. These images must be georeferenced in the same system of coordinates
as all the data (for example, Lambert 93). This may, for instance, be a geoportal screenshot.

 * BATHYMETRY:

The bathymetries in the form of point cloud and/or cross profile datasets that constitute the terrain model
on which the mesh will be based are imported into the project.

 * POLYLINES:

Polylines (imported into and/or created in SALOME) are used to define the contours of the natural and artificial
objects that will intervene in the calculation case. These lines are closed in the general case and some of them will form the mesh constraint lines. Splined polylines permit to define contours without there being any necessity
for the mesh based on them to use the points on the line: only the general shape counts. Lines can be drawn with
the mouse or imported from a file.Les polylignes (importées et/ou construites dans SALOME) permettent de définir
les contours des différents objets

 * NATURAL OBJECTS:

These are composite elements comprising, for example, land surfaces, islands, lakes, etc.
A bathymetry can generally be associated to them.

 * ARTIFICIAL OBJECTS:

This step involves building structural elements, such as embankments, canals and channels of known geometry. 

 * CALCULATION CASES :

When the calculation case is defined, the objects to be meshed are selected and any issues concerning
the bathymetric coverage (overlaps/gaps) are resolved.

This is a description of a typical sequence;
moving backwards and forwards between the various steps is perfectly possible and will certainly occur.

Preliminary data requirements
=============================

* For the image files: have map or photo background image files and have identified in the working system
  the coordinates of 2 or 3 points that are well-distributed over the image (far enough apart to ensure high precision).
  With three points, an affine transformation of the image can be applied in the unlikely case that the latter is not
  consistent with direct vertical (orthographic) projection.

* Have bathymetry data available in ASCII format.

For the moment, the bathymetry files must have either an .xyz (3 columns x,y,z) or an .asc extension
(raster grid-type format, as provided in the IGN Alti database, for example).

* If cross profiles are available for the river, they can be given either on xyz format with an empty line
  between each profile, or on SinusX format (see description below).

Image import
============

Plans, maps and photos are imported into the Images folder of the study tree in the Hydro module.
The idea is to work from satellite images and/or maps of the area to be meshed,
georeferenced in the same system of coordinates as all the data (Lambert93 for example).
It is possible to find the intersection of two images, to fusion the images and to clip an image using a polyline.

Image manipulation is introduced in the subsequent exercise.

Selection of a local reference point
====================================

**It is strongly advised to change the origin of the local coordinate system**
to avoid having to deal with very large numbers and to increase the calculation precision,
particularly for the mesh generation.

To do so, the *Hydro/change local CS* menu must be used to enter the coordinates of the new origin.

Bathymetry import 
=================

The procedure is explained in the exercise section.

 * **Note** : If the altitudes are less than 0, the bathymetry may be hidden by the maps or photos
   (which are at Z = 0 by convention).
   If the bathymetry needs to be seen at the same time as the images, this can be achieved either
   by switching to the bottom view, or by editing the bathymetry
   ("edit imported bathymetry" context menu) to invert the Z values while verifying for example
   the alignment of the maps and the bathymetry
   (and not forgetting to revert to the original Z values afterwards!). 

Retrieving bathymetry data from old mesh 
----------------------------------------

Precise details of the procedure for the following operations remain to be finalized.
The Python scripts mentioned below are not provided with this version;
they require adaptation on a case by case basis.

 * It is possible to retrieve an old mesh from a case study by converting the data from selafin format (.slf)
   to .med format (dedicated format for the Salomé platform in general),
   using the integrated file converter in SALOME-HYDRO

 * Based on the old mesh, it may be necessary to apply a translation (for example +2,000,000)
   to the y-position of the nodes to switch to a known georeferencing system
   (for example LambertIIEtendu).

 * The bathymetry (depth field Z) is then retrieved using a Python script which creates an .xyz file
   (x node position, y node position, associated Z field).

 * This .xyz file is converted into Lambert 93 using the Circé freeware (under Windows). 

 * The edge node positions are then retrieved using a Python script that traverses the nodes,
   determines whether they are at the boundary edge and creates a file in which each boundary retrieved
   is formatted for direct import into the HYDRO module.

 * This procedure enables isolating the contour of the site, bridge piers and islands, if there are any.

Importing line-type objects
===========================

The SinusX (ASCII) format described in the appendix permits describing several line and profile types.
Files in this format that respect the conventions detailed in the appendix can be imported into SALOME HYDRO.

polylines
---------

Definition : lines in the XY plane, generally used to define contours and zones. 

profiles
--------

There are two types of profile: georeferenced or not.

Georeferenced profiles are defined in XYZ coordinates and others in XZ (XY).
Utility: embankment, canal, channel and river sections. 

A single cross-section is defined for a channel, canal or an embankment and a series of profiles for a river.

For an embankment, the value Z=0 corresponds to the crest line, for a channel it is the bottom line.

Only symmetrical profiles (in relation to the crest or bottom line) are considered.

Streams
--------

River described by a series of cross profiles, ordered along an upstream-downstream line which passes
through these profiles. This line can be the hydraulic axis, though this is not compulsory.

The stream files can be XYZ files in which each profile is separated by an empty line.

Line drawing 
============

Polyline-type contours are needed to create the geometry. They are used to construct the terrain model,
 islands and the minor river bed, as well as embankments, channels, canals, roads and so forth. 

The bathymetry or maps can be displayed when entering contours as an orientation guide.

polylines
---------

There are two types of polyline in SALOME:

 * Polylines (broken lines composed of a series of straight segments, to describe a geometric object) 

 * Splines (set of arcs that form a smooth parametric curve with a continuous derivative,
   to describe a natural curve, which will adapt to the fineness of the discretisation). 

The use of splines enables contours to be defined without any need for the mesh based on these
to adhere to all the points on the line: only the general shape counts. 

The procedure is detailed in the exercise below. 

it is possible to build lines mixing polylines and splines.
See below the chapter on polyline manipulation.

Embankment or channel profiles 
------------------------------

These types of profile can be imported or drawn.
The drawing procedure is detailed in the practical exercise.

River profiles
--------------

Only the import of these profiles is supported.

Creating "natural" objects of the "immersible zone" type
========================================================

An **immersible zone** will be meshed. Dry islands can be excluded from the mesh.
In SALOME HYDRO, this type of island is differentiated from the others by deactivating the Submersible attribute.

Creating an immersible zone consists in the creation of a geometric face from one of the contours previously drawn.
This is achieved by entering the polyline (compulsory) on which the face will be built
and the desired bathymetry (optional) to associate with this geographical zone.

 * **Note**: the bathymetry is optional when creating natural objects, particularly islands.

 * **Note**: It is possible to change the display order of the different natural and artificial objects,
   which are all by convention in the z=0 plane, to put the small objects "above".

Creating "stream" type "natural" objects
========================================

The procedure is explained in detail in a later chapter.

Creating embankment or channel-type "artificial" objects
========================================================

Details of the operating procedure are provided in a later chapter.

Obstacles
=========

Complex geometric objects (buildings, etc.) imported from GEOM to constitute the dry zones
("islands" or assimilated). These objects must be put into the local reference frame coordinates
before importing them.

Strickler coefficient tables, Land Cover maps 
=============================================

It is possible to build a Strickler coefficents map (friction) on the domain of interest.

Maps giving the type of soils (Land Cover Map) can be obtained on various websites.
For instance, with a Geographical Information System tool like *Qgis*, "Corine Land Cover" maps
can be downloaded and edited.
These maps follow a standard classification of soils. 

For each study, the Strickler coefficents associated to the Land Cover Map should be adapted, following each type of soil
of the map.

The Land Cover Maps are either imported from Qgis or created from scratch in SALOME-HYDRO.
The Land Cover Maps can always be edited in SALOME-HYDRO: add, suppress, regroup, modify zones...

Calculation case set-up
=======================

When configuring a calculation case, it is possible to select only some of the previously defined objects.
The choice depends on the case complexity and several cases at different levels of complexity
(details such as bridge piers, for example, being accounted for or not) can be created from the same object base.

The domain is enclosed with a given polygonal contour defining its boundaries. 

The overlapping of different objects creates "conflicts" between zones for which choices have to be made
in terms of the bathymetry computation. 

Splitting the overlapping objects into different zones constitutes the **partition** or segmentation operation. 

Zones may be regrouped into homogeneous regions in the target mesh structure, to avoid the use of contours
that we do not want to keep as constraint lines.
 
In the calculation case, specific lines onto which the boundary conditions will be applied can be identified.

The result is exported into the geometry module. 

The procedure is explained in detail in the practical exercise. 

Geometry: GEOM module
=====================

Once the calculation case is completed and exported, it appears in the GEOM module.

This module must be activated to be able to view and modify the exported case.

In GEOM, the calculation case is seen under the name <case name>_N to which is attached
the contour(s) selected when the calculation case was defined.

Certain mesh parts may need to be identified: 

 * Faces: to mesh certain zones in a different way 

 * Segments: to define the boundary conditions.

The procedure is explained in detail in the practical exercise.
 
 * **Note**: The geometry module may be used to define a certain number of mesh constraints; for example,
   fixed points can defined in the mesh (which could for instance correspond to measurement points).
   **Any modification to the geometry results in the creation of a new object and the loss of the groups
   defined in the initial object. Groups must therefore be created last, on the final geometry,
   and modifications that provoke the loss of the HYDRO module automatic definitions should be avoided,
   if and whenever possible.** 

Mesh: SMESH module
==================

Users should refer to the SALOME training materials for guidance on the use of the SMESH Module.

The procedure for SALOME HYDRO is detailed in the exercise below.

Z Interpolation
===============

Principles
----------

In hydrodynamics, it is vital to know the bathymetry value at each computational node.
 
The bathymetry is calculated zone by zone, with each zone associated to a specific method of calculation: 

 * From point clouds 

 * From river profiles 

 * From embankments and canal/channel axes and cross-sections 

 * From the CAD of the obstacles

For the point clouds, a script is available in version 1.0 enabling to interpolate the bathymetry onto the mesh.
This script uses an algorithm which takes either the Z value of the closest point or the Z value interpolated
on a prior triangulation of the data points.
 
The procedure is explained in detail in the practical exercise.

Input of physical/numerical data for TELEMAC
============================================

The HYDROSOLVER module is used to input these parameters and to assemble the calculation case.

Description of the boundary conditions
--------------------------------------

Each boundary condition region corresponds to a named group in the mesh.
The types of boundary condition associated to a group are defined in a file whose syntax is given
in the practical example.

Dans le module HYDROSOLVER, un outil permet d'associer des types de condition limites aux groupes concernés ans le maillage, 

Editing the Case file
---------------------

The calculation parameters are defined in the Case file with the TELEMAC 2D syntax
(using either EFICAS editor provided in GYDROSOLVER module or a standard text editor).

List of files needed by TELEMAC 2D
----------------------------------

To be completed, see the example case. 

Starting and monitoring the simulation 
======================================

The HYDROSOLVER module is used to start TELEMAC 2D.

The procedure is explained in the example case.

Result analysis
===============

The PARAVIS module is used to exploit the results.
Reference can be made to the SALOME training materials for further information on the use of the PARAVIS module. 


..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Mesh creation
#########################################

.. |mesh_init| image:: ../_static/mesh_init.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |mesh_edit| image:: ../_static/mesh_edit.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |mesh_hypo_edit| image:: ../_static/mesh_hypo_edit.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |vtk_view_fitall| image:: ../_static/vtk_view_fitall.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |vtk_view_top| image:: ../_static/vtk_view_top.png
   :align: middle
   :width: 16pt
   :height: 16pt


.. |Capture_CreateMesh| image:: ../_static/Capture_CreateMesh.png
   :align: middle

.. |Capture_HypothesisConstruction| image:: ../_static/Capture_HypothesisConstruction.png
   :align: middle

.. |Capture_CreateSubMesh| image:: ../_static/Capture_CreateSubMesh.png
   :align: middle

.. |Capture_HypothesisLocalLength| image:: ../_static/Capture_HypothesisLocalLength.png
   :align: middle

.. |Capture_HypothesisNbSegments| image:: ../_static/Capture_HypothesisNbSegments.png
   :align: middle

.. |Capture_WarningOrder| image:: ../_static/Capture_WarningOrder.png
   :align: middle

.. |Capture_OrderingSubMeshes| image:: ../_static/Capture_OrderingSubMeshes.png
   :align: middle

.. |Capture_MeshComputationSucceed| image:: ../_static/Capture_MeshComputationSucceed.png
   :align: middle

.. |Capture_CreateGroupsFromGeometry| image:: ../_static/Capture_CreateGroupsFromGeometry.png
   :align: middle


Once the geometry is ready, the *SMESH* mesh module can be activated.

Introduction to SMESH operations
================================

To specify the mesh, a default algorithm is generally defined with its parameter values:
these are the underlying assumptions of the algorithm, referred to as *"hypotheses"* in the SMESH module.

This algorithm and these hypotheses apply everywhere except where a part of the geometry (*sub-shape*) has been modified.

It is therefore possible to generate submeshes on a face or a group of faces, an edge or a group of edges,
in order to define specific algorithms and/or hypotheses.

During the meshing operation, the geometry of the part to be meshed is explored, starting with dimension 1 (edges)
then dimension 2 (faces) and finally dimension 3 (volumes).  In this example, there are no volumes.

Dimension 1 meshes therefore take priority over and are a prerequisite for dimension 2 meshes.

Certain algorithms manage several dimensions (in this case, edges and faces) simultaneously.
When these algorithms tolerate prescribed mesh from user-specified edges, the definition of the sub-meshes of
the face or faces concerned will be taken into account, otherwise it will not.
Similarly, according to the algorithm selected for a face, the 1D tab of the Create mesh dialog box will
either be active or not.

As a result, there may be occasions where, for certain *sub-shapes*, several algorithms and/or hypotheses
are defined, in which case a hierarchy of priority must be established. These situations are automatically
detected and the user is offered a list of choices.

Algorithms and underlying Hypotheses for the geometry
=====================================================

In this example, a triangle mesh will be specified as the default option over the whole domain
and the mesh of the minor bed will be customised to obtain stretched triangles oriented along the flow path.

To start defining the mesh, select the *HYDRO_garonne_1* geometry in the study tree and the
*Mesh / Create Mesh* menu or click on the icon |mesh_init|.

In the *Create Mesh* dialog box, select the mesh algorithm *Netgen 1D–2D*,
which will apply by default to all of the geometry.

  |Capture_CreateMesh|

The mesh hypotheses must be specified: Click on the active button in the *Hypothesis* row to choose a hypothesis type,
and select *Netgen 2D Parameters* from the drop-down options.

In the newly opened window, select a maximum edge length of 200 (metres) and a minimum length of 50.
For the rate of increase in triangle size, *Fineness*, choose Very Fine, to preserve the quality of the triangles.

  |Capture_HypothesisConstruction|

After confirming the hypothesis selections, validate the mesh creation dialog by clicking *Apply and Close*.

The mesh appears under the name *Mesh_1* in the tree with an icon indicating its status: *not generated or incomplete*.

The mesh can be renamed, either directly or by selecting the Edit dialog from the context menu.

The edit dialog box |mesh_edit| also offers the possibility of changing the algorithm or
modifying the hypotheses: |mesh_hypo_edit|.

Sub-meshes
==========

A sub-mesh is created by selecting the mesh in the study tree (context menu: *Create sub-mesh*).

The geometry needs to be entered in the open dialog by clicking on the *litMineur* face listed under *HYDRO_garonne_1*
in the study tree. To make this selection, the navigation arrow on the *Geometry* line must be active,
which it is by default.

It is useful to immediately rename the sub-mesh (first line of the dialog).

Next, select the *Quadrangle (Medial Axis projection)* algorithm.
This algorithm reconstructs an imaginary hydraulic axis and decomposes the river into quadrangles
at right angles to the hydraulic axis.

  |Capture_CreateSubMesh|

The length of the quadrangles and their number in the river cross-section still need to be defined.
In the *1D* tab of the *litMineur* sub-mesh dialog, choose the *Wire Discretisation* algorithm
and the *Local Length* hypothesis and then select a length of 100 (metres).
It is desirable to rename the hypothesis at this stage.

**Note**: The hypotheses and algorithms can be shared between several meshes and sub-meshes,
enabling to modify in one place everything that needs to remain consistent,
so **it is useful to give them meaningful names now that will help to identify them later**.

  |Capture_HypothesisLocalLength|

The definition of the sub-mesh is validated with the *Apply and Close* button in the sub-mesh dialog.

The length defined above applies both lengthwise and width wise. To control the number of transverse mesh elements,
a new sub-mesh is necessary, which will be applied to the edge group *SectionsGaronne*.

Create a sub-mesh on *SectionsGaronne*, this time taking *Wire Discretisation* as algorithm
and *Nb. Segments* as the hypothesis.
Select 8 segments and *Equidistant distribution* to have a regularly distributed submesh.

  |Capture_HypothesisNbSegments|

Use the *Apply and Close* button to validate the defined settings for this sub-mesh.
A priority needs to be established between two definitions:

  |Capture_WarningOrder|  |Capture_OrderingSubMeshes|

Select *SectionsGaronne* and move it up to the top of the list.

Mesh generation
===============

After validating the previous step, the mesh is ready to be generated.
To do so, select the mesh and click on the *Compute* context menu.
An information box displaying basic statistics appears at the end of the computation.

|Capture_MeshComputationSucceed|

The mesh icon in the study tree has changed and now indicates as status *correctly generated*.

The mesh is still not complete for the requirements of the example case but it can now be viewed.
To display it, click on *show* then *FitAll*  |vtk_view_fitall|
and top view |vtk_view_top| (*-0Z*) in the 3D viewer icon bar.

The *Modification/Cutting of Quadrangles* menu is now used to cut the quadrangles.
In the dialog, check *apply to all*, *use diagonal 1-3* then *preview*:
the proposed modification appears and can be checked by zooming in with the mouse wheel.
Validate with *Apply and Close*.

Mesh control
============

The mesh must be verified to ensure it meets TELEMAC requirements.

Mesh orientation
----------------

For XY surface meshes, **triangles may be oriented by default in SALOME in the opposite direction to the one expected by TELEMAC**,
depending on the direction used to draw the lines. Thus, SALOME may orients triangles with the normal pointing downwards from the face.
This can be checked via the mesh colour (darker blue on the back than on the front) or by using the *Orientation of Faces* command
in the 3D view context menu of the mesh. This command draws one arrow per triangle.
Only a point is seen on the back of the face; the view angle has to be changed to see the arrows.

To reorient the faces, use the *Modification / Orientation* menu and check the *Apply to All* option.
After validating the operation, the faces change colour (lighter blue).

Over-constrained triangles
--------------------------

The use of different boundary conditions on two edges of a triangle should be avoided and, more generally,
avoid having imposing boundary conditions on two edges (no degrees of freedom on the triangle).

Mesh generators sometimes create such triangles in the corners of the mesh.  This will happen with Netgen in the case of acute angles.
Whenever possible, avoid creating acute angles at the domain boundaries.

Over-constrained triangles are detected using the *Controls / Face Controls / Over-constrained faces* menu command.
To correct this problem, the *Modification / Diagonal Inversion* menu command should be used,
selecting the internal edge of the triangle in question.

Creation of groups / Saving the mesh
====================================

Groups of mesh nodes and mesh elements are useful in the bathymetry interpolation step and for defining the boundary conditions.

Mesh groups are defined with the *Create Groups from Geometry* command in the context menu of the mesh.
The **element** and **node** groups are constituted successively by selecting
all the groups and sub Shapes of the HYDRO_garonne_1 geometry.

  |Capture_CreateGroupsFromGeometry|

These groups appear in the study tree under the mesh after validation of the dialog.
They can be seen with *show only* and it is also possible to highlight them using the *Auto Color* option in the mesh context menu.

To save the mesh in MED file format, select the mesh and use the menu command:  *File / Export / MED file*.

.. only:: html

   :ref:`ref_english_exempleInondation`

..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Basic help on CloudCompare
#########################################

.. |CloudCompare_01| image:: ../_static/CloudCompare_01.png
   :align: middle

.. |CloudCompare_02| image:: ../_static/CloudCompare_02.png
   :align: middle

.. |CloudCompare_03| image:: ../_static/CloudCompare_03.png
   :align: middle

.. |CloudCompare_04| image:: ../_static/CloudCompare_04.png
   :align: middle

.. |CloudCompare_05| image:: ../_static/CloudCompare_05.png
   :align: middle

Load data and scatter plot visualization
========================================

From the CloudCompare *File / open* menu, you can load different data formats, including xyz
(*ASCII cloud* option, ASCII text file, 3 x y z coordinates per line)
and the asc format (option *RASTER grid*, ASCII regular grid, provided by the IGN for instance).
Two examples are available in the installation of SALOME_HYDRO ::

  <appli_xxx>/bin/salome/test/HYDRO/garonne_point_L93.xyz

  <appli_xxx>/bin/salome/test/HYDRO/BDALTIr_2-0_MNT_EXT_0450_6375_LAMB93_IGN69_20110929.asc

We take the file *garonne_point_L93.xyz*. When opening the file, you can validate the first dialog without modification (*Apply*):

  |CloudCompare_01|

The second dialog box suggest a change of reference to improve the accuracy, which can be accepted such as (*Yes*):

  |CloudCompare_02|

The scatter plot is displayed, but without coloring according to the Z dimension. The line *garone_point_L93 - Cloud* is selected:

  |CloudCompare_03|

In the *Edit / Scalar fields / Export Coordinates) to SF (s)* menu, the Z coordinate is selected as a new scalar field.

  |CloudCompare_04|

The scatter plot is now colored. You can put a caption by checking the *visible* option of the *Color Scale* heading.

  |CloudCompare_05|

.. only:: html

   :ref:`ref_english_outilsComplementaires`


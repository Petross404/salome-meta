..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

####################################
Advice, pitfalls to avoid
####################################

This paragraph will be re-written according to the user feedback.

- Object overlap or lines that are very close will lead to an ugly mesh or one that is too fine.

- Avoid using too many points in the splines, when they are not necessary.

 -Anything that will lead to unnecessary complexity of the partitioning when the case is created
  and prevent the use of the automatic mode for grouping zones into regions should be avoided.

- Change of coordinate system: it is best to define a local reference point as early as possible. See tutorial.

- The two coordinate systems appear, but it is preferable for the transition from one to the other to be made simply
  (the coordinates of the new origin are rounded).

- Consistency of the imported data coordinate systems should be maintained:
  for example, put everything into Lambert 93 coordinates before import.

- Avoid the use of acute angles at domain limits: risk of creating over-constrained triangles,
  which means detecting them and inverting the diagonals.

- Save regularly the study and renaming it to preserve successive steps.
  This can be useful in case of error.

- Do not use special characters (spaces, accents...) in the names of the objects.

- Give meaningful names to the different objects (a lot in a complex study),
  as well as meshing hypotheses.

- It is important to choose well where a closed line of spline type starts:
  This first point remains until meshing, and can add an unnecessary constraint if it is misplaced.
  During the drawing riverbed, most of the time, a closed contour bigger than the study area is used.
  It is wise to put this first point out of the study area.

- In the case of river study, pay attention to cut the river perpendicularly to the hydraulic axe
  to be able to define the boundary conditions properly.

- Use of the spline type or polyline type?

   * *spline*: a single smooth line that has a continuous derivative, passing through all the points.
     This type of line is preferred for all curved lines, the control points will not be reused in the mesh,
     which provides greater flexibility in controlling the mesh refinement.

   * *polyline*: a single continuous line, composed of straight segments.
     This type of line must be used for the artificial objects composed of straight segments and whenever
     broken lines are needed. The points are kept in the mesh.

   * The two types of lines can be combined: see the section on handling polylines.

.. only:: html

   :ref:`ref_english_notionsPrealables`

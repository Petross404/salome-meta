..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Image and map background import
#########################################

.. |Hydro_module_button| image:: ../_static/HYDRO.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |Hydro_module_launch| image:: ../_static/LaunchHYDRO.png
   :align: middle

.. |import_image| image:: ../_static/importImage.png
   :align: middle

.. |import_image2| image:: ../_static/importImage2.png
   :align: middle

.. |selection_A| image:: ../_static/selectionA.png
   :align: middle

.. |selection_B| image:: ../_static/selectionB.png
   :align: middle

.. |selection_B2| image:: ../_static/selectionB2.png
   :align: middle

.. |fit_all| image:: ../_static/fitall.png
   :align: middle

.. |import_image_B| image:: ../_static/importImageB.png
   :align: middle

.. |deux_images| image:: ../_static/deuxImages.png
   :align: middle

.. |zoom_style| image:: ../_static/zoomStyle.png
   :align: middle

Loading the HYDRO module
========================

To activate the HYDRO module, it should be selected in the module scrolling list
or its icon clicked on in the ribbon: |Hydro_module_button|.

  |Hydro_module_launch|


Importing and georeferencing a first image
==========================================

Several images need to be imported in order to construct the contour of the river.
In this case, they are maps that can be downloaded from sites such as `Géoportail`_ or the IGN website.
Géoportail provides the means to find point coordinates in an image, needed for georeferencing.

.. _Géoportail: http://www.geoportail.gouv.fr/accueil

To import an image, open the pop-up menu (right click) in the SALOME HYDRO study tree *IMAGES* folder:

  |import_image|

To import an image, open the pop-up menu (right click) in the SALOME HYDRO study tree IMAGES folder::

  <appli_xxx>/bin/salome/test/HYDRO/garonne_ign_01.png

The image is displayed with two points, *A*and *B*, which will be repositioned at locations
whose coordinates in the Lambert 93 system are known, selected far enough apart to maintain
a good level of precision.

  |import_image2|

The two specific points for which the coordinates were previously located with Géoportail are shown below:

  |selection_A|

One point below and to the left of the figure *92*:  coordinates (471562, 6367750).

  |selection_B|

The second at the centre of the cross next to *Hautevignes*: coordinates (489400, 6377020).

 * **Note**: To control the zoom on the map, use the <Ctrl> key with the left mouse button.
   To move around on the map, the <Ctrl> key should be used with the mouse wheel [or the middle mouse button].

Select point A then point B sequentially, using the *Activate point A selection*
then *Activate point B selection* buttons. Once the two points have been selected,
the georeferencing dialog box looks like this:

  |selection_B2|

For the x,y coordinates of each point, the first column gives the coordinates of the image pixel and the second
column gives the geographical references.

The georeferencing is validated by clicking on the *Apply and Close* button.
You must ensure that the "eye" icon to the left of the image name in the tree is active
and re-centre the image using the *fit all* button: |fit_all|.

 The image obtained is slightly tilted to the right.

 * Note: in this view, the zoom may be performed with the mouse wheel.
   The zoom may be centred on the mouse position or at the centre of the image.
   This option is accessible in the *File/preferences* menu under the *SALOME* section,
   *3D viewer* tab, *zooming style*:

  |zoom_style|

When you move the mouse over the map, the cursor coordinates are displayed in the status bar at the bottom left
of the application.

 * **Note**: you must get used to **saving the current study regularly** (menu *File/Save* or *File/Save as*).
   It is also advised to save the different steps of the study in different files:
   *there have been previous instances where, under specific circumstances that are difficult to reproduce,
   the study was saved in an incomplete state*. This problem has only arisen in cases where studies
   incorporating data from several modules were being reloaded without the HYDRO module being activated
   before the save operation.
   To guard against the risk of an incomplete save, check that the HYDRO module is activated before saving the study.

Importing a second image, georeferenced from the first
======================================================

The second image is stored in the same place as the first ::

  <appli_xxx>/bin/salome/test/HYDRO/garonne_ign_02.png

This image is georeferenced using choose points on the reference image option in the diolog box.
Once the first image has been selected as reference, the two images are seen one above the other.
First, a point A is selected at the same location on both images, then a point B.
These two points should be as far apart as possible and the zoom sufficiently close to be precise.
The dialog box should now look like the one below:

  |import_image_B|

After having confirmed the dialog instructions and then displayed both images and re-centred the view,
the following is obtained:

  |deux_images|

Definition of a local coordinate system
=======================================

Using a local reference point as origin reduces the size of the numbers being handled.
Changing the origin significantly improves the precision of the geometry, mesh and calculation steps.
On the contrary, keeping the large number values of the initial coordinates makes it impossible
to obtain a good quality mesh.

The coordinates of the new origin are entered via the *HYDRO/Change Local CS* menu.

In this example, the point (430 000, 6 350 000), located to the south west of our study domain, is entered.

After confirming the change of origin, one sees that the cursor coordinates, which are displayed in the status bar
at the bottom left of the application, are provided in both coordinate systems (local and global).

 * **Note**: this change of origin can be made several times, at different points in the study,
   as everything that was previously imported is transposed by the application.
   However it is preferable to do this quite early to minimise the risks of error or inconsistency and,
   in any event, before exporting the HYDRO module elements, particularly the model geometry.
   It is recommended to set the local coordinate system early in the study,
   as soon as the extent of the computational domain is known.

 * After a change of origin, some entries in the study tree are marked in italic blue: they must be updated.
   Use the context menu *update*, then reset the view with *Fit All* command.

Python dump
===========

All actions/operations completed through the graphic interface have their equivalent in Python script.
A script dump of the study can be made using the *File/Dump Study* menu.
The resulting Python script can be used to reconstruct the study data in a new SALOME study.
This script is of course editable.

The dump can be made after definition of the local coordinate system and the obtained file compared with ::

  <appli_xxx>/bin/salome/test/HYDRO/h003_changeLCS.py

The correct execution of the dump can be checked by starting again with a new study
(restart SALOME, new document or just new document),
then menu *File/Load Script* and activation of the HYDRO module.

.. only:: html

   :ref:`ref_english_exempleInondation`

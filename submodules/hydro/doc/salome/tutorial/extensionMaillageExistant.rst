..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

###############################################
Extension d'un maillage existant
###############################################

.. |maillageExistant| image:: /_static/maillageExistant.png
   :align: middle

.. |dialogMeshEdgesToShapes| image:: /_static/dialogMeshEdgesToShapes.png
   :align: middle

.. |qgisDomaineInitial| image:: /_static/qgisDomaineInitial.png
   :align: middle

.. |contourExtensionSimple| image:: /_static/contourExtensionSimple.png
   :align: middle

.. |casCalculExtensionSimple| image:: /_static/casCalculExtensionSimple.png
   :align: middle

.. |casCalculExtensionSimple_0| image:: /_static/casCalculExtensionSimple_0.png
   :align: middle

.. |casCalculExtensionSimple_1| image:: /_static/casCalculExtensionSimple_1.png
   :align: middle

.. |geometrieExtensionSimple| image:: /_static/geometrieExtensionSimple.png
   :align: middle

.. |maillageExtensionSimpleGlobal| image:: /_static/maillageExtensionSimpleGlobal.png
   :align: middle

.. |maillageExtensionSimpleGlobalHyp| image:: /_static/maillageExtensionSimpleGlobalHyp.png
   :align: middle

.. |maillageExtensionSimpleImport| image:: /_static/maillageExtensionSimpleImport.png
   :align: middle

.. |maillageExtensionSimpleImportHyp| image:: /_static/maillageExtensionSimpleImportHyp.png
   :align: middle

.. |maillageExtensionSimpleResult| image:: /_static/maillageExtensionSimpleResult.png
   :align: middle

.. |qgisDessinExtensionAmont| image:: /_static/qgisDessinExtensionAmont.png
   :align: middle

.. |dialogAdjustShapeToMesh| image:: /_static/dialogAdjustShapeToMesh.png
   :align: middle

.. |extensionAmontAjustShapes| image:: /_static/extensionAmontAjustShapes.png
   :align: middle

.. |limiteExtensionAmont| image:: /_static/limiteExtensionAmont.png
   :align: middle

.. |geometrieExtensionAmont| image:: /_static/geometrieExtensionAmont.png
   :align: middle

.. |maillageExtensionAmontResult| image:: /_static/maillageExtensionAmontResult.png
   :align: middle

.. |deuxMaillages| image:: /_static/deuxMaillages.png
   :align: middle

.. |raccordDeuxMaillages| image:: /_static/raccordDeuxMaillages.png
   :align: middle

.. |troisRegions| image:: /_static/troisRegions.png
   :align: middle

.. |casCalculDeuxMaillages| image:: /_static/casCalculDeuxMaillages.png
   :align: middle

.. |geometrieRaccord| image:: /_static/geometrieRaccord.png
   :align: middle

.. |maillageRaccord| image:: /_static/maillageRaccord.png
   :align: middle

.. |zoomMaillageRaccord| image:: /_static/zoomMaillageRaccord.png
   :align: middle

.. |deuxDomainesRecouvrants| image:: /_static/deuxDomainesRecouvrants.png
   :align: middle

.. |polygoneDecoupe| image:: /_static/polygoneDecoupe.png
   :align: middle

.. |maillageDecoupe| image:: /_static/maillageDecoupe.png
   :align: middle

.. |domaineDoubleEnglobant| image:: /_static/domaineDoubleEnglobant.png
   :align: middle

.. |geometrieDoubleEnglobante| image:: /_static/geometrieDoubleEnglobante.png
   :align: middle

.. |maillageDoubleEnglobant| image:: /_static/maillageDoubleEnglobant.png
   :align: middle


Lors de la reprise d'anciennes études, il est fréquent de vouloir agrandir le domaine de l'étude,
tout en gardant si possible le maillage existant.
Nous supposons ici que nous ne disposons que d'un maillage au format MED, avec éventuellement des groupes à conserver.
Ce maillage doit être remis dans le système de coordonnées de la nouvelle étude, ici Lambert 93.
Nous pouvons garder un décalage de l'origine.

Le script *<appli_xxx>/bin/salome/test/HYDRO/g022_extensionSimpleComplete.py* permet de générer ce maillage, pour les besoins du tutoriel.
Ce maillage est alors disponible ici : *<appli_xxx>/bin/salome/test/HYDRO/tmp_test/garonne_2.med*.
Il est en coordonnées Lambert 93, avec une origine à (430000, 6350000).

  |maillageExistant|

Une fois ce maillage généré, nous poursuivons dans une nouvelle étude SALOME, pour repartir d'un module HYDRO vierge.

Afin de pouvoir facilement dessiner les contours de l'extension dans Qgis ou avec le module HYDRO,
nous générons des shapefiles correspondants aux frontières du domaine existant,
et des groupes d'edges présents dans le maillage.

Pour cela, nous utilisons le dialogue *Mesh edges to shapes* du module HYDRO (menu *HYDRO / Python plugins / Mesh edges to shapes*).

  |dialogMeshEdgesToShapes|

Les coordonnées de l'origine du maillage doivent être correctement renseignées pour un positionnement correct des shapes générées.
Comme indiqué précédemment, le maillage est en coordonnées Lambert 93, avec une origine à (430000, 6350000).
Le répertoire de sortie contiendra les shapes et le maillage, enrichi d'un groupe correspondant aux mailles de bord (*FreeBorders*).

Dans Qgis, nous pouvons importer les shapefiles *garonne_2_brd_FreeBorders.shp* et *garonne_2_brd_FreeBorders_pts.shp*
correspondants aux arêtes et noeuds des limites du maillage.

  |qgisDomaineInitial|

Extension simple, englobant complètement le maillage initial
============================================================

Le contour du nouveau domaine est dessiné dans Qgis ou HYDRO. Un exemple de shape est fourni :
*<appli_xxx>/bin/salome/test/HYDRO/extension_1_1.shp*.

  |contourExtensionSimple|

Il faut tout de suite faire le changement de repère local dans la nouvelle étude HYDRO : origine à (430000, 6350000).
Ce repère local doit être **strictement identique** au repère local du maillage existant, pour permettre la création d'une extension.

Il est souvent pratique d'importer la shapefile du contour du domaine d'origine en tant que spline (éditer la polyligne pour la transformer en spline).
Nous ne tiendrons pas compte des éventuelles îles (régions insubmersibles) du domaine initial.

* **Remarque:** La transformation du contour en spline facilite la sélection de groupe d'edges dans GEOM, si on a besoin d'en faire
  (une seule ligne à selectionner plutot que beaucoup de segments), mais, l'algorithme de maillage dans lequel on associe
  la ligne de contour au maillage d'origine échoue parfois avec certaines splines. Il faut alors reprendre le problème
  en changeant la nature de la polyligne.

Nous créons deux objets naturels, le domaine étendu, et le domaine initial, **marqué en tant que région insubmersible**.
Il ne faut pas tenir compte de l'ilôt dans le maillage existant.

  |casCalculExtensionSimple|

Nous créons un cas de calcul, en mode manuel, en ne prenant que les deux objets naturels ci-dessus.

  |casCalculExtensionSimple_0|

Lors de la création du cas, il faut inclure les groupes de bord générés automatiquement, pour ne pas avoir à les recréer lors de l'étape de géométrie.

  |casCalculExtensionSimple_1|

Après export dans GEOM, la géométrie ressemble à ceci :

  |geometrieExtensionSimple|

Pour mailler cette géométrie, il faut charger préalablement le maillage du domaine d'origine, contenant un groupe de mailles définissant les bords du maillage.
Ce groupe est généré lors de la génération des shapefiles de frontières, comme vu précédemment.

Nous procédons classiquement pour le maillage étendu et son contour extérieur :

  |maillageExtensionSimpleGlobal|

  |maillageExtensionSimpleGlobalHyp|

Nous utilisons un algorithme particulier pour le contour intérieur (le bord du maillage initial) : import des éléments d'un maillage existant.
Dans le dialogue *Hypothesis construction*, nous sélectionnons le groupe de mailles du bord extérieur du maillage existant.

  |maillageExtensionSimpleImport|

  |maillageExtensionSimpleImportHyp|

Après calcul, le maillage obtenu ressemble à ceci :

  |maillageExtensionSimpleResult|

Le maillage obtenu est conforme, il contient les groupes du maillage d'origine. Selon le sens de dessin des contours,
les nouvelles mailles peuvent être à l'envers, comme dans l'image ci-dessus. Il reste alors à inverser les nouvelles mailles,
détecter et corriger les triangles de bord surcontraints, et, bien sûr, introduire d'éventuels raffinements dans l'extension
pour avoir un maillage plus réaliste.

Par exemple, pour prolonger le lit mineur sous forme d'une région dans le cas de calcul de l'extension,
il faut dessiner une shape qui devra se raccorder correctement au lit mineur du maillage d'origine.
La technique à utiliser est vue ci-dessous, et s'appuie sur les outils d'ajustement de shapefile.

Extension partielle
===================

Ici nous souhaitons faire une extension à l'amont du maillage existant, en gardant une partie des limites du maillage d'origine.
Le contour de l'extension est dessiné approximativement, sachant qu'il va ensuite être automatiquement ajusté pour couper le contour du maillage d'origine
en passant par les noeuds de celui-ci.

L'ajustement automatique suppose que la ligne dessinée coupe la ligne de bord du maillage d'origine en deux points.
L'algorithme détecte les deux noeuds de bord du maillage existant les plus proches de la ligne dessinée et déplace les deux noeuds en regard sur la ligne dessinée
pour les faire coïncider avec ceux du maillage existant. Dans Qgis, l'affichage des noeuds du maillage d'origine aide au dessin.
En dessinant la ligne d'extension, il faut poser des noeuds au voisinage des noeuds de bord.

  |qgisDessinExtensionAmont|

Dans le dialogue *Adjust shapefile to mesh* du module HYDRO (menu *HYDRO / Python plugins / Adjust shapefile*)
il faut cocher les options *"split mesh edges shapefile"* et *"Split shapefile to adjust"*.
Les shapes modifiées sont rangées dans les répertoires respectifs des shapes de départ, avec des suffixes *"_adj"* et *"_split"*.

  |dialogAdjustShapeToMesh|

A l'import de ces shapes on obtient ceci :

  |extensionAmontAjustShapes|

il faut selectionner une des sections amont (en vert), et à l'aide du menu contextuel *Merge polylines*, inclure l'autre, pour produire ceci :

  |limiteExtensionAmont|

**Attention**, il vaut mieux que la section correspondant au maillage d'origine soit transformée en spline pour faciliter sa manipulation par la suite
*(voir la remarque du paragraphe précédent sur l'intérêt et les inconvénients de cette transformation)*.

Nous pouvons alors créer un cas de calcul très simple avec un seul objet naturel construit sur le contour amont que l'on vient de produire.
Après export dans GEOM, on recrée un groupe correspondant à la limite du maillage d'origine, en vert ci-dessous.

  |geometrieExtensionAmont|

Le maillage va être généré comme le précédent, avec l'algorithme d'import des éléments d'un maillage existant.

  |maillageExtensionAmontResult|

Regroupement de deux maillages existants
========================================

Etudions un cas plus complexe : le raccord de deux maillages existants.
Les exemples ici concernent deux zones consécutives d'un même fleuve. nous allons examiner successivement le cas où les deux maillages
sont disjoints et le cas où ils se recouvrent partiellement.

Les deux maillages sont disjoints
---------------------------------

Les scripts *<appli_xxx>/bin/salome/test/HYDRO/g022_extensionSimpleComplete.py*
et *<appli_xxx>/bin/salome/test/HYDRO/g027_domaineAmont.py*
permettent de générer ces maillages, pour les besoins du tutoriel.

  |deuxMaillages|

Il faut générer les shapefiles correspondant aux frontières des deux domaines, comme précédemment.

Le contour du raccord des domaines est dessiné dans Qgis ou HYDRO. Un exemple de shape est fourni :
*<appli_xxx>/bin/salome/test/HYDRO/raccord_4_1.shp*.
Comme nous allons utiliser l'outil d'ajustement automatique de la shape pour la faire coïncider avec les noeuds des maillages,
il faut dessiner le contour du raccord en suivant les recommandations données plus haut pour l'extension partielle.

  |raccordDeuxMaillages|

Nous utilisons une première fois le dialogue *Adjust shapefile to mesh* avec le contour du premier maillage,
en décochant les options *"split mesh edges shapefile"* et *"Split shapefile to adjust"*.

Après import de la shapefile ajustée au premier maillage, nous réutilisons le dialogue *Adjust shapefile to mesh* avec le contour du deuxième maillage
(et la shapefile ajustée), en décochant toujours les options *"split mesh edges shapefile"* et *"Split shapefile to adjust"*.

Nous pouvons importer la shapefile ajustée au deuxième maillage. **Dans cet exemple, nous ne transformons aucune des polylignes importées en spline**,
une des splines posant problème à l'algorithme de maillage dans ce cas précis (il est toujours possible de refaire un test avec spline).

Nous créons alors trois objets naturels avec la shapefile ajustée du raccord des domaines, et avec les deux domaines d'origine,
**marqués en tant que régions insubmersibles**.

  |troisRegions|

Nous pouvons ensuite créer un cas de calcul (mode manuel) avec ces trois objets.

  |casCalculDeuxMaillages|

Après export du cas dans GEOM, il faut identifier les groupes d'edges correspondant aux limites des deux maillages.

  |geometrieRaccord|

Pour le maillage, nous procédons comme pour les cas précédents, avec l'algorithme d'import des éléments d'un maillage existant.

  |maillageRaccord|

Là aussi, pour obtenir un maillage plus intéressant au niveau du lit mineur, il faudrait rajouter des lignes
de contraintes ou des objets naturels supplémentaires, en utilisant l'algorithme d'ajustement de shape pour faire
des connexions propres.

  |zoomMaillageRaccord|

Les deux maillages se recouvrent partiellement
----------------------------------------------

Les scripts *<appli_xxx>/bin/salome/test/HYDRO/g022_extensionSimpleComplete.py*
et *<appli_xxx>/bin/salome/test/HYDRO/g028_domaineAmontChevauchant.py*
permettent de générer ces maillages, pour les besoins du tutoriel.

  |deuxDomainesRecouvrants|

Nous allons nous ramener au cas précédent en enlevant des mailles dans le domaine amont.
Pour cela, nous avons besoin d'un contour de découpe. Ce contour peut être dessiné dans Qgis, en visualisant les contours des deux maillages.
Lors de l'opération de découpe, seules sont supprimées les mailles intégralement comprises dans le polygone de découpe.
Il faut laisser un espace suffisant entre les deux maillages pour permettre un remaillage correct (sans mailles distordues).

  |polygoneDecoupe|

La découpe nécessite d'avoir le maillage et le polygone dans le même référentiel. Le polygone étant en Lambert 93 sans décalage d'origine,
nous translatons le maillage pour l'amener dans ce même repère, avec le dialogue *Change coordinates*.

L'opération de découpe n'est accessible qu'en script Python, depuis la console embarquée de SALOME, ou depuis un shell SALOME : ::

  <appli_XXX>/salome shell
  python

  from salome.hydrotools.cutMesh import cutMesh
  meshFile = cutMesh(meshToCut, cutShape, meshCut, offsetX, offsetY)

Ici,

  * *meshToCut* : le chemin complet du maillage à découper, translaté dans le même repère que le polygone de découpe, exemple :"/home/user/meshOrig.med"
  * *cutShape* : le chemin complet de la shapefile du polygone de découpe,
  * *meshCut* : le chemin complet du maillage découpé, retranslaté.
  * *offsetX, offsetY* : les coordonnées du repère local du maillage découpé.

  |maillageDecoupe|

Le **maillage découpé est toujours en Lambert 93**.

Nous pouvons alors générer le contour du domaine découpé avec le plugin *Mesh edges to shape*, et le contour du nouveau maillage regroupant les deux maillages d'origine.
Ici, nous dessinons un contour englobant complètement les deux maillages. Un exemple de shape est fourni :
*<appli_xxx>/bin/salome/test/HYDRO/raccord_4_2.shp*.

  |domaineDoubleEnglobant|

La suite est alors assez similaire au cas du domaine englobant un seul maillage initial.
Chargez les shapes correspondants aux deux maillages (celui découpé et celui du modèle aval). **Faire bien attention de charger les deux polylines dans un même référentiel** (le contour du maillage découpé est, à ce stade, en Lambert93). **Si nécessaire faire le changement de repère local.**

Ici, nous aurons deux objets naturels non submersibles, une géométrie avec deux trous :

  |geometrieDoubleEnglobante|

**Faire attention de charger les deux maillages (aval et découpé) dans le même référentiel que le calculation case de *HYDRO*.**
**Attention**, il faut charger le maillage découpé qui a été créé au moment de l'utilisation du plugin *Mesh edges to shape* et qui s'appelle **xxx_brd.med**. Celui-ci contient le groupe d'edge *FreeBorders* (contour fermé) dont on aura besoin pour faire le sous-maillage s'appuyant sur le contour du maillage découpé.

Pour le maillage, nous utiliserons l'algorithme d'import des éléments d'un maillage existant, pour chaque maillage original.

  |maillageDoubleEnglobant|

Comme pour les exemples précédents, il faudrait raffiner un peu l'exemple pour avoir un maillage exploitable.

.. only:: html
 
   :ref:`ref_outilsReprise`


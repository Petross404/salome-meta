..
   Copyright (C) 2015-2016 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

#########################################
Lancement du calcul TELEMAC
#########################################

.. |HYDROSolver| image:: /_static/HYDROSolver.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |case_study| image:: /_static/case_pytel.png
   :align: middle
   :width: 16pt
   :height: 16pt

.. |CreateCasePytel| image:: /_static/CreateCasePytel.png
   :align: middle

.. |SelectCommandPytel| image:: /_static/SelectCommandPytel.png
   :align: middle

.. |CasPytel| image:: /_static/CasPytel.png
   :align: middle

.. |CasPytelRepTravail| image:: /_static/CasPytelRepTravail.png
   :align: middle

.. |CasPytelFichierCas| image:: /_static/CasPytelFichierCas.png
   :align: middle

.. |CasPytelEntreeMedIncomplete| image:: /_static/CasPytelEntreeMedIncomplete.png
   :align: middle

.. |CasPytelSave| image:: /_static/CasPytelSave.png
   :align: middle

.. |CasPytelArbre| image:: /_static/CasPytelArbre.png
   :align: middle

.. |CasPytelComputeCase| image:: /_static/CasPytelComputeCase.png
   :align: middle

.. |CasPytelCalcul| image:: /_static/CasPytelCalcul.png
   :align: middle

.. |CasPytelFinCalcul| image:: /_static/CasPytelFinCalcul.png
   :align: middle
   
.. |eficas_05| image:: /_static/eficas_05.png
   :align: middle
   
.. |eficas_06| image:: /_static/eficas_06.png
   :align: middle
   
.. |eficas_07| image:: /_static/eficas_07.png
   :align: middle
   
.. |eficas_08| image:: /_static/eficas_08.png
   :align: middle
   
.. |eficas_09| image:: /_static/eficas_09.png
   :align: middle
   
.. |eficas_10| image:: /_static/eficas_10.png
   :align: middle

Il faut maintenant activer le module HYDROSOLVER, via la liste défilante des modules, ou son icône dans le bandeau : |HYDROSolver|.
Le module HYDROSOLVER prend en charge les calculs Telemac et Mascaret ainsi que leur couplages.

Création du Cas de Calcul Parameter_Study
=========================================

Parameter_Study permet de lancer une exécution simple du code Telemac.
Nous créons un cas de Calcul Parameter_Study avec la commande *Create Parameter Study* |case_study| disponible dans le menu *hydro*
ou dans une icône du bandeau.

* **Remarque** : Les icônes du bandeau relatives au module en cours (en haut à droite) ne sont pas forcément visibles : 
  le menu popup (clic droit) dans le bandeau montre les groupes d'icônes affichés et ceux qui ne le sont pas, et permet de les gérer.
  
  |eficas_05|  

  |eficas_06|  

Aller chercher le fichier cas créé précédement avec EFICAS.

  |eficas_07|  

Enregistrer le cas study

  |eficas_08|  

Le cas apparaît dans l'arbre d'étude.

  |CasPytelArbre|

Lancement du Cas de calcul Parameter_Study
==========================================

Pour lancer le calcul Telemac, nous utilisons le menu popup du Cas, la commande *Compute Case*.

  |eficas_09|

Le calcul se déroule, la log s'affiche dans une fenêtre.

  |eficas_10|

A la fin du calcul on peut fermer la fenêtre.

  |CasPytelFinCalcul|
 
.. only:: html
 
   :ref:`ref_exempleInondation`

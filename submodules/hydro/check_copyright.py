
import os

theCopyright_line1 = '// Copyright (C) 2014-2015  EDF-R&D'
theCopyright_line2 = '// This library is free software; you can redistribute it and/or'

def check_file( theFile ):
  global files

  if theFile=='src/HYDRO_tests/test_HYDROData_LandCoverMap.cxx':
    return True; #in the file the unicode is used

  #print 'Checking %s...' % theFile
  aLines = open( theFile, 'r' ).readlines()
  if( len( aLines )==0 ):
    print('Empty file: ', theFile)
    return False

  aFirstLine = aLines[0][:-1]
  aSecondLine = aLines[1][:-1]
  if( aFirstLine != theCopyright_line1 or aSecondLine != theCopyright_line2 ):
    print('Incorrect copyright in the', theFile)
 

def check_folder( theFolder ):
  aFiles = os.listdir( theFolder ); 
  aHeaders = [x for x in aFiles if x.endswith('.h')];
  aSources = [x for x in aFiles if x.endswith('.cxx')];
  aSIPs = [x for x in aFiles if x.endswith('.sip')];
  for aHeader in aHeaders:
    check_file( theFolder + '/' + aHeader )
  for aSource in aSources:
    check_file( theFolder + '/' + aSource )
  for aSIP in aSIPs:
    check_file( theFolder + '/' + aSIP )


check_folder( 'src/HYDROData' )
check_folder( 'src/HYDROGUI' )
check_folder( 'src/HYDROPy' )
check_folder( 'src/HYDRO_tests' )

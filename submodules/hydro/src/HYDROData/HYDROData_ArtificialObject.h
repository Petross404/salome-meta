// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_ArtificialObject_HeaderFile
#define HYDROData_ArtificialObject_HeaderFile

#include <HYDROData_Object.h>

/**\class HYDROData_ArtificialObject
 * \brief The artificial objects are objects created or planned for creation by human.
 *
 */
class HYDROData_ArtificialObject : public HYDROData_Object
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Object::DataTag_First + 100 ///< first tag, to reserve
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_ArtificialObject, HYDROData_Object);

protected:

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_ArtificialObject( Geometry );

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_ArtificialObject();
};

#endif

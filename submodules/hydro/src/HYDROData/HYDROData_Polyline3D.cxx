// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Polyline3D.h"

#include "HYDROData_IAltitudeObject.h"
#include "HYDROData_Document.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_Profile.h"
#include "HYDROData_ProfileUZ.h"
#include "HYDROData_ShapesTool.h"
#include "HYDROData_Tool.h"

#include <BRep_Tool.hxx>

#include <Geom_BSplineCurve.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <GCPnts_QuasiUniformAbscissa.hxx>
#include <GCPnts_AbscissaPoint.hxx>

#include <gp_Pnt2d.hxx>
#include <gp_XY.hxx>
#include <gp_XYZ.hxx>

#include <TColStd_Array1OfReal.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Wire.hxx>

#include <TopExp.hxx>

#include <TopExp_Explorer.hxx>

#include <TopTools_SequenceOfShape.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

#include <QColor>
#include <QStringList>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Polyline3D,HYDROData_Object)


HYDROData_Polyline3D::HYDROData_Polyline3D()
: HYDROData_Object( Geom_3d )
{
}

HYDROData_Polyline3D::~HYDROData_Polyline3D()
{
}

QStringList HYDROData_Polyline3D::DumpToPython( const QString& thePyScriptPath,
                                                MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aPolylineName = GetObjPyName();

  Handle(HYDROData_PolylineXY) aRefPolyline = GetPolylineXY();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefPolyline, "SetPolylineXY" );

  Handle(HYDROData_ProfileUZ) aRefProfileUZ = GetProfileUZ();
  if ( !aRefProfileUZ.IsNull() )
  {
    Handle(HYDROData_Profile) aProfile = 
      Handle(HYDROData_Profile)::DownCast( aRefProfileUZ->GetFatherObject() );
    if ( checkObjectPythonDefinition( thePyScriptPath, theTreatedObjects, aResList, aProfile ) )
    {
      QString aProfileName = aProfile->GetObjPyName();
      if ( !aProfileName.isEmpty() )
      {
        aResList << QString( "%1.SetProfileUZ( %2.GetProfileUZ() )" )
                     .arg( aPolylineName ).arg( aProfileName );
      }
    }
  }
  else
  {
    Handle(HYDROData_IAltitudeObject) aRefBathymetry = GetAltitudeObject();
    if ( !aRefBathymetry.IsNull() )
    {
      Handle(HYDROData_ProfileUZ) aChildProfileUZ = GetChildProfileUZ();
      if ( !aChildProfileUZ.IsNull() )
      {
        Handle(HYDROData_Profile) aProfile = 
          Handle(HYDROData_Profile)::DownCast( aChildProfileUZ->GetFatherObject() );
        if ( checkObjectPythonDefinition( thePyScriptPath, theTreatedObjects, aResList, aProfile ) )
        {
          QString aProfileName = aProfile->GetObjPyName();
          if ( !aProfileName.isEmpty() )
          {
            aResList << QString( "%1.SetChildProfileUZ( %2.GetProfileUZ() )" )
                         .arg( aPolylineName ).arg( aProfileName );
          }
        }
      }

      setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefBathymetry, "SetAltitudeObject" );
    }
  }

  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aPolylineName );
  aResList << QString( "" );

  return aResList;
}

HYDROData_SequenceOfObjects HYDROData_Polyline3D::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Object::GetAllReferenceObjects();

  Handle(HYDROData_PolylineXY) aPolylineXY = GetPolylineXY();
  if ( !aPolylineXY.IsNull() )
    aResSeq.Append( aPolylineXY );

  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ();
  if ( !aProfileUZ.IsNull() )
    aResSeq.Append( aProfileUZ );

  Handle(HYDROData_ProfileUZ) aChildProfileUZ = GetChildProfileUZ( false );
  if ( !aChildProfileUZ.IsNull() )
    aResSeq.Append( aChildProfileUZ );

  return aResSeq;
}

void HYDROData_Polyline3D::Update()
{
  HYDROData_Object::Update();

  Handle(HYDROData_PolylineXY) aPolylineXY = GetPolylineXY();
  if ( aPolylineXY.IsNull() )
    return;

  bool anIsSectionClosed = aPolylineXY->IsClosedSection( 0 );
  HYDROData_IPolyline::SectionType aSectionType = aPolylineXY->GetSectionType( 0 );
  NCollection_Sequence<Polyline3DPoint> aResPoints = GetProfilePoints3D(false);

  TopoDS_Wire aResWire = HYDROData_PolylineXY::BuildWire( aSectionType, anIsSectionClosed, aResPoints );
  SetTopShape( aResWire );
  SetShape3D( aResWire );
} 

HYDROData_Polyline3D::Polyline3DPoints HYDROData_Polyline3D::GetProfilePoints3D(bool IsConvertToGlobal)
{
  Handle(HYDROData_PolylineXY) aPolylineXY = GetPolylineXY();
  if ( aPolylineXY.IsNull() )
    return Polyline3DPoints();

  HYDROData_IPolyline::PointsList aPolylinePoints = aPolylineXY->GetPoints( 0 );
  if ( aPolylinePoints.IsEmpty() )
    return Polyline3DPoints();

  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ();

  Handle(HYDROData_IAltitudeObject) anAltitude = GetAltitudeObject();
  if ( !anAltitude.IsNull() )
    aProfileUZ = GetChildProfileUZ();

  if ( aProfileUZ.IsNull() )
    return Polyline3DPoints();

  HYDROData_IPolyline::PointsList aProfilePoints = aProfileUZ->GetPoints();
  if ( aProfilePoints.IsEmpty() )
    return NCollection_Sequence<HYDROData_Polyline3D::Polyline3DPoint>();

  const HYDROData_IPolyline::Point& aFirstPoint = aPolylinePoints.First();
  const HYDROData_IPolyline::Point& aLastPoint = aPolylinePoints.Last();

  const HYDROData_IPolyline::Point& aFirstParPoint = aProfilePoints.First();
  const HYDROData_IPolyline::Point& aLastParPoint = aProfilePoints.Last();

  double aPolylineCommonDist = aPolylineXY->GetDistance( 0, aPolylinePoints.Size() - 1 );
  double aParCommonDist = gp_Pnt2d( aFirstParPoint.X(), 0 ).Distance( gp_Pnt2d( aLastParPoint.X(), 0 ) );

  NCollection_Sequence<Polyline3DPoint> aResPoints;
  
  // Add first point as is
  aResPoints.Append( Polyline3DPoint( aFirstPoint.X(), aFirstPoint.Y(), aFirstParPoint.Y() ) );

  for ( int i = 2, aNbPoints = aPolylinePoints.Size(); i < aNbPoints; ++i )
  {
    const HYDROData_IPolyline::Point& aPolylinePoint = aPolylinePoints.Value( i );

    double aDistance = aPolylineXY->GetDistance( 0, i - 1 );

    double aParLen = ( aDistance / aPolylineCommonDist ) * aParCommonDist;
    double aDepth = HYDROData_ProfileUZ::GetDepthFromDistance( aProfilePoints, aParLen );

    Polyline3DPoint aCompPoint( aPolylinePoint.X(), aPolylinePoint.Y(), aDepth );
    aResPoints.Append( aCompPoint );
  }

  // Add last point as is
  aResPoints.Append( Polyline3DPoint( aLastPoint.X(), aLastPoint.Y(), aLastParPoint.Y() ) );

  if (IsConvertToGlobal)
  {
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    for (int i=1;i<=aResPoints.Size();i++)
    {
      gp_XYZ& point = aResPoints.ChangeValue(i);
      aDoc->Transform( point, false );
    }
  }
  return aResPoints;
}

QColor HYDROData_Polyline3D::DefaultFillingColor() const
{
  return QColor( Qt::transparent );
}

QColor HYDROData_Polyline3D::DefaultBorderColor() const
{
  return QColor( Qt::red );
}

bool HYDROData_Polyline3D::SetPolylineXY( const Handle(HYDROData_PolylineXY)& thePolyline,
                                          const bool                          theIsUpdateProfile )
{
  if ( thePolyline.IsNull() )
    return false;
  
  Handle(HYDROData_PolylineXY) aPrevPolyline = GetPolylineXY();
  if ( IsEqual( aPrevPolyline, thePolyline ) )
    return true;

  SetReferenceObject( thePolyline, DataTag_PolylineXY );

  // Update the child profile object 
  if ( theIsUpdateProfile )
    updateChildProfilePoints();

  // Indicate model of the need to update the polyline presentation
  Changed( Geom_2d );

  return true;
}

Handle(HYDROData_PolylineXY) HYDROData_Polyline3D::GetPolylineXY() const
{
  return Handle(HYDROData_PolylineXY)::DownCast( 
           GetReferenceObject( DataTag_PolylineXY ) );
}

void HYDROData_Polyline3D::RemovePolylineXY()
{
  Handle(HYDROData_PolylineXY) aPrevPolyline = GetPolylineXY();
  if ( aPrevPolyline.IsNull() )
    return;

  ClearReferenceObjects( DataTag_PolylineXY );

  // Indicate model of the need to update the polyline presentation
  Changed( Geom_2d );
}

bool HYDROData_Polyline3D::SetProfileUZ( const Handle(HYDROData_ProfileUZ)& theProfile )
{
  if ( theProfile.IsNull() )
    return false;
  
  Handle(HYDROData_ProfileUZ) aPrevProfile = GetProfileUZ();
  if ( IsEqual( aPrevProfile, theProfile ) )
    return true;

  SetReferenceObject( theProfile, DataTag_ProfileUZ );

  // Remove the bathymetry, because one altitude object can be presented at time
  RemoveAltitudeObject();

  // Indicate model of the need to update the polyline presentation
  Changed( Geom_Z );

  return true;
}

Handle(HYDROData_ProfileUZ) HYDROData_Polyline3D::GetProfileUZ() const
{
  return Handle(HYDROData_ProfileUZ)::DownCast( 
           GetReferenceObject( DataTag_ProfileUZ ) );
}

void HYDROData_Polyline3D::RemoveProfileUZ()
{
  Handle(HYDROData_ProfileUZ) aPrevProfile = GetProfileUZ();
  if ( aPrevProfile.IsNull() )
    return;

  ClearReferenceObjects( DataTag_ProfileUZ );

  // Indicate model of the need to update the polyline presentation
  Changed( Geom_Z );
}

bool HYDROData_Polyline3D::SetAltitudeObject( 
  const Handle(HYDROData_IAltitudeObject)& theAltitude )
{
  Handle(HYDROData_IAltitudeObject) aPrevAltitude = GetAltitudeObject();

  if ( !HYDROData_Object::SetAltitudeObject( theAltitude ) )
    return false;

  if ( IsEqual( aPrevAltitude, theAltitude ) )
    return true;

  // Remove the u,z profile, because one altitude object can be presented at time
  RemoveProfileUZ();

  // Create the child profile object 
  updateChildProfilePoints();

  return true;
}


void HYDROData_Polyline3D::RemoveAltitudeObject()
{
  HYDROData_Object::RemoveAltitudeObject();

  // Remove the child profile object 
  removeChildProfileUZ();
}

Handle(HYDROData_ProfileUZ) HYDROData_Polyline3D::GetChildProfileUZ( const bool theIsCreate ) const
{
  Handle(HYDROData_ProfileUZ) aProfileUZ =
    Handle(HYDROData_ProfileUZ)::DownCast( GetReferenceObject( DataTag_ChildProfileUZ ) );
  if ( !theIsCreate || !aProfileUZ.IsNull() )
    return aProfileUZ;

  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return aProfileUZ;

  Handle(HYDROData_Profile) aProfile =
    Handle(HYDROData_Profile)::DownCast( aDocument->CreateObject( KIND_PROFILE ) );
  
  QString aProfilePref = GetName() + "_Profile";
  QString aProfileName = HYDROData_Tool::GenerateObjectName( aDocument, aProfilePref );

  aProfile->SetName( aProfileName );

  aProfileUZ = aProfile->GetProfileUZ();

  HYDROData_Polyline3D* me = const_cast<HYDROData_Polyline3D*>( this ); // Temporary to be revised
  me->SetChildProfileUZ( aProfileUZ );

  return aProfileUZ;
}

void HYDROData_Polyline3D::SetChildProfileUZ( const Handle(HYDROData_ProfileUZ)& theProfile )
{
  SetReferenceObject( theProfile, DataTag_ChildProfileUZ );
}

HYDROData_IPolyline::PointsList generateProfileUZPoints(
  const Handle(HYDROData_PolylineXY)&      thePolyline,
  const Handle(HYDROData_IAltitudeObject)& theAltitude )
{
  HYDROData_IPolyline::PointsList aPointsList;
  if ( thePolyline.IsNull() || theAltitude.IsNull() )
    return aPointsList;

  bool anIsSectionClosed = thePolyline->IsClosedSection( 0 );
  HYDROData_IPolyline::SectionType aSectionType = thePolyline->GetSectionType( 0 );
  HYDROData_IPolyline::PointsList aPolylinePoints = thePolyline->GetPoints( 0 );
  if ( aPolylinePoints.IsEmpty() )
    return aPointsList;

  for ( int i = 1, aNbPoints = aPolylinePoints.Size(); i <= aNbPoints; ++i )
  {
    const HYDROData_PolylineXY::Point& aSectPoint = aPolylinePoints.Value( i );

    double aPointDistance = thePolyline->GetDistance( 0, i - 1 );
    double aPointDepth = theAltitude->GetAltitudeForPoint( aSectPoint );
    if( aPointDepth == theAltitude->GetInvalidAltitude() )
      aPointDepth = 0.0;

    HYDROData_IPolyline::Point anAltitudePoint( aPointDistance, aPointDepth );
    aPointsList.Append( anAltitudePoint );
  }

  return aPointsList;
}

void HYDROData_Polyline3D::updateChildProfilePoints()
{
  Handle(HYDROData_IAltitudeObject) anAltitude = GetAltitudeObject();
  if ( anAltitude.IsNull() )
    return;

  Handle(HYDROData_ProfileUZ) aChildProfileUZ = GetChildProfileUZ();
  if ( aChildProfileUZ.IsNull() )
    return;

  Handle(HYDROData_Profile) aProfile = 
    Handle(HYDROData_Profile)::DownCast( aChildProfileUZ->GetFatherObject() );
  if ( aProfile.IsNull() )
    return;

  HYDROData_IPolyline::PointsList aProfilePoints = 
    generateProfileUZPoints( GetPolylineXY(), anAltitude );
  aProfile->SetParametricPoints( aProfilePoints );

  aProfile->Update();
}

void HYDROData_Polyline3D::removeChildProfileUZ()
{
  Handle(HYDROData_ProfileUZ) aChildProfileUZ = GetChildProfileUZ( false );
  if ( aChildProfileUZ.IsNull() )
    return;

  ClearReferenceObjects( DataTag_ChildProfileUZ );

  /* Uncomment if removing is requested
  Handle(HYDROData_Profile) aProfile = 
    Handle(HYDROData_Profile)::DownCast( aChildProfileUZ->GetFatherObject() );
  if ( !aProfile.IsNull() )
    aProfile->Remove();
  */
}

HYDROData_Polyline3D::Polyline3DPoints HYDROData_Polyline3D::GetPoints( double theEqDistance ) const
{
  Polyline3DPoints aPoints;

  Handle(HYDROData_PolylineXY) aPolylineXY = GetPolylineXY();
  TopoDS_Wire aWire = TopoDS::Wire( GetShape3D() );
  if ( aPolylineXY.IsNull() || aWire.IsNull() ) {
    return aPoints; 
  }

  // Explode polyline on edges
  TopTools_SequenceOfShape anEdges;
  HYDROData_ShapesTool::ExploreShapeToShapes( aWire, TopAbs_EDGE, anEdges );

  // Get points
  if ( !anEdges.IsEmpty() ) {
    HYDROData_IPolyline::SectionType aSectionType = aPolylineXY->GetSectionType( 0 );
    
    if ( aSectionType == HYDROData_IPolyline::SECTION_POLYLINE ) {
      // Get points from wire
      /* Seems that intermediate vertices are duplicated
      TopExp_Explorer anExp( aWire, TopAbs_VERTEX );
      for ( ; anExp.More(); anExp.Next() ) {
        TopoDS_Vertex aVertex = TopoDS::Vertex( anExp.Current() );
        if ( !aVertex.IsNull() ) {
          gp_Pnt aPnt = BRep_Tool::Pnt( aVertex );
          aPoints.Append( aPnt.XYZ() );
        }
      }
      */
      TopExp_Explorer anExp( aWire, TopAbs_EDGE );
      bool isFirst = true;
      for ( ; anExp.More(); anExp.Next() ) {
        TopoDS_Edge anEdge = TopoDS::Edge( anExp.Current() );
        if ( !anEdge.IsNull() ) {
          TopoDS_Vertex aV1, aV2;
          TopExp::Vertices( anEdge, aV1, aV2 );
          if ( isFirst ) {
            gp_Pnt aPnt1 = BRep_Tool::Pnt( aV1 );
            aPoints.Append( aPnt1.XYZ() );
          }

          gp_Pnt aPnt2 = BRep_Tool::Pnt( aV2 );
          aPoints.Append( aPnt2.XYZ() );

          isFirst = false;
        }
      }
    } else {
      // Get points from spline curve
      Standard_Real aStart, anEnd;
      TopoDS_Edge anEdge = TopoDS::Edge( anEdges.First() );
      Handle(Geom_Curve) aCurve = BRep_Tool::Curve( anEdge, aStart, anEnd );

      if( theEqDistance > 0 )
      {
        GeomAdaptor_Curve anAdaptorCurve( aCurve );
        double aLength = GCPnts_AbscissaPoint::Length( anAdaptorCurve );
        int aNbPoints = ceil( aLength / theEqDistance );
        GCPnts_QuasiUniformAbscissa aDist( anAdaptorCurve, aNbPoints );
        if( aDist.IsDone() )
        {
          aNbPoints = aDist.NbPoints();
          for( int i=1; i<=aNbPoints; i++ )
          {
            double p = aDist.Parameter( i );
            gp_Pnt aPnt;
            aCurve->D0( p, aPnt );
            aPoints.Append( aPnt.XYZ() );
          }
          return aPoints;
        }
      }

      Handle(Geom_BSplineCurve) aGeomSpline = Handle(Geom_BSplineCurve)::DownCast( aCurve );

      if ( !aGeomSpline.IsNull() ) {
        int aNbKnots = aGeomSpline->NbKnots();

        TColStd_Array1OfReal aSplineKnots( 1, aNbKnots );
        aGeomSpline->Knots( aSplineKnots );

        for ( int i = 1; i <= aNbKnots; ++i ) {
          const Standard_Real& aKnot = aSplineKnots.Value( i );
          gp_Pnt aPnt;
          aGeomSpline->D0( aKnot, aPnt );
          aPoints.Append( aPnt.XYZ() );
        }
      }
    }
  }

  return aPoints; 
}


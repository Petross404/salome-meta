// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_Tool.h>
#include <HYDROData_BoundaryPolygonTools.h>

#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>


#include <BRep_Builder.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>
#include <TopExp.hxx>
#include <NCollection_List.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>

#include <TopExp_Explorer.hxx>

#include <NCollection_IndexedDataMap.hxx>
#include <BOPAlgo_BOP.hxx>
#include <BOPAlgo_Section.hxx>
#include <TopTools_MapOfShape.hxx>


#include <Basics_OCCTVersion.hxx>

static bool FindInterBySection(const TopoDS_Face& IncF, TopoDS_Shape poly)
{
  if (IncF.IsNull())
    return false;
  //make cmp from edges of Inc_sh
  TopTools_IndexedMapOfShape IncToolsE;
  TopExp::MapShapes(IncF, TopAbs_EDGE, IncToolsE);
  BRep_Builder BB;
  TopoDS_Compound aToolCmp;
  BB.MakeCompound(aToolCmp);
  for (int i=1; i<= IncToolsE.Extent();i++)
    BB.Add(aToolCmp, IncToolsE(i));
  //
  //perform bsection - poly vs aToolCmp
  BOPAlgo_Section aBOP;
  aBOP.AddArgument(poly);
  aBOP.AddArgument(aToolCmp);
  aBOP.SetRunParallel(true);

  aBOP.Perform();
  //aBOP.GetReport());

//#if OCC_VERSION_LARGE > 0x07020000
//  if (aBOP.HasErrors())
//    return false;
//#endif

  const TopoDS_Shape& aRes=aBOP.Shape();
  if (aRes.IsNull())
    return false;

  bool IsInter = false;
  TopTools_IndexedMapOfShape aShVE;
  TopExp::MapShapes(aRes, TopAbs_VERTEX, aShVE);
  if (aShVE.IsEmpty())
  {
    TopExp::MapShapes(aRes, TopAbs_EDGE, aShVE);
    if (!aShVE.IsEmpty())
      IsInter = true;
  }
  else
    IsInter = true;
  return IsInter;
}

bool HYDROData_BoundaryPolygonTools::CutTool( const TopTools_SequenceOfShape& CutTools, 
                                              NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape, TopTools_ShapeMapHasher>& ObjToRes,
                                              NCollection_IndexedDataMap<TopoDS_Shape, TopTools_MapOfShape, TopTools_ShapeMapHasher>& BPFaceToCutEdges)
{
  BOPAlgo_Builder anAlgo;
  TopTools_MapOfShape cuttedEdges; //all new edges (Obj) after cut

  for (int i=1; i<= CutTools.Length();i++)
    anAlgo.AddArgument(CutTools(i));

  for (int i = 1; i <= ObjToRes.Extent(); i++ )
  {
    const TopoDS_Shape& Obj = ObjToRes.FindKey(i);
    if (!Obj.IsNull())
      anAlgo.AddArgument(Obj);
  }

  anAlgo.Perform(); 
#if OCC_VERSION_LARGE > 0x07020000
  if (anAlgo.HasErrors())
    return false;
#endif
  TopoDS_Shape aRes = anAlgo.Shape();  
  
  BRep_Builder BB;
  for (int i = 1; i <= ObjToRes.Extent(); i++ )
  {
    const TopoDS_Shape& Obj = ObjToRes.FindKey(i);
    Standard_Real aTol;
    if (!Obj.IsNull()) 
    {
      TopTools_ListOfShape ls = anAlgo.Modified(Obj);
      TopoDS_Shape aRes;
      if (ls.IsEmpty())
      {
        aRes = Obj; //no changes
        cuttedEdges.Add(aRes);
      }
      else if (ls.Extent() == 1)
      {
        aRes = ls.First();
        cuttedEdges.Add(aRes);
      }
      else
      {
        TopoDS_Compound cmp;
        BB.MakeCompound(cmp);
        TopTools_ListIteratorOfListOfShape aItLS(ls);
        for (; aItLS.More(); aItLS.Next()) 
        {
          const TopoDS_Shape& val = aItLS.Value();
          BB.Add(cmp, val);
          cuttedEdges.Add(val);
        }
        aRes = cmp;
      }  
      ObjToRes.ChangeFromKey(Obj) = aRes;
    }
  }

  //determine the source face 
  for (int i = 1; i <= CutTools.Length(); i++ )
  {
    TopoDS_Shape F = CutTools(i);
    TopTools_ListOfShape lsF = anAlgo.Modified(F);
    if (lsF.IsEmpty())
      lsF.Append(F);
    TopoDS_Compound cmpF;
    BB.MakeCompound(cmpF);
    TopTools_ListIteratorOfListOfShape aItLS(lsF);
    for (; aItLS.More(); aItLS.Next()) 
      BB.Add(cmpF, aItLS.Value());
    //cmpF is a compound if connected faces => modified of boudnary polygon F
    TopExp_Explorer expE(cmpF, TopAbs_EDGE);
    TopTools_MapOfShape cmpFEdges;
    for (;expE.More();expE.Next())
      cmpFEdges.Add(expE.Current());
    cmpFEdges.Intersect(cuttedEdges);
    BPFaceToCutEdges.Add(F, cmpFEdges);
  }

  return true;
}

/* OLD WAY
bool HYDROData_BoundaryPolygonTools::CutTool( const TopTools_SequenceOfShape& CutTools, 
                                              NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape>& ObjToRes )
{
  TopTools_IndexedMapOfShape EdgesTools;
  for (int i =1; i<=CutTools.Length();i++)
  {
    const TopoDS_Shape& CSH = CutTools(i);
    TopExp::MapShapes(CSH, TopAbs_EDGE, EdgesTools);
  }
  //
  BRep_Builder BB;
  TopoDS_Compound aToolCmp;
  BB.MakeCompound(aToolCmp);
  for (int i=1; i<= EdgesTools.Extent();i++)
    BB.Add(aToolCmp, EdgesTools(i));

  TopoDS_Compound aObjCmp;
  BB.MakeCompound(aObjCmp);
  for (int i = 1; i <= ObjToRes.Extent(); i++ )
  {
    const TopoDS_Shape& Obj = ObjToRes.FindKey(i);
    if (!Obj.IsNull())
      BB.Add(aObjCmp, Obj);
  }

  Standard_Real aTol;

  aTol = Precision::Confusion();
  BOPAlgo_BOP aBOP;
  //
  aBOP.AddArgument(aObjCmp);
  aBOP.AddTool(aToolCmp);
  aBOP.SetOperation(BOPAlgo_FUSE);
  aBOP.SetRunParallel(true);

  aBOP.Perform();

#if OCC_VERSION_LARGE > 0x07020000
  if (aBOP.HasErrors())
    return false;
#endif

  for (int i = 1; i <= ObjToRes.Extent(); i++ )
  {
    const TopoDS_Shape& Obj = ObjToRes.FindKey(i);
    Standard_Real aTol;
    if (!Obj.IsNull()) 
    {
      TopTools_ListOfShape ls = aBOP.Modified(Obj);
      TopoDS_Shape aRes;
      if (ls.IsEmpty())
        aRes = Obj; //no fusing of toolcmp with Obj
      else if (ls.Extent() == 1)
        aRes = ls.First();
      else
      {
        BRep_Builder BB;
        TopoDS_Compound cmp;
        BB.MakeCompound(cmp);
        TopTools_ListIteratorOfListOfShape aItLS(ls);
        for (; aItLS.More(); aItLS.Next()) 
          BB.Add(cmp, aItLS.Value());
        aRes = cmp;
      }  
      //const TopoDS_Shape& aRes=aBOP.Shape();
      ObjToRes.ChangeFromKey(Obj) = aRes;
    }
  }

  return true;
}*/


bool HYDROData_BoundaryPolygonTools::IncludeTool( const TopTools_SequenceOfShape& IncludeTools, 
  TopoDS_Shape Obj)
{
  TopExp_Explorer exp(Obj, TopAbs_VERTEX);
  if (!exp.More())
    return false;
  TopoDS_Vertex aV = TopoDS::Vertex(exp.Current());
  gp_Pnt aP = BRep_Tool::Pnt(aV);
  for (int i=1; i<=IncludeTools.Size(); i++)
  {
    TopoDS_Face IncF = TopoDS::Face(IncludeTools(i));
    bool IsInter = FindInterBySection(IncF, Obj);
    if (IsInter)
      return false;
    //
    gp_XY aP2d(aP.X(), aP.Y());
    TopAbs_State aState = HYDROData_Tool::ComputePointState(aP2d, IncF);
    if (aState != TopAbs_IN)
      return false;
  }
  return true;
}

bool HYDROData_BoundaryPolygonTools::SelectionTool( const TopTools_SequenceOfShape& SelectionTool, 
                                    TopoDS_Shape Obj)
{
  for (int i=1; i<=SelectionTool.Size(); i++)
  {
    TopoDS_Face IncF = TopoDS::Face(SelectionTool(i));
    bool IsInter = FindInterBySection(IncF, Obj);
    if (!IsInter)
      return false;
  }
  return true;
}



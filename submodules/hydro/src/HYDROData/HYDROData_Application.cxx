// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_Application.h>

#include <TColStd_SequenceOfExtendedString.hxx>

#define _STUDYID_ 1

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Application,TDocStd_Application)

static HYDROData_Application* TheApplication = new HYDROData_Application;

//=======================================================================
//function : getApplication
//purpose  : 
//=======================================================================
HYDROData_Application* HYDROData_Application::GetApplication() 
{
  return TheApplication;
}

//=======================================================================
//function : GetDocument
//purpose  : 
//=======================================================================
Handle(HYDROData_Document) HYDROData_Application::GetDocument() const
{
  if (myDocuments.IsBound(_STUDYID_)) {
    return myDocuments.Find(_STUDYID_);
  }
  // document not found => create the new one
  return Handle(HYDROData_Document)();
}

//=======================================================================
//function : OCAFApp_Application
//purpose  : 
//=======================================================================
HYDROData_Application::HYDROData_Application ()
{
  // store handle to the application to avoid nullification
  static Handle(HYDROData_Application) TheKeepHandle;
  TheKeepHandle = this;
}

//=======================================================================
//function : addDocument
//purpose  : 
//=======================================================================
void HYDROData_Application::AddDocument(const Handle(HYDROData_Document)& theDocument)
{
  myDocuments.Bind(_STUDYID_, theDocument);
}

//=======================================================================
//function : removeDocument
//purpose  : 
//=======================================================================
void HYDROData_Application::RemoveDocument(const Handle(HYDROData_Document)& theDocument)
{
  DataMapOfStudyIDDocument::Iterator anIter(myDocuments);
  for(; anIter.More(); anIter.Next())
    if (anIter.Value() == theDocument) {
      myDocuments.UnBind(anIter.Key());
      break;
    }
}

//=======================================================================
//function : Formats
//purpose  : 
//=======================================================================
void HYDROData_Application::Formats(TColStd_SequenceOfExtendedString& theFormats) 
{
  theFormats.Append(TCollection_ExtendedString ("BinOcaf")); // standard binary schema
}

//=======================================================================
//function : ResourcesName
//purpose  : 
//=======================================================================
Standard_CString HYDROData_Application::ResourcesName()
{
  return Standard_CString("Standard");
}

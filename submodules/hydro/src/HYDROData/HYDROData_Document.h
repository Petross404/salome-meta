// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Document_HeaderFile
#define HYDROData_Document_HeaderFile

#include <HYDROData_Entity.h>
#include <TDocStd_Document.hxx>
#include <Standard_Transient.hxx>

class HYDROData_InterpolatorsFactory;
class HYDROData_IProfilesInterpolator;
class HYDROData_StricklerTable;
class HYDROData_LandCoverMap;

class QFile;
class gp_Pnt2d;
class gp_Pnt;
class gp_XYZ;
class gp_XY;

/**
 * Errors that could appear on document open/save actions.
 * If there is no error, it is "OK".
 */
enum Data_DocError {
  DocError_OK = 0, ///< success
  DocError_ResourcesProblem, ///< resources files are invalid or not found
  DocError_CanNotOpen, ///< can not open file for reading or writing
  DocError_InvalidVersion, ///< version of document is different than expected
  DocError_InvalidFormat, ///< format of the document is bad
  DocError_UnknownProblem ///< problem has unknown nature
};

/**\class HYDROData_Document
 *
 * \brief Document for internal data structure of any object storage. Corresponds to the SALOME study.
 *
 * Document contains all data of the Study specific to this module.
 * Also it provides acces to this data: open/save, transactions management etc.
 * to provide access to all stored data.
 */

class HYDROData_Document : public Standard_Transient
{
public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Document, Standard_Transient);

  //! Returns the existing document or creates new if it is not exist
  HYDRODATA_EXPORT static Handle(HYDROData_Document) Document();

  //! Returns the document by object
  HYDRODATA_EXPORT static Handle(HYDROData_Document) Document( 
    const TDF_Label& theObjectLabel );

public:

  //! Returns true if data model contains document for this study
  HYDRODATA_EXPORT static bool HasDocument();


public:

  //! Loads the OCAF document from the file.
  //! \param theFileName full name of the file to load
  //! \returns error status (OK in case of success)
  HYDRODATA_EXPORT static Data_DocError Load(const char* theFileName);

  //! Saves the OCAF document to the file.
  //! \param theFileName full name of the file to store
  //! \returns error status (OK in case of success)
  HYDRODATA_EXPORT Data_DocError Save(const char* theFileName);

  //! Removes document data
  HYDRODATA_EXPORT void Close();

public:

  // Returns name of document instance in python dump script
  HYDRODATA_EXPORT QString GetDocPyName() const;

  //! Dump study document to Python script representation.
  //! \param theFileName full name of the file to store
  //! \returns true if document has been successfuly dumped
  HYDRODATA_EXPORT bool DumpToPython( const QString& thePyScriptPath,
                                      const bool     theIsMultiFile ) const;

  //! Dump model data to Python script representation.
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects,
                                                     const bool           theIsMultiFile ) const;

  HYDRODATA_EXPORT void CollectQGISValues( const QString& theAttrName,
                                           QStringList& theAttrValues,
                                           QStringList& theStricklerTypes ) const;

public:

  // Methods to work with objects presentation.

  //! Returns the order of objects presentation. Objects in returned sequence
  //! are order from top to low depending on z-level parameter. Objects that
  //! have no z-level parameter are located at the end of sequence and
  //! sorted alphabetically.
  //! Only the following types of objects considered:
  //!   1. KIND_IMAGE
  //!   2. KIND_IMMERSIBLE_ZONE
  //!   3. KIND_CHANNEL
  //!   4. KIND_RIVER
  //!   5. KIND_STREAM
  //!   6. KIND_OBSTACLE
  //!   7. KIND_DIGUE
  //!   8. KIND_POLYLINEXY
  //!   9. KIND_ZONE
  //! \param theIsAll if flag is true then all objects will be included,
  //!                 otherwise only objects which have the z-level parameter
  //! \returns ordered sequence of objects
  HYDRODATA_EXPORT HYDROData_SequenceOfObjects GetObjectsLayerOrder( 
    const Standard_Boolean theIsAll = Standard_True ) const;

  //! Sets the order of objects presentation.
  HYDRODATA_EXPORT void SetObjectsLayerOrder( const HYDROData_SequenceOfObjects& theOrder );

  //! Show object at the top of other model objects. If the object
  //! already has the z-level parameter then nothing will be done.
  HYDRODATA_EXPORT void Show( const Handle(HYDROData_Entity)& theObject );

  //! Show sequence of objects at the top of other model objects.
  //! The objects from the sequence will be sorted alphabetically at first.
  HYDRODATA_EXPORT void Show( const HYDROData_SequenceOfObjects& theObjects );

  //! Removes the order of objects presentation.
  HYDRODATA_EXPORT void RemoveObjectsLayerOrder();

  HYDRODATA_EXPORT void GetLocalCS( double&, double& ) const;
  HYDRODATA_EXPORT void SetLocalCS( double, double );
  HYDRODATA_EXPORT void Transform( double& X, double& Y, bool IsToLocalCS ) const;
  HYDRODATA_EXPORT void Transform( gp_Pnt& thePnt, bool IsToLocalCS ) const;
  HYDRODATA_EXPORT void Transform( gp_XYZ& thePnt, bool IsToLocalCS ) const;
  HYDRODATA_EXPORT void Transform( gp_XY& thePnt, bool IsToLocalCS ) const;
  HYDRODATA_EXPORT void Transform( double& X, double& Y, double& Z, bool IsToLocalCS ) const;

public:

  //! Starts a new operation (opens a tansaction)
  HYDRODATA_EXPORT void StartOperation();
  //! Finishes the previously started operation (closes the transaction)
  HYDRODATA_EXPORT void CommitOperation(
    const TCollection_ExtendedString& theName = TCollection_ExtendedString());
  //! Aborts the operation 
  HYDRODATA_EXPORT void AbortOperation();
  //! Returns true if operation has been started, but not yet finished or aborted
  HYDRODATA_EXPORT bool IsOperation();
  //! Returns true if document was modified (since creation/opening)
  HYDRODATA_EXPORT bool IsModified();

public:

  //! Returns True if there are available Undos
  HYDRODATA_EXPORT bool CanUndo();
  //! Returns a list of stored undo actions
  HYDRODATA_EXPORT const TDF_DeltaList& GetUndos();
  //! Clears a list of stored undo actions
  HYDRODATA_EXPORT void ClearUndos();
  //! Undoes last operation
  HYDRODATA_EXPORT void Undo();

  //! Returns True if there are available Redos
  HYDRODATA_EXPORT bool CanRedo();
  //! Returns a list of stored undo actions
  HYDRODATA_EXPORT const TDF_DeltaList& GetRedos();
  //! Clears a list of stored undo actions
  HYDRODATA_EXPORT void ClearRedos();
  //! Redoes last operation
  HYDRODATA_EXPORT void Redo();

public:

  //! Creates and locates in the document a new object
  //! \param theKind kind of the created object, can not be UNKNOWN
  //! \returns the created object
  HYDRODATA_EXPORT Handle(HYDROData_Entity) CreateObject(const ObjectKind theKind);


  //! Find the data object with the specified name.
  HYDRODATA_EXPORT Handle(HYDROData_Entity) FindObjectByName( 
    const QString&   theName, 
    const ObjectKind theObjectKind = KIND_UNKNOWN ) const;

  //! Find the data objects with the specified names.
  HYDRODATA_EXPORT HYDROData_SequenceOfObjects FindObjectsByNames(
    const QStringList& theNames, 
    const ObjectKind   theObjectKind = KIND_UNKNOWN ) const;

  //! Collect all data objects with given object type
  HYDRODATA_EXPORT HYDROData_SequenceOfObjects CollectAllObjects(
    const ObjectKind theObjectKind = KIND_UNKNOWN ) const;

public:
  
  //! Returns interpolator factory instance
  HYDRODATA_EXPORT HYDROData_InterpolatorsFactory* GetInterpolatorsFactory();

  //! Get the appropriate interpolator by the name.
  HYDRODATA_EXPORT HYDROData_IProfilesInterpolator* GetInterpolator( const TCollection_AsciiString& theName ) const;

  //! Get list of registered interpolator names.
  HYDRODATA_EXPORT NCollection_Sequence<TCollection_AsciiString> GetInterpolatorNames() const;

public:
  //! Returns default strickler coefficient
  HYDRODATA_EXPORT double GetDefaultStricklerCoefficient() const;

  //! Sets default strickler coefficient
  HYDRODATA_EXPORT void SetDefaultStricklerCoefficient( double ) const;

  HYDRODATA_EXPORT int GetCountQuadtree() const;
  HYDRODATA_EXPORT void SetCountQuadtree( int ) const;
  HYDRODATA_EXPORT int GetCountDelaunay() const;
  HYDRODATA_EXPORT void SetCountDelaunay( int ) const;

  HYDRODATA_EXPORT QColor GetAssociatedColor( const QString& theStricklerType, const Handle(HYDROData_StricklerTable)& theTable ) const;

protected:
  friend class HYDROData_Iterator;
  friend class test_HYDROData_Document;

  //! Creates new document: private because "Document" method must be used instead of direct creation.
  HYDRODATA_EXPORT HYDROData_Document();
  //! Creates new document by existing OCAF structure
  HYDRODATA_EXPORT HYDROData_Document(const Handle(TDocStd_Document)& theDoc);
  //! Deletes all high-level data, managed this document
  HYDRODATA_EXPORT ~HYDROData_Document();

  //! Returns the new identifier of the new object (may be used for correct ordering of objects)
  HYDRODATA_EXPORT int NewID();

  //! Returns the label where the objects are located (used by Iterator)
  HYDRODATA_EXPORT TDF_Label LabelOfObjects();

  HYDRODATA_EXPORT TDF_Label LabelOfLocalCS() const;

private:
  
  // Dump header Python part in to file \c theFile
  bool DumpToPython( QFile&               theFile,
                     MapOfTreatedObjects& theTreatedObjects ) const;

  // Dump objects of type \c theObjectKind to file \c theFile
  bool dumpPartitionToPython( QFile&               theFile,
                              const QString&       thePyScriptPath,
                              const bool           theIsMultiFile,
                              MapOfTreatedObjects& theDumpedObjects,
                              const ObjectKind&    theObjectKind ) const;
  void UpdateLCSFields() const;

private:
  Handle(TDocStd_Document) myDoc; ///< OCAF document instance corresponding for keeping all persistent data
  int myTransactionsAfterSave; ///< number of transactions after the last "save" call, used for "IsModified" method
  double myLX, myLY;

  HYDROData_InterpolatorsFactory* myInterpolatorsFactory; ///< iterpolators factory
};

#endif

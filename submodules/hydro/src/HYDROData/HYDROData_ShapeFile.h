// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDRODATA_SHAPEFILE_H
#define HYDRODATA_SHAPEFILE_H

#include "HYDROData.h"

#include <NCollection_Sequence.hxx>

#include <TopTools_SequenceOfShape.hxx>
#include <NCollection_IndexedDataMap.hxx>
#include <QStringList>

#include <vector>

//extern "C" {
#include <shapefil.h> 
//};

class gp_XYZ;
class HYDROData_PolylineXY;
class HYDROData_Polyline3D;
class HYDROData_Document;
class TopoDS_Face;
class TopoDS_Shape;
class HYDROData_Entity;
class HYDROData_LandCoverMap;

class HYDROData_ShapeFile 
{

public:

enum ShapeType
{
  ShapeType_Polyline,
  ShapeType_Polygon
};

enum DBF_FieldType
{
  DBF_FieldType_None,
  DBF_FieldType_String,
  DBF_FieldType_Integer, 
  DBF_FieldType_Double,
  DBF_FieldType_Invalid
};

enum ImportShapeType
{
  ImportShapeType_Polyline,
  ImportShapeType_Polyline3D,
  ImportShapeType_Polygon,
  ImportShapeType_All
};

struct DBF_AttrValue
{
  DBF_AttrValue() 
  {
    myIntVal = -1;
    myDoubleVal = -1.0;
    myIsNull = true;
    myFieldType = DBF_FieldType_None;
  }
  QString myStrVal;
  int myIntVal;
  double myDoubleVal;
  std::string myRawValue;
  DBF_FieldType myFieldType;
  bool myIsNull;
};

public:
  HYDRODATA_EXPORT HYDROData_ShapeFile(  );
  virtual HYDRODATA_EXPORT ~HYDROData_ShapeFile();

  //Export operation
  HYDRODATA_EXPORT void Export(Handle(HYDROData_Document) theDocument,
                               const QString& aFileName,
                               NCollection_Sequence<Handle(HYDROData_PolylineXY)> aPolyXYSeq,
                               NCollection_Sequence<Handle(HYDROData_Polyline3D)> aPoly3DSeq,
                               QStringList& aNonExpList);

  HYDRODATA_EXPORT void Export(Handle(HYDROData_Document) theDocument,
                               const QString& aFileName,
                               const Handle(HYDROData_LandCoverMap)& aLCM,
                               QStringList& aNonExpList,
                               bool bCheckLinear = true,
                               bool bUseDiscr = false, 
                               double theDefl = 0.1);

  int WriteObjectPolyXY(Handle(HYDROData_Document) theDocument, SHPHandle theShpHandle,
                        Handle(HYDROData_PolylineXY) thePoly );

  int WriteObjectPoly3D(Handle(HYDROData_Document) theDocument, SHPHandle theShpHandle,
                        Handle(HYDROData_Polyline3D) thePoly );

  int WriteObjectPolygon(Handle(HYDROData_Document) theDocument, SHPHandle theShpHandle,
                         const TopoDS_Shape& theInputShape, bool bUseDiscr, double theDefl );
  //Import
  bool Parse(SHPHandle theHandle, ShapeType theType);
  //Import Landcover
  void ReadSHPPolygon(Handle(HYDROData_Document) theDocument, SHPObject* anObj, int i, TopoDS_Face& F);

  HYDRODATA_EXPORT int ImportPolygons(Handle(HYDROData_Document) theDocument,
                                      const QString theFileName,
                                      QStringList& thePolygonsList, 
                                      TopTools_SequenceOfShape& theFaces);

  HYDRODATA_EXPORT void Free();

  //Import Polyline
  void ReadSHPPolyXY(Handle(HYDROData_Document) theDocument, SHPObject* anObj, QString theFileName, 
                     int theInd, NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities,
                     bool bReadAsPolylin = false);

  void ReadSHPPoly3D(Handle(HYDROData_Document) theDocument, SHPObject* anObj,
    const Handle(HYDROData_PolylineXY)& aPolylineXY, 
    NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities);

  HYDRODATA_EXPORT int ImportPolylines(Handle(HYDROData_Document) theDocument, const QString& theFileName, 
                                       NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*>& theEntitiesToSHPObj,
                                       ImportShapeType theShapeTypesToImport);

  HYDRODATA_EXPORT int ImportPolylines3D(Handle(HYDROData_Document) theDocument, 
                                         NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*> thePolyXYToObj, 
                                         NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities,
                                         ImportShapeType theShapeTypesToImport);

  HYDRODATA_EXPORT int OpenAndParse(const QString& theFileName);


  HYDRODATA_EXPORT QString GetShapeTypeName(int theType);

  HYDRODATA_EXPORT int GetShapeType() const;


  //DBF I/O Methods
  HYDRODATA_EXPORT bool CheckDBFFileExisting(const QString& theSHPFilePath, QString& thePathToDBFFile);
  HYDRODATA_EXPORT bool DBF_OpenDBF(const QString& thePathToDBFFile);
  HYDRODATA_EXPORT int DBF_GetNbFields();
  HYDRODATA_EXPORT QStringList DBF_GetFieldList();
  HYDRODATA_EXPORT void DBF_GetFieldTypeList(std::vector<DBF_FieldType>& FTVect);
  HYDRODATA_EXPORT int DBF_GetNbRecords();
  HYDRODATA_EXPORT void DBF_CloseDBF();
  HYDRODATA_EXPORT void DBF_GetAttributeList(int theIndexOfField, std::vector<DBF_AttrValue>& theAttrV);
  HYDRODATA_EXPORT bool DBF_WriteFieldAndValues(const QString& theFileName, const QString& theFieldName, DBF_FieldType theType, const std::vector<DBF_AttrValue>& theAttrV, bool bUseStrValue);

private:
  
  void ProcessFace(Handle(HYDROData_Document) theDocument,
                   const TopoDS_Face& theFace, SHPHandle theShpHandle,
                   bool bUseDiscr, double theDefl);

  bool CheckLinear(const TopoDS_Shape& theInpShape);

  int TryOpenShapeFile(QString theFileName);

  void GetFreeIndices(std::vector<int>& theAllowedIndexes, QString strName, size_t theObjsSize,
                      QStringList theExistingNames, QString theBaseFileName);

private:
  std::vector<SHPObject*> mySHPObjects;
  SHPHandle     myHSHP;
  DBFHandle	myHDBF;

  friend class test_HYDROData_ShapeFile;
};

#endif

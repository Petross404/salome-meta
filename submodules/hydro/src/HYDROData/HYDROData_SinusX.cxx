// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_SinusX.h"

#include <HYDROData_PolylineXY.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_Entity.h>
#include <HYDROData_Document.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Iterator.h>

#include <gp_XYZ.hxx>
#include <gp_XY.hxx>

#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QColor>
#include <QMap>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROData_SinusX::HYDROData_SinusX( ) 
{
}

HYDROData_SinusX::~HYDROData_SinusX()
{
}

void HYDROData_SinusX::CollectExistingNames(Handle(HYDROData_Document) theDocument)
{
  HYDROData_Iterator anIter( theDocument );
  int anInd = 0;
  myExistingNames.clear();
  std::vector<int> anAllowedIndexes;
  for( ; anIter.More(); anIter.Next() )
    myExistingNames.push_back(anIter.Current()->GetName());
}

QString HYDROData_SinusX::GetName(const QString& theName)
{
  if (myExistingNames.contains(theName))
  { 
    QString aName = theName; 
    while (myExistingNames.contains(aName)) 
    {
      QStringList aList = aName.split("_");
      int aLastInd = aName.lastIndexOf("_");
      bool IsNum = false; 
      int anInd = -1;
      anInd = aList.last().toInt(&IsNum);
      if (IsNum)
      {
        aName = aName.left(aLastInd) + "_";
        aName+= QString::number(++anInd);
      }
      else
        aName = theName + "_0";
    }

    myExistingNames.push_back(aName);
    return aName;
  }
  else
  {
    myExistingNames.push_back(theName);
    return theName;
  }

}


bool HYDROData_SinusX::OpenAndParse(const QString& theFilePath)
{
  if ( theFilePath.isEmpty() )
  { 
    return false;
  }

  QString anExt = theFilePath.split('.', QString::SkipEmptyParts).back();

  if (anExt == "sx")
  {
    QFile aFile (theFilePath);
    
    aFile.open(QIODevice::ReadOnly);

    Parse(aFile);
    aFile.close();
    
    return true;
  }
  else
    return false;

}

void HYDROData_SinusX::Import(Handle(HYDROData_Document) theDocument,
  NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities, std::vector<ImportOptions>* importOptions)
{
  CollectExistingNames(theDocument);
  SXToHydro(theDocument, theEntities, importOptions);
}


void HYDROData_SinusX::SXToHydro(Handle(HYDROData_Document) theDocument, NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities,
    std::vector<ImportOptions>* importOptions )
{ 
  if (importOptions && (myCurveBlocks.size() != importOptions->size()))
    importOptions = NULL; //incorrect input

  QMap<QString,Handle(HYDROData_PolylineXY) > polyMap;

  for ( size_t i = 0; i < myCurveBlocks.size(); i++ )
  {
    if (myCurveBlocks[i].myType == 4) ///  scatter plot -> to bathy
    {
      if (!importOptions || (importOptions && (*importOptions)[i].ImportAsBathy == true))
      {
        Handle(HYDROData_Bathymetry) aBath = Handle(HYDROData_Bathymetry)::DownCast( theDocument->CreateObject( KIND_BATHYMETRY ) );
        HYDROData_Bathymetry::AltitudePoints aAPoints;
        size_t n = myCurveBlocks[i].myXYZPoints.size();
        aAPoints.reserve( n );
        for (size_t j = 0; j < n; j++)
        {
          gp_XYZ aLocalPoint = gp_XYZ (myCurveBlocks[i].myXYZPoints[j]);
          theDocument->Transform(aLocalPoint, true);
          HYDROData_Bathymetry::AltitudePoint p;
          p.X = aLocalPoint.X();
          p.Y = aLocalPoint.Y();
          p.Z = aLocalPoint.Z();
          aAPoints.push_back(p);
        }

        aBath->SetAltitudePoints(aAPoints);
        aBath->SetName(GetName(myCurveBlocks[i].myName));
        theEntities.Append(aBath);
      }
    }
    if (myCurveBlocks[i].myType == 1 || myCurveBlocks[i].myType == 3) // XYZ curve or isocontour
    {
      if (!importOptions || (importOptions && (*importOptions)[i].ImportAsPolyXY == true))
      {
        NCollection_Sequence<gp_XYZ> aPoints;
        for (size_t j = 0; j < myCurveBlocks[i].myXYZPoints.size(); j++)
        {
          gp_XYZ aLocalPoint = gp_XYZ (myCurveBlocks[i].myXYZPoints[j]);
          theDocument->Transform(aLocalPoint, true);
          aPoints.Append(aLocalPoint);
        }

        QString polyName = myCurveBlocks[i].myName;
        Handle(HYDROData_PolylineXY) aPolyXY;
        if (polyMap.contains(polyName))
        	aPolyXY = polyMap[polyName];
        else
        {
        	aPolyXY = Handle(HYDROData_PolylineXY)::DownCast( theDocument->CreateObject( KIND_POLYLINEXY ) );
        	polyMap[polyName] = aPolyXY;
            aPolyXY->SetName(GetName(myCurveBlocks[i].myName));
            aPolyXY->SetWireColor(HYDROData_PolylineXY::DefaultWireColor());
            theEntities.Append(aPolyXY);
        }
        aPolyXY->AddSection( "",  
          myCurveBlocks[i].myIsSpline ? HYDROData_PolylineXY::SECTION_SPLINE : HYDROData_PolylineXY::SECTION_POLYLINE, 
          myCurveBlocks[i].myIsClosed ? true : false);
        int numSec = aPolyXY->NbSections() -1;

        for ( int k = 1; k <= aPoints.Size(); k++ ) {
          const HYDROData_Profile::ProfilePoint& aBottomPoint = aPoints.Value( k );
          aPolyXY->AddPoint( numSec, HYDROData_PolylineXY::Point( aBottomPoint.X(), aBottomPoint.Y() ) );
        }

        if (!importOptions || (importOptions && (*importOptions)[i].ImportAsProfile == true))
        {
          Handle(HYDROData_ProfileUZ) aProfileUZ = Handle(HYDROData_ProfileUZ)::DownCast( theDocument->CreateObject( KIND_PROFILEUZ ) );
          aProfileUZ->CalculateAndAddPoints(aPoints, aPolyXY, false);
          Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( theDocument->CreateObject( KIND_PROFILE ) );
          aProfile->SetParametricPoints(aProfileUZ->GetPoints());

          aProfileUZ->SetName(GetName(myCurveBlocks[i].myName));
          aProfile->SetName(GetName(myCurveBlocks[i].myName));

          theEntities.Append(aProfileUZ);
          theEntities.Append(aProfile);
        }
      }      

    }
    if (myCurveBlocks[i].myType == 2) // XYZ profile
    {
      if (!importOptions || (importOptions && (*importOptions)[i].ImportAsProfile == true))
      {
        if (myCurveBlocks[i].myCurvePlane == 2) // plane XoZ
        {
          if (myCurveBlocks[i].myAdditionalCurveInfo.size() == 4)
          {
            Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( theDocument->CreateObject( KIND_PROFILE ) );
            HYDROData_ProfileUZ::PointsList aPointList;
            for (size_t j = 0; j < myCurveBlocks[i].myXYZPoints.size(); j++)
              aPointList.Append(gp_XY (myCurveBlocks[i].myXYZPoints[j].X(), myCurveBlocks[i].myXYZPoints[j].Z()));
            aProfile->GetProfileUZ()->SetSectionType(0,  myCurveBlocks[i].myIsSpline ? HYDROData_PolylineXY::SECTION_SPLINE : HYDROData_PolylineXY::SECTION_POLYLINE);
            aProfile->GetProfileUZ()->SetSectionClosed(0, myCurveBlocks[i].myIsClosed ? true : false);
            aProfile->SetParametricPoints(aPointList);
            if ( ! (myCurveBlocks[i].myAdditionalCurveInfo[0] == 0 &&  myCurveBlocks[i].myAdditionalCurveInfo[1] == 0 && 
              myCurveBlocks[i].myAdditionalCurveInfo[2] == 0 && myCurveBlocks[i].myAdditionalCurveInfo[3] == 0) )
            { // georeferenced profile
              double xl = myCurveBlocks[i].myAdditionalCurveInfo[0];
              double yl = myCurveBlocks[i].myAdditionalCurveInfo[1];
              double xr = myCurveBlocks[i].myAdditionalCurveInfo[2];
              double yr = myCurveBlocks[i].myAdditionalCurveInfo[3];
              theDocument->Transform(xl, yl , true);
              theDocument->Transform(xr, yr , true);
              aProfile->SetLeftPoint(gp_XY(xl, yl));
              aProfile->SetRightPoint(gp_XY(xr, yr));
              aProfile->Update();
            }
            aProfile->SetName(GetName(myCurveBlocks[i].myName));
            theEntities.Append(aProfile);
          }
        }
        if (myCurveBlocks[i].myCurvePlane == 0) // plane XoY
        {
          Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( theDocument->CreateObject( KIND_PROFILE ) );
          HYDROData_Profile::ProfilePoints aPointList;
          for (size_t j = 0; j < myCurveBlocks[i].myXYZPoints.size(); j++)
          {
            gp_XYZ aLocalPoint = gp_XYZ (myCurveBlocks[i].myXYZPoints[j]);
            theDocument->Transform(aLocalPoint, true);
            aPointList.Append(aLocalPoint);
          }
          aProfile->GetProfileUZ()->SetSectionType(0,  myCurveBlocks[i].myIsSpline ? HYDROData_PolylineXY::SECTION_SPLINE : HYDROData_PolylineXY::SECTION_POLYLINE);
          aProfile->GetProfileUZ()->SetSectionClosed(0, myCurveBlocks[i].myIsClosed ? true : false);
          aProfile->SetProfilePoints(aPointList, false);
          aProfile->SetName(GetName(myCurveBlocks[i].myName));
          theEntities.Append(aProfile);
        }
      }
    }
  }

}
 

bool HYDROData_SinusX::Parse(QFile& theFile)
{
  if ( !theFile.isOpen() )
    return false;

  QString aLine;
  QString aBLine;
  QStringList aList;
  QStringList aBList;
  myCurveBlocks.clear();
  bool aTotStat = true;

  aLine = theFile.readLine().simplified();
  aList = aLine.split( ' ', QString::SkipEmptyParts );

  for (;!theFile.atEnd();) 
  {
    if (aList[0] == "B" && (aList[1] == "C" || aList[1] == "P" || aList[1] == "N" || aList[1] == "S" ))
    {  
      HYDROGUI_CurveBlock aCurveBlockInfo;
      if (aList[1] == "C")
        aCurveBlockInfo.myType = 1; // XYZ curve
      else if (aList[1] == "P")
        aCurveBlockInfo.myType = 2; // XYZ profile
      else if (aList[1] == "N")
        aCurveBlockInfo.myType = 3; // isocontour
      else if (aList[1] == "S")
        aCurveBlockInfo.myType = 4; // Scatter plot

      if (aList.size() == 9)
      {
        for (int j = 2; j < 8; j++)
          aCurveBlockInfo.myRefCoords.push_back(aList[j].toDouble());
        aCurveBlockInfo.myRefRatio = aList[8].toDouble();
      }

      QString Name;
      do
      {
        aBLine = theFile.readLine().simplified();
        aBList = aBLine.split( ' ', QString::SkipEmptyParts );
         
        if (aBList[0] == "CP")
        {
          if (aBList.size() == 2 && (aBList[1] == "0" || aBList[1] == "1" || aBList[1] == "2"))
            aCurveBlockInfo.myCurvePlane = aBList[1].toInt();
          else if (aBList.size() == 3 && (aBList[1] == "0" || aBList[1] == "1") && (aBList[2] == "0" || aBList[2] == "1"))
          {
            aCurveBlockInfo.myIsClosed = aBList[1].toInt();
            aCurveBlockInfo.myIsSpline = aBList[2].toInt();
          }
          else
          {
            for (int j = 1; j < aBList.size(); j++)
              aCurveBlockInfo.myAdditionalCurveInfo.push_back(aBList[j].toDouble());
          }
        }
        if (aBList[0] == "CN")
        {
           for (int i = 1; i < aBList.size(); i++)
             Name += aBList[i] + "_";
           if (Name.size() <= 1)
             Name = "noname_";
           Name.remove(Name.size() - 1, 1);
           aCurveBlockInfo.myName = Name;
        }
      } while (!theFile.atEnd() && aBLine[0] == 'C' );

      bool aStat;
      aTotStat = true;
      do
      {
        if (aBList.size() >= 3 && aBLine[0] != 'B' && aBLine[0] != 'C') {
          gp_XYZ anXYZ;
          anXYZ.SetX (aBList[0].toDouble(&aStat));  
          aTotStat = aTotStat && aStat;
          anXYZ.SetY (aBList[1].toDouble(&aStat));
          aTotStat = aTotStat && aStat;
          anXYZ.SetZ (aBList[2].toDouble(&aStat));
          aTotStat = aTotStat && aStat;

          aCurveBlockInfo.myXYZPoints.push_back(anXYZ);
          
          aBLine = theFile.readLine().simplified();
          aBList = aBLine.split( ' ', QString::SkipEmptyParts );
        }
        else 
          break;
    
      } while (!theFile.atEnd() || !aBLine.isEmpty());
      if (aTotStat)
        myCurveBlocks.push_back(aCurveBlockInfo);

      aLine = aBLine;
      aList = aBList;

    }
    else
    {
      aLine = theFile.readLine().simplified();
      aList = aLine.split( ' ', QString::SkipEmptyParts );
    }

  }

  return true;

}

bool HYDROData_SinusX::Export(const QString& theFilePath, const NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities)
{
  if ( theFilePath.isEmpty() )
  { 
    return false;
  }

  QString anExt = theFilePath.split('.', QString::SkipEmptyParts).back();

  if (anExt == "sx")
  {
    QFile aFile (theFilePath);
    
    aFile.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text);

    HydroToSX(aFile, theEntities);
    
    aFile.close();
    
    return true;
  }
  else
    return false;

}

void HYDROData_SinusX::HydroToSX(QFile& theFile, const NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities)
{
  QTextStream aTextStream(&theFile);
  aTextStream << "C  Generated by HYDRO Module\n";
  aTextStream << "C\n";

  for (int i = 1; i <= theEntities.Size(); i++)
  {
    Handle(HYDROData_Entity) anEnt = theEntities.Value(i);
    if (anEnt->IsKind( STANDARD_TYPE(HYDROData_Bathymetry) ))
    {
      Handle(HYDROData_Bathymetry) aBathy = Handle(HYDROData_Bathymetry)::DownCast( anEnt );
      HYDROData_Bathymetry::AltitudePoints anXYZPoints = aBathy->GetAltitudePoints(true);
      //Write to stream
      aTextStream << "B S\n";
      aTextStream << "CN " << aBathy->GetName() << "\n";
      aTextStream << "CP 0 0\n";
      aTextStream << "CP 0\n";
      for (size_t j = 0, m = anXYZPoints.size(); j < m; j++)
        aTextStream << " " << QString::number(anXYZPoints[j].X, 'f', 3)  
                    << " " << QString::number(anXYZPoints[j].Y, 'f', 3)  
                    << " " << QString::number(anXYZPoints[j].Z, 'f', 3) << "\n"; 
    }
    else if (anEnt->IsKind( STANDARD_TYPE(HYDROData_PolylineXY) ))
    {
      Handle(HYDROData_PolylineXY) aPolyXY = Handle(HYDROData_PolylineXY)::DownCast( anEnt );
      for (int j = 0; j < aPolyXY->NbSections(); j++)
      { 
        //Collect data
        bool IsClosed = aPolyXY->IsClosedSection(j);
        bool IsSpline = false;
        if (aPolyXY->GetSectionType(j) == HYDROData_PolylineXY::SECTION_SPLINE)
          IsSpline = true;
        HYDROData_PolylineXY::PointsList anXYPoints = aPolyXY->GetPoints(j, true);
        //Write to stream
        aTextStream << "B N\n";
        aTextStream << "CN " << aPolyXY->GetName() << "\n";
        aTextStream << "CP " << IsClosed << " " << IsSpline << "\n";
        aTextStream << "CP 0.0\n";
        aTextStream << "CP 0\n";
        if (aPolyXY->NbSections() > 1)
          aTextStream << "C " << aPolyXY->GetName() << "_section_" << QString::number(j) << "\n";
        for (int k = anXYPoints.Lower(); k <= anXYPoints.Upper(); k++)
         aTextStream << " " << QString::number(anXYPoints(k).X(), 'f', 3)  
                     << " " << QString::number(anXYPoints(k).Y(), 'f', 3)  
                     << " 0.000\n"; 
      }
    }
    else if (anEnt->IsKind( STANDARD_TYPE(HYDROData_Profile) ))
    {
      //Collect data
      Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( anEnt );
      HYDROData_ProfileUZ::PointsList aPointList = aProfile->GetParametricPoints();
      bool IsClosed = aProfile->GetProfileUZ(false)->IsClosedSection(0);;
      bool IsSpline = false;
      if (aProfile->GetProfileUZ(false)->GetSectionType(0) == HYDROData_PolylineXY::SECTION_SPLINE)
        IsSpline = true;
      //Write to stream
      aTextStream << "B P\n";
      aTextStream << "CN " << aProfile->GetName() << "\n";
      aTextStream << "CP " << IsClosed << " " << IsSpline << "\n";
      gp_XY aLeftPoint(0.0, 0.0);
      gp_XY aRightPoint(0.0, 0.0);
      if (aProfile->GetLeftPoint(aLeftPoint, true) && aProfile->GetRightPoint(aRightPoint, true))
        aTextStream << "CP " << QString::number(aLeftPoint.X(), 'f', 3) << " " <<
                                QString::number(aLeftPoint.Y(), 'f', 3) << " " <<
                                QString::number(aRightPoint.X(), 'f', 3) << " " <<
                                QString::number(aRightPoint.Y(), 'f', 3) << "\n";
      else
        aTextStream << "CP 0.0 0.0 0.0 0.0\n";
      aTextStream << "CP 2\n";
      for (int k = aPointList.Lower(); k <= aPointList.Upper(); k++)
         aTextStream << " " << QString::number(aPointList(k).X(), 'f', 3)  
                     << " 0.000 "
                     << QString::number(aPointList(k).Y(), 'f', 3) << "\n";  
    }
  }
}

std::vector<HYDROGUI_CurveBlock> HYDROData_SinusX::GetCurveBlocks() const
{
  return myCurveBlocks;
}



// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_BCPolygon.h"

//#include "HYDROData_IAltitudeObject.h"
#include "HYDROData_Document.h"
//#include "HYDROData_ShapesGroup.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_ShapesTool.h"

#include <TopoDS.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_HSequenceOfShape.hxx>
#include <TDataStd_Integer.hxx>

#include <HYDROData_Tool.h>

#include <QColor>
#include <QStringList>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_BCPolygon,HYDROData_Object)


HYDROData_BCPolygon::HYDROData_BCPolygon()
: HYDROData_Object( Geom_2d )
{
}

HYDROData_BCPolygon::~HYDROData_BCPolygon()
{
}

QStringList HYDROData_BCPolygon::DumpToPython( const QString& thePyScriptPath,
                                               MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  
  QString aBCName = GetObjPyName();

  Handle(HYDROData_PolylineXY) aRefPolyline = GetPolyline();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefPolyline, "SetPolyline" );

  int nType = GetBoundaryType();
  aResList << QString( "%1.SetBoundaryType(%2)" ).arg(aBCName).arg(nType);

  aResList << QString( "" );

  aResList << QString( "%1.Update()" ).arg( aBCName );
  aResList << QString( "" );

  return aResList;
}

HYDROData_SequenceOfObjects HYDROData_BCPolygon::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Object::GetAllReferenceObjects();

  Handle(HYDROData_PolylineXY) aRefPolyline = GetPolyline();
  if ( !aRefPolyline.IsNull() )
    aResSeq.Append( aRefPolyline );

  return aResSeq;
}

int HYDROData_BCPolygon::GetBoundaryType() const
{
  Handle(TDataStd_Integer) aBoundaryTypeAttr;

  int aBoundaryTypeVal = 0; //default
  if( myLab.FindAttribute(TDataStd_Integer::GetID(), aBoundaryTypeAttr ) )
    aBoundaryTypeVal = aBoundaryTypeAttr->Get();
  return aBoundaryTypeVal;
}

void HYDROData_BCPolygon::SetBoundaryType( int theBoundaryType ) const
{
  TDataStd_Integer::Set( myLab, theBoundaryType );
}

void HYDROData_BCPolygon::Update()
{
  HYDROData_Object::Update();
  SetTopShape( generateTopShape() );
}

void HYDROData_BCPolygon::Update(const TopoDS_Shape& theTopShape)
{
  HYDROData_Object::Update();
  SetTopShape( theTopShape);
}

bool HYDROData_BCPolygon::IsHas2dPrs() const
{
  return true;
}

TopoDS_Shape HYDROData_BCPolygon::generateTopShape() const
{
  return generateTopShape( GetPolyline() );
}

TopoDS_Shape HYDROData_BCPolygon::generateTopShape( const Handle(HYDROData_PolylineXY)& aPolyline )
{
  return HYDROData_Tool::PolyXY2Face(aPolyline);
}

TopoDS_Shape HYDROData_BCPolygon::GetShape3D() const
{
  return GetTopShape();
}

QColor HYDROData_BCPolygon::DefaultFillingColor() const
{
  int aBT = GetBoundaryType();
  QColor aFC;
  if (aBT == 1)
    aFC = QColor( Qt::blue );
  else if (aBT == 2)
    aFC = QColor( Qt::darkGreen );
  else if (aBT == 3)
    aFC = QColor( Qt::darkYellow );
  else 
    aFC = QColor( Qt::red );
  //aFC.setAlpha(175);
  return aFC;
}

QColor HYDROData_BCPolygon::DefaultBorderColor() const
{
  return QColor( Qt::black );
}

void HYDROData_BCPolygon::SetPolyline( const Handle(HYDROData_PolylineXY)& thePolyline )
{
  if( IsEqual( GetPolyline(), thePolyline ) )
    return;

  SetReferenceObject( thePolyline, DataTag_Polyline );
  Changed( Geom_2d );
}

Handle(HYDROData_PolylineXY) HYDROData_BCPolygon::GetPolyline() const
{
  return Handle(HYDROData_PolylineXY)::DownCast( 
           GetReferenceObject( DataTag_Polyline ) );
}

void HYDROData_BCPolygon::RemovePolyline()
{
  ClearReferenceObjects( DataTag_Polyline );
  Changed( Geom_2d );
}

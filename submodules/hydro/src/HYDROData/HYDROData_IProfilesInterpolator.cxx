// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_IProfilesInterpolator.h"
#include <cstdlib>
#include <gp_XYZ.hxx>

const int DEFAULT_RESULT_PROFILES_NB = 1; ///< default number of results profiles

HYDROData_IProfilesInterpolator::HYDROData_IProfilesInterpolator()
  : myResultProfilesNumber( DEFAULT_RESULT_PROFILES_NB ),
    myErrorCode( OK )
{
}

HYDROData_IProfilesInterpolator::~HYDROData_IProfilesInterpolator()
{
}

void HYDROData_IProfilesInterpolator::SetProfiles( const std::vector<double> theProfile1, 
                                                   const std::vector<double> theProfile2 )
{
  myProfile1 = theProfile1;
  myProfile2 = theProfile2;
}

void HYDROData_IProfilesInterpolator::SetProfiles( const NCollection_Sequence<gp_XYZ>& theProfile1, 
                                                   const NCollection_Sequence<gp_XYZ>& theProfile2 )
{
  // The first profile
  for ( int i = theProfile1.Lower(); i <= theProfile1.Upper(); i++ ) {
    const gp_XYZ& aPnt = theProfile1.Value( i );
    myProfile1.push_back( aPnt.X() );
    myProfile1.push_back( aPnt.Y() );
    myProfile1.push_back( aPnt.Z() );
  }

  // The second profile
  for ( int i = theProfile2.Lower(); i <= theProfile2.Upper(); i++ ) {
    const gp_XYZ& aPnt = theProfile2.Value( i );
    myProfile2.push_back( aPnt.X() );
    myProfile2.push_back( aPnt.Y() );
    myProfile2.push_back( aPnt.Z() );
  }
}

void HYDROData_IProfilesInterpolator::SetParameter( const TCollection_AsciiString& theName, 
                                                    const TCollection_AsciiString& theValue )
{
  myParameters[theName.ToCString()] = theValue.ToCString();
}

InterpolationError HYDROData_IProfilesInterpolator::GetErrorCode() const
{
  return myErrorCode;
}

void HYDROData_IProfilesInterpolator::SetErrorCode( const InterpolationError& theError )
{
  myErrorCode = theError;
}

TCollection_AsciiString HYDROData_IProfilesInterpolator::GetErrorMessage() const
{
  return TCollection_AsciiString(myErrorMessage.c_str());
}

void HYDROData_IProfilesInterpolator::SetErrorMessage( const std::string& theMessage )
{
  myErrorMessage = theMessage;
}

void HYDROData_IProfilesInterpolator::SetResultProfilesNumber( const int theNumber )
{
  myResultProfilesNumber = theNumber;
}

int HYDROData_IProfilesInterpolator::GetCalculatedProfilesNumber() const
{
  return myResultProfiles.size();
}

std::vector<double> HYDROData_IProfilesInterpolator::GetResultProfileCoords( const int theProfileIndex ) const
{
  std::vector<double> aResultProfile;

  if ( theProfileIndex >= 0 && theProfileIndex < (int) myResultProfiles.size() ) {
    aResultProfile = myResultProfiles.at( theProfileIndex );
  }

  return aResultProfile;
}

NCollection_Sequence<gp_XYZ> HYDROData_IProfilesInterpolator::GetResultProfilePoints( const int theProfileIndex ) const
{
  return GetPoints( GetResultProfileCoords( theProfileIndex ) );
}

void HYDROData_IProfilesInterpolator::Reset()
{
  // Reset input parameters
  myProfile1.clear();  
  myProfile2.clear();  
  myParameters.clear();

  SetResultProfilesNumber( DEFAULT_RESULT_PROFILES_NB );
  
  // Reset result data
  ClearResults();
}

std::vector<double> HYDROData_IProfilesInterpolator::GetFirstProfileCoords() const
{
  return myProfile1;
}

std::vector<double> HYDROData_IProfilesInterpolator::GetSecondProfileCoords() const
{
  return myProfile2;
}

void HYDROData_IProfilesInterpolator::ClearResults()
{
  // Clear result data
  myResultProfiles.clear();

  // Reset errors
  SetErrorCode( OK );
  SetErrorMessage( "" );
}

int HYDROData_IProfilesInterpolator::GetNbProfilesToCompute() const
{
  return myResultProfilesNumber;
}

void HYDROData_IProfilesInterpolator::InsertResultProfile( const std::vector<double>& theProfile )
{
  myResultProfiles.push_back( theProfile );
}

void HYDROData_IProfilesInterpolator::InsertResultProfile( const NCollection_Sequence<gp_XYZ>& theProfile )
{
  std::vector<double> aCoords;

  for ( int i = theProfile.Lower(); i <= theProfile.Upper(); i++ ) {
    const gp_XYZ& aPnt = theProfile.Value( i );
    aCoords.push_back( aPnt.X() );
    aCoords.push_back( aPnt.Y() );
    aCoords.push_back( aPnt.Z() );
  }

  myResultProfiles.push_back( aCoords );
}

NCollection_Sequence<gp_XYZ> HYDROData_IProfilesInterpolator::GetFirstProfilePoints() const
{
  return GetPoints( GetFirstProfileCoords() );
}

NCollection_Sequence<gp_XYZ> HYDROData_IProfilesInterpolator::GetSecondProfilePoints() const
{
  return GetPoints( GetSecondProfileCoords() );
}

NCollection_Sequence<gp_XYZ> HYDROData_IProfilesInterpolator::GetPoints( const std::vector<double>& theCoords ) const
{
  NCollection_Sequence<gp_XYZ> aProfilePoints;

  int aNbCoords = (int) theCoords.size();
  div_t aDiv = std::div( aNbCoords, 3 );
  if ( aDiv.rem == 0 ) {
    for ( int i = 0; i < aNbCoords; i += 3 ) {
      aProfilePoints.Append( gp_XYZ( theCoords[i], theCoords[i+1], theCoords[i+2] ) );
    }
  }

  return aProfilePoints;
}

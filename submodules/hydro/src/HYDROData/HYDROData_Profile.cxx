// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Profile.h"

#include "HYDROData_Document.h"
#include "HYDROData_Iterator.h"
#include "HYDROData_Tool.h"
#include "HYDROData_PolylineXY.h"

#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>

#include <BRepExtrema_ExtCC.hxx>

#include <BRep_Tool.hxx>

#include <gp_Lin.hxx>
#include <gp_XY.hxx>
#include <gp_XYZ.hxx>
#include <gp_Pnt2d.hxx>
#include <gp_Ax3.hxx>
#include <GeomAPI_ProjectPointOnSurf.hxx>
#include <Geom_Plane.hxx>

#include <TDataStd_AsciiString.hxx>
#include <TDataStd_RealArray.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Iterator.hxx>

#include <OSD_File.hxx>
#include <OSD_Protection.hxx>

#include <QColor>
#include <QStringList>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Profile, HYDROData_Object)

HYDROData_Profile::HYDROData_Profile()
: HYDROData_Object( Geom_3d )
{
}

HYDROData_Profile::~HYDROData_Profile()
{
}

QStringList HYDROData_Profile::DumpToPython( const QString&       thePyScriptPath,
                                             MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aProfileName = GetObjPyName();

  //TCollection_AsciiString aFilePath = GetFilePath();
  //if ( !aFilePath.IsEmpty() )
  //{
  //  aResList << QString( "%1.ImportFromFile( \"%2\" )" )
  //            .arg( aName ).arg( aFilePath.ToCString() );
  //}

  bool anIsValidProfile = IsValid();

  QStringList aPntsDefinition;
  QString aPntsListName = HYDROData_Tool::GenerateNameForPython( theTreatedObjects, "profile_points" );

  QString aGap = QString().fill( ' ', aPntsListName.length() + 5 );
  if ( anIsValidProfile )
  {
    HYDROData_Profile::ProfilePoints aPointsList = GetProfilePoints( true );
    for ( int k = 1, aNbPoints = aPointsList.Size(); k <= aNbPoints; ++k )
    {
      const ProfilePoint& aPoint = aPointsList.Value( k );
      aPntsDefinition << QString( aGap + "gp_XYZ( %1, %2, %3 )%4" )
                         .arg( aPoint.X() ).arg( aPoint.Y() ).arg( aPoint.Z() )
                         .arg( ( k < aNbPoints ? "," : "" ) );
    }
  }
  else
  {
    HYDROData_IPolyline::PointsList aPointsList = GetParametricPoints();
    for ( int k = 1, aNbPoints = aPointsList.Size(); k <= aNbPoints; ++k )
    {
      const HYDROData_IPolyline::Point& aPoint = aPointsList.Value( k );
      aPntsDefinition << QString( aGap + "gp_XY( %1, %2 )%3" )
                         .arg( aPoint.X() ).arg( aPoint.Y() )
                         .arg( ( k < aNbPoints ? "," : "" ) );
    }
  }

  if ( !aPntsDefinition.isEmpty() )
  {
    QString& aFirstStr = aPntsDefinition.first();
    aFirstStr = aFirstStr.trimmed();
    aFirstStr.prepend( QString( "%1 = [ " ).arg( aPntsListName ) );

    aPntsDefinition.last().append( " ];" );

    aResList << aPntsDefinition;

    aResList << QString( "%1.%3( %2 )" )
                .arg( aProfileName ).arg( aPntsListName )
                .arg( anIsValidProfile ? "SetProfilePoints" : "SetParametricPoints" );

    aResList << QString( "" );
  }

  // Set a polyline type if it is not default
  Handle(HYDROData_ProfileUZ) aPrf = GetProfileUZ( false );
  if ( !aPrf.IsNull() )
  {
    HYDROData_IPolyline::SectionType aSecType = aPrf->GetSectionType( 0 );
    if ( aSecType != HYDROData_IPolyline::SECTION_POLYLINE )
    {
      aResList << QString( "%1.GetProfileUZ().SetSectionType( 0, %2 )" )
                  .arg( aProfileName ).arg( "HYDROData_IPolyline.SECTION_SPLINE" );
      aResList << QString( "" );
    }
  }

  aResList << QString( "%1.Update()" ).arg( aProfileName );
  aResList << QString( "" );

  return aResList;
}

TopoDS_Shape HYDROData_Profile::GetTopShape() const
{
  TopoDS_Wire aWire;

  gp_XY aFirstPoint, aLastPoint;
  if ( !GetLeftPoint( aFirstPoint, false ) || !GetRightPoint( aLastPoint, false ) )
    return aWire;

  gp_Pnt aPnt1( aFirstPoint.X(), aFirstPoint.Y(), 0 );
  gp_Pnt aPnt2( aLastPoint.X(),  aLastPoint.Y(),  0 );

  BRepBuilderAPI_MakeEdge aMakeEdge( aPnt1, aPnt2 );
  TopoDS_Edge anEdge = aMakeEdge;

  BRepBuilderAPI_MakeWire aMakeWire( anEdge );
  aWire = aMakeWire;

  return aWire;
}

TopoDS_Shape HYDROData_Profile::GetShape3D(bool forceRebuild, bool reverseXCoord) const
{
  if (forceRebuild)
    return CreateProfileWire( true, reverseXCoord );
  TopoDS_Shape aShape = HYDROData_Object::GetShape3D();
  if( aShape.IsNull() )
    aShape = CreateProfileWire( true, reverseXCoord );
  return aShape;
}

TopoDS_Shape HYDROData_Profile::CreateProfileWire( bool canUseDefaultPoints, bool reverseXCoord ) const
{
  TopoDS_Wire aWire;
  Handle(HYDROData_ProfileUZ) aProfile = GetProfileUZ( false );
  if ( !aProfile.IsNull() )
  {
    ProfilePoints aProfilePoints = GetProfilePoints( false, canUseDefaultPoints );
    HYDROData_IPolyline::SectionType aSectionType = aProfile->GetSectionType( 0 );
    if (reverseXCoord)
    {
      for ( int i = 1; i <= aProfilePoints.Size() ; i++ )
      {
        gp_XYZ& aPoint = aProfilePoints.ChangeValue(i);
        aPoint.ChangeCoord(1) = -aPoint.X();
      }
    }
    aWire = HYDROData_PolylineXY::BuildWire( aSectionType, false, aProfilePoints );
  }
  return aWire;
}

void HYDROData_Profile::Update()
{
  HYDROData_Object::Update();

  TopoDS_Shape aShape = CreateProfileWire( true );
  SetShape3D( aShape );
}

QColor HYDROData_Profile::DefaultFillingColor() const
{
  return QColor( Qt::transparent );
}

QColor HYDROData_Profile::DefaultBorderColor() const
{
  return QColor( Qt::black );
}

bool HYDROData_Profile::IsValid() const
{
  gp_XY aFirstPoint, aLastPoint;
  if ( !GetLeftPoint( aFirstPoint, false ) || !GetRightPoint( aLastPoint, false ) )
    return false;

  int aNbPoints = NbPoints();
  return aNbPoints > 1;
}

void HYDROData_Profile::SetLeftPoint( const gp_XY& theGPoint, bool IsConvertFromGlobal )
{
  TDF_Label aLabel = myLab.FindChild( DataTag_FirstPoint );
  if ( aLabel.IsNull() )
    return;

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  gp_XY aLPoint = theGPoint;
  if( IsConvertFromGlobal )
    aDoc->Transform( aLPoint, true );

  Handle(TDataStd_RealArray) anArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), anArray ) )
  {
    anArray = TDataStd_RealArray::Set( aLabel, 0, 1 );
    anArray->SetID(TDataStd_RealArray::GetID());
  }

  anArray->SetValue( 0, aLPoint.X() );
  anArray->SetValue( 1, aLPoint.Y() );

  Changed( Geom_3d );
}

bool HYDROData_Profile::GetLeftPoint( gp_XY& thePoint, bool IsConvertToGlobal,
                                      bool CanUseDefault ) const
{
  HYDROData_ProfileUZ::PointsList aParametricPoints = GetParametricPoints();
  if ( aParametricPoints.Length() < 2 )
    return false;

  thePoint = GetParametricPoints().First();

  //thePoint.SetX( 0 );
  thePoint.SetY( 0 ); //default left point of not-georeferenced profile
  TDF_Label aLabel = myLab.FindChild( DataTag_FirstPoint, false );
  if ( aLabel.IsNull() )
  {
    return CanUseDefault;
  }

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(TDataStd_RealArray) anArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), anArray ) )
  {
    return CanUseDefault;
  }

  thePoint.SetX( anArray->Value( 0 ) );
  thePoint.SetY( anArray->Value( 1 ) );

  if( IsConvertToGlobal )
    aDoc->Transform( thePoint, false );

  return true;
}

void HYDROData_Profile::SetRightPoint( const gp_XY& theGPoint, bool IsConvertFromGlobal )
{
  TDF_Label aLabel = myLab.FindChild( DataTag_LastPoint );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  gp_XY aLPoint = theGPoint;
  if( IsConvertFromGlobal )
    aDoc->Transform( aLPoint, true );

  Handle(TDataStd_RealArray) anArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), anArray ) )
  {
    anArray = TDataStd_RealArray::Set( aLabel, 0, 1 );
    anArray->SetID(TDataStd_RealArray::GetID());
  }

  anArray->SetValue( 0, aLPoint.X() );
  anArray->SetValue( 1, aLPoint.Y() );

  Changed( Geom_3d );
}

bool HYDROData_Profile::GetRightPoint( gp_XY& thePoint, bool IsConvertToGlobal,
                                       bool CanUseDefault ) const
{
  HYDROData_ProfileUZ::PointsList aParametricPoints = GetParametricPoints();
  if ( aParametricPoints.Length() < 2 )
    return false;

  thePoint = GetParametricPoints().Last();
  thePoint.SetY( 0 );

  TDF_Label aLabel = myLab.FindChild( DataTag_LastPoint, false );
  if ( aLabel.IsNull() )
  {
    return CanUseDefault;
  }

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(TDataStd_RealArray) anArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), anArray ) )
  {
    return CanUseDefault;
  }

  thePoint.SetX( anArray->Value( 0 ) );
  thePoint.SetY( anArray->Value( 1 ) );

  if( IsConvertToGlobal )
    aDoc->Transform( thePoint, false );

  return true;
}

void HYDROData_Profile::Invalidate()
{
  TDF_Label aFirstLabel = myLab.FindChild( DataTag_FirstPoint, false );
  if ( !aFirstLabel.IsNull() )
    aFirstLabel.ForgetAllAttributes();

  TDF_Label aLastLabel = myLab.FindChild( DataTag_LastPoint, false );
  if ( !aLastLabel.IsNull() )
    aLastLabel.ForgetAllAttributes();

  Changed( Geom_3d );
}

Handle(HYDROData_ProfileUZ) HYDROData_Profile::GetProfileUZ( const bool theIsCreate ) const
{
  Handle(HYDROData_ProfileUZ) aProfileUZ;

  TDF_Label aLabel = myLab.FindChild( DataTag_ChildProfileUZ, theIsCreate );
  if ( aLabel.IsNull() )
    return aProfileUZ;

  aProfileUZ = Handle(HYDROData_ProfileUZ)::DownCast( HYDROData_Iterator::Object( aLabel ) );
  if ( aProfileUZ.IsNull() && theIsCreate )
  {
    aProfileUZ = Handle(HYDROData_ProfileUZ)::DownCast(
      HYDROData_Iterator::CreateObject( aLabel, KIND_PROFILEUZ ) );
  }

  return aProfileUZ;
}

int HYDROData_Profile::NbPoints() const
{
  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ( false );
  return aProfileUZ.IsNull() ? 0 : aProfileUZ->NbPoints();
}

void HYDROData_Profile::RemovePoints()
{
  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ( false );
  if ( !aProfileUZ.IsNull() )
  {
    aProfileUZ->RemoveSections();
    Changed( Geom_3d );
  }
}

void HYDROData_Profile::SetParametricPoints( const HYDROData_ProfileUZ::PointsList& thePoints )
{
  RemovePoints();

  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ();
  for ( int i = 1, n = thePoints.Length(); i <= n ; ++i )
  {
    const HYDROData_ProfileUZ::Point& aPoint = thePoints.Value( i );
    aProfileUZ->AddPoint( 0, aPoint );
  }

  Changed( Geom_3d );
}

HYDROData_ProfileUZ::PointsList HYDROData_Profile::GetParametricPoints() const
{
  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ( false );
  return aProfileUZ.IsNull() ? HYDROData_ProfileUZ::PointsList() : aProfileUZ->GetPoints();
}

void HYDROData_Profile::SetProfilePoints( const ProfilePoints& thePoints, bool IsConvertFromGlobal )
{
  RemovePoints();
  if ( thePoints.Length() < 2 )
    return;

  gp_XY aFirstPoint, aLastPoint;

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(HYDROData_ProfileUZ) aProfileUZ = GetProfileUZ();
  for ( int i = 1, n = thePoints.Length(); i <= n ; ++i )
  {
    ProfilePoint aPoint = thePoints.Value( i );
    if( IsConvertFromGlobal )
      aDoc->Transform( aPoint, true );

    gp_XY aPointXY( aPoint.X(), aPoint.Y() );

    if ( i == 1 )
      aFirstPoint = aPointXY;
    else if ( i == n )
      aLastPoint = aPointXY;

    double aDistance = gp_Pnt2d( aFirstPoint ).Distance( aPointXY );

    HYDROData_ProfileUZ::Point aParPoint( aDistance, aPoint.Z() );
    aProfileUZ->AddPoint( 0, aParPoint );
  }

  SetLeftPoint( aFirstPoint, false );//already converted to local CS
  SetRightPoint( aLastPoint, false );
}

HYDROData_Profile::ProfilePoints HYDROData_Profile::GetProfilePoints
  ( bool IsConvertToGlobal, bool CanUseDefaultLeftRight ) const
{
  gp_XY aFirstPoint, aLastPoint;
  if ( !GetLeftPoint( aFirstPoint, IsConvertToGlobal, CanUseDefaultLeftRight ) ||
       !GetRightPoint( aLastPoint, IsConvertToGlobal, CanUseDefaultLeftRight ) )
    return HYDROData_Profile::ProfilePoints();

  HYDROData_ProfileUZ::PointsList aParametricPoints = GetParametricPoints();

  return CalculateProfilePoints(aParametricPoints, aFirstPoint, aLastPoint);
}

HYDROData_Profile::ProfilePoints HYDROData_Profile::CalculateProfilePoints(
  const HYDROData_ProfileUZ::PointsList& theParametricPoints,
  const gp_XY& aFirstPoint, const gp_XY& aLastPoint)
{
  ProfilePoints aResPoints;

  if ( theParametricPoints.Length() < 2 )
    return aResPoints;

  const HYDROData_ProfileUZ::Point& aFirstParPoint = theParametricPoints.First();
  const HYDROData_ProfileUZ::Point& aLastParPoint = theParametricPoints.Last();

  double aFullLength = aLastPoint.X() - aFirstPoint.X();
  double aParFullLength = aLastParPoint.X() - aFirstParPoint.X();

  // Add first point as is
  aResPoints.Append( ProfilePoint( aFirstPoint.X(), aFirstPoint.Y(), aFirstParPoint.Y() ) );

  // Compute all other points
  for ( int i = 2, n = theParametricPoints.Length(); i < n ; ++i )
  {
    const HYDROData_ProfileUZ::Point& aParPoint = theParametricPoints.Value( i );

    double aParPointDist = aParPoint.X() - aFirstParPoint.X();
    double aRatio = aParPointDist / aParFullLength;

    double aParX = aFirstPoint.X() * (1-aRatio) + aLastPoint.X() * aRatio;
    double aParY = aFirstPoint.Y() * (1-aRatio) + aLastPoint.Y() * aRatio;

    ProfilePoint aCompPoint( aParX, aParY, aParPoint.Y() );
    aResPoints.Append( aCompPoint );
  }

  // Add last point as is
  aResPoints.Append( ProfilePoint( aLastPoint.X(), aLastPoint.Y(), aLastParPoint.Y() ) );

  return aResPoints;
}

void HYDROData_Profile::SetFilePath( const TCollection_AsciiString& theFilePath )
{
   Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_FilePath ), theFilePath );
   anAttr->SetID(TDataStd_AsciiString::GetID());
}

TCollection_AsciiString HYDROData_Profile::GetFilePath() const
{
  TCollection_AsciiString aRes;

  Handle(TDataStd_AsciiString) anAsciiStr;
  if ( myLab.FindChild( DataTag_FilePath ).FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
    aRes = anAsciiStr->Get();

  return aRes;
}

void HYDROData_Profile::SetProfileColor( const QColor& theColor )
{
  SetColor( theColor, DataTag_ProfileColor );
}

bool HYDROData_Profile::GetProfileColor(QColor& outColor) const
{
  return GetColor( outColor, DataTag_ProfileColor );
}

int HYDROData_Profile::ImportFromFile( const Handle(HYDROData_Document)& theDoc,
                                       const TCollection_AsciiString&    theFileName,
                                       NCollection_Sequence<int>&        theBadProfilesIds,
                                       bool isToProject )
{
  if ( theDoc.IsNull() || theFileName.IsEmpty() )
    return 0;

  OSD_File aFile( theFileName );
  if ( !aFile.IsReadable() )
    return 0;

  aFile.Open( OSD_ReadOnly, OSD_Protection() );
  if ( !aFile.IsOpen() )
    return 0;

  NCollection_Sequence<Handle(HYDROData_Profile)> aCreatedProfiles;

  int aProfileId = 1;
  Handle(HYDROData_Profile) aNewProfile;
  for ( ; !aFile.IsAtEnd(); ++aProfileId )
  {
    if ( aNewProfile.IsNull() )
      aNewProfile = Handle(HYDROData_Profile)::DownCast( theDoc->CreateObject( KIND_PROFILE ) );

    bool anIsRead = false;
    if ( aNewProfile->ImportFromFile( aFile, isToProject, &anIsRead ) )
    {
      aCreatedProfiles.Append( aNewProfile );
      aNewProfile.Nullify();
    }
    else if ( anIsRead )
    {
      theBadProfilesIds.Append( aProfileId );
    }
  }

  if ( !aNewProfile.IsNull() )
    aNewProfile->Remove();

  // Close the file
  aFile.Close();

  for ( int i = 1, n = aCreatedProfiles.Length(); i <= n ; ++i )
  {
    Handle(HYDROData_Profile) aProfile = aCreatedProfiles.Value( i );

    QString aProfileName = HYDROData_Tool::GenerateObjectName( theDoc, "Profile" );
    aProfile->SetName( aProfileName );

    aProfile->SetFilePath( theFileName );

    aProfile->SetBorderColor( aProfile->DefaultBorderColor() );
  }

  return aCreatedProfiles.Length();
}

bool HYDROData_Profile::ImportFromFile( const TCollection_AsciiString& theFileName,
                                        bool isToProject,
                                        bool* isNotEmpty )
{
  if( isNotEmpty )
    *isNotEmpty = false;

  // Try to open the file
  OSD_File aFile( theFileName );
  if ( !aFile.IsReadable() )
    return false;

  aFile.Open( OSD_ReadOnly, OSD_Protection() );
  if ( !aFile.IsOpen() )
    return false;

  bool aRes = ImportFromFile( aFile, isToProject, isNotEmpty );

  // Close the file
  aFile.Close();

  if ( aRes )
  {
    // Update file path
    SetFilePath( theFileName );
  }

  return aRes;
}

bool HYDROData_Profile::ImportFromFile( OSD_File& theFile,
                                        bool isToProject,
                                        bool* isNotEmpty )
{
  if( isNotEmpty )
    *isNotEmpty = false;

  if ( !theFile.IsOpen() )
    return false;

  bool aRes = true;

  bool anIsParametric = false;
  bool anIsGeoref     = false;

  HYDROData_ProfileUZ::PointsList aPointsUZ;
  ProfilePoints                   aPointsXYZ;

  double aPrevVal = -DBL_MAX;
  while ( !theFile.IsAtEnd() )
  {
    Standard_Integer aNbRead = 0;
    TCollection_AsciiString aLine;
    theFile.ReadLine( aLine, 1024, aNbRead );

    aLine.LeftAdjust(); aLine.RightAdjust();
    if ( aLine.IsEmpty() )
    {
      if ( !anIsParametric && !anIsGeoref )
        continue; // Definition is not started yet

      break; // Next profile started
    }

    // Set flag of read status to true
    if( isNotEmpty )
      *isNotEmpty = true;

    TCollection_AsciiString aValX = aLine.Token( " \t", 1 );
    TCollection_AsciiString aValY = aLine.Token( " \t", 2 );
    TCollection_AsciiString aValZ = aLine.Token( " \t", 3 );

    if ( aValX.IsEmpty() || !aValX.IsRealValue() ||
         aValY.IsEmpty() || !aValY.IsRealValue() )
    {
      aRes = false;
      break;
    }

    if ( !anIsParametric && !anIsGeoref )
    {
      anIsParametric = aValZ.IsEmpty();
      anIsGeoref = !aValZ.IsEmpty();
    }

    double aCoordX = aValX.RealValue();
    double aCoordY = aValY.RealValue();

    if ( HYDROData_Tool::IsNan( aCoordX ) || HYDROData_Tool::IsInf( aCoordX ) ||
         HYDROData_Tool::IsNan( aCoordY ) || HYDROData_Tool::IsInf( aCoordY ) )
      aRes = false;

    if ( anIsParametric )
    {
      if ( aCoordX < aPrevVal )
      {
        // Move back readed line
        theFile.Seek( -( aNbRead + 1 ), OSD_FromHere );
        break;
      }

      HYDROData_ProfileUZ::Point aPoint( aCoordX, aCoordY );
      aPointsUZ.Append( aPoint );

      aPrevVal = aCoordX;
    }
    else
    {
      if ( aValZ.IsEmpty() || !aValZ.IsRealValue() )
      {
        aRes = false;
        break;
      }

      double aCoordZ = aValZ.RealValue();
      if ( HYDROData_Tool::IsNan( aCoordZ ) || HYDROData_Tool::IsInf( aCoordZ ) )
        aRes = false;

      ProfilePoint aPoint( aCoordX, aCoordY, aCoordZ );
      aPointsXYZ.Append( aPoint );
    }
  }

  aRes = aRes && ( anIsParametric && !aPointsUZ.IsEmpty() ||
                   anIsGeoref && !aPointsXYZ.IsEmpty() );
  if ( aRes )
  {
    // Update profile points
    if ( anIsParametric )
    {
      SetParametricPoints( aPointsUZ );
    }
    else if ( anIsGeoref )
    {
      if( isToProject )
        ProjectProfilePoints( aPointsXYZ );
      SetProfilePoints( aPointsXYZ, true );
    }

    Update();
  }

  return aRes;
}

void HYDROData_Profile::UpdateLocalCS( double theDx, double theDy )
{
  gp_XY aDelta( theDx, theDy );
  gp_XY aPnt;

  GetLeftPoint( aPnt, false );
  aPnt += aDelta;
  SetLeftPoint( aPnt, false );

  GetRightPoint( aPnt, false );
  aPnt += aDelta;
  SetRightPoint( aPnt, false );
}

HYDROData_Profile::ProfilePoint HYDROData_Profile::GetBottomPoint(bool IsConvertToGlobal) const
{
  ProfilePoint aBottom;

  // Get parametric points
  HYDROData_ProfileUZ::PointsList aParametricPoints = GetParametricPoints();
  if ( aParametricPoints.Length() < 1 ) {
    return aBottom;
  }

  // Calculate midvalue for U parameter
  Standard_Real anUMidValue = aParametricPoints.First().X();
  Standard_Real anUMinValue = anUMidValue;
  Standard_Real anUMaxValue = anUMidValue;

  for ( int i = 2, aNbPoints = aParametricPoints.Size(); i <= aNbPoints; i++ ) {
    const HYDROData_IPolyline::Point& aParPoint = aParametricPoints.Value( i );
    Standard_Real anU = aParPoint.X();

    if ( anU < anUMinValue ) {
      anUMinValue = anU;
    } else if ( anU > anUMaxValue ) {
      anUMaxValue = anU;
    }
  }

  anUMidValue = ( anUMinValue + anUMaxValue ) / 2;

  // Find index of the parametric point with minimal Z value
  int aBottomIndex = 1;
  HYDROData_IPolyline::Point aParBottom = aParametricPoints.First();

  for ( int i = 2, aNbPoints = aParametricPoints.Size(); i <= aNbPoints; i++ ) {
    const HYDROData_IPolyline::Point& aParPoint = aParametricPoints.Value( i );
    if ( aParPoint.Y() < aParBottom.Y() ) {
      aBottomIndex = i;
      aParBottom = aParPoint;
    } else if ( aParPoint.Y() == aParBottom.Y() ) {
      // Check which point is neares to the U = 0.5
      if ( fabs( aParPoint.X() - anUMidValue ) < fabs( aParBottom.X() - anUMidValue ) ) {
        aBottomIndex = i;
        aParBottom = aParPoint;
      }
    }
  }

  // Find the corresponding profile point
  ProfilePoints aProfilePoints = GetProfilePoints( IsConvertToGlobal );
  if ( aBottomIndex >= 1 && aBottomIndex <= aProfilePoints.Length() ) {
    aBottom = aProfilePoints.Value( aBottomIndex );
  }

  return aBottom;
}

 HYDROData_Profile::ProfilePoint HYDROData_Profile::GetMiddlePoint( bool CanUseDefault ) const
 {
   ProfilePoint aMiddlePoint;

   gp_XY aLeftPnt, aRightPnt;
   if ( GetLeftPoint( aLeftPnt, true, CanUseDefault ) && GetRightPoint( aRightPnt, true, CanUseDefault ) ) {
     gp_XYZ aPnt1( aLeftPnt.X(), aLeftPnt.Y(), 0. );
     gp_XYZ aPnt2( aRightPnt.X(), aRightPnt.Y(), 0. );
     gp_Pnt aMiddlePoint2d( 0.5 * ( aPnt1 + aPnt2 ) );

     gp_Lin aMidLin( aMiddlePoint2d, gp::DZ() );
     TopoDS_Edge aMidEdge = BRepLib_MakeEdge( aMidLin );

    TopoDS_Shape aProf = TopoDS::Wire( GetShape3D() );
     if (aProf.IsNull())
       return aMiddlePoint;
     TopoDS_Iterator anIt( aProf );
     for ( ; anIt.More(); anIt.Next()) {
       const TopoDS_Edge& anEdge = TopoDS::Edge( anIt.Value() );

       /*
       Standard_Real aStart, anEnd;
       Handle(Geom_Curve) aCurve = BRep_Tool::Curve( anEdge, aStart, anEnd );
       gp_Pnt aMiddlePointOnCurve = aCurve->Value( ( aStart + anEnd ) / 2 );
       */

       BRepExtrema_ExtCC ExtremaEE( aMidEdge, anEdge);
       if (ExtremaEE.IsDone() && ExtremaEE.NbExt() != 0) {
         for ( Standard_Integer i = 1; i <= ExtremaEE.NbExt(); i++ ) {
           if ( ExtremaEE.SquareDistance(i) <= Precision::Confusion() ) {
             aMiddlePoint = ExtremaEE.PointOnE1(i).XYZ();
             break;
           }
         }
       }
     }
   }

   return aMiddlePoint;
 }

void HYDROData_Profile::ProjectProfilePoints( ProfilePoints& thePoints )
{
  int low = thePoints.Lower(), up = thePoints.Upper();
  gp_Pnt aFirst = thePoints.Value( low );
  gp_Pnt aLast = thePoints.Value( up );
  gp_Vec d( aFirst, aLast );
  gp_Vec n( d.Y(), -d.X(), 0 );

  Handle(Geom_Plane) aPlane = new Geom_Plane( aFirst, gp_Dir( n ) );
  for( int i=low; i<=up; i++ )
  {
    gp_XYZ p = thePoints.Value( i );
    gp_Pnt pp = GeomAPI_ProjectPointOnSurf( p, aPlane );
    thePoints.SetValue( i, pp.XYZ() );
  }
}

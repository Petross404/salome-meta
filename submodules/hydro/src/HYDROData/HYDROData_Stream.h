// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Stream_HeaderFile
#define HYDROData_Stream_HeaderFile

#include "HYDROData_NaturalObject.h"

#include <TopoDS_Face.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>

#include <Geom_BSplineCurve.hxx>
#include <vector>
#include <NCollection_DataMap.hxx>
#include <QSet>

class HYDROData_DTM;
class HYDROData_LISM;
class HYDROData_PolylineXY;
class HYDROData_Polyline3D;
class HYDROData_Profile;
class HYDROData_IProfilesInterpolator;
class TopTools_HArray1OfShape;


/**\class HYDROData_Stream
 * \brief 
 *
 */
class HYDROData_Stream : public HYDROData_NaturalObject
{
public:

  struct PrsDefinition
  {
    TopoDS_Shape myPrs3D;
    TopoDS_Shape myPrs2D;
    TopoDS_Shape myLeftBank;   // 3d curve of the left bank
    TopoDS_Shape myRightBank;  // 3d curve of the right bank
    TopoDS_Shape myInlet;      // first (inlet) 2d profile 
    TopoDS_Shape myOutlet;     // last (inlet) 2d profile 
  };

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_NaturalObject::DataTag_First + 100, ///< first tag, to reserve
    DataTag_HydraulicAxis,     ///< reference hydraulic axis
    DataTag_Profile,           ///< reference profiles
    DataTag_ParamsArray,       ///< parameters array
    DataTag_BottomPolyline,    ///< reference bottom polyline
    DataTag_InterpMethod       ///< interpolation method (via DTM(0) or LISM(1))
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Stream, HYDROData_NaturalObject);

public:

  /**
   * Creates the presentations(2D and 3D) by given hydraulic axis and profiles.
   */
  HYDRODATA_EXPORT static bool CreatePresentations( const Handle(HYDROData_DTM)& theDTM,
                                                    PrsDefinition&               thePrs );

  HYDRODATA_EXPORT static void CreatePresentations( const Handle(TColgp_HArray1OfPnt)     theArrayOfFPnt,
                                                    const Handle(TColgp_HArray1OfPnt)     theArrayOfLPnt,
                                                    const Handle(TopTools_HArray1OfShape) theArrOfProfiles,
                                                    PrsDefinition&                        thePrs );

public:

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_STREAM;}

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;

  /**
   * Update the profiles order and shape presentations of stream.
   * Call this method whenever you made changes for stream data.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Checks that object has 2D presentation. Reimlemented to retun true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Update the shape presentations of stream.
   */
  HYDRODATA_EXPORT void UpdatePrs( const Handle(HYDROData_DTM)& );

  HYDRODATA_EXPORT void UpdatePrs( const Handle(HYDROData_LISM)& theLISM );

  /**
   *
   */
  HYDRODATA_EXPORT virtual void CopyTo( const Handle(HYDROData_Entity)& theDestination,
                                        bool isGenerateNewName ) const;


  /**
   * Returns default filling color for new stream.
   */
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new stream.
   */
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;

  /**
   * Returns true if given polyline can be used as stream axis.
   */
  HYDRODATA_EXPORT static bool IsValidAsAxis( const Handle(HYDROData_PolylineXY)& theAxis );

public:      
  // Public methods to work with Stream presentation
  
  /**
   * Returns the left edge of the stream.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetLeftShape() const;

  /**
   * Returns the right edge of the stream.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetRightShape() const;

  /**
   * Returns the inlet edge of the stream.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetInletShape() const;

  /**
   * Returns the outlet edge of the stream.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetOutletShape() const;


public:      
  // Public methods to work with Stream data fields

  /**
   * Sets reference hydraulic axis object for stream.
   */
  HYDRODATA_EXPORT virtual bool SetHydraulicAxis( const Handle(HYDROData_PolylineXY)& theAxis );

  /**
   * Returns reference hydraulic axis object of stream.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_PolylineXY) GetHydraulicAxis() const;

  /**
   * Remove reference hydraulic axis object from stream.
   */
  HYDRODATA_EXPORT virtual void RemoveHydraulicAxis();

  HYDRODATA_EXPORT virtual Handle(HYDROData_PolylineXY) GetLeftBank() const;

  HYDRODATA_EXPORT virtual void SetLeftBank( const Handle(HYDROData_PolylineXY)& theBank );

  HYDRODATA_EXPORT virtual Handle(HYDROData_PolylineXY) GetRightBank() const;

  HYDRODATA_EXPORT virtual void SetRightBank( const Handle(HYDROData_PolylineXY)& theBank );
  /**
   * Returns true if profile has the intersection with the given hydraulic axis.
   * Returns the parameter of inresection point on axis if axis is presented by one curve,
   * if axis presented by set of edges the <outPar> returns a common length of segments till the intersection point. 
   */
  HYDRODATA_EXPORT static bool HasIntersection( const Handle(HYDROData_PolylineXY)& theHydAxis, 
                                                const Handle(HYDROData_Profile)&    theProfile,
                                                const TopoDS_Face&                  thePlane,
                                                Standard_Real&                      theOutPar );

  /**
   * Returns true if profile has the intersection with reference hydraulic axis.
   * Returns the parameter of inresection point on axis if axis is presented by one curve,
   * if axis presented by set of edges the <outPar> returns a common length of segments till the intersection point. 
   */
  HYDRODATA_EXPORT virtual bool HasIntersection( const Handle(HYDROData_Profile)& theProfile,
                                                 const TopoDS_Face&               thePlane,
                                                 Standard_Real&                   theOutPar ) const;

  /**
  * Builds a planar face
  */
  HYDRODATA_EXPORT static void BuildRefFace( TopoDS_Face& thePlane );

  /**
   * Add new one reference profile object for stream.
   */
  HYDRODATA_EXPORT virtual bool AddProfile( const Handle(HYDROData_Profile)& theProfile );

  /**
   * Add new one reference profile object for stream.
   */
  HYDRODATA_EXPORT virtual bool SetProfiles( const HYDROData_SequenceOfObjects& theProfiles,
                                             const bool&                        theIsToOrder = true );

  /**
   * Returns all reference profile objects of stream.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetProfiles() const;

  /**
   * Removes reference profile object from stream.
   */
  HYDRODATA_EXPORT virtual bool RemoveProfile( const Handle(HYDROData_Profile)& theProfile );

  /**
   * Removes all reference profile objects from stream.
   */
  HYDRODATA_EXPORT virtual void RemoveProfiles();

  /**
   * Generates bottom polyline for stream or update the existing bottom polyline.
   * \return true in case of success
   */
  HYDRODATA_EXPORT virtual bool GenerateBottomPolyline();

  /**
   * Returns reference bottom polyline object of stream.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Polyline3D) GetBottomPolyline() const;
  
  /**
   * Sets reference bottom polyline object for stream.
   * \param theBottom the polyline 3D
   * \return true in case of success
   */
  HYDRODATA_EXPORT virtual bool SetBottomPolyline( const Handle(HYDROData_Polyline3D)& theBottom );

  /**
   * Add interpolated profiles into the stream.
   * \param theInterpolator the interpolator
   * \return true in case of success
   */
  HYDRODATA_EXPORT virtual bool Interpolate( HYDROData_IProfilesInterpolator* theInterpolator );
  

  HYDRODATA_EXPORT double GetDDZ() const;
  HYDRODATA_EXPORT void   SetDDZ( double theDDZ );
  
  HYDRODATA_EXPORT int    GetInterpolationMethod() const;
  HYDRODATA_EXPORT void   SetInterpolationMethod( int theMode );

  HYDRODATA_EXPORT double GetSpatialStep() const;
  HYDRODATA_EXPORT void   SetSpatialStep( double theSpatialStep );

  HYDRODATA_EXPORT double GetHaxStep() const;
  HYDRODATA_EXPORT void   SetHaxStep( double theHaxStep );

  HYDRODATA_EXPORT int    GetNbProfilePoints() const;
  HYDRODATA_EXPORT void   SetNbProfilePoints( int theNbPoints );  

  HYDRODATA_EXPORT void   GetWarnings(NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings);

  HYDRODATA_EXPORT QString GetBathyName();
  
protected:

  /**
   * Insert one profile in to the stream profiles order.
   */
  void insertProfileInToOrder( const Handle(HYDROData_Profile)& theProfile,
                               const int                        theBeforeIndex = -1 );

  /**
   * Fully recompute the order of all profiles in accordance with reference hydraulic axis.
   * If hydraulic axis is not set all profiles will be removed from order.
   */
  void updateProfilesOrder();
  
  /**
   * Returns the type of child altitude object.
   * Reimplemented to create stream altitude object.
   */
  HYDRODATA_EXPORT virtual ObjectKind getAltitudeObjectType() const;

  /**
   * Builds b-spline using interpolation algorithm.
   */
  static Handle(Geom_BSplineCurve) buildInterpolationCurve(
    const Handle(TColgp_HArray1OfPnt)& theArrayOfPnt );

private:
  
  void setParametersArray( const QVector<double>& theArray );

  TColStd_Array1OfReal* getParametersArray() const;

  void removeParametersArray();

  int insertParameter( const Standard_Real& theParam );

  void removeParameter( const int& theIndex );

  Handle(HYDROData_DTM) DTM() const;

  Handle(HYDROData_LISM) LISM() const;

  void internalUpdatePrs( const PrsDefinition& aResultPrs );
  

protected:
  friend class HYDROData_Iterator;
  friend class test_HYDROData_Stream;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Stream();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_Stream();

protected:
  NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>> myWarnings;
};

#endif



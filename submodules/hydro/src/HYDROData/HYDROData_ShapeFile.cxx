// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_ShapeFile.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_LandCoverMap.h>

#include <QFile>
#include <QFileInfo>
#include <TopoDS.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <BRep_Tool.hxx>
#include <BRepTools.hxx>
#include <Precision.hxx>
#include <Geom_Curve.hxx>
#include <Geom_Line.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <gp_Pln.hxx>
#include <BRepLib.hxx>
#include <ShapeFix_Shape.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <QColor>
#include <BRepTopAdaptor_FClass2d.hxx>
#include <TopExp.hxx>
#include <OSD_Timer.hxx>
#include <BRepLib_MakeVertex.hxx>
#include <NCollection_List.hxx>
#include <GC_MakeSegment.hxx>
#include <BRep_Builder.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <GCPnts_QuasiUniformDeflection.hxx>
#include <TopLoc_Location.hxx>
#include <Geom_Plane.hxx>
#include <NCollection_Array1.hxx>
#include <BRepBndLib.hxx>
#include <Bnd_Box.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

#ifdef WIN32
  #pragma warning( disable: 4996 )
#endif

//SHP->TFaces (Import)
#define OSD_TIMER

HYDROData_ShapeFile::HYDROData_ShapeFile() : myHSHP(NULL)
{
}

HYDROData_ShapeFile::~HYDROData_ShapeFile()
{
  Free();
}

void HYDROData_ShapeFile::Export(Handle(HYDROData_Document) theDocument,
                                 const QString& aFileName,
                                 NCollection_Sequence<Handle(HYDROData_PolylineXY)> aPolyXYSeq,
                                 NCollection_Sequence<Handle(HYDROData_Polyline3D)> aPoly3DSeq,
                                 QStringList& aNonExpList)
{
  DEBTRACE("aFileName " << aFileName.toStdString());
  SHPHandle hSHPHandle;
  QStringList anExpList;
  if (!aPolyXYSeq.IsEmpty() && aPoly3DSeq.IsEmpty())
  {
    hSHPHandle = SHPCreate( aFileName.toLatin1().data(), SHPT_ARC );
    for (int i = 1; i <= aPolyXYSeq.Size(); i++)
      if (WriteObjectPolyXY(theDocument, hSHPHandle, aPolyXYSeq(i)) != 1)
        aNonExpList.append(aPolyXYSeq(i)->GetName());
      else
      {
    	anExpList.append(aPolyXYSeq(i)->GetName());
    	DEBTRACE("  aName: " << aPolyXYSeq(i)->GetName().toStdString());
      }
  }
  else if (aPolyXYSeq.IsEmpty() && !aPoly3DSeq.IsEmpty())
  {
    hSHPHandle = SHPCreate( aFileName.toLatin1().data(), SHPT_ARCZ );
    for (int i = 1; i <= aPoly3DSeq.Size(); i++)
      if (WriteObjectPoly3D(theDocument, hSHPHandle, aPoly3DSeq(i)) != 1)
        aNonExpList.append(aPoly3DSeq(i)->GetName());
      else
      {
      	anExpList.append(aPoly3DSeq(i)->GetName());
    	DEBTRACE("  aName: " << aPoly3DSeq(i)->GetName().toStdString());
      }
  }
  if (hSHPHandle->nRecords > 0)
  {
    SHPClose( hSHPHandle );
    std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
    for (int i=0; i<anExpList.size(); i++)
    {
    	HYDROData_ShapeFile::DBF_AttrValue aCurAttrV;
		aCurAttrV.myStrVal = anExpList[i];
		aCurAttrV.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
		aCurAttrV.myIsNull = false;
		theAttrV.push_back(aCurAttrV);
    }
    QString theDBFFileName = aFileName.section('.',0,-2) + ".dbf";
    DEBTRACE("theDBFFileName " << theDBFFileName.toStdString());
    DBF_WriteFieldAndValues(theDBFFileName, "name", HYDROData_ShapeFile::DBF_FieldType_String, theAttrV, false);
  }
  else
  {
    SHPClose( hSHPHandle );
    QString aFN = aFileName.simplified();
    remove (aFN.toStdString().c_str());
    remove ((aFN.simplified().replace( aFN.simplified().size() - 4, 4, ".shx")).toStdString().c_str());
  }
}

void HYDROData_ShapeFile::Export(Handle(HYDROData_Document) theDocument,
                                 const QString& aFileName,
                                 const Handle(HYDROData_LandCoverMap)& aLCM,
                                 QStringList& aNonExpList,
                                 bool bCheckLinear, bool bUseDiscr, double theDefl)
{
  if (bCheckLinear && !aLCM->CheckLinear())
    return;

  SHPHandle hSHPHandle = NULL;
  if ( !aLCM.IsNull() && !aLCM->IsEmpty())
  {
    hSHPHandle = SHPCreate( aFileName.toLatin1().data(), SHPT_POLYGON );
    HYDROData_LandCoverMap::Explorer It( aLCM );
    for( ; It.More(); It.Next())
    {
      TopoDS_Face aFace = It.Face();
      if (WriteObjectPolygon(theDocument, hSHPHandle, aFace, bUseDiscr, theDefl) != 1)
        aNonExpList.append(aLCM->GetName() + "_" +  QString::number(It.Index()));
    }
  }
  if (hSHPHandle && hSHPHandle->nRecords > 0)
    SHPClose( hSHPHandle );
  else
  {
    SHPClose( hSHPHandle );
    QString aFN = aFileName.simplified();
    remove (aFN.toStdString().c_str());
    remove ((aFN.simplified().replace( aFN.simplified().size() - 4, 4, ".shx")).toStdString().c_str());
  }
}


int HYDROData_ShapeFile::WriteObjectPolyXY(Handle(HYDROData_Document) theDocument,
                                           SHPHandle theShpHandle,
                                           Handle(HYDROData_PolylineXY) thePoly )
{
  SHPObject	*aSHPObj;
  std::vector<double> x, y;
  std::vector<int> anPartStart;
  for (int i = 0; i < thePoly->NbSections(); i++)
  {
    anPartStart.push_back(x.size());
    HYDROData_PolylineXY::PointsList aPointList = thePoly->GetPoints(i, true);
    for (int j = 1; j <= aPointList.Size(); j++)
    {
      gp_XY P = aPointList(j);
      //theDocument->Transform(P, false);
      x.push_back( P.X());
      y.push_back( P.Y());
    }
    if (thePoly->IsClosedSection(i))
    {
      gp_XY P = aPointList(1);
      //theDocument->Transform(P, false);
      x.push_back( P.X());
      y.push_back( P.Y());
    }
  }

  aSHPObj = SHPCreateObject (SHPT_ARC, -1, thePoly->NbSections(), &anPartStart[0], NULL, x.size(), &x[0], &y[0], NULL, NULL );
  SHPWriteObject( theShpHandle, -1, aSHPObj );
  SHPDestroyObject( aSHPObj );
  return 1;
}

int HYDROData_ShapeFile::WriteObjectPoly3D(Handle(HYDROData_Document) theDocument,
                                           SHPHandle theShpHandle,
                                           Handle(HYDROData_Polyline3D) thePoly )
{
  SHPObject *aSHPObj;
  std::vector<double> x, y, z;
  std::vector<int> anPartStart;
  anPartStart.push_back(0); //one section only

  HYDROData_Polyline3D::Polyline3DPoints aPointList3D = thePoly->GetProfilePoints3D(true);
  for (int j = 1; j <= aPointList3D.Size(); j++)
  {
    const gp_XYZ& P = aPointList3D(j);
    //theDocument->Transform(P, false);
    x.push_back(P.X());
    y.push_back(P.Y());
    z.push_back(P.Z());
  }
  if ( thePoly->GetPolylineXY()->IsClosedSection(0))
  {
    const gp_XYZ& P = aPointList3D(1);
    //theDocument->Transform(P, false);
    x.push_back(P.X());
    y.push_back(P.Y());
    z.push_back(P.Z());
  }

  aSHPObj = SHPCreateObject( SHPT_ARCZ, -1, 1, &anPartStart[0], NULL, x.size(), &x[0], &y[0], &z[0], NULL );
  SHPWriteObject( theShpHandle, -1, aSHPObj );
  SHPDestroyObject( aSHPObj );
  return 1;
}

int HYDROData_ShapeFile::WriteObjectPolygon(Handle(HYDROData_Document) theDocument,
                                            SHPHandle theShpHandle,
                                            const TopoDS_Shape& theInputShape,
                                            bool bUseDiscr, double theDefl)
{
  if (theInputShape.IsNull())
    return 0;

  if (theInputShape.ShapeType() == TopAbs_FACE)
  {
    ProcessFace(theDocument, TopoDS::Face(theInputShape), theShpHandle, bUseDiscr, theDefl);
  }
  else if (theInputShape.ShapeType() == TopAbs_COMPOUND)
  {
    TopExp_Explorer Ex(theInputShape, TopAbs_FACE);
    for (; Ex.More(); Ex.Next())
    {
      TopoDS_Face aF = TopoDS::Face(Ex.Current());
      if (aF.IsNull())
        continue;
      ProcessFace(theDocument, aF, theShpHandle, bUseDiscr, theDefl);
    }
  }
  else
    return 0;

  return 1;

}

void HYDROData_ShapeFile::ProcessFace(Handle(HYDROData_Document) theDocument,
                                      const TopoDS_Face& theFace,
                                      SHPHandle theShpHandle,
                                      bool bUseDiscr, double theDefl )
{
  if (theFace.ShapeType() != TopAbs_FACE)
     return;
  SHPObject	*aSHPObj;
  std::vector<double> x, y;
  std::vector<int> anPartStart;
  TopoDS_Wire OuterW = BRepTools::OuterWire(theFace);
  NCollection_Sequence<TopoDS_Wire> aWires;

  //write an outer wire first
  aWires.Append(OuterW);
  TopExp_Explorer Ex(theFace, TopAbs_WIRE);
  for (; Ex.More(); Ex.Next())
  {
    TopoDS_Wire aW = TopoDS::Wire(Ex.Current());
    if (aW.IsEqual(OuterW))
      continue;
    aWires.Append(aW);
  }

  int NbWires = 0;
  for (int k = 1; k <= aWires.Length(); k++)
  {
    TopoDS_Wire aW = aWires(k);
    if (aW.IsNull())
      continue;
    NbWires++;
    if (aW.Orientation() == TopAbs_INTERNAL)
      //cant write internal wires/edges
      continue;
    // Try to reorder edges
    Handle(ShapeFix_Wire) aSFW = new ShapeFix_Wire( aW, theFace, Precision::Confusion() );
    aSFW->ModifyTopologyMode() = Standard_False;
    aSFW->ModifyGeometryMode() = Standard_False;
    aSFW->FixReorder();
    Handle(ShapeExtend_WireData) aSEWD = aSFW->WireData();
    Standard_Integer nbE = aSEWD->NbEdges();
    if (nbE == 0)
      continue;
    //
    anPartStart.push_back(x.size());
    NCollection_Sequence<gp_Pnt2d> aPnts;
    for (Standard_Integer i = 1; i <= nbE; i++)
    {
      TopoDS_Edge E = aSEWD->Edge(i);
      if (!bUseDiscr)
      {
        TopoDS_Vertex aV = TopExp::LastVertex(E, 1);
        if (aV.IsNull())
          continue;
        gp_Pnt P = BRep_Tool::Pnt(aV);
        aPnts.Append(gp_Pnt2d(P.X(), P.Y()));
      }
      else
      {
        BRepAdaptor_Curve Cur( E );
        GCPnts_QuasiUniformDeflection Discr( Cur, theDefl );
        if( !Discr.IsDone() )
          continue; //skip edge?
        double NewDefl = theDefl/2.0;
        while (Discr.NbPoints() < 2)
        {
          Discr.Initialize(Cur, NewDefl);
          NewDefl = NewDefl/2.0;
        }
        //
        if (E.Orientation() == TopAbs_FORWARD)
        {
          for( int i = 1; i <= Discr.NbPoints(); i++ )
          {
            gp_Pnt P = Discr.Value( i );
            aPnts.Append(gp_Pnt2d(P.X(), P.Y()));
          }
        }
        else
        {
          for( int i = Discr.NbPoints(); i > 0; i-- )
          {
            gp_Pnt P = Discr.Value( i );
            aPnts.Append(gp_Pnt2d(P.X(), P.Y()));
          }
        }
      }
    }
    NCollection_Sequence<gp_Pnt2d> aNPnts;
    aNPnts.Append(aPnts.First());
    for (int j = 1; j <= aPnts.Size() - 1; j++)
    {
      if (!aPnts(j).IsEqual(aPnts(j + 1), Precision::Confusion()))
        aNPnts.Append(aPnts(j + 1));
    }

    //assume that the orientation of external wire & internal wires is correct
    //so just write all points "as-is"
    //External wire will be written in clockwise direction
    //any other wires (holes) - in anticlockwise direction
    for (int j = aNPnts.Size(); j >= 1; j--)
    {
      gp_XY P = gp_XY(aNPnts(j).X(), aNPnts(j).Y());
      theDocument->Transform(P, false);
      x.push_back( P.X());
      y.push_back( P.Y());
    }
    //first point is same as the last one => closed polygon
    gp_XY P = gp_XY(aNPnts.Last().X(), aNPnts.Last().Y());
    theDocument->Transform(P, false);
    x.push_back( P.X());
    y.push_back( P.Y());

  }

  aSHPObj = SHPCreateObject( SHPT_POLYGON, -1, NbWires, &anPartStart[0], NULL, x.size(), &x[0], &y[0], NULL, NULL );
  SHPWriteObject( theShpHandle, -1, aSHPObj );
  SHPDestroyObject( aSHPObj );

}

bool HYDROData_ShapeFile::Parse(SHPHandle theHandle, ShapeType theType)
{
  int aShapeType;
  mySHPObjects.clear();
  SHPGetInfo( theHandle, NULL, &aShapeType, NULL, NULL );
  //theShapeTypeOfFile = aShapeType;
  bool ToRead = (theType == ShapeType_Polyline &&
    (aShapeType == 3 || aShapeType == 13 || aShapeType == 23 || aShapeType == 5)) ||
   (theType == ShapeType_Polygon && aShapeType == 5);
  if (ToRead)
  {
    for (int i = 0; i < theHandle->nRecords; i++)
      mySHPObjects.push_back(SHPReadObject(theHandle, i));
    return true;
  }
  else
    return false;
}

void HYDROData_ShapeFile::ReadSHPPolygon(Handle(HYDROData_Document) theDocument, SHPObject* anObj, int i, TopoDS_Face& F)
{
  if (!anObj)
    return;
  TopoDS_Edge E;
  int nParts = anObj->nParts;
  Handle(Geom_Plane) aPlaneSur = new Geom_Plane(gp_Pnt(0,0,0), gp_Dir(0,0,1));

  BRep_Builder BB;
  BB.MakeFace(F);

  NCollection_Sequence<TopoDS_Wire> allWires;

  TopTools_SequenceOfShape aWires;
  for ( int i = 0 ; i < nParts ; i++ )
  {
    BRepBuilderAPI_MakeWire aBuilder;
    int StartIndex = anObj->panPartStart[i];
    int EndIndex;
    if (i != nParts - 1)
      EndIndex = anObj->panPartStart[i + 1];
    else
      EndIndex = anObj->nVertices;

    TopoDS_Wire W;
    BB.MakeWire(W);

    //First point is same as the last point
    int NbPnts = EndIndex - StartIndex - 1;
    NCollection_Array1<TopoDS_Vertex> VPoints(0, NbPnts);
    int j = NbPnts;
    for ( int k = StartIndex; k < EndIndex; k++ )
    {
      gp_Pnt P (anObj->padfX[k], anObj->padfY[k], 0);
      theDocument->Transform(P, true);
      VPoints.ChangeValue(j) = BRepLib_MakeVertex(P).Vertex();
      j--;
    }

    for ( int k = 0; k < VPoints.Size() - 1; k++ )
    {
      gp_Pnt P1 = BRep_Tool::Pnt(VPoints(k));
      gp_Pnt P2 = BRep_Tool::Pnt(VPoints(k + 1));
      if (P1.Distance(P2) < Precision::Confusion())
        continue;
      Handle(Geom_TrimmedCurve) aTC = GC_MakeSegment(P1, P2).Value();
      TopoDS_Edge E;
      if ( k != VPoints.Size() - 2)
        E = BRepLib_MakeEdge(aTC, VPoints(k), VPoints(k + 1)).Edge();
      else
        //the last edge => use first and last vertices
        E = BRepLib_MakeEdge(aTC, VPoints.First(), VPoints.Value(VPoints.Upper() - 1)).Edge();
      //Add edge to wire
      //If SHP file is correct then the outer wire and the holes will have the correct orientations
      W.Closed (Standard_True);
      W.Orientation(TopAbs_FORWARD);
      BB.Add(W, E);
    }
    allWires.Append(W);
  }

  int OutWIndex = -1;
  if (allWires.Size() > 1)
  {
    NCollection_Sequence<Bnd_Box> BBs;
    //try to find the largest bbox
    for (int i = 1; i <= allWires.Size(); i++)
    {
      TopoDS_Wire W = allWires(i);
      Bnd_Box BB;
      BRepBndLib::AddClose(W, BB);
      BBs.Append(BB);
    }
    for (int i = 1; i <= BBs.Size(); i++)
    {
      bool IsIn = false;
      for (int j = 1; j <= BBs.Size(); j++)
      {
        if (i == j)
          continue;
        Standard_Real iXmax, iXmin, iYmax, iYmin, z0, z1;
        Standard_Real jXmax, jXmin, jYmax, jYmin;
        BBs(i).Get(iXmin, iYmin, z0, iXmax, iYmax, z1);
        BBs(j).Get(jXmin, jYmin, z0, jXmax, jYmax, z1);
        if (!(iXmin > jXmin &&
            iYmin > jYmin &&
            iXmax < jXmax &&
            iYmax < jYmax))
          IsIn = true;
      }
      if (IsIn)
      {
        OutWIndex = i;
        break;
      }
    }
  }
  else
    OutWIndex = 1; //one wire => no need to check

  for (int i = 1; i <= allWires.Size(); i++)
  {
    TopoDS_Face DF;
    BB.MakeFace(DF);
    TopoDS_Wire W = allWires(i);
    BB.Add(DF, W);
    BB.UpdateFace(DF, aPlaneSur, TopLoc_Location(), Precision::Confusion());
    //
    BRepTopAdaptor_FClass2d FClass(DF, Precision::PConfusion());
    if ( i == OutWIndex && FClass.PerformInfinitePoint() == TopAbs_IN)
      W.Reverse();
    if ( i != OutWIndex && FClass.PerformInfinitePoint() == TopAbs_OUT)
      W.Reverse();
    //
    BB.Add(F, W);
  }

  //Add surface to the face
  BB.UpdateFace(F, aPlaneSur, TopLoc_Location(), Precision::Confusion());
  F.Closed(Standard_True);
}

int HYDROData_ShapeFile::ImportPolygons(Handle(HYDROData_Document) theDocument, const QString theFileName,
  QStringList& thePolygonsList, TopTools_SequenceOfShape& theFaces)
{
  Free();
  int Stat = TryOpenShapeFile(theFileName);
  if (Stat != 0)
    return Stat;
  myHSHP = SHPOpen( theFileName.toLatin1().data(), "rb" );
  if (!Parse(myHSHP, HYDROData_ShapeFile::ShapeType_Polygon))
    return 0;
  for (size_t i = 0; i < mySHPObjects.size(); i++)
    thePolygonsList.append("polygon_" + QString::number(i + 1));

  TopoDS_Face aF;
  if (myHSHP->nShapeType == 5)
  {
#ifdef OSD_TIMER
    OSD_Timer timer;
    timer.Start();
#endif
    for (size_t i = 0; i < mySHPObjects.size(); i++)
    {
       ReadSHPPolygon(theDocument, mySHPObjects[i], i, aF);
       theFaces.Append(aF);
    }
#ifdef OSD_TIMER
    timer.Stop();
    timer.Show();
#endif
    return 1;
  }
  else
    return 0;
}

void HYDROData_ShapeFile::Free()
{
  for (size_t i = 0; i < mySHPObjects.size(); i++ )
    free (mySHPObjects[i]);

  mySHPObjects.clear();
  if (myHSHP != NULL)
  {
    SHPClose(myHSHP);
    myHSHP = NULL;
  }
}


void HYDROData_ShapeFile::ReadSHPPolyXY(Handle(HYDROData_Document) theDocument, SHPObject* anObj, QString theFileName,
  int theInd, NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities, bool bReadAsPolyline)
{
  Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast( theDocument->CreateObject( KIND_POLYLINEXY ) );

  int nParts = anObj->nParts;
  for ( int i = 0 ; i < nParts ; i++ )
  {
    int StartIndex = anObj->panPartStart[i];
    int EndIndex;
    if (i != nParts - 1)
      EndIndex = anObj->panPartStart[i + 1];
    else
      EndIndex = anObj->nVertices;

    HYDROData_PolylineXY::SectionType aSectType = HYDROData_PolylineXY::SECTION_POLYLINE;
    if (!bReadAsPolyline)
    {
      bool IsClosed = false;
      if (anObj->padfX[StartIndex] == anObj->padfX[EndIndex - 1] &&
        anObj->padfY[StartIndex] == anObj->padfY[EndIndex - 1] )
      {
        IsClosed = true;
        aPolylineXY->AddSection( TCollection_AsciiString( ("poly_section_" + QString::number(i)).data()->toLatin1()), aSectType, true);
      }
      else
        aPolylineXY->AddSection( TCollection_AsciiString( ("poly_section_" + QString::number(i)).data()->toLatin1()), aSectType, false);

      if (IsClosed)
        EndIndex--;
    }
    else
    {
      //polygon; contours always closed
      aPolylineXY->AddSection( TCollection_AsciiString( ("poly_section_" + QString::number(i)).data()->toLatin1()), aSectType, true);
      EndIndex--;
    }
    for ( int k = StartIndex; k < EndIndex ; k++ )
    {
      HYDROData_PolylineXY::Point aSectPoint = gp_XY(anObj->padfX[k], anObj->padfY[k]);
      theDocument->Transform(aSectPoint, true);
      aPolylineXY->AddPoint( i, aSectPoint );
    }

  }

  aPolylineXY->SetWireColor( HYDROData_PolylineXY::DefaultWireColor() );
  aPolylineXY->SetName( theFileName + "_PolyXY_" + QString::number(theInd) );

  aPolylineXY->Update();
  theEntities.Append(aPolylineXY);
}


void HYDROData_ShapeFile::ReadSHPPoly3D(Handle(HYDROData_Document) theDocument, SHPObject* anObj,
  const Handle(HYDROData_PolylineXY)& aPolylineXY,
  NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities)
{
  Handle(HYDROData_Polyline3D) aPolylineObj = Handle(HYDROData_Polyline3D)::DownCast( theDocument->CreateObject( KIND_POLYLINE ) );
  Handle(HYDROData_Bathymetry) aBath = Handle(HYDROData_Bathymetry)::DownCast( theDocument->CreateObject( KIND_BATHYMETRY ) );

  HYDROData_Bathymetry::AltitudePoints aAPoints;

  int nParts = anObj->nParts;
  for ( int i = 0 ; i < nParts ; i++ )
  {
    //bool aSectClosure = true;
    int StartIndex = anObj->panPartStart[i];
    int EndIndex;
    if (i != nParts - 1)
      EndIndex = anObj->panPartStart[i + 1];
    else
      EndIndex = anObj->nVertices;

    HYDROData_PolylineXY::SectionType aSectType = HYDROData_PolylineXY::SECTION_POLYLINE;
    if (anObj->padfX[StartIndex] == anObj->padfX[EndIndex - 1] &&
      anObj->padfY[StartIndex] == anObj->padfY[EndIndex - 1] &&
      anObj->padfZ[StartIndex] == anObj->padfZ[EndIndex - 1])
      EndIndex--;
    for ( int k = StartIndex ; k < EndIndex ; k++ )
    {
      gp_XYZ xyz(anObj->padfX[k], anObj->padfY[k], anObj->padfZ[k]);
      theDocument->Transform(xyz, true);
      HYDROData_Bathymetry::AltitudePoint p;
      p.X = xyz.X();
      p.Y = xyz.Y();
      p.Z = xyz.Z();
      aAPoints.push_back(p);
    }
  }

  QString PolyXYName = aPolylineXY->GetName();
  QString aPoly3DName = PolyXYName, aBathyName = PolyXYName;
  aPoly3DName.replace(PolyXYName.lastIndexOf("_PolyXY_"), 8, "_Poly3D_");
  aBathyName.replace(PolyXYName.lastIndexOf("_Bath_"), 6, "_Poly3D_");

  aBath->SetAltitudePoints(aAPoints);
  aBath->SetName( aBathyName );

  aPolylineObj->SetPolylineXY (aPolylineXY, false);
  aPolylineObj->SetAltitudeObject(aBath);

  aPolylineObj->SetBorderColor( aPolylineObj->DefaultBorderColor() );
  aPolylineObj->SetName( aPoly3DName );

  aPolylineObj->Update();

  theEntities.Append(aPolylineObj);
}

/*
void HYDROData_ShapeFile::ReadSHPPoly3D(Handle(HYDROData_Document) theDocument, SHPObject* anObj, QString theFileName,
  int theInd, bool XYOnly, NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities)
{
  Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast( theDocument->CreateObject( KIND_POLYLINEXY ) );

  Handle(HYDROData_Polyline3D) aPolylineObj;
  Handle(HYDROData_Bathymetry) aBath;
  if (!XYOnly)
  {
    Handle(HYDROData_Polyline3D) aPolylineObj = Handle(HYDROData_Polyline3D)::DownCast( theDocument->CreateObject( KIND_POLYLINE ) );
    Handle(HYDROData_Bathymetry) aBath = Handle(HYDROData_Bathymetry)::DownCast( theDocument->CreateObject( KIND_BATHYMETRY ) );
  }

  HYDROData_Bathymetry::AltitudePoints aAPoints;

  int nParts = anObj->nParts;
  for ( int i = 0 ; i < nParts ; i++ )
  {
    //bool aSectClosure = true;
    int StartIndex = anObj->panPartStart[i];
    int EndIndex;
    if (i != nParts - 1)
      EndIndex = anObj->panPartStart[i + 1];
    else
      EndIndex = anObj->nVertices;

    bool IsClosed = false;
    HYDROData_PolylineXY::SectionType aSectType = HYDROData_PolylineXY::SECTION_POLYLINE;
    if (anObj->padfX[StartIndex] == anObj->padfX[EndIndex - 1] &&
        anObj->padfY[StartIndex] == anObj->padfY[EndIndex - 1] &&
        anObj->padfZ[StartIndex] == anObj->padfZ[EndIndex - 1])
    {
      IsClosed = true;
      aPolylineXY->AddSection( TCollection_AsciiString( ("poly_section_" + QString::number(i)).data()->toLatin1()), aSectType, true );
    }
    else
      aPolylineXY->AddSection( TCollection_AsciiString( ("poly_section_" + QString::number(i)).data()->toLatin1()), aSectType, false  );

    if (IsClosed)
      EndIndex--;
    for ( int k = StartIndex ; k < EndIndex ; k++ )
    {
      HYDROData_PolylineXY::Point aSectPoint = gp_XY(anObj->padfX[k], anObj->padfY[k]);
      theDocument->Transform(aSectPoint, true);
      aPolylineXY->AddPoint( i, aSectPoint );
      if (!XYOnly)
      {
        HYDROData_Bathymetry::AltitudePoint p;
        p.X = aSectPoint.X();
        p.Y = aSectPoint.Y();
        p.Z = anObj->padfZ[k];
        aAPoints.push_back(p);
      }
    }
  }


  QString aBathName = theFileName + "_bath_" + QString::number(theInd);
  QString aPolyXYName = theFileName + "_polyXY_" + QString::number(theInd);
  QString aPoly3DName = theFileName + "_poly3D_" + QString::number(theInd);

  aPolylineXY->SetName( aPolyXYName );
  aPolylineXY->SetWireColor(HYDROData_PolylineXY::DefaultWireColor());
  aPolylineXY->Update();

  if (!XYOnly)
  {
    aBath->SetAltitudePoints(aAPoints);
    aBath->SetName( aBathName );

    aPolylineObj->SetPolylineXY (aPolylineXY, false);
    aPolylineObj->SetAltitudeObject(aBath);

    aPolylineObj->SetBorderColor( aPolylineObj->DefaultBorderColor() );
    aPolylineObj->SetName( aPoly3DName );

    aPolylineObj->Update();
  }

  theEntities.Append(aPolylineXY);

  if (!XYOnly)
    theEntities.Append(aPolylineObj);

}*/

void HYDROData_ShapeFile::GetFreeIndices(std::vector<int>& theAllowedIndexes, QString strName, size_t theObjsSize,
                                         QStringList theExistingNames, QString theBaseFileName)
{
  int anInd = 0;
  for (;theAllowedIndexes.size() < theObjsSize;)
  {
    if (!theExistingNames.contains(theBaseFileName + strName + QString::number(anInd)))
    {
      theAllowedIndexes.push_back(anInd);
      anInd++;
    }
    else
      anInd++;
  }
}

int HYDROData_ShapeFile::OpenAndParse(const QString& theFileName)
{
  //Free();
  int aStat = TryOpenShapeFile(theFileName);
  if (aStat != 0)
    return aStat;

  //SHPHandle aHSHP;
  myHSHP = SHPOpen( theFileName.toLatin1().data(), "rb" );
  if (myHSHP == NULL)
    return -1;

  if (!Parse(myHSHP, HYDROData_ShapeFile::ShapeType_Polyline))
    return -1;

  return 1;
  //sh_type = aHSHP->nShapeType;
}

int HYDROData_ShapeFile::ImportPolylines(Handle(HYDROData_Document) theDocument,
  const QString& theFileName,
  NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*>& theEntitiesToSHPObj,
  ImportShapeType theShapeTypesToImport)
{
  int aStat = -1;
  int sh_type = myHSHP->nShapeType;
  QFileInfo aFileInfo(theFileName);
  QString aBaseFileName = aFileInfo.baseName();
  HYDROData_Iterator anIter( theDocument );
  int anInd = 0;
  QStringList anExistingNames;
  std::vector<int> anAllowedIndexes;
  for( ; anIter.More(); anIter.Next() )
    anExistingNames.push_back(anIter.Current()->GetName());

  NCollection_Sequence<Handle(HYDROData_Entity)> Ents;
  if ((theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_All ||
    theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_Polyline)
    && (sh_type == 3 || sh_type == 23))
  {
    size_t anObjsSize = mySHPObjects.size();
    GetFreeIndices(anAllowedIndexes, "_PolyXY_", anObjsSize, anExistingNames, aBaseFileName);

    for (size_t i = 0; i < mySHPObjects.size(); i++ )
    {
      ReadSHPPolyXY(theDocument, mySHPObjects[i], aBaseFileName, anAllowedIndexes[i], Ents, false);
      for (int k=1;k<=Ents.Size();k++)
        theEntitiesToSHPObj.Add(Ents(k), mySHPObjects[i]);
    }
    aStat = 1;
  }
  else if ((theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_All ||
    theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_Polyline3D) && sh_type == 13)
  {
    anInd = 0;
    for (;anAllowedIndexes.size() < mySHPObjects.size();)
    {
      if (!anExistingNames.contains(aBaseFileName + "_PolyXY_" + QString::number(anInd)) &&
          !anExistingNames.contains(aBaseFileName + "_Poly3D_" + QString::number(anInd)) &&
          !anExistingNames.contains(aBaseFileName + "_Bath_" + QString::number(anInd)))
      {
        anAllowedIndexes.push_back(anInd);
        anInd++;
      }
      else
        anInd++;
    }
    for (size_t i = 0; i < mySHPObjects.size(); i++ )
    {
      //ReadSHPPoly3D(theDocument, mySHPObjects[i], aBaseFileName, anAllowedIndexes[i], false, theEntities);
      ReadSHPPolyXY(theDocument, mySHPObjects[i], aBaseFileName, anAllowedIndexes[i], Ents, false); //TODO check Z coord on closeness
      for (int k=1;k<=Ents.Size();k++)
        theEntitiesToSHPObj.Add(Ents(k), mySHPObjects[i]);
    }
    aStat = 1;
  }
  else if ((theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_All ||
    theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_Polygon) && sh_type == 5)
  {
    //import polygon's contours as polylines
    size_t anObjsSize = mySHPObjects.size();
    GetFreeIndices(anAllowedIndexes, "_PolyXY_", anObjsSize, anExistingNames, aBaseFileName);

    for (size_t i = 0; i < mySHPObjects.size(); i++ )
    {
      ReadSHPPolyXY(theDocument, mySHPObjects[i], aBaseFileName, anAllowedIndexes[i], Ents, true);
      for (int k=1;k<=Ents.Size();k++)
        theEntitiesToSHPObj.Add(Ents(k), mySHPObjects[i]);
    }
    aStat = 2;
  }
  else
  {
    aStat = 0;
  }

  return aStat;
}

int HYDROData_ShapeFile::GetShapeType() const
{
  if (myHSHP != NULL)
    return myHSHP->nShapeType;
  else
    return -1;
}

int HYDROData_ShapeFile::ImportPolylines3D(Handle(HYDROData_Document) theDocument,
                                           NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*> thePolyXYToObj,
                                           NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities,
                                           ImportShapeType theShapeTypesToImport)
{
  int aStat = -1;
  int sh_type = myHSHP->nShapeType;
  if ((theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_All ||
    theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_Polyline3D) && sh_type == 13)
  {
    //for (size_t i = 0; i < mySHPObjects.size(); i++ )
    for (int i = 1; i <= thePolyXYToObj.Extent(); i++ )
    {
      Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast( thePolyXYToObj.FindKey(i) );
      ReadSHPPoly3D(theDocument, thePolyXYToObj(i), aPolylineXY, theEntities);
    }
    aStat = 1;
  }
  else
    aStat = 0;

  return aStat;
}


QString HYDROData_ShapeFile::GetShapeTypeName(int theType)
{
  switch (theType)
  {
    case 0:
      return "null shape";
    case 1:
      return "point (unsupported by HYDRO)";
    case 3:
      return "arc/polyline (supported by HYDRO)";
    case 5:
      return "polygon (supported by HYDRO)";
    case 8:
      return "multipoint (unsupported by HYDRO)";
    case 11:
      return "pointZ (unsupported by HYDRO)";
    case 13:
      return "arcZ/polyline (supported by HYDRO)";
    case 15:
      return "polygonZ (unsupported by HYDRO)";
    case 18:
      return "multipointZ (unsupported by HYDRO)";
    case 21:
      return "pointM (unsupported by HYDRO)";
    case 23:
      return "arcM/polyline (supported by HYDRO)";
    case 25:
      return "polygonM (unsupported by HYDRO)";
    case 28:
      return "multipointM (unsupported by HYDRO)";
    case 31:
      return "multipatch (unsupported by HYDRO)";
    default:
      return "unknown";
  }
}

int HYDROData_ShapeFile::TryOpenShapeFile(QString theFileName)
{
  QString aSHPfile = theFileName.simplified();
  QString aSHXfile = theFileName.simplified().replace( theFileName.simplified().size() - 4, 4, ".shx");

  QString anExt = theFileName.split('.', QString::SkipEmptyParts).back();
  if (anExt.toLower() != "shp")
    return -3;

  FILE* pFileSHP = NULL;
  pFileSHP = fopen (aSHPfile.toLatin1().data(), "r");
  FILE* pFileSHX = NULL;
  pFileSHX = fopen (aSHXfile.toLatin1().data(), "r");

  if (pFileSHP == NULL || pFileSHX == NULL)
  {
    if (pFileSHP == NULL)
      return -1;
    if (pFileSHX == NULL)
      return -2;
  }

  fclose (pFileSHP);
  fclose (pFileSHX);
  return 0;
}


bool HYDROData_ShapeFile::CheckDBFFileExisting(const QString& theSHPFilePath, QString& thePathToDBFFile)
{
  QString aSHPfile = theSHPFilePath.simplified();
  QString aDBFfile = theSHPFilePath.simplified().replace( theSHPFilePath.simplified().size() - 4, 4, ".dbf");
  FILE* pFileDBF = NULL;
  pFileDBF = fopen (aDBFfile.toLatin1().data(), "r");

  if (pFileDBF == NULL)
  {
    return false;
  }

  fclose (pFileDBF);
  thePathToDBFFile = aDBFfile;
  return true;
}


bool HYDROData_ShapeFile::DBF_OpenDBF(const QString& thePathToDBFFile)
{
  myHDBF = DBFOpen( thePathToDBFFile.toLatin1().data(), "r" );
  if(myHDBF != NULL)
    return true;
  else
    return false;
}

int HYDROData_ShapeFile::DBF_GetNbFields()
{
  if(myHDBF == NULL)
    return 0;
  return DBFGetFieldCount(myHDBF);
}

void HYDROData_ShapeFile::DBF_CloseDBF()
{
  if(myHDBF != NULL)
     DBFClose( myHDBF );
}

QStringList HYDROData_ShapeFile::DBF_GetFieldList()
{
  QStringList FieldList;
  int	nWidth, nDecimals;
  char chField[12];

  for( int i = 0; i < DBFGetFieldCount(myHDBF); i++ )
  {
      DBFFieldType eType;
      eType = DBFGetFieldInfo( myHDBF, i, chField, &nWidth, &nDecimals );
      FieldList.append(QString(chField));
  }

  return FieldList;
}

void HYDROData_ShapeFile::DBF_GetFieldTypeList(std::vector<DBF_FieldType>& FTVect)
{
  int nWidth, nDecimals;
  char chField[12];
  DBF_FieldType FT;
  for( int i = 0; i < DBFGetFieldCount(myHDBF); i++ )
  {
    DBFFieldType eType;
    eType = DBFGetFieldInfo( myHDBF, i, chField, &nWidth, &nDecimals );
    if( eType == FTString )
      FT = DBF_FieldType_String;
    else if( eType == FTInteger )
      FT = DBF_FieldType_Integer;
    else if( eType == FTDouble )
      FT = DBF_FieldType_Double;
    else if( eType == FTInvalid )
      FT = DBF_FieldType_Invalid;

    FTVect.push_back(FT);
  }

}

int HYDROData_ShapeFile::DBF_GetNbRecords()
{
  if(myHDBF == NULL)
    return 0;
  return DBFGetRecordCount(myHDBF);
}

void HYDROData_ShapeFile::DBF_GetAttributeList(int theIndexOfField, std::vector<DBF_AttrValue>& theAttrV)
{
  int nWidth, nDecimals;
  char chField[12];

  for( int i = 0; i < DBFGetRecordCount(myHDBF); i++ )
  {
    DBFFieldType eType;
    DBF_AttrValue anAttr;
    eType = DBFGetFieldInfo( myHDBF, theIndexOfField, chField, &nWidth, &nDecimals );

    if( DBFIsAttributeNULL( myHDBF, i, theIndexOfField ) )
    {
      anAttr.myIsNull = true;
      DBF_FieldType FT = DBF_FieldType_None;
      if( eType == FTString )
        FT = DBF_FieldType_String;
      else if( eType == FTInteger )
        FT = DBF_FieldType_Integer;
      else if( eType == FTDouble )
        FT = DBF_FieldType_Double;
      else if( eType == FTInvalid )
        FT = DBF_FieldType_Invalid;
      anAttr.myFieldType = FT;
    }
    else
    {
      switch( eType )
      {
        case FTString:
        {
          const char* chAttr = DBFReadStringAttribute( myHDBF, i, theIndexOfField );
          anAttr.myIsNull = false;
          anAttr.myFieldType = DBF_FieldType_String;
          anAttr.myRawValue = chAttr;
          anAttr.myStrVal = QString(chAttr);
          break;
        }

        case FTInteger:
        {
          int iAttr = DBFReadIntegerAttribute( myHDBF, i, theIndexOfField );
          anAttr.myIsNull = false;
          anAttr.myFieldType = DBF_FieldType_Integer;
          anAttr.myRawValue = DBFReadStringAttribute( myHDBF, i, theIndexOfField );
          anAttr.myIntVal = iAttr;
          break;
        }

        case FTDouble:
        {
          double dAttr = DBFReadDoubleAttribute( myHDBF, i, theIndexOfField );
          anAttr.myIsNull = false;
          anAttr.myFieldType = DBF_FieldType_Double;
          anAttr.myRawValue = DBFReadStringAttribute( myHDBF, i, theIndexOfField );
          anAttr.myDoubleVal = dAttr;
          break;
        }
        default:
          break;
      }
    }
    theAttrV.push_back(anAttr);
  }

}

bool HYDROData_ShapeFile::DBF_WriteFieldAndValues(const QString& theFileName, const QString& theFieldName, DBF_FieldType theType, const std::vector<DBF_AttrValue>& theAttrV, bool bUseRawValue)
{
  // Check that given field type is equal to field types of attributes values
  for (size_t i = 0; i < theAttrV.size(); i++)
  {
    if (theAttrV[i].myFieldType != theType)
      return false;
  }

  DBFHandle	hDBF;
  hDBF = DBFCreate( theFileName.toStdString().c_str() );
  if( hDBF == NULL )
	  return false;

  if (theType != DBF_FieldType_String && theType != DBF_FieldType_Integer && theType != DBF_FieldType_Double)
  {
    DBFClose( hDBF );
    return false; //cant handle any other cases
  }

  int nWidth = 20;
  switch( theType )
  {
    case DBF_FieldType_String:
    {
      DBFAddField (hDBF, theFieldName.toStdString().c_str(), FTString, nWidth, 0);
      break;
    }

    case DBF_FieldType_Integer:
    {
      DBFAddField (hDBF, theFieldName.toStdString().c_str(), FTInteger, nWidth, 0);
      break;
    }

    case DBF_FieldType_Double:
    {
      DBFAddField (hDBF, theFieldName.toStdString().c_str(), FTDouble, nWidth, 0);
      break;
    }
    default:
      break;
  }
  DEBTRACE("theAttrV.size() " << theAttrV.size());

  if (DBFGetFieldCount( hDBF ) != 1)
  {
    DBFClose( hDBF );
    return false;
  }
  if (DBFGetRecordCount( hDBF ) != 0)
  {
    DBFClose( hDBF );
    return false;
  }
  int stat = -1;

  if (bUseRawValue)
  {
    for (size_t i = 0; i < theAttrV.size(); i++)
    {
      DEBTRACE("  rawValue");
      if (!theAttrV[i].myIsNull)
        stat = DBFWriteStringAttribute(hDBF, (int)i, 0, theAttrV[i].myRawValue.c_str());
      else
        stat = DBFWriteNULLAttribute(hDBF, (int)i, 0 );

      if (stat != 1)
      {
        DBFClose( hDBF );
        return false;
      }
    }
  }
  else
  {
    for (size_t i = 0; i < theAttrV.size(); i++)
    {
      DEBTRACE("  typeValue " << theAttrV[i].myIsNull);
      if (!theAttrV[i].myIsNull)
        switch( theType )
        {
          case DBF_FieldType_String:
          {
        	DEBTRACE("  write dbf " << i << " " << theAttrV[i].myStrVal.toStdString());
            stat = DBFWriteStringAttribute(hDBF, (int)i, 0, theAttrV[i].myStrVal.toStdString().c_str());
            break;
          }

          case DBF_FieldType_Integer:
          {
            stat = DBFWriteIntegerAttribute(hDBF, (int)i, 0, theAttrV[i].myIntVal);
            break;
          }

          case DBF_FieldType_Double:
          {
            stat = DBFWriteDoubleAttribute(hDBF, (int)i, 0, theAttrV[i].myDoubleVal);
            break;
          }
          default:
            break;
        }
      else
        stat = DBFWriteNULLAttribute(hDBF, (int)i, 0 );
      DEBTRACE("  stat " << stat);
      if (stat != 1)
      {
        DBFClose( hDBF );
        return false;
      }
    }
  }

  DBFClose( hDBF );
  return true;

}


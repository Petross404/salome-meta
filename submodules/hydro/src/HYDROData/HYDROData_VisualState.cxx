// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_VisualState.h>

#include <TDataStd_AsciiString.hxx>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_VisualState, HYDROData_Entity)

HYDROData_VisualState::HYDROData_VisualState()
: HYDROData_Entity( Geom_No )
{
}

HYDROData_VisualState::~HYDROData_VisualState()
{
}

void HYDROData_VisualState::SetState( const std::string& theState )
{
  TCollection_AsciiString aString;
  Handle(TDataStd_AsciiString) aState;
  if( !myLab.FindAttribute( TDataStd_AsciiString::GetID(), aState ) )
  {
    aState = TDataStd_AsciiString::Set( myLab, aString );
    aState->SetID(TDataStd_AsciiString::GetID());
  }
  aString.AssignCat( theState.c_str() );
  aState->Set( aString );
}

std::string HYDROData_VisualState::GetState() const
{
  Handle(TDataStd_AsciiString) aState;
  if( !myLab.FindAttribute( TDataStd_AsciiString::GetID(), aState ) )
    return std::string();
  return std::string( aState->Get().ToCString() );
}

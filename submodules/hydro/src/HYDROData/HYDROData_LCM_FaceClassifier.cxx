// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_LCM_FaceClassifier.h"

#include <HYDROData_LandCoverMap.h>
#include <HYDROData_Tool.h>

#include <Bnd_Box2d.hxx>
#include <BRepTools.hxx>
#include <BRepBndLib.hxx>
#include <Bnd_Box.hxx>
#include <NCollection_UBTree.hxx>
#include <NCollection_UBTreeFiller.hxx>
#include <BRepTopAdaptor_FClass2d.hxx>
#include <TopoDS.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <Geom_Plane.hxx>
#include <BRep_Tool.hxx>
#include <Geom_Surface.hxx>
#include <ElSLib.hxx>


Standard_Boolean HYDROData_FaceClassifier_BndBoxTreeSelector::Accept (const Standard_Integer& theObj)
{
  if (theObj > myMapF2Class2d.Extent())
    return Standard_False;

  const TopoDS_Face& f = TopoDS::Face(myMapF2Class2d.FindKey(theObj));
  if(f.IsNull())
    return Standard_False;

  BRepTopAdaptor_FClass2d* class2d = myMapF2Class2d.FindFromKey(f);

  Handle(Geom_Plane) Pl = HYDROData_LCM_FaceClassifier::GetPlane(f);
  Standard_Real u, v;
  ElSLib::Parameters(Pl->Pln(), gp_Pnt(myP.X(), myP.Y(), 0.0), u, v);
  TopAbs_State aState = class2d->Perform( gp_Pnt2d(u, v), Standard_False );

  if (aState == TopAbs_IN)
  {
    myResFaces.Append(f);
    myStop = 1; //no more faces includes this point; quit
    return Standard_True;
  }
  else if (aState == TopAbs_ON)
  {
    myResFaces.Append(f);
    return Standard_True;
  }

  return Standard_False;

}

Handle(Geom_Plane) HYDROData_LCM_FaceClassifier::GetPlane(const TopoDS_Face& F)
{
  TopLoc_Location L;
  Handle(Geom_Surface) S = BRep_Tool::Surface(F, L);
  Handle(Geom_Plane) Pl = Handle(Geom_Plane)::DownCast(S->Transformed(L.Transformation()));
  return Pl;
}

void HYDROData_LCM_FaceClassifier::Classify( const std::vector<gp_XY>& thePoints,
                                             std::vector<std::set <QString> >& theTypes,
                                             std::vector<NCollection_Map<TopoDS_Face> >* theFaces) const
{
  HYDROData_LandCoverMap::Explorer anIt( *myLCM );
  HYDROData_MapOfFaceToStricklerType aMapF2ST;
  TopTools_IndexedMapOfShape aFaces;
  for( ; anIt.More(); anIt.Next() )
  {
    const TopoDS_Face& F = anIt.Face();
    aMapF2ST.Add(F, anIt.StricklerType());
    aFaces.Add(F);
  }

  HYDROData_FaceClassifier_BndBoxTree aTree;
  NCollection_UBTreeFiller <Standard_Integer, Bnd_Box2d> aTreeFiller (aTree);
  NCollection_IndexedDataMap<TopoDS_Face, BRepTopAdaptor_FClass2d*> aMapF2Class2d;

  int NbF = aFaces.Extent();
  std::vector<BRepTopAdaptor_FClass2d*> fclass2dpointers;
  fclass2dpointers.reserve(NbF);

  for (int i = 1; i <= NbF; i++)
  {
    Bnd_Box b3d;
    const TopoDS_Face& F = TopoDS::Face(aFaces(i));
    BRepBndLib::Add(F, b3d);
    Bnd_Box2d NB;
    NB.Update(b3d.CornerMin().X(), b3d.CornerMin().Y(), b3d.CornerMax().X(), b3d.CornerMax().Y() );
    aTreeFiller.Add(i, NB);
    BRepTopAdaptor_FClass2d* aClass2d = new BRepTopAdaptor_FClass2d( F, 1E-7 );
    aMapF2Class2d.Add(F, aClass2d);
    fclass2dpointers.push_back(aClass2d);
  }

  aTreeFiller.Fill();

  size_t pntsize = thePoints.size();
  theTypes.reserve(pntsize);
  if (theFaces) {
    theFaces->resize(pntsize);
  }

  Standard_Integer aSel = 0;
  for (size_t i = 0; i < pntsize; i++)
  {
    std::set<QString> aSet;

    HYDROData_FaceClassifier_BndBoxTreeSelector aSelector(aMapF2Class2d);
    const gp_Pnt2d& pnt2d = thePoints[i];
    aSelector.SetCurrentPoint(pnt2d);
    aSel = aTree.Select(aSelector);
    if (aSel > 0)
    {
      const NCollection_List<TopoDS_Face>& rf = aSelector.GetResFaces();
      NCollection_List<TopoDS_Face>::Iterator it(rf);
      for (;it.More();it.Next())
      {
        const TopoDS_Face& f = it.Value();
        QString aST = aMapF2ST.FindFromKey(f);
        aSet.insert(aST);
        if (theFaces)
          (*theFaces)[i].Add(f);
      }
    }

    theTypes.push_back(aSet);
  }

  for (size_t i = 0; i < fclass2dpointers.size(); i++)
    delete fclass2dpointers[i];
}



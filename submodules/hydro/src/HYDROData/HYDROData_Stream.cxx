// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Stream.h"

#include "HYDROData_Document.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_Polyline3D.h"
#include "HYDROData_Profile.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_ShapesTool.h"
#include "HYDROData_IAltitudeObject.h"
#include "HYDROData_IProfilesInterpolator.h"
#include "HYDROData_Tool.h"
#include "HYDROData_DTM.h"
#include "HYDROData_LISM.h"

#include <HYDROData_Bathymetry.h>

#include <TDataStd_RealArray.hxx>

#include <Precision.hxx>

#include <NCollection_DataMap.hxx>

#include <TColStd_Array1OfReal.hxx>
#include <TColStd_ListOfReal.hxx>
#include <TColStd_ListIteratorOfListOfReal.hxx>
#include <TColgp_Array1OfPnt.hxx>
#include <TColgp_HArray1OfPnt.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Shell.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopExp.hxx>
#include <TopExp_Explorer.hxx>

#include <Bnd_Box.hxx>

#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>

#include <BRepBndLib.hxx>
#include <BRepProj_Projection.hxx>
#include <BRepExtrema_ExtCC.hxx>
#include <BRepCheck_Analyzer.hxx>

#include <gp.hxx>
#include <gp_Ax1.hxx>
#include <gp_Ax2.hxx>
#include <gp_Ax3.hxx>
#include <gp_Vec.hxx>
#include <gp_Pnt.hxx>
#include <gp_Pln.hxx>

#include <GeomAPI_Interpolate.hxx>
#include <Geom_BSplineCurve.hxx>

#include <TopTools_HArray1OfShape.hxx>
#include <TopTools_IndexedMapOfOrientedShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <TopTools_IndexedMapOfShape.hxx>

#include <QColor>
#include <QStringList>
#include <QVector>

//#define DEB_STREAM 1
#ifdef DEB_STREAM
//#define DEB_HASINT 1
//#define DEB_UPDATE 1
#include <BRepTools.hxx>
#include <TCollection_AsciiString.hxx>
#endif

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

#include <assert.h>

typedef NCollection_DataMap<Standard_Real, Handle(HYDROData_Profile)> HYDROData_DataMapOfRealOfHDProfile;

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Stream,HYDROData_NaturalObject)


HYDROData_Stream::HYDROData_Stream()
: HYDROData_NaturalObject( Geom_3d )
{
}

HYDROData_Stream::~HYDROData_Stream()
{
}

QStringList HYDROData_Stream::DumpToPython( const QString&       thePyScriptPath,
                                            MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aName = GetObjPyName();

  int interpMethod = GetInterpolationMethod();
  QString anInterpMethod = QString::number( interpMethod );
  aResList << QString( "%1.SetInterpolationMethod( %2 )" ).arg( aName ).arg( anInterpMethod );

  Handle(HYDROData_PolylineXY) aHydAxis = GetHydraulicAxis();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aHydAxis, "SetHydraulicAxis" );

  HYDROData_SequenceOfObjects aSeqOfProfiles = GetProfiles();
  for ( int i = 1, aNb = aSeqOfProfiles.Size(); i <= aNb; ++i )
  {
    const Handle(HYDROData_Entity) aProfile = aSeqOfProfiles.Value( i );
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aProfile, "AddProfile" );
  }

  // Set bottom polyline if exists
  const Handle(HYDROData_Polyline3D) aBottomPolyline = GetBottomPolyline();
  if ( !aBottomPolyline.IsNull() ) {
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aBottomPolyline, "SetBottomPolyline" );
  }

  if (interpMethod==0)
  {
    QString aDDZs = QString::number( GetDDZ(), 'f', 3 );
    QString aSSteps = QString::number( GetSpatialStep(), 'f', 3 );
    aResList << QString( "%1.SetDDZ( %2 )" ).arg( aName ).arg( aDDZs );
    aResList << QString( "%1.SetSpatialStep( %2 )" ).arg( aName ).arg( aSSteps );
  }
  else if (interpMethod==1)
  {
    Handle(HYDROData_PolylineXY) aLeftBank = GetLeftBank();
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aLeftBank, "SetLeftBank" );
    Handle(HYDROData_PolylineXY) aRightBank = GetRightBank();
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRightBank, "SetRightBank" );

    QString aHaxStep = QString::number( GetHaxStep(), 'f', 3 );
    QString aNbProfilePoints = QString::number( GetNbProfilePoints() );
    aResList << QString( "%1.SetHaxStep( %2 )" ).arg( aName ).arg( aHaxStep );
    aResList << QString( "%1.SetNbProfilePoints( %2 )" ).arg( aName ).arg( aNbProfilePoints );
  }

  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aName );
  aResList << QString( "" );

  return aResList;
}

HYDROData_SequenceOfObjects HYDROData_Stream::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Object::GetAllReferenceObjects();

  Handle(HYDROData_PolylineXY) aHydAxis = GetHydraulicAxis();
  if ( !aHydAxis.IsNull() )
    aResSeq.Append( aHydAxis );

  HYDROData_SequenceOfObjects aSeqOfProfiles = GetProfiles();
  aResSeq.Append( aSeqOfProfiles );

  return aResSeq;
}

Handle(Geom_BSplineCurve) HYDROData_Stream::buildInterpolationCurve( 
  const Handle(TColgp_HArray1OfPnt)& theArrayOfPnt )
{
  Handle(Geom_BSplineCurve) aBSpline;
  GeomAPI_Interpolate anInterpolator (theArrayOfPnt, Standard_False, 1.0e-5); 
  anInterpolator.Perform() ;
  if (anInterpolator.IsDone()) 
    aBSpline = anInterpolator.Curve();
  return aBSpline; 
}

void HYDROData_Stream::GetWarnings(NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings)
{
  warnings = myWarnings;
}  

void HYDROData_Stream::Update()
{
  if (GetInterpolationMethod() == 0)
  {
    if (!GetHydraulicAxis().IsNull())
      updateProfilesOrder();

    // Update bottom polyline if exists
    const Handle(HYDROData_Polyline3D) aBottomPolyline = GetBottomPolyline();
    if ( !aBottomPolyline.IsNull() ) {
      if ( GenerateBottomPolyline() ) {
        Handle(HYDROData_PolylineXY) aPolylineXY = aBottomPolyline->GetPolylineXY();
        if ( !aPolylineXY.IsNull() ) {
          aPolylineXY->Update();
        }
        aBottomPolyline->Update();
      }
    }

    Handle(HYDROData_DTM) dtm = DTM();
    dtm->Update();
    UpdatePrs( dtm );

    myWarnings.Clear();
    dtm->GetWarnings(myWarnings);
  }
  else
  {
    Handle(HYDROData_LISM) lism = LISM();
    lism->Update();
    UpdatePrs( lism );

    myWarnings.Clear();
    //lism->GetWarnings(myWarnings);
    //TODO warnings
  }

  HYDROData_NaturalObject::Update();
}

bool HYDROData_Stream::IsHas2dPrs() const
{
  return true;
}

bool HYDROData_Stream::CreatePresentations( const Handle(HYDROData_DTM)& theDTM,
                                            PrsDefinition&              thePrs )
{
  if ( theDTM.IsNull() )
    return false;

  HYDROData_SequenceOfObjects profiles = theDTM->GetProfiles();
  if( profiles.Length() < 2 )
    return false;

  TopoDS_Shape Out3dPres;
  TopoDS_Shape Out2dPres;
  TopoDS_Shape OutLeftB;
  TopoDS_Shape OutRightB;
  TopoDS_Shape OutInlet;
  TopoDS_Shape OutOutlet;

  theDTM->GetPresentationShapes(Out3dPres, Out2dPres, OutLeftB, OutRightB, OutInlet, OutOutlet);

  thePrs.myInlet = OutInlet;
  thePrs.myOutlet = OutOutlet;
  thePrs.myLeftBank = OutLeftB;
  thePrs.myRightBank = OutRightB;
  thePrs.myPrs2D = Out2dPres;
  thePrs.myPrs3D = Out3dPres;
  /*std::vector<TopoDS_Wire> profiles3d;
  profiles3d.reserve(profiles.Length());

  // Pre-processing
  HYDROData_SequenceOfObjects::Iterator anIter( profiles );
  for (int i=1 ; anIter.More(); anIter.Next(),i++ )
  {
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( anIter.Value() );

    if ( aProfile.IsNull() )
      continue;

    const TopoDS_Shape& aProfileShape = aProfile->GetShape3D();
    //TopExp_Explorer exp(aProfileShape, TopAbs_EDGE);
    profiles3d.push_back( TopoDS::Wire(aProfileShape) );
  }*/

  return true;
}

void HYDROData_Stream::internalUpdatePrs( const PrsDefinition& aResultPrs )
{
  SetShape3D( aResultPrs.myPrs3D );
  SetTopShape( aResultPrs.myPrs2D );

  // Create the stream groups
  QString aLeftGroupName = GetName() + "_Left_Bank";

  Handle(HYDROData_ShapesGroup) aLeftGroup = createGroupObject();
  aLeftGroup->SetName( aLeftGroupName );
  aLeftGroup->AddShape( aResultPrs.myLeftBank );

  QString aRightGroupName = GetName() + "_Right_Bank";

  Handle(HYDROData_ShapesGroup) aRightGroup = createGroupObject();
  aRightGroup->SetName( aRightGroupName );
  aRightGroup->AddShape( aResultPrs.myRightBank );

  QString anInGroupName = GetName() + "_Inlet";

  Handle(HYDROData_ShapesGroup) anInGroup = createGroupObject();
  anInGroup->SetName( anInGroupName );
  anInGroup->AddShape( aResultPrs.myInlet );

  QString anOutGroupName = GetName() + "_Outlet";
  
  Handle(HYDROData_ShapesGroup) anOutGroup = createGroupObject();
  anOutGroup->SetName( anOutGroupName );
  anOutGroup->AddShape( aResultPrs.myOutlet );
}


void HYDROData_Stream::UpdatePrs( const Handle(HYDROData_DTM)& theDTM )
{
  HYDROData_NaturalObject::Update();
  
  PrsDefinition aResultPrs;
  if ( !CreatePresentations( theDTM, aResultPrs ) )
    return;

  internalUpdatePrs(aResultPrs);
}

void HYDROData_Stream::UpdatePrs( const Handle(HYDROData_LISM)& theLISM )
{
  HYDROData_NaturalObject::Update();
  PrsDefinition prsDef;
  theLISM->GetShapePresentations(prsDef);

  internalUpdatePrs(prsDef);
}

QColor HYDROData_Stream::DefaultFillingColor() const
{
  return QColor( Qt::green );
}

QColor HYDROData_Stream::DefaultBorderColor() const
{
  return QColor( Qt::transparent );
}

bool HYDROData_Stream::IsValidAsAxis( const Handle(HYDROData_PolylineXY)& theHydAxis )
{
  if ( theHydAxis.IsNull() )
    return true;

  TopoDS_Shape aHydraulicShape = theHydAxis->GetShape();
  if ( aHydraulicShape.IsNull() || 
       aHydraulicShape.ShapeType() != TopAbs_WIRE ||
       BRep_Tool::IsClosed( aHydraulicShape ) )
    return false; // The polyline must be a single not closed wire

  return true;
}

TopoDS_Shape HYDROData_Stream::GetLeftShape() const
{
  HYDROData_SequenceOfObjects aGroups = GetGroups();
  return HYDROData_Tool::getFirstShapeFromGroup( aGroups, 1);
}

TopoDS_Shape HYDROData_Stream::GetRightShape() const
{
  HYDROData_SequenceOfObjects aGroups = GetGroups();
  return HYDROData_Tool::getFirstShapeFromGroup( aGroups, 2);
}

TopoDS_Shape HYDROData_Stream::GetInletShape() const
{
  HYDROData_SequenceOfObjects aGroups = GetGroups();
  return HYDROData_Tool::getFirstShapeFromGroup( aGroups, 3);
}

TopoDS_Shape HYDROData_Stream::GetOutletShape() const
{
  HYDROData_SequenceOfObjects aGroups = GetGroups();
  return HYDROData_Tool::getFirstShapeFromGroup( aGroups, 4);
}

QString HYDROData_Stream::GetBathyName()
{
  QString name;
  ObjectKind ok = getAltitudeObjectType();
  if (ok == KIND_DTM)
  {
    Handle(HYDROData_DTM) dtm = DTM();
    if (!dtm.IsNull())
      name = dtm->GetName();
  }
  else if (ok == KIND_LISM)
  {
    Handle(HYDROData_LISM) lism = LISM();
    if (!lism.IsNull())
      name = lism->GetName();
  }
  return name;
}

Handle(HYDROData_DTM) HYDROData_Stream::DTM() const
{
  const_cast<HYDROData_Stream*>( this )->checkAndSetAltitudeObject();
  return Handle(HYDROData_DTM)::DownCast( GetAltitudeObject() );
}

Handle(HYDROData_LISM) HYDROData_Stream::LISM() const
{
  const_cast<HYDROData_Stream*>( this )->checkAndSetAltitudeObject();
  return Handle(HYDROData_LISM)::DownCast( GetAltitudeObject() );
}

double HYDROData_Stream::GetDDZ() const
{
  return DTM()->GetDDZ();
}

void HYDROData_Stream::SetDDZ( double theDDZ )
{
  DTM()->SetDDZ( theDDZ );
  Changed( Geom_3d );
}

Handle(HYDROData_PolylineXY) HYDROData_Stream::GetLeftBank() const
{
  return LISM()->GetLeftBank();
}

void HYDROData_Stream::SetLeftBank( const Handle(HYDROData_PolylineXY)& theBank )
{
  LISM()->SetLeftBank( theBank );
  Changed( Geom_3d );
}

Handle(HYDROData_PolylineXY) HYDROData_Stream::GetRightBank() const
{
  return LISM()->GetRightBank();
}

void HYDROData_Stream::SetRightBank( const Handle(HYDROData_PolylineXY)& theBank )
{
  LISM()->SetRightBank( theBank );
  Changed( Geom_3d );
}
  
  
double HYDROData_Stream::GetHaxStep() const
{
  return LISM()->GetHaxStep();
}

void HYDROData_Stream::SetHaxStep( double theHaxStep )
{
  LISM()->SetHaxStep( theHaxStep );
  Changed( Geom_3d );
}

int HYDROData_Stream::GetNbProfilePoints() const
{
  return LISM()->GetNbProfilePoints();
}

void HYDROData_Stream::SetNbProfilePoints( int theNbPoints )
{
  LISM()->SetNbProfilePoints( theNbPoints );
  Changed( Geom_3d );
}
  
double HYDROData_Stream::GetSpatialStep() const
{
  if (GetInterpolationMethod() == 0)
    return DTM()->GetSpatialStep();
  else
    return LISM()->GetHaxStep();
}

void HYDROData_Stream::SetSpatialStep( double theSpatialStep )
{
  if (GetInterpolationMethod() == 0 )  
    DTM()->SetSpatialStep( theSpatialStep );
  else
    LISM()->SetHaxStep( theSpatialStep );
  Changed( Geom_3d );
}

bool HYDROData_Stream::SetHydraulicAxis( const Handle(HYDROData_PolylineXY)& theAxis )
{
  if (GetInterpolationMethod() == 0)
  {
    if ( !IsValidAsAxis( theAxis ) )
      return false;

    Handle(HYDROData_PolylineXY) aPrevAxis = GetHydraulicAxis();
    if ( IsEqual( aPrevAxis, theAxis ) )
      return true;

    SetReferenceObject( theAxis, DataTag_HydraulicAxis );

    // Update the order of profiles
    updateProfilesOrder();

    // Indicate model of the need to update the stream presentation
    Changed( Geom_3d );
  }
  else
  {
    LISM()->SetHydraulicAxis( theAxis );
    Changed( Geom_3d );
  }

  return true;
}

Handle(HYDROData_PolylineXY) HYDROData_Stream::GetHydraulicAxis() const
{
  if (GetInterpolationMethod() == 0)
    return Handle(HYDROData_PolylineXY)::DownCast( GetReferenceObject( DataTag_HydraulicAxis ) );
  else
    return LISM()->GetHydraulicAxis();
}

void HYDROData_Stream::RemoveHydraulicAxis()
{
  Handle(HYDROData_PolylineXY) aPrevAxis = GetHydraulicAxis();
  if ( aPrevAxis.IsNull() )
    return;

  ClearReferenceObjects( DataTag_HydraulicAxis );

  // We remove the reference profiles
  RemoveProfiles();

  // Indicate model of the need to update the stream presentation
  Changed( Geom_3d );
}

bool HYDROData_Stream::HasIntersection( const Handle(HYDROData_Profile)& theProfile,
                                        const TopoDS_Face&               thePlane,
                                        Standard_Real&                   theOutPar ) const
{
  Handle(HYDROData_PolylineXY) aHydAxis = GetHydraulicAxis();
  return HasIntersection( aHydAxis, theProfile, thePlane, theOutPar );
}

#include <BRepAlgo_NormalProjection.hxx>

bool HYDROData_Stream::HasIntersection( const Handle(HYDROData_PolylineXY)& theHydAxis, 
                                        const Handle(HYDROData_Profile)&    theProfile, 
                                        const TopoDS_Face&                  thePlane,
                                        Standard_Real&                      theOutPar )
{
  if ( theProfile.IsNull() /*|| !IsValidAsAxis( theHydAxis )*/ )
    return false; 

  if (theHydAxis.IsNull())
    return true; //empty h_axis; its's OK

  TopoDS_Wire aHydraulicWire = TopoDS::Wire( theHydAxis->GetShape() ); //guide line
  TopoDS_Wire aProfileWire = TopoDS::Wire( theProfile->GetTopShape() );
  if ( aProfileWire.IsNull() )
    {
      DEBTRACE("aProfileWire.IsNull");
      return false;
    }

  //BRepProj_Projection aProjector (aProfileWire, thePlane, gp::OZ().Direction());
  BRepAlgo_NormalProjection nproj(thePlane);
  nproj.Add(aProfileWire);
  nproj.SetDefaultParams();
  nproj.Build();
  if(!nproj.IsDone())
    {
      DEBTRACE("!nproj.IsDone");
      return false;
    }
  TopoDS_Shape aPrjProfile = nproj.Projection();
  if(aPrjProfile.IsNull())
    {
      DEBTRACE("aPrjProfile.IsNull");
      return false;
    }
  TopoDS_Vertex aV1, aV2;
  if(aPrjProfile.ShapeType() == TopAbs_EDGE)
    TopExp::Vertices(TopoDS::Edge(aPrjProfile), aV1, aV2);
  else if(aPrjProfile.ShapeType() == TopAbs_WIRE)  
    TopExp::Vertices(TopoDS::Wire(aPrjProfile), aV1, aV2);
  else if(aPrjProfile.ShapeType() == TopAbs_COMPOUND){
    TopExp_Explorer anExp(aPrjProfile, TopAbs_WIRE);
    if(anExp.More()) {
      TopExp::Vertices(TopoDS::Wire(anExp.Current()), aV1, aV2);
    } else {
      anExp.Init(aPrjProfile, TopAbs_EDGE);
      if(anExp.More()) {
        TopExp::Vertices(TopoDS::Edge(anExp.Current()), aV1, aV2);
      }
    }
  }
  if(aV1.IsNull() || aV2.IsNull())
    {
      DEBTRACE("aV1.IsNull() || aV2.IsNull()");
      return false;
    }
  gp_Pnt aPnt1 = BRep_Tool::Pnt(aV1);
  gp_Pnt aPnt2 = BRep_Tool::Pnt(aV2);
  aPnt1.SetZ(0.0);
  aPnt2.SetZ(0.0);
  BRepBuilderAPI_MakeEdge aMk(aPnt1, aPnt2); 
  if(!aMk.IsDone())
    {
      DEBTRACE("!aMk.IsDone()");
      return false;
    }
  const TopoDS_Edge& anEdg2 = aMk.Edge();//Section edge
  Standard_Integer aNum(0);
  
  TopExp_Explorer anExplo(aHydraulicWire, TopAbs_EDGE);
  for(;anExplo.More();anExplo.Next()) aNum++;
  // check for self-intersection
  const Standard_Real SquareTolerance = Precision::Confusion()*Precision::Confusion();
  Standard_Boolean hasInt(false);
  Standard_Real aSqDist(DBL_MAX);
  Standard_Integer anIndx(0);
  BRepExtrema_ExtCC aCC;
  aCC.Initialize(anEdg2);
  theOutPar = 0.0;
  anExplo.Init(aHydraulicWire, TopAbs_EDGE);
  for(Standard_Integer j=1;anExplo.More();anExplo.Next(),j++) {
    const TopoDS_Edge& anEdg1 = TopoDS::Edge(anExplo.Current());
    if(anEdg1.IsNull())
      continue;
    Standard_Boolean hasSol(false);
    aCC.Perform(anEdg1);
    if(aCC.IsDone()) {
    // find minimal dist
    for(Standard_Integer i=1; i<= aCC.NbExt();i++)
      if(aCC.SquareDistance(i) < aSqDist) {
        aSqDist = aCC.SquareDistance(i);
        anIndx = i;
        hasSol = true;
      }  
    }
    if(hasSol) {
      if(aSqDist <= SquareTolerance) { // hasInt
        const gp_Pnt& aPnt = aCC.PointOnE1(anIndx);
        if(aNum > 1) {
          TopExp::Vertices(anEdg1, aV1, aV2, Standard_True);
          theOutPar += BRep_Tool::Pnt(aV1).Distance(aPnt);
        } else {
          Standard_Real aPar = aCC.ParameterOnE1(anIndx);
          theOutPar = aPar;
        }
        hasInt = true;
        break;
      } else {
          // no ints-n
        if(aNum > 1) {
          TopExp::Vertices(anEdg1, aV1, aV2);
          theOutPar += BRep_Tool::Pnt(aV1).Distance(BRep_Tool::Pnt(aV2));
        }
      }
    } else if(aNum > 1) {
      TopExp::Vertices(anEdg1, aV1, aV2);
      theOutPar += BRep_Tool::Pnt(aV1).Distance(BRep_Tool::Pnt(aV2));
    }
  }
  if(hasInt)
    return true;
  DEBTRACE("!hasInt " << aPnt1.X() << " " << aPnt1.Y() << " " << aPnt2.X() << " " << aPnt2.Y() << " --- " << aSqDist);
  return false;
}

bool HYDROData_Stream::AddProfile( const Handle(HYDROData_Profile)& theProfile )
{
  if ( theProfile.IsNull() )
    return false;

 // Handle(HYDROData_PolylineXY) aHydAxis = GetHydraulicAxis();
 // if ( aHydAxis.IsNull() )
 //   return false;

  TopoDS_Face aPlane;
  BuildRefFace( aPlane );

  Standard_Real aPar(.0);
  if ( HasReference( theProfile, DataTag_Profile ) || !HasIntersection( theProfile, aPlane, aPar ) )
    return false; // Object is already in reference list or it has no intersection
  //DEBTRACE("AddProfile - insertParameter " << aPar);
  int aProfileIndex = insertParameter( aPar );
  insertProfileInToOrder( theProfile, aProfileIndex );

  if (GetInterpolationMethod()==0)
    DTM()->SetProfiles( GetProfiles() );
  else
    LISM()->SetProfiles( GetProfiles() );
  
  // Indicate model of the need to update the stream presentation
  Changed( Geom_3d );

  return true;
}

bool HYDROData_Stream::SetProfiles( const HYDROData_SequenceOfObjects& theProfiles,
                                    const bool&                        theIsToOrder )
{
  DEBTRACE(" --- SetProfiles " <<theIsToOrder );
  if ( theIsToOrder )
  {
    for ( int i = 1; i <= theProfiles.Length(); ++i )
    {
      Handle(HYDROData_Profile) aProfile = 
        Handle(HYDROData_Profile)::DownCast( theProfiles.Value( i ) );
      if ( aProfile.IsNull() )
        continue;

      if ( !AddProfile( aProfile ) )
      {
        DTM()->SetProfiles( HYDROData_SequenceOfObjects() );
        return false;
      }
    }
  }
  else // Just store the sequence of objects as is
  {
    bool anIsToUpdate = true;

    HYDROData_SequenceOfObjects anOldProfiles = GetProfiles();
    if ( anOldProfiles.Length() == theProfiles.Length() )
    {
      anIsToUpdate = false;

      for ( int i = 1; i <= theProfiles.Length(); ++i )
      {
        Handle(HYDROData_Entity) anOldProfile = anOldProfiles.Value( i );
        Handle(HYDROData_Entity) aNewProfile = theProfiles.Value( i );
        if ( !IsEqual( anOldProfile, aNewProfile ) )
        {
          anIsToUpdate = true;
          break;
        }
      }
    }
    
    SetReferenceObjects( theProfiles, DataTag_Profile );

    if ( anIsToUpdate )
      Changed( Geom_3d );
  }

  if (GetInterpolationMethod()==0)
    DTM()->SetProfiles( GetProfiles() );
  else
    LISM()->SetProfiles( GetProfiles() );
  return true;
}

HYDROData_SequenceOfObjects HYDROData_Stream::GetProfiles() const
{
  return GetReferenceObjects( DataTag_Profile );
}

bool HYDROData_Stream::RemoveProfile( const Handle(HYDROData_Profile)& theProfile )
{
  if ( theProfile.IsNull() )
    return false;

  int aProfileIndex = -1;

  HYDROData_SequenceOfObjects aRefProfiles = GetProfiles();
  HYDROData_SequenceOfObjects::Iterator anIter( aRefProfiles );
  for ( int i = 0 ; anIter.More(); anIter.Next(), ++i )
  {
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( anIter.Value() );
    if ( aProfile.IsNull() )
      continue;

    if ( IsEqual( theProfile, aProfile ) )
    {
      aProfileIndex = i;
      break;
    }
  }

  if ( aProfileIndex == -1 )
    return false;

  RemoveReferenceObject( theProfile->Label(), DataTag_Profile );

  // Remove parameter for removed profile
  removeParameter( aProfileIndex );

  // Indicate model of the need to update the stream presentation
  Changed( Geom_3d );

  return true;
}

void HYDROData_Stream::RemoveProfiles()
{
  ClearReferenceObjects( DataTag_Profile );

  // Remove the parameters array
  removeParametersArray();

  // Indicate model of the need to update the stream presentation
  Changed( Geom_3d );
}


int HYDROData_Stream::GetInterpolationMethod() const
{
  return GetInteger( DataTag_InterpMethod );
}

void HYDROData_Stream::SetInterpolationMethod( int theMethod ) //if DTM => 0 ; if LISM => 1
{
  SetInteger( DataTag_InterpMethod, theMethod );
  Changed( Geom_3d );
}


void HYDROData_Stream::insertProfileInToOrder( const Handle(HYDROData_Profile)& theProfile,
                                               const int                        theBeforeIndex )
{
  //Handle(HYDROData_PolylineXY) aHydAxis = GetHydraulicAxis();
  if ( theProfile.IsNull() )
    return; 

  //TopoDS_Wire aHydraulicWire = TopoDS::Wire( aHydAxis->GetShape() );
  TopoDS_Wire aProfileWire = TopoDS::Wire( theProfile->GetTopShape() );
  if ( aProfileWire.IsNull() )
    return;

  if ( theBeforeIndex == -1 )
    AddReferenceObject( theProfile, DataTag_Profile );
  else
    InsertReferenceObject( theProfile, DataTag_Profile, theBeforeIndex );
}

void HYDROData_Stream::BuildRefFace( TopoDS_Face& thePlane )
{
  thePlane = BRepBuilderAPI_MakeFace(gp_Pln(gp_Pnt(0,0,0),gp_Dir(0,0,1))).Face();
}

void HYDROData_Stream::updateProfilesOrder()
{
  HYDROData_SequenceOfObjects aRefProfiles = GetProfiles();
  if ( aRefProfiles.IsEmpty() )
    return;

  // At first we remove all profiles from order
  RemoveProfiles();

  Handle(HYDROData_PolylineXY) aHydAxis = GetHydraulicAxis();
  if ( aHydAxis.IsNull() )
    return; 

  TopoDS_Face aPlane;
  BuildRefFace( aPlane );

  Standard_Real aPar( .0 );

#ifdef DEB_HASINT
  BRep_Builder aBB;
  TopoDS_Compound aCmp;
  aBB.MakeCompound(aCmp);
#endif

  HYDROData_DataMapOfRealOfHDProfile aDM;  
  TColStd_ListOfReal aList;
  HYDROData_SequenceOfObjects::Iterator anIter( aRefProfiles );
  for (int i = 1 ; anIter.More(); anIter.Next(), i++ )
  {
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( anIter.Value() );
#ifdef DEB_HASINT
  TopoDS_Wire aProfileWire = TopoDS::Wire( aProfile->GetTopShape() );
  aBB.Add( aCmp, aProfileWire ); 
#endif
    if ( aProfile.IsNull() || !HasIntersection( aProfile, aPlane, aPar ) )
      continue;
    
    aDM.Bind( aPar, aProfile );
    aList.Append( aPar );
  }
  
  if ( aList.IsEmpty() )
    return;

  QVector<double> anArr( aList.Extent() );

  TColStd_ListIteratorOfListOfReal it( aList );
  for ( int j = 1; it.More(); it.Next(), j++ )
    anArr[j-1] = it.Value();

  // sorting
  if ( aList.Extent() > 1 )
  {
    //TCollection_CompareOfReal Compar;
    //SortTools_QuickSortOfReal::Sort( anArr, Compar );
    std::sort( anArr.begin(), anArr.end() );

    for (int j = 1; j <= anArr.size(); j++) {
      const Standard_Real aKey =  anArr[j-1];
      const Handle(HYDROData_Profile)& aProfile = aDM.Find(aKey);
      insertProfileInToOrder( aProfile );
    }
  } else if ( aList.Extent() == 1 ) {
     const Standard_Real aKey = aList.Last();
     const Handle(HYDROData_Profile)& aProfile = aDM.Find(aKey);
     insertProfileInToOrder( aProfile );
  } 

  setParametersArray( anArr );

#ifdef DEB_HASINT
  TopoDS_Wire aHydraulicWire = TopoDS::Wire( aHydAxis->GetShape() );
  BRepTools::Write(aHydraulicWire, "Path.brep");
  BRepTools::Write(aCmp, "Prof.brep");
#endif
}

ObjectKind HYDROData_Stream::getAltitudeObjectType() const
{
  int InterpMethod = GetInterpolationMethod();
  if (InterpMethod == 1)
    return KIND_LISM;
  else
    return KIND_DTM;
}

void HYDROData_Stream::setParametersArray( const QVector<double>& theArray )
{
  if ( theArray.size() == 0 )
  {
    removeParametersArray();
    return;
  }

  TDF_Label aLabel = myLab.FindChild( DataTag_ParamsArray );
  
  int n = theArray.size();
  Handle(TDataStd_RealArray) aParamsArray = 
    TDataStd_RealArray::Set( aLabel, 1, n );
  aParamsArray->SetID(TDataStd_RealArray::GetID());
  for ( int i = 0; i < n; ++i )
  {
    const Standard_Real& aParam = theArray[i];
    aParamsArray->SetValue( i+1, aParam );
  }
}

TColStd_Array1OfReal* HYDROData_Stream::getParametersArray() const
{
  TColStd_Array1OfReal* anArray = NULL;

  TDF_Label aLabel = myLab.FindChild( DataTag_ParamsArray, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_RealArray) aParamsArray;
    if ( aLabel.FindAttribute( TDataStd_RealArray::GetID(), aParamsArray ) )
    {
      anArray = new TColStd_Array1OfReal( aParamsArray->Lower(), aParamsArray->Upper() );
      for ( int i = aParamsArray->Lower(), n = aParamsArray->Upper(); i <= n; ++i )
      {
        const Standard_Real& aParam = aParamsArray->Value( i );
        anArray->SetValue( i, aParam );
      }
    }
  }

  return anArray;
}

void HYDROData_Stream::removeParametersArray()
{
  TDF_Label aLabel = myLab.FindChild( DataTag_ParamsArray, false );
  if ( !aLabel.IsNull() )
    aLabel.ForgetAllAttributes();
}

int HYDROData_Stream::insertParameter( const Standard_Real& theParam )
{
  int aResIndex = -1;

  TColStd_Array1OfReal* anArr = getParametersArray();
  if ( anArr )
  {
    aResIndex = 0;

    QVector<double> aNewArr( anArr->Upper() +1 );
    bool isInserted = false;
    for ( int i = anArr->Lower(), j = i, n = anArr->Upper(); i <= n; ++i, ++j )
    {
      const Standard_Real& aStoredParam = anArr->Value( i );
      if ( !isInserted )
      {
        if ( theParam > aStoredParam )
        {
          aResIndex++;
        }
        else
        {
          if (j<=n+1)
            aNewArr[j-1] = theParam;
          isInserted = true;
          ++j;
        }
      }
      if (j<=n+1)
        aNewArr[j-1] = aStoredParam;
    }

    if ( !isInserted )
    {
      aResIndex = -1;
      aNewArr[aNewArr.size()-1] = theParam;
    }
    
    setParametersArray( aNewArr );
    delete anArr;
  }
  else
  {
    QVector<double> aNewArr( 1 );
    aNewArr[0] = theParam;
    setParametersArray( aNewArr );
  }

  return aResIndex;
}

void HYDROData_Stream::removeParameter( const int& theIndex )
{
  TDF_Label aLabel = myLab.FindChild( DataTag_ParamsArray, false );
  if ( aLabel.IsNull() )
    return;

  Handle(TDataStd_RealArray) aParamsArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), aParamsArray ) )
    return;

  if ( aParamsArray->Length() == 1 )
  {
    removeParametersArray();
    return;
  }

  QVector<double> aNewArr( aParamsArray->Upper() - 2 );

  for ( int i = aParamsArray->Lower(), j = i, k = 0, n = aParamsArray->Upper(); i <= n; ++i, ++k )
  {
    const Standard_Real& aStoredParam = aParamsArray->Value( i );
    if ( k == theIndex )
      continue;

    aNewArr[j-1] = aStoredParam;
    ++j;
  }

  setParametersArray( aNewArr );
}

bool HYDROData_Stream::GenerateBottomPolyline()
{
  // Get the document
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() ) {
    return false;
  }

  // Collect bottom points ( one bottom point from each profile of the stream )
  HYDROData_Profile::ProfilePoints aBottomPoints;
  
  HYDROData_SequenceOfObjects aSeqOfProfiles = GetProfiles();
  for ( int i = 1, aNb = aSeqOfProfiles.Size(); i <= aNb; i++ ) {
    const Handle(HYDROData_Profile) aProfile = 
      Handle(HYDROData_Profile)::DownCast( aSeqOfProfiles.Value( i ) );
    if ( aProfile.IsNull() ) {
      continue;
    }
    
    aBottomPoints.Append( aProfile->GetBottomPoint() );
  }

  int aNbBottomPoints = aBottomPoints.Size();

  if ( aNbBottomPoints < 2 ) {
    return false;
  }

  // Create bottom polyline object if the stream doesn't contain it yet
  Handle(HYDROData_Polyline3D) aBottom = GetBottomPolyline();
  if ( aBottom.IsNull() ) {
    aBottom = Handle(HYDROData_Polyline3D)::DownCast( aDocument->CreateObject( KIND_POLYLINE ) );  
    QString aBaseName = GetName() + "_bottom";
    QString aName = HYDROData_Tool::GenerateObjectName( aDocument, aBaseName, QStringList(), true );
    aBottom->SetName( aName );

    SetReferenceObject( aBottom, DataTag_BottomPolyline );
  }
  
  // Create 2D polyline if the bottom polyline doesn't contain it yet
  Handle(HYDROData_PolylineXY) aPolylineXY = aBottom->GetPolylineXY();
  if ( aPolylineXY.IsNull() ) {
    aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast( aDocument->CreateObject( KIND_POLYLINEXY ) );
    QString aBaseName = GetName() + "_bottom_2d";
    QString aName = HYDROData_Tool::GenerateObjectName( aDocument, aBaseName, QStringList(), true );
    aPolylineXY->SetName( aName );
    aBottom->SetPolylineXY( aPolylineXY, false );
  }

  aPolylineXY->RemoveSections();
  aPolylineXY->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, false );
  
  // Create profile if the bottom polyline doesn't contain it yet
  Handle(HYDROData_ProfileUZ) aProfileUZ = aBottom->GetProfileUZ();
  if ( aProfileUZ.IsNull() ) {
    Handle(HYDROData_Profile) aProfile = 
      Handle(HYDROData_Profile)::DownCast( aDocument->CreateObject( KIND_PROFILE ) );
    QString aBaseName = GetName() + "_bottom_profile";
    QString aName = HYDROData_Tool::GenerateObjectName( aDocument, aBaseName, QStringList(), true );
    aProfile->SetName( aName );
    aProfileUZ = aProfile->GetProfileUZ( true );
    aBottom->SetProfileUZ( aProfileUZ );
  }
  
  aProfileUZ->RemoveSection( 0 );

  aProfileUZ->CalculateAndAddPoints(aBottomPoints, aPolylineXY, true);
  
  return true;
}

Handle(HYDROData_Polyline3D) HYDROData_Stream::GetBottomPolyline() const
{
  return Handle(HYDROData_Polyline3D)::DownCast( 
           GetReferenceObject( DataTag_BottomPolyline ) );
}

bool HYDROData_Stream::SetBottomPolyline( const Handle(HYDROData_Polyline3D)& theBottom )
{
  if ( theBottom.IsNull() ) {
    return false;
  }

  SetReferenceObject( theBottom, DataTag_BottomPolyline );

  return true;
}

bool HYDROData_Stream::Interpolate( HYDROData_IProfilesInterpolator* theInterpolator )
{
  // Get the document
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() ) {
    return false;
  }
  
  if ( theInterpolator->GetCalculatedProfilesNumber() < 1 ) {
    theInterpolator->Calculate();
  }

  if ( theInterpolator->GetErrorCode() != OK ) {
    return false;
  }

  bool isOK = true;

  for ( int aProfileInd = 0; aProfileInd < theInterpolator->GetCalculatedProfilesNumber(); aProfileInd++ ) {
    // Get calculated point coordinates
    HYDROData_Profile::ProfilePoints aResultPoints = theInterpolator->GetResultProfilePoints( aProfileInd );
    if ( aResultPoints.IsEmpty() ) {
      isOK = false;
      continue;
    }
        
    // Create profile object
    Handle(HYDROData_Profile) aProfile = 
      Handle(HYDROData_Profile)::DownCast( aDocument->CreateObject( KIND_PROFILE ) );
    QString aBaseName = GetName() + "_interp_profile";
    QString aName = HYDROData_Tool::GenerateObjectName( aDocument, aBaseName );
    aProfile->SetName( aName );

    // Fill the profile with points
    aProfile->SetProfilePoints( aResultPoints );

    // Add profile to the stream
    bool isAdded = AddProfile( aProfile );
    if ( !isAdded ) {
      aProfile->Remove();
    }
    else
      aProfile->Update();
  }

  if ( isOK )
    Update();

  return isOK;
}

void HYDROData_Stream::CopyTo( const Handle(HYDROData_Entity)& theDestination,
                               bool isGenerateNewName ) const
{
  // Get the document
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() ) {
    return;
  }

  // Call base method
  HYDROData_Entity::CopyTo( theDestination, isGenerateNewName );

  Handle(HYDROData_Stream) aStreamCopy = 
    Handle(HYDROData_Stream)::DownCast( theDestination );

  // Copy bottom polyline if exists
  if ( !aStreamCopy.IsNull() ) {
    const Handle(HYDROData_Polyline3D) aBottom = GetBottomPolyline();
    if ( !aBottom.IsNull() ) {
      aStreamCopy->ClearReferenceObjects( DataTag_BottomPolyline );
      aStreamCopy->GenerateBottomPolyline();
      const Handle(HYDROData_Polyline3D) aBottomCopy = aStreamCopy->GetBottomPolyline();
      if ( !aBottomCopy.IsNull() && !aBottomCopy->GetPolylineXY().IsNull() ) {
        aBottomCopy->GetPolylineXY()->Update();
        aBottomCopy->Update();
      }
    }
  }
}
void HYDROData_Stream::CreatePresentations( const Handle(TColgp_HArray1OfPnt)     theArrayOfFPnt,
                                            const Handle(TColgp_HArray1OfPnt)     theArrayOfLPnt,
                                            const Handle(TopTools_HArray1OfShape) theArrOfProfiles,
                                            PrsDefinition&                        thePrs )
{

  HYDROData_Bathymetry::AltitudePoints left;
  for (int i = theArrayOfLPnt->Lower(); i <= theArrayOfLPnt->Upper(); i++)
  {
    left.push_back(HYDROData_Bathymetry::AltitudePoint(theArrayOfLPnt->Value(i).X(), 
      theArrayOfLPnt->Value(i).Y(),
      theArrayOfLPnt->Value(i).Z()));
  }

  HYDROData_Bathymetry::AltitudePoints right;
  for (int i = theArrayOfFPnt->Lower(); i <= theArrayOfFPnt->Upper(); i++)
  {
    right.push_back(HYDROData_Bathymetry::AltitudePoint(theArrayOfFPnt->Value(i).X(), 
      theArrayOfFPnt->Value(i).Y(),
      theArrayOfFPnt->Value(i).Z()));
  }

  std::vector<HYDROData_Bathymetry::AltitudePoints> dummy;
  TopTools_IndexedMapOfOrientedShape ll = HYDROData_DTM::Create3DShape(left, right, dummy);

  TopoDS_Shape LB, RB, IL, OL;

  if (!ll.IsEmpty())
  {
    TopAbs_ShapeEnum ll1_sht = ll(1).ShapeType();
    TopAbs_ShapeEnum ll2_sht = ll(2).ShapeType();
    if ((ll1_sht == TopAbs_WIRE || ll1_sht == TopAbs_EDGE) &&
      (ll2_sht == TopAbs_WIRE || ll2_sht == TopAbs_EDGE))
    {
      LB = ll(1);
      RB = ll(2);
    }
  }

  IL = TopoDS::Wire(theArrOfProfiles->Value(theArrOfProfiles->Lower())); //TODO check that
  OL = TopoDS::Wire(theArrOfProfiles->Value(theArrOfProfiles->Upper()));

  //make new compound so it's shapes will be in known order to build correct projection
  BRep_Builder BB;
  TopoDS_Compound newCmp;
  BB.MakeCompound(newCmp);
  BB.Add(newCmp, LB);
  BB.Add(newCmp, IL);
  BB.Add(newCmp, OL);
  BB.Add(newCmp, RB);

  thePrs.myPrs3D = newCmp;

  TopTools_SequenceOfShape LS;
  //HYDROData_DTM::Get2dFaceFrom3dPres( newCmp, TopoDS::Face(thePrs.myPrs2D), &LS, ind );
  
  HYDROData_DTM::GetPlanarFaceFromBanks(TopoDS::Edge(LB), TopoDS::Edge(RB), TopoDS::Face(thePrs.myPrs2D), &LS);

#ifndef NDEBUG
  TopTools_IndexedMapOfShape EE;
  TopExp::MapShapes(thePrs.myPrs2D, TopAbs_EDGE, EE);
  int noncontNb = 0;
  for (int i = 1; i <= 4; i++)
  {
    TopoDS_Shape W = LS(i);
    TopTools_IndexedMapOfShape EW;
    TopExp::MapShapes(W, TopAbs_EDGE, EW);
    for (int k = 1; k <= EW.Extent(); k++)
      noncontNb += !EE.Contains(EW(k));
  }
  //noncontNb > 0 => some problem with edge history
  assert(noncontNb == 0);
#endif

  thePrs.myLeftBank = LS(1);  
  thePrs.myInlet = LS(2);  
  thePrs.myOutlet = LS(3);  
  thePrs.myRightBank = LS(4);

}

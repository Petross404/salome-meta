// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_DTM_HeaderFile
#define HYDROData_DTM_HeaderFile

#include "HYDROData_Bathymetry.h"
#include "HYDROData_Profile.h"

#include <gp_Pnt2d.hxx>

#include<Geom2d_BSplineCurve.hxx>
#include<Geom2d_Curve.hxx>
#include<Geom_Plane.hxx>

#include <TopTools_DataMapOfShapeListOfShape.hxx>
#include <TopTools_IndexedMapOfOrientedShape.hxx>
#include <TopTools_SequenceOfShape.hxx>

#include <vector>
#include <set>
#include <NCollection_DataMap.hxx>

class gp_Pnt;
class gp_Vec2d;
class TopoDS_Edge;
class TopoDS_Wire;
class TopoDS_Face;
class TopoDS_Compound;


/**\class HYDROData_DTM
 * \brief Class that represents the Digital Terrain Model
 */
class HYDROData_DTM : public HYDROData_Bathymetry
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Bathymetry::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Profiles,
    DataTag_DDZ,
    DataTag_SpatialStep,
    DataTag_LeftBankShape,
    DataTag_RightBankShape,
    DataTag_InletShape,
    DataTag_OutletShape,
    DataTag_3DShape,
    DataTag_2DShape
  };

public:
  DEFINE_STANDARD_RTTIEXT( HYDROData_DTM, HYDROData_Bathymetry );

  HYDRODATA_EXPORT HYDROData_SequenceOfObjects GetProfiles() const;
  HYDRODATA_EXPORT void SetProfiles( const HYDROData_SequenceOfObjects& );

  HYDRODATA_EXPORT double GetDDZ() const;
  HYDRODATA_EXPORT void SetDDZ( double );

  HYDRODATA_EXPORT double GetSpatialStep() const;
  HYDRODATA_EXPORT void SetSpatialStep( double );

  HYDRODATA_EXPORT virtual void Update();

public:
  struct PointUZ
  {
    PointUZ( double u=0, double z=0 ) { U=u; Z=z; }
    double U;
    double Z;
  };
  class CurveUZ : public std::vector<PointUZ>
  {
  public:
    CurveUZ( double theXcurv, const gp_Vec2d& theProfileDir, double theDeltaZ, double theMaxZ );
    ~CurveUZ();

    double Xcurv() const;
    gp_Vec2d ProfileDir() const;
    double DeltaZ() const;
    double MaxZ() const;

    CurveUZ operator + ( const CurveUZ& ) const;
    CurveUZ operator * ( double ) const;

  private:
    double myXcurv;
    gp_Vec2d myProfileDir;
    double myDeltaZ;
    double myMaxZ;
  };

protected:
  friend class HYDROData_Stream;
  friend class HYDROData_Iterator;
  friend class test_HYDROData_DTM;

  HYDRODATA_EXPORT HYDROData_DTM();
  virtual HYDRODATA_EXPORT ~HYDROData_DTM();

  static Handle(Geom2d_BSplineCurve) CreateHydraulicAxis( 
    const std::vector<Handle(HYDROData_Profile)>& theProfiles,
    std::vector<double>& theDistances );

  static std::vector<Handle(Geom2d_Curve)> ProfileToParametric( const Handle(HYDROData_Profile)& theProfile,
                                                                double& theUMin, double& theUMax,
                                                                gp_Vec2d& theDir );

  static void GetProperties( const Handle(HYDROData_Profile)& theProfile,
                             gp_Pnt& theLowestPoint, gp_Vec2d& theDir,
                             double& theZMin, double& theZMax );

  static void ProfileDiscretization( const Handle(HYDROData_Profile)& theProfile,
                                     double theXCurv, double theMinZ, double theMaxZ, double theTopZ, double theDDZ,
                                     CurveUZ& theMidPointCurve,
                                     CurveUZ& theWidthCurve,
                                     int& intersection_nb,
                                     double theTolerance,
                                     QSet<QString>& warnings);

  static void CurveTo3D( const Handle(Geom2d_BSplineCurve)& theHydraulicAxis,
                         const CurveUZ& theMidCurve, const CurveUZ& theWidthCurve,
                         AltitudePoints& thePoints );
  
  static void Interpolate( const CurveUZ& theCurveA, const CurveUZ& theCurveB, 
                           int theNbSteps, std::vector<CurveUZ>& theInterpolation,
                           bool isAddSecond);

  static std::vector<AltitudePoints> Interpolate
    ( const Handle(Geom2d_BSplineCurve)& theHydraulicAxis,
      const Handle(HYDROData_Profile)& theProfileA,
      double theXCurvA,
      const Handle(HYDROData_Profile)& theProfileB,
      double theXCurvB,
      double theDDZ, int theNbSteps, bool isAddSecond,
      int& inter_nb_1, int& inter_nb_2,
      NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
      bool ToEstimateWarningsOnly);

  static AltitudePoints Interpolate( const std::vector<Handle(HYDROData_Profile)>& theProfiles,
                                     double theDDZ, double theSpatialStep,
                                     AltitudePoints& theLeft,
                                     AltitudePoints& theRight,
                                     std::vector<AltitudePoints>& theMainProfiles,
                                     std::set<int>& invalInd, 
                                     NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
                                     bool ToEstimateWarningsOnly);

  static void PointsToWire(const AltitudePoints& pnts, TopoDS_Wire& W );

  static void PointsToEdge(const AltitudePoints& pnts, TopoDS_Edge& E );

  //static void ProjWireOnPlane(const TopoDS_Shape& inpWire, const Handle_Geom_Plane& RefPlane,
    //TopTools_DataMapOfShapeListOfShape* E2PE);

  static bool GetPlanarFaceFromBanks(const TopoDS_Edge& LB, const TopoDS_Edge& RB, TopoDS_Face& outF,
    TopTools_SequenceOfShape* Boundr);

  static TopTools_IndexedMapOfOrientedShape Create3DShape(const AltitudePoints& left,
                                                          const AltitudePoints& right,
                                                          const std::vector<AltitudePoints>& main_profiles);

  static void CreateProfiles(const std::vector<Handle(HYDROData_Profile)>& theProfiles,
                             double theDDZ,
                             double theSpatialStep,
                             AltitudePoints& theOutLeft,
                             AltitudePoints& theOutRight,
                             AltitudePoints& theOutPoints,
                             std::vector<AltitudePoints>& theOutMainProfiles,
                             TopoDS_Shape& Out3dPres,
                             TopoDS_Shape& Out2dPres,
                             TopoDS_Shape& OutLeftB,
                             TopoDS_Shape& OutRightB,
                             TopoDS_Shape& OutInlet,
                             TopoDS_Shape& OutOutlet,
                             bool Create3dPres,
                             bool Create2dPres,
                             std::set<int>& InvInd,
                             bool& ProjStat,
                             NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
                             bool ToEstimateWarningsOnly);

  //static bool Get2dFaceFrom3dPres(const TopoDS_Compound& cmp, TopoDS_Face& outF, 
    //TopTools_SequenceOfShape* Boundr = NULL, std::set<int> ind = std::set<int>() );
  
  static int EstimateNbPoints( const std::vector<Handle(HYDROData_Profile)>& theProfiles,
                               double theDDZ, double theSpatialStep );

  void GetPresentationShapes( TopoDS_Shape& Out3dPres,
                              TopoDS_Shape& Out2dPres,
                              TopoDS_Shape& OutLeftB,
                              TopoDS_Shape& OutRightB,
                              TopoDS_Shape& OutInlet,
                              TopoDS_Shape& OutOutlet );
public:

  HYDRODATA_EXPORT static void CreateProfilesFromDTM ( const HYDROData_SequenceOfObjects& InpProfiles,
                                                       double ddz,
                                                       double step, 
                                                       AltitudePoints& points,
                                                       TopoDS_Shape& Out3dPres,
                                                       TopoDS_Shape& Out2dPres,
                                                       TopoDS_Shape& OutLeftB,
                                                       TopoDS_Shape& OutRightB,
                                                       TopoDS_Shape& OutInlet,
                                                       TopoDS_Shape& OutOutlet,
                                                       bool Create3dPres,
                                                       bool Create2dPres,
                                                       std::set<int>& InvInd,
                                                       int thePntsLimit,
                                                       bool& WireIntersections,
                                                       NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
                                                       bool& ToEstimateWarnings);

  HYDRODATA_EXPORT void GetWarnings(NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings);

  protected:
    NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>> myWarnings;
};




#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Entity_HeaderFile
#define HYDROData_Entity_HeaderFile

#include "HYDROData.h"
#include <NCollection_Sequence.hxx>
#include <TDF_Label.hxx>
#include <QMap>
#include <QString>
#include <Standard_Type.hxx>
#include <Standard_Transient.hxx>

#ifdef NONLS
#undef NONLS
#endif

#ifdef NOMINMAX
#undef NOMINMAX
#endif

class QColor;
class QVariant;
class QStringList;
class TDataStd_ReferenceList;
class HYDROData_Entity;
class TopoDS_Shape;

///! Kind of an object in a document
typedef int ObjectKind;

const ObjectKind KIND_UNKNOWN             = 0; ///! Unrecognized object
const ObjectKind KIND_IMAGE               = 1;
const ObjectKind KIND_POLYLINE            = 2;
const ObjectKind KIND_BATHYMETRY          = 3;
const ObjectKind KIND_ALTITUDE            = 4;
const ObjectKind KIND_IMMERSIBLE_ZONE     = 5;
const ObjectKind KIND_RIVER               = 6;
const ObjectKind KIND_STREAM              = 7;
const ObjectKind KIND_CONFLUENCE          = 8;
const ObjectKind KIND_CHANNEL             = 9;
const ObjectKind KIND_OBSTACLE            = 10;
const ObjectKind KIND_DIGUE               = 11;
const ObjectKind KIND_PROFILE             = 12;
const ObjectKind KIND_PROFILEUZ           = 13;
const ObjectKind KIND_POLYLINEXY          = 14;
const ObjectKind KIND_CALCULATION         = 15;
const ObjectKind KIND_ZONE                = 16;
const ObjectKind KIND_REGION              = 17;
const ObjectKind KIND_VISUAL_STATE        = 18;
const ObjectKind KIND_ARTIFICIAL_OBJECT   = 19;
const ObjectKind KIND_NATURAL_OBJECT      = 20;
const ObjectKind KIND_DUMMY_3D            = 21;
const ObjectKind KIND_SHAPES_GROUP        = 22;
const ObjectKind KIND_SPLIT_GROUP         = 23;
const ObjectKind KIND_STREAM_ALTITUDE     = 24;
const ObjectKind KIND_OBSTACLE_ALTITUDE   = 25;
const ObjectKind KIND_STRICKLER_TABLE     = 26;
const ObjectKind KIND_LAND_COVER_OBSOLETE = 27;
const ObjectKind KIND_CHANNEL_ALTITUDE    = 28;
const ObjectKind KIND_LAND_COVER_MAP      = 29;
const ObjectKind KIND_DTM                 = 30;
const ObjectKind KIND_BC_POLYGON          = 31;
const ObjectKind KIND_LISM                = 32;
const ObjectKind KIND_LAST                = KIND_BC_POLYGON+1;

class MapOfTreatedObjects : public QMap<QString,Handle(Standard_Transient)>
{
};

class HYDRODATA_EXPORT HYDROData_SequenceOfObjects : public NCollection_Sequence<Handle(HYDROData_Entity)>
{
public:
  HYDROData_SequenceOfObjects();
  HYDROData_SequenceOfObjects( const HYDROData_SequenceOfObjects& );
  HYDROData_SequenceOfObjects( const NCollection_Sequence<Handle(HYDROData_Entity)>& );
};

///! Is Equal for HYDROData_Entity mapping
HYDRODATA_EXPORT bool IsEqual(const Handle(HYDROData_Entity)& theObj1, const Handle(HYDROData_Entity)& theObj2);

/**\class HYDROData_Entity
 * \brief Generic class of any object in the data model.
 *
 * Interface for getting access to the object that belong to the data model.
 * Managed by Document. Provides access to the common properties:
 * kind of an object, name.
 */
class HYDROData_Entity : public Standard_Transient
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First  = 0,     ///< first tag, to reserve
    DataTag_ZLevel,         ///< z-level of object presentation
    DataTag_GeomChange,
    DataTag_DefaultName,    ///< to keep the automatic name, useful in Python dump
  };

public:
  enum Geometry
  {
    Geom_No = 1,
    Geom_2d = 2,
    Geom_Z  = 4,
    Geom_Groups = 8,

    Geom_3d = Geom_2d | Geom_Z,
    Geom_2d_and_groups = Geom_2d | Geom_Groups,
    Geom_All = Geom_3d | Geom_Groups | Geom_No,
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Entity, Standard_Transient);

  HYDRODATA_EXPORT static QString Type( ObjectKind );
  HYDRODATA_EXPORT virtual QString GetType() const;

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_UNKNOWN; }

  /**
   * Returns the name of this object.
   */
  HYDRODATA_EXPORT virtual QString GetName() const;

  /**
   * Returns the default name of this object.
   */
  HYDRODATA_EXPORT virtual QString GetDefaultName() const;

  /**
   * Updates the name of this object.
   */
  HYDRODATA_EXPORT virtual void SetName( const QString& theName, bool isDefault = false );

  /**
   * Returns the name of this object valid for Python script.
   */
  HYDRODATA_EXPORT virtual QString GetObjPyName() const;

  /**
   * Dump object to Python script representation.
   * Base implementation returns empty list,
   * You should reimplement this function in your derived class if it
   * has Python API and can be imported/exported from/to Python script.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Updates object state. Base implementation dose nothing.
   */
  HYDRODATA_EXPORT virtual void Update();

  HYDRODATA_EXPORT virtual void UpdateLocalCS( double theDx, double theDy );

  /**
   * Checks that object has 2D presentation. Base implementation returns false.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Show object at the top of other model objects.
   */
  HYDRODATA_EXPORT virtual void Show();


  /**
   * Returns data of object wrapped to QVariant.
   * Base implementation returns null value.
   */
  HYDRODATA_EXPORT virtual QVariant GetDataVariant();



  HYDRODATA_EXPORT virtual void ClearChanged();
  HYDRODATA_EXPORT virtual void Changed( Geometry );

  HYDRODATA_EXPORT bool IsMustBeUpdated( Geometry ) const;

  /**
   * Returns flag indicating that object is updateble or not.
   */
  HYDRODATA_EXPORT virtual bool CanBeUpdated() const;


  /**
   * Checks is object exists in the data structure.
   * \returns true is object is not exists in the data model
   */
  HYDRODATA_EXPORT bool IsRemoved() const;

  /**
   * Removes object and it child sub-objects from the data structure.
   */
  HYDRODATA_EXPORT virtual void Remove();

  /**
   * Returns flag indicating that object can be removed or not.
   * Reimplement this method in class which can't be removed
   * separately with it parent object.
   * Base implementaiton returns always TRUE.
   */
  HYDRODATA_EXPORT virtual bool CanRemove();

  /**
   * Copies all properties of this to the destinated object.
   * Objects must be the same type.
   * \param theDestination initialized object (from any document) - target of copying
   */
  HYDRODATA_EXPORT virtual void CopyTo( const Handle(HYDROData_Entity)& theDestination,
                                        bool isGenerateNewName ) const;

  /**
   * Returns the label of this object.
   */
  HYDRODATA_EXPORT TDF_Label& Label() { return myLab; }


  /**
   * Returns father object. For object created under root document label
   * this method always return NULL object.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Entity) GetFatherObject() const;


  /**
   * Returns the list of all reference objects of this object.
   * Base implementation always return empty list.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;


  /**
   * Returns the z-level for object presentation, -1 if no z-level.
   */
  HYDRODATA_EXPORT virtual bool GetZLevel( Standard_Integer& theLevel ) const;

  /**
   * Set the z-level for object presentation.
   */
  HYDRODATA_EXPORT virtual void SetZLevel( const Standard_Integer& theLevel );

  /**
   * Remove the z-level of object presentation.
   */
  HYDRODATA_EXPORT virtual void RemoveZLevel();

  /**
    Add in Python script the find instruction, to retrieve the Python object in the document by the object name.
    @param theScript the script
    @param defName the name to use, if not default name.
  */
  void findPythonReferenceObject( QStringList&                    theScript,
                                  QString                         defName = QString()) const;

  /**
    Add in Python script the set name instruction.
    @param theScript the script
    @param defName the name to use, if not default name.
  */
 void SetNameInDumpPython(QStringList&                           theScript,
                          QString                                theName = QString()) const;

    /**
   * Internal method that used to store the color attribute
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   * \param theColor color to save
   */
  HYDRODATA_EXPORT void SetColor( const QColor& theColor, const int theTag = 0 );

  /**
   * Internal method that used to retreive the color attribute
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   * \param theDefColor default color to return if attribute has not been set before
   */
  HYDRODATA_EXPORT QColor GetColor( const QColor& theDefColor, const int theTag = 0 ) const;

  /* Internal method that used to retreive the color attribute
    * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   * \param theOutColor color to return if attribute has been set before
   * \return true if attribute is present
   */
  HYDRODATA_EXPORT bool GetColor( QColor& theOutColor,
                                  const int     theTag ) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Entity( Geometry );

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_Entity();

  /**
   * Put the object to the label of the document.
   * \param theLabel new label of the object
   */
  HYDRODATA_EXPORT virtual void SetLabel( const TDF_Label& theLabel );

  /**
   * Internal method that used to store the byte array attribute
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   * \param theData pointer to bytes array
   * \param theLen number of bytes in byte array that must be stored
   */
  void SaveByteArray(const int theTag, const char* theData, const int theLen);

  /**
   * Internal method that used to retreive the content of byte array attribute
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   * \param theLen number of bytes in byte array
   * \returns pointer to the internal data structure wit harray content,
   *          or NULL if array size is zero
   */
  const char* ByteArray(const int theTag, int& theLen) const;

  /**
   * Internal method that used to store the reference object label attribute
   * \param theObj pointer to reference object
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   */
  int NbReferenceObjects( const int theTag = 0 ) const;

  /**
   * Internal method that used to check object for entry into the reference list
   * \param theObj pointer to reference object
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   */
  bool HasReference( const Handle(HYDROData_Entity)& theObj,
                     const int                      theTag = 0 ) const;

  /**
   * Internal method that used to store the reference object label attribute
   * \param theObj pointer to reference object
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   */
  void AddReferenceObject( const Handle(HYDROData_Entity)& theObj,
                           const int                      theTag = 0 );

  /**
   * Internal method that used to store the reference object label attribute
   * \param theObj pointer to reference object
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   * \param theIndex index in the list of references
             - if more that len then just append it to the end of list
             - if less than zero then prepend to the list
             - indexing starts from 0
   */
  void SetReferenceObject( const Handle(HYDROData_Entity)& theObj,
                           const int                      theTag = 0,
                           const int                      theIndex = 0 );

  /**
   * Internal method that used to store the reference object label attribute
   * \param theObj pointer to reference object
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   * \param theBeforeIndex index in the list of references
             - if more that len then just append it to the end of list
             - if less than zero then prepend to the list
             - indexing starts from 0
   */
  void InsertReferenceObject( const Handle(HYDROData_Entity)& theObj,
                              const int                      theTag = 0,
                              const int                      theBeforeIndex = 0 );

  /**
   * Internal method that used to store the reference object label attribute
   * \param theObjects sequence with pointers to reference objects
   * \param theTag tag of a label to store attribute (for 0 this is myLab)
   */
  void SetReferenceObjects( const HYDROData_SequenceOfObjects& theObjects,
                            const int                          theTag = 0 );

  /**
   * Internal method that used to retreive the reference object(s) attribute
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   * \param theIndex index in the list of references
   *        - indexing starts from 0
   * \returns pointer to reference object or NULL if label is not set
   */
  Handle(HYDROData_Entity) GetReferenceObject( const int theTag   = 0,
                                              const int theIndex = 0 ) const;

  HYDROData_SequenceOfObjects GetReferenceObjects( const int theTag = 0 ) const;

  /**
   * Internal method that used to remove the reference object attribute
   * \param theRefLabel reference object label to remove
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   */
  void RemoveReferenceObject( const TDF_Label& theRefLabel, const int theTag = 0 );

  /**
   * Internal method that used to remove the reference object attribute
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   * \param theIndex index in the list of references
   *        - indexing starts from 0
   */
  void RemoveReferenceObject( const int theTag = 0, const int theIndex = 0 );

  /**
   * Internal method that used to clear list of the reference objects attribute
   * \param theTag tag of a label that keeps the attribute (for 0 this is myLab)
   */
  void ClearReferenceObjects( const int theTag = 0 );

public:

   HYDRODATA_EXPORT virtual bool CompareLabels(const Handle(HYDROData_Entity)& theOtherObj);

protected:

  /**
   * Dump the initial object creation to a Python script.
   * You should call it from DumpToPython implementation before
   * dumping fields of the object.
   */
  HYDRODATA_EXPORT virtual QStringList dumpObjectCreation( MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Returns an object type name as a string for dumping to Python.
   */
  QString getPyTypeID() const;

  void setPythonReferenceObject( const QString&                  thePyScriptPath,
                                 MapOfTreatedObjects&            theTreatedObjects,
                                 QStringList&                    theScript,
                                 const Handle(HYDROData_Entity)& theRefObject,
                                 const QString&                  theMethod ) const;

  bool checkObjectPythonDefinition( const QString&                  thePyScriptPath,
                                    MapOfTreatedObjects&            theTreatedObjects,
                                    QStringList&                    theScript,
                                    const Handle(HYDROData_Entity)& theRefObject ) const;

  void setPythonObjectColor( QStringList&         theScript,
                             const QColor&        theColor,
                             const QColor&        theDefaultColor,
                             const QString&       theMethod ) const;
protected:

  Handle(TDataStd_ReferenceList) getReferenceList( const int  theTag,
                                                   const bool theIsCreate ) const;

  void SetShape( int theTag, const TopoDS_Shape& theShape );
  TopoDS_Shape GetShape( int theTag ) const;

  void SetDouble( int theTag, double theValue );
  double GetDouble( int theTag, double theDefValue = 0.0 ) const;

  void SetInteger( int theTag, int theValue );
  int GetInteger( int theTag, int theDefValue = 0 ) const;


  int GetGeomChangeFlag() const;

protected:
  /// Array of pointers to the properties of this object; index in this array is returned by \a AddProperty.
  TDF_Label myLab; ///< label of this object
  Geometry  myGeom;
};

#endif

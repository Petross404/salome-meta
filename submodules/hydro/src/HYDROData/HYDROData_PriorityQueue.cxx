// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_PriorityQueue.h>
#include <HYDROData_CalculationCase.h>
#include <HYDROData_Iterator.h>
#include <TDataStd_ReferenceList.hxx>
#include <TDataStd_Integer.hxx>
#include <TDF_ChildIterator.hxx>

HYDROData_PriorityQueue::HYDROData_PriorityQueue( HYDROData_CalculationCase* theCalcCase,
                                                  Standard_Integer aTag )
{
  myObjects = theCalcCase->GetGeometryObjects();
  for( int i=myObjects.Lower(), n=myObjects.Upper(); i<=n; i++ )
  {
    Handle(HYDROData_Entity) anObj = myObjects.Value( i );
    if( !anObj.IsNull() )
    {
      QString anObjName = anObj->GetName();
      myNames[anObjName] = anObj;
    }
  }

  myRules = GetRules( theCalcCase->Label().FindChild( aTag ) );
}

HYDROData_PriorityQueue::~HYDROData_PriorityQueue()
{
}

Handle(HYDROData_Entity) HYDROData_PriorityQueue::GetMostPriorityObject( const QStringList& theZoneObjects,
                                                                         HYDROData_Zone::MergeType& theMergeType ) const
{
  QStringList aSortedZoneObjects;
  for( int i=myObjects.Lower(), n=myObjects.Upper(); i<=n; i++ )
  {
    QString aName = myObjects.Value( i )->GetName();
    if( theZoneObjects.contains( aName ) )
      aSortedZoneObjects.append( aName );
  }

  Handle(HYDROData_Entity) aMostPriorityObj;
  theMergeType = HYDROData_Zone::Merge_UNKNOWN;
  QStringList::const_iterator anIt = aSortedZoneObjects.begin(), aLast = aSortedZoneObjects.end();
  for( ; anIt!=aLast; anIt++ )
  {
    HYDROData_Zone::MergeType aLocalMerge = HYDROData_Zone::Merge_UNKNOWN;
    Handle(HYDROData_Entity) anObj = myNames[*anIt];
    if( !anObj.IsNull() )
    {
      if( aMostPriorityObj.IsNull() )
      {
        aMostPriorityObj = anObj;
        continue;
      }

      bool isMorePriority = IsMorePriority( anObj, aMostPriorityObj, aLocalMerge );

      if( isMorePriority )
        aMostPriorityObj = anObj;

      if( aLocalMerge != HYDROData_Zone::Merge_UNKNOWN && 
          ( theMergeType==HYDROData_Zone::Merge_UNKNOWN || isMorePriority ) )
        theMergeType = aLocalMerge;
    }
  }
  return aMostPriorityObj;
}

bool HYDROData_PriorityQueue::IsMorePriority( const Handle(HYDROData_Entity)& theObj1,
                                              const Handle(HYDROData_Entity)& theObj2,
                                              HYDROData_Zone::MergeType& theMergeType ) const
{
  // 1. First we check custom rules
  HYDROData_ListOfRules::const_iterator anIt = myRules.begin(), aLast = myRules.end();
  for( ; anIt!=aLast; anIt++ )
  {
    if( anIt->Object1->Label()==theObj1->Label() && anIt->Object2->Label()==theObj2->Label() )
    {
      theMergeType = anIt->MergeType;
      return anIt->Priority==GREATER;
    }
    if( anIt->Object1->Label()==theObj2->Label() && anIt->Object2->Label()==theObj1->Label() )
    {
      theMergeType = anIt->MergeType;
      return anIt->Priority==LESS;
    }
  }

  // 2. If no custom rule found, the standard ordering list is applied
  for( int i=myObjects.Lower(), n=myObjects.Upper(); i<=n; i++ )
  {
    if( myObjects.Value( i )->Label() == theObj1->Label() )
    {
      theMergeType = HYDROData_Zone::Merge_Object;
      return true;
    }
    if( myObjects.Value( i )->Label() == theObj2->Label() )
    {
      theMergeType = HYDROData_Zone::Merge_Object;
      return false;
    }
  }
  return false;
}

void HYDROData_PriorityQueue::ClearRules( TDF_Label& theRulesLabel )
{
  theRulesLabel.ForgetAllAttributes( true );
}

enum HYDROData_PriorityQueueTag
{
  Object1_Tag,
  Priority_Tag,
  Object2_Tag,
  Merge_Tag,
};

void HYDROData_PriorityQueue::AddRule( TDF_Label&                      theRulesLabel,
                                       const Handle(HYDROData_Entity)& theObject1,
                                       HYDROData_PriorityType          thePriority,
                                       const Handle(HYDROData_Entity)& theObject2,
                                       HYDROData_Zone::MergeType       theMergeType )
{
  // Get the last rule index
  Standard_Integer aRuleIndex = 0;
  Handle(TDataStd_Integer) anIntVal;
  if ( theRulesLabel.FindAttribute( TDataStd_Integer::GetID(), anIntVal ) ) {
    aRuleIndex = anIntVal->Get();
  }

  TDF_Label aNewRuleLab = theRulesLabel.FindChild( aRuleIndex );

  TDF_Label anObj1Lab = aNewRuleLab.FindChild( Object1_Tag );
  Handle(TDataStd_ReferenceList) aRefs = TDataStd_ReferenceList::Set( anObj1Lab );
  aRefs->SetID(TDataStd_ReferenceList::GetID());
  aRefs->Append( theObject1->Label() );
  
  TDF_Label aPriorityLab = aNewRuleLab.FindChild( Priority_Tag );
  Handle(TDataStd_Integer) anPLAttr = TDataStd_Integer::Set( aPriorityLab, thePriority );
  anPLAttr->SetID(TDataStd_Integer::GetID());

  TDF_Label anObj2Lab = aNewRuleLab.FindChild( Object2_Tag );
  aRefs = TDataStd_ReferenceList::Set( anObj2Lab );
  aRefs->SetID(TDataStd_ReferenceList::GetID());
  aRefs->Append( theObject2->Label() );

  TDF_Label aMergeLab = aNewRuleLab.FindChild( Merge_Tag );
  Handle(TDataStd_Integer) anMLAttr = TDataStd_Integer::Set( aMergeLab, theMergeType );
  anMLAttr->SetID(TDataStd_Integer::GetID());

  // Increment the last rule index
  Handle(TDataStd_Integer) anRLAttr = TDataStd_Integer::Set( theRulesLabel, aRuleIndex + 1 );
  anRLAttr->SetID(TDataStd_Integer::GetID());
}

HYDROData_ListOfRules HYDROData_PriorityQueue::GetRules( const TDF_Label& theRulesLabel )
{
  HYDROData_ListOfRules aRules;

  Handle(TDataStd_ReferenceList) aRefs1, aRefs2;
  Handle(TDataStd_Integer) aPriorityAttr, aMergeAttr;

  TDF_ChildIterator anIt( theRulesLabel );
  for( ; anIt.More(); anIt.Next() )
  {
    TDF_Label aRuleLabel = anIt.Value();

    bool isObj1OK = aRuleLabel.FindChild    ( Object1_Tag ). FindAttribute( TDataStd_ReferenceList::GetID(), aRefs1 );
    bool isPriorityOK = aRuleLabel.FindChild( Priority_Tag ).FindAttribute( TDataStd_Integer::GetID(),       aPriorityAttr );
    bool isObj2OK = aRuleLabel.FindChild    ( Object2_Tag ). FindAttribute( TDataStd_ReferenceList::GetID(), aRefs2 );
    bool isMergeOK = aRuleLabel.FindChild   ( Merge_Tag ).   FindAttribute( TDataStd_Integer::GetID(),       aMergeAttr );

    if( isObj1OK && isPriorityOK && isObj2OK && isMergeOK )
    {
      HYDROData_CustomRule aRule;
      aRule.Object1 = HYDROData_Iterator::Object( aRefs1->First() );
      aRule.Priority = ( HYDROData_PriorityType ) aPriorityAttr->Get();
      aRule.Object2 = HYDROData_Iterator::Object( aRefs2->First() );
      aRule.MergeType = ( HYDROData_Zone::MergeType ) aMergeAttr->Get();
      aRules.append( aRule );
    }
  }

  return aRules;
}

QString HYDROData_PriorityQueue::DumpRules( const TDF_Label& theRulesLab )
{
  QString aDump = "Rules:\n";

  HYDROData_ListOfRules aRules = GetRules( theRulesLab );
  HYDROData_ListOfRules::const_iterator anIt = aRules.begin(), aLast = aRules.end();
  for( ; anIt!=aLast; anIt++ )
  {
    QString aRule = anIt->Object1->GetName() + " ";
    aRule += ( anIt->Priority == LESS ? "<" : ">" ) + QString( " " );
    aRule += anIt->Object2->GetName() + " ";

    switch( anIt->MergeType )
    {
    case HYDROData_Zone::Merge_UNKNOWN:
      aRule += "unknown";
      break;
    case HYDROData_Zone::Merge_ZMIN:
      aRule += "zmin";
      break;
    case HYDROData_Zone::Merge_ZMAX:
      aRule += "zmax";
      break;
    case HYDROData_Zone::Merge_Object:
      aRule += "object";
      break;
    }
    aDump += aRule + "\n";
  }
  return aDump;
}

void HYDROData_PriorityQueue::DumpRulesToPython( const TDF_Label& theRulesLab,
                                                 const QString& theCalcCaseName,
                                                 QStringList& theScript )
{
  HYDROData_ListOfRules aRules = GetRules( theRulesLab );
  HYDROData_ListOfRules::const_iterator anIt = aRules.begin(), aLast = aRules.end();
  for( ; anIt!=aLast; anIt++ )
  {
    QString anObj1 = anIt->Object1->GetObjPyName();
    QString anObj2 = anIt->Object2->GetObjPyName();
    QString aPriority = anIt->Priority == LESS ? "LESS" : "GREATER";
    QString aMergeType;

    HYDROData_CalculationCase::DataTag aDataTag = HYDROData_CalculationCase::DataTag_CustomRules;

    switch( anIt->MergeType )
    {
    case HYDROData_Zone::Merge_UNKNOWN:
      aMergeType = "HYDROData_Zone.Merge_UNKNOWN";
      break;
    case HYDROData_Zone::Merge_ZMIN:
      aMergeType = "HYDROData_Zone.Merge_ZMIN";
      break;
    case HYDROData_Zone::Merge_ZMAX:
      aMergeType = "HYDROData_Zone.Merge_ZMAX";
      break;
    case HYDROData_Zone::Merge_Object:
      aMergeType = "HYDROData_Zone.Merge_Object";
      break;
    }

    QString aRule = QString( "%0.AddRule( %1, %2, %3, %4, %5 )" ).
      arg( theCalcCaseName ).arg( anObj1 ).arg( aPriority ).arg( anObj2 ).arg( aMergeType ).arg( aDataTag );

    theScript << aRule;
  }
}

bool HYDROData_PriorityQueue::GetRule( const TDF_Label& theRulesLab,
                                       int theIndex, 
                                       Handle(HYDROData_Entity)&  theObject1,
                                       HYDROData_PriorityType&    thePriority,
                                       Handle(HYDROData_Entity)&  theObject2,
                                       HYDROData_Zone::MergeType& theMergeType )
{
  TDF_Label aRuleLabel = theRulesLab.FindChild( theIndex );

  Handle(TDataStd_ReferenceList) aRefs1, aRefs2;
  Handle(TDataStd_Integer) aPriorityAttr, aMergeAttr;

  bool isObj1OK = aRuleLabel.FindChild    ( Object1_Tag ). FindAttribute( TDataStd_ReferenceList::GetID(), aRefs1 );
  bool isPriorityOK = aRuleLabel.FindChild( Priority_Tag ).FindAttribute( TDataStd_Integer::GetID(),       aPriorityAttr );
  bool isObj2OK = aRuleLabel.FindChild    ( Object2_Tag ). FindAttribute( TDataStd_ReferenceList::GetID(), aRefs2 );
  bool isMergeOK = aRuleLabel.FindChild   ( Merge_Tag ).   FindAttribute( TDataStd_Integer::GetID(),       aMergeAttr );

  bool isOK = isObj1OK && isPriorityOK && isObj2OK && isMergeOK;
  if( isOK )
  {
    theObject1   = HYDROData_Iterator::Object( aRefs1->First() );
    thePriority  = ( HYDROData_PriorityType ) aPriorityAttr->Get();
    theObject2   = HYDROData_Iterator::Object( aRefs2->First() );
    theMergeType = ( HYDROData_Zone::MergeType ) aMergeAttr->Get();
  }
  return isOK;
}

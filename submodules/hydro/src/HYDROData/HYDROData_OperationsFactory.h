// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_OperationsFactory_HeaderFile
#define HYDROData_OperationsFactory_HeaderFile

#include <HYDROData.h>
#include <HYDROData_Image.h>
#include <QMap>

class ImageComposer_Operator;
class HYDROData_Document;

/**\class HYDROData_OperationsFactory
 *
 * \brief This class provides the unified management of operations on images.
 *
 * Object is created as one global instance and allows to register and launch
 * all registered operations in general way. To register a new operation just
 * call REGISTER_HYDRO_OPERATION(operation_name) in some method implementation.
 * This macro will call constructor of this operation (it must inherit
 * ImageComposer_Operator) and factory will set up arguments and call this 
 * operator by demand.
 */

class HYDROData_OperationsFactory
{
public:

  //! Returns the global factory
  HYDRODATA_EXPORT static HYDROData_OperationsFactory* Factory();
  
  /**
   * Registers the operator by the name, used by REGISTER_HYDRO_OPERATION macro
   * \param theOperator new instance of the operator that will be used for
   *                    processing of operation with such kind
   */
  HYDRODATA_EXPORT static void Register(
    ImageComposer_Operator* theOperator);
  
  /**
   * Creates a new Image object in the data structure by the operator data.
   * \param theDoc document where it must be created
   * \param theOperator base operator for this Image: will be used in "Update" to recompute the image
   * \returns created object related to the data structure
   */
  HYDRODATA_EXPORT Handle(HYDROData_Image) CreateImage(
    Handle(HYDROData_Document) theDoc, const ImageComposer_Operator* theOperator);

  /**
   * Returns the operator, initialized by the properties of theImage
   * \param theImage data structures object, that contains all arguments 
   *                 required for creation of operation
   * \returns NULL if operator type is unknown
   */
  HYDRODATA_EXPORT ImageComposer_Operator* Operator(
    Handle(HYDROData_Image) theImage) const;
 

  /**
   * Returns the appropriate operator by the name
   * \param theName name of the operator, equals to the operation_name constructor
   * \returns NULL if operator with such name is not registered yet
   */
  HYDRODATA_EXPORT ImageComposer_Operator* Operator(const QString theName) const;

protected:

  //! Not public constructor that creates only one, global instance of this factory.
  HYDROData_OperationsFactory();  

private:
  //! Map that stores all operators, isentified by strings
  typedef QMap<QString, ImageComposer_Operator*> FactoryOperators;
  
  FactoryOperators myOps; ///< all operators stored by a factory
};

/**
 * Macro that is used for registered operators, see C++ of this class to see
 * example of hte registration.
 */
#define REGISTER_HYDRO_OPERATION(operation_name) \
  HYDROData_OperationsFactory::Factory()->Register(new operation_name);

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_ImmersibleZone.h"

#include "HYDROData_IAltitudeObject.h"
#include "HYDROData_Document.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_ShapesTool.h"

#include <HYDROData_Tool.h>

#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Compound.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_HSequenceOfShape.hxx>


#include <ShapeAnalysis.hxx>
#include <ShapeAnalysis_FreeBounds.hxx>


#include <QColor>
#include <QStringList>

#define DEB_IMZ
#include <BRepTools.hxx>
//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

//#define HYDRODATA_IMZONE_DEB 1

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_ImmersibleZone,HYDROData_NaturalObject)


HYDROData_ImmersibleZone::HYDROData_ImmersibleZone()
: HYDROData_NaturalObject( Geom_2d )
{
}

HYDROData_ImmersibleZone::~HYDROData_ImmersibleZone()
{
}

QStringList HYDROData_ImmersibleZone::DumpToPython( const QString& thePyScriptPath,
                                                    MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  
  QString aZoneName = GetObjPyName();

  Handle(HYDROData_IAltitudeObject) aRefAltitude = GetAltitudeObject();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefAltitude, "SetAltitudeObject" );

  Handle(HYDROData_PolylineXY) aRefPolyline = GetPolyline();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefPolyline, "SetPolyline" );

  if (!this->IsSubmersible())
    {
      aResList << QString( "%1.SetIsSubmersible(False)" ).arg( aZoneName );
    }

  aResList << QString( "" );

  aResList << QString( "%1.Update()" ).arg( aZoneName );
  aResList << QString( "" );

  return aResList;
}

HYDROData_SequenceOfObjects HYDROData_ImmersibleZone::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_NaturalObject::GetAllReferenceObjects();

  Handle(HYDROData_PolylineXY) aRefPolyline = GetPolyline();
  if ( !aRefPolyline.IsNull() )
    aResSeq.Append( aRefPolyline );

  return aResSeq;
}

void HYDROData_ImmersibleZone::Update()
{
  HYDROData_NaturalObject::Update();
  
  RemoveGroupObjects();
  TopoDS_Shape aResShape = generateTopShape();
  SetTopShape( aResShape );

  createGroupObjects();
}

bool HYDROData_ImmersibleZone::IsHas2dPrs() const
{
  return true;
}

TopoDS_Shape HYDROData_ImmersibleZone::generateTopShape() const
{
  return generateTopShape( GetPolyline() );
}

TopoDS_Shape HYDROData_ImmersibleZone::generateTopShape( const Handle(HYDROData_PolylineXY)& aPolyline )
{
  return HYDROData_Tool::PolyXY2Face(aPolyline);
}

void HYDROData_ImmersibleZone::createGroupObjects()
{
  TopoDS_Shape aZoneShape = GetTopShape();
  
  // Temporary solution while the restriction for polylines is not implemented
  // and shape for zone can be compound and not face only
  if ( !aZoneShape.IsNull() && aZoneShape.ShapeType() != TopAbs_FACE )
  {
    TopExp_Explorer aZoneFaceExp( aZoneShape, TopAbs_FACE );
    if ( aZoneFaceExp.More() )
      aZoneShape = aZoneFaceExp.Current(); // Take only first face into account
  }

  if ( aZoneShape.IsNull() || aZoneShape.ShapeType() != TopAbs_FACE )
    return;

  TopoDS_Face aZoneFace = TopoDS::Face( aZoneShape );
  
  TopoDS_Wire aZoneOuterWire = ShapeAnalysis::OuterWire( aZoneFace );

  // Create outer edges group
  QString anOutWiresGroupName = GetName() + "_Outer";

  Handle(HYDROData_ShapesGroup) anOutWiresGroup = createGroupObject();
  anOutWiresGroup->SetName( anOutWiresGroupName );

  TopTools_SequenceOfShape anOuterEdges;
  HYDROData_ShapesTool::ExploreShapeToShapes( aZoneOuterWire, TopAbs_EDGE, anOuterEdges );
  anOutWiresGroup->SetShapes( anOuterEdges );

  int anInnerCounter = 1;
  TopExp_Explorer aZoneFaceExp( aZoneFace, TopAbs_WIRE );
  for ( ; aZoneFaceExp.More(); aZoneFaceExp.Next() )
  {
    TopoDS_Wire aZoneWire = TopoDS::Wire( aZoneFaceExp.Current() );
    if ( aZoneWire.IsEqual( aZoneOuterWire ) )
      continue; // Skip the outer wire

    TopTools_SequenceOfShape anInnerEdges;
    HYDROData_ShapesTool::ExploreShapeToShapes( aZoneWire, TopAbs_EDGE, anInnerEdges );
    if ( anInnerEdges.IsEmpty() )
      continue;

    QString anInWiresGroupName = GetName() + "_Inner_" + QString::number( anInnerCounter++ );

    Handle(HYDROData_ShapesGroup) anInWiresGroup = createGroupObject();
    anInWiresGroup->SetName( anInWiresGroupName );

    anInWiresGroup->SetShapes( anInnerEdges );
  }  
}

TopoDS_Shape HYDROData_ImmersibleZone::GetShape3D() const
{
  return GetTopShape();
}

QColor HYDROData_ImmersibleZone::DefaultFillingColor() const
{
  return QColor( Qt::darkBlue );
}

QColor HYDROData_ImmersibleZone::DefaultBorderColor() const
{
  return QColor( Qt::transparent );
}

void HYDROData_ImmersibleZone::SetPolyline( const Handle(HYDROData_PolylineXY)& thePolyline )
{
  if( IsEqual( GetPolyline(), thePolyline ) )
    return;

  SetReferenceObject( thePolyline, DataTag_Polyline );
  Changed( Geom_2d );
}

Handle(HYDROData_PolylineXY) HYDROData_ImmersibleZone::GetPolyline() const
{
  return Handle(HYDROData_PolylineXY)::DownCast( 
           GetReferenceObject( DataTag_Polyline ) );
}

void HYDROData_ImmersibleZone::RemovePolyline()
{
  ClearReferenceObjects( DataTag_Polyline );
  Changed( Geom_2d );
}

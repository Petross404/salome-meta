// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Zone.h"

#include "HYDROData_ArtificialObject.h"
#include "HYDROData_IAltitudeObject.h"
#include "HYDROData_Document.h"
#include "HYDROData_NaturalObject.h"
#include <QStringList>
#include <TDataStd_Integer.hxx>
#include <TopoDS_Shape.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Zone, HYDROData_Entity)


HYDROData_Zone::HYDROData_Zone()
: HYDROData_Entity( Geom_2d )
{
  myInterpolator = NULL;
}

HYDROData_Zone::~HYDROData_Zone()
{
}

bool HYDROData_Zone::CanBeUpdated() const
{
  return false;
}

bool HYDROData_Zone::IsHas2dPrs() const
{
  return true;
}

bool HYDROData_Zone::CanRemove()
{
  return false;
}

HYDROData_SequenceOfObjects HYDROData_Zone::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Entity::GetAllReferenceObjects();

  HYDROData_SequenceOfObjects aSeqOfObjects = GetObjects();
  aResSeq.Append( aSeqOfObjects );

  return aResSeq;
}

void HYDROData_Zone::SetShape( const TopoDS_Shape& theShape )
{
  HYDROData_Entity::SetShape( DataTag_Shape, theShape );
}

TopoDS_Shape HYDROData_Zone::GetShape() const
{
  return HYDROData_Entity::GetShape( DataTag_Shape );
}

bool HYDROData_Zone::IsMergingNeed() const
{
  // Zones based on geometry objects (compare altitudes)
  Handle(HYDROData_IAltitudeObject) aRefAltitude;

  HYDROData_SequenceOfObjects anObjects = GetObjects();
  HYDROData_SequenceOfObjects::Iterator anObjsIter( anObjects );
  for ( ; anObjsIter.More(); anObjsIter.Next() )
  {
    Handle(HYDROData_Object) aRefGeomObj =
      Handle(HYDROData_Object)::DownCast( anObjsIter.Value() );
    if ( aRefGeomObj.IsNull() )
      continue;

    Handle(HYDROData_IAltitudeObject) anObjAltitude = aRefGeomObj->GetAltitudeObject();
    if ( anObjAltitude.IsNull() )
      continue;

    if ( aRefAltitude.IsNull() )
    {
      aRefAltitude = anObjAltitude;
      continue;
    }

    if ( !IsEqual( aRefAltitude, anObjAltitude ) )
      return true;
  }

  return false;
}

void HYDROData_Zone::SetInterpolator( HYDROData_IInterpolator* theInter )
{
  myInterpolator = theInter;
}

HYDROData_IInterpolator* HYDROData_Zone::GetInterpolator() const
{
  return myInterpolator;
}

void HYDROData_Zone::SetMergeType( const MergeType& theType )
{
  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( myLab.FindChild( DataTag_MergeType ), (int)theType );
  anAttr->SetID(TDataStd_Integer::GetID());
}

HYDROData_Zone::MergeType HYDROData_Zone::GetMergeType() const
{
  MergeType aMergeType = Merge_UNKNOWN;
  
  TDF_Label aLabel = myLab.FindChild( DataTag_MergeType, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_Integer) anInt;
    if ( aLabel.FindAttribute( TDataStd_Integer::GetID(), anInt ) )
      aMergeType = (MergeType)anInt->Get();
  }

  return aMergeType;
}

void HYDROData_Zone::SetMergeObject( const Handle(HYDROData_Entity)& theObject )
{
  
  DEBTRACE("HYDROData_Zone::SetMergeObject " << theObject->GetName().toStdString());
  SetReferenceObject( theObject, DataTag_MergeObject );
}

Handle(HYDROData_Entity) HYDROData_Zone::GetMergeObject() const
{
  return GetReferenceObject( DataTag_MergeObject );
}

void HYDROData_Zone::RemoveMergeObject()
{
  ClearReferenceObjects( DataTag_MergeObject );
}

bool HYDROData_Zone::AddObject( const Handle(HYDROData_Entity)& theObject )
{
  if ( theObject.IsNull() )
    return false;
  
  if ( !theObject->IsKind( STANDARD_TYPE(HYDROData_ArtificialObject) ) &&
       !theObject->IsKind( STANDARD_TYPE(HYDROData_NaturalObject) ) )
    return false; // Wrong type of object

  if ( HasReference( theObject, DataTag_Object ) )
    return false; // Object is already in reference list

  AddReferenceObject( theObject, DataTag_Object );

  return true;
}

HYDROData_SequenceOfObjects HYDROData_Zone::GetObjects() const
{
  return GetReferenceObjects( DataTag_Object );
}

void HYDROData_Zone::RemoveObjects()
{
  ClearReferenceObjects( DataTag_Object );
}

bool HYDROData_Zone::IsSubmersible() const
{
  HYDROData_SequenceOfObjects anObjects = GetObjects();
  HYDROData_SequenceOfObjects::Iterator anObjsIter( anObjects );
  for ( ; anObjsIter.More(); anObjsIter.Next() )
  {
    Handle(HYDROData_Object) aRefGeomObj =
      Handle(HYDROData_Object)::DownCast( anObjsIter.Value() );
    if ( aRefGeomObj.IsNull() )
      continue;

    if( !aRefGeomObj->IsSubmersible() )
      return false; //if one of geometry objects is not submersible the zone is considered as not submersible
  }
  return true;
}


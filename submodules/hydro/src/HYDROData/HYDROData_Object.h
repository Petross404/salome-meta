// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Object_HeaderFile
#define HYDROData_Object_HeaderFile

#include <HYDROData_Entity.h>

class TopoDS_Shape;
class HYDROData_IAltitudeObject;
class HYDROData_DummyObject3D;
class HYDROData_ShapesGroup;

/**\class HYDROData_Object
 * \brief The base class for all geometrical objects in the HYDRO module.
 *
 */
class HYDROData_Object : public HYDROData_Entity
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_TopShape,
    DataTag_Shape3D,
    DataTag_AltitudeObject,      ///< reference altitude object
    DataTag_FillingColor,        ///< filling color of geometrical object
    DataTag_BorderColor,         ///< border color of geometrical object
    DataTag_Object3D,            ///< child 3D object
    DataTag_EdgesGroup,          ///< child group objects
    DataTag_ChildAltitudeObject, ///< child altitude object
    DataTag_IsSubmersible,       ///< the attribute "is submersible"
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Object, HYDROData_Entity);

  /**
   * Updates the name of this object.
   * Reimplemented to update the names child groups.
   */
  HYDRODATA_EXPORT virtual void SetName( const QString& theName, bool isDefault = false );

  /**
   * Update the geometry object.
   * Call this method whenever you made changes for object data.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;

  /**
   * Sets the "MustBeUpdated" flag: if object is depended on updated features.
   * Reimplemented to update the state of child 3D object.
   */
  HYDRODATA_EXPORT virtual void Changed( Geometry );


  /**
   * Sets the top(2d projection) shape of the object.
   */
  HYDRODATA_EXPORT virtual void SetTopShape( const TopoDS_Shape& theShape );

  /**
   * Returns the top shape of the object.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetTopShape() const;

  /**
   * Sets the 3d shape of the object.
   */
  HYDRODATA_EXPORT virtual void SetShape3D( const TopoDS_Shape& theShape );

  /**
   * Returns the 3d shape of the object.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape3D() const;

  /**
   * Returns reference object which represent the 3D shape of object.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_DummyObject3D) GetObject3D() const;


  /**
   * Returns sequence of object groups.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetGroups() const;

  /**
   * Returns group data model object by it id.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_ShapesGroup) GetGroup( const int theGroupId ) const;

  /**
   * Returns group id by data model object.
   */
  HYDRODATA_EXPORT virtual int GetGroupId( const Handle(HYDROData_ShapesGroup)& theGroup ) const;


  /**
   * Set reference altitude object for geometry object.
   */
  HYDRODATA_EXPORT virtual bool SetAltitudeObject( const Handle(HYDROData_IAltitudeObject)& theAltitude );

  /**
   * Returns reference altitude object of geometry object.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_IAltitudeObject) GetAltitudeObject() const;

  /**
   * Clear the reference altitude object for geometry object.
   */
  HYDRODATA_EXPORT virtual void RemoveAltitudeObject();


   /**
   * Sets filling color for object.
   */
  HYDRODATA_EXPORT virtual void SetFillingColor( const QColor& theColor );

  /**
   * Returns filling color of object.
   */
  HYDRODATA_EXPORT virtual QColor GetFillingColor() const;

   /**
   * Sets border color for object.
   */
  HYDRODATA_EXPORT virtual void SetBorderColor( const QColor& theColor );

  /**
   * Returns border color of object.
   */
  HYDRODATA_EXPORT virtual QColor GetBorderColor() const;

  HYDRODATA_EXPORT bool IsSubmersible() const;
  HYDRODATA_EXPORT void SetIsSubmersible( bool ) const;

  HYDRODATA_EXPORT virtual void GetBoundaries( QList<TopoDS_Shape>& theBoundShapes,
                                               QStringList& theBoundNames ) const;

protected:

  /**
   * Creates new object in the internal data structure. Use higher level objects
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Object( Geometry );

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_Object();

protected:
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;

  /**
   * Dump the initial object creation to a Python script.
   * Reimplemented to dump the object colors.
   */
  HYDRODATA_EXPORT virtual QStringList dumpObjectCreation( MapOfTreatedObjects& theTreatedObjects ) const;


  /**
   * Checks and if necessary create child 3D object.
   * Reimplement this function in your subclass if you
   * do not want to create child 3D object.
   */
  HYDRODATA_EXPORT virtual void checkAndSetObject3D();


  /**
   * Returns the type of child altitude object.
   * Base implementation returns KIND_UNKNOWN, it means that child altitude
   * object will not be created inside of checkAndSetAltitudeObject() function.
   * Reimplement this function in your subclass an return correct altitude
   * object type if you want to create child altitude object.
   */
  HYDRODATA_EXPORT virtual ObjectKind getAltitudeObjectType() const;

  /**
   * Checks and if necessary create child altitude object.
   */
  HYDRODATA_EXPORT virtual void checkAndSetAltitudeObject();

  /**
   * Return the child altitude object.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_IAltitudeObject) getChildAltitudeObject() const;


  /**
   * Create new one child group object.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_ShapesGroup) createGroupObject();

  /**
   * Remove all child group objects.
   */
  HYDRODATA_EXPORT virtual void RemoveGroupObjects();

  /**
   * Removes the top shape from data label of the object.
   */
  HYDRODATA_EXPORT void RemoveTopShape();

  /**
   * Removes the 3d shape from data label of the object.
   */
  HYDRODATA_EXPORT void RemoveShape3D();
};

#endif

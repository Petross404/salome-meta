// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Lambert93_HeaderFile
#define HYDROData_Lambert93_HeaderFile

#include <HYDROData.h>

class HYDROData_Lambert93
{
public:
  static double HYDRODATA_EXPORT  toRad( double theDeg );
  static double HYDRODATA_EXPORT  toDeg( double theRad );

  static void HYDRODATA_EXPORT    toXY( double theLatitudeDeg, double theLongitudeDeg,
                                        double& theX, double& theY );

  static void HYDRODATA_EXPORT    toGeo( double theX, double theY,
                                         double& theLatitudeDeg, double& theLongitudeDeg,
                                         double theEps = 1e-5 );

  static void HYDRODATA_EXPORT    DMSToDeg( int theDeg,
                                            int theMin,
                                            double theSec,
                                            double& theDegOut );

  static void HYDRODATA_EXPORT    DMSToSec( int theDeg,
                                            int theMin,
                                            double theSec,
                                            double& theSecOut );

  static void HYDRODATA_EXPORT    degToDMS( double theDegIn,
                                            int& theDeg,
                                            int& theMin,
                                            double& theSec );

  static void HYDRODATA_EXPORT    secToDMS( double theSecIn,
                                            int& theDeg,
                                            int& theMin,
                                            double& theSec );

private:
  static double                   calc_rho( double phi );
  static double                   calc_phi_inv( double rho, double eps );
  static double                   calc_phi_ign( double rho, double eps );
};

#endif

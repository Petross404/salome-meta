// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_TopoCurve.h>

#include <Approx_Curve3d.hxx>
#include <BRep_Builder.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAdaptor_HCurve.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <Extrema_ExtCC.hxx>
#include <Extrema_ExtPC.hxx>
#include <BRepExtrema_DistShapeShape.hxx>
#include <GeomAPI_Interpolate.hxx>
#include <Geom_BSplineCurve.hxx>
#include <Precision.hxx>
#include <ShapeAnalysis_TransferParametersProj.hxx>
#include <ShapeBuild_Edge.hxx>
#include <TColgp_Array1OfVec.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <TColStd_HArray1OfBoolean.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Wire.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>
#include <TopTools_ListOfShape.hxx>

#include <iostream>
#include <sstream>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"
#include <BRepTools.hxx>

/*! The type is intended to traverse the container
 *  either from the begin to the end or vice versa.
 */
template<typename ContainerType, typename IteratorType>
class Iterator
{
private:
  IteratorType myIterator; //!< The iterator.
  IteratorType myIteratorLimit; //!< The iterator limit.

  //! The pointer to the method to traverse the next item.
  IteratorType& (IteratorType::*myNext)();

public:
  //! The constructor.
  Iterator(
    const ContainerType& theContainer,
    const bool theIsForward)
  {
    if (theIsForward)
    {
      myIterator = theContainer.begin();
      myIteratorLimit = theContainer.end();
      myNext = &IteratorType::operator++;
    }
    else
    {
      myIterator = --theContainer.end();
      myIteratorLimit = --theContainer.begin();
      myNext = &IteratorType::operator--;
    }
  }

  //! Returna 'true' if the container contains not yet traversed item.
  bool More() const
  {
    return myIterator != myIteratorLimit;
  }

  //! Traverses to the next item.
  IteratorType& operator ++()
  {
    return (myIterator.*myNext)();
  }

  //! Returns the iterator.
  IteratorType& operator *() {return myIterator;}
};

/*! Inserts the value after the position.
 *
 */
template<typename ItemType> static void InsertAfter(
  const typename std::list<ItemType>::iterator& thePosition,
  const ItemType& theValue,
  std::list<ItemType>& theList)
{
  typename std::list<ItemType>::iterator aEIt2 = thePosition;
  if (++aEIt2 != theList.end())
  {
    theList.insert(aEIt2, theValue);
  }
  else
  {
    theList.push_back(theValue);
  }
}

/*! Converts the curve to a smooth cubic B-spline using the deflection.
 *
 */
static Handle(Geom_BSplineCurve) BSpline(
  const BRepAdaptor_Curve& theCurve, const double theDeflection)
{
  Handle(BRepAdaptor_HCurve) aCurve = new BRepAdaptor_HCurve(theCurve);
  Approx_Curve3d aConverter(aCurve, theDeflection, GeomAbs_C1, 4, 3);
  Handle(Geom_BSplineCurve) aBSpline;
  return aConverter.HasResult() ? aConverter.Curve() : aBSpline;
}

/*! Replaces the vertex of the edge considering the edge orientation.
 *
 */
static TopoDS_Edge ReplaceVertex(
  const TopoDS_Edge& theEdge, const bool theIsEndVertex)
{
  TopoDS_Vertex aVertices[] = {
    TopExp::FirstVertex(theEdge, Standard_True),
    TopExp::LastVertex(theEdge, Standard_True)};
  aVertices[theIsEndVertex ? 1 : 0].EmptyCopy();
  TopoDS_Edge aNewEdge = TopoDS::Edge(theEdge.Oriented(TopAbs_FORWARD));
  aNewEdge =
    ShapeBuild_Edge().CopyReplaceVertices(aNewEdge, aVertices[0], aVertices[1]);
  aNewEdge.Orientation(theEdge.Orientation());
  return aNewEdge;
}

/*! Projects the point to the curve.
 *
 */
double ProjectPointToCurve(
  const gp_XYZ& thePoint,
  const Adaptor3d_Curve& theCurve,
  double& theParameter)
{
  // Calculate the nearest curve internal extremum.
  Extrema_ExtPC aAlgo(thePoint, theCurve);
  int aMinEN = -2;
  double aMinSqDist = DBL_MAX;
  if (aAlgo.IsDone())
  {
    const int aECount = aAlgo.NbExt();
    for (int aEN = 1; aEN <= aECount; ++aEN)
    {
      const gp_XYZ& aP = aAlgo.Point(aEN).Value().XYZ();
      const double aSqDist = (thePoint - aP).SquareModulus();
      if (aMinSqDist > aSqDist)
      {
        aMinSqDist = aSqDist;
        aMinEN = aEN;
      }
    }
  }

  // Calculate the nearest curve end extremum.
  const double aParams[] =
    {theCurve.FirstParameter(), theCurve.LastParameter()};
  const gp_XYZ aEnds[] =
    {theCurve.Value(aParams[0]).XYZ(), theCurve.Value(aParams[1]).XYZ()};
  const double aSqDists[] = {
    (thePoint - aEnds[0]).SquareModulus(),
    (thePoint - aEnds[1]).SquareModulus()};
  for (int aEI = 0; aEI < 2; ++aEI)
  {
    if (aMinSqDist > aSqDists[aEI])
    {
      aMinSqDist = aSqDists[aEI];
      aMinEN = -aEI;
    }
  }

  if (aMinEN <= 0)
  {
    theParameter = aParams[-aMinEN];
    return aMinSqDist;
  }

  const Extrema_POnCurv& aPOnC = aAlgo.Point(aMinEN);
  const gp_XYZ& aP = aPOnC.Value().XYZ();
  theParameter = aPOnC.Parameter();
  for (int aEI = 0; aEI < 2; ++aEI)
  {
    if (Abs(theParameter - aParams[aEI]) < Precision::PConfusion() ||
      (aP - aEnds[aEI]).SquareModulus() < Precision::SquareConfusion())
    {
      theParameter = aParams[aEI];
    }
  }
  return aMinSqDist;
}

// Projects the point to the edge.
static double ProjectPointToEdge(
  const gp_XYZ& thePoint, const TopoDS_Edge& theEdge, double& theParameter)
{
  return ProjectPointToCurve(thePoint, BRepAdaptor_Curve(theEdge), theParameter);
}

/*! Adds the parameter to the curve parameter list.
 *
 */
static int AddParameter(
  const Adaptor3d_Curve& theCurve,
  const double theParameter,
  std::list<double>& theParameters)
{
  // Check the coincidence.
  std::list<double> aEndParams;
  aEndParams.push_back(theCurve.FirstParameter());
  aEndParams.push_back(theCurve.LastParameter());
  std::list<double>* aParams[] = {&theParameters, &aEndParams};
  const gp_XYZ aPoint = theCurve.Value(theParameter).XYZ();
  for (int aLI = 0; aLI < 2; ++aLI)
  {
    std::list<double>::iterator aPIt = aParams[aLI]->begin();
    std::list<double>::iterator aLastPIt = aParams[aLI]->end();
    for (int aPI = 0; aPIt != aLastPIt; ++aPI, ++aPIt)
    {
      const double aParam = *aPIt;
      if (Abs(theParameter - aParam) < Precision::PConfusion() ||
        (theCurve.Value(aParam).XYZ() - aPoint).SquareModulus() <=
          Precision::SquareConfusion())
      {
        int aIntCount = 0;
        if (aLI != 0)
        {
          if (aPI == 0)
          {
            theParameters.push_front(aEndParams.front());
          }
          else
          {
            theParameters.push_back(aEndParams.back());
          }
          ++aIntCount;
        }
        return aIntCount;
      }
    }
  }

  // Calculate the position to insert.
  std::list<double>::iterator aPIt = theParameters.begin();
  std::list<double>::iterator aLastPIt = theParameters.end();
  if (aPIt != aLastPIt && *aPIt < theParameter)
  {
    for (++aPIt; aPIt != aLastPIt && *aPIt < theParameter; ++aPIt);
    if (aPIt != aLastPIt)
    {
      theParameters.insert(aPIt, theParameter);
    }
    else
    {
      theParameters.push_back(theParameter);
    }
  }
  else
  {
    theParameters.push_front(theParameter);
  }
  return 1;
}

/*! Intersects the first curve by the second one and
 *  adds the intersection parameters to the ordered list.
 */
static int IntersectShape(
    const TopoDS_Edge& theEdge1,
    const TopoDS_Edge& theEdge2,
    std::list<double>& theParameters)
{
    if (theEdge1.IsSame(theEdge2))
        Standard_ConstructionError::Raise("The lines to make intersection must be different");

    int nbSols = 0;
    BRepAdaptor_Curve aCurve1 = BRepAdaptor_Curve(theEdge1);

    // --- Calculate Lines Intersections Points: two times, changing the order (sometimes intersections not detected)

    BRepExtrema_DistShapeShape dst(theEdge1, theEdge2);  // first order
    if (dst.IsDone())
    {
        DEBTRACE("nb solutions found: " << dst.NbSolution());
        gp_Pnt P1, P2;
        for (int i = 1; i <= dst.NbSolution(); i++)
        {
            P1 = dst.PointOnShape1(i);
            P2 = dst.PointOnShape2(i);
            Standard_Real Dist = P1.Distance(P2);
            DEBTRACE("distance solution "<< i << " : " << Dist);
            if (Dist <= Precision::Confusion())
            {
                double par1;
                dst.ParOnEdgeS1(i, par1);
                DEBTRACE("parameter: " << par1);
                nbSols += AddParameter(aCurve1, par1, theParameters); // add only new parameters
            }
            else
            {
                DEBTRACE("not an Intersection Point");
            }
        }
    }

    BRepExtrema_DistShapeShape dst2(theEdge2, theEdge1);  // second order
    if (dst2.IsDone())
    {
        DEBTRACE("nb solutions found: " << dst.NbSolution());
        gp_Pnt P1, P2;
        for (int i = 1; i <= dst2.NbSolution(); i++)
        {
            P1 = dst2.PointOnShape1(i);
            P2 = dst2.PointOnShape2(i);
            Standard_Real Dist = P1.Distance(P2);
            DEBTRACE("distance solution "<< i << " : " << Dist);
            if (Dist <= Precision::Confusion())
            {
                double par1;
                dst2.ParOnEdgeS2(i, par1);
                DEBTRACE("parameter: " << par1);
                nbSols += AddParameter(aCurve1, par1, theParameters); // add only new parameters
            }
            else
            {
                DEBTRACE("not an Intersection Point");
            }
        }
    }
    return nbSols;
}

/*! Intersects the first edge by the second one and
 *  adds the intersection parameters to the ordered list.
 */
static int IntersectEdge(
  const TopoDS_Edge& theEdge1,
  const TopoDS_Edge& theEdge2,
  std::list<double>& theParameters)
{
    return IntersectShape(theEdge1, theEdge2, theParameters);
}

/*! Returns the curve tangent in the position: 0 - start, 1 - end.
 *
 */
static gp_XYZ Tangent(const Adaptor3d_Curve& theCurve, const int thePosition)
{
  const Standard_Real aParam = (thePosition == 0) ?
    theCurve.FirstParameter() : theCurve.LastParameter();
  gp_Pnt aP;
  gp_Vec aV;
  theCurve.D1(aParam, aP, aV);
  Standard_Real aNorm = aV.Magnitude();
  aNorm = (aNorm >= Precision::PConfusion()) ? aNorm : 0;
  return ((1 / aNorm) * aV).XYZ();
}

/*! Returns the edge tangent in the position: 0 - start, 1 - end.
 *
 */
static gp_XYZ Tangent(const TopoDS_Edge& theEdge, const int thePosition)
{
  BRepAdaptor_Curve aCurve(theEdge);
  return Tangent(BRepAdaptor_Curve(theEdge), thePosition);
}

static bool Interpolate(
  const gp_XYZ thePoint1,
  const gp_XYZ thePoint2,
  const gp_XYZ theTangent1,
  const gp_XYZ theTangent2,
  Handle(Geom_BSplineCurve)& theBSpline)
{
  Handle(TColgp_HArray1OfPnt) aPs = new TColgp_HArray1OfPnt(1, 2);
  TColgp_Array1OfVec aTs(1, 2);
  Handle(TColStd_HArray1OfBoolean) aTFs = new TColStd_HArray1OfBoolean(1, 2);
  aPs->SetValue(1, thePoint1);
  aPs->SetValue(2, thePoint2);
  aTs.SetValue(1, theTangent1);
  aTs.SetValue(2, theTangent2);
  aTFs->SetValue(1, Standard_True);
  aTFs->SetValue(2, Standard_True);
  GeomAPI_Interpolate aInterpolator(aPs, Standard_False, 0);
  aInterpolator.Load(aTs, aTFs, Standard_False);
  aInterpolator.Perform();
  const bool aResult = (aInterpolator.IsDone() == Standard_True);
  if (aResult)
  {
    theBSpline = aInterpolator.Curve();
  }
  return aResult;
}

bool HYDROData_TopoCurve::Initialize(const TopoDS_Wire& theWire)
{
  // Check for non-emptiness.
  myEdges.clear();
  TopTools_IndexedDataMapOfShapeListOfShape aVertexToEdges;
  TopExp::MapShapesAndAncestors(theWire,
    TopAbs_VERTEX, TopAbs_EDGE, aVertexToEdges);
  const int aVCount = aVertexToEdges.Extent();   // number of edges (an edge can be a spline with intermediate vertices)
  DEBTRACE("initialize VCount= "<< aVCount);
  if (aVCount == 0)
  {
    return false;
  }

  // Check for 1 manifoldness.
  bool isClosed = false;
  {
    int aEndCount = 0;
    for (int aVN = 1; aVN <= aVCount; ++aVN)
    {
      const int aEdgeCount = aVertexToEdges.FindFromIndex(aVN).Extent();
      if (aEdgeCount == 1)
      {
        ++aEndCount;
      }
      if (aEdgeCount > 2)
      {
        return false;
      }
    }
    isClosed = (aEndCount == 0);
    if (!isClosed && aEndCount != 2)
    {
      return false;
    }
  }
  std::string brepName = "theWireToSplit";
  brepName += ".brep";
  BRepTools::Write( theWire, brepName.c_str() );

  // Find the start, i.e. the end which is the first vertex (or just the first vertex when closed) ==> aVN
  int aVN = 1;
  if (!isClosed)
  {
    for (; aVertexToEdges.FindFromIndex(aVN).Extent() == 2; ++aVN);
    if (!aVertexToEdges.FindKey(aVN).IsEqual(TopExp::FirstVertex(
      TopoDS::Edge(aVertexToEdges.FindFromIndex(aVN).First()), Standard_True)))
    {
      for (; aVertexToEdges.FindFromIndex(aVN).Extent() == 2; ++aVN);
    }
  }
  else
  {
    TopTools_ListOfShape& aEdges = aVertexToEdges.ChangeFromIndex(1);
    if (!aVertexToEdges.FindKey(aVN).IsEqual(
      TopExp::FirstVertex(TopoDS::Edge(aEdges.First()), Standard_True)))
    {
      const TopoDS_Shape aEdge = aEdges.First();
      aEdges.First() = aEdges.Last();
      aEdges.Last() = aEdge;
    }
  }

  // Calculate the edge order ==> list in myEdges
  TopTools_ListOfShape* aEdges = &aVertexToEdges.ChangeFromIndex(aVN); // 1 or 2 edges from first vertex
  while (!aEdges->IsEmpty())
  {
    const TopoDS_Edge aEdge = TopoDS::Edge(aEdges->First());
    aEdges->RemoveFirst();
    myEdges.push_back(aEdge);                                          // add an edge in the list
    int aVN2 = aVertexToEdges.FindIndex(TopExp::FirstVertex(aEdge));
    if (aVN2 == aVN)
    {
      aVN2 = aVertexToEdges.FindIndex(TopExp::LastVertex(aEdge));
    }
    aVN = aVN2;                                                        // next vertex

    aEdges = &aVertexToEdges.ChangeFromIndex(aVN2);
    const TopoDS_Edge aEdge2 = TopoDS::Edge(aEdges->First());
    if (aEdge2.IsEqual(aEdge))
    {
      aEdges->RemoveFirst();
    }
    else
    {
      aEdges->Clear();
      aEdges->Append(aEdge2);                                          // next edge
    }
  }

  // Check for connectedness and free vertex.
  return (aVCount - myEdges.size()) == (isClosed ? 0 : 1);
}

TopoDS_Wire HYDROData_TopoCurve::Wire() const
{
  TopoDS_Wire aWire;
  BRep_Builder aBuilder;
  aBuilder.MakeWire(aWire);
  std::list<TopoDS_Edge>::const_iterator aEItLast = myEdges.end();
  std::list<TopoDS_Edge>::const_iterator aEIt = myEdges.begin();
  for (; aEIt != aEItLast; ++aEIt)
  {
    aBuilder.Add(aWire, *aEIt);
  }
  return aWire;
}

bool HYDROData_TopoCurve::Cut(
  const std::list<TopoDS_Edge>::iterator& theEdgePosition,
  const double theParameter,
  HYDROData_TopoCurve& theCurve)
{
  bool aResult = false;

  // Locate the edge.
  std::list<TopoDS_Edge>::iterator aFirstEIt = myEdges.begin();
  std::list<TopoDS_Edge>::iterator aEIt = aFirstEIt;
  for (; aEIt != theEdgePosition; ++aEIt);

  // Cut the edge.
  TopoDS_Edge aEdge = *aEIt;
  BRepAdaptor_Curve aCurve(aEdge);
  int aParamI = -1;
  const double aEdgeEndParams[] =
    {aCurve.FirstParameter(), aCurve.LastParameter()};
  if (Abs(theParameter - aEdgeEndParams[0]) < Precision::PConfusion())
  {
    aParamI = 0;
  }
  else if (Abs(theParameter - aEdgeEndParams[1]) < Precision::PConfusion())
  {
    aParamI = 1;
  }
  const TopAbs_Orientation aOrient = aEdge.Orientation();
  if (aOrient == TopAbs_REVERSED)
  {
    aParamI ^= 1;
  }
  const bool isClosed = IsClosed();
  DEBTRACE("aParamI: " << aParamI << " isClosed: "<< isClosed);
  if (aParamI < 0)
  {
    aEdge.Orientation(TopAbs_FORWARD);
    TopoDS_Vertex aSplitV1, aSplitV2;
    BRep_Builder().MakeVertex(
      aSplitV1, aCurve.Value(theParameter), Precision::Confusion());
    BRep_Builder().MakeVertex(
      aSplitV2, aCurve.Value(theParameter), Precision::Confusion());
    TopoDS_Edge aEParts[] = {
      ShapeBuild_Edge().CopyReplaceVertices(aEdge, TopoDS_Vertex(),
        TopoDS::Vertex(aSplitV1.Oriented(TopAbs_REVERSED))),
      ShapeBuild_Edge().CopyReplaceVertices(aEdge, aSplitV2, TopoDS_Vertex())};
    ShapeBuild_Edge().CopyPCurves(aEParts[0], aEdge);
    ShapeBuild_Edge().CopyPCurves(aEParts[1], aEdge);
    BRep_Builder().SameRange(aEParts[0], Standard_False);
    BRep_Builder().SameRange(aEParts[1], Standard_False);
    BRep_Builder().SameParameter(aEParts[0], Standard_False);
    BRep_Builder().SameParameter(aEParts[1], Standard_False);
    ShapeAnalysis_TransferParametersProj aSATPP(aEdge, TopoDS_Face());
    aSATPP.SetMaxTolerance(Precision::Confusion());
    aSATPP.TransferRange(aEParts[0],
      aEdgeEndParams[0], theParameter, Standard_False);
    aSATPP.TransferRange(aEParts[1],
      theParameter, aEdgeEndParams[1], Standard_False);
    aEParts[0].Orientation(aOrient);
    aEParts[1].Orientation(aOrient);

    const int aFirstPI = (aOrient != TopAbs_REVERSED) ? 0 : 1;
    *aEIt = aEParts[aFirstPI];
    InsertAfter(aEIt, aEParts[1 - aFirstPI], myEdges);
    ++aEIt;

    aResult = true;
  }
  else
  {
    TopoDS_Edge aNewEdge = ReplaceVertex(aEdge, (aParamI == 0) ? false : true);
    *aEIt = aNewEdge;
    if (aParamI > 0)
    {
      ++aEIt;

      std::list<TopoDS_Edge>::iterator aEdgePosition = theEdgePosition;
      if (isClosed || ++aEdgePosition != myEdges.end())
      {
        aResult = true;
      }
    }
    else
    {
      if (isClosed || theEdgePosition != aFirstEIt)
      {
        aResult = true;
      }
    }
  }

  // Calculate the curve parts.
  std::list<TopoDS_Edge>::iterator aLastEIt = myEdges.end();
  if (aEIt != aFirstEIt && aEIt != aLastEIt)
  {
    std::list<TopoDS_Edge>* aEdges = !isClosed ? &theCurve.myEdges : &myEdges;
    aEdges->splice(aEdges->begin(), myEdges, aEIt, aLastEIt);
  }

  return aResult;
}

void HYDROData_TopoCurve::Cut(
  const std::list<TopoDS_Edge>::const_iterator& theEdgePosition,
  const double theParameter,
  HYDROData_TopoCurve& theCurve1,
  HYDROData_TopoCurve& theCurve2) const
{
  theCurve1 = *this;
  std::list<TopoDS_Edge>::const_iterator aEPos = myEdges.begin();
  std::list<TopoDS_Edge>::iterator aEPos1 = theCurve1.myEdges.begin();
  for (; aEPos != theEdgePosition; ++aEPos1, ++aEPos);
  theCurve1.Cut(aEPos1, theParameter, theCurve2);
}

bool HYDROData_TopoCurve::Cut(
  const std::deque<std::list<double> >& theParameters,
  std::deque<HYDROData_TopoCurve>& theCurves) const
{
  bool aResult = false;
  HYDROData_TopoCurve aCurves[2];
  aCurves[0] = *this;
  int aCI = 0;
  std::list<TopoDS_Edge>::iterator aEIt = aCurves[0].myEdges.begin();
  std::deque<std::list<double> >::const_iterator aPLIt = theParameters.begin();
  for (std::deque<std::list<double> >::const_iterator aLastPLIt =
    theParameters.end(); aPLIt != aLastPLIt; ++aPLIt)
  {
    TopoDS_Edge aNextEdge;
    {
      std::list<TopoDS_Edge>::iterator aNextEIt = aEIt;
      ++aNextEIt;
      if (aNextEIt != aCurves[aCI].myEdges.end())
      {
        aNextEdge = *aNextEIt;
      }
    }

    for (Iterator<std::list<double>, std::list<double>::const_iterator> aPIt(
      *aPLIt, (aEIt->Orientation() != TopAbs_REVERSED)); aPIt.More(); ++aPIt)
    {
      const int aCI1 = 1 - aCI;
      aResult |= aCurves[aCI].Cut(aEIt, **aPIt, aCurves[aCI1]);
      if (!aCurves[aCI1].IsEmpty())
      {
        theCurves.push_back(HYDROData_TopoCurve());
        theCurves.back().append(aCurves[aCI]);
        aEIt = aCurves[aCI1].myEdges.begin();
        aCI = aCI1;
      }
      else
      {
        aEIt = aCurves[aCI].myEdges.begin();
      }
    }

    if (!aNextEdge.IsNull() && !aEIt->IsEqual(aNextEdge))
    {
      ++aEIt;
    }
  }
  theCurves.push_back(aCurves[aCI]);
  return aResult;
}

double HYDROData_TopoCurve::Project(
  const gp_XYZ& thePoint,
  std::list<TopoDS_Edge>::const_iterator& theEdgeIterator,
  double& theParameter) const
{
  double aMinSqDist = DBL_MAX;
  std::list<TopoDS_Edge>::const_iterator aEIt = myEdges.begin();
  std::list<TopoDS_Edge>::const_iterator aLastEIt = myEdges.end();
  for (; aEIt != aLastEIt; ++aEIt)
  {
    double aParam;
    const double aSqDist = ProjectPointToEdge(thePoint, *aEIt, aParam);
    if (aMinSqDist > aSqDist)
    {
      aMinSqDist = aSqDist;
      theEdgeIterator = aEIt;
      theParameter = aParam;
    }
  }
  return aMinSqDist;
}

int HYDROData_TopoCurve::Intersect(
  const TopoDS_Wire& theWire,
  std::deque<std::list<double> >& theParameters) const
{
  std::string brepName = "theWireTool";
  brepName += ".brep";
  BRepTools::Write( theWire, brepName.c_str() );

  int aIntCount = 0;
  theParameters.resize(myEdges.size());
  DEBTRACE("myEdges.size() " << myEdges.size());
  std::list<TopoDS_Edge>::const_iterator aEIt = myEdges.begin();
  std::list<TopoDS_Edge>::const_iterator aLastEIt = myEdges.end();
  std::deque<std::list<double> >::iterator aPIt = theParameters.begin();
  for (; aEIt != aLastEIt; ++aPIt, ++aEIt)
  {
    DEBTRACE("---");
    const TopoDS_Edge& aEdge = *aEIt;
    std::list<double>& aParams = *aPIt;
    TopExp_Explorer aEIt2(theWire, TopAbs_EDGE);
    for (; aEIt2.More(); aEIt2.Next())
    {
      aIntCount += IntersectEdge(aEdge,TopoDS::Edge(aEIt2.Current()), aParams);
      DEBTRACE("aParams.size() " << aParams.size());
    }
  }
  DEBTRACE("aIntCount " << aIntCount);
  return aIntCount;
}

void HYDROData_TopoCurve::CloseCurve()
{
  const TopoDS_Vertex aVertex = TopoDS::Vertex(TopExp::LastVertex(
    myEdges.back(), Standard_True).Oriented(TopAbs_FORWARD));
  TopoDS_Edge& aEdge = myEdges.front();
  const TopoDS_Edge aForwardEdge = TopoDS::Edge(aEdge.Oriented(TopAbs_FORWARD));
  aEdge = TopoDS::Edge(ShapeBuild_Edge().CopyReplaceVertices(
    aForwardEdge, aVertex, TopoDS_Vertex()).Oriented(aEdge.Orientation()));
}

void HYDROData_TopoCurve::Merge(
  const int thePosition, HYDROData_TopoCurve& theCurve)
{
  if (thePosition == 0)
  {
    const TopoDS_Vertex aVertex = TopoDS::Vertex(TopExp::LastVertex(
      theCurve.myEdges.back(), Standard_True).Oriented(TopAbs_FORWARD));
    TopoDS_Edge& aEdge = myEdges.front();
    aEdge = TopoDS::Edge(ShapeBuild_Edge().CopyReplaceVertices(
        TopoDS::Edge(aEdge.Oriented(TopAbs_FORWARD)), aVertex, TopoDS_Vertex()).
      Oriented(aEdge.Orientation()));
    prepend(theCurve);
  }
  else
  {
    const TopoDS_Vertex aVertex = TopoDS::Vertex(TopExp::FirstVertex(
      theCurve.myEdges.front(), Standard_True).Oriented(TopAbs_REVERSED));
    TopoDS_Edge& aEdge = myEdges.back();
    aEdge = TopoDS::Edge(ShapeBuild_Edge().CopyReplaceVertices(
        TopoDS::Edge(aEdge.Oriented(TopAbs_FORWARD)), TopoDS_Vertex(), aVertex).
      Oriented(aEdge.Orientation()));
    append(theCurve);
  }
}

void HYDROData_TopoCurve::Merge(
  const double theTolerance, std::deque<HYDROData_TopoCurve>& theCurves)
{
  // Process the curve closeness.
  const double aSqTol = theTolerance * theTolerance;
  const gp_XYZ aPs[] = {
    BRep_Tool::Pnt(TopExp::FirstVertex(myEdges.front(), Standard_True)).XYZ(),
    BRep_Tool::Pnt(TopExp::LastVertex(myEdges.back(), Standard_True)).XYZ()};
  bool isClosed = IsClosed();
  if (!isClosed && (aPs[0] - aPs[1]).SquareModulus() <= aSqTol)
  {
    CloseCurve();
    isClosed = true;
  }

  // Find the merge places.
  HYDROData_TopoCurve* aCurves[] = {NULL, NULL};
  int aOrder = 0;
  if (!isClosed)
  {
    std::deque<HYDROData_TopoCurve>::iterator aCIt = theCurves.begin();
    std::deque<HYDROData_TopoCurve>::iterator aLastCIt = theCurves.end();
    for (; aCIt != aLastCIt; ++aCIt)
    {
      HYDROData_TopoCurve& aCurve = *aCIt;
      if (aCurve.IsEmpty() || aCurve.IsClosed())
      {
        continue;
      }

      const gp_XYZ aP1 = BRep_Tool::Pnt(
        TopExp::FirstVertex(aCurve.myEdges.front(), Standard_True)).XYZ();
      if (aCurves[0] == NULL && (aPs[1] - aP1).SquareModulus() <= aSqTol)
      {
        aCurves[0] = &aCurve;
      }

      const gp_XYZ aP2 = BRep_Tool::Pnt(
        TopExp::LastVertex(aCurve.myEdges.back(), Standard_True)).XYZ();
      if (aCurves[1] == NULL && (aPs[0] - aP2).SquareModulus() <= aSqTol)
      {
        aCurves[1] = &aCurve;
        aOrder = (aCurves[0] == NULL) ? 1 : 0;
      }
    }
  }

  if (aCurves[0] == NULL && aCurves[1] == NULL)
  {
    theCurves.push_back(HYDROData_TopoCurve());
    theCurves.back().append(*this);
  }
  else if (aCurves[1] == NULL)
  {
    aCurves[0]->Merge(0, *this);
  }
  else if (aCurves[0] == NULL)
  {
    aCurves[1]->Merge(1, *this);
  }
  else
  {
    aCurves[aOrder]->Merge(aOrder, *this);
    if (aCurves[0] != aCurves[1])
    {
      aCurves[aOrder]->Merge(aOrder, *aCurves[1 - aOrder]);
    }
    else
    {
      aCurves[aOrder]->CloseCurve();
    }
  }
}

bool HYDROData_TopoCurve::Connect(
  const Standard_Real theTolerance, std::deque<HYDROData_TopoCurve>& theCurves)
{
  const double aSqTol = theTolerance * theTolerance;
  const gp_XYZ aPs[] = {
    BRep_Tool::Pnt(TopExp::FirstVertex(myEdges.front(), Standard_True)).XYZ(),
    BRep_Tool::Pnt(TopExp::LastVertex(myEdges.back(), Standard_True)).XYZ()};
  bool isClosed = IsClosed();
  if (!isClosed && (aPs[0] - aPs[1]).SquareModulus() <= aSqTol)
  {
    CloseCurve();
    isClosed = true;
  }

  if (!isClosed)
  {
    std::deque<HYDROData_TopoCurve>::iterator aCIt = theCurves.begin();
    std::deque<HYDROData_TopoCurve>::iterator aLastCIt = theCurves.end();
    for (; aCIt != aLastCIt; ++aCIt)
    {
      HYDROData_TopoCurve& aCurve2 = *aCIt;
      if (aCurve2.IsEmpty() || aCurve2.IsClosed())
      {
        continue;
      }

      const TopoDS_Edge* aEdges2[] =
        {&aCurve2.myEdges.front(), &aCurve2.myEdges.back()};
      const gp_XYZ aPs2[] = {
        BRep_Tool::Pnt(TopExp::FirstVertex(*aEdges2[0], Standard_True)).XYZ(),
        BRep_Tool::Pnt(TopExp::LastVertex(*aEdges2[1], Standard_True)).XYZ()};
      const double aSqDists[] =
        {(aPs[1] - aPs2[0]).SquareModulus(), (aPs[0] - aPs2[1]).SquareModulus()};
      const int aOrder = (aSqDists[0] <= aSqDists[1]) ? 0 : 1;
      if (aSqDists[aOrder] > aSqTol)
      {
        const TopoDS_Edge& aEdge =
          (aOrder == 0) ? myEdges.back() : myEdges.front();
        const gp_XYZ aPs3[] = {aPs[1 - aOrder], aPs2[aOrder]};
        const gp_XYZ aTs[] =
          {Tangent(aEdge, 1 - aOrder), Tangent(*aEdges2[aOrder], aOrder)};
        Handle(Geom_BSplineCurve) aBSpline;
        if (!Interpolate(aPs3[aOrder], aPs3[1 - aOrder],
          aTs[aOrder], aTs[1 - aOrder], aBSpline))
        {
          return false;
        }

        HYDROData_TopoCurve aECurve = BRepBuilderAPI_MakeEdge(aBSpline).Edge();
        aCurve2.Merge(aOrder, aECurve);
      }
      aCurve2.Merge(aOrder, *this);
      if (aSqDists[1 - aOrder] <= aSqTol)
      {
        aCurve2.CloseCurve();
      }
      return true;
    }
  }

  theCurves.push_back(HYDROData_TopoCurve());
  theCurves.back().append(*this);
  return true;
}

int HYDROData_TopoCurve::BSplinePiecewiseCurve(
  const double theDeflection, HYDROData_TopoCurve& theCurve) const
{
  int aPieceCount = 0;
  std::list<TopoDS_Edge>::const_iterator aLastEIt = myEdges.end();
  std::list<TopoDS_Edge>::const_iterator aEIt = myEdges.begin();
  TopoDS_Vertex aEndVertex;
  TopoDS_Edge aPrevEdge;
  for (; aEIt != aLastEIt; ++aEIt)
  {
    Handle(Geom_BSplineCurve) aBSpline =
      ::BSpline(BRepAdaptor_Curve(*aEIt), theDeflection);
    if (aBSpline.IsNull())
    {
      return 0;
    }

    if (aEIt->Orientation() == TopAbs_REVERSED)
    {
      aBSpline->Reverse();
    }

    TopoDS_Edge aEdge;
    BRep_Builder().MakeEdge(aEdge, aBSpline, Precision::Confusion());
    TopoDS_Vertex aVertex;
    BRep_Builder().MakeVertex(aVertex,
      aBSpline->Value(aBSpline->FirstParameter()), Precision::Confusion());
    if (!aPrevEdge.IsNull())
    {
      BRep_Builder().Add(aPrevEdge, aVertex.Oriented(TopAbs_REVERSED));
    }
    else
    {
      aEndVertex = aVertex;
    }
    BRep_Builder().Add(aEdge, aVertex);
    theCurve.myEdges.push_back(aEdge);
    aPieceCount += aBSpline->NbKnots() - 1;
    aPrevEdge = aEdge;
  }

  if (!IsClosed())
  {
    BRepAdaptor_Curve aCurve(aPrevEdge);
    BRep_Builder().MakeVertex(aEndVertex,
      aCurve.Value(aCurve.LastParameter()), Precision::Confusion());
  }
  BRep_Builder().Add(aPrevEdge, aEndVertex.Oriented(TopAbs_REVERSED));
  return aPieceCount;
}

bool HYDROData_TopoCurve::ValuesInKnots(std::list<gp_XYZ>& theValues) const
{
  std::list<TopoDS_Edge>::const_iterator aLastEIt = myEdges.end();
  std::list<TopoDS_Edge>::const_iterator aEIt = myEdges.begin();
  for (; aEIt != aLastEIt; ++aEIt)
  {
    Handle(Geom_BSplineCurve) aCurve;
    {
      TopLoc_Location aLoc;
      double aParams[2];
      aCurve = Handle(Geom_BSplineCurve)::
        DownCast(BRep_Tool::Curve(*aEIt, aLoc, aParams[0], aParams[1]));
      if (!aLoc.IsIdentity() || aEIt->Orientation() != TopAbs_FORWARD ||
        aCurve.IsNull())
      {
        return false;
      }
    }

    for (int aNbKnots = aCurve->NbKnots(), aKN = 1; aKN < aNbKnots; ++aKN)
    {
      theValues.push_back(aCurve->Value(aCurve->Knot(aKN)).XYZ());
    }
  }

  if (!IsClosed())
  {
    TopLoc_Location aLoc;
    double aParams[2];
    Handle(Geom_Curve) aCurve =
      BRep_Tool::Curve(myEdges.back(), aLoc, aParams[0], aParams[1]);
    theValues.push_back(aCurve->Value(aParams[1]).XYZ());
  }
  return true;
}

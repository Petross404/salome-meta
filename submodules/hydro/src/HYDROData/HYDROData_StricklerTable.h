// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_STRICKLERTABLE_HeaderFile
#define HYDROData_STRICKLERTABLE_HeaderFile

#include <HYDROData_Entity.h>

#include <TColStd_SequenceOfExtendedString.hxx>

class TDataStd_NamedData;

class HYDROData_StricklerTable : public HYDROData_Entity
{
protected:
  friend class HYDROData_Iterator;

  enum DataTag
  {
    DataTag_Table = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_AttrName,
  };

  HYDRODATA_EXPORT HYDROData_StricklerTable();
  HYDRODATA_EXPORT ~HYDROData_StricklerTable();

public:
  DEFINE_STANDARD_RTTIEXT( HYDROData_StricklerTable, HYDROData_Entity );

  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const;

  HYDRODATA_EXPORT bool Import( const QString& theFileName );
  HYDRODATA_EXPORT bool Export( const QString& theFileName );

  HYDRODATA_EXPORT double Get( const QString& theType, double theDefault ) const;
  HYDRODATA_EXPORT void Set( const QString& theType, double theCoefficient );

  HYDRODATA_EXPORT QStringList GetTypes() const;
  HYDRODATA_EXPORT void GetCoefficientRange( double& theMin, double& theMax ) const;

  HYDRODATA_EXPORT bool HasType( const QString& theType ) const;

  HYDRODATA_EXPORT void Clear();

  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  HYDRODATA_EXPORT QString GetAttrName() const;
  HYDRODATA_EXPORT bool    SetAttrName( const QString& ) const;

  HYDRODATA_EXPORT QString GetAttrValue( const QString& theType ) const;
  HYDRODATA_EXPORT void    SetAttrValue( const QString& theType, const QString& theAttrValue ) const;

  HYDRODATA_EXPORT QString GetType( const QString& theAttrValue ) const;

  HYDRODATA_EXPORT QColor GetColor( const QString& theType ) const;
  HYDRODATA_EXPORT void SetColor( const QString& theType, const QColor& theColor ) const;

private:
  Handle(TDataStd_NamedData) Map() const;
};

#endif

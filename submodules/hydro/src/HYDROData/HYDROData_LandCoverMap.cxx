// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_LandCoverMap.h>
#include <HYDROData_Object.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Tool.h>
#include <HYDROData_ShapeFile.h>
#include <HYDROData_Document.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_ShapesTool.h>

#include <BOPAlgo_BOP.hxx>
#include <BOPAlgo_Builder.hxx>
#include <BOPAlgo_PaveFiller.hxx>
#include <TopTools_ListOfShape.hxx>
#include <BRep_Builder.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <GCPnts_QuasiUniformDeflection.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Iterator.hxx>
#include <TopoDS_Shell.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <BOPAlgo_PaveFiller.hxx>
#include <BRepTools.hxx>
#include <TopExp_Explorer.hxx>
#include <ShapeUpgrade_UnifySameDomain.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <ShapeBuild_ReShape.hxx>
#include <ShapeFix_Shape.hxx>
#include <BRepCheck_Shell.hxx>
#include <BRepCheck_ListOfStatus.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <Geom_Curve.hxx>
#include <Geom_Line.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <TopTools_DataMapOfShapeListOfShape.hxx>
#include <NCollection_DoubleMap.hxx>
#include <HYDROData_LCM_FaceClassifier.h>
#include <QDir>

#include <stdexcept>

#include <QFile>
#include <QString>
#include <QTextStream>
#include <QFileInfo>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

const char TELEMAC_FORMAT = 'f';
const int TELEMAC_PRECISION = 3;


IMPLEMENT_STANDARD_RTTIEXT(HYDROData_LandCoverMap, HYDROData_Entity)

/**
  Constructor
  @param theMap the land cover map to iterate through
*/
HYDROData_LandCoverMap::Explorer::Explorer( const HYDROData_LandCoverMap& theMap )
{
  Init( theMap );
}

HYDROData_LandCoverMap::Explorer::Explorer( const Handle( HYDROData_LandCoverMap )& theMap )
{
  if( theMap.IsNull() )
  {
    myExplorer = 0;
    myIndex = -1;
  }
  else
    Init( *theMap );
}

/**
  Initialize the iterator
  @param theMap the land cover map to iterate through
*/
void HYDROData_LandCoverMap::Explorer::Init( const HYDROData_LandCoverMap& theMap )
{
  TopoDS_Shape aShape = theMap.GetShape();
  if( aShape.IsNull() )
    myExplorer = 0;
  else
  {
    myExplorer = new TopExp_Explorer();
    myExplorer->Init( aShape, TopAbs_FACE );
  }

  theMap.myLab.FindChild( DataTag_Types ).FindAttribute( TDataStd_ExtStringArray::GetID(), myArray );
  if( myArray.IsNull() )
    myIndex = -1;
  else
    myIndex = myArray->Lower();
}

/**
  Destructor
*/
HYDROData_LandCoverMap::Explorer::~Explorer()
{
  delete myExplorer;
}

/**
  Return the current 0-based index of the iterator
  @return the current index
*/
int HYDROData_LandCoverMap::Explorer::Index() const
{
  if( myArray.IsNull() )
    return -1;
  else
    return myIndex - myArray->Lower();
}

/**
  Return if the iterator has more elements
  @return if the iterator has more elements
*/
bool HYDROData_LandCoverMap::Explorer::More() const
{
  return !myArray.IsNull() && myExplorer && myExplorer->More();
}

/**
  Move iterator to the next element
*/
void HYDROData_LandCoverMap::Explorer::Next()
{
  if( myExplorer )
  {
    myExplorer->Next();
    myIndex++;
  }
}

/**
  Get the current land cover (face)
  @return the land cover's face
*/
TopoDS_Face HYDROData_LandCoverMap::Explorer::Face() const
{
  if( myExplorer )
    return TopoDS::Face( myExplorer->Current() );
  else
    return TopoDS_Face();
}

/**
  Get the current land cover's Strickler type
  @return the land cover's Strickler type
*/
QString HYDROData_LandCoverMap::Explorer::StricklerType() const
{
  if( myArray.IsNull() || myIndex < myArray->Lower() || myIndex > myArray->Upper() )
    return "";
  else
    return HYDROData_Tool::toQString( myArray->Value( myIndex ) );
}

/**
  Set the Strickler type for the current land cover
  @param theType the Strickler type
*/
void HYDROData_LandCoverMap::Explorer::SetStricklerType( const QString& theType )
{
  if( myArray.IsNull() || myIndex < myArray->Lower() || myIndex > myArray->Upper() )
    return;
  else
    myArray->SetValue( myIndex, HYDROData_Tool::toExtString( theType ) );
}

/**
  Constructor
*/
HYDROData_LandCoverMap::HYDROData_LandCoverMap()
  : HYDROData_Entity( Geom_No )
{
}

/**
  Destructor
*/
HYDROData_LandCoverMap::~HYDROData_LandCoverMap()
{
}

/**
  Get object's kind
  @return object's kind
*/
const ObjectKind HYDROData_LandCoverMap::GetKind() const
{
  return KIND_LAND_COVER_MAP;
}

int HYDROData_LandCoverMap::GetLCCount() const
{
  Explorer anIt( *this );
  int i = 0;
  for( ; anIt.More(); anIt.Next() )
    i++;
  return i;
}

bool HYDROData_LandCoverMap::IsEmpty() const
{
  Explorer anIt( *this );
  if ( !anIt.More() )
    return true;
  else
    return false;
}

/**
  Load attributes from DBF File
///
*/
HYDROData_LandCoverMap::DBFStatus HYDROData_LandCoverMap::ImportDBF( const QString& theDBFFileName,
                                                                     const QString& theFieldName,
                                                                     const QStringList& theDBFValues,
                                                                     const QStringList& theStricklerTypes,
                                                                     const QList<int>& theIndices )
{
  if (theDBFValues.size() != theStricklerTypes.size())
    return DBFStatus_DIFF_SIZE_ERROR;
  HYDROData_ShapeFile aDBFImporter;
  if (!aDBFImporter.DBF_OpenDBF(theDBFFileName))
    return DBFStatus_OPEN_FILE_ERROR; //cant open file

  QStringList FieldList = aDBFImporter.DBF_GetFieldList();
  int FieldNameIndex = FieldList.indexOf(theFieldName);
  if (FieldNameIndex == -1)
    return DBFStatus_NO_SUCH_FIELD_ERROR; //no such field

  std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
  aDBFImporter.DBF_GetAttributeList(FieldNameIndex, theAttrV );

  bool allOK = true;
  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
  {
    int CurIndex = anIt.Index();
    int anIndex = CurIndex;
    if( !theIndices.isEmpty() )
      anIndex = theIndices[CurIndex];

    HYDROData_ShapeFile::DBF_AttrValue AValue = theAttrV[anIndex];
    int StricklerTypesInd = theDBFValues.indexOf( QString( AValue.myStrVal ) );
    if ( StricklerTypesInd != -1)
      anIt.SetStricklerType( theStricklerTypes[StricklerTypesInd] );
    else
      allOK = false;
  }
  if (allOK)
    return DBFStatus_OK;
  else
    return DBFStatus_NO_DBFVALUES_CORRESPONDENCE_WARNING;
}

/**
  Export attributes to DBF File
///
*/
void HYDROData_LandCoverMap::ExportDBF( const QString& theDBFFileName,
                                        const QString& theFieldName,
                                        const QStringList& theDBFValues,
                                        const QStringList& theStricklerTypes) const
{
  if (theDBFValues.size() != theStricklerTypes.size())
    return;
  HYDROData_ShapeFile anExporter;
  std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
  {
    QString CurST = anIt.StricklerType();
    HYDROData_ShapeFile::DBF_AttrValue aCurAttrV;
    aCurAttrV.myIsNull = false;
    int StricklerTypesInd = theStricklerTypes.indexOf(CurST);
    if (StricklerTypesInd != -1)
    {
      aCurAttrV.myStrVal = theDBFValues[StricklerTypesInd];
      aCurAttrV.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
      theAttrV.push_back(aCurAttrV);
    }
    else
      aCurAttrV.myIsNull = true;
  }
  //use actual str value; not the raw value
  anExporter.DBF_WriteFieldAndValues(theDBFFileName, theFieldName, HYDROData_ShapeFile::DBF_FieldType_String, theAttrV, false);

}

int HashCode( const gp_Pnt& thePoint, const Standard_Integer theUpper )
{
  int aHashX = HashCode( thePoint.X(), theUpper );
  int aHashY = HashCode( thePoint.Y(), theUpper );
  return (aHashX^aHashY)%theUpper;
}

bool operator == ( const gp_Pnt& thePoint1, const gp_Pnt& thePoint2 )
{
  return thePoint1.IsEqual( thePoint2, Precision::Confusion() );
}

bool EdgeDiscretization( const TopoDS_Edge& theEdge,
                         Standard_Real theDeflection,
                         NCollection_IndexedMap<gp_Pnt>& theVerticesMap,
                         QList<int>& theVerticesIds )
{
  BRepAdaptor_Curve aCurve( theEdge );
  GCPnts_QuasiUniformDeflection aDiscrete( aCurve, theDeflection );
  if( !aDiscrete.IsDone() )
    return false;

  int n = aDiscrete.NbPoints();
  for( int i=1; i<=n; i++ )
  {
    gp_Pnt aPnt = aDiscrete.Value( i );
    int anId;
    if( theVerticesMap.Contains( aPnt ) )
      anId = theVerticesMap.FindIndex( aPnt );
    else
    {
      anId = theVerticesMap.Size();
      theVerticesMap.Add( aPnt );
    }
    theVerticesIds.append( anId );
  }
  return true;
}

/**
  Export the land cover map for the solver (Telemac)
  @param theFileName the name of file
  @return if the export is successful
*/
bool HYDROData_LandCoverMap::ExportTelemac( const QString& theFileName,
                                            double theDeflection,
                                            const Handle(HYDROData_StricklerTable)& theTable,
                                            QString& statMessage) const
{
  TopoDS_Shape aLandCoverMapShape = GetShape();
  TopTools_ListOfShape aListOfFaces;
  Explorer anIt( *this );
  QMap<Handle(TopoDS_TShape), QString> aTypesMap;
  for( ; anIt.More(); anIt.Next() )
  {
    aListOfFaces.Append( anIt.Face() );
    aTypesMap.insert( anIt.Face().TShape(), anIt.StricklerType() );
  }

  TopoDS_Shape aShape = MergeFaces( aListOfFaces, false, NULL );

  NCollection_IndexedMap<gp_Pnt> aVerticesMap;
  NCollection_IndexedDataMap< TopoDS_Edge, QList<int> > anEdgesMap;
  typedef QPair< QString, QList<int> > FaceData;
  NCollection_IndexedDataMap< TopoDS_Face, FaceData > aFacesMap;

  // add into the map all edges existing in the shell
  TopExp_Explorer anExp1( aShape, TopAbs_EDGE );
  for( ; anExp1.More(); anExp1.Next() )
  {
    TopoDS_Edge anEdge = TopoDS::Edge( anExp1.Current() );
    QList<int> aVerticesIdsList;
    if( EdgeDiscretization( anEdge, theDeflection, aVerticesMap, aVerticesIdsList ) )
      anEdgesMap.Add( anEdge, aVerticesIdsList );
  }

  // add into the map all faces existing in the shell and correspondence between face and edges ids
  TopExp_Explorer anExp2( aShape, TopAbs_FACE );
  for( ; anExp2.More(); anExp2.Next() )
  {
    TopoDS_Face aFace = TopoDS::Face( anExp2.Current() );
    TopExp_Explorer anExp3( aFace, TopAbs_EDGE );
    QList<int> anEdgesIdsList;
    for( ; anExp3.More(); anExp3.Next() )
    {
      TopoDS_Edge anEdge = TopoDS::Edge( anExp3.Current() );
      int anEdgeId = anEdgesMap.FindIndex( anEdge );
      anEdgesIdsList.append( anEdgeId );
    }

    FaceData aData;
    aData.first = aTypesMap[aFace.TShape()];
    aData.second = anEdgesIdsList;
    aFacesMap.Add( aFace, aData );
  }

  QFile aFile( theFileName );
  if( !aFile.open( QFile::WriteOnly | QFile::Text ) )
  {
    QString homeFilePath = QDir::home().absoluteFilePath( theFileName );
    aFile.setFileName(homeFilePath);
    if (aFile.open( QFile::WriteOnly | QFile::Text ) )
      statMessage = "Telemac file have been exported to the home directory: " + homeFilePath;
    else
      return false;
  }
  else
  {
    QString absFilePath = QDir::current().absoluteFilePath( theFileName );
    statMessage = "Telemac file have been exported to the current directory: " + absFilePath;
  }

  QTextStream aStream( &aFile );
  aStream << "# nodes\n";
  NCollection_IndexedMap<gp_Pnt>::Iterator anIt1( aVerticesMap );
  for( ; anIt1.More(); anIt1.Next() )
  {
    gp_Pnt aPnt = anIt1.Value();
    aStream << QString::number( aPnt.X(), TELEMAC_FORMAT, TELEMAC_PRECISION );
    aStream << " ";
    aStream << QString::number( aPnt.Y(), TELEMAC_FORMAT, TELEMAC_PRECISION );
    aStream << " ";
    aStream << QString::number( aPnt.Z(), TELEMAC_FORMAT, TELEMAC_PRECISION );
    aStream << "\n";
  }
  aStream << "\n";

  aStream << "# edges\n";
  NCollection_IndexedDataMap< TopoDS_Edge, QList<int> >::Iterator anIt2( anEdgesMap );
  for( ; anIt2.More(); anIt2.Next() )
  {
    QList<int> aVerticesIds = anIt2.Value();
    foreach( int anId, aVerticesIds )
      aStream << anId << " ";
    aStream << "\n";
  }
  aStream << "\n";

  aStream << "# faces\n";
  NCollection_IndexedDataMap< TopoDS_Face, FaceData >::Iterator anIt3( aFacesMap );
  for( ; anIt3.More(); anIt3.Next() )
  {
    QString aType = anIt3.Value().first;
    double aCoeff = theTable->Get( aType, 0.0 );
    QList<int> anEdgesIds = anIt3.Value().second;
    //aStream << "\"" << aType << "\" ";
    aStream << QString::number( aCoeff, TELEMAC_FORMAT, TELEMAC_PRECISION ) << " ";
    foreach( int anId, anEdgesIds )
      aStream << anId << " ";
    aStream << "\n";
  }
  aStream << "\n";

  aFile.close();
  return true;
}

/**
  Add a new object as land cover
  @param theObject the object to add as land cover
  @param theType the Strickler type for the new land cover
  @return if the addition is successful
*/
bool HYDROData_LandCoverMap::Add( const Handle( HYDROData_Object )& theObject, const QString& theType )
{
  if( theObject.IsNull() )
    return false;

  TopoDS_Shape aShape = theObject->GetTopShape();
  TopoDS_Face aFace;

  if( aShape.ShapeType() ==TopAbs_FACE )
  {
    aFace = TopoDS::Face(aShape);
  }
  else if ( aShape.ShapeType() ==TopAbs_COMPOUND )
  {
    TopoDS_Iterator It(aShape);
    for (; It.More(); It.Next())
      if (It.Value().ShapeType() == TopAbs_FACE)
      {
        aFace = TopoDS::Face(It.Value());
        break;
      }
  }

  if (aFace.IsNull())
    return false;
  return LocalPartition( aFace, theType );
}

bool HYDROData_LandCoverMap::Add( const TopoDS_Wire& theWire, const QString& theType )
{
  if( !theWire.Closed() )
    return false;

  TopoDS_Face aFace = BRepBuilderAPI_MakeFace( theWire, Standard_True ).Face();
  return LocalPartition( aFace, theType );
}

/**
  Add a new polyline as land cover
  @param thePolyline the polyline to add as land cover
  @param theType the Strickler type for the new land cover
  @return if the addition is successful
*/
bool HYDROData_LandCoverMap::Add( const Handle( HYDROData_PolylineXY )& thePolyline, const QString& theType )
{
  if( thePolyline.IsNull() )
    return false;

  TopoDS_Shape aShape = thePolyline->GetShape();
  if( aShape.ShapeType()==TopAbs_WIRE )
    return Add( TopoDS::Wire( aShape ), theType );

  if( aShape.ShapeType()==TopAbs_COMPOUND )
  {
    TopExp_Explorer anExp( aShape, TopAbs_WIRE );
    for( ; anExp.More(); anExp.Next() )
    {
      TopoDS_Wire aPart = TopoDS::Wire( anExp.Current() );
      if( !Add( aPart, theType ) )
        return false;
    }
    return true;
  }

  return false;
}

/**
  Remove the given face from land cover map
  @param theFace the face to be removed
  @return if the removing is successful
*/
bool HYDROData_LandCoverMap::Remove( const TopoDS_Face& theFace )
{
  TopTools_ListOfShape aList;
  aList.Append( theFace );
  return Remove( aList );
}

/**
  Remove the given faces from land cover map
  @param theFacesToRemove the face list to be removed
  @return if the removing is successful
*/
bool HYDROData_LandCoverMap::Remove( const TopTools_ListOfShape& theFacesToRemove )
{
  HYDROData_MapOfFaceToStricklerType aFacesToRemove, aNewFaces;
  TopTools_ListIteratorOfListOfShape aFIt( theFacesToRemove );
  for( ; aFIt.More(); aFIt.Next() )
  {
    TopoDS_Shape aShape = aFIt.Value();
    if( aShape.ShapeType()==TopAbs_FACE )
      aFacesToRemove.Add( TopoDS::Face( aShape ), "" );
  }

  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
    if( !aFacesToRemove.Contains( anIt.Face() ) )
      aNewFaces.Add( anIt.Face(), anIt.StricklerType() );

  if ( aNewFaces.IsEmpty() )
    return false;

  StoreLandCovers( aNewFaces );
  return true;
}

/**
  Split the land cover map by the given polyline
  @param thePolyline the tool polyline to split the land cover map
  @return if the removing is successful
*/
bool HYDROData_LandCoverMap::Split( const Handle( HYDROData_PolylineXY )& thePolyline )
{
  if( thePolyline.IsNull() )
    return false;

  TopoDS_Shape aShape = thePolyline->GetShape();
  return Split( aShape );
}


/**
  Split the land cover map by the given polyline
  @param theShape the tool polyline to split the land cover map
  @return if the removing is successful
*/
bool HYDROData_LandCoverMap::Split( const TopoDS_Shape& theShape )
{
  int aNbCL = GetLCCount();
  bool aResult = LocalPartition( theShape, "" );
  return aResult && aNbCL != GetLCCount();
}


/**
  Merge the given faces in the land cover
  @param theFaces the faces to merge in the land cover map
  @param theType the Strickler type for the merged land cover
  @return if the merge is successful
*/
bool HYDROData_LandCoverMap::Merge( const TopTools_ListOfShape& theFaces, const QString& theType )
{
  // 1. to fuse the faces into the new face
  TopoDS_Shape aMergedFace = MergeFaces( theFaces, true, NULL );
  bool aStat = true;
  if( !aMergedFace.IsNull() )
  {
    // 2. to remove the merged faces from the current map
    Remove( theFaces );
    TopExp_Explorer Exp(aMergedFace, TopAbs_FACE);
    for( ; Exp.More(); Exp.Next() )
    {
      const TopoDS_Face& aCF = TopoDS::Face(Exp.Current());
      // 3. to add the face into the map
      aStat = aStat && LocalPartition( aCF, theType );
    }
  }
  else
    aStat = false;
  return aStat;
}

/**
  Merge the given faces into the shell/face
  @param theFaces the faces to merge
  @param IsToUnify if the common edges should be removed (fused)
  @param theTolerance the operation's tolerance
  @return result shape (face or shell)
*/

TopoDS_Shape HYDROData_LandCoverMap::MergeFaces( const TopTools_ListOfShape& theFaces,
                                                 bool IsToUnify,
                                                 TopTools_IndexedDataMapOfShapeListOfShape* theShHistory,
                                                 double theTolerance)
{
  //DEBTRACE("MergeFaces");
  int anError;
  TopTools_ListIteratorOfListOfShape anIt;
  TopTools_ListOfShape aLC;
  anIt.Initialize(theFaces);
  for( ; anIt.More(); anIt.Next() )
  {
    if (anIt.Value().ShapeType() != TopAbs_FACE)
      return TopoDS_Shape();
    aLC.Append( anIt.Value() );
  }

  BOPAlgo_PaveFiller aPF;
  aPF.SetArguments( aLC );
  aPF.SetRunParallel( Standard_False );
  aPF.SetFuzzyValue( theTolerance );

  aPF.Perform();
  if( aPF.HasErrors())
    return TopoDS_Shape();

  BOPAlgo_Builder anAlgo;
  anIt.Initialize( theFaces );
  for( ; anIt.More(); anIt.Next() )
    anAlgo.AddArgument( anIt.Value() );

  anAlgo.PerformWithFiller( aPF );
  if( anAlgo.HasErrors())
    return TopoDS_Shape();

  const TopoDS_Shape& aMergedShape = anAlgo.Shape();

  // retrieve history of modifications
  if (theShHistory)
  {
    theShHistory->Clear();
    anIt.Initialize(theFaces);
    for( ; anIt.More(); anIt.Next() )
    {
      const TopTools_ListOfShape aMLS = anAlgo.Modified( anIt.Value() );
      theShHistory->Add(anIt.Value(), aMLS);
    }
  }
  //

  BRep_Builder aBuilder;
  TopoDS_Shell aShell;
  aBuilder.MakeShell( aShell );
  aShell.Closed( Standard_False );
  TopExp_Explorer anExplorer( aMergedShape, TopAbs_FACE );
  for( ; anExplorer.More(); anExplorer.Next() )
  {
    const TopoDS_Face& aFace = TopoDS::Face(anExplorer.Current());
    if( aFace.IsNull() )
      continue;
    if( aFace.ShapeType() == TopAbs_FACE )
    {
      aBuilder.Add( aShell, aFace );
      aShell.Closed( Standard_False );
    }
  }

  TopoDS_Shape aResult;
  if( IsToUnify )
  {
    ShapeUpgrade_UnifySameDomain aUSD;
    aUSD.Initialize( aShell );
    aUSD.Build();
    aResult = aUSD.Shape();
  }
  else
    aResult = aShell;

  anExplorer.Init( aResult, TopAbs_FACE );
  int n = 0;
  TopoDS_Face anOneFace;
  for( ; anExplorer.More(); anExplorer.Next(), n++ )
    anOneFace = TopoDS::Face( anExplorer.Current() );

  if (n == 1)
    aResult = anOneFace;
  else if (aResult.ShapeType() == TopAbs_SHELL)
  {
    BRepCheck_Shell aBCS(TopoDS::Shell(aResult));
    if (aBCS.Status().First() != BRepCheck_NoError)
    {
      ShapeFix_Shell aFixer;
      aFixer.FixFaceOrientation(TopoDS::Shell(aResult), 1);
      aResult = aFixer.Shape();
    }
  }

  return aResult;
}
/**
  Change Strickler type for the list of faces to the given one
  @param theFaces the faces to change type
  @param theType the Strickler type for the given land cover(s)
  @return if the change type operation is successful
*/
bool HYDROData_LandCoverMap::ChangeType( const TopTools_ListOfShape& theFaces, const QString& theType )
{
  HYDROData_MapOfFaceToStricklerType aFacesToChangeType;
  TopTools_ListIteratorOfListOfShape aFIt( theFaces );
  for( ; aFIt.More(); aFIt.Next() )
  {
    TopoDS_Shape aShape = aFIt.Value();
    if( aShape.ShapeType()==TopAbs_FACE )
      aFacesToChangeType.Add( TopoDS::Face( aShape ), "" );
  }

  int aNbChanges = 0;
  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
    if( aFacesToChangeType.Contains( anIt.Face() ) )
    {
      anIt.SetStricklerType( theType );
      aNbChanges++;
    }
  if ( aNbChanges != theFaces.Extent() )
    return false;

  return true;
}

/**
  Get the shape of the land cover map
*/
TopoDS_Shape HYDROData_LandCoverMap::GetShape() const
{
  return HYDROData_Entity::GetShape( DataTag_Shape );
}

/**
  Get Strickler type of the given land cover
  @param theLandCover the land cover to get Strickler type of
  @return name of Strickler type
*/
QString HYDROData_LandCoverMap::StricklerType( const TopoDS_Face& theLandCover ) const
{
  QString aType = "";

  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
    if( anIt.Face().IsEqual( theLandCover) )
    {
      aType = anIt.StricklerType();
      break;
    }

  return aType;
}

/**
  Set the shape of the land cover map
  @param theShape the new shape for the land cover map
*/
void HYDROData_LandCoverMap::SetShape( const TopoDS_Shape& theShape )
{
  HYDROData_Entity::SetShape( DataTag_Shape, theShape );
}

/**
  Perform the local partition algorithm on the land cover
  @param theNewShape the new shape to add into the land cover
  @param theNewType the new Strickler type for the new land cover
  @return if the local partition is successful
*/
bool HYDROData_LandCoverMap::LocalPartition( const TopoDS_Shape& theNewShape, const QString& theNewType )
{
  if( theNewShape.IsNull() )
    return false;

  TopTools_ListOfShape aShapesList;
  BOPAlgo_PaveFiller aPaveFiller;
  HYDROData_MapOfFaceToStricklerType aNewFaces;

  // add faces to shapes list
  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
    aShapesList.Append( anIt.Face() );
  aShapesList.Append( theNewShape );

  //DEBTRACE("theNewType " << theNewType);
  if( aShapesList.Size()==1 && theNewShape.ShapeType()==TopAbs_FACE )
  {
    aNewFaces.Add( TopoDS::Face( theNewShape ), theNewType );
    StoreLandCovers( aNewFaces );
    return true;
  }

  // prepare pave filler
  aPaveFiller.SetArguments( aShapesList );
  aPaveFiller.Perform();
  if( aPaveFiller.HasErrors())
    return false;

  // add faces to builder
  BOPAlgo_Builder aBuilder;
  anIt.Init( *this );
  for( ; anIt.More(); anIt.Next() )
    aBuilder.AddArgument( anIt.Face() );
  aBuilder.AddArgument( theNewShape );

  // perform the partition with the pave filler
  aBuilder.PerformWithFiller( aPaveFiller );
  if( aBuilder.HasErrors())
    return false;

  //std::cout << "History:" << std::endl;
  // analysis of the history
  //     a. to fill map of shapes which come from the new face
  NCollection_IndexedMap<int> aShapesFromNewFace;
  //std::cout << "from NEW " << theNewShape << ":" << theNewType << std::endl;
  TopTools_ListOfShape aModified = aBuilder.Modified( theNewShape );
  TopTools_ListIteratorOfListOfShape aMIt( aModified );
  for( ; aMIt.More(); aMIt.Next() )
  {
    //std::cout << "   " << aMIt.Value() << std::endl;
    //DEBTRACE(aMIt.Value());
    int aKey = (int)(uintptr_t)aMIt.Value().TShape().operator->();
    aShapesFromNewFace.Add( aKey );
  }

  //     b. to fill map of parts except parts from new face
  anIt.Init( *this );
  for( ; anIt.More(); anIt.Next() )
  {
    QString aSType = anIt.StricklerType();
    //DEBTRACE(anIt.StricklerType() << " " << anIt.Face());
    //std::cout << "from " << anIt.Face() << ": " << anIt.StricklerType() << std::endl;
    TopTools_ListOfShape aModified = aBuilder.Modified( anIt.Face() );
    //
    if( aModified.Extent() == 0 )
      aModified.Append( anIt.Face() );
    //DEBTRACE(anIt.StricklerType() << " " << anIt.Face());

    TopTools_ListIteratorOfListOfShape aMIt( aModified );
    for( ; aMIt.More(); aMIt.Next() )
    {
      TopoDS_Shape aShape = aMIt.Value();
      bool isFace = aShape.ShapeType()==TopAbs_FACE;
      int aKey = (int)(uintptr_t)aShape.TShape().operator->();
      bool isAlsoFromNew = aShapesFromNewFace.Contains( aKey );
      //std::cout << "   " << aShape << " " << isAlsoFromNew << std::endl;
      if( isFace && !isAlsoFromNew )
        aNewFaces.Add( TopoDS::Face( aShape ), aSType );
    }
    //DEBTRACE(anIt.StricklerType() << " " << anIt.Face());
  }
  //     c. add the new shape if it is face with its type
  if( theNewShape.ShapeType()==TopAbs_FACE )
    aNewFaces.Add( TopoDS::Face( theNewShape ), theNewType );
  //DEBTRACE(theNewShape << " " << theNewType);

  // convert map of shape to type to compound and list of types
  StoreLandCovers( aNewFaces );

//  anIt.Init( *this );
//  for( ; anIt.More(); anIt.Next() )
//  {
//     DEBTRACE(anIt.StricklerType() << " " << anIt.Face());
//  }
  return true;
}

/**
  Replace the set of land covers in the land cover map
  @param theMap the map of shape (face) to Strickler type (string)
*/
void HYDROData_LandCoverMap::StoreLandCovers( const HYDROData_MapOfFaceToStricklerType& theMap )
{
  TopTools_ListOfShape aListOfFaces;

  //DEBTRACE("theMap.Extent() " << theMap.Extent());
  for( int i = 1; i <= theMap.Extent(); i++ )
  {
    TopoDS_Face aFace = theMap.FindKey(i);
    if( aFace.IsNull() )
      continue;
    aListOfFaces.Append(aFace);
  }

  TopTools_IndexedDataMapOfShapeListOfShape ShHistory;
  ShHistory.Clear();

  TopoDS_Shape aResult;
  if( aListOfFaces.Extent() == 1 )
    aResult = aListOfFaces.First();
  else if( aListOfFaces.Extent() > 1 )
    aResult = MergeFaces( aListOfFaces, false, &ShHistory );

  //remove internal edges
  //if nothing changes => the result shape should be the same
  //hence the map will be empty

  NCollection_IndexedDataMap<TopoDS_Face, TopoDS_Face> ShF2FHistory;
  RemoveInternal(aResult, &ShF2FHistory);

  //one face => mark as unchanged
  if( aListOfFaces.Extent() == 1 )
    ShHistory.Add(aResult, TopTools_ListOfShape());

  NCollection_IndexedDataMap<TopoDS_Face, QString> aChF2ST;
  QStringList aSTypes;
  //
  for( int i = 1; i <= theMap.Extent(); i++ )
  {
    TopoDS_Face aFF = theMap.FindKey(i);
    //DEBTRACE("  --- " << aFF);
    if( aFF.IsNull() )
      continue;
    //DEBTRACE(ShHistory.IsEmpty());
    //DEBTRACE(aFF.Checked());
    TopTools_ListOfShape aLS;
    try
    {
      aLS = ShHistory.FindFromKey(aFF); //TODO: bug to fix. Observed on an incomplete split of a face
    }
    catch (...)
    {
      DEBTRACE("TODO: bug to fix. Observed on an incomplete split of a face");
      //continue; // No, keep aLS empty and propagate the type of the original face
    }
    if (aLS.IsEmpty())
    {
      //DEBTRACE("--- aLS.IsEmpty()");
      QString aSType = theMap.FindFromKey(aFF);
      //DEBTRACE("  --- " << aSType.toStdString());
      if (ShF2FHistory.Contains(aFF))
        {
          //DEBTRACE("ShF2FHistory.FindFromKey(aFF) " << ShF2FHistory.FindFromKey(aFF));
          aChF2ST.Add(ShF2FHistory.FindFromKey(aFF), aSType);
        }
      else
        {
          //DEBTRACE("aFF " << aFF);
          aChF2ST.Add(aFF, aSType);
        }
    }
    else
    {
      //DEBTRACE("--- !aLS.IsEmpty()");
      TopTools_ListIteratorOfListOfShape anIt(aLS);
      for (; anIt.More(); anIt.Next())
      {
        QString aSType = theMap.FindFromKey(aFF);
        //DEBTRACE("  --- " << aSType.toStdString());
        const TopoDS_Face& aMF = TopoDS::Face(anIt.Value());
        //if (ShF2FHistory.Contains(aFF))
        if (ShF2FHistory.Contains(aMF))
          {
            //DEBTRACE("ShF2FHistory.FindFromKey(aMF) " << ShF2FHistory.FindFromKey(aFF));
            aChF2ST.Add(ShF2FHistory.FindFromKey(aMF), aSType);
          }
        else
          {
            //DEBTRACE("aMF " << aMF);
            aChF2ST.Add(aMF, aSType);
          }
      }
    }
  }
  //
  SetShape( aResult );
  //
  //Explorer Exp(*this);
  TopExp_Explorer FExp(aResult, TopAbs_FACE);
  for( ; FExp.More(); FExp.Next() )
  {
    TopoDS_Face aFace = TopoDS::Face(FExp.Current());
    QString aST = "";
    if (aChF2ST.Contains(aFace))
      aST = aChF2ST.FindFromKey(aFace);
    //DEBTRACE("aFace "  << aFace << " aST " << aST.toStdString());
    aSTypes << aST;
  }

  Handle( TDataStd_ExtStringArray ) aTypes = TDataStd_ExtStringArray::Set( myLab.FindChild( DataTag_Types ), 0, aSTypes.size() - 1, Standard_True );
  aTypes->SetID(TDataStd_ExtStringArray::GetID());
  int k = 0;
  foreach (QString aST, aSTypes)
  {
    //DEBTRACE("aST " << aST.toStdString());
    aTypes->SetValue( k, HYDROData_Tool::toExtString( aST ) );
    k++;
  }

}

/**
   Checks that object has 2D presentation. Reimlemented to retun true.
*/
bool HYDROData_LandCoverMap::IsHas2dPrs() const
{
  return true;
}

/**
  Find the land cover for the given point
  @param thePoint the point laying in some land cover
  @param theType the returned type
  @return the found land cover's face
*/
TopoDS_Face HYDROData_LandCoverMap::FindByPoint( const gp_Pnt2d& thePoint, QString& theType ) const
{
  //TODO: some more optimal algorithm
  Explorer anIt( *this );
  for( ; anIt.More(); anIt.Next() )
    if( HYDROData_Tool::ComputePointState( thePoint.XY(), anIt.Face() ) == TopAbs_IN )
    {
      theType = anIt.StricklerType();
      return anIt.Face();
    }

  theType = "";
  return TopoDS_Face();
}

void Dump( const QString& theName, const QStringList& theList, QStringList& theLines )
{
  theLines.append( QString( "%1 = []" ).arg( theName ) );
  foreach( QString anItem, theList )
    theLines.append( QString( "%1.append( u\"%2\" )" ).arg( theName ).arg( anItem ) );
}

/**
  Dump to Python
  @param theTreatedObjects the map of treated objects
*/
QStringList HYDROData_LandCoverMap::DumpToPython( const QString&       thePyScriptPath,
                                                  MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aName = GetObjPyName();

  QString aShpFileName = thePyScriptPath;
  aShpFileName.replace( ".py", ".shp" );
  QString aDbfFileName = thePyScriptPath;
  aDbfFileName.replace( ".py", ".dbf" );

  ExportSHP( aShpFileName, true, 0.1 );

  QString anAttr = "CODE_06"; //TODO: some custom choice
  QStringList anAttrValues, aTypes;
  HYDROData_Document::Document( myLab )->CollectQGISValues( anAttr, anAttrValues, aTypes );
  ExportDBF( aDbfFileName, anAttr, anAttrValues, aTypes );

  aResList << QString( "if not(%1.ImportSHP( '%2' )):" ).
    arg( aName ).arg( QFileInfo( aShpFileName ).fileName() );
  aResList << QString( "  raise ValueError('problem while loading LandCoverMap shape')" );

  Dump( "attr_values", anAttrValues, aResList );
  Dump( "types", aTypes, aResList );
  aResList << QString( "if (%1.ImportDBF( '%2', '%3', attr_values, types ) != %1.DBFStatus_OK):" ).
    arg( aName ).arg( QFileInfo( aDbfFileName ).fileName() ).arg( anAttr );
  aResList << QString( "  raise ValueError('problem while loading LandCoverMap data base')" );

  return aResList;
}

void HYDROData_LandCoverMap::RemoveInternal(TopoDS_Shape& ShToRebuild, NCollection_IndexedDataMap<TopoDS_Face, TopoDS_Face>* aF2FReplace)
{
  //DEBTRACE("RemoveInternal");
  //Shape must be topologically correct
  TopExp_Explorer anExpF(ShToRebuild, TopAbs_FACE);
  //
  for(; anExpF.More(); anExpF.Next() )
  {
    TopoDS_Face CurFace = TopoDS::Face(anExpF.Current());
    //
    TopExp_Explorer anExp(CurFace, TopAbs_EDGE);
    TopTools_ListOfShape anEdgesToRemove;
    //
    for(; anExp.More(); anExp.Next() )
    {
      TopoDS_Edge CurEdge = TopoDS::Edge(anExp.Current());
      if (CurEdge.Orientation() == TopAbs_INTERNAL)
        anEdgesToRemove.Append(CurEdge);
    }
    //
    if (!anEdgesToRemove.IsEmpty())
    {
      Handle(ShapeBuild_ReShape) aReshape = new ShapeBuild_ReShape();
      TopoDS_Shape OutF = aReshape->Apply(CurFace);
      TopTools_ListIteratorOfListOfShape aIt(anEdgesToRemove);
      for (; aIt.More(); aIt.Next())
        aReshape->Remove(aIt.Value());
      OutF = aReshape->Apply(CurFace);

      Handle(ShapeFix_Shape) sfs = new ShapeFix_Shape;
      sfs->Init(OutF);
      sfs->Perform();
      OutF = sfs->Shape();
      aF2FReplace->Add(CurFace, TopoDS::Face(OutF));
    }
  }
  //
  Handle(ShapeBuild_ReShape) anExtReshape = new ShapeBuild_ReShape();
  for (int i = 1; i <= aF2FReplace->Extent(); i++)
  {
    TopoDS_Face aFK = aF2FReplace->FindKey(i);
    TopoDS_Face aFV = aF2FReplace->FindFromIndex(i);
    anExtReshape->Replace(aFK, aFV);
    ShToRebuild = anExtReshape->Apply(ShToRebuild);
  }
//  for (int i = 1; i <= aF2FReplace->Extent(); i++)
//    {
//      DEBTRACE("aF2FReplace key,value " << aF2FReplace->FindKey(i) << " " << aF2FReplace->FindFromIndex(i));
//    }

}

void HYDROData_LandCoverMap::SetTransparency( double theTransparency )
{
  SetDouble( DataTag_Transparency, theTransparency );
}

double HYDROData_LandCoverMap::GetTransparency() const
{
  return GetDouble( DataTag_Transparency, 0.25 );
}

bool HYDROData_LandCoverMap::ImportSHP( const QString& theSHPFileName,
                                        const QList<int>& theIndices )
{
  HYDROData_ShapeFile anImporter;
  QStringList aPolyList;
  TopTools_SequenceOfShape aFaces;
  //int aSHapeType = -1;
  int Stat = anImporter.ImportPolygons(HYDROData_Document::Document(), theSHPFileName, aPolyList, aFaces);
  //
  if (Stat != 1)
    return false;
  //
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;
  int maxInd = *std::max_element(theIndices.begin(), theIndices.end());
  if (maxInd > aPolyList.length())
    return false;
  //
  if (theIndices.empty())
  {
    //import all shapes
    for ( int i = 1; i <=aFaces.Length(); i++ )
    {
      TopoDS_Shape aShape = aFaces(i);
      if ( aShape.IsNull() )
        continue;
      aMapFace2ST.Add( TopoDS::Face( aShape ), "" );
    }
  }
  else
  {
    //import given indices
    foreach ( int Ind, theIndices )
    {
      TopoDS_Shape aShape = aFaces(Ind + 1);
      if ( aShape.IsNull() )
        continue;
      aMapFace2ST.Add( TopoDS::Face( aShape ), "" );
    }
  }
  //
  StoreLandCovers(aMapFace2ST);
  return true;
}

bool HYDROData_LandCoverMap::ExportSHP( const QString& theSHPFileName, bool bUseDiscr, double theDefl) const
{
  HYDROData_ShapeFile anExporter;
  QStringList aList;
  anExporter.Export(HYDROData_Document::Document(), theSHPFileName, this, aList, bUseDiscr, theDefl );
  if (aList.empty())
    return true;
  else
    return false;
}

bool HYDROData_LandCoverMap::CheckLinear()
{
  TopoDS_Shape InpShape = GetShape();
  TopExp_Explorer anEdgeEx(InpShape, TopAbs_EDGE);
  for (; anEdgeEx.More(); anEdgeEx.Next())
  {
    TopoDS_Edge E = TopoDS::Edge(anEdgeEx.Current());
    double aFP, aLP;
    Handle(Geom_Curve) aCur = BRep_Tool::Curve(E, aFP, aLP);
    Handle(Geom_Line) aLine = Handle(Geom_Line)::DownCast(aCur);
    if (aLine.IsNull())
    {
      Handle(Geom_TrimmedCurve) aTC = Handle(Geom_TrimmedCurve)::DownCast(aCur);
      if (!aTC.IsNull())
      {
        Handle(Geom_Line) aLine = Handle(Geom_Line)::DownCast(aTC->BasisCurve());
        if (aLine.IsNull())
          return false;
      }
      else
        return false;
    }
  }
  return true;
}

void HYDROData_LandCoverMap::UpdateLocalCS( double theDx, double theDy )
{
  TopoDS_Shape aShape = GetShape();
  TopoDS_Shape aLocatedShape = HYDROData_ShapesTool::Translated( aShape, theDx, theDy, 0 );
  SetShape( aLocatedShape );
}

void HYDROData_LandCoverMap::ClassifyPoints( const std::vector<gp_XY>& thePoints,
                                             std::vector<std::set <QString> >& theTypes ) const
{
  HYDROData_LCM_FaceClassifier FC(this);
  FC.Classify(thePoints, theTypes, NULL);
}

void HYDROData_LandCoverMap::ClassifyPoints( const std::vector<gp_XY>& thePoints,
                                             const Handle(HYDROData_StricklerTable)& theTable,
                                             std::vector<int>& theTypes ) const
{
  std::vector<std::set <QString> > types;
  HYDROData_LCM_FaceClassifier FC(this);
  FC.Classify(thePoints, types, NULL);

  size_t n = types.size();
  theTypes.resize( n );
  for( size_t i=0; i<n; i++ )
  {
    const std::set<QString>& sub_types = types[i];
    int aType = 0;
    if( !sub_types.empty() )
    {
      const QString& aSType = *sub_types.begin();
      aType = theTable->GetAttrValue( aSType ).toInt();
    }
    theTypes[i] = aType;
  }
}

void HYDROData_LandCoverMap::ClassifyPoints( const std::vector<gp_XY>& thePoints,
  const Handle(HYDROData_StricklerTable)& theTable,
  std::vector<double>& theCoeffs, double DefValue, bool UseMax ) const
{
  std::vector<std::set <QString> > Types;
  HYDROData_LCM_FaceClassifier FC(this);
  FC.Classify(thePoints, Types, NULL);

  theCoeffs.reserve(thePoints.size());

  for (size_t i = 0; i < Types.size(); i++)
  {
    const std::set<QString>& SStr = Types[i];
    if (SStr.empty())
      theCoeffs.push_back( DefValue );
    else
    {
      std::set<QString>::const_iterator it;
      std::vector<double> C1;
      for (it = SStr.begin(); it != SStr.end(); ++it)
        C1.push_back(theTable->Get( *it, DefValue ));
      double Val;
      if (UseMax)
        Val = *(std::max_element( C1.begin(), C1.end() ) );
      else
        Val = *(std::min_element( C1.begin(), C1.end() ) );
      theCoeffs.push_back( Val );
    }
  }

}

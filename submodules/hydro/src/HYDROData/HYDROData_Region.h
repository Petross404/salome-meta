// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Region_HeaderFile
#define HYDROData_Region_HeaderFile

#include "HYDROData_ShapesGroup.h"


class HYDROData_Zone;
class HYDROData_Document;
class TopoDS_Shape;
class TopoDS_Face;
class QStringList;

/**\class HYDROData_Region
 * \brief Regions are groups (lists) of zones, they can include one or several zones.
 */
class HYDROData_Region : public HYDROData_Entity
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_ChildZone,    ///< child zones
    DataTag_Zone,         ///< reference zones
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Region, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_REGION; }

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects,
                                                     QString defRegName = QString()) const;

  /**
   * Returns flag indicating that object is updateble or not.
   */
  HYDRODATA_EXPORT virtual bool CanBeUpdated() const;


  /**
   * Removes object and it child sub-objects from the data structure.
   * Reimplemented to update names of regions in father calculation.
   */
  HYDRODATA_EXPORT virtual void Remove();

  /**
   * Returns flag indicating that object can be removed or not.
   */
  HYDRODATA_EXPORT virtual bool CanRemove();

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;


  /**
   * Add new one reference zone for region.
   * The label of theZone is changed in case if old parent is not this region.
   */
  HYDRODATA_EXPORT virtual bool AddZone( const Handle(HYDROData_Zone)& theZone );

  /**
   * Returns all reference zone of region.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetZones() const;

  /**
   * Removes reference zone from region.
   */
  HYDRODATA_EXPORT virtual void RemoveZone( const Handle(HYDROData_Zone)& theZone, bool removeRegion = true );

  /**
   * Removes all reference zones from region.
   */
  HYDRODATA_EXPORT virtual void RemoveZones();

  /**
   * \brief Returns the shape of the region.
   * Shape of the region could be:
   * - a face which is the union of the region zones faces
   * - a shell if the zones faces can't be united into one face
   * \return shape as TopoDS_Shape
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape( HYDROData_ShapesGroup::SeqOfGroupsDefs* theSeqOfGroups = 0, 
                                                  const TopTools_SequenceOfShape* IntSh = NULL ) const;

  HYDRODATA_EXPORT bool IsSubmersible() const;

  /**
   * Create new one reference zone for region on child label.
   * The new zone is added into the list of reference zones.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Zone) addNewZone( const Handle(HYDROData_Document)& theDoc,
                                                              const QString& thePrefix,
                                                              const TopoDS_Face& theFace,
                                                              const QStringList& theRefObjects );

protected:

  friend class HYDROData_CalculationCase;
  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Region();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_Region();
};

#endif

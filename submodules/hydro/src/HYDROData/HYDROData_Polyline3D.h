// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Polyline3D_HeaderFile
#define HYDROData_Polyline3D_HeaderFile

#include "HYDROData_Object.h"

class HYDROData_PolylineXY;
class HYDROData_ProfileUZ;
class gp_XYZ;

/**\class HYDROData_Polyline3D
 * \brief 
 *
 */
class HYDROData_Polyline3D : public HYDROData_Object
{
public:

  typedef gp_XYZ                                Polyline3DPoint;
  typedef NCollection_Sequence<Polyline3DPoint> Polyline3DPoints;

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Object::DataTag_First + 100, ///< first tag, to reserve
    DataTag_PolylineXY,          ///< reference hydraulic axis
    DataTag_ProfileUZ,           ///< reference profile
    DataTag_ChildProfileUZ,      ///< reference profile
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Polyline3D, HYDROData_Object);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_POLYLINE;}

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;

  /**
   * Update the shape presentations of stream.
   * Call this method whenever you made changes for stream data.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Returns default filling color for new 3D polyline.
   */
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new 3D polyline.
   */
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;


public:      
  // Public methods to work with 3D polyline
  
  /**
   * Sets reference x,y polyline object for 3D polyline.
   */
  HYDRODATA_EXPORT virtual bool SetPolylineXY( const Handle(HYDROData_PolylineXY)& thePolyline,
                                               const bool                          theIsUpdateProfile = true );

  /**
   * Returns reference x,y polyline object of 3D polyline.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_PolylineXY) GetPolylineXY() const;

  /**
   * Remove reference x,y polyline object from 3D polyline.
   */
  HYDRODATA_EXPORT virtual void RemovePolylineXY();


  /**
   * Sets reference u,z profile object for 3D polyline.
   */
  HYDRODATA_EXPORT virtual bool SetProfileUZ( const Handle(HYDROData_ProfileUZ)& theProfile );

  /**
   * Returns reference u,z profile object of 3D polyline.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_ProfileUZ) GetProfileUZ() const;

  /**
   * Remove reference u,z profile object from 3D polyline.
   */
  HYDRODATA_EXPORT virtual void RemoveProfileUZ();


  /**
   * Set reference bathymetry object for geometry object.
   * Reimplemented to remove reference u,z profile.
   */
  HYDRODATA_EXPORT virtual bool SetAltitudeObject( const Handle(HYDROData_IAltitudeObject)& theAltitude );

  /**
   * Clear the reference bathymetry object for geometry object.
   * Reimplemented to remove child u,z profile.
   */
  HYDRODATA_EXPORT virtual void RemoveAltitudeObject();

  HYDRODATA_EXPORT Polyline3DPoints GetProfilePoints3D(bool IsConvertToGlobal);

  /**
   * Returns the child u,z profile which has been generated from bathymetry.
   */
  HYDRODATA_EXPORT Handle(HYDROData_ProfileUZ) GetChildProfileUZ( const bool theIsCreate = true ) const;

  /**
   * Sets the child u,z profile for polyline.
   */
  HYDRODATA_EXPORT void SetChildProfileUZ( const Handle(HYDROData_ProfileUZ)& theProfile );

  /**
   * Returns list of polyline points.
   * \return list of 3D points
   */
  HYDRODATA_EXPORT Polyline3DPoints GetPoints( double theEqDistance = -1 ) const;

protected:
  /**
   * Checks and if necessary create child 3D object.
   * Reimplemented to prevent creation of 3D child object.
   */
  HYDRODATA_EXPORT virtual void checkAndSetObject3D() {}


protected:

  void updateChildProfilePoints();

  void removeChildProfileUZ();


protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Polyline3D();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_Polyline3D();
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Application_HeaderFile
#define HYDROData_Application_HeaderFile

#include <HYDROData_Document.h> 
#include <TDocStd_Application.hxx>
#include <NCollection_DataMap.hxx>

typedef NCollection_DataMap<int, Handle(HYDROData_Document)> DataMapOfStudyIDDocument;

/**\class HYDROData_Application
 *
 * \brief Realization of Open CASCADE application abstraction. Class for 
 *        internal use inside of a package only.
 * 
 * Application supports the formats and document management. It is uses OCAF-lke
 * architecture and just implements specific features of the module.
 */
class HYDROData_Application : public TDocStd_Application
{
public: // useful methods inside of the package

  //! Retuns the application: one per process    
  HYDRODATA_EXPORT static HYDROData_Application* GetApplication();   

public: // Redefined OCAF methods
  //! Return name of resource (i.e. "Standard")
  HYDRODATA_EXPORT Standard_CString ResourcesName();
  //! Return format (i.e "MDTV-Standard")
  //! \param theFormats sequence of allowed formats for input/output
  HYDRODATA_EXPORT virtual void Formats(TColStd_SequenceOfExtendedString& theFormats);    
  //! Constructor
  //! Use method GetInstance() method to obtain 
  //! the static instance of the object (or derive your own application)
  HYDRODATA_EXPORT HYDROData_Application();

  // CASCADE RTTI
  DEFINE_STANDARD_RTTIEXT(HYDROData_Application, TDocStd_Application)

private:
  //! Returns document, if document doesn't exists return null
  Handle(HYDROData_Document) GetDocument() const;

  //! Appends document to the application
  void AddDocument(const Handle(HYDROData_Document)& theDocument);

  //! Removes document from the application
  void RemoveDocument( const Handle(HYDROData_Document)& theDocument );

  //! map from SALOME study ID to the document
  DataMapOfStudyIDDocument myDocuments;

  friend class HYDROData_Document; // to manipulate documents of application
};

#endif

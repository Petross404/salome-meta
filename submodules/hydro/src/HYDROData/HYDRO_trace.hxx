#ifndef __HYDROTRACE_HXX__
#define __HYDROTRACE_HXX__

#include <iostream>
#include <sstream>
//#include <pthread.h>

#ifdef _DEVDEBUG_
//#define DEBTRACE(msg) {std::cerr<<std::flush<<pthread_self()<<":"__FILE__<<" ["<<__LINE__<<"] : "<<msg<<std::endl<<std::flush;}
#define DEBTRACE(msg) {std::cerr << std::flush <<  __FILE__ << " [" << __LINE__ << "] : " << msg << std::endl << std::flush;}
#else
#define DEBTRACE(msg)
#endif

#endif

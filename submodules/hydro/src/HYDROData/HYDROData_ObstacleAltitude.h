// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_ObstacleAltitude_HeaderFile
#define HYDROData_ObstacleAltitude_HeaderFile


#include "HYDROData_IAltitudeObject.h"



/**\class HYDROData_ObstacleAltitude
 * \brief Class that stores/retreives information about the obstacle altitude.
 *
 */
class HYDROData_ObstacleAltitude : public HYDROData_IAltitudeObject
{
protected:

  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_IAltitudeObject::DataTag_First + 100, ///< first tag, to reserve
  };

public:

  DEFINE_STANDARD_RTTIEXT(HYDROData_ObstacleAltitude, HYDROData_IAltitudeObject);

  /**
   * Returns the kind of this object. 
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_OBSTACLE_ALTITUDE; }

public:      

  // Public methods to work with altitudes.

  /**
   * Returns altitude for given point.
   * \param thePoint the point to examine
   * \return altitude value
   */
  HYDRODATA_EXPORT virtual double           GetAltitudeForPoint( const gp_XY& thePoint,
                                                                 int theMethod = 0) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_ObstacleAltitude();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT  ~HYDROData_ObstacleAltitude();
};

#endif

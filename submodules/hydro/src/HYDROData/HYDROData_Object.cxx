// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Object.h"

#include "HYDROData_DummyObject3D.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_Tool.h"
#include "HYDROData_Iterator.h"
#include "HYDROData_IAltitudeObject.h"
#include <TopoDS_Shape.hxx>
#include <TDataStd_Integer.hxx>
#include <TDataStd_Real.hxx>

#include <QColor>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Object,HYDROData_Entity)

HYDROData_Object::HYDROData_Object( Geometry theGeometry )
  : HYDROData_Entity( theGeometry )
{
}

HYDROData_Object::~HYDROData_Object()
{
}

void HYDROData_Object::SetName( const QString& theName, bool isDefault)
{
  //DEBTRACE("SetName");
  QString anOldObjName = GetName();
  if ( anOldObjName != theName )
  {
    HYDROData_SequenceOfObjects aGroups = GetGroups();
    HYDROData_SequenceOfObjects::Iterator anIter( aGroups );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_ShapesGroup) aGroup =
        Handle(HYDROData_ShapesGroup)::DownCast( anIter.Value() );
      if ( aGroup.IsNull() )
        continue;

      HYDROData_Tool::UpdateChildObjectName( anOldObjName, theName, aGroup );
    }

    Handle(HYDROData_DummyObject3D) anObject3D = GetObject3D();
    if ( !anObject3D.IsNull() )
      HYDROData_Tool::UpdateChildObjectName( anOldObjName, theName, anObject3D );

    Handle(HYDROData_IAltitudeObject) anAltitudeObj = getChildAltitudeObject();
    if ( !anAltitudeObj.IsNull() )
      HYDROData_Tool::UpdateChildObjectName( anOldObjName, theName, anAltitudeObj );
  }

  HYDROData_Entity::SetName( theName, isDefault );
}

void HYDROData_Object::Update()
{
  DEBTRACE("Update");
  if( IsMustBeUpdated( Geom_2d ) )
  {
    RemoveTopShape();
    RemoveGroupObjects();
  }
  if( IsMustBeUpdated( Geom_3d ) )
    RemoveShape3D();

  checkAndSetAltitudeObject();
  HYDROData_Entity::Update();
}

HYDROData_SequenceOfObjects HYDROData_Object::GetAllReferenceObjects() const
{
  //DEBTRACE("GetAllReferenceObjects");
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Entity::GetAllReferenceObjects();

  Handle(HYDROData_IAltitudeObject) aRefAltitude = GetAltitudeObject();
  if ( !aRefAltitude.IsNull() )
    aResSeq.Append( aRefAltitude );

  return aResSeq;
}

void HYDROData_Object::Changed( Geometry theChangedGeometry )
{
  DEBTRACE("Changed");
  HYDROData_Entity::Changed( theChangedGeometry );

  Handle(HYDROData_DummyObject3D) anObject3D = GetObject3D();
  if ( !anObject3D.IsNull() )
    anObject3D->Changed( theChangedGeometry );
}

void HYDROData_Object::SetTopShape( const TopoDS_Shape& theShape )
{
  DEBTRACE("SetTopShape");
  HYDROData_Entity::SetShape( DataTag_TopShape, theShape );
}

TopoDS_Shape HYDROData_Object::GetTopShape() const
{
  //DEBTRACE("GetTopShape");
  return HYDROData_Entity::GetShape( DataTag_TopShape );
}

void HYDROData_Object::SetShape3D( const TopoDS_Shape& theShape )
{
  DEBTRACE("SetShape3D");
  HYDROData_Entity::SetShape( DataTag_Shape3D, theShape );
  // Check the object 3D existance
  checkAndSetObject3D();
}

TopoDS_Shape HYDROData_Object::GetShape3D() const
{
  //DEBTRACE("GetShape3D");
  return HYDROData_Entity::GetShape( DataTag_Shape3D );
}

Handle(HYDROData_DummyObject3D) HYDROData_Object::GetObject3D() const
{
  //DEBTRACE("GetObject3D");
  Handle(HYDROData_DummyObject3D) anObject;

  TDF_Label aLabel = myLab.FindChild( DataTag_Object3D, false );
  if ( !aLabel.IsNull() )
  {
    TDF_Label aChildLabel = aLabel.FindChild( 0, false );
    if ( !aChildLabel.IsNull() )
    {
      anObject = Handle(HYDROData_DummyObject3D)::DownCast(
        HYDROData_Iterator::Object( aChildLabel ) );
    }
  }

  return anObject;
}

void HYDROData_Object::checkAndSetObject3D()
{
  DEBTRACE("checkAndSetObject3D");
  TDF_Label aLabel = myLab.FindChild( DataTag_Object3D, false );
  if ( !aLabel.IsNull() )
    return;

  TDF_Label aChildLabel = myLab.FindChild( DataTag_Object3D ).FindChild( 0 );
  HYDROData_Iterator::CreateObject( aChildLabel, KIND_DUMMY_3D );
}

HYDROData_SequenceOfObjects HYDROData_Object::GetGroups() const
{
  //DEBTRACE("GetGroups");
  return GetReferenceObjects( DataTag_EdgesGroup );
}

Handle(HYDROData_ShapesGroup) HYDROData_Object::GetGroup( const int theGroupId ) const
{
  //DEBTRACE("GetGroup");
  Handle(HYDROData_ShapesGroup) aResGroup;

  HYDROData_SequenceOfObjects aGroups = GetGroups();
  if ( theGroupId < 0 || theGroupId >= aGroups.Length() )
    return aResGroup;

  aResGroup = Handle(HYDROData_ShapesGroup)::DownCast( aGroups.Value( theGroupId + 1 ) );

  return aResGroup;
}

int HYDROData_Object::GetGroupId( const Handle(HYDROData_ShapesGroup)& theGroup ) const
{
  //DEBTRACE("GetGroupId");
  int aRes = -1;

  HYDROData_SequenceOfObjects aGroups = GetGroups();
  for ( int i = 1, n = aGroups.Length(); i <= n; ++i )
  {
    Handle(HYDROData_ShapesGroup) aGroup =
      Handle(HYDROData_ShapesGroup)::DownCast( aGroups.Value( i ) );
    if ( theGroup->CompareLabels ( aGroup ) )
    {
      aRes = i - 1;
      break;
    }
  }

  return aRes;
}

bool HYDROData_Object::SetAltitudeObject(
  const Handle(HYDROData_IAltitudeObject)& theAltitude )
{
  DEBTRACE("SetAltitudeObject");
  if ( theAltitude.IsNull() )
    return false;

  Handle(HYDROData_IAltitudeObject) aPrevAltitude = GetAltitudeObject();
  if (!aPrevAltitude.IsNull())
    DEBTRACE("aPrevAltitude: " << aPrevAltitude->GetName())
  DEBTRACE("theAltitude: " << theAltitude->GetName())
  if ( IsEqual( aPrevAltitude, theAltitude ) )
    return true;

  SetReferenceObject( theAltitude, DataTag_AltitudeObject );

  // #636: In the case of the altitude object change the geometry of the main object is not changed,
  // to the object should not be marked as updated
  //SetToUpdate( true );

  return true;
}

Handle(HYDROData_IAltitudeObject) HYDROData_Object::GetAltitudeObject() const
{
  //DEBTRACE("GetAltitudeObject");
  return Handle(HYDROData_IAltitudeObject)::DownCast(
           GetReferenceObject( DataTag_AltitudeObject ) );
}

void HYDROData_Object::RemoveAltitudeObject()
{
  DEBTRACE("RemoveAltitudeObject");
  Handle(HYDROData_IAltitudeObject) aPrevAltitude = GetAltitudeObject();
  if ( aPrevAltitude.IsNull() )
    return;
  DEBTRACE("aPrevAltitude: " << aPrevAltitude->GetName())
  ClearReferenceObjects( DataTag_AltitudeObject );

  // #636: In the case of the altitude object change the geometry of the main object is not changed,
  // to the object should not be marked as updated
  //SetToUpdate( true );
}

void HYDROData_Object::SetFillingColor( const QColor& theColor )
{
  //DEBTRACE("SetFillingColor");
  SetColor( theColor, DataTag_FillingColor );
}

QColor HYDROData_Object::GetFillingColor() const
{
  //DEBTRACE("GetFillingColor");
  return GetColor( DefaultFillingColor(), DataTag_FillingColor );
}

void HYDROData_Object::SetBorderColor( const QColor& theColor )
{
  //DEBTRACE("SetBorderColor");
  SetColor( theColor, DataTag_BorderColor );
}

QColor HYDROData_Object::GetBorderColor() const
{
  //DEBTRACE("GetBorderColor");
  return GetColor( DefaultBorderColor(), DataTag_BorderColor );
}

QColor HYDROData_Object::DefaultFillingColor() const
{
  //DEBTRACE("DefaultFillingColor");
  return QColor( Qt::yellow );
}

QColor HYDROData_Object::DefaultBorderColor() const
{
  //DEBTRACE("DefaultBorderColor");
  return QColor( Qt::transparent );
}

QStringList HYDROData_Object::dumpObjectCreation( MapOfTreatedObjects& theTreatedObjects ) const
{
  DEBTRACE("dumpObjectCreation");
  QStringList aResList = HYDROData_Entity::dumpObjectCreation( theTreatedObjects );
  if ( aResList.isEmpty() )
    return aResList; //Object was not created

  QStringList aColorsDef;

  QColor aFillingColor = GetFillingColor();
  setPythonObjectColor( aColorsDef, aFillingColor, DefaultFillingColor(), "SetFillingColor" );

  QColor aBorderColor = GetBorderColor();
  setPythonObjectColor( aColorsDef, aBorderColor, DefaultBorderColor(), "SetBorderColor" );

  if ( !aColorsDef.isEmpty() )
  {
    aResList << aColorsDef;
    aResList << QString( "" );
  }

  return aResList;
}

ObjectKind HYDROData_Object::getAltitudeObjectType() const
{
  return KIND_UNKNOWN;
}

Handle(HYDROData_IAltitudeObject) HYDROData_Object::getChildAltitudeObject() const
{
  //DEBTRACE("getChildAltitudeObject");
  Handle(HYDROData_IAltitudeObject) anObject;

  TDF_Label aLabel = myLab.FindChild( DataTag_ChildAltitudeObject, false );
  if ( !aLabel.IsNull() )
  {
    TDF_Label aChildLabel = aLabel.FindChild( 0, false );
    if ( !aChildLabel.IsNull() )
    {
      anObject = Handle(HYDROData_IAltitudeObject)::DownCast(
        HYDROData_Iterator::Object( aChildLabel ) );
    }
  }

  return anObject;
}

void HYDROData_Object::checkAndSetAltitudeObject()
{
  DEBTRACE("checkAndSetAltitudeObject");
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();

  ObjectKind anAltitudeObjectType = getAltitudeObjectType();
  DEBTRACE("HYDROData_Object::checkAndSetAltitudeObject anAltitudeObjectType="<< anAltitudeObjectType);
  if ( anAltitudeObjectType == KIND_UNKNOWN )
  {
    DEBTRACE("anAltitudeObjectType == KIND_UNKNOWN");
    return; // No need to create altitude object
  }

  Handle(HYDROData_IAltitudeObject) altObject = GetAltitudeObject();
  if( !altObject.IsNull() )
      DEBTRACE(altObject->GetName());
  if( !altObject.IsNull() && altObject->GetKind()==anAltitudeObjectType )
    return;

  TDF_Label aChildLabel = myLab.FindChild( DataTag_ChildAltitudeObject ).FindChild( 0 );

  Handle(HYDROData_IAltitudeObject) anAltitudeObject =
    Handle(HYDROData_IAltitudeObject)::DownCast(
      HYDROData_Iterator::CreateObject( aChildLabel, anAltitudeObjectType ) );

  QString anAltitudePref = GetName() + "_Altitude";
  DEBTRACE("anAltitudePref " << anAltitudePref.toStdString());
  QString anAltitudeName = HYDROData_Tool::GenerateObjectName( aDocument, anAltitudePref );
  anAltitudeObject->SetName( anAltitudeName );

  SetAltitudeObject( anAltitudeObject );
}

Handle(HYDROData_ShapesGroup) HYDROData_Object::createGroupObject()
{
  DEBTRACE("createGroupObject");
  TDF_Label aNewLab = myLab.FindChild( DataTag_EdgesGroup ).NewChild();

  Handle(HYDROData_ShapesGroup) aNewGroup =
    Handle(HYDROData_ShapesGroup)::DownCast( HYDROData_Iterator::CreateObject( aNewLab, KIND_SHAPES_GROUP ) );
  AddReferenceObject( aNewGroup, DataTag_EdgesGroup );

  return aNewGroup;
}

void HYDROData_Object::RemoveGroupObjects()
{
  DEBTRACE("RemoveGroupObjects");
  TDF_Label aLabel = myLab.FindChild( DataTag_EdgesGroup, false );
  if ( !aLabel.IsNull() )
    aLabel.ForgetAllAttributes();
}

void HYDROData_Object::RemoveTopShape()
{
  DEBTRACE("RemoveTopShape");
  HYDROData_Entity::SetShape( DataTag_TopShape, TopoDS_Shape() );
}

void HYDROData_Object::RemoveShape3D()
{
  DEBTRACE("RemoveShape3D");
  HYDROData_Entity::SetShape( DataTag_Shape3D, TopoDS_Shape() );
}

bool HYDROData_Object::IsSubmersible() const
{
  //DEBTRACE("IsSubmersible");
  Handle(TDataStd_Integer) aSubMersibleAttr;

  bool isSubmersible = true; //default
  if( myLab.FindAttribute(TDataStd_Integer::GetID(), aSubMersibleAttr ) )
  {
    int aValue = aSubMersibleAttr->Get();
    isSubmersible = ( aValue != 0 );
  }
  return isSubmersible;
}

void HYDROData_Object::SetIsSubmersible( bool isSubmersible ) const
{
  DEBTRACE("SetIsSubmersible");
  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( myLab, isSubmersible ? 1 : 0 );
  anAttr->SetID(TDataStd_Integer::GetID());
}

void HYDROData_Object::GetBoundaries( QList<TopoDS_Shape>& theBoundShapes,
                                      QStringList& theBoundNames ) const
{
  DEBTRACE("GetBoundaries");
  HYDROData_SequenceOfObjects aGroups = GetGroups();
    HYDROData_SequenceOfObjects::Iterator anIter( aGroups );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_ShapesGroup) aGroup =
        Handle(HYDROData_ShapesGroup)::DownCast( anIter.Value() );
      if( aGroup.IsNull() )
        continue;

      QString aName = aGroup->GetName();
      TopTools_SequenceOfShape aShapes;
      aGroup->GetShapes( aShapes );
      for( int i=1, n=aShapes.Length(); i<=n; i++ )
      {
        theBoundShapes.append( aShapes( i ) );
        theBoundNames.append( aName );
      }
    }
}

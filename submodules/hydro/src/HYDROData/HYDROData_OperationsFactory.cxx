// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include<HYDROData_OperationsFactory.h>

#include<HYDROData_Document.h>
#include<HYDROData_Iterator.h>

#include<ImageComposer_ColorMaskOperator.h>
#include<ImageComposer_CropOperator.h>
#include<ImageComposer_CutOperator.h>
#include<ImageComposer_FuseOperator.h>

#include <typeinfo>

// global instance
HYDROData_OperationsFactory* FACTORY = 0;

HYDROData_OperationsFactory* HYDROData_OperationsFactory::Factory()
{
  if (!FACTORY) {
    FACTORY = new HYDROData_OperationsFactory;
    // default operations
    REGISTER_HYDRO_OPERATION(ImageComposer_ColorMaskOperator)
    REGISTER_HYDRO_OPERATION(ImageComposer_CropOperator)
    REGISTER_HYDRO_OPERATION(ImageComposer_CutOperator)
    REGISTER_HYDRO_OPERATION(ImageComposer_FuseOperator)
  }
  return FACTORY;
}

void HYDROData_OperationsFactory::Register(
  ImageComposer_Operator* theOperator)
{
  if ( !theOperator )
    return;

  FACTORY->myOps[ theOperator->name() ] = theOperator;
}

HYDROData_OperationsFactory::HYDROData_OperationsFactory()
{
}

ImageComposer_Operator* HYDROData_OperationsFactory::Operator(
  Handle(HYDROData_Image) theImage ) const
{
  // retreive operator instance by name
  ImageComposer_Operator* anOp = Operator( theImage->OperatorName() );
  if ( !anOp )
    return anOp;

  // fill arguments of the operator from theImage
  anOp->setBinArgs( theImage->Args() );

  return anOp;
}

ImageComposer_Operator* HYDROData_OperationsFactory::Operator(const QString theName) const
{
  return myOps.contains( theName ) ? myOps[ theName ] : NULL;
}

Handle(HYDROData_Image) HYDROData_OperationsFactory::CreateImage(
  Handle(HYDROData_Document) theDoc, const ImageComposer_Operator* theOperator)
{
  // create an object
  Handle(HYDROData_Image) anImage = 
    Handle(HYDROData_Image)::DownCast(theDoc->CreateObject(KIND_IMAGE));
  // get data from operation
  if (theOperator) {
    anImage->SetOperatorName( theOperator->name() );
    anImage->SetArgs( theOperator->getBinArgs() );
  }
  return anImage;
}

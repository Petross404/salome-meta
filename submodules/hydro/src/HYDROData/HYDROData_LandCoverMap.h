// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_LANDCOVER_MAP_HeaderFile
#define HYDROData_LANDCOVER_MAP_HeaderFile

#include <HYDROData_Entity.h>
#include <TDataStd_ExtStringArray.hxx>
#include <NCollection_IndexedDataMap.hxx>
#include <QString>
#include <TopoDS_Face.hxx>
#include <TopExp_Explorer.hxx>
#include <vector>
#include <set>

class HYDROData_StricklerTable;
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>

typedef NCollection_IndexedDataMap<TopoDS_Face, QString> HYDROData_MapOfFaceToStricklerType;

class TopoDS_Shape;
class TopoDS_Wire;
class TopoDS_Iterator;
class HYDROData_PolylineXY;
class HYDROData_Object;
class gp_XY;

class HYDROData_LandCoverMap : public HYDROData_Entity
{
protected:
  friend class HYDROData_Iterator;

  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Shape,                                         ///< the shape presentation of the land cover map
    DataTag_Types,
    DataTag_Transparency,
  };

public:
  class Explorer
  {
  public:
    Explorer( const HYDROData_LandCoverMap& );
    HYDRODATA_EXPORT Explorer( const Handle( HYDROData_LandCoverMap )& );
    HYDRODATA_EXPORT ~Explorer();

    void Init( const HYDROData_LandCoverMap& );
    HYDRODATA_EXPORT bool More() const;
    HYDRODATA_EXPORT void Next();

    int Index() const;
    HYDRODATA_EXPORT TopoDS_Face Face() const;
    HYDRODATA_EXPORT QString StricklerType() const;
    void SetStricklerType( const QString& );

  private:
    TopExp_Explorer* myExplorer;
    int              myIndex;
    Handle(TDataStd_ExtStringArray) myArray;
  };

  enum DBFStatus
  {
    DBFStatus_OK,
    DBFStatus_DIFF_SIZE_ERROR,
    DBFStatus_OPEN_FILE_ERROR,
    DBFStatus_NO_SUCH_FIELD_ERROR,
    DBFStatus_NO_DBFVALUES_CORRESPONDENCE_WARNING
  };

  HYDRODATA_EXPORT HYDROData_LandCoverMap();
  HYDRODATA_EXPORT virtual ~HYDROData_LandCoverMap();

  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const;

  HYDRODATA_EXPORT bool ImportSHP( const QString& theSHPFileName,
                                   const QList<int>& theIndices = QList<int>() );

  HYDRODATA_EXPORT bool ExportSHP( const QString& theSHPFileName, bool bUseDiscr = false, double theDefl = 0.1) const;

  HYDRODATA_EXPORT DBFStatus ImportDBF( const QString& theDBFFileName,
                                        const QString& theFieldName,
                                        const QStringList& DBFValues,
                                        const QStringList& StricklerTypes,
                                        const QList<int>& theIndices = QList<int>() );

  HYDRODATA_EXPORT void ExportDBF( const QString& theDBFFileName,
                                   const QString& theFieldName,
                                   const QStringList& theDBFValues,
                                   const QStringList& theStricklerTypes ) const;

  HYDRODATA_EXPORT bool ExportTelemac( const QString& theFileName,
                                       double theDeflection,
                                       const Handle(HYDROData_StricklerTable)& theTable,
                                       QString& statMessage) const;

  HYDRODATA_EXPORT bool Add( const Handle( HYDROData_Object )&, const QString& theType );
  HYDRODATA_EXPORT bool Add( const Handle( HYDROData_PolylineXY )&, const QString& theType );

  using HYDROData_Entity::Remove;
  HYDRODATA_EXPORT bool Remove( const TopoDS_Face& );
  HYDRODATA_EXPORT bool Remove( const TopTools_ListOfShape& );

  HYDRODATA_EXPORT bool Split( const Handle( HYDROData_PolylineXY )& );
  HYDRODATA_EXPORT bool Split( const TopoDS_Shape& );
  HYDRODATA_EXPORT bool Merge( const TopTools_ListOfShape&, const QString& theType );

  HYDRODATA_EXPORT bool ChangeType( const TopTools_ListOfShape&, const QString& theType );

  HYDRODATA_EXPORT TopoDS_Face FindByPoint( const gp_Pnt2d&, QString& theType ) const;

  HYDRODATA_EXPORT TopoDS_Shape GetShape() const;

  HYDRODATA_EXPORT QString StricklerType( const TopoDS_Face& ) const;

  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  HYDRODATA_EXPORT int GetLCCount() const;
  HYDRODATA_EXPORT bool IsEmpty() const;

  HYDRODATA_EXPORT void StoreLandCovers( const HYDROData_MapOfFaceToStricklerType& );

  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  HYDRODATA_EXPORT void SetTransparency( double );
  HYDRODATA_EXPORT double GetTransparency() const;

  HYDRODATA_EXPORT void RemoveInternal(TopoDS_Shape& ShToRebuild, NCollection_IndexedDataMap<TopoDS_Face, TopoDS_Face>* aF2FReplace);

  HYDRODATA_EXPORT bool CheckLinear();

  HYDRODATA_EXPORT virtual void UpdateLocalCS( double theDx, double theDy );

  HYDRODATA_EXPORT void ClassifyPoints( const std::vector<gp_XY>& thePoints,
                                        std::vector<std::set <QString> >& theTypes ) const;

  HYDRODATA_EXPORT void ClassifyPoints( const std::vector<gp_XY>& thePoints,
                                        const Handle(HYDROData_StricklerTable)& theTable,
                                        std::vector<int>& theTypes ) const;

  HYDRODATA_EXPORT void ClassifyPoints( const std::vector<gp_XY>& thePoints,
                                        const Handle(HYDROData_StricklerTable)& theTable,
                                        std::vector<double>& theCoeffs, double DefValue, bool UseMax ) const;

protected:
  void SetShape( const TopoDS_Shape& );

  bool Add( const TopoDS_Wire&, const QString& );

  bool LocalPartition( const TopoDS_Shape&, const QString& theNewType );

  static TopoDS_Shape MergeFaces(const TopTools_ListOfShape& theFaces,
                                 bool IsToUnify,
                                 TopTools_IndexedDataMapOfShapeListOfShape* theShHistory = NULL,
                                 double theTolerance = 1E-5 );

public:
  DEFINE_STANDARD_RTTIEXT( HYDROData_LandCoverMap, HYDROData_Entity );

private:
  friend class Explorer;
  friend class test_HYDROData_LandCoverMap;
};

#endif

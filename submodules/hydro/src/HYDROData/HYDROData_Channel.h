// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Channel_HeaderFile
#define HYDROData_Channel_HeaderFile

#include "HYDROData_ArtificialObject.h"

#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

class HYDROData_Polyline3D;
class HYDROData_Profile;

/**\class HYDROData_Channel
 * \brief 
 *
 */
class HYDROData_Channel : public HYDROData_ArtificialObject
{
public:

  struct PrsDefinition
  {
    TopoDS_Shape myPrs3D;
    TopoDS_Face  myPrs2D;
    TopoDS_Shape myLeftBank;
    TopoDS_Shape myRightBank;
    TopoDS_Shape myInlet;
    TopoDS_Shape myOutlet;
  };

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_ArtificialObject::DataTag_First + 100, ///< first tag, to reserve
    DataTag_GuideLine, 
    DataTag_Profile,
    DataTag_EquiDistance,
    DataTag_ProfileMode,
    DataTag_LC,
    DataTag_DeltaZ,
    DataTag_CoteZ
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Channel, HYDROData_ArtificialObject);

public:

  /**
   * Creates the presentations(2D and 3D) by given guide line and profile.
   */

  HYDRODATA_EXPORT static bool CreatePresentations( const Handle(HYDROData_Polyline3D)& theGuideLine,
                                                    const Handle(HYDROData_Profile)&    theProfile,
                                                    PrsDefinition&                      thePrs,
                                                    double                              theEquiDistance,
                                                    bool                                ReverseXCoord = false);

  HYDRODATA_EXPORT static bool CreatePresentations( const Handle(HYDROData_Polyline3D)& theGuideLine,
                                                    double                              LC,
                                                    double                              deltaZ,
                                                    double                              coteZ,
                                                    PrsDefinition&                      thePrs,
                                                    double                              theEquiDistance,
                                                    bool                                ReverseXCoord = false);

protected:
  static bool internalCreatePresentations( bool mode,
                                           const Handle(HYDROData_Polyline3D)& theGuideLine,
                                           const Handle(HYDROData_Profile)&    theProfile,
                                           const TopoDS_Wire&                  theProfWire,
                                           const gp_Pnt&                       theMiddlePnt,
                                           PrsDefinition&                      thePrs,
                                           double                              theEquiDistance,
                                           bool                                ReverseXCoord);

public:

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_CHANNEL;}

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;

  /**
   * Returns the left edge of the channel.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetLeftShape() const;

  /**
   * Returns the right edge of the channel.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetRightShape() const;

  /**
   * Update the shape presentations of stream.
   * Call this method whenever you made changes for channel data.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Checks that object has 2D presentation. Reimlemented to retun true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Returns default filling color for new channel.
   */
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new channel.
   */
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;


public:      
  // Public methods to work with Channel
  
  /**
   * Sets reference guide line object for channel.
   */
  HYDRODATA_EXPORT virtual bool SetGuideLine( const Handle(HYDROData_Polyline3D)& theGuideLine );

  /**
   * Returns reference guide line object of channel.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Polyline3D) GetGuideLine() const;

  /**
   * Remove reference guide line object from channel.
   */
  HYDRODATA_EXPORT virtual void RemoveGuideLine();


  /**
   * Sets reference profile object for channel.
   */
  HYDRODATA_EXPORT virtual bool SetProfile( const Handle(HYDROData_Profile)& theProfile );

  /**
   * Returns reference profile object of channel.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Profile) GetProfile() const;

  /**
   * Remove reference profile object from channel.
   */
  HYDRODATA_EXPORT virtual void RemoveProfile();

  HYDRODATA_EXPORT void SetEquiDistance( double );
  HYDRODATA_EXPORT double GetEquiDistance() const;

  
  HYDRODATA_EXPORT void SetLCValue( double );
  HYDRODATA_EXPORT double GetLCValue() const;
  
  HYDRODATA_EXPORT void SetDeltaZValue( double );
  HYDRODATA_EXPORT double GetDeltaZValue() const;
  
  HYDRODATA_EXPORT void SetCoteZValue( double );
  HYDRODATA_EXPORT double GetCoteZValue() const;

  HYDRODATA_EXPORT void SetProfileMode( bool );
  HYDRODATA_EXPORT bool GetProfileMode() const;

protected:
  /**
   * Returns the type of child altitude object.
   * Reimplemented to create chanel altitude object.
   */
  HYDRODATA_EXPORT virtual ObjectKind getAltitudeObjectType() const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Channel();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_Channel();
};

#endif

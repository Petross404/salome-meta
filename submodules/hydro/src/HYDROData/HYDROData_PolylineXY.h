// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_PolylineXY_HeaderFile
#define HYDROData_PolylineXY_HeaderFile

#include "HYDROData_IPolyline.h"
#include <TopTools_HSequenceOfShape.hxx>
#include <QString>
#include <QStringList>

class QPainterPath;
class QTransform;
class TopoDS_Wire;
class gp_XYZ;
class gp_Pnt;
class HYDROData_Document;

/**\class HYDROData_PolylineXY
 * \brief Class that stores/retrieves information about the
 *        parametric profile points.
 */
class HYDROData_PolylineXY : public HYDROData_IPolyline
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_IPolyline::DataTag_First + 100, ///< first tag, to reserve
    DataTag_GeomObjectEntry,  ///< study entry of the imported GEOM object
    DataTag_DBFTableInfo
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_PolylineXY, HYDROData_IPolyline);


  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_POLYLINEXY;}

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Update the wire contour on the basis of the polyline data.
   * Call this method whenever you made changes for polyline data.
   */
  HYDRODATA_EXPORT virtual void Update();

  HYDRODATA_EXPORT virtual void UpdateLocalCS( double theDx, double theDy );

  /**
   * Checks that object has 2D presentation. Re-implemented to return true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Returns data of object wrapped to QVariant.
   * Re-implemented to wrap and return saved path.
   */
  HYDRODATA_EXPORT virtual QVariant GetDataVariant();

  /**
   * Returns default wire color for new polyline.
   */
  HYDRODATA_EXPORT static QColor DefaultWireColor();

  HYDRODATA_EXPORT bool IsCustom() const;
  HYDRODATA_EXPORT bool GetIsInCustomFlag() const;
  HYDRODATA_EXPORT void SetIsInCustomFlag( bool theValue );

public:

  /**
   * Returns the presentation of polyline section in CAS manner.
   */
  HYDRODATA_EXPORT static TopoDS_Wire BuildWire( const SectionType&                  theType,
                                                 const bool&                         theIsClosed,
                                                 const NCollection_Sequence<gp_XYZ>& thePoints );

  /**
   * Returns the presentation of polyline section in Qt manner.
   */
  HYDRODATA_EXPORT static void BuildPainterPath( QPainterPath&                       thePath,
                                                 const SectionType&                  theType,
                                                 const bool&                         theIsClosed,
                                                 const NCollection_Sequence<gp_XYZ>& thePoints );

public:

  /**
   * Imports shape from IOR.
   * \param theIOR the IOR of Geom object
   * \return \c true if shape has been successfully imported
   */
  HYDRODATA_EXPORT virtual bool ImportFromGeomIOR( const TCollection_AsciiString& theIOR );

  /**
   * Stores the study entry of the imported GEOM object.
   * \param theEntry GEOM object entry
   */
  HYDRODATA_EXPORT void SetGeomObjectEntry( const TCollection_AsciiString& theEntry );

  /**
   * Returns the imported GEOM object entry.
   */
  HYDRODATA_EXPORT TCollection_AsciiString GetGeomObjectEntry() const;

public:

  /**
   * Returns the 3D presentation of all points.
   */
  HYDRODATA_EXPORT virtual bool ImportShape( const TopoDS_Shape& theShape,
                                             bool IsInterpolationAllowed,
                                             const Handle( HYDROData_PolylineXY )& theOldPolyline,
                                             bool IsClosureAllowed = true,
                                             double theDeviation = 1.);

  /**
   * Returns flag indicating that polyline can be edited or not.
   */
  HYDRODATA_EXPORT virtual bool IsEditable() const;

  
  /**
   * Returns true if polyline is closed
   * \param theIsSimpleCheck flag indicating the type of checking
   *        - if true then all wires checked on closures
   *        - if false then for positive result polyline should consist of
   *          only one wire and which must be closed
   */
  HYDRODATA_EXPORT bool IsClosed( const bool theIsSimpleCheck = true ) const;

  /**
   * Returns connected wires and their number, by rebuild of the wires from the edges, looking for connections
   */
  HYDRODATA_EXPORT int GetNbConnectedWires(Handle(TopTools_HSequenceOfShape)& aConnectedWires) const;

   /**
   * Returns the distance between first and point with index thePointIndex
   * at the section with index theSectionIndex. -1 is returned if error is occurred.
   */
  HYDRODATA_EXPORT double GetDistance( const int theSectionIndex,
                                       const int thePointIndex ) const;

  /**
   * Returns number of sections.
   */
  HYDRODATA_EXPORT virtual int NbSections() const;

  /**
   * Adds new one section.
   * \param theSectName name of the section
   * \param theSectionType type of section
   * \param theIsClosed flag indicates closures of section
   */
  HYDRODATA_EXPORT virtual void AddSection( const TCollection_AsciiString& theSectName,
                                            const SectionType              theSectionType,
                                            const bool                     theIsClosed );

  /**
   * Returns name of section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual TCollection_AsciiString GetSectionName( const int theSectionIndex ) const;

  /**
   * Set name for section with given index.
   * \param theSectionIndex index of section
   * \param theSectionName new section name
   */
  HYDRODATA_EXPORT virtual void SetSectionName( const int                      theSectionIndex, 
                                                const TCollection_AsciiString& theSectionName );

  /**
   * Returns type of section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual SectionType GetSectionType( const int theSectionIndex ) const;

  /**
   * Set type for section with given index.
   * \param theSectionIndex index of section
   * \param theSectionType new section type
   */
  HYDRODATA_EXPORT virtual void SetSectionType( const int         theSectionIndex, 
                                                const SectionType theSectionType );

  /**
   * Returns true if section with given index is closed.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual bool IsClosedSection( const int theSectionIndex ) const;

  /**
   * Set closed flag for section with given index.
   * \param theSectionIndex index of section
   * \param theIsClosed new closures state
   */
  HYDRODATA_EXPORT virtual void SetSectionClosed( const int  theSectionIndex, 
                                                  const bool theIsClosed );

  /**
   * Adds new one section.
   * \param theSectName name of the section
   * \param theSectionType type of section
   * \param theIsClosed flag indicates closures of section
   */
  HYDRODATA_EXPORT virtual void GetSections( NCollection_Sequence<TCollection_AsciiString>& theSectNames,
                                             NCollection_Sequence<SectionType>&             theSectTypes,
                                             NCollection_Sequence<bool>&                    theSectClosures ) const;

  /**
   * Removes section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual void RemoveSection( const int theSectionIndex );

  /**
   * Removes all sections.
   */
  HYDRODATA_EXPORT virtual void RemoveSections();


  /**
   * Adds new point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoint point to add
   * \param theBeforeIndex if not equal -1 then insert point in this pos
   */
  HYDRODATA_EXPORT virtual void AddPoint( const int    theSectionIndex,
                                          const Point& thePoint,
                                          const int    thePointIndex = -1 );

  /**
   * Replaces point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoint new point
   * \param thePointIndex index of point to replace
   */
  HYDRODATA_EXPORT virtual void SetPoint( const int    theSectionIndex,
                                          const Point& thePoint,
                                          const int    thePointIndex );

  /**
   * Replaces point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoints new points
   */
  HYDRODATA_EXPORT virtual void SetPoints( const int         theSectionIndex,
                                           const PointsList& thePoints );

  /**
   * Removes point from section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePointIndex index of point
   */
  HYDRODATA_EXPORT virtual void RemovePoint( const int theSectionIndex,
                                             const int thePointIndex );


  /**
   * Returns list of points.
   * \param theSectionIndex if not equal -1 then list of points returned
   *                        only for section with this index
   * \return list of points
   */
  HYDRODATA_EXPORT virtual PointsList GetPoints( const int theSectionIndex = -1, bool IsConvertToGlobal = false ) const;


  /**
   * Returns the painter path.
   * Note: currently only the first section of the polyline data is taken into account.
   * \return polyline painter path.
   */
  HYDRODATA_EXPORT virtual QPainterPath GetPainterPath() const;

  /**
   * Transform the polyline points.
   * @param theTrsf the transformation
   */
  HYDRODATA_EXPORT void Transform( const QTransform& theTrsf );

  HYDRODATA_EXPORT void SetDBFInfo(const QStringList& theDBFTable);  

  HYDRODATA_EXPORT bool GetDBFInfo(QStringList& theDBFTable) const;

  HYDRODATA_EXPORT static HYDROData_SequenceOfObjects ImportShapesFromFile( const QString& theFileName );

  HYDRODATA_EXPORT static void ExportShapeXY(Handle(HYDROData_Document) theDocument,
                                             const QString& aFileName,
                                             const NCollection_Sequence<Handle(HYDROData_PolylineXY)>& aPolyXYSeq,
                                             QStringList& aNonExpList);

protected:

  /**
   * Sets the flag indicating that polyline can be edited or not.
   */
  HYDRODATA_EXPORT virtual void setEditable( const bool theIsEditable );

  HYDRODATA_EXPORT void Interpolate();

protected:

  friend class HYDROData_Profile;
  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_PolylineXY();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_PolylineXY();

private:
  bool myIsInCustomFlag;
};

#endif

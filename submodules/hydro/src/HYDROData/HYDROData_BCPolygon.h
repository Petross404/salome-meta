// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_BCPolygon_HeaderFile
#define HYDROData_BCPolygon_HeaderFile

#include <HYDROData_Object.h>

class HYDROData_PolylineXY;

/**\class HYDROData_BCPolygon
 * \brief 
 *
 */
class HYDROData_BCPolygon : public HYDROData_Object
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Object::DataTag_First + 100,
    DataTag_Polyline,     ///< reference polyline
    DataTag_BoundaryType  ///< boundary type: cutting(1), bounding(2), selection(3) 
};

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_BCPolygon, HYDROData_Object);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_BC_POLYGON;}

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;

  /**
   * Update the object.
   * Call this method whenever you made changes for object data.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Update the object with new top shape.
   * Call this method whenever you made changes for object data.
   */
  HYDRODATA_EXPORT void Update(const TopoDS_Shape& theTopShape);

  /**
   * Checks that object has 2D presentation. Reimlemented to retun true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Returns the 3d shape of the object.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape3D() const;

  /**
   * Returns default filling color for new zone.
   */
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new zone.
   */
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;

  /**
   * Sets reference polyline object for zone.
   */
  HYDRODATA_EXPORT virtual void SetPolyline( const Handle(HYDROData_PolylineXY)& thePolyline );

  /**
   * Returns reference polyline object of zone.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_PolylineXY) GetPolyline() const;

  /**
   * Remove reference polyline object of zone.
   */
  HYDRODATA_EXPORT virtual void RemovePolyline();

  HYDRODATA_EXPORT TopoDS_Shape generateTopShape() const;

  HYDRODATA_EXPORT static TopoDS_Shape generateTopShape( const Handle(HYDROData_PolylineXY)& );

  HYDRODATA_EXPORT int GetBoundaryType() const;

  HYDRODATA_EXPORT void SetBoundaryType( int ) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_BCPolygon();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_BCPolygon();
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_IInterpolator_HeaderFile
#define HYDROData_IInterpolator_HeaderFile

#include "HYDROData_IAltitudeObject.h"

class gp_XY;

/**\class HYDROData_IInterpolator
 * \brief The base class to provide interface for interpolation.
 */
class HYDROData_IInterpolator
{
public:

  /**
   * Public empty constructor.
   */
  HYDRODATA_EXPORT HYDROData_IInterpolator();

  /**
   * Public virtual destructor.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_IInterpolator();

public:

  /**
   * Returns the altitude value for given point.
   * Reimplement this method in order to change the interpolation algorithm.
   */
  virtual HYDRODATA_EXPORT double GetAltitudeForPoint( 
    const double theCoordX, const double theCoordY ) const;

  /**
   * Returns the altitude value for given point.
   */
  HYDRODATA_EXPORT double GetAltitudeForPoint( const gp_XY& thePoint ) const;


public:

  /**
   * Sets the altitude object for interpolation.
   */
  HYDRODATA_EXPORT void SetAltitudeObject( 
    const Handle(HYDROData_IAltitudeObject)& theAltitude );

  /**
   * Returns the altitude object for interpolation.
   */
  HYDRODATA_EXPORT Handle(HYDROData_IAltitudeObject) GetAltitudeObject() const;

private:
  Handle(HYDROData_IAltitudeObject)  myAltitudeObject;
};

#endif

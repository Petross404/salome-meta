// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_CalculationCase_HeaderFile
#define HYDROData_CalculationCase_HeaderFile

#include <HYDROData_ShapesGroup.h>
#include <HYDROData_SplitToZonesTool.h>
#include <HYDROData_PriorityQueue.h>
#include <HYDROData_Zone.h>
#include <HYDROData_Warning.h>
#include <vector>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#ifndef LIGHT_MODE
// IDL includes
#include <SALOMEconfig.h>
#include CORBA_SERVER_HEADER(GEOM_Gen)
#endif

#ifdef WIN32
  #pragma warning( default: 4251 )
#endif

class gp_XY;

class TopoDS_Shape;
class TopoDS_Shell;

class HYDROData_Object;
class HYDROData_Region;
class HYDROData_Zone;
class HYDROData_PolylineXY;
class HYDROData_ShapesGroup;
class HYDROData_SplitShapesGroup;
class HYDROData_Document;
class HYDROData_StricklerTable;
class HYDROData_LandCoverMap;
class HYDROData_BCPolygon;

/**\class HYDROData_CalculationCase
 * \brief Calculation case is defined by selection of Geometry objects with or without �Zone of water�.
 *
 */
class HYDROData_CalculationCase : public HYDROData_Entity
{
public:

  enum PointClassification
  {
    POINT_OUT,  ///< point is out of zone face
    POINT_IN,   ///< point is inside of zone face
    POINT_ON    ///< point is on the edge of zone face
  };

  enum AssignmentMode
  {
    MANUAL = 0,
    AUTOMATIC,
  };

public:

  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_GeometryObject,            ///< reference geometry objects
    DataTag_ChildRegion,               ///< child regions
    DataTag_Region,                    ///< reference regions
    DataTag_Polyline,                  ///< reference boundary polyline
    DataTag_GeometryGroup,             ///< reference geometry groups
    DataTag_SplitGroups,               ///< reference split groups
    DataTag_CustomRules,               ///< custom rules
    DataTag_AssignmentMode,            ///< assignment mode
    DataTag_StricklerTable,            ///< reference Strickler table
    DataTag_InterPoly,                 ///< intersection polyline

    DataTag_LandCover_Obsolete,                 ///< reference land covers
    DataTag_CustomLandCoverRules_Obsolete,      ///< custom rules for land covers priority
    DataTag_AssignmentLandCoverMode_Obsolete,   ///< assignment mode of land covers priority
    DataTag_ChildLandCoverRegion_Obsolete,      ///< child land cover regions
    DataTag_LandCoverRegion_Obsolete,           ///< reference land cover regions

    DataTag_LandCoverMap,              ///< reference to land cover map
    DataTag_BCPolygon                  ///< reference boundary polygons
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_CalculationCase, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_CALCULATION; }

  /**
   * Updates the name of this object.
   * Reimplemented to update the names of regions, zones and split groups.
   */
  HYDRODATA_EXPORT virtual void SetName( const QString& theName, bool isDefault = false );

  /**
   * Dump Calculation object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Update the calcualtion case object.
   * Call this method whenever you made changes for object data.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;

public:
  // Public methods to work with Calculation

  /**
   * Add new one reference geometry object for calculation case.
   */
  HYDRODATA_EXPORT virtual bool AddGeometryObject( const Handle(HYDROData_Object)& theObject );

  /**
   * Returns all reference geometry objects of calculation case.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetGeometryObjects() const;

  /**
   * Removes reference geometry object from calculation case.
   * \param theObject the object to remove
   */
  HYDRODATA_EXPORT virtual void RemoveGeometryObject( const Handle(HYDROData_Object)& theObject );

  /**
   * Removes all reference geometry objects from calculation case.
   */
  HYDRODATA_EXPORT virtual void RemoveGeometryObjects();


  /**
   * Add new one reference geometry group for calculation case.
   * \param theGroup the group to add
   */
  HYDRODATA_EXPORT virtual bool AddGeometryGroup( const Handle(HYDROData_ShapesGroup)& theGroup );

  /**
   * Returns all reference geometry groups of calculation case.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetGeometryGroups() const;

  /**
   * Removes reference geometry group from calculation case.
   * \param theGroup the group to remove
   */
  HYDRODATA_EXPORT virtual void RemoveGeometryGroup( const Handle(HYDROData_ShapesGroup)& theGroup );

  /**
   * Removes all reference geometry groups from calculation case.
   */
  HYDRODATA_EXPORT virtual void RemoveGeometryGroups();


  /**
   * Sets reference boundary polyline object for calculation case.
   */
  HYDRODATA_EXPORT virtual void SetBoundaryPolyline( const Handle(HYDROData_PolylineXY)& thePolyline );

  /**
   * Returns reference boundary polyline object of calculation case.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_PolylineXY) GetBoundaryPolyline() const;

  /**
   * Remove reference boundary polyline object from calculation case.
   */
  HYDRODATA_EXPORT virtual void RemoveBoundaryPolyline();


  /**
   * Sets reference Strickler table for calculation case.
   */
  HYDRODATA_EXPORT virtual void SetStricklerTable( const Handle(HYDROData_StricklerTable)& theStricklerTable );

  /**
   * Returns reference Strickler table of calculation case.
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_StricklerTable) GetStricklerTable() const;

  /**
   * Remove reference Strickler table from calculation case.
   */
  HYDRODATA_EXPORT virtual void RemoveStricklerTable();

  /**
   * Returns reference Land Cover Map of calculation case.
   * \return the referenced Land Cover Map
   */
  HYDRODATA_EXPORT Handle(HYDROData_LandCoverMap) GetLandCoverMap() const;

  HYDRODATA_EXPORT void SetLandCoverMap( const Handle(HYDROData_LandCoverMap)& );

  /**
   * Add new one child region for calculation case.
   * The new region is added into the list of reference regions.
   * The label of theZone is changed during this operation
   * because of new region becomes the new parent for this zone.
   * \return the created region
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Region) AddNewRegion( const Handle(HYDROData_Zone)& theZone );


  /**
   * Add new one reference region for calculation case.
   * The label of theRegion is changed in case if old parent is not this calculation.
   * \return true in case of success
   */
  HYDRODATA_EXPORT virtual bool AddRegion( const Handle(HYDROData_Region)& theRegion );

  /**
   * Returns all reference regions of calculation case.
   * \return the list of reference regions
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetRegions() const;

  /**
   * Updates names of regions to correct order.
   */
  HYDRODATA_EXPORT virtual void UpdateRegionsOrder();

  /**
   * Removes reference region from calculation case.
   * \param theRegion the region to remove
   */
  HYDRODATA_EXPORT virtual void RemoveRegion( const Handle(HYDROData_Region)& theRegion );

  /**
   * Removes all reference regions from calculation case.
   */
  HYDRODATA_EXPORT virtual void RemoveRegions();

  /**
   * Returns all reference geometry groups of calculation case.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetSplitGroups() const;

  /**
   * Removes all reference geometry groups from calculation case.
   */
  HYDRODATA_EXPORT virtual void RemoveSplitGroups();

  HYDRODATA_EXPORT virtual bool AddInterPoly( const Handle(HYDROData_PolylineXY)& theInterPolyline );

  HYDRODATA_EXPORT virtual void RemoveInterPolyObject( const Handle(HYDROData_PolylineXY)& theInterPolyline );

  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetInterPolyObjects() const;

  HYDRODATA_EXPORT virtual bool AddBoundaryPolygon( const Handle(HYDROData_BCPolygon)& theBCPolygon );

  HYDRODATA_EXPORT virtual void RemoveBoundaryPolygon( const Handle(HYDROData_BCPolygon)& theBCPolygon );

  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetBoundaryPolygons() const;

  /**
   * \brief Set the Container Name to use when not default (FactoryServer).
   * \param theContainerName the name of the container used for GEOM
   *        (to be used by scripts for distributed execution)
   */
  HYDRODATA_EXPORT virtual void SetContainerName( const QString& theContainerName );

  /**
   * Exports the calculation case data (shell and groups) to GEOM module.
   * \return the entry of the GEOM object (empty string in the case of error)
   */
  HYDRODATA_EXPORT virtual QString Export() const;

  /**
   * Exports the calculation case data (shell and groups) to GEOM module.
   * \param theGeomEngine GEOM module engine
   * \param theGeomObjEntry the published GEOM object entry
   * \param theErrorMsg the error message
   * \return true in case of success
   */
#ifndef LIGHT_MODE
  HYDRODATA_EXPORT virtual bool Export( GEOM::GEOM_Gen_var  theGeomEngine,
                                        QString& theGeomObjEntry,
                                        QString& theErrorMsg,
                                        QString& statMess) const;
#endif

public:
  // Public methods to work with Calculation services

  /**
   * Returns altitude for given point.
   * \param thePoint the point to examine
   * \return result altitude value
   */
  HYDRODATA_EXPORT virtual double GetAltitudeForPoint( const gp_XY& thePoint ) const;

  /**
   * Returns altitude for given point on given region.
   * \param thePoint the point to examine
   * \param theRegion reference region to check
   * \return result altitude value
   */
  HYDRODATA_EXPORT virtual double GetAltitudeForPoint( const gp_XY&                    thePoint,
                                                       const Handle(HYDROData_Region)& theRegion,
                                                       int theMethod = 0) const;

  /**
   * Returns altitude for given point on given zone.
   * \param thePoint the point to examine
   * \param theZone reference zone to check
   * \return result altitude value
   */
  HYDRODATA_EXPORT virtual double GetAltitudeForPoint( const gp_XY&                  thePoint,
                                                       const Handle(HYDROData_Zone)& theZone,
                                                       int theMethod = 0) const;

  /**
   * Returns strickler coefficient for given point.
   * \param thePoint the point to examine
   * \return result strickler coefficient
   */
  HYDRODATA_EXPORT double GetStricklerCoefficientForPoint( const gp_XY& thePoint ) const;

  HYDRODATA_EXPORT std::vector<double> GetStricklerCoefficientForPoints(const std::vector<gp_XY>& thePoints,
                                                                        double DefValue,
                                                                        bool UseMax ) const;

  HYDRODATA_EXPORT std::vector<int> GetStricklerTypeForPoints( const std::vector<gp_XY>& thePoints ) const;

  /**
   * Returns altitudes for given points on given region.
   * \param thePoints the points to examine
   * \param theRegion reference region to check
   * \return result altitude value
   */
  HYDRODATA_EXPORT virtual NCollection_Sequence<double> GetAltitudesForPoints(
    const NCollection_Sequence<gp_XY>& thePoints,
    const Handle(HYDROData_Region)&    theRegion,
    int theMethod = 0) const;

  /**
   * Returns altitudes for given points on given zone.
   * \param thePoints the points to examine
   * \param theZone reference zone to check
   * \return result altitude value
   */
  HYDRODATA_EXPORT virtual NCollection_Sequence<double> GetAltitudesForPoints(
    const NCollection_Sequence<gp_XY>& thePoints,
    const Handle(HYDROData_Zone)&      theZone,
    int theMethod = 0) const;

  /**
   * Returns region to which the point is belongs.
   * \param thePoint the point to examine
   * if it is needed to search Land Cover region
   * \return result region
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Region) GetRegionFromPoint( const gp_XY& thePoint ) const;

  /**
   * Returns zone to which the point is belongs.
   * \param thePoint the point to examine
   * if it is needed to search Land Cover zone
   * \return result zone
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Zone) GetZoneFromPoint( const gp_XY& thePoint ) const;

  /**
   * Returns classification of point for given zone.
   * \param thePoint the point to examine
   * \param theZone the zone to examine
   * \return result classification
   */
  HYDRODATA_EXPORT virtual PointClassification GetPointClassification(
    const gp_XY&                  thePoint,
    const Handle(HYDROData_Zone)& theZone ) const;

  HYDRODATA_EXPORT void SetAssignmentMode( AssignmentMode theMode );
  HYDRODATA_EXPORT AssignmentMode GetAssignmentMode() const;

  HYDRODATA_EXPORT void ClearRules( HYDROData_CalculationCase::DataTag theDataTag,
                                    const bool theIsSetToUpdate = true );
  HYDRODATA_EXPORT void AddRule( const Handle(HYDROData_Entity)&     theObject1,
                                 HYDROData_PriorityType              thePriority,
                                 const Handle(HYDROData_Entity)&     theObject2,
                                 HYDROData_Zone::MergeType theMergeType,
                                 HYDROData_CalculationCase::DataTag  theDataTag );
  HYDRODATA_EXPORT bool GetRule( int theIndex,
                                 Handle(HYDROData_Entity)&           theObject1,
                                 HYDROData_PriorityType&             thePriority,
                                 Handle(HYDROData_Entity)&           theObject2,
                                 HYDROData_Zone::MergeType& theMergeType,
                                 HYDROData_CalculationCase::DataTag& theDataTag ) const;

  HYDRODATA_EXPORT QString DumpRules() const;

  HYDRODATA_EXPORT HYDROData_Warning GetLastWarning() const;

private:

  /**
   * Add new one region for calculation case.
   * The new region is added into the list of reference regions.
   */

  public: //TEMP TODO
  HYDRODATA_EXPORT Handle(HYDROData_Region) addNewRegion( const Handle(HYDROData_Document)& theDoc,
                                         const QString& thePrefixOrName,
                                         bool isPrefix = true );

  /**
   * Add new one split edges group for calculation case.
   */
  Handle(HYDROData_SplitShapesGroup) addNewSplitGroup( const QString& theName );

  /**
   * Exports the given faces as shell and the given groups to GEOM module.
   * \param theGeomEngine GEOM module engine
   * \param theFaces the list of faces to make shell
   * \param theSplitGroups the list of groups
   * \return true in case of success
   */
#ifndef LIGHT_MODE
  bool Export( GEOM::GEOM_Gen_var                            theGeomEngine,
               const NCollection_IndexedDataMap<TopoDS_Shape, QString>& aFacesToName,
               const HYDROData_ShapesGroup::SeqOfGroupsDefs& theGroupsDefs,
               QString& theGeomObjEntry ) const;
#endif

  void CreateRegionsDef( const Handle(HYDROData_Document)& theDoc,
                         const HYDROData_SplitToZonesTool::SplitDataList& theZones );
  void CreateRegionsAuto( const Handle(HYDROData_Document)& theDoc,
                          const HYDROData_SplitToZonesTool::SplitDataList& theZones );

  void CreateEdgeGroupsDef( const Handle(HYDROData_Document)& theDoc,
                            const HYDROData_SplitToZonesTool::SplitDataList& theEdges );

  void DumpRulesToPython( const QString& theCalcCaseName, QStringList& theScript ) const;

  void SetWarning( HYDROData_WarningType theType = WARN_OK, const QString& theData = "" );

  void UpdateRegionsNames( const HYDROData_SequenceOfObjects& theRegions,
                           const QString& theOldCaseName,
                           const QString& theName );

  void DumpRegionsToPython( QStringList&                       theResList,
                            const QString&                     thePyScriptPath,
                            MapOfTreatedObjects&               theTreatedObjects,
                            const HYDROData_SequenceOfObjects& theRegions ) const;

protected:
  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_CalculationCase();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_CalculationCase();

  void DumpSampleMeshing( QStringList& theScript,
                          const QString& theGeomShapeName,
                          const QString& theMeshName ) const;

private:
  HYDROData_Warning myLastWarning;
};

#endif

// Copyright (C) 2007-2014  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  HYDRO HYDROData_Octree : Octree implementation (from SMESH module)
//  File      : HYDROData_Octree.hxx
//  Created   : Tue Jan 16 16:00:00 2007
//  Author    : Nicolas Geimer & Aurélien Motteux (OCC)
//  Module    : HYDROData
//
#ifndef _HYDROData_OCTREE_HXX_
#define _HYDROData_OCTREE_HXX_

#include <Standard.hxx>
#include <Standard_Macro.hxx>

#include "HYDROData_Tree.hxx"
#include <Bnd_B3d.hxx>

//================================================================================

/*!
 * \brief 3D tree of anything.
 * Methods to implement in a descendant are:
 * - Bnd_B3d*       buildRootBox(); // box of the whole tree
 * - descendant*    newChild() const; // a new child instance
 * - void           buildChildrenData(); // Fill in data of the children
 */
class Standard_EXPORT HYDROData_Octree: public HYDROData_Tree<Bnd_B3d, 8>
{
public:
  typedef HYDROData_Tree<Bnd_B3d, 8> TBaseTree;

  //! Constructor. limit must be provided at tree root construction.
  //! limit will be deleted by HYDROData_Octree
  HYDROData_Octree(HYDROData_TreeLimit* limit = 0);
  virtual ~HYDROData_Octree() {};

  //! Compute the biggest dimension of my box
  double maxSize() const;

  //! Return index of a child the given point is in
  inline static int getChildIndex(double x, double y, double z, const gp_XYZ& boxMiddle)
  {
    return (x > boxMiddle.X()) + (y > boxMiddle.Y()) * 2 + (z > boxMiddle.Z()) * 4;
  };

protected:

  //! Allocate a bndbox according to childIndex. childIndex is zero based
  virtual Bnd_B3d* newChildBox(int childIndex) const;
};

#endif

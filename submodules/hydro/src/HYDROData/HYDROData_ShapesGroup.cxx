// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_ShapesGroup.h"
#include "HYDROData_ShapesTool.h"

#include <BRepBuilderAPI_MakeShape.hxx>
#include <TDF_ChildIDIterator.hxx>
#include <TNaming_Builder.hxx>
#include <TNaming_NamedShape.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>

#include <Precision.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <ShapeUpgrade_UnifySameDomain.hxx>
#include <BRepTools_ReShape.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_ShapesGroup,HYDROData_Entity)

void HYDROData_ShapesGroup::GroupDefinition::Update( SeqOfGroupsDefs*          theGroupsDefs,
                                                     BRepBuilderAPI_MakeShape* theAlgo )
{
  if ( !theGroupsDefs || !theAlgo )
    return;

  SeqOfGroupsDefs::Iterator anIter( *theGroupsDefs );
  for ( ; anIter.More(); anIter.Next() )
  {
    GroupDefinition& aGroupDef = anIter.ChangeValue();
    if ( aGroupDef.Shapes.IsEmpty() )
      continue;

    TopTools_ListOfShape aShapesToAdd;
    for ( int i = 1; i <= aGroupDef.Shapes.Length(); ++i )
    {
      TopoDS_Shape aShape = aGroupDef.Shapes.Value( i );

      const TopTools_ListOfShape& aModifiedByAlgo = theAlgo->Modified( aShape );
      if ( !aModifiedByAlgo.IsEmpty() )
      {
        HYDROData_ShapesTool::AddShapes( aShapesToAdd, aModifiedByAlgo );
        aGroupDef.Shapes.Remove( i );
        --i;
      }
      else
      {
        const TopTools_ListOfShape& aGeneratedByAlgo = theAlgo->Generated( aShape );
        if ( !aGeneratedByAlgo.IsEmpty() )
        {
          HYDROData_ShapesTool::AddShapes( aShapesToAdd, aGeneratedByAlgo );
          aGroupDef.Shapes.Remove( i );
          --i;
        }
      }
    }

    HYDROData_ShapesTool::AddShapes( aGroupDef.Shapes, aShapesToAdd );
  }
}

void HYDROData_ShapesGroup::GroupDefinition::Update( SeqOfGroupsDefs*              theGroupsDefs,
                                                     ShapeUpgrade_UnifySameDomain* theAlgo )
{
  if ( !theGroupsDefs || !theAlgo )
    return;

  SeqOfGroupsDefs::Iterator anIter( *theGroupsDefs );
  if (theAlgo->History().IsNull())
    return;

  for ( ; anIter.More(); anIter.Next() )
  {
    GroupDefinition& aGroupDef = anIter.ChangeValue();
    if ( aGroupDef.Shapes.IsEmpty() )
      continue;

    TopTools_ListOfShape aShapesToAdd;
    for ( int i = 1; i <= aGroupDef.Shapes.Length(); ++i )
    {
      TopoDS_Shape aShape = aGroupDef.Shapes.Value( i );

      TopoDS_Shape aModifSh;
      if ( !aShape.IsNull())
      {
        const TopTools_ListOfShape& listModified = theAlgo->History()->Modified(aShape);
        if (listModified.Extent() > 0)
        {
          aModifSh = listModified.First();
          // Edge has been modified
          aShapesToAdd.Append( aModifSh );
          aGroupDef.Shapes.Remove( i );
          --i;
        }
        else if ( theAlgo->History()->IsRemoved(aShape) )
        {
          // Edge has been removed
          aGroupDef.Shapes.Remove( i );
          --i;
        }

      }
   }

    HYDROData_ShapesTool::AddShapes( aGroupDef.Shapes, aShapesToAdd );
  }
}

void HYDROData_ShapesGroup::GroupDefinition::Update( SeqOfGroupsDefs*              theGroupsDefs,
                                                     BRepTools_ReShape*            theAlgo )
{
  if ( !theGroupsDefs || !theAlgo )
    return;

  SeqOfGroupsDefs::Iterator anIter( *theGroupsDefs );
  for ( ; anIter.More(); anIter.Next() )
  {
    GroupDefinition& aGroupDef = anIter.ChangeValue();
    if ( aGroupDef.Shapes.IsEmpty() )
      continue;

    TopTools_ListOfShape aShapesToAdd;
    for ( int i = 1; i <= aGroupDef.Shapes.Length(); ++i )
    {
      TopoDS_Shape aShape = aGroupDef.Shapes.Value( i );

      TopoDS_Shape aGeneratedByAlgo = theAlgo->Value( aShape );
      if ( aGeneratedByAlgo.IsNull() )
      {
        // Edge has been removed
        aGroupDef.Shapes.Remove( i );
        --i;
      }
      else if ( !aShape.IsEqual( aGeneratedByAlgo ) )
      {
        // Edge has been modified
        aShapesToAdd.Append( aGeneratedByAlgo );
        aGroupDef.Shapes.Remove( i );
        --i;
      }
    }

    HYDROData_ShapesTool::AddShapes( aGroupDef.Shapes, aShapesToAdd );
  }
}

void HYDROData_ShapesGroup::GroupDefinition::Dump( std::ostream& theStream ) const
{
  theStream << "Name: " << Name.ToCString() << "\n";
  HYDROData_ShapesTool::DumpSequenceOfShapes( theStream, Shapes );
}

void HYDROData_ShapesGroup::GroupDefinition::Dump( std::ostream&           theStream,
                                                   const SeqOfGroupsDefs& theGroups  )
{
  SeqOfGroupsDefs::Iterator anIter( theGroups );
  for ( ; anIter.More(); anIter.Next() )
  {
    const GroupDefinition& aGroupDef = anIter.Value();
    aGroupDef.Dump( theStream );
  }
}



HYDROData_ShapesGroup::HYDROData_ShapesGroup()
: HYDROData_Entity( Geom_Groups )
{
}

HYDROData_ShapesGroup::~HYDROData_ShapesGroup()
{
}

bool HYDROData_ShapesGroup::CanBeUpdated() const
{
  return false;
}

bool HYDROData_ShapesGroup::CanRemove()
{
  return false;
}

void HYDROData_ShapesGroup::AddShape( const TopoDS_Shape& theShape )
{
  if ( theShape.IsNull() )
    return;

  TDF_Label aNewLab = myLab.FindChild( DataTag_Shape ).NewChild();

  TNaming_Builder aBuilder( aNewLab );
  aBuilder.Generated( theShape );
}

void HYDROData_ShapesGroup::SetShapes( const TopTools_SequenceOfShape& theShapes )
{
  RemoveShapes();

  for ( int i = 1, n = theShapes.Length(); i <= n; ++i )
  {
    const TopoDS_Shape& aShape = theShapes.Value( i );
    AddShape( aShape );
  }
}

void HYDROData_ShapesGroup::SetShapes( const TopTools_ListOfShape& theShapes )
{
  RemoveShapes();

  TopTools_ListIteratorOfListOfShape anIter( theShapes );
  for ( ; anIter.More(); anIter.Next() )
  {
    const TopoDS_Shape& aShape = anIter.Value();
    AddShape( aShape );
  }
}

void HYDROData_ShapesGroup::GetShapes( TopTools_SequenceOfShape& theShapes ) const
{
  theShapes.Clear();

  TDF_Label aLabel = myLab.FindChild( DataTag_Shape, false );
  if ( aLabel.IsNull() )
    return;

  TDF_ChildIDIterator aChildIt( aLabel, TNaming_NamedShape::GetID() );
  for ( ; aChildIt.More(); aChildIt.Next() )
  {
    Handle(TNaming_NamedShape) aNamedShape =
      Handle(TNaming_NamedShape)::DownCast( aChildIt.Value() );
    if ( aNamedShape.IsNull() )
      continue;

    TopoDS_Shape aStoredShape = aNamedShape->Get();
    if ( aStoredShape.IsNull() )
      continue;

    theShapes.Append( aStoredShape );
  }
}

void HYDROData_ShapesGroup::RemoveShapes()
{
  TDF_Label aLabel = myLab.FindChild( DataTag_Shape, false );
  if ( !aLabel.IsNull() )
    aLabel.ForgetAllAttributes();
}


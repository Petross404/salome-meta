// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Obstacle.h"

#include "HYDROData_Document.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_ShapesTool.h"
#include "HYDROData_Tool.h"
#include "HYDROData_GeomTool.h"

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#ifndef LIGHT_MODE
#include <Basics_Utils.hxx>
#endif

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

#include <BRepTools.hxx>
#include <BRep_Builder.hxx>

#include <IGESControl_Reader.hxx>
#include <IGESData_IGESModel.hxx>

#include <STEPControl_Reader.hxx>

#include <Interface_Static.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Iterator.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Edge.hxx>

#include <TDataStd_AsciiString.hxx>
#include <TDataStd_RealArray.hxx>

#include <TColStd_SequenceOfAsciiString.hxx>

#include <TopExp_Explorer.hxx>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#include <QColor>
#include <QFile>
#include <QFileInfo>
#include <QStringList>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

#include <Standard_ErrorHandler.hxx> // CAREFUL ! position of this file is critic

#include <HYDROData_Projection.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Obstacle,HYDROData_ArtificialObject)


HYDROData_Obstacle::HYDROData_Obstacle()
: HYDROData_ArtificialObject( Geom_3d )
{
}

HYDROData_Obstacle::~HYDROData_Obstacle()
{
}

QStringList HYDROData_Obstacle::DumpToPython( const QString&       thePyScriptPath,
                                              MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  
  QString anObstacleName = GetObjPyName();

  TCollection_AsciiString aGeomObjectEntry = GetGeomObjectEntry();
  QString aFilePath = GetFilePath();

  if ( !aGeomObjectEntry.IsEmpty() )
  {
    QString aSalomeObjName = HYDROData_Tool::GenerateNameForPython( theTreatedObjects, "obstacle_sobj" );
    // aResList << QString( "%1 = theStudy.FindObjectID( \"%2\" )" )
    //            .arg( aSalomeObjName ).arg( aGeomObjectEntry.ToCString() );
    QString aGeomObjectName( GetGeomObjectName().ToCString() );
    if ( !aGeomObjectName.isEmpty() ) {
      aResList << QString( "%1 = theStudy.FindObjectByName( \"%2\", \"GEOM\" )[0];" )
                  .arg( aSalomeObjName ).arg( aGeomObjectName );

      aResList << QString( "%1.ImportFromGeomIOR( %2.GetIOR() )" )
                  .arg( anObstacleName ).arg( aSalomeObjName );

      aResList << QString( "%1.SetGeomObjectEntry( %2.GetID() )" )
                  .arg( anObstacleName ).arg( aSalomeObjName );
    }
    aResList << QString( "" );
  }
  else if ( !aFilePath.isEmpty() )
  {
    aResList << QString( "%1.ImportFromFile( \"%2\" )" )
                .arg( anObstacleName ).arg( aFilePath );
    aResList << QString( "" );
  }

  // Write the translation points
  double aDx, aDy, aDz;
  if ( getTranslation( aDx, aDy, aDz ) )
  {
    aResList << QString( "%1.Translate( %2, %3, %4 )" )
                .arg( anObstacleName ).arg( aDx ).arg( aDy ).arg( aDz );
    aResList << QString( "" );
  }

  aResList << QString( "%1.Update()" ).arg( anObstacleName );
  aResList << QString( "" );

  return aResList;
}

void HYDROData_Obstacle::Update()
{
  RemoveGroupObjects();
  createGroupObjects();
  checkAndSetAltitudeObject();

  HYDROData_Entity::Update();
}

bool HYDROData_Obstacle::IsHas2dPrs() const
{
  return true;
}

void HYDROData_Obstacle::SetShape3D( const TopoDS_Shape& theShape )
{
  TopoDS_Face aShape2d = HYDROData_Projection::MakeProjection( theShape );
  HYDROData_ArtificialObject::SetShape3D( theShape );
  HYDROData_ArtificialObject::SetTopShape( aShape2d );
}

QColor HYDROData_Obstacle::DefaultFillingColor() const
{
  return QColor( Qt::yellow );
}

QColor HYDROData_Obstacle::DefaultBorderColor() const
{
  return QColor( Qt::transparent );
}

bool HYDROData_Obstacle::ImportFromFile( const QString& theFilePath )
{
  // Check the file existence
  QFileInfo aFileInfo( theFilePath );
  if ( !aFileInfo.exists() ) {
    return false;
  }

  bool aRes = false;
  TopoDS_Shape aShape;

  // Import file
  QString aFileSuf = aFileInfo.suffix().toLower();
  if ( aFileSuf == "brep" ) {
    aShape = ImportBREP( theFilePath );
  } else if ( aFileSuf == "iges" || aFileSuf == "igs" ) {
    aShape = ImportIGES( theFilePath );
  } else if ( aFileSuf == "step" ) {
    aShape = ImportSTEP( theFilePath );
  }
 
  // Check the result shape
  aRes = !aShape.IsNull();

  // Set shape to the obstacle in case of success
  if ( aRes ) {
    SetShape3D( aShape );
    SetFilePath( theFilePath );
  }

  return aRes;
}

void HYDROData_Obstacle::SetFilePath( const QString& theFilePath )
{
  TCollection_AsciiString anAsciiStr( theFilePath.toStdString().c_str() );
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_FilePath ), anAsciiStr );
  anAttr->SetID(TDataStd_AsciiString::GetID());
}

QString HYDROData_Obstacle::GetFilePath() const
{
  QString aRes;

  TDF_Label aLabel = myLab.FindChild( DataTag_FilePath, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
      aRes = QString( anAsciiStr->Get().ToCString() );
  }

  return aRes;
}

bool HYDROData_Obstacle::ImportFromGeomIOR( const TCollection_AsciiString& theIOR )
{
  bool aRes = false;

#ifndef LIGHT_MODE
  if ( theIOR.IsEmpty() ) {
    return aRes;
  }
  
  // TopoDS_Shape aShape = GEOMBase::GetShapeFromIOR( theIOR.ToCString() );
  TopoDS_Shape aShape = HYDROData_GeomTool::GetShapeFromIOR( theIOR.ToCString() );

  if ( !aShape.IsNull() ) {
    SetShape3D( aShape );
    aRes = true;
  }
#endif

  return aRes;
}

void HYDROData_Obstacle::SetGeomObjectEntry( const TCollection_AsciiString& theEntry )
{
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_GeomObjectEntry ), theEntry );
  anAttr->SetID(TDataStd_AsciiString::GetID());
}

TCollection_AsciiString HYDROData_Obstacle::GetGeomObjectEntry() const
{
  TCollection_AsciiString aRes;

  TDF_Label aLabel = myLab.FindChild( DataTag_GeomObjectEntry, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
      aRes = anAsciiStr->Get();
  }

  return aRes;
}

TCollection_AsciiString HYDROData_Obstacle::GetGeomObjectName() const
{
  TCollection_AsciiString aRes;

#ifndef LIGHT_MODE
  SALOMEDS::Study_var aDSStudy = HYDROData_GeomTool::GetStudy();

  if ( !aDSStudy->_is_nil() ) {
    TCollection_AsciiString anEntry = GetGeomObjectEntry();
    SALOMEDS::SObject_var aSObject = aDSStudy->FindObjectID( anEntry.ToCString() );
    if ( !aSObject->_is_nil() ) {
      aRes = TCollection_AsciiString( aSObject->GetName() );
    }
  }
#endif
  return aRes;
}

void HYDROData_Obstacle::Translate( const double theDx,
                                    const double theDy,
                                    const double theDz )
{
  TopoDS_Shape aShape3D = GetShape3D();
  
  TopoDS_Shape aTranslatedShape3D = HYDROData_ShapesTool::Translated( aShape3D, theDx, theDy, theDz );
  if ( aTranslatedShape3D.IsNull() )
    return;

  SetShape3D( aTranslatedShape3D );

  setTranslation( theDx, theDy, theDz );
}

TopoDS_Shape HYDROData_Obstacle::ImportBREP( const QString& theFilePath ) const
{
  TopoDS_Shape aResShape;

  BRep_Builder aBrepBuilder;
  BRepTools::Read( aResShape, qPrintable(theFilePath), aBrepBuilder );

  return aResShape;
}

TopoDS_Shape HYDROData_Obstacle::ImportIGES( const QString& theFilePath ) const
{
  TopoDS_Shape aResShape;

#ifndef LIGHT_MODE
  // Set "C" numeric locale to save numbers correctly
  Kernel_Utils::Localizer loc;

  IGESControl_Reader aReader;

  Interface_Static::SetCVal("xstep.cascade.unit","M");

  try {
    OCC_CATCH_SIGNALS;

    IFSelect_ReturnStatus status = aReader.ReadFile(qPrintable(theFilePath));

    if (status == IFSelect_RetDone) {
      // Rescale units
      Handle(IGESData_IGESModel) aModel =
        Handle(IGESData_IGESModel)::DownCast(aReader.Model());
      if (!aModel.IsNull()) {
        IGESData_GlobalSection aGS = aModel->GlobalSection();
        aGS.SetUnitFlag(6);
        aModel->SetGlobalSection(aGS);
      }
    
      aReader.ClearShapes();
      aReader.TransferRoots();

      aResShape = aReader.OneShape();
    } 
    else {
      aResShape.Nullify();
    }
  }
  catch(Standard_Failure) {
    aResShape.Nullify();
  }
#endif

  return aResShape;
}

TopoDS_Shape HYDROData_Obstacle::ImportSTEP( const QString& theFilePath ) const
{
  TopoDS_Shape aResShape;

#ifndef LIGHT_MODE
  // Set "C" numeric locale to save numbers correctly
  Kernel_Utils::Localizer loc;

  STEPControl_Reader aReader;

  // Convert to METERS
  Interface_Static::SetCVal("xstep.cascade.unit","M");
  Interface_Static::SetIVal("read.step.ideas", 1);
  Interface_Static::SetIVal("read.step.nonmanifold", 1);

  BRep_Builder B;
  TopoDS_Compound compound;
  B.MakeCompound(compound);

  try {
    OCC_CATCH_SIGNALS;

    IFSelect_ReturnStatus status = aReader.ReadFile( qPrintable(theFilePath) );

    if (status == IFSelect_RetDone) {
      // Rescale units
      // set UnitFlag to units from file
      TColStd_SequenceOfAsciiString anUnitLengthNames;
      TColStd_SequenceOfAsciiString anUnitAngleNames;
      TColStd_SequenceOfAsciiString anUnitSolidAngleNames;
      aReader.FileUnits(anUnitLengthNames, anUnitAngleNames, anUnitSolidAngleNames);
      if (anUnitLengthNames.Length() > 0) {
        TCollection_AsciiString aLenUnits = anUnitLengthNames.First();
        if (aLenUnits == "millimetre")
          Interface_Static::SetCVal("xstep.cascade.unit", "MM");
        else if (aLenUnits == "centimetre")
          Interface_Static::SetCVal("xstep.cascade.unit", "CM");
        else if (aLenUnits == "metre" || aLenUnits.IsEmpty())
          Interface_Static::SetCVal("xstep.cascade.unit", "M");
        else if (aLenUnits == "INCH")
          Interface_Static::SetCVal("xstep.cascade.unit", "INCH");
        else {
          // The file contains not supported units
          return aResShape;
        }
      }
        
      Standard_Boolean failsonly = Standard_False;
      aReader.PrintCheckLoad(failsonly, IFSelect_ItemsByEntity);

      // Root transfers
      Standard_Integer nbr = aReader.NbRootsForTransfer();
      aReader.PrintCheckTransfer(failsonly, IFSelect_ItemsByEntity);

      for (Standard_Integer n = 1; n <= nbr; n++) {
        Standard_Boolean ok = aReader.TransferRoot(n);
        // Collecting resulting entities
        Standard_Integer nbs = aReader.NbShapes();
        if (!ok || nbs == 0) {
          continue; // skip empty root
        } 
        else if (nbr == 1 && nbs == 1) { // For a single entity
          aResShape = aReader.Shape(1);
          // ATTENTION: this is a workaround for mantis issue 0020442 remark 0010776
          // It should be removed after patching OCCT for bug OCC22436
          // (fix for OCCT is expected in service pack next to OCCT6.3sp12)
          if (aResShape.ShapeType() == TopAbs_COMPOUND) {
            int nbSub1 = 0;
            TopoDS_Shape currShape;
            TopoDS_Iterator It (aResShape, Standard_True, Standard_True);
            for (; It.More(); It.Next()) {
              nbSub1++;
              currShape = It.Value();
            }
            if (nbSub1 == 1)
              aResShape = currShape;
          }
          // END workaround
          break;
        }

        for (Standard_Integer i = 1; i <= nbs; i++) {
          TopoDS_Shape aShape = aReader.Shape(i);
          if (aShape.IsNull()) {
            continue;
          }
          else {
            B.Add(compound, aShape);
          }
        }
      }
  
      if (aResShape.IsNull())
        aResShape = compound;

      // Check if any BRep entity has been read, there must be at least a vertex
      if ( !TopExp_Explorer( aResShape, TopAbs_VERTEX ).More() ) {
        // No geometrical data in the imported file
        return TopoDS_Shape();
      }
    }
    else {
      aResShape.Nullify();
    }
  }
  catch (Standard_Failure) {
    aResShape.Nullify();
  }
#endif

  return aResShape;
}

ObjectKind HYDROData_Obstacle::getAltitudeObjectType() const
{
  DEBTRACE("HYDROData_Obstacle::getAltitudeObjectType");
  return KIND_OBSTACLE_ALTITUDE;
}

void HYDROData_Obstacle::createGroupObjects()
{
  TopoDS_Shape anObstacleShape = GetTopShape();
  if ( !anObstacleShape.IsNull() )
  {
    TopTools_SequenceOfShape aWireEdges;
    HYDROData_ShapesTool::ExploreShapeToShapes( anObstacleShape, TopAbs_EDGE, aWireEdges );
    if ( !aWireEdges.IsEmpty() )
    {
      QString aWireGroupName = GetName() + "_Outer_Wire";

      Handle(HYDROData_ShapesGroup) anExtWireGroup = createGroupObject();
      anExtWireGroup->SetName( aWireGroupName );
     
      anExtWireGroup->SetShapes( aWireEdges );
    }
  }
}

void HYDROData_Obstacle::setTranslation( const double theDx, 
                                         const double theDy, 
                                         const double theDz )
{
  TDF_Label aLabel = myLab.FindChild( DataTag_Translation );

  double aCurDx = theDx;
  double aCurDy = theDy;
  double aCurDz = theDz;

  double aPrevDx, aPrevDy, aPrevDz;
  if ( getTranslation( aPrevDx, aPrevDy, aPrevDz ) )
  {
    aCurDx += aPrevDx;
    aCurDy += aPrevDy;
    aCurDz += aPrevDz;
  }
  
  Handle(TDataStd_RealArray) aCoeffsArray = TDataStd_RealArray::Set( aLabel, 1, 3 );
  aCoeffsArray->SetID(TDataStd_RealArray::GetID());
  aCoeffsArray->SetValue( 1, aCurDx );
  aCoeffsArray->SetValue( 2, aCurDy );
  aCoeffsArray->SetValue( 3, aCurDz );
}

bool HYDROData_Obstacle::getTranslation( double& theDx, double& theDy, double& theDz ) const
{
  theDx = theDy = theDz = 0.0;

  TDF_Label aLabel = myLab.FindChild( DataTag_Translation, false );
  if ( aLabel.IsNull() )
    return false;

  Handle(TDataStd_RealArray) aCoeffsArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), aCoeffsArray ) )
    return false;

  theDx = aCoeffsArray->Value( 1 );
  theDy = aCoeffsArray->Value( 2 );
  theDz = aCoeffsArray->Value( 3 );

  return true;
}

void HYDROData_Obstacle::UpdateLocalCS( double theDx, double theDy )
{
  Translate( theDx, theDy, 0 );
}


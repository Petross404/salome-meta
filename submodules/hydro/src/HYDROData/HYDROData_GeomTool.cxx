// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_GeomTool.h"

#ifndef LIGHT_MODE

#include <TopoDS_Shape.hxx>

#include <BRepTools.hxx>
#include <GEOMBase.h>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#include <SALOME_NamingService.hxx>
#include <SALOME_LifeCycleCORBA.hxx>

#include <TopTools_MapOfShape.hxx>
#include <TopExp_Explorer.hxx>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

#include <QStringList>
#include <QVector>
#include <QSet>

static int _argc = 0;
static CORBA::ORB_var _ORB = CORBA::ORB_init( _argc, 0, "omniORB4"/*CORBA::ORB_ID*/ );
static SALOME_NamingService _NS( _ORB );
static SALOME_LifeCycleCORBA _LCC( &_NS );
static SALOMEDS::Study_var _STUDY = SALOMEDS::Study::_nil();

QString HYDROData_GeomTool::myContainerName = "FactoryServer";

void HYDROData_GeomTool::SetContainerName( const QString& theContainerName )
{
  myContainerName = theContainerName;
}

TopoDS_Shape HYDROData_GeomTool::GetShapeFromIOR( const QString& theIOR )
{
  // Note that GEOMBase::GetShape() cause crash in batch mode

  TopoDS_Shape aResShape;
   
  CORBA::Object_var aCorbaObj = _ORB->string_to_object( qPrintable( theIOR ) );
  if ( !CORBA::is_nil( aCorbaObj ) ) {
    GEOM::GEOM_Object_var aGeomObj = GEOM::GEOM_Object::_narrow( aCorbaObj );

    Engines::EngineComponent_var aComp =
      _LCC.FindOrLoad_Component( myContainerName.toStdString().c_str(), "GEOM" );
    GEOM::GEOM_Gen_var aComponentGeom = GEOM::GEOM_Gen::_narrow( aComp );

    aResShape = GEOM_Client::get_client().GetShape( aComponentGeom, aGeomObj );
  }

  return aResShape;
}

GEOM::GEOM_Gen_var HYDROData_GeomTool::GetGeomGen()
{
  Engines::EngineComponent_var aComponent = _LCC.FindOrLoad_Component( myContainerName.toStdString().c_str(), "GEOM" );
  GEOM::GEOM_Gen_var aGEOMEngine = GEOM::GEOM_Gen::_narrow( aComponent );

  return aGEOMEngine._retn();
}

SALOMEDS::Study_var HYDROData_GeomTool::GetStudy()
{
  if (_STUDY->_is_nil())
  {
    CORBA::Object_var aStudyObject = _NS.Resolve("/Study");
    _STUDY = SALOMEDS::Study::_narrow(aStudyObject);
  }
  return _STUDY._retn();
}

QString HYDROData_GeomTool::GetFreeName( const QString& theBaseName )
{
  QString aName = theBaseName;

  SALOMEDS::Study_var theStudy = GetStudy();
  if ( !theStudy->_is_nil() ) {
    // collect all object names of GEOM component
    QSet<QString> anUsedNames;
    SALOMEDS::SComponent_var aComponent = theStudy->FindComponent( "GEOM" );
    if ( !aComponent->_is_nil() ) {
      SALOMEDS::ChildIterator_var anIter = theStudy->NewChildIterator( aComponent );
      for ( anIter->InitEx( true ); anIter->More(); anIter->Next() ) {
        anUsedNames.insert( anIter->Value()->GetName() );
      }
    }

    // build an unique name
    int aNumber = 0;
    bool isUnique = false;
    QString aPrefix = theBaseName;
    QStringList aParts = aPrefix.split( "_", QString::KeepEmptyParts );
    if ( aParts.count() > 1 ) {
      bool isOk;
      aNumber = aParts.last().toLong( &isOk );
      if ( isOk ) {
        aParts.removeLast();
        aPrefix = aParts.join( "_" );
        aNumber--;
      }
    }
    
    while ( !isUnique ) {
      aName = aPrefix + "_" + QString::number( ++aNumber );
      isUnique = ( !anUsedNames.contains( aName ) );
    }
  }

  return aName;
}

GEOM::GEOM_Object_ptr HYDROData_GeomTool::publishShapeInGEOM(
  GEOM::GEOM_Gen_var theGeomEngine,
  const TopoDS_Shape& theShape, const QString& theName,
  QString& theGeomObjEntry )
{
  theGeomObjEntry = "";
  GEOM::GEOM_Object_var aGeomObj;

  if ( theGeomEngine->_is_nil() || 
       theShape.IsNull() ) {
    return aGeomObj._retn();
  }

  std::ostringstream aStreamShape;
  // Write TopoDS_Shape in ASCII format to the stream
  BRepTools::Write( theShape, aStreamShape );
  // Returns the number of bytes that have been stored in the stream's buffer.
  int aSize = aStreamShape.str().size();
  // Allocate octect buffer of required size
  CORBA::Octet* anOctetBuf = SALOMEDS::TMPFile::allocbuf( aSize );
  // Copy ostrstream content to the octect buffer
  memcpy( anOctetBuf, aStreamShape.str().c_str(), aSize );
  // Create TMPFile
  SALOMEDS::TMPFile_var aSeqFile = new SALOMEDS::TMPFile( aSize, aSize, anOctetBuf, 1 );

  // Restore shape from the stream and get the GEOM object
  GEOM::GEOM_IInsertOperations_var anInsOp = theGeomEngine->GetIInsertOperations();
  aGeomObj = anInsOp->RestoreShape( aSeqFile );
  
  // Publish the GEOM object
  theGeomObjEntry = publishGEOMObject( theGeomEngine, aGeomObj, theName );
  
  return aGeomObj._retn();
}


GEOM::GEOM_Object_ptr HYDROData_GeomTool::ExplodeShapeInGEOMandPublish( GEOM::GEOM_Gen_var theGeomEngine, 
  const TopoDS_Shape& theShape,   
  const NCollection_IndexedDataMap<TopoDS_Shape, QString, TopTools_ShapeMapHasher>& aFacesToNameModif,  
  const QString& theName,
  QString& theGeomObjEntry)
{ 
  GEOM::GEOM_Object_ptr aGeomObj = HYDROData_GeomTool::publishShapeInGEOM( theGeomEngine, theShape, theName, theGeomObjEntry );

  GEOM::GEOM_IShapesOperations_var anExpOp = theGeomEngine->GetIShapesOperations();
  GEOM::ListOfGO* aFc = anExpOp->MakeExplode(aGeomObj, TopAbs_FACE, false);

  TopTools_MapOfShape mapShape;

  NCollection_IndexedDataMap<TopoDS_Shape, QString, TopTools_ShapeMapHasher> aFF2N;
  for (int i = 1; i <= aFacesToNameModif.Extent(); i++)
  {
    TopExp_Explorer exp (aFacesToNameModif.FindKey(i), TopAbs_FACE);
    for (;exp.More(); exp.Next())
      aFF2N.Add(exp.Current(), aFacesToNameModif.FindFromIndex(i));
  }

  TopExp_Explorer exp (theShape, TopAbs_FACE);
  QVector<QString> Names;
  for (; exp.More(); exp.Next())
  {
    const TopoDS_Shape& csh = exp.Current();
    if (mapShape.Add(csh))
    {
      //listShape.Append(csh);
      QString Qstr = aFF2N.FindFromKey(csh);
      Names.push_back(Qstr);
    }
  }

  for (size_t i = 0; i < aFc->length(); i++)
  {
    std::string name = Names[i].toStdString();
    GEOM::GEOM_Object_ptr anObj = aFc->operator[](i);
    //GEOMBase::PublishSubObject( anObj, name.c_str() );
    theGeomEngine->AddInStudy( anObj, name.c_str(), aGeomObj );
  }

  return aGeomObj;
}


 GEOM::GEOM_Object_ptr HYDROData_GeomTool::createFaceInGEOM( GEOM::GEOM_Gen_var theGeomEngine,
                                                             const int theWidth,
                                                             const int theHeight,
                                                             const QString& theName,
                                                             QString& theFaceEntry )
{
  theFaceEntry = "";
  GEOM::GEOM_Object_var aGeomObj;

  if ( theGeomEngine->_is_nil() ) {
    return aGeomObj._retn();
  }

  GEOM::GEOM_IBasicOperations_var aBasicOperations = theGeomEngine->GetIBasicOperations();
  GEOM::GEOM_IBlocksOperations_var aBlocksOperations = theGeomEngine->GetIBlocksOperations();
         
  GEOM::GEOM_Object_var P1 = aBasicOperations->MakePointXYZ( -0.5 * theWidth, -0.5 * theHeight, 0 );
  GEOM::GEOM_Object_var P2 = aBasicOperations->MakePointXYZ(  0.5 * theWidth, -0.5 * theHeight, 0 );
  GEOM::GEOM_Object_var P3 = aBasicOperations->MakePointXYZ(  0.5 * theWidth,  0.5 * theHeight, 0 );
  GEOM::GEOM_Object_var P4 = aBasicOperations->MakePointXYZ( -0.5 * theWidth,  0.5 * theHeight, 0 );
  
  GEOM::GEOM_Object_var aFace = aBlocksOperations->MakeQuad4Vertices( P1, P2 ,P3, P4 );
    
  // Publish the face
  theFaceEntry = publishGEOMObject( theGeomEngine, aFace, theName );

  return aFace._retn();
}

QString HYDROData_GeomTool::publishGEOMObject( GEOM::GEOM_Gen_var theGeomEngine,
                                               GEOM::GEOM_Object_ptr theGeomObj,
                                               const QString& theName )
{
  QString anEntry;

  if ( !theGeomObj->_is_nil() ) {
    QString aName = HYDROData_GeomTool::GetFreeName( theName );

    SALOMEDS::SObject_var aResultSO = 
      theGeomEngine->PublishInStudy( SALOMEDS::SObject::_nil(), 
                                     theGeomObj, qPrintable( aName ) );
    if ( !aResultSO->_is_nil() ) {
      anEntry = aResultSO->GetID();
    }
  }

  return anEntry;
}

#endif

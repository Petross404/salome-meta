// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_AltitudeObject_HeaderFile
#define HYDROData_AltitudeObject_HeaderFile


#include "HYDROData_IAltitudeObject.h"



/**\class HYDROData_AltitudeObject
 * \brief Class that stores/retreives information about the Altitude.
 *
 */
class HYDROData_AltitudeObject : public HYDROData_IAltitudeObject
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_IAltitudeObject::DataTag_First + 100, ///< first tag, to reserve
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_AltitudeObject, HYDROData_IAltitudeObject);

  /**
   * Returns the kind of this object. 
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_ALTITUDE; }


  /**
   * Dump Altitude object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

public:      

  // Public methods to work with altitudes.

  /**
   * Returns altitude for given point.
   * \param thePoint the point to examine
   * \return altitude value
   */
  HYDRODATA_EXPORT virtual double           GetAltitudeForPoint( const gp_XY& thePoint,
                                                                 int theMethod = 0) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_AltitudeObject();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT  ~HYDROData_AltitudeObject();
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_DummyObject3D_HeaderFile
#define HYDROData_DummyObject3D_HeaderFile

#include <HYDROData_Entity.h>

class HYDROData_Object;
class TopoDS_Shape;

/**\class HYDROData_DummyObject3D
 * \brief The artificial objects are objects created or planned for creation by human.
 *
 */
class HYDROData_DummyObject3D : public HYDROData_Entity
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100 ///< first tag, to reserve
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_DummyObject3D, HYDROData_Entity);

public:

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_DUMMY_3D; }

  /**
   * Returns the name of this object.
   */
  HYDRODATA_EXPORT virtual QString GetName() const;

  /**
   * Returns flag indicating that object is updateble or not.
   */
  HYDRODATA_EXPORT virtual bool CanBeUpdated() const;

  /**
   * Returns flag indicating that object can be removed or not.
   */
  HYDRODATA_EXPORT virtual bool CanRemove();


  /**
   * Returns the parent object.
   */
  HYDRODATA_EXPORT Handle(HYDROData_Object) GetObject() const;

  /**
   * Returns the 3d shape of the parent object.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape() const;


  /**
   * Returns filling color of object.
   */
  HYDRODATA_EXPORT virtual QColor GetFillingColor() const;

  /**
   * Returns border color of object.
   */
  HYDRODATA_EXPORT virtual QColor GetBorderColor() const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_DummyObject3D();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_DummyObject3D();
};

#endif

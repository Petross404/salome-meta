// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_ShapesTool_HeaderFile
#define HYDROData_ShapesTool_HeaderFile

#include "HYDROData.h"

#include <TopAbs_ShapeEnum.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>

class TopoDS_Shape;
class TopoDS_Vertex;
class TopoDS_Edge;

class HYDRODATA_EXPORT HYDROData_ShapesTool {

public:

  /**
   * \brief Explore the incoming shape to shapes with given type.
   * \param theInShape object to explore
   * \param theExpType type to explore
   * \param theOutShapes[out] list of result shapes if any
   */
  static void                           ExploreShapeToShapes( const TopoDS_Shape&       theInShape,
                                                              const TopAbs_ShapeEnum&   theExpType,
                                                              TopTools_SequenceOfShape& theOutShapes );

  /**
   * \brief Gets the first and last vertex from shape. Shape can be an Edge or Wire.
   * \param theInShape object to explore
   * \param theFirstVertex[out] first shape vertex
   * \param theLastVertex[out] last shape vertex
   * \return true if shape is of correct type
   */
  static bool                           Vertices( const TopoDS_Shape& theInShape,
                                                  TopoDS_Vertex&      theFirstVertex,
                                                  TopoDS_Vertex&      theLastVertex,
                                                  const bool          theIsCumOri = false );

  /**
   * \brief Compare two vertices on coordinates equation.
   * \param theFirstVert first vertex
   * \param theSecondVert second vertex
   * \param theIs2D flag indicating the 2D equation (ie. z coord will be ignored)
   * \return \c true if cooredinates of vertices is equals
   */
  static bool                           IsVerticesEquals( const TopoDS_Vertex& theFirstVert,
                                                          const TopoDS_Vertex& theSecondVert,
                                                          const bool           theIs2D = false  );

  /**
   * \brief Compare two edges on points coordinates equation.
   * \param theFirstVert first edge
   * \param theSecondVert second edge
   * \param theIs2D flag indicating the 2D equation (ie. z coord will be ignored)
   * \return \c true if coordinates of all points of given edges is equals
   */
  static bool                           IsEdgesEquals( const TopoDS_Edge& theFirstEdge,
                                                       const TopoDS_Edge& theSecondEdge,
                                                       const bool         theIs2D = false );

  /**
   * \brief Translate the shape to the given distance.
   * \param theShape shape to translate
   * \param theDx X vector component
   * \param theDy Y vector component
   * \param theDz Z vector component
   * \return result translated shape
   */
  static TopoDS_Shape                   Translated( const TopoDS_Shape& theShape,
                                                    const double        theDx,
                                                    const double        theDy,
                                                    const double        theDz );

  /**
   * \brief Adds the sequence of shapes to other sequence.
   * \param theShapes sequence to which the shapes will be added
   * \param theShapesToAdd sequence from which the shapes will be extracted
   */
  static void                           AddShapes( TopTools_SequenceOfShape&       theShapes,
                                                   const TopTools_SequenceOfShape& theShapesToAdd );

  /**
   * \brief Adds the list of shapes to the sequence.
   * \param theShapes sequence to which the shapes will be added
   * \param theShapesToAdd list from which the shapes will be extracted
   */
  static void                           AddShapes( TopTools_SequenceOfShape&   theShapes,
                                                   const TopTools_ListOfShape& theShapesToAdd );

  /**
   * \brief Adds the sequence of shapes to the list.
   * \param theShapes list to which the shapes will be added
   * \param theShapesToAdd sequence from which the shapes will be extracted
   */
  static void                           AddShapes( TopTools_ListOfShape&           theShapes,
                                                   const TopTools_SequenceOfShape& theShapesToAdd );

  /**
   * \brief Adds the list of shapes to other list.
   * \param theShapes list to which the shapes will be added
   * \param theShapesToAdd list from which the shapes will be extracted
   */
  static void                           AddShapes( TopTools_ListOfShape&       theShapes,
                                                   const TopTools_ListOfShape& theShapesToAdd );


  /**
   * \brief Explode and dump the shape to the stream.
   */
  static void                           DumpShapeSubShapes( std::ostream&           theStream,
                                                            const char*             theTitle,
                                                            const TopoDS_Shape&     theShape,
                                                            const TopAbs_ShapeEnum& theExpType );

  /**
   * \brief Dump the shapes sequence to the stream.
   */
  static void                           DumpSequenceOfShapes( std::ostream&                   theStream,
                                                              const TopTools_SequenceOfShape& theShapes );
};


#endif



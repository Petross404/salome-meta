// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_BSplineOperation.h>
#include <GeomConvert_BSplineCurveToBezierCurve.hxx>
#include <Geom_BezierCurve.hxx>
#include <gp_Pnt.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <QPainterPath>

#include <CurveCreator_Utils.hxx>

Handle(Geom_BSplineCurve) HYDROData_BSplineOperation::ComputeCurve(
  const NCollection_Sequence<gp_XYZ>& thePoints,
  const bool                          theIsClosed,
  const double                        theTolerance )
{
  // skip equal points
  int aNbPoints = thePoints.Size();
  NCollection_Sequence<gp_XYZ> aPoints;
  if ( aNbPoints > 0 ) {
    gp_XYZ aPrevPoint = thePoints.Value( 1 );
    aPoints.Append( aPrevPoint );
    for( int i = 2 ; i <= aNbPoints; ++i )
    {
      gp_XYZ aPoint( thePoints.Value( i ) );
      if ( !aPoint.IsEqual( aPrevPoint, theTolerance ) )
        aPoints.Append( aPoint );
      aPrevPoint = aPoint;
    }
  }

  // fill array for algorithm by the received coordinates
  aNbPoints = aPoints.Size();
  Handle(TColgp_HArray1OfPnt) aHCurvePoints = new TColgp_HArray1OfPnt( 1, aNbPoints );
  for ( int i = 1; i <= aNbPoints; i++ )
  {
    gp_Pnt aPnt( aPoints.Value( i ) );
    aHCurvePoints->SetValue( i, aPnt );
  }

  // compute BSpline
  Handle(Geom_BSplineCurve) aBSpline;
  if( CurveCreator_Utils::constructBSpline( aHCurvePoints, theIsClosed, aBSpline ) )
    return aBSpline;
  else
    return Handle(Geom_BSplineCurve)();
}

void HYDROData_BSplineOperation::ComputePath( const Handle(Geom_BSplineCurve)& theCurve,
                                              QPainterPath& thePath )
{
  if ( theCurve.IsNull() ) // returns an empty Path if original curve is invalid
    return;

  GeomConvert_BSplineCurveToBezierCurve aConverter(theCurve);
  int a, aNumArcs = aConverter.NbArcs();
  for(a = 1; a <= aNumArcs; a++)
  {
    Handle(Geom_BezierCurve) anArc = aConverter.Arc(a);
    if (a == 1) { // set a start point
      gp_Pnt aStart = anArc->StartPoint();
      thePath.moveTo(aStart.X(), aStart.Y());
    }
    gp_Pnt anEnd = anArc->EndPoint();
    if (anArc->NbPoles() == 3) { // quadric segment in the path (pole 1 is start, pole 3 is end)
      gp_Pnt aPole = anArc->Pole(2);
      thePath.quadTo(aPole.X(), aPole.Y(), anEnd.X(), anEnd.Y());
    } else if (anArc->NbPoles() == 4) { // cubic segment (usually this is used)
      gp_Pnt aPole1 = anArc->Pole(2);
      gp_Pnt aPole2 = anArc->Pole(3);
      thePath.cubicTo(
        aPole1.X(), aPole1.Y(), aPole2.X(), aPole2.Y(), anEnd.X(), anEnd.Y());
    } else { // error, another number of poles is not supported
      continue;
    }
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Transform_HeaderFile
#define HYDROData_Transform_HeaderFile

#include <Standard.hxx>
#include <Standard_Macro.hxx>
#include <Standard_Boolean.hxx>
#include <Standard_Integer.hxx>

#include <gp_Trsf.hxx>

#include <TopoDS_Shape.hxx>
#include <TopTools_ListOfShape.hxx>

#include <BRepBuilderAPI_Transform.hxx>

#include <TopTools_ListOfShape.hxx>
#include <TopTools_MapOfShape.hxx>

#include <BOPAlgo_BuilderShape.hxx>
#include <BOPAlgo_Builder.hxx>

#include <TCollection_AsciiString.hxx>
//=======================================================================
//class    : HYDROData_Transform
//purpose  :
//=======================================================================
class HYDROData_Transform : public BOPAlgo_BuilderShape
{
 public:
  Standard_EXPORT
    HYDROData_Transform();
  //
  Standard_EXPORT
    virtual ~HYDROData_Transform();
  //
  Standard_EXPORT
    void SetArgument(const TopoDS_Shape& theShape);
  //
  Standard_EXPORT
    const TopoDS_Shape& Argument()const;
  //
  Standard_EXPORT
    void SetToTransform(const Standard_Boolean theFlag);
  //
  Standard_EXPORT
    Standard_Boolean ToTransform()const;
  //
  Standard_EXPORT
    void SetTreshold(const Standard_Real theTreshold);
  //
  Standard_EXPORT
    Standard_Real Treshold()const;
  //
  Standard_EXPORT
    void Detect();
  //
  Standard_EXPORT
    virtual void Clear();
  //
  Standard_EXPORT
    virtual void Perform();
  //
  // History
  Standard_EXPORT
    virtual const TopTools_ListOfShape& Generated
      (const TopoDS_Shape& theShape);
  //
  Standard_EXPORT
    virtual const TopTools_ListOfShape& Modified
      (const TopoDS_Shape& theShape);
  //
  Standard_EXPORT
    virtual Standard_Boolean IsDeleted
      (const TopoDS_Shape& theShape);
  //
  Standard_EXPORT
    virtual Standard_Boolean HasDeleted();
  //
  Standard_EXPORT
    virtual Standard_Boolean HasGenerated();
  //
  Standard_EXPORT
    virtual Standard_Boolean HasModified();

  Standard_EXPORT
   static void SetFileName(const TCollection_AsciiString& theNameAfter);

  //
  // Inner content
 protected:
  Standard_EXPORT
    virtual void CheckData();
  //
  Standard_EXPORT
    void Prepare();
  //
  Standard_EXPORT
    void Transform1();
  //
  Standard_EXPORT
    void Transform2();
  //
  Standard_EXPORT
    void BuildFuse();
  //
  Standard_EXPORT
    void ComputeTrsf();
  //
 protected:
  Standard_Boolean myToTransform;
  Standard_Real myTreshold;
  TopoDS_Shape myArgument;
  //
  gp_Trsf myTrsf1;
  gp_Trsf myTrsf2;
  BRepBuilderAPI_Transform* myTransform1;
  BRepBuilderAPI_Transform* myTransform2;
  BOPAlgo_Builder* myBuilder;
  //
  int myErrorStatus; //no longer used since occt 7.2.1 (added for backcompatibility)
  int myWarningStatus;  //no longer used since occt 7.2.1
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDRODATA_SINUSX_H
#define HYDRODATA_SINUSX_H

#include <vector>
#include <utility>
#include <HYDROData.h>
#include <NCollection_Sequence.hxx>
#include <QString>
#include <QStringList>

class QFile;
class gp_XYZ;
class HYDROData_Entity;
class HYDROData_Document;

struct HYDROGUI_CurveBlock
{
  //HYDROGUI_CurveBlock() : myType(-1), myIsConnected(false), myIsClosed(false), myName(""), myCurvePlane(-1), myRefRatio(1.0) 
    //{};
  std::vector<gp_XYZ> myXYZPoints;
  int myType;
  bool myIsSpline;
  bool myIsClosed;
  int myCurvePlane;
  QString myName;
  std::vector<double> myAdditionalCurveInfo;
  std::vector<double> myRefCoords;
  double myRefRatio;
};

class HYDRODATA_EXPORT HYDROData_SinusX 
{
public:
  struct ImportOptions
  {
    ImportOptions() : ImportAsBathy(false), ImportAsProfile(false), ImportAsPolyXY(false)
    {};
    bool ImportAsBathy;
    bool ImportAsProfile;
    bool ImportAsPolyXY;
  };

public:
  HYDROData_SinusX( );
  virtual ~HYDROData_SinusX();

  bool OpenAndParse(const QString& theFilePath);
  void Import(Handle(HYDROData_Document) theDocument, NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities,
       std::vector<ImportOptions>* importOptions = NULL);
  
  bool Export(const QString& theFilePath, const NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities);
  std::vector<HYDROGUI_CurveBlock> GetCurveBlocks() const;
  
private:
  void SXToHydro(Handle(HYDROData_Document) theDocument, NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities,
    std::vector<ImportOptions>* importOptions);
  bool Parse( QFile& theFile );
  void CollectExistingNames(Handle(HYDROData_Document) theDocument);
  QString GetName(const QString& theName);
  void HydroToSX( QFile& theFile, const NCollection_Sequence<Handle(HYDROData_Entity)>& theEntities);

private:

  std::vector<HYDROGUI_CurveBlock> myCurveBlocks;
  QStringList myExistingNames;
};

#endif

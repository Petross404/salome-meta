// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Bathymetry_HeaderFile
#define HYDROData_Bathymetry_HeaderFile

#include "HYDROData_IAltitudeObject.h"
#include <vector>

class QFile;
class gp_XYZ;
class gp_XY;
class HYDROData_PolylineXY;
class HYDROData_QuadtreeNode;
class vtkPolyData;
class vtkIdList;


/**\class HYDROData_Bathymetry
 * \brief Class that stores/retreives information about the Bathymetry.
 *
 * The Bathymetry represents measurement of the altitude of points on the terrain.
 */
class HYDROData_Bathymetry : public HYDROData_IAltitudeObject
{
public:
  struct HYDRODATA_EXPORT AltitudePoint
  {
    AltitudePoint( double x=0, double y=0, double z=0 );
    double X;
    double Y;
    double Z;

    double SquareDistance( const AltitudePoint& ) const;
  };
  typedef std::vector<AltitudePoint> AltitudePoints;

protected:

  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_IAltitudeObject::DataTag_First + 100, ///< first tag, to reserve
    DataTag_AltitudePoints,    ///< altitude points, array of reals
    DataTag_FilePath,          ///< bathymetry imported file path
    DataTag_FilePaths,         ///< bathymetry imported file paths
    DataTag_AltitudesInverted, ///< flag to invert z values
    DataTag_Quadtree,          ///< quadtree associated to the zone
    DataTag_Delaunay,          ///< Delaunay 2D associated to the zone
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Bathymetry, HYDROData_IAltitudeObject);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_BATHYMETRY; }


  /**
   * Dump Bathymetry object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

public:      
  // Public methods to work with Bathymetry altitudes.

  /**
   * Replace current altitude points by new one.
   * \param thePoints the altitude points list
   */
  HYDRODATA_EXPORT virtual void             SetAltitudePoints( const AltitudePoints& thePoints );

  /**
   * Returns altitude points list.
   * \return points list
   */
  HYDRODATA_EXPORT virtual AltitudePoints   GetAltitudePoints(bool IsConvertToGlobal = false) const;
  HYDRODATA_EXPORT virtual HYDROData_QuadtreeNode* GetQuadtreeNodes() const;
  HYDRODATA_EXPORT virtual HYDROData_QuadtreeNode* ComputeQuadtreeNodes( int key) const;

  HYDRODATA_EXPORT virtual NCollection_Sequence<double> GetAltitudesForPoints( const NCollection_Sequence<gp_XY>& thePoints, int theMethod = 0) const;

#ifndef LIGHT_MODE
  HYDRODATA_EXPORT virtual vtkPolyData* GetVtkDelaunay2D() const;
  HYDRODATA_EXPORT virtual vtkPolyData* ComputeVtkDelaunay2D(int key) const;
#endif

  /**
   * Remove all altitude points.
   */
  HYDRODATA_EXPORT virtual void             RemoveAltitudePoints();

  /**
   * Returns altitude for given point.
   * \param thePoint the point to examine
   * \param theMethod interpolation model, default 0 = nearest point
   * \return altitude value
   */
  HYDRODATA_EXPORT virtual double           GetAltitudeForPoint( const gp_XY& thePoint, int theMethod=0 ) const;

public:
  // Public methods to work with files.

  /**
   * Stores the bathymetry file path
   * \param theFilePath image file path
   */
  HYDRODATA_EXPORT void                     SetFilePath( const TCollection_AsciiString& theFilePath );

  HYDRODATA_EXPORT void                     SetFilePaths( const QStringList& theFilePaths );

  /**
   * Returns uploaded bathymetry file path
   */
  HYDRODATA_EXPORT TCollection_AsciiString  GetFilePath() const;

  HYDRODATA_EXPORT QStringList GetFilePaths() const;

  /**
   * Set flag indicating needs to invert altitude values
   * \param theIsInverted new invert value
   * \param theIsUpdate flag indicating necessity to update points
   */
  HYDRODATA_EXPORT void                     SetAltitudesInverted( const bool theIsInverted,
                                                                  const bool theIsUpdate = true );

  /**
   * Returns flag indicating needs to invert altitude values.
   */
  HYDRODATA_EXPORT bool                     IsAltitudesInverted() const;

  /**
   * Imports Bathymetry data from file. The supported file types:
   *  - xyz
   * \param theFileName the path to file
   * \return \c true if file has been successfully read
   */
  HYDRODATA_EXPORT virtual bool             ImportFromFiles( const QStringList& theFileNames );

  HYDRODATA_EXPORT virtual bool             ImportFromFile( const QString& theFileName );

  HYDRODATA_EXPORT Handle(HYDROData_PolylineXY) CreateBoundaryPolyline() const;

  HYDRODATA_EXPORT virtual void UpdateLocalCS( double theDx, double theDy );

private:

  /**
   * Imports Bathymetry data from 'XYZ' file.
   */
  bool                                      importFromXYZFile( QFile&          theFile,
                                                               AltitudePoints& thePoints ) const;
  //static int myQuadTreeNumber;
  static std::map<int, HYDROData_QuadtreeNode*> myQuadtrees;
#ifndef LIGHT_MODE
  //static int myDelaunayNumber;
  static std::map<int, vtkPolyData*> myDelaunay2D;
#endif
  bool                                      importFromASCFile( QFile&          theFile,
                                                               AltitudePoints& thePoints ) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Bathymetry();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_Bathymetry();
};

#endif

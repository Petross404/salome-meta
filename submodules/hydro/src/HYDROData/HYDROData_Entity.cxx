// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_Entity.h>
#include <HYDROData_Document.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Tool.h>
#include <Standard_GUID.hxx>
#include <TDataStd_Name.hxx>
#include <TDataStd_ByteArray.hxx>
#include <TDataStd_UAttribute.hxx>
#include <TDataStd_AsciiString.hxx>
#include <TDataStd_Integer.hxx>
#include <TDataStd_IntegerArray.hxx>
#include <TDataStd_ReferenceList.hxx>
#include <TDataStd_Real.hxx>
#include <TDF_CopyLabel.hxx>
#include <TDF_ListIteratorOfLabelList.hxx>
#include <TNaming_Builder.hxx>
#include <TNaming_NamedShape.hxx>
#include <TopoDS_Shape.hxx>
#include <QColor>
#include <QRegExp>
#include <QStringList>
#include <QVariant>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROData_SequenceOfObjects::HYDROData_SequenceOfObjects()
  : NCollection_Sequence<Handle(HYDROData_Entity)>()
{
}

HYDROData_SequenceOfObjects::HYDROData_SequenceOfObjects( const HYDROData_SequenceOfObjects& theSequence )
  : NCollection_Sequence<Handle(HYDROData_Entity)>( theSequence )
{
}

HYDROData_SequenceOfObjects::HYDROData_SequenceOfObjects( const NCollection_Sequence<Handle(HYDROData_Entity)>& theSequence )
  : NCollection_Sequence<Handle(HYDROData_Entity)>( theSequence )
{
}

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Entity,Standard_Transient)

// is equal function for unique object mapping
bool IsEqual(const Handle(HYDROData_Entity)& theObj1, const Handle(HYDROData_Entity)& theObj2)
{
  if ( !theObj1.IsNull() && !theObj2.IsNull() )
    return theObj1->Label() == theObj2->Label();
  return false;
}

QString HYDROData_Entity::GetName() const
{
  Handle(TDataStd_Name) aName;
  if (myLab.FindAttribute(TDataStd_Name::GetID(), aName)) {
    TCollection_AsciiString aStr(aName->Get());
    return QString(aStr.ToCString());
  }
  return QString();
}

QString HYDROData_Entity::GetType() const
{
  return Type( GetKind() );
}

QString HYDROData_Entity::Type( ObjectKind theKind )
{
  switch( theKind )
  {
  case KIND_UNKNOWN:
    return "Unknown";
  case KIND_IMAGE:
    return "Image";
  case KIND_POLYLINE:
    return "Polyline";
  case KIND_BATHYMETRY:
    return "Bathymetry";
  case KIND_ALTITUDE:
    return "Altitude";
  case KIND_IMMERSIBLE_ZONE:
    return "Immersible_zone";
  case KIND_RIVER:
    return "River";
  case KIND_STREAM:
    return "Stream";
  case KIND_CONFLUENCE:
    return "Confluence";
  case KIND_CHANNEL:
    return "Channel";
  case KIND_OBSTACLE:
    return "Obstacle";
  case KIND_DIGUE:
    return "Digue";
  case KIND_PROFILE:
    return "Profile";
  case KIND_PROFILEUZ:
    return "Profile_UZ";
  case KIND_POLYLINEXY:
    return "Polyline_XY";
  case KIND_CALCULATION:
    return "Calculation_case";
  case KIND_ZONE:
    return "Zone";
  case KIND_REGION:
    return "Region";
  case KIND_VISUAL_STATE:
    return "Visual_state";
  case KIND_ARTIFICIAL_OBJECT:
    return "Artificial_object";
  case KIND_NATURAL_OBJECT:
    return "Natural_object";
  case KIND_DUMMY_3D:
    return "Dummy_3d";
  case KIND_SHAPES_GROUP:
    return "Shapes_group";
  case KIND_SPLIT_GROUP:
    return "Split_group";
  case KIND_STREAM_ALTITUDE:
    return "Stream_altitude";
  case KIND_OBSTACLE_ALTITUDE:
    return "Obstacle_altitude";
  case KIND_STRICKLER_TABLE:
    return "Strickler_table";
  case KIND_LAND_COVER_OBSOLETE:
    return "Land_cover";
  case KIND_CHANNEL_ALTITUDE:
    return "Channel_altitude";
  case KIND_LAND_COVER_MAP:
    return "Land_cover_map";
  case KIND_DTM:
    return "DTM";
  case KIND_LISM:
    return "LISM";
  case KIND_BC_POLYGON:
    return "Boundary_Polygon";
  }
  return "";
}

QString HYDROData_Entity::GetObjPyName() const
{
  QString aName = GetName();
  aName.replace(QRegExp("[\\W]"), "_");

  if( aName[0].isDigit() )
    aName = GetType() + "_" + aName;

  return aName;
}

QString HYDROData_Entity::GetDefaultName() const
{
  QString aName;

  TDF_Label aLabel = myLab.FindChild(DataTag_DefaultName, false);
  if (!aLabel.IsNull())
    {
      Handle(TDataStd_AsciiString) anAsciiStr;
      if (aLabel.FindAttribute(TDataStd_AsciiString::GetID(), anAsciiStr))
        aName = QString(anAsciiStr->Get().ToCString());
    }
  else
    aName = GetName();

  return aName;

}

void HYDROData_Entity::SetName(const QString& theName, bool isDefault)
{
  Handle(TDataStd_Name) A = TDataStd_Name::Set(myLab, TCollection_ExtendedString(theName.toLatin1().constData()));
  A->SetID(TDataStd_Name::GetID());
  if (isDefault)
  {
    TDF_Label aDefaultNameLabel = myLab.FindChild( DataTag_DefaultName, true );
    if ( aDefaultNameLabel.IsNull() )
      return;
    Handle(TDataStd_AsciiString) theDefaultName;

    if ( !aDefaultNameLabel.FindAttribute( TDataStd_AsciiString::GetID(), theDefaultName ))
    {
      TCollection_AsciiString aName = theName.toStdString().c_str();
      theDefaultName = TDataStd_AsciiString::Set(myLab.FindChild( DataTag_DefaultName), aName );
      theDefaultName->SetID( TDataStd_AsciiString::GetID() );
    }
  }
}

QStringList HYDROData_Entity::DumpToPython( const QString& thePyScriptPath,
                                            MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList anEmptyList;
  return anEmptyList;
}

void HYDROData_Entity::Update()
{
  ClearChanged();
}

void HYDROData_Entity::UpdateLocalCS( double theDx, double theDy )
{
  //On the base level no actions are necessary
}

bool HYDROData_Entity::IsHas2dPrs() const
{
  return false;
}

void HYDROData_Entity::Show()
{
  if ( !IsHas2dPrs() )
    return;

  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return;

  aDocument->Show( this );
}

QVariant HYDROData_Entity::GetDataVariant()
{
  return QVariant();
}

void HYDROData_Entity::ClearChanged()
{
  Handle(TDataStd_Integer) A = TDataStd_Integer::Set( myLab.FindChild( DataTag_GeomChange ), 0 );
  A->SetID(TDataStd_Integer::GetID());
}

int HYDROData_Entity::GetGeomChangeFlag() const
{
  int aGeomChangeFlag = 0;
  Handle(TDataStd_Integer) aGeomChangeAttr;
  TDF_Label aGeomChangeLab = myLab.FindChild( DataTag_GeomChange );
  aGeomChangeLab.FindAttribute( TDataStd_Integer::GetID(), aGeomChangeAttr );
  if ( !aGeomChangeAttr.IsNull() )
    aGeomChangeFlag = aGeomChangeAttr->Get();
  return aGeomChangeFlag;
}

void HYDROData_Entity::Changed( Geometry theChangedGeometry )
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if( aDocument.IsNull() )
    return;

  int aGeomChangeFlag = 0;
  Handle(TDataStd_Integer) aGeomChangeAttr;
  TDF_Label aGeomChangeLab = myLab.FindChild( DataTag_GeomChange );
  aGeomChangeLab.FindAttribute( TDataStd_Integer::GetID(), aGeomChangeAttr );
  if ( !aGeomChangeAttr.IsNull() )
    aGeomChangeFlag = aGeomChangeAttr->Get();

  int aBitsToChange = ( myGeom & theChangedGeometry );
  if( aBitsToChange == 0 )
    return;

  aGeomChangeFlag = ( aGeomChangeFlag | aBitsToChange );
  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( aGeomChangeLab, aGeomChangeFlag );
  anAttr->SetID( TDataStd_Integer::GetID() );

  HYDROData_Iterator anIter( aDocument );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIter.Current();
    if( anObject.IsNull() )
      continue;
    HYDROData_SequenceOfObjects aRefSeq = anObject->GetAllReferenceObjects();
    for ( int i = 1, n = aRefSeq.Length(); i <= n; ++i )
    {
      Handle(HYDROData_Entity) aRefObject = aRefSeq.Value( i );
      if( aRefObject->Label()==myLab )
        anObject->Changed( theChangedGeometry );
    }
  }
}

bool HYDROData_Entity::IsMustBeUpdated( Geometry theGeom ) const
{
  return ( ( GetGeomChangeFlag() & theGeom ) != 0 );
}

bool HYDROData_Entity::CanBeUpdated() const
{
  return true;
}

bool HYDROData_Entity::IsRemoved() const
{
  return !myLab.HasAttribute();
}

void HYDROData_Entity::Remove()
{
  return myLab.ForgetAllAttributes( true );
}

bool HYDROData_Entity::CanRemove()
{
  return true;
}

HYDROData_Entity::HYDROData_Entity( Geometry theGeom )
  : myGeom( theGeom )
{
}

HYDROData_Entity::~HYDROData_Entity()
{
}

void HYDROData_Entity::CopyTo( const Handle(HYDROData_Entity)& theDestination,
                               bool isGenerateNewName ) const
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() ) {
    return;
  }

  TDF_CopyLabel aCopy(myLab, theDestination->Label());
  aCopy.Perform();

  if( isGenerateNewName )
  {
    // generate a new unique name for the clone object:
    // case 1: Image_1 -> Image_2
    // case 2: ImageObj -> ImageObj_1
    QString aName = theDestination->GetName();
    QString aPrefix = aName;
    if( aName.contains( '_' ) ) { // case 1
      QString aSuffix = aName.section( '_', -1 );
      bool anIsInteger = false;
      aSuffix.toInt( &anIsInteger );
      if( anIsInteger )
        aPrefix = aName.section( '_', 0, -2 );
    } else { // case 2
      aPrefix = aName;
    }

    aName = HYDROData_Tool::GenerateObjectName( aDocument, aPrefix );
    theDestination->SetName( aName );
  }
}

Handle(HYDROData_Entity) HYDROData_Entity::GetFatherObject() const
{
  Handle(HYDROData_Entity) aFather;

  if ( !myLab.IsNull() )
  {
    TDF_Label aFatherLabel = myLab.Father();

    while ( aFather.IsNull() && !aFatherLabel.IsNull() && !aFatherLabel.IsRoot() )
    {
      aFather = HYDROData_Iterator::Object( aFatherLabel );
      aFatherLabel = aFatherLabel.Father();
    }
  }

  return aFather;
}

HYDROData_SequenceOfObjects HYDROData_Entity::GetAllReferenceObjects() const
{
  return HYDROData_SequenceOfObjects();
}

bool HYDROData_Entity::GetZLevel( Standard_Integer& theLevel ) const
{
  theLevel = -1;

  TDF_Label aLabel = myLab.FindChild( DataTag_ZLevel, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_Integer) anIntVal;
    if ( aLabel.FindAttribute( TDataStd_Integer::GetID(), anIntVal ) )
    {
      theLevel = anIntVal->Get();
      return true;
    }
  }

  return false;
}

void HYDROData_Entity::SetZLevel( const Standard_Integer& theLevel )
{
  Handle(TDataStd_Integer) A = TDataStd_Integer::Set( myLab.FindChild( DataTag_ZLevel ), theLevel );
  A->SetID(TDataStd_Integer::GetID());
}

void HYDROData_Entity::RemoveZLevel()
{
  TDF_Label aLabel = myLab.FindChild( DataTag_ZLevel, false );
  if ( !aLabel.IsNull() )
    aLabel.ForgetAllAttributes();
}

void HYDROData_Entity::SetLabel( const TDF_Label& theLabel )
{
  myLab = theLabel;
}

void HYDROData_Entity::SaveByteArray( const int   theTag,
                                      const char* theData,
                                      const int   theLen )
{
  TDF_Label aLab = theTag == 0 ? myLab : myLab.FindChild(theTag);
  // array is empty, remove the attribute
  if (theLen <= 0) {
    aLab.ForgetAttribute(TDataStd_ByteArray::GetID());
    return;
  }
  // store data of image in byte array
  Handle(TDataStd_ByteArray) aData;
  if (!aLab.FindAttribute(TDataStd_ByteArray::GetID(), aData)) {
    aData = TDataStd_ByteArray::Set(aLab, 1, theLen);
    aData->SetID( TDataStd_ByteArray::GetID() );
  }
  Standard_Byte* Byte0 = &(aData->InternalArray()->ChangeArray1().ChangeFirst());
  memcpy(Byte0, theData, theLen * sizeof (char));
  // copy bytes one by one
  if (aData->Length() != theLen) {
    Handle(TColStd_HArray1OfByte) aNewData = new TColStd_HArray1OfByte(1, theLen);
    for(int a = 0; a < theLen; a++)
      aNewData->SetValue(a + 1, theData[a]);
    aData->ChangeArray(aNewData);
  }
  else
  {
  //  for(int a = 0; a < theLen; a++)
  //    aData->SetValue(a + 1, theData[a]);
  }
}

const char* HYDROData_Entity::ByteArray(const int theTag, int& theLen) const
{
  TDF_Label aLab = theTag == 0 ? myLab : myLab.FindChild(theTag);
  Handle(TDataStd_ByteArray) aData;
  if (!aLab.FindAttribute(TDataStd_ByteArray::GetID(), aData))
    return NULL; // return empty image if there is no array
  theLen = aData->Length();
  if (theLen)
    return (const char*)(&(aData->InternalArray()->ChangeArray1().ChangeValue(1)));
  return NULL;
}

int HYDROData_Entity::NbReferenceObjects( const int theTag ) const
{
  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, false );
  return aRefs.IsNull() ? 0 : aRefs->Extent();
}

bool HYDROData_Entity::HasReference( const Handle(HYDROData_Entity)& theObj,
                                     const int                      theTag ) const
{
  if ( theObj.IsNull() )
    return false;

  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, false );
  if ( aRefs.IsNull() || aRefs->IsEmpty() )
    return false;

  TDF_ListIteratorOfLabelList aListIt( aRefs->List() );
  for ( ; aListIt.More(); aListIt.Next() )
  {
    const TDF_Label& aRefLabel = aListIt.Value();
    if  ( theObj->Label() == aRefLabel )
      return true;
  }

  return false;
}

void HYDROData_Entity::AddReferenceObject( const Handle(HYDROData_Entity)& theObj,
                                           const int                      theTag )
{
  if ( theObj.IsNull() )
    return;

  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, true );
  aRefs->Append( theObj->Label() );
  aRefs->SetID(TDataStd_ReferenceList::GetID());
}

void HYDROData_Entity::SetReferenceObject( const Handle(HYDROData_Entity)& theObj,
                                           const int                      theTag,
                                           const int                      theIndex )
{
  if ( theObj.IsNull() )
  {
    RemoveReferenceObject( theTag, theIndex );
    return;
  }

  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, true );

  if ( theIndex >= aRefs->Extent() )
  {
    aRefs->Append( theObj->Label() );
  }
  else if ( theIndex < 0 )
  {
    aRefs->Prepend( theObj->Label() );
  }
  else
  {
    RemoveReferenceObject( theTag, theIndex );

    Handle(HYDROData_Entity) aBeforeObj = GetReferenceObject( theTag, theIndex );

    aRefs = getReferenceList( theTag, true ); // because reference list can be removed
    if ( !aBeforeObj.IsNull() )
      aRefs->InsertBefore( theObj->Label(), aBeforeObj->Label() );
    else
      aRefs->Append( theObj->Label() );
  }
}

void HYDROData_Entity::InsertReferenceObject( const Handle(HYDROData_Entity)& theObj,
                                              const int                      theTag,
                                              const int                      theBeforeIndex )
{
  if ( theObj.IsNull() )
    return;

  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, true );

  if ( theBeforeIndex >= aRefs->Extent() )
  {
    aRefs->Append( theObj->Label() );
  }
  else if ( theBeforeIndex < 0 )
  {
    aRefs->Prepend( theObj->Label() );
  }
  else
  {
    Handle(HYDROData_Entity) aBeforeObj = GetReferenceObject( theTag, theBeforeIndex );
    if ( !aBeforeObj.IsNull() )
      aRefs->InsertBefore( theObj->Label(), aBeforeObj->Label() );
    else
      aRefs->Append( theObj->Label() );
  }
}

void HYDROData_Entity::SetReferenceObjects( const HYDROData_SequenceOfObjects& theObjects,
                                            const int                          theTag )
{
  ClearReferenceObjects( theTag );
  if ( theObjects.IsEmpty() )
    return;

  HYDROData_SequenceOfObjects::Iterator anIter( theObjects );
  for ( ; anIter.More(); anIter.Next() )
    AddReferenceObject( anIter.Value(), theTag );
}

Handle(HYDROData_Entity) HYDROData_Entity::GetReferenceObject( const int theTag,
                                                               const int theIndex ) const
{
  //DEBTRACE("GetReferenceObject " << theTag << " " << theIndex);
  Handle(HYDROData_Entity) aRes;

  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, false );
  if ( aRefs.IsNull() || theIndex < 0 || theIndex >= aRefs->Extent() )
    return aRes;

  TDF_ListIteratorOfLabelList anIter( aRefs->List() );
  for ( int anIndex = 0; anIndex != theIndex && anIter.More(); anIter.Next(), ++anIndex );

  const TDF_Label& aRefLabel = anIter.Value();
  aRes = HYDROData_Iterator::Object( aRefLabel );

  return aRes;
}

HYDROData_SequenceOfObjects HYDROData_Entity::GetReferenceObjects( const int theTag ) const
{
  HYDROData_SequenceOfObjects aRes;

  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, false );
  if ( aRefs.IsNull() )
    return aRes;

  TDF_ListIteratorOfLabelList anIter( aRefs->List() );
  for ( ; anIter.More(); anIter.Next() )
  {
    const TDF_Label& aRefLabel = anIter.Value();

    Handle(HYDROData_Entity) aRefObject = HYDROData_Iterator::Object( aRefLabel );
    if ( aRefObject.IsNull() )
      continue;

    aRes.Append( aRefObject );
  }

  return aRes;
}

void HYDROData_Entity::RemoveReferenceObject( const TDF_Label& theRefLabel,
                                              const int        theTag )
{
  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, false );
  if ( aRefs.IsNull() )
    return;

  if ( aRefs->Extent() == 1 )
  {
    // remove all if only one
    ClearReferenceObjects( theTag );
    return;
  }

  aRefs->Remove( theRefLabel );
}

void HYDROData_Entity::RemoveReferenceObject( const int theTag,
                                              const int theIndex )
{
  Handle(TDataStd_ReferenceList) aRefs = getReferenceList( theTag, false );
  if ( aRefs.IsNull() )
    return;

  if ( aRefs->Extent() == 1 && theIndex == 0 )
  {
    // remove all if only one
    ClearReferenceObjects( theTag );
    return;
  }

  int anIndex = 0;
  TDF_ListIteratorOfLabelList anIter( aRefs->List() );
  for ( ; anIndex != theIndex && anIter.More(); anIter.Next(), ++anIndex );

  if ( anIndex != theIndex || !anIter.More() )
    return;

  const TDF_Label& aRefLabel = anIter.Value();
  aRefs->Remove( aRefLabel );
}

void HYDROData_Entity::ClearReferenceObjects( const int theTag )
{
  TDF_Label aSetLabel = theTag == 0 ? myLab : myLab.FindChild( theTag );
  aSetLabel.ForgetAttribute( TDataStd_ReferenceList::GetID() );
}

Handle(TDataStd_ReferenceList) HYDROData_Entity::getReferenceList( const int theTag,
                                                                   const bool theIsCreate ) const
{
  //DEBTRACE("getReferenceList " << theTag << " " << theIsCreate);
  TDF_Label aLabel = theTag == 0 ? myLab : myLab.FindChild( theTag );

  Handle(TDataStd_ReferenceList) aRefs;
  if ( !aLabel.FindAttribute( TDataStd_ReferenceList::GetID(), aRefs ) && theIsCreate )
  {
    aRefs = TDataStd_ReferenceList::Set( aLabel );
    aRefs->SetID(TDataStd_ReferenceList::GetID());
  }
  return aRefs;
}

void HYDROData_Entity::SetColor( const QColor& theColor,
                                 const int     theTag )
{
  TDF_Label aLabel = theTag == 0 ? myLab : myLab.FindChild( theTag );

  Handle(TDataStd_IntegerArray) aColorArray;
  if ( !aLabel.FindAttribute( TDataStd_IntegerArray::GetID(), aColorArray ) )
  {
    aColorArray = TDataStd_IntegerArray::Set( aLabel, 1, 4 );
    aColorArray->SetID(TDataStd_IntegerArray::GetID());
  }

  aColorArray->SetValue( 1, theColor.red()   );
  aColorArray->SetValue( 2, theColor.green() );
  aColorArray->SetValue( 3, theColor.blue()  );
  aColorArray->SetValue( 4, theColor.alpha() );
}

QColor HYDROData_Entity::GetColor( const QColor& theDefColor,
                                   const int     theTag ) const
{
  QColor aResColor = theDefColor;

  TDF_Label aLabel = theTag == 0 ? myLab : myLab.FindChild( theTag );

  Handle(TDataStd_IntegerArray) aColorArray;
  if ( aLabel.FindAttribute( TDataStd_IntegerArray::GetID(), aColorArray ) )
  {
    aResColor.setRed(   aColorArray->Value( 1 ) );
    aResColor.setGreen( aColorArray->Value( 2 ) );
    aResColor.setBlue(  aColorArray->Value( 3 ) );
    aResColor.setAlpha( aColorArray->Value( 4 ) );
  }

  return aResColor;
}

bool HYDROData_Entity::GetColor( QColor& theOutColor,
                                 const int     theTag ) const
{
  TDF_Label aLabel = theTag == 0 ? myLab : myLab.FindChild( theTag );

  Handle(TDataStd_IntegerArray) aColorArray;
  if ( aLabel.FindAttribute( TDataStd_IntegerArray::GetID(), aColorArray ) )
  {
    theOutColor.setRed(   aColorArray->Value( 1 ) );
    theOutColor.setGreen( aColorArray->Value( 2 ) );
    theOutColor.setBlue(  aColorArray->Value( 3 ) );
    theOutColor.setAlpha( aColorArray->Value( 4 ) );
    return true;
  }

  return false;
}


QStringList HYDROData_Entity::dumpObjectCreation( MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList;

  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return aResList;

  QString aDocName = aDocument->GetDocPyName();
  QString aName = GetObjPyName();

  aResList << QString( "%1 = %2.CreateObject( %3 )" )
              .arg( aName ).arg( aDocName ).arg( getPyTypeID() );
  aResList << QString( "%1.SetName( \"%2\" )" )
              .arg( aName ).arg( GetName() );
  aResList << QString( "" );

  if ( IsHas2dPrs() )
  {
    // Dump object z-level in viewer
    Standard_Integer anObjZLevel = -1;
    if ( GetZLevel( anObjZLevel ) )
    {
      aResList << QString( "%1.SetZLevel( %2 )" )
                  .arg( aName ).arg( anObjZLevel );
      aResList << QString( "" );
    }
  }

  return aResList;
}

QString HYDROData_Entity::getPyTypeID() const
{
  //DEBTRACE("HYDROData_Entity::getPyTypeID " << GetKind());
  switch( GetKind() )
  {
    case KIND_IMAGE:             return "KIND_IMAGE";
    case KIND_POLYLINE:          return "KIND_POLYLINE";
    case KIND_BATHYMETRY:        return "KIND_BATHYMETRY";
    case KIND_ALTITUDE:          return "KIND_ALTITUDE";
    case KIND_IMMERSIBLE_ZONE:   return "KIND_IMMERSIBLE_ZONE";
    case KIND_RIVER:             return "KIND_RIVER";
    case KIND_STREAM:            return "KIND_STREAM";
    case KIND_CONFLUENCE:        return "KIND_CONFLUENCE";
    case KIND_CHANNEL:           return "KIND_CHANNEL";
    case KIND_OBSTACLE:          return "KIND_OBSTACLE";
    case KIND_DIGUE:             return "KIND_DIGUE";
    case KIND_PROFILE:           return "KIND_PROFILE";
    case KIND_PROFILEUZ:         return "KIND_PROFILEUZ";
    case KIND_POLYLINEXY:        return "KIND_POLYLINEXY";
    case KIND_CALCULATION:       return "KIND_CALCULATION";
    case KIND_ZONE:              return "KIND_ZONE";
    case KIND_REGION:            return "KIND_REGION";
    case KIND_VISUAL_STATE:      return "KIND_VISUAL_STATE";
    case KIND_ARTIFICIAL_OBJECT: return "KIND_ARTIFICIAL_OBJECT";
    case KIND_NATURAL_OBJECT:    return "KIND_NATURAL_OBJECT";
    case KIND_DUMMY_3D:          return "KIND_DUMMY_3D";
    case KIND_SHAPES_GROUP:      return "KIND_SHAPES_GROUP";
    case KIND_SPLIT_GROUP:       return "KIND_SPLIT_GROUP";
    case KIND_STREAM_ALTITUDE:   return "KIND_STREAM_ALTITUDE";
    case KIND_OBSTACLE_ALTITUDE: return "KIND_OBSTACLE_ALTITUDE";
    case KIND_STRICKLER_TABLE:   return "KIND_STRICKLER_TABLE";
    case KIND_LAND_COVER_OBSOLETE: return "";
    case KIND_CHANNEL_ALTITUDE:  return "KIND_CHANNEL_ALTITUDE";
    case KIND_LAND_COVER_MAP:    return "KIND_LAND_COVER_MAP";
    case KIND_BC_POLYGON:        return "KIND_BC_POLYGON";
    default:                     return "KIND_UNKNOWN"; ///! Unrecognized object
  }
}

void HYDROData_Entity::setPythonReferenceObject( const QString&                  thePyScriptPath,
                                                 MapOfTreatedObjects&            theTreatedObjects,
                                                 QStringList&                    theScript,
                                                 const Handle(HYDROData_Entity)& theRefObject,
                                                 const QString&                  theMethod ) const
{
  if ( !checkObjectPythonDefinition( thePyScriptPath, theTreatedObjects, theScript, theRefObject ) )
    return;

  QString aRefObjName = theRefObject->GetObjPyName();

  QString anObjName = GetObjPyName();
  theScript << QString( "%1.%2( %3 )" )
               .arg( anObjName ).arg( theMethod ).arg( aRefObjName );
}

bool HYDROData_Entity::checkObjectPythonDefinition( const QString&                  thePyScriptPath,
                                                    MapOfTreatedObjects&            theTreatedObjects,
                                                    QStringList&                    theScript,
                                                    const Handle(HYDROData_Entity)& theRefObject ) const
{
  if ( theRefObject.IsNull() )
    return false;

  QString aRefObjName = theRefObject->GetName();
  if ( aRefObjName.isEmpty() )
    return false;

  if ( theTreatedObjects.contains( aRefObjName ) )
    return true;

  // The definition of reference object must be dumped before this
  QStringList aRefObjDump = theRefObject->DumpToPython( thePyScriptPath, theTreatedObjects );
  if ( aRefObjDump.isEmpty() )
    return false;

  QStringList aTmpList = theScript;
  theScript = aRefObjDump;

  theScript << QString( "" );
  theScript << aTmpList;

  theTreatedObjects.insert( aRefObjName, theRefObject );

  return true;
}

void HYDROData_Entity::setPythonObjectColor( QStringList&         theScript,
                                             const QColor&        theColor,
                                             const QColor&        theDefaultColor,
                                             const QString&       theMethod ) const
{
  if ( theColor == theDefaultColor )
    return; //Do not set the color for object if it like default

  QString anObjName = GetObjPyName();
  theScript << QString( "%1.%2( QColor( %3, %4, %5, %6 ) )" )
              .arg( anObjName ).arg( theMethod )
              .arg( theColor.red()  ).arg( theColor.green() )
              .arg( theColor.blue() ).arg( theColor.alpha() );
}

void HYDROData_Entity::findPythonReferenceObject( QStringList& theScript,
                                                  QString      defName) const
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return;

  if (defName.isEmpty())
    theScript << QString( "%1 = %2.FindObjectByName( \"%3\" )" ).arg( GetObjPyName() )
                                                                .arg( aDocument->GetDocPyName() )
                                                                .arg( GetDefaultName() );
  else
    theScript << QString( "%1 = %2.FindObjectByName( \"%3\" )" ).arg( GetObjPyName() )
                                                                .arg( aDocument->GetDocPyName() )
                                                                .arg( defName );
}

void HYDROData_Entity::SetNameInDumpPython(QStringList&  theScript,
                                           QString       theName) const
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return;

  if (theName.isEmpty())
      theScript << QString( "%1.SetName( \"%2\" )" ).arg( GetObjPyName() )
                                                    .arg( GetName() );
  else
    theScript << QString( "%1.SetName( \"%2\" )" ).arg( GetObjPyName() )
                                                  .arg( theName );
}

void HYDROData_Entity::SetShape( int theTag, const TopoDS_Shape& theShape )
{
  TNaming_Builder aBuilder( myLab.FindChild( theTag ) );
  aBuilder.Generated( theShape );
  aBuilder.NamedShape()->SetID(TNaming_NamedShape::GetID());
}

TopoDS_Shape HYDROData_Entity::GetShape( int theTag ) const
{
  TDF_Label aShapeLabel = myLab.FindChild( theTag, false );
  if ( !aShapeLabel.IsNull() )
  {
    Handle(TNaming_NamedShape) aNamedShape;
    if ( aShapeLabel.FindAttribute( TNaming_NamedShape::GetID(), aNamedShape ) )
      return aNamedShape->Get();
  }
  return TopoDS_Shape();
}

void HYDROData_Entity::SetDouble( int theTag, double theValue )
{
  Handle(TDataStd_Real) anAttr;
  TDF_Label aLabel = myLab.FindChild( theTag );
  if( !aLabel.FindAttribute( TDataStd_Real::GetID(), anAttr ) )
  {
    anAttr = new TDataStd_Real();
    anAttr->SetID(TDataStd_Real::GetID());
    aLabel.AddAttribute( anAttr );
  }
  anAttr->Set( theValue );
}

double HYDROData_Entity::GetDouble( int theTag, double theDefValue ) const
{
  Handle(TDataStd_Real) anAttr;
  TDF_Label aLabel = myLab.FindChild( theTag );
  if( !aLabel.FindAttribute( TDataStd_Real::GetID(), anAttr ) )
    return theDefValue;

  return anAttr->Get();
}

void HYDROData_Entity::SetInteger( int theTag, int theValue )
{
  Handle(TDataStd_Integer) anAttr;
  TDF_Label aLabel = myLab.FindChild( theTag );
  if( !aLabel.FindAttribute( TDataStd_Integer::GetID(), anAttr ) )
  {
    anAttr = new TDataStd_Integer();
    anAttr->SetID(TDataStd_Integer::GetID());
    aLabel.AddAttribute(anAttr);
  }
  anAttr->Set( theValue );
}

int HYDROData_Entity::GetInteger( int theTag, int theDefValue ) const
{
  Handle(TDataStd_Integer) anAttr;
  TDF_Label aLabel = myLab.FindChild( theTag );
  if( !aLabel.FindAttribute( TDataStd_Integer::GetID(), anAttr ) )
    return theDefValue;

  return anAttr->Get();
}

bool HYDROData_Entity::CompareLabels(const Handle(HYDROData_Entity)& theOtherObj)
{
  if ( !theOtherObj.IsNull() )
    return this->Label() == theOtherObj->Label();
  return false;
}


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_BSplineOperation_HeaderFile
#define HYDROData_BSplineOperation_HeaderFile

#include <HYDROData.h>

#include <Geom_BSplineCurve.hxx>

#include <NCollection_Sequence.hxx>

class QPainterPath;
class gp_XYZ;

/**\class HYDROData_BSplineOperation
 *
 * \brief Allows to work with splines: create, convert to Qt ToolPath.
 *
 * Uses GEOM module for creation of BSplines, OCCT algorithms for 
 * manipulation and conversion.
 */

class HYDRODATA_EXPORT HYDROData_BSplineOperation
{
public:
  static Handle(Geom_BSplineCurve) ComputeCurve(
    const NCollection_Sequence<gp_XYZ>& thePoints,
    const bool                          theIsClosed,
    const double                        theTolerance );

  //! Performs conversion from BSpline curve to QPainterPath made from Bezier curves
  //! \returns computed PainterPath, not stored in this class, so calling of this method is not fast
  static void ComputePath( const Handle(Geom_BSplineCurve)& theCurve, QPainterPath& thePath );
};

#endif

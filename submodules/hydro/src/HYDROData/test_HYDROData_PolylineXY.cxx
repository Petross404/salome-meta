// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include<test_HYDROData_PolylineXY.h>

#include <HYDROData_Document.h>
#include <HYDROData_PolylineXY.h>

#include <QList>
#include <QPointF>

void test_HYDROData_PolylineXY::testPolyline()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline = 
    Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));

  aPolyline->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  aPolyline->AddSection( "Section_2", HYDROData_PolylineXY::SECTION_SPLINE, true );

  int aNbSections = aPolyline->NbSections();
  CPPUNIT_ASSERT( aNbSections == 2 );
  
  NCollection_Sequence<TCollection_AsciiString>           aSectNames;
  NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
  NCollection_Sequence<bool>                              aSectClosures;
  aPolyline->GetSections( aSectNames, aSectTypes, aSectClosures );

  CPPUNIT_ASSERT( aSectNames.Value( 0 ) == "Section_1" );
  CPPUNIT_ASSERT( aSectTypes.Value( 0 ) == HYDROData_PolylineXY::SECTION_POLYLINE );
  CPPUNIT_ASSERT( aSectClosures.Value( 0 ) == false );

  CPPUNIT_ASSERT( aSectNames.Value( 1 ) == "Section_2" );
  CPPUNIT_ASSERT( aSectTypes.Value( 1 ) == HYDROData_PolylineXY::SECTION_SPLINE );
  CPPUNIT_ASSERT( aSectClosures.Value( 1 ) == true );

  aDoc->Close();
}


void test_HYDROData_PolylineXY::testCopy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(HYDROData_PolylineXY) aPolyline1 = 
    Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));


//  aPolyline1->setPoints(aPoints);

  Handle(HYDROData_PolylineXY) aPolyline2 = 
    Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));

  aPolyline1->CopyTo(aPolyline2, true);


  aDoc->Close();
}

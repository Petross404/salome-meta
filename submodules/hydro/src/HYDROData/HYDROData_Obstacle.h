// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Obstacle_HeaderFile
#define HYDROData_Obstacle_HeaderFile

#include "HYDROData_ArtificialObject.h"

/**\class HYDROData_Obstacle
 * \brief 
 *
 */
class HYDROData_Obstacle : public HYDROData_ArtificialObject
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_ArtificialObject::DataTag_First + 100, ///< first tag, to reserve
    DataTag_GeomObjectEntry, ///< study entry of the imported GEOM object
    DataTag_FilePath,        ///< imported file path
    DataTag_Translation,     ///< translation coefficients
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Obstacle, HYDROData_ArtificialObject);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_OBSTACLE;}

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Update the immersible zone object.
   * Call this method whenever you made changes for object data.
   */
  HYDRODATA_EXPORT virtual void Update();

  HYDRODATA_EXPORT virtual void UpdateLocalCS( double theDx, double theDy );

  /**
   * Checks that object has 2D presentation. Reimlemented to retun true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Returns default filling color for new obstacle.
   */
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new obstacle.
   */
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;


  /**
   * Sets the 3d shape of the object.
   */
  HYDRODATA_EXPORT virtual void SetShape3D( const TopoDS_Shape& theShape );

public:

  /**
   * Imports shape from file containing geometrical object and sets this
   * shape as the obstacle shape.
   * The supported file types:
   *  - BREP
   *  - IGES
   *  - STEP
   *  - ACIS
   * \param theFilePath the path to file
   * \return \c true if file has been successfully read
   */
  HYDRODATA_EXPORT virtual bool ImportFromFile( const QString& theFilePath );

  /**
   * Stores the file path of the imported file.
   * \param theFilePath file path
   */
  HYDRODATA_EXPORT void SetFilePath(const QString& theFilePath);

  /**
   * Returns the imported file path.
   */
  HYDRODATA_EXPORT QString GetFilePath() const;

  
  /**
   * Imports shape from IOR.
   * \param theIOR the IOR of Geom object
   * \return \c true if shape has been successfully imported
   */
  HYDRODATA_EXPORT virtual bool ImportFromGeomIOR( const TCollection_AsciiString& theIOR );

  /**
   * Stores the study entry of the imported GEOM object.
   * \param theEntry GEOM object entry
   */
  HYDRODATA_EXPORT void SetGeomObjectEntry( const TCollection_AsciiString& theEntry );

  /**
   * Returns the imported GEOM object entry.
   */
  HYDRODATA_EXPORT TCollection_AsciiString GetGeomObjectEntry() const;

  /**
   * Returns the imported GEOM object name.
   */
  HYDRODATA_EXPORT TCollection_AsciiString GetGeomObjectName() const;

  /**
   * Translate the obstacle to the given distance.
   */
  HYDRODATA_EXPORT void Translate( const double theDx,
                                   const double theDy,
                                   const double theDz );

protected:
  /**
   * Returns the type of child altitude object.
   * Reimplemented to create obstacle altitude object.
   */
  HYDRODATA_EXPORT virtual ObjectKind getAltitudeObjectType() const;

private:

  /**
   * Imports shape from the BREP file.
   * \param theFilePath the path to file
   * \return shape as TopoDS_Shape (null shape if import was failed)
   */
  TopoDS_Shape ImportBREP( const QString& theFilePath ) const;

  /**
   * Imports shape from the IGES file.
   * \param theFilePath the path to file
   * \return shape as TopoDS_Shape (null shape if import was failed)
   */
  TopoDS_Shape ImportIGES( const QString& theFilePath ) const;

  /**
   * Imports shape from the STEP file.
   * \param theFilePath the path to file
   * \return shape as TopoDS_Shape (null shape if import was failed)
   */
  TopoDS_Shape ImportSTEP( const QString& theFilePath ) const;

  /**
   * Create all necessary child group objects.
   */
  void createGroupObjects();

  /**
   * Sets the translation coefficients.
   */
  void setTranslation( const double theDx, const double theDy, const double theDz );

  /**
   * Returns the translation coefficients.
   */
  bool getTranslation( double& theDx, double& theDy, double& theDz ) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Obstacle();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_Obstacle();

};

#endif

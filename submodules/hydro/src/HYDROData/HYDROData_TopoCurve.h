// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDRODATA_TOPOCURVE_H
#define HYDRODATA_TOPOCURVE_H

#include <deque>
#include <Geom_BSplineCurve.hxx>
#include <HYDROData.h>
#include <list>
#include <TopExp.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Vertex.hxx>
#include <gp_XYZ.hxx>
#include <Adaptor3d_Curve.hxx>

class TopoDS_Wire;

//! Get the parameter of the projected point on the curve, and return the distance of the original point
double ProjectPointToCurve(const gp_XYZ& thePoint,
                           const Adaptor3d_Curve& theCurve,
                           double& theParameter);

//! The type represents a 1 manifold connected topo curve
//! with forward orientation.
class HYDROData_TopoCurve
{
public:
  //! The default computer.
  HYDROData_TopoCurve() {}

  //! Constructs the curve from the edge.
  HYDROData_TopoCurve(const TopoDS_Edge& theEdge) {myEdges.push_back(theEdge);}

  //! Initializes the curve by the wire.
  //! Returns 'false' if the wire is not 1 manifold or
  //! is disconnected or is empty.
  HYDRODATA_EXPORT bool Initialize(const TopoDS_Wire& theWire);

  //! Converts the curve to the wire.
  HYDRODATA_EXPORT TopoDS_Wire Wire() const;

  //! Returns 'true' if the curve has no edges.
  bool IsEmpty() const {return myEdges.empty();}

  //! Returns 'true' whether the curve is closed.
  bool IsClosed() const
  {
    return TopExp::LastVertex(myEdges.back(), Standard_True).
        IsSame(TopExp::FirstVertex(myEdges.front(), Standard_True)) ==
      Standard_True;
  }

  //! Returns the curve edges.
  const std::list<TopoDS_Edge>& Edges() const {return myEdges;}

  //! Returns the curve edges.
  std::list<TopoDS_Edge>& Edges() {return myEdges;}

  //! Cuts the curve in the given parameter of the given edge and
  //! fills the cut part.
  //! Returns 'true' if:
  //! -  the curve is open and was split into two parts or
  //! -  the curve is closed and was cut into an open curve.
  HYDRODATA_EXPORT bool Cut(
    const std::list<TopoDS_Edge>::iterator& theEdgePosition,
    const double theParameter,
    HYDROData_TopoCurve& theCurve);

  //! Cuts the curve in the given parameter of the given edge and
  //! fills the cut parts.
  HYDRODATA_EXPORT void Cut(
      const std::list<TopoDS_Edge>::const_iterator& theEdgePosition,
      const double theParameter,
      HYDROData_TopoCurve& theCurve1,
      HYDROData_TopoCurve& theCurve2) const;

  //! Cuts the curve at the parameters.
  //! Each parameter vector list corresponds to the curve edge and
  //! is ordered in the ascending order.
  HYDRODATA_EXPORT bool Cut(
    const std::deque<std::list<double> >& theParameters,
    std::deque<HYDROData_TopoCurve>& theCurves) const;

  //! Projects the point to the nearest point of the curve.
  //! Fills the iterator of the edge and
  //! the parameter on the edge for the projection.
  //! Returns the square distance from the point to the projection.
  HYDRODATA_EXPORT double Project(
    const gp_XYZ& thePoint,
    std::list<TopoDS_Edge>::const_iterator& theEdgeIterator,
    double& theParameter) const;

  //! Adds the intersection parameters of the curve with the wire to the vector.
  HYDRODATA_EXPORT int Intersect(
    const TopoDS_Wire& theWire,
    std::deque<std::list<double> >& theParameters) const;

  //! Replaces the first vertex by the last one.
  HYDRODATA_EXPORT void CloseCurve();

  //! Topologically extends the curve by the parameter at the position:
  //! 0 - start, 1 - end.
  HYDRODATA_EXPORT void Merge(
    const int thePosition, HYDROData_TopoCurve& theCurve);

  //! Topologically merges the curve to the curves.
  HYDRODATA_EXPORT void Merge(
    const double theTolerance, std::deque<HYDROData_TopoCurve>& theCurves);

  //! Topologically merges the wire curve to the curves.
  //! Returns 'false' if the wire is not a curve.
  static bool Merge(
    const double theTolerance,
    const TopoDS_Wire& theWire,
    std::deque<HYDROData_TopoCurve>& theCurves)
  {
    HYDROData_TopoCurve aCurve;
    const bool aResult = aCurve.Initialize(theWire);
    if (aResult)
    {
      aCurve.Merge(theTolerance, theCurves);
    }
    return aResult;
  }

  //! Topologically connects the curve to the first open one of the curves.
  //! If the gap between the curve and the first open curve is more than the
  //! tolerance then the gap is smoothly filled by a curve.
  //! It is assumed that only one of the parameter curves may be open.
  //! If the curve is closed then it is added to the curves.
  HYDRODATA_EXPORT bool Connect(
    const Standard_Real theTolerance,
    std::deque<HYDROData_TopoCurve>& theCurves);

  //! Works by the same way as the above method.
  static bool Connect(
    const Standard_Real theTolerance,
    const TopoDS_Wire& theWire,
    std::deque<HYDROData_TopoCurve>& theCurves)
  {
    HYDROData_TopoCurve aCurve;
    if (aCurve.Initialize(theWire))
    {
      return aCurve.Connect(theTolerance, theCurves);
    }

    return false;
  }

  //! Creates a B-spline piecewise curve corresponding to the curve
  //! and using the deflection.
  //! Returns the piece count.
  //! Returns 0 in the case of any error.
  HYDRODATA_EXPORT int BSplinePiecewiseCurve(
    const double theDeflection, HYDROData_TopoCurve& theCurve) const;

  //! Calculates the values of the curve in its knots.
  //! Returns 'false' if a curve edge has a non-identity location or a non-forward
  //! orientation or has no a B-spline representation.
  HYDRODATA_EXPORT bool ValuesInKnots(std::list<gp_XYZ>& theValues) const;

private:
  //! Transfers the edges of the parameter to this curve end.
  void append(HYDROData_TopoCurve& theCurve)
    {myEdges.splice(myEdges.end(), theCurve.myEdges);}

  //! Transfers the edges of the parameter to this curve start.
  void prepend(HYDROData_TopoCurve& theCurve)
    {myEdges.splice(myEdges.begin(), theCurve.myEdges);}

  std::list<TopoDS_Edge> myEdges; //!< The ordered curve edges.
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_PriorityQueue_HeaderFile
#define HYDROData_PriorityQueue_HeaderFile

#include <QMap>
#include <HYDROData_Entity.h>
#include <HYDROData_Zone.h>

class HYDROData_CalculationCase;

enum HYDROData_PriorityType
{
  LESS,
  GREATER,
};

struct HYDROData_CustomRule
{
  Handle(HYDROData_Entity)  Object1;
  HYDROData_PriorityType    Priority;
  Handle(HYDROData_Entity)  Object2;
  HYDROData_Zone::MergeType MergeType;
};

typedef QList<HYDROData_CustomRule> HYDROData_ListOfRules;

class HYDROData_PriorityQueue
{
public:
  HYDROData_PriorityQueue( HYDROData_CalculationCase* theCalcCase,
                           Standard_Integer aTag );
  ~HYDROData_PriorityQueue();

  Handle(HYDROData_Entity) GetMostPriorityObject( const QStringList& theZoneObjects,
                                                 HYDROData_Zone::MergeType& theMergeType ) const;

  bool IsMorePriority( const Handle(HYDROData_Entity)& theObj1,
                       const Handle(HYDROData_Entity)& theObj2,
                       HYDROData_Zone::MergeType& theMergeType ) const;

  static void ClearRules( TDF_Label& theRulesLabel );
  static void AddRule( TDF_Label& theRulesLabel,
                       const Handle(HYDROData_Entity)&    theObject1,
                       HYDROData_PriorityType             thePriority,
                       const Handle(HYDROData_Entity)&    theObject2,
                       HYDROData_Zone::MergeType theMergeType );
  static HYDROData_ListOfRules GetRules( const TDF_Label& theRulesLabel );
  static QString DumpRules( const TDF_Label& theRulesLab );
  static void DumpRulesToPython( const TDF_Label& theRulesLab,
                                 const QString& theCalcCaseName,
                                 QStringList& theScript );
  static bool GetRule( const TDF_Label& theRulesLab,
                       int theIndex, 
                       Handle(HYDROData_Entity)&           theObject1,
                       HYDROData_PriorityType&             thePriority,
                       Handle(HYDROData_Entity)&           theObject2,
                       HYDROData_Zone::MergeType& theMergeType );
private:
  typedef QMap<QString, Handle(HYDROData_Entity)> MapNameToObject;

  HYDROData_SequenceOfObjects myObjects;  ///< the ordered list of objects (default priority)
  MapNameToObject             myNames;    ///< the map of name to object
  HYDROData_ListOfRules       myRules;    ///< the list of rules
};

#endif


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_LandCover.h"

#include "HYDROData_PolylineXY.h"

#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <TDataStd_AsciiString.hxx>
#include <TNaming_Builder.hxx>
#include <TNaming_NamedShape.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <NCollection_IncAllocator.hxx>
#include <BOPAlgo_BOP.hxx>
#include <ShapeAnalysis_Wire.hxx>
#include <Precision.hxx>
#include <TopTools_SequenceOfShape.hxx>

#include <QColor>
#include <QStringList>

IMPLEMENT_STANDARD_RTTIEXT( HYDROData_LandCover, HYDROData_Entity )

HYDROData_LandCover::HYDROData_LandCover()
: HYDROData_Entity( Geom_2d )
{
}

HYDROData_LandCover::~HYDROData_LandCover()
{
}

const ObjectKind HYDROData_LandCover::GetKind() const
{
  return KIND_LAND_COVER;
}

QStringList HYDROData_LandCover::DumpToPython( MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aName = GetObjPyName();
  
  // Set Strickler type
  QString aType = GetStricklerType();
  if ( !aType.isEmpty() ) {
    aResList << QString( "" );
    ///< \TODO to be implemented:
    // aResList << QString( "%1.SetStricklerType( \"%2\" );" ).arg( aName ).arg( aType );
    aResList << QString( "" );
  }

  // Set polylines
  ///< \TODO to be implemented: 
  
  aResList << QString( "" );
  aResList << QString( "%1.Update();" ).arg( aName );
  aResList << QString( "" );

  return aResList;
}

HYDROData_SequenceOfObjects HYDROData_LandCover::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Entity::GetAllReferenceObjects();

  HYDROData_SequenceOfObjects aSeqOfPolylines = GetPolylines();
  aResSeq.Append( aSeqOfPolylines );

  return aResSeq;
}

bool HYDROData_LandCover::IsHas2dPrs() const
{
  return true;
}

void HYDROData_LandCover::SetStricklerType( const QString& theType )
{
  TCollection_AsciiString anAsciiStr( theType.toStdString().c_str() );
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_StricklerType ), anAsciiStr );
  anAttr->SetID(TDataStd_AsciiString::GetID());

}

QString HYDROData_LandCover::GetStricklerType() const
{
  QString aType;

  TDF_Label aLabel = myLab.FindChild( DataTag_StricklerType, false );
  if ( !aLabel.IsNull() ) {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) ) {
      aType = QString( anAsciiStr->Get().ToCString() );
    }
  }

  return aType;
}

void HYDROData_LandCover::Update()
{
  HYDROData_Entity::Update();
  
  removeShape();

  TCollection_AsciiString anErrorMsg;
  TopoDS_Shape aResShape = buildShape( GetPolylines(), anErrorMsg );
  
  setShape( aResShape );
}

void HYDROData_LandCover::SetPolylines( const HYDROData_SequenceOfObjects& thePolylines )
{
  SetReferenceObjects( thePolylines, DataTag_Polylines );
  Changed( Geom_2d );
}

HYDROData_SequenceOfObjects HYDROData_LandCover::GetPolylines() const
{
  return GetReferenceObjects( DataTag_Polylines );
}

TopoDS_Shape HYDROData_LandCover::GetShape() const
{
  TopoDS_Shape aShape;

  TDF_Label aLabel = myLab.FindChild( DataTag_Shape, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TNaming_NamedShape) aNamedShape;
    if( aLabel.FindAttribute( TNaming_NamedShape::GetID(), aNamedShape ) ) {
      aShape = aNamedShape->Get();
    }
  }

  return aShape;
}

void HYDROData_LandCover::SetFillingColor( const QColor& theColor )
{
  SetColor( theColor, DataTag_FillingColor );
}

QColor HYDROData_LandCover::GetFillingColor() const
{
  return GetColor( DefaultFillingColor(), DataTag_FillingColor );
}

void HYDROData_LandCover::SetBorderColor( const QColor& theColor )
{
  SetColor( theColor, DataTag_BorderColor );
}

QColor HYDROData_LandCover::GetBorderColor() const
{
  return GetColor( DefaultBorderColor(), DataTag_BorderColor );
}

QColor HYDROData_LandCover::DefaultFillingColor()
{
  return QColor( Qt::magenta );
}

QColor HYDROData_LandCover::DefaultBorderColor()
{
  return QColor( Qt::transparent );
}

void HYDROData_LandCover::setShape( const TopoDS_Shape& theShape )
{
  TNaming_Builder aBuilder( myLab.FindChild( DataTag_Shape ) );
  aBuilder.Generated( theShape );
}

void HYDROData_LandCover::removeShape()
{
  TDF_Label aLabel = myLab.FindChild( DataTag_Shape, false );
  if ( !aLabel.IsNull() ) {
    aLabel.ForgetAllAttributes();
  }
}

TopoDS_Shape HYDROData_LandCover::buildShape( const HYDROData_SequenceOfObjects& thePolylines,
                                              TCollection_AsciiString& theErrorMsg )
{
  theErrorMsg.Clear();
  TopoDS_Shape aResShape;

  BRepBuilderAPI_MakeWire aMakeWire;
  
  TopTools_ListOfShape aClosedWires;

  int aNbPolylines = thePolylines.Length();
  for ( int i = 1; i <= aNbPolylines; ++i ) {
    Handle(HYDROData_PolylineXY) aPolyline = 
      Handle(HYDROData_PolylineXY)::DownCast( thePolylines.Value( i ) );
    
    if ( aPolyline.IsNull() ) {
      continue;
    }

    TopoDS_Shape aPolyShape = aPolyline->GetShape();
    if ( aPolyShape.IsNull() ) {
      continue;
    }

    // Extract polyline wire(s)
    TopTools_ListOfShape aPolylineWires;
      
    if ( aPolyShape.ShapeType() == TopAbs_WIRE ) {
      const TopoDS_Wire& aPolylineWire = TopoDS::Wire( aPolyShape );
      if ( !aPolylineWire.IsNull() ) {
        aPolylineWires.Append( aPolylineWire );
      }
    } else if ( aPolyShape.ShapeType() == TopAbs_COMPOUND ) {
      TopExp_Explorer anExp( aPolyShape, TopAbs_WIRE );
      for (; anExp.More(); anExp.Next() ) {
        if(!anExp.Current().IsNull()) {
          const TopoDS_Wire& aWire = TopoDS::Wire( anExp.Current() );
          aPolylineWires.Append( aWire );
        }
      }
    }
    
    TopTools_ListIteratorOfListOfShape anIt( aPolylineWires );
    for ( ; anIt.More(); anIt.Next() ) {
      TopoDS_Wire& aWire = TopoDS::Wire( anIt.Value() );
      
      if ( aWire.Closed() ) {
        aClosedWires.Append( aWire );
      } else {
        aMakeWire.Add( aWire );
        aMakeWire.Build();
        if ( aMakeWire.IsDone() ) {
          if ( aMakeWire.Wire().Closed() ) {
            aClosedWires.Append( aMakeWire.Wire() );
            aMakeWire = BRepBuilderAPI_MakeWire();
          }
        }
      }
    }
  }

  if ( aClosedWires.Extent() == 1 ) {
    // make face
    TopoDS_Wire aW = TopoDS::Wire( aClosedWires.First());
    BRepBuilderAPI_MakeFace aMakeFace( aW );
    aMakeFace.Build();
    if( aMakeFace.IsDone() ) 
    {
      Handle(ShapeAnalysis_Wire) aSAW = new ShapeAnalysis_Wire(aW, aMakeFace.Face(), Precision::Confusion());
      if (!aSAW->CheckSelfIntersection())
        aResShape = aMakeFace.Face();
      else
        theErrorMsg = "Can't create landcover on the given polyline\nSelf-intersection of wire have been detected";
    }
  } else if ( aClosedWires.Extent() > 1 ) {
    // make compound
    BRep_Builder aBuilder;
    TopoDS_Compound aCompound;
    aBuilder.MakeCompound( aCompound );
    TopTools_SequenceOfShape aSeq;
    TopTools_ListIteratorOfListOfShape aWiresIter( aClosedWires );
    bool anErrStat = false;
    for ( ; aWiresIter.More() && !anErrStat; aWiresIter.Next() )
    {
      TopoDS_Wire aW = TopoDS::Wire( aWiresIter.Value() );
      BRepBuilderAPI_MakeFace aMakeFace( aW );
      aMakeFace.Build();
      if( aMakeFace.IsDone() ) 
      {
        Handle(ShapeAnalysis_Wire) aSAW = new ShapeAnalysis_Wire(aW, aMakeFace.Face(), Precision::Confusion());
        if (!aSAW->CheckSelfIntersection())
          aSeq.Append( aMakeFace.Face() );
        else
        {
          anErrStat = true;
          theErrorMsg = "Can't create landcover on the given polyline\nSelf-intersection of wire(s) have been detected";
        }
      }
    }
    if (!anErrStat)
    {
      for (int i = 1; i <= aSeq.Length(); i++)
        aBuilder.Add( aCompound, aSeq(i) );
      aResShape = aCompound;
    }
    else
      aResShape = TopoDS_Shape();
  } else if ( aNbPolylines > 0 ) {
    TCollection_AsciiString aSourceName = aNbPolylines > 1 ? "polylines" : "polyline";
    theErrorMsg = "Can't build closed contour on the given ";
    theErrorMsg += aSourceName;
  }

  ///< \TODO to be reimplemented
  /*
  TopoDS_Shape anArgShape;
  TopTools_ListOfShape aToolShapes;
  
  HYDROData_SequenceOfObjects aRefPolylines = GetPolylines();
  for ( int i = 1, n = aRefPolylines.Length(); i <= n; ++i ) {
    Handle(HYDROData_PolylineXY) aPolyline = 
      Handle(HYDROData_PolylineXY)::DownCast( aRefPolylines.Value( i ) );
    
    if ( aPolyline.IsNull() ) {
      continue;
    }

    if ( !aPolyline->IsClosed() ) {
      continue;
    }

    TopoDS_Shape aPolyShape = aPolyline->GetShape();
    if ( aPolyShape.IsNull() || aPolyShape.ShapeType() != TopAbs_WIRE ) {
      continue;
    }

    const TopoDS_Wire& aPolylineWire = TopoDS::Wire( aPolyShape );
    if ( aPolylineWire.IsNull() ) {
      continue;
    }

    TopoDS_Face aResultFace = TopoDS_Face();
    BRepBuilderAPI_MakeFace aMakeFace( aPolylineWire, Standard_True );
    aMakeFace.Build();
    if( aMakeFace.IsDone() ) {
      aResultFace = aMakeFace.Face();
    }

    if( aResultFace.IsNull() ) {
      continue;
    }

    if ( anArgShape.IsNull() ) {
      anArgShape = aResultFace;
    } else {
      aToolShapes.Append( aResultFace );
    }
  }

  aResShape = anArgShape;

  if ( !anArgShape.IsNull() && aToolShapes.Extent() > 0 ) {
    Handle(NCollection_BaseAllocator)aAL=new NCollection_IncAllocator;
    BOPAlgo_BOP aBOP(aAL);
 
    aBOP.AddArgument( anArgShape );

    TopTools_ListIteratorOfListOfShape anIt(aToolShapes);
    for( ; anIt.More(); anIt.Next() ) {
      aBOP.AddTool( anIt.Value() );
    }

    aBOP.SetOperation( BOPAlgo_CUT );
    aBOP.Perform();

    if ( !aBOP.Shape().IsNull() ) {
      aResShape = aBOP.Shape();
    }
  }
  */
  
  return aResShape;
}
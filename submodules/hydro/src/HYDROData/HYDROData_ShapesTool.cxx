// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_ShapesTool.h"

#include <BRep_Tool.hxx>

#include <gp_Pnt.hxx>

#include <Precision.hxx>

#include <TopExp.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Wire.hxx>

#include <TopTools_SequenceOfShape.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>

#include <TopExp_Explorer.hxx>

void HYDROData_ShapesTool::ExploreShapeToShapes( const TopoDS_Shape&       theInShape,
                                                 const TopAbs_ShapeEnum&   theExpType,
                                                 TopTools_SequenceOfShape& theOutShapes )
{
  theOutShapes.Clear();

  if ( theInShape.IsNull() )
    return;

  TopExp_Explorer anExp( theInShape, theExpType );
  for ( ; anExp.More(); anExp.Next() )
  {
    TopoDS_Shape anExpShape = anExp.Current();
    theOutShapes.Append( anExpShape );
  }
}

bool HYDROData_ShapesTool::Vertices( const TopoDS_Shape& theInShape,
                                     TopoDS_Vertex&      theFirstVertex,
                                     TopoDS_Vertex&      theLastVertex,
                                     const bool          theIsCumOri  )
{
  if ( theInShape.IsNull() )
    return false;

  if ( theInShape.ShapeType() == TopAbs_EDGE )
  {
    TopoDS_Edge anEdge = TopoDS::Edge( theInShape );
    TopExp::Vertices( anEdge, theFirstVertex, theLastVertex, theIsCumOri );
  }
  else if ( theInShape.ShapeType() == TopAbs_WIRE )
  {
    TopoDS_Wire aWire = TopoDS::Wire( theInShape );
    TopExp::Vertices( aWire, theFirstVertex, theLastVertex );
  }
  else
  {
    return false;
  }

  return true;
}

bool HYDROData_ShapesTool::IsVerticesEquals( const TopoDS_Vertex& theFirstVert,
                                             const TopoDS_Vertex& theSecondVert,
                                             const bool           theIs2D )
{
  gp_Pnt aFirstPoint = BRep_Tool::Pnt( theFirstVert );
  gp_Pnt aSecondPoint = BRep_Tool::Pnt( theSecondVert );

  // Set the Z coord to zero for 2D equations
  if ( theIs2D )
  {
    aFirstPoint.SetZ( 0.0 );
    aSecondPoint.SetZ( 0.0 );
  }

  return aFirstPoint.IsEqual( aSecondPoint, Precision::Confusion() )==Standard_True;
}

TopoDS_Shape HYDROData_ShapesTool::Translated( const TopoDS_Shape& theShape,
                                               const double        theDx,
                                               const double        theDy,
                                               const double        theDz )
{
  TopoDS_Shape aTranslatedShape;
  if ( theShape.IsNull() )
    return aTranslatedShape;

  gp_Vec aVec( theDx, theDy, theDz );

  gp_Trsf aTrsf;
  aTrsf.SetTranslation( aVec );

  TopLoc_Location aLocOrig = theShape.Location();
  gp_Trsf aTrsfOrig = aLocOrig.Transformation();

  TopLoc_Location aLocRes( aTrsf * aTrsfOrig );
  aTranslatedShape = theShape.Located( aLocRes );

  return aTranslatedShape;
}

bool HYDROData_ShapesTool::IsEdgesEquals( const TopoDS_Edge& theFirstEdge,
                                          const TopoDS_Edge& theSecondEdge,
                                          const bool         theIs2D )
{
  TopoDS_Vertex aFFirstVert, aFLastVert, aSFirstVert, aSLastVert;
  TopExp::Vertices( theFirstEdge, aFFirstVert, aFLastVert );
  TopExp::Vertices( theSecondEdge, aSFirstVert, aSLastVert );
  return IsVerticesEquals( aFFirstVert, aSFirstVert, theIs2D ) &&
         IsVerticesEquals( aFLastVert, aSLastVert, theIs2D );
}

void HYDROData_ShapesTool::AddShapes( TopTools_SequenceOfShape&       theShapes,
                                      const TopTools_SequenceOfShape& theShapesToAdd )
{
  for ( int i = 1, n = theShapesToAdd.Length(); i <= n; ++i )
  {
    const TopoDS_Shape& aShape = theShapesToAdd.Value( i );
    theShapes.Append( aShape );
  }
}

void HYDROData_ShapesTool::AddShapes( TopTools_SequenceOfShape&   theShapes,
                                      const TopTools_ListOfShape& theShapesToAdd )
{
  TopTools_ListIteratorOfListOfShape anIter( theShapesToAdd );
  for ( ; anIter.More(); anIter.Next() )
  {
    const TopoDS_Shape& aShape = anIter.Value();
    theShapes.Append( aShape );
  }
}

void HYDROData_ShapesTool::AddShapes( TopTools_ListOfShape&           theShapes,
                                      const TopTools_SequenceOfShape& theShapesToAdd )
{
  for ( int i = 1, n = theShapesToAdd.Length(); i <= n; ++i )
  {
    const TopoDS_Shape& aShape = theShapesToAdd.Value( i );
    theShapes.Append( aShape );
  }
}

void HYDROData_ShapesTool::AddShapes( TopTools_ListOfShape&       theShapes,
                                      const TopTools_ListOfShape& theShapesToAdd )
{
  TopTools_ListIteratorOfListOfShape anIter( theShapesToAdd );
  for ( ; anIter.More(); anIter.Next() )
  {
    const TopoDS_Shape& aShape = anIter.Value();
    theShapes.Append( aShape );
  }
}

void HYDROData_ShapesTool::DumpShapeSubShapes( std::ostream&           theStream,
                                               const char*             theTitle,
                                               const TopoDS_Shape&     theShape,
                                               const TopAbs_ShapeEnum& theExpType )
{
  TopTools_SequenceOfShape aShapeSubShapes;
  ExploreShapeToShapes( theShape, theExpType, aShapeSubShapes );

  theStream << theTitle << "\n";
  DumpSequenceOfShapes( theStream, aShapeSubShapes );
}

void HYDROData_ShapesTool::DumpSequenceOfShapes( std::ostream&                   theStream,
                                                 const TopTools_SequenceOfShape& theShapes )
{
  for ( int i = 1, n = theShapes.Length(); i <= n; ++i )
  {
    const TopoDS_Shape& aShape = theShapes.Value( i );
    theStream << "Shape " << i << " : " << aShape.TShape().get() << "\n";
  }
}

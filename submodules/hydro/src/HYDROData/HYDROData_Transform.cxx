// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_Transform.h>

#include <math.h>

#include <Bnd_Box.hxx>
#include <gp_Vec.hxx>

#include <TopoDS_Shape.hxx>
#include <TopoDS_Iterator.hxx>

#include <BRepBndLib.hxx>

#include <TopTools_ListIteratorOfListOfShape.hxx>

#include <TopTools_ListOfShape.hxx>
#include <TopTools_MapOfShape.hxx>

#include <TopTools_ListOfShape.hxx>

#include <BOPAlgo_BuilderShape.hxx>
#include <BOPAlgo_PaveFiller.hxx>

//#define DEB_SPLIT_TO_ZONES_CHECK_PARTITION 1
#ifdef DEB_SPLIT_TO_ZONES_CHECK_PARTITION
#include <BRepTools.hxx>
#include <TCollection_AsciiString.hxx>
static TCollection_AsciiString fileNameAfter("AfterTranslation");
#endif

void HYDROData_Transform::SetFileName(const TCollection_AsciiString& theNameAfter)
{
#ifdef DEB_SPLIT_TO_ZONES_CHECK_PARTITION
  fileNameAfter = theNameAfter;
#endif
}
//=======================================================================
//function : HYDROData_Transform
//purpose  :
//=======================================================================
HYDROData_Transform::HYDROData_Transform()
:
  BOPAlgo_BuilderShape()
{
  myToTransform=Standard_False;
  myTreshold=10000.;
  myTransform1=NULL;
  myTransform2=NULL;
  myBuilder=NULL;
  myErrorStatus=0;
  myWarningStatus=0;
}
//=======================================================================
//function : ~
//purpose  :
//=======================================================================
HYDROData_Transform::~HYDROData_Transform()
{
  Clear();
}
//=======================================================================
//function : Clear
//purpose  :
//=======================================================================
void HYDROData_Transform::Clear()
{
  myTreshold=10000.;
  //
  if (myTransform1) {
    delete myTransform1;
    myTransform1=NULL;
  }
  if (myTransform2) {
    delete myTransform2;
    myTransform2=NULL;
  }
  if (myBuilder) {
    delete myBuilder;
    myBuilder=NULL;
  }
  //
  //BOPAlgo_BuilderShape::PrepareHistory();
}
//=======================================================================
//function : SetArgument
//purpose  :
//=======================================================================
void HYDROData_Transform::SetArgument(const TopoDS_Shape& theShape)
{
  myArgument=theShape;
}
//=======================================================================
//function : AddArgument
//purpose  :
//=======================================================================
const TopoDS_Shape& HYDROData_Transform::Argument()const
{
  return myArgument;
}
//=======================================================================
//function : SetTreshold
//purpose  :
//=======================================================================
void HYDROData_Transform::SetTreshold(const Standard_Real theTreshold)
{
  myTreshold=theTreshold;
}
//=======================================================================
//function : Treshold
//purpose  :
//=======================================================================
Standard_Real HYDROData_Transform::Treshold()const
{
  return myTreshold;
}
//=======================================================================
//function : SetToTransform
//purpose  :
//=======================================================================
void HYDROData_Transform::SetToTransform(const Standard_Boolean theFlag)
{
  myToTransform=theFlag;
}
//=======================================================================
//function : ToTransform
//purpose  :
//=======================================================================
Standard_Boolean HYDROData_Transform::ToTransform()const
{
  return myToTransform;
}
//=======================================================================
//function : CheckData
//purpose  :
//=======================================================================
void HYDROData_Transform::CheckData()
{
  if(myArgument.IsNull()) {
    myErrorStatus=10;
  }
}
//=======================================================================
//function : Prepare
//purpose  :
//=======================================================================
void HYDROData_Transform::Prepare()
{
  TopoDS_Shape aSx;
  //
  myErrorStatus=0;
  myWarningStatus=0;
  myShape=aSx;
}
//=======================================================================
//function : Perform
//purpose  :
//=======================================================================
void HYDROData_Transform::Perform()
{
  Prepare();
  //
  CheckData();
  if(myErrorStatus) {
    return;
  }
  //
  Clear();
  if(myErrorStatus) {
    return;
  }
  //
  ComputeTrsf();
  if(myErrorStatus) {
    return;
  }
  //
  Transform1();
  if(myErrorStatus) {
    return;
  }
  //
  BuildFuse();
  if(myErrorStatus) {
    return;
  }
  //
  Transform2();
}
//=======================================================================
//function : Detect
//purpose  :
//=======================================================================
void HYDROData_Transform::Detect()
{
  Standard_Real aXmin, aYmin, aZmin, aXmax, aYmax, aZmax;
  Standard_Real aXc, aYc, aZc;
  Bnd_Box aBox;
  gp_Vec aVec;
  //
  myErrorStatus=0;
  //
  BRepBndLib::Add(myArgument, aBox);
  //
  aBox.Get(aXmin, aYmin, aZmin, aXmax, aYmax, aZmax);
  //
  aXc=0.5*(aXmin+aXmax);
  aYc=0.5*(aYmin+aYmax);
  aZc=0.5*(aZmin+aZmax);
  //printf(" aVc : { %lf %lf %lf }\n", aXc, aYc, aZc);
  //
  myToTransform=Standard_False;
  if (fabs(aXc)>myTreshold ||
      fabs(aYc)>myTreshold ||
      fabs(aZc)>myTreshold) {
    myToTransform=!myToTransform;
  }
  //
  aVec.SetCoord(-aXc, -aYc, -aZc);
  myTrsf1.SetTranslation(aVec);
  //
  aVec.SetCoord(aXc, aYc, aZc);
  myTrsf2.SetTranslation(aVec);
}
//=======================================================================
//function : ComputeTrsf
//purpose  :
//=======================================================================
void HYDROData_Transform::ComputeTrsf()
{
  if (!myToTransform) {
    gp_Trsf aTrsf;
    //
    myTrsf1=aTrsf;
    myTrsf2=aTrsf;
  }
}
//=======================================================================
//function : Transform1
//purpose  :
//=======================================================================
void HYDROData_Transform::Transform1()
{
  myErrorStatus=0;
  //
  if (myToTransform) {//pkv ft
    Standard_Boolean bIsDone;
    //
    myErrorStatus=0;
    //
    myTransform1=new BRepBuilderAPI_Transform(myTrsf1);
    //
    myTransform1->Perform(myArgument);
    bIsDone=myTransform1->IsDone();
    if (!bIsDone) {
      myErrorStatus=11;
    }
  }
}
//=======================================================================
//function : BuildFuse
//purpose  :
//=======================================================================
void HYDROData_Transform::BuildFuse()
{
  Standard_Integer iErr;
  TopoDS_Iterator aIt;
  TopTools_ListOfShape aLC;
  BOPAlgo_PaveFiller aPF;
  //
  myErrorStatus=0;
  //
   //modified by NIZNHY-PKV Wed Jan 15 13:24:48 2014f
  const TopoDS_Shape& aS1=
    (myToTransform)? myTransform1->Shape() : myArgument;
  //const TopoDS_Shape& aS1=()Transform1->Shape();
  //modified by NIZNHY-PKV Wed Jan 15 13:24:52 2014t

#ifdef DEB_SPLIT_TO_ZONES_CHECK_PARTITION
  TCollection_AsciiString aNameBefore = fileNameAfter + ".brep";
  BRepTools::Write(aS1, aNameBefore.ToCString());
#endif
  //
  aIt.Initialize(aS1);
  for (; aIt.More(); aIt.Next()) {
    const TopoDS_Shape& aS1x=aIt.Value();
    aLC.Append(aS1x);
  }
  //
  aPF.SetArguments(aLC);
  //
  aPF.Perform();
  if (aPF.HasErrors())
  {
    myErrorStatus=20;
    return;
  }
  //
  myBuilder=new BOPAlgo_Builder;
  //
  aIt.Initialize(aS1);
  for (; aIt.More(); aIt.Next()) {
    const TopoDS_Shape& aS1x=aIt.Value();
    myBuilder->AddArgument(aS1x);
  }
  //
  myBuilder->PerformWithFiller(aPF);
  if (myBuilder->HasErrors())
  {
    myErrorStatus=21;
    return;
  }
}
//=======================================================================
//function : Transform2
//purpose  :
//=======================================================================
void HYDROData_Transform::Transform2()
{
  myErrorStatus=0;
  //
  const TopoDS_Shape& aR1=myBuilder->Shape();
  //
  myShape=aR1; //pkv ft
  if (myToTransform) { //pkv ft
    Standard_Boolean bIsDone;
    //
    myTransform2=new BRepBuilderAPI_Transform(myTrsf2);
    //
    myTransform2->Perform(aR1);
    bIsDone=myTransform2->IsDone();
    if (!bIsDone) {
      myErrorStatus=12;
      return;
    }
    //
    const TopoDS_Shape& aR2=myTransform2->Shape();
    //
    myShape=aR2;
  }
}
//
//=======================================================================
//function : Modified
//purpose  :
//=======================================================================
const TopTools_ListOfShape& HYDROData_Transform::Modified
  (const TopoDS_Shape& aS)
{
  TopTools_ListOfShape* pHistShapes;
  TopTools_ListIteratorOfListOfShape aItLS;
  //
  pHistShapes=(TopTools_ListOfShape*)&myHistShapes;
  pHistShapes->Clear();
  //
  if (myToTransform) { //pkv ft
    const TopTools_ListOfShape& aLS1=myTransform1->Modified(aS);
    const TopoDS_Shape& aS1=aLS1.First();
    //
    const TopTools_ListOfShape& aLS1B=myBuilder->Modified(aS1);
    //
    if (aLS1B.IsEmpty()) { //pkv ft
      const TopTools_ListOfShape& aLS2B=myTransform2->Modified(aS1);
      const TopoDS_Shape& aS2B=aLS2B.First();
      pHistShapes->Append(aS2B);
    }
    else {
      aItLS.Initialize(aLS1B);
      for (; aItLS.More(); aItLS.Next()) {
	const TopoDS_Shape& aS1B=aItLS.Value();
	const TopTools_ListOfShape& aLS2B=myTransform2->Modified(aS1B);
	const TopoDS_Shape& aS2B=aLS2B.First();
	pHistShapes->Append(aS2B);
      }
    }
  }
  else {
    const TopTools_ListOfShape& aLS1B=myBuilder->Modified(aS);
    aItLS.Initialize(aLS1B);
    for (; aItLS.More(); aItLS.Next()) {
      const TopoDS_Shape& aS1B=aItLS.Value();
      pHistShapes->Append(aS1B);
    }
  }
  //
  return myHistShapes;
}
//=======================================================================
//function : Generated
//purpose  :
//=======================================================================
const TopTools_ListOfShape& HYDROData_Transform::Generated
  (const TopoDS_Shape& aS)
{
  TopTools_ListOfShape* pHistShapes;
  TopTools_ListIteratorOfListOfShape aItLS;
  //
  pHistShapes=(TopTools_ListOfShape*)&myHistShapes;
  pHistShapes->Clear();
  //
  if (myToTransform) { //pkv ft
    const TopTools_ListOfShape& aLS1=myTransform1->Modified(aS);
    const TopoDS_Shape& aS1=aLS1.First();
    //
    const TopTools_ListOfShape& aLS1B=myBuilder->Generated(aS1);
    if (aLS1B.IsEmpty()) { //pkv ft
      const TopTools_ListOfShape& aLS2B=myTransform2->Modified(aS1);
      const TopoDS_Shape& aS2B=aLS2B.First();
      pHistShapes->Append(aS2B);
    }
    else {
      aItLS.Initialize(aLS1B);
      for (; aItLS.More(); aItLS.Next()) {
	const TopoDS_Shape& aS1B=aItLS.Value();
	const TopTools_ListOfShape& aLS2B=myTransform2->Modified(aS1B);
	const TopoDS_Shape& aS2B=aLS2B.First();
	pHistShapes->Append(aS2B);
      }
    }
  }
  else {
    const TopTools_ListOfShape& aLS1B=myBuilder->Generated(aS);
    aItLS.Initialize(aLS1B);
    for (; aItLS.More(); aItLS.Next()) {
      const TopoDS_Shape& aS1B=aItLS.Value();
      pHistShapes->Append(aS1B);
    }
  }
  //
  return myHistShapes;
}
//=======================================================================
//function : IsDeleted
//purpose  :
//=======================================================================
Standard_Boolean HYDROData_Transform::IsDeleted(const TopoDS_Shape& aS)
{
  Standard_Boolean bIsDeleted;
  //
  if (myToTransform) { //pkv ft
    const TopTools_ListOfShape& aLS1=myTransform1->Modified(aS);
    const TopoDS_Shape& aS1=aLS1.First();
    //
    bIsDeleted=myBuilder->IsDeleted(aS1);
  }
  else {
    bIsDeleted=myBuilder->IsDeleted(aS);
  }
  //
  return bIsDeleted;
}
//=======================================================================
//function : HasDeleted
//purpose  :
//=======================================================================
Standard_Boolean HYDROData_Transform::HasDeleted()
{
  return myBuilder->HasDeleted();
}
//=======================================================================
//function : HasGenerated
//purpose  :
//=======================================================================
Standard_Boolean HYDROData_Transform::HasGenerated()
{
  return myBuilder->HasGenerated();
}
//=======================================================================
//function : HasModified
//purpose  :
//=======================================================================
Standard_Boolean HYDROData_Transform::HasModified()
{
  return myBuilder->HasModified();
}

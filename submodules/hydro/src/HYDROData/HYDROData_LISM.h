// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_LISM_HeaderFile
#define HYDROData_LISM_HeaderFile

#include "HYDROData_Bathymetry.h"
#include "HYDROData_Profile.h"

#include <HYDROData_PolylineXY.h>
#include <HYDROData_Entity.h>
#include <HYDROData_Stream.h>

/**\class HYDROData_LISM
 * \
 */
class HYDROData_LISM : public HYDROData_Bathymetry
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Bathymetry::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Profiles,
    DataTag_HaxStep,
    DataTag_NbProfilePoints,
    DataTag_HydraulicAxis,    
    DataTag_LeftBank,
    DataTag_RightBank,
    DataTag_LeftBankShape,
    DataTag_RightBankShape,
    DataTag_InletShape,
    DataTag_OutletShape,
    DataTag_3DShape,
    DataTag_2DShape 
  };

public:

  HYDRODATA_EXPORT HYDROData_LISM();
  virtual HYDRODATA_EXPORT ~HYDROData_LISM();

  DEFINE_STANDARD_RTTIEXT( HYDROData_LISM, HYDROData_Bathymetry );

  HYDRODATA_EXPORT HYDROData_SequenceOfObjects GetProfiles() const;
  HYDRODATA_EXPORT void SetProfiles( const HYDROData_SequenceOfObjects& );

  HYDRODATA_EXPORT bool SetLeftBank( const Handle(HYDROData_PolylineXY)& theBank );
  HYDRODATA_EXPORT Handle(HYDROData_PolylineXY) GetLeftBank() const;

  HYDRODATA_EXPORT bool SetRightBank( const Handle(HYDROData_PolylineXY)& theBank );
  HYDRODATA_EXPORT Handle(HYDROData_PolylineXY) GetRightBank() const;
                   
  HYDRODATA_EXPORT bool SetHydraulicAxis( const Handle(HYDROData_PolylineXY)& theAxis );
  HYDRODATA_EXPORT Handle(HYDROData_PolylineXY) GetHydraulicAxis() const;

  HYDRODATA_EXPORT int GetNbProfilePoints() const;
  HYDRODATA_EXPORT void SetNbProfilePoints( int );

  HYDRODATA_EXPORT double GetHaxStep() const;
  HYDRODATA_EXPORT void SetHaxStep( double );

  HYDRODATA_EXPORT virtual void Update();

  HYDRODATA_EXPORT void GetShapePresentations( HYDROData_Stream::PrsDefinition& prsDef);


};




#endif

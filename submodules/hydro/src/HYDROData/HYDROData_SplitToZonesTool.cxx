// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_SplitToZonesTool.h"

#include "HYDROData_PolylineXY.h"
#include "HYDROData_ShapesGroup.h"
#include <HYDROData_Transform.h>
#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Common.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRep_Builder.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Edge.hxx>
#include <BRepCheck_Analyzer.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <gp_Pln.hxx>
#include <BRepGProp.hxx>
#include <GProp_GProps.hxx>
#include <Geom_Plane.hxx>
#include <BRepBuilderAPI_FindPlane.hxx>

#include <BOPAlgo_BOP.hxx>
#include <BOPAlgo_Builder.hxx>
#include <TopExp.hxx>
#include <assert.h>

//#define DEB_SPLIT_TO_ZONES 1
//#define DEB_SPLIT_TO_ZONES_CHECK_PARTITION 1
#if (defined (DEB_SPLIT_TO_ZONES) || defined(DEB_SPLIT_TO_ZONES_CHECK_PARTITION))
#include <BRepTools.hxx>
static TCollection_AsciiString fileNameBefore("BeforeTranslation");
#endif

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

TopoDS_Face HYDROData_SplitToZonesTool::SplitData::Face() const
{
  TopoDS_Face aResFace;

  if( !Shape.IsNull() )
  {
    if ( Shape.ShapeType() == TopAbs_FACE )
    {
      aResFace = TopoDS::Face( Shape );
    }
    else if ( Shape.ShapeType() == TopAbs_WIRE )
    {
      BRepBuilderAPI_MakeFace aMakeFace( TopoDS::Wire( Shape ), Standard_True );
      aMakeFace.Build();
      if( aMakeFace.IsDone() )
        aResFace = aMakeFace.Face();
    }
  }

  return aResFace;
}

Standard_Boolean HYDROData_SplitToZonesTool::buildLimFace(const TopoDS_Wire& theBndWire, TopoDS_Face& outFace) 
{
  GProp_GProps G;
  BRepGProp::LinearProperties(theBndWire,G);
  const gp_Pnt& aCPnt = G.CentreOfMass();
  gp_Pln aPln;
  BRepBuilderAPI_FindPlane fndPlane (theBndWire, Precision::Confusion());  
  if(fndPlane.Found())
    aPln = fndPlane.Plane()->Pln();
  else
    aPln = gp_Pln(aCPnt, gp::OZ().Direction());
  BRepBuilderAPI_MakeFace  aMkFace(aPln, theBndWire);
  if(aMkFace.IsDone()) {
    outFace = aMkFace.Face();      
    if(!outFace.IsNull()) {
#ifdef DEB_SPLIT_TO_ZONES
//      BRepTools::Write(limFace,"FL.brep");
#endif
      return Standard_True;
    }
  }
  return Standard_False;
}
//======================================================================================

void HYDROData_SplitToZonesTool::SetFileNames(const QString& theNameBefore, const QString& theNameAfter)
{
#ifdef DEB_SPLIT_TO_ZONES_CHECK_PARTITION
  if(!theNameBefore.isNull() && !theNameBefore.isEmpty())
  {
    TCollection_AsciiString aName (theNameBefore.toStdString().data());
    fileNameBefore = aName;
  }
  if(!theNameAfter.isNull() && !theNameAfter.isEmpty())
  {
    TCollection_AsciiString aName (theNameAfter.toStdString().data());
    HYDROData_Transform::SetFileName (aName);
  }
#endif
}
//======================================================================================
bool HYDROData_SplitToZonesTool::SplitFaces(const TopoDS_Compound& theComp, 
                                                        HYDROData_Transform& theTool)
{
  theTool.Clear();
  theTool.SetArgument(theComp);
  theTool.Detect();
  const Standard_Boolean bToTransform = theTool.ToTransform();  
  theTool.SetToTransform(bToTransform);  
  theTool.Perform();
  return theTool.HasErrors();
}

//======================================================================================
HYDROData_SplitToZonesTool::SplitDataList
  HYDROData_SplitToZonesTool::Split( const HYDROData_SequenceOfObjects&  theObjectList,
                                     const HYDROData_SequenceOfObjects&  theGroupsList,
                                     const Handle(HYDROData_PolylineXY)& thePolyline,
                                     const HYDROData_SequenceOfObjects& InterPolys)
{
#ifndef NDEBUG
  DEBTRACE("Split");
#endif
  SplitDataList anOutputSplitDataList;
  NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher> OutNE;
  if(theObjectList.IsEmpty())
    return anOutputSplitDataList;
  // Preparation. 
  // Collect the object shapes to split. InputDataList will contain elements which will hold shape & name_of_shape.
  SplitDataList anInputSplitDataList;
#ifdef DEB_SPLIT_TO_ZONES
  TCollection_AsciiString aNam("Faces_");
#endif
  for( int anIndex = 1, aLength = theObjectList.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObj = theObjectList.Value( anIndex );
    TopoDS_Shape aShape;

    Handle(HYDROData_Object) aGeomObj = Handle(HYDROData_Object)::DownCast( anObj );
    if( !aGeomObj.IsNull() ) {
      aShape = aGeomObj->GetTopShape();
    }

    if ( aShape.IsNull() )
      continue;  
#ifdef DEB_SPLIT_TO_ZONES
   TCollection_AsciiString aName = aNam + anIndex + "_a.brep";
   BRepTools::Write(aShape, aName.ToCString());
#endif
    if ( aShape.ShapeType() == TopAbs_COMPOUND ) {
      // Create split data for each face contained in the compound
      TopExp_Explorer anExp( aShape, TopAbs_FACE );
      for ( ; anExp.More(); anExp.Next() ) {
        const TopoDS_Face& aFace = TopoDS::Face( anExp.Current() );
        if ( !aFace.IsNull() ) {
          SplitData aSplitData( SplitData::Data_Zone, aFace, anObj->GetName() );
          anInputSplitDataList.append( aSplitData );
        }
      }
    } else {
      SplitData aSplitData( SplitData::Data_Zone, aShape, anObj->GetName() );
      anInputSplitDataList.append( aSplitData );
    }
  }
  //
  SplitDataList anInputGroupList;
  for( int anIndex = 1, aLength = theGroupsList.Length(); anIndex <=aLength; anIndex++ )
  {
    Handle(HYDROData_ShapesGroup) aGeomGroup = 
      Handle(HYDROData_ShapesGroup)::DownCast( theGroupsList.Value( anIndex ) );
    if( aGeomGroup.IsNull() )
      continue;
      
    TopTools_SequenceOfShape aGroupShapes;
    aGeomGroup->GetShapes( aGroupShapes );
    for( int i = 1, aNbShapes = aGroupShapes.Length(); i <= aNbShapes; i++ ) {
      const TopoDS_Shape& aGroupShape = aGroupShapes.Value( i );
      if ( aGroupShape.IsNull() )
        continue;  

      if ( aGroupShape.ShapeType() == TopAbs_COMPOUND ) {    
        TopExp_Explorer anExp( aGroupShape, TopAbs_EDGE );
        for ( ; anExp.More(); anExp.Next() ) {
          const TopoDS_Edge& anEdge = TopoDS::Edge( anExp.Current() );
          if ( !anEdge.IsNull() ) {
            SplitData aSplitData( SplitData::Data_Edge, anEdge, aGeomGroup->GetName() );
            anInputGroupList.append( aSplitData );
          }
        }
      } else {
        SplitData aSplitData( SplitData::Data_Edge, aGroupShape, aGeomGroup->GetName() );
        anInputGroupList.append( aSplitData );
#ifdef DEB_SPLIT_TO_ZONES
        QString aStr = aSplitData.ObjectNames.join(" "); 
        DEBTRACE("Group# = " <<anIndex <<" Nmb in grp = " << i<< " Names = "<<aStr.toStdString() << " size = " <<aSplitData.ObjectNames.size());
#endif
      }
    }
  }
  // If only one shape is given we don't split it 
  // algorithm just returns the unpacked input data
  bool limplus1Object(false);
  if(theObjectList.Size() == 1 ) 
  {
    if(thePolyline.IsNull()) 
    {
      //anOutputSplitDataList.append(anInputSplitDataList); 
      SplitData SD = anInputSplitDataList.at(0);
      TopTools_ListOfShape newshs;
      TopTools_IndexedDataMapOfShapeListOfShape OutOrSh2M;
      CutFaceByEdges(SD.Face(), newshs, InterPolys, &OutNE, &OutOrSh2M);
      for (TopTools_ListIteratorOfListOfShape it(newshs); it.More(); it.Next())
      {
        SplitData NSD = SD;
        NSD.Shape = it.Value();
        anOutputSplitDataList.append(NSD);
      }

      NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher>::Iterator spit(OutNE);
      for (;spit.More();spit.Next())
      {
        const TopoDS_Edge& E = TopoDS::Edge(spit.Key());
        if (E.IsNull())
          continue;
        SplitData SDI(SplitData::Data_IntEdge, E, spit.Value()->GetName());   
        anOutputSplitDataList.append(SDI);
      }

#ifndef NDEBUG
      //check CutFaceByEdges method: ensure that SD.Face() edges fully covered by
      //history map (OutOrSh2M)
      if (!OutOrSh2M.IsEmpty())
      {
        TopTools_IndexedMapOfShape EE;
        TopExp::MapShapes(SD.Face(), TopAbs_EDGE, EE);
        int noncontNb = 0;      
        for (int i = 1; i <= EE.Extent(); i++)
        {
          const TopoDS_Shape& E = EE(i);
          noncontNb += !OutOrSh2M.Contains(E);
        }
        //noncontNb > 0 => some problem with edge history
        assert(noncontNb == 0);
      }
#endif

      if(!theGroupsList.IsEmpty() ) 
      {
        SplitDataList ModifInpGroupList;
        SplitDataListIterator it(anInputGroupList);
        while( it.hasNext() )
        {
          const SplitData& SData = it.next();
          if (SData.Type != SplitData::Data_Edge)
            ModifInpGroupList.append(SData); //add as is
          const TopoDS_Shape& SData_sh = SData.Shape;
          TopTools_ListOfShape modif_ls;
          if (!InterPolys.IsEmpty())
            if (OutOrSh2M.Contains(SData_sh))
              modif_ls = OutOrSh2M.FindFromKey(SData_sh);
            //else TODO -  show message that one of the object should be updated

#ifndef NDEBUG
          if (!InterPolys.IsEmpty() && OutOrSh2M.IsEmpty())
            assert (true);
#endif
          if (modif_ls.IsEmpty())
            ModifInpGroupList.append(SData); //non modified
          else
          {
            TopTools_ListIteratorOfListOfShape itl_modif(modif_ls);
            for (;itl_modif.More();itl_modif.Next())
            {
              const TopoDS_Shape& CSH = itl_modif.Value();
              if (CSH.ShapeType() == TopAbs_EDGE)
              {
                SplitData NewSData;
                NewSData.ObjectNames = SData.ObjectNames;
                NewSData.Type = SData.Type;
                NewSData.Shape = CSH;
                ModifInpGroupList.append(NewSData);
              }
            }
          }

        }
        anOutputSplitDataList.append(ModifInpGroupList);
      }
      return anOutputSplitDataList;
    }
    else
      limplus1Object = true;// size =1 && hasLimits
  }
  HYDROData_DataMapOfShapeListOfString aDM3;
  if(!anInputGroupList.isEmpty()) {// Old edge ==> List_Of_Names
    QStringList aListOfNames;
    for (int i=0;i < anInputGroupList.size() ;i++) {
      const TopoDS_Shape& aSh = anInputGroupList.at(i).Shape;
      aDM3.Add(aSh, anInputGroupList.at(i).ObjectNames);
    }
  }

  // Step 1. Prepare Partition structures. 
  TopoDS_Shape aResult;
  TopTools_ListOfShape aLS;  
  QStringList aListOfNames;
  TopoDS_Compound aCmp;
  BRep_Builder aBB;
  aBB.MakeCompound(aCmp);
  for (int i=0;i < anInputSplitDataList.size() ;i++) {
    const TopoDS_Shape& aSh = anInputSplitDataList.at(i).Shape;
    aDM3.Add(aSh, anInputSplitDataList.at(i).ObjectNames);
    aLS.Append(aSh);
    aBB.Add(aCmp,aSh);
#ifdef DEB_SPLIT_TO_ZONES
    TCollection_AsciiString aName = aNam + i + "_b.brep";
    BRepTools::Write(aSh, aName.ToCString());
#endif
  }
#ifdef DEB_SPLIT_TO_ZONES_CHECK_PARTITION
  TCollection_AsciiString aNameBefore = fileNameBefore + "_c.brep";
  BRepTools::Write(aCmp, aNameBefore.ToCString());
#endif

  HYDROData_DataMapOfShapeListOfShape aDM1;
  if(anInputSplitDataList.size() > 1) {
    HYDROData_Transform splitTool; 
    bool anErr = SplitFaces(aCmp, splitTool);    
    if(anErr)
      return anOutputSplitDataList;
    aResult = splitTool.Shape();
    if (aResult.IsNull()) 
      return anOutputSplitDataList;
    BRepCheck_Analyzer aCheck (aResult);
    if(!aCheck.IsValid()) {
  #ifdef DEB_SPLIT_TO_ZONES
      DEBTRACE("result is not valid");
      BRepTools::Write(aResult, "SplitFacesNV.brep");  
  #endif
      return anOutputSplitDataList;
    }
  #ifdef DEB_SPLIT_TO_ZONES
    BRepTools::Write(aResult, "SplitFacesV.brep");
  #endif
    
    // Step 3. Collect history  
    //HYDROData_DataMapOfShapeListOfShape aDM1;
    TopTools_ListIteratorOfListOfShape anIt(aLS); 
  #ifdef DEB_SPLIT_TO_ZONES
    TCollection_AsciiString aNamM ("EdgM_");
    TCollection_AsciiString aNamG ("EdgG_");
  #endif
    for (int i =1;anIt.More();anIt.Next(),i++) {
      Standard_Boolean foundF(Standard_False);
      const TopTools_ListOfShape& aListOfNew = splitTool.Modified(anIt.Value());
      if(!aListOfNew.IsEmpty())
        foundF = Standard_True;

      TopTools_ListOfShape aList;

      TopTools_ListIteratorOfListOfShape it(aListOfNew);
      for(;it.More();it.Next())       
        aList.Append(it.Value());
	  /* *********************************************************************
      // Bug in History: partition should give only modified entities! => temporary solution is used
      //const TopTools_ListOfShape& aListOfGen = splitTool.Generated(anIt.Value());
      //if(!aListOfGen.IsEmpty())
        //foundF = Standard_True;
      //it.Initialize(aListOfGen);    
      //for(;it.More();it.Next())     
       // aList.Append(it.Value());  
	   ********************************************************************* */
      if(!foundF) // face is not modified
        aList.Append (anIt.Value());
      aDM1.Add(anIt.Value(), aList);
  #ifdef DEB_SPLIT_TO_ZONES
      TCollection_AsciiString aName;
  #endif
      if(!anInputGroupList.isEmpty() ) { // 1
        TopExp_Explorer exp (anIt.Value(), TopAbs_EDGE);
        for (int j =1;exp.More();exp.Next(),j++) {
          aList.Clear();
          Standard_Boolean foundE(Standard_False);
          const TopTools_ListOfShape& aListM = splitTool.Modified(exp.Current()); 
#ifndef NDEBUG
          DEBTRACE("NB_EDGE_M = " << aListM.Extent());
#endif
          if(aListM.Extent()) foundE = Standard_True;
          it.Initialize(aListM);    
          for(int k=1;it.More();it.Next(),k++) {    
            aList.Append(it.Value());
  #ifdef DEB_SPLIT_TO_ZONES
            aName = aNamM + i + j +k +"_d.brep";
            BRepTools::Write(it.Value(),aName.ToCString());
  #endif
          }
          /* *********************************************************************
		  //const TopTools_ListOfShape& aListG = splitTool.Generated(exp.Current());
          //if(aListG.Extent()) foundE = Standard_True;
          //it.Initialize(aListG);    
          //for(int k=1;it.More();it.Next(),k++)
            //aList.Append(it.Value());
          //cout << "NB_EDGE = " << aList.Extent() <<endl;
		  ************************************************************************** */
          if(!foundE) {
            aList.Append (exp.Current());
  #ifdef DEB_SPLIT_TO_ZONES
            aName = aNamG + i + j +"_e.brep";
            BRepTools::Write(exp.Current(),aName.ToCString());
            DEBTRACE(aName.ToCString()<< " = " << exp.Current().TShape());
  #endif
          }
          aDM1.Add(exp.Current(), aList);
        }
      }
    }
  } else {
      aResult = anInputSplitDataList.at(0).Shape; // get single input shape
  }

  // aDM2: NewShape ==> ListOfOldShapes
  HYDROData_DataMapOfShapeListOfShape aDM2;
  // make limiting face
  HYDROData_DataMapOfShapeListOfShape aDM4;
  Standard_Boolean hasLimits(Standard_False);
  QString aBndName;
  HYDROData_MapOfShape aBndView;
  if (! thePolyline.IsNull()) {
    Handle(TopTools_HSequenceOfShape) aConnectedWires = new TopTools_HSequenceOfShape;
    int nbWires = thePolyline->GetNbConnectedWires(aConnectedWires);
    const TopoDS_Wire aBndWire = TopoDS::Wire(aConnectedWires->Value(1));
    if(!aBndWire.IsNull()) {
      TopoDS_Face limFace;       
      if(buildLimFace(aBndWire, limFace)) {
        TopoDS_Shape aComResult;
        BRepAlgoAPI_Common mkCom(aResult, limFace);
        if(mkCom.IsDone()) {
          aComResult = mkCom.Shape();
          BRepCheck_Analyzer aCheck (aComResult);
          if(aCheck.IsValid()) {
#ifdef DEB_SPLIT_TO_ZONES
            BRepTools::Write(aComResult,"CommonV.brep");
            BRepTools::Write(limFace,"limFace.brep");
#endif    
            aBndName = thePolyline->GetName();
            hasLimits = Standard_True; // DM2 should be filled here
            TopExp_Explorer exp (limFace, TopAbs_EDGE);
            for (int i =1;exp.More();exp.Next(),i++) {
              const TopoDS_Shape& anEdge = exp.Current();
              if(anEdge.IsNull()) continue;
              aBndView.Add(anEdge);
              QStringList aListOfNames;
              aListOfNames.append(aBndName);
              aDM3.Add(anEdge, aListOfNames);
              TopTools_ListOfShape aList;
              aList.Append(anEdge);
              aDM1.Add(anEdge,aList);
            }
            HYDROData_MapOfShape aView;
            exp.Init (aResult, TopAbs_FACE);
            for (int i =1;exp.More();exp.Next(),i++) {
              const TopoDS_Shape& aFace = exp.Current();
              if(!aFace.IsNull()) {
                const TopTools_ListOfShape& aListOfNew = mkCom.Modified(aFace);
#ifndef NDEBUG
                DEBTRACE("Modified: " << aListOfNew.Extent());
#endif
                if(!aListOfNew.IsEmpty()) {
                  aDM4.Add(aFace, aListOfNew);
#ifdef DEB_SPLIT_TO_ZONES
                  TCollection_AsciiString aName = aNam + i + "_f.brep";
                  BRepTools::Write(aListOfNew.Last(), aName.ToCString());
#endif
                }
                else {
                    if(!mkCom.IsDeleted(aFace)) {
                      const TopTools_ListOfShape& aListOfGen = mkCom.Generated(aFace);    
                      if(!aListOfGen.IsEmpty()) {
                        /* aDM4.Bind(aFace, aListOfGen); ???   */
#ifdef DEB_SPLIT_TO_ZONES
                        TCollection_AsciiString aName = aNam + i + "_g.brep";
                        BRepTools::Write(aListOfGen.Last(), aName.ToCString());
#endif
                      }
                       else {
                        TopTools_ListOfShape aList; 
                        aList.Append(aFace);
                        aDM4.Add(aFace, aList); //the same face - not modified
                       }
                    }
             }
             TopExp_Explorer exp2 (aFace, TopAbs_EDGE);
             for (int j =1;exp2.More();exp2.Next(),j++) {
               const TopoDS_Shape& anEdge = exp2.Current();
               if(!anEdge.IsNull()) {
                 if(aView.Contains(anEdge)) continue;
                 aView.Add(anEdge);
                 const TopTools_ListOfShape& aListOfNewEd = mkCom.Modified(anEdge);                 
                 if(!aListOfNewEd.IsEmpty())
                   aDM4.Add(anEdge, aListOfNewEd);
                 else {
                   if(!mkCom.IsDeleted(anEdge)) {
                     const TopTools_ListOfShape& aListOfGenEd = mkCom.Generated(anEdge);
                     if(!aListOfGenEd.IsEmpty()) {
                       /* aDM4.Bind(anEdge, aListOfGenEd); ???*/
                     } else {
                       TopTools_ListOfShape aList; 
                       aList.Append(anEdge);
                       aDM4.Add(anEdge, aList);//the same edge - not modified
                     }
                   }
                 }
               }
             }
            }
            } //end DM4 filling (phase 1)
            //cout << "DM4 Ext = " <<aDM4.Extent() <<endl;
         // phase 2 (from tool)
#ifdef DEB_SPLIT_TO_ZONES
         TCollection_AsciiString aNam("BndEd_");
#endif
         TopExp_Explorer expt (limFace, TopAbs_EDGE);
         for(int i =1;expt.More();expt.Next(),i++) {
           const TopoDS_Shape& anEdge = expt.Current();
           if(!anEdge.IsNull()) {
             const TopTools_ListOfShape& aListOfNewEd = mkCom.Modified(anEdge);    
#ifdef DEB_SPLIT_TO_ZONES
             TopTools_ListIteratorOfListOfShape itl(aListOfNewEd);
             for(int j=1;itl.More();itl.Next(),j++) {
               TCollection_AsciiString aName = aNam + i + "_" + j + "_j.brep";
               BRepTools::Write(itl.Value(), aName.ToCString());
               DEBTRACE(aName.ToCString()<<" = "<< itl.Value().TShape());
             }
#endif
             if(!aListOfNewEd.IsEmpty())
               aDM4.Add(anEdge, aListOfNewEd);
             else {
               if(!mkCom.IsDeleted(anEdge)) {
                 const TopTools_ListOfShape& aListOfGenEd = mkCom.Generated(anEdge);
                 if(!aListOfGenEd.IsEmpty()) {
                   /* aDM4.Bind(anEdge, aListOfGenEd); ??? */
                 } else {
                   TopTools_ListOfShape aList; 
                   aList.Append(anEdge);
                   aDM4.Add(anEdge, aList);//the same edge - not modified
                 }
               }
             }
           }
         }
         //cout << "DM4 Ext = " <<aDM4.Extent() <<endl;
         if(limplus1Object) {
           // fill DM1 (old - new) and DM2 (new - old)
           HYDROData_DataMapIteratorOfDataMapOfShapeListOfShape mIt(aDM4);
           for(;mIt.More();mIt.Next()) {
             const TopoDS_Shape& aKey = mIt.Key();//old
             TopTools_ListOfShape aList;
             aList.Append(aKey);
             const TopTools_ListOfShape& aListOfNew = mIt.Value();
             aDM1.Add(aKey, aListOfNew);
             TopTools_ListIteratorOfListOfShape it(aListOfNew);
             for(;it.More();it.Next()) {
               if(!aDM2.Contains(it.Value()))
                 aDM2.Add(it.Value(), aList);
               else {
                 TopTools_ListOfShape& aList = aDM2.ChangeFromKey(it.Value());
                 aList.Prepend(aKey);
               }
             }
           }
         } else {
           HYDROData_DataMapIteratorOfDataMapOfShapeListOfShape aMIt(aDM1); 
           //DM4 contains Old - New after common op. DM1: old - new after Split op.
           for(;aMIt.More();aMIt.Next()) {
             const TopoDS_Shape& aKey = aMIt.Key();
             TopTools_ListOfShape aList;
             aList.Append(aKey);
             const TopTools_ListOfShape& aListOfNew = aMIt.Value();
             TopTools_ListIteratorOfListOfShape it(aListOfNew);
             for(;it.More();it.Next()) {// iterate new: b1, b2, b3...
               if(!aDM4.Contains(it.Value())) // bi - is deleted
                 continue; // go to the next bi
               else {
                 const TopTools_ListOfShape& aListOfNew4 = aDM4.FindFromKey(it.Value());
                  TopTools_ListIteratorOfListOfShape it4(aListOfNew4); // {c1, c2, c3,...}
                  for(;it4.More();it4.Next()) {
                    if(!aDM2.Contains(it4.Value()))
                      aDM2.Add(it4.Value(), aList);
                    else {
                      TopTools_ListOfShape& aList = aDM2.ChangeFromKey(it4.Value());
                      aList.Prepend(aKey);
                    }
                  }
               }
             }
           }
         }
         } else {
             hasLimits = Standard_False;
#ifdef DEB_SPLIT_TO_ZONES
             BRepTools::Write(aComResult,"CommonNV.brep");
#endif    
          }
       }
      }
    }
  }// end limits processing
  if(!hasLimits) {
    HYDROData_DataMapIteratorOfDataMapOfShapeListOfShape aMIt(aDM1);
    for(;aMIt.More();aMIt.Next()) {
      const TopoDS_Shape& aKey = aMIt.Key();
      TopTools_ListOfShape aList;
      aList.Append(aKey);
      const TopTools_ListOfShape& aListOfNew = aMIt.Value();
      TopTools_ListIteratorOfListOfShape it(aListOfNew);
      for(;it.More();it.Next()) {
        if(!aDM2.Contains(it.Value()))
          aDM2.Add(it.Value(), aList);
        else {
          TopTools_ListOfShape& aList = aDM2.ChangeFromKey(it.Value());
          aList.Prepend(aKey);
        }
      }
    }
  }


#ifdef DEB_SPLIT_TO_ZONES
  BRep_Builder B;
  TopoDS_Compound cmp2;
  B.MakeCompound(cmp2);
  HYDROData_DataMapIteratorOfDataMapOfShapeListOfShape dIt(aDM2);
  for(int i =1;dIt.More();dIt.Next(),i++)
  {
    const TopoDS_Shape& aKey = dIt.Key();
    B.Add(cmp2, aKey);
  }
#endif

  AddInternalEdges(aDM2, InterPolys, &OutNE);
  NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher>::Iterator spit(OutNE);
  for (;spit.More();spit.Next())
  {
    const TopoDS_Edge& E = TopoDS::Edge(spit.Key());
    if (E.IsNull())
      continue;
    SplitData SDI(SplitData::Data_IntEdge, E, spit.Value()->GetName());   
    anOutputSplitDataList.append(SDI);
  }

  // Step 4. Fill output structure.
#ifdef DEB_SPLIT_TO_ZONES
  TCollection_AsciiString aNam4 ("SC_");
#endif  
  HYDROData_DataMapIteratorOfDataMapOfShapeListOfShape aMIt(aDM2);
  for(int i =1;aMIt.More();aMIt.Next(),i++) {
    SplitData aDestSplitData;
    const TopoDS_Shape& aKey = aMIt.Key(); //new
    aDestSplitData.Shape = aKey;
    if(aKey.ShapeType() == TopAbs_FACE)
      aDestSplitData.Type = SplitData::Data_Zone;
    else {
      aDestSplitData.Type = SplitData::Data_Edge;
#ifdef DEB_SPLIT_TO_ZONES
      TCollection_AsciiString aName = aNam4 + i + "_k.brep";
      BRepTools::Write(aKey,aName.ToCString());
#endif    
    }

    QStringList aListOfNames; // names processing
    const TopTools_ListOfShape& aListOfOld = aMIt.Value();
    TopTools_ListIteratorOfListOfShape it(aListOfOld);
    for(int j =1;it.More();it.Next(),j++) {    
      const TopoDS_Shape& aSh = it.Value(); //old
      if(aDM3.Contains(aSh)) {
        const QStringList& ObjectNames = aDM3.FindFromKey(aSh);    
        aListOfNames.append(ObjectNames);
#ifdef DEB_SPLIT_TO_ZONES
        TCollection_AsciiString aName = aNam4 + i +"_" + j + "_l.brep";
        BRepTools::Write(aSh ,aName.ToCString());        
#endif    
      }    else {
#ifdef DEB_SPLIT_TO_ZONES
        TCollection_AsciiString aName = aNam4 +"__" + i +"_" + j + "_m.brep";
        BRepTools::Write(aSh ,aName.ToCString());        
        DEBTRACE(aName.ToCString()<<" = "<< aSh.TShape());
#endif
          if(aBndView.Contains(aSh) && hasLimits) {            
            aListOfNames.append(aBndName);
#ifdef DEB_SPLIT_TO_ZONES
            DEBTRACE(" BndName = "<<aBndName.toStdString());
#endif
          }
      }
    }

    aDestSplitData.ObjectNames = aListOfNames;  
    anOutputSplitDataList.append(aDestSplitData);    
#ifdef DEB_SPLIT_TO_ZONES
    QString aStr = aDestSplitData.ObjectNames.join(" "); 
    DEBTRACE("New# = " << i<< " Names = "<<aStr.toStdString() << " size = " <<aDestSplitData.ObjectNames.size());
#endif
  }

  return anOutputSplitDataList;
}

HYDROData_SplitToZonesTool::SplitDataList
  HYDROData_SplitToZonesTool::Split( const HYDROData_SequenceOfObjects& theObjectList )
{
  HYDROData_SequenceOfObjects aGeomGroups;
  Handle(HYDROData_PolylineXY) aPolyline;
  HYDROData_SequenceOfObjects InterPolys;

  return Split( theObjectList, aGeomGroups, aPolyline, InterPolys );
}

void HYDROData_SplitToZonesTool::AddInternalEdges(HYDROData_DataMapOfShapeListOfShape& DM,
   const HYDROData_SequenceOfObjects& thePolylines,
   NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher>* OutNE)
{

  HYDROData_SequenceOfObjects::Iterator it(thePolylines);
  TopTools_ListOfShape Wires;
  NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher> W2P;
  for (;it.More();it.Next())
  {
    Handle(HYDROData_PolylineXY) P = Handle(HYDROData_PolylineXY)::DownCast(it.Value());
    const TopoDS_Shape& CW = P->GetShape();
    Wires.Append(CW);
    W2P.Bind(CW, P);
  }

  HYDROData_DataMapOfShapeListOfShape newDM;
  TopTools_IndexedDataMapOfShapeShape OutNE1;

  BRep_Builder BB;
  TopoDS_Compound cmpDM;
  BB.MakeCompound(cmpDM);
  TopTools_ListOfShape out;

  for (int i = 1; i <= DM.Extent();i++)
  {
    const TopoDS_Shape& K = DM.FindKey(i);
    BB.Add(cmpDM, K);
  }

  TopTools_IndexedDataMapOfShapeListOfShape OInSH2MSH;
  CutByEdges(cmpDM, Wires, out, true, &OutNE1, &OInSH2MSH);

  for (int i = 1; i <= DM.Extent();i++)
  {
    const TopoDS_Shape& K = DM.FindKey(i);
    const TopTools_ListOfShape& V = DM.FindFromIndex(i);  
    const TopTools_ListOfShape* modifSh = OInSH2MSH.Seek(K);
    if (modifSh && !modifSh->IsEmpty()) 
    {
      TopTools_ListIteratorOfListOfShape it(*modifSh);
      for (;it.More(); it.Next())
      {
        const TopoDS_Shape& NF = it.Value();
        if (!NF.IsNull())
          newDM.Add(NF, V);
      }
    }
    else
      newDM.Add(K, V); //was not modified by bop (CutByEdges)
  }



  for (int i = 1; i <= OutNE1.Extent(); i++)
    OutNE->Bind(OutNE1.FindKey(i), W2P(OutNE1.FindFromIndex(i)));

  DM = newDM;
}

void HYDROData_SplitToZonesTool::CutFaceByEdges(const TopoDS_Face& in, 
                                                TopTools_ListOfShape& out, 
                                                const HYDROData_SequenceOfObjects& thePolylines,
                                                NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher>* OutNE,                                                
                                                TopTools_IndexedDataMapOfShapeListOfShape* OutOrSh2M)
{

  HYDROData_SequenceOfObjects::Iterator it(thePolylines);
  TopTools_ListOfShape Wires;
  NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher> W2P;
  for (;it.More();it.Next())
  {
    Handle(HYDROData_PolylineXY) P = Handle(HYDROData_PolylineXY)::DownCast(it.Value());
    const TopoDS_Shape& CW = P->GetShape();
    Wires.Append(CW);
    W2P.Bind(CW, P);
  }

  TopTools_IndexedDataMapOfShapeShape OutNE1;
  CutByEdges(in, Wires, out, false, &OutNE1, OutOrSh2M);

  for (int i = 1; i <= OutNE1.Extent(); i++)
    OutNE->Bind(OutNE1.FindKey(i), W2P(OutNE1.FindFromIndex(i)));

}

int HYDROData_SplitToZonesTool::CutByEdges(const TopoDS_Shape& InSh, const TopTools_ListOfShape& InW,
                                           TopTools_ListOfShape& outShs, 
                                           bool OutShapeAsRes,
                                           TopTools_IndexedDataMapOfShapeShape* OutNE, 
                                           TopTools_IndexedDataMapOfShapeListOfShape* OInSH2MSH)
{
  int anError;
  if (InSh.IsNull())
    return -1;
  if (InW.IsEmpty())
  {
    outShs.Clear();
    outShs.Append(InSh);
    return 0;
  }
  TopTools_ListIteratorOfListOfShape anIt;
  BOPAlgo_Builder anAlgo;
  anAlgo.AddArgument(InSh);

  anIt.Initialize( InW );
  for( ; anIt.More(); anIt.Next() )
    anAlgo.AddArgument( anIt.Value() );

  anAlgo.Perform(); 
  if (anAlgo.HasErrors())
    return -1;

  if (!OutShapeAsRes)
  {
    outShs = anAlgo.Modified( InSh );
    if (outShs.IsEmpty())
      outShs.Append(InSh);
  }
  else
  {
    TopoDS_Shape Res = anAlgo.Shape();
    outShs.Append(Res);
  }

  if (OutNE)
  {
    TopTools_IndexedDataMapOfShapeShape NonFOutNE;
    anIt.Initialize( InW );
    for( ; anIt.More(); anIt.Next() )
    {
      const TopoDS_Shape& OSH = anIt.Value(); 
      TopExp_Explorer exp(OSH, TopAbs_EDGE);
      for (;exp.More();exp.Next())
      {
        const TopoDS_Edge& OE = TopoDS::Edge(exp.Current());
        const TopTools_ListOfShape& ls = anAlgo.Modified(OE);
        TopTools_ListIteratorOfListOfShape itls(ls);
        for (;itls.More();itls.Next())
          NonFOutNE.Add(itls.Value(), OSH);
      }
    }

    //filter edges; if face contain an edge => keep it
    TopTools_IndexedMapOfShape AllEdges; //edges from output shape
    for (anIt.Initialize(outShs); anIt.More();anIt.Next())
      TopExp::MapShapes(anIt.Value(), TopAbs_EDGE, AllEdges);

    for (int i = 1; i <= NonFOutNE.Extent(); i++)
    {
      const TopoDS_Shape& CE = NonFOutNE.FindKey(i);
      const TopoDS_Shape& V = NonFOutNE.FindFromIndex(i);
      if (AllEdges.Contains(CE))
        OutNE->Add(CE, V);
    }
  }

  //map all sub-shapes from the original InSh to modified shapes  
  if (OInSH2MSH)
  {
    TopTools_IndexedMapOfShape allSh;
    TopExp::MapShapes(InSh, allSh);
    for(int i = 1; i <= allSh.Extent(); i++ )
    {
      const TopoDS_Shape& or_sh = allSh(i); 
      const TopTools_ListOfShape& ls = anAlgo.Modified(or_sh);
      OInSH2MSH->Add(or_sh, ls);
    }
  }

  return 0;
}

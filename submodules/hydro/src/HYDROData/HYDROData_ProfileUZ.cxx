// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_ProfileUZ.h"

#include "HYDROData_Tool.h"

#include <gp_XY.hxx>
#include <gp_Pnt2d.hxx>

#include <TColStd_ListIteratorOfListOfReal.hxx>

#include <TDataStd_BooleanList.hxx>
#include <TDataStd_ExtStringList.hxx>
#include <TDataStd_IntegerList.hxx>
#include <TDataStd_RealList.hxx>

#include <TopoDS_Shape.hxx>

#include <HYDROData_Profile.h>
#include <HYDROData_PolylineXY.h>


IMPLEMENT_STANDARD_RTTIEXT(HYDROData_ProfileUZ, HYDROData_IPolyline)

HYDROData_ProfileUZ::HYDROData_ProfileUZ()
: HYDROData_IPolyline()
{
}

HYDROData_ProfileUZ::~HYDROData_ProfileUZ()
{
}

TopoDS_Shape HYDROData_ProfileUZ::GetShape() const
{
  return TopoDS_Shape();
}

double HYDROData_ProfileUZ::GetDepthFromDistance( const PointsList& thePoints,
                                                  const double&     theDistance )
{
  double aResDepth = 0.0;

  int aNbPoints = thePoints.Size();
  if ( aNbPoints < 2 )
    return aResDepth;

  double aCompDist = 0.0;
  HYDROData_IPolyline::Point aPrevPoint = thePoints.First();
  for ( int i = 2; i <= aNbPoints; ++i )
  {
    const Point& aCurPoint = thePoints.Value( i );

    double aPntDist = gp_Pnt2d( aPrevPoint.X(), 0 ).Distance( gp_Pnt2d( aCurPoint.X(), 0 ) );

    aCompDist += aPntDist;

    if ( theDistance < aCompDist )
    {
      double aComPntDist = gp_Pnt2d( thePoints.First().X(), 0 ).Distance( gp_Pnt2d( aPrevPoint.X(), 0 ) );

      double aFindDist = theDistance - aComPntDist;
      double aRatio = aFindDist / ( aPntDist - aFindDist );

      aResDepth = ( aPrevPoint.Y() + aRatio * aCurPoint.Y() ) / ( 1 + aRatio );
      break;
    }
    else aResDepth = aCurPoint.Y();  // TODO: workaround for normalized flat altitudes

    aPrevPoint = aCurPoint;
  }

  return aResDepth;
}

int HYDROData_ProfileUZ::NbSections() const
{
  return 1;
}

void HYDROData_ProfileUZ::AddSection( const TCollection_AsciiString& /*theSectName*/,
                                      const SectionType              /*theSectionType*/,
                                      const bool                     /*theIsClosed*/ )
{
}

TCollection_AsciiString HYDROData_ProfileUZ::GetSectionName( const int /*theSectionIndex*/ ) const
{
  return "Section_1";
}

void HYDROData_ProfileUZ::SetSectionName( const int                      /*theSectionIndex*/, 
                                          const TCollection_AsciiString& /*theSectionName*/ )
{
}

HYDROData_ProfileUZ::SectionType HYDROData_ProfileUZ::GetSectionType( const int /*theSectionIndex*/ ) const
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aTypesList.IsNull() || aTypesList->IsEmpty() )
    return SECTION_POLYLINE;

  return (SectionType)aTypesList->First();
}

void HYDROData_ProfileUZ::SetSectionType( const int         /*theSectionIndex*/, 
                                          const SectionType theSectionType )
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList );
  if ( aTypesList.IsNull()  )
    return;

  // Refill the existing list
  aTypesList->Clear();
  aTypesList->Append( theSectionType );
}

bool HYDROData_ProfileUZ::IsClosedSection( const int /*theSectionIndex*/ ) const
{
  return false;
}

void HYDROData_ProfileUZ::SetSectionClosed( const int  /*theSectionIndex*/, 
                                            const bool /*theIsClosed*/ )
{
}

void HYDROData_ProfileUZ::RemoveSection( const int /*theSectionIndex*/ )
{
  RemoveSections();
}

void HYDROData_ProfileUZ::RemoveSections()
{
  removePointsLists( 0 );
}

void HYDROData_ProfileUZ::AddPoint( const int    /*theSectionIndex*/,
                                    const Point& thePoint,
                                    const int    thePointIndex )
{
  double aNewCoordU = thePoint.X();
  double aNewCoordZ = thePoint.Y();

  Handle(TDataStd_RealList) aListU, aListZ;
  getPointsLists( 0, aListU, aListZ );

  if ( aListU->IsEmpty() || aNewCoordU > aListU->Last() )
  {
    aListU->Append( aNewCoordU );
    aListZ->Append( aNewCoordZ );
    return;
  }
  else if ( aNewCoordU < aListU->First() )
  {
    aListU->Prepend( aNewCoordU );
    aListZ->Prepend( aNewCoordZ );
    return;
  }

  TColStd_ListOfReal anOldListU;
  anOldListU = aListU->List();

  TColStd_ListOfReal anOldListZ;
  anOldListZ = aListZ->List();

  // Crsat new lists
  removePointsLists( 0 );
  getPointsLists( 0, aListU, aListZ );

  bool anIsInserted = false;
  TColStd_ListIteratorOfListOfReal anIterU( anOldListU );
  TColStd_ListIteratorOfListOfReal anIterZ( anOldListZ );
  for ( ; anIterU.More() && anIterZ.More(); anIterU.Next(), anIterZ.Next() )
  {
    double aCoordU = anIterU.Value();
    double aCoordZ = anIterZ.Value();

    if ( !anIsInserted )
    {
      if ( ValuesEquals( aNewCoordU, aCoordU ) )
      {
        // Just update Z value
        aCoordZ = aNewCoordZ;
        anIsInserted = true;
      }
      else if ( aNewCoordU < aCoordU )
      {
        // Insert new point
        aListU->Append( aNewCoordU );
        aListZ->Append( aNewCoordZ );
        anIsInserted = true;
      }
    }

    aListU->Append( aCoordU );
    aListZ->Append( aCoordZ );
  }
}

void HYDROData_ProfileUZ::SetPoint( const int    theSectionIndex,
                                    const Point& thePoint,
                                    const int    /*thePointIndex*/ )
{
  AddPoint( theSectionIndex, thePoint );
}

void HYDROData_ProfileUZ::RemovePoint( const int /*theSectionIndex*/,
                                       const int thePointIndex )
{
  Handle(TDataStd_RealList) aListU, aListZ;
  getPointsLists( 0, aListU, aListZ, false );
  if ( aListU.IsNull() || aListZ.IsNull() || aListU->IsEmpty() )
    return;

  TColStd_ListOfReal anOldListU;
  anOldListU = aListU->List();

  TColStd_ListOfReal anOldListZ;
  anOldListZ = aListZ->List();

  // Creat new lists
  removePointsLists( 0 );
  getPointsLists( 0, aListU, aListZ );

  bool anIsInserted = false;
  TColStd_ListIteratorOfListOfReal anIterU( anOldListU );
  TColStd_ListIteratorOfListOfReal anIterZ( anOldListZ );
  for ( int i = 0; anIterU.More() && anIterZ.More(); anIterU.Next(), anIterZ.Next(), ++i )
  {
    if ( i == thePointIndex )
      continue; // skip index to remove

    aListU->Append( anIterU.Value() );
    aListZ->Append( anIterZ.Value() );
  }
}

HYDROData_ProfileUZ::PointsList HYDROData_ProfileUZ::GetPoints( const int /*theSectionIndex*/, bool /*IsConvertToGlobal*/ ) const
{
  PointsList aResList;

  Handle(TDataStd_RealList) aListU, aListZ;
  getPointsLists( 0, aListU, aListZ, false );
  if ( aListU.IsNull() || aListZ.IsNull() )
    return aResList;

  TColStd_ListIteratorOfListOfReal anIterU( aListU->List() );
  TColStd_ListIteratorOfListOfReal anIterZ( aListZ->List() );
  for ( ; anIterU.More() && anIterZ.More(); anIterU.Next(), anIterZ.Next() )
  {
    Point aPoint( anIterU.Value(), anIterZ.Value() );
    aResList.Append( aPoint );
  }

  return aResList;
}

void HYDROData_ProfileUZ::CalculateAndAddPoints(const NCollection_Sequence<gp_XYZ>& theXYZPoints, 
  Handle(HYDROData_PolylineXY)& thePolylineXY,
  bool fillPolyXY)
{
   // Fill 2D polyline
  if (fillPolyXY)
  {
    for ( int i = 1; i <= theXYZPoints.Size(); i++ ) {
      const HYDROData_Profile::ProfilePoint& aBottomPoint = theXYZPoints.Value( i );
      thePolylineXY->AddPoint( 0, HYDROData_PolylineXY::Point( aBottomPoint.X(), aBottomPoint.Y() ) );
    }
  }

  // Calculate profile UZ points

  // First point
  const HYDROData_Profile::ProfilePoint& aFirstBottomPoint = theXYZPoints.First();
  AddPoint( 0, HYDROData_ProfileUZ::Point( 0, aFirstBottomPoint.Z() ) );

  // Intermediate points
  double aPolylineCommonDist = thePolylineXY->GetDistance( 0, thePolylineXY->NbPoints( 0 ) - 1 );

  for ( int i = 2, aNbPoints = theXYZPoints.Size(); i < aNbPoints; i++ ) {
    const HYDROData_Profile::ProfilePoint& aBottomPoint = theXYZPoints.Value( i );
    
    double aDistance = thePolylineXY->GetDistance( 0, i - 1 );
    
    Standard_Real anU = aDistance; // = ( aDistance / aPolylineCommonDist ) * aPolylineCommonDist;
    AddPoint( 0, HYDROData_ProfileUZ::Point( anU, aBottomPoint.Z() ) );
  }
  
  // Last point
  const HYDROData_Profile::ProfilePoint& aLastBottomPoint = theXYZPoints.Last();
  AddPoint( 0, HYDROData_ProfileUZ::Point( aPolylineCommonDist, aLastBottomPoint.Z() ) );
 
}



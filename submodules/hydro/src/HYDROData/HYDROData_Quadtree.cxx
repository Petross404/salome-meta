// Copyright (C) 2007-2014  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  HYDROData_Quadtree : Quadtree implementation  (from SMESH module)
//  File      : HYDROData_Quadtree.cxx
//  Module    : HYDROData
//
#include "HYDROData_Quadtree.hxx"

/*!
 * Constructor. limit must be provided at tree root construction.
 * limit will be deleted by HYDROData_Quadtree.
 */
HYDROData_Quadtree::HYDROData_Quadtree(HYDROData_TreeLimit* limit) :
    TBaseTree(limit)
{
}

/*!
 * \brief Allocate a bndbox according to childIndex. childIndex is zero based
 */
Bnd_B2d* HYDROData_Quadtree::newChildBox(int childIndex) const
{
  gp_XY ming = getBox()->CornerMin();
  gp_XY maxg = getBox()->CornerMax();
  gp_XY HSize = (maxg - ming) / 2.;
  gp_XY childHsize = HSize / 2.;

  gp_XY minChild(ming.X() + childIndex % 2 * HSize.X(), ming.Y() + (childIndex > 1) * HSize.Y());

  return new Bnd_B2d(minChild + childHsize, childHsize);
}

/*!
 * \brief Compute the biggest dimension of my box
 */
double HYDROData_Quadtree::maxSize() const
{
  if (getBox() && !getBox()->IsVoid())
    {
      gp_XY ming = getBox()->CornerMin();
      gp_XY maxg = getBox()->CornerMax();
      gp_XY Size = (maxg - ming);
      double returnVal = (Size.X() > Size.Y()) ? Size.X() : Size.Y();
      return returnVal;
    }
  return 0.;
}

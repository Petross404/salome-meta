// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_ShapesGroup_HeaderFile
#define HYDROData_ShapesGroup_HeaderFile

#include <HYDROData_Entity.h>
#include <TopTools_SequenceOfShape.hxx>
#include <TopTools_ListOfShape.hxx>
#include <QString>

class TopoDS_Shape;
class BRepBuilderAPI_MakeShape;
class ShapeUpgrade_UnifySameDomain;
class BRepTools_ReShape;

/**\class HYDROData_ShapesGroup
 * \brief Class that stores/retreives the sequence of shapes.
 */
class HYDROData_ShapesGroup : public HYDROData_Entity
{
public:

  struct GroupDefinition
  {
    GroupDefinition() {}

    static void Update( NCollection_Sequence<GroupDefinition>* theGroupsDefs,
                        BRepBuilderAPI_MakeShape*              theAlgo );

    static void Update( NCollection_Sequence<GroupDefinition>* theGroupsDefs,
                        ShapeUpgrade_UnifySameDomain*          theAlgo );

    static void Update( NCollection_Sequence<GroupDefinition>* theGroupsDefs,
                        BRepTools_ReShape*                     theAlgo );
    

    void        Dump( std::ostream& theStream ) const;

    static void Dump( std::ostream&                                theStream,
                      const NCollection_Sequence<GroupDefinition>& theGroups );

    TCollection_AsciiString            Name;
    TopTools_SequenceOfShape           Shapes;
  };
  typedef NCollection_Sequence<GroupDefinition> SeqOfGroupsDefs;

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Shape,   ///< reference edges
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_ShapesGroup, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_SHAPES_GROUP; }

  /**
   * Returns flag indicating that object is updateble or not.
   */
  HYDRODATA_EXPORT virtual bool CanBeUpdated() const;

    /**
   * Returns flag indicating that object can be removed or not.
   */
  HYDRODATA_EXPORT virtual bool CanRemove();

public:      
  // Public methods to work with reference Shapes

  /**
   * Add new one shape for the group.
   */
  HYDRODATA_EXPORT virtual void AddShape( const TopoDS_Shape& theShape );

  /**
   * Sets new sequence of shapes for the group.
   */
  HYDRODATA_EXPORT virtual void SetShapes( const TopTools_SequenceOfShape& theShapes );

  /**
   * Sets new list of shapes for the group.
   */
  HYDRODATA_EXPORT virtual void SetShapes( const TopTools_ListOfShape& theShapes );

  /**
   * Returns all shapes of the group.
   */
  HYDRODATA_EXPORT virtual void GetShapes( TopTools_SequenceOfShape& theShapes ) const;

  /**
   * Removes all shapes from the group.
   */
  HYDRODATA_EXPORT virtual void RemoveShapes();


protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_ShapesGroup();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_ShapesGroup();
};

#endif

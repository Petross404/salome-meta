// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Confluence.h"

#include "HYDROData_Document.h"

#include <TopoDS_Shape.hxx>

#include <QStringList>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Confluence,HYDROData_NaturalObject)


HYDROData_Confluence::HYDROData_Confluence()
: HYDROData_NaturalObject( Geom_3d )
{
}

HYDROData_Confluence::~HYDROData_Confluence()
{
}

QStringList HYDROData_Confluence::DumpToPython( const QString& thePyScriptPath,
                                                MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aName = GetObjPyName();

  // TODO

  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aName );
  aResList << QString( "" );

  return aResList;
}

void HYDROData_Confluence::UpdateLocalCS( double theDx, double theDy )
{
  //TODO
}




// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Region.h"

#include "HYDROData_CalculationCase.h"
#include "HYDROData_Document.h"
#include "HYDROData_Iterator.h"
#include "HYDROData_Object.h"
#include "HYDROData_ShapesTool.h"
#include "HYDROData_Zone.h"
#include "HYDROData_Tool.h"

#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Shell.hxx>
#include <TopoDS_Face.hxx>

#include <TopExp.hxx>

#include <TopTools_ListOfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_IndexedMapOfShape.hxx>


#include <BRep_Builder.hxx>
#include <BRepAlgoAPI_Fuse.hxx>

#include <ShapeUpgrade_UnifySameDomain.hxx>

#include <QStringList>
#include <QColor>

#include "Geom_Plane.hxx"
#include "gp_Pln.hxx"
#include "BRepTools_ReShape.hxx"

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

//#define DEB_GET_REGION_SHAPE

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Region, HYDROData_Entity)


HYDROData_Region::HYDROData_Region()
 : HYDROData_Entity( Geom_2d )
{
}

HYDROData_Region::~HYDROData_Region()
{
}

bool HYDROData_Region::CanBeUpdated() const
{
  return false;
}

void HYDROData_Region::Remove()
{
  Handle(HYDROData_CalculationCase) aFatherCalc = 
    Handle(HYDROData_CalculationCase)::DownCast( GetFatherObject() );

  HYDROData_Entity::Remove();

  if ( !aFatherCalc.IsNull() )
    aFatherCalc->UpdateRegionsOrder();
}

bool HYDROData_Region::CanRemove()
{
  return false;
}

HYDROData_SequenceOfObjects HYDROData_Region::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Entity::GetAllReferenceObjects();

  HYDROData_SequenceOfObjects aSeqOfZones = GetZones();
  aResSeq.Append( aSeqOfZones );

  return aResSeq;
}

bool HYDROData_Region::AddZone( const Handle(HYDROData_Zone)& theZone )
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();

  if ( theZone.IsNull() )
    return false;
  
  if ( HasReference( theZone, DataTag_Zone ) )
    return false; // Object is already in reference list

  // Move the zone from other region
  Handle(HYDROData_Region) aFatherRegion = 
    Handle(HYDROData_Region)::DownCast( theZone->GetFatherObject() );
  if ( !aFatherRegion.IsNull() && aFatherRegion->Label() != myLab )
  {
    Handle(HYDROData_Zone) aNewZone = addNewZone( aDocument, "", TopoDS_Face(), QStringList() );
    theZone->CopyTo( aNewZone, false );

    // To prevent changing of stored shape
    aNewZone->SetShape( theZone->GetShape() );

    aFatherRegion->RemoveZone( theZone );

    theZone->SetLabel( aNewZone->Label() );
  }
  else
  {
    AddReferenceObject( theZone, DataTag_Zone );
  }

  return true;
}

HYDROData_SequenceOfObjects HYDROData_Region::GetZones() const
{
  return GetReferenceObjects( DataTag_Zone );
}

void HYDROData_Region::RemoveZone( const Handle(HYDROData_Zone)& theZone,  bool removeRegion )
{
  if ( theZone.IsNull() )
    return;

  RemoveReferenceObject( theZone->Label(), DataTag_Zone );

  // Remove zone from data model
  Handle(HYDROData_Region) aFatherRegion = 
    Handle(HYDROData_Region)::DownCast( theZone->GetFatherObject() );
  if ( !aFatherRegion.IsNull() && aFatherRegion->Label() == myLab )
    theZone->Remove();

  if (removeRegion)
  {
    // If the last zone has been removed from region we remove this region
    HYDROData_SequenceOfObjects aRefZones = GetZones();
    if ( aRefZones.IsEmpty() )
      Remove();
  }
}

void HYDROData_Region::RemoveZones()
{
  ClearReferenceObjects( DataTag_Zone );
  myLab.FindChild( DataTag_ChildZone ).ForgetAllAttributes( true );
}

Handle(HYDROData_Zone) HYDROData_Region::addNewZone( const Handle(HYDROData_Document)& theDoc,
                                                     const QString& thePrefix,
                                                     const TopoDS_Face& theFace,
                                                     const QStringList& theRefObjects )
{
  TDF_Label aNewLab = myLab.FindChild( DataTag_ChildZone ).NewChild();

  Handle(HYDROData_Zone) aNewZone =
    Handle(HYDROData_Zone)::DownCast( HYDROData_Iterator::CreateObject( aNewLab, KIND_ZONE ) );
  AddZone( aNewZone );

  QString aZoneName = HYDROData_Tool::GenerateObjectName( theDoc, thePrefix );
  aNewZone->SetName( aZoneName );

  aNewZone->SetShape( theFace );

  // Add the reference object for zone
  for ( int i = 0, n = theRefObjects.length(); i < n; ++i )
  {
    const QString& anObjName = theRefObjects.at( i );
    Handle(HYDROData_Entity) aRefObject = theDoc->FindObjectByName( anObjName );
    if ( aRefObject.IsNull() )
      continue;
    DEBTRACE("aRefObject name: " << aRefObject->GetName());
    aNewZone->AddObject( aRefObject );
  }

  return aNewZone;
}

void getUsedGroups( const TopoDS_Shape&                     theShape,
                    HYDROData_ShapesGroup::SeqOfGroupsDefs& theOriGroups,
                    HYDROData_ShapesGroup::SeqOfGroupsDefs& theUsedGroups )
{
#ifdef DEB_GET_REGION_SHAPE
  HYDROData_ShapesTool::DumpShapeSubShapes( std::cout, "Zone face edges:", theShape, TopAbs_EDGE );
#endif

  TopTools_IndexedMapOfShape aMapOfSubShapes;
  TopExp::MapShapes( theShape, TopAbs_EDGE, aMapOfSubShapes );

  HYDROData_ShapesGroup::SeqOfGroupsDefs::Iterator anIter( theOriGroups );
  for ( ; anIter.More(); anIter.Next() )
  {
    HYDROData_ShapesGroup::GroupDefinition& anOriGroupDef = anIter.ChangeValue();
    if ( anOriGroupDef.Shapes.IsEmpty() )
      continue;

    for ( int i = 1; i <= anOriGroupDef.Shapes.Length(); ++i )
    {
      TopoDS_Shape aGroupEdge = anOriGroupDef.Shapes.Value( i );
      
      int aShapeIndex = aMapOfSubShapes.FindIndex( aGroupEdge );
      if ( aShapeIndex <= 0 )
        continue;

      anOriGroupDef.Shapes.Remove( i );
      --i;

      bool anIsAdded = false;

      HYDROData_ShapesGroup::SeqOfGroupsDefs::Iterator aUsedIter( theUsedGroups );
      for ( ; aUsedIter.More(); aUsedIter.Next() )
      {
        HYDROData_ShapesGroup::GroupDefinition& aUsedGroupDef = aUsedIter.ChangeValue();
        if ( aUsedGroupDef.Name != anOriGroupDef.Name )
          continue;

        aUsedGroupDef.Shapes.Append( aGroupEdge );
        anIsAdded = true;
        break;
      }

      if ( !anIsAdded )
      {
        HYDROData_ShapesGroup::GroupDefinition aUsedGroupDef;
        aUsedGroupDef.Name = anOriGroupDef.Name;
        aUsedGroupDef.Shapes.Append( aGroupEdge );
        theUsedGroups.Append( aUsedGroupDef );
      }
    }
  }
}


TopoDS_Shape HYDROData_Region::GetShape( HYDROData_ShapesGroup::SeqOfGroupsDefs* theSeqOfGroups, const TopTools_SequenceOfShape* IntSh ) const
{
  DEBTRACE("GetShape");
  HYDROData_ShapesGroup::SeqOfGroupsDefs aSeqOfGroups;
  HYDROData_ShapesGroup::SeqOfGroupsDefs aSeqOfUsedGroups;
  if ( theSeqOfGroups )
    aSeqOfGroups = *theSeqOfGroups;

#ifdef DEB_GET_REGION_SHAPE
  HYDROData_ShapesGroup::GroupDefinition::Dump( std::cout, aSeqOfGroups );
#endif

  TopoDS_Shape aResShape;

  // Unite the region zones (each zone is a face) into one face (united face)
  // If the zones can't be united into the single face - unite them into shell

  // Collect the list of region faces
  TopTools_ListOfShape aRegionFacesList;

  HYDROData_SequenceOfObjects aZones = GetZones();
  HYDROData_SequenceOfObjects::Iterator aZoneIter( aZones );
  TopTools_IndexedMapOfShape AllE;
  TopTools_IndexedMapOfShape IE; //int edges

  for ( ; aZoneIter.More(); aZoneIter.Next() )
  {
    Handle(HYDROData_Zone) aZone =
      Handle(HYDROData_Zone)::DownCast( aZoneIter.Value() );
    if ( aZone.IsNull() )
      continue;

    TopoDS_Shape aZoneShape = aZone->GetShape();
    if ( aZoneShape.IsNull() || aZoneShape.ShapeType() != TopAbs_FACE )
      continue;

    TopoDS_Face aZoneFace = TopoDS::Face( aZoneShape );
    aRegionFacesList.Append( aZoneFace );
    TopExp::MapShapes(aZoneFace, TopAbs_EDGE, AllE); // collect all edges
    getUsedGroups( aZoneFace, aSeqOfGroups, aSeqOfUsedGroups );
  } // zones iterator

  DEBTRACE("--- IntSh->Length():" << IntSh->Length());
  if (IntSh)
  {
    for (int i = 1; i <= IntSh->Length(); i++)
    {
      const TopoDS_Shape& CS = (*IntSh)(i);
      if (AllE.Contains(CS))
        IE.Add(CS);
    }
  }
  
  if ( aRegionFacesList.IsEmpty() )
    return aResShape;

  TopoDS_Face aRegionFace;

  if ( aRegionFacesList.Extent() == 1 )
  {
    DEBTRACE("--- aRegionFacesList.Extent() == 1 ");
    aResShape = TopoDS::Face( aRegionFacesList.First() );
  }
  else
  {
#ifdef DEB_GET_REGION_SHAPE
    HYDROData_ShapesGroup::GroupDefinition::Dump( std::cout, aSeqOfUsedGroups );
#endif

    // Try to fuse all region faces into one common face
    TopoDS_Shape aFuseShape;
    TopTools_ListIteratorOfListOfShape aFaceIter( aRegionFacesList );
    for ( ; aFaceIter.More(); aFaceIter.Next() )
    {
      if ( aFuseShape.IsNull() )
      {
        aFuseShape = aFaceIter.Value();
        continue;
      }

      BRepAlgoAPI_Fuse aFuse( aFuseShape, aFaceIter.Value() );
      if ( !aFuse.IsDone() )
      {
        aFuseShape.Nullify();
        break;
      }

      aFuseShape = aFuse.Shape();

      //update history of internal edges
      TopTools_IndexedMapOfShape DIE;
      TopTools_ListOfShape newSh1, newSh2;
      DEBTRACE("IE.Extent():" << IE.Extent());
      for (int i = 1; i <= IE.Extent(); i++)
      {
        const TopoDS_Shape& CSH = IE(i);
        newSh1.Clear();
        newSh2.Clear();
        newSh1 = aFuse.Modified(CSH);
        if (newSh1.IsEmpty())
        {
          newSh2 = aFuse.Generated(CSH);
          if (newSh2.IsEmpty())
            DIE.Add(CSH);
          else
            for (TopTools_ListIteratorOfListOfShape lt(newSh2); lt.More(); lt.Next())
              if (!lt.Value().IsNull())
                 DIE.Add(lt.Value());
        }
        else
        {
          for (TopTools_ListIteratorOfListOfShape lt(newSh1); lt.More(); lt.Next())
            if (!lt.Value().IsNull())
              DIE.Add(lt.Value());
        }
      }
      IE = DIE;
      //update groups
      HYDROData_ShapesGroup::GroupDefinition::Update( &aSeqOfUsedGroups, &aFuse );
    } // faces iterator

#ifdef DEB_GET_REGION_SHAPE
    HYDROData_ShapesTool::DumpShapeSubShapes( std::cout, "Fused face edges:", aFuseShape, TopAbs_EDGE );
#endif

    aFuseShape = HYDROData_Tool::RebuildCmp(aFuseShape);

    ShapeUpgrade_UnifySameDomain unif( aFuseShape, Standard_False, Standard_True, Standard_False );
    if (!IE.IsEmpty())
    {
      TopTools_MapOfShape IEM;
      for (int i = 1; i <= IE.Extent(); i++)
        IEM.Add(IE(i));
      unif.KeepShapes(IEM);
    }

    unif.Build();
    TopoDS_Shape anUnitedShape;
    anUnitedShape = unif.Shape();

    HYDROData_ShapesGroup::GroupDefinition::Update( &aSeqOfUsedGroups, &unif );

    //cout << "inif:   fuseshape: " << aFuseShape.TShape().get() << "unifS " << anUnitedShape.TShape().get() << endl;
#ifdef DEB_GET_REGION_SHAPE
    HYDROData_ShapesGroup::GroupDefinition::Dump( std::cout, aSeqOfUsedGroups );
#endif

    TopTools_SequenceOfShape aShapeFaces;
    HYDROData_ShapesTool::ExploreShapeToShapes( anUnitedShape, TopAbs_FACE, aShapeFaces );
    if ( aShapeFaces.Length() == 1 ) //it should be either face or compound of faces (?)
    {
      const TopoDS_Face& CF = TopoDS::Face( aShapeFaces.Value( 1 ));
      aResShape = CF;
    }
    else
    {
      TopTools_SequenceOfShape aShapeShells;
      HYDROData_ShapesTool::ExploreShapeToShapes( anUnitedShape, TopAbs_SHELL, aShapeShells );
      if (aShapeShells.Length() == 1)
        aResShape = TopoDS::Shell(aShapeShells(1));
      else
        aResShape = anUnitedShape;
    }

    // Update the sequence of groups
    if ( theSeqOfGroups )
    {
      HYDROData_ShapesGroup::SeqOfGroupsDefs::Iterator aUsedIter( aSeqOfUsedGroups );
      for ( ; aUsedIter.More(); aUsedIter.Next() )
      {
        const HYDROData_ShapesGroup::GroupDefinition& aUsedGroupDef = aUsedIter.Value();
        if ( aUsedGroupDef.Shapes.IsEmpty() )
          continue;

        HYDROData_ShapesGroup::SeqOfGroupsDefs::Iterator anOriIter( aSeqOfGroups );
        for ( ; anOriIter.More(); anOriIter.Next() )
        {
          HYDROData_ShapesGroup::GroupDefinition& anOriGroupDef = anOriIter.ChangeValue();
          if ( anOriGroupDef.Name != aUsedGroupDef.Name )
            continue;

          HYDROData_ShapesTool::AddShapes( anOriGroupDef.Shapes, aUsedGroupDef.Shapes );
          break;
        }
      }

      *theSeqOfGroups = aSeqOfGroups;
    }
  }
                                                     
  return aResShape;
}

QStringList HYDROData_Region::DumpToPython( const QString&       thePyScriptPath,
                                            MapOfTreatedObjects& theTreatedObjects,
                                            QString              defRegName ) const
{
  QStringList aResList;

  // Find region
  findPythonReferenceObject( aResList, defRegName );

  // Add zones
  HYDROData_SequenceOfObjects aZones = GetZones();
  HYDROData_SequenceOfObjects::Iterator aZonesIter( aZones );
  for ( ; aZonesIter.More(); aZonesIter.Next() ) {
    Handle(HYDROData_Zone) aZone =
      Handle(HYDROData_Zone)::DownCast( aZonesIter.Value() );
    if ( aZone.IsNull() ) {
      continue;
    }
    
    // find zone
    aZone->findPythonReferenceObject( aResList );
    theTreatedObjects.insert( aZone->GetName(), aZone );
    
    // set zone merge type
    QString aMergeTypeStr;
    HYDROData_Zone::MergeType aMergeType = aZone->GetMergeType();
    if ( aMergeType == HYDROData_Zone::Merge_ZMIN ) {
      aMergeTypeStr = "HYDROData_Zone.Merge_ZMIN";
    } else if ( aMergeType == HYDROData_Zone::Merge_ZMAX ) {
      aMergeTypeStr = "HYDROData_Zone.Merge_ZMAX";
    } else if ( aMergeType == HYDROData_Zone::Merge_Object ) {
      aMergeTypeStr = "HYDROData_Zone.Merge_Object";
    }

    if ( !aMergeTypeStr.isEmpty() ) {
      aResList << QString( "%1.SetMergeType( %2 )" ).arg( aZone->GetObjPyName() ).arg( aMergeTypeStr );
    }
    if ( aMergeType == HYDROData_Zone::Merge_Object ) {
      Handle(HYDROData_Entity) aMergeObject = aZone->GetMergeObject();
      if ( !aMergeObject.IsNull() ) {
        aMergeObject->findPythonReferenceObject( aResList );
        aResList << QString( "%1.SetMergeObject( %2 )" ).arg( aZone->GetObjPyName() )
                                                        .arg( aMergeObject->GetObjPyName() );
      }
    }
    // set color
    QColor zoneColor = aZone->GetColor(Qt::darkBlue);
    aResList << QString( "%1.SetColor( QColor( %2, %3, %4 ))" )
                 .arg( aZone->GetObjPyName() ).arg( zoneColor.red() ).arg( zoneColor.green() ).arg( zoneColor.blue() );
    // add zone
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aZone, "AddZone" );

  }

  return aResList;
}

bool HYDROData_Region::IsSubmersible() const
{
  HYDROData_SequenceOfObjects aZones = GetZones();
  HYDROData_SequenceOfObjects::Iterator aZonesIter( aZones );
  for ( ; aZonesIter.More(); aZonesIter.Next() )
  {
    Handle(HYDROData_Zone) aZone =
      Handle(HYDROData_Zone)::DownCast( aZonesIter.Value() );
    if ( !aZone->IsSubmersible() )
      return false; //if one of zones is not submersible the region is considered as not submersible
  }
  return true;
}

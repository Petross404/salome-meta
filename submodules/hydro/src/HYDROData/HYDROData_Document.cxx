// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_Document.h>
#include <HYDROData_Application.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Tool.h>
#include <HYDROData_InterpolatorsFactory.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_LandCoverMap.h>

#include <TDataStd_Real.hxx>
#include <TDataStd_Integer.hxx>
#include <TDataXtd_Position.hxx>

#include <TDF_Delta.hxx>

#include <gp_Pnt.hxx>

#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QColor>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Document,Standard_Transient)

#define PYTHON_DOC_NAME "hydro_doc"

static const int UNDO_LIMIT = 10; // number of possible undo operations in the module

static const int TAG_PROPS = 1; // general properties tag
static const int TAG_PROPS_NEW_ID = 1; // general properties: tag for storage of the new object ID
static const int TAG_OBJECTS = 2; // tag of the objects sub-tree
static const int TAG_HISTORY = 3; // tag of the history sub-tree (Root for History)
static const int TAG_LOCAL_CS = 4; // tag of local coordinate system information
static const int TAG_DEF_STRICKLER_COEFF = 5; // tag of default strickler coefficient
static const int TAG_COUNT_QUADTREE = 6; // tag of number of quadtrees created so far
static const int TAG_COUNT_DELAUNAY = 7; // tag of number of Delaunay triangulations created so far
static const gp_Pnt2d DEFAULT_LOCAL_CS( 0, 0 );

using namespace std;

typedef QMap<Standard_Integer, Handle(HYDROData_Entity)> MapOfOrdered;
typedef QMap<QString, Handle(HYDROData_Entity)> MapOfUnordered;

Handle(HYDROData_Document) HYDROData_Document::Document()
{
  Handle(HYDROData_Document) aResult = 
    HYDROData_Application::GetApplication()->GetDocument();
  if (aResult.IsNull()) {
    DEBTRACE("new HYDROData_Document");
    aResult = new HYDROData_Document();
    HYDROData_Application::GetApplication()->AddDocument(aResult);
  }
  return aResult;
}

Handle(HYDROData_Document) HYDROData_Document::Document(
  const TDF_Label& theObjectLabel )
{
  Handle(HYDROData_Document) aResDoc;
  if ( theObjectLabel.IsNull() )
    return aResDoc;

  Handle(TDocStd_Document) anObjDoc;
  try
  {
    anObjDoc = TDocStd_Document::Get( theObjectLabel );
  }
  catch( ... )
  {
  }

  if ( anObjDoc.IsNull() )
    return aResDoc;

  HYDROData_Application* anApp = HYDROData_Application::GetApplication();
  aResDoc = anApp->GetDocument();

  return aResDoc;
}

bool HYDROData_Document::HasDocument()
{
  Handle(HYDROData_Document) aResult = 
    HYDROData_Application::GetApplication()->GetDocument();
  return !aResult.IsNull();
}

Data_DocError HYDROData_Document::Load(const char* theFileName)
{
  DEBTRACE("Load");
  Handle(TDocStd_Document) aResult;
  TCollection_ExtendedString aPath ((const Standard_CString)theFileName);
  PCDM_ReaderStatus aStatus = (PCDM_ReaderStatus) -1;
  try
  {
    aStatus = HYDROData_Application::GetApplication()->Open (aPath, aResult);
  }
  catch (Standard_Failure)
  {}
  if (!aResult.IsNull()) {
    aResult->SetUndoLimit(UNDO_LIMIT);
    HYDROData_Application::GetApplication()->AddDocument(new HYDROData_Document(aResult));
  }
  // recognize error
  Data_DocError anError;
  switch(aStatus) {
  case PCDM_RS_OK:
    anError = DocError_OK;
    break;
  case PCDM_RS_NoDriver:
  case PCDM_RS_UnknownFileDriver:
  case PCDM_RS_NoSchema:
  case PCDM_RS_DriverFailure:
  case PCDM_RS_WrongResource:
    anError = DocError_ResourcesProblem;
    break;
  case PCDM_RS_OpenError:
  case PCDM_RS_NoDocument:
  case PCDM_RS_WrongStreamMode:
  case PCDM_RS_PermissionDenied:
    anError = DocError_CanNotOpen;
    break;
  case PCDM_RS_NoVersion:
    anError = DocError_InvalidVersion;
    break;
  case PCDM_RS_ExtensionFailure:
  case PCDM_RS_FormatFailure:
  case PCDM_RS_TypeFailure:
  case PCDM_RS_TypeNotFoundInSchema:
  case PCDM_RS_UnrecognizedFileFormat:
    anError = DocError_InvalidFormat;
    break;
  case PCDM_RS_MakeFailure:
  default:
    anError = DocError_UnknownProblem;
    break;
  }
  return anError;
}

Data_DocError HYDROData_Document::Save(const char* theFileName)
{
  DEBTRACE("Save");
  TCollection_ExtendedString aPath ((const Standard_CString)theFileName);
  PCDM_StoreStatus aStatus;
  try {
    aStatus = HYDROData_Application::GetApplication()->SaveAs (myDoc, aPath);
  }
  catch (Standard_Failure) {}
  myTransactionsAfterSave = 0;
  Standard::Purge(); // Release free memory

  // recognize error
  Data_DocError anError;
  switch(aStatus) {
  case PCDM_SS_OK:
    anError = DocError_OK;
    break;
  case PCDM_SS_DriverFailure:
    anError = DocError_ResourcesProblem;
    break;
  case PCDM_SS_WriteFailure:
  //case PCDM_SS_DiskWritingFailure:
  //case PCDM_SS_UserRightsFailure:
    anError = DocError_CanNotOpen;
    break;
  default:
    anError = DocError_UnknownProblem;
    break;
  }
  return anError;
}

void HYDROData_Document::Close()
{
  DEBTRACE("Close");
  myDoc->Close();
  HYDROData_Application::GetApplication()->RemoveDocument(this);
}

double HYDROData_Document::GetDefaultStricklerCoefficient() const
{
    double aRes = 0;
    TDF_Label aLabel = myDoc->Main().FindChild(TAG_DEF_STRICKLER_COEFF, Standard_False);
    if ( !aLabel.IsNull() )
    {
        Handle(TDataStd_Real) anAttr;
        if ( aLabel.FindAttribute( TDataStd_Real::GetID(), anAttr ) )
            aRes = anAttr->Get();
    }

    return aRes;
}

void HYDROData_Document::SetDefaultStricklerCoefficient( double theCoeff ) const
{
    TDF_Label aLabel = myDoc->Main().FindChild(TAG_DEF_STRICKLER_COEFF);
    if ( !aLabel.IsNull() )
    {
        Handle(TDataStd_Real) anAttr;
        if ( !aLabel.FindAttribute( TDataStd_Real::GetID(), anAttr ) )
        {
          anAttr = new TDataStd_Real();
          aLabel.AddAttribute(anAttr);
          anAttr->SetID(TDataStd_Real::GetID());
        }
        anAttr->Set( theCoeff );
    }
}

int HYDROData_Document::GetCountQuadtree() const
{
  int nbQuad = 0;
  TDF_Label aLabel = myDoc->Main().FindChild(TAG_COUNT_QUADTREE, Standard_False);
  if ( !aLabel.IsNull() )
  {
      Handle(TDataStd_Integer) anAttr;
      if ( aLabel.FindAttribute( TDataStd_Integer::GetID(), anAttr ) )
          nbQuad = anAttr->Get();
  }
  return nbQuad;
}

void HYDROData_Document::SetCountQuadtree( int nbQuad) const
{
  TDF_Label aLabel = myDoc->Main().FindChild(TAG_COUNT_QUADTREE);
  if ( !aLabel.IsNull() )
  {
      Handle(TDataStd_Integer) anAttr;
      if ( !aLabel.FindAttribute( TDataStd_Integer::GetID(), anAttr ) )
      {
        anAttr = new TDataStd_Integer();
        aLabel.AddAttribute(anAttr);
        anAttr->SetID(TDataStd_Integer::GetID());
        }
      anAttr->Set( nbQuad );
  }
}

int HYDROData_Document::GetCountDelaunay() const
{
  int nbDelaunay = 0;
  TDF_Label aLabel = myDoc->Main().FindChild(TAG_COUNT_DELAUNAY, Standard_False);
  if ( !aLabel.IsNull() )
  {
      Handle(TDataStd_Integer) anAttr;
      if ( aLabel.FindAttribute( TDataStd_Integer::GetID(), anAttr ) )
        nbDelaunay = anAttr->Get();
  }
  return nbDelaunay;
}

void HYDROData_Document::SetCountDelaunay( int nbDelaunay) const
{
  TDF_Label aLabel = myDoc->Main().FindChild(TAG_COUNT_DELAUNAY);
  if ( !aLabel.IsNull() )
  {
      Handle(TDataStd_Integer) anAttr;
      if ( !aLabel.FindAttribute( TDataStd_Integer::GetID(), anAttr ) )
      {
        anAttr = new TDataStd_Integer();
        aLabel.AddAttribute(anAttr);
        anAttr->SetID(TDataStd_Integer::GetID());
      }
      anAttr->Set( nbDelaunay );
  }
}

bool HYDROData_Document::DumpToPython( const QString& thePyScriptPath,
                                       const bool     theIsMultiFile ) const
{
  DEBTRACE("DumpToPython");
  // Try to open the file
  QFile aFile( thePyScriptPath );
  if ( !aFile.open( QIODevice::WriteOnly | QFile::Text ) )
    return false;

  MapOfTreatedObjects aTreatedObjects;

  // Dump header for python script
  QStringList aHeaderDump = DumpToPython( thePyScriptPath, aTreatedObjects, theIsMultiFile );
  if ( aHeaderDump.isEmpty() )
    return false;

  HYDROData_Tool::WriteStringsToFile( aFile, aHeaderDump );

  bool aRes = true;

  // Dump the local CS data to Python 
  UpdateLCSFields();
  QString aLCS = QString( "%1.SetLocalCS( %2, %3 )" ).arg( GetDocPyName() ).arg( myLX, 0, 'f', 3 ).arg( myLY, 0, 'f', 3 );
  if( theIsMultiFile )
    aLCS.prepend( "  " );
  HYDROData_Tool::WriteStringsToFile( aFile, QStringList() << aLCS );

  // Dump all model objects to Python script
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_IMAGE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_STRICKLER_TABLE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_POLYLINEXY );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_BATHYMETRY );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_PROFILE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_POLYLINE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_IMMERSIBLE_ZONE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_STREAM );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_CHANNEL );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_DIGUE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_OBSTACLE );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_LAND_COVER_MAP );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_BC_POLYGON );
  aRes = aRes && dumpPartitionToPython( aFile, thePyScriptPath, theIsMultiFile, aTreatedObjects, KIND_CALCULATION );

  // Dump code to close python fuction
  if ( aRes && theIsMultiFile )
  {
    QStringList aFooterScript;
    aFooterScript << QString( "" );
    aFooterScript << QString( "  pass" );
    HYDROData_Tool::WriteStringsToFile( aFile, aFooterScript );
  }

  return aRes;
}

QString HYDROData_Document::GetDocPyName() const
{
  QString aDocName = PYTHON_DOC_NAME;
  
  return aDocName;
}

QStringList HYDROData_Document::DumpToPython( const QString& thePyScriptPath,
                                              MapOfTreatedObjects& theTreatedObjects,
                                              const bool           theIsMultiFile ) const
{
  DEBTRACE("DumpToPython");
  QString aDocName = GetDocPyName();

  // Append document in to the map of treated objects to prevent names overlaping
  theTreatedObjects.insert( aDocName, this );

  QStringList aResScript;

  aResScript << QString( "from HYDROPy import *" );
  aResScript << QString( "from PyQt5.QtCore import *" );
  aResScript << QString( "from PyQt5.QtGui import *" );

  if ( theIsMultiFile )
  {
    aResScript << QString( "import salome" );
    aResScript << QString( "" );
    aResScript << QString( "def RebuildData():" );
    aResScript << QString( "  %1 = HYDROData_Document.Document()" ).arg( aDocName );
  }
  else
  {
    aResScript << QString( "" );
    aResScript << QString( "%1 = HYDROData_Document.Document()" ).arg( aDocName );
  }

  return aResScript;
}

bool HYDROData_Document::dumpPartitionToPython( QFile&               theFile,
                                                const QString&       thePyScriptPath,
                                                const bool           theIsMultiFile,
                                                MapOfTreatedObjects& theTreatedObjects,
                                                const ObjectKind&    theObjectKind ) const
{
  DEBTRACE("dumpPartitionToPython");
  if ( !theFile.isOpen() )
    return false;

  QTextStream anOutStream( &theFile );

  bool aRes = true;

  HYDROData_Iterator anIterator( this, theObjectKind );
  for( ; anIterator.More(); anIterator.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIterator.Current();
    if ( anObject.IsNull() )
      continue;

    QString anObjName = anObject->GetName();
    if ( theTreatedObjects.contains( anObjName ) )
      continue;

    theTreatedObjects.insert( anObjName, anObject );

    QStringList anObjDump = anObject->DumpToPython( thePyScriptPath, theTreatedObjects );

    if ( theIsMultiFile )
    {
      // For multifile dump we use the function, see the document dump header
      QStringList::iterator anIt = anObjDump.begin();
      for ( ; anIt != anObjDump.end(); ++anIt )
        anIt->prepend( "  " );
    }
    
    HYDROData_Tool::WriteStringsToFile( theFile, anObjDump );
  }
  
  return aRes;
}

bool takeLastDigits( QString& theStr, int& aRes )
{
  aRes = -1;

  QString anStrNum;
  for ( int i = theStr.length() - 1; i >= 0; --i )
  {
    const QChar& aChar = theStr.at( i );
    if ( !aChar.isDigit() )
      break;

    anStrNum.prepend( aChar );
  }

  if ( anStrNum.isEmpty() )
    return false;

  theStr.remove( theStr.length() - anStrNum.length(), anStrNum.length() );
  aRes = anStrNum.toInt();

  return true;
}

bool isObjectNameLessThan( const QString& theStr1, const QString& theStr2 )
{
  QString aStr1 = theStr1, aStr2 = theStr2;

  int aNum1 = -1, aNum2 = -1;
  if ( takeLastDigits( aStr1, aNum1 ) && takeLastDigits( aStr2, aNum2 ) )
  {
    if ( aStr1 == aStr2 )
      return aNum1 < aNum2;
  }

  return theStr1 < theStr2;
}

HYDROData_SequenceOfObjects HYDROData_Document::GetObjectsLayerOrder(
  const Standard_Boolean theIsAll ) const
{
  HYDROData_SequenceOfObjects anOrder;

  MapOfOrdered   aMapOfOrdered;
  MapOfUnordered aMapOfUnordered;

  HYDROData_Iterator anIter( this );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIter.Current();
    if ( anObject.IsNull() || !anObject->IsHas2dPrs() )
      continue;

    Standard_Integer anObjZLevel = -1;
    if ( anObject->GetZLevel( anObjZLevel ) )
    {
      aMapOfOrdered.insert( anObjZLevel, anObject );
    }
    else
    {
      QString anObjName = anObject->GetName();
      if ( anObjName.isEmpty() )
        continue;

      aMapOfUnordered.insert( anObjName, anObject );
    }
  }

  MapOfOrdered::const_iterator anOrderedMapIt = aMapOfOrdered.constBegin();
  for ( ; anOrderedMapIt != aMapOfOrdered.constEnd(); anOrderedMapIt++ )
  {
    const Handle(HYDROData_Entity)& anObject = anOrderedMapIt.value();
    anOrder.Prepend( anObject );
  }

  if ( theIsAll )
  {
    QStringList aSortedNames = aMapOfUnordered.keys();
    qSort( aSortedNames.begin(), aSortedNames.end(), isObjectNameLessThan );

    for ( int i = 0; i < aSortedNames.length(); ++i )
    {
      QString anObjName = aSortedNames.value( i );

      const Handle(HYDROData_Entity)& anObject = aMapOfUnordered.value( anObjName );
      anOrder.Append( anObject );
    }
  }

  return anOrder;
}

void HYDROData_Document::SetObjectsLayerOrder( const HYDROData_SequenceOfObjects& theOrder )
{
  // At first we remove previous model order
  RemoveObjectsLayerOrder();

  // Make new objects order
  Standard_Integer aLevel = 0;
  for ( int i = theOrder.Length(), n = 1; i >= n; --i )
  {
    const Handle(HYDROData_Entity)& anObject = theOrder.Value( i );
    if ( anObject.IsNull() || !anObject->IsHas2dPrs() )
      continue;

    anObject->SetZLevel( aLevel++ );
  }
}

void HYDROData_Document::Show( const Handle(HYDROData_Entity)& theObject )
{
  HYDROData_SequenceOfObjects anOrder;
  anOrder.Append( theObject );
  Show( anOrder );
}

void HYDROData_Document::Show( const HYDROData_SequenceOfObjects& theObjects )
{
  MapOfUnordered aMapOfUnordered;

  for ( int i = 1, n = theObjects.Length(); i <= n; ++i )
  {
    const Handle(HYDROData_Entity)& anObject = theObjects.Value( i );
    if ( anObject.IsNull() || !anObject->IsHas2dPrs() )
      continue;

    Standard_Integer anObjZLevel = -1;
    if ( anObject->GetZLevel( anObjZLevel ) )
    {
      continue; // Skip objects that already have the z-level
    }
    else
    {
      QString anObjName = anObject->GetName();
      if ( anObjName.isEmpty() )
        continue;

      aMapOfUnordered.insert( anObjName, anObject );
    }
  }

  if ( aMapOfUnordered.isEmpty() )
    return; // Nothing to show

  Standard_Integer aTopId = 0;

  HYDROData_SequenceOfObjects aModelOrder = GetObjectsLayerOrder( Standard_False );
  if ( !aModelOrder.IsEmpty() )
  {
    const Handle(HYDROData_Entity)& anObject = aModelOrder.First();
    anObject->GetZLevel( aTopId );
    aTopId++;
  }

  aTopId += aMapOfUnordered.size() - 1;

  QStringList aSortedNames = aMapOfUnordered.keys();
  qSort( aSortedNames.begin(), aSortedNames.end(), isObjectNameLessThan );

  for ( int i = 0; i < aSortedNames.length(); ++i )
  {
    QString anObjName = aSortedNames.value( i );

    const Handle(HYDROData_Entity)& anObject = aMapOfUnordered.value( anObjName );
    anObject->SetZLevel( aTopId-- );
  }
}

void HYDROData_Document::RemoveObjectsLayerOrder()
{
  HYDROData_Iterator anIter( this );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIter.Current();
    if ( anObject.IsNull() || !anObject->IsHas2dPrs() )
      continue;

    anObject->RemoveZLevel();
  }
}

void HYDROData_Document::StartOperation()
{
  myDoc->NewCommand();
}

void HYDROData_Document::CommitOperation(const TCollection_ExtendedString& theName)
{
  if( !myDoc->CommitCommand() ) // it means that there were no modifications done
  {
    myDoc->NewCommand();
    NewID(); // workaround: do something just to modify the document
    myDoc->CommitCommand();
  }
  myTransactionsAfterSave++;

  if( theName.Length() != 0 )
  {
    const TDF_DeltaList& aList = GetUndos();
    if( !aList.IsEmpty() )
    {
      Handle(TDF_Delta) aDelta = aList.Last();
      if( !aDelta.IsNull() )
        aDelta->SetName( theName );
    }
  }
}

void HYDROData_Document::AbortOperation()
{
  myDoc->AbortCommand();
}

bool HYDROData_Document::IsOperation()
{
  return myDoc->HasOpenCommand() != 0;
}

bool HYDROData_Document::IsModified()
{
  return myTransactionsAfterSave != 0;
}

bool HYDROData_Document::CanUndo()
{
  return myDoc->GetAvailableUndos() > 0;
}

const TDF_DeltaList& HYDROData_Document::GetUndos()
{
  return myDoc->GetUndos();
}

void HYDROData_Document::ClearUndos()
{
  return myDoc->ClearUndos();
}

void HYDROData_Document::Undo()
{
  myDoc->Undo();
  myTransactionsAfterSave--;
}

bool HYDROData_Document::CanRedo()
{
  return myDoc->GetAvailableRedos() > 0;
}

const TDF_DeltaList& HYDROData_Document::GetRedos()
{
  return myDoc->GetRedos();
}

void HYDROData_Document::ClearRedos()
{
  return myDoc->ClearRedos();
}

void HYDROData_Document::Redo()
{
  myDoc->Redo();
  myTransactionsAfterSave++;
}

Handle(HYDROData_Entity) HYDROData_Document::CreateObject( const ObjectKind theKind )
{
  return HYDROData_Iterator::CreateObject( this, theKind );
}

Handle(HYDROData_Entity) HYDROData_Document::FindObjectByName( 
  const QString&   theName,
  const ObjectKind theObjectKind ) const
{
  Handle(HYDROData_Entity) anObject;
  if ( theName.isEmpty() )
    return anObject;

  QStringList aNamesList;
  aNamesList << theName;

  HYDROData_SequenceOfObjects aSeqOfObjs = FindObjectsByNames( aNamesList, theObjectKind );
  if( aSeqOfObjs.IsEmpty() )
    return anObject;
  
  anObject = aSeqOfObjs.First();
  return anObject;
}

HYDROData_SequenceOfObjects HYDROData_Document::FindObjectsByNames(
  const QStringList& theNames, 
  const ObjectKind   theObjectKind ) const
{
  HYDROData_SequenceOfObjects aResSeq;

  QStringList aNamesList = theNames;

  HYDROData_Iterator anIter( this, theObjectKind );
  for( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIter.Current();
    if( anObject.IsNull() )
      continue;

    QString anObjName = anObject->GetName();
    if ( anObjName.isEmpty() || !aNamesList.contains( anObjName ) )
      continue;

    aResSeq.Append( anObject );

    aNamesList.removeAll( anObjName );
    if ( aNamesList.isEmpty() )
      break;
  }

  return aResSeq;
}

HYDROData_SequenceOfObjects HYDROData_Document::CollectAllObjects( const ObjectKind theObjectKind ) const
{
  HYDROData_SequenceOfObjects aResSeq;
  HYDROData_Iterator anIter( this, theObjectKind );
  for( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIter.Current();
    if( anObject.IsNull() )
      continue;
    aResSeq.Append( anObject );
  }
  return aResSeq;
}

HYDROData_Document::HYDROData_Document()
{
  DEBTRACE("HYDROData_Document");
  HYDROData_Application::GetApplication()->NewDocument("BinOcaf", myDoc);
  myDoc->SetUndoLimit(UNDO_LIMIT);
  NewID(); // needed to have at least one attribute in initial document to avoid errors
  myTransactionsAfterSave = 0;
  myLX = -1;
  myLY = -1;

  myInterpolatorsFactory = 0;
}

HYDROData_Document::HYDROData_Document(const Handle(TDocStd_Document)& theDoc)
{
  DEBTRACE("HYDROData_Document");
  myDoc = theDoc;
  myTransactionsAfterSave = 0;
  myLX = -1;
  myLY = -1;

  myInterpolatorsFactory = 0;
}

HYDROData_Document::~HYDROData_Document()
{
  DEBTRACE("~HYDROData_Document");
}

int HYDROData_Document::NewID()
{
  TDF_Label anIDLab = myDoc->Main().FindChild(TAG_PROPS).
    FindChild(TAG_PROPS_NEW_ID);
  Handle(TDataStd_Integer) anInt;
  if (!anIDLab.FindAttribute(TDataStd_Integer::GetID(), anInt)) {
    anInt = TDataStd_Integer::Set(anIDLab, 0);
    anInt->SetID(TDataStd_Integer::GetID());
  }
  // just increment value and return
  anInt->Set(anInt->Get() + 1);
  return anInt->Get();
}

TDF_Label HYDROData_Document::LabelOfObjects()
{
  return myDoc->Main().FindChild(TAG_OBJECTS);
}

TDF_Label HYDROData_Document::LabelOfLocalCS() const
{
  return myDoc->Main().FindChild(TAG_LOCAL_CS);
}

void HYDROData_Document::GetLocalCS( double& theLX, double& theLY ) const
{
  TDF_Label aLocalCSLab = LabelOfLocalCS();

  Handle( TDataXtd_Position ) aLocalCS;
  if( aLocalCSLab.FindAttribute( TDataXtd_Position::GetID(), aLocalCS ) )
  {
    gp_Pnt aLocalCS3d = aLocalCS->GetPosition();
    theLX = aLocalCS3d.X();
    theLY = aLocalCS3d.Y();
  }
  else
  {
    theLX = DEFAULT_LOCAL_CS.X();
    theLY = DEFAULT_LOCAL_CS.Y();
  }
}

void HYDROData_Document::SetLocalCS( double theLX, double theLY )
{
  UpdateLCSFields();

  // update the local CS data in attribute
  TDF_Label aLocalCSLab = LabelOfLocalCS();
  Handle( TDataXtd_Position ) aLocalCS;
  if( !aLocalCSLab.FindAttribute( TDataXtd_Position::GetID(), aLocalCS ) )
  {
    aLocalCS = TDataXtd_Position::Set( aLocalCSLab );
    aLocalCS->SetID(TDataXtd_Position::GetID());
  }

  gp_Pnt aLocalCS3d( theLX, theLY, 0 );
  aLocalCS->SetPosition( aLocalCS3d );

  // calculate delta for coordinates
  double aDX = myLX - theLX;
  double aDY = myLY - theLY;

  // update the local CS data in internal fields
  myLX = theLX;
  myLY = theLY;

  //update all objects in the document
  HYDROData_Iterator anIterator( this, KIND_UNKNOWN );
  for( ; anIterator.More(); anIterator.Next() )
    anIterator.Current()->UpdateLocalCS( aDX, aDY );
}

void HYDROData_Document::UpdateLCSFields() const
{
  if( myLX >= 0 && myLY >= 0 )
    return;

  double aLX, aLY;
  GetLocalCS( aLX, aLY );
  HYDROData_Document* aThat = const_cast<HYDROData_Document*>( this );
  aThat->myLX = aLX;
  aThat->myLY = aLY;
}

void HYDROData_Document::Transform( double& X, double& Y, bool IsToLocalCS ) const
{
  UpdateLCSFields();
  if( IsToLocalCS )
  {
    X -= myLX;
    Y -= myLY;
  }
  else
  {
    X += myLX;
    Y += myLY;
  }
}

void HYDROData_Document::Transform( gp_Pnt& thePnt, bool IsToLocalCS ) const
{
  double X = thePnt.X();
  double Y = thePnt.Y();
  double Z = thePnt.Z();
  Transform( X, Y, IsToLocalCS );
  thePnt = gp_Pnt( X, Y, Z ); 
}

void HYDROData_Document::Transform( gp_XYZ& thePnt, bool IsToLocalCS ) const
{
  double X = thePnt.X();
  double Y = thePnt.Y();
  double Z = thePnt.Z();
  Transform( X, Y, IsToLocalCS );
  thePnt = gp_XYZ( X, Y, Z ); 
}

void HYDROData_Document::Transform( double& X, double& Y, double& Z, bool IsToLocalCS ) const
{
  Transform( X, Y, IsToLocalCS );
}

void HYDROData_Document::Transform( gp_XY& thePnt, bool IsToLocalCS ) const
{
  double X = thePnt.X();
  double Y = thePnt.Y();
  Transform( X, Y, IsToLocalCS );
  thePnt = gp_XY( X, Y ); 
}

HYDROData_InterpolatorsFactory* HYDROData_Document::GetInterpolatorsFactory()
{
  if ( !myInterpolatorsFactory ) {
    myInterpolatorsFactory = new HYDROData_InterpolatorsFactory();
  }

  return myInterpolatorsFactory;
}

HYDROData_IProfilesInterpolator* HYDROData_Document::GetInterpolator( const TCollection_AsciiString& theName ) const
{
  HYDROData_IProfilesInterpolator* anInterpolator = NULL;

  HYDROData_Document* aThat = const_cast<HYDROData_Document*>( this );
  HYDROData_InterpolatorsFactory* aFactory = aThat->GetInterpolatorsFactory();
  if ( aFactory ) {
    anInterpolator = aFactory->GetInterpolator( theName );
  }

  return anInterpolator;
}
 
NCollection_Sequence<TCollection_AsciiString> HYDROData_Document::GetInterpolatorNames() const
{
  NCollection_Sequence<TCollection_AsciiString> aNames;

  HYDROData_Document* aThat = const_cast<HYDROData_Document*>( this );
  HYDROData_InterpolatorsFactory* aFactory = aThat->GetInterpolatorsFactory();
  if ( aFactory ) {
    aNames = aFactory->GetInterpolatorNames();
  }

  return aNames;
}

QColor HYDROData_Document::GetAssociatedColor( const QString& theStricklerType, const Handle(HYDROData_StricklerTable)& theTable ) const
{
  if( !theTable.IsNull() && theTable->HasType( theStricklerType ) )
    return theTable->GetColor( theStricklerType );

  HYDROData_Iterator anIt( this, KIND_STRICKLER_TABLE );
  for( ; anIt.More(); anIt.Next() )
  {
    Handle(HYDROData_StricklerTable) aTable = Handle(HYDROData_StricklerTable)::DownCast( anIt.Current() );
    if( aTable->HasType( theStricklerType ) )
      return aTable->GetColor( theStricklerType );
  }
  return QColor();
}

void HYDROData_Document::CollectQGISValues( const QString& theAttrName,
                                            QStringList& theAttrValues,
                                            QStringList& theStricklerTypes ) const
{
  HYDROData_Iterator It( this, KIND_STRICKLER_TABLE );
  for( ; It.More(); It.Next() )
  {
    Handle(HYDROData_StricklerTable) aTable = Handle(HYDROData_StricklerTable)::DownCast( It.Current() );
    if( !aTable.IsNull() && aTable->GetAttrName()==theAttrName )
    {
      theAttrValues.clear();
      theStricklerTypes = aTable->GetTypes();
      foreach( QString aType, theStricklerTypes )
      {
        QString anAttrValue = aTable->GetAttrValue( aType );
        theAttrValues.append( anAttrValue );
      }
    }
  }
}

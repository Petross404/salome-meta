
#include <HYDROData_DTM.h>
#include <HYDROData_Profile.h>

#include <Geom2d_BSplineCurve.hxx>
#include <Geom2dAPI_Interpolate.hxx>
#include <TColgp_HArray1OfPnt2d.hxx>
#include <TColgp_Array1OfVec2d.hxx>
#include <TColStd_HArray1OfBoolean.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopExp_Explorer.hxx>
#include <BRep_Tool.hxx>
#include <gp_Ax3.hxx>
#include <Geom_Line.hxx>
#include <Geom2d_Line.hxx>
#include <Geom2d_TrimmedCurve.hxx>
#include <Geom_BSplineCurve.hxx>
#include <Geom2d_BSplineCurve.hxx>
#include <GeomAPI_Interpolate.hxx>
#include <TColStd_Array1OfReal.hxx>
#include <TColStd_Array1OfInteger.hxx>
#include <TColgp_Array1OfPnt.hxx>
#include <TColgp_Array1OfVec.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <Geom2dAPI_InterCurveCurve.hxx>
#include <Geom2dAPI_ProjectPointOnCurve.hxx>
#include <Geom2dAdaptor_Curve.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <limits>
#include <Bnd_Box2d.hxx>

#include <BRepLib_MakeEdge.hxx>
#include <BRepLib_MakeWire.hxx>
#include <BRep_Builder.hxx>
#include <GeomProjLib.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <BRepTools_WireExplorer.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfOrientedShape.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>

#include <BRepLib_MakeEdge.hxx>
#include <BRepLib_MakeWire.hxx>
#include <BRep_Builder.hxx>
#include <ShapeAnalysis_Wire.hxx>
#include <BRepAlgo_NormalProjection.hxx>
#include <ShapeUpgrade_UnifySameDomain.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BOPAlgo_Builder.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <GeomProjLib.hxx>
#include <gp_Pln.hxx>
#include <TopTools_IndexedDataMapOfShapeShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>
#include <assert.h>
#include <NCollection_DataMap.hxx>
#include <QSet>
#include <QString>
#include <float.h>

double PREC = 0.001;
IMPLEMENT_STANDARD_RTTIEXT( HYDROData_DTM, HYDROData_Bathymetry )

HYDROData_DTM::CurveUZ::CurveUZ( double theXCurv, const gp_Vec2d& theProfileDir, double theDeltaZ, double theMaxZ )
  : myXcurv( theXCurv ), myProfileDir( theProfileDir ), myDeltaZ( theDeltaZ ), myMaxZ (theMaxZ)
{
}

HYDROData_DTM::CurveUZ::~CurveUZ()
{
}

double HYDROData_DTM::CurveUZ::Xcurv() const
{
  return myXcurv;
}

gp_Vec2d HYDROData_DTM::CurveUZ::ProfileDir() const
{
  return myProfileDir;
}

double HYDROData_DTM::CurveUZ::DeltaZ() const
{
  return myDeltaZ;
}

double HYDROData_DTM::CurveUZ::MaxZ() const
{
  return myMaxZ;
}

HYDROData_DTM::CurveUZ HYDROData_DTM::CurveUZ::operator + ( const CurveUZ& c ) const
{
  HYDROData_DTM::CurveUZ res( Xcurv() + c.Xcurv(), ProfileDir() + c.ProfileDir(), DeltaZ() + c.DeltaZ(), MaxZ() + c.MaxZ() );
  size_t n = size(), n1 = c.size();
  if( n!=n1 )
  {
    std::cout << "Warning: different number of points in curves: " << n << ", " << n1 << std::endl;
  }
  int q = n < n1 ? n : n1;
  res.reserve( q );
  for( int i=0; i<q; i++ )
  {
    PointUZ p;
    p.U = operator[]( i ).U + c[i].U;
    p.Z = operator[]( i ).Z;
    res.push_back( p );
  }
  return res;
}

HYDROData_DTM::CurveUZ HYDROData_DTM::CurveUZ::operator * ( double d ) const
{
  HYDROData_DTM::CurveUZ res( Xcurv()*d, ProfileDir()*d, DeltaZ()*d, MaxZ()*d );
  size_t n = size();
  res.reserve( n );
  for( int i=0; i<n; i++ )
  {
    PointUZ p;
    p.U = operator[]( i ).U * d;
    p.Z = operator[]( i ).Z;
    res.push_back( p );
  }
  return res;
}




HYDROData_DTM::HYDROData_DTM()
{
}

HYDROData_DTM::~HYDROData_DTM()
{
}

HYDROData_SequenceOfObjects HYDROData_DTM::GetProfiles() const
{
  return GetReferenceObjects( DataTag_Profiles );
}

void HYDROData_DTM::SetProfiles( const HYDROData_SequenceOfObjects& theProfiles )
{
  SetReferenceObjects( theProfiles, DataTag_Profiles );
  Changed( Geom_3d );
}

double HYDROData_DTM::GetDDZ() const
{
  return GetDouble( DataTag_DDZ );
}

void HYDROData_DTM::SetDDZ( double theDDZ )
{
  SetDouble( DataTag_DDZ, theDDZ );
  Changed( Geom_3d );
}

double HYDROData_DTM::GetSpatialStep() const
{
  return GetDouble( DataTag_SpatialStep );
}

void HYDROData_DTM::SetSpatialStep( double theSpatialStep )
{
  SetDouble( DataTag_SpatialStep, theSpatialStep );
  Changed( Geom_3d );
}

void HYDROData_DTM::PointsToWire(const AltitudePoints& pnts, TopoDS_Wire& W )
{
 
  BRepBuilderAPI_MakePolygon PM;
  for (int i = 0; i < pnts.size(); i++)
    PM.Add(gp_Pnt(pnts[i].X, pnts[i].Y, pnts[i].Z));
  
  W = PM.Wire();
}

void HYDROData_DTM::PointsToEdge(const AltitudePoints& pnts, TopoDS_Edge& E )
{ 
  Handle(TColgp_HArray1OfPnt) gpPoints = new TColgp_HArray1OfPnt(1, (int)pnts.size());

  for (int i = 0; i < pnts.size(); i++)
    gpPoints->SetValue(i+1, gp_Pnt(pnts[i].X, pnts[i].Y, pnts[i].Z));

  GeomAPI_Interpolate anInterpolator(gpPoints, Standard_False,1.0e-6);
  anInterpolator.Perform() ;
  if (anInterpolator.IsDone()) 
  {
    Handle(Geom_BSplineCurve) C = anInterpolator.Curve();
    E = BRepBuilderAPI_MakeEdge(C).Edge();
  }
}

TopTools_IndexedMapOfOrientedShape HYDROData_DTM::Create3DShape(const AltitudePoints& left,
                                                                const AltitudePoints& right,
                                                                const std::vector<AltitudePoints>& main_profiles)
{  
  TopTools_IndexedMapOfOrientedShape ll;
  //TopoDS_Wire LWire, RWire;
  //PointsToWire(left, LWire);
  //PointsToWire(right, RWire);
  TopoDS_Edge LEdge, REdge;
  PointsToEdge(left, LEdge);
  PointsToEdge(right, REdge);
  if (!LEdge.IsNull())
    ll.Add(LEdge.Oriented(TopAbs_FORWARD));

  for (int k = 0; k < main_profiles.size(); k++)
  {
    TopoDS_Wire W;
    PointsToWire(main_profiles[k], W);
    TopAbs_Orientation Ori = TopAbs_INTERNAL;
    if (k == 0 || k == main_profiles.size() - 1)
      Ori = TopAbs_FORWARD;
    ll.Add(W.Oriented(Ori));
  }

  if (!REdge.IsNull())
    ll.Add(REdge.Oriented(TopAbs_FORWARD)); 
  //yes, add subshapes in this order (left + profiles + right)
  //otherwise the projected wire will be non-manifold

  return ll;
}


void HYDROData_DTM::Update()
{
  AltitudePoints points;
  TopoDS_Shape Out3dPres;
  TopoDS_Shape Out2dPres;
  TopoDS_Shape OutLeftB;
  TopoDS_Shape OutRightB;
  TopoDS_Shape OutInlet;
  TopoDS_Shape OutOutlet;

  HYDROData_SequenceOfObjects objs = GetProfiles();  
  double ddz = GetDDZ();
  double step = GetSpatialStep();
  std::set<int> InvInd;
  bool WireIntersections; //__TODO
  myWarnings.Clear();
  bool ToEstimateWarnings; //NOT NEEDED here
  CreateProfilesFromDTM( objs, ddz, step, points, Out3dPres, Out2dPres, OutLeftB, OutRightB, OutInlet, OutOutlet,
    true, true, InvInd, -1, WireIntersections, myWarnings, ToEstimateWarnings );
  SetAltitudePoints( points );  
  
  SetShape( DataTag_LeftBankShape, OutLeftB);
  SetShape( DataTag_RightBankShape, OutRightB);
  SetShape( DataTag_InletShape, OutInlet);
  SetShape( DataTag_OutletShape, OutOutlet );
  SetShape( DataTag_3DShape, Out3dPres );
  SetShape( DataTag_2DShape, Out2dPres );

  HYDROData_Bathymetry::Update();
}

void HYDROData_DTM::GetPresentationShapes( TopoDS_Shape& Out3dPres,
                                           TopoDS_Shape& Out2dPres,
                                           TopoDS_Shape& OutLeftB,
                                           TopoDS_Shape& OutRightB,
                                           TopoDS_Shape& OutInlet,
                                           TopoDS_Shape& OutOutlet )
{
  //without update!
  OutLeftB = GetShape( DataTag_LeftBankShape);
  OutRightB = GetShape( DataTag_RightBankShape);
  OutInlet = GetShape( DataTag_InletShape);
  OutOutlet = GetShape( DataTag_OutletShape );
  Out3dPres = GetShape( DataTag_3DShape );
  Out2dPres = GetShape( DataTag_2DShape );
}
void HYDROData_DTM::CreateProfilesFromDTM (const HYDROData_SequenceOfObjects& InpProfiles,
                                           double ddz,
                                           double step, 
                                           AltitudePoints& points,
                                           TopoDS_Shape& Out3dPres,
                                           TopoDS_Shape& Out2dPres,
                                           TopoDS_Shape& OutLeftB,
                                           TopoDS_Shape& OutRightB,
                                           TopoDS_Shape& OutInlet,
                                           TopoDS_Shape& OutOutlet,
                                           bool Create3dPres,
                                           bool Create2dPres,
                                           std::set<int>& InvInd,
                                           int thePntsLimit,
                                           bool& WireIntersections, 
                                           NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
                                           bool& ToEstimateWarnings)
{
  int aLower = InpProfiles.Lower(), anUpper = InpProfiles.Upper();
  size_t n = anUpper - aLower + 1;

  std::vector<Handle(HYDROData_Profile)> profiles;
  profiles.reserve( n ); 
  for( int i=aLower; i<=anUpper; i++ )
  {
    Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( InpProfiles.Value( i ) );
    if( !aProfile.IsNull() )
      profiles.push_back( aProfile );
  }
  const double EPS = 1E-3;
  AltitudePoints left;
  AltitudePoints right;
  std::vector<AltitudePoints> main_profiles;

  bool ToEstimateWarningsOnly = false;
  if( thePntsLimit > 0 )
  {
    int aNbPoints = EstimateNbPoints( profiles, ddz, step );
    if( aNbPoints < 0 || aNbPoints > thePntsLimit )
    {
      ToEstimateWarningsOnly = true;
      //return;
    }
  }

  if( profiles.size() < 2 )
    return;

  if( ddz>EPS && step>EPS )
    CreateProfiles(profiles, ddz, step, left, right, points, main_profiles, 
    Out3dPres, Out2dPres, OutLeftB, OutRightB, OutInlet, OutOutlet, Create3dPres, 
    Create2dPres, InvInd, WireIntersections, warnings, ToEstimateWarningsOnly );

  ToEstimateWarnings = ToEstimateWarningsOnly;
}

bool HYDROData_DTM::GetPlanarFaceFromBanks( const TopoDS_Edge& LB, const TopoDS_Edge& RB, TopoDS_Face& outF,
  TopTools_SequenceOfShape* Boundr)
{
  BRep_Builder BB;
  TopoDS_Face F;
  Handle_Geom_Plane refpl = new Geom_Plane(gp_Pnt(0,0,0), gp_Dir(0,0,1));

  TopoDS_Vertex VFI, VLI, VFO, VLO;
  TopoDS_Edge prLB;
  TopoDS_Edge prRB;


  BRepAdaptor_Curve LBAD(LB);
  Handle_Geom_Curve LBPC = GeomProjLib::ProjectOnPlane(LBAD.Curve().Curve(), refpl, gp_Dir(0, 0, -1), 1 );
  prLB = BRepLib_MakeEdge(LBPC).Edge();

  BRepAdaptor_Curve RBAD(RB);
  Handle_Geom_Curve RBPC = GeomProjLib::ProjectOnPlane(RBAD.Curve().Curve(), refpl, gp_Dir(0, 0, -1), 1 );
  prRB = BRepLib_MakeEdge(RBPC).Edge();

  TopExp::Vertices(prLB, VFI, VFO, 1);
  TopExp::Vertices(prRB, VLI, VLO, 1);
  TopoDS_Edge prIL = BRepLib_MakeEdge(VFI, VLI).Edge();
  TopoDS_Edge prOL = BRepLib_MakeEdge(VFO, VLO).Edge();
  TopoDS_Wire prW = BRepLib_MakeWire(prLB, prIL, prOL, prRB).Wire();
  outF = BRepBuilderAPI_MakeFace(refpl->Pln(), prW, 1).Face();

  if (Boundr)
  {
    Boundr->Append(prLB);
    Boundr->Append(prIL);
    Boundr->Append(prOL);
    Boundr->Append(prRB);
  }

  ShapeAnalysis_Wire WA(prW, outF, Precision::Confusion());
  bool res = WA.CheckSelfIntersection();
  return !res;
}

void HYDROData_DTM::CreateProfiles(const std::vector<Handle(HYDROData_Profile)>& theProfiles,
                                   double theDDZ,
                                   double theSpatialStep,
                                   AltitudePoints& theOutLeft,
                                   AltitudePoints& theOutRight,
                                   AltitudePoints& theOutPoints,
                                   std::vector<AltitudePoints>& theOutMainProfiles,
                                   TopoDS_Shape& Out3dPres,
                                   TopoDS_Shape& Out2dPres,
                                   TopoDS_Shape& OutLeftB,
                                   TopoDS_Shape& OutRightB,
                                   TopoDS_Shape& OutInlet,
                                   TopoDS_Shape& OutOutlet,
                                   bool Create3dPres,
                                   bool Create2dPres,
                                   std::set<int>& InvInd,
                                   bool& ProjStat,
                                   NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
                                   bool ToEstimateWarningsOnly)
{
  if (theProfiles.empty())
    return;
  theOutPoints = Interpolate( theProfiles, theDDZ, theSpatialStep, theOutLeft, theOutRight,
    theOutMainProfiles, InvInd, warnings, ToEstimateWarningsOnly );
  //note that if Create3dPres is false => Create2dPres flag is meaningless!
  if( theOutPoints.empty() )
    return;

  if (Create3dPres)
  {
    TopTools_IndexedMapOfOrientedShape ll = Create3DShape( theOutLeft, theOutRight, theOutMainProfiles);
    
    if (ll.IsEmpty())
      return;
    BRep_Builder BB;
    TopoDS_Compound cmp;
    BB.MakeCompound(cmp);
    for (int i = 1; i <= ll.Extent(); i++)
      BB.Add(cmp, ll(i));

    Out3dPres = cmp;

    //same order as in HYDROData_DTM::Update()
    OutLeftB = ll(1);
    OutRightB = ll(ll.Extent());
    OutInlet = ll(2);
    OutOutlet = ll(ll.Extent() - 1);

    if (Create2dPres)
    {
      TopoDS_Face outF;
      ProjStat = GetPlanarFaceFromBanks(TopoDS::Edge(OutLeftB), TopoDS::Edge(OutRightB), outF, NULL);
      Out2dPres = outF;
    };
  }
}




void HYDROData_DTM::GetProperties( const Handle(HYDROData_Profile)& theProfile,
                    gp_Pnt& theLowestPoint, gp_Vec2d& theDir,
                    double& theZMin, double& theZMax )
{
  theLowestPoint = theProfile->GetBottomPoint();
  
  gp_XY aLeft, aRight;
  theProfile->GetLeftPoint( aLeft, true, true );
  theProfile->GetRightPoint( aRight, true, true );
  double x = aRight.X()-aLeft.X();
  double y = aRight.Y()-aLeft.Y();
  theDir = gp_Vec2d( x, y );

  HYDROData_Profile::ProfilePoints points = theProfile->GetProfilePoints();
  int lo = points.Lower();
  int up = points.Upper();
  theZMin = DBL_MAX;
  theZMax = -theZMin;
  for( int i=lo; i<=up; i++ )
  {
    double z = points.Value( i ).Z();
    if( z>theZMax )
      theZMax = z;
    if( z<theZMin )
      theZMin = z;
  }
}

inline gp_Pnt2d To2D( const gp_Pnt& thePnt, const gp_Trsf& theTr,
                      double& theUMin, double& theUMax )
{
  gp_Pnt p = thePnt.Transformed( theTr );
  double u = p.X();
  double z = p.Z();
  if( u<theUMin )
    theUMin = u;
  if( u>theUMax )
    theUMax = u;
  return gp_Pnt2d( u, z );
}

Handle(TColgp_HArray1OfPnt2d) To2D( const TColgp_Array1OfPnt& thePoints,
                                    const gp_Trsf& theTr,
                                    double& theUMin, double& theUMax )
{
  int low = thePoints.Lower(), up = thePoints.Upper();
  Handle(TColgp_HArray1OfPnt2d) points = new TColgp_HArray1OfPnt2d( low, up );
  for( int i=low; i<=up; i++ )
    points->SetValue( i, To2D( thePoints.Value( i ), theTr, theUMin, theUMax ) );
  return points;
}

Handle(Geom2d_Curve) CurveTo2D( const Handle(Geom_Curve)& theCurve, 
                                Standard_Real theFirst, Standard_Real theLast,
                                const gp_Trsf& theTr,
                                double& theUMin, double& theUMax )
{
  if( theCurve->IsKind( STANDARD_TYPE( Geom_Line ) ) )
  {
    gp_Pnt aFirstPnt, aLastPnt;
    theCurve->D0( theFirst, aFirstPnt );
    theCurve->D0( theLast, aLastPnt );

    gp_Pnt2d
      aFirst2d = To2D( aFirstPnt, theTr, theUMin, theUMax ),
      aLast2d = To2D( aLastPnt, theTr, theUMin, theUMax );

    gp_Vec2d dir( aFirst2d, aLast2d );
    Handle(Geom2d_Line) aLine2d = new Geom2d_Line( aFirst2d, gp_Dir2d( dir.X(), dir.Y() ) );
    return new Geom2d_TrimmedCurve( aLine2d, 0, aLast2d.Distance( aFirst2d ) );
  }

  if( theCurve->IsKind( STANDARD_TYPE( Geom_BSplineCurve ) ) )
  {
    Handle(Geom_BSplineCurve) aSpline = Handle(Geom_BSplineCurve)::DownCast( theCurve );

    Handle(TColgp_HArray1OfPnt2d) poles = To2D( aSpline->Poles(), theTr, theUMin, theUMax );
    const TColStd_Array1OfReal& knots = aSpline->Knots();
    const TColStd_Array1OfInteger& multiplicities = aSpline->Multiplicities();
    int aDegree = aSpline->Degree();

    return new Geom2d_BSplineCurve( poles->Array1(), knots, multiplicities, aDegree );
  }

  return Handle(Geom2d_Curve)();
}

#include <GCE2d_MakeSegment.hxx>
#include <Geom2dAPI_InterCurveCurve.hxx>
bool IsCooriented( const Handle(HYDROData_Profile)& theProfile1,
                   const Handle(HYDROData_Profile)& theProfile2 ) 
{
  if( theProfile1==theProfile2 )
    return true;
  
  gp_XY lp1, rp1, lp2, rp2;
  theProfile1->GetLeftPoint(lp1);
  theProfile1->GetRightPoint(rp1);
  theProfile2->GetLeftPoint(lp2);
  theProfile2->GetRightPoint(rp2);

  GCE2d_MakeSegment s1(lp1, lp2);
  GCE2d_MakeSegment s2(rp1, rp2);

  Geom2dAPI_InterCurveCurve inter;
  inter.Init(s1.Value(), s2.Value());
  if (inter.NbPoints() == 0)
    return true;
  else
    return false;
}

Handle(Geom2d_BSplineCurve) HYDROData_DTM::CreateHydraulicAxis( 
  const std::vector<Handle(HYDROData_Profile)>& theProfiles,
  std::vector<double>& theDistances )
{
  size_t n = theProfiles.size();
  if( n==1 )
    return Handle_Geom2d_BSplineCurve();

  Handle_Geom2d_BSplineCurve aResult;

  Handle(TColgp_HArray1OfPnt2d) points = new TColgp_HArray1OfPnt2d( 1, (int)n );
  TColgp_Array1OfVec2d tangents( 1, (int)n );
  Handle(TColStd_HArray1OfBoolean) flags = new TColStd_HArray1OfBoolean( 1, (int)n );

  // Stage 1. Orient all profiles to be co-oriented with the first profile
  theProfiles[0]->Update();
  for( size_t i = 1; i < n; i++ )
  {
    Handle(HYDROData_Profile) aProfile = theProfiles[i];
    Handle(HYDROData_Profile) aPrevProfile = theProfiles[i-1];

    if( !IsCooriented( aProfile, aPrevProfile ) )
    {
      gp_XY lp, rp;
      aProfile->GetLeftPoint( lp, true );
      aProfile->GetRightPoint( rp, true );
      aProfile->SetLeftPoint( rp, true );
      aProfile->SetRightPoint( lp, true );
    }
    aProfile->Update();
  }

  // Stage 2. Calculate normals so that each normal "points" to the next profile
  for( size_t i = 0; i < n; i++ )
  {
    Handle(HYDROData_Profile) aProfile = theProfiles[i];
    Handle(HYDROData_Profile) aNextProfile = i==n-1 ? theProfiles[i-1] : theProfiles[i+1];

    gp_Pnt aLowest;
    gp_Vec2d aNormal;
    double zmin, zmax;

    gp_XYZ curP = aProfile->GetBottomPoint(true);
    gp_XY curP2d = gp_XY(curP.X(), curP.Y());

    gp_XYZ nextP = aNextProfile->GetBottomPoint(true);
    gp_XY nextP2d = gp_XY(nextP.X(), nextP.Y());

    gp_Vec2d aPrTangent;
    GetProperties( aProfile, aLowest, aPrTangent, zmin, zmax );
    aNormal.SetCoord( -aPrTangent.Y(), aPrTangent.X() );

    gp_Vec2d aDirToNextProfile(nextP2d.X() - curP2d.X(), nextP2d.Y() - curP2d.Y() );
    if( i==n-1 )
      aDirToNextProfile.Reverse();

    if (aNormal.Dot(aDirToNextProfile) < 0)
      aNormal.Reverse();

    aNormal.Normalize();

    points->SetValue( (int)(i+1), gp_Pnt2d( aLowest.X(), aLowest.Y() ) );
    tangents.SetValue( (int)(i+1), aNormal );
    flags->SetValue( (int)(i+1), Standard_True );
  }

  Geom2dAPI_Interpolate anInterpolator( points, Standard_False, Standard_False );
  anInterpolator.Load( tangents, flags );
  anInterpolator.Perform();
  if( anInterpolator.IsDone() )
  {
    aResult = anInterpolator.Curve();

    //fill the distances vector
    Geom2dAdaptor_Curve anAdaptor( aResult );

    theDistances.clear();
    theDistances.reserve( n );
    Standard_Real aParamFirst = anAdaptor.FirstParameter(), aParamLast = anAdaptor.LastParameter();
    for( size_t i = 1; i <= n; i++ )
    {
      gp_Pnt2d aPnt = points->Value( (Standard_Integer)i );
      Geom2dAPI_ProjectPointOnCurve aProject( aPnt, aResult );
      Standard_Real aParam = aProject.LowerDistanceParameter();
      double aDistance = GCPnts_AbscissaPoint::Length( anAdaptor, aParamFirst, aParam );  
      theDistances.push_back( aDistance );
    }
  }
  return aResult;
}

std::vector<Handle(Geom2d_Curve)> HYDROData_DTM::ProfileToParametric( 
  const Handle(HYDROData_Profile)& theProfile,
  double& theUMin, double& theUMax, gp_Vec2d& theDir )
{
  std::vector<Handle(Geom2d_Curve)> curves;
  
  // Transformation of the coordinate systems
  gp_Pnt aLowest;
  double zmin, zmax;
  GetProperties( theProfile, aLowest, theDir, zmin, zmax );

  gp_Ax3 aStd3d( gp_Pnt( 0, 0, 0 ), gp_Dir( 0, 0, 1 ), gp_Dir( 1, 0, 0 ) );
  gp_Ax3 aLocal( gp_Pnt( aLowest.X(), aLowest.Y(), 0 ), gp_Dir( 0, 0, 1 ), gp_Dir( theDir.X(), theDir.Y(), 0 ) );

  gp_Trsf aTransf;
  aTransf.SetTransformation( aStd3d, aLocal );

  // Iteration via edges
  TopoDS_Wire aWire = TopoDS::Wire( theProfile->GetShape3D() );
  TopExp_Explorer anExp( aWire, TopAbs_EDGE );
  for( ; anExp.More(); anExp.Next() )
  {
    // Extract an edge from wire
    TopoDS_Edge anEdge = TopoDS::Edge( anExp.Current() );

    // Extract a curve corresponding to the edge
    TopLoc_Location aLoc;
    Standard_Real aFirst, aLast;
    Handle(Geom_Curve) aCurve = BRep_Tool::Curve( anEdge, aLoc, aFirst, aLast );

    // Convert the curve to 2d CS
    Handle(Geom2d_Curve) aCurve2d = CurveTo2D( aCurve, aFirst, aLast, aTransf, theUMin, theUMax );
    if( !aCurve2d.IsNull() )
      curves.push_back( aCurve2d );
  }
  return curves;
}


//bool CalcMidWidth( const std::set<double>& intersections, double& theMid, double& theWid )
//{
//  double umin = std::numeric_limits<double>::max(),
//         umax = -umin;
//
//  size_t n = intersections.size();
//  if( n <= 0 )
//    return false;
//
//  std::set<double>::const_iterator it = intersections.begin(), last = intersections.end();
//  for( ; it!=last; it++ )
//  {
//    double u = *it;
//    if( u<umin )
//      umin = u;
//    if( u>umax )
//      umax = u;
//  }
//  theMid = ( umin+umax )/2;
//  theWid = umax-umin;
//  return true;
//}



bool CalcMidWidth( const Bnd_Box2d& inters_bnd, double& theMid, double& theWid )
{
  double umin = DBL_MAX,
         umax = -umin;

  if (inters_bnd.IsVoid())
    return false;
  //size_t n = intersections.size();
  //if( n <= 0 )
  //  return false;

  double xmin, ymin, xmax, ymax;
  inters_bnd.Get(xmin, ymin, xmax, ymax);
  if (Abs(ymax-ymin)>PREC)
    return false;
  
  theMid = ( xmax+xmin )/2;
  theWid = xmax-xmin;
  return true;
}



void HYDROData_DTM::ProfileDiscretization( const Handle(HYDROData_Profile)& theProfile, 
                                           double theXCurv, double theMinZ, double theMaxZ, double theTopZ, double theDDZ,
                                           CurveUZ& theMidPointCurve,
                                           CurveUZ& theWidthCurve,                                           
                                           int& intersection_nb,
                                           double theTolerance,
                                           QSet<QString>& warnings)
{
  warnings.clear();
  double aDblMax = std::numeric_limits<double>::max(),
         aUMin = aDblMax,
         aUMax = -aUMin,
         aVMax = 1000000;
  
  gp_Vec2d aProfileDir;
  std::vector<Handle(Geom2d_Curve)> curves = ProfileToParametric( theProfile, aUMin, aUMax, aProfileDir );
  size_t n = curves.size();

  if( n==0 )
  {
    warnings.insert("no curves for discretization; skipped");
    return;
  }

  // we add the "virtual" vertical lines to simulate the intersection with profile 
  gp_Pnt2d aFirst, aLast;
  curves[0]->D0( curves[0]->FirstParameter(), aFirst );
  curves[n-1]->D0( curves[n-1]->LastParameter(), aLast );
  Handle(Geom2d_Line) aV1 = new Geom2d_Line( aFirst, gp_Dir2d( 0, 1 ) );
  Handle(Geom2d_TrimmedCurve) aT1 = new Geom2d_TrimmedCurve( aV1, 0.0, aVMax );
  
  Handle(Geom2d_Line) aV2 = new Geom2d_Line( aLast, gp_Dir2d( 0, 1 ) );
  Handle(Geom2d_TrimmedCurve) aT2 = new Geom2d_TrimmedCurve( aV2, 0.0, aVMax );

  curves.push_back( aT1 );
  curves.push_back( aT2 );
  
  int psize = ( int )( ( theMaxZ-theMinZ ) / theDDZ + 1 );
  theMidPointCurve = CurveUZ( theXCurv, aProfileDir, theMinZ, theTopZ);
  theMidPointCurve.reserve( psize );
  theWidthCurve = CurveUZ( theXCurv, aProfileDir, theMinZ, theTopZ );
  theWidthCurve.reserve( psize );

  {
    bool PlatoCase = false;
    if (Abs(aFirst.Y() - aLast.Y()) > PREC)
    {
      warnings.insert("One of the extreme points is higher than another");
    }
    double ZminExtr = Min(aFirst.Y(), aLast.Y()); 
    double ZmaxExtr = Max(aFirst.Y(), aLast.Y()); 
    Handle(Geom2d_Line) aLine = new Geom2d_Line( gp_Pnt2d( 0, ZminExtr ), gp_Dir2d( 1, 0 ) );
    std::vector<gp_Pnt2d> intersections;
    for( int i = 0; i < n; i++ )
    {
      Handle(Geom2d_Curve) aCurve = curves[i];
      Geom2dAPI_InterCurveCurve anIntersect( aCurve, aLine, theTolerance );
      for( int k=1, m=anIntersect.NbPoints(); k<=m; k++ )
        intersections.push_back(anIntersect.Point(k));
      if (anIntersect.NbSegments() > 0 )
        PlatoCase = true;
    }
    std::vector<gp_Pnt2d> interm_intersections;
    for (int i=0;i<intersections.size();i++)
    {
      gp_Pnt2d int_p2d = intersections[i];
      //check for intermid. points: shoudl be higher than ZminExtr and not in intersection with first,last points
      if (aFirst.Distance(int_p2d) > PREC && aLast.Distance(int_p2d) > PREC && int_p2d.Y() > ZminExtr)
        interm_intersections.push_back(int_p2d);
    }
    if (!interm_intersections.empty())
      warnings.insert("One of the internal points is higher than extreme points");
    if (ZminExtr != ZmaxExtr && !PlatoCase)
    {
      //additional check of plato for zmax
      Handle(Geom2d_Line) aLine = new Geom2d_Line( gp_Pnt2d( 0, ZmaxExtr ), gp_Dir2d( 1, 0 ) );
      std::vector<gp_Pnt2d> intersections;
      for( int i = 0; i < n; i++ )
      {
        Handle(Geom2d_Curve) aCurve = curves[i];
        Geom2dAPI_InterCurveCurve anIntersect( aCurve, aLine, theTolerance );
        if (anIntersect.NbSegments() > 0 )
        {
          PlatoCase = true;
          break;
        }
      }
    }
    if (PlatoCase)
      warnings.insert("Plato case on extremes");
  }

  n = curves.size();  

  // for each discrete value of z we search intersection with profile
  for( double z1 = theMinZ; z1 <= theMaxZ; z1 += theDDZ )
  {
    Handle(Geom2d_Line) aLine = new Geom2d_Line( gp_Pnt2d( 0, z1 ), gp_Dir2d( 1, 0 ) );
    std::set<double> intersections;    //TODO the solutions should be treated with some epsilon~1e-8 (computation error of intersector)
    Bnd_Box2d intersect_bndbox;
    for( size_t i = 0; i < n; i++ )
    {
      Handle(Geom2d_Curve) aCurve = curves[i];
      Geom2dAPI_InterCurveCurve anIntersect( aCurve, aLine, theTolerance );
      for( int k=1, m=anIntersect.NbPoints(); k<=m; k++ )
      {
        intersections.insert( anIntersect.Point( k ).X() );
        intersect_bndbox.Add( anIntersect.Point(k));
      }
      //
      for( int k=1, m=anIntersect.NbSegments(); k<=m; k++ )
      {
        Handle(Geom2d_Curve) Curve1,Curve2;
        anIntersect.Segment(k, Curve1, Curve2 );
        double f = Curve2->FirstParameter();
        double l = Curve2->LastParameter();
        gp_Pnt2d Pf, Pl;
        Curve2->D0(f, Pf);
        Curve2->D0(l, Pl);
        intersect_bndbox.Add( Pf );
        intersect_bndbox.Add( Pl );
        intersections.insert( Pf.X() );
        intersections.insert( Pl.X() );
      }
    }

    intersection_nb = intersections.size();
    if (intersection_nb == 0)
    {
      warnings.insert("No intersections between profile & altitude Z-lines found; skipped");
      return;
    }
    else if (intersection_nb > 2)
    {
      warnings.insert("More than 2 intersections between profile & altitude Z-lines found");
    }
    double xmin, ymin, xmax, ymax;
    intersect_bndbox.Get(xmin, ymin, xmax, ymax);
    //if (Abs(xmax-xmin)>PREC)
    if (intersection_nb >= 1)
    {
      double u_mid, u_wid;
      if( !CalcMidWidth( intersect_bndbox, u_mid, u_wid ) )
        continue;

      double z = z1 - theMinZ;
      PointUZ p_mid;
      p_mid.U = u_mid;
      p_mid.Z = z;
      theMidPointCurve.push_back( p_mid );

      PointUZ p_wid;
      p_wid.U = u_wid;
      p_wid.Z = z;
      theWidthCurve.push_back( p_wid );
    }
  

    //intersection_nb = intersections.size();
    //if( intersection_nb >= 1 )
    //{
    //  double u_mid, u_wid;
    //  if( !CalcMidWidth( intersections, u_mid, u_wid ) )
    //    continue;
    //
    //  double z = z1 - theMinZ;
    //  PointUZ p_mid;
    //  p_mid.U = u_mid;
    //  p_mid.Z = z;
    //  theMidPointCurve.push_back( p_mid );
    //
    //  PointUZ p_wid;
    //  p_wid.U = u_wid;
    //  p_wid.Z = z;
    //  theWidthCurve.push_back( p_wid );
    //}
  }
}

void HYDROData_DTM::Interpolate( const CurveUZ& theCurveA, const CurveUZ& theCurveB, 
                                 int theNbSteps, std::vector<CurveUZ>& theInterpolation,
                                 bool isAddSecond )
{
  theInterpolation.clear();
  int d = isAddSecond ? 2 : 1;
  theInterpolation.reserve( theNbSteps+d );
  double dt = 1.0 / double( theNbSteps + 1 );
  double t = dt;
  theInterpolation.push_back( theCurveA );
  for( int i=0; i<theNbSteps; i++, t+=dt )
  {
    CurveUZ anInterp = theCurveA*(1-t) + theCurveB*t;
    theInterpolation.push_back( anInterp );
  }
  if( isAddSecond )
    theInterpolation.push_back( theCurveB );
}
#include <BRepLib_MakeEdge2d.hxx>
void HYDROData_DTM::CurveTo3D( const Handle(Geom2d_BSplineCurve)& theHydraulicAxis,
                               const CurveUZ& theMidCurve, const CurveUZ& theWidthCurve,
                               AltitudePoints& thePoints )
{
  Geom2dAdaptor_Curve anAdaptor( theHydraulicAxis );
  TopoDS_Edge E2d = BRepLib_MakeEdge2d(theHydraulicAxis).Edge();
  GCPnts_AbscissaPoint ap( anAdaptor, theMidCurve.Xcurv(), anAdaptor.FirstParameter() );  
  double aParam = ap.Parameter();

  gp_Pnt2d point;
  anAdaptor.D0( aParam, point );
  gp_Vec2d profile_dir = theMidCurve.ProfileDir();
  //gp_Dir tangent_n( -profile_dir.Y(), profile_dir.X(), dz );
  profile_dir.Normalize();
  
  size_t n = theMidCurve.size();
  std::map<double, AltitudePoint> sorted_points;
  bool isOnTop = false;
  for( size_t i=0; i<n; i++ ) // build the two banks of the interpolated profile, from bottom to top
  {
    double param1 = theMidCurve[i].U - theWidthCurve[i].U / 2;
    double param2 = theMidCurve[i].U + theWidthCurve[i].U / 2;

    gp_Pnt2d p1 = point.Translated( param1 * profile_dir);
    gp_Pnt2d p2 = point.Translated( param2 * profile_dir);

    bool arrivedOnTop = false;
    double z = 0;
    if (theMidCurve[i].Z <= theMidCurve.MaxZ())
      z = theMidCurve[i].Z + theMidCurve.DeltaZ();
    else
      {
        z = theMidCurve.MaxZ() + theMidCurve.DeltaZ(); // limit z to linear interpolation between maxima on extremity profiles
        arrivedOnTop = true; // do not keep points after this one
      }
    if (!isOnTop)
      {
        AltitudePoint p3d_1( p1.X(), p1.Y(), z ), p3d_2( p2.X(), p2.Y(), z );

        sorted_points[param1] = p3d_1;
        sorted_points[param2] = p3d_2;
      }
    //if (arrivedOnTop)
    //  isOnTop =true; // do not keep points after this one (commented: leads to strange limits of 2D shape)
  }

  thePoints.reserve( sorted_points.size() );
  const double EPS = 1E-12;
  std::map<double, AltitudePoint>::const_iterator it = sorted_points.begin(), last = sorted_points.end();
  for( ; it!=last; it++ )
    if( thePoints.empty() || thePoints.back().SquareDistance( it->second ) > EPS )
      thePoints.push_back( it->second );
}

//inline double max( double a, double b )
//{
//  if( a>b )
//    return a;
//  else
//    return b;
//}
//
//inline double min( double a, double b )
//{
//  if( a<b )
//    return a;
//  else
//    return b;
//}

#include <BRepLib_MakeWire.hxx>

std::vector<HYDROData_Bathymetry::AltitudePoints> HYDROData_DTM::Interpolate
  ( const Handle(Geom2d_BSplineCurve)& theHydraulicAxis,
    const Handle(HYDROData_Profile)& theProfileA,
    double theXCurvA,
    const Handle(HYDROData_Profile)& theProfileB,
    double theXCurvB,
    double theDDZ, int theNbSteps, bool isAddSecond,
    int& inter_nb_1, int& inter_nb_2,
    NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
    bool ToEstimateWarningsOnly)
{
  double zminA, zmaxA, zminB, zmaxB;
  gp_Pnt lowestA, lowestB;
  gp_Vec2d dirA, dirB;

  GetProperties( theProfileA, lowestA, dirA, zminA, zmaxA ); 
  GetProperties( theProfileB, lowestB, dirB, zminB, zmaxB ); 

  
  double hmax = zmaxA-zminA > zmaxB-zminB ? zmaxA-zminA : zmaxB-zminB;

  //double dz = zminB - zminA;
  //double zmin = min( zminA, zminB );
  //double zmax = max( zmaxA, zmaxB );

  CurveUZ midA(0, gp_Vec2d(), 0, 0), midB(0, gp_Vec2d(), 0, 0);
  CurveUZ widA(0, gp_Vec2d(), 0, 0), widB(0, gp_Vec2d(), 0, 0);

  QSet<QString> warnings_per_profileA, warnings_per_profileB;
  
  ProfileDiscretization( theProfileA, theXCurvA, zminA, zminA+hmax, zmaxA-zminA, 
    theDDZ, midA, widA, inter_nb_1, 1E-6, warnings_per_profileA );
  ProfileDiscretization( theProfileB, theXCurvB, zminB, zminB+hmax, zmaxB-zminB, 
    theDDZ, midB, widB, inter_nb_2, 1E-6, warnings_per_profileB );

  //process warnings
  if (warnings.IsBound(theProfileA))
  {
    QSet<QString>& warnings_per_profileA_old = warnings.ChangeFind(theProfileA);
    warnings_per_profileA_old+=warnings_per_profileA;
  }
  else
    warnings.Bind(theProfileA, warnings_per_profileA);
  
  if (warnings.IsBound(theProfileB))
  {
    QSet<QString>& warnings_per_profileB_old = warnings.ChangeFind(theProfileB);
    warnings_per_profileB_old+=warnings_per_profileB;
  }
  else
    warnings.Bind(theProfileB, warnings_per_profileB);
  //

  if (ToEstimateWarningsOnly)
    return std::vector<AltitudePoints>();

  std::vector<CurveUZ> mid, wid;
  Interpolate( midA, midB, theNbSteps, mid, isAddSecond );
  Interpolate( widA, widB, theNbSteps, wid, isAddSecond );

  size_t p = mid.size();
  size_t q = p>0 ? 2*mid[0].size() : 1;
  std::vector<AltitudePoints> points;
  points.resize( p );

  for( size_t i=0; i<p; i++ )
  {
    points[i].reserve( q );
    CurveTo3D( theHydraulicAxis, mid[i], wid[i], points[i] );
  }

  return points;
}

HYDROData_Bathymetry::AltitudePoints HYDROData_DTM::Interpolate
  ( const std::vector<Handle(HYDROData_Profile)>& theProfiles,
    double theDDZ, double theSpatialStep,
    AltitudePoints& theLeft,
    AltitudePoints& theRight,
    std::vector<AltitudePoints>& theMainProfiles,
    std::set<int>& invalInd,
    NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings,
    bool ToEstimateWarningsOnly)
{
  AltitudePoints points;
  size_t n = theProfiles.size();
  if( n<=1 )
    return points;

  std::vector<double> distances;
  Handle(Geom2d_BSplineCurve) aHydraulicAxis;
  
  if (!ToEstimateWarningsOnly)
  {
    aHydraulicAxis = CreateHydraulicAxis( theProfiles, distances );
    if( aHydraulicAxis.IsNull() )
      return points;
  }

  theMainProfiles.reserve( n );

  for( size_t i=0, n1=n-1; i<n1; i++ )
  {
    double aDistance = 0;
    int aNbSteps = -1;
    bool isAddSecond = false;
  
    if (!ToEstimateWarningsOnly)
    {
      aDistance = distances[i+1]-distances[i];
      aNbSteps = int(aDistance/theSpatialStep);
      isAddSecond = i==n1-1;
    }

    // 1. Calculate interpolated profiles
    int inter_nb_1, inter_nb_2;
    std::vector<AltitudePoints> local_points = Interpolate( aHydraulicAxis, theProfiles[i], 
      ToEstimateWarningsOnly ? 0 : distances[i], 
      theProfiles[i+1], 
      ToEstimateWarningsOnly ? 0 : distances[i+1], 
      theDDZ, aNbSteps, isAddSecond, inter_nb_1, inter_nb_2, warnings,
      ToEstimateWarningsOnly);
    int lps = local_points.size();

    if (lps == 0)
      continue;

    if (inter_nb_1 > 2)
      invalInd.insert(i);

    if (inter_nb_2 > 2)
      invalInd.insert(i+1);

    // 2. Put all points into the global container
    for( size_t j=0; j<lps; j++ )
    {
      const AltitudePoints& lp = local_points[j];
      if( i==0 && j==0 )
        points.reserve( lp.size() * n );
      for( size_t k=0, ks=lp.size(); k<ks; k++ )
        points.push_back( lp[k] );
    }

    // 3. Get left/right banks' points
    if( i==0 )
    {
      theLeft.reserve( lps * n );
      theRight.reserve( lps * n );
    }
    for( size_t j=0; j<lps; j++ )
    {
      const AltitudePoints& lp = local_points[j];
      theLeft.push_back( lp[0] );
      theRight.push_back( lp[lp.size()-1] );
    }

    // 4. Get main profiles points
    theMainProfiles.push_back( local_points[0] );
    if( isAddSecond )
      theMainProfiles.push_back( local_points[lps-1] );
  }
  return points;
}

int HYDROData_DTM::EstimateNbPoints( const std::vector<Handle(HYDROData_Profile)>& theProfiles,
                                     double theDDZ, double theSpatialStep )
{
  size_t n = theProfiles.size();
  if( n<=1 )
    return 0;
  if( theDDZ<1E-6 || theSpatialStep<1E-6 )
    return 1 << 20;

  std::vector<double> distances;
  Handle(Geom2d_BSplineCurve) aHydraulicAxis = CreateHydraulicAxis( theProfiles, distances );
  if( aHydraulicAxis.IsNull() )
    return 0;

  double aCompleteDistance = distances[n-1];
  int aNbSteps = int( aCompleteDistance / theSpatialStep ) + 1;
  gp_Pnt aLowest;
  gp_Vec2d aDir;
  double aZMin, aZMax;
  GetProperties( theProfiles[0], aLowest, aDir, aZMin, aZMax );
  int aNbZSteps = (aZMax-aZMin)/theDDZ;

  if( aNbSteps > ( 1<<16 ) || aNbZSteps > ( 1<<16 ) )
    return 1 << 20;

  return aNbSteps * aNbZSteps;
}

void HYDROData_DTM::GetWarnings(NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings)
{
  warnings = myWarnings;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_PolylineXY.h"

#include <HYDROData_Polyline3D.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_ShapeFile.h>
#include <HYDROData_Profile.h>

#include "HYDROData_BSplineOperation.h"
#include "HYDROData_Document.h"
#include "HYDROData_ShapesTool.h"
#include "HYDROData_Tool.h"
#include "HYDROData_PolylineOperator.h"
#include "HYDROData_TopoCurve.h"

#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakePolygon.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepOffsetAPI_NormalProjection.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <Quantity_Parameter.hxx>

#ifndef LIGHT_MODE
#include <GEOMBase.h>
#endif

#include <GeomAPI_ProjectPointOnCurve.hxx>
#include <GeomAdaptor_Curve.hxx>
#include <Geom_Line.hxx>
#include <Geom_BSplineCurve.hxx>

#include <GCPnts_AbscissaPoint.hxx>
#include <GCPnts_QuasiUniformDeflection.hxx>
#include <GCPnts_UniformDeflection.hxx>

#include <ImageComposer_MetaTypes.h>

#include <gp_Pnt.hxx>
#include <gp_XY.hxx>
#include <gp_Pln.hxx>

#include <NCollection_Map.hxx>

#include <TCollection_ExtendedString.hxx>

#include <TDataStd_ListIteratorOfListOfByte.hxx>
#include <TColStd_ListIteratorOfListOfInteger.hxx>
#include <TColStd_ListIteratorOfListOfReal.hxx>

#include <TColStd_Array1OfReal.hxx>
#include <TColgp_Array1OfPnt.hxx>

#include <TDataStd_AsciiString.hxx>
#include <TDataStd_BooleanList.hxx>
#include <TDataStd_ExtStringList.hxx>
#include <TDataStd_IntegerList.hxx>
#include <TDataStd_ListIteratorOfListOfExtendedString.hxx>
#include <TDataStd_RealList.hxx>
#include <TDataStd_UAttribute.hxx>
#include <TDataStd_ExtStringArray.hxx>

#include <TopoDS_Iterator.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopTools_HSequenceOfShape.hxx>
#include <TopExp_Explorer.hxx>
#include <ShapeAnalysis_FreeBounds.hxx>
#include <TopoDS.hxx>

#include <QColor>
#include <QPainterPath>
#include <QVariant>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"
#include <BRepTools.hxx>
#include <sstream>
#include <cmath>

static const Standard_GUID GUID_IS_UNEDITABLE("e5799736-9030-4051-91a4-2e58321fa153");

const double LOCAL_SELECTION_TOLERANCE = 0.0001;

TCollection_AsciiString getUniqueSectionName( const NCollection_Sequence<TCollection_AsciiString>& theNamesSeq )
{
  NCollection_Map<TCollection_AsciiString> aNamesMap;

  for ( int i = 1, n = theNamesSeq.Size(); i <= n; ++i )
  {
    const TCollection_AsciiString& aSectName = theNamesSeq.Value( i );
    aNamesMap.Add( aSectName );
  }

  TCollection_AsciiString aResName;

  int aPrefIdx = 1;
  do
  {
    aResName = TCollection_AsciiString( "Section_" ) + aPrefIdx;
    ++aPrefIdx;
  }
  while ( aNamesMap.Contains( aResName ) );

  return aResName;
}

TCollection_AsciiString getUniqueSectionName( const Handle(TDataStd_ExtStringList)& theNamesList )
{
  NCollection_Sequence<TCollection_AsciiString> aNamesSeq;

  TDataStd_ListIteratorOfListOfExtendedString aNamesIter( theNamesList->List() );
  for ( ; aNamesIter.More(); aNamesIter.Next() )
    aNamesSeq.Append( aNamesIter.Value() );

  return getUniqueSectionName( aNamesSeq );
}

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_PolylineXY, HYDROData_IPolyline)

HYDROData_PolylineXY::HYDROData_PolylineXY()
: HYDROData_IPolyline(),
  myIsInCustomFlag( false )
{
}

HYDROData_PolylineXY::~HYDROData_PolylineXY()
{
}

QStringList HYDROData_PolylineXY::DumpToPython( const QString&       thePyScriptPath,
                                                MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aPolylineName = GetObjPyName();

  // Set the wire color
  QStringList aWireColorDef;

  //QColor aWireColor = GetWireColor();
  //setPythonObjectColor( aWireColorDef, aWireColor, DefaultWireColor(), "SetWireColor" );

  int nbSec = NbSections();
  for (int i = 0; i < nbSec; i++)
  {
    QColor aColor;
    GetSectionColor(i, aColor);
    setPythonPolylineSectionColor(aWireColorDef, i, aColor );
    //aWireColorDef << "";
  }
  
  if ( !aWireColorDef.isEmpty() )
  {
    aResList << aWireColorDef;
    aResList << QString( "" );
  }

  bool anIsEditable = IsEditable();
  if ( !anIsEditable )
  {
    // If polyline is not editable we try to import the shape from geom
    TCollection_AsciiString aGeomObjectEntry = GetGeomObjectEntry();
    if ( !aGeomObjectEntry.IsEmpty() )
    {
      QString aSalomeObjName = HYDROData_Tool::GenerateNameForPython( theTreatedObjects, "polyline_sobj" );
      aResList << QString( "%1 = salome.myStudy.FindObjectID( \"%2\" )" )
                  .arg( aSalomeObjName ).arg( aGeomObjectEntry.ToCString() );

      aResList << QString( "%1.ImportFromGeomIOR( %2.GetIOR() )" )
                  .arg( aPolylineName ).arg( aSalomeObjName );

      aResList << QString( "%1.SetGeomObjectEntry( \"%2\" )" )
                  .arg( aPolylineName ).arg( aGeomObjectEntry.ToCString() );
    }
  }
  else
  {
    // Set polyline data
    NCollection_Sequence<TCollection_AsciiString>           aSectNames;
    NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
    NCollection_Sequence<bool>                              aSectClosures;
    GetSections( aSectNames, aSectTypes, aSectClosures );

    for ( int i = 1, n = aSectNames.Size(); i <= n; ++i )
    {
      const TCollection_AsciiString& aSectName = aSectNames.Value( i );
      const SectionType& aSectType = aSectTypes.Value( i );
      bool aSectClosure = aSectClosures.Value( i );

      aResList << QString( "%1.AddSection( \"%2\", %3, %4 )" ).arg( aPolylineName )
                  .arg( aSectName.ToCString() ).arg( aSectType ).arg( aSectClosure );

      HYDROData_IPolyline::PointsList aSectPointsList = GetPoints( i - 1 );
      for ( int k = 1, aNbPoints = aSectPointsList.Size(); k <= aNbPoints; ++k )
      {
        const Point& aSectPoint = aSectPointsList.Value( k );

        QString anXStr = QString::number( aSectPoint.X(), 'f', 2 );
        QString anYStr = QString::number( aSectPoint.Y(), 'f', 2 );
        aResList << QString( "%1.AddPoint( %2, gp_XY( %3, %4 ) )" ).arg( aPolylineName )
          .arg( i - 1 ).arg( anXStr ).arg( anYStr );
      }
    }
  }
  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aPolylineName );
  aResList << QString( "" );

  return aResList;
}

QVariant HYDROData_PolylineXY::GetDataVariant()
{
  QPainterPath aPath = GetPainterPath();

  QVariant aVarData;
  aVarData.setValue<QPainterPath>( aPath );
  
  return aVarData;
}

QColor HYDROData_PolylineXY::DefaultWireColor()
{
  return QColor( Qt::black );
}

bool HYDROData_PolylineXY::ImportFromGeomIOR( const TCollection_AsciiString& theIOR )
{
#ifdef LIGHT_MODE
  return false;
#else
  if ( theIOR.IsEmpty() )
    return false;

  TopoDS_Shape aShape = GEOMBase::GetShapeFromIOR( theIOR.ToCString() );
  if ( aShape.IsNull() )
    return false;

  return ImportShape( aShape, false, NULL );
#endif
}

void HYDROData_PolylineXY::SetGeomObjectEntry( const TCollection_AsciiString& theEntry )
{
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_GeomObjectEntry ), theEntry );
  anAttr->SetID(TDataStd_AsciiString::GetID());
}

TCollection_AsciiString HYDROData_PolylineXY::GetGeomObjectEntry() const
{
  TCollection_AsciiString aRes;

  TDF_Label aLabel = myLab.FindChild( DataTag_GeomObjectEntry, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
      aRes = anAsciiStr->Get();
  }

  return aRes;
}

bool convertEdgeToSection( const TopoDS_Edge&                                       theEdge,
                           NCollection_Sequence<TCollection_AsciiString>&           theSectNames,
                           NCollection_Sequence<HYDROData_PolylineXY::SectionType>& theSectTypes,
                           NCollection_Sequence<bool>&                              theSectClosures,
                           NCollection_Sequence<HYDROData_PolylineXY::PointsList>&  theSectPoints,
                           bool                                                     IsCanBeClosed,
                           bool                                                     IsInterpolationAllowed,
                           double                                                   theDeflection )
{
  Standard_Real aFirst = 0.0, aLast = 0.0;
  Handle(Geom_Curve) anEdgeGeomCurve = BRep_Tool::Curve( theEdge, aFirst, aLast );
  if ( anEdgeGeomCurve.IsNull() )
    return false;

  TCollection_AsciiString aSectName = getUniqueSectionName( theSectNames );
  bool anIsEdgeClosed = anEdgeGeomCurve->IsClosed();

  HYDROData_PolylineXY::SectionType aSectionType = HYDROData_PolylineXY::SECTION_POLYLINE;
  HYDROData_PolylineXY::PointsList aPointsList;

  if( anEdgeGeomCurve->IsKind( STANDARD_TYPE(Geom_Line) ) )
  {
    Handle(Geom_Line) aGeomLine = Handle(Geom_Line)::DownCast( anEdgeGeomCurve );

    gp_Pnt aFirstPoint, aLastPoint;
    aGeomLine->D0( aFirst, aFirstPoint );
    aGeomLine->D0( aLast, aLastPoint );

    HYDROData_PolylineXY::Point aSectFirstPoint( aFirstPoint.X(), aFirstPoint.Y() );
    aPointsList.Append( aSectFirstPoint );

    HYDROData_PolylineXY::Point aSectLastPoint( aLastPoint.X(), aLastPoint.Y() );
    aPointsList.Append( aSectLastPoint );
  }
  else if ( anEdgeGeomCurve->IsKind( STANDARD_TYPE(Geom_BSplineCurve) ) || IsInterpolationAllowed )
  {
    aSectionType = HYDROData_PolylineXY::SECTION_SPLINE;

    BRepAdaptor_Curve anAdaptorCurve( theEdge );
    GCPnts_QuasiUniformDeflection aDiscrete( anAdaptorCurve, theDeflection );
    //GCPnts_UniformDeflection aDiscrete( anAdaptorCurve, theDeflection );

    int aNbPoints = aDiscrete.NbPoints();

    // Decrease the number of imported poles because of last one 
    // pole is the closing point which are the start point
    if ( anIsEdgeClosed ) aNbPoints--;

    for ( int i = 1; i <= aNbPoints; ++i )
    {
      const gp_Pnt& aPoint = aDiscrete.Value( i );

      HYDROData_PolylineXY::Point aSectPoint( aPoint.X(), aPoint.Y() );
      aPointsList.Append( aSectPoint );
    }
  }
  else
  {
    // Other curve types are not supported
    return false;
  }

  if ( aPointsList.IsEmpty() )
    return false;

  theSectNames.Append( aSectName );
  theSectTypes.Append( aSectionType );
  theSectClosures.Append( anIsEdgeClosed );
  theSectPoints.Append( aPointsList );

  return true;
}


bool convertEdgesToSections( const TopoDS_Edge&                                       theEdge,
                             NCollection_Sequence<TCollection_AsciiString>&           theSectNames,
                             NCollection_Sequence<HYDROData_PolylineXY::SectionType>& theSectTypes,
                             NCollection_Sequence<bool>&                              theSectClosures,
                             NCollection_Sequence<HYDROData_PolylineXY::PointsList>&  theSectPoints,
                             bool                                                     IsCanBeClosed,
                             bool                                                     IsInterpolationAllowed,
                             double                                                   theDeflection,
                             const Handle( HYDROData_PolylineXY )&                    theOldPolyline )
{
  DEBTRACE("convertEdgesToSections")
  Standard_Real aFirst = 0.0, aLast = 0.0;
  Handle(Geom_Curve) anEdgeGeomCurve = BRep_Tool::Curve( theEdge, aFirst, aLast );
  if ( anEdgeGeomCurve.IsNull() )
    return false;

  bool isPrevious = (theSectTypes.Size() > 0);
  DEBTRACE("nb sections: " << theSectTypes.Size());
  HYDROData_PolylineXY::SectionType prevSectType = HYDROData_PolylineXY::SECTION_SPLINE;;
  HYDROData_PolylineXY::PointsList prevPointList;
  bool isPrevClosed = true;
  if (isPrevious)
    {
       prevSectType = theSectTypes.Last();
       prevPointList = theSectPoints.Last();
       isPrevClosed = theSectClosures.Last();
    }

  bool anIsEdgeClosed = anEdgeGeomCurve->IsClosed();
  anIsEdgeClosed &= IsCanBeClosed; // on split, resulting edges are normally not closed...
  HYDROData_PolylineXY::SectionType aSectionType ;
  if( anEdgeGeomCurve->IsKind( STANDARD_TYPE(Geom_Line) ) )
    {
      aSectionType = HYDROData_PolylineXY::SECTION_POLYLINE;
    }
  else if ( anEdgeGeomCurve->IsKind( STANDARD_TYPE(Geom_BSplineCurve) ) || IsInterpolationAllowed )
    {
      aSectionType = HYDROData_PolylineXY::SECTION_SPLINE;
    }
  else
    {
      // Other curve types are not supported
      return false;
    }

  bool isNewSection = !isPrevious || isPrevClosed || anIsEdgeClosed || prevSectType != aSectionType;
  DEBTRACE(isNewSection <<": " << !isPrevious << " " << isPrevClosed << " " << anIsEdgeClosed << " " << (prevSectType != aSectionType));

  HYDROData_PolylineXY::PointsList aPointsList;
  if (!isNewSection)
    {
      aPointsList = prevPointList;
    }

  if( aSectionType == HYDROData_PolylineXY::SECTION_POLYLINE )
    {
      DEBTRACE("SECTION_POLYLINE");
      Handle(Geom_Line) aGeomLine = Handle(Geom_Line)::DownCast( anEdgeGeomCurve );

      gp_Pnt aFirstPoint, aLastPoint;
      aGeomLine->D0( aFirst, aFirstPoint );
      aGeomLine->D0( aLast, aLastPoint );
      HYDROData_PolylineXY::Point aSectFirstPoint( aFirstPoint.X(), aFirstPoint.Y() );
      HYDROData_PolylineXY::Point aSectLastPoint( aLastPoint.X(), aLastPoint.Y() );
     if (!isNewSection)
        {
          if (aSectFirstPoint == prevPointList.Last())
            {
              DEBTRACE("points shared: a");//aPointsList.Append( aSectFirstPoint );
              aPointsList.Append( aSectLastPoint );
            }
          else if (aSectLastPoint == prevPointList.Last())
            {
              DEBTRACE("points shared: b");//aPointsList.Append( aSectLastPoint );
              aPointsList.Append( aSectFirstPoint );
            }
          else if (aSectFirstPoint == prevPointList.First())
            {
              DEBTRACE("points shared: c");//aPointsList.Prepend( aSectFirstPoint );
              aPointsList.Prepend( aSectLastPoint );
            }
          else if (aSectLastPoint == prevPointList.First())
            {
              DEBTRACE("points shared: d");//aPointsList.Prepend( aSectLastPoint );
              aPointsList.Prepend( aSectFirstPoint );
            }
          else
            {
              DEBTRACE("no point shared")
              isNewSection = true; // no point shared, new section
              aPointsList.Clear();
              aPointsList.Append( aSectFirstPoint );
              aPointsList.Append( aSectLastPoint );
            }
        }
     else
       {
         DEBTRACE("new section");
         aPointsList.Append( aSectFirstPoint );
         aPointsList.Append( aSectLastPoint );
       }
    }
  else // aSectionType == HYDROData_PolylineXY::SECTION_SPLINE
    {
      DEBTRACE("SECTION_SPLINE");
      isNewSection = true;
      aPointsList.Clear();

      BRepAdaptor_Curve anAdaptorCurve(theEdge);
      if (theOldPolyline.IsNull()) // --- no previous polyline: build a set of points from scratch for the spline
        {
          GCPnts_QuasiUniformDeflection aDiscrete(anAdaptorCurve, theDeflection);

          int aNbPoints = aDiscrete.NbPoints();

          // Decrease the number of imported poles because of last one
          // pole is the closing point which are the start point
          if (anIsEdgeClosed)
            aNbPoints--;

          for (int i = 1; i <= aNbPoints; ++i)
            {
              const gp_Pnt& aPoint = aDiscrete.Value(i);
              HYDROData_PolylineXY::Point aSectPoint(aPoint.X(), aPoint.Y());
              aPointsList.Append(aSectPoint);
            }

          gp_Pnt endPts[] = {gp_Pnt(aPointsList.First().X(),aPointsList.First().Y(), 0),
                             gp_Pnt(aPointsList.Last().X(),aPointsList.Last().Y(), 0) };
          DEBTRACE("curve start: "<< endPts[0].X() << " " << endPts[0].Y());
          DEBTRACE("curve end: "<< endPts[1].X() << " " << endPts[1].Y());
        }
      else // --- split of a previous polyline: try to retrieve old sets of points and add intersection points
        {
          const gp_Pnt aEndPs[] = { anAdaptorCurve.Value(anAdaptorCurve.FirstParameter()).XYZ(),
                                    anAdaptorCurve.Value(anAdaptorCurve.LastParameter()).XYZ() };
          double midPar = (anAdaptorCurve.LastParameter() + anAdaptorCurve.FirstParameter())/2;
          gp_Pnt midPnt;
          anAdaptorCurve.D0(midPar, midPnt);
          DEBTRACE("curve first point: " << aEndPs[0].X() << " " << aEndPs[0].Y() << " " << aEndPs[0].Z());
          DEBTRACE("curve last point: " << aEndPs[1].X() << " " << aEndPs[1].Y() << " " << aEndPs[1].Z());
          DEBTRACE("curve mid point: " << midPnt.X() << " " << midPnt.Y() << " " << midPnt.Z());

          std::vector<TopoDS_Wire> aCurves;
          HYDROData_PolylineOperator::GetWires(theOldPolyline, aCurves);

          int nbSections = theOldPolyline->NbSections();
          DEBTRACE("nbSections: "<< nbSections << ", nbCurves: " << aCurves.size() );
          for (int isec = 0; isec < nbSections; isec++)
            {
              DEBTRACE("section: "<< isec);
              bool isOldSectionclosed = theOldPolyline->IsClosedSection(isec);
              TopoDS_Wire aWire = aCurves[isec]; // we suppose sections and wires are in the same order
              TopExp_Explorer anExp(aWire, TopAbs_EDGE);
              TopoDS_Edge anEdge = TopoDS::Edge(anExp.Current()); // the first is OK: only one normally with splines
              BRepAdaptor_Curve adaptorOldCurve(anEdge);
              double pfirst = adaptorOldCurve.FirstParameter();
              double plast = adaptorOldCurve.LastParameter();
              DEBTRACE("previous curve first last : "<< pfirst << " " << plast);
              double p[3] = {-1, -1};
              double d0= ProjectPointToCurve(aEndPs[0].XYZ(), adaptorOldCurve,p[0]);
              double d1= ProjectPointToCurve(aEndPs[1].XYZ(), adaptorOldCurve,p[1]);
              double d2= ProjectPointToCurve(midPnt.XYZ(), adaptorOldCurve, p[2]);
              DEBTRACE("d0: "<<d0<<" d1: "<<d1<<" d2: "<<d2<<" p0: "<<p[0]<<" p1: "<<p[1]<<" p2: "<<p[2]);
              if ((d0 < 1.e-3) && (d1 < 1.e-3) && (d2 < 1.e-3)) // we got the good old curve (and the good section)
                {
                  double pmin = p[0];
                  double pmax = p[1];
                  bool forward = true;
                  DEBTRACE("isOldSectionclosed: " << isOldSectionclosed);
                  if (!isOldSectionclosed) // no need to check first and last points on an open curve
                    {
                      if (pmin > pmax)
                        {
                          pmin = p[1];
                          pmax = p[0];
                          forward = false;
                        }
                    }
                  else // old section closed: check if we use first or last points...
                    {
                      if((pmin < pmax) && ((std::fabs(pmin - pfirst)> 1.e-3) && (std::fabs(pmax - plast) >1.e-3))) // internal points forward
                        forward = true;
                      else if ((pmin > pmax) && ((std::fabs(pmin - plast)> 1.e-3) && (std::fabs(pmax - pfirst) >1.e-3))) // internal points reverse
                        {
                          pmin = p[1];
                          pmax = p[0];
                          forward = false;
                        }
                      else if ((std::fabs(pmin - plast) <1.e-3) && (p[2] < pmax)) // forward, replace pmin par pfirst
                          pmin = pfirst;
                      else if ((std::fabs(pmin - plast) <1.e-3) && (p[2] > pmax)) // reverse
                        {
                          pmin = p[1];
                          pmax = p[0];
                          forward = false;
                        }
                      else if ((std::fabs(pmax - pfirst) <1.e-3) && (p[2] < pmin)) // reverse
                        {
                          pmin = p[1];
                          pmax = p[0];
                          forward = false;
                        }
                      else if ((std::fabs(pmax - pfirst) <1.e-3) && (p[2] > pmin)) // forward, replace pmax par plast
                        pmax = plast;
                   }
                  DEBTRACE("forward: "<< forward << " pmin " << pmin <<  " pmax " << pmax);
                  HYDROData_PolylineXY::Point aFirstPoint, aLastPoint;
                  if (forward)
                    {
                      aFirstPoint = HYDROData_PolylineXY::Point(aEndPs[0].X(), aEndPs[0].Y());
                      aLastPoint = HYDROData_PolylineXY::Point(aEndPs[1].X(), aEndPs[1].Y());
                    }
                  else
                    {
                      aFirstPoint = HYDROData_PolylineXY::Point(aEndPs[1].X(), aEndPs[1].Y());
                      aLastPoint = HYDROData_PolylineXY::Point(aEndPs[0].X(), aEndPs[0].Y());
                    }
                  aPointsList.Append(aFirstPoint);

                  HYDROData_PolylineXY::PointsList aSectPoints = theOldPolyline->GetPoints(isec, false);
                  int nbPoints = aSectPoints.Length();
                  DEBTRACE("nbPoints " << nbPoints);
                  if (forward)
                    for (int i=1; i<=nbPoints; i++)
                      {
                        HYDROData_PolylineXY::Point aPoint = aSectPoints.Value(i);
                        gp_XYZ p(aPoint.X(), aPoint.Y(), 0);
                        double param =-1;
                        double d = ProjectPointToCurve(p, adaptorOldCurve, param);
                        if ((param > pmin) && (param < pmax))
                          {
                            DEBTRACE("param: " << param);
                            aPointsList.Append(aPoint);
                          }
                      }
                  else
                    for (int i=nbPoints; i>0; i--)
                      {
                        HYDROData_PolylineXY::Point aPoint = aSectPoints.Value(i);
                        gp_XYZ p(aPoint.X(), aPoint.Y(), 0);
                        double param =-1;
                        double d = ProjectPointToCurve(p, adaptorOldCurve, param);
                        if ((param > pmin) && (param < pmax))
                          {
                            DEBTRACE("param: " << param);
                            aPointsList.Append(aPoint);
                          }
                      }

                  aPointsList.Append(aLastPoint);
                }

            }
        }
    }

  if ( aPointsList.IsEmpty() )
    return false;

  TCollection_AsciiString aSectName = getUniqueSectionName( theSectNames );
  if (isNewSection)
    {
      DEBTRACE("isNewSection");
      theSectNames.Append( aSectName );
      theSectTypes.Append( aSectionType );
      theSectClosures.Append( anIsEdgeClosed );
      theSectPoints.Append( aPointsList );
    }
  else
    {
      DEBTRACE("sameSection");
      theSectPoints.SetValue(theSectPoints.Length(), aPointsList);
    }

  return true;
}

bool HYDROData_PolylineXY::ImportShape( const TopoDS_Shape& theShape,
                                        bool IsInterpolationAllowed,
                                        const Handle( HYDROData_PolylineXY )& theOldPolyline,
                                        bool IsClosureAllowed,
                                        double theDeviation )
{
  DEBTRACE("ImportShape");
  if ( theShape.IsNull() )
    return false;

  //std::string brepName = this->GetName().toStdString();
  //brepName += ".brep";
  //BRepTools::Write( theShape, brepName.c_str() );

  RemoveSections();

  bool anIsCanBeImported = false;

  NCollection_Sequence<TCollection_AsciiString> aSectNames;
  NCollection_Sequence<SectionType>             aSectTypes;
  NCollection_Sequence<bool>                    aSectClosures;
  NCollection_Sequence<PointsList>              aSectPoints;

  if ( theShape.ShapeType() == TopAbs_EDGE )
  {
      DEBTRACE("TopAbs_EDGE");
    TopoDS_Edge anEdge = TopoDS::Edge( theShape );
//    anIsCanBeImported = convertEdgeToSection( anEdge, aSectNames, aSectTypes,
//      aSectClosures, aSectPoints, true, IsInterpolationAllowed, theDeviation );
    anIsCanBeImported = convertEdgesToSections( anEdge, aSectNames, aSectTypes, aSectClosures,
                                                aSectPoints, IsClosureAllowed, IsInterpolationAllowed,
                                                theDeviation, theOldPolyline );
  }
  else if ( theShape.ShapeType() == TopAbs_WIRE )
  {
      DEBTRACE("TopAbs_WIRE");
    TopTools_SequenceOfShape anEdges;
    HYDROData_ShapesTool::ExploreShapeToShapes( theShape, TopAbs_EDGE, anEdges );

    anIsCanBeImported = !anEdges.IsEmpty();
    for ( int i = 1, n = anEdges.Length(); i <= n && anIsCanBeImported; ++i )
    {
      TopoDS_Edge aWireEdge = TopoDS::Edge( anEdges.Value( i ) );
      anIsCanBeImported = convertEdgesToSections( aWireEdge, aSectNames, aSectTypes, aSectClosures,
                                                  aSectPoints, IsClosureAllowed, IsInterpolationAllowed,
                                                  theDeviation, theOldPolyline );
    }
  }

  if ( anIsCanBeImported )
  {
    for ( int i = 1, n = aSectNames.Length(); i <= n; ++i )
    {
      const TCollection_AsciiString& aSectName = aSectNames.Value( i );
      const SectionType& aSectType = aSectTypes.Value( i );
      bool anIsSectionClosed = aSectClosures.Value( i );
      const PointsList& aSectPointsList = aSectPoints( i );

      AddSection( aSectName, aSectType, anIsSectionClosed );
      SetPoints( i - 1, aSectPointsList );
    }
  }
  else
  {
    TopoDS_Shape aShape = theShape;

    if ( theShape.ShapeType() == TopAbs_EDGE )
    {
      // We make the wire from incoming edge because of other algorithms
      // are waiting at least the wire from polyline
      TopoDS_Edge anEdge = TopoDS::Edge( theShape );
      BRepBuilderAPI_MakeWire aMakeWire( anEdge );
      aMakeWire.Build();
      if ( aMakeWire.IsDone() )
        aShape = aMakeWire.Wire();
    }

    gp_Pln aPlane( gp_Pnt( 0, 0, 0 ), gp_Dir( 0, 0, 1 ) );
    BRepBuilderAPI_MakeFace aMakeFace( aPlane );
    aMakeFace.Build();
    BRepOffsetAPI_NormalProjection aProj( aMakeFace.Face() );
    aProj.Add( aShape );
    aProj.Build();
    TopoDS_Shape aResult;
    if( aProj.IsDone() )
      aResult = aProj.Shape();

    SetShape( aResult );
  }

  setEditable( anIsCanBeImported );
  Update();
  return true;
}

TopoDS_Wire HYDROData_PolylineXY::BuildWire( const SectionType&                  theType,
                                             const bool&                         theIsClosed,
                                             const NCollection_Sequence<gp_XYZ>& thePoints )
{
  TopoDS_Wire aWire;
  if( theType == SECTION_POLYLINE )
  {
    int aNbPoints = thePoints.Length();
    BRepBuilderAPI_MakePolygon aMakeWire;
    for ( int i = 1, n = aNbPoints; i <= n ; ++i )
    {
      gp_XYZ aPoint = thePoints.Value( i );
      gp_Pnt aPnt( aPoint.X(), aPoint.Y(), aPoint.Z() );
      aMakeWire.Add( aPnt );
    }
    if( theIsClosed && ( aNbPoints > 2 ) )
      aMakeWire.Close();

    if ( aMakeWire.IsDone() )
      aWire = aMakeWire.Wire();
  }
  else //if( theType == PolylineSection::SECTION_SPLINE )
  {
    BRepBuilderAPI_MakeWire aMakeWire;

    if ( thePoints.Size() > 1 )
    {
      Handle(Geom_BSplineCurve) aCurve = 
        HYDROData_BSplineOperation::ComputeCurve( thePoints, theIsClosed, LOCAL_SELECTION_TOLERANCE );

      TopoDS_Edge anEdge = BRepBuilderAPI_MakeEdge( aCurve ).Edge();
      aMakeWire.Add( anEdge );
    }
    aMakeWire.Build();
    if ( aMakeWire.IsDone() )
      aWire = aMakeWire;
  }

  return aWire;
}

void HYDROData_PolylineXY::BuildPainterPath( QPainterPath&                       thePath,
                                             const SectionType&                  theType,
                                             const bool&                         theIsClosed,
                                             const NCollection_Sequence<gp_XYZ>& thePoints )
{
  if ( thePoints.IsEmpty() )
    return;

  if ( theType == SECTION_POLYLINE )
  {
    const gp_XYZ& aFirstPoint = thePoints.Value( 1 );
    thePath.moveTo( aFirstPoint.X(), aFirstPoint.Y() );

    for( int i = 2, n = thePoints.Size(); i <= n; ++i )
    {
      const gp_XYZ& aSectPoint = thePoints.Value( i );

      thePath.lineTo( aSectPoint.X(), aSectPoint.Y() );
    }

    if( theIsClosed )
      thePath.closeSubpath();
  }
  else
  {
    Handle(Geom_BSplineCurve) aCurve = 
      HYDROData_BSplineOperation::ComputeCurve( thePoints, theIsClosed, LOCAL_SELECTION_TOLERANCE );
    HYDROData_BSplineOperation::ComputePath( aCurve, thePath );
  }
}

void HYDROData_PolylineXY::Update()
{
  if ( !IsEditable() )
  {
    // If polyline is not editable we no need to update it wire
    ClearChanged();
    return;
  }

  HYDROData_IPolyline::Update();

  NCollection_Sequence<TCollection_AsciiString>           aSectNames;
  NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
  NCollection_Sequence<bool>                              aSectClosures;
  GetSections( aSectNames, aSectTypes, aSectClosures );

  //BRepBuilderAPI_MakeWire aMakeWire;

  TopTools_ListOfShape aSectionWiresList;

  for ( int aSectionId = 1, aNbSects = aSectNames.Size(); aSectionId <= aNbSects; aSectionId++ )
  {
    TCollection_AsciiString aSectName = aSectNames.Value( aSectionId );
    SectionType aSectionType = aSectTypes.Value( aSectionId );
    bool anIsSectionClosed = aSectClosures.Value( aSectionId );

    PointsList aSectPointsList = GetPoints( aSectionId - 1 );
    if ( aSectPointsList.IsEmpty() )
      continue;
    
    NCollection_Sequence<gp_XYZ> aPoints;
    for( int i = 1, n = aSectPointsList.Size(); i <= n; ++i )
    {
      const Point& aSectPoint = aSectPointsList.Value( i );

      gp_XYZ aPoint( aSectPoint.X(), aSectPoint.Y(), 0.0 );
      aPoints.Append( aPoint );
    }

    TopoDS_Wire aSectionWire = BuildWire( aSectionType, anIsSectionClosed, aPoints );
    if ( !aSectionWire.IsNull() ) {
      aSectionWiresList.Append( aSectionWire );
      //aMakeWire.Add( aSectionWire );
    }
  }
// all input wires in the <aSectionWiresList>

  TopoDS_Shape aResult;
  BRep_Builder aBB;
  TopoDS_Compound aCmp;
  aBB.MakeCompound(aCmp);
  if (aSectionWiresList.Size() == 1)
    aResult = aSectionWiresList.First();
  else if (aSectionWiresList.Size() > 1)
  {
    TopTools_ListIteratorOfListOfShape it(aSectionWiresList);
    for(;it.More();it.Next())
    {
      aBB.Add(aCmp, it.Value());
    }
    aResult = aCmp;
  }

  //Handle(TopTools_HSequenceOfShape) aSeqWires = new TopTools_HSequenceOfShape;
  //Handle(TopTools_HSequenceOfShape) aSeqEdges = new TopTools_HSequenceOfShape;
  //TopTools_ListIteratorOfListOfShape it(aSectionWiresList);
  //for(;it.More();it.Next())
  //{
  //  TopExp_Explorer it2(it.Value(), TopAbs_EDGE);
  //  for(;it2.More();it2.Next()) 
  //    aSeqEdges->Append(it2.Current());
  //}
  //
  //BRep_Builder aBB;
  //TopoDS_Compound aCmp;
  //TopoDS_Shape aResult;
  //aBB.MakeCompound(aCmp);
  //if(aSeqEdges->Length() >1)
  //{
  //  ShapeAnalysis_FreeBounds::ConnectEdgesToWires( aSeqEdges, 1E-5, Standard_True, aSeqWires );
  //
  //  if( aSeqWires->Length()==1 )
  //    aResult = aSeqWires->Value( 1 );
  //  else
  //  {
  //    for (Standard_Integer i = 1; i <= aSeqWires->Length();i++)
  //    {
  //      const TopoDS_Shape& aS1 = aSeqWires->Value(i);
  //      aBB.Add(aCmp, aS1);
  //    }
  //    aResult = aCmp;
  //  }
  //}
  //else if (aSeqEdges->Length() == 1)
  //{
  //  BRepBuilderAPI_MakeWire mkWire (TopoDS::Edge(aSeqEdges->Value(1)));
  //  if (mkWire.IsDone())
  //    aResult = mkWire.Wire();
  //}

  SetShape( aResult );
}

bool HYDROData_PolylineXY::IsHas2dPrs() const
{
  return true;
}

bool HYDROData_PolylineXY::IsEditable() const
{
  return !myLab.IsAttribute( GUID_IS_UNEDITABLE );
}

void HYDROData_PolylineXY::setEditable( const bool theIsEditable )
{
  if ( !theIsEditable )
    TDataStd_UAttribute::Set( myLab, GUID_IS_UNEDITABLE );
  else
    myLab.ForgetAttribute( GUID_IS_UNEDITABLE );
}

/**
 * Returns true if polyline is closed
 */
bool HYDROData_PolylineXY::IsClosed(const bool theIsSimpleCheck) const
{
  DEBTRACE("IsClosed " << theIsSimpleCheck << " " << GetName());
  bool anIsClosed = false;

  TopoDS_Shape aShape = GetShape();
  if (aShape.IsNull())
    return anIsClosed;

  TopTools_SequenceOfShape aWires;
  HYDROData_ShapesTool::ExploreShapeToShapes(aShape, TopAbs_WIRE, aWires);

  int aNbWires = aWires.Length();
  if (theIsSimpleCheck)
    {
      anIsClosed = aNbWires > 0;
      for (int i = 1; i <= aNbWires && anIsClosed; ++i)
        {
          const TopoDS_Shape& aWire = aWires.Value(i);
          anIsClosed = BRep_Tool::IsClosed(aWire);
        }
    }
  else
    {
      if (aNbWires == 1)
        anIsClosed = BRep_Tool::IsClosed(aWires.First());
      else
        {
          DEBTRACE("aNbWires " << aNbWires);
          Handle(TopTools_HSequenceOfShape) aSeqWires = new TopTools_HSequenceOfShape;
          Handle(TopTools_HSequenceOfShape) aSeqEdges = new TopTools_HSequenceOfShape;
          for (int i = 1; i <= aNbWires; ++i)
            {
              const TopoDS_Shape& aWire = aWires.Value(i);
              TopExp_Explorer it2(aWire, TopAbs_EDGE);
              for (; it2.More(); it2.Next())
                aSeqEdges->Append(it2.Current());
            }
          if (aSeqEdges->Length() > 1)
            {
              DEBTRACE(aSeqEdges->Length());
              ShapeAnalysis_FreeBounds::ConnectEdgesToWires(aSeqEdges, 1E-5, Standard_False, aSeqWires);
              if (aSeqWires->Length() == 1)
                {
                  DEBTRACE(aSeqWires->Length());
                  const TopoDS_Wire& aPolylineWire = TopoDS::Wire(aSeqWires->Value(1));
                  anIsClosed = BRep_Tool::IsClosed(aPolylineWire);
                }
            }
        }
    }

  return anIsClosed;
}

int HYDROData_PolylineXY::GetNbConnectedWires(Handle(TopTools_HSequenceOfShape)& aConnectedWires) const
{
  TopoDS_Shape aShape = GetShape();
  if (aShape.IsNull())
    return 0;
  int aNbconnectedWires = 0;
  TopTools_SequenceOfShape aWires;
  HYDROData_ShapesTool::ExploreShapeToShapes(aShape, TopAbs_WIRE, aWires);
  int aNbWires = aWires.Length();
  if (aNbWires == 1)
    {
      aNbconnectedWires = aNbWires;
      aConnectedWires->Append(aWires.First());
    }
  else
    {
      DEBTRACE("aNbWires " << aNbWires);
      Handle(TopTools_HSequenceOfShape) aSeqWires = new TopTools_HSequenceOfShape;
      Handle(TopTools_HSequenceOfShape) aSeqEdges = new TopTools_HSequenceOfShape;
      for (int i = 1; i <= aNbWires; ++i)
        {
          const TopoDS_Shape& aWire = aWires.Value(i);
          TopExp_Explorer it2(aWire, TopAbs_EDGE);
          for (; it2.More(); it2.Next())
            aSeqEdges->Append(it2.Current());
        }
      if (aSeqEdges->Length() > 1)
        {
          DEBTRACE(aSeqEdges->Length());
          ShapeAnalysis_FreeBounds::ConnectEdgesToWires(aSeqEdges, 1E-5, Standard_False, aSeqWires);
        }
      aConnectedWires = aSeqWires;
      aNbconnectedWires = aConnectedWires->Length();
      DEBTRACE("aNbconnectedWires " << aNbconnectedWires);
    }
return aNbconnectedWires;
}

double HYDROData_PolylineXY::GetDistance( const int theSectionIndex,
                                          const int thePointIndex ) const
{
  double aResDistance = -1;
  if ( theSectionIndex < 0 || theSectionIndex >= NbSections() )
    return aResDistance;

  if ( thePointIndex == 0 )
    return 0.0;

  SectionType aSectionType = GetSectionType( theSectionIndex );
  bool anIsSectionClosed = IsClosedSection( theSectionIndex );
  PointsList aSectPointsList = GetPoints( theSectionIndex );
  if ( thePointIndex < 0 || thePointIndex >= aSectPointsList.Size()  )
    return aResDistance;

  if ( aSectionType == SECTION_POLYLINE )
  {
    aResDistance = 0.0;
  
    Point aPrevPoint = aSectPointsList.Value( 1 );
    for ( int i = 2, aNbPoints = aSectPointsList.Size(); i <= aNbPoints; ++i )
    {
      const Point& aSectPoint = aSectPointsList.Value( i );
      aResDistance += gp_Pnt2d( aPrevPoint ).Distance( aSectPoint );
      aPrevPoint = aSectPoint;

      if ( thePointIndex == i - 1 )
        break;
    }
  }
  else
  {
    gp_XYZ aPointToTest;

    int aSectNbPoints = aSectPointsList.Size();
    NCollection_Sequence<gp_XYZ> aPoints;
    for( int i = 1 ; i <= aSectNbPoints; ++i )
    {
      const Point& aSectPoint = aSectPointsList.Value( i );

      gp_XYZ aPoint( aSectPoint.X(), aSectPoint.Y(), 0.0 );
      aPoints.Append( aPoint );

      if ( thePointIndex == i - 1 )
        aPointToTest = aPoint;
    }

    Handle(Geom_BSplineCurve) aCurve = 
      HYDROData_BSplineOperation::ComputeCurve( aPoints, anIsSectionClosed, LOCAL_SELECTION_TOLERANCE );

    Quantity_Parameter aFirstParam = aCurve->FirstParameter();
    Quantity_Parameter aSecondParam = aCurve->LastParameter();

    if ( thePointIndex != aSectNbPoints - 1 )
    {
      GeomAPI_ProjectPointOnCurve aProject( aPointToTest, aCurve );
      aSecondParam = aProject.LowerDistanceParameter();
    }

    GeomAdaptor_Curve anAdap( aCurve );
    
    aResDistance = GCPnts_AbscissaPoint::Length( anAdap, aFirstParam, aSecondParam );
  }

  return aResDistance;
}

int HYDROData_PolylineXY::NbSections() const
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList)   aTypesList;
  Handle(TDataStd_BooleanList)   aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );

  return !aClosuresList.IsNull() ? aClosuresList->Extent() : 0;
}

void HYDROData_PolylineXY::AddSection( const TCollection_AsciiString& theSectName,
                                       const SectionType              theSectionType,
                                       const bool                     theIsClosed )
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList)   aTypesList;
  Handle(TDataStd_BooleanList)   aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList );

  TCollection_ExtendedString aSectName( theSectName );
  if ( aSectName.Length() <= 0 )
    aSectName = getUniqueSectionName( aNamesList );

  aNamesList->Append( aSectName );
  aTypesList->Append( theSectionType );
  aClosuresList->Append( theIsClosed );

  Changed( Geom_2d );
}

TCollection_AsciiString HYDROData_PolylineXY::GetSectionName( const int theSectionIndex ) const
{
  TCollection_AsciiString aResName;

  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aNamesList.IsNull() || theSectionIndex >= aNamesList->Extent() )
    return aResName;

  TDataStd_ListIteratorOfListOfExtendedString aNamesIter( aNamesList->List() );
  for ( int i = 0; aNamesIter.More() && i != theSectionIndex; aNamesIter.Next(), ++i );

  if ( aNamesIter.More() )
    aResName = aNamesIter.Value();

  return aResName;
}

void HYDROData_PolylineXY::SetSectionName( const int                      theSectionIndex, 
                                           const TCollection_AsciiString& theSectionName )
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aNamesList.IsNull() || theSectionIndex >= aNamesList->Extent() )
    return;

  TDataStd_ListOfExtendedString anOldNamesList;
  anOldNamesList = aNamesList->List();

  // Refill the existing list
  aNamesList->Clear();

  TCollection_ExtendedString aNewSectName = theSectionName;

  TDataStd_ListIteratorOfListOfExtendedString aNamesIter( anOldNamesList );
  for ( int i = 0; aNamesIter.More(); aNamesIter.Next(), ++i )
    aNamesList->Append( i == theSectionIndex ? aNewSectName : aNamesIter.Value() );
}

HYDROData_PolylineXY::SectionType HYDROData_PolylineXY::GetSectionType( const int theSectionIndex ) const
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aTypesList.IsNull() || theSectionIndex >= aTypesList->Extent() )
    return SECTION_POLYLINE;

  TColStd_ListIteratorOfListOfInteger aTypesIter( aTypesList->List() );
  for ( int i = 0; aTypesIter.More() && i != theSectionIndex; aTypesIter.Next(), ++i );

  return aTypesIter.More() ? (SectionType)aTypesIter.Value() : SECTION_POLYLINE;
}

void HYDROData_PolylineXY::SetSectionType( const int         theSectionIndex, 
                                           const SectionType theSectionType )
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aTypesList.IsNull() || theSectionIndex >= aTypesList->Extent() )
    return;

  TColStd_ListOfInteger anOldTypesList;
  anOldTypesList = aTypesList->List();

  // Refill the existing list
  aTypesList->Clear();

  TColStd_ListIteratorOfListOfInteger aTypesIter( anOldTypesList );
  for ( int i = 0; aTypesIter.More(); aTypesIter.Next(), ++i )
    aTypesList->Append( i == theSectionIndex ? theSectionType : aTypesIter.Value() );

  Changed( Geom_2d );
}

bool HYDROData_PolylineXY::IsClosedSection( const int theSectionIndex ) const
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aClosuresList.IsNull() || theSectionIndex >= aClosuresList->Extent() )
    return false;

  TDataStd_ListIteratorOfListOfByte aClosuresIter( aClosuresList->List() );
  for ( int i = 0; aClosuresIter.More() && i != theSectionIndex; aClosuresIter.Next(), ++i );

  return aClosuresIter.More() ? (bool)aClosuresIter.Value() : false;
}

void HYDROData_PolylineXY::SetSectionClosed( const int  theSectionIndex, 
                                             const bool theIsClosed )
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aClosuresList.IsNull() || theSectionIndex >= aClosuresList->Extent() )
    return;

  TDataStd_ListOfByte anOldClosuresList;
  anOldClosuresList = aClosuresList->List();

  // Refill the existing list
  aClosuresList->Clear();

  TDataStd_ListIteratorOfListOfByte aClosuresIter( anOldClosuresList );
  for ( int i = 0; aClosuresIter.More(); aClosuresIter.Next(), ++i )
    aClosuresList->Append( i == theSectionIndex ? theIsClosed : (bool)aClosuresIter.Value() );

  Changed( Geom_2d );
}

void HYDROData_PolylineXY::GetSections( NCollection_Sequence<TCollection_AsciiString>& theSectNames,
                                        NCollection_Sequence<SectionType>&             theSectTypes,
                                        NCollection_Sequence<bool>&                    theSectClosures ) const
{
  theSectNames.Clear();
  theSectTypes.Clear();
  theSectClosures.Clear();

  if( IsCustom() )
  {
    const_cast<HYDROData_PolylineXY*>( this )->Interpolate();
  }

  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList) aTypesList;
  Handle(TDataStd_BooleanList) aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aNamesList.IsNull() || aTypesList.IsNull() || aClosuresList.IsNull() )
    return;

  TDataStd_ListIteratorOfListOfExtendedString aNamesIter( aNamesList->List() );
  TColStd_ListIteratorOfListOfInteger aTypesIter( aTypesList->List() );
  TDataStd_ListIteratorOfListOfByte aClosuresIter( aClosuresList->List() );
  for ( ; aNamesIter.More() && aTypesIter.More() && aClosuresIter.More();
          aNamesIter.Next(), aTypesIter.Next(), aClosuresIter.Next() )
  {
    const TCollection_ExtendedString& aSectName = aNamesIter.Value();
    SectionType aSectType = (SectionType)aTypesIter.Value();
    bool aSectClosures = aClosuresIter.Value();

    theSectNames.Append( aSectName );
    theSectTypes.Append( aSectType );
    theSectClosures.Append( aSectClosures );
  }
}

void HYDROData_PolylineXY::RemoveSection( const int theSectionIndex )
{
  Handle(TDataStd_ExtStringList) aNamesList;
  Handle(TDataStd_IntegerList)   aTypesList;
  Handle(TDataStd_BooleanList)   aClosuresList;
  getSectionsLists( aNamesList, aTypesList, aClosuresList, false );
  if ( aNamesList.IsNull() || theSectionIndex >= aNamesList->Extent() )
    return;

  if ( aNamesList->Extent() == 1 )
  {
    removeSectionsLists();
    removePointsLists();
    removeSectionColor();
  }
  else
  {
    TDataStd_ListOfExtendedString anOldNamesList;
    anOldNamesList = aNamesList->List();

    TColStd_ListOfInteger anOldTypesList;
    anOldTypesList = aTypesList->List();

    TDataStd_ListOfByte anOldClosuresList;
    anOldClosuresList = aClosuresList->List();

    // Refill the existing lists
    aNamesList->Clear();
    aTypesList->Clear();
    aClosuresList->Clear();

    TDataStd_ListIteratorOfListOfExtendedString aNamesIter( anOldNamesList );
    TColStd_ListIteratorOfListOfInteger aTypesIter( anOldTypesList );
    TDataStd_ListIteratorOfListOfByte aClosuresIter( anOldClosuresList );
    for ( int i = 0; aNamesIter.More() && aTypesIter.More() && aClosuresIter.More();
                     aNamesIter.Next(), aTypesIter.Next(), aClosuresIter.Next(), ++i )
    {
      if ( i == theSectionIndex )
        continue; // skip index to remove

      aNamesList->Append( aNamesIter.Value() );
      aTypesList->Append( aTypesIter.Value() );
      aClosuresList->Append( (bool)aClosuresIter.Value() );
    }

    // Remove points that belongs to removed section
    removePointsLists( theSectionIndex ); 
    removeSectionColor (theSectionIndex);
  }

  Changed( Geom_2d );
}

void HYDROData_PolylineXY::RemoveSections()
{
  removeSectionsLists();
  removePointsLists();
  Changed( Geom_2d );
}

void HYDROData_PolylineXY::AddPoint( const int    theSectionIndex,
                                     const Point& thePoint,
                                     const int    thePointIndex )
{
  Handle(TDataStd_RealList) aListX, aListY;
  getPointsLists( theSectionIndex, aListX, aListY );

  if ( thePointIndex < 0 || thePointIndex >= aListX->Extent() )
  {
    aListX->Append( thePoint.X() );
    aListY->Append( thePoint.Y() );
  }
  else
  {
    TColStd_ListOfReal anOldListX;
    anOldListX = aListX->List();

    TColStd_ListOfReal anOldListY;
    anOldListY = aListY->List();

    // Refill the existing lists
    aListX->Clear();
    aListY->Clear();

    TColStd_ListIteratorOfListOfReal anIterX( anOldListX );
    TColStd_ListIteratorOfListOfReal anIterY( anOldListY );
    for ( int i = 0; anIterX.More() && anIterY.More(); anIterX.Next(), anIterY.Next(), ++i )
    {
      double aCoordX = anIterX.Value();
      double aCoordY = anIterY.Value();

      if ( i == thePointIndex )
      {
        // Insert our new point
        aListX->Append( thePoint.X() );
        aListY->Append( thePoint.Y() );
      }

      aListX->Append( aCoordX );
      aListY->Append( aCoordY );
    }
  }

  Changed( Geom_2d );
}

void HYDROData_PolylineXY::SetPoint( const int    theSectionIndex,
                                     const Point& thePoint,
                                     const int    thePointIndex )
{
  Handle(TDataStd_RealList) aListX, aListY;
  getPointsLists( theSectionIndex, aListX, aListY );

  if ( thePointIndex < 0 )
  {
    aListX->Prepend( thePoint.X() );
    aListY->Prepend( thePoint.Y() );
  }
  else if ( thePointIndex >= aListX->Extent() )
  {
    aListX->Append( thePoint.X() );
    aListY->Append( thePoint.Y() );
  }
  else
  {
    TColStd_ListOfReal anOldListX;
    anOldListX = aListX->List();

    TColStd_ListOfReal anOldListY;
    anOldListY = aListY->List();

    // Refill the existing lists
    aListX->Clear();
    aListY->Clear();

    TColStd_ListIteratorOfListOfReal anIterX( anOldListX );
    TColStd_ListIteratorOfListOfReal anIterY( anOldListY );
    for ( int i = 0; anIterX.More() && anIterY.More(); anIterX.Next(), anIterY.Next(), ++i )
    {
      double aCoordX = anIterX.Value();
      double aCoordY = anIterY.Value();

      if ( i == thePointIndex )
      {
        // Insert our new point instead of old one
        aCoordX = thePoint.X();
        aCoordY = thePoint.Y();
      }

      aListX->Append( aCoordX );
      aListY->Append( aCoordY );
    }
  }

  Changed( Geom_2d );
}

void HYDROData_PolylineXY::SetPoints( const int         theSectionIndex,
                                      const PointsList& thePoints )
{
  Handle(TDataStd_RealList) aListX, aListY;
  getPointsLists( theSectionIndex, aListX, aListY );

  aListX->Clear();
  aListY->Clear();

  for ( int i = 1, n = thePoints.Length(); i <= n; ++i )
  {
    const Point& aPoint = thePoints.Value( i );
    aListX->Append( aPoint.X() );
    aListY->Append( aPoint.Y() );
  }
}

void HYDROData_PolylineXY::RemovePoint( const int theSectionIndex,
                                        const int thePointIndex )
{
  Handle(TDataStd_RealList) aListX, aListY;
  getPointsLists( theSectionIndex, aListX, aListY, false );
  if ( aListX.IsNull() || aListY.IsNull() || aListX->IsEmpty() )
    return;

  if ( aListX->Extent() == 1 )
  {
    removePointsLists( theSectionIndex );
  }
  else
  {
    TColStd_ListOfReal anOldListX;
    anOldListX = aListX->List();

    TColStd_ListOfReal anOldListY;
    anOldListY = aListY->List();

    // Refill the existing lists
    aListX->Clear();
    aListY->Clear();

    TColStd_ListIteratorOfListOfReal anIterX( anOldListX );
    TColStd_ListIteratorOfListOfReal anIterY( anOldListY );
    for ( int i = 0; anIterX.More() && anIterY.More(); anIterX.Next(), anIterY.Next(), ++i )
    {
      if ( i == thePointIndex )
        continue; // skip index to remove

      aListX->Append( anIterX.Value() );
      aListY->Append( anIterY.Value() );
    }
  }

  Changed( Geom_2d );
}

HYDROData_PolylineXY::PointsList HYDROData_PolylineXY::GetPoints( const int theSectionIndex, bool IsConvertToGlobal ) const
{
  PointsList aResList;

  if( IsCustom() )
  {
    const_cast<HYDROData_PolylineXY*>( this )->Interpolate();
  }
 
  Handle(TDataStd_RealList) aListX, aListY;
  getPointsLists( theSectionIndex, aListX, aListY, false );
  if ( aListX.IsNull() || aListY.IsNull() || aListX->IsEmpty() )
    return aResList;

  TColStd_ListIteratorOfListOfReal anIterX( aListX->List() );
  TColStd_ListIteratorOfListOfReal anIterY( aListY->List() );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  for ( ; anIterX.More() && anIterY.More(); anIterX.Next(), anIterY.Next() )
  {
    Point aPoint( anIterX.Value(), anIterY.Value() );
    if (IsConvertToGlobal)
      aDoc->Transform( aPoint, false );
    aResList.Append( aPoint );
  }

  return aResList;
}

QPainterPath HYDROData_PolylineXY::GetPainterPath() const
{
  QPainterPath aPath;

  NCollection_Sequence<TCollection_AsciiString>           aSectNames;
  NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
  NCollection_Sequence<bool>                              aSectClosures;
  GetSections( aSectNames, aSectTypes, aSectClosures );

  for ( int aSectionId = 1, aNbSects = aSectNames.Size(); aSectionId <= aNbSects; aSectionId++ )
  {
    TCollection_AsciiString aSectName = aSectNames.Value( aSectionId );
    SectionType aSectionType = aSectTypes.Value( aSectionId );
    bool anIsSectionClosed = aSectClosures.Value( aSectionId );

    PointsList aSectPointsList = GetPoints( aSectionId - 1 );
    if ( aSectPointsList.IsEmpty() )
      continue;

    NCollection_Sequence<gp_XYZ> aPoints;
    for( int i = 1, n = aSectPointsList.Size(); i <= n; ++i )
    {
      const Point& aSectPoint = aSectPointsList.Value( i );

      gp_XYZ aPoint( aSectPoint.X(), aSectPoint.Y(), 0.0 );
      aPoints.Append( aPoint );
    }

    BuildPainterPath( aPath, aSectionType, anIsSectionClosed, aPoints );
  }

  return aPath;
}

void HYDROData_PolylineXY::UpdateLocalCS( double theDx, double theDy )
{
  NCollection_Sequence<TCollection_AsciiString>           aSectNames;
  NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
  NCollection_Sequence<bool>                              aSectClosures;
  GetSections( aSectNames, aSectTypes, aSectClosures );

  gp_XY aDelta( theDx, theDy );
  for ( int i = 0, aNbSects = aSectNames.Size(); i < aNbSects; i++ )
  {
    PointsList aPoints = GetPoints( i );
    for( int j = 1, n = aPoints.Size(); j <= n; ++j )
    {
      Point& aPoint = aPoints.ChangeValue( j );
      aPoint += aDelta;
    }
    SetPoints( i, aPoints );
  }
  Changed( Geom_2d );
}

void HYDROData_PolylineXY::Transform( const QTransform& theTrsf )
{
  NCollection_Sequence<TCollection_AsciiString>           aSectNames;
  NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
  NCollection_Sequence<bool>                              aSectClosures;
  GetSections( aSectNames, aSectTypes, aSectClosures );

  for ( int i = 0, aNbSects = aSectNames.Size(); i < aNbSects; i++ ) {
    PointsList aPoints = GetPoints( i );
    for( int j = 1, n = aPoints.Size(); j <= n; ++j ) {
      Point& aPoint = aPoints.ChangeValue( j );

      QPointF aTrsfPoint = theTrsf.map( QPointF( aPoint.X(), aPoint.Y() ) );

      aPoint.SetX( aTrsfPoint.x() );
      aPoint.SetY( aTrsfPoint.y() );
    }
    SetPoints( i, aPoints );
  }

  Update();
}

bool HYDROData_PolylineXY::IsCustom() const
{
  if( myIsInCustomFlag )
    return false;

  bool isNull = GetShape().IsNull();
  int aNbPoints = 0;

  HYDROData_PolylineXY* aThat = const_cast<HYDROData_PolylineXY*>( this );
  aThat->myIsInCustomFlag = true;
  for( int i=0, n=NbSections(); i<n; i++ )
    aNbPoints += NbPoints( i );
  aThat->myIsInCustomFlag = false;

  return !isNull && aNbPoints == 0;
}

bool HYDROData_PolylineXY::GetIsInCustomFlag() const
{
  return myIsInCustomFlag;
}

void HYDROData_PolylineXY::SetIsInCustomFlag( bool theValue )
{
  myIsInCustomFlag = theValue;
}

void HYDROData_PolylineXY::Interpolate()
{
  ImportShape( GetShape(), true, NULL );
}

void HYDROData_PolylineXY::SetDBFInfo( const QStringList& theDBFTable )   
{ 
  int i = 1;
  Handle_TDataStd_ExtStringArray TExtStrArr = 
    TDataStd_ExtStringArray::Set( myLab.FindChild( DataTag_DBFTableInfo), 1, theDBFTable.size() );
  foreach (QString val, theDBFTable)
  {
    std::string StdVal = val.toStdString();
    const char* aCVal = StdVal.c_str();
    TExtStrArr->SetValue(i, TCollection_ExtendedString(aCVal));
    i++;
  }
}

bool HYDROData_PolylineXY::GetDBFInfo(QStringList& theDBFTable) const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_DBFTableInfo, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_ExtStringArray) anExtStrArr;
    if ( aLabel.FindAttribute( TDataStd_ExtStringArray::GetID(), anExtStrArr ) )
    {
      for (int i = anExtStrArr->Lower(); i <= anExtStrArr->Upper(); i++ )
      {
        Standard_ExtString str = anExtStrArr->Value(i).ToExtString();
        TCollection_AsciiString aText (str);
        theDBFTable << QString(aText.ToCString());
      }
    }
  }
  else
    return false;
  return true;
}


HYDROData_SequenceOfObjects HYDROData_PolylineXY::ImportShapesFromFile(const QString& aFileName)
{
    HYDROData_SequenceOfObjects importedEntities;
  if (aFileName.isEmpty())
    return importedEntities;

  QString anExt = aFileName.split('.', QString::SkipEmptyParts).back();
  anExt.toLower();
  bool importXY = false;
  Handle(HYDROData_Document) theDocument = HYDROData_Document::Document();

  importXY = true;
  if (anExt == "shp")
    {
      HYDROData_ShapeFile anImporter;
      NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*> theEntitiesToSHPObj;
      NCollection_Sequence<Handle(HYDROData_Entity)> PolyEnt;
      int aShapeTypeOfFile = -1;
      int aStat = anImporter.OpenAndParse(aFileName);
      if (aStat == 1)
        {
          aStat = anImporter.ImportPolylines(theDocument, aFileName, theEntitiesToSHPObj,
                                             HYDROData_ShapeFile::ImportShapeType_All);
          for (int k = 1; k <= theEntitiesToSHPObj.Extent(); k++)
            {
              PolyEnt.Append(theEntitiesToSHPObj.FindKey(k));
            }
          if (aStat == 1 || aStat == 2)
            {
              //try to import DBF
              QString aDBFFileName;
              aDBFFileName = aFileName.simplified().replace(aFileName.simplified().size() - 4, 4, ".dbf");
              bool DBF_Stat = anImporter.DBF_OpenDBF(aDBFFileName);
              if (DBF_Stat)
                {
                  QStringList aFieldList = anImporter.DBF_GetFieldList();
                  int nbRecords = anImporter.DBF_GetNbRecords();
                  if (theEntitiesToSHPObj.Extent() == nbRecords)
                    {
                      int indNameAttrFound = -1;
                      int k = 0;
                      bool bUseNameAttrFound = false;
                      for (QStringList::iterator it = aFieldList.begin(); it != aFieldList.end(); it++, k++)
                        if (QString::compare(*it, "name", Qt::CaseInsensitive) == 0)
                          {
                            indNameAttrFound = k;
                            break;
                          }

                      if (indNameAttrFound != -1)
                        bUseNameAttrFound = true;

                      std::vector<std::vector<HYDROData_ShapeFile::DBF_AttrValue>> anAttrVV;
                      for (int i = 0; i < aFieldList.size(); i++)
                        {
                          std::vector<HYDROData_ShapeFile::DBF_AttrValue> anAttrV;
                          anAttrVV.push_back(anAttrV);
                          anImporter.DBF_GetAttributeList(i, anAttrVV[i]);
                        }

                      int indNULL = 1;
                      for (int i = 1; i <= theEntitiesToSHPObj.Extent(); i++)
                        {
                          Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast(
                              theEntitiesToSHPObj.FindKey(i));
                          QStringList aDBFinfo;
                          aDBFinfo << aFieldList; //first, the table header
                          for (int j = 0; j < aFieldList.size(); j++)
                            {
                              QString attr;
                              const HYDROData_ShapeFile::DBF_AttrValue &attrV = anAttrVV[j][i - 1];
                              if (attrV.myFieldType == HYDROData_ShapeFile::DBF_FieldType_String)
                                aDBFinfo << attrV.myStrVal;
                              else if (attrV.myFieldType == HYDROData_ShapeFile::DBF_FieldType_Integer)
                                aDBFinfo << QString::number(attrV.myIntVal);
                              else if (attrV.myFieldType == HYDROData_ShapeFile::DBF_FieldType_Double)
                                aDBFinfo << QString::number(attrV.myDoubleVal);
                              else
                                aDBFinfo << "";
                            }
                          aPolylineXY->SetDBFInfo(aDBFinfo);
                          if (bUseNameAttrFound)
                            {
                              QString nameOfPoly = aDBFinfo[aDBFinfo.size() / 2 + indNameAttrFound];
                              if (!nameOfPoly.isEmpty())
                                aPolylineXY->SetName(nameOfPoly);
                              else
                                {
                                  aPolylineXY->SetName("null_name_" + QString::number(indNULL));
                                  indNULL++;
                                }
                            }
                        }
                    }
                }
            }

          if (anImporter.GetShapeType() == 13) //Polyline 3D
            {
              NCollection_Sequence<Handle(HYDROData_Entity)> CheckedPolylines = PolyEnt; //aDLG->GetCheckedPolylines();
              NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*> CheckedEntitiesToSHPObj;
              for (int k = 1; k <= CheckedPolylines.Size(); k++)
                {
                  SHPObject *const*SHPObkP = theEntitiesToSHPObj.Seek(CheckedPolylines(k));
                  if (SHPObkP != NULL)
                    CheckedEntitiesToSHPObj.Add(CheckedPolylines(k), *SHPObkP);
                }
              NCollection_Sequence<Handle(HYDROData_Entity)> theEntitiesPoly3D;
              anImporter.ImportPolylines3D(theDocument, CheckedEntitiesToSHPObj, theEntitiesPoly3D,
                                           HYDROData_ShapeFile::ImportShapeType_All);
              PolyEnt.Append(theEntitiesPoly3D);

            }
          anImporter.Free();
        }
      importedEntities.Append(PolyEnt);
    }
  else if (anExt == "xy" || (importXY && anExt == "xyz"))
    {
      if (!HYDROData_Tool::importPolylineFromXYZ(aFileName, theDocument, true, importedEntities))
        DEBTRACE("no polyline imported");
    }
  else if (anExt == "xyz")
    {
      if (!HYDROData_Tool::importPolylineFromXYZ(aFileName, theDocument, false, importedEntities))
        DEBTRACE("no polyline imported");
    }
  return importedEntities;
}


void HYDROData_PolylineXY::ExportShapeXY(Handle(HYDROData_Document) theDocument,
                                         const QString& aFileName,
                                         const NCollection_Sequence<Handle(HYDROData_PolylineXY)>& aPolyXYSeq,
                                         QStringList& aNonExpList)
{
    HYDROData_ShapeFile shp = HYDROData_ShapeFile();
    NCollection_Sequence<Handle(HYDROData_Polyline3D)> emptySeq;
    shp.Export(theDocument, aFileName, aPolyXYSeq, emptySeq, aNonExpList);
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_ProfileUZ_HeaderFile
#define HYDROData_ProfileUZ_HeaderFile

#include "HYDROData_IPolyline.h"

class gp_XYZ;
class HYDROData_PolylineXY;

/**\class HYDROData_ProfileUZ
 * \brief Class that stores/retreives information about the 
 *        parametric profile points.
 */
class HYDROData_ProfileUZ : public HYDROData_IPolyline
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_IPolyline::DataTag_First + 100, ///< first tag, to reserve
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_ProfileUZ, HYDROData_IPolyline);


  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_PROFILEUZ;}

public:

  /**
   * Returns the 3D presentation of all points.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape() const;

  /**
   * Returns the depth for given distance.
   */
  HYDRODATA_EXPORT static double GetDepthFromDistance( const PointsList& thePoints,
                                                       const double&     theDistance );


  /**
   * Returns number of sections.
   */
  HYDRODATA_EXPORT virtual int NbSections() const;

  /**
   * Adds new one section.
   * \param theSectName name of the section
   * \param theSectionType type of section
   * \param theIsClosed flag indicates closures of section
   */
  HYDRODATA_EXPORT virtual void AddSection( const TCollection_AsciiString& theSectName,
                                            const SectionType              theSectionType,
                                            const bool                     theIsClosed );

  /**
   * Returns name of section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual TCollection_AsciiString GetSectionName( const int theSectionIndex ) const;

  /**
   * Set name for section with given index.
   * \param theSectionIndex index of section
   * \param theSectionName new section name
   */
  HYDRODATA_EXPORT virtual void SetSectionName( const int                      theSectionIndex, 
                                                const TCollection_AsciiString& theSectionName );

  /**
   * Returns type of section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual SectionType GetSectionType( const int theSectionIndex ) const;

  /**
   * Set type for section with given index.
   * \param theSectionIndex index of section
   * \param theSectionType new section type
   */
  HYDRODATA_EXPORT virtual void SetSectionType( const int         theSectionIndex, 
                                                const SectionType theSectionType );

  /**
   * Returns true if section with given index is closed.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual bool IsClosedSection( const int theSectionIndex ) const;

  /**
   * Set closed flag for section with given index.
   * \param theSectionIndex index of section
   * \param theIsClosed new closures state
   */
  HYDRODATA_EXPORT virtual void SetSectionClosed( const int  theSectionIndex, 
                                                  const bool theIsClosed );

  /**
   * Removes section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual void RemoveSection( const int theSectionIndex );

  /**
   * Removes all sections.
   */
  HYDRODATA_EXPORT virtual void RemoveSections();


  /**
   * Adds new point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoint point to add
   * \param theBeforeIndex if not equal -1 then insert point in this pos
   */
  HYDRODATA_EXPORT virtual void AddPoint( const int    theSectionIndex,
                                          const Point& thePoint,
                                          const int    thePointIndex = -1 );

  /**
   * Replaces point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoint new point
   * \param thePointIndex index of point to replace
   */
  HYDRODATA_EXPORT virtual void SetPoint( const int    theSectionIndex,
                                          const Point& thePoint,
                                          const int    thePointIndex );

  /**
   * Removes point from section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePointIndex index of point
   */
  HYDRODATA_EXPORT virtual void RemovePoint( const int theSectionIndex,
                                             const int thePointIndex );


  /**
   * Returns list of points.
   * \param theSectionIndex if not equal -1 then list of points returned
   *                        only for section with this index
   * \return list of points
   */
  HYDRODATA_EXPORT virtual PointsList GetPoints( const int theSectionIndex = -1, bool IsConvertToGlobal = false ) const;

  HYDRODATA_EXPORT virtual void CalculateAndAddPoints(const NCollection_Sequence<gp_XYZ>& theXYZPoints,
     Handle(HYDROData_PolylineXY)& thePolylineXY, bool fillPolyXY);

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_ProfileUZ();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_ProfileUZ();
};

#endif

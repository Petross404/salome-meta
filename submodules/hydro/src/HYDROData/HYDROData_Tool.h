// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Tool_HeaderFile
#define HYDROData_Tool_HeaderFile

#include "HYDROData.h"
#include <QStringList>
#include <Precision.hxx>
#include <QVector>
#include <TopTools_SequenceOfShape.hxx>
#include <NCollection_IndexedDataMap.hxx>
#include <Message_ProgressIndicator.hxx>
#include <Message_ProgressRange.hxx>
#include <Geom2d_Curve.hxx>
#include <Geom_Curve.hxx>
#include <BRepAdaptor_Curve.hxx>


class HYDROData_PolylineXY;
class HYDROData_Document;
class HYDROData_Entity;
class HYDROData_SequenceOfObjects;
class MapOfTreatedObjects;
class gp_XY;
class QColor;
class QFile;
class TCollection_ExtendedString;

#ifdef WIN32
  enum TopAbs_State;
#else
  #include <TopAbs_State.hxx>
#endif
class TopoDS_Edge;
class TopoDS_Face;
class TopoDS_Shape;
class TopoDS_Wire;
class Quantity_Color;
class QColor;

class HYDRODATA_EXPORT HYDROData_Tool {

public:
  enum ExecStatus
  {
    None,
    Running,
    Finished
  };

public:

  static void                           WriteStringsToFile( QFile&             theFile,
                                                            const QStringList& theStrings,
                                                            const QString&     theSep = "\n" );

  /**
   * \brief Generate name for new object.
   * \param theDoc document
   * \param thePrefix name prefix
   * \param theUsedNames list of already used names
   * \param theIsTryToUsePurePrefix if true - the prefix will be returned if the name equal to the prefix is not busy
   * \return generated name
   */
  static QString GenerateObjectName( const Handle(HYDROData_Document)& theDoc,
                                                            const QString&                   thePrefix,
                                                            const QStringList&               theUsedNames = QStringList(),
                                                            const bool                       theIsTryToUsePurePrefix = false );


  static bool ExtractGeneratedObjectName(const QString& theName, int& outValue, QString& thePrefName);

  /**
   * \brief Checks the type of object.
   * \param theObject object to check
   * \return true if object is geometry object
   */
  static bool                           IsGeometryObject( const Handle(HYDROData_Entity)& theObject );

  /**
   * \brief Updates the child object name.
   * \param theOldStr old father object name
   * \param theNewStr new father object name
   * \param theObject object to update
   */
  static void                           UpdateChildObjectName( const QString&                  theOldStr,
                                                               const QString&                  theNewStr,
                                                               const Handle(HYDROData_Entity)& theObject );


  /**
   * \brief Generate name for new object in python environment.
   * \param theTreatedObjects objects which was alredy created by dump operation
   * \param thePrefix name prefix
   * \return generated name
   */
  static QString                        GenerateNameForPython( const MapOfTreatedObjects& theTreatedObjects,
                                                               const QString&             thePrefix );
 /**
  * Computes Point State from TopAbs (simplified & limited method).
  */

  static TopAbs_State                    ComputePointState( const gp_XY& thePnt2d,
	                                                    const TopoDS_Face& theFace );

  static double GetAltitudeForEdge( const TopoDS_Edge& theEdge,
                                    const gp_XY& thePoint,
                                    double theParameterTolerance,
                                    double theSquareDistanceTolerance,
                                    double theInvalidAltitude );
  static double GetAltitudeForWire( const TopoDS_Wire& theWire,
                                    const gp_XY& thePoint,
                                    double theParameterTolerance,
                                    double theSquareDistanceTolerance,
                                    double theInvalidAltitude );

  /**
   * \brief Returns the first shape from the group.
   * \param theGroups the list of groups
   * \param theGroupId the group id
   */
  static TopoDS_Shape getFirstShapeFromGroup( const HYDROData_SequenceOfObjects& theGroups,
                                              const int                          theGroupId );

  static TCollection_ExtendedString toExtString( const QString& );
  static QString                    toQString( const TCollection_ExtendedString& );

  static Quantity_Color toOccColor( const QColor& );
  static QColor toQtColor( const Quantity_Color& );

  static QColor GenerateRandColor();
  static bool GenerateNonRepeatableRandColors(int nbColorsToGen, QVector<QColor>& theColors);
  static void GenerateRepeatableRandColors(int nbColorsToGen, QVector<QColor>& theColors);

  static bool IsNan( double theValue );
  static bool IsInf( double theValue );

  /**
  Rebuilds shape container (like compound/compsolid/shell) which contains faces (shared or nonshared with each other)
  */
  static TopoDS_Shape RebuildCmp(const TopoDS_Shape& in);

  static TopoDS_Shape PolyXY2Face(const Handle(HYDROData_PolylineXY)& aPolyline);

  static bool importFromXYZ( QString& theFileName, bool bImportXY, std::vector<gp_XYZ>& thePointsXYZ,
                             std::vector<gp_XY>& thePointsXY);

  static bool importPolylineFromXYZ(QString aFilename, Handle(HYDROData_Document) theDocument,
                                    bool importXY, NCollection_Sequence<Handle(HYDROData_Entity)>& importedEntities);

  static Handle(Geom2d_Curve) BRepAdaptorTo2DCurve( const BRepAdaptor_Curve& ad );

  static void SetSIProgress(const Handle(Message_ProgressIndicator)& thePI);
  static const Message_ProgressRange& GetSIProgress();

  static void SetZIProgress(const Handle(Message_ProgressIndicator)& thePI);
  static const Message_ProgressRange& GetZIProgress();

  static void SetTriangulationStatus(const ExecStatus& theStatus);
  static const ExecStatus& GetTriangulationStatus();

private:
  static Message_ProgressRange& StricklerInterpolationProgress();
  static Message_ProgressRange& BathymetryInterpolationProgress();
  static ExecStatus myTriangulationStatus;





};

inline bool ValuesEquals( const double& theFirst, const double& theSecond )
{
  return theFirst > ( theSecond - Precision::Confusion() ) &&
         theFirst < ( theSecond + Precision::Confusion() );
}

inline bool ValuesMoreEquals( const double& theFirst, const double& theSecond )
{
  return theFirst >= theSecond || ValuesEquals( theFirst, theSecond );
}

inline  bool ValuesLessEquals( const double& theFirst, const double& theSecond )
{
  return theFirst <= theSecond || ValuesEquals( theFirst, theSecond );
}

HYDRODATA_EXPORT std::ostream& operator<<( std::ostream& theStream, const QString& theText );
HYDRODATA_EXPORT std::ostream& operator<<( std::ostream& theStream, const QColor& theText );
HYDRODATA_EXPORT std::ostream& operator<<( std::ostream& theStream, const TopoDS_Shape& theShape );
HYDRODATA_EXPORT std::ostream& operator<<( std::ostream& theStream, const TopoDS_Face& theFace );
HYDRODATA_EXPORT bool operator == ( const gp_XY& thePoint1, const gp_XY& thePoint2 );
HYDRODATA_EXPORT std::ostream& operator<<( std::ostream& theStream, const gp_XY& theXY );

#endif



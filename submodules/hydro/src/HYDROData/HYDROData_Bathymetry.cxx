// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Bathymetry.h"
#include "HYDROData_Document.h"
#include "HYDROData_Tool.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_QuadtreeNode.hxx"

#include <gp_XY.hxx>
#include <gp_XYZ.hxx>

#include <TDataStd_RealArray.hxx>
#include <TDataStd_AsciiString.hxx>
#include <TDataStd_Integer.hxx>
#include <TDataStd_ExtStringArray.hxx>

#include <QColor>
#include <QFile>
#include <QFileInfo>
#include <QPointF>
#include <QPolygonF>
#include <QStringList>
#include <QString>

#ifndef LIGHT_MODE
#include <vtkPoints.h>
#include <vtkDelaunay2D.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkIdList.h>
#endif

#include <iostream>

#include <math.h>

// #define _TIMER
#ifdef _TIMER
#include <OSD_Timer.hxx>
#endif

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

const int BLOCK_SIZE = 10000;

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Bathymetry, HYDROData_IAltitudeObject)

//int HYDROData_Bathymetry::myQuadTreeNumber = 0;
std::map<int, HYDROData_QuadtreeNode*> HYDROData_Bathymetry::myQuadtrees;

#ifndef LIGHT_MODE
//int HYDROData_Bathymetry::myDelaunayNumber = 0;
std::map<int, vtkPolyData*> HYDROData_Bathymetry::myDelaunay2D;
#endif

inline double sqr( double x )
{
  return x*x;
}

HYDROData_Bathymetry::AltitudePoint::AltitudePoint( double x, double y, double z )
{
  X=x; Y=y; Z=z;
}

double HYDROData_Bathymetry::AltitudePoint::SquareDistance( const HYDROData_Bathymetry::AltitudePoint& p ) const
{
  double d = 0;
  d += sqr( X - p.X );
  d += sqr( Y - p.Y );
  //d += sqr( Z - p.Z );
  return d;
}

HYDROData_Bathymetry::HYDROData_Bathymetry()
: HYDROData_IAltitudeObject()
{
}

HYDROData_Bathymetry::~HYDROData_Bathymetry()
{
}

QStringList HYDROData_Bathymetry::DumpToPython( const QString& thePyScriptPath,
                                                MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aBathymetryName = GetObjPyName();

  aResList << QString( "%1.SetAltitudesInverted( %2 )" )
              .arg( aBathymetryName ).arg( IsAltitudesInverted() );

  TCollection_AsciiString aFilePath = GetFilePath();
  aResList << QString( "if not(%1.ImportFromFile( \"%2\" )):" )
              .arg( aBathymetryName ).arg( aFilePath.ToCString() );
  aResList << QString( "  raise ValueError('problem while loading bathymetry')" );
  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aBathymetryName );
  aResList << QString( "" );

  return aResList;
}

void HYDROData_Bathymetry::SetAltitudePoints( const HYDROData_Bathymetry::AltitudePoints& thePoints )
{
  RemoveAltitudePoints();

  if ( thePoints.empty() )
    return;

  // Save coordinates
  Handle(TDataStd_RealArray) aCoordsArray =
    TDataStd_RealArray::Set( myLab.FindChild( DataTag_AltitudePoints ), 0, thePoints.size() * 3 - 1 );
  aCoordsArray->SetID(TDataStd_RealArray::GetID());

  HYDROData_Bathymetry::AltitudePoints::const_iterator anIter = thePoints.begin(), aLast = thePoints.end();
  for ( int i = 0 ; anIter!=aLast; ++i, ++anIter )
  {
    const HYDROData_Bathymetry::AltitudePoint& aPoint = *anIter;

    aCoordsArray->SetValue( i * 3, aPoint.X );
    aCoordsArray->SetValue( i * 3 + 1, aPoint.Y );
    aCoordsArray->SetValue( i * 3 + 2, aPoint.Z );
  }

  Changed( Geom_Z );
}

HYDROData_Bathymetry::AltitudePoints HYDROData_Bathymetry::GetAltitudePoints(bool IsConvertToGlobal) const
{
  HYDROData_Bathymetry::AltitudePoints aPoints;

  TDF_Label aLabel = myLab.FindChild( DataTag_AltitudePoints, false );
  if ( aLabel.IsNull() )
    return aPoints;

  Handle(TDataStd_RealArray) aCoordsArray;
  if ( !aLabel.FindAttribute( TDataStd_RealArray::GetID(), aCoordsArray ) )
    return aPoints;

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  int q = ( aCoordsArray->Upper() - aCoordsArray->Lower() + 1 ) / 3;
  aPoints.reserve( q );
  for ( int i = aCoordsArray->Lower(), n = aCoordsArray->Upper(); i <= n; )
  {
    if ( i + 3 > n + 1 )
      break;

    HYDROData_Bathymetry::AltitudePoint aPoint;
    aPoint.X = aCoordsArray->Value( i++ );
    aPoint.Y = aCoordsArray->Value( i++ );
    aPoint.Z = aCoordsArray->Value( i++ );

    if( IsConvertToGlobal )
      aDoc->Transform( aPoint.X, aPoint.Y, aPoint.Z, false );
    aPoints.push_back( aPoint );
  }

  return aPoints;
}

HYDROData_QuadtreeNode* HYDROData_Bathymetry::GetQuadtreeNodes() const
{
  TDF_Label aLabel2 = myLab.FindChild(DataTag_Quadtree, false);
  if (aLabel2.IsNull())
    {
      TDF_Label aLabel = myLab.FindChild(DataTag_AltitudePoints, false);
      if (aLabel.IsNull())
        return 0;

      int aQuadTreeNumber = 0;
      Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
      if ( ! aDocument.IsNull() )
        {
          aQuadTreeNumber = aDocument->GetCountQuadtree();
          DEBTRACE("aQuadTreeNumber " << aQuadTreeNumber);
          aQuadTreeNumber++;
          aDocument->SetCountQuadtree(aQuadTreeNumber);
        }
      else
          DEBTRACE("document.IsNull()");
      DEBTRACE("compute Quadtree "<< aQuadTreeNumber);
      HYDROData_QuadtreeNode* aQuadtree = ComputeQuadtreeNodes(aQuadTreeNumber);
      return aQuadtree;
    }
  else
    {
      Handle(TDataStd_Integer) aQuadtreeNum;
      if ( aLabel2.FindAttribute( TDataStd_Integer::GetID(), aQuadtreeNum ) )
        {
          if (myQuadtrees.find(aQuadtreeNum->Get()) != myQuadtrees.end())
            return myQuadtrees[aQuadtreeNum->Get()];
          else
            {
              DEBTRACE("recompute Quadtree "<< aQuadtreeNum->Get());
              HYDROData_QuadtreeNode* aQuadtree = ComputeQuadtreeNodes(aQuadtreeNum->Get());
              return aQuadtree;
            }
        }
      else DEBTRACE("no attribute TDataStd_Integer");
    }
  return 0;
}

HYDROData_QuadtreeNode* HYDROData_Bathymetry::ComputeQuadtreeNodes( int key) const
{
  TDF_Label aLabel = myLab.FindChild(DataTag_AltitudePoints, false);
  if (aLabel.IsNull())
    return 0;

  Handle(TDataStd_RealArray) aCoordsArray;
  if (!aLabel.FindAttribute(TDataStd_RealArray::GetID(), aCoordsArray))
    return 0;

  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( myLab.FindChild( DataTag_Quadtree ), key );
  anAttr->SetID(TDataStd_Integer::GetID());
  DEBTRACE("GetQuadtreeNodes init " << this << " " << key);
  HYDROData_QuadtreeNode* aQuadtree = new HYDROData_QuadtreeNode(0, 30, 5, 0.);

  Nodes_3D* aListOfNodes = new Nodes_3D();

  int index =0;
  for (int i = aCoordsArray->Lower(), n = aCoordsArray->Upper(); i <= n;)
    {
      if (i + 3 > n + 1)
        break;

      double x = aCoordsArray->Value(i++);
      double y = aCoordsArray->Value(i++);
      double z = aCoordsArray->Value(i++);
      gpi_XYZ* aPoint = new gpi_XYZ(x, y, z, index);
      index++;
      aListOfNodes->push_back(aPoint);
    }
  DEBTRACE("  GetQuadtreeNodes call setNodesAndCompute");
  aQuadtree->setNodesAndCompute(aListOfNodes);

  myQuadtrees[key] = aQuadtree;

  return aQuadtree;
}

#ifndef LIGHT_MODE
vtkPolyData* HYDROData_Bathymetry::GetVtkDelaunay2D() const
{
  TDF_Label aLabel2 = myLab.FindChild(DataTag_Delaunay, false);
  if (aLabel2.IsNull())
    {
      TDF_Label aLabel = myLab.FindChild(DataTag_AltitudePoints, false);
      if (aLabel.IsNull())
        return 0;

      int aDelaunayNumber = 0;
      Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
      if ( ! aDocument.IsNull() )
        {
          aDelaunayNumber = aDocument->GetCountDelaunay();
          DEBTRACE("aDelaunayNumber " << aDelaunayNumber);
          aDelaunayNumber++;
          aDocument->SetCountDelaunay(aDelaunayNumber);
        }
      else
          DEBTRACE("document.IsNull()");
      DEBTRACE("compute Delaunay "<< aDelaunayNumber);
      vtkPolyData* data = ComputeVtkDelaunay2D(aDelaunayNumber);
      return data;
    }
  else
    {
      Handle(TDataStd_Integer) aDelaunayNum;
      if ( aLabel2.FindAttribute( TDataStd_Integer::GetID(), aDelaunayNum ) )
        {
          if (myDelaunay2D.find(aDelaunayNum->Get()) != myDelaunay2D.end())
            return myDelaunay2D[aDelaunayNum->Get()];
          else
            {
              DEBTRACE("recompute Delaunay "<< aDelaunayNum->Get());
              vtkPolyData* data = ComputeVtkDelaunay2D(aDelaunayNum->Get());
              return data;
            }
        }
      else DEBTRACE("no attribute TDataStd_Integer");
    }
  return 0;
}

vtkPolyData* HYDROData_Bathymetry::ComputeVtkDelaunay2D(int key) const
{
  TDF_Label aLabel = myLab.FindChild(DataTag_AltitudePoints, false);
  if (aLabel.IsNull())
    return 0;

  Handle(TDataStd_RealArray) aCoordsArray;
  if (!aLabel.FindAttribute(TDataStd_RealArray::GetID(), aCoordsArray))
    return 0;

  HYDROData_Tool::SetTriangulationStatus(HYDROData_Tool::Running);

  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( myLab.FindChild( DataTag_Delaunay ), key );
  anAttr->SetID(TDataStd_Integer::GetID());
  DEBTRACE("GetVtkDelaunay2D init " << this << " " << key);
  vtkPoints *points = vtkPoints::New();
  points->Allocate(aCoordsArray->Upper() +1);
  for (int i = aCoordsArray->Lower(), n = aCoordsArray->Upper(); i <= n;)
    {
      if (i + 3 > n + 1)
        break;
      double x = aCoordsArray->Value(i++);
      double y = aCoordsArray->Value(i++);
      double z = aCoordsArray->Value(i++);
      vtkIdType index = points->InsertNextPoint(x, y, z); // same index than in GetQuadtreeNodes
      //DEBTRACE("  " << index);
    }
  vtkPolyData* profile = vtkPolyData::New();
  profile->SetPoints(points);
  DEBTRACE("Number of Points: "<< points->GetNumberOfPoints());

  vtkDelaunay2D* delaunay2D = vtkDelaunay2D::New();
  delaunay2D->SetInputData(profile);
  delaunay2D->Update();
  vtkPolyData* data = delaunay2D->GetOutput();
  data->BuildLinks();
  myDelaunay2D[key] = data;

  HYDROData_Tool::SetTriangulationStatus(HYDROData_Tool::Finished);

  return data;
}

#endif


void HYDROData_Bathymetry::RemoveAltitudePoints()
{
  TDF_Label aLabel = myLab.FindChild( DataTag_AltitudePoints, false );
  if ( !aLabel.IsNull() )
  {
    aLabel.ForgetAllAttributes();
    Changed( Geom_Z );
  }
}

void interpolateAltitudeForPoints( const gp_XY&                   thePoint,
                                   const HYDROData_Bathymetry::AltitudePoint& theFirstPoint,
                                   const HYDROData_Bathymetry::AltitudePoint& theSecPoint,
                                   HYDROData_Bathymetry::AltitudePoint&       theResPoint,
                                   const bool&                    theIsVertical )
{
  double aCoordX = thePoint.X();
  double aCoordY = thePoint.Y();

  if ( theIsVertical )
  {
    aCoordX = theFirstPoint.X;

    if ( !ValuesEquals( theFirstPoint.X, theSecPoint.X ) )
    {
      // Recalculate X coordinate by equation of line from two points
      aCoordX = ( ( ( thePoint.Y() - theFirstPoint.Y ) * ( theSecPoint.X - theFirstPoint.X ) ) /
                  ( theSecPoint.Y - theFirstPoint.Y ) ) + theFirstPoint.X;
    }
  }
  else
  {
    aCoordY = theFirstPoint.Y;

    if ( !ValuesEquals( theFirstPoint.Y, theSecPoint.Y ) )
    {
      // Recalculate y by equation of line from two points
      aCoordY = ( ( ( thePoint.X() - theFirstPoint.X ) * ( theSecPoint.Y - theFirstPoint.Y ) ) /
                  ( theSecPoint.X - theFirstPoint.X ) ) + theFirstPoint.Y;
    }
  }

  theResPoint.X = aCoordX;
  theResPoint.Y = aCoordY;

  // Calculate coefficient for interpolation
  double aLength = Sqrt( Pow( theSecPoint.Y - theFirstPoint.Y, 2 ) +
                         Pow( theSecPoint.X - theFirstPoint.X, 2 ) );

  double aInterCoeff = 0;
  if ( aLength != 0 )
   aInterCoeff = ( theSecPoint.Z - theFirstPoint.Z ) / aLength;


  double aNewLength = Sqrt( Pow( theResPoint.Y - theFirstPoint.Y, 2 ) +
                            Pow( theResPoint.X - theFirstPoint.X, 2 ) );

  // Calculate interpolated value
  double aResVal = theFirstPoint.Z + aInterCoeff * aNewLength;

  theResPoint.Z = aResVal;
}
#ifndef LIGHT_MODE
bool interpolZtriangle(const gp_XY& point, vtkPolyData* delaunay2D, vtkIdList* triangle, double& z)
{

  int nbPts = triangle->GetNumberOfIds();
  if (nbPts != 3)
    {
      //DEBTRACE("not a triangle ?");
      return false;
    }
  vtkIdType s[3];
  double v[3][3]; // v[i][j] = j coordinate of node i
  for (int i=0; i<3; i++)
    {
      s[i] = triangle->GetId(i);
      delaunay2D->GetPoint(s[i],v[i]);
    }
  //DEBTRACE("triangle node id: " << s[0] << " " << s[1] << " " << s[2]);
  //DEBTRACE("triangle node 0: " << v[0][0]  << " " << v[0][1] << " " << v[0][2]);
  //DEBTRACE("triangle node 1: " << v[1][0]  << " " << v[1][1] << " " << v[1][2]);
  //DEBTRACE("triangle node 2: " << v[2][0]  << " " << v[2][1] << " " << v[2][2]);

  // compute barycentric coordinates (https://en.wikipedia.org/wiki/Barycentric_coordinate_system)
  //     det = (y2-y3)(x1-x3)+(x3-x2)(y1-y3)
  double det = (v[1][1]-v[2][1])*(v[0][0]-v[2][0]) + (v[2][0]-v[1][0])*(v[0][1]-v[2][1]);
  if (det == 0)
    {
      //DEBTRACE("flat triangle ?");
      return false;
    }

  //     l0  = ((y2-y3)(x -x3)+(x3-x2)(y -y3))/det
  double l0  = (v[1][1]-v[2][1])*(point.X()-v[2][0]) + (v[2][0]-v[1][0])*(point.Y()-v[2][1]);
  l0 = l0/det;

  //     l1  = ((y3-y1)(x -x3)+(x1-x3)(y -y3))/det
  double l1  = (v[2][1]-v[0][1])*(point.X()-v[2][0]) + (v[0][0]-v[2][0])*(point.Y()-v[2][1]);
  l1 = l1/det;

  double l2  = 1 -l0 -l1;
  //DEBTRACE("l0, l1, l2: " << l0  << " "  << l1  << " "  << l2);

  if ((l0>=0) && (l0<=1) && (l1>=0) && (l1<=1) && (l2>=0) && (l2<=1))
    {
      z = l0*v[0][2] + l1*v[1][2] + l2*v[2][2];
      return true;
    }
  return false;
}
#endif


NCollection_Sequence<double> HYDROData_Bathymetry::GetAltitudesForPoints( const NCollection_Sequence<gp_XY>& thePoints, int theMethod) const
{
  DEBTRACE("HYDROData_Bathymetry::GetAltitudesForPoints " << GetName().toStdString());
  NCollection_Sequence<double> aResSeq;
  for ( int i = 1, n = thePoints.Length(); i <= n; ++i )
  {
    const gp_XY& thePnt = thePoints.Value(i);
    double anAltitude = GetAltitudeForPoint( thePnt, theMethod );
    aResSeq.Append( anAltitude );
  }
  return aResSeq;
}

double HYDROData_Bathymetry::GetAltitudeForPoint(const gp_XY& thePoint, int theMethod) const
{
  DEBTRACE("GetAltitudeForPoint p(" << thePoint.X() << ", " << thePoint.Y() << "), interpolation method: " << theMethod);
  double anInvalidAltitude = GetInvalidAltitude();
  double aResAltitude = anInvalidAltitude;

  HYDROData_QuadtreeNode* aQuadtree = GetQuadtreeNodes();
  if (!aQuadtree )
    {
      DEBTRACE("  no Quadtree");
      return aResAltitude;
    }

  std::map<double, const gpi_XYZ*> dist2nodes;
  aQuadtree->NodesAround(thePoint, dist2nodes, aQuadtree->getPrecision());
  while (dist2nodes.size() == 0)
    {
      aQuadtree->setPrecision(aQuadtree->getPrecision() *2);
      DEBTRACE("adjust precision to: " << aQuadtree->getPrecision());
      aQuadtree->NodesAround(thePoint, dist2nodes, aQuadtree->getPrecision());
    }
  std::map<double, const gpi_XYZ*>::const_iterator it = dist2nodes.begin();
  aResAltitude = it->second->Z();
  int nodeIndex = it->second->getIndex();
  DEBTRACE("  number of points found: " << dist2nodes.size() << " nearest z: " << aResAltitude << " point index: " << nodeIndex);

  // --- for coarse bathymetry clouds (when the TELEMAC mesh is more refined than the bathymetry cloud)
  //     interpolation is required.
  //     - get a Delaunay2D mesh on the bathymetry cloud,
  //     - get the triangle containing the point in the Delaunay2D mesh,
  //     - interpolate altitude

  bool isBathyInterpolRequired = false;
  if (theMethod)
    isBathyInterpolRequired =true;

#ifndef LIGHT_MODE
  if (isBathyInterpolRequired)
    {
      vtkPolyData* aDelaunay2D = GetVtkDelaunay2D();
      vtkIdList* cells= vtkIdList::New();
      cells->Allocate(64);
      vtkIdList* points= vtkIdList::New();
      points->Allocate(64);
      aDelaunay2D->GetPointCells(nodeIndex, cells);
      vtkIdType nbCells = cells->GetNumberOfIds();
      DEBTRACE("  triangles on nearest point: " << nbCells);
      bool isInside = false;
      for (int i=0; i<nbCells; i++)
        {
          aDelaunay2D->GetCellPoints(cells->GetId(i), points);
          double z = 0;
          isInside = interpolZtriangle(thePoint, aDelaunay2D, points, z);
          if (isInside)
            {
              aResAltitude = z;
              DEBTRACE("  interpolated z: " << z);
              break;
            }
        }
      if (!isInside)
      {
          DEBTRACE("  point outside triangles, nearest z kept");
      }
    }
  #endif
  return aResAltitude;
}

void HYDROData_Bathymetry::SetFilePath( const TCollection_AsciiString& theFilePath )
{
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_FilePath ), theFilePath );
  anAttr->SetID(TDataStd_AsciiString::GetID());
}

void HYDROData_Bathymetry::SetFilePaths( const QStringList& theFilePaths )
{
  int i = 1;
  Handle_TDataStd_ExtStringArray TExtStrArr = TDataStd_ExtStringArray::Set( myLab.FindChild( DataTag_FilePaths ), 1, theFilePaths.size() );
  TExtStrArr->SetID(TDataStd_ExtStringArray::GetID());
  foreach (QString filepath, theFilePaths)
  {
    std::string sstr = filepath.toStdString();
    const char* Val = sstr.c_str();
    TExtStrArr->SetValue(i, TCollection_ExtendedString(Val));
    i++;
  }
}

TCollection_AsciiString HYDROData_Bathymetry::GetFilePath() const
{
  TCollection_AsciiString aRes;

  TDF_Label aLabel = myLab.FindChild( DataTag_FilePath, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
      aRes = anAsciiStr->Get();
  }
  else
  {
    aLabel = myLab.FindChild( DataTag_FilePaths, false );
    if ( !aLabel.IsNull() )
    {
      Handle(TDataStd_ExtStringArray) anExtStrArr;
      if ( aLabel.FindAttribute( TDataStd_ExtStringArray::GetID(), anExtStrArr ) )
        aRes = anExtStrArr->Value(1); //try take the first; convert extstring to asciistring
    }
  }

  return aRes;
}

QStringList HYDROData_Bathymetry::GetFilePaths() const
{
  QStringList aResL;

  TDF_Label aLabel = myLab.FindChild( DataTag_FilePaths, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_ExtStringArray) anExtStrArr;
    if ( aLabel.FindAttribute( TDataStd_ExtStringArray::GetID(), anExtStrArr ) )
    {
      for (int i = anExtStrArr->Lower(); i <= anExtStrArr->Upper(); i++ )
      {
        Standard_ExtString str = anExtStrArr->Value(i).ToExtString();
        TCollection_AsciiString aText (str);
        aResL << QString(aText.ToCString());
      }
    }
  }
  else //backward compatibility
  {
    TDF_Label anOldLabel = myLab.FindChild( DataTag_FilePath, false );
    if ( !anOldLabel.IsNull() )
    {
      Handle(TDataStd_AsciiString) anAsciiStr;
      if ( anOldLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
        aResL << QString(anAsciiStr->Get().ToCString());
    }
  }

  return aResL;
}

void HYDROData_Bathymetry::SetAltitudesInverted( const bool theIsInverted,
                                                 const bool theIsUpdate )
{
  bool anIsAltitudesInverted = IsAltitudesInverted();
  if ( anIsAltitudesInverted == theIsInverted )
    return;

  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( myLab.FindChild( DataTag_AltitudesInverted ), (Standard_Integer)theIsInverted );
  anAttr->SetID(TDataStd_Integer::GetID());
  Changed( Geom_Z );

  if ( !theIsUpdate )
    return;

  // Update altitude points
  HYDROData_Bathymetry::AltitudePoints anAltitudePoints = GetAltitudePoints();
  if ( anAltitudePoints.empty() )
    return;

  HYDROData_Bathymetry::AltitudePoints::iterator anIter = anAltitudePoints.begin(), aLast = anAltitudePoints.end();
  for ( ; anIter!=aLast; ++anIter )
  {
    HYDROData_Bathymetry::AltitudePoint& aPoint = *anIter;
    aPoint.Z *= -1;
  }

  SetAltitudePoints( anAltitudePoints );
}

bool HYDROData_Bathymetry::IsAltitudesInverted() const
{
  bool aRes = false;

  TDF_Label aLabel = myLab.FindChild( DataTag_AltitudesInverted, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_Integer) anIntVal;
    if ( aLabel.FindAttribute( TDataStd_Integer::GetID(), anIntVal ) )
      aRes = (bool)anIntVal->Get();
  }

  return aRes;
}

bool HYDROData_Bathymetry::ImportFromFile( const QString& theFileName )
{
  return ImportFromFiles(QStringList(theFileName));
}

bool HYDROData_Bathymetry::ImportFromFiles( const QStringList& theFileNames )
{
  AltitudePoints AllPoints;
  bool Stat = false;

  foreach (QString theFileName, theFileNames)
  {
    // Try to open the file
    QFile aFile( theFileName );
    if ( !aFile.exists() || !aFile.open( QIODevice::ReadOnly ) )
      continue;

    QString aFileSuf = QFileInfo( aFile ).suffix().toLower();

    HYDROData_Bathymetry::AltitudePoints aPoints;

    // Try to import the file
    if ( aFileSuf == "xyz" )
      Stat = importFromXYZFile( aFile, aPoints );
    else if ( aFileSuf == "asc" )
      Stat = importFromASCFile( aFile, aPoints );

    if (!Stat)
      continue; //ignore this points

    // Close the file
    aFile.close();

    AllPoints.insert(AllPoints.end(), aPoints.begin(), aPoints.end());
  }

  // Convert from global to local CS
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  HYDROData_Bathymetry::AltitudePoints::iterator anIter = AllPoints.begin(), aLast = AllPoints.end();
  for ( ; anIter!=aLast; ++anIter )
  {
    HYDROData_Bathymetry::AltitudePoint& aPoint = *anIter;
    aDoc->Transform( aPoint.X, aPoint.Y, aPoint.Z, true );
  }

  if ( Stat )
  {
    // Update file path and altitude points of this Bathymetry
    SetFilePaths (theFileNames );
    SetAltitudePoints( AllPoints );
  }

  return Stat && !AllPoints.empty();
}

bool HYDROData_Bathymetry::importFromXYZFile( QFile&          theFile,
                                              HYDROData_Bathymetry::AltitudePoints& thePoints ) const
{
  if ( !theFile.isOpen() )
    return false;

  // Strings in file is written as:
  //  1. X(float) Y(float) Z(float)
  //  2. X(float) Y(float) Z(float)
  //  ...

#ifdef _TIMER
  OSD_Timer aTimer;
  aTimer.Start();
#endif

  bool anIsAltitudesInverted = IsAltitudesInverted();
  while ( !theFile.atEnd() )
  {
    std::string aLine = theFile.readLine().simplified().toStdString();
    if ( aLine.empty() )
      continue;

    HYDROData_Bathymetry::AltitudePoint aPoint;
    if( sscanf( aLine.c_str(), "%lf %lf %lf", &aPoint.X, &aPoint.Y, &aPoint.Z )!=3 )
      return false;

    /*QStringList aValues = aLine.split( ' ', QString::SkipEmptyParts );
    if ( aValues.length() < 3 )
      return false;

    QString anX = aValues.value( 0 );
    QString anY = aValues.value( 1 );
    QString aZ  = aValues.value( 2 );

    bool isXOk = false, isYOk = false, isZOk = false;

    aPoint.X = anX.toDouble( &isXOk );
    aPoint.Y = anY.toDouble( &isYOk );
    aPoint.Z = aZ.toDouble( &isZOk );

    if ( !isXOk || !isYOk || !isZOk )
      return false;*/

    if ( HYDROData_Tool::IsNan( aPoint.X ) || HYDROData_Tool::IsInf( aPoint.X ) ||
         HYDROData_Tool::IsNan( aPoint.Y ) || HYDROData_Tool::IsInf( aPoint.Y ) ||
         HYDROData_Tool::IsNan( aPoint.Z ) || HYDROData_Tool::IsInf( aPoint.Z ) )
      return false;

    // Invert the z value if requested
    if ( anIsAltitudesInverted )
      aPoint.Z = -aPoint.Z;

    if( thePoints.size()>=thePoints.capacity() )
      thePoints.reserve( thePoints.size()+BLOCK_SIZE );

    thePoints.push_back( aPoint );
  }

#ifdef _TIMER
  aTimer.Stop();
  std::ofstream stream( "W:/HYDRO/WORK/log.txt", std::ofstream::out );
  aTimer.Show( stream );
#endif

  return true;
}

bool HYDROData_Bathymetry::importFromASCFile( QFile&          theFile,
                                              HYDROData_Bathymetry::AltitudePoints& thePoints ) const
{
  if ( !theFile.isOpen() )
    return false;

  QString aLine;
  QStringList aStrList;

  int aNCols;
  int aNRows;
  double anXllCorner;
  double anYllCorner;
  double aCellSize;
  double aNoDataValue;

  aLine = theFile.readLine().simplified();
  aStrList = aLine.split( ' ', QString::SkipEmptyParts );
  if ( aStrList.length() != 2 && aStrList[0].toLower() != "ncols" )
    return false;
  aNCols = aStrList[1].toInt();

  aLine = theFile.readLine().simplified();
  aStrList = aLine.split( ' ', QString::SkipEmptyParts );
  if ( aStrList.length() != 2 && aStrList[0].toLower() != "nrows" )
    return false;
  aNRows = aStrList[1].toInt();

  aLine = theFile.readLine().simplified();
  aStrList = aLine.split( ' ', QString::SkipEmptyParts );
  if ( aStrList.length() != 2 && aStrList[0].toLower() != "xllcorner" )
    return false;
  anXllCorner = aStrList[1].toDouble();

  aLine = theFile.readLine().simplified();
  aStrList = aLine.split( ' ', QString::SkipEmptyParts );
  if ( aStrList.length() != 2 && aStrList[0].toLower() != "yllcorner" )
    return false;
  anYllCorner = aStrList[1].toDouble();

  aLine = theFile.readLine().simplified();
  aStrList = aLine.split( ' ', QString::SkipEmptyParts );
  if ( aStrList.length() != 2 && aStrList[0].toLower() != "cellsize" )
    return false;
  aCellSize = aStrList[1].toDouble();

  aLine = theFile.readLine().simplified();
  aStrList = aLine.split( ' ', QString::SkipEmptyParts );
  if ( aStrList.length() != 2 && aStrList[0].toLower() != "nodata_value" )
    return false;
  aNoDataValue = aStrList[1].toDouble();

  bool anIsAltitudesInverted = IsAltitudesInverted();

  int i = 0;
  int aStrLength = 0;
  while ( !theFile.atEnd() )
  {
    aLine = theFile.readLine().simplified();
    aStrList = aLine.split( ' ', QString::SkipEmptyParts );

    aStrLength =  aStrList.length();
    if ( aStrLength == 0 )
      continue;

    if ( aStrLength != aNRows )
      return false;

    for (int j = 0; j < aNCols; j++)
    {
      if (aStrList[j].toDouble() != aNoDataValue)
      {
        HYDROData_Bathymetry::AltitudePoint aPoint;
        aPoint.X = anXllCorner + aCellSize*(j + 0.5);
        aPoint.Y = anYllCorner + aCellSize*(aNRows - i + 0.5);
        aPoint.Z = aStrList[j].toDouble();

        if ( anIsAltitudesInverted )
         aPoint.Z = -aPoint.Z;

        if( thePoints.size()>=thePoints.capacity() )
          thePoints.reserve( thePoints.size()+BLOCK_SIZE );
        thePoints.push_back(aPoint);
      }
    }
    i++;
  }

  return true;
}


Handle(HYDROData_PolylineXY) HYDROData_Bathymetry::CreateBoundaryPolyline() const
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  Handle(HYDROData_PolylineXY) aResult =
    Handle(HYDROData_PolylineXY)::DownCast( aDocument->CreateObject( KIND_POLYLINEXY ) );

  if( aResult.IsNull() )
    return aResult;

  //search free name
  QString aPolylinePref = GetName() + "_Boundary";
  QString aPolylineName = HYDROData_Tool::GenerateObjectName( aDocument, aPolylinePref );
  aResult->SetName( aPolylineName );

  double Xmin = 0.0, Xmax = 0.0, Ymin = 0.0, Ymax = 0.0;
  bool isFirst = true;
  HYDROData_Bathymetry::AltitudePoints aPoints = GetAltitudePoints();

  HYDROData_Bathymetry::AltitudePoints::const_iterator anIter = aPoints.begin(), aLast = aPoints.end();
  for ( ; anIter!=aLast; ++anIter )
  {
    const HYDROData_Bathymetry::AltitudePoint& aPoint = *anIter;

    double x = aPoint.X, y = aPoint.Y;
    if( isFirst || x<Xmin )
      Xmin = x;
    if( isFirst || x>Xmax )
      Xmax = x;
    if( isFirst || y<Ymin )
      Ymin = y;
    if( isFirst || y>Ymax )
      Ymax = y;
    isFirst = false;
  }

  aResult->AddSection( "bound", HYDROData_IPolyline::SECTION_POLYLINE, true );
  aResult->AddPoint( 0, HYDROData_IPolyline::Point( Xmin, Ymin ) );
  aResult->AddPoint( 0, HYDROData_IPolyline::Point( Xmin, Ymax ) );
  aResult->AddPoint( 0, HYDROData_IPolyline::Point( Xmax, Ymax ) );
  aResult->AddPoint( 0, HYDROData_IPolyline::Point( Xmax, Ymin ) );

  aResult->SetWireColor( HYDROData_PolylineXY::DefaultWireColor() );

  aResult->Update();

  return aResult;
}

void HYDROData_Bathymetry::UpdateLocalCS( double theDx, double theDy )
{
  gp_XYZ aDelta( theDx, theDy, 0 );
  HYDROData_Bathymetry::AltitudePoints aPoints = GetAltitudePoints();
  HYDROData_Bathymetry::AltitudePoints::iterator anIter = aPoints.begin(), aLast = aPoints.end();
  for ( int i = 0; anIter!=aLast; ++i, ++anIter )
  {
    HYDROData_Bathymetry::AltitudePoint& aPoint = *anIter;
    aPoint.X += aDelta.X();
    aPoint.Y += aDelta.Y();
    aPoint.Z += aDelta.Z();
  }
  SetAltitudePoints( aPoints );
}


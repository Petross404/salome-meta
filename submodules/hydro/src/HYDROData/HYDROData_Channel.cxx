// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Channel.h"

#include "HYDROData_Document.h"
#include "HYDROData_Polyline3D.h"
#include "HYDROData_Profile.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_Projection.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_ShapesTool.h"
#include "HYDROData_Stream.h"
#include "HYDROData_Tool.h"
#include "HYDROData_ChannelAltitude.h"

#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepLib_MakePolygon.hxx>

#include <BRepOffsetAPI_MakePipeShell.hxx>
#include <BRepOffsetAPI_MakePipe.hxx>
#include <BRepCheck_Analyzer.hxx>

#include <BRep_Tool.hxx>

#include <BRepBuilderAPI_Transform.hxx>

#include <BRepLib_MakeEdge.hxx>
#include <BRepLib_MakeWire.hxx>

#include <GeomAPI_ProjectPointOnCurve.hxx>

#include <gp_Ax1.hxx>

#include <TopExp.hxx>
#include <TopExp_Explorer.hxx>

#include <TColgp_HArray1OfPnt.hxx>
#include <TColgp_Array1OfDir.hxx>

#include <TColStd_Array1OfReal.hxx>

#include <TopTools_HArray1OfShape.hxx>
#include <TopTools_SequenceOfShape.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Vertex.hxx>

#include <Quantity_Parameter.hxx>

#define DEB_CHANNEL 1
#ifdef DEB_CHANNEL
#include <BRepTools.hxx>
#endif

#include <QColor>
#include <QStringList>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Channel,HYDROData_ArtificialObject)


HYDROData_Channel::HYDROData_Channel()
: HYDROData_ArtificialObject( Geom_3d )
{
}

HYDROData_Channel::~HYDROData_Channel()
{
}

QStringList HYDROData_Channel::DumpToPython( const QString& thePyScriptPath,
  MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aName = GetObjPyName();

  Handle(HYDROData_Polyline3D) aRefGideLine = GetGuideLine();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefGideLine, "SetGuideLine" );

  bool mode = GetProfileMode();
  QString aMode = mode ? "True" : "False" ;
  aResList << QString( "%1.SetProfileMode( %2 )" ).arg( aName ).arg( aMode );

  if (mode)
  {
    Handle(HYDROData_Profile) aRefProfile = GetProfile();
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefProfile, "SetProfile" );
  }
  else
  {
    QString aLC = QString::number( GetLCValue(), 'f', 3 );
    QString aDeltaZ = QString::number( GetDeltaZValue(), 'f', 3 );
    QString aCoteZ = QString::number( GetCoteZValue(), 'f', 3 );

    aResList << QString( "%1.SetLCValue( %2 )" ).arg( aName ).arg( aLC );
    aResList << QString( "%1.SetDeltaZValue( %2 )" ).arg( aName ).arg( aDeltaZ );
    aResList << QString( "%1.SetCoteZValue( %2 )" ).arg( aName ).arg( aCoteZ );
  }

  aResList << QString( "%1.SetEquiDistance( %2 )" ).arg( aName ).arg( GetEquiDistance() );

  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aName );
  aResList << QString( "" );

  return aResList;

}

HYDROData_SequenceOfObjects HYDROData_Channel::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_ArtificialObject::GetAllReferenceObjects();

  Handle(HYDROData_Polyline3D) aGuideLine = GetGuideLine();
  if ( !aGuideLine.IsNull() )
    aResSeq.Append( aGuideLine );

  Handle(HYDROData_Profile) aProfile = GetProfile();
  if ( !aProfile.IsNull() )
    aResSeq.Append( aProfile );

  return aResSeq;
}

bool HYDROData_Channel::CreatePresentations( const Handle(HYDROData_Polyline3D)& theGuideLine,
                                             const Handle(HYDROData_Profile)&    theProfile,
                                             PrsDefinition&                      thePrs,
                                             double                              theEquiDistance,
                                             bool                                ReverseXCoord)
{
  return internalCreatePresentations( true, theGuideLine, theProfile, TopoDS_Wire(), gp_Pnt(), thePrs, theEquiDistance, ReverseXCoord);
}

bool HYDROData_Channel::CreatePresentations( const Handle(HYDROData_Polyline3D)& theGuideLine,
                                             double                              LC,
                                             double                              deltaZ,
                                             double                              coteZ,
                                             PrsDefinition&                      thePrs,
                                             double                              theEquiDistance,
                                             bool                                ReverseXCoord)
{
  Handle(HYDROData_Profile) theProfile;
  TopoDS_Wire W;
  gp_Pnt MP;

  gp_Pnt2d A1(-0.75*LC, coteZ-LC/2.0), A2(0.75*LC, coteZ -LC/2.0), B1(-LC/2.0, coteZ), B2(LC/2.0, coteZ), C(0, coteZ+deltaZ);
  gp_Pnt A1_3d(A1.X(), 0, A1.Y()), A2_3d(A2.X(), 0, A2.Y()), B1_3d(B1.X(), 0, B1.Y()), B2_3d(B2.X(), 0, B2.Y()), C_3d(C.X(), 0, C.Y());
  BRepLib_MakePolygon PM;
  PM.Add(A1_3d);
  PM.Add(B1_3d);
  PM.Add(C_3d);
  PM.Add(B2_3d);
  PM.Add(A2_3d);
  W = PM.Wire();
  return internalCreatePresentations( false, theGuideLine, theProfile, W, MP, thePrs, theEquiDistance, ReverseXCoord);
}

bool HYDROData_Channel::internalCreatePresentations( bool mode,
                                                     const Handle(HYDROData_Polyline3D)& theGuideLine,
                                                     const Handle(HYDROData_Profile)&    theProfile,
                                                     const TopoDS_Wire&                  theProfWire,
                                                     const gp_Pnt&                       theMiddlePnt,
                                                     PrsDefinition&                      thePrs,
                                                     double                              theEquiDistance,
                                                     bool                                ReverseXCoord)
{
  // Check input parameters
  if ( theGuideLine.IsNull() )
  {
    return false;
  }
  if (mode && theProfile.IsNull() || !mode && theProfWire.IsNull() )
    return false;

  TopoDS_Wire aPathWire = TopoDS::Wire( theGuideLine->GetShape3D() );
  TopoDS_Wire aProfileWire = theProfWire;
  //ignore ReverseXCoord if mode is false
  if (mode)
  {
    if (!ReverseXCoord)
      aProfileWire = TopoDS::Wire( theProfile->GetShape3D(false, false) ); //temp force rebuild
    else
      aProfileWire = TopoDS::Wire( theProfile->GetShape3D(true, true));
  }
  if ( aPathWire.IsNull() || aProfileWire.IsNull() ) {
    return false;
  }

#ifdef DEB_CHANNEL
  std::string brepName = "guideline_";
  brepName += theGuideLine->GetName().toStdString();
  brepName += ".brep";
  BRepTools::Write( aPathWire, brepName.c_str() );
  brepName = "profile_";
  brepName += theGuideLine->GetName().toStdString();
  brepName += ".brep";
  BRepTools::Write( aProfileWire, brepName.c_str() );
#endif

  // Pre-processing
  Handle(HYDROData_PolylineXY) aPolylineXY = theGuideLine->GetPolylineXY();
  if ( aPolylineXY.IsNull() ) {
    return false;
  }

  /*
  HYDROData_IPolyline::SectionType aSectionType = aPolylineXY->GetSectionType( 0 );
  HYDROData_IPolyline::PointsList aPolylinePoints = aPolylineXY->GetPoints( 0 );
  int aNbPoints = aPolylinePoints.Length();
  */

  HYDROData_Polyline3D::Polyline3DPoints aPolylinePoints3D = theGuideLine->GetPoints( theEquiDistance );
  int aNbPoints = aPolylinePoints3D.Length();
  if ( aNbPoints < 2 )
    return false;
  
  // Get tangent in each point of the guide line ( 2D )
  TColgp_Array1OfDir aTangents( 1, aNbPoints );

  HYDROData_IPolyline::SectionType aSectionType = aPolylineXY->GetSectionType( 0 );
  
  if( aSectionType == HYDROData_IPolyline::SECTION_POLYLINE )
  {
    for ( int i = 1; i <= aNbPoints; ++i ) {
      gp_XYZ aPnt = aPolylinePoints3D.Value( i );
      aPnt.SetZ( 0. );
      gp_XYZ aPrevPnt;
      if ( i > 1 ) {
        aPrevPnt = aPolylinePoints3D.Value( i - 1 );
        aPrevPnt.SetZ( 0. );
      }

      gp_Vec aDir;
      if ( i < aNbPoints ) {
        gp_XYZ aNextPnt = aPolylinePoints3D.Value( i + 1 );
        aNextPnt.SetZ( 0. );

        gp_Vec anEdgeVec( aPnt, aNextPnt );

        if ( i == 1 ) {
          aDir = anEdgeVec;
        } else {
          gp_Vec aPrevVec( aPrevPnt, aPnt );
          aDir = aPrevVec.Normalized() + anEdgeVec.Normalized();
        }
      } else {
        aDir = gp_Vec( aPrevPnt, aPnt );
      }
            
      aTangents.SetValue( i, aDir );
    }
  } else {
    // Get curve from the first edge ( 2D )
    TopTools_SequenceOfShape anEdges;
    HYDROData_ShapesTool::ExploreShapeToShapes( aPolylineXY->GetShape(), TopAbs_EDGE, anEdges );
    Standard_Real aStart, anEnd;

    Handle(Geom_Curve) aCurve = BRep_Tool::Curve( TopoDS::Edge( anEdges.First() ), aStart, anEnd );
    GeomAPI_ProjectPointOnCurve aProject;

    // Get tangents
    for ( int i = 1; i <= aNbPoints; ++i ) {
      gp_XYZ aPointToTest = aPolylinePoints3D.Value( i );
      aPointToTest.SetZ( 0. );

      aProject.Init( aPointToTest, aCurve );
      Quantity_Parameter aParam = aProject.LowerDistanceParameter();
      gp_Pnt aPnt;
      gp_Vec aDir;
      aCurve->D1( aParam, aPnt, aDir);

      aTangents.SetValue( i, aDir );
    }
  }

  // Get the profile middle point ( 3D )
  gp_Pnt aMiddlePoint = theMiddlePnt;
  if (mode)
  {
    aMiddlePoint = theProfile->GetMiddlePoint( true );
  }

  // Translate the profile to each point on the guide line ( 3D )
  Handle(TColgp_HArray1OfPnt) anArrayOfFPnt = new TColgp_HArray1OfPnt(1, aNbPoints );
  Handle(TColgp_HArray1OfPnt) anArrayOfLPnt = new TColgp_HArray1OfPnt(1, aNbPoints );  
  Handle(TopTools_HArray1OfShape) anArrOfProfiles = new TopTools_HArray1OfShape( 1, aNbPoints );

  for ( int i = 1; i <= aNbPoints; ++i ) {
    // Get point on the guide line
    gp_Pnt aPointOnGuide( aPolylinePoints3D.Value( i ) );

    // Define translation and rotation:
    gp_Trsf Translation, Rotation;

    // Translation
    Translation.SetTranslation( aMiddlePoint, aPointOnGuide );
    TopoDS_Wire aTransformedProfile = 
      TopoDS::Wire( BRepBuilderAPI_Transform( aProfileWire, Translation, Standard_True ) );

    // Rotation
    gp_Vec aVertical( 0., 0., 1. );
    TopoDS_Vertex aLeftVertex, aRightVertex;
    TopExp::Vertices( aTransformedProfile, aLeftVertex, aRightVertex );
    gp_Pnt aLeftPoint  = BRep_Tool::Pnt( aLeftVertex );
    gp_Pnt aRightPoint = BRep_Tool::Pnt( aRightVertex );
    gp_Vec aLeftToRight( aLeftPoint, aRightPoint);
    gp_Vec NormalToProfile = aVertical ^ aLeftToRight;

    gp_Vec aDir = aTangents.Value( i );
    gp_Vec AxisOfRotation = NormalToProfile ^ aDir;
    if (AxisOfRotation.Magnitude() <= gp::Resolution()) {
      if ( aVertical * aLeftToRight < 0. ) {
        gp_Ax1 theVertical(aPointOnGuide, gp::DZ() );
        Rotation.SetRotation(theVertical, M_PI);
      }
    } else {
      gp_Ax1 theAxis(aPointOnGuide, AxisOfRotation);
      Standard_Real theAngle = NormalToProfile.AngleWithRef(aDir, AxisOfRotation);
      Rotation.SetRotation(theAxis, theAngle);
    }

    aTransformedProfile = TopoDS::Wire(BRepBuilderAPI_Transform( aTransformedProfile, Rotation, Standard_True) );

    // Get the first and the last points of the transformed profile
    TopoDS_Vertex V1, V2;
    TopExp::Vertices( aTransformedProfile, V1, V2 );

    // Fill the data
    anArrayOfFPnt->SetValue( i, BRep_Tool::Pnt( V1 ) );
    anArrayOfLPnt->SetValue( i, BRep_Tool::Pnt( V2 ) );
       
    anArrOfProfiles->SetValue( i, aTransformedProfile );
  }

  // Create presentation
  HYDROData_Stream::PrsDefinition aPrs;
  Handle(TopTools_HArray1OfShape) anArrOf2DProfiles; // we don't need 2D profiles for channel/digue presentation

  HYDROData_Stream::CreatePresentations( anArrayOfFPnt, anArrayOfLPnt, anArrOfProfiles, aPrs );
  thePrs.myInlet =  aPrs.myInlet;       
  thePrs.myOutlet =  aPrs.myOutlet; 
  thePrs.myLeftBank = aPrs.myLeftBank; 
  thePrs.myRightBank = aPrs.myRightBank;
  thePrs.myPrs2D = TopoDS::Face(aPrs.myPrs2D); 
  thePrs.myPrs3D = aPrs.myPrs3D; 

  //thePrs.myPrs2D = TopoDS::Face( aPrs.myPrs2D );
  //BRepBuilderAPI_MakeWire aMakeWire( aPrs.myLeftBank ) ;
  //thePrs.myLeftBank = aMakeWire.Wire();
  //aMakeWire = BRepBuilderAPI_MakeWire( aPrs.myRightBank );
  //thePrs.myRightBank = aMakeWire.Wire();
  //aMakeWire = BRepBuilderAPI_MakeWire( aPrs.myInlet );
  //thePrs.myInlet = aMakeWire.Wire();
  //aMakeWire = BRepBuilderAPI_MakeWire( aPrs.myOutlet );
  //thePrs.myOutlet = aMakeWire.Wire();

  return true;
}

void HYDROData_Channel::Update()
{
  HYDROData_ArtificialObject::Update();

  Handle(HYDROData_Polyline3D) aGuideLine = GetGuideLine();

  PrsDefinition aResultPrs;
  double anEquiDistance = GetEquiDistance();

  bool invDirection = false;
  Handle(HYDROData_IAltitudeObject) anObjAltitude = GetAltitudeObject();
  Handle(HYDROData_ChannelAltitude) aChannelAlt = Handle(HYDROData_ChannelAltitude)::DownCast(anObjAltitude);
  if (!aChannelAlt.IsNull())
    invDirection = aChannelAlt->GetInvertDirection();

  if (GetProfileMode())
  {
    Handle(HYDROData_Profile) aProfile = GetProfile();
    if ( !CreatePresentations( aGuideLine, aProfile, aResultPrs, anEquiDistance, invDirection ) )
      return;
  }
  else
  {
    double lc = GetLCValue();
    double deltaz = GetDeltaZValue();
    double cotez = GetCoteZValue();
    if ( !CreatePresentations( aGuideLine, lc, deltaz, cotez, aResultPrs, anEquiDistance, invDirection ) )
      return;
  }

  SetShape3D( aResultPrs.myPrs3D );
  SetTopShape( aResultPrs.myPrs2D );

  // Create groups for channel
  TopTools_SequenceOfShape aLeftBankEdges;
  HYDROData_ShapesTool::ExploreShapeToShapes( aResultPrs.myLeftBank, TopAbs_EDGE, aLeftBankEdges );

  TopTools_SequenceOfShape aRightBankEdges;
  HYDROData_ShapesTool::ExploreShapeToShapes( aResultPrs.myRightBank, TopAbs_EDGE, aRightBankEdges );

  TopTools_SequenceOfShape anInletEdges;
  HYDROData_ShapesTool::ExploreShapeToShapes( aResultPrs.myInlet, TopAbs_EDGE, anInletEdges );

  TopTools_SequenceOfShape anOutletEdges;
  HYDROData_ShapesTool::ExploreShapeToShapes( aResultPrs.myOutlet, TopAbs_EDGE, anOutletEdges );
    
  RemoveGroupObjects();
  QString aLeftGroupName = GetName() + "_Left_Bank";
  Handle(HYDROData_ShapesGroup) aLeftGroup = createGroupObject();
  aLeftGroup->SetName( aLeftGroupName );
  aLeftGroup->SetShapes( aLeftBankEdges );

  QString aRightGroupName = GetName() + "_Right_Bank";

  Handle(HYDROData_ShapesGroup) aRightGroup = createGroupObject();
  aRightGroup->SetName( aRightGroupName );
  aRightGroup->SetShapes( aRightBankEdges );

  QString anInGroupName = GetName() + "_Inlet";

  Handle(HYDROData_ShapesGroup) anInGroup = createGroupObject();
  anInGroup->SetName( anInGroupName );
  anInGroup->SetShapes( anInletEdges );

  QString anOutGroupName = GetName() + "_Outlet";

  Handle(HYDROData_ShapesGroup) anOutGroup = createGroupObject();
  anOutGroup->SetName( anOutGroupName );
  anOutGroup->SetShapes( anOutletEdges );
}

bool HYDROData_Channel::IsHas2dPrs() const
{
  return true;
}

QColor HYDROData_Channel::DefaultFillingColor() const
{
  return QColor( Qt::blue );
}

QColor HYDROData_Channel::DefaultBorderColor() const
{
  return QColor( Qt::transparent );
}

bool HYDROData_Channel::SetGuideLine( const Handle(HYDROData_Polyline3D)& theGuideLine )
{
  Handle(HYDROData_Polyline3D) aPrevGuideLine = GetGuideLine();

  if ( theGuideLine.IsNull() )
  {
    RemoveGuideLine();
    return !aPrevGuideLine.IsNull();
  }

  if ( IsEqual( aPrevGuideLine, theGuideLine ) )
    return false;

  TopoDS_Wire aHydraulicWire = TopoDS::Wire( theGuideLine->GetTopShape() );
  if ( aHydraulicWire.IsNull() )
    return false; // The polyline must be a single wire

  SetReferenceObject( theGuideLine, DataTag_GuideLine );

  // Indicate model of the need to update the chanel presentation
  Changed( Geom_3d );

  return true;
}

Handle(HYDROData_Polyline3D) HYDROData_Channel::GetGuideLine() const
{
  return Handle(HYDROData_Polyline3D)::DownCast( 
           GetReferenceObject( DataTag_GuideLine ) );
}

void HYDROData_Channel::RemoveGuideLine()
{
  Handle(HYDROData_Polyline3D) aPrevGuideLine = GetGuideLine();
  if ( aPrevGuideLine.IsNull() )
    return;

  ClearReferenceObjects( DataTag_GuideLine );

  // Indicate model of the need to update the chanel presentation
  Changed( Geom_3d );
}

bool HYDROData_Channel::SetProfile( const Handle(HYDROData_Profile)& theProfile )
{
  Handle(HYDROData_Profile) aPrevProfile = GetProfile();

  if ( theProfile.IsNull() )
  {
    RemoveProfile();
    return !aPrevProfile.IsNull();
  }

  if ( IsEqual( aPrevProfile, theProfile ) )
    return false;

  SetReferenceObject( theProfile, DataTag_Profile );

  // Indicate model of the need to update the chanel presentation
  Changed( Geom_3d );

  return true;
}

Handle(HYDROData_Profile) HYDROData_Channel::GetProfile() const
{
  return Handle(HYDROData_Profile)::DownCast( 
           GetReferenceObject( DataTag_Profile ) );
}

void HYDROData_Channel::RemoveProfile()
{
  Handle(HYDROData_Profile) aPrevProfile = GetProfile();
  if ( aPrevProfile.IsNull() )
    return;

  ClearReferenceObjects( DataTag_Profile );

  // Indicate model of the need to update the chanel presentation
  Changed( Geom_3d );
}

ObjectKind HYDROData_Channel::getAltitudeObjectType() const
{
  //DEBTRACE("HYDROData_Channel::getAltitudeObjectType");
  return KIND_CHANNEL_ALTITUDE;
  //return KIND_STREAM_ALTITUDE;
}

TopoDS_Shape HYDROData_Channel::GetLeftShape() const
{
  HYDROData_SequenceOfObjects aGroups = GetGroups();
  return HYDROData_Tool::getFirstShapeFromGroup( aGroups, 1);
}

TopoDS_Shape HYDROData_Channel::GetRightShape() const
{
  HYDROData_SequenceOfObjects aGroups = GetGroups();
  return HYDROData_Tool::getFirstShapeFromGroup( aGroups, 2);
}

void HYDROData_Channel::SetEquiDistance( double theEquiDistance )
{
  double anEquiDistance = theEquiDistance > 0 ? theEquiDistance : 1E-3;
  SetDouble( DataTag_EquiDistance, theEquiDistance );
}

double HYDROData_Channel::GetEquiDistance() const
{
  return GetDouble( DataTag_EquiDistance, 1.0 );
}

void HYDROData_Channel::SetLCValue( double val )
{
  SetDouble( DataTag_LC, val );
}

double HYDROData_Channel::GetLCValue() const
{
  return GetDouble( DataTag_LC, 1.0 );
}

void HYDROData_Channel::SetDeltaZValue( double val )
{
  SetDouble( DataTag_DeltaZ, val );
}

double HYDROData_Channel::GetDeltaZValue() const
{
  return GetDouble( DataTag_DeltaZ, 1.0 );
}

void HYDROData_Channel::SetCoteZValue( double val )
{
  SetDouble( DataTag_CoteZ, val );
}

double HYDROData_Channel::GetCoteZValue() const
{
  return GetDouble( DataTag_CoteZ, 1.0 );
}

void HYDROData_Channel::SetProfileMode( bool mode )
{
  SetInteger( DataTag_ProfileMode, (bool)mode );
}

bool HYDROData_Channel::GetProfileMode() const
{
  return (bool)GetInteger( DataTag_ProfileMode, 1 );
}

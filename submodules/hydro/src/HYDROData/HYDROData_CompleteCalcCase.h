// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_CompleteCalcCase_HeaderFile
#define HYDROData_CompleteCalcCase_HeaderFile


/**\class HYDROData_CompleteCalcCase
 *
 * \brief Allows to add new objects (natural/artificial objects, polylines) to already existing calc.case without full recomputation 
 *
*/

#include <NCollection_Sequence.hxx>
#include <NCollection_Map.hxx>
#include <HYDROData_CalculationCase.h>
#include <HYDROData_Document.h>

class HYDRODATA_EXPORT HYDROData_CompleteCalcCase
{
public:

  static bool AddObjects( const Handle(HYDROData_Document)& doc,
                          Handle(HYDROData_CalculationCase)& theCalcCase, 
                          NCollection_Sequence<Handle(HYDROData_Entity)> theNewObjects,
                          bool IsUseOrigNamingOfNewRegions,
                          bool& IsIntersectionOfNewObj,
                          NCollection_Sequence<Handle(HYDROData_Region)>& theNewRegions);

};

#endif

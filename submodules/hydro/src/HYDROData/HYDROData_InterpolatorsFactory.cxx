// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_InterpolatorsFactory.h"

#include "HYDROData_LinearInterpolator.h"

HYDROData_InterpolatorsFactory::HYDROData_InterpolatorsFactory()
{
  Register<HYDROData_LinearInterpolator>( "Linear" );
}

HYDROData_InterpolatorsFactory::~HYDROData_InterpolatorsFactory()
{
}

HYDROData_IProfilesInterpolator* HYDROData_InterpolatorsFactory::GetInterpolator( const TCollection_AsciiString& theName ) const
{
  HYDROData_IProfilesInterpolator* anInterpolator = NULL;

  FactoryInterpolators::const_iterator anIt = myInterpolators.find(theName.ToCString());
  if ( anIt != myInterpolators.end() ) {
    anInterpolator = anIt->second;
  }

  return anInterpolator;
}

NCollection_Sequence<TCollection_AsciiString> HYDROData_InterpolatorsFactory::GetInterpolatorNames() const
{
  NCollection_Sequence<TCollection_AsciiString> aNames;

  FactoryInterpolators::const_iterator anIt = myInterpolators.begin();
  for ( ; anIt != myInterpolators.end(); anIt++ ) {
    aNames.Append( anIt->first.c_str() );
  }

  return aNames;
}
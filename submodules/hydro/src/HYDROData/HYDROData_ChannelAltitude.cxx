// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_ChannelAltitude.h"

#include "HYDROData_Document.h"
#include "HYDROData_Object.h"
#include "HYDROData_Channel.h"
#include "HYDROData_Projection.h"
#include "HYDROData_Polyline3D.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_ProfileUZ.h"
#include "HYDROData_Profile.h"
#include <TDataStd_Integer.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"
#include <QString>

#include <BRepBuilderAPI_MakeVertex.hxx>
#include <BRepExtrema_DistShapeShape.hxx>

#include <Precision.hxx>
#include <TopAbs.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Wire.hxx>
#include <TopExp_Explorer.hxx>
#include <TopAbs_ShapeEnum.hxx>
#include <BRep_Tool.hxx>
#include <BRepTools.hxx>
#include <Geom_Curve.hxx>

#include <gp_Trsf.hxx>
#include <gp_Pnt.hxx>
#include <gp_Vec.hxx>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_ChannelAltitude, HYDROData_IAltitudeObject)

HYDROData_ChannelAltitude::HYDROData_ChannelAltitude()
: HYDROData_IAltitudeObject()
{
}

HYDROData_ChannelAltitude::~HYDROData_ChannelAltitude()
{
}

double HYDROData_ChannelAltitude::GetAltitudeForPoint( const gp_XY& thePoint,
                                                       int theMethod) const
{
  DEBTRACE("GetAltitudeForPoint p(" << thePoint.X() << ", " << thePoint.Y() << ")");
  double aResAltitude = GetInvalidAltitude();

  Handle(HYDROData_Channel) aChannel =
  Handle(HYDROData_Channel)::DownCast(GetFatherObject());
  if (aChannel.IsNull())
    {
      DEBTRACE("aChannel.IsNull()");
      return aResAltitude;
    }
  DEBTRACE("aChannel: " << aChannel->GetName().toStdString());

  Handle(HYDROData_Polyline3D) aGuideLine = aChannel->GetGuideLine();
  if (aGuideLine.IsNull())
    {
      DEBTRACE("aGuideLine.IsNull()");
      return aResAltitude;
    }
  //DEBTRACE("aGuideLine: " << aGuideLine->GetName().toStdString());

  Handle(HYDROData_PolylineXY) aGuideXY = aGuideLine->GetPolylineXY();
  if (aGuideXY.IsNull())
    {
      DEBTRACE("aGuideXY.IsNull()");
      return aResAltitude;
    }
  //DEBTRACE("aGuideXY: " << aGuideXY->GetName().toStdString());

  Handle(HYDROData_ProfileUZ) aGuideUZ = aGuideLine->GetProfileUZ();
  if (aGuideUZ.IsNull())
    {
      aGuideUZ = aGuideLine->GetChildProfileUZ(); // profile obtained from bathymetry
    }
  if (aGuideUZ.IsNull())
    {
      DEBTRACE("aGuideUZ.IsNull()");
      return aResAltitude;
    }
  //DEBTRACE("aGuideUZ: " << aGuideUZ->GetName().toStdString());

  Handle (HYDROData_Profile) aProfile = aChannel->GetProfile();
  if (aProfile.IsNull())
    {
      return aResAltitude;
    }
  //DEBTRACE("aProfile: " << aProfile->GetName().toStdString());

  // --- See GEOMImpl_ProjectionDriver.cxx

  gp_XY LP, RP;
  aProfile->GetLeftPoint(LP);
  aProfile->GetRightPoint(RP);

  TopoDS_Shape aShape =  aGuideXY->GetShape();
  gp_Pnt P1(thePoint.X(), thePoint.Y(), 0);
  TopoDS_Shape aPoint = BRepBuilderAPI_MakeVertex(P1).Shape();

  if (aPoint.IsNull() || aShape.IsNull())
    {
      DEBTRACE("aPoint.IsNull() || aShape.IsNull()");
      return aResAltitude;
    }

  if (aShape.ShapeType() != TopAbs_EDGE && aShape.ShapeType() != TopAbs_WIRE)
    {
      DEBTRACE("Projection aborted : the shape is neither an edge nor a wire");
      return aResAltitude;
    }

  // Perform projection.
  BRepExtrema_DistShapeShape aDistShSh(aPoint, aShape, Extrema_ExtFlag_MIN);

  if (aDistShSh.IsDone() == Standard_False)
    {
      DEBTRACE("Projection not done");
      return aResAltitude;
    }

  Standard_Boolean hasValidSolution = Standard_False;
  Standard_Integer aNbSolutions = aDistShSh.NbSolution();
  Standard_Integer i;
  double aParam = 0.;
  Standard_Real aTolConf = BRep_Tool::Tolerance(TopoDS::Vertex(aPoint));
  Standard_Real aTolAng = 1.e-4;

  for (i = 1; i <= aNbSolutions; i++)
    {
      Standard_Boolean isValid = Standard_False;
      BRepExtrema_SupportType aSupportType = aDistShSh.SupportTypeShape2(i);
      TopoDS_Shape aSupportShape = aDistShSh.SupportOnShape2(i);

      if (aSupportType == BRepExtrema_IsOnEdge)
        {
          // Minimal distance inside edge is really a projection.
          isValid = Standard_True;
          aDistShSh.ParOnEdgeS2(i, aParam);
        }
      else if (aSupportType == BRepExtrema_IsVertex)
        {
          TopExp_Explorer anExp(aShape, TopAbs_EDGE);

          if (aDistShSh.Value() <= aTolConf)
            {
              // The point lies on the shape. This means this point
              // is really a projection.
              for (; anExp.More() && !isValid; anExp.Next())
                {
                  TopoDS_Edge aCurEdge = TopoDS::Edge(anExp.Current());

                  if (aCurEdge.IsNull() == Standard_False)
                    {
                      TopoDS_Vertex aVtx[2];

                      TopExp::Vertices(aCurEdge, aVtx[0], aVtx[1]);

                      for (int j = 0; j < 2; j++)
                        {
                          if (aSupportShape.IsSame(aVtx[j]))
                            {
                              // The current edge is a projection edge.
                              isValid = Standard_True;
                              aSupportShape = aCurEdge;
                              aParam = BRep_Tool::Parameter(aVtx[j], aCurEdge);
                              break;
                            }
                        }
                    }
                }
            }
          else
            {
              // Minimal distance to vertex is not always a real projection.
              gp_Pnt aPnt = BRep_Tool::Pnt(TopoDS::Vertex(aPoint));
              gp_Pnt aPrjPnt = BRep_Tool::Pnt(TopoDS::Vertex(aSupportShape));
              gp_Vec aDProjP(aPrjPnt, aPnt);

              for (; anExp.More() && !isValid; anExp.Next())
                {
                  TopoDS_Edge aCurEdge = TopoDS::Edge(anExp.Current());

                  if (aCurEdge.IsNull() == Standard_False)
                    {
                      TopoDS_Vertex aVtx[2];

                      TopExp::Vertices(aCurEdge, aVtx[0], aVtx[1]);

                      for (int j = 0; j < 2; j++)
                        {
                          if (aSupportShape.IsSame(aVtx[j]))
                            {
                              // Check if the point is a projection to the current edge.
                              Standard_Real anEdgePars[2];
                              Handle(Geom_Curve) aCurve = BRep_Tool::Curve(aCurEdge, anEdgePars[0], anEdgePars[1]);
                              gp_Pnt aVal;
                              gp_Vec aD1;

                              aParam = BRep_Tool::Parameter(aVtx[j], aCurEdge);
                              aCurve->D1(aParam, aVal, aD1);

                              if (Abs(aD1.Dot(aDProjP)) <= aTolAng)
                                {
                                  // The current edge is a projection edge.
                                  isValid = Standard_True;
                                  aSupportShape = aCurEdge;
                                  break;
                                }
                            }
                        }
                    }
                }
            }
        }

      if (isValid)
        {
          if (hasValidSolution)
            {
              DEBTRACE("Projection aborted : multiple solutions");
              return aResAltitude;
            }

          // Store the valid solution.
          hasValidSolution = Standard_True;

          // profile altitude at projection point
          HYDROData_IPolyline::PointsList aProfilePoints = aGuideUZ->GetPoints();
          if ( aProfilePoints.IsEmpty() )
            {
              DEBTRACE("empty profile UZ");
              return aResAltitude;
            }
          double aDepth = HYDROData_ProfileUZ::GetDepthFromDistance( aProfilePoints, aParam );
          DEBTRACE("profile altitude: " << aDepth);

          // Compute edge index.
          TopExp_Explorer anExp(aShape, TopAbs_EDGE);
          int anIndex = 0;
          for (; anExp.More(); anExp.Next(), anIndex++)
            {
              if (aSupportShape.IsSame(anExp.Current()))
                {
                  break;
                }
            }

          // get the XY distance from point to guideline
          const gp_Pnt &aPntProj = aDistShSh.PointOnShape2(i);
          DEBTRACE("projection: (" << aPntProj.X() << ", " << aPntProj.Y() << ", " << aPntProj.Z() << ")");
          gp_XY aProjXY = gp_XY(aPntProj.X(), aPntProj.Y());
          aProjXY.Subtract(thePoint);
          double distance = aProjXY.Modulus();

          gp_Vec2d aProjDir = aProjXY; 
          gp_Vec2d aProfileDir(LP, RP);
          double aSign = 1.0;
          if( aProfileDir.Dot(aProjDir) < 0 )
            aSign = -1.0;

          distance *= aSign;
          DEBTRACE("distance to guideline " << distance);

          if (GetInvertDirection())
            distance*=-1;

          // get delta altitude on section (supposed symmetric) from guideline distance (aParam)
          double delta = 0;
          int i1 = 0;
          int i2 = 0;
          gp_XY pt1 = gp_XY();
          gp_XY pt2 = gp_XY();
          HYDROData_ProfileUZ::PointsList aSectionPoints = aProfile->GetParametricPoints();
          for ( int i = 1, aNbPoints = aSectionPoints.Size(); i <= aNbPoints; ++i )
            {
              const HYDROData_IPolyline::Point& aPolylinePoint = aSectionPoints.Value( i );
              DEBTRACE(" i, size, profile point: " << i <<  " " << aSectionPoints.Size() << " " << aPolylinePoint.X() << " " << aPolylinePoint.Y());
              if (aPolylinePoint.X() < distance)
                {
                  i1 = i;
                  pt1 = aPolylinePoint;
                }
              if (aPolylinePoint.X() >= distance)
                {
                  i2 = i;
                  pt2 = aPolylinePoint;
                  break;
                }
            }
          if ((i1 == 0) && (i2 == 0))
            {
              DEBTRACE("Projection aborted : non centered profile");
              return aResAltitude;
            }
          else if (i1 == 0)  // distance <  - profile width
            {
              DEBTRACE("distance < - profile width");
              delta = pt2.Y();
            }
          else if (i1 == aSectionPoints.Size()) // distance >= profile width
            {
              DEBTRACE("distance >= profile width");
              delta = pt1.Y();
            }
          else
            {
              delta = pt1.Y() + (pt2.Y() - pt1.Y())*(distance -pt1.X())/(pt2.X()-pt1.X());
            }
          aResAltitude = delta + aDepth;
          DEBTRACE("distance XY: "<< aParam << " distance to guideline: " << distance << " final altitude: " << aResAltitude << " delta: " << delta);
          return aResAltitude;
        }
    }

  if (!hasValidSolution)
    {
      DEBTRACE("Projection aborted : no projection");
      return aResAltitude;
    }

  return aResAltitude;
}


void HYDROData_ChannelAltitude::SetInvertDirection(bool IsInverted)
{
  if ( GetInvertDirection() == IsInverted )
    return;

  TDataStd_Integer::Set( myLab.FindChild( DataTag_InvertDirection ), (Standard_Integer)IsInverted );
}

bool HYDROData_ChannelAltitude::GetInvertDirection() const
{
  bool aRes = false;

  TDF_Label aLabel = myLab.FindChild( DataTag_InvertDirection, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_Integer) anIntVal;
    if ( aLabel.FindAttribute( TDataStd_Integer::GetID(), anIntVal ) )
      aRes = (bool)anIntVal->Get();
  }

  return aRes;
}



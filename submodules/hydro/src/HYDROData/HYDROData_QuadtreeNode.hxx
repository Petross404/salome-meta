// Copyright (C) 2007-2014  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  HYDROData HYDROData_QuadtreeNode : Quadtree with Nodes set (from SMESH module)
//  inherits global class HYDROData_Quadtree
//  File      : HYDROData_QuadtreeNode.hxx
//  Created   : Tue Jan 16 16:00:00 2007
//  Author    : Nicolas Geimer & Aurelien Motteux  (OCC)
//  Module    : HYDROData
//
#ifndef _HYDROData_QUADTREENODE_HXX_
#define _HYDROData_QUADTREENODE_HXX_

#include <Standard.hxx>
#include <Standard_Macro.hxx>

#include "HYDROData_Quadtree.hxx"
#include <gp_Pnt.hxx>
#include <gp_XYZ.hxx>

#include <list>
#include <set>
#include <vector>
#include <map>

//forward declaration
class HYDROData_QuadtreeNode;
class gp_XY;

class Standard_EXPORT gpi_XYZ: public gp_XYZ
{
public:
  gpi_XYZ();

  gpi_XYZ(const Standard_Real X, const Standard_Real Y, const Standard_Real Z, int i): gp_XYZ(X, Y, Z), myIndex(i) {}

  int getIndex() const {return myIndex; }
protected:
  int myIndex;
};

typedef std::list<gpi_XYZ*> Nodes_3D;

class Standard_EXPORT HYDROData_QuadtreeNode: public HYDROData_Quadtree
{

public:

  //! public constructor: when theNodes is non null, compute the quadtree
  HYDROData_QuadtreeNode(Nodes_3D* theNodes,
                         const int maxLevel = 8,
                         const int maxNbNodes = 5,
                         const double minBoxSize = 0.);

  //! destructor
  virtual ~HYDROData_QuadtreeNode();

  //! tu use when no nodes where given to the public constructor
  virtual void setNodesAndCompute(Nodes_3D* theNodes);

  //! Tells us if Node is inside the current box with the precision "precision"
  virtual const bool isInside(const gp_XY& p, const double precision = 0.);

  //! Return in Result a list of Nodes potentials to be near Node
  void NodesAround(const gp_XY& Node,
                   std::list<const gpi_XYZ*>* Result,
                   const double precision = 0.);

  //! Return in dist2Nodes nodes mapped to their square distance from Node
  bool NodesAround(const gp_XY& node,
                   std::map<double, const gpi_XYZ*>& dist2Nodes,
                   double precision);

  //! Update data according to node movement
  void UpdateByMoveNode(const gpi_XYZ* node, const gp_Pnt& toPnt);

  //! Return nb nodes in a tree
  int NbNodes() const
  {
    if (myNodes)
      return myNodes->size();
    else
      return 0;
  }

  //! tells us if the tree as already bin computed
  bool isEmpty() const { return myChildren == 0; }

  inline double getPrecision() {return myPrecision; }
  inline void setPrecision(double precision)  {myPrecision = precision; }

protected:

  struct Limit: public HYDROData_TreeLimit
  {
    int myMaxNbNodes;
    Limit(int maxLevel, double minSize, int maxNbNodes) :
        HYDROData_TreeLimit(maxLevel, minSize), myMaxNbNodes(maxNbNodes) {}
  };

  int getMaxNbNodes() const;

  HYDROData_QuadtreeNode();

  //! Compute the bounding box of the whole set of nodes myNodes
  virtual Bnd_B2d* buildRootBox();

  //! Shares the father's data with each of his child
  virtual void buildChildrenData();

  //! Construct an empty HYDROData_QuadtreeNode used by HYDROData_Quadtree::buildChildren()
  virtual HYDROData_Quadtree* newChild() const;

  //! The set of nodes inside the box of the Quadtree (Empty if Quadtree is not a leaf)
  Nodes_3D* myNodes;

  //! distance to look for nearest nodes
  double myPrecision;
};

#endif

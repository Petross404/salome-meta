// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_StricklerTable.h>
#include <HYDROData_Tool.h>
#include <HYDROData_Iterator.h>

#include <TDataStd_NamedData.hxx>

#include <TDataStd_DataMapOfStringReal.hxx>
#include <TColStd_DataMapOfStringInteger.hxx>
#include <TDataStd_DataMapOfStringString.hxx>
#include <TDataStd_DataMapIteratorOfDataMapOfStringReal.hxx>
#include <TDataStd_DataMapIteratorOfDataMapOfStringString.hxx>
#include <TDataStd_AsciiString.hxx>

#include <TCollection_AsciiString.hxx>
#include <TCollection_ExtendedString.hxx>

#include <QString>
#include <QColor>
#include <QStringList>
#include <QFile>
#include <QTextStream>

IMPLEMENT_STANDARD_RTTIEXT( HYDROData_StricklerTable, HYDROData_Entity )

HYDROData_StricklerTable::HYDROData_StricklerTable()
: HYDROData_Entity( Geom_No )
{
}

HYDROData_StricklerTable::~HYDROData_StricklerTable()
{
}

const ObjectKind HYDROData_StricklerTable::GetKind() const
{
  return KIND_STRICKLER_TABLE;
}

bool HYDROData_StricklerTable::Import( const QString& theFileName )
{
  Handle(TDataStd_NamedData) aMap = Map();
  if( aMap.IsNull() )
    return false;

  QFile aFile( theFileName );
  if( !aFile.open( QFile::ReadOnly | QFile::Text ) )
    return false;

  aMap->ChangeReals( TDataStd_DataMapOfStringReal() );
  aMap->ChangeStrings( TDataStd_DataMapOfStringString() );
  aMap->ChangeIntegers( TColStd_DataMapOfStringInteger() );

  QTextStream aStream( &aFile );
  int i=0;
  while( !aStream.atEnd() )
  {
    QString aLine = aStream.readLine();
    if( i==0 )
    {
      SetAttrName( aLine );
    }
    else
    {
      int p1 = aLine.indexOf( '"' );
      int p2 = aLine.indexOf( '"', p1+1 );
      QString aType = aLine.mid( p1+1, p2-p1-1 );
      QStringList aParts = aLine.mid( p2+1 ).split( ' ', QString::SkipEmptyParts );
      double aCoeff = aParts[0].toDouble();
      int aColorInt = aParts[1].toInt( 0, 16 );
      QString anAttrValue = aParts.size()>2 ? aParts[2] : QString();
      
      TCollection_ExtendedString aTypeExt = HYDROData_Tool::toExtString( aType );
      aMap->SetReal( aTypeExt, aCoeff );
      aMap->SetInteger( aTypeExt, aColorInt );
      aMap->SetString( aTypeExt, HYDROData_Tool::toExtString( anAttrValue ) );
    }
    i++;
  }
  aFile.close();

  return true;
}

bool HYDROData_StricklerTable::Export( const QString& theFileName )
{
  Handle(TDataStd_NamedData) aMap = Map();
  if( aMap.IsNull() )
    return false;

  QFile aFile( theFileName );
  if( !aFile.open( QFile::WriteOnly | QFile::Text ) )
    return false;

  QTextStream aStream( &aFile );
  aStream.setCodec( "UTF-8" );
  aStream.setGenerateByteOrderMark( true );

  aStream << GetAttrName() << "\n";

  bool aRes = true;
  QStringList aTypes = GetTypes();
  foreach( QString aType, aTypes )
  {
    TCollection_ExtendedString aTypeExt = HYDROData_Tool::toExtString( aType );

    aStream << "\"" << aType << "\" " << Get( aType, 0.0 );

    QString aColor = QString::number( aMap->GetInteger( aTypeExt ), 16 ).toUpper();
    aColor = QString( 6-aColor.length(), '0' ) + aColor;
    QString anAttrValue = HYDROData_Tool::toQString( aMap->GetString( aTypeExt ) );

    aStream << " " << aColor;
    if( !anAttrValue.isEmpty() )
      aStream << " " << anAttrValue;
    aStream << "\n";
  }

  aFile.close();
  return aRes;
}

double HYDROData_StricklerTable::Get( const QString& theType, double theDefault ) const
{
  Handle( TDataStd_NamedData ) aMap = Map();
  TCollection_ExtendedString aType = HYDROData_Tool::toExtString( theType );
  if( aMap->HasReal( aType ) )
    return aMap->GetReal( aType );
  else
    return theDefault;
}

void HYDROData_StricklerTable::Set( const QString& theType, double theCoefficient )
{
  Handle(TDataStd_NamedData) aMap = Map();
  aMap->SetReal( HYDROData_Tool::toExtString( theType ), theCoefficient );
}

void HYDROData_StricklerTable::GetCoefficientRange( double& theMin, double& theMax ) const
{
  theMin = 0;
  theMax = 0;
  
  Handle(TDataStd_NamedData) aMap = Map();
  Standard_Boolean isFirst = Standard_True;
  for ( TDataStd_DataMapIteratorOfDataMapOfStringReal it( aMap->GetRealsContainer() ); it.More(); it.Next() )
  {
    Standard_Real aValue = it.Value();
    if ( theMin == 0 || aValue < theMin )
    {
      theMin = aValue;
    }
    if ( theMax == 0 || aValue > theMax )
    {
      theMax = aValue;
    }
  }
}

QStringList HYDROData_StricklerTable::GetTypes() const
{
  QStringList aSeq;
  Handle(TDataStd_NamedData) aMap = Map();
  if( !aMap.IsNull() )
  {
    for( TDataStd_DataMapIteratorOfDataMapOfStringReal it( aMap->GetRealsContainer() ); it.More(); it.Next() )
       aSeq.append( HYDROData_Tool::toQString( it.Key() ) );
  }
  aSeq.sort();
  return aSeq;
}

bool HYDROData_StricklerTable::HasType( const QString& theType ) const
{
  Handle(TDataStd_NamedData) aMap = Map();
  
  TCollection_ExtendedString aType = HYDROData_Tool::toExtString( theType );
  return !aMap.IsNull() && aMap->HasReal( aType );
}

void HYDROData_StricklerTable::Clear()
{
  Handle(TDataStd_NamedData) aMap = Map();
  if( !aMap.IsNull() )
    aMap->ChangeReals( TDataStd_DataMapOfStringReal() );
}

QStringList HYDROData_StricklerTable::DumpToPython( const QString& thePyScriptPath,
                                                    MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString aPyName = GetObjPyName();

  QString anAttrName = GetAttrName();
  aResList << QString( "%1.SetAttrName( \"%2\" )" ).arg( aPyName ).arg( anAttrName );

  aResList << QString( "" );
  Handle(TDataStd_NamedData) aMap = Map();
  if( !aMap.IsNull() )
  {
    for( TDataStd_DataMapIteratorOfDataMapOfStringReal it( aMap->GetRealsContainer() ); it.More(); it.Next() )
    {
      QString aType = HYDROData_Tool::toQString( it.Key() );
      Standard_Real aValue = it.Value();
      aResList << QString( "%1.Set( u\"%2\", %3 )" ).arg( aPyName ).arg( aType ).arg( aValue );

      QString anAttrValue = GetAttrValue( aType );
      aResList << QString( "%1.SetAttrValue( u\"%2\", \"%3\" )" ).arg( aPyName ).arg( aType ).arg( anAttrValue );
      
      QColor aColor = GetColor( aType );
      aResList << QString( "%1.SetColor( u\"%2\", QColor( %3, %4, %5 ) )" ).
        arg( aPyName ).arg( aType ).arg( aColor.red() ).arg( aColor.green() ).arg( aColor.blue() );
      aResList << QString();
    }
  }
  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( aPyName );

  return aResList;
}

Handle(TDataStd_NamedData) HYDROData_StricklerTable::Map() const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_Table );
  Handle( TDataStd_NamedData ) aMap;
  if( !aLabel.FindAttribute( TDataStd_NamedData::GetID(), aMap ) )
    aMap = TDataStd_NamedData::Set( aLabel );
  return aMap;
}

QString HYDROData_StricklerTable::GetAttrName() const
{
  Handle(TDataStd_AsciiString) aName;
  if( myLab.FindChild( DataTag_AttrName ).FindAttribute(TDataStd_AsciiString::GetID(), aName)) {
    TCollection_AsciiString aStr(aName->Get());
    return QString(aStr.ToCString());
  }
  return QString();}

bool HYDROData_StricklerTable::SetAttrName( const QString& theAttrName ) const
{
  HYDROData_Iterator anIt( HYDROData_Document::Document(), KIND_STRICKLER_TABLE );
  for( ; anIt.More(); anIt.Next() )
  {
    Handle( HYDROData_StricklerTable ) aTable = 
      Handle( HYDROData_StricklerTable )::DownCast( anIt.Current() );
    if( aTable->Label()==myLab )
      continue;

    if( theAttrName==aTable->GetAttrName() )
      return false;
  }

  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_AttrName ), HYDROData_Tool::toExtString( theAttrName ) );
  anAttr->SetID(TDataStd_AsciiString::GetID());
  return true;
}

QString HYDROData_StricklerTable::GetAttrValue( const QString& theType ) const
{
  Handle( TDataStd_NamedData ) aMap = Map();
  TCollection_ExtendedString aType = HYDROData_Tool::toExtString( theType );
  if( aMap->HasString( aType ) )
    return HYDROData_Tool::toQString( aMap->GetString( aType ) );
  else
    return "";
}

void HYDROData_StricklerTable::SetAttrValue( const QString& theType, const QString& theAttrValue ) const
{
  Handle( TDataStd_NamedData ) aMap = Map();
  TCollection_ExtendedString aType = HYDROData_Tool::toExtString( theType );
  aMap->SetString( aType, HYDROData_Tool::toExtString( theAttrValue ) );
}

QString HYDROData_StricklerTable::GetType( const QString& theAttrValue ) const
{
  if( theAttrValue.isEmpty() )
    return "";

  Handle( TDataStd_NamedData ) aMap = Map();
  TCollection_ExtendedString anAttrValue = HYDROData_Tool::toExtString( theAttrValue );
  for( TDataStd_DataMapIteratorOfDataMapOfStringString it( aMap->GetStringsContainer() ); it.More(); it.Next() )
  {
    if( it.Value() == anAttrValue )
    {
      QString aType = HYDROData_Tool::toQString( it.Key() );
      return aType;
    }
  }
  return "";
}

QColor HYDROData_StricklerTable::GetColor( const QString& theType ) const
{
  Handle( TDataStd_NamedData ) aMap = Map();
  TCollection_ExtendedString aType = HYDROData_Tool::toExtString( theType );
  if( aMap->HasInteger( aType ) )
  {
    int aColorInt = aMap->GetInteger( aType );
    int b = aColorInt % 256;
    int g = (aColorInt>>8) % 256;
    int r = (aColorInt>>16) % 256;
    return QColor( r, g, b );
  }
  else
    return QColor();
}

void HYDROData_StricklerTable::SetColor( const QString& theType, const QColor& theColor ) const
{
  Handle( TDataStd_NamedData ) aMap = Map();
  TCollection_ExtendedString aType = HYDROData_Tool::toExtString( theType );
  int r = theColor.red();
  int g = theColor.green();
  int b = theColor.blue();
  int aColorInt = ( r<<16 ) + ( g<<8 ) + b;
  aMap->SetInteger( aType, aColorInt );

  // synchronize the color for the same type in other maps
  HYDROData_Iterator anIt( HYDROData_Document::Document(), KIND_STRICKLER_TABLE );
  for( ; anIt.More(); anIt.Next() )
  {
    Handle( HYDROData_StricklerTable ) aTable = 
      Handle( HYDROData_StricklerTable )::DownCast( anIt.Current() );
    if( aTable->Label()==myLab )
      continue;

    if( aTable->HasType( theType ) )
      aTable->Map()->SetInteger( aType, aColorInt );
  }
}

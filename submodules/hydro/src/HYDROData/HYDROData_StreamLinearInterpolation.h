// Copyright (C) 2007-2019  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef _HYDROData_HYDRODATA_STREAMLINEARINTERPOLATION_HXX_
#define _HYDROData_HYDROData_STREAMLINEARINTERPOLATION_HXX_

#include <HYDROData_Entity.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_Stream.h>
#include <string>
class TopoDS_Face;

class HYDROData_StreamLinearInterpolation
{
public:

HYDRODATA_EXPORT static void Perform(const HYDROData_SequenceOfObjects& profiles,
  int pointsToInsert,
  double stepOnHA, 
  const Handle(HYDROData_PolylineXY)& hax,  
  const Handle(HYDROData_PolylineXY)& LB, 
  const Handle(HYDROData_PolylineXY)& RB,
  HYDROData_Bathymetry::AltitudePoints& outBathypoints,  
  bool buildPresentationShapes,
  bool estimateWarnOnly,
  HYDROData_Stream::PrsDefinition& prsDef,
  std::vector<std::string>* warnings = NULL);
};

#endif

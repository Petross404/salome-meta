// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROData_PolylineOperator.h>
#include <HYDROData_Document.h>
#include <HYDROData_TopoCurve.h>
#include <HYDROData_Object.h>

#ifndef LIGHT_MODE
#include <CurveCreator_Utils.hxx>
#endif

#include <BRepAdaptor_Curve.hxx>
#include <BRep_Builder.hxx>
#include <BRep_Tool.hxx>
#include <BRepBuilderAPI_MakeEdge2d.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <Extrema_ExtCC.hxx>
#include <Extrema_ExtPC.hxx>
#include <GeomAPI_Interpolate.hxx>
#include <NCollection_Vector.hxx>
#include <Precision.hxx>
#include <ShapeAnalysis_TransferParametersProj.hxx>
#include <ShapeBuild_Edge.hxx>
#include <TColgp_Array1OfVec.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <TColStd_HArray1OfBoolean.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopExp.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>
#include <QStringList>
#include <QColor>
#include <Geom_BSplineCurve.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <BRepLib_MakeWire.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"
#include <BRepTools.hxx>
#include <sstream>

template<class T> void append( std::vector<T>& theList, const std::vector<T>& theList2 )
{
  int aSize = theList.size();
  int aNewSize = aSize + theList2.size();

  if( aSize==aNewSize )
    return;

  theList.resize( aNewSize );
  for( int i=aSize, j=0; i<aNewSize; i++, j++ )
    theList[i] = theList2[j];
}

bool HYDROData_PolylineOperator::SplitPoint( const Handle( HYDROData_Document )& theDoc,
                                             const Handle( HYDROData_PolylineXY )& thePolyline,
                                             const gp_Pnt2d& thePoint,
                                             double theTolerance ) const
{
  if (thePolyline.IsNull())
  {
    return false;
  }

  std::vector<gp_Pnt2d> aPointsList( 1 );
  aPointsList[0] = thePoint;
  std::vector<TopoDS_Wire> aCurves;
  GetWires(thePolyline, aCurves);
  bool isOK = true;

  std::vector<QColor> aSectColors;
  int nbSec = thePolyline->NbSections();
  for (int i = 0; i < nbSec; i++)
  {
    QColor aColor;
    thePolyline->GetSectionColor(i, aColor);
    aSectColors.push_back(aColor);
  }

  bool bsetColor = aCurves.size() == aSectColors.size();
  for( int i=0, n=aCurves.size(); i<n; i++ )
  {
    std::vector<TopoDS_Shape> aCurvesList;
    Split( aCurves[i], thePoint, theTolerance, aCurvesList );
    NCollection_IndexedDataMap<Handle(HYDROData_PolylineXY), TopoDS_Shape> outPoly2Sh;
    bool isLocalOK = CreatePolylines( theDoc, thePolyline, aCurvesList, true, outPoly2Sh );
    isOK = isOK && isLocalOK;
    if (bsetColor)
    {
      QColor aColor = aSectColors[i];
      if( aColor.isValid() )
      {
        for (int j=1;j<=outPoly2Sh.Extent();j++)
        {
          const Handle(HYDROData_PolylineXY)& Poly = outPoly2Sh.FindKey(j); 
          int nbSec = Poly->NbSections();
          for (int k = 0; k < nbSec; k++)
            Poly->SetSectionColor(k, aColor);
        }
      }
    }
  }

  return isOK;
}

bool HYDROData_PolylineOperator::SplitTool( const Handle( HYDROData_Document )& theDoc,
                                            const Handle( HYDROData_PolylineXY )& thePolyline,
                                            const Handle( HYDROData_PolylineXY )& theTool,
                                            double theTolerance,
                                            bool& theIsIntersected) const
{
  if (thePolyline.IsNull() || theTool.IsNull())
  {
    return false;
  }

  HYDROData_SequenceOfObjects aSeq;
  aSeq.Append( theTool );
  return split( theDoc, thePolyline, aSeq, theTolerance, -1, theIsIntersected);
}

bool HYDROData_PolylineOperator::SplitAll( const Handle( HYDROData_Document )& theDoc,
                                           const HYDROData_SequenceOfObjects& thePolylines,
                                           double theTolerance )
{
  int f = thePolylines.Lower(), l = thePolylines.Upper();
  for( int i=f; i<=l; i++ )
  {
    Handle( HYDROData_PolylineXY ) aPolyline = Handle( HYDROData_PolylineXY )::DownCast( thePolylines.Value( i ) );
    bool isIntersected;
    if( !split( theDoc, aPolyline, thePolylines, theTolerance, i, isIntersected) )
      return false;
  }
  return true;
}

bool HYDROData_PolylineOperator::Merge( const Handle( HYDROData_Document )& theDoc,
                                        const QString& theName,
                                        const HYDROData_SequenceOfObjects& thePolylines,
                                        bool isConnectByNewSegment,
                                        double theTolerance )
{
  Handle( HYDROData_PolylineXY ) aNewPolyline =
    Handle( HYDROData_PolylineXY )::DownCast( theDoc->CreateObject( KIND_POLYLINEXY ) );
  int ins =0;

  HYDROData_SequenceOfObjects::Iterator aPIt(thePolylines);

  for (int ip=1; aPIt.More(); aPIt.Next(), ip++)
  {
    Handle(HYDROData_PolylineXY) aPolyline =
      Handle(HYDROData_PolylineXY)::DownCast(aPIt.Value());
    NCollection_Sequence<TCollection_AsciiString>           aSectNames;
    NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
    NCollection_Sequence<bool>                              aSectClosures;
    aPolyline->GetSections( aSectNames, aSectTypes, aSectClosures );    

    for ( int i = 1, n = aSectNames.Size(); i <= n; ++i )
    {
      const TCollection_AsciiString& aSectName = aSectNames.Value( i ) + "_" + ip;
      const HYDROData_PolylineXY::SectionType& aSectType = aSectTypes.Value( i );
      bool aSectClosure = aSectClosures.Value( i );
      aNewPolyline->AddSection(aSectName, aSectType, aSectClosure);
      HYDROData_PolylineXY::PointsList aPointsList = aPolyline->GetPoints(i-1, false);
      QColor sectColor; 
      aPolyline->GetSectionColor(i-1, sectColor);
      aNewPolyline->SetPoints(ins, aPointsList);
      if (sectColor.isValid())
        aNewPolyline->SetSectionColor(ins, sectColor);
      ins++;
    }
  }
  QString aName = theName;
  if( aName.isEmpty() )
    {
      aName = "merged";
      int anIndex = 1;
      QString aNewName = aName + "_" + QString::number(anIndex);
      while (!theDoc->FindObjectByName(aNewName).IsNull())  // the object with such a name is not found
        {
          anIndex++;
          aNewName = aName + "_" + QString::number(anIndex);
        }
      aName = aNewName;
    }

  aNewPolyline->SetName(aName);
  aNewPolyline->Update();
  return true;
}

bool HYDROData_PolylineOperator::split( const Handle( HYDROData_Document )& theDoc,
                                        const Handle( HYDROData_PolylineXY )& thePolyline,
                                        const HYDROData_SequenceOfObjects& theTools,
                                        double theTolerance,
                                        int theIgnoreIndex,
                                        bool& theIsIntersected) const
{
  theIsIntersected = false;

  if (thePolyline.IsNull())
  {
    return false;
  }

  std::vector<TopoDS_Wire> aCurves;
  GetWires(thePolyline, aCurves);
  std::vector<TopoDS_Wire> aToolCurves;
  for( int i=theTools.Lower(), n=theTools.Upper(); i<=n; i++ )
    if( i!=theIgnoreIndex )
    {
      Handle( HYDROData_PolylineXY ) aToolPolyline = 
        Handle( HYDROData_PolylineXY )::DownCast( theTools.Value( i ) );
      if (!aToolPolyline.IsNull())
      {
        std::vector<TopoDS_Wire> aTCurves;
        GetWires(aToolPolyline, aTCurves);
        append( aToolCurves, aTCurves);
      }
    }

  if (aToolCurves.empty())
  {
    return false;
  }

  std::vector<QColor> aSectColors;
  int nbSec = thePolyline->NbSections();

  const int aPSCount = aCurves.size();
  const int aTSCount = aToolCurves.size();
  std::vector<TopoDS_Shape> aResult;

  bool bSetColor = aPSCount == nbSec;
  if (bSetColor)
  {
    for (int i = 0; i < nbSec; i++)
    {
      QColor aColor;
      thePolyline->GetSectionColor(i, aColor);
      aSectColors.push_back(aColor);
    }
  }
  NCollection_DataMap <TopoDS_Shape, QColor, TopTools_ShapeMapHasher> W2Color;

  for (int aPSI = 0; aPSI < aPSCount; ++aPSI)
  {
    HYDROData_TopoCurve aCurve;
    DEBTRACE("Initialize curve " << aPSI);
    if (!aCurve.Initialize(aCurves[aPSI]))    // empty, non manifold (or internal bug!)
    {
      DEBTRACE("discarded curve (empty or non manifold)!");
      continue;
    }

    std::deque<std::list<double> > aParams;
    for (int aTSI = 0; aTSI < aTSCount; ++aTSI)
    {
      aCurve.Intersect(aToolCurves[aTSI], aParams);
    }

    std::deque<HYDROData_TopoCurve> aSplitCurves;
    theIsIntersected |= aCurve.Cut(aParams, aSplitCurves);
    std::deque<HYDROData_TopoCurve>::const_iterator aCIt =
      aSplitCurves.begin();
    std::deque<HYDROData_TopoCurve>::const_iterator aLastCIt =
      aSplitCurves.end();
    for (int iw=0; aCIt != aLastCIt; ++aCIt, iw++)
    {
      //std::stringstream brepName;
      //brepName << "theSplitWire_";
      //brepName << iw;
      //brepName << ".brep";
      //BRepTools::Write(aCIt->Wire() , brepName.str().c_str() );
      const TopoDS_Wire& aW = aCIt->Wire();
      if (bSetColor)
        W2Color.Bind(aW, aSectColors[aPSI]);
      aResult.push_back(aW);
    }
  }

  NCollection_IndexedDataMap<Handle(HYDROData_PolylineXY), TopoDS_Shape> outPoly2Sh;
  CreatePolylines( theDoc, thePolyline, aResult, true, outPoly2Sh );
  
  for (int j=1;j<=outPoly2Sh.Extent();j++)
  {
    const Handle(HYDROData_PolylineXY)& Poly = outPoly2Sh.FindKey(j); 
    const TopoDS_Shape& Sh = outPoly2Sh.FindFromIndex(j); 

    int nbSec = Poly->NbSections();
    for (int k = 0; k < nbSec; k++)
    {
      const QColor* color = W2Color.Seek(Sh);
      if (color)
        Poly->SetSectionColor(k, *color);
    }
  }
  
  return true;
}

void HYDROData_PolylineOperator::GetWires(
  const Handle( HYDROData_PolylineXY )& thePolyline,
  std::vector<TopoDS_Wire>& theWires)
{
  TopoDS_Shape aShape = thePolyline->GetShape();
  if (aShape.IsNull())
    return;
  if( aShape.ShapeType()==TopAbs_WIRE )
  {
    theWires.push_back( TopoDS::Wire( aShape ) );
  }
  else
  {
    TopExp_Explorer anExp( aShape, TopAbs_WIRE );
    for( ; anExp.More(); anExp.Next() )
    {
      theWires.push_back( TopoDS::Wire( anExp.Current() ) );
    }
  }
}

void HYDROData_PolylineOperator::Split(
  const TopoDS_Wire& theWire,
  const gp_Pnt2d& thePoint,
  double theTolerance,
  std::vector<TopoDS_Shape>& theWires)
{
  HYDROData_TopoCurve aCurve;
  if (!aCurve.Initialize(theWire))
  {
    theWires.push_back(theWire);
    return;
  }

  const gp_XYZ aP(thePoint.X(), thePoint.Y(), 0);
  std::list<TopoDS_Edge>::const_iterator aEPos;
  double aParam;
  aCurve.Project(aP, aEPos, aParam);
  HYDROData_TopoCurve aCurve1, aCurve2;
  aCurve.Cut(aEPos, aParam, aCurve1, aCurve2);
  theWires.push_back(aCurve1.Wire());
  if (!aCurve2.IsEmpty())
  {
    theWires.push_back(aCurve2.Wire());
  }
}

bool HYDROData_PolylineOperator::CreatePolylines( const Handle( HYDROData_Document )& theDoc,
                                                  const Handle( HYDROData_PolylineXY )& theOldPolyline,
                                                  const std::vector<TopoDS_Shape>& theShapes,
                                                  bool isUseIndices,
                                                  NCollection_IndexedDataMap<Handle(HYDROData_PolylineXY), TopoDS_Shape>& outPoly2Sh)
{
  if( theDoc.IsNull() )
    return false;

  if ( theOldPolyline.IsNull() )
    return false;
  const QString& theNamePrefix = theOldPolyline->GetName();

  int n = theShapes.size();
  DEBTRACE("theShapes.size() "<< n);
  int anIndex = 1;
  for( int i=0; i<n; i++ )
  {
    Handle( HYDROData_PolylineXY ) aPolyline = 
      Handle( HYDROData_PolylineXY )::DownCast( theDoc->CreateObject( KIND_POLYLINEXY ) );
    if( aPolyline.IsNull() )
      return false;

    aPolyline->ImportShape(theShapes[i], false, theOldPolyline, false);

    if( isUseIndices )
    {
      QString aNewName = theNamePrefix + "_" + QString::number( anIndex );
      while( !theDoc->FindObjectByName( aNewName ).IsNull() )  // the object with such a name is not found
      {
        anIndex++;
        aNewName = theNamePrefix + "_" + QString::number( anIndex );
      }
      aPolyline->SetName( aNewName );
    }
    else
    {
      aPolyline->SetName( theNamePrefix );
    }

    outPoly2Sh.Add(aPolyline, theShapes[i]);
  }
  return true;
}

double HYDROData_PolylineOperator::ReduceDeflection(
  const double theDeflection,
  HYDROData_TopoCurve& theCurve,
  int& thePieceCount)
{
  // Construct the approximating B-spline.
  std::list<gp_XYZ> aPs;
  if (!theCurve.ValuesInKnots(aPs))
  {
    return -1;
  }

  Handle(TColgp_HArray1OfPnt) aPs2 = new TColgp_HArray1OfPnt(1, aPs.size());
  {
    std::list<gp_XYZ>::const_iterator aLastPIt = aPs.end();
    std::list<gp_XYZ>::const_iterator aPIt = aPs.begin();
    for (int aPN = 1; aPIt != aLastPIt; ++aPN, ++aPIt)
    {
      aPs2->SetValue(aPN, *aPIt);
    }
  }
  Handle(Geom_BSplineCurve) aBSpline2;
  const bool isClosed = theCurve.IsClosed();
#ifndef LIGHT_MODE
  if (!CurveCreator_Utils::constructBSpline(aPs2, isClosed, aBSpline2))
#endif
  {
    return -1;
  }

  // Calculate the piece deflections.
  std::deque<double> aSqDefls;
  double aMaxSqDefl = 0;
  std::list<TopoDS_Edge>& aEdges = theCurve.Edges();
  std::list<TopoDS_Edge>::const_iterator aLastEIt = aEdges.end();
  {
    std::list<TopoDS_Edge>::const_iterator aEIt = aEdges.begin();
    for (int aPrevKCount = 0; aEIt != aLastEIt; ++aEIt)
    {
      TopLoc_Location aLoc;
      double aParams[2];
      Handle(Geom_BSplineCurve) aBSpline = Handle(Geom_BSplineCurve)::DownCast(
        BRep_Tool::Curve(*aEIt, aLoc, aParams[0], aParams[1]));
      const int aKCount = aBSpline->NbKnots();
      for (int aKN = 1; aKN < aKCount; ++aKN)
      {
        const double aParam =
          (aBSpline->Knot(aKN) + aBSpline->Knot(aKN + 1)) * 0.5;
        const double aParam2 = (aBSpline2->Knot(aPrevKCount + aKN) +
          aBSpline2->Knot(aPrevKCount + aKN + 1)) * 0.5;
        const double aSqDefl = Abs(aBSpline->Value(aParam).
          SquareDistance(aBSpline2->Value(aParam2)));
        aSqDefls.push_back(aSqDefl);
        if (aMaxSqDefl < aSqDefl)
        {
          aMaxSqDefl = aSqDefl;
        }
      }
      aPrevKCount += aKCount - 1;
    }
  }

  // Check whether the reducing is necessary.
  const double aMaxDefl = Sqrt(aMaxSqDefl);
  if (aMaxDefl <= theDeflection)
  {
    return aMaxDefl;
  }

  // Reduce the deflections.
  const double aThresSqDefl =
    Max(aMaxSqDefl * 0.25, theDeflection * theDeflection);
  std::list<TopoDS_Edge>::iterator aEIt = aEdges.begin();
  std::deque<double>::const_iterator aSqDIt = aSqDefls.begin();
  thePieceCount = 0;
  for (; aEIt != aLastEIt; ++aEIt)
  {
    TopLoc_Location aLoc;
    double aParams[2];
    Handle(Geom_BSplineCurve) aBSpline = Handle(Geom_BSplineCurve)::DownCast(
      BRep_Tool::Curve(*aEIt, aLoc, aParams[0], aParams[1]));
    Handle(Geom_BSplineCurve) aBSpline2 =
      Handle(Geom_BSplineCurve)::DownCast(aBSpline->Copy());
    const int aKCount = aBSpline->NbKnots();
    for (int aKN = 1; aKN < aKCount; ++aSqDIt, ++aKN)
    {
      if (*aSqDIt > aThresSqDefl)
      {
        aBSpline2->InsertKnot(
          (aBSpline->Knot(aKN) + aBSpline->Knot(aKN + 1)) * 0.5);
      }
    }
    TopoDS_Edge aEdge;
    BRep_Builder().MakeEdge(aEdge, aBSpline2, Precision::Confusion());
    BRep_Builder().Add(aEdge, TopExp::FirstVertex(*aEIt));
    BRep_Builder().Add(aEdge, TopExp::LastVertex(*aEIt));
    thePieceCount += aBSpline2->NbKnots() - 1;
    *aEIt = aEdge;
  }
  return aMaxDefl;
}

bool HYDROData_PolylineOperator::Extract( const Handle(HYDROData_Document)& theDocument,
                                          const Handle(HYDROData_Object)& theObject )
{
  if( theObject.IsNull() || theDocument.IsNull() )
    return false;

  QList<TopoDS_Shape> aBoundShapes;
  QStringList aBoundNames;
  QMap<QString, TopTools_IndexedMapOfShape> aNameToShMap;

  theObject->GetBoundaries( aBoundShapes, aBoundNames );

  for( int i=0, n=aBoundShapes.size(); i<n; i++ )
  {
    TopoDS_Shape aShape = aBoundShapes[i];
    if( aShape.IsNull() )
      continue;

    QString aBoundName = i<aBoundNames.size() ? aBoundNames[i] : "";
    
    if (!aNameToShMap.contains(aBoundName))
    {
      TopTools_IndexedMapOfShape IM;
      IM.Add(aShape);
      aNameToShMap[aBoundName] = IM;
    }
    else
      aNameToShMap[aBoundName].Add(aShape);
     
  }

  foreach( QString K, aNameToShMap.keys() )
  {
    const TopTools_IndexedMapOfShape& IM = aNameToShMap.value(K);
    TopTools_ListOfShape LSE;
    for (int i = 1; i <= IM.Extent(); i++)
    {
      const TopoDS_Edge& E = TopoDS::Edge(IM(i));
      if (E.IsNull())
        continue;
      LSE.Append(E);
    }


    TopoDS_Shape aShapeOut;
    if (LSE.Extent() == 1)
    {
      aShapeOut = LSE.First();
    }
    else if (LSE.Extent() > 1)
    {
      BRepLib_MakeWire WM;
      WM.Add(LSE);
      if (WM.IsDone())
        aShapeOut = WM.Wire();
      else
        continue;
    }
    else continue;

    Handle( HYDROData_PolylineXY ) aPolyline = 
      Handle( HYDROData_PolylineXY )::DownCast( theDocument->CreateObject( KIND_POLYLINEXY ) );

    if( aPolyline.IsNull() )
      return false;

    aPolyline->SetShape( aShapeOut );

    int anIndex = 0;
    QString aName = K;
    while( !theDocument->FindObjectByName( aName ).IsNull() )
    {
      anIndex++;
      aName = K + "_" + QString::number( anIndex );
    }
    aPolyline->SetName( aName );
  }

  return true;
}

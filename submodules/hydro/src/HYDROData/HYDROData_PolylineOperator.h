// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_PolylineOperator_HeaderFile
#define HYDROData_PolylineOperator_HeaderFile

#include <HYDROData_PolylineXY.h>
#include <Geom2d_Curve.hxx>
#include <gp_Pnt2d.hxx>
#include <vector>
#include <NCollection_IndexedDataMap.hxx>

class HYDROData_Document;
class HYDROData_TopoCurve;
class HYDROData_Object;

class HYDRODATA_EXPORT HYDROData_PolylineOperator
{
public:
  bool SplitPoint( const Handle( HYDROData_Document )& theDoc,
                   const Handle( HYDROData_PolylineXY )& thePolyline,
                   const gp_Pnt2d& thePoint,
                   double theTolerance ) const;
  bool SplitTool( const Handle( HYDROData_Document )& theDoc,
                  const Handle( HYDROData_PolylineXY )& thePolyline,
                  const Handle( HYDROData_PolylineXY )& theTool,
                  double theTolerance,
                  bool& theIsIntersected) const;
  bool SplitAll( const Handle( HYDROData_Document )& theDoc,
                 const HYDROData_SequenceOfObjects& thePolylines,
                 double theTolerance );
  bool Merge( const Handle( HYDROData_Document )& theDoc,
              const QString& theName,
              const HYDROData_SequenceOfObjects& thePolylines,
              bool isConnectByNewSegment,
              double theTolerance );

  static void GetWires(
    const Handle( HYDROData_PolylineXY )& thePolyline,
    std::vector<TopoDS_Wire>& theWires);

  //! The method is intended to approximate the B-spline edge curve
  //! by a 'Hydro' spline section.
  //! Inserts the knot in the middle of each such B-spline piece that
  //! the middle deflection is more than the required deflection and
  //! more than the half of the maximal middle deflection.
  //! Returns the initial maximal middle deflection.
  //! Returns a negative value in the case of any error.
  static double ReduceDeflection(
    const double theDeflection,
    HYDROData_TopoCurve& theCurve,
    int& thePieceCount);

  bool Extract( const Handle(HYDROData_Document)& theDocument,
                const Handle(HYDROData_Object)& theObject );

protected:
  bool split( const Handle( HYDROData_Document )& theDoc,
              const Handle( HYDROData_PolylineXY )& thePolyline,
              const HYDROData_SequenceOfObjects& theTools,
              double theTolerance,
              int theIgnoreIndex,
              bool& theIsIntersected) const;

  static void Split(
    const TopoDS_Wire& theWire,
    const gp_Pnt2d& thePoint,
    double theTolerance,
    std::vector<TopoDS_Shape>& theWires);

  static bool CreatePolylines( const Handle( HYDROData_Document )& theDoc,
                               const Handle( HYDROData_PolylineXY )& theOldPolyline,
                               const std::vector<TopoDS_Shape>& theShape,
                               bool isUseIndices,
                               NCollection_IndexedDataMap<Handle(HYDROData_PolylineXY), TopoDS_Shape>& outPoly2Sh);
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_IProfilesInterpolator_HeaderFile
#define HYDROData_IProfilesInterpolator_HeaderFile

#include "HYDROData.h"

#include <TCollection_AsciiString.hxx>
#include <NCollection_Sequence.hxx>

#include <string>
#include <vector>
#include <map>
#include <gp_XYZ.hxx>


/**
 * Errors that could appear on interpolation calculations.
 * If there is no error, it is "OK".
 */
enum InterpolationError {
  OK = 0, ///< success
  InvalidParametersError, ///< input parameters are invalid
  UnknownError ///< problem has unknown nature
};

/**\class HYDROData_IProfilesInterpolator
 * \brief The base class to provide interface for profiles interpolation.
 */
class HYDROData_IProfilesInterpolator
{
public:

  /**
   * Public constructor.
   */
  HYDRODATA_EXPORT HYDROData_IProfilesInterpolator();

  /**
   * Public virtual destructor.
   */
  virtual HYDRODATA_EXPORT ~HYDROData_IProfilesInterpolator();

public:

  /**
   * Get description of the interpolation algorithm.
   * \return the description string
   */
  HYDRODATA_EXPORT virtual TCollection_AsciiString GetDescription() const = 0;

  /**
   * Set profiles as vectors of point coordinates [x1, y1, z1, x2, y2, z2, ...].
   * \param theProfile1 the first profile points
   * \param theProfile1 the second profile points
   */
  HYDRODATA_EXPORT virtual void SetProfiles( const std::vector<double> theProfile1, const std::vector<double> theProfile2 );

  /**
   * Set profiles as sequences of gp_XYZ points.
   * \param theProfile1 the first profile points
   * \param theProfile1 the second profile points
   */
  HYDRODATA_EXPORT virtual void SetProfiles( const NCollection_Sequence<gp_XYZ>& theProfile1, 
                                             const NCollection_Sequence<gp_XYZ>& theProfile2 );

  /**
   * Set number of profiles to compute.
   * \param theNumber the number of profiles to be computed
   */
  HYDRODATA_EXPORT virtual void SetResultProfilesNumber( const int theNumber );

  /**
   * Set the additional parameter.
   * \param the parameter name
   * \param the parameter value
   */
  HYDRODATA_EXPORT virtual void SetParameter( const TCollection_AsciiString& theName, 
                                              const TCollection_AsciiString& theValue );

  /**
   * Get the last error code.
   * \return the error code from InterpolationError enumeration
   */
  HYDRODATA_EXPORT virtual InterpolationError GetErrorCode() const;

  /**
   * Get string description of the last error.
   * \return the string description
   */
  HYDRODATA_EXPORT virtual TCollection_AsciiString GetErrorMessage() const;

  /**
   * Reset interpolator state: both input and output data are reset.
   */
  HYDRODATA_EXPORT virtual void Reset();

public:
  /**
   * Perform interpolation calculations.
   */
  HYDRODATA_EXPORT virtual void Calculate() = 0;

  /**
   * Get number of calculated profiles ( could be less than the number of profiles to be computed set as a parameter ).
   * \return the number of really calculated profiles
   */
  HYDRODATA_EXPORT virtual int GetCalculatedProfilesNumber() const;

  /**
   * Get result profile by index as a vector of point coordinates [x1, y1, z1, x2, y2, z2, ...].
   * \param theProfileIndex the profile index [0, <number of profiles to compute>]
   * \return the profile with the given index or empty vector if the index is out of range
   */
  HYDRODATA_EXPORT std::vector<double> GetResultProfileCoords( const int theProfileIndex ) const;

  /**
   * Get result profile by index as a sequence of gp_XYZ points.
   * \param theProfileIndex the profile index [0, <number of profiles to compute>]
   * \return the profile with the given index or empty sequence if the index is out of range
   */
  HYDRODATA_EXPORT NCollection_Sequence<gp_XYZ> GetResultProfilePoints( const int theProfileIndex ) const;

protected:
  /**
   * Set the error code.
   * \param theError the error code from InterpolationError enumeration
   */
  HYDRODATA_EXPORT virtual void SetErrorCode( const InterpolationError& theError );

  /**
   * Set the error string description.
   * \param the string description
   */
  HYDRODATA_EXPORT virtual void SetErrorMessage( const std::string& theMessage );

  /**
   * Get the first profile coordinates.
   * \return the first profile points
   */
  HYDRODATA_EXPORT std::vector<double> GetFirstProfileCoords() const;

  /**
   * Get the second profile coordinates.
   * \return the second profile points
   */
  HYDRODATA_EXPORT std::vector<double> GetSecondProfileCoords() const;

  /**
   * Get the first profile points.
   * \return the first profile points
   */
  HYDRODATA_EXPORT NCollection_Sequence<gp_XYZ> GetFirstProfilePoints() const;

  /**
   * Get the second profile points.
   * \return the second profile points
   */
  HYDRODATA_EXPORT NCollection_Sequence<gp_XYZ> GetSecondProfilePoints() const;

  /**
   * Get number of profiles to compute.
   * \return the current value of number of result profiles parameter
   */
  HYDRODATA_EXPORT virtual int GetNbProfilesToCompute() const;

  /**
   * Clear result data (including errors).
   */
  HYDRODATA_EXPORT void ClearResults();

  /**
   * Insert the calculated profile to the resuls as a list of coordinates.
   * \param theProfile the list of coordinates [x1, y1, z1, x2, y2, z2, ...]
   */
  HYDRODATA_EXPORT void InsertResultProfile( const std::vector<double>& theProfile );

  /**
   * Insert the calculated profile to the resuls as a list of points.
   * \param theProfile the list of points gp_XYZ
   */
  HYDRODATA_EXPORT void InsertResultProfile( const NCollection_Sequence<gp_XYZ>& theProfile );

private:
  NCollection_Sequence<gp_XYZ> GetPoints( const std::vector<double>& theCoords ) const;

private:
  std::vector<double> myProfile1, myProfile2; ///< the two input profiles
  int myResultProfilesNumber; ///< the number of profiles to compute
  std::map<std::string, std::string> myParameters; ///< the map of additional parameters

  InterpolationError myErrorCode; ///< the last error code
  std::string myErrorMessage; ///< the last error message

  std::vector< std::vector<double> > myResultProfiles; ///< the list of result profiles
};

#endif

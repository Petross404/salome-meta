// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Profile_HeaderFile
#define HYDROData_Profile_HeaderFile

#include "HYDROData_Object.h"
#include "HYDROData_ProfileUZ.h"

class gp_XYZ;
class OSD_File;
class HYDROData_Document;

/**\class HYDROData_Profile
 * \brief Class that stores/retreives information about the profile.
 */
class HYDROData_Profile : public HYDROData_Object
{
public:

  typedef gp_XYZ                             ProfilePoint;
  typedef NCollection_Sequence<ProfilePoint> ProfilePoints;

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Object::DataTag_First + 100, ///< first tag, to reserve
    DataTag_FirstPoint,       ///< first(left) point
    DataTag_LastPoint,        ///< last(right) point
    DataTag_ChildProfileUZ,   ///< child parametric profile
    DataTag_FilePath,          ///< profile imported file path
    DataTag_ProfileColor      ///< color of profile
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Profile, HYDROData_Object);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_PROFILE; }

  /**
   * Dump object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Returns the top shape of the object.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetTopShape() const;

  /**
   * Returns the 3d shape of the object.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape3D(bool forceRebuild = false, bool reverseXCoord = false) const;

  /**
   * Updates profile 3D presentation.
   * Call this method whenever you made changes in data structure.
   * This method does not called automatically since it may take a very long time.
   */
  HYDRODATA_EXPORT virtual void Update();

  HYDRODATA_EXPORT virtual void UpdateLocalCS( double theDx, double theDy );

  HYDRODATA_EXPORT static ProfilePoints CalculateProfilePoints(const HYDROData_ProfileUZ::PointsList& theParametricPoints, 
  const gp_XY& aFirstPoint, const gp_XY& aLastPoint);

  /**
   * Returns default filling color for new profile.
   */
  HYDRODATA_EXPORT virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new profile.
   */
  HYDRODATA_EXPORT virtual QColor DefaultBorderColor() const;


public:

  /**
   * Check profile data and returns true if profile is valid.
   * Validity is determined by:
   *   - First(left) and Last(right) point was set
   *   - Parametric points size is more than 1
   */
  HYDRODATA_EXPORT bool IsValid() const;

  /**
   * Invalidate profile first and last points.
   */
  HYDRODATA_EXPORT void Invalidate();
  
public:

  // Public methods to work with profile points.

  /**
   * Set first(left) point for profile.
   * \param thePoint the point
   */
  HYDRODATA_EXPORT void SetLeftPoint( const gp_XY& thePoint, bool IsConvertFromGlobal = false );

  /**
   * Returns first(left) point of profile.
   * \param thePoint[out] profile first point
   * \return true if point has been set
   */
  HYDRODATA_EXPORT bool GetLeftPoint( gp_XY& thePoint, bool IsConvertToGlobal = false,
                                      bool CanUseDefault = false ) const;

  /**
   * Set last(right) point for profile.
   * \param thePoint the point
   */
  HYDRODATA_EXPORT void SetRightPoint( const gp_XY& thePoint, bool IsConvertFromGlobal = false );

  /**
   * Returns last(right) point of profile.
   * \param thePoint[out] profile last point
   * \return true if point has been set
   */
  HYDRODATA_EXPORT bool GetRightPoint( gp_XY& thePoint, bool IsConvertToGlobal = false,
                                       bool CanUseDefault = false ) const;

  /**
   * Returns object which store parametric presentation of profile points.
   * \return profile U,Z
   */
  HYDRODATA_EXPORT Handle(HYDROData_ProfileUZ) GetProfileUZ( const bool theIsCreate = true ) const;


  /**
   * Return number of profile points.
   * \return number of points
   */
  HYDRODATA_EXPORT int NbPoints() const;

  /**
   * Remove all profile points.
   */
  HYDRODATA_EXPORT void RemovePoints();


  /**
   * Replace current profile parametric points by new one.
   * \param thePoints the list with new points in parametric form
   */
  HYDRODATA_EXPORT void SetParametricPoints( const HYDROData_ProfileUZ::PointsList& thePoints );

  /**
   * Returns profile points in parametric form.
   * \return points list
   */
  HYDRODATA_EXPORT HYDROData_ProfileUZ::PointsList GetParametricPoints() const;


  /**
   * Replace current profile points by new one.
   * First and last points will be automatically updated.
   * \param thePoints the list with new profile points
   */
  HYDRODATA_EXPORT void SetProfilePoints( const ProfilePoints& thePoints, bool IsConvertFromGlobal = true );

  /**
   * Returns profile points.
   * Empty sequence is returned if first or last point was not set.
   * \return profile points list
   */
  HYDRODATA_EXPORT ProfilePoints GetProfilePoints( bool IsConvertToGlobal = false,
                                                   bool CanUseDefaultLeftRight = false ) const;


  /**
   * Return profile point with minimal Z value.
   * \return non-parametric profile point
   */
  HYDRODATA_EXPORT ProfilePoint GetBottomPoint(bool IsConvertToGlobal = false) const;

  /**
   * Return profile middle point.
   * \return non-parametric profile point
   */
  HYDRODATA_EXPORT ProfilePoint GetMiddlePoint( bool CanUseDefault ) const;

public:
  // Public methods to work with files.

  /**
   * Stores the profile file path
   * \param theFilePath profile file path
   */
  HYDRODATA_EXPORT void SetFilePath( const TCollection_AsciiString& theFilePath );

  /**
   * Returns uploaded profile file path
   */
  HYDRODATA_EXPORT TCollection_AsciiString  GetFilePath() const;


  HYDRODATA_EXPORT void SetProfileColor( const QColor& theColor );

  HYDRODATA_EXPORT bool GetProfileColor(QColor&) const;


  /**
   * Imports Profile data from file. The supported file types:
   *  - parametric presentation of profile (2 points in line U,Z)
   *  - georeferenced presentation of profile (3 points in line X,Y,Z)
   * Create as many objects as many profiles in the file are defined.
   * \param theFileName the path to file
   * \return \c number of successfully imported profiles
   */
  HYDRODATA_EXPORT static int ImportFromFile( const Handle(HYDROData_Document)& theDoc,
                                               const TCollection_AsciiString&    theFileName,
                                               NCollection_Sequence<int>&        theBadProfilesIds,
                                               bool isToProject = true );

  /**
   * Imports Profile data from file.
   * \param theFileName the path to file
   * \param theIsRead set to true if at least one non empty string was read from file
   * \return \c true if file has been successfully read
   */
  HYDRODATA_EXPORT virtual bool ImportFromFile( const TCollection_AsciiString& theFileName,
                                                bool isToProject = true,
                                                bool* isNotEmpty = 0 );

  /**
   * Imports Profile data from file. 
   * \param theFile file to read
   * \param theIsRead set to true if at least one non empty string was read from file
   * \return \c true if file has been successfully read
   */
  HYDRODATA_EXPORT virtual bool ImportFromFile( OSD_File& theFile,
                                                bool isToProject = true,
                                                bool* isNotEmpty = 0 );

protected:
  /**
   * Checks and if necessary create child 3D object.
   * Reimplemented to prevent creation of 3D child object.
   */
  HYDRODATA_EXPORT virtual void checkAndSetObject3D() {}

  TopoDS_Shape CreateProfileWire( bool canUseDefaultPoints, bool reverseXCoord = false) const;

  static void ProjectProfilePoints( ProfilePoints& thePoints );

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Profile();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_Profile();
};

#endif

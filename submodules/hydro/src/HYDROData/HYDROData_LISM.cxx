// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com


#include <HYDROData_LISM.h>
#include <HYDROData_Profile.h>
#include <HYDROData_StreamLinearInterpolation.h>
#include <TopoDS_Face.hxx>

#include <QSet>
#include <QString>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT( HYDROData_LISM, HYDROData_Bathymetry )

HYDROData_LISM::HYDROData_LISM()
{
}

HYDROData_LISM::~HYDROData_LISM()
{
}

//
HYDROData_SequenceOfObjects HYDROData_LISM::GetProfiles() const
{
  return GetReferenceObjects( DataTag_Profiles );
}

void HYDROData_LISM::SetProfiles( const HYDROData_SequenceOfObjects& theProfiles )
{
  SetReferenceObjects( theProfiles, DataTag_Profiles );
  Changed( Geom_3d );
}

//
bool HYDROData_LISM::SetLeftBank( const Handle(HYDROData_PolylineXY)& theBank )
{
  if (theBank.IsNull())
    return false;

  if ( !HYDROData_Stream::IsValidAsAxis( theBank ) )
    return false;

  Handle(HYDROData_PolylineXY) aPrevBank = GetLeftBank();
  if ( IsEqual( aPrevBank, theBank ) )
    return true;

  SetReferenceObject( theBank, DataTag_LeftBank );
  Changed( Geom_3d );
  return true;
}

Handle(HYDROData_PolylineXY) HYDROData_LISM::GetLeftBank() const
{
  return Handle(HYDROData_PolylineXY)::DownCast(GetReferenceObject( DataTag_LeftBank ) );
}

bool HYDROData_LISM::SetRightBank( const Handle(HYDROData_PolylineXY)& theBank )
{
  if (theBank.IsNull())
    return false;

  if ( !HYDROData_Stream::IsValidAsAxis( theBank ) )
    return false;

  Handle(HYDROData_PolylineXY) aPrevBank = GetRightBank();
  if ( IsEqual( aPrevBank, theBank ) )
    return true;

  SetReferenceObject( theBank, DataTag_RightBank );
  Changed( Geom_3d );
  return true;
}

bool HYDROData_LISM::SetHydraulicAxis( const Handle(HYDROData_PolylineXY)& theAxis )
{
  if ( !HYDROData_Stream::IsValidAsAxis( theAxis ) )
    return false;

  Handle(HYDROData_PolylineXY) aPrevAxis = GetHydraulicAxis();
  if ( IsEqual( aPrevAxis, theAxis ) )
    return true;

  SetReferenceObject( theAxis, DataTag_HydraulicAxis );

  Changed( Geom_3d );
  return true;
}

Handle(HYDROData_PolylineXY) HYDROData_LISM::GetHydraulicAxis() const
{
  return Handle(HYDROData_PolylineXY)::DownCast( GetReferenceObject( DataTag_HydraulicAxis ) );
}


Handle(HYDROData_PolylineXY) HYDROData_LISM::GetRightBank() const
{
  return Handle(HYDROData_PolylineXY)::DownCast(GetReferenceObject( DataTag_RightBank ) );
}

int HYDROData_LISM::GetNbProfilePoints() const
{
  return GetInteger( DataTag_NbProfilePoints );
}

void HYDROData_LISM::SetNbProfilePoints( int theNbProints )
{
  SetInteger( DataTag_NbProfilePoints, theNbProints );
  Changed( Geom_3d );
}

double HYDROData_LISM::GetHaxStep() const
{
  return GetDouble( DataTag_HaxStep );
}

void HYDROData_LISM::SetHaxStep( double theHaxStep )
{
  SetDouble( DataTag_HaxStep, theHaxStep );
  Changed( Geom_3d );
}

void HYDROData_LISM::Update()
{
  DEBTRACE("Update " << GetName().toStdString());
  AltitudePoints anOutPoints;
  HYDROData_SequenceOfObjects aRefProfiles = GetProfiles();  
  int nbprofilepoints = GetNbProfilePoints();
  double step = GetHaxStep();
  Handle(HYDROData_PolylineXY) aHAX = GetHydraulicAxis();
  Handle(HYDROData_PolylineXY) aLB = GetLeftBank();
  Handle(HYDROData_PolylineXY) aRB = GetRightBank();

  std::vector<std::string> warnings;
  HYDROData_Stream::PrsDefinition prsDef;
  HYDROData_StreamLinearInterpolation::Perform(aRefProfiles, nbprofilepoints, step, aHAX, aLB, aRB, anOutPoints, true, false, prsDef, &warnings);

  SetAltitudePoints( anOutPoints );  
 
  SetShape( DataTag_LeftBankShape, prsDef.myLeftBank);
  SetShape( DataTag_RightBankShape, prsDef.myRightBank);
  SetShape( DataTag_InletShape, prsDef.myInlet);
  SetShape( DataTag_OutletShape, prsDef.myOutlet );
  SetShape( DataTag_3DShape, prsDef.myPrs3D );
  SetShape( DataTag_2DShape, prsDef.myPrs2D );

  HYDROData_Bathymetry::Update();
}


void HYDROData_LISM::GetShapePresentations( HYDROData_Stream::PrsDefinition& prsDef)
{
  prsDef.myLeftBank = GetShape( DataTag_LeftBankShape);
  prsDef.myRightBank = GetShape( DataTag_RightBankShape);
  prsDef.myInlet = GetShape( DataTag_InletShape);
  prsDef.myOutlet = GetShape( DataTag_OutletShape );
  prsDef.myPrs3D = GetShape( DataTag_3DShape );
  prsDef.myPrs2D  = GetShape( DataTag_2DShape );
}


/*void HYDROData_LISM::GetWarnings(NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>& warnings)
{
  warnings = myWarnings;
}
   */

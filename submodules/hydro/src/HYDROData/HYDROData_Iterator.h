// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Iterator_HeaderFile
#define HYDROData_Iterator_HeaderFile

#include "HYDROData_Document.h"

#include <TDF_ChildIDIterator.hxx>

/**\class HYDROData_Iterator
 * \brief Allows to iterate all objects by the document and kind of object.
 *
 * Using the document data structures, iterates through objects, filtered by 
 * kind of this object. This class must have information about all kinds of 
 * objects of the document: for correct iteration and creation of them.
 */
class HYDROData_Iterator
{
public:
  /**
   * Initializes iterator by objects of the document.
   * \param theDoc document to iterate
   * \param theKind kind of the iterated object, can be UNKNOWN: to iterate all objects
   */
  HYDRODATA_EXPORT HYDROData_Iterator( const Handle(HYDROData_Document)& theDoc,
                                       const ObjectKind theKind = KIND_UNKNOWN);

  /**
   * Iterates to the next object
   */
  HYDRODATA_EXPORT void Next();

  /**
   * Returns true if current object exists
   */
  HYDRODATA_EXPORT bool More() const;

  /**
   * Returns the current object of the iterator.
   */
  HYDRODATA_EXPORT Handle(HYDROData_Entity) Current();

public:

  /**
   * Creates object in the document on the given label
   * \param theDoc document where object will be located
   * \param theKind kind of the new object, can not be UNKNOWN
   */
  static HYDRODATA_EXPORT Handle(HYDROData_Entity) CreateObject(
    TDF_Label&        theNewLabel, 
    const ObjectKind& theObjectKind );

  /**
   * Returns object associated to the given label.
   */
  static HYDRODATA_EXPORT Handle(HYDROData_Entity) Object( 
    const TDF_Label& theLabel );

protected:

  friend class HYDROData_Document;
  friend class HYDROData_Entity;

  /**
   * Creates object in the document, call HYDROData_Document method to create 
   * objects from other packages.
   * \param theDoc document where object will be located
   * \param theKind kind of the new object, can not be UNKNOWN
   */
  static Handle(HYDROData_Entity) CreateObject(
    const Handle(HYDROData_Document)& theDoc,
    const ObjectKind&                 theObjectKind );

  TDF_ChildIDIterator myIter; ///< iterator by the objects in the document
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_IAltitudeObject_HeaderFile
#define HYDROData_IAltitudeObject_HeaderFile

#include "HYDROData_Entity.h"

class gp_XY;


/**\class HYDROData_IAltitudeObject
 * \briefThe base class for all altitude objects in the HYDRO module.
 *
 */
class HYDROData_IAltitudeObject : public HYDROData_Entity
{
protected:

  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_IAltitudeObject, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const = 0;

public:
  // Public methods to work with altitudes.

  /**
   * Returns altitude points list.
   * \return points list
   */
  HYDRODATA_EXPORT static double            GetInvalidAltitude();

  /**
   * Returns altitude for given point.
   * \param thePoint the point to examine
   * \return altitude value
   */
  HYDRODATA_EXPORT virtual double           GetAltitudeForPoint( const gp_XY& thePoint, int theMethod=0 ) const = 0;


protected:

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_IAltitudeObject();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_IAltitudeObject();
};

#endif

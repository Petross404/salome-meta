// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDRODATA_LCM_FACECLASSIFIER_H
#define HYDRODATA_LCM_FACECLASSIFIER_H

#include <HYDROData.h>
#include <vector>
#include <set>
#include <QString>

#include "Bnd_Box2d.hxx" 
#include <NCollection_UBTree.hxx>
#include <NCollection_List.hxx>
#include <NCollection_IndexedDataMap.hxx>
#include <TopoDS_Face.hxx>
#include <NCollection_Map.hxx>
#include <Geom_Plane.hxx>

class BRepTopAdaptor_FClass2d;
class HYDROData_LandCoverMap;

typedef NCollection_UBTree <Standard_Integer, Bnd_Box2d> HYDROData_FaceClassifier_BndBoxTree;

class HYDROData_FaceClassifier_BndBoxTreeSelector : public HYDROData_FaceClassifier_BndBoxTree::Selector
{
public:
  HYDROData_FaceClassifier_BndBoxTreeSelector(const NCollection_IndexedDataMap<TopoDS_Face, BRepTopAdaptor_FClass2d*>& theMapOfShape)
    : HYDROData_FaceClassifier_BndBoxTreeSelector::Selector(), myMapF2Class2d (theMapOfShape)
  {}

  Standard_Boolean Reject (const Bnd_Box2d& theBox) const
  {
    return (theBox.IsOut (myP));
  }

  Standard_Boolean Accept (const Standard_Integer& theObj);

  const NCollection_List<TopoDS_Face>& GetResFaces () const 
  { 
    return myResFaces;
  }

  const TopoDS_Face& GetFirstFace() const 
  { 
    if (!myResFaces.IsEmpty())
      return myResFaces.First();
    else
      TopoDS_Face();
  }

  void SetCurrentPoint (const gp_Pnt2d& theP) 
  { 
    myP = theP;
  }

private:
  HYDROData_FaceClassifier_BndBoxTreeSelector(const HYDROData_FaceClassifier_BndBoxTreeSelector& );
  HYDROData_FaceClassifier_BndBoxTreeSelector& operator=(const HYDROData_FaceClassifier_BndBoxTreeSelector& );

private:
  const NCollection_IndexedDataMap<TopoDS_Face, BRepTopAdaptor_FClass2d*>& myMapF2Class2d;
  gp_Pnt2d myP;
  NCollection_List<TopoDS_Face> myResFaces;
};

class HYDRODATA_EXPORT HYDROData_LCM_FaceClassifier
{

public:

  HYDROData_LCM_FaceClassifier(const HYDROData_LandCoverMap* const theLCM) : myLCM(theLCM)
  {};

  ~HYDROData_LCM_FaceClassifier()
  {};

  void Classify( const std::vector<gp_XY>& thePoints, 
    std::vector<std::set <QString> >& theTypes, 
    std::vector<NCollection_Map <TopoDS_Face> >* theFaces) const;

  static Handle(Geom_Plane) GetPlane(const TopoDS_Face& F);


private:
  const HYDROData_LandCoverMap* const myLCM;

};


#endif

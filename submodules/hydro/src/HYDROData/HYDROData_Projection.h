// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Projection_HeaderFile
#define HYDROData_Projection_HeaderFile

#include <Standard_Real.hxx>
#include <IntCurvesFace_ShapeIntersector.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>

class TopoDS_Shape;
class TopoDS_Edge;
class TopoDS_Face;
class TopoDS_Wire;
class TopoDS_Vertex;
class gp_Pnt;

class HYDROData_Make3dMesh
{
public:
  HYDROData_Make3dMesh( const TopoDS_Shape& aShape,
                        const Standard_Real Tolerance );
  
  Standard_Boolean GetHighestOriginal(const Standard_Real aX,
                                      const Standard_Real aY,
                                      gp_Pnt& HighestPoint );
  
private:
  IntCurvesFace_ShapeIntersector myIntersector;
};

class HYDROData_Projection
{
public:
  static TopoDS_Edge FindClosestDirection( const TopoDS_Vertex& StartVertex,
                                           const TopTools_ListOfShape& Candidates,
                                           const gp_Vec& StartDir );
  static TopoDS_Wire BuildOutWire( const TopTools_IndexedDataMapOfShapeListOfShape& VEmap,
                                   const TopoDS_Vertex& StartVertex,
                                   TopoDS_Edge&   StartEdge );
  static TopoDS_Face MakeProjection( const TopoDS_Shape& aShape );
};

#endif

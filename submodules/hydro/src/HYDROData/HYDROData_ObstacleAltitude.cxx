// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_ObstacleAltitude.h"

#include "HYDROData_Document.h"
#include "HYDROData_Object.h"
#include "HYDROData_Projection.h"

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"
#include <QString>

#include <Precision.hxx>

#include <TopoDS_Shape.hxx>
#include <BRepTools.hxx>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_ObstacleAltitude, HYDROData_IAltitudeObject)

HYDROData_ObstacleAltitude::HYDROData_ObstacleAltitude()
: HYDROData_IAltitudeObject()
{
}

HYDROData_ObstacleAltitude::~HYDROData_ObstacleAltitude()
{
}

double HYDROData_ObstacleAltitude::GetAltitudeForPoint( const gp_XY& thePoint,
                                                        int theMethod) const
{
  DEBTRACE("HYDROData_ObstacleAltitude::GetAltitudeForPoint");
  double aResAltitude = GetInvalidAltitude();

  Handle(HYDROData_Object) anObject =
    Handle(HYDROData_Object)::DownCast( GetFatherObject() );
  if ( anObject.IsNull() )
  {
	DEBTRACE("anObject.IsNull()");
    return aResAltitude;
  }
//  DEBTRACE("object: " << anObject->GetName().toStdString());

  TopoDS_Shape anObjectShape3D = anObject->GetShape3D();
  if ( anObjectShape3D.IsNull() )
  {
	DEBTRACE("anObjectShape3D.IsNull()");
    return aResAltitude;
  }
//  else
//  {
//	DEBTRACE("anObjectShape3D type " << anObjectShape3D.ShapeType());
//	BRepTools::Write(anObjectShape3D, "digue3D.brep");
//  }

  HYDROData_Make3dMesh aMesher3D( anObjectShape3D, Precision::Intersection() );

  gp_Pnt aHighestPoint;
  if ( aMesher3D.GetHighestOriginal( thePoint.X(), thePoint.Y(), aHighestPoint ) )
    aResAltitude = aHighestPoint.Z();
  DEBTRACE("aResAltitude=" << aResAltitude);
  return aResAltitude;
}





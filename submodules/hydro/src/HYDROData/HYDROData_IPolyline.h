// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_IPolyline_HeaderFile
#define HYDROData_IPolyline_HeaderFile

#include "HYDROData_Entity.h"

class gp_XY;
class TopoDS_Shape;
class TDataStd_RealList;
class TDataStd_ExtStringList;
class TDataStd_BooleanList;
class TDataStd_IntegerList;
class Quantity_Color;


/**\class HYDROData_IPolyline
 * \brief Base class that stores/retreives information about the 2D points.
 */
class HYDROData_IPolyline : public HYDROData_Entity
{
public:
  enum SectionType{ SECTION_POLYLINE = 0, SECTION_SPLINE = 1 };

  typedef gp_XY                       Point;
  typedef NCollection_Sequence<Point> PointsList;

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Points,
    DataTag_Sections,
    DataTag_PolylineShape,
    DataTag_WireColor,  //DEPRECATED; DataTag_SectionColors for coloring of individual section of polyline
    DataTag_SectionColors,
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_IPolyline, HYDROData_Entity);

public:

  /**
   * Sets wire color for object.
   */
  HYDRODATA_EXPORT virtual void SetWireColor( const QColor& theColor );

  /**
   * Returns wire color of object.
   */
  HYDRODATA_EXPORT virtual QColor GetWireColor() const; //DEPRECATED

  /**
   * Returns default wire color for new object.
   */
  HYDRODATA_EXPORT static QColor DefaultWireColor();

public:
  /**
   * Returns number of sections.
   */
  HYDRODATA_EXPORT virtual int NbSections() const = 0;

  /**
   * Adds new one section.
   * \param theSectName name of the section
   * \param theSectionType type of section
   * \param theIsClosed flag indicates closures of section
   */
  HYDRODATA_EXPORT virtual void AddSection( const TCollection_AsciiString& theSectName,
                                            const SectionType              theSectionType,
                                            const bool                     theIsClosed ) = 0;

  /**
   * Returns name of section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual TCollection_AsciiString GetSectionName( const int theSectionIndex ) const = 0;

  /**
   * Set name for section with given index.
   * \param theSectionIndex index of section
   * \param theSectionName new section name
   */
  HYDRODATA_EXPORT virtual void SetSectionName( const int                      theSectionIndex, 
                                                const TCollection_AsciiString& theSectionName ) = 0;

  /**
   * Returns type of section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual SectionType GetSectionType( const int theSectionIndex ) const = 0;

  /** 
   * Set type for section with given index.
   * \param theSectionIndex index of section
   * \param theSectionType new section type
   */
  HYDRODATA_EXPORT virtual void SetSectionType( const int         theSectionIndex, 
                                                const SectionType theSectionType ) = 0;

  /**
   * Returns true if section with given index is closed.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual bool IsClosedSection( const int theSectionIndex ) const = 0;

  /**
   * Set closed flag for section with given index.
   * \param theSectionIndex index of section
   * \param theIsClosed new closures state
   */
  HYDRODATA_EXPORT virtual void SetSectionClosed( const int  theSectionIndex, 
                                                  const bool theIsClosed ) = 0;

  /**
   * Removes section with given index.
   * \param theSectionIndex index of section
   */
  HYDRODATA_EXPORT virtual void RemoveSection( const int theSectionIndex ) = 0;

  /**
   * Removes all sections.
   */
  HYDRODATA_EXPORT virtual void RemoveSections() = 0;


  /**
   * Return number of profile points.
   * \return number of points
   */
  HYDRODATA_EXPORT int NbPoints( const int theSectionIndex = -1 ) const;

  /**
   * Adds new point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoint point to add
   * \param theBeforeIndex if not equal -1 then insert point before this index
   */
  HYDRODATA_EXPORT virtual void AddPoint( const int    theSectionIndex,
                                          const Point& thePoint,
                                          const int    theBeforeIndex = -1 ) = 0;

  /**
   * Replaces point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoint new point
   * \param thePointIndex index of point to replace
   */
  HYDRODATA_EXPORT virtual void SetPoint( const int    theSectionIndex,
                                          const Point& thePoint,
                                          const int    thePointIndex ) = 0;

  /**
   * Removes point from section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePointIndex index of point
   */
  HYDRODATA_EXPORT virtual void RemovePoint( const int theSectionIndex,
                                             const int thePointIndex ) = 0;


  /**
   * Returns list of points.
   * \param theSectionIndex if not equal -1 then list of points returned
   *                        only for section with this index
   * \return list of points
   */
  HYDRODATA_EXPORT virtual PointsList GetPoints( const int theSectionIndex = -1, bool IsConvertToGlobal = false ) const = 0;

  HYDRODATA_EXPORT TopoDS_Shape GetShape() const;
  HYDRODATA_EXPORT void SetShape( const TopoDS_Shape& theShape );

  HYDRODATA_EXPORT virtual void SetSectionColor( const int theSectionIndex, const QColor& theColor );

  HYDRODATA_EXPORT virtual bool GetSectionColor( const int theSectionIndex, QColor &theColor) const;

  HYDRODATA_EXPORT void removeSectionColor( const int theSectionIndex = -1 ) const;

protected:
  void RemovePolylineShape();

  void getSectionsLists( Handle(TDataStd_ExtStringList)& theNamesList,
                         Handle(TDataStd_IntegerList)&   theTypesList,
                         Handle(TDataStd_BooleanList)&   theClosuresList,
                         const bool                      theIsCreate = true ) const;

  void removeSectionsLists();

  void getPointsLists( const int                  theSectionIndex,
                       Handle(TDataStd_RealList)& theListX,
                       Handle(TDataStd_RealList)& theListY,
                       const bool                 theIsCreate = true ) const;

  void removePointsLists( const int theSectionIndex = -1 ) const;

  void setPythonPolylineSectionColor( QStringList&  theScript,
                                      const int     theSectIndex,
                                      const QColor& theColor ) const;


protected:

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_IPolyline();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_IPolyline();
};

#endif

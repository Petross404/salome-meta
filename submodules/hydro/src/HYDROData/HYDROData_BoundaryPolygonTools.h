// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_BoundaryPolygonTools_HeaderFile
#define HYDROData_BoundaryPolygonTools_HeaderFile

#include "HYDROData.h"
#include <TopTools_SequenceOfShape.hxx>
#include <NCollection_IndexedDataMap.hxx>
#include <TopTools_ShapeMapHasher.hxx>
#include <TopTools_MapOfShape.hxx>

class TopoDS_Shape;

class HYDRODATA_EXPORT HYDROData_BoundaryPolygonTools 
{

public:

  static bool CutTool( const TopTools_SequenceOfShape& CutTools, NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape, TopTools_ShapeMapHasher>& ObjToRes,
                                              NCollection_IndexedDataMap<TopoDS_Shape, TopTools_MapOfShape, TopTools_ShapeMapHasher>& BPFaceToCutEdges );

  static bool IncludeTool( const TopTools_SequenceOfShape& IncludeTools, 
                                  TopoDS_Shape Obj);

  static bool SelectionTool( const TopTools_SequenceOfShape& SelectionTool, 
                                    TopoDS_Shape Obj) ;

};

#endif



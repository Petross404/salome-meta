// Copyright (C) 2007-2014  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  HYDRO HYDROData_QuadtreeNode : Quadtree with Nodes set (from SMESH module)
//  inherites class HYDROData_Quadtree
//  File      : HYDROData_QuadtreeNode.cxx
//  Created   : Tue Jan 16 16:00:00 2007
//  Author    : Nicolas Geimer & Aurelien Motteux (OCC)
//  Module    : HYDROData
//
#include "HYDROData_QuadtreeNode.hxx"

#include <gp_Pnt.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

using namespace std;

/*!
 * \brief Constructor : Build all the Quadtree using Compute()
 * \param theNodes - Set of nodes, the Quadtree is built from this nodes
 * \param maxLevel - Maximum level for the leaves
 * \param maxNbNodes - Maximum number of nodes, a leaf can contain
 * \param minBoxSize - Minimal size of the Quadtree Box
 */
HYDROData_QuadtreeNode::HYDROData_QuadtreeNode(Nodes_3D* theNodes,
                                               const int maxLevel,
                                               const int maxNbNodes,
                                               const double minBoxSize) :
    HYDROData_Quadtree(new Limit(maxLevel, minBoxSize, maxNbNodes)), myNodes(theNodes), myPrecision(0.25)
{
  //DEBTRACE("---------------------------- HYDROData_QuadtreeNode root constructor");
   if (myNodes)
    {
      //DEBTRACE(" --- start compute");
      compute();
      //DEBTRACE(" --- end compute");
    }
}

/*!
 * \brief Constructor used to allocate a child
 */
HYDROData_QuadtreeNode::HYDROData_QuadtreeNode() :
    HYDROData_Quadtree(), myNodes(0), myPrecision(0.25)
{
  myNodes = new Nodes_3D();
}

/*!
 * \brief virtual destructor: deletes myNodes
 */
HYDROData_QuadtreeNode::~HYDROData_QuadtreeNode()
{
  if (myNodes)
    {
      delete myNodes;
      myNodes = 0;
    }
}

/*!
 * \brief Set the nodes and compute the quadtree,
 * when the nodes are not given to the public constructor (delayed compute)
 */
void HYDROData_QuadtreeNode::setNodesAndCompute(Nodes_3D* theNodes)
{
  myNodes = theNodes;
  if (myNodes)
    {
      DEBTRACE(" --- start compute");
      compute();
      DEBTRACE(" --- end compute : children & height " << this->nbChildren() << " " << this->getHeight());
      DEBTRACE("Bounding box min: " << this->myBox->CornerMin().X() << " "  << this->myBox->CornerMin().Y());
      DEBTRACE("Bounding box max: " << this->myBox->CornerMax().X() << " "  << this->myBox->CornerMax().Y());
    }
}

/*!
 * \brief Return max number of nodes in a tree leaf
 */
int HYDROData_QuadtreeNode::getMaxNbNodes() const
{
  return ((Limit*) myLimit)->myMaxNbNodes;
}

/*!
 * \brief Construct an empty HYDROData_QuadtreeNode used by HYDROData_Quadtree::buildChildren()
 */
HYDROData_Quadtree* HYDROData_QuadtreeNode::newChild() const
{
  return new HYDROData_QuadtreeNode();
}

/*!
 * \brief Compute the first bounding box
 *
 * We take the max/min coord of the nodes
 */
Bnd_B2d* HYDROData_QuadtreeNode::buildRootBox()
{
  Bnd_B2d* box = new Bnd_B2d;
  Nodes_3D::iterator it = myNodes->begin();
  for (; it != myNodes->end(); it++)
    {
      const gpi_XYZ* n1 = *it;
      gp_XY p1(n1->X(), n1->Y());
      box->Add(p1);
    }
  if (myNodes->size() <= getMaxNbNodes())
    myIsLeaf = true;

  return box;
}

/*!
 * \brief Tells us if Node is inside the current box with the precision "precision"
 * \param Node - Node
 * \param precision - The box is enlarged with this precision
 * \retval bool - True if Node is in the box within precision
 */
const bool HYDROData_QuadtreeNode::isInside(const gp_XY& p, const double precision)
{
  if (precision <= 0.)
    return !(getBox()->IsOut(p));
  Bnd_B2d BoxWithPrecision = *getBox();
  BoxWithPrecision.Enlarge(precision);
  return !BoxWithPrecision.IsOut(p);
}

/*!
 * \brief Set the data of the children
 * Shares the father's data with each of his child
 */
void HYDROData_QuadtreeNode::buildChildrenData()
{
  gp_XY min = getBox()->CornerMin();
  gp_XY max = getBox()->CornerMax();
  gp_XY mid = (min + max) / 2.;

  //DEBTRACE("buildChildrenData level, min, max, nbNodes "<<level()<< " ("<< min.X()<<","<< min.Y()<<") ("<< max.X()<<","<< max.Y()<<") "<< myNodes->size());
  Nodes_3D::iterator it = myNodes->begin();
  while (it != myNodes->end())
    {
      gpi_XYZ* n1 = *it;
      int ChildBoxNum = getChildIndex(n1->X(), n1->Y(), mid);
      HYDROData_QuadtreeNode* myChild = dynamic_cast<HYDROData_QuadtreeNode*>(myChildren[ChildBoxNum]);
      myChild->myNodes->push_back(n1);
      myNodes->erase(it);
      it = myNodes->begin();
    }
  for (int i = 0; i < 4; i++)
    {
      HYDROData_QuadtreeNode* myChild = dynamic_cast<HYDROData_QuadtreeNode*>(myChildren[i]);
      if (myChild->myNodes->size() <= getMaxNbNodes())
        {
          myChild->myIsLeaf = true;
          //DEBTRACE("buildChildrenData leaf nbNodes "<< myChild->myNodes->size());
        }
    }
}

/*!
 * \brief Return in Result a list of Nodes potentials to be near Node
 * \param Node - Node
 * \param precision - precision used
 * \param Result - list of Nodes potentials to be near Node
 */
void HYDROData_QuadtreeNode::NodesAround(const gp_XY& Node,
                                         list<const gpi_XYZ*>* Result,
                                         const double precision)
{
  gp_XY p(Node);
  if (isInside(p, precision))
    {
      if (isLeaf())
        {
          Result->insert(Result->end(), myNodes->begin(), myNodes->end());
          //DEBTRACE("Leaf result "<< Result->size());
        }
      else
        {
          for (int i = 0; i < 4; i++)
            {
              HYDROData_QuadtreeNode* myChild = dynamic_cast<HYDROData_QuadtreeNode*>(myChildren[i]);
              myChild->NodesAround(p, Result, precision);
            }
        }
    }
}

/*!
 * \brief Return in dist2Nodes nodes mapped to their square distance from Node
 *  \param node - node to find nodes closest to
 *  \param dist2Nodes - map of found nodes and their distances
 *  \param precision - radius of a sphere to check nodes inside
 *  \retval bool - true if an exact overlapping found !!!
 */
bool HYDROData_QuadtreeNode::NodesAround(const gp_XY& node,
                                         map<double, const gpi_XYZ*>& dist2Nodes,
                                         double precision)
{
  if (!dist2Nodes.empty())
    precision = min(precision, sqrt(dist2Nodes.begin()->first));
  else if (precision == 0.)
    precision = maxSize() / 2;

  if (isInside(node, precision))
    {
      if (!isLeaf())
        {
          // first check a child containing node
          gp_XY mid = (getBox()->CornerMin() + getBox()->CornerMax()) / 2.;
          int nodeChild = getChildIndex(node.X(), node.Y(), mid);
          if (((HYDROData_QuadtreeNode*) myChildren[nodeChild])->NodesAround(node, dist2Nodes, precision))
            return true;

          for (int i = 0; i < 4; i++)
            if (i != nodeChild)
              if (((HYDROData_QuadtreeNode*) myChildren[i])->NodesAround(node, dist2Nodes, precision))
                return true;
        }
      else if (NbNodes() > 0)
        {
          double minDist = precision * precision;
          Nodes_3D::iterator nIt = myNodes->begin();
          for (; nIt != myNodes->end(); ++nIt)
            {
              const gpi_XYZ* p = *nIt;
              gp_XY p2(p->X(), p->Y());
              double dist2 = (node - p2).SquareModulus();
              if (dist2 < minDist)
                dist2Nodes.insert(make_pair(minDist = dist2, p));
            }
//       if ( dist2Nodes.size() > 1 ) // leave only closest node in dist2Nodes
//         dist2Nodes.erase( ++dist2Nodes.begin(), dist2Nodes.end());

// true if an exact overlapping found
          return (sqrt(minDist) <= precision * 1e-12);
        }
    }
  return false;
}

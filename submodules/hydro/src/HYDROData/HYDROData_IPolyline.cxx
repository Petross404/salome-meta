// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_IPolyline.h"

#include <gp_XY.hxx>

#include <TDataStd_BooleanList.hxx>
#include <TDataStd_ExtStringList.hxx>
#include <TDataStd_IntegerList.hxx>
#include <TDataStd_RealList.hxx>
#include <TDataStd_IntegerArray.hxx>
#include <TopoDS_Shape.hxx>
#include <QColor>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_IPolyline, HYDROData_Entity)

HYDROData_IPolyline::HYDROData_IPolyline()
: HYDROData_Entity( Geom_2d )
{
}

HYDROData_IPolyline::~HYDROData_IPolyline()
{
}

void HYDROData_IPolyline::SetWireColor( const QColor& theColor )
{
  //SetColor( theColor, DataTag_WireColor ); //DEPRECATED;
  //set theColor for all sections
  int nbSec = NbSections();
  for (int i = 0; i < nbSec; i++)
    SetSectionColor(i, theColor);
}

QColor HYDROData_IPolyline::GetWireColor() const
{
  return GetColor( DefaultWireColor(), DataTag_WireColor );
}

QColor HYDROData_IPolyline::DefaultWireColor()
{
  return QColor( Qt::black );
}

int HYDROData_IPolyline::NbPoints( const int theSectionIndex ) const
{
  return GetPoints( theSectionIndex ).Length();
}

TopoDS_Shape HYDROData_IPolyline::GetShape() const
{
  return HYDROData_Entity::GetShape( DataTag_PolylineShape );
}

void HYDROData_IPolyline::SetShape( const TopoDS_Shape& theShape )
{
  HYDROData_Entity::SetShape( DataTag_PolylineShape, theShape );
}

void HYDROData_IPolyline::RemovePolylineShape()
{
  SetShape( TopoDS_Shape() );
}

void HYDROData_IPolyline::getSectionsLists( Handle(TDataStd_ExtStringList)& theNamesList,
                                            Handle(TDataStd_IntegerList)&   theTypesList,
                                            Handle(TDataStd_BooleanList)&   theClosuresList,
                                            const bool                      theIsCreate ) const
{
  theNamesList.Nullify();
  theTypesList.Nullify();
  theClosuresList.Nullify();

  TDF_Label aSectLabel = myLab.FindChild( DataTag_Sections, theIsCreate );
  if ( aSectLabel.IsNull() )
    return;

  if ( !aSectLabel.FindAttribute( TDataStd_ExtStringList::GetID(), theNamesList ) && theIsCreate )
  {
    theNamesList = TDataStd_ExtStringList::Set( aSectLabel );
    theNamesList->SetID(TDataStd_ExtStringList::GetID());
  }

  if ( !aSectLabel.FindAttribute( TDataStd_IntegerList::GetID(), theTypesList ) && theIsCreate )
  {
    theTypesList = TDataStd_IntegerList::Set( aSectLabel );
    theTypesList->SetID(TDataStd_IntegerList::GetID());
  }

  if ( !aSectLabel.FindAttribute( TDataStd_BooleanList::GetID(), theClosuresList ) && theIsCreate )
  {
    theClosuresList = TDataStd_BooleanList::Set( aSectLabel );
    theClosuresList->SetID(TDataStd_BooleanList::GetID());
  }
}

void HYDROData_IPolyline::removeSectionsLists()
{
  TDF_Label aSectsLabel = myLab.FindChild( DataTag_Sections, false );
  if ( !aSectsLabel.IsNull() )
    aSectsLabel.ForgetAllAttributes();
}

void HYDROData_IPolyline::getPointsLists( const int                  theSectionIndex,
                                          Handle(TDataStd_RealList)& theListX,
                                          Handle(TDataStd_RealList)& theListY,
                                          const bool                 theIsCreate ) const
{
  theListX.Nullify();
  theListY.Nullify();

  TDF_Label aLabel = myLab.FindChild( DataTag_Points, theIsCreate );
  if ( aLabel.IsNull() )
    return;

  TDF_Label aSectLabel = aLabel.FindChild( theSectionIndex, theIsCreate );
  if ( aSectLabel.IsNull() )
    return;

  TDF_Label aLabelX = aSectLabel.FindChild( 0, theIsCreate );
  if ( !aLabelX.IsNull() )
  {
    if ( !aLabelX.FindAttribute( TDataStd_RealList::GetID(), theListX ) && theIsCreate )
    {
      theListX = TDataStd_RealList::Set( aLabelX );
      theListX->SetID(TDataStd_RealList::GetID());
    }
  }

  TDF_Label aLabelY = aSectLabel.FindChild( 1, theIsCreate );
  if ( !aLabelY.IsNull() )
  {
    if ( !aLabelY.FindAttribute( TDataStd_RealList::GetID(), theListY ) && theIsCreate )
    {
      theListY = TDataStd_RealList::Set( aLabelY );
      theListY->SetID(TDataStd_RealList::GetID());
    }
  }
}

void HYDROData_IPolyline::removePointsLists( const int theSectionIndex ) const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_Points, false );
  if ( aLabel.IsNull() )
    return;

  if ( theSectionIndex < 0 )
  {
    aLabel.ForgetAllAttributes();
  }
  else
  {
    TDF_Label aSectLabel = aLabel.FindChild( theSectionIndex, false );
    if ( !aSectLabel.IsNull() )
      aSectLabel.ForgetAllAttributes();
  }
}

bool HYDROData_IPolyline::GetSectionColor( const int theSectionIndex, QColor &theColor) const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_SectionColors, false );
  if ( aLabel.IsNull() )
    return false;

  TDF_Label aSectLabel = aLabel.FindChild( theSectionIndex, false );
  if ( aSectLabel.IsNull() )
    return false;

  Handle(TDataStd_IntegerArray) aColorArray;

  if ( aSectLabel.FindAttribute( TDataStd_IntegerArray::GetID(), aColorArray ) )
  {
    theColor.setRed(   aColorArray->Value( 1 ) );
    theColor.setGreen( aColorArray->Value( 2 ) );
    theColor.setBlue(  aColorArray->Value( 3 ) );
    theColor.setAlpha( aColorArray->Value( 4 ) );
  }
}

void HYDROData_IPolyline::SetSectionColor( const int theSectionIndex,
                                           const QColor& theColor )
{
  TDF_Label aLabel = myLab.FindChild( DataTag_SectionColors, true );
  if ( aLabel.IsNull() )
    return;

  TDF_Label aSectLabel = aLabel.FindChild( theSectionIndex, true );
  if ( aSectLabel.IsNull() )
    return;

  Handle(TDataStd_IntegerArray) aColorArray;

  if ( !aSectLabel.FindAttribute( TDataStd_IntegerArray::GetID(), aColorArray ) )
    aColorArray = TDataStd_IntegerArray::Set( aSectLabel, 1, 4 );

  aColorArray->SetValue( 1, theColor.red()   );
  aColorArray->SetValue( 2, theColor.green() );
  aColorArray->SetValue( 3, theColor.blue()  );
  aColorArray->SetValue( 4, theColor.alpha() );
}

void HYDROData_IPolyline::removeSectionColor( const int theSectionIndex ) const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_SectionColors, false );
  if ( aLabel.IsNull() )
    return;

  if ( theSectionIndex < 0 )
  {
    aLabel.ForgetAllAttributes();
  }
  else
  {
    TDF_Label aSectLabel = aLabel.FindChild( theSectionIndex, false );
    if ( !aSectLabel.IsNull() )
      aSectLabel.ForgetAllAttributes();
  }
}

 void HYDROData_IPolyline::setPythonPolylineSectionColor( QStringList&  theScript,
                                                          const int     theSectIndex,
                                                          const QColor& theColor ) const
{
  QString anObjName = GetObjPyName();
  theScript << QString( "%1.SetSectionColor( %2, QColor( %3, %4, %5, %6 ) )" )
              .arg( anObjName ).arg( theSectIndex )
              .arg( theColor.red()  ).arg( theColor.green() )
              .arg( theColor.blue() ).arg( theColor.alpha() );
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Image.h"

#include "HYDROData_Document.h"
#include "HYDROData_Lambert93.h"
#include "HYDROData_OperationsFactory.h"
#include "HYDROData_Tool.h"

#include <TDataStd_RealArray.hxx>
#include <TDataStd_ByteArray.hxx>
#include <TDataStd_Integer.hxx>
#include <TDataStd_IntegerArray.hxx>
#include <TDataStd_ReferenceList.hxx>
#include <TDataStd_UAttribute.hxx>
#include <TDataStd_AsciiString.hxx>

#include <NCSDefs.h>
#include <NCSFile.h>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#include <ImageComposer_Operator.h>
#include <ImageComposer_MetaTypes.h>

#include <QStringList>
#include <QFile>
#include <QFileInfo>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

static const Standard_GUID GUID_SELF_SPLIT("997995aa-5c19-40bf-9a60-ab4b70ad04d8");
static const Standard_GUID GUID_HAS_LOCAL_POINTS("FD8841AA-FC44-42fa-B6A7-0F682CCC6F27");
static const Standard_GUID GUID_HAS_GLOBAL_POINTS("330D0E81-742D-4ea3-92D4-484877CFA7C1");

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_Image, HYDROData_Entity)

HYDROData_Image::HYDROData_Image()
: HYDROData_Entity( Geom_2d )
{
}

HYDROData_Image::~HYDROData_Image()
{
}

QStringList HYDROData_Image::DumpToPython( const QString&       thePyScriptPath,
                                           MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  QString anImageName = GetObjPyName();

  QString aFilePath = GetFilePath();
  if ( !aFilePath.isEmpty() )
  {
    aResList << QString( "" );
    aResList << QString( "if not(%1.LoadImage( \"%2\" )):" )
                .arg( anImageName ).arg( aFilePath );
    aResList << QString( "  raise ValueError('problem while loading image')" );
    aResList << QString( "" );

    // Dump transformation points for image

    bool anIsByTwoPoints = IsByTwoPoints();

    QPoint aLocalPointA, aLocalPointB, aLocalPointC;
    if ( GetLocalPoints( aLocalPointA, aLocalPointB, aLocalPointC ) )
    {
      QString aGap = QString().fill( ' ', anImageName.size() + 17 );

      aResList << QString( "%1.SetLocalPoints( QPoint( %2, %3 )," )
                  .arg( anImageName ).arg( aLocalPointA.x() ).arg( aLocalPointA.y() );
      aResList << QString( aGap             + "QPoint( %1, %2 )" )
                  .arg( aLocalPointB.x() ).arg( aLocalPointB.y() );
      if ( !anIsByTwoPoints )
      {
        aResList.last().append( "," );
        aResList << QString( aGap             +  "QPoint( %1, %2 ) )" )
                    .arg( aLocalPointC.x() ).arg( aLocalPointC.y() );
      }
      else
      {
        aResList.last().append( " )" );
      }
      aResList << QString( "" );
    }

    HYDROData_Image::TransformationMode aTransformationMode;
    QPointF aTrsfPointA, aTrsfPointB, aTrsfPointC;
    if ( GetGlobalPoints( aTransformationMode, aTrsfPointA, aTrsfPointB, aTrsfPointC ) )
    {
      QString aGap = QString().fill( ' ', anImageName.size() + 18 );

      aResList << QString( "%1.SetGlobalPoints( %2," )
                  .arg( anImageName ).arg( aTransformationMode );
      aResList << QString( aGap             +  "QPointF( %1, %2 )," )
                  .arg( aTrsfPointA.x(), 0, 'f', 3  ).arg( aTrsfPointA.y(), 0, 'f', 3  );
      aResList << QString( aGap             +  "QPointF( %1, %2 )" )
                  .arg( aTrsfPointB.x(), 0, 'f', 3  ).arg( aTrsfPointB.y(), 0, 'f', 3  );
      if ( !anIsByTwoPoints )
      {
        aResList.last().append( "," );
        aResList << QString( aGap             +  "QPointF( %1, %2 ) )" )
                    .arg( aTrsfPointC.x(), 0, 'f', 3  ).arg( aTrsfPointC.y(), 0, 'f', 3  );
      }
      else
      {
        aResList.last().append( " )" );
      }

      if ( aTransformationMode == ReferenceImage )
      {
        Handle(HYDROData_Image) aRefImg = GetTrsfReferenceImage();
        setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefImg, "SetTrsfReferenceImage" );
      }
    }
  }
  else
  {
    // Image is composed from other image(s)

    QString anOperatorName = OperatorName();
    if ( !anOperatorName.isEmpty() )
    {
      aResList << QString( "" );

      aResList << QString( "%1.SetOperatorName( \"%2\" )" )
                  .arg( anImageName ).arg( anOperatorName );

      ImageComposer_Operator* anImageOp = 
        HYDROData_OperationsFactory::Factory()->Operator( OperatorName() );
      if ( anImageOp )
      {
        // Dump operation arguments
        QString anOpArgsArrayName;
        QStringList anOpArgs = anImageOp->dumpArgsToPython( anOpArgsArrayName );
        if ( !anOpArgs.isEmpty() )
        {
          aResList << QString( "" );
          aResList << anOpArgs;

          aResList << QString( "" );
          aResList << QString( "%1.SetArgs( %2 )" )
                      .arg( anImageName ).arg( anOpArgsArrayName );
        }
      }
    }
    
    int aNbReferences = NbReferences();
    if ( aNbReferences > 0 )
    {
      aResList << QString( "" );

      for ( int i = 0; i < aNbReferences; ++i )
      {
        Handle(HYDROData_Image) aRefImg = Handle(HYDROData_Image)::DownCast( Reference( i ) );
        setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefImg, "AppendReference" );
      }
    }
  }

  aResList << QString( "" );
  aResList << QString( "%1.Update()" ).arg( anImageName );

  return aResList;
}

void HYDROData_Image::Update()
{
  bool anIsToUpdate = IsMustBeUpdated( Geom_2d );

  HYDROData_Entity::Update();

  if ( !anIsToUpdate )
    return;

  HYDROData_OperationsFactory* aFactory = HYDROData_OperationsFactory::Factory();

  ImageComposer_Operator* anOp = aFactory->Operator( OperatorName() );
  if ( anOp ) // Update image if there is an operation
  {
    // Fill by arguments and process the operation
    anOp->setBinArgs( Args() );

    QVariant anObj1, anObj2;
    int aNbReferences = NbReferences();

    if ( aNbReferences > 0 )
    {
      // First referenced object
      Handle(HYDROData_Entity) aRefObj = Reference( 0 );
      if ( !aRefObj.IsNull() )
      {
        anObj1 = aRefObj->GetDataVariant();
        if ( !anObj1.isNull() && anObj1.canConvert<ImageComposer_Image>() )
        {
          ImageComposer_Image anImage = anObj1.value<ImageComposer_Image>();
          QTransform aTransform = anImage.transform();
          SetTrsf( aTransform );
        }
      }
    }

    if ( aNbReferences > 1 )
    {
      // Second referenced object
      Handle(HYDROData_Entity) aRefObj = Reference( 1 );
      if ( !aRefObj.IsNull() )
        anObj2 = aRefObj->GetDataVariant();
    }

    ImageComposer_Image aResImg = anOp->process( anObj1, anObj2 );
    SetImage( aResImg );
    SetTrsf( aResImg.transform() );
  }
  else // Update image if it positioned relatively to other image
  {
    UpdateTrsf();
  }

  ClearChanged();
}

bool HYDROData_Image::IsHas2dPrs() const
{
  return true;
}

QVariant HYDROData_Image::GetDataVariant()
{
  QTransform aTransform = Trsf();

  ImageComposer_Image anImage = Image();
  anImage.setTransform( aTransform );

  QVariant aVarData;
  aVarData.setValue<ImageComposer_Image>( anImage );
  
  return aVarData;
}

HYDROData_SequenceOfObjects HYDROData_Image::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Entity::GetAllReferenceObjects();

  Handle(HYDROData_Image) aRefImage = GetTrsfReferenceImage();
  if ( !aRefImage.IsNull() )
    aResSeq.Append( aRefImage );

  HYDROData_SequenceOfObjects aSeqRefObjects = GetReferenceObjects( 0 );
  aResSeq.Append( aSeqRefObjects );

  return aResSeq;
}

void HYDROData_Image::SetImage(const QImage& theImage)
{
  if ( theImage.isNull() )
  {
    // for empty image remove all previously stored attributes
    myLab.ForgetAttribute(TDataStd_IntegerArray::GetID());
    myLab.ForgetAttribute(TDataStd_ByteArray::GetID());
  }
  else
  {
    QImage anImage;

    // convert 8-bits images
    if ( theImage.format() == QImage::Format_Indexed8 ) {
      anImage = theImage.convertToFormat( QImage::Format_RGB32 );
    } else {
      anImage = theImage;
    }

    // store width, height, bytes per line and format in integer array 
    Handle(TDataStd_IntegerArray) aParams;
    if (!myLab.FindAttribute(TDataStd_IntegerArray::GetID(), aParams)) {
      aParams = TDataStd_IntegerArray::Set(myLab, 1, 4);
      aParams->SetID(TDataStd_IntegerArray::GetID());
    }
    aParams->SetValue(1, anImage.width());
    aParams->SetValue(2, anImage.height());
    aParams->SetValue(3, anImage.bytesPerLine());
    aParams->SetValue(4, (int)(anImage.format()));
    // store data of image in byte array
    const char* aData = (const char*)(anImage.bits());
    SaveByteArray(0, aData, anImage.byteCount());
  }

  Changed( Geom_2d );
}

bool HYDROData_Image::LoadImage( const QString& theFilePath )
{
  QFileInfo aFI(theFilePath);
  QImage anImage;
  HYDROData_Image::ECW_FileInfo* theECWInfo;
  if (aFI.suffix().toLower() == "ecw")
  {
    theECWInfo = new HYDROData_Image::ECW_FileInfo;
    HYDROData_Image::OpenECW(theFilePath.toLatin1().data(), anImage, theECWInfo);
  }
  else
    anImage = QImage(theFilePath);
  SetImage( anImage );
  SetFilePath( theFilePath );
  return !anImage.isNull();
}

bool HYDROData_Image::OpenECW(char* theFileName, QImage& theImage, ECW_FileInfo* theECWInfo)
{
  NCSFileView *pNCSFileView;
  NCSFileViewFileInfo *pNCSFileInfo;
  NCSError eError = NCS_SUCCESS;
  UINT32 band, nBands;
  UINT32 XSize, YSize;
  NCSecwInit();

  eError = NCScbmOpenFileView(theFileName, &pNCSFileView, NULL);
  if(eError != NCS_SUCCESS)
    return false;

  eError = NCScbmGetViewFileInfo(pNCSFileView, &pNCSFileInfo);
  if(eError != NCS_SUCCESS)
    return false;

  XSize = pNCSFileInfo->nSizeX;
  YSize = pNCSFileInfo->nSizeY;
  nBands = pNCSFileInfo->nBands;
  if (theECWInfo)
  {
    //ECW_CellUnits myCellSizeUnits;
    CellSizeUnits aCellUnits = pNCSFileInfo->eCellSizeUnits;
    if (aCellUnits == ECW_CELL_UNITS_METERS)
      theECWInfo->myCellSizeUnits = ECW_CellUnits_Meters;
    else if (aCellUnits == ECW_CELL_UNITS_DEGREES)
      theECWInfo->myCellSizeUnits = ECW_CellUnits_Deg;
    else if (aCellUnits == ECW_CELL_UNITS_FEET)
      theECWInfo->myCellSizeUnits = ECW_CellUnits_Feet;
    else
      theECWInfo->myCellSizeUnits = ECW_CellUnits_Unknown;
    theECWInfo->myCellIncrementX = pNCSFileInfo->fCellIncrementX;
    theECWInfo->myCellIncrementY = pNCSFileInfo->fCellIncrementY;
    theECWInfo->myOriginX = pNCSFileInfo->fOriginX;
    theECWInfo->myOriginY = pNCSFileInfo->fOriginY;
    theECWInfo->myXSize = pNCSFileInfo->nSizeX;
    theECWInfo->myYSize = pNCSFileInfo->nSizeY;
  }

  std::vector<UINT32> band_list(nBands);
  for( band = 0; band < nBands; band++ )
    band_list[band] = band;

  eError = NCScbmSetFileView(pNCSFileView, nBands, &band_list[0], 0, 0, XSize - 1, YSize - 1, XSize, YSize); //view an image into the original size

  if(eError != NCS_SUCCESS)
  {
    NCScbmCloseFileView(pNCSFileView);
    return false;
  }

  UINT8 *pRGBTriplets;
  pRGBTriplets = (UINT8 *) malloc(XSize*3);

  QImage anImage(XSize, YSize, QImage::Format_RGB32);

  for(UINT32 line = 0; line < YSize; line++)
  {
    NCSEcwReadStatus eStatus;
    eStatus = NCScbmReadViewLineRGB(pNCSFileView, pRGBTriplets);
    if(eStatus == NCSECW_READ_OK)
    {
      QRgb* crp = (QRgb*)anImage.scanLine(line);  
      for(UINT32 j = 0; j < XSize; j++)
      {
        QRgb val = qRgb((int)pRGBTriplets[j*3],(int)pRGBTriplets[j*3+1],(int)pRGBTriplets[j*3+2]);
        memcpy((void*)(crp+j), &val, sizeof(QRgb)); 
      }
    }
    else    
    {
      free(pRGBTriplets);
      NCScbmCloseFileView(pNCSFileView);
      return false;
    }
  }

  free(pRGBTriplets);
  NCScbmCloseFileView(pNCSFileView);
  theImage = anImage;
  return true;
}

bool HYDROData_Image::LoadImageECW( const QString& theFilePath )
{
  QImage anImage;
  if (HYDROData_Image::OpenECW(theFilePath.toLatin1().data(), anImage, NULL))
  {
    SetImage( anImage );
    SetFilePath( theFilePath );
  }
  return !anImage.isNull();
}

QImage HYDROData_Image::Image()
{
  Handle(TDataStd_IntegerArray) aParams;
  if (!myLab.FindAttribute(TDataStd_IntegerArray::GetID(), aParams))
    return QImage(); // return empty image if there is no array
  int aLen = 0;
  uchar* anArray = (uchar*)ByteArray(0, aLen);
  if (!aLen)
    return QImage(); // return empty image if there is no array
  QImage aResult(anArray, aParams->Value(1), aParams->Value(2),
                 aParams->Value(3), QImage::Format(aParams->Value(4)));
  return aResult;
}

void HYDROData_Image::SetFilePath( const QString& theFilePath )
{
  TCollection_AsciiString anAsciiStr( theFilePath.toStdString().c_str() );
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_FilePath ), anAsciiStr );
  anAttr->SetID(TDataStd_AsciiString::GetID());
  Changed( Geom_2d );
}

QString HYDROData_Image::GetFilePath() const
{
  QString aRes;

  TDF_Label aLabel = myLab.FindChild( DataTag_FilePath, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
      aRes = QString( anAsciiStr->Get().ToCString() );
  }

  return aRes;
}

void HYDROData_Image::SetTrsf(const QTransform& theTrsf)
{
  // locate 9 coeffs of matrix into the real array
  Handle(TDataStd_RealArray) anArray;
  if (!myLab.FindAttribute(TDataStd_RealArray::GetID(), anArray)) {
    if (theTrsf.isIdentity()) return; // no need to store identity transformation
    anArray = TDataStd_RealArray::Set(myLab, 1, 9);
    anArray->SetID(TDataStd_RealArray::GetID());
  }
  anArray->SetValue(1, theTrsf.m11());
  anArray->SetValue(2, theTrsf.m12());
  anArray->SetValue(3, theTrsf.m13());
  anArray->SetValue(4, theTrsf.m21());
  anArray->SetValue(5, theTrsf.m22());
  anArray->SetValue(6, theTrsf.m23());
  anArray->SetValue(7, theTrsf.m31());
  anArray->SetValue(8, theTrsf.m32());
  anArray->SetValue(9, theTrsf.m33());

  Changed( Geom_2d );
}

QTransform HYDROData_Image::Trsf() const
{
  // get 9 coeffs of matrix from the real array
  Handle(TDataStd_RealArray) anArray;
  if (!myLab.FindAttribute(TDataStd_RealArray::GetID(), anArray))
    return QTransform(); // return identity if there is no array
  QTransform aTrsf(
    anArray->Value(1), anArray->Value(2), anArray->Value(3), 
    anArray->Value(4), anArray->Value(5), anArray->Value(6), 
    anArray->Value(7), anArray->Value(8), anArray->Value(9));
  return aTrsf;
}

void HYDROData_Image::UpdateTrsf()
{
  QPoint aPointA, aPointB, aPointC;
  if ( !GetLocalPoints( aPointA, aPointB, aPointC ) )
    return;

  TransformationMode aTrsfMode;
  QPointF aTrsfPointA, aTrsfPointB, aTrsfPointC;
  if ( !GetGlobalPoints( aTrsfMode, aTrsfPointA, aTrsfPointB, aTrsfPointC ) )
    return;

  QTransform aRefTransform;
  Handle(HYDROData_Image) aRefImage = GetTrsfReferenceImage();

  bool anIsRefImage = aTrsfMode == ReferenceImage;
  if ( anIsRefImage )
  {
    if ( aRefImage.IsNull() )
      return;

    aRefTransform = aRefImage->Trsf();
  }

  bool anIsByTwoPoints = IsByTwoPoints();

  // Convert lambert coordinates to cartesian
  if ( aTrsfMode == ManualGeodesic )
  {
    double aXCart = 0, aYCart = 0;

    HYDROData_Lambert93::toXY( aTrsfPointA.y(), aTrsfPointA.x(), aXCart, aYCart );
    aTrsfPointA = QPointF( aXCart, aYCart );

    HYDROData_Lambert93::toXY( aTrsfPointB.y(), aTrsfPointB.x(), aXCart, aYCart );
    aTrsfPointB = QPointF( aXCart, aYCart );

    if ( !anIsByTwoPoints )
    {
      HYDROData_Lambert93::toXY( aTrsfPointC.y(), aTrsfPointC.x(), aXCart, aYCart );
      aTrsfPointC = QPointF( aXCart, aYCart );
    }
  }

  // generate third points if needed
  if ( anIsByTwoPoints )
  {
    aPointC = generateThirdPoint( aPointA, aPointB, true ).toPoint();
    aTrsfPointC = generateThirdPoint( aTrsfPointA, aTrsfPointB, anIsRefImage );
  }

  int xa = aPointA.x();
  int ya = aPointA.y();
  int xb = aPointB.x();
  int yb = aPointB.y();
  int xc = aPointC.x();
  int yc = aPointC.y();

  double xta = aTrsfPointA.x();
  double yta = aTrsfPointA.y();
  double xtb = aTrsfPointB.x();
  double ytb = aTrsfPointB.y();
  double xtc = aTrsfPointC.x();
  double ytc = aTrsfPointC.y();

  // first, check that three input points don't belong to a single line
  if( ( yb - ya ) * ( xc - xa ) == ( yc - ya ) * ( xb - xa ) )
    return;

  // the same check for the reference points
  if( anIsRefImage && ( ( ytb - yta ) * ( xtc - xta ) == ( ytc - yta ) * ( xtb - xta ) ) )
    return;

  QTransform aTransform1( xa, ya, 1, xb, yb, 1, xc, yc, 1 );
  QTransform aTransform2( xta, yta, 1, xtb, ytb, 1, xtc, ytc, 1 );

  bool anIsInvertible = false;
  QTransform aTransform1Inverted = aTransform1.inverted( &anIsInvertible );
  if( !anIsInvertible )
    return;

  QTransform aResTransform = aTransform1Inverted * aTransform2;
  if( anIsRefImage )
    aResTransform *= aRefTransform;

  SetTrsf( aResTransform );
}

bool HYDROData_Image::IsByTwoPoints() const
{
  if ( !HasLocalPoints() || !HasGlobalPoints() )
    return false;

  QPoint aPointA, aPointB, aPointC;
  GetLocalPoints( aPointA, aPointB, aPointC );

  return aPointC.x() < 0 && aPointC.y() < 0; 
}

bool HYDROData_Image::HasReferences() const
{
  Handle(HYDROData_Image) aRefImage = GetTrsfReferenceImage();
  int aNbReferences = NbReferences();

  return !aRefImage.IsNull() || aNbReferences > 0;
}

void HYDROData_Image::RemoveAllReferences()
{
  if ( !HasReferences() )
    return;

  Handle(HYDROData_Image) aRefImage = GetTrsfReferenceImage();
  if ( !aRefImage.IsNull() )
  {
    RemoveTrsfReferenceImage();
  }
  else
  {
    ClearReferences();
    SetOperatorName( "" );
    SetArgs( "" );
    SetIsSelfSplit( false );
  }

  bool anIsByTwoPoints = IsByTwoPoints();

  QImage anImage = Image();
  if ( anImage.isNull() )
  {
    ClearChanged();
    return;
  }

  // Set local points to default position
  QPoint aLocalPointA = QPoint( 0, 0 );
  QPoint aLocalPointB = QPoint( anImage.width(), 0 );
  QPoint aLocalPointC = anIsByTwoPoints ? QPoint( INT_MIN, INT_MIN ) : QPoint( 0, anImage.height() );

  SetLocalPoints( aLocalPointA, aLocalPointB, aLocalPointC, false );

  // Calculate global points
  QTransform aTransform = Trsf();

  QPointF aTrsfPointA = QPointF( aTransform.map( aLocalPointA ) );
  QPointF aTrsfPointB = QPointF( aTransform.map( aLocalPointB ) );
  QPointF aTrsfPointC = anIsByTwoPoints ? QPointF( INT_MIN, INT_MIN ) : 
                                          QPointF( aTransform.map( aLocalPointC ) );

  SetGlobalPoints( ManualCartesian, aTrsfPointA, aTrsfPointB, aTrsfPointC );

  ClearChanged();
}

void HYDROData_Image::SetLocalPoints( const QPoint& thePointA,
                                      const QPoint& thePointB,
                                      const QPoint& thePointC,
                                      const bool    theIsUpdate )
{
  Handle(TDataStd_RealArray) anArray;
  if ( !myLab.FindChild( DataTag_TrsfPoints ).FindAttribute( TDataStd_RealArray::GetID(), anArray ) )
  {
    anArray = TDataStd_RealArray::Set( myLab.FindChild( DataTag_TrsfPoints ), 1, 12 );
    anArray->SetID(TDataStd_RealArray::GetID());
  }

  anArray->SetValue( 1, thePointA.x() );
  anArray->SetValue( 2, thePointA.y() );
  anArray->SetValue( 3, thePointB.x() );
  anArray->SetValue( 4, thePointB.y() );
  anArray->SetValue( 5, thePointC.x() );
  anArray->SetValue( 6, thePointC.y() );

  TDataStd_UAttribute::Set( myLab.FindChild( DataTag_TrsfPoints ), GUID_HAS_LOCAL_POINTS );

  if ( theIsUpdate )
    UpdateTrsf();

  Changed( Geom_2d );
}

bool HYDROData_Image::GetLocalPoints( QPoint& thePointA,
                                      QPoint& thePointB,
                                      QPoint& thePointC ) const
{
  if ( !HasLocalPoints() )
    return false;

  Handle(TDataStd_RealArray) anArray;
  myLab.FindChild( DataTag_TrsfPoints ).FindAttribute( TDataStd_RealArray::GetID(), anArray );

  thePointA = QPointF( anArray->Value( 1 ), anArray->Value( 2 ) ).toPoint();
  thePointB = QPointF( anArray->Value( 3 ), anArray->Value( 4 ) ).toPoint();
  thePointC = QPointF( anArray->Value( 5 ), anArray->Value( 6 ) ).toPoint();

  return true;
}

bool HYDROData_Image::HasLocalPoints() const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_TrsfPoints, false );
  if ( aLabel.IsNull() || !aLabel.IsAttribute( GUID_HAS_LOCAL_POINTS ) )
    return false;

  Handle(TDataStd_RealArray) anArray;
  return aLabel.FindAttribute( TDataStd_RealArray::GetID(), anArray );
}


void HYDROData_Image::SetGlobalPoints( const TransformationMode& theMode,
                                       const QPointF&            thePointA,
                                       const QPointF&            thePointB,
                                       const QPointF&            thePointC,
                                       const bool                theIsUpdate )
{
  Handle(TDataStd_RealArray) anArray;
  if ( !myLab.FindChild( DataTag_TrsfPoints ).FindAttribute( TDataStd_RealArray::GetID(), anArray ) )
  {
    anArray = TDataStd_RealArray::Set( myLab.FindChild( DataTag_TrsfPoints ), 1, 12 );
    anArray->SetID(TDataStd_RealArray::GetID());
  }

  anArray->SetValue( 7,  thePointA.x() );
  anArray->SetValue( 8,  thePointA.y() );
  anArray->SetValue( 9,  thePointB.x() );
  anArray->SetValue( 10, thePointB.y() );
  anArray->SetValue( 11, thePointC.x() );
  anArray->SetValue( 12, thePointC.y() );
  
  SetTrsfMode( theMode );

  TDataStd_UAttribute::Set( myLab.FindChild( DataTag_TrsfPoints ), GUID_HAS_GLOBAL_POINTS );

  if ( theIsUpdate )
    UpdateTrsf();

  Changed( Geom_2d );
}

bool HYDROData_Image::GetGlobalPoints( TransformationMode& theMode,
                                       QPointF&            thePointA,
                                       QPointF&            thePointB,
                                       QPointF&            thePointC ) const
{
  if ( !HasGlobalPoints() )
    return false;

  theMode = GetTrsfMode();

  Handle(TDataStd_RealArray) anArray;
  myLab.FindChild( DataTag_TrsfPoints ).FindAttribute( TDataStd_RealArray::GetID(), anArray );

  thePointA = QPointF( anArray->Value( 7  ), anArray->Value( 8  ) );
  thePointB = QPointF( anArray->Value( 9  ), anArray->Value( 10 ) );
  thePointC = QPointF( anArray->Value( 11 ), anArray->Value( 12 ) );

  return true;
}

bool HYDROData_Image::SetGlobalPointsFromFile( const QString& theFileName )
{
  bool aRes = false;

  // Try to open the file
  QFile aFile( theFileName );
  if ( !aFile.exists() || !aFile.open( QIODevice::ReadOnly ) ) {
    return aRes;
  }

  QPointF aPointA, aPointB;
  double aXmin, anYmin, aXmax, anYmax;
  aXmin = anYmin = aXmax = anYmax = -1;

  while ( !aFile.atEnd() && 
          ( aXmin < 0 || anYmin < 0 || aXmax < 0 || anYmax < 0 ) ) {
    // Read line
    QString aLine = aFile.readLine().simplified();
    aLine.replace( " ", "" );
    if ( aLine.isEmpty() ) {
      continue;
    }

    // Try to read double value after ":"
    bool isDoubleOk = false;
    double aDoubleValue = -1;
    QStringList aValues = aLine.split( ":", QString::SkipEmptyParts );
    if ( aValues.count() == 2 ) {
      aDoubleValue = aValues.last().toDouble( &isDoubleOk );
    }

    // Check the result
    if ( !isDoubleOk ||
         HYDROData_Tool::IsNan( aDoubleValue ) ||
         HYDROData_Tool::IsInf( aDoubleValue ) ) {
      continue;
    }

    // Set the value
    if ( aLine.startsWith( "Xminimum" ) ) {
      aXmin = aDoubleValue;    
    } 
    else if ( aLine.startsWith( "Yminimum" ) ) {
      anYmin = aDoubleValue; 
    }
    else if ( aLine.startsWith( "Xmaximum" ) ) {
      aXmax = aDoubleValue;
    }
    else if ( aLine.startsWith( "Ymaximum" ) ) {
      anYmax = aDoubleValue;  
    }
  }

  // Close the file
  aFile.close();

  if ( aXmin >= 0 && anYmin >= 0 ) {
    aPointA.setX( aXmin );
    aPointA.setY( anYmin );
  }
    
  if ( aXmax >= 0 && anYmax >= 0 ) {
    aPointB.setX( aXmax );
    aPointB.setY( anYmax );
  }

  if ( !aPointA.isNull() && !aPointB.isNull() ) {
    SetGlobalPoints( ManualCartesian, aPointA, aPointB );
    aRes = true;
  }

  return aRes;
}

bool HYDROData_Image::HasGlobalPoints() const
{
  TDF_Label aLabel = myLab.FindChild( DataTag_TrsfPoints, false );
  if ( aLabel.IsNull() || !aLabel.IsAttribute( GUID_HAS_GLOBAL_POINTS ) )
    return false;

  Handle(TDataStd_RealArray) anArray;
  return aLabel.FindAttribute( TDataStd_RealArray::GetID(), anArray );
}

void HYDROData_Image::SetReferencePoints( const Handle(HYDROData_Image)& theRefImage,
                                          const QPointF&                 thePointA,
                                          const QPointF&                 thePointB,
                                          const QPointF&                 thePointC,
                                          const bool                     theIsUpdate )
{
  SetTrsfReferenceImage( theRefImage );
  SetGlobalPoints( ReferenceImage, thePointA, thePointB, thePointC, theIsUpdate );
}

bool HYDROData_Image::GetReferencePoints( Handle(HYDROData_Image)& theRefImage,
                                          QPointF&                 thePointA,
                                          QPointF&                 thePointB,
                                          QPointF&                 thePointC ) const
{
  if ( !HasReferencePoints() )
    return false;

  theRefImage = GetTrsfReferenceImage();

  TransformationMode aMode;
  GetGlobalPoints( aMode, thePointA, thePointB, thePointC );

  return true;
}

bool HYDROData_Image::HasReferencePoints() const
{
  if ( !HasGlobalPoints() )
    return false;

  Handle(HYDROData_Image) aRefImage = GetTrsfReferenceImage();
  if ( aRefImage.IsNull() )
    return false;

  TransformationMode aTrsfMode = GetTrsfMode();
  if ( aTrsfMode != ReferenceImage )
    return false;

  return true;
}

void HYDROData_Image::SetTrsfMode( const TransformationMode& theMode )
{
  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( myLab.FindChild( DataTag_TrsfMode ), (int)theMode );
  anAttr->SetID(TDataStd_Integer::GetID());
  Changed( Geom_2d );
}

HYDROData_Image::TransformationMode HYDROData_Image::GetTrsfMode() const
{
  TransformationMode aResMode = ManualGeodesic;

  TDF_Label aLabel = myLab.FindChild( DataTag_TrsfPoints, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_Integer) aMode;
    if ( myLab.FindChild( DataTag_TrsfMode ).FindAttribute( TDataStd_Integer::GetID(), aMode ) )
      aResMode = (TransformationMode)aMode->Get();
  }

  return aResMode;
}

void HYDROData_Image::SetTrsfReferenceImage( const Handle(HYDROData_Image)& theRefImage )
{
  SetReferenceObject( theRefImage, DataTag_TrsfImage );
  Changed( Geom_2d );
}

Handle(HYDROData_Image) HYDROData_Image::GetTrsfReferenceImage() const
{
  return Handle(HYDROData_Image)::DownCast( GetReferenceObject( DataTag_TrsfImage ) );
}

void HYDROData_Image::RemoveTrsfReferenceImage()
{
  RemoveReferenceObject( DataTag_TrsfImage );
  Changed( Geom_2d );
}

void HYDROData_Image::AppendReference( const Handle(HYDROData_Entity)& theReferenced )
{
  AddReferenceObject( theReferenced, 0 );
  Changed( Geom_2d );
}

int HYDROData_Image::NbReferences() const
{
  return NbReferenceObjects( 0 );
}

Handle(HYDROData_Entity) HYDROData_Image::Reference( const int theIndex ) const
{
  return GetReferenceObject( 0, theIndex );
}

void HYDROData_Image::ChangeReference(
    const int theIndex, Handle(HYDROData_Entity) theReferenced)
{
  SetReferenceObject( theReferenced, 0, theIndex );
  Changed( Geom_2d );
}

void HYDROData_Image::RemoveReference(const int theIndex)
{
  RemoveReferenceObject( 0, theIndex );
  Changed( Geom_2d );
}

void HYDROData_Image::ClearReferences()
{
  ClearReferenceObjects( 0 );
  Changed( Geom_2d );
}

void HYDROData_Image::SetOperatorName( const QString theOpName )
{
  TCollection_AsciiString anAsciiStr( theOpName.toStdString().c_str() );
  Handle(TDataStd_AsciiString) anAttr = TDataStd_AsciiString::Set( myLab.FindChild( DataTag_Operator ), anAsciiStr );
  anAttr->SetID(TDataStd_AsciiString::GetID());
  Changed( Geom_2d );
}

QString HYDROData_Image::OperatorName() const
{
  QString aRes;

  TDF_Label aLabel = myLab.FindChild( DataTag_Operator, false );
  if ( !aLabel.IsNull() )
  {
    Handle(TDataStd_AsciiString) anAsciiStr;
    if ( aLabel.FindAttribute( TDataStd_AsciiString::GetID(), anAsciiStr ) )
      aRes = QString( anAsciiStr->Get().ToCString() );
  }

  return aRes;
}

void HYDROData_Image::SetArgs(const QByteArray& theArgs)
{
  SaveByteArray(DataTag_Operator, theArgs.constData(), theArgs.length());
  Changed( Geom_2d );
}

QByteArray HYDROData_Image::Args() const
{
  int aLen = 0;
  const char* aData = ByteArray(DataTag_Operator, aLen);
  if (!aLen)
    return QByteArray();
  return QByteArray(aData, aLen);
}

void HYDROData_Image::SetIsSelfSplit(bool theFlag)
{
  if (theFlag) {
    TDataStd_UAttribute::Set(myLab, GUID_SELF_SPLIT);
  } else {
    myLab.ForgetAttribute(GUID_SELF_SPLIT);
  }
  Changed( Geom_2d );
}

bool HYDROData_Image::IsSelfSplit() const
{
  return myLab.IsAttribute(GUID_SELF_SPLIT);
}

QPointF HYDROData_Image::generateThirdPoint( const QPointF& thePointA,
                                             const QPointF& thePointB,
                                             const bool&    theIsLocal ) const
{
  // Rotate vector to 90 degrees : clockwise - for local
  //                               counterclockwise - for global
  const double aTheta = theIsLocal ? -M_PI_2 : M_PI_2;

  QPointF aResPoint;

  // Move to (0,0) for correct rotation
  double x = thePointB.x() - thePointA.x();
  double y = thePointB.y() - thePointA.y();

  aResPoint.setX( x * cos( aTheta ) - y * sin( aTheta ) );
  aResPoint.setY( x * sin( aTheta ) + y * cos( aTheta ) );

  // Move back to origin position
  aResPoint.setX( aResPoint.x() + thePointA.x() );
  aResPoint.setY( aResPoint.y() + thePointA.y() );

  return aResPoint;
}


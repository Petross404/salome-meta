// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_CalculationCase.h"
#include "HYDROData_ArtificialObject.h"
#include "HYDROData_IAltitudeObject.h"
#include "HYDROData_Document.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_Iterator.h"
#include "HYDROData_NaturalObject.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_StricklerTable.h"
#include "HYDROData_LandCoverMap.h"
#include "HYDROData_SplitShapesGroup.h"
#include "HYDROData_Region.h"
#include "HYDROData_Tool.h"
#include "HYDROData_GeomTool.h"
#include <HYDROData_Tool.h>
#include <HYDROData_BCPolygon.h>
#include <HYDROData_BoundaryPolygonTools.h>
#include <HYDROData_SplitToZonesTool.h>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#ifndef LIGHT_MODE
#include <GEOMBase.h>
#endif

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

#include <QSet>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QThread>

#include <TopoDS.hxx>
#include <TopoDS_Shell.hxx>
#include <TopoDS_Edge.hxx>

#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_Sewing.hxx>
#include <BRepTopAdaptor_FClass2d.hxx>
#include <BRepTools_ReShape.hxx>
#include <BRepLib_MakeWire.hxx>

#include <BRepTools.hxx>

#include <TopAbs.hxx>
#include <TopExp_Explorer.hxx>
#include <TopExp.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TDataStd_Integer.hxx>

//#define  DEB_CALCULATION 1
#ifdef DEB_CALCULATION
#include <BRepTools.hxx>
#include <TopLoc_Location.hxx>
#endif
#define CALCULATION_REGIONS_PREF GetName() + "_Reg"
#define CALCULATION_ZONES_PREF GetName() + "_Zone"
#define CALCULATION_GROUPS_PREF GetName() + "_"
//#define DEB_CLASS2D 1
#ifdef DEB_CLASS2D
#include <BRepBuilderAPI_MakeVertex.hxx>
#endif

#define EXPORT_NAME "HYDRO_" + GetName()

#ifndef LIGHT_MODE
#include <SALOME_NamingService.hxx>
#include <SALOME_LifeCycleCORBA.hxx>
#endif

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_CalculationCase, HYDROData_Entity)

HYDROData_CalculationCase::HYDROData_CalculationCase()
: HYDROData_Entity( Geom_2d_and_groups )
{
}

HYDROData_CalculationCase::~HYDROData_CalculationCase()
{
}

void HYDROData_CalculationCase::SetName( const QString& theName, bool isDefault )
{
  QString anOldCaseName = GetName();
  if ( anOldCaseName != theName )
  {
    // Update names of regions and its zones
    UpdateRegionsNames( GetRegions(), anOldCaseName, theName );

    HYDROData_SequenceOfObjects aGroups = GetSplitGroups();

    HYDROData_SequenceOfObjects::Iterator anIter;
    anIter.Init( aGroups );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_SplitShapesGroup) aGroup =
        Handle(HYDROData_SplitShapesGroup)::DownCast( anIter.Value() );
      if ( aGroup.IsNull() )
        continue;

      HYDROData_Tool::UpdateChildObjectName( anOldCaseName, theName, aGroup );
    }
  }

  HYDROData_Entity::SetName( theName, isDefault );
}

QStringList HYDROData_CalculationCase::DumpToPython( const QString&       thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const
{
  QStringList aResList = dumpObjectCreation( theTreatedObjects );
  aResList.prepend( "# Calculation case" );

  QString aCalculName = GetObjPyName();

  AssignmentMode aMode = GetAssignmentMode();
  QString aModeStr = aMode==MANUAL ? "HYDROData_CalculationCase.MANUAL" : "HYDROData_CalculationCase.AUTOMATIC";
  aResList << QString( "%0.SetAssignmentMode( %1 )" ).arg( aCalculName ).arg( aModeStr );

  HYDROData_SequenceOfObjects aGeomObjects = GetGeometryObjects();
  HYDROData_SequenceOfObjects::Iterator anIter( aGeomObjects );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Object) aRefGeomObj =
      Handle(HYDROData_Object)::DownCast( anIter.Value() );
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aRefGeomObj, "AddGeometryObject" );
  }
  aResList << QString( "" );

  QString aGroupName = HYDROData_Tool::GenerateNameForPython( theTreatedObjects, "case_geom_group" );

  HYDROData_SequenceOfObjects aGeomGroups = GetGeometryGroups();
  anIter.Init( aGeomGroups );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_ShapesGroup) aGeomGroup =
      Handle(HYDROData_ShapesGroup)::DownCast( anIter.Value() );
    if ( aGeomGroup.IsNull() )
      continue;

    Handle(HYDROData_Object) aFatherGeom =
      Handle(HYDROData_Object)::DownCast( aGeomGroup->GetFatherObject() );
    if ( aFatherGeom.IsNull() )
      continue;

    int aGroupId = aFatherGeom->GetGroupId( aGeomGroup );
    aResList << QString( "%1 = %2.GetGroup( %3 )" )
              .arg( aGroupName ).arg( aFatherGeom->GetObjPyName() ).arg( aGroupId );

    aResList << QString( "%1.AddGeometryGroup( %2 )" ).arg( aCalculName ).arg( aGroupName );
  }

  HYDROData_SequenceOfObjects aBPolygons = GetBoundaryPolygons();
  for (int i = 1; i <= aBPolygons.Size(); i++ )
  {
    Handle(HYDROData_BCPolygon) aBCPoly = Handle(HYDROData_BCPolygon)::DownCast( aBPolygons(i) );
    setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aBCPoly, "AddBoundaryPolygon" );
  }
  aResList << QString( "" );

  Handle(HYDROData_PolylineXY) aBoundaryPolyline = GetBoundaryPolyline();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aBoundaryPolyline, "SetBoundaryPolyline" );

  Handle(HYDROData_StricklerTable) aStricklerTable = GetStricklerTable();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aStricklerTable, "SetStricklerTable" );

  Handle(HYDROData_LandCoverMap) aLandCoverMap = GetLandCoverMap();
  setPythonReferenceObject( thePyScriptPath, theTreatedObjects, aResList, aLandCoverMap, "SetLandCoverMap" );

  if( aMode==AUTOMATIC )
    DumpRulesToPython( aCalculName, aResList );

  aResList << QString( "" );
  aResList << "# Start the algorithm of the partition and assignment";
  aResList << QString( "%1.Update()" ).arg( aCalculName );

  if( aMode==MANUAL )
  {
    // Now we restore the
    // - regions and zones order
    DumpRegionsToPython( aResList, thePyScriptPath, theTreatedObjects, GetRegions() );
  }

  // Export calculation case
  aResList << QString( "" );
  aResList << "# Export of the calculation case";
  QString anEntryVar = aCalculName + "_entry";
  aResList << QString( "%1 = %2.Export()" ).arg( anEntryVar ).arg( aCalculName );

  // Get geometry object and print debug information
  aResList << "";
  aResList << "# Get geometry shape and print debug information";
  aResList << "import GEOM";
  aResList << QString( "print (\"Entry:\", %1)" ).arg( anEntryVar );
  QString aGeomShapeName = aCalculName + "_geom";
  aResList << QString( "HYDRO_%1 = salome.IDToObject( str( %2 ) )" ).arg( GetName() ).arg( anEntryVar );
  aResList << QString( "print (\"Geom shape:\", HYDRO_%1)" ).arg( GetName() );
  aResList << QString( "print (\"Geom shape name:\", HYDRO_%1.GetName())" ).arg( GetName() );


  //DumpSampleMeshing( aResList, aGeomShapeName, aCalculName+"_mesh" );

  aResList << QString( "" );
  return aResList;
}

void HYDROData_CalculationCase::DumpSampleMeshing( QStringList& theResList,
                                                   const QString& theGeomShapeName,
                                                   const QString& theMeshName ) const
{
  theResList << "";
  theResList << "# Meshing";
  theResList << "import SMESH, SALOMEDS";
  theResList << "from salome.smesh import smeshBuilder";
  theResList << "from salome.geom import geomBuilder";

  theResList << QString( "smesh = smeshBuilder.New()" );
  theResList << QString( "%1 = smesh.Mesh( %2 )" ).arg( theMeshName ).arg( theGeomShapeName );
  theResList << QString( "MEFISTO_2D = %1.Triangle( algo=smeshBuilder.MEFISTO )" ).arg( theMeshName );
  theResList << "Max_Element_Area_1 = MEFISTO_2D.MaxElementArea( 10 )";
  theResList << QString( "Regular_1D = %1.Segment()" ).arg( theMeshName );
  theResList << "Max_Size_1 = Regular_1D.MaxSize(10)";
  theResList << QString( "isDone = %1.Compute()" ).arg( theMeshName );

  theResList << "";
  theResList << "# Set names of Mesh objects";
  theResList << "smesh.SetName( MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D' )";
  theResList << "smesh.SetName( Regular_1D.GetAlgorithm(), 'Regular_1D' )";
  theResList << "smesh.SetName( Max_Size_1, 'Max Size_1' )";
  theResList << "smesh.SetName( Max_Element_Area_1, 'Max. Element Area_1' )";
  theResList << QString( "smesh.SetName( %1.GetMesh(), '%1' )" ).arg( theMeshName );

  theResList << "";
  theResList << "# Greate SMESH groups";
  theResList << QString( "geompy = geomBuilder.New()" );
  theResList << QString( "geom_groups = geompy.GetGroups( %1 )" ).arg( theGeomShapeName );
  theResList << QString( "for group in geom_groups:" );
  theResList << QString( "    smesh_group = %1.GroupOnGeom(group, group.GetName(), SMESH.EDGE)" )
                .arg( theMeshName );
  theResList << QString( "    smesh.SetName(smesh_group, group.GetName())" );
  theResList << QString( "    print (\"SMESH group '%s': %s)\" % (smesh_group.GetName(), smesh_group)" );
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetAllReferenceObjects() const
{
  HYDROData_SequenceOfObjects aResSeq = HYDROData_Entity::GetAllReferenceObjects();

  Handle(HYDROData_PolylineXY) aBoundaryPolyline = GetBoundaryPolyline();
  if ( !aBoundaryPolyline.IsNull() )
    aResSeq.Append( aBoundaryPolyline );

  HYDROData_SequenceOfObjects aSeqOfGeomObjs = GetGeometryObjects();
  aResSeq.Append( aSeqOfGeomObjs );

  // Regions
  HYDROData_SequenceOfObjects aSeqOfRegions = GetRegions();
  aResSeq.Append( aSeqOfRegions );

  return aResSeq;
}

static void FilterEdgesByIncludeSelectionBoundaryPolygons( const HYDROData_SplitToZonesTool::SplitDataList& anEdgesList,
                                                           HYDROData_SequenceOfObjects aBoundaryPolygons,
                                                           HYDROData_SplitToZonesTool::SplitDataList& outEdgesList)
{
  //perform boundary condition polygons on EdgesList
  TopTools_SequenceOfShape IncTools, SelectionTools;
  HYDROData_SplitToZonesTool::SplitDataListIterator anIter(anEdgesList);
  for (int i=1; i<=aBoundaryPolygons.Size();i++)
  {
    Handle(HYDROData_BCPolygon) aBCPoly = Handle(HYDROData_BCPolygon)::DownCast( aBoundaryPolygons(i));
    TopoDS_Shape aPolyTopShape = aBCPoly->GetTopShape();
    int bType = aBCPoly->GetBoundaryType();
    if (bType == 2)
      IncTools.Append(aPolyTopShape);
    else if (bType == 3)
      SelectionTools.Append(aPolyTopShape);
  }

  while( anIter.hasNext() )
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
    if ( aSplitData.ObjectNames.isEmpty() || aSplitData.Shape.IsNull() )
      continue;
    if (aSplitData.Shape.ShapeType() != TopAbs_EDGE)
      continue;
    if (HYDROData_BoundaryPolygonTools::IncludeTool(IncTools, aSplitData.Shape) && HYDROData_BoundaryPolygonTools::SelectionTool(SelectionTools, aSplitData.Shape))
      outEdgesList.append(aSplitData);
  }
}


static void SplitEdgesByBoundaryPolygons( const HYDROData_SplitToZonesTool::SplitDataList& anEdgesList,
                                          const HYDROData_SequenceOfObjects& aBoundaryPolygons,
                                          NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape, TopTools_ShapeMapHasher>& ObjToRes,
                                          HYDROData_SplitToZonesTool::SplitDataList& outBoundaryPolygonEdgesList)
{
  //perform boundary condition polygons on EdgesList
  TopTools_SequenceOfShape CutTools;
  NCollection_DataMap<TopoDS_Shape, QString, TopTools_ShapeMapHasher> BPolyToName;

  ObjToRes.Clear();
  HYDROData_SplitToZonesTool::SplitDataListIterator anIter(anEdgesList);
  for (int i=1; i<=aBoundaryPolygons.Size();i++)
  {
    Handle(HYDROData_BCPolygon) aBCPoly = Handle(HYDROData_BCPolygon)::DownCast( aBoundaryPolygons(i));
    TopoDS_Shape aPolyTopShape = aBCPoly->GetTopShape();
    int bType = aBCPoly->GetBoundaryType();
    if (bType == 1)
    {
      CutTools.Append(aPolyTopShape);
      QString bp_name = aBCPoly->GetName();
      BPolyToName.Bind(aPolyTopShape, bp_name);
    }
  }

  while( anIter.hasNext() )
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
    if ( aSplitData.ObjectNames.isEmpty() || aSplitData.Shape.IsNull() )
      continue;
    if (aSplitData.Shape.ShapeType() != TopAbs_EDGE)
      continue;
    ObjToRes.Add(aSplitData.Shape, TopoDS_Shape());
  }

  NCollection_IndexedDataMap<TopoDS_Shape, TopTools_MapOfShape, TopTools_ShapeMapHasher> BPFaceToCutEdges;
  HYDROData_BoundaryPolygonTools::CutTool(CutTools, ObjToRes, BPFaceToCutEdges);
  //
  for (int i = 1; i <= CutTools.Length(); i++ )
  {
    TopoDS_Shape F = CutTools(i);
    const QString* name = BPolyToName.Seek(F);
    const TopTools_MapOfShape* EdgesIn = BPFaceToCutEdges.Seek(F); // EdgesIn : edges inside boudnary polygon F
    if (name && EdgesIn)
    {
      TopTools_MapIteratorOfMapOfShape it(*EdgesIn);
      for (;it.More();it.Next())
      {
        TopoDS_Edge E = TopoDS::Edge(it.Value());
        if (!E.IsNull())
        {
          HYDROData_SplitToZonesTool::SplitData SD(HYDROData_SplitToZonesTool::SplitData::Data_Edge, E, *name);
          outBoundaryPolygonEdgesList.append(SD);
        }
      }
    }
  }
}


static void PerformEdgeReplInZones(const HYDROData_SplitToZonesTool::SplitDataList& ZoneList,
                                   const NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape, TopTools_ShapeMapHasher>& ObjToRes,
                                   HYDROData_SplitToZonesTool::SplitDataList& outZoneList)
{
  HYDROData_SplitToZonesTool::SplitDataListIterator it( ZoneList );
  BRepTools_ReShape reshaper;
  for (;it.hasNext();)
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = it.next();
    if ( aSplitData.ObjectNames.isEmpty() || aSplitData.Shape.IsNull() )
      continue;

    TopoDS_Shape mS = aSplitData.Shape;
    for (int i=1; i<=ObjToRes.Extent();i++)
    {
      TopoDS_Shape K = ObjToRes.FindKey(i);
      TopoDS_Shape V = ObjToRes.FindFromIndex(i);
      if (V.IsNull())
        continue;
      if (V.ShapeType() != TopAbs_EDGE && V.ShapeType() != TopAbs_WIRE && V.ShapeType() != TopAbs_COMPOUND)
        continue;
      if (V.ShapeType() == TopAbs_COMPOUND)
      {
        TopExp_Explorer exp(V, TopAbs_EDGE);
        TopTools_ListOfShape lE;
        for (;exp.More();exp.Next())
          lE.Append(exp.Current());
        if (lE.Extent() == 1)
          V = lE.First();
        else if (lE.Extent() > 1)
        {
          BRepLib_MakeWire MW;
          MW.Add(lE);
          if (MW.IsDone())
            V = MW.Wire();
          else
            continue;
        }
        else
          continue;
      }
      reshaper.Replace(K, V);
    }
    TopoDS_Shape nS = reshaper.Apply(mS);
    HYDROData_SplitToZonesTool::SplitData aNS(aSplitData.Type, nS, aSplitData.ObjectNames);
    outZoneList.append(aNS);
  }
}

static void CreateNewEdgeList( const HYDROData_SplitToZonesTool::SplitDataList& theEdges,
                               const NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape, TopTools_ShapeMapHasher>& ObjToRes,
                               HYDROData_SplitToZonesTool::SplitDataList& newEdges)
{
  HYDROData_SplitToZonesTool::SplitDataListIterator anIter( theEdges );
  while( anIter.hasNext() )
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
    if ( aSplitData.ObjectNames.isEmpty() || aSplitData.Shape.IsNull() )
      continue;

    const TopoDS_Shape* aDivShape = ObjToRes.Seek(aSplitData.Shape);
    if (aDivShape)
    {
      TopExp_Explorer exp(*aDivShape, TopAbs_EDGE);
      for (;exp.More();exp.Next())
      {
        HYDROData_SplitToZonesTool::SplitData anNewSData(aSplitData.Type, exp.Current(), aSplitData.ObjectNames);
        newEdges.append(anNewSData);
      }
    }
    else
      newEdges.append(aSplitData);
  }
}


void HYDROData_CalculationCase::Update()
{
  HYDROData_Entity::Update();
  SetWarning();

  // At first we remove previously created objects
  RemoveRegions();
  RemoveSplitGroups();

  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return;

  // Split to zones
  HYDROData_SplitToZonesTool::SplitDataList aZonesList, anEdgesList;

  Handle(HYDROData_PolylineXY) aBoundaryPolyline = GetBoundaryPolyline();

  HYDROData_SequenceOfObjects InterPolys = GetInterPolyObjects();

  HYDROData_SequenceOfObjects aGeomObjects = GetGeometryObjects();
  if ( !aGeomObjects.IsEmpty() ) {
    HYDROData_SequenceOfObjects aGeomGroups = GetGeometryGroups();

    HYDROData_SplitToZonesTool::SplitDataList aSplitObjects =
      HYDROData_SplitToZonesTool::Split( aGeomObjects, aGeomGroups, aBoundaryPolyline, InterPolys );
    if ( !aSplitObjects.isEmpty() ) {
      HYDROData_SplitToZonesTool::SplitDataListIterator anIter( aSplitObjects );
      while( anIter.hasNext() ) {
        const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
        if ( aSplitData.Type == HYDROData_SplitToZonesTool::SplitData::Data_Zone )
          aZonesList.append( aSplitData );
        else if ( aSplitData.Type == HYDROData_SplitToZonesTool::SplitData::Data_Edge ||
                  aSplitData.Type == HYDROData_SplitToZonesTool::SplitData::Data_IntEdge)
          anEdgesList.append( aSplitData );
      }
    }
  }

  //
  //split edges by boundary polygons
  HYDROData_SequenceOfObjects aBoundaryPolygons = GetBoundaryPolygons();
  //edge to splitted edge (compound of edges or original edge)
  NCollection_IndexedDataMap<TopoDS_Shape, TopoDS_Shape, TopTools_ShapeMapHasher> ObjToRes;
  //split edge list by BP
  HYDROData_SplitToZonesTool::SplitDataList outBoundaryPolygonEdgesList; //new groups - for each BP of type 1 (cut) create a group of edges which are inside of this BP
  SplitEdgesByBoundaryPolygons(anEdgesList, aBoundaryPolygons, ObjToRes, outBoundaryPolygonEdgesList);
  HYDROData_SplitToZonesTool::SplitDataList aNewZonesList;
  //replace splitted edges in zone list (faces)
  PerformEdgeReplInZones(aZonesList, ObjToRes, aNewZonesList);
  //
  //create new edges list based on splitting info from ObjToRes
  HYDROData_SplitToZonesTool::SplitDataList newEdgesList1,newEdgesList2;
  CreateNewEdgeList(anEdgesList, ObjToRes, newEdgesList1);
  //
  newEdgesList1.append(outBoundaryPolygonEdgesList); //append new list of groups
  //filter out edges list by include&selection tools
  FilterEdgesByIncludeSelectionBoundaryPolygons(newEdgesList1,aBoundaryPolygons,newEdgesList2);

  switch( GetAssignmentMode() )
  {
  case MANUAL:
    CreateRegionsDef( aDocument,aNewZonesList );
    break;
  case AUTOMATIC:
    CreateRegionsAuto( aDocument,aNewZonesList );
    break;
  }

  CreateEdgeGroupsDef( aDocument, newEdgesList2 );
}

void HYDROData_CalculationCase::CreateRegionsDef( const Handle(HYDROData_Document)& theDoc,
                                                  const HYDROData_SplitToZonesTool::SplitDataList& theZones )
{
  // Create result regions for case, by default one zone for one region
  QString aRegsPref = CALCULATION_REGIONS_PREF;
  QString aZonesPref = CALCULATION_ZONES_PREF;

  HYDROData_SplitToZonesTool::SplitDataListIterator anIter( theZones );
  while( anIter.hasNext() )
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
    // Create new region
    Handle(HYDROData_Region) aRegion = addNewRegion( theDoc, aRegsPref );

    // Add the zone for region
    Handle(HYDROData_Zone) aRegionZone = aRegion->addNewZone( theDoc, aZonesPref, aSplitData.Face(), aSplitData.ObjectNames );
  }
}

void HYDROData_CalculationCase::CreateRegionsAuto( const Handle(HYDROData_Document)& theDoc,
                                                   const HYDROData_SplitToZonesTool::SplitDataList& theZones )
{
  DEBTRACE("HYDROData_CalculationCase::CreateRegionsAuto");
  QMap<QString, Handle(HYDROData_Region)> aRegionsMap; //object name to region
  QMap<QString, QString> aRegionNameToObjNameMap;
  QString aZonesPref = CALCULATION_ZONES_PREF;
  HYDROData_PriorityQueue aPr( this, DataTag_CustomRules );

  // 1. First we create a default region for each object included into the calculation case
  HYDROData_SequenceOfObjects anObjects = GetGeometryObjects();
  for( int i = anObjects.Lower(), n = anObjects.Upper(); i<=n; i++ )
  {
    Handle(HYDROData_Entity) anObj = anObjects.Value( i );
    if( anObj.IsNull() )
      continue;
    QString anObjName = anObj->GetName();
    QString aRegName = anObjName + "_reg";
    Handle(HYDROData_Region) aRegion = addNewRegion( theDoc, aRegName, false );
    aRegionsMap.insert( anObjName, aRegion );
    aRegionNameToObjNameMap.insert( aRegName, anObjName );
  }

  // 2. Now for each zone it is necessary to determine the most priority object
  //    and assign to zone to corresponding region
  HYDROData_SplitToZonesTool::SplitDataListIterator anIter( theZones );
  while( anIter.hasNext() )
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
    HYDROData_Zone::MergeType aMergeType;
    Handle(HYDROData_Entity) aRegObj = aPr.GetMostPriorityObject( aSplitData.ObjectNames, aMergeType );
    if( aRegObj.IsNull() )
      continue;
    Handle(HYDROData_Region) aRegion = aRegionsMap[aRegObj->GetName()];
    if( aRegion.IsNull() )
      continue;
    Handle(HYDROData_Zone) aRegionZone = aRegion->addNewZone( theDoc, aZonesPref, aSplitData.Face(), aSplitData.ObjectNames );

    if( aSplitData.ObjectNames.count() > 1 && aMergeType==HYDROData_Zone::Merge_UNKNOWN )
    {
      qDebug( "Error in algorithm: unresolved conflicts" );
    }

    Handle(HYDROData_Entity) aMergeEntity = aRegObj;
    Handle(HYDROData_Object) aMergeObject = Handle(HYDROData_Object)::DownCast( aMergeEntity );
    if ( !aMergeObject.IsNull() ) {
      DEBTRACE("aMergeEntity " << aMergeEntity->GetName().toStdString());
      aMergeEntity = aMergeObject->GetAltitudeObject();
    }

    switch( aMergeType )
    {
    case HYDROData_Zone::Merge_ZMIN:
    case HYDROData_Zone::Merge_ZMAX:
      aRegionZone->SetMergeType( aMergeType );
      break;
    case HYDROData_Zone::Merge_Object:
      aRegionZone->SetMergeType( aMergeType );
      aRegionZone->RemoveMergeObject();
      aRegionZone->SetMergeObject( aMergeEntity );
      break;
    }
  }

  QStringList anObjectsWithEmptyRegions;
  QMap<QString, Handle(HYDROData_Region)>::const_iterator
    anIt = aRegionsMap.begin(), aLast = aRegionsMap.end();
  for( ; anIt!=aLast; anIt++ )
  {
    Handle(HYDROData_Region) aRegion = anIt.value();
    if( aRegion->GetZones().IsEmpty() )
    {
      QString aRegName = aRegion->GetName();
      QString anObjName = aRegionNameToObjNameMap[aRegName];
      anObjectsWithEmptyRegions.append( anObjName );
    }
  }

  if( !anObjectsWithEmptyRegions.empty() )
  {
    QString aData = anObjectsWithEmptyRegions.join( ", " );
    SetWarning( WARN_EMPTY_REGIONS, aData );
  }
}

void HYDROData_CalculationCase::CreateEdgeGroupsDef( const Handle(HYDROData_Document)& theDoc,
                                                     const HYDROData_SplitToZonesTool::SplitDataList& theEdges )
{
  QMap<QString,Handle(HYDROData_SplitShapesGroup)> aSplitEdgesGroupsMap;

  HYDROData_SplitToZonesTool::SplitDataListIterator anIter( theEdges );
  while( anIter.hasNext() )
  {
    const HYDROData_SplitToZonesTool::SplitData& aSplitData = anIter.next();
    // Create new edges group
    if ( aSplitData.ObjectNames.isEmpty() || aSplitData.Shape.IsNull() )
      continue;

    QString anObjName = aSplitData.ObjectNames.first();
    if ( anObjName.isEmpty() )
      continue;
#ifdef DEB_CALCULATION
    QString aStr = aSplitData.ObjectNames.join(" ");
	  cout << " CCase: Names = "<<aStr.toStdString() << " size = " <<aSplitData.ObjectNames.size() <<endl;
#endif
    Handle(HYDROData_SplitShapesGroup) aSplitGroup;
    if ( !aSplitEdgesGroupsMap.contains( anObjName ) )
    {
      aSplitGroup = addNewSplitGroup( CALCULATION_GROUPS_PREF + anObjName );
      aSplitEdgesGroupsMap.insert( anObjName, aSplitGroup );
    }
    else
    {
      aSplitGroup = aSplitEdgesGroupsMap[ anObjName ];
    }
    if ( aSplitGroup.IsNull() )
      continue;

      aSplitGroup->AddShape( aSplitData.Shape );

    TopTools_SequenceOfShape theShapes;
    aSplitGroup->GetShapes(theShapes);

    if (aSplitData.Type == HYDROData_SplitToZonesTool::SplitData::Data_IntEdge)
      aSplitGroup->SetInternal(true);
  }
}

bool HYDROData_CalculationCase::AddGeometryObject( const Handle(HYDROData_Object)& theObject )
{
  if ( !HYDROData_Tool::IsGeometryObject( theObject ) )
    return false; // Wrong type of object

  if ( HasReference( theObject, DataTag_GeometryObject ) )
    return false; // Object is already in reference list

  AddReferenceObject( theObject, DataTag_GeometryObject );

  // Indicate model of the need to update splitting
  Changed( Geom_2d );

  return true;
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetGeometryObjects() const
{
  return GetReferenceObjects( DataTag_GeometryObject );
}

void HYDROData_CalculationCase::RemoveGeometryObject( const Handle(HYDROData_Object)& theObject )
{
  if ( theObject.IsNull() )
    return;

  RemoveReferenceObject( theObject->Label(), DataTag_GeometryObject );

  // Indicate model of the need to update splitting
  Changed( Geom_2d );
}

void HYDROData_CalculationCase::RemoveGeometryObjects()
{
  ClearReferenceObjects( DataTag_GeometryObject );

  // Indicate model of the need to update splitting
  Changed( Geom_2d );
}

bool HYDROData_CalculationCase::AddGeometryGroup( const Handle(HYDROData_ShapesGroup)& theGroup )
{
  if ( theGroup.IsNull() )
    return false;

  if ( HasReference( theGroup, DataTag_GeometryGroup ) )
    return false; // Object is already in reference list

  AddReferenceObject( theGroup, DataTag_GeometryGroup );

  // Indicate model of the need to update splitting
  Changed( Geom_Groups );

  return true;
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetGeometryGroups() const
{
  return GetReferenceObjects( DataTag_GeometryGroup );
}

void HYDROData_CalculationCase::RemoveGeometryGroup( const Handle(HYDROData_ShapesGroup)& theGroup )
{
  if ( theGroup.IsNull() )
    return;

  RemoveReferenceObject( theGroup->Label(), DataTag_GeometryGroup );

  // Indicate model of the need to update splitting
  Changed( Geom_Groups );
}

void HYDROData_CalculationCase::RemoveGeometryGroups()
{
  ClearReferenceObjects( DataTag_GeometryGroup );

  // Indicate model of the need to update splitting
  Changed( Geom_Groups );
}

void HYDROData_CalculationCase::SetBoundaryPolyline( const Handle(HYDROData_PolylineXY)& thePolyline )
{
  Handle(HYDROData_PolylineXY) aPrevPolyline = GetBoundaryPolyline();

  SetReferenceObject( thePolyline, DataTag_Polyline );

  // Indicate model of the need to update zones splitting
  if( !IsEqual( aPrevPolyline, thePolyline ) )
    Changed( Geom_2d );
}

Handle(HYDROData_PolylineXY) HYDROData_CalculationCase::GetBoundaryPolyline() const
{
  return Handle(HYDROData_PolylineXY)::DownCast(
           GetReferenceObject( DataTag_Polyline ) );
}

void HYDROData_CalculationCase::RemoveBoundaryPolyline()
{
  Handle(HYDROData_PolylineXY) aPrevPolyline = GetBoundaryPolyline();

  ClearReferenceObjects( DataTag_Polyline );

  // Indicate model of the need to update zones splitting
  Changed( Geom_2d );
}

void HYDROData_CalculationCase::SetStricklerTable( const Handle(HYDROData_StricklerTable)& theStricklerTable )
{
  Handle(HYDROData_StricklerTable) aPrevStricklerTable = GetStricklerTable();

  SetReferenceObject( theStricklerTable, DataTag_StricklerTable );

  // Indicate model of the need to update land covers partition
  if( !IsEqual( aPrevStricklerTable, theStricklerTable ) )
    Changed( Geom_No );
}

Handle(HYDROData_StricklerTable) HYDROData_CalculationCase::GetStricklerTable() const
{
  return Handle(HYDROData_StricklerTable)::DownCast(
           GetReferenceObject( DataTag_StricklerTable ) );
}

void HYDROData_CalculationCase::RemoveStricklerTable()
{
  Handle(HYDROData_StricklerTable) aPrevStricklerTable = GetStricklerTable();

  ClearReferenceObjects( DataTag_StricklerTable );

  // Indicate model of the need to update land covers partition
  Changed( Geom_No );
}

Handle(HYDROData_LandCoverMap) HYDROData_CalculationCase::GetLandCoverMap() const
{
  Handle(HYDROData_LandCoverMap) aMap = Handle(HYDROData_LandCoverMap)::DownCast(
    GetReferenceObject( DataTag_LandCoverMap ) );
  DEBTRACE("GetLandCoverMap " << aMap.IsNull());
  return aMap;
}

void HYDROData_CalculationCase::SetLandCoverMap( const Handle(HYDROData_LandCoverMap)& theMap )
{
  SetReferenceObject( theMap, DataTag_LandCoverMap );
}

Handle(HYDROData_Region) HYDROData_CalculationCase::AddNewRegion( const Handle(HYDROData_Zone)& theZone )
{
  Changed( Geom_No );
  Changed( Geom_No );
  Changed( Geom_No );
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  Handle(HYDROData_Region) aNewRegion = addNewRegion( aDocument, CALCULATION_REGIONS_PREF );
  if ( aNewRegion.IsNull() )
    return aNewRegion;

  aNewRegion->AddZone( theZone );

  return aNewRegion;
}

bool HYDROData_CalculationCase::AddRegion( const Handle(HYDROData_Region)& theRegion )
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();

  if ( theRegion.IsNull() )
    return false;

  HYDROData_CalculationCase::DataTag aDataTag = DataTag_Region;

  if ( HasReference( theRegion, aDataTag ) )
    return false; // Object is already in reference list

  // Move the region from other calculation
  Handle(HYDROData_CalculationCase) aFatherCalc =
    Handle(HYDROData_CalculationCase)::DownCast( theRegion->GetFatherObject() );
  if ( !aFatherCalc.IsNull() && aFatherCalc->Label() != myLab )
  {
    Handle(HYDROData_Region) aNewRegion = addNewRegion( aDocument, CALCULATION_REGIONS_PREF );
    theRegion->CopyTo( aNewRegion, true );

    aFatherCalc->RemoveRegion( theRegion );

    theRegion->SetLabel( aNewRegion->Label() );
  }
  else
  {
    AddReferenceObject( theRegion, aDataTag );
  }

  return true;
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetRegions() const
{
  return GetReferenceObjects( DataTag_Region );
}

void HYDROData_CalculationCase::UpdateRegionsOrder()
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() )
    return;

  QString aRegsPref = CALCULATION_REGIONS_PREF;
  std::map<int, Handle(HYDROData_Region)> IndToRegion; //index in the name of region to region itself
  HYDROData_SequenceOfObjects aRegions = GetRegions();
  HYDROData_SequenceOfObjects::Iterator anIter( aRegions );
  QString aName;
  int aVal;
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Region) aRegion = Handle(HYDROData_Region)::DownCast( anIter.Value() );
    if ( aRegion.IsNull() )
      continue;

    if (HYDROData_Tool::ExtractGeneratedObjectName(aRegion->GetName(), aVal, aName) && aName == aRegsPref)
      IndToRegion[aVal] = aRegion;
  }

  int nbR = aRegions.Length();
  std::map<int, Handle(HYDROData_Region)>::iterator it = IndToRegion.begin();
  for( int i = 1; it != IndToRegion.end(); ++it )
    if (it->first <= nbR + 1)
    {
      QString aNewName = QString("%1_%2").arg(aRegsPref).arg(QString::number(i));
      it->second->SetName(aNewName);
      i++;
    }
}

void HYDROData_CalculationCase::RemoveRegion( const Handle(HYDROData_Region)& theRegion )
{
  if ( theRegion.IsNull() )
    return;

  HYDROData_CalculationCase::DataTag aDataTag = DataTag_Region;
  RemoveReferenceObject( theRegion->Label(), aDataTag );

  // Remove region from data model
  Handle(HYDROData_CalculationCase) aFatherCalc =
    Handle(HYDROData_CalculationCase)::DownCast( theRegion->GetFatherObject() );
  if ( !aFatherCalc.IsNull() && aFatherCalc->Label() == myLab )
    theRegion->Remove();
}

void HYDROData_CalculationCase::RemoveRegions()
{
  myLab.FindChild( DataTag_ChildRegion ).ForgetAllAttributes();
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetSplitGroups() const
{
  return GetReferenceObjects( DataTag_SplitGroups );
}

void HYDROData_CalculationCase::RemoveSplitGroups()
{
  myLab.FindChild( DataTag_SplitGroups ).ForgetAllAttributes();
}

double HYDROData_CalculationCase::GetAltitudeForPoint( const gp_XY& thePoint ) const
{
  Handle(HYDROData_Zone) aZone = GetZoneFromPoint( thePoint );
  return GetAltitudeForPoint( thePoint, aZone );
}

double HYDROData_CalculationCase::GetAltitudeForPoint( const gp_XY&                    thePoint,
                                                       const Handle(HYDROData_Region)& theRegion,
                                                       int theMethod) const
{
  double aResAltitude = HYDROData_IAltitudeObject::GetInvalidAltitude();

  Handle(HYDROData_Zone) aZone = GetZoneFromPoint( thePoint );
  if ( !aZone.IsNull() )
  {
    DEBTRACE("GetAltitudeForPoint Region " << theRegion->GetName().toStdString() << " Zone " << aZone->GetName().toStdString());
    Handle(HYDROData_Region) aRefRegion = Handle(HYDROData_Region)::DownCast( aZone->GetFatherObject() );
    if ( IsEqual( aRefRegion, theRegion ) )
      aResAltitude = GetAltitudeForPoint( thePoint, aZone, theMethod );
    else
      {
        DEBTRACE("GetAltitudeForPoint Region " << aRefRegion->GetName().toStdString() << " Zone " << aZone->GetName().toStdString() << " ---------------------------");
        aResAltitude = GetAltitudeForPoint( thePoint, aZone, theMethod );
      }
  }
  else
    {
      DEBTRACE(" --- GetAltitudeForPoint No Zone ---");
    }

  return aResAltitude;
}

double HYDROData_CalculationCase::GetAltitudeForPoint( const gp_XY&                  thePoint,
                                                       const Handle(HYDROData_Zone)& theZone,
                                                       int theMethod) const
{
  DEBTRACE("GetAltitudeForPoint Zone " << theZone->GetName().toStdString());
  double aResAltitude = HYDROData_IAltitudeObject::GetInvalidAltitude();
  if ( theZone.IsNull() )
  {
	DEBTRACE("Zone nulle");
    return aResAltitude;
  }

  HYDROData_Zone::MergeType aZoneMergeType = theZone->GetMergeType();
  DEBTRACE("aZoneMergeType " << aZoneMergeType);
  if ( !theZone->IsMergingNeed() )
  {
    aZoneMergeType = HYDROData_Zone::Merge_UNKNOWN;
    DEBTRACE("---");
  }
  else if ( aZoneMergeType == HYDROData_Zone::Merge_UNKNOWN )
  {
	DEBTRACE("GetAltitudeForPoint Zone " << theZone->GetName().toStdString() << " Merge_UNKNOWN");
    return aResAltitude;
  }

  HYDROData_IInterpolator* aZoneInterpolator = theZone->GetInterpolator();
  if ( aZoneMergeType == HYDROData_Zone::Merge_Object )
  {
    Handle(HYDROData_IAltitudeObject) aMergeAltitude =
      Handle(HYDROData_IAltitudeObject)::DownCast( theZone->GetMergeObject() );
    if ( !aMergeAltitude.IsNull() )
    {
      if ( aZoneInterpolator != NULL )
      {
    	DEBTRACE("aZoneInterpolator != NULL");
        aZoneInterpolator->SetAltitudeObject( aMergeAltitude );
        aResAltitude = aZoneInterpolator->GetAltitudeForPoint( thePoint );
      }
      else
      {
    	DEBTRACE("aZoneInterpolator == NULL");
        aResAltitude = aMergeAltitude->GetAltitudeForPoint( thePoint, theMethod );
      }
    }
  }
  else
  {
	DEBTRACE("aZoneMergeType != HYDROData_Zone::Merge_Object");
    HYDROData_SequenceOfObjects aZoneObjects = theZone->GetObjects();
    HYDROData_SequenceOfObjects::Iterator anIter( aZoneObjects );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Object) aZoneObj =
        Handle(HYDROData_Object)::DownCast( anIter.Value() );
      if ( aZoneObj.IsNull() )
        continue;

      Handle(HYDROData_IAltitudeObject) anObjAltitude = aZoneObj->GetAltitudeObject();
      if ( anObjAltitude.IsNull() )
        continue;

      double aPointAltitude = 0.0;
      if ( aZoneInterpolator != NULL )
      {
    	DEBTRACE("aZoneInterpolator != NULL");
        aZoneInterpolator->SetAltitudeObject( anObjAltitude );
        aPointAltitude = aZoneInterpolator->GetAltitudeForPoint( thePoint );
      }
      else
      {
    	DEBTRACE("aZoneInterpolator == NULL");
        aPointAltitude = anObjAltitude->GetAltitudeForPoint( thePoint, theMethod );
      }

      if ( ValuesEquals( aPointAltitude, HYDROData_IAltitudeObject::GetInvalidAltitude() ) )
        continue;

      if ( aZoneMergeType == HYDROData_Zone::Merge_UNKNOWN )
      {
        aResAltitude = aPointAltitude;
        break;
      }
      else if ( aZoneMergeType == HYDROData_Zone::Merge_ZMIN )
      {
        if ( ValuesEquals( aResAltitude, HYDROData_IAltitudeObject::GetInvalidAltitude() ) ||
             aResAltitude > aPointAltitude )
        {
          aResAltitude = aPointAltitude;
        }
      }
      else if ( aZoneMergeType == HYDROData_Zone::Merge_ZMAX )
      {
        if ( ValuesEquals( aResAltitude, HYDROData_IAltitudeObject::GetInvalidAltitude() ) ||
             aResAltitude < aPointAltitude )
        {
          aResAltitude = aPointAltitude;
        }
      }
    }
  }

  return aResAltitude;
}

NCollection_Sequence<double> HYDROData_CalculationCase::GetAltitudesForPoints(
  const NCollection_Sequence<gp_XY>& thePoints,
  const Handle(HYDROData_Region)&    theRegion,
  int theMethod) const
{
  DEBTRACE("HYDROData_CalculationCase::GetAltitudesForPoints " << theRegion->GetName().toStdString());
  NCollection_Sequence<double> aResSeq;

  QFuture<void> aFuture = QtConcurrent::run([&]() {
    int aNbPoints = thePoints.Length();

    for ( int i = 1, n = aNbPoints; i <= n; ++i)
    {
      const gp_XY& thePnt = thePoints.Value( i );

      double anAltitude = GetAltitudeForPoint( thePnt, theRegion, theMethod );
      aResSeq.Append( anAltitude );
    }
  });

  while( aFuture.isRunning() ) {

  }

  return aResSeq;
}

NCollection_Sequence<double> HYDROData_CalculationCase::GetAltitudesForPoints(
  const NCollection_Sequence<gp_XY>& thePoints,
  const Handle(HYDROData_Zone)&      theZone,
  int theMethod) const
{
  NCollection_Sequence<double> aResSeq;

  for ( int i = 1, n = thePoints.Length(); i <= n; ++i )
  {
    const gp_XY& thePnt = thePoints.Value( i );

    double anAltitude = GetAltitudeForPoint( thePnt, theZone, theMethod );
    aResSeq.Append( anAltitude );
  }

  return aResSeq;
}

double HYDROData_CalculationCase::GetStricklerCoefficientForPoint( const gp_XY& thePoint ) const
{
  Handle( HYDROData_LandCoverMap ) aMap = GetLandCoverMap();
  Handle( HYDROData_StricklerTable ) aTable = GetStricklerTable();
  if( aMap.IsNull() )
    return 0.0;

  QString aType;
  aMap->FindByPoint( thePoint, aType );
  double aCoeff = aTable->Get( aType, 0.0 );
  return aCoeff;
}

std::vector<double> HYDROData_CalculationCase::GetStricklerCoefficientForPoints(const std::vector<gp_XY>& thePoints,
  double DefValue, bool UseMax ) const
{
  DEBTRACE("GetStricklerCoefficientForPoints");
  Handle( HYDROData_LandCoverMap ) aLCM = GetLandCoverMap();
  Handle( HYDROData_StricklerTable ) aTable = GetStricklerTable();
  std::vector<double> theCoeffs;
  DEBTRACE("aLCM.IsNull() " << aLCM.IsNull());
  DEBTRACE("aTable.IsNull() "<< aTable.IsNull());
  if( aLCM.IsNull() || aTable.IsNull() )
    return theCoeffs;


  QFuture<void> aFuture = QtConcurrent::run([&]() {
    aLCM->ClassifyPoints(thePoints, aTable, theCoeffs, DefValue, UseMax );
  });

  while( aFuture.isRunning() ) {
    QThread::msleep(200);
  }

  return theCoeffs;
}

std::vector<int> HYDROData_CalculationCase::GetStricklerTypeForPoints( const std::vector<gp_XY>& thePoints ) const
{
  Handle( HYDROData_LandCoverMap ) aLCM = GetLandCoverMap();
  Handle( HYDROData_StricklerTable ) aTable = GetStricklerTable();
  std::vector<int> types;
  if( aLCM.IsNull() || aTable.IsNull() )
    return types;

  aLCM->ClassifyPoints(thePoints, aTable, types );
  return types;
}

Handle(HYDROData_Region) HYDROData_CalculationCase::GetRegionFromPoint( const gp_XY& thePoint ) const
{
  Handle(HYDROData_Region) aResRegion;

  Handle(HYDROData_Zone) aZone = GetZoneFromPoint( thePoint );
  if ( !aZone.IsNull() )
    aResRegion = Handle(HYDROData_Region)::DownCast( aZone->GetFatherObject() );

  return aResRegion;
}

Handle(HYDROData_Zone) HYDROData_CalculationCase::GetZoneFromPoint( const gp_XY& thePoint ) const
{
  Handle(HYDROData_Zone) aResZone;

  HYDROData_SequenceOfObjects aRegions = GetRegions();

  HYDROData_SequenceOfObjects::Iterator anIter( aRegions );
  for ( ; anIter.More() && aResZone.IsNull(); anIter.Next() )
  {
    Handle(HYDROData_Region) aRegion =
      Handle(HYDROData_Region)::DownCast( anIter.Value() );
    if ( aRegion.IsNull() )
      continue;

    HYDROData_SequenceOfObjects aZones = aRegion->GetZones();
    HYDROData_SequenceOfObjects::Iterator aZonesIter( aZones );
    for ( ; aZonesIter.More() && aResZone.IsNull(); aZonesIter.Next() )
    {
      Handle(HYDROData_Zone) aRegZone =
        Handle(HYDROData_Zone)::DownCast( aZonesIter.Value() );
      if ( aRegZone.IsNull() )
        continue;

      PointClassification aPointRelation = GetPointClassification( thePoint, aRegZone );
      if ( aPointRelation != POINT_OUT )
        aResZone = aRegZone; // We found the desired zone
    }
  }

  return aResZone;
}

HYDROData_CalculationCase::PointClassification HYDROData_CalculationCase::GetPointClassification(
  const gp_XY&                  thePoint,
  const Handle(HYDROData_Zone)& theZone ) const
{
  PointClassification aRes = POINT_OUT;
  if ( theZone.IsNull() )
    return aRes;

  TopoDS_Face aZoneFace = TopoDS::Face( theZone->GetShape() );
  if ( aZoneFace.IsNull() )
    return aRes;
#ifdef DEB_CLASS2D
	  TopoDS_Compound aCmp;
      BRep_Builder aBB;
      aBB.MakeCompound(aCmp);
	  aBB.Add(aCmp, aZoneFace);
	  gp_Pnt aPnt (thePoint.X(), thePoint.Y(), 0.);
	  BRepBuilderAPI_MakeVertex aMk(aPnt);
	  aBB.Add(aCmp, aMk.Vertex());
	  BRepTools::Write(aCmp, "FCL2d.brep");
#endif
  TopAbs_State State = HYDROData_Tool::ComputePointState(thePoint, aZoneFace);
  if (State == TopAbs_OUT)
    aRes =  POINT_OUT;
  else if(State == TopAbs_IN)
    aRes =  POINT_IN;
  else if(State == TopAbs_ON)
    aRes =  POINT_ON;
  return aRes;
}

Handle(HYDROData_Region) HYDROData_CalculationCase::addNewRegion( const Handle(HYDROData_Document)& theDoc,
                                                                  const QString& thePrefixOrName,
                                                                  bool isPrefix )
{
  TDF_Label aNewLab = myLab.FindChild( DataTag_ChildRegion ).NewChild();
  int aTag = aNewLab.Tag();

  Handle(HYDROData_Region) aNewRegion =
    Handle(HYDROData_Region)::DownCast( HYDROData_Iterator::CreateObject( aNewLab, KIND_REGION ) );
  AddRegion( aNewRegion );

  QString aRegionName = isPrefix ? HYDROData_Tool::GenerateObjectName( theDoc, thePrefixOrName ) : thePrefixOrName;
  aNewRegion->SetName( aRegionName, true );

  return aNewRegion;
}

Handle(HYDROData_SplitShapesGroup) HYDROData_CalculationCase::addNewSplitGroup( const QString& theName )
{
  TDF_Label aNewLab = myLab.FindChild( DataTag_SplitGroups ).NewChild();

  Handle(HYDROData_SplitShapesGroup) aNewGroup =
    Handle(HYDROData_SplitShapesGroup)::DownCast(
      HYDROData_Iterator::CreateObject( aNewLab, KIND_SPLIT_GROUP ) );
  AddReferenceObject( aNewGroup, DataTag_SplitGroups );

  aNewGroup->SetName( theName );

  return aNewGroup;
}

void HYDROData_CalculationCase::SetContainerName( const QString& theContainerName )
{
#ifndef LIGHT_MODE
  HYDROData_GeomTool::SetContainerName( theContainerName );
#endif
}

QString HYDROData_CalculationCase::Export() const
{
#ifdef LIGHT_MODE
  return "";
#else
  GEOM::GEOM_Gen_var aGEOMEngine = HYDROData_GeomTool::GetGeomGen();

  QString aGeomObjEntry, anErrorMsg;
  QString statMess;
  bool isOK = Export( aGEOMEngine, aGeomObjEntry, anErrorMsg, statMess );
  return isOK ? aGeomObjEntry : QString();
#endif
}

#ifndef LIGHT_MODE
bool HYDROData_CalculationCase::Export( GEOM::GEOM_Gen_var  theGeomEngine,
                                        QString& theGeomObjEntry,
                                        QString& theErrorMsg,
                                        QString& statMess) const
{
  DEBTRACE("Export");
  HYDROData_ShapesGroup::SeqOfGroupsDefs aSeqOfGroupsDefs;

  // Get groups definitions
  HYDROData_SequenceOfObjects aSplitGroups = GetSplitGroups();

  TopTools_SequenceOfShape IntSh; //internal edges
  HYDROData_SequenceOfObjects::Iterator anIter( aSplitGroups );
  for ( ; anIter.More(); anIter.Next() )
  {
    // Get shapes group
    Handle(HYDROData_ShapesGroup) aGroup =
      Handle(HYDROData_ShapesGroup)::DownCast( anIter.Value() );
    if ( aGroup.IsNull() )
      continue;

    HYDROData_ShapesGroup::GroupDefinition aGroupDef;

    aGroupDef.Name = aGroup->GetName().toLatin1().constData();
    aGroup->GetShapes( aGroupDef.Shapes );

    aSeqOfGroupsDefs.Append( aGroupDef );

    Handle(HYDROData_SplitShapesGroup) aSSGroup = Handle(HYDROData_SplitShapesGroup)::DownCast( anIter.Value() );
    TopTools_SequenceOfShape dummy;
    if (!aSSGroup.IsNull())
      if (aSSGroup->GetInternal())
      {
        aSSGroup->GetShapes(dummy);
        IntSh.Append(dummy);
      }

  }

  // Get faces
  bool isAllNotSubmersible = true;
  HYDROData_SequenceOfObjects aCaseRegions = GetRegions();
  HYDROData_SequenceOfObjects::Iterator aRegionIter( aCaseRegions );
  NCollection_IndexedDataMap<TopoDS_Shape, QString> aShToNames;
  for ( ; aRegionIter.More(); aRegionIter.Next() )
  {
    Handle(HYDROData_Region) aRegion =
      Handle(HYDROData_Region)::DownCast( aRegionIter.Value() );
    if( aRegion.IsNull() || !aRegion->IsSubmersible() )
      continue;

    if ( isAllNotSubmersible )
      isAllNotSubmersible = false;

    TopoDS_Shape aRegionShape = aRegion->GetShape( &aSeqOfGroupsDefs, &IntSh );
    aShToNames.Add( aRegionShape, aRegion->GetName() );
  }

  bool aRes = false;

  if ( aCaseRegions.IsEmpty() ) {
    theErrorMsg = QString("the list of regions is empty.");
  } else if ( isAllNotSubmersible ) {
    theErrorMsg = QString("there are no submersible regions.");
  } else {
    aRes = Export( theGeomEngine, aShToNames, aSeqOfGroupsDefs, theGeomObjEntry );;
  }

  if( aRes && !GetLandCoverMap().IsNull() && !GetStricklerTable().IsNull() )
  {
    QString aTelemacFileName = GetName() + ".telemac";
    aRes = GetLandCoverMap()->ExportTelemac( aTelemacFileName, 1E-2, GetStricklerTable(), statMess );
    if (!aRes)
      theErrorMsg = QString( "The export to TELEMAC %1 failed" ).arg( aTelemacFileName );
  }
  return aRes;
}

bool HYDROData_CalculationCase::Export( GEOM::GEOM_Gen_var                            theGeomEngine,
                                        const NCollection_IndexedDataMap<TopoDS_Shape, QString>& aShToName,
                                        const HYDROData_ShapesGroup::SeqOfGroupsDefs& theGroupsDefs,
                                        QString& theGeomObjEntry ) const
{
  // Sew faces
  BRepBuilderAPI_Sewing aSewing( Precision::Confusion() * 10.0 );
  aSewing.SetNonManifoldMode( Standard_False );
#ifdef DEB_CALCULATION
  TCollection_AsciiString aNam("Sh_");
  int i=1;
#endif
  TopTools_DataMapOfShapeListOfShape SH2M;
  for ( int i = 1; i <= aShToName.Extent(); i++ )
  {
    const TopoDS_Shape& aShape = aShToName.FindKey(i);
    if ( aShape.IsNull() )
      continue;

    SH2M.Bind(aShape, TopTools_ListOfShape());
    TopTools_ListOfShape& LM = SH2M.ChangeFind(aShape);
    if ( aShape.ShapeType() == TopAbs_FACE || aShape.ShapeType() == TopAbs_SHELL )
    {
      aSewing.Add( aShape );
      LM.Append(aShape);
    }
    else if (aShape.ShapeType() == TopAbs_COMPOUND)
    {
      TopExp_Explorer anExp( aShape, TopAbs_SHELL );
      for (; anExp.More(); anExp.Next() )
      {
        aSewing.Add( anExp.Current() );
        LM.Append(anExp.Current());
      }
      anExp.Init( aShape, TopAbs_FACE, TopAbs_SHELL );
      for (; anExp.More(); anExp.Next() )
      {
        aSewing.Add( anExp.Current() );
        LM.Append(anExp.Current());
      }
    }
  } // faces iterator

  aSewing.Perform();
  TopoDS_Shape aSewedShape = aSewing.SewedShape();

  NCollection_IndexedDataMap<TopoDS_Shape, QString, TopTools_ShapeMapHasher> aFacesToNameModif;

  for ( int i = 1; i <= aShToName.Extent(); i++ )
  {
    const TopoDS_Shape& CurShape = aShToName.FindKey(i);
    const QString& Qstr = aShToName.FindFromIndex(i);
    const TopTools_ListOfShape& LM = SH2M(CurShape);
    for (TopTools_ListIteratorOfListOfShape it(LM); it.More(); it.Next())
    {
      const TopoDS_Shape& csh = it.Value();
      if (aSewing.IsModified(csh))
        aFacesToNameModif.Add(aSewing.Modified(csh), Qstr);
      else
        aFacesToNameModif.Add(csh, Qstr);
    }
  }


  // If the sewed shape is empty - return false
  if ( aSewedShape.IsNull() || !TopoDS_Iterator( aSewedShape ).More() )
    return false;

#ifdef DEB_CALCULATION
  BRepTools::Write(aSewedShape ,"Sew.brep");
#endif  // Publish the sewed shape
  QString aName = EXPORT_NAME;
  GEOM::GEOM_Object_ptr aMainShape =
    HYDROData_GeomTool::ExplodeShapeInGEOMandPublish( theGeomEngine, aSewedShape, aFacesToNameModif, aName, theGeomObjEntry );

  if ( aMainShape->_is_nil() )
    return false;

  if ( theGroupsDefs.IsEmpty() )
    return true;

  // Create groups
  TopTools_IndexedMapOfShape aMapOfSubShapes;
  TopExp::MapShapes( aSewedShape, aMapOfSubShapes );

  NCollection_DataMap< TCollection_AsciiString, NCollection_Sequence<int> > aGroupsData;

  for ( int aGrId = 1, nbGroups = theGroupsDefs.Length(); aGrId <= nbGroups; ++aGrId )
  {
    const HYDROData_ShapesGroup::GroupDefinition& aGroupDef = theGroupsDefs.Value( aGrId );

    NCollection_Sequence<int> aGroupIndexes;
    for( int i = 1, n = aGroupDef.Shapes.Length(); i <= n; i++ )
    {
      const TopoDS_Shape& aShape = aGroupDef.Shapes.Value( i );
#ifdef DEB_CALCULATION
      cout << "\nOld shape(" << i << ") = " << aShape.TShape() <<endl;
#endif

      TopoDS_Shape aModifiedShape = aShape;
      if ( aSewing.IsModified( aShape ) )
        aModifiedShape = aSewing.Modified( aShape );
      else if ( aSewing.IsModifiedSubShape( aShape ) )
        aModifiedShape = aSewing.ModifiedSubShape( aShape );

#ifdef DEB_CALCULATION
      const TopLoc_Location& aL1 = aShape.Location();
      const TopLoc_Location& aL2 = aModifiedShape.Location();
      cout << "\nNew shape(" << i << ") = " << aModifiedShape.TShape() << " Location is Equal = " << aL1.IsEqual(aL2)<<endl;
#endif

      int anIndex = aMapOfSubShapes.FindIndex(aModifiedShape);
      if ( anIndex > 0 ) {
        aGroupIndexes.Append( anIndex );
      } else {
#ifdef DEB_CALCULATION
        TCollection_AsciiString aNam("Lost_");
        if(!aMapOfSubShapes.Contains(aModifiedShape)) {
        for ( int anIndex = 1; anIndex <= aMapOfSubShapes.Extent(); anIndex++ )
        {
           const TopoDS_Shape& aS = aMapOfSubShapes.FindKey( anIndex );
           if ( aModifiedShape.IsPartner( aS ) )
           {
             cout <<"\nIndex in Map = " << anIndex << "TShape = " << aS.TShape() <<endl;
             TCollection_AsciiString aName = aNam + i + "_" + anIndex + ".brep";
             BRepTools::Write(aS ,aName.ToCString());
            break;
           }
         }
        }
#endif
      }
    }
    if ( !aGroupIndexes.IsEmpty() )
      aGroupsData.Bind( aGroupDef.Name, aGroupIndexes );
  }

  if ( !aGroupsData.IsEmpty() )
  {
    GEOM::GEOM_IGroupOperations_var aGroupOp =
      theGeomEngine->GetIGroupOperations();

    NCollection_DataMap< TCollection_AsciiString, NCollection_Sequence<int> >::Iterator aMapIt( aGroupsData );
    for ( ; aMapIt.More(); aMapIt.Next() )
    {
      const TCollection_AsciiString& aGroupName = aMapIt.Key();
      const NCollection_Sequence<int>& aGroupIndexes = aMapIt.Value();

      GEOM::GEOM_Object_var aGeomGroup = aGroupOp->CreateGroup( aMainShape, TopAbs_EDGE );
      if ( CORBA::is_nil( aGeomGroup ) || !aGroupOp->IsDone() )
        continue;

      GEOM::ListOfLong_var aGeomIndexes = new GEOM::ListOfLong;
      aGeomIndexes->length( aGroupIndexes.Length() );

      for( int i = 1, n = aGroupIndexes.Length(); i <= n; i++ )
        aGeomIndexes[ i - 1 ] = aGroupIndexes.Value( i );

      aGroupOp->UnionIDs( aGeomGroup, aGeomIndexes );
      if ( aGroupOp->IsDone() )
      {
        SALOMEDS::SObject_var aGroupSO =
          theGeomEngine->AddInStudy( aGeomGroup, aGroupName.ToCString(), aMainShape );
      }
    }
  }

  return true;
}
#endif

void HYDROData_CalculationCase::ClearRules( HYDROData_CalculationCase::DataTag theDataTag,
                                            const bool theIsSetToUpdate )
{
  TDF_Label aRulesLab = myLab.FindChild( theDataTag );
  HYDROData_PriorityQueue::ClearRules( aRulesLab );

  // Indicate model of the need to update splitting
  if ( theIsSetToUpdate ) {
    Changed( Geom_2d );
  }
}

void HYDROData_CalculationCase::AddRule( const Handle(HYDROData_Entity)&    theObject1,
                                         HYDROData_PriorityType             thePriority,
                                         const Handle(HYDROData_Entity)&    theObject2,
                                         HYDROData_Zone::MergeType          theMergeType,
                                         HYDROData_CalculationCase::DataTag theDataTag )
{
  TDF_Label aRulesLab = myLab.FindChild( theDataTag );
  HYDROData_PriorityQueue::AddRule( aRulesLab, theObject1, thePriority, theObject2, theMergeType );

  // Indicate model of the need to update splitting
  Changed( Geom_2d );
}

QString HYDROData_CalculationCase::DumpRules() const
{
  TDF_Label aRulesLab = myLab.FindChild( DataTag_CustomRules );
  return HYDROData_PriorityQueue::DumpRules( aRulesLab );
}

void HYDROData_CalculationCase::SetAssignmentMode( AssignmentMode theMode )
{
  TDF_Label aModeLab = myLab.FindChild( DataTag_AssignmentMode );
  Handle(TDataStd_Integer) anAttr = TDataStd_Integer::Set( aModeLab, ( int ) theMode );
  anAttr->SetID(TDataStd_Integer::GetID());
  // Indicate model of the need to update splitting
  Changed( Geom_2d );
}

HYDROData_CalculationCase::AssignmentMode HYDROData_CalculationCase::GetAssignmentMode() const
{
  Handle(TDataStd_Integer) aModeAttr;
  bool isOK = myLab.FindChild( DataTag_AssignmentMode ).FindAttribute( TDataStd_Integer::GetID(), aModeAttr );
  if( isOK )
    return ( AssignmentMode ) aModeAttr->Get();
  else
    return MANUAL;
}

void HYDROData_CalculationCase::DumpRulesToPython( const QString& theCalcCaseName,
                                                   QStringList& theScript ) const
{
  TDF_Label aRulesLab = myLab.FindChild( DataTag_CustomRules );
  HYDROData_PriorityQueue::DumpRulesToPython( aRulesLab, theCalcCaseName, theScript );
}

HYDROData_Warning HYDROData_CalculationCase::GetLastWarning() const
{
  return myLastWarning;
}

void HYDROData_CalculationCase::SetWarning( HYDROData_WarningType theType, const QString& theData )
{
  myLastWarning.Type = theType;
  myLastWarning.Data = theData;
}

void HYDROData_CalculationCase::UpdateRegionsNames( const HYDROData_SequenceOfObjects& theRegions,
                                                    const QString& theOldCaseName,
                                                    const QString& theName )
{
  HYDROData_SequenceOfObjects::Iterator anIter( theRegions );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Region) aRegion =
      Handle(HYDROData_Region)::DownCast( anIter.Value() );
    if ( aRegion.IsNull() )
      continue;

    HYDROData_Tool::UpdateChildObjectName( theOldCaseName, theName, aRegion );

    HYDROData_SequenceOfObjects aZones = aRegion->GetZones();
    HYDROData_SequenceOfObjects::Iterator anIter( aZones );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Zone) aRegZone =
        Handle(HYDROData_Zone)::DownCast( anIter.Value() );
      if ( aRegZone.IsNull() )
        continue;

      HYDROData_Tool::UpdateChildObjectName( theOldCaseName, theName, aRegZone );
    }
  }
}

void HYDROData_CalculationCase::DumpRegionsToPython( QStringList& theResList,
                                                     const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects,
                                                     const HYDROData_SequenceOfObjects& theRegions ) const
{
  HYDROData_SequenceOfObjects::Iterator anIter;
  anIter.Init(theRegions);
  for (int ireg = 1; anIter.More(); anIter.Next(), ireg++)
    {
      Handle(HYDROData_Region) aRegion = Handle(HYDROData_Region)::DownCast(anIter.Value());
      if (aRegion.IsNull())
        continue;
      QString defRegName = this->GetName();
      QString regSuffix = QString("_Reg_%1").arg(ireg);
      defRegName += regSuffix;
      theTreatedObjects.insert(aRegion->GetName(), aRegion);
      QStringList aRegDump = aRegion->DumpToPython(thePyScriptPath, theTreatedObjects, defRegName);
      theResList << aRegDump;
    }
  for (anIter.Init(theRegions); anIter.More(); anIter.Next())
    {
      Handle(HYDROData_Region) aRegion = Handle(HYDROData_Region)::DownCast(anIter.Value());
      if (aRegion.IsNull())
        continue;
      QStringList aRegDump;
      aRegion->SetNameInDumpPython(aRegDump);
      theResList << aRegDump;
    }
}

bool HYDROData_CalculationCase::GetRule( int theIndex,
                                         Handle(HYDROData_Entity)&           theObject1,
                                         HYDROData_PriorityType&             thePriority,
                                         Handle(HYDROData_Entity)&           theObject2,
                                         HYDROData_Zone::MergeType&          theMergeType,
                                         HYDROData_CalculationCase::DataTag& theDataTag) const
{
  TDF_Label aRulesLab = myLab.FindChild( theDataTag );
  return HYDROData_PriorityQueue::GetRule( aRulesLab, theIndex,
    theObject1, thePriority, theObject2, theMergeType );
}

bool HYDROData_CalculationCase::AddInterPoly( const Handle(HYDROData_PolylineXY)& theInterPolyline )
{
  HYDROData_CalculationCase::DataTag aDataTag = DataTag_InterPoly;

  if ( HasReference( theInterPolyline, aDataTag ) )
    return false;

  AddReferenceObject( theInterPolyline, aDataTag );

  Changed( Geom_2d );

  return true;
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetInterPolyObjects() const
{
  return GetReferenceObjects( DataTag_InterPoly );
}

void HYDROData_CalculationCase::RemoveInterPolyObject( const Handle(HYDROData_PolylineXY)& theInterPolyline )
{
  if ( theInterPolyline.IsNull() )
    return;

  RemoveReferenceObject( theInterPolyline->Label(), DataTag_InterPoly );

  Changed( Geom_2d );
}

bool HYDROData_CalculationCase::AddBoundaryPolygon( const Handle(HYDROData_BCPolygon)& theBCPolygon )
{
  HYDROData_CalculationCase::DataTag aDataTag = DataTag_BCPolygon;

  if ( HasReference( theBCPolygon, aDataTag ) )
    return false;

  AddReferenceObject( theBCPolygon, aDataTag );

  Changed( Geom_2d );

  return true;
}

HYDROData_SequenceOfObjects HYDROData_CalculationCase::GetBoundaryPolygons() const
{
  return GetReferenceObjects( DataTag_BCPolygon );
}

void HYDROData_CalculationCase::RemoveBoundaryPolygon( const Handle(HYDROData_BCPolygon)& theBCPolygon )
{
  if ( theBCPolygon.IsNull() )
    return;

  RemoveReferenceObject( theBCPolygon->Label(), DataTag_BCPolygon );

  Changed( Geom_2d );
}





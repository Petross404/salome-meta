// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Zone_HeaderFile
#define HYDROData_Zone_HeaderFile

#include "HYDROData_Entity.h"
#include "HYDROData_IInterpolator.h"

class HYDROData_LandCover;
class TopoDS_Shape;


/**\class HYDROData_Zone
 * \brief Class that stores/retreives information about the 2d face.
 */
class HYDROData_Zone : public HYDROData_Entity
{

public:
  // Enumeration of mergin types for conflict altitudes/types
  enum MergeType
  {
    Merge_UNKNOWN, // Undefined
    Merge_ZMIN,    // The minimum values
    Merge_ZMAX,    // The maximum values
    Merge_Object   // Only one altitude/land cover will be taken into account
  };

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Shape,           ///< reference shape
    DataTag_Object,          ///< reference objects
    DataTag_MergeType,       ///< mergin type of conflict bathymetries/types
    DataTag_MergeObject,     ///< reference altitude/land cover for conflict merge
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Zone, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_ZONE; }


  /**
   * Returns flag indicating that object is updateble or not.
   */
  HYDRODATA_EXPORT virtual bool CanBeUpdated() const;


  /**
   * Checks that object has 2D presentation. Reimlemented to retun true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;


  /**
   * Returns flag indicating that object can be removed or not.
   */
  HYDRODATA_EXPORT virtual bool CanRemove();

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;


  /**
   * Sets the shape of the zone.
   */
  HYDRODATA_EXPORT virtual void SetShape( const TopoDS_Shape& theShape );

  /**
   * Returns the shape of the zone.
   */
  HYDRODATA_EXPORT virtual TopoDS_Shape GetShape() const;


  /**
   * Returns true if zone needs merge of bathymetries.
   */
  HYDRODATA_EXPORT virtual bool IsMergingNeed() const;


  /**
   * Sets the interpolator for zone. By default it is NULL and original
   * interpolation algorithms are used to calculate points altitudes.
   * If you set interpolator it won't be stored in the data model structure,
   * it will be deleted during that time as this zone will be freed.
   */
  HYDRODATA_EXPORT virtual void SetInterpolator( HYDROData_IInterpolator* theInter );

  /**
   * * Returns the interpolator of zone object.
   */
  HYDRODATA_EXPORT virtual HYDROData_IInterpolator* GetInterpolator() const;
  
  /**
   * Sets the merging type for conflict altitudes.
   * By default it is set to Merge_UNKNOWN.
   */
  HYDRODATA_EXPORT virtual void SetMergeType( const MergeType& theType );

  /**
   * Returns the merging type for conflict altitudes/types.
   */
  HYDRODATA_EXPORT virtual MergeType GetMergeType() const;


  /**
   * Sets the reference object to resolve the conflict.
   * This object is used only in case of "Merge_Object" merge type.
   * \param theObject the merge object
   */
  HYDRODATA_EXPORT virtual void SetMergeObject( const Handle(HYDROData_Entity)& theObject );

  /**
   * Returns the reference object to resolve the conflict.
   * \return the merge object
   */
  HYDRODATA_EXPORT virtual Handle(HYDROData_Entity) GetMergeObject() const;

  /**
   * Removes the reference object for resolving the conflict.
   */
  HYDRODATA_EXPORT virtual void RemoveMergeObject();
  
  /**
   * Add new one object for zone.
   * \param theObject the object to add
   */
  HYDRODATA_EXPORT virtual bool AddObject( const Handle(HYDROData_Entity)& theObject );

  /**
   * Returns all objects of zone.
   * \return the list of objects
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetObjects() const;

  /**
   * Removes all objects from zone.
   */
  HYDRODATA_EXPORT virtual void RemoveObjects();

  /**
   * Returns submersible flag.
   * \return true if all objects of the zone is submersible
   */
  HYDRODATA_EXPORT bool IsSubmersible() const;

protected:

  friend class HYDROData_Region;
  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Zone();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_Zone();

private:
  HYDROData_IInterpolator* myInterpolator;

};

#endif

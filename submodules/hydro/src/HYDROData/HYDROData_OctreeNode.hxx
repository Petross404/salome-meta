// Copyright (C) 2007-2014  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  HYDROData HYDROData_OctreeNode : Octree with Nodes set (from SMESH module)
//  inherites global class HYDROData_Octree
//  File      : HYDROData_OctreeNode.hxx
//  Created   : Tue Jan 16 16:00:00 2007
//  Author    : Nicolas Geimer & Aurelien Motteux  (OCC)
//  Module    : HYDROData
//
#ifndef _HYDROData_OCTREENODE_HXX_
#define _HYDROData_OCTREENODE_HXX_

#include <Standard.hxx>
#include <Standard_Macro.hxx>

#include "HYDROData_Octree.hxx"
#include <gp_Pnt.hxx>

#include <list>
#include <set>
#include <vector>
#include <map>

//forward declaration
class HYDROData_OctreeNode;
class gp_XYZ;

typedef gp_XYZ Node3D;
//typedef NCollection_Sequence<Node3D> Nodes3D;
typedef std::list<const Node3D*> Nodes3D;

class Standard_EXPORT HYDROData_OctreeNode : public HYDROData_Octree {

public:

  // Constructor
  HYDROData_OctreeNode (const Nodes3D& theNodes,  const int maxLevel = 8,
                        const int maxNbNodes = 5, const double minBoxSize = 0.);

//=============================
/*!
 * \brief Empty destructor
 */
//=============================
  virtual ~HYDROData_OctreeNode () {};

  // Tells us if Node is inside the current box with the precision "precision"
  virtual const bool isInside(const gp_XYZ& p, const double precision = 0.);

  // Return in Result a list of Nodes potentials to be near Node
  void               NodesAround(const Node3D *            Node,
                                 std::list<const Node3D*>* Result,
                                 const double              precision = 0.);

  // Return in dist2Nodes nodes mapped to their square distance from Node
  bool               NodesAround(const gp_XYZ&                    node,
                                 std::map<double, const Node3D*>& dist2Nodes,
                                 double                           precision);

  // Return in theGroupsOfNodes a list of group of nodes close to each other within theTolerance
  // Search for all the nodes in nodes
//  void               FindCoincidentNodes ( TIDSortedNodeSet*           nodes,
//                                           const double                theTolerance,
//                                           std::list< std::list< const Node3D*> >* theGroupsOfNodes);

  // Static method that return in theGroupsOfNodes a list of group of nodes close to each other within
  // theTolerance search for all the nodes in nodes
//  static void        FindCoincidentNodes ( TIDSortedNodeSet&                              nodes,
//                                           std::list< std::list< const Node3D*> >* theGroupsOfNodes,
//                                           const double theTolerance = 0.00001,
//                                           const int maxLevel = -1,
//                                           const int maxNbNodes = 5);
  /*!
   * \brief Update data according to node movement
   */
  void                        UpdateByMoveNode( const Node3D* node, const gp_Pnt& toPnt );
  /*!
   * \brief Return iterator over children
   */
//  HYDROData_OctreeNodeIteratorPtr GetChildrenIterator();
  /*!
   * \brief Return nodes iterator
   */
  //SMDS_NodeIteratorPtr        GetNodeIterator();
  /*!
   * \brief Return nb nodes in a tree
   */
  int                         NbNodes() const { return myNodes.size(); }

protected:

  struct Limit : public HYDROData_TreeLimit
  {
    int myMaxNbNodes;
    Limit(int maxLevel, double minSize, int maxNbNodes)
      :HYDROData_TreeLimit(maxLevel, minSize), myMaxNbNodes(maxNbNodes) {}
  };

  int                   getMaxNbNodes() const;

  HYDROData_OctreeNode();

  // Compute the bounding box of the whole set of nodes myNodes
  virtual Bnd_B3d*      buildRootBox();

  // Shares the father's data with each of his child
  virtual void          buildChildrenData();

  // Construct an empty HYDROData_OctreeNode used by HYDROData_Octree::buildChildren()
  virtual HYDROData_Octree* newChild() const;

//  // Return in result a list of nodes closed to Node and remove it from SetOfNodes
//  void                  FindCoincidentNodes( const Node3D *            Node,
//                                             TIDSortedNodeSet*                SetOfNodes,
//                                             std::list<const Node3D*>* Result,
//                                             const double                     precision);

  // The set of nodes inside the box of the Octree (Empty if Octree is not a leaf)
  Nodes3D   myNodes;

};

#endif

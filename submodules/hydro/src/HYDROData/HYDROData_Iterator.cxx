// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_Iterator.h"

#include "HYDROData_AltitudeObject.h"
#include "HYDROData_Bathymetry.h"
#include "HYDROData_CalculationCase.h"
#include "HYDROData_Channel.h"
#include "HYDROData_Confluence.h"
#include "HYDROData_Digue.h"
#include "HYDROData_DummyObject3D.h"
#include "HYDROData_Image.h"
#include "HYDROData_ImmersibleZone.h"
#include "HYDROData_LandCoverMap.h"
#include "HYDROData_Obstacle.h"
#include "HYDROData_ObstacleAltitude.h"
#include "HYDROData_ChannelAltitude.h"
#include "HYDROData_Polyline3D.h"
#include "HYDROData_PolylineXY.h"
#include "HYDROData_Profile.h"
#include "HYDROData_ProfileUZ.h"
#include "HYDROData_Region.h"
#include "HYDROData_River.h"
#include "HYDROData_ShapesGroup.h"
#include "HYDROData_SplitShapesGroup.h"
#include "HYDROData_Stream.h"
#include "HYDROData_StreamAltitude.h"
#include "HYDROData_VisualState.h"
#include "HYDROData_Zone.h"
#include "HYDROData_StricklerTable.h"
#include "HYDROData_DTM.h"
#include <HYDROData_LISM.h>
#include "HYDROData_BCPolygon.h"
#include <TDataStd_Name.hxx>
#include <TDataStd_NamedData.hxx>

#include <NCollection_DataMap.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

//! Returns label by root objects kind and the kind of the object
static TDF_Label GetLabelByKind(TDF_Label theRoot, ObjectKind theKind)
{
  if (theKind == KIND_UNKNOWN) return theRoot;
  return theRoot.FindChild(theKind);
}

HYDROData_Iterator::HYDROData_Iterator( const Handle(HYDROData_Document)& theDoc,
                                        const ObjectKind                  theKind )
: myIter( GetLabelByKind( theDoc->LabelOfObjects(), theKind ), 
          TDataStd_Name::GetID(), theKind == KIND_UNKNOWN ) // iterate all sub-objects for unknown kind
{
}

void HYDROData_Iterator::Next()
{
  myIter.Next();
  // omit the properties iteration in case of UNKNOWN kind filtering
  while ( myIter.More() && myIter.Value()->Label().Depth() < 4 )
    myIter.Next();
}

bool HYDROData_Iterator::More() const
{
  return myIter.More();
}

Handle(HYDROData_Entity) HYDROData_Iterator::Current()
{
  return Object(myIter.Value()->Label());
}

Handle(HYDROData_Entity) HYDROData_Iterator::CreateObject(
  const Handle(HYDROData_Document)& theDoc,
  const ObjectKind&                 theObjectKind )
{
  TDF_Label aNewLab = 
    GetLabelByKind( theDoc->LabelOfObjects(), theObjectKind ).FindChild( theDoc->NewID() );
  return CreateObject( aNewLab, theObjectKind );
}

Handle(HYDROData_Entity) HYDROData_Iterator::CreateObject( TDF_Label&        theNewLabel, 
                                                           const ObjectKind& theObjectKind )
{
  // Object exists if there is a name attribute
  TDataStd_Name::Set( theNewLabel, "" );

  // Store the type of object in data label
  TDataStd_NamedData::Set( theNewLabel );

  Handle(TDataStd_NamedData) aNamedData;
  theNewLabel.FindAttribute( TDataStd_NamedData::GetID(), aNamedData );
  aNamedData->SetInteger( "ObjectKind", theObjectKind ); //??CAF

  return Object( theNewLabel );
}

Handle(HYDROData_Entity) HYDROData_Iterator::Object( const TDF_Label& theLabel )
{
  Handle(HYDROData_Entity) aResult;

  // If label has no name attribute it mean that this is not object or
  // this object has been removed from document
  Handle(TDataStd_Name) aNameAtt;
  if ( !theLabel.FindAttribute( TDataStd_Name::GetID(), aNameAtt ) )
    return aResult;

  ObjectKind aKind = KIND_UNKNOWN;

  // Retrieve the type of object from label
  Handle(TDataStd_NamedData) aNamedData;
  if ( theLabel.FindAttribute( TDataStd_NamedData::GetID(), aNamedData ) )
    aKind = aNamedData->GetInteger( "ObjectKind" );

  if ( aKind == KIND_UNKNOWN )
    aKind = theLabel.Father().Tag(); // Try to get type from father label
  
  switch( aKind )
  {
    DEBTRACE("HYDROData_Iterator::Object " << aKind);
    case KIND_IMAGE:              aResult = new HYDROData_Image();                break;
    case KIND_POLYLINE:           aResult = new HYDROData_Polyline3D();           break;
    case KIND_BATHYMETRY:         aResult = new HYDROData_Bathymetry();           break;
    case KIND_ALTITUDE:           aResult = new HYDROData_AltitudeObject();       break;
    case KIND_IMMERSIBLE_ZONE:    aResult = new HYDROData_ImmersibleZone();       break;
    case KIND_RIVER:              aResult = new HYDROData_River();                break;
    case KIND_STREAM:             aResult = new HYDROData_Stream();               break;
    case KIND_CONFLUENCE:         aResult = new HYDROData_Confluence();           break;
    case KIND_CHANNEL:            aResult = new HYDROData_Channel();              break;
    case KIND_OBSTACLE:           aResult = new HYDROData_Obstacle();             break;
    case KIND_DIGUE:              aResult = new HYDROData_Digue();                break;
    case KIND_PROFILE:            aResult = new HYDROData_Profile();              break;
    case KIND_PROFILEUZ:          aResult = new HYDROData_ProfileUZ();            break;
    case KIND_POLYLINEXY:         aResult = new HYDROData_PolylineXY();           break;
    case KIND_CALCULATION:        aResult = new HYDROData_CalculationCase();      break;
    case KIND_REGION:             aResult = new HYDROData_Region();               break;
    case KIND_ZONE:               aResult = new HYDROData_Zone();                 break;
    case KIND_VISUAL_STATE:       aResult = new HYDROData_VisualState();          break;
    case KIND_DUMMY_3D:           aResult = new HYDROData_DummyObject3D();        break;
    case KIND_SHAPES_GROUP:       aResult = new HYDROData_ShapesGroup();          break;
    case KIND_SPLIT_GROUP:        aResult = new HYDROData_SplitShapesGroup();     break;
    case KIND_STREAM_ALTITUDE:    aResult = new HYDROData_StreamAltitude();       break;
    case KIND_OBSTACLE_ALTITUDE:  aResult = new HYDROData_ObstacleAltitude();     break;
    case KIND_STRICKLER_TABLE:    aResult = new HYDROData_StricklerTable();       break;
    case KIND_LAND_COVER_OBSOLETE: break;
    case KIND_CHANNEL_ALTITUDE:   aResult = new HYDROData_ChannelAltitude();      break;
    case KIND_LAND_COVER_MAP:     aResult = new HYDROData_LandCoverMap();         break;
    case KIND_DTM:                aResult = new HYDROData_DTM();                  break;
    case KIND_BC_POLYGON:         aResult = new HYDROData_BCPolygon();            break;
    case KIND_LISM:               aResult = new HYDROData_LISM();                  break;
    default:                       break;
  }

  if ( !aResult.IsNull() )
    aResult->SetLabel( theLabel );

  return aResult;
}

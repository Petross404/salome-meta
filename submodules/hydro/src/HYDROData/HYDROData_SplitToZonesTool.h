// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_SplitToZonesTool_H
#define HYDROData_SplitToZonesTool_H

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#include <HYDROData_Object.h>
#include <HYDROData_Transform.h>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Compound.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <TopTools_ShapeMapHasher.hxx>
#include <NCollection_IndexedMap.hxx>
#include <TopTools_IndexedDataMapOfShapeShape.hxx>
#include <TopTools_IndexedDataMapOfShapeListOfShape.hxx>
#include <TopTools_ListOfShape.hxx>
#include <QStringList>
#include <NCollection_DataMap.hxx>

class HYDROData_PolylineXY;

/**
 * \class HYDROData_SplitToZonesTool
 * \brief This class contains methods used for splitting geometry objects
 *        into non-intersected regions.
 */
class HYDRODATA_EXPORT HYDROData_SplitToZonesTool
{
public:
  struct HYDRODATA_EXPORT SplitData
  {
    enum SplitObjectType
    {
      Data_None,
      Data_Zone,
      Data_Edge,
      Data_IntEdge
    };

    TopoDS_Shape    Shape;
    QStringList     ObjectNames;
    SplitObjectType Type;

    SplitData() :
     Type( Data_None ) {}
    
    SplitData( const SplitObjectType& theType,
               const TopoDS_Shape&    theShape,
               const QStringList&     theObjectNames ) :
      Type( theType ), Shape( theShape ), ObjectNames( theObjectNames ) {}

    SplitData( const SplitObjectType& theType,
               const TopoDS_Shape&    theShape,
               const QString&         theObjectName ) :
      Type( theType ), Shape( theShape ), ObjectNames( theObjectName ) {}

    TopoDS_Face Face() const;
  };

  typedef QList        <SplitData> SplitDataList;
  typedef QListIterator<SplitData> SplitDataListIterator;  
  typedef NCollection_IndexedMap<TopoDS_Shape, TopTools_ShapeMapHasher> HYDROData_MapOfShape;
  typedef HYDROData_MapOfShape::Iterator HYDROData_MapIteratorOfMapOfShape;
  typedef NCollection_IndexedDataMap<TopoDS_Shape, TopTools_ListOfShape, TopTools_ShapeMapHasher> HYDROData_DataMapOfShapeListOfShape;
  typedef HYDROData_DataMapOfShapeListOfShape::Iterator HYDROData_DataMapIteratorOfDataMapOfShapeListOfShape;
  typedef NCollection_IndexedDataMap<TopoDS_Shape,  QStringList, TopTools_ShapeMapHasher> HYDROData_DataMapOfShapeListOfString;
  typedef HYDROData_DataMapOfShapeListOfString::Iterator HYDROData_DataMapIteratorOfDataMapOfShapeListOfString;
  #undef _NCollection_MapHasher

public:

  static SplitDataList       Split( const HYDROData_SequenceOfObjects&  theObjectList,
                                    const HYDROData_SequenceOfObjects&  theGroupsList,
                                    const Handle(HYDROData_PolylineXY)& thePolyline,
                                    const HYDROData_SequenceOfObjects& InterPolys );

  static SplitDataList       Split( const HYDROData_SequenceOfObjects&  theObjectList );

  static void AddInternalEdges(HYDROData_DataMapOfShapeListOfShape& DM,
                               const HYDROData_SequenceOfObjects& thePolylines,
                               NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher>* OutNE);

  static void CutFaceByEdges(const TopoDS_Face& in, 
                             TopTools_ListOfShape& out, 
                             const HYDROData_SequenceOfObjects& thePolylines,
                             NCollection_DataMap<TopoDS_Shape, Handle(HYDROData_PolylineXY), TopTools_ShapeMapHasher>* OutNE,
                             TopTools_IndexedDataMapOfShapeListOfShape* OutOrSh2M);

  static int CutByEdges(const TopoDS_Shape& InSh, const TopTools_ListOfShape& InW,
                        TopTools_ListOfShape& outShs,
                        bool OutShapeAsRes,
                        TopTools_IndexedDataMapOfShapeShape* OutNE2OE,
                        TopTools_IndexedDataMapOfShapeListOfShape* OInSH2MSH);

  static void SetFileNames(const QString& theNameBefore, const QString& theNameAfter);

  static Standard_Boolean buildLimFace(const TopoDS_Wire& theBndWire, TopoDS_Face& outFace);


private:

  /**
   * Split input faces.
   */
  static bool SplitFaces(const TopoDS_Compound& theComp, HYDROData_Transform& theTool);
};

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif


#endif

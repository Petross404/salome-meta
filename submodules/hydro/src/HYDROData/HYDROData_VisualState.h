// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_VisualState_HeaderFile
#define HYDROData_VisualState_HeaderFile

#include <HYDROData_Entity.h>

/**\class HYDROData_VisualState
 * \brief Class that stores/retreives information about the visual state.
 *
 * Keeps the visual state as encoded text string.
 */
class HYDROData_VisualState : public HYDROData_Entity
{
protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_State ///< encoded visual state
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_VisualState, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const { return KIND_VISUAL_STATE; }

  /**
   * Stores the encoded visual state
   * \param theState new state
   */
  HYDRODATA_EXPORT void SetState( const std::string& theState );

  /**
   * Returns the encoded visual state
   */
  HYDRODATA_EXPORT std::string GetState() const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDROData_VisualState();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  ~HYDROData_VisualState();

};

#endif

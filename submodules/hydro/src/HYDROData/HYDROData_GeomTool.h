// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_GeomTool_HeaderFile
#define HYDROData_GeomTool_HeaderFile

#include "HYDROData.h"
#include <NCollection_IndexedDataMap.hxx>
#include <TopTools_ShapeMapHasher.hxx>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#ifndef LIGHT_MODE
#include <GEOM_Client.hxx>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

class TopoDS_Shape;
class QString;

class HYDRODATA_EXPORT HYDROData_GeomTool {

public:

  /**
   * \brief Set the Container Name to use when not default (FactoryServer).
   * \param theContainerName the name of the container used for GEOM 
   *        (to be used by scripts for distributed execution)
   */
  static void SetContainerName( const QString& theContainerName );
  
  /**
   * \brief Get shape by the specified IOR.
   * \param theIOR the GEOM object IOR
   * \return the shape
   */
  static TopoDS_Shape GetShapeFromIOR( const QString& theIOR );

  /**
   * \brief Get GEOM engine.
   * \return the GEOM engine
   */
  static GEOM::GEOM_Gen_var GetGeomGen();

  /**
   * \brief Get Study.
   * \return the study
   */
  static SALOMEDS::Study_var GetStudy();

  /**
   * \brief Get free name for GEOM object.
   * \param theBaseName the base name
   * \return the default name
   */
  static QString GetFreeName( const QString& theBaseName );

  /**
   * Publish the given shape in GEOM as a GEOM object.
   * \param theGeomEngine GEOM module engine
   * \param theShape the shape to publish as a GEOM object
   * \param theName the name of the published object
   * \param theGeomObjEntry the entry of the published object
   * \return the published GEOM object
   */
  static GEOM::GEOM_Object_ptr publishShapeInGEOM( GEOM::GEOM_Gen_var  theGeomEngine,
                                                   const TopoDS_Shape& theShape, 
                                                   const QString&      theName,
                                                   QString& theGeomObjEntry );

  /**
   * Create and publish face in GEOM as a GEOM object.
   * \param theGeomEngine GEOM module engine
   * \param theWidth the face width
   * \param theHeight the face height
   * \param theName the name of the published face
   * \param theFaceEntry the entry of the published face
   * \return the published GEOM object
   */
  static GEOM::GEOM_Object_ptr createFaceInGEOM( GEOM::GEOM_Gen_var theGeomEngine,
                                                 const int theWidth,
                                                 const int theHeight,
                                                 const QString& theName,
                                                 QString& theFaceEntry );

  /**
   * Publish the given GEOM object in the study.
   * \param theGeomEngine GEOM module engine
   * \param theGeomObj the GEOM object
   * \param theName the object name
   * \return the entry of the published object (empty string in case of fail)
   */
  static QString publishGEOMObject( GEOM::GEOM_Gen_var theGeomEngine,
                                    GEOM::GEOM_Object_ptr theGeomObj,
                                    const QString& theName );

  static GEOM::GEOM_Object_ptr ExplodeShapeInGEOMandPublish( GEOM::GEOM_Gen_var theGeomEngine, 
                                                      const TopoDS_Shape& theShape, 
                                                      const NCollection_IndexedDataMap<TopoDS_Shape, QString, TopTools_ShapeMapHasher>& aShToNameModif,
                                                      const QString& theName,
                                                      QString& theGeomObjEntry);

protected:
  static QString myContainerName;
};

#endif

#endif



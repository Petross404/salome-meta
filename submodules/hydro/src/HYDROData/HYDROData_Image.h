// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_Image_HeaderFile
#define HYDROData_Image_HeaderFile

#include <HYDROData_Entity.h>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#include <QImage>
#include <QTransform>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

/**\class HYDROData_Image
 * \brief Class that stores/retreives information about the image.
 *
 * Keeps image as binary array, transformation and other properties
 * of image with correspondent API for forkind wit hthese properties.
 */
class HYDROData_Image : public HYDROData_Entity
{

public:

  enum TransformationMode
  {
    ManualGeodesic = 0,
    ManualCartesian,
    CartesianFromFile,
    ReferenceImage
  };

  enum ECW_CellUnits
  {
    ECW_CellUnits_Meters = 1,
    ECW_CellUnits_Deg = 2,
    ECW_CellUnits_Feet = 3,
    ECW_CellUnits_Unknown = -1
  };

  struct ECW_FileInfo
  {
    ECW_CellUnits myCellSizeUnits;
    double myCellIncrementX;
    double myCellIncrementY;
    double myOriginX;
    double myOriginY;
    int myXSize;
    int myYSize;
  };

protected:
  /**
   * Enumeration of tags corresponding to the persistent object parameters.
   */
  enum DataTag
  {
    DataTag_First = HYDROData_Entity::DataTag_First + 100, ///< first tag, to reserve
    DataTag_Operator,        ///< name of the operator that must be executed for image update
    DataTag_TrsfMode,        ///< transformation mode (0 - Lambert93, 1 - Cartesian, 2 - on other Image)
    DataTag_TrsfPoints,      ///< image transformation points 
    DataTag_TrsfImage,      ///< reference transformation image
    DataTag_FilePath         ///< image imported file path
  };

public:
  DEFINE_STANDARD_RTTIEXT(HYDROData_Image, HYDROData_Entity);

  /**
   * Returns the kind of this object. Must be redefined in all objects of known type.
   */
  HYDRODATA_EXPORT virtual const ObjectKind GetKind() const {return KIND_IMAGE;}

  /**
   * Dump Image object to Python script representation.
   */
  HYDRODATA_EXPORT virtual QStringList DumpToPython( const QString& thePyScriptPath,
                                                     MapOfTreatedObjects& theTreatedObjects ) const;

  /**
   * Updates object state.
   * Reimplemented to update an Image object in the data structure.
   * Call this method whenever you made changes for operator or reference objects.
   * If it is changed, sets "MustBeUpdated" flag to other depended images.
   */
  HYDRODATA_EXPORT virtual void Update();

  /**
   * Checks that object has 2D presentation. Reimlemented to retun true.
   */
  HYDRODATA_EXPORT virtual bool IsHas2dPrs() const;

  /**
   * Returns data of object wrapped to QVariant.
   * Reimplemented to wrap and return saved image.
   * Transformation are applied to result image.
   */
  HYDRODATA_EXPORT virtual QVariant GetDataVariant();

  /**
   * Returns the list of all reference objects of this object.
   */
  HYDRODATA_EXPORT virtual HYDROData_SequenceOfObjects GetAllReferenceObjects() const;


  /**
   * Stores the image
   * \param theImage new image
   */
  HYDRODATA_EXPORT void SetImage(const QImage& theImage);

  /**
   * Load the image from file
   * \param theFilePath path to image
   */
  HYDRODATA_EXPORT bool LoadImage(const QString& theFilePath);

  /**
   * Load the ECW image from file
   * \param theFilePath path to image
   */
  HYDRODATA_EXPORT bool LoadImageECW( const QString& theFilePath );


  HYDRODATA_EXPORT static bool OpenECW(char* theFileName, QImage& theImage, ECW_FileInfo* theECWInfo);

  /**
   * Returns the kept image
   */
  HYDRODATA_EXPORT QImage Image();

  /**
   * Stores the image file path
   * \param theFilePath image file path
   */
  HYDRODATA_EXPORT void SetFilePath(const QString& theFilePath);

  /**
   * Returns uploaded image file path
   */
  HYDRODATA_EXPORT QString GetFilePath() const;

  /**
   * Stores the image transformation
   * \param theTrsf new transformation
   */
  HYDRODATA_EXPORT void SetTrsf(const QTransform& theTrsf);

  /**
   * Returns the kept transformation, or "identity" if not yet stored
   */
  HYDRODATA_EXPORT QTransform Trsf() const;

  /**
   * Updates the matrix of transformation in accordance with reference points.
   * \param theTrsf new transformation
   */
  HYDRODATA_EXPORT void UpdateTrsf();


  /**
   * Returns true if transformation is done by two points only
   */
  HYDRODATA_EXPORT bool IsByTwoPoints() const;


  /**
   * Removes all references from this image.
   */
  HYDRODATA_EXPORT bool HasReferences() const;

  /**
   * Removes all references from this image.
   */
  HYDRODATA_EXPORT void RemoveAllReferences();


  /**
   * Stores the transformation points in local cs of image
   * \param thePointA point A
   * \param thePointB point B
   * \param thePointC point C
   */
  HYDRODATA_EXPORT void SetLocalPoints( const QPoint& thePointA,
                                        const QPoint& thePointB,
                                        const QPoint& thePointC = QPoint( INT_MIN, INT_MIN ),
                                        const bool    theIsUpdate = true );

  /**
   * Returns the transformation points in local cs of image
   * \param thePointA point A
   * \param thePointB point B
   * \param thePointC point C
   * \return true if all parameters has been set before
   */
  HYDRODATA_EXPORT bool GetLocalPoints( QPoint& thePointA,
                                        QPoint& thePointB,
                                        QPoint& thePointC ) const;

  /**
   * Returns true if local points has been set before
   */
  HYDRODATA_EXPORT bool HasLocalPoints() const;


  /**
   * Stores the transformation points in global cs
   * \param theMode transformation mode
   * \param thePointA point A
   * \param thePointB point B
   * \param thePointC point C
   */
  HYDRODATA_EXPORT void SetGlobalPoints( const TransformationMode& theMode,
                                         const QPointF&            thePointA,
                                         const QPointF&            thePointB,
                                         const QPointF&            thePointC = QPoint( INT_MIN, INT_MIN ),
                                         const bool                theIsUpdate = true  );

  /**
   * Returns the transformation points in global cs
   * \param theMode transformation mode
   * \param thePointA point A
   * \param thePointB point B
   * \param thePointC point C
   * \return true if all parameters has been set before
   */
  HYDRODATA_EXPORT bool GetGlobalPoints( TransformationMode& theMode,
                                         QPointF&            thePointA,
                                         QPointF&            thePointB,
                                         QPointF&            thePointC ) const;

  /**
   * Get transformation points from the file and stores them in global cs
   * \param theFileName the image georeferencement file name
   * \return true in case of success
   */
  HYDRODATA_EXPORT bool SetGlobalPointsFromFile( const QString& theFileName );

  /**
   * Returns true if global points has been set before
   */
  HYDRODATA_EXPORT bool HasGlobalPoints() const;


  /**
   * Stores the transformation points in reference image local cs
   * \param theRefImage reference image
   * \param thePointA point A
   * \param thePointB point B
   * \param thePointC point C
   */
  HYDRODATA_EXPORT void SetReferencePoints( const Handle(HYDROData_Image)& theRefImage,
                                            const QPointF&                 thePointA,
                                            const QPointF&                 thePointB,
                                            const QPointF&                 thePointC = QPoint( INT_MIN, INT_MIN ),
                                            const bool                     theIsUpdate = true );

  /**
   * Returns the transformation points in reference image local cs
   * \param theRefImage reference image
   * \param thePointA point A
   * \param thePointB point B
   * \param thePointC point C
   * \return true if all parameters has been set correctly
   */
  HYDRODATA_EXPORT bool GetReferencePoints( Handle(HYDROData_Image)& theRefImage,
                                            QPointF&                 thePointA,
                                            QPointF&                 thePointB,
                                            QPointF&                 thePointC ) const;

  /**
   * Returns true if reference points has been set before
   */
  HYDRODATA_EXPORT bool HasReferencePoints() const;


  /**
   * Stores the reference image for transformation
   * \param theRefImage reference image
   */
  HYDRODATA_EXPORT void SetTrsfReferenceImage( const Handle(HYDROData_Image)& theRefImage );

  /**
   * Returns the reference image for transformation
   */
  HYDRODATA_EXPORT Handle(HYDROData_Image) GetTrsfReferenceImage() const;

  /**
   * Removes the reference image for transformation
   */
  HYDRODATA_EXPORT void RemoveTrsfReferenceImage();


  /**
   * Stores the transformation mode
   */
  HYDRODATA_EXPORT void SetTrsfMode( const TransformationMode& theMode );

  /**
   * Returns the transformation mode
   */
  HYDRODATA_EXPORT TransformationMode GetTrsfMode() const;


  /**
   * Returns the number of referenced objects
   * \return zero if there is no references
   */
  HYDRODATA_EXPORT int NbReferences() const;

  /**
   * Appends reference to other object (image or polyline).
   * \param theReferenced the object referenced by this
   */
  HYDRODATA_EXPORT void AppendReference( const Handle(HYDROData_Entity)& theReferenced );

  /**
   * Returns reference by index.
   * \param theIndex number of reference [0; NbReference)
   * \returns the referenced object, or Null if index is invalid
   */
  HYDRODATA_EXPORT Handle(HYDROData_Entity) Reference(const int theIndex) const;

  /**
   * Updates reference by index. If index is one-bigger than \a NbReferences, 
   * this method appends it to the end (NbReferences is incremented).
   * \param theIndex number of reference [0; NbReference]
   * \param theReferenced the object referenced by this
   */
  HYDRODATA_EXPORT void ChangeReference(
    const int theIndex, Handle(HYDROData_Entity) theReferenced);

  /**
   * Removes reference by index
   * \param theIndex number of reference [0; NbReference)
   */
  HYDRODATA_EXPORT void RemoveReference(const int theIndex);

  /**
   * Removes all references.
   */
  HYDRODATA_EXPORT void ClearReferences();


  /**
   * Stores the operator name
   * \param theOpName name of the operator that must be executed for image update
   */
  HYDRODATA_EXPORT void SetOperatorName(const QString theOpName);

  /**
   * Returns the operator name
   * \returns the name of the operator that must be executed for image update
   */
  HYDRODATA_EXPORT QString OperatorName() const;

  /**
   * Stores the operator arguments
   * \param theArgs array that stores the operator arguments, needed for execution
   */
  HYDRODATA_EXPORT void SetArgs(const QByteArray& theArgs);

  /**
   * Returns the operator arguments
   * \returns array that stores the operator arguments, needed for execution
   */
  HYDRODATA_EXPORT QByteArray Args() const;
  

  /**
   * Marks the image as self-split.
   * \param theFlag is true for self-plit image
   */
  HYDRODATA_EXPORT void SetIsSelfSplit(bool theFlag);

  /**
   * Checks that the image is self-split.
   * \returns true if image is self-split
   */
  HYDRODATA_EXPORT bool IsSelfSplit() const;

private:

  QPointF generateThirdPoint( const QPointF& thePointA,
                              const QPointF& thePointB,
                              const bool&    theIsLocal ) const;

protected:

  friend class HYDROData_Iterator;

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDRODATA_EXPORT HYDROData_Image();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  HYDRODATA_EXPORT ~HYDROData_Image();

};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_DummyObject3D.h"

#include "HYDROData_Object.h"

#include <TopoDS_Shape.hxx>

#include <QString>
#include <QColor>

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_DummyObject3D,HYDROData_Entity)

HYDROData_DummyObject3D::HYDROData_DummyObject3D()
: HYDROData_Entity( Geom_3d )
{
}

HYDROData_DummyObject3D::~HYDROData_DummyObject3D()
{
}

QString HYDROData_DummyObject3D::GetName() const
{
  QString aName;

  Handle(HYDROData_Object) aFatherObj = GetObject();
  if ( !aFatherObj.IsNull() )
    aName = aFatherObj->GetName();

  aName += "_3D";

  return aName;
}

bool HYDROData_DummyObject3D::CanBeUpdated() const
{
  return false;
}

bool HYDROData_DummyObject3D::CanRemove()
{
  return false;
}

Handle(HYDROData_Object) HYDROData_DummyObject3D::GetObject() const
{
  return Handle(HYDROData_Object)::DownCast( GetFatherObject() );
}

TopoDS_Shape HYDROData_DummyObject3D::GetShape() const
{
  TopoDS_Shape aResShape;

  Handle(HYDROData_Object) aFatherObj = GetObject();
  if ( !aFatherObj.IsNull() )
    aResShape = aFatherObj->GetShape3D();

  return aResShape;
}

QColor HYDROData_DummyObject3D::GetFillingColor() const
{
  QColor aResColor( Qt::yellow );

  Handle(HYDROData_Object) aFatherObj = GetObject();
  if ( !aFatherObj.IsNull() )
    aResColor = aFatherObj->GetFillingColor();

  return aResColor;
}

QColor HYDROData_DummyObject3D::GetBorderColor() const
{
  QColor aResColor( Qt::transparent );

  Handle(HYDROData_Object) aFatherObj = GetObject();
  if ( !aFatherObj.IsNull() )
    aResColor = aFatherObj->GetBorderColor();

  return aResColor;
}



// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_IInterpolator.h"

#include <gp_XY.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROData_IInterpolator::HYDROData_IInterpolator()
{
}

HYDROData_IInterpolator::~HYDROData_IInterpolator()
{
}

void HYDROData_IInterpolator::SetAltitudeObject( const Handle(HYDROData_IAltitudeObject)& theAltitude )
{
  myAltitudeObject = theAltitude;
}

Handle(HYDROData_IAltitudeObject) HYDROData_IInterpolator::GetAltitudeObject() const
{
  return myAltitudeObject;
}

double HYDROData_IInterpolator::GetAltitudeForPoint( const double theCoordX, const double theCoordY ) const
{
  DEBTRACE("HYDROData_IInterpolator::GetAltitudeForPoint");
  return myAltitudeObject.IsNull() ? HYDROData_IAltitudeObject::GetInvalidAltitude() :
         myAltitudeObject->GetAltitudeForPoint( gp_XY( theCoordX, theCoordY ) );
}

double HYDROData_IInterpolator::GetAltitudeForPoint( const gp_XY& thePoint) const
{
  DEBTRACE("HYDROData_IInterpolator::GetAltitudeForPoint");
  return GetAltitudeForPoint( thePoint.X(), thePoint.Y() );
}

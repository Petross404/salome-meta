// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROData_InterpolatorsFactory_HeaderFile
#define HYDROData_InterpolatorsFactory_HeaderFile

#include <HYDROData.h>

#include <TCollection_AsciiString.hxx>
#include <NCollection_Sequence.hxx>

#include <string>
#include <map>

class HYDROData_IProfilesInterpolator;

/**
 * \class HYDROData_InterpolatorsFactory
 *
 * \brief Profile interpolators factory.
 *
 * Allows to register different types of interpolatotors (these interpolators should inherit HYDROData_IProfilesInterpolator).
 * Once the interpolator of a certain type is registered it should be got by its name.
 */

class HYDROData_InterpolatorsFactory
{
public:

  /**
   * Public constructor.
   */
  HYDROData_InterpolatorsFactory();

  /**
   * Public virtual destructor.
   */
  virtual ~HYDROData_InterpolatorsFactory();

  /**
   * Registers the interpolator of a certain type with the given name.
   * \param theName the interpolator name used as identifier
   */
  template <class T> /*HYDRODATA_EXPORT */void Register( const TCollection_AsciiString& theName )
  {
    myInterpolators[theName.ToCString()] = new T();
  }

  /**
   * Get the appropriate interpolator by the name.
   * \param theName name of the interpolator
   * \returns the appropriate interpolator or NULL if interpolator with such name is not registered
   */
  HYDRODATA_EXPORT HYDROData_IProfilesInterpolator* GetInterpolator( const TCollection_AsciiString& theName ) const;

  /**
   * Get list of registered interpolator names.
   * \return the list of unique names
   */
  HYDRODATA_EXPORT NCollection_Sequence<TCollection_AsciiString> GetInterpolatorNames() const;
  
private:
  //! Map that stores all interpolators, identified by interpolator name (string)
  typedef std::map<std::string, HYDROData_IProfilesInterpolator*> FactoryInterpolators;
  
  FactoryInterpolators myInterpolators; ///< all interpolators stored by factory
};

#endif
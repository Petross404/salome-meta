// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROData_IAltitudeObject.h"

#define INVALID_ALTITUDE_VALUE -9999.0

IMPLEMENT_STANDARD_RTTIEXT(HYDROData_IAltitudeObject, HYDROData_Entity)

HYDROData_IAltitudeObject::HYDROData_IAltitudeObject()
: HYDROData_Entity( Geom_Z )
{
}

HYDROData_IAltitudeObject::~HYDROData_IAltitudeObject()
{
}

double HYDROData_IAltitudeObject::GetInvalidAltitude()
{
  return INVALID_ALTITUDE_VALUE;
}

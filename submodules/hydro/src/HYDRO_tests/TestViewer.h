// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#pragma once

#include <AIS_InteractiveContext.hxx>
#include <Standard_Version.hxx>

class OCCViewer_ViewManager;
class OCCViewer_Viewer;
class OCCViewer_ViewFrame;
class TopoDS_Shape;
class QString;
class QColor;
class QImage;
class AIS_InteractiveObject;
class AIS_ColorScale;

#define OCC_VERSION_LARGE (OCC_VERSION_MAJOR << 24 | OCC_VERSION_MINOR << 16 | OCC_VERSION_MAINTENANCE << 8)
#if OCC_VERSION_LARGE >= 0x07020000
const bool SWAP_RGB = false;
#else
const bool SWAP_RGB = true;
#endif

class TestViewer
{
public:
  static OCCViewer_ViewManager* viewManager();
  static OCCViewer_Viewer* viewer();
  static OCCViewer_ViewFrame* viewWindow();
  static Handle(AIS_InteractiveContext) context();

  static void eraseAll( bool isUpdate, bool eraseStructures = false );
  static void show( const Handle(AIS_InteractiveObject)& theObject,
                    int theMode, int theSelectionMode, bool isFitAll, const char* theKey );
  static void show( const TopoDS_Shape& theShape, int theMode, bool isFitAll, const QColor& theColor,
                    int theUIANb = 10, int theVIANb = 10);
  static void show( const TopoDS_Shape& theShape, int theMode, bool isFitAll, const char* theKey,
                    int theUIANb = 10, int theVIANb = 10);
  static bool AssertImages( QString& theMessage, const QImage* = 0, const char* theCase = 0, bool swapRGB = SWAP_RGB );
  static QColor GetColor(int i);

  static Handle(AIS_ColorScale) colorScale();
  static void showColorScale( bool );
  static bool ColorScaleIsDisplayed();

  static void select( int theViewX, int theViewY );

  static bool areScriptsEqual( const QString& theBaseName,
                               bool isExpectedUtf8,
                               bool isActualUtf8,
                               int theLinesToOmit,
                               QString& theMsg );

  static void setKey( const QString& );
  static void fitAll();
  static QImage diff( const QImage& im1, const QImage& im2 );

private:
  static OCCViewer_ViewManager* myViewManager;
  static OCCViewer_ViewFrame* myViewWindow;
  static QString myKey;
};

#define CPPUNIT_ASSERT_IMAGES                              \
  {                                                        \
    QString aMessage;                                      \
    if( !TestViewer::AssertImages( aMessage ) )            \
    {                                                      \
      TestViewer::showColorScale( false );                 \
      std::string aMessageStl = aMessage.toStdString();    \
      CPPUNIT_FAIL( aMessageStl.c_str() );                 \
    }                                                      \
  }                                                        \

#define CPPUNIT_ASSERT_IMAGES2( theImage, theCase )        \
  {                                                        \
    QString aMessage;                                      \
    if( !TestViewer::AssertImages( aMessage, theImage, theCase ) ) \
    {                                                      \
      TestViewer::showColorScale( false );                 \
      std::string aMessageStl = aMessage.toStdString();    \
      CPPUNIT_FAIL( aMessageStl.c_str() );                 \
    }                                                      \
  }                                                        \

#define CPPUNIT_ASSERT_IMAGES3( theImage, theCase, theSwapRGB )        \
  {                                                        \
    QString aMessage;                                      \
    if( !TestViewer::AssertImages( aMessage, theImage, theCase, theSwapRGB ) ) \
    {                                                      \
      TestViewer::showColorScale( false );                 \
      std::string aMessageStl = aMessage.toStdString();    \
      CPPUNIT_FAIL( aMessageStl.c_str() );                 \
    }                                                      \
  }

#define CPPUNIT_ASSERT_SCRIPTS_EQUAL( theBaseName,         \
                                      isExpectedUtf8,      \
                                      isActualUtf8,        \
                                      theLinesToOmit )     \
  {                                                        \
    QString aMsg;                                          \
    if( !TestViewer::areScriptsEqual(                      \
          theBaseName, isExpectedUtf8,                     \
          isActualUtf8, theLinesToOmit, aMsg ) )           \
    {                                                      \
      std::string aStlMsg = "Scripts are not equal: " +    \
        aMsg.toStdString();                                \
      CPPUNIT_FAIL( aStlMsg );                             \
    }                                                      \
  }                                                        \
                                                           \


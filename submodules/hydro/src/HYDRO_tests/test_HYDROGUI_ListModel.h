// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <cppunit/extensions/HelperMacros.h>

class HYDROGUI_ListModel;

class test_HYDROGUI_ListModel : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(test_HYDROGUI_ListModel);
  CPPUNIT_TEST(testMoveUp);
  CPPUNIT_TEST(testMoveOnTop);
  CPPUNIT_TEST(testMoveDown);
  CPPUNIT_TEST(testMoveOnBottom);
  CPPUNIT_TEST(testDragAndDrop);
  CPPUNIT_TEST_SUITE_END();

private:
  std::string GetObjects( HYDROGUI_ListModel* theModel ) const;

public:
  void testMoveUp();
  void testMoveOnTop();
  void testMoveDown();
  void testMoveOnBottom();
  void testDragAndDrop();
};

CPPUNIT_TEST_SUITE_REGISTRATION(test_HYDROGUI_ListModel);
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION(test_HYDROGUI_ListModel, "HYDROGUI_ListModel");

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifdef WIN32
  #pragma warning( disable: 4251 )
#endif

#include <cppunit/extensions/HelperMacros.h>

class HYDROGUI_Overview;
class QImage;
class QColor;

class test_Overview : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( test_Overview );
  //CPPUNIT_TEST( test_default );
  //CPPUNIT_TEST( test_presentation );
  //CPPUNIT_TEST( test_actions_in_main );
  //CPPUNIT_TEST( test_actions_in_overview );
  //CPPUNIT_TEST( test_set_mainview_2_times );
  CPPUNIT_TEST_SUITE_END();

public:
  void test_default();
  void test_presentation();
  void test_actions_in_main();
  void test_actions_in_overview();
  void test_set_mainview_2_times();

private:
  static HYDROGUI_Overview* overView();
  static QImage dumpViews();

  void create();
  void showShape();

private:
  static HYDROGUI_Overview* myOverview;
};

CPPUNIT_TEST_SUITE_REGISTRATION( test_Overview );
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( test_Overview, "Overview" );

#ifdef WIN32
  #pragma warning( default: 4251 )
#endif


find_package( Qt5Core )
find_package( Qt5Widgets )
set(CMAKE_AUTOMOC ON)

set( EXTERNAL_FILES
  ../HYDROData/HYDROData_AltitudeObject.cxx
  ../HYDROData/HYDROData_Application.cxx
  ../HYDROData/HYDROData_ArtificialObject.cxx
  ../HYDROData/HYDROData_Bathymetry.cxx
  ../HYDROData/HYDROData_BSplineOperation.cxx
  ../HYDROData/HYDROData_CalculationCase.cxx
  ../HYDROData/HYDROData_CompleteCalcCase.cxx
  ../HYDROData/HYDROData_Channel.cxx
  ../HYDROData/HYDROData_ChannelAltitude.cxx
  ../HYDROData/HYDROData_Confluence.cxx
  ../HYDROData/HYDROData_Digue.cxx
  ../HYDROData/HYDROData_Document.cxx
  ../HYDROData/HYDROData_DummyObject3D.cxx
  ../HYDROData/HYDROData_Entity.cxx
  ../HYDROData/HYDROData_IAltitudeObject.cxx
  ../HYDROData/HYDROData_IInterpolator.cxx
  ../HYDROData/HYDROData_Image.cxx
  ../HYDROData/HYDROData_ImmersibleZone.cxx
  ../HYDROData/HYDROData_BCPolygon.cxx
  ../HYDROData/HYDROData_BoundaryPolygonTools.cxx
  ../HYDROData/HYDROData_IPolyline.cxx
  ../HYDROData/HYDROData_Iterator.cxx
  ../HYDROData/HYDROData_Lambert93.cxx
  ../HYDROData/HYDROData_NaturalObject.cxx
  ../HYDROData/HYDROData_Object.cxx
  ../HYDROData/HYDROData_Obstacle.cxx
  ../HYDROData/HYDROData_ObstacleAltitude.cxx
  ../HYDROData/HYDROData_OperationsFactory.cxx
  ../HYDROData/HYDROData_PolylineOperator.cxx
  ../HYDROData/HYDROData_PolylineXY.cxx
  ../HYDROData/HYDROData_Polyline3D.cxx
  ../HYDROData/HYDROData_PriorityQueue.cxx
  ../HYDROData/HYDROData_Profile.cxx
  ../HYDROData/HYDROData_ProfileUZ.cxx
  ../HYDROData/HYDROData_Projection.cxx
  ../HYDROData/HYDROData_Region.cxx
  ../HYDROData/HYDROData_River.cxx
  ../HYDROData/HYDROData_ShapesGroup.cxx
  ../HYDROData/HYDROData_ShapesTool.cxx
  ../HYDROData/HYDROData_SplitShapesGroup.cxx
  ../HYDROData/HYDROData_SplitToZonesTool.cxx
  ../HYDROData/HYDROData_Stream.cxx
  ../HYDROData/HYDROData_StreamAltitude.cxx
  ../HYDROData/HYDROData_StricklerTable.cxx
  ../HYDROData/HYDROData_Tool.cxx
  ../HYDROData/HYDROData_TopoCurve.cxx
  ../HYDROData/HYDROData_Transform.cxx
  ../HYDROData/HYDROData_VisualState.cxx
  ../HYDROData/HYDROData_Zone.cxx
  ../HYDROData/HYDROData_GeomTool.cxx
  ../HYDROData/HYDROData_IProfilesInterpolator.cxx
  ../HYDROData/HYDROData_LinearInterpolator.cxx
  ../HYDROData/HYDROData_InterpolatorsFactory.cxx
  ../HYDROData/HYDROData_SinusX.cxx
  ../HYDROData/HYDROData_ShapeFile.cxx
  ../HYDROData/HYDROData_LandCoverMap.cxx
  ../HYDROData/HYDROData_Quadtree.cxx
  ../HYDROData/HYDROData_QuadtreeNode.cxx
  ../HYDROData/HYDROData_LCM_FaceClassifier.cxx
  ../HYDROData/HYDROData_DTM.cxx
  ../HYDROData/HYDROData_LISM.cxx
  ../HYDROData/HYDROData_StreamLinearInterpolation.cxx
  ../HYDROGUI/HYDROGUI_AISShape.cxx
  ../HYDROGUI/HYDROGUI_AISTrihedron.cxx
  ../HYDROGUI/HYDROGUI_BathymetryPrs.cxx
  ../HYDROGUI/HYDROGUI_CurveCreatorProfile.cxx
  ../HYDROGUI/HYDROGUI_DataObject.cxx
  ../HYDROGUI/HYDROGUI_InputPanel.cxx
  ../HYDROGUI/HYDROGUI_LandCoverArgsFilter.cxx
  ../HYDROGUI/HYDROGUI_LandCoverMapPrs.cxx
  ../HYDROGUI/HYDROGUI_LineEditDoubleValidator.cxx
  ../HYDROGUI/HYDROGUI_ListModel.cxx
  ../HYDROGUI/HYDROGUI_ListSelector.cxx
  ../HYDROGUI/HYDROGUI_OrderedListWidget.cxx
  ../HYDROGUI/HYDROGUI_Overview.cxx
  ../HYDROGUI/HYDROGUI_Polyline.cxx
  ../HYDROGUI/HYDROGUI_ProfileDlg.cxx
  ../HYDROGUI/HYDROGUI_ShapeBathymetry.cxx
  ../HYDROGUI/HYDROGUI_Shape.cxx
  ../HYDROGUI/HYDROGUI_StreamDlg.cxx
  ../HYDROGUI/HYDROGUI_StricklerTableDlg.cxx
  ../HYDROGUI/HYDROGUI_Tool.cxx
  ../HYDROGUI/HYDROGUI_ViewerDlg.cxx
  ../HYDROGUI/HYDROGUI_ZoneTool.cxx
)
# if not -DLIGHT_MODE, to be completed. Link with library HYDROData & HYDROGUI instead
#  ../HYDROGUI/HYDROGUI_AbstractDisplayer.cxx
#  ../HYDROGUI/HYDROGUI_Displayer.cxx
#  ../HYDROGUI/HYDROGUI_Module.cxx
#  ../HYDROGUI/HYDROGUI_OCCDisplayer.cxx
#  ../HYDROGUI/HYDROGUI_Operations.cxx
#  ../HYDROGUI/HYDROGUI_Tool2.cxx
#  ../HYDROGUI/HYDROGUI_VTKPrsDisplayer.cxx


set( MOC_HEADERS
  ../HYDROGUI/HYDROGUI_InputPanel.h
  ../HYDROGUI/HYDROGUI_StricklerTableDlg.h
  ../HYDROGUI/HYDROGUI_StreamDlg.h
  ../HYDROGUI/HYDROGUI_ListSelector.h
  ../HYDROGUI/HYDROGUI_OrderedListWidget.h
  ../HYDROGUI/HYDROGUI_Overview.h
  ../HYDROGUI/HYDROGUI_ZoneTool.h
  ../HYDROGUI/HYDROGUI_ProfileDlg.h
  ../HYDROGUI/HYDROGUI_ViewerDlg.h
)

#QT5_WRAP_MOC( PROJECT_MOC_HEADERS ${MOC_HEADERS} )

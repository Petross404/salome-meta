// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <TestViewer.h>
#include <HYDROData_Tool.h>
#include <OCCViewer_ViewManager.h>
#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif
#include <OCCViewer_ViewPort3d.h>
#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif
#include <OCCViewer_ViewModel.h>
#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif
#include <OCCViewer_ViewFrame.h>
#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif
#include <AIS_Shape.hxx>
#include <AIS_ColorScale.hxx>
#include <Prs3d_PointAspect.hxx>
#include <TopoDS_Iterator.hxx>
#include <QDir>
#include <QPainter>
#include <QHash>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <Prs3d_IsoAspect.hxx>

#include <GEOMUtils.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

#include <cppunit/TestAssert.h>
#include <QApplication>

OCCViewer_ViewManager* TestViewer::myViewManager = 0;
OCCViewer_ViewFrame* TestViewer::myViewWindow = 0;
QString TestViewer::myKey = "";

extern QString REF_DATA_PATH;
extern QString TMP_DIR;

OCCViewer_ViewManager* TestViewer::viewManager()
{
  if( myViewManager )
    return myViewManager;

  myViewManager = new OCCViewer_ViewManager( 0, 0 );
  OCCViewer_Viewer* aViewer = new OCCViewer_Viewer( true );

  aViewer->setTrihedronSize( 0, true );
  aViewer->setInteractionStyle( 0 );
  aViewer->setZoomingStyle( 1 );

  myViewManager->setViewModel( aViewer );
  myViewWindow = dynamic_cast<OCCViewer_ViewFrame*>( myViewManager->createViewWindow() );
  aViewer->setTrihedronShown( false );
  return myViewManager;
}

OCCViewer_Viewer* TestViewer::viewer()
{
  return dynamic_cast<OCCViewer_Viewer*>( viewManager()->getViewModel() );
}

OCCViewer_ViewFrame* TestViewer::viewWindow()
{
  viewManager(); //to create the view if it was not created earlier
  return myViewWindow;
}

Handle(AIS_InteractiveContext) TestViewer::context()
{
  return viewer()->getAISContext();
}

QColor TestViewer::GetColor(int i)
{
  static QVector<QColor> aCV;
  if( aCV.isEmpty() )
  {
    aCV  << QColor(0,0,255)
         << QColor(0,255,0)
         << QColor(255,0,0)
         << QColor(255,255,20)
         << QColor(20,255,255)
         << QColor(100,100,20)
         << QColor(10,100,150);
  }
  if (i < aCV.size())
    return aCV[i];
  else
  {
    QColor TestColor = aCV[i % aCV.size()];
    QColor NewColor((TestColor.red() + i * 41) % 256,
      (TestColor.green() + i * 13) % 256,
      (TestColor.blue() + i * 23) % 256);
    return NewColor;
  }
}

void TestViewer::eraseAll( bool isUpdate, bool eraseStructures )
{
  context()->EraseAll( isUpdate );
  if( eraseStructures )
  {
    Graphic3d_MapOfStructure GmapS;
    viewer()->getViewer3d()->StructureManager()->DisplayedStructures(GmapS);
    for (Graphic3d_MapOfStructure::Iterator it(GmapS); it.More(); it.Next())
    {
      Handle(Graphic3d_Structure) GS = it.Key();
      GS->Erase();
    }
  }
  viewer()->setTrihedronShown( false );
  qApp->processEvents();
}

void TestViewer::show( const Handle(AIS_InteractiveObject)& theObject,
                       int theMode, int theSelectionMode, bool isFitAll, const char* theKey )
{
  QString aNewKey = theKey;
  if( !aNewKey.isEmpty() )
  {
    myKey = aNewKey;
    eraseAll( false, true );
  }

  if( theSelectionMode > 0 )
  {
    context()->Deactivate(); //OpenLocalContext();
    context()->Display( theObject, theMode, theSelectionMode, true );
    context()->Activate( theObject, theSelectionMode, Standard_True );
  }
  else
    context()->Display( theObject, theMode, theSelectionMode, true );

  if( isFitAll )
    fitAll();
}

void TestViewer::fitAll()
{
  viewWindow()->onTopView();
  viewWindow()->onFitAll();
}

void TestViewer::show( const TopoDS_Shape& theShape, int theMode, bool isFitAll, const QColor& theColor,
  int theUIANb, int theVIANb)
{
  Handle(AIS_Shape) aShape = new AIS_Shape( theShape );
  std::cout <<theShape;
  if( theShape.ShapeType()==TopAbs_VERTEX )
    aShape->Attributes()->PointAspect()->SetTypeOfMarker( Aspect_TOM_X );
  if (theShape.ShapeType()==TopAbs_FACE)
  {
    context()->DefaultDrawer()->UIsoAspect()->SetNumber(theUIANb);
    context()->DefaultDrawer()->VIsoAspect()->SetNumber(theVIANb);
    Handle_Prs3d_Drawer aDrawer = aShape->Attributes();
    aDrawer->UIsoAspect()->SetNumber(theUIANb);
    aDrawer->VIsoAspect()->SetNumber(theVIANb);
  }
  aShape->SetMaterial( Graphic3d_NOM_PLASTIC );
  aShape->SetColor( HYDROData_Tool::toOccColor( theColor ) );
  context()->Display( aShape, theMode, 0, Standard_False );

  if( isFitAll )
    fitAll();
}

void TestViewer::show( const TopoDS_Shape& theShape, int theMode, bool isFitAll, const char* theKey,
                       int theUIANb, int theVIANb)
{
  QString aNewKey = theKey;
  if( !aNewKey.isEmpty() )
  {
    eraseAll( false );
    myKey = aNewKey;
  }

  if( theShape.IsNull() )
    return;

  int i = 0;
  //show all faces first
  TopTools_ListOfShape aListOfFaces;
  TopExp_Explorer aFE( theShape, TopAbs_FACE );
  for( ; aFE.More(); aFE.Next() )
    aListOfFaces.Append(aFE.Current());
  GEOMUtils::SortShapes(aListOfFaces);
  TopTools_ListIteratorOfListOfShape aLF(aListOfFaces);
  for( ; aLF.More(); aLF.Next(), i++)
    show( aLF.Value(), theMode, false, GetColor(i), theUIANb, theVIANb );

  //show all independent wires
  TopTools_ListOfShape aListOfWires;
  TopExp_Explorer aWE( theShape, TopAbs_WIRE, TopAbs_FACE );
  for( ; aWE.More(); aWE.Next() )
    aListOfWires.Append(aWE.Current());
  GEOMUtils::SortShapes(aListOfWires);
  TopTools_ListIteratorOfListOfShape aLW(aListOfWires);
  for( ; aLW.More(); aLW.Next(), i++)
    show( aLW.Value(), theMode, false, GetColor(i) );

  //show all independent edges
  TopTools_ListOfShape aListOfEdges;
  TopExp_Explorer anEE( theShape, TopAbs_EDGE, TopAbs_WIRE );
  for( ; anEE.More(); anEE.Next())
    aListOfEdges.Append(anEE.Current());
  GEOMUtils::SortShapes(aListOfEdges);
  TopTools_ListIteratorOfListOfShape aLE(aListOfEdges);
  for( ; aLE.More(); aLE.Next(), i++)
    show( aLE.Value(), theMode, false, GetColor(i) );

  if( isFitAll )
    fitAll();
}

/*void TestViewer::ShowShape(const TopoDS_Shape& theShape, int theMode, int& i)
{
  if( theShape.ShapeType()==TopAbs_SHELL )
  {
    TopoDS_Iterator anIt( theShape );
    for( ; anIt.More(); anIt.Next())
    {
      show( anIt.Value(), theMode, false, GetColor(i) );
      i++;
    }
  }
  else if (theShape.ShapeType()==TopAbs_FACE ||
           theShape.ShapeType()==TopAbs_WIRE ||
           theShape.ShapeType()==TopAbs_EDGE ||
           theShape.ShapeType()==TopAbs_VERTEX )
  {
    show( theShape, theMode, false, GetColor(i) );
    i++;
  }
}*/

bool AreImagesEqual( const QImage& theImage1, const QImage& theImage2, double theTolerance )
{
  if( theImage1.isNull() || theImage2.isNull() )
    return theImage1.isNull() == theImage2.isNull();

  if( theImage1.size() != theImage2.size() )
    return false;

  int aBytesCount = theImage1.byteCount();
  const uchar *aBytes1 = theImage1.constBits();
  const uchar *aBytes2 = theImage2.constBits();
  int aBytesCountE = 0;
  for( int i=0; i<aBytesCount; i++ )
    if( aBytes1[i] != aBytes2[i] )
      aBytesCountE++;

  if ((double)aBytesCountE / (double)aBytesCount > theTolerance)
    return false;

  return true;
}

QImage TestViewer::diff( const QImage& theExpectedRefImage, const QImage& theActualImage )
{
  const QImage::Format aFormat = QImage::Format_RGB32;

  QImage anExpectedRefImage = theExpectedRefImage.convertToFormat( aFormat );
  QImage anActualImage = theActualImage.convertToFormat( aFormat );

  QImage aDiff( anExpectedRefImage.width(), anExpectedRefImage.height(), aFormat );
  QPainter aPainter( &aDiff );
  aPainter.drawImage( 0, 0, anExpectedRefImage );
  aPainter.setCompositionMode( QPainter::RasterOp_SourceXorDestination );
  aPainter.drawImage( 0, 0, anActualImage );
  return aDiff;
}

bool TestViewer::AssertImages( QString& theMessage, const QImage* theImage, const char* theCase, bool swapRGB )
{
  QImage anActualImage;
  if( theImage )
    anActualImage = *theImage;
  else
    anActualImage = viewWindow()->getView(OCCViewer_ViewFrame::MAIN_VIEW)->dumpView();

  if( swapRGB )
  {
    // A temporary patch for bug in SALOME/OCC dump; the result image contains swapped RGB colors
    anActualImage = anActualImage.rgbSwapped();
  }


  if( theCase )
    myKey = theCase;

  QString anExpectedRefFilePath = REF_DATA_PATH;
  anExpectedRefFilePath += "/" + myKey + ".png";
  std::ifstream file(anExpectedRefFilePath.toStdString().c_str());
  if (!file)
  {
    std::cerr << "Missing reference image " << anExpectedRefFilePath.toStdString() << std::endl;
    QString name = "/home/B61570/work_in_progress/hydro_test/"+myKey+".png";
    anActualImage.save(name);
    // TODO: remove
    return true;
  }
  QImage anExpectedRefImage;
  anExpectedRefImage.load( anExpectedRefFilePath );
  //std::cout << "Expected image loading: " << anExpectedRefFilePath.toStdString() << std::endl;

  if( AreImagesEqual( anActualImage, anExpectedRefImage, 0.001 ) )
  {
    theMessage = "";
    return true;
  }

  QString aPath = TMP_DIR + "/" + myKey + ".png";
  anActualImage.save( aPath );
  //std::cout << "Actual image: " << aPath.toStdString() << std::endl;

  //std::cout << anActualImage.width() << "x" << anActualImage.height() << std::endl;
  theMessage = "The viewer contents does not correspond to the reference image: " + myKey;

  QImage aDiff = diff( anExpectedRefImage, anActualImage );

  QString aDiffFilePath = TMP_DIR + "/" + myKey + "_diff.png";
  aDiff.save( aDiffFilePath );
  //std::cout << "Diff image: " << aDiffFilePath.toStdString() << std::endl;

  QString anExpected = TMP_DIR + "/" + myKey + "_1.png";
  //std::cout << "Expected image: " << anExpected.toStdString() << std::endl;
  anExpectedRefImage.save( anExpected );

  return false;
}

Handle(AIS_ColorScale) TestViewer::colorScale()
{
  static Handle(AIS_ColorScale) aColorScale = new AIS_ColorScale();

  return aColorScale;
}

void TestViewer::showColorScale( bool isShow )
{
  Handle(AIS_ColorScale) aColorScale = colorScale();
  if( aColorScale.IsNull() )
    return;

  Standard_Real anXPos = 0.05;
  Standard_Real anYPos = 0.1;
  Standard_Real aWidth = 0.2;
  Standard_Real aHeight = 0.5;
  Standard_Integer aTextHeight = 14;
  Standard_Integer aNbIntervals = 30;

  aColorScale->SetXPosition( anXPos );
  aColorScale->SetYPosition( anYPos );
  aColorScale->SetWidth( aWidth );
  aColorScale->SetHeight( aHeight );
  aColorScale->SetTextHeight( aTextHeight );
  aColorScale->SetNumberOfIntervals( aNbIntervals );

  aColorScale->SetTitle( "test" );
  aColorScale->SetRange( 0, 1 );

  aColorScale->SetToUpdate();

  if( isShow )
  {
    if( !context()->IsDisplayed( aColorScale ) )
      context()->Display( aColorScale, true );
  }
  else
  {
    if( context()->IsDisplayed( aColorScale ) )
      context()->Erase( aColorScale, true );
  }
}

bool TestViewer::ColorScaleIsDisplayed()
{
  return context()->IsDisplayed( colorScale() );
}

void TestViewer::select( int theViewX, int theViewY )
{
  Handle(V3d_View) aView = myViewWindow->getViewPort()->getView();
  context()->MoveTo( theViewX, theViewY, aView, true);
  context()->Select(true);
  // context()->CloseAllContexts();
}

QString GetLine( QFile& theFile, bool isUtf8 )
{
  QByteArray aLineData = theFile.readLine();
  QString aLine;
  if( isUtf8 )
    aLine = QString::fromUtf8( aLineData );
  else
    aLine = aLineData;
  return aLine;
}

bool TestViewer::areScriptsEqual( const QString& theBaseName,
                                  bool isExpectedUtf8,
                                  bool isActualUtf8,
                                  int theLinesToOmit,
                                  QString& theMsg )
{
  QString anExpectedRefFilePath = REF_DATA_PATH;
  anExpectedRefFilePath += "/" + theBaseName;

  QString anActualFilePath = TMP_DIR + "/" + theBaseName;

  QFile anExpected( anExpectedRefFilePath );
  QFile anActual( anActualFilePath );
  if( !anExpected.open( QFile::ReadOnly | QFile::Text ) )
  {
    theMsg = "Expected file cannot be opened: " + anExpectedRefFilePath;
    return false;
  }

  if( !anActual.open( QFile::ReadOnly | QFile::Text ) )
  {
    theMsg = "Actual file cannot be opened: " + anActualFilePath;
    return false;
  }

  for( int i=0; i<theLinesToOmit; i++ )
    anExpected.readLine();

  bool isEqual = true;
  int i = 1;
  while( !anExpected.atEnd() && isEqual )
  {
    QString anExpectedLine = GetLine( anExpected, isExpectedUtf8 );
    QString anActualLine = GetLine( anActual, isActualUtf8 );
    isEqual = anExpectedLine == anActualLine;
    if( !isEqual )
      theMsg = QString( "line %1\nActual: %2\nExpected: %3" ).arg( i ).arg( anActualLine ).arg( anExpectedLine );
    i++;
  }

  while( !anActual.atEnd() && isEqual )
  {
    QString anActualLine = GetLine( anActual, isActualUtf8 );
    anActualLine = anActualLine.trimmed();
    if( !anActualLine.isEmpty() )
      isEqual = false;
  }

  anExpected.close();
  anActual.close();

  return isEqual;
}

void TestViewer::setKey( const QString& theKey )
{
  myKey = theKey;
}

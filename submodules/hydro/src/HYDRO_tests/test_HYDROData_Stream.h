// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <cppunit/extensions/HelperMacros.h>

class test_HYDROData_Stream : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( test_HYDROData_Stream );
  CPPUNIT_TEST( test_dialog );
  CPPUNIT_TEST( test_alt_object );
  CPPUNIT_TEST( test_params_sync );
  CPPUNIT_TEST( test_dump );
  CPPUNIT_TEST( test_presentation );
  //CPPUNIT_TEST( test_lism_1 );
  CPPUNIT_TEST_SUITE_END();

public:
  void setUp();
  void tearDown();

  void test_dialog();
  void test_alt_object();
  void test_params_sync();
  void test_dump();
  void test_presentation();
  void test_lism_1();
};

CPPUNIT_TEST_SUITE_REGISTRATION( test_HYDROData_Stream );
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( test_HYDROData_Stream, "HYDROData_Stream" );

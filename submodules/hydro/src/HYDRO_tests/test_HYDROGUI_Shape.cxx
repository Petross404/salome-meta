// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#undef HYDROGUI_EXPORTS

#include <test_HYDROGUI_Shape.h>
#include <HYDROGUI_Shape.h>
#include <TestShape.h>
#include <TestViewer.h>
#include <TopoDS_Face.hxx>

void test_HYDROGUI_Shape::test_face_in_preview()
{
  TopoDS_Face aFace = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );

  Handle(AIS_InteractiveContext) aContext = TestViewer::context();
  Handle(HYDROData_Entity) anEntity; //it should be null as in preview

  HYDROGUI_Shape* aPreview = new HYDROGUI_Shape( aContext, anEntity );
  aPreview->setFace( aFace, true, true, "" );
  aPreview->setFillingColor( Qt::red, false, false );
  aPreview->setBorderColor( Qt::darkBlue, false, false );
  
  TestViewer::show( aPreview->getAISObjects()[0], AIS_Shaded, 0, true, "Shape_preview_im_zone" );
  CPPUNIT_ASSERT_IMAGES

  delete aPreview;
}

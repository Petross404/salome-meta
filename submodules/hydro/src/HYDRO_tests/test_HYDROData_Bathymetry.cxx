// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_Bathymetry.h>

#include <HYDROData_Document.h>
#include <HYDROData_Tool.h>
#include <HYDROData_Bathymetry.h>

#include <gp_XY.hxx>
#include <gp_XYZ.hxx>

#include <QDir>
#include <QFile>
#include <QTextStream>

#include <gp_Pnt2d.hxx>

const double EPS = 1E-4;
extern QString TMP_DIR;

void generateOne( QTextStream& theStream,
                  double theFirstX, double theFirstY,
                  double theLastX, double theLastY )
{
  const int aNbPoints = 50;
  double aStepZ = -5;
  const double dt = 1.0 / aNbPoints;
  
  for( double t=0.0; t<=1.0; t += dt )
  {
    double anX = theFirstX * (1-t) + theLastX * t;
    double anY = theFirstY * (1-t) + theLastY * t;
    double aZ = aStepZ * aStepZ + sin( 10*t );
    theStream << anX << " " << anY << " " << aZ << " \n";

    aStepZ += 0.2;
  }
}

bool test_HYDROData_Bathymetry::createTestFile( const QString& theFileName )
{
  QFile aTmpFile( theFileName );
  if ( !aTmpFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
    return false;

  {
    QTextStream anOutStream( &aTmpFile );

    generateOne( anOutStream, 0, 5, 0, -5 );
    generateOne( anOutStream, 10, 5, 10, -5 );
    generateOne( anOutStream, 20, 5, 20, -5 );
    generateOne( anOutStream, 30, 5, 30, -5 );
    generateOne( anOutStream, 40, 5, 40, -5 );
    generateOne( anOutStream, 45.833271, 6.703101, 50.85397, -3.794724 );
    generateOne( anOutStream, 50.85397, 11.267372, 56.787525, 2.367043 );
    generateOne( anOutStream, 52.67968, 16.516285, 62.721077, 10.354518 );
    generateOne( anOutStream, 55.190029, 23.819118, 65.23143, 17.657352 );
    generateOne( anOutStream, 58.385021, 31.806595, 68.198204, 23.819118 );
    generateOne( anOutStream, 65.916069, 38.653004, 75.044609, 27.470537 );
    generateOne( anOutStream, 76.870323, 41.847992, 82.575661, 28.38339 );
    generateOne( anOutStream, 92.845276, 40.022282, 91.932419, 26.557682 );
    generateOne( anOutStream, 106.081657, 35.686226, 98.778824, 21.99341 );
    generateOne( anOutStream, 119.774475, 29.980886, 106.994514, 14.91879 );
    generateOne( anOutStream, 133.239075, 26.785896, 115.666634, 5.790246 );
    generateOne( anOutStream, 150.811523, 20.167702, 125.47982, -4.707579 );
    generateOne( anOutStream, 163.591476, 13.777717, 135.977631, -12.92327 );
    generateOne( anOutStream, 183.446045, 11.267367, 151.496155, -24.105736 );
    generateOne( anOutStream, 197.367081, 8.98523100, 172.720016, -31.180355 );
    generateOne( anOutStream, 214.026672, 9.669872, 197.823502, -38.483189 );
    generateOne( anOutStream, 235.706985, 9.89809100, 220.873108, -47.155304 );
    generateOne( anOutStream, 266.744019, 9.213447, 248.715134, -58.565987 );
    generateOne( anOutStream, 307.366028, 10.354514, 278.611145, -67.922737 );
    generateOne( anOutStream, 342.510925, 7.159524, 316.951019, -84.81053 );
    generateOne( anOutStream, 384.659393, -4.55408, 350.913635, -100.02983 );
    generateOne( anOutStream, 443.097107, -21.015415, 391.243927, -118.960365 );
    generateOne( anOutStream, 500.711792, -34.184483, 440.62793, -147.7677 );
    generateOne( anOutStream, 547.626587, -52.291954, 482.604309, -169.990509 );
    generateOne( anOutStream, 589.603027, -68.753288, 534.45752, -193.8594 );
    generateOne( anOutStream, 633.225525, -97.560631, 569.026306, -206.205444 );
    generateOne( anOutStream, 673.555786, -120.606499, 601.948975, -224.312912 );
    generateOne( anOutStream, 719.647522, -148.590775, 654.625305, -248.181854 );
    generateOne( anOutStream, 770.677673, -183.982635, 708.947693, -278.635315 );
    generateOne( anOutStream, 824.177002, -225.959045, 742.69342, -314.027191 );
    generateOne( anOutStream, 855.937317, -260.368958, 793.05932, -341.853638 );
    generateOne( anOutStream, 884.168213, -294.37439, 838.6137700, -355.969116 );
    generateOne( anOutStream, 915.607178, -327.096588, 875.185669, -366.234894 );
    generateOne( anOutStream, 940.630066, -348.269775, 903.416565, -381.633575 );
    generateOne( anOutStream, 963.728088, -370.726166, 936.780396, -397.032257 );
    generateOne( anOutStream, 983.618042, -390.61615, 959.878418, -411.789337 );
    generateOne( anOutStream, 999.658325, -407.939667, 978.485107, -425.904785 );
    generateOne( anOutStream, 1018.265076, -422.696716, 996.450256, -443.228302 );
    generateOne( anOutStream, 1040.079834, -441.303467, 1017.623413, -459.9102175 );
    generateOne( anOutStream, 1055.478516, -456.060547, 1034.946899, -475.9505 );
    generateOne( anOutStream, 1074.085327, -474.025665, 1058.044922, -493.274017 );
  }

  aTmpFile.close();

  return true;
}

void test_HYDROData_Bathymetry::testFileImport()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Bathymetry) aBathymetry = 
    Handle(HYDROData_Bathymetry)::DownCast( aDoc->CreateObject( KIND_BATHYMETRY ) );

  QString aFileName = TMP_DIR + QDir::separator() + "test.xyz";
  if ( !createTestFile( aFileName ) )
    return; // No file has been created

  CPPUNIT_ASSERT( aBathymetry->ImportFromFiles( QStringList(aFileName)) );

  HYDROData_Bathymetry::AltitudePoints anAltitudePoints = aBathymetry->GetAltitudePoints();
  CPPUNIT_ASSERT_EQUAL( 2300, (int)anAltitudePoints.size() );

  gp_XY aTestPoint( 1, 1 );
  double anAltitude = aBathymetry->GetAltitudeForPoint( aTestPoint, 1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.2432, anAltitude, EPS );

  aTestPoint = gp_XY( 0.5, 0.5 );
  anAltitude = aBathymetry->GetAltitudeForPoint( aTestPoint, 1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -0.591602, anAltitude, EPS );

  aTestPoint = gp_XY( 1.5, 1 );
  anAltitude = aBathymetry->GetAltitudeForPoint( aTestPoint, 1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.2432, anAltitude, EPS );

  aTestPoint = gp_XY( 1.5, 0.7 );
  anAltitude = aBathymetry->GetAltitudeForPoint( aTestPoint, 1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -0.591602, anAltitude, EPS );

  aTestPoint = gp_XY( 1.5, -0.7 );
  anAltitude = aBathymetry->GetAltitudeForPoint( aTestPoint, 1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -0.271267, anAltitude, EPS );

  aTestPoint = gp_XY( 2, 3.5 );
  anAltitude = aBathymetry->GetAltitudeForPoint( aTestPoint, 1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 13.9454, anAltitude, EPS );

  aDoc->Close();
}


void test_HYDROData_Bathymetry::testCopy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  
  Handle(HYDROData_Bathymetry) aBathymetry1 = 
    Handle(HYDROData_Bathymetry)::DownCast( aDoc->CreateObject( KIND_BATHYMETRY ) );

  QString aFileName = TMP_DIR + QDir::separator() + "test.xyz";

  bool anIsFileCreated = createTestFile( aFileName );
  
  if ( anIsFileCreated )
  {
    CPPUNIT_ASSERT( aBathymetry1->ImportFromFiles( QStringList(aFileName ) ) );

    HYDROData_Bathymetry::AltitudePoints anAltitudePoints = aBathymetry1->GetAltitudePoints();
    CPPUNIT_ASSERT_EQUAL( 2300, (int)anAltitudePoints.size() );
  }

  Handle(HYDROData_Bathymetry) aBathymetry2 = 
    Handle(HYDROData_Bathymetry)::DownCast( aDoc->CreateObject( KIND_BATHYMETRY ) );

  aBathymetry1->CopyTo( aBathymetry2, true );

  if ( anIsFileCreated )
  {
    HYDROData_Bathymetry::AltitudePoints anAltitudePoints = aBathymetry2->GetAltitudePoints();
    CPPUNIT_ASSERT_EQUAL( 2300, (int)anAltitudePoints.size() );
  }

  aDoc->Close();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <cppunit/extensions/HelperMacros.h>

class test_HYDROData_Image : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(test_HYDROData_Image);
  CPPUNIT_TEST(testQImage);
  CPPUNIT_TEST(testTrsf);
  CPPUNIT_TEST(testReferences);
  CPPUNIT_TEST(testCopy);
  CPPUNIT_TEST(testDiff);
  CPPUNIT_TEST(test_static_loadECW);
  CPPUNIT_TEST(test_loadECW);
  CPPUNIT_TEST_SUITE_END();

private:

public:

  void setUp() {}

  void tearDown() {}

  // checks save/restore QImages information
  void testQImage();

  // checks the transformations
  void testTrsf();

  // checks the references management
  void testReferences();

  // checks the image properties copy/paste
  void testCopy();
  void testDiff();

  //  checks if content of reference png file and ecw file is the same
  void test_static_loadECW();
  void test_loadECW();
};

CPPUNIT_TEST_SUITE_REGISTRATION(test_HYDROData_Image);
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION(test_HYDROData_Image, "HYDROData_Image");

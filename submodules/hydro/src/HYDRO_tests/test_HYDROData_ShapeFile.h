// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#include <cppunit/extensions/HelperMacros.h>
#include <QString>

class test_HYDROData_ShapeFile : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( test_HYDROData_ShapeFile );
  CPPUNIT_TEST( test_openDbf );     
  CPPUNIT_TEST( test_NbFieldsDbf );     
  CPPUNIT_TEST( test_FieldListDbf );
  CPPUNIT_TEST( test_FieldTypeListDbf );
  CPPUNIT_TEST( test_NbRecordsDbf );
  CPPUNIT_TEST( test_GetAttrListIndex );
  CPPUNIT_TEST( test_DbfWrite );

  CPPUNIT_TEST_SUITE_END();

public:
  void test_openDbf();
  void test_NbFieldsDbf();
  void test_FieldListDbf();
  void test_FieldTypeListDbf();
  void test_NbRecordsDbf();
  void test_GetAttrListIndex ();
  void test_DbfWrite();
private:
  bool compare_two_files(const QString& File1, const QString& File2);
};

CPPUNIT_TEST_SUITE_REGISTRATION( test_HYDROData_ShapeFile );
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( test_HYDROData_ShapeFile, "HYDROData_ShapeFile" );


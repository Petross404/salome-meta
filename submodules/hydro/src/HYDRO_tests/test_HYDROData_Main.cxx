// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <stdexcept>
#include <TestViewer.h>
#include <TestLib_Listener.h>
#include <TestLib_Runner.h>
#include <OCCViewer_ViewFrame.h>
#include <SUIT_Session.h>
#include <QApplication>
#include <QColor>
#include <QTest>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

#ifdef WIN32
QString REF_DATA_PATH = qgetenv( "HYDRO_ROOT_DIR" ) + "/bin/salome/test/HYDRO/HYDRO";
QString TMP_DIR = QDir::tempPath();
#else
QString REF_DATA_PATH = qgetenv( "HYDRO_ROOT_DIR" ) + "/bin/salome/test/HYDRO/HYDRO";
QString TMP_DIR = qgetenv( "HYDRO_ROOT_DIR" ) + "/test_ref";
#endif


int MAIN_W = 800, MAIN_H = 600;

int main( int argc, char* argv[] )
{
  QApplication anApp( argc, argv );
  SUIT_Session aSession;
  aSession.startApplication("std");

  QDir().mkdir( TMP_DIR );

  OCCViewer_ViewFrame* aWindow = TestViewer::viewWindow();

  aWindow->setGeometry( 400, 100, MAIN_W, MAIN_H );
  aWindow->show();
  QTest::qWaitForWindowExposed( aWindow );

  int dy = 34;
  //std::cout << dx << "," << dy << std::endl;
  aWindow->resize( MAIN_W, MAIN_H+dy );
  anApp.processEvents();

  std::string testPath = (argc > 1) ? std::string(argv[1]) : "";

  // Create the event manager and test controller
  CppUnit::TestResult controller;

  // Add a listener that collects test result
  CppUnit::TestResultCollector result;
  controller.addListener( &result );

  // Add a listener that print dots as test run.
  TestLib_Listener progress;
  controller.addListener( &progress );

  CppUnit::TestFactoryRegistry& registry =
    CppUnit::TestFactoryRegistry::getRegistry();
  // Add the top suite to the test runner
  TestLib_Runner runner;
#ifdef WIN32
  QString aPath = qgetenv( "HYDRO_SRC_DIR" ) + "/src/tests.cfg";
#else
  QString aPath = qgetenv( "HYDRO_ROOT_DIR" ) + "/tests.cfg";
#endif
  runner.Load( aPath.toStdString() );
  runner.addTest( registry.makeTest() );
  try
  {
    std::cout << "Running tests "  << testPath << "..." << std::endl;
    runner.Run( controller );

    std::cerr << std::endl;

    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
    outputter.write();
  }
  catch ( std::invalid_argument &e )  // Test path not resolved
  {
    std::cerr  <<  std::endl
      <<  "ERROR: "  <<  e.what()
      << std::endl;
    return 0;
  }
  bool isOK = result.wasSuccessful();

#ifndef WIN32
  DEBTRACE("End of tests");
#endif

  aWindow->close();
  aSession.closeSession();
  anApp.exit(!isOK);

#ifndef WIN32
  DEBTRACE("--- TODO: exception on exit..."); // TODO: exception on exit...
#endif

  int ms = progress.GetCompleteTimeInMS();
  printf( "\n\n" );
  printf( "%i TESTS in %i SUITES\n", progress.GetNbTests(), progress.GetNbSuites() );
  printf( "COMPLETE TESTS TIME: %i ms\n", ms );
  progress.DumpFailures();

  return result.wasSuccessful() ? 0 : 1;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#undef HYDROGUI_EXPORTS

#include <test_HYDROGUI_LandCoverMapDlg.h>
#include <HYDROGUI_LandCoverArgsFilter.h>
#include <HYDROGUI_Operations.h>
#include <HYDROData_Document.h>
#include <HYDROData_PolylineXY.h>
#include <gp_XY.hxx>

bool isOK( const HYDROGUI_LandCoverArgsFilter& theFilter, 
           const Handle(HYDROData_Document)& theDocument,
           ObjectKind theKind )
{
  Handle(HYDROData_Entity) anEntity = theDocument->CreateObject( theKind );
  return theFilter.isOk( anEntity );
}

void test_HYDROGUI_LandCoverMapDlg::test_objects_filtering_refs_707()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  
  HYDROGUI_LandCoverArgsFilter f( AddLandCoverId );

  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_IMAGE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_POLYLINE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_BATHYMETRY ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_ALTITUDE ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_IMMERSIBLE_ZONE ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_RIVER ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_STREAM ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_CONFLUENCE ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_CHANNEL ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_OBSTACLE ) );
  CPPUNIT_ASSERT_EQUAL( true,  isOK( f, aDoc, KIND_DIGUE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_PROFILE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_PROFILEUZ ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_CALCULATION ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_ZONE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_REGION ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_VISUAL_STATE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_ARTIFICIAL_OBJECT ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_NATURAL_OBJECT ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_DUMMY_3D ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_SHAPES_GROUP ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_SPLIT_GROUP ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_STREAM_ALTITUDE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_OBSTACLE_ALTITUDE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_LAND_COVER_OBSOLETE ) );
  CPPUNIT_ASSERT_EQUAL( false, isOK( f, aDoc, KIND_LAND_COVER_MAP ) );

  Handle(HYDROData_PolylineXY) aClosedPoly = Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aClosedPoly->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, true );
  aClosedPoly->AddPoint( 0, gp_XY( 0, 0 ) );
  aClosedPoly->AddPoint( 0, gp_XY( 20, 0 ) );
  aClosedPoly->AddPoint( 0, gp_XY( 10, 10 ) );
  aClosedPoly->Update();
  CPPUNIT_ASSERT_EQUAL( true, f.isOk( aClosedPoly ) );

  Handle(HYDROData_PolylineXY) anOpenPoly = Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  anOpenPoly->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, false );
  anOpenPoly->AddPoint( 0, gp_XY( 0, 0 ) );
  anOpenPoly->AddPoint( 0, gp_XY( 20, 0 ) );
  anOpenPoly->AddPoint( 0, gp_XY( 10, 10 ) );
  anOpenPoly->Update();
  CPPUNIT_ASSERT_EQUAL( false, f.isOk( anOpenPoly ) );

  HYDROGUI_LandCoverArgsFilter fs( SplitLandCoverId );
  CPPUNIT_ASSERT_EQUAL( true, fs.isOk( aClosedPoly ) );
  CPPUNIT_ASSERT_EQUAL( true, fs.isOk( anOpenPoly ) );

  aDoc->Close();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#undef HYDROGUI_EXPORTS

#include <test_HYDROGUI_ProfilesDlg.h>

#define private public
#include <HYDROGUI_ProfileDlg.h>
#include <CurveCreator_Widget.h>
#undef private 

#include <HYDROGUI_CurveCreatorProfile.h>
#include <CurveCreator_TableView.h>
#include <CurveCreator_Displayer.hxx>
#include <CurveCreator_Utils.hxx>

#include <HYDROData_Document.h>
#include <HYDROData_Profile.h>


#include <TestViewer.h>
#include <QApplication>
#include <QAction>
#include <QSplitter>
#include <QTest>

#define CPPUNIT_ASSERT_WIDGET( theWidget, theCase )                           \
  {                                                                           \
    QPixmap aPixmap = QPixmap::grabWindow( theWidget->winId() );              \
    QImage anImage = aPixmap.toImage();                                       \
    QString aMessage;                                                         \
    if( !TestViewer::AssertImages( aMessage, &anImage, theCase, false ) )     \
    {                                                                         \
      TestViewer::showColorScale( false );                                    \
      std::string aMessageStl = aMessage.toStdString();                       \
      CPPUNIT_FAIL( aMessageStl.c_str() );                                    \
    }                                                                         \
  }                                                                           \


void test_HYDROGUI_ProfilesDlg::test_default_size()
{
  return;
  HYDROGUI_ProfileDlg* dlg = new HYDROGUI_ProfileDlg( 0, "test", true );
  dlg->resize( 320, 640 );
  dlg->show();
  QTest::qWaitForWindowExposed( dlg );

  CPPUNIT_ASSERT_WIDGET( dlg, "profiles_dlg_presentation" );

  dlg->deleteLater();
  qApp->processEvents();
}

HYDROGUI_CurveCreatorProfile* profile()
{
  static HYDROGUI_CurveCreatorProfile* aProfile = 0;
  if( aProfile )
    return aProfile;

  aProfile = new HYDROGUI_CurveCreatorProfile();
  CurveCreator::Coordinates coords;
  coords.push_back( 0 );
  coords.push_back( 20 );
  coords.push_back( 30 );
  coords.push_back( 0 );
  coords.push_back( 60 );
  coords.push_back( 0 );
  coords.push_back( 90 );
  coords.push_back( 20 );
  aProfile->addPoints( coords, 0 );
  return aProfile;
}

void show_profile( HYDROGUI_ProfileDlg* dlg )
{
  Handle(AIS_InteractiveContext) aCtx = dlg->getAISContext();
  CurveCreator_Displayer* aDisplayer = new CurveCreator_Displayer( aCtx );
  profile()->setDisplayer( aDisplayer );
  aDisplayer->display( profile()->getAISObject( true ), true );
}

void select_points( HYDROGUI_ProfileDlg* dlg )
{
  CurveCreator_ICurve::SectionToPointList sel;
  sel.push_back( CurveCreator_ICurve::SectionToPoint( 0, 0 ) );
  sel.push_back( CurveCreator_ICurve::SectionToPoint( 0, 1 ) );
  sel.push_back( CurveCreator_ICurve::SectionToPoint( 0, 2 ) );
  sel.push_back( CurveCreator_ICurve::SectionToPoint( 0, 3 ) );
  dlg->myEditorWidget->getAction( CurveCreator_Widget::ModificationMode )->toggle();
  dlg->myEditorWidget->onModificationMode( true );
  dlg->myEditorWidget->setSelectedPoints( sel );
  dlg->myEditorWidget->updateLocalPointView();
}

std::string coords( HYDROGUI_ProfileDlg* dlg, int col=2 )
{
  int n = dlg->myEditorWidget->myLocalPointView->rowCount();
  QStringList datas;
  for( int r=0; r<n; r++ )
  {
    QString data = dlg->myEditorWidget->myLocalPointView->item(r, col)->data( Qt::DisplayRole ).toString();
    datas.append( data );
  }
  return datas.join( ", " ).toStdString();
}

void setCoords( HYDROGUI_ProfileDlg* dlg, int theIndex, double theValue )
{
  QAbstractItemModel* m = dlg->myEditorWidget->myLocalPointView->model();
  QModelIndex index = m->index( theIndex, 2 );

  m->setData( index, theValue, Qt::UserRole );
  dlg->myEditorWidget->updateLocalPointView();
}

void test_HYDROGUI_ProfilesDlg::test_points_table()
{
  return;
  HYDROGUI_ProfileDlg* dlg = new HYDROGUI_ProfileDlg( 0, "test", false ); 
  dlg->resize( 320, 800 );
  dlg->setProfile( profile() );
  dlg->show();
  QTest::qWaitForWindowExposed( dlg );

  show_profile( dlg );  
  select_points( dlg );

  QList<int> sizes;
  sizes.append( 25 );
  sizes.append( 100 );
  sizes.append( 200 );
  sizes.append( 25 );
  sizes.append( 25 );
  dlg->splitter()->setSizes( sizes );
  qApp->processEvents();

  CPPUNIT_ASSERT_WIDGET( dlg, "profiles_selected_points" );
  CPPUNIT_ASSERT_EQUAL( std::string( "0, 30, 60, 90" ), coords( dlg ) );
  CPPUNIT_ASSERT_EQUAL( std::string( "20, 0, 0, 20" ), coords( dlg, 3 ) );
  
  setCoords( dlg, 0, 130 );
  CPPUNIT_ASSERT_EQUAL( std::string( "130, 30, 60, 90" ), coords( dlg ) );
  CPPUNIT_ASSERT_EQUAL( std::string( "20, 0, 0, 20" ), coords( dlg, 3 ) );

  setCoords( dlg, 0, 0 );
  CPPUNIT_ASSERT_EQUAL( std::string( "0, 30, 60, 90" ), coords( dlg ) );
  CPPUNIT_ASSERT_EQUAL( std::string( "20, 0, 0, 20" ), coords( dlg, 3 ) );

  setCoords( dlg, 0, 80 );
  dlg->myEditorWidget->myLocalPointView->OnHeaderClick( 2 );
  CPPUNIT_ASSERT_EQUAL( std::string( "30, 60, 80, 90" ), coords( dlg ) );
  CPPUNIT_ASSERT_EQUAL( std::string( "0, 0, 20, 20" ), coords( dlg, 3 ) );

  setCoords( dlg, 2, 0 );
  CPPUNIT_ASSERT_EQUAL( std::string( "30, 60, 0, 90" ), coords( dlg ) );
  CPPUNIT_ASSERT_EQUAL( std::string( "0, 0, 20, 20" ), coords( dlg, 3 ) );

  //qApp->processEvents();
  //QTest::qWait( 50000 );

  dlg->deleteLater();
  qApp->processEvents();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_DTM.h>
#include <HYDROData_Document.h>
#include <HYDROData_Profile.h>
#include <HYDROData_DTM.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_CalculationCase.h>
#include <Geom2d_Curve.hxx>
#include <Geom2d_BSplineCurve.hxx>
#include <gp_XY.hxx>
#include <gp_Pnt2d.hxx>
#include <TColgp_Array1OfPnt2d.hxx>
#include <TestViewer.h>
#include <AIS_InteractiveContext.hxx>
#include <AIS_PointCloud.hxx>
#include <HYDROGUI_ShapeBathymetry.h>
#include <AIS_ColorScale.hxx>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPixmap>
#include <QApplication>
#include <QTest>
#include <QSet>
#include <QString>

const double EPS = 1E-3;

extern QString REF_DATA_PATH;
NCollection_Sequence<HYDROData_IPolyline::Point> points;

class DTM_item : public QGraphicsItem
{
public:
  DTM_item( HYDROGUI_ShapeBathymetry* theDTM, double d )
  {
    Handle(AIS_PointCloud) pc = Handle(AIS_PointCloud)::DownCast( theDTM->getAISObjects()[0] );
    Handle(Graphic3d_ArrayOfPoints) pp = pc->GetPoints();
    myBB = QRectF();
    double xmin, xmax, ymin, ymax;
    myD = d;

    int n = pp->VertexNumber();

    for( int i=1; i<=n; i++ )
    {
      gp_Pnt pnt = pp->Vertice( i );
      Quantity_Color col = pp->VertexColor( i );

      int r = col.Red()*255;
      int g = col.Green()*255;
      int b = col.Blue()*255;
      int val = ( r << 16 ) + ( g << 8 ) + b;
      double x = pnt.X();
      double y = -pnt.Y();
      QPointF aPnt( x, y );
      myPoints[val].append( aPnt );
      if( i==1 )
      {
        xmin = x;
        xmax = x;
        ymin = y;
        ymax = y;
      }
      else
      {
        if( x>xmax )
          xmax = x;
        if( x<xmin )
          xmin = x;
        if( y>ymax )
          ymax = y;
        if( y<ymin )
          ymin = y;
      }
    }

    xmin -= 10;
    xmax += 10;
    ymin -= 10;
    ymax += 10;
    myBB.setRect( xmin, ymin, xmax-xmin, ymax-ymin );
  }

  virtual QRectF boundingRect() const
  {
    return myBB;
  }

  virtual void paint( QPainter* p, const QStyleOptionGraphicsItem*, QWidget* )
  {
    QMap<int, QList<QPointF> >::const_iterator it = myPoints.begin(), last = myPoints.end();
    for( ; it!=last; it++ )
    {
      int r = ( it.key() >> 16 ) % 256;
      int g = ( it.key() >> 8  ) % 256;
      int b = ( it.key() >> 0  ) % 256;
      QColor aColor( r, g, b );
      QBrush aBrush( aColor );
      foreach( QPointF pnt, it.value() )
      {
        QRectF r( pnt.x()-myD/2, pnt.y()-myD/2, myD, myD );
        p->fillRect( r, aBrush );
      }
    }
  }

private:
  QRectF myBB;
  double myD;
  QMap<int, QList<QPointF> > myPoints;
};

QImage draw_DTM( HYDROGUI_ShapeBathymetry* theDTM, double theD, int theWidth, int theHeight )
{
  QGraphicsScene aScene;
  QGraphicsView aView;
  DTM_item anItem( theDTM, theD );

  aView.setScene( &aScene );
  aView.setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
  aView.setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
  aScene.addItem( &anItem );

  aView.resize( theWidth, theHeight );
  QRectF bb = anItem.boundingRect();
  aView.fitInView( bb, Qt::KeepAspectRatio );
  QApplication::processEvents();

  QPixmap aPixmap = QPixmap::grabWidget( &aView );
  return aPixmap.toImage();
}


void test_HYDROData_DTM::setUp()
{
  points.Clear();
  points.Append( gp_XY( 0.0, 5.0 ) );
  points.Append( gp_XY( 1.0, 1.0 ) );
  points.Append( gp_XY( 1.5, 0.0 ) );
  points.Append( gp_XY( 4.0, 4.0 ) );
}

void test_HYDROData_DTM::test_creation()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_DTM) DTM =
    Handle(HYDROData_DTM)::DownCast( aDoc->CreateObject( KIND_DTM ) );

  CPPUNIT_ASSERT_EQUAL( false, (bool)DTM.IsNull() );

  aDoc->Close();
}

void test_HYDROData_DTM::test_hydraulic_axis()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  Handle(HYDROData_Profile) aProfile3 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile1->SetParametricPoints( points );
  aProfile1->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile1->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile1->SetRightPoint( gp_XY( 20, 0 ) );

  aProfile2->SetParametricPoints( points );
  aProfile2->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile2->SetLeftPoint( gp_XY( 50, 0 ) );
  aProfile2->SetRightPoint( gp_XY( 60, 10 ) );

  aProfile3->SetParametricPoints( points );
  aProfile3->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile3->SetLeftPoint( gp_XY( 200, 50 ) );
  aProfile3->SetRightPoint( gp_XY( 210, 40 ) );

  std::vector<double> distances;
  std::vector<Handle(HYDROData_Profile)> profiles;
  profiles.push_back( aProfile1 );
  profiles.push_back( aProfile2 );
  profiles.push_back( aProfile3 );

  Handle(Geom2d_BSplineCurve) HA = HYDROData_DTM::CreateHydraulicAxis( profiles, distances );
  CPPUNIT_ASSERT_EQUAL( false, (bool)HA.IsNull() );
  CPPUNIT_ASSERT_EQUAL( 3, (int)distances.size() );

  CPPUNIT_ASSERT_DOUBLES_EQUAL(   0.0,   distances[0], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  44.859, distances[1], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 207.661, distances[2], EPS );

  gp_Pnt2d p;
  gp_Vec2d q;
  HA->D1( 0, p, q );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 13.75, p.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 6.25, p.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.532, q.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.532, q.Y(), EPS );

  aDoc->Close();
}

void test_HYDROData_DTM::test_profile_conversion_to_2d()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile1->SetParametricPoints( points );
  aProfile1->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile1->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile1->SetRightPoint( gp_XY( 20, 20 ) );

  aProfile2->SetParametricPoints( points );
  aProfile2->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_SPLINE );
  aProfile2->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile2->SetRightPoint( gp_XY( 20, 20 ) );

  double aUMin1 = DBL_MAX,
         aUMax1 = -aUMin1,
         aUMin2 = aUMin1,
         aUMax2 = aUMax1;
  gp_Vec2d aProfileDir;
  std::vector<Handle(Geom2d_Curve)> curves1 = HYDROData_DTM::ProfileToParametric( aProfile1, aUMin1, aUMax1, aProfileDir );
  std::vector<Handle(Geom2d_Curve)> curves2 = HYDROData_DTM::ProfileToParametric( aProfile2, aUMin2, aUMax2, aProfileDir );

  gp_Pnt2d aFirst, aLast;
  CPPUNIT_ASSERT_EQUAL( 3, (int)curves1.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -5.303, aUMin1, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  8.839, aUMax1, EPS );
  curves1[0]->D0( curves1[0]->FirstParameter(), aFirst );
  curves1[0]->D0( curves1[0]->LastParameter(), aLast );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -5.303, aFirst.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  5.0,   aFirst.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -1.768, aLast.X(),  EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.0,   aLast.Y(),  EPS );
  curves1[1]->D0( curves1[1]->FirstParameter(), aFirst );
  curves1[1]->D0( curves1[1]->LastParameter(), aLast );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -1.768, aFirst.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.0,   aFirst.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aLast.X(),  EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aLast.Y(),  EPS );
  curves1[2]->D0( curves1[2]->FirstParameter(), aFirst );
  curves1[2]->D0( curves1[2]->LastParameter(), aLast );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aFirst.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aFirst.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  8.839, aLast.X(),  EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  4.0,   aLast.Y(),  EPS );

  CPPUNIT_ASSERT_EQUAL( 1, (int)curves2.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -5.303, aUMin2, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  8.839, aUMax2, EPS );
  Handle(Geom2d_BSplineCurve) aBSpline = Handle(Geom2d_BSplineCurve)::DownCast( curves2[0] );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aBSpline.IsNull() );
  const TColgp_Array1OfPnt2d& poles = aBSpline->Poles();
  CPPUNIT_ASSERT_EQUAL( 1, (int)poles.Lower() );
  CPPUNIT_ASSERT_EQUAL( 8, (int)poles.Upper() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -5.303, poles.Value( 1 ).X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  5.0,   poles.Value( 1 ).Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -4.125, poles.Value( 2 ).X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  3.667, poles.Value( 2 ).Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -3.150, poles.Value( 3 ).X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  2.120, poles.Value( 3 ).Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -1.242, poles.Value( 4 ).X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.574, poles.Value( 4 ).Y(), EPS );

  aDoc->Close();
}

void test_HYDROData_DTM::test_profile_properties()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile->SetParametricPoints( points );
  aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile->SetRightPoint( gp_XY( 20, 25 ) );

  gp_Pnt lp;
  gp_Vec2d dd;
  double zmin, zmax;
  HYDROData_DTM::GetProperties( aProfile, lp, dd, zmin, zmax );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 13.75, lp.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 15.625, lp.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0, lp.Z(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 10, dd.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 15, dd.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.0, zmin, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 5.0, zmax, EPS );

 /* HYDROData_DTM::GetProperties( aProfile, lp, dd, true, zmin, zmax );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 13.75, lp.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 15.625, lp.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0, lp.Z(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -15, dd.X(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 10, dd.Y(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.0, zmin, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 5.0, zmax, EPS );*/

  aDoc->Close();
}

void test_HYDROData_DTM::test_profile_discretization_polyline()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile->SetParametricPoints( points );
  aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile->SetRightPoint( gp_XY( 20, 20 ) );

  HYDROData_DTM::CurveUZ aMid( 0.0, gp_Vec2d(), 0, 0 ), aWid( 0.0, gp_Vec2d(), 0, 0 );
  int dummy = 0;
  QSet<QString> warnings;
  HYDROData_DTM::ProfileDiscretization( aProfile, 0.0, 0.0, 5.0, 5.0, 0.5, aMid, aWid, dummy, 1E-6, warnings );
  CPPUNIT_ASSERT_EQUAL( 11, (int)aMid.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aMid[0].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aMid[0].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.11,  aMid[1].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.5,   aMid[1].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.215, aMid[5].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  2.5,   aMid[5].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.768, aMid[10].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  5.0,   aMid[10].Z, EPS );

  CPPUNIT_ASSERT_EQUAL( 11, (int)aWid.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aWid[0].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aWid[0].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.989, aWid[1].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.5,   aWid[1].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  8.618, aWid[5].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  2.5,   aWid[5].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 14.142, aWid[10].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  5.0,   aWid[10].Z, EPS );

  aDoc->Close();
}

void test_HYDROData_DTM::test_profile_discretization_spline()
{
Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile->SetParametricPoints( points );
  aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_SPLINE );
  aProfile->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile->SetRightPoint( gp_XY( 20, 20 ) );

  HYDROData_DTM::CurveUZ aMid( 0.0, gp_Vec2d(), 0, 0 ), aWid( 0.0, gp_Vec2d(), 0, 0 );
  int dummy = 0 ;
  QSet<QString> warnings;
  HYDROData_DTM::ProfileDiscretization( aProfile, 0.0, 0.0, 5.0, 5.0, 0.5, aMid, aWid, dummy, 1E-6, warnings );
  CPPUNIT_ASSERT_EQUAL( 11, (int)aMid.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.242, aMid[0].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aMid[0].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.755, aMid[1].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.5,   aMid[1].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.473, aMid[5].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  2.5,   aMid[5].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  1.768, aMid[10].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  5.0,   aMid[10].Z, EPS );

  CPPUNIT_ASSERT_EQUAL( 11, (int)aWid.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.484, aWid[0].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.0,   aWid[0].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  3.809, aWid[1].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  0.5,   aWid[1].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  9.472, aWid[5].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  2.5,   aWid[5].Z, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 14.142, aWid[10].U, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  5.0,   aWid[10].Z, EPS );

  aDoc->Close();
}

void operator << ( std::ostream& s, const HYDROData_DTM::PointUZ& p )
{
  s << "(" << p.U << "; " << p.Z << ") ";
}

void operator << ( std::ostream& s, const HYDROData_DTM::AltitudePoint& p )
{
  s << "(" << p.X << "; " << p.Y << "; " << p.Z << ") ";
}

bool operator == ( const HYDROData_DTM::PointUZ& p1, const HYDROData_DTM::PointUZ& p2 )
{
  return fabs(p1.U-p2.U)<EPS && fabs(p1.Z-p2.Z)<EPS;
}

bool operator == ( const HYDROData_DTM::AltitudePoint& p1, const HYDROData_DTM::AltitudePoint& p2 )
{
  return fabs(p1.X-p2.X)<EPS && fabs(p1.Y-p2.Y)<EPS && fabs(p1.Z-p2.Z)<EPS;
}

void operator << ( std::ostream& s, const HYDROData_DTM::CurveUZ& c )
{
  size_t n = c.size();
  for( size_t i=0; i<n; i++ )
    s << c[i];
}

void test_HYDROData_DTM::test_curves_interpolation()
{
  HYDROData_DTM::CurveUZ A(1.0, gp_Vec2d(), 0, 0), B(2.0, gp_Vec2d(), 0, 0);
  A.push_back( HYDROData_DTM::PointUZ( 0, 0 ) );
  A.push_back( HYDROData_DTM::PointUZ( 1, 1 ) );
  A.push_back( HYDROData_DTM::PointUZ( 2, 2 ) );
  B.push_back( HYDROData_DTM::PointUZ( 10, 0 ) );
  B.push_back( HYDROData_DTM::PointUZ( 15, 1 ) );
  B.push_back( HYDROData_DTM::PointUZ( 20, 2 ) );

  std::vector<HYDROData_DTM::CurveUZ> i1;
  HYDROData_DTM::Interpolate( A, B, 1, i1, false );

  CPPUNIT_ASSERT_EQUAL( 2, (int)i1.size() );
  CPPUNIT_ASSERT_EQUAL( A, i1[0] );
  CPPUNIT_ASSERT_EQUAL( 3, (int)i1[1].size() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 5, 0 ), i1[1][0] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 8, 1 ), i1[1][1] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 11, 2 ), i1[1][2] );

  std::vector<HYDROData_DTM::CurveUZ> i2;
  HYDROData_DTM::Interpolate( A, B, 1, i2, true );

  CPPUNIT_ASSERT_EQUAL( 3, (int)i2.size() );
  CPPUNIT_ASSERT_EQUAL( A, i2[0] );
  CPPUNIT_ASSERT_EQUAL( 3, (int)i2[1].size() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 5, 0 ), i2[1][0] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 8, 1 ), i2[1][1] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 11, 2 ), i2[1][2] );
  CPPUNIT_ASSERT_EQUAL( B, i2[2] );

  std::vector<HYDROData_DTM::CurveUZ> i3;
  HYDROData_DTM::Interpolate( A, B, 3, i3, false );

  CPPUNIT_ASSERT_EQUAL( 4, (int)i3.size() );
  CPPUNIT_ASSERT_EQUAL( A, i3[0] );
  CPPUNIT_ASSERT_EQUAL( 3, (int)i3[1].size() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 2.5, 0 ), i3[1][0] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 4.5, 1 ), i3[1][1] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::PointUZ( 6.5, 2 ), i3[1][2] );
}

void test_HYDROData_DTM::test_curve_to_3d()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile1->SetParametricPoints( points );
  aProfile1->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile1->SetLeftPoint( gp_XY( 20, 0 ) );
  aProfile1->SetRightPoint( gp_XY( 10, 10 ) );

  aProfile2->SetParametricPoints( points );
  aProfile2->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile2->SetLeftPoint( gp_XY( 100, 0 ) );
  aProfile2->SetRightPoint( gp_XY( 110, 0 ) );

  std::vector<double> distances;
  std::vector<Handle(HYDROData_Profile)> profiles;
  profiles.push_back( aProfile1 );
  profiles.push_back( aProfile2 );

  Handle(Geom2d_BSplineCurve) HA = HYDROData_DTM::CreateHydraulicAxis( profiles, distances );
  HYDROData_DTM::AltitudePoints points;
  HYDROData_DTM::CurveUZ mid( 5.0, gp_Vec2d(-10,10), 0, 10 );
  mid.push_back( HYDROData_DTM::PointUZ( 0, 5 ) );
  mid.push_back( HYDROData_DTM::PointUZ( 1, 6 ) );
  HYDROData_DTM::CurveUZ wid( 5.0, gp_Vec2d(-10,10), 0, 10 );
  wid.push_back( HYDROData_DTM::PointUZ( 2, 5 ) );
  wid.push_back( HYDROData_DTM::PointUZ( 6, 6 ) );
  HYDROData_DTM::CurveTo3D( HA, mid, wid, points );

  CPPUNIT_ASSERT_EQUAL( 4, (int)points.size() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::AltitudePoint( 21.588, 5.419, 6.0 ), points[0] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::AltitudePoint( 20.881, 6.126, 5.0 ), points[1] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::AltitudePoint( 19.466, 7.541, 5.0 ), points[2] );
  CPPUNIT_ASSERT_EQUAL( HYDROData_DTM::AltitudePoint( 17.345, 9.662, 6.0 ), points[3] );

  aDoc->Close();
}

void test_HYDROData_DTM::test_presentation()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_DTM) DTM = Handle(HYDROData_DTM)::DownCast( aDoc->CreateObject( KIND_DTM ) );

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile1->SetParametricPoints( points );
  aProfile1->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_SPLINE );
  aProfile1->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile1->SetRightPoint( gp_XY( 20, 0 ) );

  aProfile2->SetParametricPoints( points );
  aProfile2->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_SPLINE );
  aProfile2->SetLeftPoint( gp_XY( 110, 10 ) );
  aProfile2->SetRightPoint( gp_XY( 100, 0 ) );

  HYDROData_SequenceOfObjects seq;
  seq.Append( aProfile1 );
  seq.Append( aProfile2 );
  DTM->SetProfiles( seq );
  DTM->SetDDZ( 0.1 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.1, DTM->GetDDZ(), EPS );
  DTM->SetSpatialStep( 1.0 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0, DTM->GetSpatialStep(), EPS );
  DTM->Update();

  CPPUNIT_ASSERT_EQUAL( 9108, (int)DTM->GetAltitudePoints().size() );

  Handle(AIS_InteractiveContext) aContext = TestViewer::context();
  HYDROGUI_ShapeBathymetry* aBathPrs = new HYDROGUI_ShapeBathymetry( 0, aContext, DTM );
  aBathPrs->update( true, false );
  aBathPrs->RescaleDefault();

  bool ColorScaleIsDisp = TestViewer::ColorScaleIsDisplayed();
  TestViewer::showColorScale( true );
  Handle(AIS_ColorScale) aCS = TestViewer::colorScale();
  aCS->SetMin( 0.0 );
  aCS->SetMax( 5.0 );
  aCS->SetNumberOfIntervals( 10 );
  aBathPrs->UpdateWithColorScale( aCS );
  //QTest::qWait(50000);
  QImage aDTMPrs = draw_DTM( aBathPrs, 0.5, 600, 600 );
  CPPUNIT_ASSERT_IMAGES3( &aDTMPrs, "DTM_1", false );
  delete aBathPrs;
  TestViewer::showColorScale( ColorScaleIsDisp );

  aDoc->Close();
}

void test_HYDROData_DTM::test_garonne()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/Profiles.xyz";
  NCollection_Sequence<int> bad_ids;

  int aSize = HYDROData_Profile::ImportFromFile( aDoc, fname, bad_ids, true );

  CPPUNIT_ASSERT_EQUAL( 0, bad_ids.Size() );
  CPPUNIT_ASSERT_EQUAL( 46, aSize );

  HYDROData_SequenceOfObjects profiles;
  HYDROData_Iterator it( aDoc, KIND_PROFILE );
  for( int i=0; it.More(); it.Next(), i++ )
  {
    if( i>=25 && i<=35 )
      profiles.Append( Handle(HYDROData_Profile)::DownCast( it.Current() ) );
  }

  CPPUNIT_ASSERT_EQUAL( 11, (int)profiles.Size() );

  Handle(HYDROData_DTM) DTM = Handle(HYDROData_DTM)::DownCast( aDoc->CreateObject( KIND_DTM ) );
  DTM->SetProfiles( profiles );
  DTM->SetDDZ( 0.1 );
  DTM->SetSpatialStep( 1.0 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.1, DTM->GetDDZ(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0, DTM->GetSpatialStep(), EPS );
  DTM->Update();

  CPPUNIT_ASSERT_EQUAL( 281898, (int)DTM->GetAltitudePoints().size() );

  Handle(AIS_InteractiveContext) aContext = TestViewer::context();
  HYDROGUI_ShapeBathymetry* aBathPrs = new HYDROGUI_ShapeBathymetry( 0, aContext, DTM );
  aBathPrs->update( true, false );
  aBathPrs->RescaleDefault();

  bool ColorScaleIsDisp = TestViewer::ColorScaleIsDisplayed();

  TestViewer::showColorScale( true );
  Handle(AIS_ColorScale) aCS = TestViewer::colorScale();
  aCS->SetMin( 0.0 );
  aCS->SetMax( 25.0 );
  aCS->SetNumberOfIntervals( 30 );
  aBathPrs->UpdateWithColorScale( aCS );

  QImage aDTMPrs = draw_DTM( aBathPrs, 0.5, 600, 600 );
  CPPUNIT_ASSERT_IMAGES3( &aDTMPrs, "DTM_2", false );
  TestViewer::showColorScale( ColorScaleIsDisp );
  delete aBathPrs;
  aDoc->Close();
}

void test_HYDROData_DTM::test_classifier_1()
{
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/pb_1066.cbf";
  CPPUNIT_ASSERT_EQUAL( (int)DocError_OK, (int)HYDROData_Document::Load( fname.ToCString() ) );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_CalculationCase) aCase =
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->FindObjectByName( "Case_1" ) );
  CPPUNIT_ASSERT_EQUAL( false, aCase.IsNull() );
  std::vector<gp_XY> points;
  points.push_back( gp_XY( 43.4842, 3.33176  ) );
  points.push_back( gp_XY( -125.777, 2.24728 ) );
  points.push_back( gp_XY( -60.1628, 168.262 ) );
  points.push_back( gp_XY( 21.8055587645, 154.699344457 ) );
  points.push_back( gp_XY( -84.4764138524, 79.2606012276 ) );
  points.push_back( gp_XY( -73.4132070504, 69.7096313266 ) );

  std::vector<double> values = aCase->GetStricklerCoefficientForPoints( points, 0.0, true );
  CPPUNIT_ASSERT_EQUAL( 6, (int)values.size() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0123, values[0], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0123, values[1], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0221, values[2], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0221, values[3], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0221, values[4], EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.0221, values[5], EPS );

  std::vector<int> types = aCase->GetStricklerTypeForPoints( points );
  CPPUNIT_ASSERT_EQUAL( 6, (int)types.size() );
  CPPUNIT_ASSERT_EQUAL( 123, types[0] );
  CPPUNIT_ASSERT_EQUAL( 123, types[1] );
  CPPUNIT_ASSERT_EQUAL( 221, types[2] );
  CPPUNIT_ASSERT_EQUAL( 221, types[3] );
  CPPUNIT_ASSERT_EQUAL( 221, types[4] );
  CPPUNIT_ASSERT_EQUAL( 221, types[5] );

  aDoc->Close();
}

void test_HYDROData_DTM::test_profile_discretization_warnings()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  {
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

    NCollection_Sequence<gp_XY> pnts;
    pnts.Append( gp_XY( 0.0, 1.0 ) );
    pnts.Append( gp_XY( 1.0, 0.0 ) );
    pnts.Append( gp_XY( 2.0, 4.0 ) );
    pnts.Append( gp_XY( 3.0, 5.0 ) );

    aProfile->SetParametricPoints( pnts );
    aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
    aProfile->SetLeftPoint( gp_XY( 10, 10 ) );
    aProfile->SetRightPoint( gp_XY( 10, 20 ) );

    HYDROData_DTM::CurveUZ aMid( 0.0, gp_Vec2d(), 0, 0 ), aWid( 0.0, gp_Vec2d(), 0, 0 );
    int dummy = 0;
    QSet<QString> warnings;
    HYDROData_DTM::ProfileDiscretization( aProfile, 0.0, 0.0, 5.0, 5.0, 0.5, aMid, aWid, dummy, 1E-6, warnings );

    //CPPUNIT_ASSERT_EQUAL(1, warnings.size());
    CPPUNIT_ASSERT( warnings.contains("One of the extreme points is higher than another"));
  }
  {
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

    NCollection_Sequence<gp_XY> pnts;
    pnts.Append( gp_XY( 0.0, 1.0 ) );
    pnts.Append( gp_XY( 1.0, 0.0 ) );
    pnts.Append( gp_XY( 2.0, 5.0 ) );
    pnts.Append( gp_XY( 3.0, 0.0 ) );
    pnts.Append( gp_XY( 4.0, 1.0 ) );

    aProfile->SetParametricPoints( pnts );
    aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
    aProfile->SetLeftPoint( gp_XY( 10, 10 ) );
    aProfile->SetRightPoint( gp_XY( 10, 20 ) );

    HYDROData_DTM::CurveUZ aMid( 0.0, gp_Vec2d(), 0, 0 ), aWid( 0.0, gp_Vec2d(), 0, 0 );
    int dummy = 0;
    QSet<QString> warnings;
    HYDROData_DTM::ProfileDiscretization( aProfile, 0.0, 0.0, 5.0, 5.0, 0.5, aMid, aWid, dummy, 1E-6, warnings );

    CPPUNIT_ASSERT_EQUAL(1, warnings.size());
    CPPUNIT_ASSERT( warnings.contains("More than 2 intersections between profile & altitude Z-lines found"));
  }
  {
    //plato case
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

    NCollection_Sequence<gp_XY> pnts;
    pnts.Append( gp_XY( 0.0, 5.0 ) );
    pnts.Append( gp_XY( 1.0, 5.0 ) );
    pnts.Append( gp_XY( 2.0, 0.5 ) );
    pnts.Append( gp_XY( 3.0, 5.0 ) );
    pnts.Append( gp_XY( 4.0, 5.0 ) );

    aProfile->SetParametricPoints( pnts );
    aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
    aProfile->SetLeftPoint( gp_XY( 10, 10 ) );
    aProfile->SetRightPoint( gp_XY( 10, 20 ) );

    HYDROData_DTM::CurveUZ aMid( 0.0, gp_Vec2d(), 0, 0 ), aWid( 0.0, gp_Vec2d(), 0, 0 );
    int dummy = 0;
    QSet<QString> warnings;
    HYDROData_DTM::ProfileDiscretization( aProfile, 0.0, 0.0, 5.0, 5.0, 0.5, aMid, aWid, dummy, 1E-6, warnings );

    CPPUNIT_ASSERT_EQUAL(2, warnings.size());
    CPPUNIT_ASSERT( warnings.contains("Plato case on extremes"));
  }

  aDoc->Close();
}

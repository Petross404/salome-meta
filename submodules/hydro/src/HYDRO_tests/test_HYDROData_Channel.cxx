// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_Channel.h>
#include <HYDROData_Document.h>
#include <HYDROData_Channel.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_Profile.h>
#include <HYDROGUI_Polyline.h>

#include <TestShape.h>
#include <TestViewer.h>

#include <AIS_DisplayMode.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <QColor>
#include <QString>

TopoDS_Edge Spline( const QList<double>& theXYList, bool isClosed = false );

void test_HYDROData_Channel::test_channel_prs_refs_751()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) Polyline_2 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_2->SetName( "polyline2d_1" );
  Polyline_2->AddSection( "", HYDROData_IPolyline::SECTION_SPLINE, false );
  Polyline_2->AddPoint( 0, gp_XY( -192.40, 397.86 ) );
  Polyline_2->AddPoint( 0, gp_XY( -53.44, 293.35 ) );
  Polyline_2->AddPoint( 0, gp_XY( 102.14, 225.65 ) );
  Polyline_2->AddPoint( 0, gp_XY( 251.78, 108.08 ) );
  Polyline_2->Update();

  CPPUNIT_ASSERT_EQUAL( false, (bool)Polyline_2->GetShape().IsNull() );

  Handle(HYDROData_Profile) aProfile0 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );
  aProfile0->SetName( "profile_0" );

  HYDROData_IPolyline::PointsList aPoints;
  aPoints.Append( gp_XY( 0.0, 0.0 ) );
  aPoints.Append( gp_XY( 1.0, 0.0 ) );
  aProfile0->GetProfileUZ( true );
  aProfile0->SetParametricPoints( aPoints );
  aProfile0->Update();

  Handle(HYDROData_Polyline3D) aPolyline3d =
    Handle(HYDROData_Polyline3D)::DownCast( aDoc->CreateObject( KIND_POLYLINE ) );
  aPolyline3d->SetName( "polyline3d_1" );
  aPolyline3d->SetPolylineXY( Polyline_2 );
  aPolyline3d->SetProfileUZ( aProfile0->GetProfileUZ() );
  aPolyline3d->Update();

  Handle(HYDROData_Profile) aProfile =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );
  aProfile->SetName( "profile_1" );

  aPoints.Clear();
  aPoints.Append( gp_XY( 0.0, 0.1 ) );
  aPoints.Append( gp_XY( 12.5, 0.0 ) );
  aPoints.Append( gp_XY( 25.0, 0.1 ) );
  aProfile->GetProfileUZ( true );
  aProfile->SetParametricPoints( aPoints );
  aProfile->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_SPLINE );
  aProfile->Update();

  CPPUNIT_ASSERT_EQUAL( false, (bool)aProfile->GetShape3D().IsNull() );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aPolyline3d->GetShape3D().IsNull() );

  Handle(HYDROData_Channel) aChannel =
    Handle(HYDROData_Channel)::DownCast( aDoc->CreateObject( KIND_CHANNEL ) );
  aChannel->SetName( "channel_1" );

  aChannel->SetGuideLine( aPolyline3d );
  aChannel->SetProfile( aProfile );
  aChannel->Update();
  CPPUNIT_ASSERT_EQUAL( false, (bool)aChannel->GetTopShape().IsNull() );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aChannel->GetShape3D().IsNull() );

  TestViewer::show( aChannel->GetTopShape(), AIS_Shaded, true, "Channel" );
  TestViewer::show( aChannel->GetLeftShape(), AIS_WireFrame, true, Qt::red );
  TestViewer::show( aChannel->GetRightShape(), AIS_WireFrame, true, Qt::red );
  Handle(HYDROGUI_Polyline) Polyline_Prs = new HYDROGUI_Polyline( aPolyline3d->GetShape3D() );
  Polyline_Prs->SetColor( Quantity_NOC_WHITE );
  TestViewer::show( Polyline_Prs, AIS_WireFrame, 0, true, "Channel" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

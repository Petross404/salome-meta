// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include<test_HYDROData_Iterator.h>

#include <HYDROData_Document.h>
#include <HYDROData_Iterator.h>

#include <QString>

void test_HYDROData_Iterator::testOneKind()
{
  static const QString aName1("test_name1");
  static const QString aName2("test_name2");
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Entity) anObj = aDoc->CreateObject(KIND_IMAGE); // image object
  anObj->SetName(aName1);
  // first HYDROData_Entity must be destroyed because there is no hander pointer naymore
  anObj = aDoc->CreateObject(KIND_IMAGE); // second image object
  anObj->SetName(aName2);

  HYDROData_Iterator anIter(aDoc, KIND_IMAGE);
  CPPUNIT_ASSERT(anIter.More());
  CPPUNIT_ASSERT(!anIter.Current().IsNull());
  CPPUNIT_ASSERT_EQUAL(aName1.toStdString(), anIter.Current()->GetName().toStdString());

  anIter.Next();
  CPPUNIT_ASSERT(anIter.More());
  CPPUNIT_ASSERT(!anIter.Current().IsNull());
  CPPUNIT_ASSERT_EQUAL(aName2.toStdString(), anIter.Current()->GetName().toStdString());

  anIter.Next();
  CPPUNIT_ASSERT(!anIter.More());

  aDoc->Close();
}

void test_HYDROData_Iterator::testAllKinds()
{
  static const QString aName1("test_name1");
  static const QString aName2("test_name2");
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Entity) anObj = aDoc->CreateObject(KIND_IMAGE); // image object
  anObj->SetName(aName1);
  // first HYDROData_Entity must be destroyed because there is no hander pointer naymore
  anObj = aDoc->CreateObject(KIND_IMAGE); // second image object
  anObj->SetName(aName2);

  HYDROData_Iterator anIter(aDoc, KIND_UNKNOWN);
  CPPUNIT_ASSERT(anIter.More());
  CPPUNIT_ASSERT(!anIter.Current().IsNull());
  CPPUNIT_ASSERT_EQUAL(aName1.toStdString(), anIter.Current()->GetName().toStdString());

  anIter.Next();
  CPPUNIT_ASSERT(anIter.More());
  CPPUNIT_ASSERT(!anIter.Current().IsNull());
  CPPUNIT_ASSERT_EQUAL(aName2.toStdString(), anIter.Current()->GetName().toStdString());

  anIter.Next();
  CPPUNIT_ASSERT(!anIter.More());

  aDoc->Close();
}

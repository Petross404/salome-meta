// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifdef WIN32
  #pragma warning( disable: 4251 )
#endif

#include <cppunit/extensions/HelperMacros.h>
#define SHP_TESTS

class test_HYDROData_LandCoverMap : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( test_HYDROData_LandCoverMap );
  CPPUNIT_TEST( test_add_2_objects );
  CPPUNIT_TEST( test_add_2_section_poly );
  CPPUNIT_TEST( test_add_triangles );
  CPPUNIT_TEST( test_split );
  CPPUNIT_TEST( test_split_2 );
  CPPUNIT_TEST( test_incomplete_split );
  CPPUNIT_TEST( test_merge );
  CPPUNIT_TEST( test_merge_refs_691 );
  CPPUNIT_TEST( test_remove );
  CPPUNIT_TEST( test_merge_faces_boxes );
  CPPUNIT_TEST( test_merge_faces_circles );
  CPPUNIT_TEST( test_import_dbf );
  //CPPUNIT_TEST( test_land_cover_prs_by_types );
  //CPPUNIT_TEST( test_land_cover_prs_by_coeff );
  CPPUNIT_TEST( test_dump_python );
  CPPUNIT_TEST( test_transparent_prs );
  CPPUNIT_TEST( test_assign_to_calc_case );
  CPPUNIT_TEST( test_export_telemac );
  CPPUNIT_TEST( test_copy );
  CPPUNIT_TEST( test_lcm_classification );
#ifdef SHP_TESTS
  CPPUNIT_TEST( test_shp_import_cyp );
  CPPUNIT_TEST( test_shp_import_clc_dec );
  CPPUNIT_TEST( test_shp_import_nld_areas );
  CPPUNIT_TEST( test_shp_loop_back );
  CPPUNIT_TEST( test_shp_clc_classification_perf );
  CPPUNIT_TEST( test_shp_clc_classification_check );
#endif
  CPPUNIT_TEST_SUITE_END();

public:
  void test_add_2_objects();
  void test_add_2_section_poly();
  void test_add_triangles();
  void test_split();
  void test_split_2();
  void test_incomplete_split();
  void test_merge();
  void test_merge_refs_691();
  void test_remove();
  void test_merge_faces_boxes();
  void test_merge_faces_circles();
  void test_import_dbf();
  void test_land_cover_prs_by_types();
  void test_land_cover_prs_by_coeff();
  void test_dump_python();
  void test_transparent_prs();
  void test_assign_to_calc_case();
  void test_lcm_classification();
  void test_shp_import_cyp();
  void test_shp_import_clc_dec();
  void test_shp_import_nld_areas();
  void test_shp_loop_back();
  void test_export_telemac();
  void test_copy();
  void test_shp_clc_classification_perf();
  void test_shp_clc_classification_check();
};

CPPUNIT_TEST_SUITE_REGISTRATION( test_HYDROData_LandCoverMap );
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( test_HYDROData_LandCoverMap, "HYDROData_LandCoverMap" );

#ifdef WIN32
  #pragma warning( default: 4251 )
#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROGUI_BathymetryPrs.h>
#include <HYDROGUI_ShapeBathymetry.h>
#include <TestViewer.h>
#include <AIS_ColorScale.hxx>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewFrame.h>
#include <OCCViewer_ViewPort3d.h>
#include <QTest>

extern QString REF_DATA_PATH;
const double EPS = 1E-3;

void test_HYDROGUI_BathymetryPrs::importTestBath( const Handle(HYDROData_Document)& theDoc )
{
  myBath = Handle(HYDROData_Bathymetry)::DownCast( theDoc->CreateObject( KIND_BATHYMETRY ) );

  QString fname = (REF_DATA_PATH + "/bathy.xyz");
  CPPUNIT_ASSERT( myBath->ImportFromFiles( QStringList() << fname ) );

  HYDROData_Bathymetry::AltitudePoints anAltitudePoints = myBath->GetAltitudePoints();
  CPPUNIT_ASSERT_EQUAL( 14781, (int)anAltitudePoints.size() );
}

void test_HYDROGUI_BathymetryPrs::createBathPrs()
{
  myBathPrs = new HYDROGUI_ShapeBathymetry( 0, TestViewer::context(), myBath );
  myBathPrs->Build();
  myBathPrs->getAISObjects()[0]->SetAutoHilight( Standard_False );

  double min, max;
  myBathPrs->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 175.56, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 185.65, max, EPS );
}

void test_HYDROGUI_BathymetryPrs::updateColors()
{
  double min, max;
  myBathPrs->GetRange( min, max );

  TestViewer::colorScale()->SetRange( min, max );
  myBathPrs->UpdateWithColorScale( TestViewer::colorScale() );
}

void test_HYDROGUI_BathymetryPrs::test_presentation()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 0, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES
  //QTest::qWait( 50000 );

  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

void select( int x1, int y1, int x2, int y2 )
{
  QPoint p1( x1, y1 ), p2( x2, y2 );
  QWidget* w = TestViewer::viewWindow()->getViewPort();
  QTest::mousePress( w, Qt::LeftButton, Qt::NoModifier, p1 );
  QTest::mouseEvent( QTest::MouseMove, w, Qt::LeftButton, Qt::NoModifier, p2 );
  QTest::mouseRelease( w, Qt::LeftButton, Qt::NoModifier, p2 );
  qApp->processEvents();
}

void test_HYDROGUI_BathymetryPrs::test_selection()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 1, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  select( 150, 5, 350, 140 );
  TestViewer::setKey( "bathy_selection" );
  CPPUNIT_ASSERT_IMAGES

  select( 5, 5, 6, 6 );
  TestViewer::setKey( "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES

  //QTest::qWait( 50000 );

  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

void test_HYDROGUI_BathymetryPrs::test_rescale_by_selection()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 1, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  // 1. Rescale by selection
  select( 150, 100, 350, 125 );
  myBathPrs->RescaleBySelection();
  updateColors(); // In HYDRO GUI it should be done by displayer

  //QTest::qWait( 50000 );

  double min, max;
  myBathPrs->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 175.56, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 182.70, max, EPS );

  TestViewer::colorScale()->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 177.78, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 182.70, max, EPS );

  TestViewer::setKey( "bathy_rescaled_selection" );
  CPPUNIT_ASSERT_IMAGES;

  //QTest::qWait( 50000 );

  // 2. User rescale
  myBathPrs->Rescale( 180, 181 );
  updateColors(); // In HYDRO GUI it should be done by displayer

  myBathPrs->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 180.0, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 181.0, max, EPS );

  TestViewer::colorScale()->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 180.0, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 181.0, max, EPS );

  TestViewer::setKey( "bathy_rescaled_user" );
  CPPUNIT_ASSERT_IMAGES;

  //QTest::qWait( 50000 );

  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

void test_HYDROGUI_BathymetryPrs::test_rescale_by_visible()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 1, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  // Change aspect to see a part of bathymetry
  OCCViewer_ViewPort3d* vp = TestViewer::viewWindow()->getViewPort();
  Handle(V3d_View) v = vp->getView();
  v->SetProj( 1, 1, 1 );
  vp->fitRect( QRect( 500, 380, 100, 70 ) );
  qApp->processEvents();

  // Rescale to the visible part of bathymetry
  myBathPrs->RescaleByVisible( TestViewer::viewWindow() );
  updateColors(); // In HYDRO GUI it should be done by displayer
  vp->fitAll(); // Necessary to see the whole presentation

  // Check parameters
  double min, max;
  myBathPrs->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 176.18, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 179.55, max, EPS );

  TestViewer::colorScale()->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 176.18, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 179.55, max, EPS );

  TestViewer::setKey( "bathy_rescaled_visible" );
  CPPUNIT_ASSERT_IMAGES;

  //QTest::qWait( 50000 );
  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

void test_HYDROGUI_BathymetryPrs::test_text_presentation()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 1, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  // Assign text labels
  int x1 = 150, y1 = 100, x2 = 200, y2 = 150;
  select( x1, y1, x2, y2 );
  myBathPrs->TextLabels( true );
  OCCViewer_ViewPort3d* vp = TestViewer::viewWindow()->getViewPort();
  vp->fitRect( QRect( x1, y1, x2-x1, y2-y1 ) );

  qApp->processEvents();

  TestViewer::setKey( "bathy_text_labels" );
  CPPUNIT_ASSERT_IMAGES;

  // Disable text labels
  myBathPrs->TextLabels( false );
  vp->fitAll();
  qApp->processEvents();
  TestViewer::setKey( "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  // Special case: flag=false + non-empty selection
  select( x1, y1, x2, y2 );
  myBathPrs->TextLabels( false );
  vp->fitAll();
  qApp->processEvents();
  TestViewer::setKey( "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;


  //QTest::qWait( 50000 );

  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

void test_HYDROGUI_BathymetryPrs::test_rescale_default()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 1, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  double min, max;

  // 1. User rescale
  myBathPrs->Rescale( 180, 181 );
  updateColors(); // In HYDRO GUI it should be done by displayer

  myBathPrs->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 180.0, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 181.0, max, EPS );

  TestViewer::colorScale()->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 180.0, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 181.0, max, EPS );

  TestViewer::setKey( "bathy_rescaled_user" );
  CPPUNIT_ASSERT_IMAGES;

  //QTest::qWait( 50000 );

  // 2. Default rescale
  myBathPrs->RescaleDefault();
  updateColors(); // In HYDRO GUI it should be done by displayer

  myBathPrs->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 175.56, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 185.65, max, EPS );

  TestViewer::colorScale()->GetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 175.56, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 185.65, max, EPS );

  TestViewer::setKey( "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  //QTest::qWait( 50000 );

  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

void test_HYDROGUI_BathymetryPrs::test_fit_on_selected()
{
  TestViewer::eraseAll( true, true );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  importTestBath( aDoc );
  TestViewer::showColorScale( false );

  createBathPrs();
  updateColors();

  Handle(AIS_InteractiveObject) bprs = myBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !bprs.IsNull() );

  TestViewer::show( bprs, 0, 1, true, "bathy_prs" );
  CPPUNIT_ASSERT_IMAGES;

  // Fit selected points on bathymetry
  int x1 = 100, y1 = 50, x2 = 250, y2 = 200;
  select( x1, y1, x2, y2 );
  TestViewer::viewWindow()->onFitSelection();
  TestViewer::setKey( "bathy_prs_fit_selected" );
  CPPUNIT_ASSERT_IMAGES;

  //QTest::qWait( 50000 );

  aDoc->Close();
  TestViewer::eraseAll( true, true );
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#pragma once

#include <cppunit/TestListener.h>
#include <vector>
#include <string>

typedef long long INT64;   ///< the cross-platform type definition for 64-bits integer

/**
  \class TestLib_Listener
  \brief Implementation of the custom listener printing the time of test execution
*/
class TestLib_Listener : public CppUnit::TestListener
{
public:
  TestLib_Listener();
  virtual ~TestLib_Listener();

  void Clear();
  INT64 GetCompleteTimeInMS() const;
  int GetNbTests() const;
  int GetNbSuites() const;
  void DumpFailures();
    
  virtual void startTest( CppUnit::Test* );
  virtual void endTest( CppUnit::Test* );

  virtual void startSuite( CppUnit::Test* );
  virtual void endSuite( CppUnit::Test* );

  virtual void addFailure( const CppUnit::TestFailure& );

private:
  INT64 myStart; ///< start time in milliseconds
  INT64 myComplete; ///< complete time of all tests execution in milliseconds
  int myNbTests;
  int myNbSuites;
  std::vector<std::string> myFailures;
};


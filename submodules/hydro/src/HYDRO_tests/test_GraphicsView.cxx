// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_GraphicsView.h>
#include <GraphicsView_ViewManager.h>
#include <GraphicsView_Viewer.h>
#include <GraphicsView_ViewFrame.h>
#include <GraphicsView_ViewPort.h>
#include <GraphicsView_Object.h>
#include <GraphicsView_Scene.h>
#include <QTest>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneWheelEvent>
#include <TestViewer.h>

extern QString REF_DATA_PATH;

class TestObject : public GraphicsView_Object
{
public:
  TestObject();
  virtual ~TestObject();

  virtual void compute();
  virtual QRectF getRect() const;
  virtual QRectF boundingRect() const;

private:
  QGraphicsPixmapItem* myItem;
};


TestObject::TestObject()
  : myItem( 0 )
{
  compute();
}

TestObject::~TestObject()
{
}

void TestObject::compute()
{
  QPixmap image( REF_DATA_PATH + "/garonne.png" );
  myItem = new QGraphicsPixmapItem( image );
  addToGroup( myItem );
}

QRectF TestObject::getRect() const
{
  return QRectF( 0, 0, 400, 300 );
}

QRectF TestObject::boundingRect() const
{
  if( myItem )
    return myItem->boundingRect();
  else
    return QRectF();
}



#define CPPUNIT_ASSERT_VIEW( theCase )                              \
  {                                                                 \
    qApp->processEvents();                                          \
    QImage im = aViewPort->dumpView();                              \
    const bool SWAP_RGB_ORDER = true;                               \
    if( SWAP_RGB_ORDER )                                            \
    {                                                               \
      /* A temporary patch for bug in SALOME/OCC dump;              \
         the result image contains swapped RGB colors  */           \
      im = im.rgbSwapped();                                         \
    }                                                               \
    CPPUNIT_ASSERT_IMAGES2( &im, theCase );                         \
  }                                                                 \

void test_GraphicsView::test_wheel_zoom()
{
  return;
  TestViewer::eraseAll( true, true );

  GraphicsView_ViewManager* aViewManager = new GraphicsView_ViewManager( 0, 0 );
  GraphicsView_Viewer* aViewer = new GraphicsView_Viewer( "test", 0 );

  aViewManager->setViewModel( aViewer );
  GraphicsView_ViewFrame* aWindow = dynamic_cast<GraphicsView_ViewFrame*>( aViewManager->createViewWindow() );
  aWindow->setGeometry( 100, 100, 600, 450 );
  aWindow->show();

  qApp->processEvents();

  // 1. Initialize presentation
  GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort();
  aViewPort->setInteractionFlag( GraphicsView_ViewPort::GlobalWheelScaling );
  // This is necessary to activate wheeling zoom

  TestObject* obj = new TestObject();
  aViewPort->addItem( obj );
  aViewPort->fitAll();
  qApp->processEvents();
  CPPUNIT_ASSERT_VIEW( "gv_fitall" );

  // 2. Mouse wheel zoom
  QWheelEvent we1( QPoint( 10, 10 ), 120*10, Qt::NoButton, Qt::NoModifier );
  qApp->sendEvent( aViewPort->viewport(), &we1 );
  qApp->processEvents();
  CPPUNIT_ASSERT_VIEW( "gv_zoomed_1" );

  //QTest::qWait( 50000 );

  QWheelEvent we2( QPoint( 650, 500 ), 120*10, Qt::NoButton, Qt::NoModifier );
  qApp->sendEvent( aViewPort->viewport(), &we2 );
  CPPUNIT_ASSERT_VIEW( "gv_zoomed_2" );

  aWindow->setVisible(false);
  aWindow->deleteLater();
  qApp->processEvents();

  //QTest::qWait( 50000 );
}

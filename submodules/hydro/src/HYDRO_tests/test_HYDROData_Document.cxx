// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include<test_HYDROData_Document.h>

#include <HYDROData_Document.h>
#include <QFile>

void test_HYDROData_Document::testSaveOpen()
{
  // temporarly created file name (in the current directory)
  const char* aTestFile = "TestDoc.cbf";
  // save
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  CPPUNIT_ASSERT(!aDoc.IsNull());
  // keep some saved information to check after retreive
  aDoc->NewID();
  int anID = aDoc->NewID();
  Data_DocError aStatus = aDoc->Save(aTestFile);
  CPPUNIT_ASSERT(aStatus == DocError_OK);
  aDoc->Close();
  CPPUNIT_ASSERT(!HYDROData_Document::HasDocument());

  // open
  aStatus = HYDROData_Document::Load(aTestFile);
  CPPUNIT_ASSERT(aStatus == DocError_OK);
  CPPUNIT_ASSERT(HYDROData_Document::HasDocument());
  aDoc = HYDROData_Document::Document();
  CPPUNIT_ASSERT(!aDoc.IsNull());
  // check that retreived correctly
  CPPUNIT_ASSERT(aDoc->NewID() == anID + 1);

  // remove the created file using Qt functionality
  QFile aFile(aTestFile);
  aFile.remove();

  aDoc->Close();
}

void test_HYDROData_Document::testOperations()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  CPPUNIT_ASSERT(!aDoc.IsNull());
  CPPUNIT_ASSERT(!aDoc->IsOperation());
  CPPUNIT_ASSERT(!aDoc->IsModified());
  // commit operation
  aDoc->StartOperation();
  CPPUNIT_ASSERT(aDoc->IsOperation());
  int anID = aDoc->NewID();
  aDoc->CommitOperation();
  CPPUNIT_ASSERT(!aDoc->IsOperation());
  CPPUNIT_ASSERT(aDoc->IsModified());
  // abort operation
  aDoc->StartOperation();
  CPPUNIT_ASSERT(aDoc->IsOperation());
  int anIDAborted = aDoc->NewID();
  aDoc->AbortOperation();
  CPPUNIT_ASSERT(!aDoc->IsOperation());
  CPPUNIT_ASSERT(aDoc->IsModified());

  CPPUNIT_ASSERT(anID + 1 == aDoc->NewID());

  aDoc->Close();
}

void test_HYDROData_Document::testUndoRedo()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  CPPUNIT_ASSERT(!aDoc.IsNull());
  CPPUNIT_ASSERT(!aDoc->CanUndo());
  CPPUNIT_ASSERT(!aDoc->CanRedo());
  // commit operation
  aDoc->StartOperation();
  CPPUNIT_ASSERT(aDoc->IsOperation());
  int anID = aDoc->NewID();
  aDoc->CommitOperation();
  CPPUNIT_ASSERT(aDoc->CanUndo());
  CPPUNIT_ASSERT(!aDoc->CanRedo());
  CPPUNIT_ASSERT(aDoc->IsModified());
  // undo
  aDoc->Undo();
  CPPUNIT_ASSERT(!aDoc->CanUndo());
  CPPUNIT_ASSERT(aDoc->CanRedo());
  CPPUNIT_ASSERT(anID == aDoc->NewID());
  CPPUNIT_ASSERT(!aDoc->IsModified());
  // redo
  aDoc->Redo();
  CPPUNIT_ASSERT(aDoc->CanUndo());
  CPPUNIT_ASSERT(!aDoc->CanRedo());
  CPPUNIT_ASSERT(aDoc->IsModified());

  aDoc->Close();
}

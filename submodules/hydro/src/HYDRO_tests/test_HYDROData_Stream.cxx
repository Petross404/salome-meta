// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_Stream.h>
#include <HYDROGUI_StreamDlg.h>
#include <HYDROData_Document.h>
#include <HYDROData_DTM.h>
#include <HYDROData_Stream.h>
#include <HYDROData_IPolyline.h>
#include <HYDROData_Profile.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Iterator.h>
#include <AIS_InteractiveContext.hxx>
#include <AIS_ColorScale.hxx>
#include <TestViewer.h>
#include <QApplication>
#include <QTest>

#include <HYDROData_LISM.h>
#include <HYDROData_SinusX.h>
#include <HYDROGUI_ShapeBathymetry.h>

extern QString REF_DATA_PATH;
NCollection_Sequence<HYDROData_IPolyline::Point> points2;
const double EPS = 1E-3;

void test_HYDROData_Stream::setUp()
{
  points2.Clear();
  points2.Append( gp_XY( 0.0, 5.0 ) );
  points2.Append( gp_XY( 1.0, 1.0 ) );
  points2.Append( gp_XY( 1.5, 0.0 ) );
  points2.Append( gp_XY( 4.0, 4.0 ) );
}

void test_HYDROData_Stream::tearDown()
{
}

void test_HYDROData_Stream::test_dialog()
{
  return;
  TestViewer::eraseAll( true, true );

  HYDROGUI_StreamDlg* aDlg = new HYDROGUI_StreamDlg( 0, "stream" );
  aDlg->show();
  qApp->processEvents();

  QImage aStreamDlgImage = QPixmap::grabWidget( aDlg ).toImage();
  CPPUNIT_ASSERT_IMAGES2( &aStreamDlgImage, "StreamDlg" );

  aDlg->setDDZ( 12.34 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 12.34, aDlg->getDDZ(), EPS );

  aDlg->setSpatialStep( 56.78 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 56.78, aDlg->getSpatialStep(), EPS );

  delete aDlg;
}

void test_HYDROData_Stream::test_alt_object()
{
  TestViewer::eraseAll( true, true );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Stream) aStream =
    Handle(HYDROData_Stream)::DownCast( aDoc->CreateObject( KIND_STREAM ) );

  CPPUNIT_ASSERT_EQUAL( false, (bool)aStream.IsNull() );
  CPPUNIT_ASSERT_EQUAL( true, (bool)aStream->GetAltitudeObject().IsNull() );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aStream->DTM().IsNull() );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aStream->GetAltitudeObject().IsNull() );
  CPPUNIT_ASSERT_EQUAL( KIND_DTM, aStream->getAltitudeObjectType() );

  Handle(HYDROData_DTM) aDTM =
    Handle(HYDROData_DTM)::DownCast( aStream->GetAltitudeObject() );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aDTM.IsNull() );

  aDoc->Close();
}

void test_HYDROData_Stream::test_params_sync()
{
  TestViewer::eraseAll( true, true );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Stream) aStream =
    Handle(HYDROData_Stream)::DownCast( aDoc->CreateObject( KIND_STREAM ) );
  Handle(HYDROData_DTM) aDTM = aStream->DTM();
  CPPUNIT_ASSERT_EQUAL( false, (bool)aDTM.IsNull() );

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile1->SetParametricPoints( points2 );
  aProfile1->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile1->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile1->SetRightPoint( gp_XY( 20, 0 ) );

  aProfile2->SetParametricPoints( points2 );
  aProfile2->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile2->SetLeftPoint( gp_XY( 50, 0 ) );
  aProfile2->SetRightPoint( gp_XY( 60, 10 ) );

  HYDROData_SequenceOfObjects profiles;
  profiles.Append( aProfile1 );
  profiles.Append( aProfile2 );

  aStream->SetProfiles( profiles, false );
  aStream->SetDDZ( 3.14 );
  aStream->SetSpatialStep( 4.14 );

  CPPUNIT_ASSERT_DOUBLES_EQUAL( 3.14, aStream->GetDDZ(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 3.14, aDTM->GetDDZ(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 4.14, aStream->GetSpatialStep(), EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 4.14, aDTM->GetSpatialStep(), EPS );

  HYDROData_SequenceOfObjects profiles1 = aStream->GetProfiles();
  CPPUNIT_ASSERT_EQUAL( 2, profiles1.Size() );
  CPPUNIT_ASSERT( profiles.Value(1)->Label() == profiles1.Value(1)->Label() );
  CPPUNIT_ASSERT( profiles.Value(2)->Label() == profiles1.Value(2)->Label() );

  HYDROData_SequenceOfObjects profiles2 = aDTM->GetProfiles();
  CPPUNIT_ASSERT_EQUAL( 2, profiles2.Size() );
  CPPUNIT_ASSERT( profiles.Value(1)->Label() == profiles2.Value(1)->Label() );
  CPPUNIT_ASSERT( profiles.Value(2)->Label() == profiles2.Value(2)->Label() );

  aDoc->Close();
}

void test_HYDROData_Stream::test_dump()
{
  TestViewer::eraseAll( true, true );

  // Case 1. Without hydraulic axis
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Stream) aStream1 =
    Handle(HYDROData_Stream)::DownCast( aDoc->CreateObject( KIND_STREAM ) );
  aStream1->SetName( "stream_1" );

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );
  aProfile1->SetName( "p1" );

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );
  aProfile2->SetName( "p2" );

  aProfile1->SetParametricPoints( points2 );
  aProfile1->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile1->SetLeftPoint( gp_XY( 10, 10 ) );
  aProfile1->SetRightPoint( gp_XY( 20, 0 ) );

  aProfile2->SetParametricPoints( points2 );
  aProfile2->GetProfileUZ()->SetSectionType( 0, HYDROData_IPolyline::SECTION_POLYLINE );
  aProfile2->SetLeftPoint( gp_XY( 50, 0 ) );
  aProfile2->SetRightPoint( gp_XY( 60, 10 ) );

  HYDROData_SequenceOfObjects profiles;
  profiles.Append( aProfile1 );
  profiles.Append( aProfile2 );

  aStream1->SetProfiles( profiles, false );
  aStream1->SetDDZ( 0.2 );
  aStream1->SetSpatialStep( 3.0 );

  MapOfTreatedObjects objs;
  objs["p1"] = aProfile1;
  objs["p2"] = aProfile2;

  QStringList aScript1 = aStream1->DumpToPython( "", objs );

  CPPUNIT_ASSERT_EQUAL( 11, aScript1.size() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1 = hydro_doc.CreateObject( KIND_STREAM )" ), aScript1[0].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.SetName( \"stream_1\" )" ),                 aScript1[1].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "" ),                                                 aScript1[2].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.SetInterpolationMethod( 0 )" ),             aScript1[3].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.AddProfile( p1 )" ),                        aScript1[4].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.AddProfile( p2 )" ),                        aScript1[5].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.SetDDZ( 0.200 )" ),                         aScript1[6].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.SetSpatialStep( 3.000 )" ),                 aScript1[7].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "" ),                                                 aScript1[8].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_1.Update()" ),                                aScript1[9].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "" ),                                                 aScript1[10].toStdString() );

  // Case 2. With hydraulic axis

  Handle(HYDROData_Stream) aStream2 =
    Handle(HYDROData_Stream)::DownCast( aDoc->CreateObject( KIND_STREAM ) );
  aStream2->SetName( "stream_2" );

  Handle(HYDROData_PolylineXY) anHAxis =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  anHAxis->SetName( "axis" );

  aStream2->SetProfiles( profiles, false );
  aStream2->SetDDZ( 0.2 );
  aStream2->SetSpatialStep( 3.0 );
  aStream2->SetReferenceObject( anHAxis, HYDROData_Stream::DataTag_HydraulicAxis );

  objs.clear();
  objs["p1"] = aProfile1;
  objs["p2"] = aProfile2;
  objs["axis"] = anHAxis;

  QStringList aScript2 = aStream2->DumpToPython( "", objs );
  CPPUNIT_ASSERT_EQUAL( 12, aScript2.size() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2 = hydro_doc.CreateObject( KIND_STREAM )" ), aScript2[0].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.SetName( \"stream_2\" )" ),                 aScript2[1].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "" ),                                                 aScript2[2].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.SetInterpolationMethod( 0 )" ),             aScript2[3].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.SetHydraulicAxis( axis )" ),                aScript2[4].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.AddProfile( p1 )" ),                        aScript2[5].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.AddProfile( p2 )" ),                        aScript2[6].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.SetDDZ( 0.200 )" ),                         aScript2[7].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.SetSpatialStep( 3.000 )" ),                 aScript2[8].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "" ),                                                 aScript2[9].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "stream_2.Update()" ),                                aScript2[10].toStdString() );
  CPPUNIT_ASSERT_EQUAL( std::string( "" ),                                                 aScript2[11].toStdString() );

  aDoc->Close();
}

void test_HYDROData_Stream::test_presentation()
{
  TestViewer::eraseAll( true, true );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/Profiles.xyz";
  NCollection_Sequence<int> bad_ids;

  int aSize = HYDROData_Profile::ImportFromFile( aDoc, fname, bad_ids, true );

  CPPUNIT_ASSERT_EQUAL( 0, bad_ids.Size() );
  CPPUNIT_ASSERT_EQUAL( 46, aSize );

  HYDROData_SequenceOfObjects profiles;
  HYDROData_Iterator it( aDoc, KIND_PROFILE );
  for( int i=0; it.More(); it.Next(), i++ )
  {
    if( i>=25 && i<=35 )
    {
      it.Current()->Update();
      profiles.Append( Handle(HYDROData_Profile)::DownCast( it.Current() ) );
    }
  }

  Handle(HYDROData_Stream) aStream =
    Handle(HYDROData_Stream)::DownCast( aDoc->CreateObject( KIND_STREAM ) );

  aStream->SetProfiles( profiles, false );
  aStream->SetDDZ( 0.2 );
  aStream->SetSpatialStep( 10 );
  aStream->Update();

  TopoDS_Shape aPrs3d = aStream->GetShape3D();
  TopoDS_Shape aPrs2d = aStream->GetTopShape();

  TestViewer::show( aPrs2d, 0, true, "stream_dtm_2d", 1, 1 );
  CPPUNIT_ASSERT_IMAGES;

  TestViewer::eraseAll( true );
  TestViewer::show( aPrs3d, 0, true, "stream_dtm_3d" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}


void test_HYDROData_Stream::test_lism_1()
{
  TCollection_AsciiString ref_path = REF_DATA_PATH.toLatin1().data();

  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/study_lism_1.cbf";
  CPPUNIT_ASSERT_EQUAL( (int)DocError_OK, (int)HYDROData_Document::Load( fname.ToCString() ) );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  HYDROData_SequenceOfObjects profiles;
  HYDROData_Iterator it( aDoc, KIND_PROFILE );
  for( int i=0; it.More(); it.Next(), i++ )
  {
    if( i>=15 && i<=42 )
      profiles.Append( Handle(HYDROData_Profile)::DownCast( it.Current() ) );
  }
  CPPUNIT_ASSERT_EQUAL( 28, (int)profiles.Size() );
  ///

  Handle(HYDROData_PolylineXY) aPolyXY_AX = Handle(HYDROData_PolylineXY)::DownCast(aDoc->FindObjectByName("AX"));
  Handle(HYDROData_PolylineXY) aPolyXY_LB = Handle(HYDROData_PolylineXY)::DownCast(aDoc->FindObjectByName("LB"));
  Handle(HYDROData_PolylineXY) aPolyXY_RB = Handle(HYDROData_PolylineXY)::DownCast(aDoc->FindObjectByName("RB"));

  aPolyXY_AX->Update();
  aPolyXY_LB->Update();
  aPolyXY_RB->Update();
  //
  Handle(HYDROData_LISM) aLISM = Handle(HYDROData_LISM)::DownCast( aDoc->CreateObject( KIND_LISM ) );
  aLISM->SetProfiles( profiles );
  aLISM->SetHaxStep(2.0);
  aLISM->SetNbProfilePoints( 200 );
  aLISM->SetHydraulicAxis(aPolyXY_AX);
  aLISM->SetLeftBank(aPolyXY_LB);
  aLISM->SetRightBank(aPolyXY_RB);
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 2.0, aLISM->GetHaxStep(), EPS );
  CPPUNIT_ASSERT_EQUAL( 200, aLISM->GetNbProfilePoints() );
  aLISM->Update();

  CPPUNIT_ASSERT_EQUAL( 99000, (int)aLISM->GetAltitudePoints().size() );

  Handle(AIS_InteractiveContext) aContext = TestViewer::context();
  HYDROGUI_ShapeBathymetry* aBathPrs = new HYDROGUI_ShapeBathymetry( 0, aContext, aLISM );
  //aBathPrs->Build();
  aBathPrs->update( true, false );
  aBathPrs->RescaleDefault();
  double min, max;
  aBathPrs->GetRange( min, max );
  TestViewer::colorScale()->SetRange( min, max );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( -0.98390276785714281, min, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 25.991840625000002, max, EPS );

  aBathPrs->UpdateWithColorScale( TestViewer::colorScale() );

  Handle(AIS_InteractiveObject) lism_prs = aBathPrs->getAISObjects()[0];
  CPPUNIT_ASSERT( !lism_prs.IsNull() );

  TestViewer::show( lism_prs, 0, 0, true, "lism_prs" );
  CPPUNIT_ASSERT_IMAGES

  delete aBathPrs;
  aDoc->Close();
}


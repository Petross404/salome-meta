// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_StricklerTable.h>
#include <HYDROData_Document.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_Tool.h>
#include <HYDROGUI_StricklerTableDlg.h>
#include <TestViewer.h>
#include <QColor>
#include <QDir>
#include <QStringList>
#include <QTableWidget>

const QString DEF_STR_PATH = qgetenv( "HYDRO_ROOT_DIR" ) + "/share/salome/resources/hydro/def_strickler_table_06.txt";
extern QString TMP_DIR;

void test_HYDROData_StricklerTable::test_import()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );

  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  QStringList aTypes = aTable->GetTypes();
  CPPUNIT_ASSERT_EQUAL( 44, aTypes.size() );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_06" ), aTable->GetAttrName() );

  QString aType = QString::fromUtf8("Périmètres irrigués en permanence");
  CPPUNIT_ASSERT_EQUAL( aType, aTypes[25] );
  CPPUNIT_ASSERT_EQUAL( QColor( 255, 255, 0 ), aTable->GetColor( aType ) );
  CPPUNIT_ASSERT_EQUAL( QString( "212" ), aTable->GetAttrValue( aType ) );

  aType = QString::fromUtf8("Végétation clairsemée");
  CPPUNIT_ASSERT_EQUAL( aType, aTypes[38] );
  CPPUNIT_ASSERT_EQUAL( QColor( 204, 255, 204 ), aTable->GetColor( aType ) );
  CPPUNIT_ASSERT_EQUAL( QString( "333" ), aTable->GetAttrValue( aType ) );

  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_import_export_equivalence()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );

  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );
  QString aTmpPath = TMP_DIR + "/stricker.txt";
  CPPUNIT_ASSERT_EQUAL( true, aTable->Export( aTmpPath ) );

  QFile aRefFile( DEF_STR_PATH ), aTmpFile( aTmpPath );
  CPPUNIT_ASSERT_EQUAL( true, aRefFile.open( QFile::ReadOnly ) );
  CPPUNIT_ASSERT_EQUAL( true, aTmpFile.open( QFile::ReadOnly ) );

  QByteArray aRefContents = aRefFile.readAll();
  QByteArray aTmpContents = aTmpFile.readAll();

//  bool isEqual = aRefContents.size()==aTmpContents.size();
//  CPPUNIT_ASSERT_EQUAL( true, isEqual );
//  for( int i=0, n=aRefContents.size(); isEqual && i<n; i++ )
//    if( aRefContents[i]!=aTmpContents[i] )
//      isEqual = false;
//
//  CPPUNIT_ASSERT_EQUAL( true, isEqual );

  aRefFile.close();
  aTmpFile.close();
  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_type_by_attr()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );

  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  CPPUNIT_ASSERT_EQUAL( QString( "Plans d'eau" ), aTable->GetType( "512" ) );
  CPPUNIT_ASSERT_EQUAL( QString( "" ), aTable->GetType( "125" ) );
  CPPUNIT_ASSERT_EQUAL( QString( "" ), aTable->GetType( "" ) );

  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_unique_attr_name()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable1 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  Handle(HYDROData_StricklerTable) aTable2 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );

  CPPUNIT_ASSERT_EQUAL( true, aTable1->SetAttrName( "CODE_06" ) );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_06" ), aTable1->GetAttrName() );
  CPPUNIT_ASSERT_EQUAL( QString( "" ), aTable2->GetAttrName() );

  CPPUNIT_ASSERT_EQUAL( false, aTable2->SetAttrName( "CODE_06" ) );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_06" ), aTable1->GetAttrName() );
  CPPUNIT_ASSERT_EQUAL( QString( "" ), aTable2->GetAttrName() );

  CPPUNIT_ASSERT_EQUAL( true, aTable2->SetAttrName( "CODE_07" ) );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_06" ), aTable1->GetAttrName() );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_07" ), aTable2->GetAttrName() );

  CPPUNIT_ASSERT_EQUAL( false, aTable1->SetAttrName( "CODE_07" ) );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_06" ), aTable1->GetAttrName() );
  CPPUNIT_ASSERT_EQUAL( QString( "CODE_07" ), aTable2->GetAttrName() );

  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_colors_sync()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable1 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  Handle(HYDROData_StricklerTable) aTable2 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );

  aTable1->Set( "test1", 10 );
  aTable1->Set( "test2", 20 );
  aTable1->SetColor( "test1", QColor( 255, 0, 0 ) );
  aTable1->SetColor( "test2", QColor( 255, 0, 1 ) );
  CPPUNIT_ASSERT_EQUAL( QColor( 255, 0, 0 ), aTable1->GetColor( "test1" ) );
  CPPUNIT_ASSERT_EQUAL( QColor( 255, 0, 1 ), aTable1->GetColor( "test2" ) );

  aTable2->Set( "test1", 15 );
  aTable2->SetColor( "test1", QColor( 255, 255, 0 ) );
  CPPUNIT_ASSERT_EQUAL( QColor( 255, 255, 0 ), aTable1->GetColor( "test1" ) );
  CPPUNIT_ASSERT_EQUAL( QColor( 255, 255, 0 ), aTable2->GetColor( "test1" ) );

  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_duplication_refs_613()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable1 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  aTable1->Set( "type1", 1.5 );
  aTable1->SetColor( "type1", QColor( 10, 20, 30 ) );
  aTable1->SetName( "DefStrickler_1" );

  Handle(HYDROData_StricklerTable) aTable2 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  aTable1->CopyTo( aTable2, true );

  CPPUNIT_ASSERT_EQUAL( QString( "DefStrickler_2" ), aTable2->GetName() );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.5, aTable2->Get( "type1", 0.0 ), 1E-5 );
  CPPUNIT_ASSERT_EQUAL( QColor( 10, 20, 30 ), aTable2->GetColor( "type1" ) );
  
  aDoc->StartOperation();
  aTable1->Remove();
  aTable2->Remove();
  Handle(HYDROData_StricklerTable) aTable3 =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  aTable3->SetName( "DefStrickler_1" );
  aDoc->CommitOperation();

  HYDROData_Iterator anIt1( aDoc, KIND_STRICKLER_TABLE );
  CPPUNIT_ASSERT_EQUAL( true, anIt1.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "DefStrickler_1" ), anIt1.Current()->GetName() );
  anIt1.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt1.More() );

  aDoc->Undo();

  HYDROData_Iterator anIt2( aDoc, KIND_STRICKLER_TABLE );
  CPPUNIT_ASSERT_EQUAL( true, anIt2.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "DefStrickler_1" ), anIt2.Current()->GetName() );
  anIt2.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt2.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "DefStrickler_2" ), anIt2.Current()->GetName() );
  anIt2.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt2.More() );
  
  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_dump_python()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  aTable->SetName( "ST" );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  QString aTmpPath = TMP_DIR + "/st_dump.py";

  CPPUNIT_ASSERT_EQUAL( true, aDoc->DumpToPython( aTmpPath, false ) );
  
  CPPUNIT_ASSERT_SCRIPTS_EQUAL( "st_dump.py", true, true, 0 );

  aDoc->Close();
}

void test_HYDROData_StricklerTable::test_add_row_in_gui_refs_717()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );

  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  HYDROGUI_StricklerTableDlg* aDlg = new HYDROGUI_StricklerTableDlg( 0, "", 0 );
  aDlg->setGuiData( aTable );
  CPPUNIT_ASSERT_EQUAL( 44, aDlg->myTable->rowCount() );
  aDlg->onAddCoefficient();
  aDlg->getGuiData( aTable );

  aDlg->deleteLater();
  aDoc->Close();
}

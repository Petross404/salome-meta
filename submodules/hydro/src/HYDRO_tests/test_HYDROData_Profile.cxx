// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_Profile.h>

#include <HYDROData_Document.h>
#include <HYDROData_Tool.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Iterator.h>

#include <TopoDS_Shape.hxx>

#include <gp_XY.hxx>
#include <gp_XYZ.hxx>

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QString>

const double EPS = 1E-2;
extern QString REF_DATA_PATH;
extern QString TMP_DIR;

bool test_HYDROData_Profile::createTestFile( const QString& theFileName,
                                             const bool     theIsParametric )
{
  QFile aTmpFile( theFileName );
  if ( !aTmpFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
    return false;

  if ( theIsParametric )
  {
    QTextStream anOutStream( &aTmpFile );

    anOutStream << "0      182.15 \n";
    anOutStream << "4      181.95 \n";
    anOutStream << "10.18  181.63 \n";
    anOutStream << "14.75  179.27 \n";
    anOutStream << "19.75  178.87 \n";

    anOutStream << "\n";

    anOutStream << "-5   50    \n";
    anOutStream << "0    15    \n";
    anOutStream << "10.1 10    \n";
    anOutStream << "20   20    \n";
    anOutStream << "250  0.005 \n";
  }
  else
  {
    QTextStream anOutStream( &aTmpFile );

    anOutStream << "1040499.17 6788618.13 182.15 \n";
    anOutStream << "1040503.12 6788618.79 181.95 \n";
    anOutStream << "1040509.21 6788619.81 181.63 \n";
    anOutStream << "1040513.72 6788620.56 179.27 \n";
    anOutStream << "1040518.65 6788621.38 178.87 \n";
  }

  aTmpFile.close();

  return true;
}

void test_HYDROData_Profile::testFileImport()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  QString aParamFileName = TMP_DIR + QDir::separator() + "parametric.pa";
  QString aGeorefFileName = TMP_DIR + QDir::separator() + "georef.pa";
  if ( !createTestFile( aParamFileName, true ) || !createTestFile( aGeorefFileName, false ) )
    return; // No file has been created

  TCollection_AsciiString aFileName( aParamFileName.toStdString().c_str() );

  NCollection_Sequence<int> aBadProfilesList;
  CPPUNIT_ASSERT( HYDROData_Profile::ImportFromFile( aDoc, aFileName, aBadProfilesList, true ) );

  int aProfileCount = 0;
  HYDROData_Iterator aDocIter( aDoc, KIND_PROFILE );
  for ( ; aDocIter.More(); aDocIter.Next() )
  {
    Handle(HYDROData_Profile) aProfile =
      Handle(HYDROData_Profile)::DownCast( aDocIter.Current() );
    if ( aProfile.IsNull() )
      continue;

    CPPUNIT_ASSERT( aProfile->IsValid() == false );
    CPPUNIT_ASSERT( aProfile->NbPoints() == 5 );

    aProfileCount++;
  }
  CPPUNIT_ASSERT( aProfileCount == 2 );

  Handle(HYDROData_Profile) aGeorefProfile =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aFileName = TCollection_AsciiString( aGeorefFileName.toStdString().c_str() );
  bool notEmpty = false;
  CPPUNIT_ASSERT( aGeorefProfile->ImportFromFile( aFileName, true, &notEmpty ) );
  CPPUNIT_ASSERT( notEmpty );

  // Check validity of imported profile
  CPPUNIT_ASSERT( aGeorefProfile->IsValid() );

  CPPUNIT_ASSERT( aGeorefProfile->GetTopShape().IsNull() == false );

  aGeorefProfile->Update();
  CPPUNIT_ASSERT( aGeorefProfile->GetShape3D().IsNull() == false );

  HYDROData_Profile::ProfilePoints aProfilePoints = aGeorefProfile->GetProfilePoints();
  CPPUNIT_ASSERT( aProfilePoints.Length() == 5 );

  HYDROData_Profile::ProfilePoint aProfilePoint = aProfilePoints.Value( 3 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( aProfilePoint.X(), 1040509.21, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( aProfilePoint.Y(), 6788619.81, EPS );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( aProfilePoint.Z(), 181.63, EPS );

  aDoc->Close();
}


void test_HYDROData_Profile::testCopy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Profile) aProfile1 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  QString aFileName = TMP_DIR + QDir::separator() + "georef.pa";

  bool anIsFileCreated = createTestFile( aFileName, false );

  if ( anIsFileCreated )
  {
    bool notEmpty = false;
    TCollection_AsciiString anAsciiFileName( aFileName.toStdString().c_str() );
    CPPUNIT_ASSERT( aProfile1->ImportFromFile( anAsciiFileName, true, &notEmpty ) );
    CPPUNIT_ASSERT( notEmpty );

    CPPUNIT_ASSERT( aProfile1->IsValid() );
    CPPUNIT_ASSERT( aProfile1->NbPoints() == 5 );
  }

  Handle(HYDROData_Profile) aProfile2 =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );

  aProfile1->CopyTo( aProfile2, false );

  if ( anIsFileCreated )
  {
    CPPUNIT_ASSERT( aProfile2->IsValid() );
    CPPUNIT_ASSERT( aProfile2->NbPoints() == 5 );
  }

  aDoc->Close();
}

void operator << ( std::ostream& s, const gp_XYZ& p )
{
  s << "(" << p.X() << "; " << p.Y() << "; " << p.Z() << ") ";
}

bool operator == ( const gp_XYZ& p1, const gp_XYZ& p2 )
{
  return fabs(p1.X()-p2.X())<EPS && fabs(p1.Y()-p2.Y())<EPS && fabs(p1.Z()-p2.Z())<EPS;
}

void test_HYDROData_Profile::testProjection()
{
  std::string aPath = ( REF_DATA_PATH+"/profiles1.xyz" ).toStdString();

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  TCollection_AsciiString aFileName( aPath.c_str() );
  NCollection_Sequence<int> aBadProfilesList;
  CPPUNIT_ASSERT( HYDROData_Profile::ImportFromFile( aDoc, aFileName, aBadProfilesList, false ) );
  CPPUNIT_ASSERT( HYDROData_Profile::ImportFromFile( aDoc, aFileName, aBadProfilesList, true  ) );

  HYDROData_Iterator it( aDoc, KIND_PROFILE );
  CPPUNIT_ASSERT( it.More() );
  Handle(HYDROData_Profile) p1 = Handle(HYDROData_Profile)::DownCast( it.Current() ); it.Next();
  CPPUNIT_ASSERT( it.More() );
  Handle(HYDROData_Profile) p2 = Handle(HYDROData_Profile)::DownCast( it.Current() ); it.Next();
  CPPUNIT_ASSERT( !it.More() );

  CPPUNIT_ASSERT_EQUAL( QString( "Profile_1" ), p1->GetName() );
  CPPUNIT_ASSERT_EQUAL( QString( "Profile_2" ), p2->GetName() );

  HYDROData_Profile::ProfilePoints pp1 = p1->GetProfilePoints();
  int low1 = pp1.Lower(), up1 = pp1.Upper();
  CPPUNIT_ASSERT_EQUAL( 1, low1 );
  CPPUNIT_ASSERT_EQUAL( 6, up1 );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 1.0,      2.0,     5.0 ), pp1.Value( 1 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 2.04019,  4.0838,  4.0 ), pp1.Value( 2 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 2.9601,   5.9202,  3.0 ), pp1.Value( 3 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 4.08026,  8.16052, 3.0 ), pp1.Value( 4 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 4.9202,   9.84041, 4.0 ), pp1.Value( 5 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 6.0,     12.0,     5.0 ), pp1.Value( 6 ) );

  HYDROData_Profile::ProfilePoints pp2 = p2->GetProfilePoints();
  int low2 = pp2.Lower(), up2 = pp2.Upper();
  CPPUNIT_ASSERT_EQUAL( 1, low2 );
  CPPUNIT_ASSERT_EQUAL( 6, up2 );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 1.0,      2.0,     5.0 ), pp2.Value( 1 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 2.04019,  4.0838,  4.0 ), pp2.Value( 2 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 2.9601,   5.9202,  3.0 ), pp2.Value( 3 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 4.08,     8.16,    3.0 ), pp2.Value( 4 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 4.92,     9.84,    4.0 ), pp2.Value( 5 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XYZ( 6.0,     12.0,     5.0 ), pp2.Value( 6 ) );

  aDoc->Close();
}

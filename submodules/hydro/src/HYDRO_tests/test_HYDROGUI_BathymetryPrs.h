// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <cppunit/extensions/HelperMacros.h>
#include <HYDROData_Document.h>
#include <HYDROData_Bathymetry.h>

class HYDROGUI_ShapeBathymetry;

class test_HYDROGUI_BathymetryPrs : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( test_HYDROGUI_BathymetryPrs );
  CPPUNIT_TEST( test_presentation );
  //CPPUNIT_TEST( test_selection );
  //CPPUNIT_TEST( test_rescale_by_selection );
  CPPUNIT_TEST( test_rescale_by_visible );
  //CPPUNIT_TEST( test_text_presentation );
  CPPUNIT_TEST( test_rescale_default );
  //CPPUNIT_TEST( test_fit_on_selected );
  CPPUNIT_TEST_SUITE_END();

private:
  void importTestBath( const Handle(HYDROData_Document)& );
  void createBathPrs();
  void updateColors();

  void test_presentation();
  void test_selection();
  void test_rescale_by_selection();
  void test_rescale_by_visible();
  void test_text_presentation();
  void test_rescale_default();
  void test_fit_on_selected();

private:
  Handle(HYDROData_Bathymetry) myBath;
  HYDROGUI_ShapeBathymetry* myBathPrs;
};

CPPUNIT_TEST_SUITE_REGISTRATION( test_HYDROGUI_BathymetryPrs );
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( test_HYDROGUI_BathymetryPrs, "HYDROGUI_BathymetryPrs" );

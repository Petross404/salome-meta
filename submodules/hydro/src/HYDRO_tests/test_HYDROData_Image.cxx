// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include<test_HYDROData_Image.h>

#include <HYDROData_Document.h>
#include <HYDROData_Image.h>
#include <TestViewer.h>
#include <QPainter>
#include <QImage>

extern QString REF_DATA_PATH;

static QImage TestImage() {
  QImage aPic(50, 40, QImage::Format_RGB32);
  QPainter aPainter(&aPic);
  aPainter.drawEllipse(6, 7, 38, 30);
  aPainter.drawLine(0, 40, 10, 0);
  aPainter.drawLine(10, 0, 25, 35);
  aPainter.drawLine(25, 35, 40, 0);
  aPainter.drawLine(40, 0, 50, 40);
  return aPic;
}

void test_HYDROData_Image::testQImage()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Image) anImage = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  // empty image
  QImage anEmpty = anImage->Image();
  CPPUNIT_ASSERT(anEmpty.isNull());

  // prepare Qt Image for testing
  QImage aPic(TestImage());
  anImage->SetImage(aPic);
  QImage aRestored = anImage->Image();

  CPPUNIT_ASSERT(!aRestored.isNull());
  //aRestored.save("pic2.bmp");
  CPPUNIT_ASSERT(aPic == aRestored);

  aDoc->Close();
}

void test_HYDROData_Image::testTrsf()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Image) anImage = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  // empty trsf
  QTransform anEmpty = anImage->Trsf();
  CPPUNIT_ASSERT(anEmpty.isIdentity());

  // prepare Qt transformation for testing
  QTransform aTrsf;
  aTrsf.translate(50, 50);
  aTrsf.rotate(45);
  aTrsf.scale(0.5, 1.0);

  anImage->SetTrsf(aTrsf);
  QTransform aRestored = anImage->Trsf();

  CPPUNIT_ASSERT(!aRestored.isIdentity());
  CPPUNIT_ASSERT(aTrsf == aRestored);

  aDoc->Close();
}

void test_HYDROData_Image::testReferences()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Image) anImage1 = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  Handle(HYDROData_Image) anImage2 = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  Handle(HYDROData_Image) anImage3 = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));

  // check initially there is no references
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 0);

  // append reference
  anImage3->AppendReference(anImage1);
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 1);
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(0), anImage1));

  // change reference
  anImage3->ChangeReference(0, anImage2);
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 1);
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(0), anImage2));

  // append one more reference
  anImage3->AppendReference(anImage1);
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 2);
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(0), anImage2));
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(1), anImage1));

  // remove reference
  anImage3->RemoveReference(0);
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 1);
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(0), anImage1));

  // remove reference
  anImage3->RemoveReference(0);
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 0);

  // append two references once again and remove all references
  anImage3->ChangeReference(0, anImage1);
  anImage3->ChangeReference(1, anImage2);
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 2);
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(0), anImage1));
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(1), anImage2));
  anImage3->ClearReferences();
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 0);

  aDoc->Close();
}

void test_HYDROData_Image::testCopy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Image) anImage1 = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  // fill image 1
  anImage1->SetImage(TestImage());

  QTransform aTrsf;
  aTrsf.translate(50, 50);
  aTrsf.rotate(45);
  aTrsf.scale(0.5, 1.0);
  anImage1->SetTrsf(aTrsf);

  Handle(HYDROData_Image) anImage2 = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));

  anImage1->AppendReference(anImage2);

  // copy image to the new one
  Handle(HYDROData_Image) anImage3 = 
    Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  anImage1->CopyTo(anImage3, true);

  // check all fields are correctly copied
  CPPUNIT_ASSERT(anImage1->Image() == anImage3->Image());
  CPPUNIT_ASSERT(anImage1->Trsf() == anImage3->Trsf());
  CPPUNIT_ASSERT_EQUAL(anImage3->NbReferences(), 1);
  CPPUNIT_ASSERT(IsEqual(anImage3->Reference(0), anImage2));

  aDoc->Close();
}

void test_HYDROData_Image::test_static_loadECW()
{
  QImage ecwImage;
  HYDROData_Image::ECW_FileInfo* ECWInfo = new HYDROData_Image::ECW_FileInfo;
  QString refECWPath = REF_DATA_PATH + "/01-2009-0865-6525-LA93.ecw";    
  QString refPNGPath = REF_DATA_PATH + "/01-2009-0865-6525-LA93.png";
  //
  CPPUNIT_ASSERT ( HYDROData_Image::OpenECW(refECWPath.toLatin1().data(), ecwImage, ECWInfo) );
  CPPUNIT_ASSERT_EQUAL (10000, ECWInfo->myXSize);
  CPPUNIT_ASSERT_EQUAL (10000, ECWInfo->myYSize);
  CPPUNIT_ASSERT_DOUBLES_EQUAL (0.5, ECWInfo->myCellIncrementX, 0.0001);
  CPPUNIT_ASSERT_DOUBLES_EQUAL (-0.5, ECWInfo->myCellIncrementY, 0.0001);  
  CPPUNIT_ASSERT_DOUBLES_EQUAL (865000, ECWInfo->myOriginX, 0.0001);
  CPPUNIT_ASSERT_DOUBLES_EQUAL (6525000, ECWInfo->myOriginY, 0.0001);  
  //QImage pngImage(refPNGPath);
  //CPPUNIT_ASSERT( ecwImage == pngImage);
}

void test_HYDROData_Image::test_loadECW()
{
  QImage ecwImage;
  QString refECWPath = REF_DATA_PATH + "/01-2009-0865-6525-LA93.ecw";    
  QString refPNGPath = REF_DATA_PATH + "/01-2009-0865-6525-LA93.png";
  //
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(HYDROData_Image) ecwHImage = Handle(HYDROData_Image)::DownCast(aDoc->CreateObject(KIND_IMAGE));
  //
  CPPUNIT_ASSERT (ecwHImage->LoadImageECW(refECWPath));
  //QImage pngImage(refPNGPath);
  //CPPUNIT_ASSERT( ecwHImage->Image() == pngImage);

  aDoc->Close();
}


void test_HYDROData_Image::testDiff()
{
  QImage im1, im2;
  im1.load( REF_DATA_PATH + "/LandCover_Triangles.png" );
  im2.load( REF_DATA_PATH + "/LandCover_Triangles_Split.png" );

  QImage im3 = TestViewer::diff( im1, im2 );
  CPPUNIT_ASSERT_IMAGES3( &im3, "diff_image", false );
}

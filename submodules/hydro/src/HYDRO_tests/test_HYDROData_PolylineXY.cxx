// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include<test_HYDROData_PolylineXY.h>

#include <HYDROData_Channel.h>
#include <HYDROData_Document.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_PolylineOperator.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_Tool.h>
#include <HYDROGUI_Shape.h>
#include <HYDROGUI_Polyline.h>
#include <HYDROData_SinusX.h>
#include <BRep_Builder.hxx>

#include <AIS_DisplayMode.hxx>
#include <AIS_PointCloud.hxx>
#include <Prs3d_LineAspect.hxx>
#include <Prs3d_PointAspect.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <QColor>
#include <QList>
#include <QPointF>
#include <QTest>

#include <TestShape.h>
#include <TestViewer.h>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Wire.hxx>
#include <gp_XY.hxx>
#include <HYDROData_Tool.h>

extern QString REF_DATA_PATH;

void test_HYDROData_PolylineXY::test_polyline()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));

  aPolyline->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  aPolyline->AddSection( "Section_2", HYDROData_PolylineXY::SECTION_SPLINE, true );

  int aNbSections = aPolyline->NbSections();
  CPPUNIT_ASSERT( aNbSections == 2 );

  NCollection_Sequence<TCollection_AsciiString>           aSectNames;
  NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
  NCollection_Sequence<bool>                              aSectClosures;
  aPolyline->GetSections( aSectNames, aSectTypes, aSectClosures );

  CPPUNIT_ASSERT_EQUAL( 2, aSectNames.Size() );
  CPPUNIT_ASSERT( aSectNames.Value( 1 ) == "Section_1" );
  CPPUNIT_ASSERT( aSectTypes.Value( 1 ) == HYDROData_PolylineXY::SECTION_POLYLINE );
  CPPUNIT_ASSERT( aSectClosures.Value( 1 ) == false );

  CPPUNIT_ASSERT( aSectNames.Value( 2 ) == "Section_2" );
  CPPUNIT_ASSERT( aSectTypes.Value( 2 ) == HYDROData_PolylineXY::SECTION_SPLINE );
  CPPUNIT_ASSERT( aSectClosures.Value( 2 ) == true );

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_copy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(HYDROData_PolylineXY) aPolyline1 =
    Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));


//  aPolyline1->setPoints(aPoints);

  Handle(HYDROData_PolylineXY) aPolyline2 =
    Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));

  aPolyline1->CopyTo(aPolyline2, true);


  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_split_refs_624()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aPolyline->SetName( "test" );

  QList<double> aCoords = QList<double>() << 10 << 10 << 20 << 10 << 20 << 20 << 10 << 20;
  TopoDS_Wire aWire = Wire2d( aCoords, true );
  aPolyline->SetShape( aWire );

  gp_Pnt2d aPnt( 20, 20 );

  TestViewer::show( aPolyline->GetShape(), 0, true, "LandCoverMap_Split_Polyline" );
  //TestViewer::show( BRepBuilderAPI_MakeVertex( aPnt ).Vertex(), 1, true, Qt::green );
  CPPUNIT_ASSERT_IMAGES

  HYDROData_PolylineOperator anOp;
  CPPUNIT_ASSERT_EQUAL( true, anOp.SplitPoint( aDoc, aPolyline, aPnt, 1E-3 ) );

  HYDROData_Iterator anIt( aDoc, KIND_POLYLINEXY );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test" ), anIt.Current()->GetName() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test_1" ), anIt.Current()->GetName() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_extraction_immersible_zone()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aPolyline->SetName( "test" );

  QList<double> aCoords = QList<double>() << 10 << 10 << 20 << 10 << 20 << 20 << 10 << 20;
  TopoDS_Wire aWire = Wire2d( aCoords, true );
  aPolyline->SetShape( aWire );

  Handle(HYDROData_ImmersibleZone) aZone =
    Handle(HYDROData_ImmersibleZone)::DownCast( aDoc->CreateObject( KIND_IMMERSIBLE_ZONE ) );
  aZone->SetName( "zone" );
  aZone->SetPolyline( aPolyline );
  aZone->Update();

  CPPUNIT_ASSERT_EQUAL( false, (bool)aZone->GetTopShape().IsNull() );

  HYDROData_PolylineOperator anOp;
  CPPUNIT_ASSERT_EQUAL( true, anOp.Extract( aDoc, aZone ) );

  Handle(HYDROData_PolylineXY) anOuter =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->FindObjectByName( "zone_Outer_1", KIND_POLYLINEXY ) );
  CPPUNIT_ASSERT_EQUAL( false, (bool)anOuter.IsNull() );

  TestViewer::show( aZone->GetTopShape(), 1, true, "Extraction_ImmZone" );
  TestViewer::show( anOuter->GetShape(), 0, true, Qt::red );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_extraction_channel_refs_611()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline2d =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aPolyline2d->SetName( "polyline2d_1" );
  aPolyline2d->AddSection( "", HYDROData_IPolyline::SECTION_SPLINE, false );

  Handle(HYDROData_Polyline3D) aPolyline3d =
    Handle(HYDROData_Polyline3D)::DownCast( aDoc->CreateObject( KIND_POLYLINE ) );
  aPolyline3d->SetName( "polyline3d_1" );
  aPolyline3d->SetPolylineXY( aPolyline2d );

  QList<double> aCoords = QList<double>() << 10 << 10 << 20 << 10 << 20 << 20 << 10 << 20;
  TopoDS_Wire aWire = Wire2d( aCoords, false );
  aPolyline2d->SetShape( aWire );
  aPolyline3d->SetTopShape( aWire );
  aPolyline3d->SetShape3D( aWire );

  Handle(HYDROData_Profile) aProfile =
    Handle(HYDROData_Profile)::DownCast( aDoc->CreateObject( KIND_PROFILE ) );
  aProfile->SetName( "profile_1" );

  QList<double> aCoordsPr = QList<double>() << 0.0 << 0.1 << 0.0 << 0.0 << 1.0 << 0.0;
  TopoDS_Wire aWirePr = Wire3d( aCoordsPr, false );
  aProfile->SetTopShape( aWirePr );
  aProfile->SetShape3D( aWirePr );


  Handle(HYDROData_Channel) aChannel =
    Handle(HYDROData_Channel)::DownCast( aDoc->CreateObject( KIND_CHANNEL ) );
  aChannel->SetName( "channel_1" );

  aChannel->SetGuideLine( aPolyline3d );
  aChannel->SetProfile( aProfile );
  aChannel->Update();
  CPPUNIT_ASSERT_EQUAL( false, (bool)aChannel->GetTopShape().IsNull() );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aChannel->GetShape3D().IsNull() );

  HYDROData_PolylineOperator anOp;
  CPPUNIT_ASSERT_EQUAL( true, anOp.Extract( aDoc, aChannel ) );

  Handle(HYDROData_PolylineXY) aLeft =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->FindObjectByName( "channel_1_Left_Bank_1", KIND_POLYLINEXY ) );
  Handle(HYDROData_PolylineXY) aRight =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->FindObjectByName( "channel_1_Right_Bank_1", KIND_POLYLINEXY ) );
  CPPUNIT_ASSERT_EQUAL( false, (bool)aRight.IsNull() );

  TestViewer::show( aChannel->GetTopShape(), 1, true, "Extraction_Channel" );
  TestViewer::show( aLeft->GetShape(), 0, true, Qt::red );
  TestViewer::show( aRight->GetShape(), 0, true, Qt::red );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_presentation()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline2d =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aPolyline2d->SetName( "polyline2d_1" );
  aPolyline2d->AddSection( "", HYDROData_IPolyline::SECTION_SPLINE, false );

  QList<gp_XY> aPoints = QList<gp_XY>() << gp_XY(  0,  0 )
                                        << gp_XY( 10, 10 )
                                        << gp_XY( 20, 40 )
                                        << gp_XY( 30, 10 )
                                        << gp_XY( 40, 50 )
                                        << gp_XY( 50, 60 )
                                        << gp_XY( -10, 40 )
                                        << gp_XY( -9, 39 )
                                        << gp_XY( -8, 38 )
                                        << gp_XY(  0, 20 );
  Handle(TColgp_HArray1OfPnt) aPnts = new TColgp_HArray1OfPnt( 1, aPoints.size() );
  int i = 1;
  foreach( gp_XY aPoint, aPoints )
  {
    aPolyline2d->AddPoint( 0, aPoint );
    aPnts->SetValue( i, gp_Pnt( aPoint.X(), aPoint.Y(), 0 ) );
    i++;
  }
  aPolyline2d->Update();

  CPPUNIT_ASSERT_EQUAL( false, (bool)aPolyline2d->GetShape().IsNull() );

  Handle(AIS_PointCloud) aPointsPrs = new AIS_PointCloud();
  aPointsPrs->SetPoints( aPnts );
  aPointsPrs->SetColor( Quantity_NOC_BLUE1 );
  aPointsPrs->Attributes()->PointAspect()->SetTypeOfMarker( Aspect_TOM_O );

  aPolyline2d->SetWireColor( Qt::darkGreen );
  HYDROGUI_Shape* aNewPolylinePrs = new HYDROGUI_Shape( TestViewer::context(), aPolyline2d );
  aNewPolylinePrs->update( true, true );


  // Check default type
  Handle(HYDROGUI_Arrow) arr = Handle(HYDROGUI_Arrow)::DownCast( aNewPolylinePrs->getAISObjects()[1] );
  CPPUNIT_ASSERT_EQUAL( HYDROGUI_Arrow::Cone, arr->GetType() );
  CPPUNIT_ASSERT_EQUAL( 35, arr->GetSize() );


  // Check polyline presentation with default (cone) arrow
  TestViewer::eraseAll(true);
  TestViewer::show( aPointsPrs, AIS_PointCloud::DM_Points, 0, true, "Polyline_Presentation" );
  TestViewer::show( aPolyline2d->GetShape(), 0, true, Qt::red );
  //TestViewer::show( aNewPolylinePrs, AIS_PointCloud::DM_Points, 0, true, "" );
  aNewPolylinePrs->setBorderColor( Qt::blue );
  aNewPolylinePrs->display();
  TestViewer::fitAll();
  CPPUNIT_ASSERT_IMAGES


  // Check polyline presentation with triangle arrow
  arr->SetType( HYDROGUI_Arrow::Triangle );
  TestViewer::eraseAll(true);
  TestViewer::show( aPointsPrs, AIS_PointCloud::DM_Points, 0, true, "Polyline_Presentation_triangle" );
  TestViewer::show( aPolyline2d->GetShape(), 0, true, Qt::red );
  //TestViewer::show( aNewPolylinePrs, AIS_PointCloud::DM_Points, 0, true, "" );
  TestViewer::context()->RecomputePrsOnly( arr, true );
  aNewPolylinePrs->display();
  TestViewer::fitAll();
  CPPUNIT_ASSERT_IMAGES

  //QTest::qWait( 50000 );
  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_split_refs_627()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aPolyline->SetName( "test" );


  //QList<double> aCoords = QList<double>() << 10 << 10 << 20 << 10 << 20 << 20 << 10 << 20;
  aPolyline->AddSection( "", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  aPolyline->AddPoint( 0, gp_XY( 10, 10  ) );
  aPolyline->AddPoint( 0, gp_XY( 20, 10 ) );
  aPolyline->AddPoint( 0, gp_XY( 20, 20 ) );
  aPolyline->AddPoint( 0, gp_XY( 10, 20 ) );
  aPolyline->Update();


  //TopoDS_Wire aWire = Wire2d( aCoords, false );
  //aPolyline->SetShape( aWire );
  TopoDS_Shape aW = aPolyline->GetShape();
  CPPUNIT_ASSERT (aW.ShapeType() == TopAbs_WIRE);

  aPolyline->SetWireColor( Qt::red );

  gp_Pnt2d aPnt( 20, 20 );

  HYDROData_PolylineOperator anOp;
  CPPUNIT_ASSERT_EQUAL( true, anOp.SplitPoint( aDoc, aPolyline, aPnt, 1E-3 ) );

  TestViewer::show( TopoDS_Shape(), 0, true, "Split_Polylines_Colors" );
  HYDROData_Iterator anIt( aDoc, KIND_POLYLINEXY );
  for( ; anIt.More(); anIt.Next() )
  {
    Handle(HYDROData_PolylineXY) anObj = Handle(HYDROData_PolylineXY)::DownCast( anIt.Current() );
    if( aPolyline->Label() != anObj->Label() )
    {
      QColor color;
      anObj->GetSectionColor(0, color);
      TestViewer::show( anObj->GetShape(), 0, true, color );
    }
  }
  CPPUNIT_ASSERT_IMAGES

  HYDROData_Iterator anIt2( aDoc, KIND_POLYLINEXY );
  CPPUNIT_ASSERT_EQUAL( true, anIt2.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test" ), anIt2.Current()->GetName() );

  QColor color1, color2, color3;
  Handle(HYDROData_PolylineXY)::DownCast( anIt2.Current() )->GetSectionColor(0, color1);

  CPPUNIT_ASSERT_EQUAL( QColor( Qt::red ), color1);
  anIt2.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt2.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test_1" ), anIt2.Current()->GetName() );

  Handle(HYDROData_PolylineXY)::DownCast( anIt2.Current() )->GetSectionColor(0, color2);
  CPPUNIT_ASSERT_EQUAL( QColor( Qt::red ), color2 );

  anIt2.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt2.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test_2" ), anIt2.Current()->GetName() );
  Handle(HYDROData_PolylineXY)::DownCast( anIt2.Current() )->GetSectionColor(0, color3);
  CPPUNIT_ASSERT_EQUAL( QColor( Qt::red ), color3 );
  anIt2.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt2.More() );
  anIt2.Next();

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_custom_polylines()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline1 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Handle(HYDROData_PolylineXY) aPolyline2 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Handle(HYDROData_PolylineXY) aPolyline3 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );

  aPolyline1->SetName( "test1" );
  aPolyline2->SetName( "test2" );
  aPolyline3->SetName( "test3" );

  CPPUNIT_ASSERT_EQUAL( false, aPolyline1->IsCustom() );
  aPolyline1->AddSection( "", HYDROData_IPolyline::SECTION_SPLINE, false );
  aPolyline1->Update();
  CPPUNIT_ASSERT_EQUAL( false, aPolyline1->IsCustom() );
  aPolyline1->AddPoint( 0, gp_XY( 0, 0 ) );
  aPolyline1->Update();
  CPPUNIT_ASSERT_EQUAL( false, aPolyline1->IsCustom() );

  CPPUNIT_ASSERT_EQUAL( false, aPolyline2->IsCustom() );
  aPolyline2->SetShape( Wire2d( QList<double>() << 0 << 0 << 10 << 10 << 20 << 0 ) );
  CPPUNIT_ASSERT_EQUAL( true, aPolyline2->IsCustom() );
  HYDROData_PolylineXY::PointsList aPointsList = aPolyline2->GetPoints( 0 );

  CPPUNIT_ASSERT_EQUAL( false, aPolyline2->IsCustom() );
  CPPUNIT_ASSERT_EQUAL( 5, aPointsList.Size() );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 0, 0 ), aPointsList.Value( 1 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 5, 7.5 ), aPointsList.Value( 2 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 10, 10 ), aPointsList.Value( 3 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 15, 7.5 ), aPointsList.Value( 4 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 20, 0 ), aPointsList.Value( 5 ) );


  CPPUNIT_ASSERT_EQUAL( false, aPolyline3->IsCustom() );
  aPolyline3->SetShape( WireCirc( gp_Pnt(), 10.0 ) );
  CPPUNIT_ASSERT_EQUAL( true, aPolyline3->IsCustom() );
  aPointsList = aPolyline3->GetPoints( 0 );

  CPPUNIT_ASSERT_EQUAL( HYDROData_PolylineXY::SECTION_SPLINE, aPolyline3->GetSectionType( 0 ) );
  CPPUNIT_ASSERT_EQUAL( true, aPolyline3->IsClosedSection( 0 ) );
  CPPUNIT_ASSERT_EQUAL( 7, aPointsList.Size() );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 10, 0 ), aPointsList.Value( 1 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 6.2349, 7.81831 ), aPointsList.Value( 2 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( -2.225, 9.749 ), aPointsList.Value( 3 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( -9.01, 4.339 ), aPointsList.Value( 4 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( -9.01, -4.339 ), aPointsList.Value( 5 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( -2.225, -9.749 ), aPointsList.Value( 6 ) );
  CPPUNIT_ASSERT_EQUAL( gp_XY( 6.2349, -7.81831 ), aPointsList.Value( 7 ) );

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_merge_refs_630()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline1 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Handle(HYDROData_PolylineXY) aPolyline2 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );

  aPolyline1->SetName( "test1" );
  aPolyline1->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, false );
  aPolyline1->AddPoint( 0, gp_XY( 0, 0 ) );
  aPolyline1->AddPoint( 0, gp_XY( 10, 0 ) );
  aPolyline1->AddPoint( 0, gp_XY( 10, 10 ) );
  aPolyline1->Update();

  aPolyline2->SetName( "test2" );
  aPolyline2->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, false );
  aPolyline2->AddPoint( 0, gp_XY( 20, 20 ) );
  aPolyline2->AddPoint( 0, gp_XY( 30, 20 ) );
  aPolyline2->AddPoint( 0, gp_XY( 30, 0 ) );
  aPolyline2->Update();

  HYDROData_PolylineOperator anOp;
  HYDROData_SequenceOfObjects aPolylines;
  aPolylines.Append( aPolyline1 );
  aPolylines.Append( aPolyline2 );
  CPPUNIT_ASSERT_EQUAL( true, anOp.Merge( aDoc, "", aPolylines, true, 1E-3 ) );
  //TODO: check false in merge

  HYDROData_Iterator anIt( aDoc, KIND_POLYLINEXY );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.Current()->GetName() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test2" ), anIt.Current()->GetName() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "merged_1" ), anIt.Current()->GetName() );
  Handle(HYDROData_PolylineXY) aMerged =
    Handle(HYDROData_PolylineXY)::DownCast( anIt.Current() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  TestViewer::show( aMerged->GetShape(), 0, true, "Merge_Polylines" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_split_straight_refs_634()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) aPolyline1 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Handle(HYDROData_PolylineXY) aPolyline2 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );

  aPolyline1->SetName( "test1" );
  aPolyline1->AddSection( "", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  aPolyline1->AddPoint( 0, gp_XY( 0, 0 ) );
  aPolyline1->AddPoint( 0, gp_XY( 10, 20 ) );
  aPolyline1->AddPoint( 0, gp_XY( 30, 10 ) );
  aPolyline1->Update();

  aPolyline2->SetName( "test2" );
  aPolyline2->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, false );
  aPolyline2->AddPoint( 0, gp_XY( 0, 30 ) );
  aPolyline2->AddPoint( 0, gp_XY( 10, 10 ) );
  aPolyline2->AddPoint( 0, gp_XY( 30, 20 ) );
  aPolyline2->Update();

  HYDROData_PolylineOperator anOp;
  HYDROData_SequenceOfObjects aPolylines;
  aPolylines.Append( aPolyline1 );
  aPolylines.Append( aPolyline2 );
  bool isIntersected;
  CPPUNIT_ASSERT_EQUAL( true, anOp.SplitTool( aDoc, aPolyline1, aPolyline2, 1E-3, isIntersected ) );
  CPPUNIT_ASSERT_EQUAL( true, isIntersected );

  HYDROData_Iterator anIt( aDoc, KIND_POLYLINEXY );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.Current()->GetName() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test2" ), anIt.Current()->GetName() );
  anIt.Next();

  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1_1" ), anIt.Current()->GetName() );
  Handle(HYDROData_PolylineXY) aPart1 =
    Handle(HYDROData_PolylineXY)::DownCast( anIt.Current() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_PolylineXY::SECTION_POLYLINE, aPart1->GetSectionType( 0 ) );
  anIt.Next();

  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1_2" ), anIt.Current()->GetName() );
  Handle(HYDROData_PolylineXY) aPart2 =
    Handle(HYDROData_PolylineXY)::DownCast( anIt.Current() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_PolylineXY::SECTION_POLYLINE, aPart2->GetSectionType( 0 ) );
  anIt.Next();

  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1_3" ), anIt.Current()->GetName() );
  Handle(HYDROData_PolylineXY) aPart3 =
    Handle(HYDROData_PolylineXY)::DownCast( anIt.Current() );
  CPPUNIT_ASSERT_EQUAL( HYDROData_PolylineXY::SECTION_POLYLINE, aPart3->GetSectionType( 0 ) );
  anIt.Next();

  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  TestViewer::show( aPart1->GetShape(), 0, true, "Split_Straight" );
  TestViewer::show( aPart2->GetShape(), 0, true, Qt::red );
  TestViewer::show( aPart3->GetShape(), 0, true, Qt::green );
  TestViewer::show( aPolyline2->GetShape(), 0, true, Qt::darkGreen );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_import_from_xyz()
{
  NCollection_Sequence<Handle(HYDROData_Entity)> ents;
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/profiles1.xyz";

  NCollection_Sequence<Handle(HYDROData_Entity)> importedEntities;
  bool stat = HYDROData_Tool::importPolylineFromXYZ(QString(fname.ToCString()), aDoc, true, importedEntities);
  CPPUNIT_ASSERT (stat);
  Handle(HYDROData_PolylineXY) aPolyXY = Handle(HYDROData_PolylineXY)::DownCast(importedEntities.First());
  CPPUNIT_ASSERT_EQUAL (importedEntities.Size(), 1);
  CPPUNIT_ASSERT (!aPolyXY.IsNull());
  TestViewer::show( aPolyXY->GetShape(), 0, true, "Polyline_import_XY" );
  CPPUNIT_ASSERT_IMAGES
  importedEntities.Clear();

  stat = HYDROData_Tool::importPolylineFromXYZ(QString(fname.ToCString()), aDoc, false, importedEntities);
  CPPUNIT_ASSERT (stat);
  CPPUNIT_ASSERT_EQUAL (importedEntities.Size(), 2);
  Handle(HYDROData_Polyline3D) aPoly3D;
  aPolyXY = Handle(HYDROData_PolylineXY)::DownCast(importedEntities.First());
  aPoly3D = Handle(HYDROData_Polyline3D)::DownCast(importedEntities.Last());
  CPPUNIT_ASSERT (!aPolyXY.IsNull());
  CPPUNIT_ASSERT (!aPoly3D.IsNull());
  TestViewer::show( aPolyXY->GetShape(), 0, false, "Polyline_import_XYZ" );
  TestViewer::show( aPoly3D->GetShape3D(), 0, false, "Polyline_import_XYZ_3D" );
  CPPUNIT_ASSERT_IMAGES
}


void test_HYDROData_PolylineXY::test_import_from_sx()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/polylines_sx.sx";

  HYDROData_SinusX aSinusXImporter;
  NCollection_Sequence<Handle(HYDROData_Entity)> importedEntities;
  //bool stat = aSinusXImporter.Import(QString(fname.ToCString()), aDoc, importedEntities);
  bool ParseStat = aSinusXImporter.OpenAndParse(QString(fname.ToCString()));
  CPPUNIT_ASSERT (ParseStat);
  aSinusXImporter.Import(aDoc, importedEntities, NULL);
  CPPUNIT_ASSERT_EQUAL (importedEntities.Size(), 9);

  Handle(HYDROData_PolylineXY) aPolyXY1 = Handle(HYDROData_PolylineXY)::DownCast(importedEntities(1));
  Handle(HYDROData_PolylineXY) aPolyXY2 = Handle(HYDROData_PolylineXY)::DownCast(importedEntities(4));
  Handle(HYDROData_PolylineXY) aPolyXY3 = Handle(HYDROData_PolylineXY)::DownCast(importedEntities(7));

  CPPUNIT_ASSERT (!aPolyXY1.IsNull());
  CPPUNIT_ASSERT (!aPolyXY2.IsNull());
  CPPUNIT_ASSERT (!aPolyXY3.IsNull());

  aPolyXY1->Update();
  aPolyXY2->Update();
  aPolyXY3->Update();

  CPPUNIT_ASSERT_EQUAL (aPolyXY1->GetName(), QString("AXIS"));
  CPPUNIT_ASSERT_EQUAL (aPolyXY2->GetName(), QString("LB"));
  CPPUNIT_ASSERT_EQUAL (aPolyXY3->GetName(), QString("RB"));

  TopoDS_Shape sh1 = aPolyXY1->GetShape();
  TopoDS_Shape sh2 = aPolyXY2->GetShape();
  TopoDS_Shape sh3 = aPolyXY3->GetShape();
  CPPUNIT_ASSERT (!sh1.IsNull());
  CPPUNIT_ASSERT (!sh2.IsNull());
  CPPUNIT_ASSERT (!sh3.IsNull());

  BRep_Builder BB;
  TopoDS_Compound cmp;
  BB.MakeCompound(cmp);
  BB.Add(cmp, sh1);
  BB.Add(cmp, sh2);
  BB.Add(cmp, sh3);

  TestViewer::show( cmp, 0, true, "Polylines_import_SX" );
  CPPUNIT_ASSERT_IMAGES
  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_import_from_sx_options()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/trait_cote_lambert1N.sx";

  HYDROData_SinusX aSinusXImporter;
  NCollection_Sequence<Handle(HYDROData_Entity)> importedEntities;
  //bool stat = aSinusXImporter.Import(QString(fname.ToCString()), aDoc, importedEntities);
  bool ParseStat = aSinusXImporter.OpenAndParse(QString(fname.ToCString()));
  CPPUNIT_ASSERT (ParseStat);

  std::vector<HYDROData_SinusX::ImportOptions> options;
  int size = aSinusXImporter.GetCurveBlocks().size();
  CPPUNIT_ASSERT_EQUAL(33, size);
  for (int i = 0; i < 33; i++)
  {
    HYDROData_SinusX::ImportOptions option;
    option.ImportAsBathy = false;
    if (i>12)
    {
      option.ImportAsPolyXY = true;
      option.ImportAsProfile = true;
    }
    else
    {
      option.ImportAsPolyXY = false;
      option.ImportAsProfile = false;
    }
    options.push_back(option);
  }

  aSinusXImporter.Import(aDoc, importedEntities, &options);
  CPPUNIT_ASSERT_EQUAL (importedEntities.Size(), 60);
  aDoc->Close();
}

void test_HYDROData_PolylineXY::test_polyline_dbf_info_simple()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast(aDoc->CreateObject(KIND_POLYLINEXY));
  QStringList dbf_dummy, dbf_out;
  dbf_dummy << "n1" << "n2" << "n3" << "n4";
  aPolyline->SetDBFInfo(dbf_dummy);
  CPPUNIT_ASSERT (aPolyline->GetDBFInfo(dbf_out));
  CPPUNIT_ASSERT_EQUAL (dbf_out.size(), 4);
  CPPUNIT_ASSERT ("n1" == dbf_out[0]);
  CPPUNIT_ASSERT ("n2" == dbf_out[1]);
  CPPUNIT_ASSERT ("n3" == dbf_out[2]);
  CPPUNIT_ASSERT ("n4" == dbf_out[3]);
}



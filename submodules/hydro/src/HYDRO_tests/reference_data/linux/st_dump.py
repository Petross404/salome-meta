from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0.000, 0.000 )

ST = hydro_doc.CreateObject( KIND_STRICKLER_TABLE )
ST.SetName( "ST" )

ST.SetAttrName( "CODE_06" )

ST.Set( u"Pelouses et pâturages naturels", 31 )
ST.SetAttrValue( u"Pelouses et pâturages naturels", "321" )
ST.SetColor( u"Pelouses et pâturages naturels", QColor( 204, 242, 77 ) )

ST.Set( u"Aéroports", 43 )
ST.SetAttrValue( u"Aéroports", "124" )
ST.SetColor( u"Aéroports", QColor( 230, 204, 230 ) )

ST.Set( u"Systèmes culturaux et parcellaires complexes", 19 )
ST.SetAttrValue( u"Systèmes culturaux et parcellaires complexes", "242" )
ST.SetColor( u"Systèmes culturaux et parcellaires complexes", QColor( 255, 230, 77 ) )

ST.Set( u"Cultures annuelles associées à des cultures permanentes", 27 )
ST.SetAttrValue( u"Cultures annuelles associées à des cultures permanentes", "241" )
ST.SetColor( u"Cultures annuelles associées à des cultures permanentes", QColor( 255, 230, 166 ) )

ST.Set( u"Forêt et végétation arbustive en mutation", 9 )
ST.SetAttrValue( u"Forêt et végétation arbustive en mutation", "324" )
ST.SetColor( u"Forêt et végétation arbustive en mutation", QColor( 166, 242, 0 ) )

ST.Set( u"Décharges", 21 )
ST.SetAttrValue( u"Décharges", "132" )
ST.SetColor( u"Décharges", QColor( 166, 77, 0 ) )

ST.Set( u"Végétation clairsemée", 43 )
ST.SetAttrValue( u"Végétation clairsemée", "333" )
ST.SetColor( u"Végétation clairsemée", QColor( 204, 255, 204 ) )

ST.Set( u"Prairies et autres surfaces toujours en herbe à usage agricole", 33 )
ST.SetAttrValue( u"Prairies et autres surfaces toujours en herbe à usage agricole", "231" )
ST.SetColor( u"Prairies et autres surfaces toujours en herbe à usage agricole", QColor( 230, 230, 77 ) )

ST.Set( u"Périmètres irrigués en permanence", 43 )
ST.SetAttrValue( u"Périmètres irrigués en permanence", "212" )
ST.SetColor( u"Périmètres irrigués en permanence", QColor( 255, 255, 0 ) )

ST.Set( u"Plans d'eau", 90 )
ST.SetAttrValue( u"Plans d'eau", "512" )
ST.SetColor( u"Plans d'eau", QColor( 128, 242, 230 ) )

ST.Set( u"Territoires agroforestiers", 14 )
ST.SetAttrValue( u"Territoires agroforestiers", "244" )
ST.SetColor( u"Territoires agroforestiers", QColor( 242, 204, 166 ) )

ST.Set( u"Forêts mélangées", 10 )
ST.SetAttrValue( u"Forêts mélangées", "313" )
ST.SetColor( u"Forêts mélangées", QColor( 77, 255, 0 ) )

ST.Set( u"Glaciers et neiges éternelles", 75 )
ST.SetAttrValue( u"Glaciers et neiges éternelles", "335" )
ST.SetColor( u"Glaciers et neiges éternelles", QColor( 166, 230, 204 ) )

ST.Set( u"Plages, dunes et sable", 39 )
ST.SetAttrValue( u"Plages, dunes et sable", "331" )
ST.SetColor( u"Plages, dunes et sable", QColor( 230, 230, 230 ) )

ST.Set( u"Zones incendiées", 65 )
ST.SetAttrValue( u"Zones incendiées", "334" )
ST.SetColor( u"Zones incendiées", QColor( 0, 0, 0 ) )

ST.Set( u"Tissu urbain continu", 15 )
ST.SetAttrValue( u"Tissu urbain continu", "111" )
ST.SetColor( u"Tissu urbain continu", QColor( 230, 0, 77 ) )

ST.Set( u"Chantiers", 17 )
ST.SetAttrValue( u"Chantiers", "133" )
ST.SetColor( u"Chantiers", QColor( 255, 77, 255 ) )

ST.Set( u"Estuaires", 98 )
ST.SetAttrValue( u"Estuaires", "522" )
ST.SetColor( u"Estuaires", QColor( 166, 255, 230 ) )

ST.Set( u"Marais maritimes", 74 )
ST.SetAttrValue( u"Marais maritimes", "421" )
ST.SetColor( u"Marais maritimes", QColor( 204, 204, 255 ) )

ST.Set( u"Forêts de conifères", 13 )
ST.SetAttrValue( u"Forêts de conifères", "312" )
ST.SetColor( u"Forêts de conifères", QColor( 0, 166, 0 ) )

ST.Set( u"Surfaces essentiellement agricoles, interrompues par des espaces naturels importants", 16 )
ST.SetAttrValue( u"Surfaces essentiellement agricoles, interrompues par des espaces naturels importants", "243" )
ST.SetColor( u"Surfaces essentiellement agricoles, interrompues par des espaces naturels importants", QColor( 230, 204, 77 ) )

ST.Set( u"Tourbières", 65 )
ST.SetAttrValue( u"Tourbières", "412" )
ST.SetColor( u"Tourbières", QColor( 77, 77, 255 ) )

ST.Set( u"Extraction de matériaux", 19 )
ST.SetAttrValue( u"Extraction de matériaux", "131" )
ST.SetColor( u"Extraction de matériaux", QColor( 166, 0, 204 ) )

ST.Set( u"Réseaux routier et ferroviaire et espaces associés", 35 )
ST.SetAttrValue( u"Réseaux routier et ferroviaire et espaces associés", "122" )
ST.SetColor( u"Réseaux routier et ferroviaire et espaces associés", QColor( 204, 0, 0 ) )

ST.Set( u"Mers et océans", 99 )
ST.SetAttrValue( u"Mers et océans", "523" )
ST.SetColor( u"Mers et océans", QColor( 230, 242, 255 ) )

ST.Set( u"Equipements sportifs et de loisirs", 40 )
ST.SetAttrValue( u"Equipements sportifs et de loisirs", "142" )
ST.SetColor( u"Equipements sportifs et de loisirs", QColor( 255, 230, 255 ) )

ST.Set( u"Forêts de feuillus", 9 )
ST.SetAttrValue( u"Forêts de feuillus", "311" )
ST.SetColor( u"Forêts de feuillus", QColor( 128, 255, 0 ) )

ST.Set( u"Vergers et petits fruits", 25 )
ST.SetAttrValue( u"Vergers et petits fruits", "222" )
ST.SetColor( u"Vergers et petits fruits", QColor( 242, 166, 77 ) )

ST.Set( u"Végétation sclérophylle", 10 )
ST.SetAttrValue( u"Végétation sclérophylle", "323" )
ST.SetColor( u"Végétation sclérophylle", QColor( 166, 230, 77 ) )

ST.Set( u"Landes et broussailles", 12 )
ST.SetAttrValue( u"Landes et broussailles", "322" )
ST.SetColor( u"Landes et broussailles", QColor( 166, 255, 128 ) )

ST.Set( u"Tissu urbain discontinu", 32 )
ST.SetAttrValue( u"Tissu urbain discontinu", "112" )
ST.SetColor( u"Tissu urbain discontinu", QColor( 255, 0, 0 ) )

ST.Set( u"Cours et voies d'eau", 88 )
ST.SetAttrValue( u"Cours et voies d'eau", "511" )
ST.SetColor( u"Cours et voies d'eau", QColor( 0, 204, 242 ) )

ST.Set( u"Oliveraies", 26 )
ST.SetAttrValue( u"Oliveraies", "223" )
ST.SetColor( u"Oliveraies", QColor( 230, 166, 0 ) )

ST.Set( u"Vignobles", 24 )
ST.SetAttrValue( u"Vignobles", "221" )
ST.SetColor( u"Vignobles", QColor( 230, 128, 0 ) )

ST.Set( u"Rizières", 42 )
ST.SetAttrValue( u"Rizières", "213" )
ST.SetColor( u"Rizières", QColor( 230, 230, 0 ) )

ST.Set( u"Zones portuaires", 45 )
ST.SetAttrValue( u"Zones portuaires", "123" )
ST.SetColor( u"Zones portuaires", QColor( 230, 204, 204 ) )

ST.Set( u"Zones industrielles ou commerciales et installations publiques", 30 )
ST.SetAttrValue( u"Zones industrielles ou commerciales et installations publiques", "121" )
ST.SetColor( u"Zones industrielles ou commerciales et installations publiques", QColor( 204, 77, 242 ) )

ST.Set( u"Marais salants", 73 )
ST.SetAttrValue( u"Marais salants", "422" )
ST.SetColor( u"Marais salants", QColor( 230, 230, 255 ) )

ST.Set( u"Marais intérieurs", 60 )
ST.SetAttrValue( u"Marais intérieurs", "411" )
ST.SetColor( u"Marais intérieurs", QColor( 166, 166, 255 ) )

ST.Set( u"Espaces verts urbains", 25 )
ST.SetAttrValue( u"Espaces verts urbains", "141" )
ST.SetColor( u"Espaces verts urbains", QColor( 255, 166, 255 ) )

ST.Set( u"Lagunes littorales", 95 )
ST.SetAttrValue( u"Lagunes littorales", "521" )
ST.SetColor( u"Lagunes littorales", QColor( 0, 255, 166 ) )

ST.Set( u"Roches nues", 45 )
ST.SetAttrValue( u"Roches nues", "332" )
ST.SetColor( u"Roches nues", QColor( 204, 204, 204 ) )

ST.Set( u"Terres arables hors périmètres d'irrigation", 31 )
ST.SetAttrValue( u"Terres arables hors périmètres d'irrigation", "211" )
ST.SetColor( u"Terres arables hors périmètres d'irrigation", QColor( 255, 255, 168 ) )

ST.Set( u"Zones intertidales", 75 )
ST.SetAttrValue( u"Zones intertidales", "423" )
ST.SetColor( u"Zones intertidales", QColor( 166, 166, 230 ) )


ST.Update()


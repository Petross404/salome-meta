﻿# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v7.6.0 with dump python functionality
###

import sys
import salome

salome.salome_init()

import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert( 0, r'D:/asl/hydro/studies')

###
### HYDRO component
###

from HYDROPy import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

hydro_doc = HYDROData_Document.Document();

hydro_doc.SetLocalCS( 0, 0 )

DefStr = hydro_doc.CreateObject( KIND_STRICKLER_TABLE );
DefStr.SetName( "DefStr" );

DefStr.SetAttrName( "CODE_06" );

DefStr.Set( "Zones de champs, prairies, sans cultures", 20 );
DefStr.SetAttrValue( "Zones de champs, prairies, sans cultures", "511" );
DefStr.SetColor( "Zones de champs, prairies, sans cultures", QColor( 255, 0, 0 ) );

DefStr.Set( "Zones à faible urbanisation (bourg)", 9 );
DefStr.SetAttrValue( "Zones à faible urbanisation (bourg)", "" );
DefStr.SetColor( "Zones à faible urbanisation (bourg)", QColor( 0, 0, 255 ) );

DefStr.Set( "Zones de champs cultivé à végétation haute", 12.5 );
DefStr.SetAttrValue( "Zones de champs cultivé à végétation haute", "" );
DefStr.SetColor( "Zones de champs cultivé à végétation haute", QColor( 0, 255, 0 ) );

DefStr.Set( "Canaux artificiels en béton", 65 );
DefStr.SetAttrValue( "Canaux artificiels en béton", "" );
DefStr.SetColor( "Canaux artificiels en béton", QColor( 136, 136, 136 ) );

DefStr.Set( "Canaux naturels", 35 );
DefStr.SetAttrValue( "Canaux naturels", "" );
DefStr.SetColor( "Canaux naturels", QColor( 255, 0, 255 ) );

DefStr.Set( "Zones à forte urbanisation (agglomération)", 9 );
DefStr.SetAttrValue( "Zones à forte urbanisation (agglomération)", "" );
DefStr.SetColor( "Zones à forte urbanisation (agglomération)", QColor( 18, 52, 86 ) );

DefStr.Set( "Zones de champs cultivé à végétation basse", 17.5 );
DefStr.SetAttrValue( "Zones de champs cultivé à végétation basse", "512" );
DefStr.SetColor( "Zones de champs cultivé à végétation basse", QColor( 255, 255, 0 ) );

DefStr.Set( "Zones d'arbustes, de sous-bois", 10 );
DefStr.SetAttrValue( "Zones d'arbustes, de sous-bois", "" );
DefStr.SetColor( "Zones d'arbustes, de sous-bois", QColor( 0, 255, 255 ) );


DefStr.Update();

test_LCM = hydro_doc.CreateObject( KIND_LAND_COVER_MAP );
test_LCM.SetName( "test_LCM" );

attr_values = QStringList()
attr_values.append( "" )
attr_values.append( "" )
attr_values.append( "" )
attr_values.append( "512" )
attr_values.append( "" )
attr_values.append( "511" )
attr_values.append( "" )
attr_values.append( "" )
types = QStringList()
types.append( "Canaux artificiels en béton" )
types.append( "Canaux naturels" )
types.append( "Zones d'arbustes, de sous-bois" )
types.append( "Zones de champs cultivé à végétation basse" )
types.append( "Zones de champs cultivé à végétation haute" )
types.append( "Zones de champs, prairies, sans cultures" )
types.append( "Zones à faible urbanisation (bourg)" )
types.append( "Zones à forte urbanisation (agglomération)" )
test_LCM.ImportSHP( 'lc_dump.shp' )
test_LCM.ImportDBF( 'lc_dump.dbf', 'CODE_06', attr_values, types )


from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

hydro_doc = HYDROData_Document.Document()

hydro_doc.SetLocalCS( 0.000, 0.000 )

ST = hydro_doc.CreateObject( KIND_STRICKLER_TABLE )
ST.SetName( "ST" )

ST.SetAttrName( "CODE_06" )

ST.Set( u"Pelouses et p�turages naturels", 31 )
ST.SetAttrValue( u"Pelouses et p�turages naturels", "321" )
ST.SetColor( u"Pelouses et p�turages naturels", QColor( 204, 242, 77 ) )

ST.Set( u"A�roports", 43 )
ST.SetAttrValue( u"A�roports", "124" )
ST.SetColor( u"A�roports", QColor( 230, 204, 230 ) )

ST.Set( u"Syst�mes culturaux et parcellaires complexes", 19 )
ST.SetAttrValue( u"Syst�mes culturaux et parcellaires complexes", "242" )
ST.SetColor( u"Syst�mes culturaux et parcellaires complexes", QColor( 255, 230, 77 ) )

ST.Set( u"Cultures annuelles associ�es � des cultures permanentes", 27 )
ST.SetAttrValue( u"Cultures annuelles associ�es � des cultures permanentes", "241" )
ST.SetColor( u"Cultures annuelles associ�es � des cultures permanentes", QColor( 255, 230, 166 ) )

ST.Set( u"For�t et v�g�tation arbustive en mutation", 9 )
ST.SetAttrValue( u"For�t et v�g�tation arbustive en mutation", "324" )
ST.SetColor( u"For�t et v�g�tation arbustive en mutation", QColor( 166, 242, 0 ) )

ST.Set( u"D�charges", 21 )
ST.SetAttrValue( u"D�charges", "132" )
ST.SetColor( u"D�charges", QColor( 166, 77, 0 ) )

ST.Set( u"V�g�tation clairsem�e", 43 )
ST.SetAttrValue( u"V�g�tation clairsem�e", "333" )
ST.SetColor( u"V�g�tation clairsem�e", QColor( 204, 255, 204 ) )

ST.Set( u"Prairies et autres surfaces toujours en herbe � usage agricole", 33 )
ST.SetAttrValue( u"Prairies et autres surfaces toujours en herbe � usage agricole", "231" )
ST.SetColor( u"Prairies et autres surfaces toujours en herbe � usage agricole", QColor( 230, 230, 77 ) )

ST.Set( u"P�rim�tres irrigu�s en permanence", 43 )
ST.SetAttrValue( u"P�rim�tres irrigu�s en permanence", "212" )
ST.SetColor( u"P�rim�tres irrigu�s en permanence", QColor( 255, 255, 0 ) )

ST.Set( u"Plans d'eau", 90 )
ST.SetAttrValue( u"Plans d'eau", "512" )
ST.SetColor( u"Plans d'eau", QColor( 128, 242, 230 ) )

ST.Set( u"Territoires agroforestiers", 14 )
ST.SetAttrValue( u"Territoires agroforestiers", "244" )
ST.SetColor( u"Territoires agroforestiers", QColor( 242, 204, 166 ) )

ST.Set( u"For�ts m�lang�es", 10 )
ST.SetAttrValue( u"For�ts m�lang�es", "313" )
ST.SetColor( u"For�ts m�lang�es", QColor( 77, 255, 0 ) )

ST.Set( u"Glaciers et neiges �ternelles", 75 )
ST.SetAttrValue( u"Glaciers et neiges �ternelles", "335" )
ST.SetColor( u"Glaciers et neiges �ternelles", QColor( 166, 230, 204 ) )

ST.Set( u"Plages, dunes et sable", 39 )
ST.SetAttrValue( u"Plages, dunes et sable", "331" )
ST.SetColor( u"Plages, dunes et sable", QColor( 230, 230, 230 ) )

ST.Set( u"Zones incendi�es", 65 )
ST.SetAttrValue( u"Zones incendi�es", "334" )
ST.SetColor( u"Zones incendi�es", QColor( 0, 0, 0 ) )

ST.Set( u"Tissu urbain continu", 15 )
ST.SetAttrValue( u"Tissu urbain continu", "111" )
ST.SetColor( u"Tissu urbain continu", QColor( 230, 0, 77 ) )

ST.Set( u"Chantiers", 17 )
ST.SetAttrValue( u"Chantiers", "133" )
ST.SetColor( u"Chantiers", QColor( 255, 77, 255 ) )

ST.Set( u"Estuaires", 98 )
ST.SetAttrValue( u"Estuaires", "522" )
ST.SetColor( u"Estuaires", QColor( 166, 255, 230 ) )

ST.Set( u"Marais maritimes", 74 )
ST.SetAttrValue( u"Marais maritimes", "421" )
ST.SetColor( u"Marais maritimes", QColor( 204, 204, 255 ) )

ST.Set( u"For�ts de conif�res", 13 )
ST.SetAttrValue( u"For�ts de conif�res", "312" )
ST.SetColor( u"For�ts de conif�res", QColor( 0, 166, 0 ) )

ST.Set( u"Surfaces essentiellement agricoles, interrompues par des espaces naturels importants", 16 )
ST.SetAttrValue( u"Surfaces essentiellement agricoles, interrompues par des espaces naturels importants", "243" )
ST.SetColor( u"Surfaces essentiellement agricoles, interrompues par des espaces naturels importants", QColor( 230, 204, 77 ) )

ST.Set( u"Tourbi�res", 65 )
ST.SetAttrValue( u"Tourbi�res", "412" )
ST.SetColor( u"Tourbi�res", QColor( 77, 77, 255 ) )

ST.Set( u"Extraction de mat�riaux", 19 )
ST.SetAttrValue( u"Extraction de mat�riaux", "131" )
ST.SetColor( u"Extraction de mat�riaux", QColor( 166, 0, 204 ) )

ST.Set( u"R�seaux routier et ferroviaire et espaces associ�s", 35 )
ST.SetAttrValue( u"R�seaux routier et ferroviaire et espaces associ�s", "122" )
ST.SetColor( u"R�seaux routier et ferroviaire et espaces associ�s", QColor( 204, 0, 0 ) )

ST.Set( u"Mers et oc�ans", 99 )
ST.SetAttrValue( u"Mers et oc�ans", "523" )
ST.SetColor( u"Mers et oc�ans", QColor( 230, 242, 255 ) )

ST.Set( u"Equipements sportifs et de loisirs", 40 )
ST.SetAttrValue( u"Equipements sportifs et de loisirs", "142" )
ST.SetColor( u"Equipements sportifs et de loisirs", QColor( 255, 230, 255 ) )

ST.Set( u"For�ts de feuillus", 9 )
ST.SetAttrValue( u"For�ts de feuillus", "311" )
ST.SetColor( u"For�ts de feuillus", QColor( 128, 255, 0 ) )

ST.Set( u"Vergers et petits fruits", 25 )
ST.SetAttrValue( u"Vergers et petits fruits", "222" )
ST.SetColor( u"Vergers et petits fruits", QColor( 242, 166, 77 ) )

ST.Set( u"V�g�tation scl�rophylle", 10 )
ST.SetAttrValue( u"V�g�tation scl�rophylle", "323" )
ST.SetColor( u"V�g�tation scl�rophylle", QColor( 166, 230, 77 ) )

ST.Set( u"Landes et broussailles", 12 )
ST.SetAttrValue( u"Landes et broussailles", "322" )
ST.SetColor( u"Landes et broussailles", QColor( 166, 255, 128 ) )

ST.Set( u"Tissu urbain discontinu", 32 )
ST.SetAttrValue( u"Tissu urbain discontinu", "112" )
ST.SetColor( u"Tissu urbain discontinu", QColor( 255, 0, 0 ) )

ST.Set( u"Cours et voies d'eau", 88 )
ST.SetAttrValue( u"Cours et voies d'eau", "511" )
ST.SetColor( u"Cours et voies d'eau", QColor( 0, 204, 242 ) )

ST.Set( u"Oliveraies", 26 )
ST.SetAttrValue( u"Oliveraies", "223" )
ST.SetColor( u"Oliveraies", QColor( 230, 166, 0 ) )

ST.Set( u"Vignobles", 24 )
ST.SetAttrValue( u"Vignobles", "221" )
ST.SetColor( u"Vignobles", QColor( 230, 128, 0 ) )

ST.Set( u"Rizi�res", 42 )
ST.SetAttrValue( u"Rizi�res", "213" )
ST.SetColor( u"Rizi�res", QColor( 230, 230, 0 ) )

ST.Set( u"Zones portuaires", 45 )
ST.SetAttrValue( u"Zones portuaires", "123" )
ST.SetColor( u"Zones portuaires", QColor( 230, 204, 204 ) )

ST.Set( u"Zones industrielles ou commerciales et installations publiques", 30 )
ST.SetAttrValue( u"Zones industrielles ou commerciales et installations publiques", "121" )
ST.SetColor( u"Zones industrielles ou commerciales et installations publiques", QColor( 204, 77, 242 ) )

ST.Set( u"Marais salants", 73 )
ST.SetAttrValue( u"Marais salants", "422" )
ST.SetColor( u"Marais salants", QColor( 230, 230, 255 ) )

ST.Set( u"Marais int�rieurs", 60 )
ST.SetAttrValue( u"Marais int�rieurs", "411" )
ST.SetColor( u"Marais int�rieurs", QColor( 166, 166, 255 ) )

ST.Set( u"Espaces verts urbains", 25 )
ST.SetAttrValue( u"Espaces verts urbains", "141" )
ST.SetColor( u"Espaces verts urbains", QColor( 255, 166, 255 ) )

ST.Set( u"Lagunes littorales", 95 )
ST.SetAttrValue( u"Lagunes littorales", "521" )
ST.SetColor( u"Lagunes littorales", QColor( 0, 255, 166 ) )

ST.Set( u"Roches nues", 45 )
ST.SetAttrValue( u"Roches nues", "332" )
ST.SetColor( u"Roches nues", QColor( 204, 204, 204 ) )

ST.Set( u"Terres arables hors p�rim�tres d'irrigation", 31 )
ST.SetAttrValue( u"Terres arables hors p�rim�tres d'irrigation", "211" )
ST.SetColor( u"Terres arables hors p�rim�tres d'irrigation", QColor( 255, 255, 168 ) )

ST.Set( u"Zones intertidales", 75 )
ST.SetAttrValue( u"Zones intertidales", "423" )
ST.SetColor( u"Zones intertidales", QColor( 166, 166, 230 ) )


ST.Update()


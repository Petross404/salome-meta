﻿// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_LandCoverMap.h>
#include <HYDROData_CalculationCase.h>
#include <HYDROData_Document.h>
#include <HYDROData_LandCoverMap.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_Tool.h>
#include <HYDROData_ShapeFile.h>
#include <HYDROGUI_LandCoverMapPrs.h>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS.hxx>
#include <TestViewer.h>
#include <TestShape.h>
#include <TopTools_ListOfShape.hxx>
#include <AIS_DisplayMode.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <QString>
#include <QColor>
#include <QMap>
#include <QDir>
#include <BRepTools.hxx>
#include <BRep_Builder.hxx>
#include <BRepCheck_Analyzer.hxx>
#include <OSD_Timer.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

extern QString REF_DATA_PATH;
const QString DEF_STR_PATH = qgetenv( "HYDRO_ROOT_DIR" ) + "/share/salome/resources/hydro/def_strickler_table_06.txt";
extern QString TMP_DIR;

void test_HYDROData_LandCoverMap::test_add_2_objects()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 10 << 10 << 50 << 20 << 30 << 50 << 15 << 30 );
  //DEBTRACE("--- ajout test1 " << aLC1);
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, "test1" ) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 30 << 20 << 60 << 10 << 70 << 35 << 40 << 40 );
  //DEBTRACE("--- ajout test2 " << aLC2);
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, "test2" ) );

  HYDROData_LandCoverMap::Explorer anIt( aMap );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  //DEBTRACE(anIt.Face() << " " << anIt.StricklerType());
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.StricklerType() );
  anIt.Next();
  //DEBTRACE(anIt.Face() << " " << anIt.StricklerType());
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test2" ), anIt.StricklerType() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  //DEBTRACE("--- show")
  TestViewer::show( aMap->GetShape(), AIS_Shaded, true, "LandCoverMap_Add_2_Objects" );
  CPPUNIT_ASSERT_IMAGES

  anIt.Init(*aMap);
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  //DEBTRACE(anIt.Face() << " " << anIt.StricklerType());
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.StricklerType() );
  anIt.Next();
  //DEBTRACE(anIt.Face() << " " << anIt.StricklerType());
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test2" ), anIt.StricklerType() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_split()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );

  TopoDS_Face aLC = Face2d( QList<double>() << 10 << 10 << 50 << 20 << 30 << 50 << 15 << 30 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC, "test1" ) );

  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  TopoDS_Wire aWire = Wire2d( QList<double>() << 10 << 40 << 30 << 10 << 40 << 10 << 60 << 10, false );
  aPolyline->SetShape( aWire );

  CPPUNIT_ASSERT_EQUAL( true, aMap->Split( aPolyline ) );

  TestViewer::show( aMap->GetShape(), AIS_Shaded, true, "LandCoverMap_Split_1" );
  TestViewer::show( aWire, 0, true, Qt::blue );
  CPPUNIT_ASSERT_IMAGES

  HYDROData_LandCoverMap::Explorer anIt( aMap );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.StricklerType() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.StricklerType() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_incomplete_split()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );

  TopoDS_Face aLC = Face2d( QList<double>() << 10 << 10 << 50 << 20 << 30 << 50 << 15 << 30 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC, "test1" ) );

  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  TopoDS_Wire aWire = Wire2d( QList<double>() << 10 << 40 << 30 << 10 << 40 << 10, false );
  aPolyline->SetShape( aWire );

  CPPUNIT_ASSERT_EQUAL( false, aMap->Split( aPolyline ) );

  TestViewer::show( aMap->GetShape(), AIS_Shaded, true, "LandCoverMap_Split_2" );
  TestViewer::show( aWire, 0, true, Qt::green );
  CPPUNIT_ASSERT_IMAGES

  HYDROData_LandCoverMap::Explorer anIt( aMap );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test1" ), anIt.StricklerType() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_merge()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 12 << 19 << 82 << 9 << 126 << 53 << 107 << 80 << 29 << 75 );

  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, "test1" ) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, "test2" ) );

  TopoDS_Face aLC3 = Face2d( QList<double>() << 4 << 54 << 1   << 47 << 51  << 45 <<
                                            127 << 42 << 145 << 43 << 148 << 60 << 90 << 65 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC3, "test3" ) );

  //TestViewer::show( aMap->GetShape(), AIS_Shaded, true );
  //TestViewer::AssertEqual( "LandCoverMap_Before_Merge" );

  //BRepTools::Write( aMap->GetShape(), "c:\\map.brep" );

  QString aType1, aType2;
  gp_Pnt2d aPnt1( 25, 35 );
  gp_Pnt2d aPnt2( 45, 55 );
  //gp_Pnt2d aPnt3( 45, 10 );
  //gp_Pnt2d aPnt4( 45, 80 );
  TopTools_ListOfShape aList;
  aList.Append( aMap->FindByPoint( aPnt1, aType1 ) );
  aList.Append( aMap->FindByPoint( aPnt2, aType2 ) );
  CPPUNIT_ASSERT_EQUAL( true, aMap->Merge( aList, "new" ) );

  TestViewer::show( aMap->GetShape(), AIS_Shaded, true, "LandCoverMap_Merge_1" );
  //TestViewer::show( BRepBuilderAPI_MakeEdge( gp_Pnt(aPnt1.X(), aPnt1.Y(), 0), gp_Pnt(aPnt2.X(), aPnt2.Y(), 0) ).Edge(), QColor( Qt::blue ), AIS_Shaded );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_merge_refs_691()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) Polyline_4 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_4->SetName( "test_Polyline_4" );
  Polyline_4->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_4->AddPoint( 0, gp_XY( -242.741935, -16.129032 ) );
  Polyline_4->AddPoint( 0, gp_XY( -339.516129, -95.161290 ) );
  Polyline_4->AddPoint( 0, gp_XY( -204.032258, -167.740000 ) );
  Polyline_4->AddPoint( 0, gp_XY( -86.290000, -158.060000 ) );
  Polyline_4->AddPoint( 0, gp_XY( -83.064516, -37.096774 ) );
  Polyline_4->AddPoint( 0, gp_XY( -116.935484, 17.741935 ) );
  Polyline_4->AddSection( "Section_2", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_4->AddPoint( 1, gp_XY( -239.516129, -295.161290 ) );
  Polyline_4->AddPoint( 1, gp_XY( -204.030000, -167.740000 ) );
  Polyline_4->AddPoint( 1, gp_XY( -86.290000, -158.060000 ) );
  Polyline_4->AddPoint( 1, gp_XY( -2.419355, -288.709677 ) );
  Polyline_4->AddPoint( 1, gp_XY( -165.322581, -351.612903 ) );
  Polyline_4->Update();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( true, aMap->Add( Polyline_4, "test" ) );

  TopTools_ListOfShape aList;
  HYDROData_LandCoverMap::Explorer anIt( aMap );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test" ), anIt.StricklerType() );
  aList.Append( anIt.Face() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString( "test" ), anIt.StricklerType() );
  aList.Append( anIt.Face() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  BRepCheck_Analyzer aCheck1( aMap->GetShape() );
  CPPUNIT_ASSERT_EQUAL( true, (bool)aCheck1.IsValid() );

  CPPUNIT_ASSERT_EQUAL( true, aMap->Merge( aList, "test" ) );

  BRepCheck_Analyzer aCheck2( aMap->GetShape() );
  CPPUNIT_ASSERT_EQUAL( true, (bool)aCheck2.IsValid() );

  // we try construct the triangulation to check that it is correct
  BRepMesh_IncrementalMesh anIncMesh( aMap->GetShape(), 0.01 );
  CPPUNIT_ASSERT_EQUAL( true, (bool)anIncMesh.IsDone() );
  BRepCheck_Analyzer aCheck3( aMap->GetShape() );
  CPPUNIT_ASSERT_EQUAL( true, (bool)aCheck3.IsValid() );

  Handle(HYDROGUI_LandCoverMapPrs) aPrs = new HYDROGUI_LandCoverMapPrs( aMap );
  TestViewer::show( aPrs, AIS_Shaded, 0, true, "LandCoverMap_Merge_2" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_remove()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 12 << 19 << 82 << 9 << 126 << 53 << 107 << 80 << 29 << 75 );

  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, "test1" ) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, "test2" ) );

  TopoDS_Face aLC3 = Face2d( QList<double>() << 4 << 54 << 1   << 47 << 51  << 45 <<
                                            127 << 42 << 145 << 43 << 148 << 60 << 90 << 65 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC3, "test3" ) );

  QString aType1, aType2;
  gp_Pnt2d aPnt1( 25, 35 );
  gp_Pnt2d aPnt2( 45, 55 );
  TopTools_ListOfShape aList;
  aList.Append( aMap->FindByPoint( aPnt1, aType1 ) );
  aList.Append( aMap->FindByPoint( aPnt2, aType2 ) );
  CPPUNIT_ASSERT_EQUAL( true, aMap->Remove( aList ) );

  TestViewer::show( aMap->GetShape(), AIS_Shaded, true, "LandCoverMap_Remove_1" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_merge_faces_boxes()
{
  TopoDS_Shape pp1, pp2, pp3, pp4;
  BRep_Builder BB;
  BRepTools::Read(pp1, (REF_DATA_PATH + "/pp1.brep").toStdString().c_str(), BB);
  BRepTools::Read(pp2, (REF_DATA_PATH + "/pp2.brep").toStdString().c_str(), BB);
  BRepTools::Read(pp3, (REF_DATA_PATH + "/pp3.brep").toStdString().c_str(), BB);
  BRepTools::Read(pp4, (REF_DATA_PATH + "/pp4.brep").toStdString().c_str(), BB);

  CPPUNIT_ASSERT(!pp1.IsNull());
  CPPUNIT_ASSERT(!pp2.IsNull());
  CPPUNIT_ASSERT(!pp3.IsNull());
  CPPUNIT_ASSERT(!pp4.IsNull());

  //Test mergeFaces() func // boxes // USD == true
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_11" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_FACE);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp3);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_12" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp2);
    Faces.Append(pp4);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_13" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp3);
    Faces.Append(pp4);
    Faces.Append(pp2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_14" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_FACE);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  //

  //Test mergeFaces() func // boxes // USD == false
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_21" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_SHELL);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp3);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_22" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp2);
    Faces.Append(pp4);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_23" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(pp1);
    Faces.Append(pp3);
    Faces.Append(pp4);
    Faces.Append(pp2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_boxes_24" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_SHELL);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }

}


void test_HYDROData_LandCoverMap::test_merge_faces_circles()
{
  TopoDS_Shape ff1, ff2, ff3;
  BRep_Builder BB;
  BRepTools::Read(ff1, (REF_DATA_PATH + "/ff1.brep").toStdString().c_str(), BB);
  BRepTools::Read(ff2, (REF_DATA_PATH + "/ff2.brep").toStdString().c_str(), BB);
  BRepTools::Read(ff3, (REF_DATA_PATH + "/ff3.brep").toStdString().c_str(), BB);

  CPPUNIT_ASSERT(!ff1.IsNull());
  CPPUNIT_ASSERT(!ff2.IsNull());
  CPPUNIT_ASSERT(!ff3.IsNull());

  //Test mergeFaces() func // circles // USD == true
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_11" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_FACE);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff3);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_12" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff2);
    Faces.Append(ff3);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_13" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff3);
    Faces.Append(ff2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, true );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_14" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  //

  //Test mergeFaces() func // circles // USD == false
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_21" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_SHELL);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff3);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_22" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff2);
    Faces.Append(ff3);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_23" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
  {
    TopTools_ListOfShape Faces;
    Faces.Append(ff1);
    Faces.Append(ff3);
    Faces.Append(ff2);
    TopoDS_Shape aMergedFace = HYDROData_LandCoverMap::MergeFaces( Faces, false );
    TestViewer::show( aMergedFace, AIS_Shaded, true, "merge_faces_circles_24" );
    CPPUNIT_ASSERT_IMAGES
    CPPUNIT_ASSERT(aMergedFace.ShapeType() == TopAbs_COMPOUND);
    BRepCheck_Analyzer aBCA(aMergedFace);
    CPPUNIT_ASSERT(aBCA.IsValid());
  }
}

void test_HYDROData_LandCoverMap::test_import_dbf()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );
  QString aFileName = REF_DATA_PATH + "/t1.dbf";
  QStringList aDBFV;
  QStringList aST;
  aDBFV.append("100");
  aDBFV.append("200");
  aDBFV.append("300");
  aST.append("water");
  aST.append("forest");
  aST.append("road");
  // aST.size() == aDBFV.size()!!
  Handle(HYDROData_PolylineXY) aPolyline =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  TopoDS_Wire aWire = Wire2d( QList<double>() << 10 << 40 << 30 << 10 << 40 << 10 << 60 << 10 );
  aPolyline->SetShape( aWire );
  for (int i = 0; i < 3; i++)
    aMap->Add(aPolyline, "");
  TopoDS_Face aLC1 = Face2d( QList<double>() << 10 << 10 << 50 << 20 << 30 << 50 << 15 << 30 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, "test1" ) );

  CPPUNIT_ASSERT_EQUAL( true, aMap->Split( aPolyline ) );
  QList<int> Inds = QList<int>() << 1 << 2 << 3;
  aMap->ImportDBF(aFileName, "TESTFIELD1", aDBFV, aST, Inds);
  HYDROData_LandCoverMap::Explorer anIt( aMap );
  CPPUNIT_ASSERT_EQUAL( QString( "forest" ), anIt.StricklerType() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( QString( "road" ), anIt.StricklerType() );

  aDoc->Close();
}

#include <QTest>

void test_HYDROData_LandCoverMap::test_land_cover_prs_by_types()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 12 << 19 << 82 << 9 << 126 << 53 << 107 << 80 << 29 << 75 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, QString::fromUtf8("Zones de champs cultivé à végétation basse")) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, QString::fromUtf8("Zones de champs cultivé à végétation haute")) );

  TopoDS_Face aLC3 = Face2d( QList<double>() << 4 << 54 << 1   << 47 << 51  << 45 <<
                                            127 << 42 << 145 << 43 << 148 << 60 << 90 << 65 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC3, QString::fromUtf8("Zones de champs, prairies, sans cultures")) );

  // build presentation object
  Handle(HYDROGUI_LandCoverMapPrs) aPrs = new HYDROGUI_LandCoverMapPrs( aMap );
  aPrs->SetTable( aTable );
  // show presentation in viewer
  TestViewer::show( aPrs, AIS_Shaded, 4, true, "LandCoverMap_PrsByTypes" );
  // select one of faces (first)
  TestViewer::select( 40, 20 );
  QTest::qWait(1000);
   TestViewer::select( 40, 60 );
  QTest::qWait(1000);
   TestViewer::select( 80, 60 );
  QTest::qWait(1000);
   TestViewer::select( 80, 100 );
  QTest::qWait(1000);
   TestViewer::select( 130, 40 );
  QTest::qWait(1000);
   TestViewer::select( 130, 60 );
  QTest::qWait(1000);
   TestViewer::select( 80, 20 );
  QTest::qWait(1000);
   TestViewer::select( 40, 80 );
  QTest::qWait(1000);

  CPPUNIT_ASSERT_IMAGES
  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_land_cover_prs_by_coeff()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 12 << 19 << 82 << 9 << 126 << 53 << 107 << 80 << 29 << 75 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, QString::fromUtf8("Zones de champs cultivé à végétation basse")) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, QString::fromUtf8("Zones de champs cultivé à végétation haute")) );

  TopoDS_Face aLC3 = Face2d( QList<double>() << 4 << 54 << 1   << 47 << 51  << 45 <<
                                            127 << 42 << 145 << 43 << 148 << 60 << 90 << 65 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC3, QString::fromUtf8("Zones de champs, prairies, sans cultures")) );

  Handle(HYDROGUI_LandCoverMapPrs) aPrs = new HYDROGUI_LandCoverMapPrs( aMap );
  aPrs->SetTable( aTable );
  TestViewer::showColorScale( true );
  aPrs->SetColorScale( TestViewer::colorScale() );
  TestViewer::show( aPrs, AIS_Shaded, 4, true, "LandCoverMap_PrsByCoeff" );

  CPPUNIT_ASSERT_IMAGES

  TestViewer::showColorScale( false );
  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_dump_python()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );
  aTable->SetName( "DefStr" );

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  aMap->SetName( "test_LCM" );

  CPPUNIT_ASSERT_EQUAL( KIND_LAND_COVER_MAP, aMap->GetKind() );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 12 << 19 << 82 << 9 << 126 << 53 << 107 << 80 << 29 << 75 );

  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, "test1" ) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, "test2" ) );

  TopoDS_Face aLC3 = Face2d( QList<double>() << 4 << 54 << 1   << 47 << 51  << 45 <<
                                            127 << 42 << 145 << 43 << 148 << 60 << 90 << 65 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC3, "test3" ) );

  QString aBaseName = "lc_dump.py";
  QString aBaseShp = "lc_dump.shp";
  QString aBaseDbf = "lc_dump.dbf";
  QString aTmpPath = TMP_DIR + "/" + aBaseName;
  CPPUNIT_ASSERT_EQUAL( true, aDoc->DumpToPython( aTmpPath, false ) );

  //TODO: CPPUNIT_ASSERT_SCRIPTS_EQUAL( aBaseName, true, false, 20 );
  //TODO: CPPUNIT_ASSERT_FILES_EQUAL( aBaseShp );
  //TODO: CPPUNIT_ASSERT_FILES_EQUAL( aBaseDbf );

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_transparent_prs()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  Handle(HYDROData_PolylineXY) aPoly =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  aPoly->AddSection( "", HYDROData_PolylineXY::SECTION_SPLINE, true );
  aPoly->AddPoint( 0, gp_XY( 0, 0 ) );
  aPoly->AddPoint( 0, gp_XY( 20, 0 ) );
  aPoly->AddPoint( 0, gp_XY( 10, 10 ) );
  aPoly->Update();

  Handle(HYDROData_ImmersibleZone) aZone =
    Handle(HYDROData_ImmersibleZone)::DownCast( aDoc->CreateObject( KIND_IMMERSIBLE_ZONE ) );
  aZone->SetPolyline( aPoly );
  aZone->Update();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  aMap->LocalPartition( Face2d( QList<double>() << 1 << 1 << 10 << 10 << 10 << 20 ), QString::fromUtf8("Zones de champs cultivé à végétation haute"));
  aMap->LocalPartition( Face2d( QList<double>() << 5 << 5 << 10 << 5 << 10 << 8 << 5 << 12 << 5 << 8 ), QString::fromUtf8("Zones de champs cultivé à végétation haute"));
  aMap->SetName( "test_LCM" );

  TestViewer::show( aZone->GetTopShape(), AIS_Shaded, true, "LandCoverMap_TransparentPrs" );

  aMap->SetTransparency( 0.5 );
  CPPUNIT_ASSERT_DOUBLES_EQUAL( 0.5, aMap->GetTransparency(), 1E-6 );

  Handle(HYDROGUI_LandCoverMapPrs) aPrs = new HYDROGUI_LandCoverMapPrs( aMap );
  aPrs->SetTable( aTable );
  TestViewer::show( aPrs, AIS_Shaded, 0, true, "" );

  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_add_triangles()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) Polyline_1 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );

  Polyline_1->SetName( "Polyline_1" );
  Polyline_1->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_1->AddPoint( 0, gp_XY( -264.84, 323.44 ) );
  Polyline_1->AddPoint( 0, gp_XY( 254.45, 301.19 ) );
  Polyline_1->AddPoint( 0, gp_XY( -291.54, -47.48 ) );
  Polyline_1->Update();

  Handle(HYDROData_PolylineXY) Polyline_2 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_2->SetName( "Polyline_2" );
  Polyline_2->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  Polyline_2->AddPoint( 0, gp_XY( -42.28, -351.63 ) );
  Polyline_2->AddPoint( 0, gp_XY( 218.84, -146.88 ) );
  Polyline_2->AddPoint( 0, gp_XY( 413.20, -235.91 ) );
  Polyline_2->AddPoint( 0, gp_XY( 466.62, -388.72 ) );
  Polyline_2->Update();

  Handle(HYDROData_PolylineXY) Polyline_3 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_3->SetName( "Polyline_3" );
  Polyline_3->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_3->AddPoint( 0, gp_XY( -108.31, 213.65 ) );
  Polyline_3->AddPoint( 0, gp_XY( 127.60, 382.79 ) );
  Polyline_3->AddPoint( 0, gp_XY( 192.88, -91.99 ) );
  Polyline_3->Update();

  Handle(HYDROData_PolylineXY) Polyline_4 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_4->SetName( "Polyline_4" );
  Polyline_4->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_4->AddPoint( 0, gp_XY( 215.52, 344.61 ) );
  Polyline_4->AddPoint( 0, gp_XY( 68.25, 258.52 ) );
  Polyline_4->AddPoint( 0, gp_XY( 200.58, 154.65 ) );
  Polyline_4->Update();

  Handle(HYDROData_PolylineXY) Polyline_5 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_5->SetName( "Polyline_5" );
  Polyline_5->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  Polyline_5->AddPoint( 0, gp_XY( 16.15, -58.08 ) );
  Polyline_5->AddPoint( 0, gp_XY( 116.47, 84.21 ) );
  Polyline_5->AddPoint( 0, gp_XY( 266.59, 29.43 ) );
  Polyline_5->Update();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  CPPUNIT_ASSERT_EQUAL( true, aMap->Add( Polyline_1, "" ) );
  CPPUNIT_ASSERT_EQUAL( true, aMap->Add( Polyline_3, "" ) );
  CPPUNIT_ASSERT_EQUAL( true, aMap->Add( Polyline_4, "" ) );

  Handle(HYDROGUI_LandCoverMapPrs) aPrs = new HYDROGUI_LandCoverMapPrs( aMap );
  TopoDS_Shape aSh = aMap->GetShape();
  TestViewer::show( aPrs, AIS_Shaded, 0, true, "LandCover_Triangles" );
  CPPUNIT_ASSERT_IMAGES

  CPPUNIT_ASSERT_EQUAL( true, aMap->Split( Polyline_5 ) );
  Handle(HYDROGUI_LandCoverMapPrs) aPrs2 = new HYDROGUI_LandCoverMapPrs( aMap );
  TestViewer::show( aPrs2, AIS_Shaded, 0, true, "LandCover_Triangles_Split" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_assign_to_calc_case()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  Handle(HYDROData_CalculationCase) aCalcCase =
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->CreateObject( KIND_CALCULATION ) );

  aCalcCase->SetLandCoverMap( aMap );
  CPPUNIT_ASSERT_EQUAL( aMap->myLab, aCalcCase->GetLandCoverMap()->myLab );

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_shp_import_cyp()
{
  //TestViewer::eraseAll(true);

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  QString aFileName = REF_DATA_PATH + "/cyprus_natural.shp";
  HYDROData_ShapeFile anImporter;
  QStringList PolygonList;
  TopTools_SequenceOfShape PolygonFaces;

  int aStat = anImporter.ImportPolygons(aDoc, aFileName, PolygonList, PolygonFaces);
  int Type = anImporter.GetShapeType();
  CPPUNIT_ASSERT(aStat == 1);
  CPPUNIT_ASSERT_EQUAL(5, Type);
  CPPUNIT_ASSERT_EQUAL(268, PolygonFaces.Length());

  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;

  for ( int i = 1; i <= 20; i++ )
  {
    TopoDS_Shape aShape = PolygonFaces(i);
    if ( aShape.IsNull() )
      continue;
    aMapFace2ST.Add( TopoDS::Face( aShape ), "" );
  }

  LCM->StoreLandCovers(aMapFace2ST);
  TestViewer::show( LCM->GetShape(), AIS_Shaded, true, "cyprus_natural_all" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_shp_import_clc_dec()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  QString aFileName = REF_DATA_PATH + "/CLC_decoupe.shp";
  HYDROData_ShapeFile anImporter;
  QStringList PolygonList;
  TopTools_SequenceOfShape PolygonFaces;
  int aStat = anImporter.ImportPolygons(aDoc, aFileName, PolygonList, PolygonFaces);
    int Type = anImporter.GetShapeType();

  CPPUNIT_ASSERT(aStat == 1);
  CPPUNIT_ASSERT_EQUAL(5, Type);
  CPPUNIT_ASSERT_EQUAL(625, PolygonFaces.Length());

  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;

  for ( int i = 150; i <= 160; i++ )
  {
    TopoDS_Shape aShape = PolygonFaces(i);
    if ( aShape.IsNull() )
      continue;
    aMapFace2ST.Add( TopoDS::Face( aShape ), "" );
  }

  LCM->StoreLandCovers(aMapFace2ST);
  TestViewer::show( LCM->GetShape(), AIS_Shaded, true, "clc_dec_150_350" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_shp_import_nld_areas()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  QString aFileName = REF_DATA_PATH + "/NLD_water_areas_dcw.shp";
  HYDROData_ShapeFile anImporter;
  QStringList PolygonList;
  TopTools_SequenceOfShape PolygonFaces;
  int aStat = anImporter.ImportPolygons(aDoc, aFileName, PolygonList, PolygonFaces);
  int Type = anImporter.GetShapeType();
  CPPUNIT_ASSERT(aStat == 1);
  CPPUNIT_ASSERT_EQUAL(5, Type);
  CPPUNIT_ASSERT_EQUAL(127, PolygonFaces.Length());

  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;

  for ( int i = 1; i <= PolygonFaces.Length(); i++ )
  {
    TopoDS_Shape aShape = PolygonFaces(i);
    if ( aShape.IsNull() )
      continue;
    aMapFace2ST.Add( TopoDS::Face( aShape ), "" );
  }

  LCM->StoreLandCovers(aMapFace2ST);
  TestViewer::show( LCM->GetShape(), AIS_Shaded, true, "NLD_water_areas_dcw" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_shp_loop_back()
{
  QString aFFileName = REF_DATA_PATH + "/CLC06-cut_1.shp";
  QString aSFileName = REF_DATA_PATH + "/CLC06-cut_1_res.shp";

  //This test verify only some geom data (shp+shx) without any dbf reading/writing
  {
    //DEBTRACE("aFFileName: " << aFFileName.toStdString());
    //DEBTRACE("aSFileName: " << aSFileName.toStdString());
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    HYDROData_ShapeFile anImporter;
    QStringList PolygonList;
    TopTools_SequenceOfShape PolygonFaces;

    //import LCM from file (#2-4; #12-14 polygons)
    CPPUNIT_ASSERT( anImporter.ImportPolygons(aDoc, aFFileName, PolygonList, PolygonFaces));
    Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
    HYDROData_MapOfFaceToStricklerType aMapFace2ST;

    aMapFace2ST.Add( TopoDS::Face( PolygonFaces(2) ), "" );
    aMapFace2ST.Add( TopoDS::Face( PolygonFaces(3) ), "" );
    aMapFace2ST.Add( TopoDS::Face( PolygonFaces(4) ), "" );
    aMapFace2ST.Add( TopoDS::Face( PolygonFaces(12) ), "" );
    aMapFace2ST.Add( TopoDS::Face( PolygonFaces(13) ), "" );
    aMapFace2ST.Add( TopoDS::Face( PolygonFaces(14) ), "" );

    LCM->StoreLandCovers(aMapFace2ST);
    LCM->SetName("lcm_1");

    TopoDS_Shape aSh = LCM->GetShape();
    BRepCheck_Analyzer aBCA(aSh);
    CPPUNIT_ASSERT(aBCA.IsValid());

    //export lcm
    HYDROData_ShapeFile anExporter;
    QStringList aNonExpList;
    anExporter.Export(aDoc, aSFileName, LCM, aNonExpList);
    CPPUNIT_ASSERT (aNonExpList.empty());

    aDoc->Close();
  }
  {
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    HYDROData_ShapeFile anImporter;
    QStringList PolygonList;
    TopTools_SequenceOfShape PolygonFaces;
    //import all
    CPPUNIT_ASSERT( anImporter.ImportPolygons(aDoc, aSFileName, PolygonList, PolygonFaces));
    Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
    HYDROData_MapOfFaceToStricklerType aMapFace2ST;
    CPPUNIT_ASSERT_EQUAL(PolygonFaces.Length(), 6);
    for ( int i = 1; i <= PolygonFaces.Length(); i++ )
      aMapFace2ST.Add( TopoDS::Face( PolygonFaces(i) ), "" );

    LCM->StoreLandCovers(aMapFace2ST);
    LCM->SetName("lcm_2");

    TopoDS_Shape OutSh = LCM->GetShape();
    BRepCheck_Analyzer aBCA(OutSh);
    CPPUNIT_ASSERT(aBCA.IsValid());

    TestViewer::show( OutSh, AIS_Shaded, true, "CLC06-cut_1_res" );
    CPPUNIT_ASSERT_IMAGES

    aDoc->Close();
  }
  CPPUNIT_ASSERT_EQUAL(0, remove(aSFileName.toStdString().c_str()));
  TestViewer::eraseAll(true, true);

}

void test_HYDROData_LandCoverMap::test_add_2_section_poly()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) Polyline_12 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_12->SetName( "Polyline_12" );

  Polyline_12->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_12->AddPoint( 0, gp_XY( -148.70, 145.68 ) );
  Polyline_12->AddPoint( 0, gp_XY( -81.71, 210.97 ) );
  Polyline_12->AddPoint( 0, gp_XY( -21.09, 122.79 ) );
  Polyline_12->AddPoint( 0, gp_XY( -106.30, 102.44 ) );
  Polyline_12->AddSection( "Section_2", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_12->AddPoint( 1, gp_XY( -7.09, 227.08 ) );
  Polyline_12->AddPoint( 1, gp_XY( 46.32, 228.78 ) );
  Polyline_12->AddPoint( 1, gp_XY( 69.64, 165.61 ) );
  Polyline_12->AddPoint( 1, gp_XY( 13.68, 156.28 ) );

  Polyline_12->Update();

  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  CPPUNIT_ASSERT_EQUAL( true, LCM->Add( Polyline_12, "" ) );

  TestViewer::show( LCM->GetShape(), AIS_Shaded, true, "lcm_poly_2_sections" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_split_2()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_PolylineXY) Polyline_1 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_1->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_1->AddPoint( 0, gp_XY( -264.84, 323.44 ) );
  Polyline_1->AddPoint( 0, gp_XY( 254.45, 301.19 ) );
  Polyline_1->AddPoint( 0, gp_XY( -291.54, -47.48 ) );
  Polyline_1->Update();

  Handle(HYDROData_PolylineXY) Polyline_6 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_6->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_6->AddPoint( 0, gp_XY( -155.15, 199.47 ) );
  Polyline_6->AddPoint( 0, gp_XY( -71.19, 289.12 ) );
  Polyline_6->AddPoint( 0, gp_XY( 22.01, 211.57 ) );
  Polyline_6->AddPoint( 0, gp_XY( -84.00, 134.73 ) );
  Polyline_6->Update();

  Handle(HYDROData_PolylineXY) Polyline_7 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_7->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_7->AddPoint( 0, gp_XY( -50.56, 232.20 ) );
  Polyline_7->AddPoint( 0, gp_XY( 40.51, 286.98 ) );
  Polyline_7->AddPoint( 0, gp_XY( 118.77, 256.39 ) );
  Polyline_7->AddPoint( 0, gp_XY( 72.52, 114.10 ) );
  Polyline_7->Update();

  Handle(HYDROData_PolylineXY) Polyline_8 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_8->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  Polyline_8->AddPoint( 0, gp_XY( -44.16, 190.93 ) );
  Polyline_8->AddPoint( 0, gp_XY( 9.91, 226.51 ) );
  Polyline_8->AddPoint( 0, gp_XY( 35.53, 175.99 ) );
  Polyline_8->AddPoint( 0, gp_XY( -9.30, 157.50 ) );
  Polyline_8->Update();

  Handle(HYDROData_PolylineXY) Polyline_9 =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_9->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, false );
  Polyline_9->AddPoint( 0, gp_XY( -103.92, 288.40 ) );
  Polyline_9->AddPoint( 0, gp_XY( -100.09, 285.22 ) );
  Polyline_9->AddPoint( 0, gp_XY( -96.10, 281.90 ) );
  Polyline_9->AddPoint( 0, gp_XY( -90.43, 277.19 ) );
  Polyline_9->AddPoint( 0, gp_XY( -53.41, 246.43 ) );
  Polyline_9->AddPoint( 0, gp_XY( 10.62, 245.01 ) );


  Polyline_9->Update();


  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  CPPUNIT_ASSERT_EQUAL( true, LCM->Add( Polyline_1, "" ) );
  CPPUNIT_ASSERT_EQUAL( true, LCM->Add( Polyline_6, "" ) );
  CPPUNIT_ASSERT_EQUAL( true, LCM->Add( Polyline_7, "" ) );

  CPPUNIT_ASSERT_EQUAL( true, LCM->Split( Polyline_9 ) );

  //Handle(HYDROGUI_LandCoverMapPrs) aPrs1 = new HYDROGUI_LandCoverMapPrs( LCM );
  TestViewer::show( LCM->GetShape(), AIS_Shaded, /*0,*/ true, "LCM_split_2a" );
  CPPUNIT_ASSERT_IMAGES

  CPPUNIT_ASSERT_EQUAL( true, LCM->Split( Polyline_8 ) );

  // Handle(HYDROGUI_LandCoverMapPrs) aPrs2 = new HYDROGUI_LandCoverMapPrs( LCM );
  TestViewer::show( LCM->GetShape(), AIS_Shaded, /*0,*/ true, "LCM_split_2b" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_export_telemac()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );

  QStringList types = aTable->GetTypes();
  //foreach( QString type, types )
  //  std::cout << type.toStdString() << std::endl;

  QString type7 = types[7];
  QString type8 = types[8];
  QString type9 = types[9];

  TopoDS_Face aLC1 = Face2d( QList<double>() << 12 << 19 << 82 << 9 << 126 << 53 << 107 << 80 << 29 << 75 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, type7 ) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 21 << 34 << 24 << 25 << 37   << 37 << 40  << 61 <<
                                              44 << 95 << 85 << 100 << 104 << 66 << 107 << 33 <<
                                             128 << 18 << 140 << 50 << 131 << 89 << 104 << 111 <<
                                              31 << 114 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, type8 ) );

  TopoDS_Face aLC3 = Face2d( QList<double>() << 4 << 54 << 1   << 47 << 51  << 45 <<
                                            127 << 42 << 145 << 43 << 148 << 60 << 90 << 65 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC3, type9 ) );

  QString aTmpFileName = "test.telemac";
  QString aTmpPath = TMP_DIR + "/" + aTmpFileName;
  QString messStat;
  CPPUNIT_ASSERT_EQUAL( true, aMap->ExportTelemac( aTmpPath, 1E-4, aTable, messStat ) );
  CPPUNIT_ASSERT_SCRIPTS_EQUAL( aTmpFileName, true, true, 0 );

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_copy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_LandCoverMap) aMap =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  aMap->SetName( "map_1" );

  TopoDS_Face aLC1 = Face2d( QList<double>() << 10 << 10 << 30 << 10 << 20 << 20 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC1, QString::fromUtf8("Forêts de conifères")) );

  TopoDS_Face aLC2 = Face2d( QList<double>() << 110 << 10 << 130 << 10 << 120 << 20 );
  CPPUNIT_ASSERT_EQUAL( true, aMap->LocalPartition( aLC2, QString::fromUtf8("Forêts de feuillus")) );

  Handle(HYDROData_LandCoverMap) aMap2 =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  aMap->CopyTo( aMap2, true );

  CPPUNIT_ASSERT_EQUAL( QString( "map_2" ), aMap2->GetName() );
  HYDROData_LandCoverMap::Explorer anIt( aMap2 );
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString::fromUtf8("Forêts de feuillus"), anIt.StricklerType() );
  CPPUNIT_ASSERT( anIt.Face().TShape()!=aLC1.TShape() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( true, anIt.More() );
  CPPUNIT_ASSERT_EQUAL( QString::fromUtf8("Forêts de conifères"), anIt.StricklerType() );
  CPPUNIT_ASSERT( anIt.Face().TShape()!=aLC2.TShape() );
  anIt.Next();
  CPPUNIT_ASSERT_EQUAL( false, anIt.More() );

  TopoDS_Shape aShape1 = aMap->GetShape();
  TopoDS_Shape aShape2 = aMap2->GetShape();
  gp_Trsf aTr;
  aTr.SetTranslation( gp_Vec( 0, 50, 0 ) );
  aShape2.Move( TopLoc_Location( aTr ) );
  TestViewer::show( aShape1, AIS_Shaded, /*0,*/ true, "LCM_copied" );
  TestViewer::show( aShape2, AIS_Shaded, /*0,*/ true, "" );
  CPPUNIT_ASSERT_IMAGES

  aDoc->Close();
}

void test_HYDROData_LandCoverMap::test_shp_clc_classification_perf()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  QString aFileName = REF_DATA_PATH + "/CLC_decoupe.shp";
  HYDROData_ShapeFile anImporter;
  QStringList PolygonList;
  TopTools_SequenceOfShape PolygonFaces;
  int aStat = anImporter.ImportPolygons(aDoc, aFileName, PolygonList, PolygonFaces);
  int Type = anImporter.GetShapeType();
  CPPUNIT_ASSERT(aStat == 1);
  CPPUNIT_ASSERT_EQUAL(5, Type);
  CPPUNIT_ASSERT_EQUAL(625, PolygonFaces.Length());

  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;

#ifdef NDEBUG
  int SI = 100;
  int EI = 110;
#else
  int SI = 100;
  int EI = 110;
#endif

  for ( int i = SI; i <= EI; i++ )
  {
    TopoDS_Shape aShape = PolygonFaces(i);
    if ( aShape.IsNull() )
      continue;
    aMapFace2ST.Add( TopoDS::Face( aShape ), "ST_" + QString::number(i) );
  }

  LCM->StoreLandCovers(aMapFace2ST);

  std::vector<gp_XY> pnts;
#ifdef NDEBUG
  int N = 1000; //1000*1000 points; uniform distribution for release mode
#else
  int N = 100;
#endif

  pnts.reserve(N);
  double x0 = 448646.91897505691;
  double x1 = 487420.3990381231;
  double y0 = 6373566.5122489957;
  double y1 = 6392203.4117361344;
  for (size_t i=0; i < N; i++)
  {
    for (size_t j=0; j < N; j++)
    {
      double px = x0 + (x1-x0)*((double)i/(double)N);
      double py = y0 + (y1-y0)*((double)j/(double)N);
      pnts.push_back(gp_XY(px,py));
    }
  }
  OSD_Timer aTimer;
  std::vector<std::set <QString> > TRes;
  aTimer.Start();
  LCM->ClassifyPoints(pnts, TRes);
  aTimer.Stop();
#ifdef NDEBUG
  std::cout << "Time: " << aTimer.ElapsedTime() << std::endl;
  CPPUNIT_ASSERT( aTimer.ElapsedTime() < 6.0);
  aTimer.Show();
#endif
}

void test_HYDROData_LandCoverMap::test_shp_clc_classification_check()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  QString aFileName = REF_DATA_PATH + "/CLC_decoupe.shp";
  HYDROData_ShapeFile anImporter;
  QStringList PolygonList;
  TopTools_SequenceOfShape PolygonFaces;
  int aStat = anImporter.ImportPolygons(aDoc, aFileName, PolygonList, PolygonFaces);
  CPPUNIT_ASSERT(aStat == 1);
  Handle(HYDROData_LandCoverMap) LCM = Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;

  aMapFace2ST.Add( TopoDS::Face( PolygonFaces(172) ), "Tissu urbain continu" );
  aMapFace2ST.Add( TopoDS::Face( PolygonFaces(179) ), "Aéroports" );
  aMapFace2ST.Add( TopoDS::Face( PolygonFaces(185) ), "Rizières" );
  aMapFace2ST.Add( TopoDS::Face( PolygonFaces(187) ), "Vignobles" );
  aMapFace2ST.Add( TopoDS::Face( PolygonFaces(190) ), "Oliveraies" );
  aMapFace2ST.Add( TopoDS::Face( PolygonFaces(196) ), "Estuaires" );

  LCM->StoreLandCovers(aMapFace2ST);

  TopoDS_Shape Sh = LCM->GetShape();

  std::vector<gp_XY> pnts;
  pnts.push_back(gp_XY(0,0));

  pnts.push_back(gp_XY(468380, 6382300));
  pnts.push_back(gp_XY(468380, 6382900));
  pnts.push_back(gp_XY(468380, 6383200));
  pnts.push_back(gp_XY(468250, 6384700));
  pnts.push_back(gp_XY(470350, 6384700));
  pnts.push_back(gp_XY(469279.642874048, 6385132.45048612 ));

  std::vector<std::set <QString> > TRes;
  LCM->ClassifyPoints(pnts, TRes);

  CPPUNIT_ASSERT (TRes[0].empty());
  CPPUNIT_ASSERT (TRes[1].empty());
  CPPUNIT_ASSERT (TRes[2].empty());
  CPPUNIT_ASSERT_EQUAL (*TRes[3].begin(), QString("Estuaires"));
  CPPUNIT_ASSERT_EQUAL (*TRes[4].begin(), QString("Oliveraies"));
  CPPUNIT_ASSERT_EQUAL (*TRes[5].begin(), QString("Vignobles"));

  //std::less comparator; so compare first and second elem safely
  CPPUNIT_ASSERT_EQUAL (*TRes[6].begin(), QString("Estuaires"));
  CPPUNIT_ASSERT_EQUAL (*(++TRes[6].begin()), QString("Tissu urbain continu"));

  ///
  Handle(HYDROData_StricklerTable) aTable = Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );
  std::vector<double> coeffs;
  LCM->ClassifyPoints(pnts, aTable, coeffs, 0.0, true);

  CPPUNIT_ASSERT_EQUAL (coeffs[0], 0.0);
  CPPUNIT_ASSERT_EQUAL (coeffs[1], 0.0);
  CPPUNIT_ASSERT_EQUAL (coeffs[2], 0.0);
  CPPUNIT_ASSERT_EQUAL (coeffs[3], 98.0);
  CPPUNIT_ASSERT_EQUAL (coeffs[4], 26.0);
  CPPUNIT_ASSERT_EQUAL (coeffs[5], 24.0);
  CPPUNIT_ASSERT_EQUAL (coeffs[6], 98.0);

}

void test_HYDROData_LandCoverMap::test_lcm_classification()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_StricklerTable) aTable =
    Handle(HYDROData_StricklerTable)::DownCast( aDoc->CreateObject( KIND_STRICKLER_TABLE ) );
  CPPUNIT_ASSERT_EQUAL( true, aTable->Import( DEF_STR_PATH ) );

  Handle(HYDROData_PolylineXY) ContourP = Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  ContourP->SetName( "Contour" );
  ContourP->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, true );
  ContourP->AddPoint( 0, gp_XY( 89.57, 81.63 ) );
  ContourP->AddPoint( 0, gp_XY( 479.59, 81.63 ) );
  ContourP->AddPoint( 0, gp_XY( 472.79, 671.20 ) );
  ContourP->AddPoint( 0, gp_XY( 69.16, 696.15 ) );
  ContourP->Update();

  Handle(HYDROData_PolylineXY) Poly_Sens_TrigoP =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Poly_Sens_TrigoP->SetName( "Poly_Sens_Trigo" );
  Poly_Sens_TrigoP->SetZLevel( 3 );
  Poly_Sens_TrigoP->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, 1 );
  Poly_Sens_TrigoP->AddPoint( 0, gp_XY( 130.46, 223.57 ) );
  Poly_Sens_TrigoP->AddPoint( 0, gp_XY( 252.16, 239.58 ) );
  Poly_Sens_TrigoP->AddPoint( 0, gp_XY( 240.95, 498.99 ) );
  Poly_Sens_TrigoP->AddPoint( 0, gp_XY( 119.26, 510.20 ) );
  Poly_Sens_TrigoP->AddPoint( 0, gp_XY( 141.67, 378.89 ) );
  Poly_Sens_TrigoP->Update();

  Handle(HYDROData_PolylineXY) Polyline_Sens_HoraireP =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );
  Polyline_Sens_HoraireP->SetName( "Polyline_Sens_Horaire" );
  Polyline_Sens_HoraireP->SetZLevel( 4 );
  Polyline_Sens_HoraireP->AddSection( "Section_1", HYDROData_PolylineXY::SECTION_POLYLINE, 1 );
  Polyline_Sens_HoraireP->AddPoint( 0, gp_XY( 313.01, 470.16 ) );
  Polyline_Sens_HoraireP->AddPoint( 0, gp_XY( 313.01, 627.09 ) );
  Polyline_Sens_HoraireP->AddPoint( 0, gp_XY( 426.70, 633.49 ) );
  Polyline_Sens_HoraireP->AddPoint( 0, gp_XY( 442.71, 398.11 ) );
  Polyline_Sens_HoraireP->Update();


  Handle(HYDROData_LandCoverMap) Land_cover_map_1 =
    Handle(HYDROData_LandCoverMap)::DownCast( aDoc->CreateObject( KIND_LAND_COVER_MAP ) );
  Land_cover_map_1->SetName( "Land cover map_1" );
  Land_cover_map_1->SetZLevel( 2 );
  CPPUNIT_ASSERT_EQUAL( true, Land_cover_map_1->Add( ContourP, "Vignobles" ) );
  CPPUNIT_ASSERT_EQUAL( true, Land_cover_map_1->Add( Poly_Sens_TrigoP, "Oliveraies" ) );
  CPPUNIT_ASSERT_EQUAL( true, Land_cover_map_1->Add( Polyline_Sens_HoraireP, "Estuaires" ) );

  Handle(HYDROGUI_LandCoverMapPrs) aPrs = new HYDROGUI_LandCoverMapPrs( Land_cover_map_1 );
  TopoDS_Shape aSh = Land_cover_map_1->GetShape();
  TestViewer::show( aPrs, AIS_Shaded, 0, true, "Land_cover_map_1" );

  std::vector<gp_XY> pnts;
  pnts.push_back(gp_XY(90, 90));
  pnts.push_back(gp_XY(300, 90));
  pnts.push_back(gp_XY(200, 350));
  pnts.push_back(gp_XY(400, 500));
  std::vector<double> theCoeffs;
  Land_cover_map_1->ClassifyPoints(pnts, aTable, theCoeffs, -1, true );
  CPPUNIT_ASSERT_EQUAL((int)theCoeffs.size(), 4);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(theCoeffs[0], 24, 0.0001);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(theCoeffs[1], 24, 0.0001);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(theCoeffs[2], 26, 0.0001);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(theCoeffs[3], 98, 0.0001);

  aDoc->Close();
}


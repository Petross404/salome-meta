// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <cppunit/extensions/HelperMacros.h>

class test_HYDROData_PolylineXY : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE( test_HYDROData_PolylineXY );
  CPPUNIT_TEST( test_polyline );
  CPPUNIT_TEST( test_copy );
  CPPUNIT_TEST( test_split_refs_624 );
  CPPUNIT_TEST( test_split_refs_627 );
  //CPPUNIT_TEST( test_presentation );
  CPPUNIT_TEST( test_extraction_immersible_zone );
  CPPUNIT_TEST( test_extraction_channel_refs_611 );
  CPPUNIT_TEST( test_custom_polylines );
  CPPUNIT_TEST( test_merge_refs_630 );
  CPPUNIT_TEST( test_split_straight_refs_634 );
  CPPUNIT_TEST( test_import_from_xyz );
  CPPUNIT_TEST( test_import_from_sx );
  CPPUNIT_TEST( test_import_from_sx_options );
  CPPUNIT_TEST( test_polyline_dbf_info_simple );
  CPPUNIT_TEST_SUITE_END();

private:

public:

  void setUp() {}

  void tearDown() {}

  // checks save/restore QImages information
  void test_polyline();

  // checks the image properties copy/paste
  void test_copy();

  void test_split_refs_624();
  void test_split_refs_627();

  void test_extraction_immersible_zone();
  void test_extraction_channel_refs_611();
  void test_presentation();
  void test_custom_polylines();
  void test_merge_refs_630();
  void test_split_straight_refs_634();
  void test_import_from_xyz();
  void test_import_from_sx();
  void test_import_from_sx_options();
  void test_polyline_dbf_info_simple();

};

CPPUNIT_TEST_SUITE_REGISTRATION(test_HYDROData_PolylineXY);
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION(test_HYDROData_PolylineXY, "HYDROData_PolylineXY");

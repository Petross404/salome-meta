// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_Entity.h>

#include <HYDROData_Document.h>
#include <HYDROData_Entity.h>

#include <QString>

void test_HYDROData_Entity::testName()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Entity) anObj = aDoc->CreateObject(KIND_IMAGE); // any object
  static const QString aName("test_name");
  anObj->SetName(aName);
  CPPUNIT_ASSERT_EQUAL(aName.toStdString(), anObj->GetName().toStdString());
  
  aDoc->Close();
}

void test_HYDROData_Entity::testRemove()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  
  Handle(HYDROData_Entity) anObj = aDoc->CreateObject(KIND_IMAGE); // any object
  CPPUNIT_ASSERT(!anObj->IsRemoved());
  anObj->Remove();
  CPPUNIT_ASSERT(anObj->IsRemoved());

  aDoc->Close();
}

void test_HYDROData_Entity::testCopy()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Entity) anObj = aDoc->CreateObject(KIND_IMAGE); // any object
  static const QString aName("test_name");
  anObj->SetName(aName);

  Handle(HYDROData_Entity) aCopy = aDoc->CreateObject(KIND_IMAGE); // object for copy
  CPPUNIT_ASSERT(aCopy->GetName().isEmpty());
  anObj->CopyTo(aCopy, false);

  // check the copied object has same name as original
  CPPUNIT_ASSERT_EQUAL(aName.toStdString(), aCopy->GetName().toStdString());

  aDoc->Close();
}

void test_HYDROData_Entity::testPythonNameChoice()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Entity) anObj = aDoc->CreateObject(KIND_IMAGE); // any object
  
  anObj->SetName("test");
  CPPUNIT_ASSERT_EQUAL( anObj->GetObjPyName().toStdString(), std::string( "test" ) );

  anObj->SetName("test_1");
  CPPUNIT_ASSERT_EQUAL( anObj->GetObjPyName().toStdString(), std::string( "test_1" ) );

  anObj->SetName("test 1");
  CPPUNIT_ASSERT_EQUAL( anObj->GetObjPyName().toStdString(), std::string( "test_1" ) );

  anObj->SetName("  test abc    hello     ");
  CPPUNIT_ASSERT_EQUAL( anObj->GetObjPyName().toStdString(), std::string( "__test_abc____hello_____" ) );

  anObj->SetName("123");
  CPPUNIT_ASSERT_EQUAL( anObj->GetObjPyName().toStdString(), std::string( "Image_123" ) );

  anObj->SetName("  123");
  CPPUNIT_ASSERT_EQUAL( anObj->GetObjPyName().toStdString(), std::string( "__123" ) );

  aDoc->Close();
}

void test_HYDROData_Entity::testTypes()
{
  for( int i=0; i<KIND_LAST; i++ )
  {
    CPPUNIT_ASSERT_EQUAL( false, HYDROData_Entity::Type( (ObjectKind)i ).isEmpty() );
    CPPUNIT_ASSERT_EQUAL( false, HYDROData_Entity::Type( (ObjectKind)i ).contains(' ') );
  }
}

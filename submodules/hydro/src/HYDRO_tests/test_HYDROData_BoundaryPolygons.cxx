// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_BoundaryPolygons.h>
#include <HYDROData_Document.h>
#include <HYDROData_CalculationCase.h>
#include <QTest>
#include <BRep_Builder.hxx>
#include <TopExp.hxx>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>
#include <HYDROData_ShapesGroup.h>
#include <HYDROData_BCPolygon.h>
#include <TopExp_Explorer.hxx>

extern QString REF_DATA_PATH;

void test_HYDROData_BoundaryPolygons::test_bp_1()
{  
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/test_boundary_polygon_calc_case_1.cbf";
  CPPUNIT_ASSERT_EQUAL( (int)DocError_OK, (int)HYDROData_Document::Load( fname.ToCString() ) );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();  
  //Calc case already contains already contains 4 boundary poltgons (3 cut, 1 include)
  //Update it to recalculate the shape groups
  Handle(HYDROData_CalculationCase) aCase = 
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->FindObjectByName( "Case_1" ) );
  CPPUNIT_ASSERT_EQUAL( false, aCase.IsNull() ); 
  aCase->Update();
  CPPUNIT_ASSERT_EQUAL(4, aCase->GetBoundaryPolygons().Size());
  HYDROData_SequenceOfObjects aSplitGroups = aCase->GetSplitGroups();
  CPPUNIT_ASSERT_EQUAL(4, aSplitGroups.Size());

  Handle(HYDROData_ShapesGroup) aGroup1;
  TopTools_MapOfShape EdgesFromBPGroups;
  for (int i=1; i<=4;i++)
  {
    Handle(HYDROData_ShapesGroup) aGroup = Handle(HYDROData_ShapesGroup)::DownCast( aSplitGroups(i) );    
    if (aGroup->GetName() == "Case_1_Immersible zone_1_Outer")
    {
      aGroup1 = aGroup;
    }
    else
    {
      TopTools_SequenceOfShape aSeqShapes1;
      aGroup->GetShapes( aSeqShapes1 );
      for (int j=1; j<=aSeqShapes1.Size();j++)
      {
        TopExp_Explorer exp(aSeqShapes1(j), TopAbs_EDGE);
        for (;exp.More();exp.Next())
          EdgesFromBPGroups.Add(exp.Current());
      }
    }
  }
  CPPUNIT_ASSERT( !aGroup1.IsNull() ); 

  TopTools_SequenceOfShape aSeqShapes;
  aGroup1->GetShapes( aSeqShapes );  
  CPPUNIT_ASSERT_EQUAL( 78, aSeqShapes.Size());
  //
  TopTools_MapOfShape EdgesFromOuter;

  for (int i=1;i<=aSeqShapes.Size();i++)
    EdgesFromOuter.Add(aSeqShapes(i));

  //check that EdgesFromBPGroups is a PART of Case_1_Immersible zone_1_Outer group
  int ext1 = EdgesFromBPGroups.Extent();
  EdgesFromOuter.Intersect(EdgesFromBPGroups);
  int ext2 = EdgesFromOuter.Extent();
  CPPUNIT_ASSERT_EQUAL(35, ext1);
  CPPUNIT_ASSERT_EQUAL(ext1, ext2);
  //
  BRep_Builder BB;
  TopTools_IndexedMapOfShape ME, MV;
  GProp_GProps G;
  double total_len = 0;
  for (int i=1; i<=aSeqShapes.Size();i++)
  {
    const TopoDS_Shape& aCSH = aSeqShapes(i);
    TopExp::MapShapes(aCSH, TopAbs_EDGE, ME);
    TopExp::MapShapes(aCSH, TopAbs_VERTEX, MV);
    BRepGProp::LinearProperties(aCSH, G);
    total_len += G.Mass();
  }
  CPPUNIT_ASSERT_EQUAL( 78, ME.Extent());
  CPPUNIT_ASSERT_EQUAL( 78, MV.Extent());
  CPPUNIT_ASSERT_DOUBLES_EQUAL(  22.074, total_len , 0.001);

  aDoc->Close();
}

void test_HYDROData_BoundaryPolygons::test_bp_2()
{
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/test_boundary_polygon_calc_case_2.cbf";
  CPPUNIT_ASSERT_EQUAL( (int)DocError_OK, (int)HYDROData_Document::Load( fname.ToCString() ) );
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  //add boundary polygons to case_1
  Handle(HYDROData_CalculationCase) aCase = 
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->FindObjectByName( "Case_1" ) );
  CPPUNIT_ASSERT_EQUAL( false, aCase.IsNull() ); 
  double len1=0, len2=0;
  {
    //check linear properties before applying of boundary polygons
    CPPUNIT_ASSERT_EQUAL(0, aCase->GetBoundaryPolygons().Size());
    HYDROData_SequenceOfObjects aSplitGroups = aCase->GetSplitGroups();
    Handle(HYDROData_ShapesGroup) aGroup = Handle(HYDROData_ShapesGroup)::DownCast( aSplitGroups.First() );
    std::string name = aGroup->GetName().toStdString();
    CPPUNIT_ASSERT_EQUAL( name, std::string("Case_1_Immersible zone_1_Outer") ); 
    TopTools_SequenceOfShape aSeqShapes;
    aGroup->GetShapes( aSeqShapes );  
    CPPUNIT_ASSERT_EQUAL( 36, aSeqShapes.Size());
    TopTools_IndexedMapOfShape ME, MV;
    GProp_GProps G;
    for (int i=1; i<=aSeqShapes.Size();i++)
    {
      const TopoDS_Shape& aCSH = aSeqShapes(i);
      BRepGProp::LinearProperties(aCSH, G);
      len1 += G.Mass();
    }
    CPPUNIT_ASSERT_DOUBLES_EQUAL(  22.074, len1 , 0.001);
  }

  //add boudnary polygons to case_1. (3 cut tools, 1 include tool)
  Handle(HYDROData_BCPolygon) aBP3 = Handle(HYDROData_BCPolygon)::DownCast( aDoc->FindObjectByName("BP_poly3"));
  aCase->AddBoundaryPolygon(aBP3);
  Handle(HYDROData_BCPolygon) aBP4 = Handle(HYDROData_BCPolygon)::DownCast( aDoc->FindObjectByName("BP_poly4"));
  aCase->AddBoundaryPolygon(aBP4);
  Handle(HYDROData_BCPolygon) aBP5 = Handle(HYDROData_BCPolygon)::DownCast( aDoc->FindObjectByName("BP_poly5"));
  aCase->AddBoundaryPolygon(aBP5);
  Handle(HYDROData_BCPolygon) aBP6 = Handle(HYDROData_BCPolygon)::DownCast( aDoc->FindObjectByName("BP_poly6"));
  aCase->AddBoundaryPolygon(aBP6);

  aCase->Update();
  {
    //check linear properties after applying of boundary polygons
    HYDROData_SequenceOfObjects aSplitGroups = aCase->GetSplitGroups();
    Handle(HYDROData_ShapesGroup) aGroup = Handle(HYDROData_ShapesGroup)::DownCast( aSplitGroups.First() );
    std::string name = aGroup->GetName().toStdString();
    CPPUNIT_ASSERT_EQUAL( name, std::string("Case_1_Immersible zone_1_Outer") ); 

    TopTools_SequenceOfShape aSeqShapes;
    aGroup->GetShapes( aSeqShapes );  
    CPPUNIT_ASSERT_EQUAL( 78, aSeqShapes.Size());
    BRep_Builder BB;
    TopTools_IndexedMapOfShape ME, MV;
    GProp_GProps G;

    for (int i=1; i<=aSeqShapes.Size();i++)
    {
      const TopoDS_Shape& aCSH = aSeqShapes(i);
      TopExp::MapShapes(aCSH, TopAbs_EDGE, ME);
      TopExp::MapShapes(aCSH, TopAbs_VERTEX, MV);
      BRepGProp::LinearProperties(aCSH, G);
      len2 += G.Mass();
    }
    CPPUNIT_ASSERT_EQUAL( 78, ME.Extent());
    CPPUNIT_ASSERT_EQUAL( 78, MV.Extent());
    //len1 == len2
    CPPUNIT_ASSERT_DOUBLES_EQUAL( 22.074, len2, 0.001);
  }

  aDoc->Close();
} 


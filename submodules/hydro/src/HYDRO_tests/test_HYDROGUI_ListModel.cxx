// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#undef HYDROGUI_EXPORTS

#include <test_HYDROGUI_ListModel.h>
#include <HYDROData_Document.h>
#include <HYDROGUI_ListModel.h>

Handle(HYDROData_Document) GetDocument()
{
  return HYDROData_Document::Document();
}

HYDROGUI_ListModel::Object2VisibleList CreateTestObjects( int theObjCount )
{
  HYDROGUI_ListModel::Object2VisibleList anObjects;

  for( int i=0; i<theObjCount; i++ )
  {
    Handle(HYDROData_Entity) anObj = GetDocument()->CreateObject( KIND_IMMERSIBLE_ZONE );

    std::string aName = " ";
    aName[0] = 'A' + i;
    anObj->SetName( QString::fromStdString( aName ) );

    bool isVisible = i%2==0;

    anObjects.append( HYDROGUI_ListModel::Object2Visible( anObj, isVisible ) );
  }
  return anObjects;
}

std::string test_HYDROGUI_ListModel::GetObjects( HYDROGUI_ListModel* theModel ) const
{
  std::string anObjects;
  for( int i=0, n=theModel->myObjects.size(); i<n; i++ )
  {
    std::string anObjName = theModel->myObjects[i].first->GetName().toStdString();
    if( theModel->isObjectVisible( i ) )
      anObjName = "*" + anObjName;
    if( i>0 )
      anObjects += ", ";
    anObjects += anObjName;
  }
  return anObjects;
}

/**
  Test move up algorithm.
*/
void test_HYDROGUI_ListModel::testMoveUp()
{
  HYDROGUI_ListModel* aModel = new HYDROGUI_ListModel();
  aModel->setObjects( CreateTestObjects( 6 ) );
  const HYDROGUI_ListModel::OpType anUp = HYDROGUI_ListModel::Up;

  // 0. Check the initial state
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 1. [ *A ] / ALL OBJECTS
  aModel->move( QList<int>() << 0, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 2. [ *A, B ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 3. [ *A, *C ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 2, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 4. [ *A, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 5, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 5. [ F ] X 6 times / ALL OBJECTS
  aModel->move( QList<int>() << 5, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, F, *E" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 4, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, F, D, *E" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 3, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, F, *C, D, *E" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 2, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, F, B, *C, D, *E" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 1, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *A, B, *C, D, *E" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 0, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *A, B, *C, D, *E" ), GetObjects( aModel ) );

  // 6. [ *A, B ] / ALL OBJECTS
  aModel->move( QList<int>() << 1 << 2, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, F, *C, D, *E" ), GetObjects( aModel ) );

  // 7. [ B, *C, *E ] / ALL OBJECTS
  aModel->move( QList<int>() << 1 << 3 << 5, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *A, *C, F, *E, D" ), GetObjects( aModel ) );

  // 8. [ *A, *C, F, *E, D ] / ALL OBJECTS
  aModel->move( QList<int>() << 1 << 2 << 3 << 4 << 5, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, *C, F, *E, D, B" ), GetObjects( aModel ) );

  // 9. [ *E ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 3, anUp, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, *E, *C, F, D, B" ), GetObjects( aModel ) );

  // 10. [ *E, *C ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 1 << 2, anUp, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, *C, *A, F, D, B" ), GetObjects( aModel ) );

  // 11. [ *A, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 2 << 3, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, *A, F, *C, D, B" ), GetObjects( aModel ) );

  // 12. [ *A, *C ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 1 << 3, anUp, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, *C, *E, F, D, B" ), GetObjects( aModel ) );

  // 13. [] / ALL OBJECTS
  aModel->move( QList<int>(), anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, *C, *E, F, D, B"), GetObjects( aModel ) );

  // 14. [ *A, *C, *E, F, D, B ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1 << 2 << 3 << 4 << 5, anUp, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, *C, *E, F, D, B"), GetObjects( aModel ) );

  delete aModel;
}

/**
  Test move on top algorithm.
*/
void test_HYDROGUI_ListModel::testMoveOnTop()
{
  HYDROGUI_ListModel* aModel = new HYDROGUI_ListModel();
  aModel->setObjects( CreateTestObjects( 6 ) );
  const HYDROGUI_ListModel::OpType aTop = HYDROGUI_ListModel::Top;

  // 0. Check the initial state
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 1. [ *A ] / ALL OBJECTS
  aModel->move( QList<int>() << 0, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 2. [ *A ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 0, aTop, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 3. [ F ] / ALL OBJECTS
  aModel->move( QList<int>() << 5, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *A, B, *C, D, *E" ), GetObjects( aModel ) );

  // 4. [ *E ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 5, aTop, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, F, *A, B, *C, D" ), GetObjects( aModel ) );

  // 5. [ *E, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, F, *A, B, *C, D" ), GetObjects( aModel ) );

  // 6. [ *E, *A ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 2, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, *A, F, B, *C, D" ), GetObjects( aModel ) );

  // 7. [ *A, F, *C ] / ALL OBJECTS
  aModel->move( QList<int>() << 1 << 2 << 4, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, F, *C, *E, B, D" ), GetObjects( aModel ) );

  // 8.  [ F, *C, *E, B, D ] / ALL OBJECTS
  aModel->move( QList<int>() << 1 << 2 << 3 << 4 << 5, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *C, *E, B, D, *A" ), GetObjects( aModel ) );

  // 9. [] / ALL OBJECTS
  aModel->move( QList<int>(), aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *C, *E, B, D, *A" ), GetObjects( aModel ) );

  // 10. [*F, *C, *E, B, D, *A] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1 << 2 << 3 << 4 << 5, aTop, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *C, *E, B, D, *A" ), GetObjects( aModel ) );

  delete aModel;
}

/**
  Test move down algorithm.
*/
void test_HYDROGUI_ListModel::testMoveDown()
{
  HYDROGUI_ListModel* aModel = new HYDROGUI_ListModel();
  aModel->setObjects( CreateTestObjects( 6 ) );
  const HYDROGUI_ListModel::OpType aDown = HYDROGUI_ListModel::Down;

  // 0. Check the initial state
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 1. [ F ] / ALL OBJECTS
  aModel->move( QList<int>() << 5, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 2. [ *E, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 4 << 5, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 3. [ D, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 3 << 5, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 4. [ *A, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 5, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 5. [ *A ] X 6 times / ALL OBJECTS
  aModel->move( QList<int>() << 0, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *A, *C, D, *E, F" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 1, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, *A, D, *E, F" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 2, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, D, *A, *E, F" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 3, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, D, *E, *A, F" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 4, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, D, *E, F, *A" ), GetObjects( aModel ) );
  aModel->move( QList<int>() << 5, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, D, *E, F, *A" ), GetObjects( aModel ) );

  // 6. [ *E, *F ] / ALL OBJECTS
  aModel->move( QList<int>() << 3 << 4, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, D, *A, *E, F" ), GetObjects( aModel ) );

  // 7. [ B, D, *E ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 2 << 4, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*C, B, *A, D, F, *E" ), GetObjects( aModel ) );

  // 8. [ *C, B, *A, D, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1 << 2 << 3 << 4, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, *C, B, *A, D, F" ), GetObjects( aModel ) );

  // 9. [ *C ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 1, aDown, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, B, *A, *C, D, F" ), GetObjects( aModel ) );

  // 10. [ *E, *A ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 0 << 2, aDown, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, *E, *A, D, F" ), GetObjects( aModel ) );

  // 11. [ *E, *A ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 2 << 3, aDown, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, *E, *A, D, F" ), GetObjects( aModel ) );

  // 12. [ *C, *E, *A ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 1 << 2 << 3, aDown, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, *E, *A, D, F" ), GetObjects( aModel ) );

  // 13. [] / ALL OBJECTS
  aModel->move( QList<int>(), aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, *E, *A, D, F"), GetObjects( aModel ) );

  // 14. [ B, *C, *E, *A, D, F ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1 << 2 << 3 << 4 << 5, aDown, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, *E, *A, D, F"), GetObjects( aModel ) );

  delete aModel;
}

/**
  Test move on bottom algorithm.
*/
void test_HYDROGUI_ListModel::testMoveOnBottom()
{
  HYDROGUI_ListModel* aModel = new HYDROGUI_ListModel();
  aModel->setObjects( CreateTestObjects( 6 ) );
  const HYDROGUI_ListModel::OpType aBottom = HYDROGUI_ListModel::Bottom;

  // 0. Check the initial state
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 1. [ F ] / ALL OBJECTS
  aModel->move( QList<int>() << 5, aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 2. [ F ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 5, aBottom, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, B, *C, D, *E, F" ), GetObjects( aModel ) );

  // 3. [ *A ] / ALL OBJECTS
  aModel->move( QList<int>() << 0, aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, *C, D, *E, F, *A" ), GetObjects( aModel ) );

  // 4. [ *C ] / VISIBLE OBJECTS
  aModel->move( QList<int>() << 1, aBottom, true );
  CPPUNIT_ASSERT_EQUAL( std::string( "B, D, *E, F, *A, *C" ), GetObjects( aModel ) );

  // 5. [ B, D ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1, aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, F, *A, *C, B, D" ), GetObjects( aModel ) );

  // 6. [ *C, *D ] / ALL OBJECTS
  aModel->move( QList<int>() << 3 << 5, aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, F, *A, B, *C, D" ), GetObjects( aModel ) );

  // 7. [ *E, *A, *C ] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 2 << 4, aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, B, D, *E, *A, *C" ), GetObjects( aModel ) );

  // 8.  [ B, D, *E, *A ] / ALL OBJECTS
  aModel->move( QList<int>() << 1 << 2 << 3 << 4 , aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *C, B, D, *E, *A" ), GetObjects( aModel ) );

  // 9. [] / ALL OBJECTS
  aModel->move( QList<int>(), aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *C, B, D, *E, *A" ), GetObjects( aModel ) );

  // 10. [F, *C, B, D, *E, *A] / ALL OBJECTS
  aModel->move( QList<int>() << 0 << 1 << 2 << 3 << 4 << 5, aBottom, false );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *C, B, D, *E, *A" ), GetObjects( aModel ) );

  delete aModel;
}

/**
  Test drag and drop algorithm.
*/
void test_HYDROGUI_ListModel::testDragAndDrop()
{
  HYDROGUI_ListModel* aModel = new HYDROGUI_ListModel();
  aModel->setObjects( CreateTestObjects( 8 ) );
  const HYDROGUI_ListModel::OpType aDnD = HYDROGUI_ListModel::DragAndDrop;

  // 0. Check the initial state
  std::string anInitialState = std::string( "*A, B, *C, D, *E, F, *G, H" );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 1. [] -> B ( i=1 )
  bool aRes = aModel->move( QList<int>(), aDnD, false, 1 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 2. ALL -> B ( i=1 )
  QList<int> anAll;
  anAll << 0 << 1 << 2 << 3 << 4 << 5 << 6 << 7;
  aRes = aModel->move( anAll, aDnD, false, 1 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 3. [D, *E, *G] -> D : drop item is among dragged items ( at the beginning )
  aRes = aModel->move( QList<int>() << 3 << 4 << 6, aDnD, false, 3 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 4. [D, *E, *G] -> *E : drop item is among dragged items ( in the middle )
  aRes = aModel->move( QList<int>() << 3 << 4 << 6, aDnD, false, 4 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 5. [D, *E, *G] -> *G : drop item is among dragged items ( at the end )
  aRes = aModel->move( QList<int>() << 3 << 4 << 6, aDnD, false, 6 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 6. [D, *E, *G] -> -1 : drop item index is out of range ( less than zero )
  aRes = aModel->move( QList<int>() << 3 << 4 << 6, aDnD, false, -1 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 7. [D, *E, *G] -> -1 : drop item index is out of range ( more than than list length )
  aRes = aModel->move( QList<int>() << 3 << 4 << 6, aDnD, false, 9 );
  CPPUNIT_ASSERT_EQUAL( false, aRes );
  CPPUNIT_ASSERT_EQUAL( anInitialState, GetObjects( aModel ) );

  // 8. [D, *E, *G] -> B ( i = 1 )
  aRes = aModel->move( QList<int>() << 3 << 4 << 6, aDnD, false, 1 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, D, *E, *G, B, *C, F, H" ), GetObjects( aModel ) );

  // 9. [*E, F] -> *G
  aRes = aModel->move( QList<int>() << 2 << 6, aDnD, false, 3 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, D, *E, F, *G, B, *C, H" ), GetObjects( aModel ) );

  // 10. [*E, F, *G] -> *A
  aRes = aModel->move( QList<int>() << 2 << 3 << 4, aDnD, false, 0 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, F, *G, *A, D, B, *C, H" ), GetObjects( aModel ) );

  // 11. [*G, D, *C, H] -> B
  aRes = aModel->move( QList<int>() << 2 << 4 << 6 << 7, aDnD, false, 5 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "*E, F, *A, *G, D, *C, H, B" ), GetObjects( aModel ) );

  // 12. [F, *A, *G, D, *C, H, B] -> *E
  QList<int> anAllWithoutFirst;
  anAllWithoutFirst << 1 << 2 << 3 << 4 << 5 << 6 << 7;
  aRes = aModel->move( anAllWithoutFirst, aDnD, false, 0 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *A, *G, D, *C, H, B, *E" ), GetObjects( aModel ) );

  // 13. [*A, *G] -> D : no changes
  aRes = aModel->move(  QList<int>() << 1 << 2, aDnD, false, 3 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *A, *G, D, *C, H, B, *E" ), GetObjects( aModel ) );

  // 14. [F, *G] -> D
  aRes = aModel->move(  QList<int>() << 0 << 2, aDnD, false, 3 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "*A, F, *G, D, *C, H, B, *E" ), GetObjects( aModel ) );

  // 15. [*A, *G, *C, H, *E] -> D
  aRes = aModel->move(  QList<int>() << 0 << 2 << 4 << 5 << 7, aDnD, false, 3 );
  CPPUNIT_ASSERT_EQUAL( true, aRes );
  CPPUNIT_ASSERT_EQUAL( std::string( "F, *A, *G, *C, H, *E, D, B" ), GetObjects( aModel ) );
}

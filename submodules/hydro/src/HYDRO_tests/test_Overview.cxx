// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_Overview.h>
#include <BRep_Builder.hxx>
#include <BRepTools.hxx>
#include <AIS_Shape.hxx>
#include <TestViewer.h>
#include <QString>
#include <QTest>
#include <QPainter>

#include <HYDROGUI_Overview.h>
#include <OCCViewer_ViewFrame.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewPort3d.h>
#include <V3d_View.hxx>

extern QString REF_DATA_PATH;
HYDROGUI_Overview* test_Overview::myOverview = 0;

#define CPPUNIT_ASSERT_OVERVIEW( theCase )                               \
  {                                                                      \
    QString aMessage;                                                    \
    QImage aDump = dumpViews();                                          \
    if( !TestViewer::AssertImages( aMessage, &aDump, theCase, false ) )  \
    {                                                                    \
      TestViewer::showColorScale( false );                               \
      std::string aMessageStl = aMessage.toStdString();                  \
      CPPUNIT_FAIL( aMessageStl.c_str() );                               \
    }                                                                    \
  } 

const int m = 5; //margins
const QColor aColor = Qt::blue;
const QBrush SOLID( aColor );
const int WIDTH = 2;
const QPen PSOLID( SOLID, WIDTH );
const QColor BACK = Qt::white;

QImage test_Overview::dumpViews()
{
  QImage aMain = TestViewer::viewWindow()->getView(OCCViewer_ViewFrame::MAIN_VIEW)->dumpView();
  QImage anOverview = myOverview->dump();

  if (SWAP_RGB)
    aMain = aMain.rgbSwapped();              //PATCH for image came from OCCT dump
  //anOverview = anOverview.rgbSwapped();  //overview dump already normalizes the image, the line is not necessary!!!

  int w1 = aMain.width();
  int w2 = anOverview.width();
  int h1 = aMain.height();
  int h2 = anOverview.height();
  int w = w1 + w2 + 2*WIDTH;
  int h = qMax( h1, h2 ) + 2*WIDTH;

  QPixmap pix( w, h );
  pix.fill( BACK );

  QPainter painter( &pix );
  painter.setPen( PSOLID );

  //static int q = 0;
  //q++;
  //anOverview.save( QString( "/tmp/hydro/overview_" ) + QString::number( q ) + ".png" );
  //aMain.save( QString( "/tmp/hydro/main_" ) + QString::number( q ) + ".png" );

  painter.drawImage( WIDTH, WIDTH, anOverview );
  painter.drawImage( w2+WIDTH, WIDTH, aMain );

  painter.drawRect( WIDTH, WIDTH, w2, h2 );
  painter.drawRect( w2+WIDTH, WIDTH, w1, h1 );

  QImage res = pix.toImage();
  //res.save( QString( "/tmp/hydro/result_" ) + QString::number( q ) + ".png" );
  
  return res;
}

extern int MAIN_W, MAIN_H;

void test_Overview::create()
{
  TestViewer::eraseAll( true, true );
  
  // Initialization of the empty viewer

  static bool isCreated = false;
  if( !isCreated )
  {
    myOverview = new HYDROGUI_Overview( "Test overview" );
    myOverview->show();

    // default mouse position
    QTest::mouseMove( TestViewer::viewWindow(), QPoint( 0, 0 ) );

    isCreated = true;
  }
  myOverview->setMainView( TestViewer::viewWindow() );
  //TestViewer::viewWindow()->setGeometry( 400, 100, MAIN_W, MAIN_H );
  TestViewer::viewWindow()->onTopView();
  myOverview->setGeometry( 100, 100, 200, 200 );
}

void test_Overview::showShape()
{
  // Show loaded shape in the viewer
  BRep_Builder B;
  TopoDS_Shape shape;
  std::string fname = (REF_DATA_PATH + "/test_zone.brep").toStdString();
  //std::cout << "Loading shape: " << fname << std::endl;
  BRepTools::Read( shape, fname.c_str(), B );
  TestViewer::show( shape, AIS_Shaded, true, 0x03399FF );
  
  qApp->processEvents();
  myOverview->setTopView(); //TODO: automatic fit all on show???*/
}

void fitAllWithRestore( OCCViewer_ViewPort3d* vp )
{
  double s = vp->getView()->Scale();
  double x0, y0, z0; 
  vp->getView()->Convert( 0, 0, x0, y0, z0 );
  TestViewer::viewWindow()->onFitAll();
  vp->getView()->SetScale( s );
  int xp, yp;
  vp->getView()->Convert( x0, y0, z0, xp, yp );
  vp->pan( -xp, -yp );
}




void test_Overview::test_default()
{
  create();
  //QTest::qWait( 50000 );
  CPPUNIT_ASSERT_OVERVIEW( "overview_empty" );
}


void test_Overview::test_presentation()
{
  create();
  showShape();
  
  //QTest::qWait( 20000 );
  CPPUNIT_ASSERT_OVERVIEW( "overview_prs" );
}

void test_Overview::test_actions_in_main()
{
  create();
  showShape();
  TestViewer::viewWindow()->onTopView();
  qApp->processEvents();

  OCCViewer_ViewWindow* aMain = TestViewer::viewWindow()->getView( OCCViewer_ViewFrame::MAIN_VIEW );
  OCCViewer_ViewPort3d* vp = aMain->getViewPort();

  // 1. selection in main view
  QTest::mouseMove( vp, QPoint(150,150) );
  TestViewer::context()->MoveTo(150, 150, vp->getView(), Standard_True );
  TestViewer::context()->Select( Standard_True );
  qApp->processEvents();
  //QTest::qWait( 50000 );
  //aMain->dumpView().save("/tmp/hydro/dump.png");
  //aMain->dumpView().rgbSwapped().save("/tmp/hydro/dump2.png");
  CPPUNIT_ASSERT_OVERVIEW( "overview_selection" );

  // 2. mouse wheel zoom
  QWheelEvent we( QPoint( 243, 316 ), 120*20, Qt::NoButton, Qt::NoModifier );
  qApp->sendEvent( vp, &we );
  qApp->processEvents();
  CPPUNIT_ASSERT_OVERVIEW( "overview_zoomed_1" );

  // 3. zoom via mouse
  const int d = 100;
  vp->zoom( 243, 316, 243+d, 316+d ); 
  CPPUNIT_ASSERT_OVERVIEW( "overview_zoomed_2" );

  // 4. panning via mouse
  vp->pan( 300, -250 ); 
  CPPUNIT_ASSERT_OVERVIEW( "overview_panned_1" );

  // 5. reverse zoom and rotation via mouse
  vp->getView()->AutoZFit();
  vp->getView()->ZFitAll();
  vp->getView()->Camera()->SetZRange( -10, 10 );
  vp->zoom( 243+d, 416+d, 243, 416 ); 
  vp->startRotation( 400, 400, OCCViewer_ViewWindow::BBCENTER, gp_Pnt() );
  vp->rotate( 200, 300, OCCViewer_ViewWindow::BBCENTER, gp_Pnt() );

  fitAllWithRestore( vp );
  // it is necessary to apply fit all to fit correct zmin/zmax 
  // and after that we restore the previous aspect
  CPPUNIT_ASSERT_OVERVIEW( "overview_rotated_1" );

  //QTest::qWait( 50000 );
}

void test_Overview::test_set_mainview_2_times()
{
  create();
  showShape();
  TestViewer::viewWindow()->onTopView();

  OCCViewer_ViewWindow* aMain = TestViewer::viewWindow()->getView( OCCViewer_ViewFrame::MAIN_VIEW );
  OCCViewer_ViewPort3d* vp = aMain->getViewPort();

  QTest::mouseMove( vp, QPoint(150,150) );
  TestViewer::context()->MoveTo(150, 150, vp->getView(), Standard_True );
  TestViewer::context()->Select( Standard_True );
  qApp->processEvents();
  
  //QTest::qWait( 20000 );
  CPPUNIT_ASSERT_OVERVIEW( "overview_selection" );

  myOverview->setMainView( TestViewer::viewWindow() );
  qApp->processEvents();

  CPPUNIT_ASSERT_OVERVIEW( "overview_selection_a" );
}

void test_Overview::test_actions_in_overview()
{
  create();
  showShape();
  TestViewer::viewWindow()->onTopView();

  OCCViewer_ViewWindow* aMain = TestViewer::viewWindow()->getView( OCCViewer_ViewFrame::MAIN_VIEW );
  OCCViewer_ViewPort3d* vp = aMain->getViewPort();

  QTest::mouseMove( vp, QPoint(150,150) );
  TestViewer::context()->MoveTo(150, 150, vp->getView(), Standard_True );
  TestViewer::context()->Select( Standard_True );
  qApp->processEvents();

  QWheelEvent we( QPoint( 243, 316 ), 120*20, Qt::NoButton, Qt::NoModifier );
  qApp->sendEvent( vp, &we );
  qApp->processEvents();
  CPPUNIT_ASSERT_OVERVIEW( "overview_zoomed_1" );

  QTest::mouseMove( myOverview->getViewPort(false), QPoint( 150, 50 ) );
  QTest::mouseClick( myOverview->getViewPort(false), Qt::LeftButton, Qt::KeyboardModifiers(), QPoint( 150, 50 ) );
  qApp->processEvents();

  CPPUNIT_ASSERT_OVERVIEW( "overview_drag" );

  //QTest::qWait( 50000 );
}

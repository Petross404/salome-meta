// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifdef WIN32
  #pragma warning( disable: 4251 )
#endif

#include <cppunit/extensions/HelperMacros.h>

class test_HYDROData_StricklerTable : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( test_HYDROData_StricklerTable );
  CPPUNIT_TEST( test_import );
  CPPUNIT_TEST( test_import_export_equivalence );
  CPPUNIT_TEST( test_type_by_attr );
  CPPUNIT_TEST( test_unique_attr_name );
  CPPUNIT_TEST( test_colors_sync );
  CPPUNIT_TEST( test_duplication_refs_613 );
  CPPUNIT_TEST( test_dump_python );
  CPPUNIT_TEST( test_add_row_in_gui_refs_717 );
  CPPUNIT_TEST_SUITE_END();

public:
  void test_import();
  void test_import_export_equivalence();
  void test_type_by_attr();
  void test_unique_attr_name();
  void test_colors_sync();
  void test_duplication_refs_613();
  void test_dump_python();
  void test_add_row_in_gui_refs_717();
};

CPPUNIT_TEST_SUITE_REGISTRATION( test_HYDROData_StricklerTable );
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION( test_HYDROData_StricklerTable, "HYDROData_StricklerTable" );

#ifdef WIN32
  #pragma warning( default: 4251 )
#endif

﻿// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_CalcCase.h>
#include <HYDROData_CalculationCase.h>
#include <HYDROData_Document.h>
#include <HYDROData_Tool.h>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS.hxx>
#include <TestViewer.h>
#include <TestShape.h>
#include <TopTools_ListOfShape.hxx>
#include <AIS_DisplayMode.hxx>
#include <QString>
#include <QColor>
#include <BRep_Builder.hxx>
#include <BRepTools.hxx>
#include <HYDROData_CompleteCalcCase.h>
#include <QSet>
#include <HYDROData_Region.h>
#include <TopExp.hxx>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>

extern QString REF_DATA_PATH;

static TopoDS_Shape GetCalcCaseShape(Handle(HYDROData_CalculationCase) theCalcCase)
{
  BRep_Builder B;
  TopoDS_Compound cmp;
  B.MakeCompound(cmp);
  HYDROData_SequenceOfObjects aRegions = theCalcCase->GetRegions();
  for ( int i = 1; i <= aRegions.Size(); i++ )
  {
    Handle(HYDROData_Region) aRegion = Handle(HYDROData_Region)::DownCast( aRegions(i) );
    if ( !aRegion.IsNull() )
    {
      HYDROData_SequenceOfObjects aZones = aRegion->GetZones();
      for ( int j = 1; j <= aZones.Size(); j++ )
      {
        Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( aZones(j) );
        TopoDS_Shape aSh = aZone->GetShape();
        B.Add(cmp, aSh);
      }
    }
  }
  return cmp;
}

void test_HYDROData_CalcCase::test_add_int_wires()
{
  {
    TopoDS_Shape out;
    TopTools_ListOfShape Wires;

    BRep_Builder B;
    TopoDS_Shape InF;
    TopoDS_Shape InP;
    BRepTools::Read(InP, (REF_DATA_PATH + "/p1.brep").toStdString().c_str(), B);
    BRepTools::Read(InF, (REF_DATA_PATH + "/r2.brep").toStdString().c_str(), B);
    Wires.Append(InP);

    TopTools_ListOfShape OutSh;
    TopTools_IndexedDataMapOfShapeShape ls;
    HYDROData_SplitToZonesTool::CutByEdges(InF, Wires, OutSh, false, &ls, NULL);

    CPPUNIT_ASSERT_EQUAL(2, OutSh.Extent());
    TopoDS_Compound cmp;
    B.MakeCompound(cmp);
    B.Add(cmp, OutSh.First());
    B.Add(cmp, OutSh.Last());
    TestViewer::show( cmp, AIS_Shaded, true, "cc_int_w_1" );
    //CPPUNIT_ASSERT_IMAGES
  }

  {
    TopoDS_Shape out;
    TopTools_ListOfShape Wires;

    BRep_Builder B;
    TopoDS_Shape InF;
    TopoDS_Shape InP;
    BRepTools::Read(InP, (REF_DATA_PATH + "/p2.brep").toStdString().c_str(), B);
    BRepTools::Read(InF, (REF_DATA_PATH + "/r2.brep").toStdString().c_str(), B);
    Wires.Append(InP);

    TopTools_ListOfShape OutSh;
    TopTools_IndexedDataMapOfShapeShape ls;
    HYDROData_SplitToZonesTool::CutByEdges(InF, Wires, OutSh, false, &ls, NULL);
    CPPUNIT_ASSERT_EQUAL(1, OutSh.Extent());
    TestViewer::show( OutSh.First(), AIS_WireFrame, true, "cc_int_w_2" );
    //CPPUNIT_ASSERT_IMAGES
  }

  {
    TopoDS_Shape out;
    TopTools_ListOfShape Wires;

    BRep_Builder B;
    TopoDS_Shape InF;
    TopoDS_Shape InP;
    BRepTools::Read(InP, (REF_DATA_PATH + "/p3.brep").toStdString().c_str(), B);
    BRepTools::Read(InF, (REF_DATA_PATH + "/r2.brep").toStdString().c_str(), B);
    Wires.Append(InP);

    TopTools_ListOfShape OutSh;
    TopTools_IndexedDataMapOfShapeShape ls;
    HYDROData_SplitToZonesTool::CutByEdges(InF, Wires, OutSh, false, &ls, NULL);
    CPPUNIT_ASSERT_EQUAL(1, OutSh.Extent());
    TestViewer::show( OutSh.First(), AIS_WireFrame, true, "cc_int_w_3", 1, 1 );
    CPPUNIT_ASSERT_IMAGES
  }

}


void test_HYDROData_CalcCase::test_complete_1()
{  
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/study_cc_1.cbf";
  CPPUNIT_ASSERT_EQUAL( (int)DocError_OK, (int)HYDROData_Document::Load( fname.ToCString() ) );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();  

  Handle(HYDROData_CalculationCase) aCase = 
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->FindObjectByName( "Case_1" ) );
  CPPUNIT_ASSERT_EQUAL( false, aCase.IsNull() ); 
  {
    //before complet eeop
    TopoDS_Shape cmp = GetCalcCaseShape(aCase);
    TopTools_IndexedMapOfShape M;
    TopExp::MapShapes(cmp, M);
    CPPUNIT_ASSERT_EQUAL(29, M.Extent());
    GProp_GProps G;
    BRepGProp::SurfaceProperties(cmp, G);
    CPPUNIT_ASSERT_DOUBLES_EQUAL( 179876.15713670343, G.Mass(), 0.001 );
  }

  NCollection_Sequence<Handle(HYDROData_Entity)> theNewObjects;
  Handle(HYDROData_Entity) aNewZone = aDoc->FindObjectByName( "Immersible zone_4" );
  Handle(HYDROData_Entity) aNewPoly = aDoc->FindObjectByName( "Polyline_5__BB" );
  theNewObjects.Append(aNewZone);
  theNewObjects.Append(aNewPoly);


  bool is_int = false;
  NCollection_Sequence<Handle(HYDROData_Region)> theNewRegions;
  QSet<QString> newRegionEntries;
  HYDROData_CompleteCalcCase::AddObjects(aDoc, aCase, theNewObjects, true, is_int, theNewRegions);  

  /// check the result
  CPPUNIT_ASSERT_EQUAL(false, is_int);

  HYDROData_SequenceOfObjects aRegions = aCase->GetRegions();
  CPPUNIT_ASSERT_EQUAL(4, aRegions.Size());

  CPPUNIT_ASSERT_EQUAL(QString("OREG1"), aRegions(1)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("OREGMIN"), aRegions(2)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("OREG2"), aRegions(3)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("Immersible zone_4_reg"), aRegions(4)->GetName());

  HYDROData_SequenceOfObjects aZonesReg1 = Handle(HYDROData_Region)::DownCast( aRegions(1) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(3, aZonesReg1.Size());

  HYDROData_SequenceOfObjects aZonesReg2 = Handle(HYDROData_Region)::DownCast( aRegions(2) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(1, aZonesReg2.Size());

  HYDROData_SequenceOfObjects aZonesReg3 = Handle(HYDROData_Region)::DownCast( aRegions(3) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(3, aZonesReg3.Size());

  HYDROData_SequenceOfObjects aZonesReg4 = Handle(HYDROData_Region)::DownCast( aRegions(4) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(5, aZonesReg4.Size());
  
  {
    //REG 1; "OREG1"
    std::set<std::string> aZonesReg1Names;
    for ( int j = 1; j <= 3; j++ )
    {
      Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( aZonesReg1(j) );
      aZonesReg1Names.insert( aZone->GetName().toStdString());
    }

    std::set<std::string> aZonesReg1NamesExpect;
    aZonesReg1NamesExpect.insert("Case_1_Zone_2");
    aZonesReg1NamesExpect.insert("Case_1_Zone_1_1");
    aZonesReg1NamesExpect.insert("Case_1_Zone_1_2");
    CPPUNIT_ASSERT(aZonesReg1NamesExpect == aZonesReg1Names);
  }
  {
    TopoDS_Shape cmp = GetCalcCaseShape(aCase);
    TopTools_IndexedMapOfShape M;
    TopExp::MapShapes(cmp, M);
    CPPUNIT_ASSERT_EQUAL(72, M.Extent());
    GProp_GProps G;
    BRepGProp::SurfaceProperties(cmp, G);
    CPPUNIT_ASSERT_DOUBLES_EQUAL( 196013.5, G.Mass(), 0.1 );
    TestViewer::show( cmp, AIS_Shaded, true, "cc_complete_1" );
    CPPUNIT_ASSERT_IMAGES
  }

  aDoc->Close();
}

void test_HYDROData_CalcCase::test_complete_2()
{  
  TCollection_AsciiString fname = REF_DATA_PATH.toLatin1().data();
  fname += "/study_cc_2.cbf";
  CPPUNIT_ASSERT_EQUAL( (int)DocError_OK, (int)HYDROData_Document::Load( fname.ToCString() ) );

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();  

  Handle(HYDROData_CalculationCase) aCase = 
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->FindObjectByName( "Case_1" ) );
 
  NCollection_Sequence<Handle(HYDROData_Entity)> theNewObjects;
  Handle(HYDROData_Entity) aNewZone1 = aDoc->FindObjectByName( "Immersible zone_4" );
  Handle(HYDROData_Entity) aNewZone2 = aDoc->FindObjectByName( "Immersible zone_5" );
  Handle(HYDROData_Entity) aNewZone3 = aDoc->FindObjectByName( "Immersible zone_6" );
  Handle(HYDROData_Entity) aNewPoly1 = aDoc->FindObjectByName( "Polyline___BB" );
  Handle(HYDROData_Entity) aNewPoly2 = aDoc->FindObjectByName( "Polyline_1" );
  Handle(HYDROData_Entity) aNewPoly3 = aDoc->FindObjectByName( "Polyline_2" );
  Handle(HYDROData_Entity) aNewPoly4 = aDoc->FindObjectByName( "Polyline_3" );
  Handle(HYDROData_Entity) aNewPoly5 = aDoc->FindObjectByName( "Polyline_4" );
  Handle(HYDROData_Entity) aNewPoly6 = aDoc->FindObjectByName( "Polyline_5" );
  Handle(HYDROData_Entity) aNewPoly7 = aDoc->FindObjectByName( "Polyline_6" );
  Handle(HYDROData_Entity) aNewPoly8 = aDoc->FindObjectByName( "Polyline_7" );
  Handle(HYDROData_Entity) aNewPoly9 = aDoc->FindObjectByName( "Polyline_8" );

  theNewObjects.Append(aNewZone1);
  theNewObjects.Append(aNewZone2);
  theNewObjects.Append(aNewZone3);
  theNewObjects.Append(aNewPoly1);
  theNewObjects.Append(aNewPoly2);
  theNewObjects.Append(aNewPoly3);
  theNewObjects.Append(aNewPoly4);
  theNewObjects.Append(aNewPoly5);
  theNewObjects.Append(aNewPoly6);
  theNewObjects.Append(aNewPoly7);
  theNewObjects.Append(aNewPoly8);
  theNewObjects.Append(aNewPoly9);

  bool is_int = false;
  NCollection_Sequence<Handle(HYDROData_Region)> theNewRegions;
  QSet<QString> newRegionEntries;
  HYDROData_CompleteCalcCase::AddObjects(aDoc, aCase, theNewObjects, true, is_int, theNewRegions);  

  /// check the result
  CPPUNIT_ASSERT_EQUAL(true, is_int);

  HYDROData_SequenceOfObjects aRegions = aCase->GetRegions();
  CPPUNIT_ASSERT_EQUAL(6, aRegions.Size());

  CPPUNIT_ASSERT_EQUAL(QString("OREG1"), aRegions(1)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("OREGMIN"), aRegions(2)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("OREG2"), aRegions(3)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("Immersible zone_4_reg"), aRegions(4)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("Immersible zone_5_reg"), aRegions(5)->GetName());
  CPPUNIT_ASSERT_EQUAL(QString("Immersible zone_6_reg"), aRegions(6)->GetName());

  HYDROData_SequenceOfObjects aZonesReg1 = Handle(HYDROData_Region)::DownCast( aRegions(1) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(3, aZonesReg1.Size());
  HYDROData_SequenceOfObjects aZonesReg2 = Handle(HYDROData_Region)::DownCast( aRegions(2) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(2, aZonesReg2.Size());
  HYDROData_SequenceOfObjects aZonesReg3 = Handle(HYDROData_Region)::DownCast( aRegions(3) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(3, aZonesReg3.Size());
  HYDROData_SequenceOfObjects aZonesReg4 = Handle(HYDROData_Region)::DownCast( aRegions(4) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(11, aZonesReg4.Size());
  HYDROData_SequenceOfObjects aZonesReg5 = Handle(HYDROData_Region)::DownCast( aRegions(5) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(6, aZonesReg5.Size());
  HYDROData_SequenceOfObjects aZonesReg6 = Handle(HYDROData_Region)::DownCast( aRegions(6) )->GetZones();
  CPPUNIT_ASSERT_EQUAL(5, aZonesReg6.Size());

  {
    //REG 4; imm.zone4
    //first priority; shouldn't change the area after complete (surf.mass)
    double m=0;
    BRep_Builder B; TopoDS_Compound ccc; B.MakeCompound(ccc);
    for ( int j = 1; j <= 11; j++ )
    {
      Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( aZonesReg4(j) );
      TopoDS_Shape sh = aZone->GetShape();
      B.Add(ccc, sh);
      GProp_GProps G;
      BRepGProp::SurfaceProperties(sh, G);
      m+=G.Mass();
    }
    GProp_GProps G1;
    Handle(HYDROData_Object) aNewZone1c = Handle(HYDROData_Object)::DownCast( aNewZone1 );
    BRepGProp::SurfaceProperties(aNewZone1c->GetTopShape(), G1);

    double diff = fabs(G1.Mass()-m); //there is still a difference after calling of general fuse on complicated cases
    CPPUNIT_ASSERT(diff < 20.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(23387.2, G1.Mass(), 0.1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(23370.1, m, 0.1);    
  }
  {
    TopoDS_Shape cmp = GetCalcCaseShape(aCase);
    TopTools_IndexedMapOfShape M;
    TopExp::MapShapes(cmp, M);
    CPPUNIT_ASSERT_EQUAL(197, M.Extent());
    GProp_GProps G;
    BRepGProp::SurfaceProperties(cmp, G);
    CPPUNIT_ASSERT_DOUBLES_EQUAL( 225164.3, G.Mass(), 0.1 );
    TestViewer::show( cmp, AIS_Shaded, true, "cc_complete_2" );
    CPPUNIT_ASSERT_IMAGES
  }
  aDoc->Close();
}



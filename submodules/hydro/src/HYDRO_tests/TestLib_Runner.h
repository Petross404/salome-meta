// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#pragma once

#include <cppunit/TestRunner.h>
#include <list>

#ifdef WIN32
  #pragma warning( disable: 4251 )
  #pragma warning( disable: 4275 )
#endif

/**
  \class TestLib_Runner
  \brief Implementation of the custom runner allowing start a subset of tests
*/
class TestLib_Runner : public CPPUNIT_NS::TestRunner
{
public:
  void Add( const std::string& theSubstr );
  bool Load( const std::string& theConfigFile );
  void Run( CPPUNIT_NS::TestResult& theController );

protected:
  void GetTests( CPPUNIT_NS::Test* theRoot, std::list<std::string>& theAllowedTests ) const;
  bool IsAllowed( const std::string& theTestName ) const;

private:
  std::list<std::string> mySubStrings; ///< the list of substring patterns 
};

#ifdef WIN32
  #pragma warning( default: 4251 )
  #pragma warning( default: 4275 )
#endif


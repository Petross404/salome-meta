﻿// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_Tool.h>
#include <HYDROData_Tool.h>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS.hxx>
#include <TestViewer.h>
#include <TestShape.h>
#include <TopTools_ListOfShape.hxx>
#include <AIS_DisplayMode.hxx>
#include <QString>
#include <QColor>
#include <BRep_Builder.hxx>
#include <BRepTools.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TopoDS_Iterator.hxx>

extern QString REF_DATA_PATH;

void test_HYDROData_Tool::test_rebuild_cmp()
{
  QString RefPath = REF_DATA_PATH;
  BRep_Builder bb;
  {
    TopoDS_Shape in, out;

    BRepTools::Read(in, (RefPath + "/f_cmp.brep").toStdString().c_str(), bb);
    out = HYDROData_Tool::RebuildCmp(in);

    {
      TopTools_IndexedMapOfShape dummy;
      TopExp::MapShapes(out, TopAbs_COMPOUND, dummy);
      CPPUNIT_ASSERT_EQUAL(1, dummy.Extent());

      dummy.Clear();
      TopExp::MapShapes(out, TopAbs_SHELL, dummy);
      CPPUNIT_ASSERT_EQUAL(3, dummy.Extent());

      dummy.Clear();
      TopExp::MapShapes(out, TopAbs_FACE, dummy);
      CPPUNIT_ASSERT_EQUAL(14, dummy.Extent());

      std::multiset<int> nbF, ndFref;
      ndFref.insert(1); //one face
      ndFref.insert(4); // one shell (contains 4 faces)
      ndFref.insert(4); // one shell (contains 4 faces)
      ndFref.insert(5); // one shell (contains 5 faces)
      TopoDS_Iterator itS(out);
      for (;itS.More();itS.Next())
      {
        const TopoDS_Shape& CSH = itS.Value();
        dummy.Clear();
        TopExp::MapShapes(CSH, TopAbs_FACE, dummy);
        nbF.insert(dummy.Extent());
      }

      CPPUNIT_ASSERT(ndFref == nbF); 

    }

    TestViewer::show( out, AIS_Shaded, true, "rebuild_cmp_out" );
    CPPUNIT_ASSERT_IMAGES
  }

}




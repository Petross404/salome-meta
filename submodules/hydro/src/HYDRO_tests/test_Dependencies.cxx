// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_Dependencies.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_CalculationCase.h>
#include <HYDROData_Channel.h>
#include <HYDROData_Document.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_PolylineXY.h>

#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>

void test_Dependencies::test_dependencies_broad_cast()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_Bathymetry) aBath =
    Handle(HYDROData_Bathymetry)::DownCast( aDoc->CreateObject( KIND_BATHYMETRY ) );

  Handle(HYDROData_PolylineXY) aPoly =
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );

  Handle(HYDROData_Polyline3D) aPoly3d =
    Handle(HYDROData_Polyline3D)::DownCast( aDoc->CreateObject( KIND_POLYLINE ) );

  Handle(HYDROData_ImmersibleZone) aZone =
    Handle(HYDROData_ImmersibleZone)::DownCast( aDoc->CreateObject( KIND_IMMERSIBLE_ZONE ) );

  Handle(HYDROData_Channel) aChannel =
    Handle(HYDROData_Channel)::DownCast( aDoc->CreateObject( KIND_CHANNEL ) );

  Handle(HYDROData_CalculationCase) aCase =
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->CreateObject( KIND_CALCULATION ) );

  aZone->SetPolyline( aPoly );
  aZone->SetAltitudeObject( aBath );
  aZone->Update();

  aPoly3d->SetPolylineXY( aPoly );
  aPoly3d->SetAltitudeObject( aBath );
  aPoly3d->Update();

  TopoDS_Edge anEdge = BRepBuilderAPI_MakeEdge( gp_Pnt(), gp_Pnt( 1, 0, 0 ) ).Edge();
  TopoDS_Wire aWire = BRepBuilderAPI_MakeWire( anEdge ).Wire();
  aPoly3d->SetTopShape( aWire );
  aPoly3d->SetShape3D( aWire );
  CPPUNIT_ASSERT_EQUAL( true, aChannel->SetGuideLine( aPoly3d ) );
  aChannel->Update();

  aCase->AddGeometryObject( aZone );
  aCase->AddGeometryObject( aChannel );
  aCase->ClearChanged();

  // 1. Change only Z values of the bathymetry
  aBath->SetAltitudesInverted( true );

  CPPUNIT_ASSERT_EQUAL( false, aZone->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );
  CPPUNIT_ASSERT_EQUAL( false, aPoly3d->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );
  CPPUNIT_ASSERT_EQUAL( true, aPoly3d->IsMustBeUpdated( HYDROData_Entity::Geom_Z ) );
  CPPUNIT_ASSERT_EQUAL( true, aPoly3d->IsMustBeUpdated( HYDROData_Entity::Geom_3d ) );
  CPPUNIT_ASSERT_EQUAL( false, aChannel->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );
  CPPUNIT_ASSERT_EQUAL( true, aChannel->IsMustBeUpdated( HYDROData_Entity::Geom_Z ) );
  CPPUNIT_ASSERT_EQUAL( true, aChannel->IsMustBeUpdated( HYDROData_Entity::Geom_3d ) );
  CPPUNIT_ASSERT_EQUAL( false, aCase->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );

  aPoly3d->ClearChanged();
  aChannel->ClearChanged();

  // 2. "Change" the polyline 2d
  aPoly->Changed( HYDROData_Entity::Geom_2d );

  CPPUNIT_ASSERT_EQUAL( true, aZone->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );
  CPPUNIT_ASSERT_EQUAL( true, aPoly3d->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );
  CPPUNIT_ASSERT_EQUAL( false, aPoly3d->IsMustBeUpdated( HYDROData_Entity::Geom_Z ) );
  CPPUNIT_ASSERT_EQUAL( true, aPoly3d->IsMustBeUpdated( HYDROData_Entity::Geom_3d ) );
  CPPUNIT_ASSERT_EQUAL( true, aChannel->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );
  CPPUNIT_ASSERT_EQUAL( false, aChannel->IsMustBeUpdated( HYDROData_Entity::Geom_Z ) );
  CPPUNIT_ASSERT_EQUAL( true, aChannel->IsMustBeUpdated( HYDROData_Entity::Geom_3d ) );
  CPPUNIT_ASSERT_EQUAL( true, aCase->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) );

  aDoc->Close();
}

void test_Dependencies::test_calc_case_ref_objects()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  Handle(HYDROData_ImmersibleZone) aZone =
    Handle(HYDROData_ImmersibleZone)::DownCast( aDoc->CreateObject( KIND_IMMERSIBLE_ZONE ) );

  Handle(HYDROData_CalculationCase) aCase =
    Handle(HYDROData_CalculationCase)::DownCast( aDoc->CreateObject( KIND_CALCULATION ) );

  aCase->AddGeometryObject( aZone );
  CPPUNIT_ASSERT_EQUAL( 1, aCase->GetAllReferenceObjects().Size() );
  CPPUNIT_ASSERT_EQUAL( aZone->Label(), aCase->GetAllReferenceObjects().First()->Label() );

  aDoc->Close();
}

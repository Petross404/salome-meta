// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#pragma once

#include <QList>

class TopoDS_Edge;
class TopoDS_Wire;
class TopoDS_Face;
class gp_Pnt;

TopoDS_Edge Edge2d( const QList<double>& theXYList, bool isClosed = false );
TopoDS_Wire Wire2d( const QList<double>& theXYList, bool isClosed = false );

TopoDS_Edge Edge3d( const QList<double>& theXYZList, bool isClosed = false );
TopoDS_Wire Wire3d( const QList<double>& theXYZList, bool isClosed = false );


TopoDS_Wire WireCirc( const gp_Pnt& theCenter, double theRadius );

TopoDS_Face Face2d( const QList<double>& theXYList );
TopoDS_Face Face3d( const QList<double>& theXYZList );

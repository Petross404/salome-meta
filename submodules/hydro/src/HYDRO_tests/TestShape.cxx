// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <TestShape.h>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>
#include <TColgp_HArray1OfPnt.hxx>
#include <GeomAPI_Interpolate.hxx>
#include <gp_Circ.hxx>

TopoDS_Edge Edge2d( const QList<double>& theXYList, bool isClosed )
{
  int n = theXYList.size()/2;
  Handle(TColgp_HArray1OfPnt) aPointsArray = new TColgp_HArray1OfPnt( 1, n );
  for( int i=1; i<=n; i++ )
  {
    double x = theXYList[2*i-2];
    double y = theXYList[2*i-1];
    gp_Pnt aPnt( x, y, 0 );
    aPointsArray->SetValue( i, aPnt );
  }
  GeomAPI_Interpolate anInterpolator( aPointsArray, isClosed, 1E-3 );
  anInterpolator.Perform();
  bool aResult = anInterpolator.IsDone() == Standard_True;
  if( aResult )
  {
    Handle( Geom_BSplineCurve ) aCurve = anInterpolator.Curve();
    return BRepBuilderAPI_MakeEdge( aCurve ).Edge();
  }
  else
    return TopoDS_Edge();
}

TopoDS_Wire Wire2d( const QList<double>& theXYList, bool isClosed )
{
  return BRepBuilderAPI_MakeWire( Edge2d( theXYList, isClosed ) ).Wire();
}

TopoDS_Edge Edge3d( const QList<double>& theXYZList, bool isClosed )
{
  int n = theXYZList.size()/3;
  Handle(TColgp_HArray1OfPnt) aPointsArray = new TColgp_HArray1OfPnt( 1, n );
  for( int i=1; i<=n; i++ )
  {
    double x = theXYZList[3*i-3];
    double y = theXYZList[3*i-2];
    double z = theXYZList[3*i-1];
    gp_Pnt aPnt( x, y, z );
    aPointsArray->SetValue( i, aPnt );
  }
  GeomAPI_Interpolate anInterpolator( aPointsArray, isClosed, 1E-3 );
  anInterpolator.Perform();
  bool aResult = anInterpolator.IsDone() == Standard_True;
  if( aResult )
  {
    Handle( Geom_BSplineCurve ) aCurve = anInterpolator.Curve();
    return BRepBuilderAPI_MakeEdge( aCurve ).Edge();
  }
  else
    return TopoDS_Edge();
}

TopoDS_Wire Wire3d( const QList<double>& theXYZList, bool isClosed )
{
  return BRepBuilderAPI_MakeWire( Edge3d( theXYZList, isClosed ) ).Wire();
}

TopoDS_Face Face2d( const QList<double>& theXYList )
{
  return BRepBuilderAPI_MakeFace( Wire2d( theXYList, true ), Standard_True ).Face();
}

TopoDS_Face Face3d( const QList<double>& theXYZList )
{
  return BRepBuilderAPI_MakeFace( Wire3d( theXYZList, true ), Standard_True ).Face();
}

TopoDS_Wire WireCirc( const gp_Pnt& theCenter, double theRadius )
{
  gp_Circ aCircle( gp_Ax2( theCenter, gp_Dir( 0, 0, 1 ) ), theRadius );
  TopoDS_Edge anEdge = BRepBuilderAPI_MakeEdge( aCircle ).Edge();
  return BRepBuilderAPI_MakeWire( anEdge ).Wire();
}

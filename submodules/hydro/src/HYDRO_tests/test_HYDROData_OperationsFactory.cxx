// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_OperationsFactory.h>

#include <HYDROData_Document.h>
#include <HYDROData_Image.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_OperationsFactory.h>

#include <ImageComposer_CropOperator.h>

#include <gp_XY.hxx>

#include <QPainter>

void test_HYDROData_OperationsFactory::testCreate()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  
  HYDROData_OperationsFactory* aFactory = HYDROData_OperationsFactory::Factory();
  CPPUNIT_ASSERT(aFactory);
  Handle(HYDROData_Image) anImage = aFactory->CreateImage(aDoc, NULL);
  CPPUNIT_ASSERT(!anImage.IsNull());
  CPPUNIT_ASSERT(anImage->Image().isNull());
  
  aDoc->Close();
}

static QImage TestImage() {
  QImage aPic(50, 40, QImage::Format_RGB32);
  aPic.fill(Qt::white);
  QPainter aPainter(&aPic);
  aPainter.drawEllipse(6, 7, 38, 30);
  aPainter.drawLine(0, 40, 10, 0);
  aPainter.drawLine(10, 0, 25, 35);
  aPainter.drawLine(25, 35, 40, 0);
  aPainter.drawLine(40, 0, 50, 40);
  return aPic;
}

void test_HYDROData_OperationsFactory::testCrop()
{
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  HYDROData_OperationsFactory* aFactory = HYDROData_OperationsFactory::Factory();
  
  // prepare the original image and crop-path
  Handle(HYDROData_Image) anOriImage = 
    Handle(HYDROData_Image)::DownCast( aDoc->CreateObject( KIND_IMAGE ) );

  QImage aTestImage = TestImage();
  anOriImage->SetImage( aTestImage );

  Handle(HYDROData_PolylineXY) aCropPolyline = 
    Handle(HYDROData_PolylineXY)::DownCast( aDoc->CreateObject( KIND_POLYLINEXY ) );

  aCropPolyline->AddSection( "", HYDROData_PolylineXY::SECTION_POLYLINE, true );

  aCropPolyline->AddPoint( 0, HYDROData_PolylineXY::Point( 25, 0  ) );
  aCropPolyline->AddPoint( 0, HYDROData_PolylineXY::Point( 0,  20 ) );
  aCropPolyline->AddPoint( 0, HYDROData_PolylineXY::Point( 25, 40 ) );
  aCropPolyline->AddPoint( 0, HYDROData_PolylineXY::Point( 50, 20 ) );

  // prepare Composer Operation
  ImageComposer_Operator* aCropOp = 
    aFactory->Operator( ImageComposer_CropOperator::Type() );
  
  CPPUNIT_ASSERT( aCropOp );

  aCropOp->setArgs( Qt::red );

  // create crop - image 
  Handle(HYDROData_Image) aCropImage = aFactory->CreateImage( aDoc, aCropOp );
  CPPUNIT_ASSERT( !aCropImage.IsNull() );

  aCropImage->AppendReference( anOriImage );
  aCropImage->AppendReference( aCropPolyline );
  aCropImage->Update();

  // check crop operation was performed
  CPPUNIT_ASSERT( !aCropImage->Image().isNull() );
  CPPUNIT_ASSERT( aCropImage->Image() != aTestImage );
  
  aDoc->Close();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <TestLib_Listener.h>
#include <cppunit/Test.h>
#include <cppunit/TestFailure.h>
#include <cppunit/SourceLine.h>
#include <iostream>
#include <QStringList>

#ifdef WIN32
  #include <windows.h>
#endif

#ifdef WIN32
/**
  Convert the given time structure to milliseconds number
  @param theTime the time structure to convert
  @return the calculated number of milliseconds
*/
INT64 GetMilliseconds( const FILETIME& theTime )
{
  SYSTEMTIME aTime;
  FileTimeToSystemTime( &theTime, &aTime );
  INT64 aRes = 0;
  aRes += aTime.wHour * ( 3600 * 1000 );
  aRes += aTime.wMinute * ( 60 * 1000 );
  aRes += aTime.wSecond * ( 1000 );
  aRes += aTime.wMilliseconds;
  return aRes;
}
#endif

/**
  Calculate the complete execution time for the current thread 
  @return the calculated time as the number of milliseconds
*/
INT64 GetTimeForCurrentThreadInMs()
{
#ifdef WIN32
  FILETIME aCreationTime, anExitTime, aKernelTime, aUserTime;
  GetThreadTimes( GetCurrentThread(), &aCreationTime, &anExitTime, &aKernelTime, &aUserTime );
  INT64 aKernelMs = GetMilliseconds( aKernelTime );
  INT64 aUserMs = GetMilliseconds( aUserTime );
  return aKernelMs + aUserMs;
#else
  struct timespec aTime;
  clock_gettime( CLOCK_MONOTONIC, &aTime );
  INT64 aSec = aTime.tv_sec;
  aSec *= 1000;
  aSec += aTime.tv_nsec / 1000000;
  return aSec;
#endif
}

/**
  \brief Constructor
*/
TestLib_Listener::TestLib_Listener()
: myStart( 0 ), myComplete( 0 ), myNbTests( 0 ), myNbSuites( 0 )
{
}

/**
  \brief Destructor
*/
TestLib_Listener::~TestLib_Listener()
{
}

/**
  \brief Clear the internal state of the listener
*/
void TestLib_Listener::Clear()
{
  myStart = 0;
  myComplete = 0;
}

/**
  Get complete time of all tests execution in milliseconds
  @return complete time in milliseconds
*/
INT64 TestLib_Listener::GetCompleteTimeInMS() const
{
  return myComplete;
}

/**
  Handler for test start
  @param theTest the started tests
*/
void TestLib_Listener::startTest( CppUnit::Test* theTest )
{
  std::string aName;
  if( theTest )
    aName = theTest->getName();

  std::cout << aName << "...";
  myStart = GetTimeForCurrentThreadInMs();
}

/**
  Handler for test finish
  @param theTest the finished tests
*/
void TestLib_Listener::endTest( CppUnit::Test* theTest )
{
  INT64 aCurTime = GetTimeForCurrentThreadInMs();
  INT64 anExecTimeMS = aCurTime - myStart;
  myComplete += anExecTimeMS;
  std::cout << " " << anExecTimeMS << " ms" << std::endl;

  myNbTests++;
}

int TestLib_Listener::GetNbTests() const
{
  return myNbTests;
}

int TestLib_Listener::GetNbSuites() const
{
  return myNbSuites;
}

/**
  Handler for test suite start
  @param theTest the started test suite
*/
void TestLib_Listener::startSuite( CppUnit::Test* theTest )
{
}

/**
  Handler for test suite finish
  @param theTest the finished test suite
*/
void TestLib_Listener::endSuite( CppUnit::Test* theTest )
{
  myNbSuites++;
}

/**
  Handler for test failure
  @param theFailure the failure information
*/
void TestLib_Listener::addFailure( const CppUnit::TestFailure& theFailure )
{
  std::string failedTest = theFailure.failedTest()->getName();
  CppUnit::SourceLine failedLocation = theFailure.sourceLine();

  QString aFile = QString::fromStdString( failedLocation.fileName() );
  aFile.replace( '\\', '/' );
  QStringList parts = aFile.split( '/' );
  aFile = parts.last();
  std::string cFile = aFile.toStdString();

  int aLine = failedLocation.lineNumber();

  static char aBuf[1024];
  sprintf( aBuf, "Failed %s: %s : %i", failedTest.c_str(), cFile.c_str(), aLine );

  myFailures.push_back( aBuf );
}

void TestLib_Listener::DumpFailures()
{
  int n = (int)myFailures.size();
  printf( "FAILED TESTS: %i\n", n );
  for( int i=0; i<n; i++ )
    printf( "  %s\n", myFailures[i].c_str() );
}

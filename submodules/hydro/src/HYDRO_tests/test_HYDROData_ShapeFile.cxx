// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <test_HYDROData_ShapeFile.h>
#include <HYDROData_ShapeFile.h>
#include <QStringList>
#include <vector>

extern QString REF_DATA_PATH;

bool test_HYDROData_ShapeFile::compare_two_files(const QString& File1, const QString& File2)
{
  FILE *fp1, *fp2;
  fp1 = fopen(File1.toStdString().c_str(), "r");
  fp2 = fopen(File2.toStdString().c_str(), "r");
  CPPUNIT_ASSERT(fp1);
  CPPUNIT_ASSERT(fp2);
  char ch1, ch2;
  while (((ch1 = fgetc(fp1)) != EOF) &&((ch2 = fgetc(fp2)) != EOF))
  {
    if (!ch1 == ch2)
    {
      fclose(fp1);
      fclose(fp2);
      return false;
    }
  }

  fclose(fp1);
  fclose(fp2);
  return true;
}

void test_HYDROData_ShapeFile::test_openDbf()
{
  QString aDBFPath = REF_DATA_PATH + "/cyprus_natural.dbf";
  HYDROData_ShapeFile aSHPFile;
  bool Stat = aSHPFile.DBF_OpenDBF(aDBFPath);
  CPPUNIT_ASSERT_EQUAL( true, Stat );  
  aSHPFile.DBF_CloseDBF();
}

void test_HYDROData_ShapeFile::test_NbFieldsDbf()
{
  QString aDBFPath = REF_DATA_PATH + "/cyprus_natural.dbf";
  HYDROData_ShapeFile aSHPFile;
  CPPUNIT_ASSERT_EQUAL(true, aSHPFile.DBF_OpenDBF(aDBFPath));
  int NbF = aSHPFile.DBF_GetNbFields();
  CPPUNIT_ASSERT_EQUAL( 2, NbF );  
  aSHPFile.DBF_CloseDBF();
}

void test_HYDROData_ShapeFile::test_FieldListDbf()
{
  QString aDBFPath = REF_DATA_PATH + "/cyprus_natural.dbf";
  HYDROData_ShapeFile aSHPFile;
  CPPUNIT_ASSERT_EQUAL(true, aSHPFile.DBF_OpenDBF(aDBFPath));

  QStringList FL = aSHPFile.DBF_GetFieldList();
  CPPUNIT_ASSERT_EQUAL(true, "NAME" == FL[0]);
  CPPUNIT_ASSERT_EQUAL(true, "TYPE" == FL[1]);
  aSHPFile.DBF_CloseDBF();
}

void test_HYDROData_ShapeFile::test_FieldTypeListDbf()
{
  QString aDBFPath = REF_DATA_PATH + "/cyprus_natural.dbf";
  HYDROData_ShapeFile aSHPFile;
  CPPUNIT_ASSERT_EQUAL(true, aSHPFile.DBF_OpenDBF(aDBFPath));

  std::vector<HYDROData_ShapeFile::DBF_FieldType> FTVect;
  aSHPFile.DBF_GetFieldTypeList(FTVect);
  CPPUNIT_ASSERT_EQUAL(HYDROData_ShapeFile::DBF_FieldType_String, FTVect[0]);
  CPPUNIT_ASSERT_EQUAL(HYDROData_ShapeFile::DBF_FieldType_String, FTVect[1]);
  aSHPFile.DBF_CloseDBF();
}

void test_HYDROData_ShapeFile::test_NbRecordsDbf()
{
  QString aDBFPath = REF_DATA_PATH + "/cyprus_natural.dbf";
  HYDROData_ShapeFile aSHPFile;
  CPPUNIT_ASSERT_EQUAL(true, aSHPFile.DBF_OpenDBF(aDBFPath));
  int NbR = aSHPFile.DBF_GetNbRecords();
  CPPUNIT_ASSERT_EQUAL( 268, NbR );  
  aSHPFile.DBF_CloseDBF();
}

void test_HYDROData_ShapeFile::test_GetAttrListIndex()
{
  { //cyprus_natural
    QString aDBFPath = REF_DATA_PATH + "/cyprus_natural.dbf";
    HYDROData_ShapeFile aSHPFile;
    CPPUNIT_ASSERT_EQUAL(true, aSHPFile.DBF_OpenDBF(aDBFPath));
    std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
    aSHPFile.DBF_GetAttributeList(0, theAttrV ); //get all records take from the first field

    //Check Values

    //Non-null attr
    CPPUNIT_ASSERT_EQUAL( 268, (int)theAttrV.size() ); 
    CPPUNIT_ASSERT_EQUAL( true, std::string("Ovgos Dam") == theAttrV[22].myRawValue);
    CPPUNIT_ASSERT_EQUAL( true, QString("Ovgos Dam") == theAttrV[22].myStrVal);
    CPPUNIT_ASSERT_EQUAL( false, theAttrV[22].myIsNull);
    CPPUNIT_ASSERT_EQUAL( HYDROData_ShapeFile::DBF_FieldType_String, theAttrV[22].myFieldType);

    //Null attr
    CPPUNIT_ASSERT_EQUAL( true, theAttrV[35].myIsNull);
    CPPUNIT_ASSERT_EQUAL( true, theAttrV[35].myRawValue.empty());
    CPPUNIT_ASSERT_EQUAL( HYDROData_ShapeFile::DBF_FieldType_String, theAttrV[35].myFieldType);

    theAttrV.clear();
    aSHPFile.DBF_GetAttributeList(1, theAttrV ); //second field
    CPPUNIT_ASSERT_EQUAL( 268, (int)theAttrV.size() ); 
    CPPUNIT_ASSERT_EQUAL( true, std::string("water") == theAttrV[3].myRawValue);
    CPPUNIT_ASSERT_EQUAL( true, QString("water") == theAttrV[3].myStrVal);
    CPPUNIT_ASSERT_EQUAL( false, theAttrV[3].myIsNull);
    CPPUNIT_ASSERT_EQUAL( HYDROData_ShapeFile::DBF_FieldType_String, theAttrV[3].myFieldType);

    aSHPFile.DBF_CloseDBF();
  }
  { //CLC06
    QString aDBFPath = REF_DATA_PATH + "/CLC06.dbf";
    HYDROData_ShapeFile aSHPFile;
    CPPUNIT_ASSERT_EQUAL (true, aSHPFile.DBF_OpenDBF(aDBFPath));
    std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;

    aSHPFile.DBF_GetAttributeList(0, theAttrV ); 
    CPPUNIT_ASSERT_EQUAL( 3269, (int)theAttrV.size() ); 
    CPPUNIT_ASSERT_EQUAL( true, std::string("FR-184045") == theAttrV[2].myRawValue);
    CPPUNIT_ASSERT_EQUAL( true, QString("FR-184045") == theAttrV[2].myStrVal);
    CPPUNIT_ASSERT_EQUAL( false, theAttrV[2].myIsNull);
    CPPUNIT_ASSERT_EQUAL( HYDROData_ShapeFile::DBF_FieldType_String, theAttrV[2].myFieldType);
    //
    theAttrV.clear();
    aSHPFile.DBF_GetAttributeList(2, theAttrV ); 
    CPPUNIT_ASSERT_EQUAL( 3269, (int)theAttrV.size() ); 
    CPPUNIT_ASSERT_EQUAL( true, std::string("6621.1654324936000000000000000000") == theAttrV[2].myRawValue);
    CPPUNIT_ASSERT_EQUAL( 6621.1654324935998375, theAttrV[2].myDoubleVal);
    CPPUNIT_ASSERT_EQUAL( false, theAttrV[2].myIsNull);
    CPPUNIT_ASSERT_EQUAL( HYDROData_ShapeFile::DBF_FieldType_Double, theAttrV[2].myFieldType);

    aSHPFile.DBF_CloseDBF();
  }
}


void test_HYDROData_ShapeFile::test_DbfWrite()
{
  {
    HYDROData_ShapeFile aSHPFile; 
    QString tempFN = REF_DATA_PATH + "/temp_dbf1.dbf";
    QString refFN = REF_DATA_PATH + "/ref_dbf1.dbf";
    std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
    HYDROData_ShapeFile::DBF_AttrValue theAttr1;
    theAttr1.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
    theAttr1.myIsNull = false;
    theAttr1.myRawValue = "test_value1";
    theAttr1.myStrVal = "test_value1";
    theAttrV.push_back(theAttr1);
    HYDROData_ShapeFile::DBF_AttrValue theAttr2;
    theAttr2.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
    theAttr2.myIsNull = false;
    theAttr2.myRawValue = "test_value2";
    theAttr2.myStrVal = "test_value2";
    theAttrV.push_back(theAttr2);
    //
    CPPUNIT_ASSERT(aSHPFile.DBF_WriteFieldAndValues(tempFN, "test_field_name", HYDROData_ShapeFile::DBF_FieldType_String, theAttrV, true));

    CPPUNIT_ASSERT(compare_two_files(tempFN, refFN));
    CPPUNIT_ASSERT_EQUAL( 0, remove (tempFN.toStdString().c_str()));
  }

  {
    HYDROData_ShapeFile aSHPFile; 
    QString tempFN = REF_DATA_PATH + "/temp_dbf2.dbf";
    QString refFN = REF_DATA_PATH + "/ref_dbf2.dbf";
    std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
    HYDROData_ShapeFile::DBF_AttrValue theAttr1;
    theAttr1.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
    theAttr1.myIsNull = false;
    theAttr1.myRawValue = "test_value1";
    theAttr1.myStrVal = "test_value1";
    theAttrV.push_back(theAttr1);

    HYDROData_ShapeFile::DBF_AttrValue theAttr2;
    theAttr2.myIsNull = false;
    theAttr2.myRawValue = "test_value_K";
    theAttr2.myStrVal = "test_value_K";
    theAttr2.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
    for (int ii = 0; ii < 100; ii++)
      theAttrV.push_back(theAttr2);

    HYDROData_ShapeFile::DBF_AttrValue theAttr3;
    theAttr3.myIsNull = true;
    theAttr3.myFieldType = HYDROData_ShapeFile::DBF_FieldType_String;
    for (int ii = 0; ii < 100; ii++)
      theAttrV.push_back(theAttr3);
    //
    CPPUNIT_ASSERT(aSHPFile.DBF_WriteFieldAndValues(tempFN, "test_field_name", HYDROData_ShapeFile::DBF_FieldType_String, theAttrV, true));

    CPPUNIT_ASSERT(compare_two_files(tempFN, refFN));
    CPPUNIT_ASSERT_EQUAL( 0, remove (tempFN.toStdString().c_str()));
  }

  {
    HYDROData_ShapeFile aSHPFile; 
    QString tempFN = REF_DATA_PATH + "/temp_dbf3.dbf";
    QString refFN = REF_DATA_PATH + "/ref_dbf3.dbf";
    std::vector<HYDROData_ShapeFile::DBF_AttrValue> theAttrV;
    HYDROData_ShapeFile::DBF_AttrValue theAttr1;
    theAttr1.myFieldType = HYDROData_ShapeFile::DBF_FieldType_Integer;
    theAttr1.myIsNull = false;

    for (int ii = 0; ii < 100; ii++)
    {
      theAttr1.myIntVal = ii;
      theAttrV.push_back(theAttr1);
    }

    //
    CPPUNIT_ASSERT(aSHPFile.DBF_WriteFieldAndValues(tempFN, "test_field_name", HYDROData_ShapeFile::DBF_FieldType_Integer, theAttrV, false));

    CPPUNIT_ASSERT(compare_two_files(tempFN, refFN));
    CPPUNIT_ASSERT_EQUAL( 0, remove (tempFN.toStdString().c_str()));
  }

}


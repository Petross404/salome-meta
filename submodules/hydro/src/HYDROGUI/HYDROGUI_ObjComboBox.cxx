// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ObjComboBox.h"

#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Module.h"

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>

#include <QTimer>
#include <QLabel>
#include <QLayout>
#include <QComboBox>

#include <HYDROData_Iterator.h>
#include <HYDROData_Document.h>

HYDROGUI_ObjComboBox::HYDROGUI_ObjComboBox( HYDROGUI_Module* theModule, const QString& theTitle, const ObjectKind& theType, QWidget* theParent )
    : QWidget( theParent ),
    myType( theType ),
    myModule( theModule ),
    myFilter( 0 )
{
    QBoxLayout* base = new QHBoxLayout( this );
    base->setMargin( 0 );

    if ( !theTitle.isEmpty() )
        base->addWidget( new QLabel( theTitle, this ) );
    base->addWidget( myObject = new QComboBox( this ) );

    myObject->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred ) );

    // Connect signals and slots
    connect( myObject, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onIndexChanged( int ) ) );
    connect( selectionMgr(), SIGNAL( selectionChanged() ), this, SLOT( onSelectionChanged() ) );
}

HYDROGUI_ObjComboBox::~HYDROGUI_ObjComboBox()
{
}

HYDROGUI_Module* HYDROGUI_ObjComboBox::module() const
{
    return myModule;
}

ObjectKind HYDROGUI_ObjComboBox::objectType() const
{
    return myType;
}

HYDROGUI_ObjComboBoxFilter* HYDROGUI_ObjComboBox::objectFilter() const
{
    return myFilter;
}

void HYDROGUI_ObjComboBox::setObjectFilter( HYDROGUI_ObjComboBoxFilter* filter )
{
    myFilter = filter;
}

void HYDROGUI_ObjComboBox::reset()
{
    bool block = myObject->signalsBlocked();
    myObject->blockSignals( true );

    myObject->clear();
    myObjects.Clear();
    myObject->addItems( objectNames( myObjects ) );
    myObject->setCurrentIndex( -1 );

    myObject->blockSignals( block );

    onIndexChanged( myObject->currentIndex() );
    updateSelection();
}

void HYDROGUI_ObjComboBox::setSelectedObject( const QString& theName )
{
    int aNewIdx = myObject->findText( theName );
    if ( aNewIdx != myObject->currentIndex() )
        myObject->setCurrentIndex( aNewIdx );
}

QString HYDROGUI_ObjComboBox::selectedObject() const
{
    return myObject->currentText();
}

SUIT_SelectionMgr* HYDROGUI_ObjComboBox::selectionMgr() const
{
    SUIT_SelectionMgr* aSelMgr = 0;
    if ( module() )
    {
        LightApp_Application* app = module()->getApp();
        if ( app )
            aSelMgr = app->selectionMgr();
    }
    return aSelMgr;
}

QStringList HYDROGUI_ObjComboBox::objectNames( HYDROData_SequenceOfObjects& theObjects ) const
{
    QStringList aNames;
    for ( HYDROData_Iterator it( HYDROData_Document::Document(), objectType() ); it.More(); it.Next() )
    {
        if ( !objectFilter() || objectFilter()->isOk( it.Current() ) )
        {
          theObjects.Append( it.Current() );
            aNames.append( it.Current()->GetName() );
        }
    }
    return aNames;
}

void HYDROGUI_ObjComboBox::onIndexChanged( int idx )
{
    if ( idx < 0 )
        return;

    emit objectSelected( myObject->itemText( idx ) );
}

void HYDROGUI_ObjComboBox::onSelectionChanged()
{
    if ( !objectFilter() || objectFilter()->isActive( this ) )
        QTimer::singleShot( 0, this, SLOT( updateSelection() ) );
}

void HYDROGUI_ObjComboBox::updateSelection()
{
    int idx = -1;
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::GetSelectedObject( module() );
    if ( !anObject.IsNull() )
        idx = myObject->findText( anObject->GetName() );

    if ( idx >= 0 && myObject->currentIndex() != idx )
    {
        myObject->setCurrentIndex( idx );
        emit objectSelected( myObject->itemText( idx ) );
    }
}

Handle( HYDROData_Entity ) HYDROGUI_ObjComboBox::GetObject() const
{
  int anIndex = myObject->currentIndex();
  if( anIndex>=0 && anIndex<myObjects.Length() )
    return myObjects.Value( myObjects.Lower() + anIndex );
  else
    return Handle( HYDROData_Entity )();
}


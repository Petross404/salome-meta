// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_RecognizeContoursOp.h"

#include "HYDROGUI_RecognizeContoursDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_OCCSelector.h"
#include "HYDROGUI_ZLayers.h"

#include <HYDROData_Document.h>
#include <HYDROData_GeomTool.h>
#include <HYDROData_ShapesTool.h>
#include <HYDROData_PolylineXY.h>

#include <GeometryGUI.h>
#include <GeometryGUI_Operations.h>
#include <GEOM_Constants.h>
#include <GEOMBase.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <SalomeApp_Study.h>

#include <LightApp_Application.h>
#include <LightApp_DataOwner.h>
#include <LightApp_Displayer.h>
#include <LightApp_SelectionMgr.h>

#include <SUIT_Desktop.h>
#include <SUIT_ViewManager.h>

#include <BRepBuilderAPI_GTransform.hxx>
#include <gp_GTrsf.hxx>

#include <QDialog>
#include <QDir>
#include <QTemporaryFile>

/**
  Constructor.
*/
HYDROGUI_RecognizeContoursOp::HYDROGUI_RecognizeContoursOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ),
  myGEOMOpName( "" )
{
  setName( tr( "CONTOURS_RECOGNITION" ) );
}

/**
  Destructor.
*/
HYDROGUI_RecognizeContoursOp::~HYDROGUI_RecognizeContoursOp()
{
  cleanup();
}

/**
*/
void HYDROGUI_RecognizeContoursOp::startOperation()
{
  HYDROGUI_Operation::startOperation();
  
  // Set preview view manager
  if ( !getPreviewManager() ) {
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                       module()->getApp()->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  }

  if ( !isApplyAndClose() ) {
    return;
  }

  // Get the selected image
  myImage = Handle(HYDROData_Image)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if ( myImage.IsNull() ) {
    abort();
    return;
  }
  QString anImageName = myImage->GetName();

  // Create temporary graphics file
  QImage aQImage = myImage->Image();
  myTmpImageFile = new QTemporaryFile( QDir::tempPath() + QDir::separator() + anImageName );
  if ( !myTmpImageFile->open() || 
       !aQImage.save( myTmpImageFile->fileName(), "PNG", 100 ) ) {
    abort();
    return;
  }

  // Create the input panel
  HYDROGUI_RecognizeContoursDlg* aPanel = 
    ::qobject_cast<HYDROGUI_RecognizeContoursDlg*>( inputPanel() );
  if ( !aPanel ) {
    return;
  }

  // Reset the panel
  aPanel->reset();
  aPanel->setImageName( anImageName );

  // Get active study
  SalomeApp_Study* aStudy = 
    dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );

  // Get active view manager
  SUIT_ViewManager* aViewMgr = module()->getApp()->activeViewManager();

  // Export the selected image to GEOM module
  if ( aStudy && aViewMgr ) {
    GEOM::GEOM_Gen_var aGeomEngine = GeometryGUI::GetGeomGen();

    QString aGeomPictureEntry;

    HYDROData_GeomTool::createFaceInGEOM( aGeomEngine, aQImage.width(), aQImage.height(), 
                                          anImageName, aGeomPictureEntry );
    
    if ( !aGeomPictureEntry.isEmpty() ) {
      aStudy->setObjectProperty( aViewMgr->getGlobalId(), aGeomPictureEntry, 
                                 GEOM::propertyName( GEOM::Texture ), myTmpImageFile->fileName() );
    
      // update the object browser
      module()->getApp()->updateObjectBrowser( true );

      // browse the published GEOM picture
      QStringList anEntries;
      anEntries << aGeomPictureEntry;
      browseObjects( anEntries );

      // Add GEOM picture object entry to the list of temporary geom objects
      myTmpGeomObjects << aGeomPictureEntry;
    }
  }

  // Activate GEOM module operation
  LightApp_Application* anApp = module()->getApp();
  if ( anApp ) {
    connect( anApp, SIGNAL( operationFinished( const QString&, const QString&, const QStringList& ) ), 
             this, SLOT( onExternalOperationFinished( const QString&, const QString&, const QStringList& ) ) );

    module()->getApp()->activateOperation( "Geometry", GEOMOp::OpFeatureDetect );
  }
}

/**
*/
void HYDROGUI_RecognizeContoursOp::abortOperation()
{
  LightApp_Application* anApp = module()->getApp();
  if ( anApp ) {
    anApp->disconnect( this );
  }

  cleanup();

  HYDROGUI_Operation::abortOperation();
}

/**
*/
void HYDROGUI_RecognizeContoursOp::commitOperation()
{
  if ( isApplyAndClose() ) {
    cleanup();
  }

  HYDROGUI_Operation::commitOperation();
}

/**
*/
bool HYDROGUI_RecognizeContoursOp::processApply( int& theUpdateFlags,
                                                QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries )
{
  // Check the original image
  if ( myImage.IsNull() ) {
    return false;
  }

  // Get panel
  HYDROGUI_RecognizeContoursDlg* aPanel = 
    ::qobject_cast<HYDROGUI_RecognizeContoursDlg*>( inputPanel() );
  if ( !aPanel ) {
    return false;
  }
  
  // Check if contour GEOM object exists
  if ( myGeomContourEntry.isEmpty() ) {
    theErrorMsg = tr( "NO_DETECTED_CONTOURS" );
    return false;
  }

  // Get selected polylines
  QStringList aSelectedtPolylines = aPanel->getSelectedtPolylineNames();
  // Remove the selected polylines from the panel
  aPanel->removePolylineNames( aSelectedtPolylines );

  // Create polylines
  foreach ( QString aName, aSelectedtPolylines ) {
    TopoDS_Shape aShape = myPolylineShapes.value( aName )->getTopoShape();
    if ( aShape.IsNull() ) {
      continue;
    }

    Handle(HYDROData_PolylineXY) aPolylineObj =
      Handle(HYDROData_PolylineXY)::DownCast( doc()->CreateObject( KIND_POLYLINEXY ) );

    if( !aPolylineObj.IsNull() ) {
      aPolylineObj->SetName( aName );
      aPolylineObj->ImportShape( aShape, false, NULL );
      aPolylineObj->SetWireColor( HYDROData_PolylineXY::DefaultWireColor() );

      aPolylineObj->Update();
      module()->setIsToUpdate( aPolylineObj );
    }

    // Remove the shape from the map
    HYDROGUI_Shape* aShapeToDelete = myPolylineShapes.take( aName );
    delete aShapeToDelete;
  }
 
  theUpdateFlags = UF_Model;

  return true;
}

/**
*/
HYDROGUI_InputPanel* HYDROGUI_RecognizeContoursOp::createInputPanel() const
{
  HYDROGUI_InputPanel* aPanel = new HYDROGUI_RecognizeContoursDlg( module(), getName() );

  connect( aPanel, SIGNAL( selectionChanged( const QStringList& ) ), this, SLOT( onSelectionChanged( const QStringList& ) ) );

  return aPanel;
}

/**
 * Called when the operation perfomed by another module is finished.
 * \param theModuleName the name of the module which perfomed the operation
 * \param theOperationName the operation name
 * \param theEntryList the list of the created objects entries
 */
void HYDROGUI_RecognizeContoursOp::onExternalOperationFinished( 
  const QString& theModuleName, const QString& theOperationName,
  const QStringList& theEntryList )
{
  // Process "Geometry" module operations only
  if ( theModuleName != "Geometry" ) {
    return;
  }
    
  // Store the operation name
  myGEOMOpName = theOperationName;

  // Close the dialog corresponding to the external operation
  closeExternalOperationDlg();
  
  // Erase the GEOM objects
  LightApp_Displayer().Erase( theEntryList );

  // Add GEOM object entries to the list of temporary GEOM objects
  myTmpGeomObjects << theEntryList;

  if ( theEntryList.count() == 1 ) {
    myGeomContourEntry = theEntryList.first();

    // Update the list of polylines
    updateRecognizedPolylines();
  }
}

/**
  Close the GEOM contours detection dialog.
*/
void HYDROGUI_RecognizeContoursOp::closeExternalOperationDlg()
{
  if ( myGEOMOpName.isEmpty() ) {
    return;
  }

  SUIT_Desktop* aDesktop = module()->getApp()->desktop();
  if ( aDesktop ) {
    QList<QDialog*> aDialogs = aDesktop->findChildren<QDialog*>();
    foreach ( QDialog* aDlg, aDialogs ) {
      if ( typeid(*aDlg).name() == myGEOMOpName ) {
        aDlg->close();
        break;
      }
    }
  }
}

/**
  Update the list of recognized polylines by exploding the GEOM contour object.
*/
void HYDROGUI_RecognizeContoursOp::updateRecognizedPolylines()
{
  // Erase the preview
  erasePreview();

  // Get active study
  SalomeApp_Study* aStudy = 
    dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );
  if ( !aStudy ) {
    return;
  }

  // Explode the compound
  _PTR(SObject) aSObject( aStudy->studyDS()->FindObjectID( qPrintable( myGeomContourEntry ) ) );
  if ( aSObject ) {
    TopoDS_Shape aShape = GEOMBase::GetShapeFromIOR( aSObject->GetIOR().c_str() );

    TopTools_SequenceOfShape aSubShapes;
    HYDROData_ShapesTool::ExploreShapeToShapes( aShape, TopAbs_WIRE, aSubShapes );
    if ( aSubShapes.Length() < 1 ) {
      HYDROData_ShapesTool::ExploreShapeToShapes( aShape, TopAbs_EDGE, aSubShapes );
    }

    Handle(AIS_InteractiveContext) aCtx = NULL;

    // Display preview
    OCCViewer_ViewManager* aViewManager = getPreviewManager();
    if ( aViewManager ) {
      if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() ) {
        // aViewer->enablePreselection( true );
        // aViewer->enableSelection( true );
        aCtx = aViewer->getAISContext();
        connect( aViewer, SIGNAL( selectionChanged() ), this, SLOT( onViewerSelectionChanged() ) );
      }
    }

    QTransform aTrsf = myImage->Trsf();
    QImage anImage = myImage->Image();
    QRectF aRect( QPointF( 0, 0 ), QPointF( anImage.width(), anImage.height() ) );
    aRect = aTrsf.mapRect( aRect );
    aTrsf.setMatrix( aTrsf.m11(), aTrsf.m12(),  aTrsf.m13(),
                     aTrsf.m21(), -aTrsf.m22(), aTrsf.m23(),
                     aTrsf.m31() + aRect.width() * 0.5, aTrsf.m32 () - aRect.height() * 0.5, aTrsf.m33() );

    /*
    QTransform aTrsf = myImage->Trsf();
    gp_Mat aMat( aTrsf.m11(), aTrsf.m21(), 0, 
                 aTrsf.m12(), -aTrsf.m22(), 0, 
                 0,           0,           1 );
    QImage anImage = myImage->Image();
    QRectF aRect( QPointF( 0, 0 ), QPointF( anImage.width(), anImage.height() ) );
    aRect = aTrsf.mapRect( aRect );
    gp_XYZ aVec( aTrsf.m31() + aRect.width() * 0.5, 
                 aTrsf.m32() - aRect.height() * 0.5, 0 );

    BRepBuilderAPI_GTransform aBuilder( gp_GTrsf( aMat, aVec ) );
    */

    Handle(HYDROData_PolylineXY) aPolylineObj = Handle(HYDROData_PolylineXY)::DownCast( doc()->CreateObject( KIND_POLYLINEXY ) );
    QStringList aNamesList;
    for ( int i = 1; i <= aSubShapes.Length(); i++ ) {
      TopoDS_Shape aSubShape = aSubShapes.Value( i );

      // Transform the sub-shape
      aPolylineObj->ImportShape( aSubShape, false, NULL );
      aPolylineObj->Transform( aTrsf );

      /*
      aBuilder.Perform( aSubShape, Standard_True );
      if ( aBuilder.IsDone() ) {
        aSubShape = aBuilder.Shape();
      }*/
      
      HYDROGUI_Shape* aShape = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );
      aShape->setShape( aPolylineObj->GetShape(), true, false );
      aShape->setBorderColor( HYDROData_PolylineXY::DefaultWireColor(), false, false );
      
      QString aPrefix = QString("%1_%2_%3").arg( myImage->GetName(), "Contour", QString::number( i ) );
      QString aName = HYDROGUI_Tool::GenerateObjectName( module(), aPrefix, QStringList(), true );
      myPolylineShapes.insert( aName, aShape);
      aNamesList << aName;
    }

    aPolylineObj->Remove();

    if ( !aCtx.IsNull() ) {
      UpdateZLayersOfHilightPresentationsOfDisplayedObjects( aCtx, Graphic3d_ZLayerId_TopOSD );
      aCtx->UpdateCurrentViewer();
    }

    // Get panel
    HYDROGUI_RecognizeContoursDlg* aPanel = 
      ::qobject_cast<HYDROGUI_RecognizeContoursDlg*>( inputPanel() );
    if ( aPanel ) {
      aPanel->setPolylineNames( aNamesList );
    }
  }
}

/**
  Erase the preview.
*/
void HYDROGUI_RecognizeContoursOp::erasePreview()
{
  foreach ( HYDROGUI_Shape* aShape, myPolylineShapes ) {
    delete aShape;
  }

  myPolylineShapes.clear();
}

/**
  Called when selection of the recognized polylines is changed.
*/
void HYDROGUI_RecognizeContoursOp::onSelectionChanged( const QStringList& theSelectedNames )
{
  Handle(AIS_InteractiveContext) aCtx = NULL;

  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager ) {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() ) {
      aCtx = aViewer->getAISContext();
    }
  }

  if ( !aCtx.IsNull() ) {
    foreach ( QString aName, myPolylineShapes.keys() ) {
      Handle(AIS_InteractiveObject) anObject = 
        myPolylineShapes.value(aName)->getAISObjects()[0];

      bool isSelected = theSelectedNames.contains( aName );
      if ( ( isSelected && !aCtx->IsSelected( anObject) ) ||
           ( !isSelected && aCtx->IsSelected( anObject) ) ) {
        aCtx->AddOrRemoveSelected( anObject, Standard_False );
      }
      // myPolylineShapes[aName]->highlight( isSelected, true );
    }
    aCtx->UpdateCurrentViewer();
  }
}

/**
  Called when selection in the viewer is changed.
*/
void HYDROGUI_RecognizeContoursOp::onViewerSelectionChanged()
{
  // Get panel
  HYDROGUI_RecognizeContoursDlg* aPanel = 
    ::qobject_cast<HYDROGUI_RecognizeContoursDlg*>( inputPanel() );
  if ( !aPanel ) {
    return;
  }


  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  Handle(AIS_InteractiveContext) aCtx = NULL;
  if ( aViewManager ) {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() ) {
      aCtx = aViewer->getAISContext();
    }
  }
  
  if ( !aCtx.IsNull() ) {
    QStringList aSelectedNames;

    foreach ( QString aName, myPolylineShapes.keys() ) {
      bool isSelected = aCtx->IsSelected( myPolylineShapes.value(aName)->getAISObjects()[0] );
      if ( isSelected ) {
        aSelectedNames << aName;
      }
    }

    aPanel->setSelectedPolylineNames( aSelectedNames );
  }
}

/**
  Do the operation data cleanup.
*/
void HYDROGUI_RecognizeContoursOp::cleanup()
{
  // Close the external operation dialog
  closeExternalOperationDlg();

  // Erase the preview 
  erasePreview();

  // Delete temporary image file
  if ( myTmpImageFile ) {
    delete myTmpImageFile;
    myTmpImageFile = NULL;
  }

  // Delete temporary GEOM objects
  if ( !myTmpGeomObjects.isEmpty() ) {
    HYDROGUI_Tool::DeleteGeomObjects( module(), myTmpGeomObjects );
    // update the object browser
    module()->getApp()->updateObjectBrowser( true );
  }
}

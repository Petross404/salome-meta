// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_NameValidator.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Module.h"

HYDROGUI_NameValidator::HYDROGUI_NameValidator( HYDROGUI_Module* theModule, QObject* parent )
: QValidator( parent ), myModule( theModule )
{
}

HYDROGUI_NameValidator::~HYDROGUI_NameValidator()
{
}

QValidator::State HYDROGUI_NameValidator::validate( QString & theName, int & thePos ) const
{
  State aRes = Acceptable;
  QString aName = theName.simplified();

  if( !myEditedObject.IsNull() ) // To avoid problems when myEditedObject is not set yet
  {
    if ( aName.isEmpty() )
    {
      aRes = Intermediate;
    }
    else if( myEditedObject->GetName() != theName )
    {
      // check that there are no other objects with the same name in the document
      Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( myModule, aName );
      if ( !anObject.IsNull() )
      {
        aRes = Intermediate;
      }
    }
  }

  return aRes;
}

void HYDROGUI_NameValidator::fixup( QString & theName ) const
{
  theName = theName.simplified();
  if ( theName.isEmpty() )
  {
    emit emptyName();
  }
  else
  {
    emit alreadyExists( theName );
  }

  if( !myEditedObject.IsNull() )
  {
    theName = myEditedObject->GetName();
  }
}

void HYDROGUI_NameValidator::setEditedObject( const Handle(HYDROData_Entity) theObj )
{
  myEditedObject = theObj;
}

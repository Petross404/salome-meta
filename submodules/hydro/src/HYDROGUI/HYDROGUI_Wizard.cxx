// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Wizard.h"
#include "HYDROGUI_Module.h"
#include <CAM_Application.h>
#include <SUIT_Desktop.h>
#include <QFrame>
#include <QVBoxLayout>
#include <QWidget>
#include <QPushButton>
#include <QAbstractButton>
#include <QStackedWidget>
#include <QWizardPage>

HYDROGUI_Wizard::HYDROGUI_Wizard( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle, false )
{

  myWizard = new QStackedWidget( theModule->application()->desktop() );

  myNext = new QPushButton( tr( "NEXT" ), buttonFrame() );
  myBack = new QPushButton( tr( "BACK" ), buttonFrame() );
  myFinish = new QPushButton( tr( "FINISH" ), buttonFrame() );

  QHBoxLayout* aBtnsLayout = qobject_cast<QHBoxLayout*>(buttonFrame()->layout());
  aBtnsLayout->addWidget( myBack, 0 );
  aBtnsLayout->addWidget( myNext, 0 );
  aBtnsLayout->addWidget( myFinish, 0 );
  aBtnsLayout->addWidget( myCancel, 0 );
  aBtnsLayout->addStretch( 1 );
  aBtnsLayout->addWidget( myHelp, 0 );

  myBack->setEnabled( false );
  myNext->setVisible( false );

  connect( myNext,  SIGNAL( clicked() ), SLOT( onNext() ) );
  connect( myBack,  SIGNAL( clicked() ), SLOT( onBack() ) );
  connect( myFinish, SIGNAL( clicked() ), SLOT( onApply() ) );

  addWidget( myWizard );
}

HYDROGUI_Wizard::~HYDROGUI_Wizard()
{
}

QStackedWidget* HYDROGUI_Wizard::wizard() const
{
  return myWizard;
}

int HYDROGUI_Wizard::addPage( QWizardPage* thePage )
{
  if ( myWizard->count() > 0 )
  {
    SetButtonsState(false);
    /*myNext->setVisible( true );
    myFinish->setVisible( false );*/
  }
  return myWizard->addWidget( thePage );
}

void HYDROGUI_Wizard::onNext()
{
  if ( !acceptCurrent() )
    return;

  if ( myWizard->count() <= 0 )
    return;

  myBack->setEnabled( true );
  int aCurIdx = myWizard->currentIndex();
  if ( IsLastPage() )
  {
    // Go to the last page
    SetButtonsState( true );
    /*myNext->setVisible( false );
    myFinish->setVisible( true );*/
  }
  myWizard->setCurrentIndex( aCurIdx + 1 );

  emit Next( myWizard->currentIndex() );
}

void HYDROGUI_Wizard::onBack()
{
  if ( myWizard->count() <= 0 )
    return;

  if ( myWizard->count() > 1 )
  {
    /*myNext->setVisible( true );
    myFinish->setVisible( false );*/
    SetButtonsState( false );
  }
  else
  {
    // Wizard has a single page - show finish
    SetButtonsState( true );
    /*myNext->setVisible( false );
    myFinish->setVisible( true );*/
  }

  int aCurIdx = myWizard->currentIndex();
  myWizard->setCurrentIndex( aCurIdx - 1 );

  aCurIdx = myWizard->currentIndex();
  if ( aCurIdx <= 0 )
  {
    // Disable back if go to the first page
    myBack->setEnabled( false );
  }

  emit Back( aCurIdx );
}

void HYDROGUI_Wizard::onFirstPage()
{
  if ( myWizard->count() > 1 ) {
    /*myNext->setVisible( true );
    myFinish->setVisible( false );*/
    SetButtonsState( false );
    myWizard->setCurrentIndex( 0 );
    myBack->setEnabled( false );
    emit Back( myWizard->currentIndex() );
  }
}

bool HYDROGUI_Wizard::acceptCurrent() const
{
  return true;
}

QAbstractButton* HYDROGUI_Wizard::button( QWizard::WizardButton theBtnId ) const
{
  QAbstractButton* aRes = 0;
  switch( theBtnId )
  {
    case QWizard::BackButton:
      aRes = myBack;
      break;
    case QWizard::NextButton:
      aRes = myNext;
      break;
    case QWizard::FinishButton:
      aRes = myFinish;
      break;
    case QWizard::CancelButton:
      aRes = myCancel;
      break;
    case QWizard::HelpButton:
      aRes = myHelp;
  }
  return aRes;
}

bool HYDROGUI_Wizard::IsLastPage()
{
  if ( myWizard->currentIndex() == ( myWizard->count() - 2 ))
    return true;
  else
    return false;
}

void HYDROGUI_Wizard::SetButtonsState (bool IsLastPage)
{
  if (!IsLastPage)
  {
    myNext->setVisible( true );
    myFinish->setVisible( false );
  }
  else
  {
    myNext->setVisible( false );
    myFinish->setVisible( true );
  }
}

QPushButton* HYDROGUI_Wizard::BackButton()
{
   return myBack;
}

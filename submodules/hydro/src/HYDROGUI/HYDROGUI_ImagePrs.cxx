// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImagePrs.h"

#include <Prs3d_Root.hxx>

#include <SelectMgr_Selection.hxx>
#include <SelectMgr_EntityOwner.hxx>
#include <Select3D_SensitiveFace.hxx>

#include <Graphic3d_Group.hxx>
#include <Graphic3d_AspectLine3d.hxx>
#include <Graphic3d_Texture2Dmanual.hxx>
#include <Graphic3d_ArrayOfPolylines.hxx>
#include <Graphic3d_ArrayOfTriangles.hxx>
#include <Graphic3d_AspectFillArea3d.hxx>

#include <TColgp_Array1OfPnt.hxx>

IMPLEMENT_STANDARD_RTTIEXT(HYDROGUI_ImagePrs, AIS_InteractiveObject)

HYDROGUI_ImagePrs::HYDROGUI_ImagePrs()
  : AIS_InteractiveObject()
{
}

HYDROGUI_ImagePrs::HYDROGUI_ImagePrs( const Handle(Image_PixMap)& theImage, const QPolygonF& theContour )
    : AIS_InteractiveObject(),
    myImage( theImage ),
    myContour( theContour )
{
}

HYDROGUI_ImagePrs::~HYDROGUI_ImagePrs()
{
}

QPolygonF HYDROGUI_ImagePrs::GetContour() const
{
    return myContour;
}

void HYDROGUI_ImagePrs::SetContour( const QPolygonF& theRect )
{
    if ( myContour != theRect )
        SetToUpdate();

    myContour = theRect;
}

Handle(Image_PixMap) HYDROGUI_ImagePrs::GetImage() const
{
    return myImage;
}

void HYDROGUI_ImagePrs::SetImage( const Handle(Image_PixMap)& theImage )
{
    if ( myImage != theImage )
        SetToUpdate();

    myImage = theImage;
}

void HYDROGUI_ImagePrs::ComputeSelection( const Handle(SelectMgr_Selection)& theSelection, const Standard_Integer theMode )
{
    if ( myContour.isEmpty() )
        return;

    if ( theMode == 0 )
    {
        Handle(SelectMgr_EntityOwner) anOwner = new SelectMgr_EntityOwner( this );

        TColgp_Array1OfPnt aPoints( 0, myContour.size() - 1 );
        for ( int i = 0; i < myContour.size(); i++ )
            aPoints.SetValue( aPoints.Lower() + i, convert( myContour[i] ) );

        Handle(Select3D_SensitiveFace) aSensitiveFace = new Select3D_SensitiveFace( anOwner, aPoints, Select3D_TOS_INTERIOR );
        theSelection->Add( aSensitiveFace );
    }
}

void HYDROGUI_ImagePrs::Compute( const Handle(PrsMgr_PresentationManager3d)&,
                                 const Handle(Prs3d_Presentation)& aPrs,
                                 const Standard_Integer aMode )
{
    aPrs->Clear();
    Handle(Graphic3d_Group) aGroup = Prs3d_Root::CurrentGroup( aPrs );

    if( myImage.IsNull() )
      return;

    if ( aMode == 0 )
    {
        Handle(Graphic3d_ArrayOfPolylines) aSegments = new Graphic3d_ArrayOfPolylines( 5 );
        aSegments->AddVertex( convert( myContour[0] ) );
        aSegments->AddVertex( convert( myContour[1] ) );
        aSegments->AddVertex( convert( myContour[2] ) );
        aSegments->AddVertex( convert( myContour[3] ) );
        aSegments->AddVertex( convert( myContour[0] ) );

        aGroup->AddPrimitiveArray( aSegments );
    }
    else
    {
        Graphic3d_MaterialAspect aMat( Graphic3d_NOM_PLASTIC );
        Handle(Graphic3d_AspectFillArea3d) aFillAspect =
            new Graphic3d_AspectFillArea3d( Aspect_IS_SOLID, Quantity_NOC_WHITE, Quantity_NOC_BLACK, Aspect_TOL_SOLID, 1.0, aMat, aMat );

        Handle(Graphic3d_TextureMap) aTex = new Graphic3d_Texture2Dmanual( myImage );
        aTex->DisableModulate();
        aFillAspect->SetTextureMapOn();
        aFillAspect->SetTextureMap( aTex );

        aGroup->SetGroupPrimitivesAspect( aFillAspect );

        Handle(Graphic3d_ArrayOfTriangles) aTriangles = new Graphic3d_ArrayOfTriangles( 6, 0, Standard_False, Standard_False, Standard_True );

        aTriangles->AddVertex( convert( myContour[0] ), gp_Pnt2d( 0, myImage->IsTopDown() ? 0 : 1 ) );
        aTriangles->AddVertex( convert( myContour[1] ), gp_Pnt2d( 1, myImage->IsTopDown() ? 0 : 1 ) );
        aTriangles->AddVertex( convert( myContour[2] ), gp_Pnt2d( 1, myImage->IsTopDown() ? 1 : 0 ) );

        aTriangles->AddVertex( convert( myContour[2] ), gp_Pnt2d( 1, myImage->IsTopDown() ? 1 : 0 ) );
        aTriangles->AddVertex( convert( myContour[3] ), gp_Pnt2d( 0, myImage->IsTopDown() ? 1 : 0 ) );
        aTriangles->AddVertex( convert( myContour[0] ), gp_Pnt2d( 0, myImage->IsTopDown() ? 0 : 1 ) );

        aGroup->AddPrimitiveArray( aTriangles );
    }
}

gp_Pnt HYDROGUI_ImagePrs::convert( const QPointF& thePoint ) const
{
    return gp_Pnt( thePoint.x(), thePoint.y(), 0 );
}

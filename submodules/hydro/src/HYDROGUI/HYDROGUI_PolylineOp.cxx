// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PolylineOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_PolylineDlg.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Document.h>
#include <HYDROData_PolylineOperator.h>
#include <HYDROData_TopoCurve.h>

#include <CurveCreator_Curve.hxx>
#include <CurveCreator_Displayer.hxx>
#include <CurveCreator_Utils.hxx>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_UpdateFlags.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>

#include <OCCViewer_AISSelector.h>

#include <Precision.hxx>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>

#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

//static int ZValueIncrement = 0;
static const double HYDROGUI_MAXIMAL_DEFLECTION = 1e-2;

HYDROGUI_PolylineOp::HYDROGUI_PolylineOp( HYDROGUI_Module* theModule, bool theIsEdit )
: HYDROGUI_Operation( theModule ), 
  myIsEdit( theIsEdit ),
  myCurve( NULL )
{
  setName( theIsEdit ? tr( "EDIT_POLYLINE" ) : tr( "CREATE_POLYLINE" ) );
}

HYDROGUI_PolylineOp::~HYDROGUI_PolylineOp()
{
  erasePreview();
}

/**
 * Redirect the delete action to input panel
 */
void HYDROGUI_PolylineOp::deleteSelected()
{
  HYDROGUI_PolylineDlg* aPanel = (HYDROGUI_PolylineDlg*)inputPanel();
  aPanel->deleteSelected();
}

/**
 * Checks whether there are some to delete
 */
bool HYDROGUI_PolylineOp::deleteEnabled()
{
  HYDROGUI_PolylineDlg* aPanel = (HYDROGUI_PolylineDlg*)inputPanel();
  return aPanel->deleteEnabled();
}

/**
 * Set Z layer for the operation preview.
 \param theLayer a layer position
 */
void HYDROGUI_PolylineOp::updatePreviewZLayer( int theLayer )
{
  HYDROGUI_Operation::updatePreviewZLayer( theLayer );

  int aZLayer = getPreviewZLayer();
  if ( aZLayer >= 0 )
  {
    if( getPreviewManager() )
    {
      if ( OCCViewer_Viewer* aViewer = getPreviewManager()->getOCCViewer() )
      {
        Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
        if( !aCtx.IsNull() )
        {
          Handle(AIS_InteractiveObject) anObject = myCurve->getAISObject( true );
          aCtx->SetZLayer( anObject, aZLayer );
        }
      }
    }
  }
}

void HYDROGUI_PolylineOp::startOperation()
{
  DEBTRACE("startOperation");
  if( myIsEdit )
  {
    if ( isApplyAndClose() )
	  myEditedObject = Handle(HYDROData_PolylineXY)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if ( !myEditedObject.IsNull() && !myEditedObject->IsEditable() )
    {
      // Polyline is imported from GEOM module an is not recognized as
      // polyline or spline and exist only as shape presentation
      SUIT_MessageBox::critical( module()->getApp()->desktop(),
                                 tr( "POLYLINE_IS_UNEDITABLE_TLT" ),
                                 tr( "POLYLINE_IS_UNEDITABLE_MSG" ) );
      abort();
      return;
    }
  }

  if( myCurve )
    delete myCurve;

  myCurve = new CurveCreator_Curve( CurveCreator::Dim2d );

  HYDROGUI_Operation::startOperation();

  HYDROGUI_PolylineDlg* aPanel = (HYDROGUI_PolylineDlg*)inputPanel();
  aPanel->reset();

  LightApp_Application* anApp = module()->getApp();
  OCCViewer_ViewManager* aViewManager =
    dynamic_cast<OCCViewer_ViewManager*>( anApp->getViewManager( OCCViewer_Viewer::Type(), true ) );
  aPanel->setOCCViewer( aViewManager ? aViewManager->getOCCViewer() : 0 );
  setPreviewManager( aViewManager );

  if ( isApplyAndClose() )
    setCursor();

  QString aPolylineName;
  if( !myEditedObject.IsNull() )
  {
    NCollection_Sequence<TCollection_AsciiString>           aSectNames;
    NCollection_Sequence<HYDROData_PolylineXY::SectionType> aSectTypes;
    NCollection_Sequence<bool>                              aSectClosures;
    bool aIsInCustom = myEditedObject->GetIsInCustomFlag();
    myEditedObject->SetIsInCustomFlag( true );
    myEditedObject->GetSections( aSectNames, aSectTypes, aSectClosures );
    myEditedObject->SetIsInCustomFlag( aIsInCustom );

    if (!aSectNames.IsEmpty())
    {
      for ( int i = 1, n = aSectNames.Size(); i <= n; ++i )
      {
        QString aSectName = HYDROGUI_Tool::ToQString( aSectNames.Value( i ) );
        HYDROData_PolylineXY::SectionType aSectType = aSectTypes.Value( i );
        bool aSectClosure = aSectClosures.Value( i );

        CurveCreator::SectionType aCurveType = CurveCreator::Polyline;
        if( aSectType == HYDROData_PolylineXY::SECTION_SPLINE )
          aCurveType = CurveCreator::Spline;

        CurveCreator::Coordinates aCurveCoords;

        HYDROData_PolylineXY::PointsList aSectPointsList =
          myEditedObject->GetPoints( i - 1 );
        for (int k = 1, aNbPoints = aSectPointsList.Size(); k <= aNbPoints; ++k)
        {
          const HYDROData_PolylineXY::Point& aSectPoint =
            aSectPointsList.Value( k );
          aCurveCoords.push_back( aSectPoint.X() );
          aCurveCoords.push_back( aSectPoint.Y() );
        }

        Quantity_Color aColor = CurveCreator_Utils::getRandColor();      
        QColor aQColor = CurveCreator_Utils::colorConv(aColor);
        myEditedObject->GetSectionColor(i-1, aQColor);

        myCurve->addSectionInternal( aSectName.toStdString(),
          aCurveType, aSectClosure, aCurveCoords, CurveCreator_Utils::colorConv(aQColor) );
      }
    }
    else
    {
      std::deque<CurveCreator::Coordinates> aPs;
      std::deque<bool> isCloseds;
      std::vector<TopoDS_Wire> aWires;
      HYDROData_PolylineOperator::GetWires(myEditedObject, aWires);
      const int aSCount = aWires.size();
      bool isError = false;
      for (int aSI = 0; aSI < aSCount; ++aSI)
      {
        HYDROData_TopoCurve aCurve, aCurve2;
        std::list<gp_XYZ> aPs2;
        int aMaxPieceCount;
        if (!aCurve.Initialize(aWires[aSI]) ||
          (aMaxPieceCount = aCurve.BSplinePiecewiseCurve(
            HYDROGUI_MAXIMAL_DEFLECTION * 0.1, aCurve2)) == 0)
        {
          isError = true;
          break;
        }

        double aDefl;
        aMaxPieceCount *= 100;
        int aPieceCount = 0;
        while (aPieceCount < aMaxPieceCount &&
          (aDefl = HYDROData_PolylineOperator::ReduceDeflection(
              HYDROGUI_MAXIMAL_DEFLECTION, aCurve2, aPieceCount)) >
            HYDROGUI_MAXIMAL_DEFLECTION);
        if (aDefl < 0 || !aCurve2.ValuesInKnots(aPs2))
        {
          isError = true;
          break;
        }

        aPs.push_back(CurveCreator::Coordinates());
        CurveCreator::Coordinates& aPs3 = aPs.back();
        std::list<gp_XYZ>::const_iterator aLastPIt = aPs2.end();
        std::list<gp_XYZ>::const_iterator aPIt = aPs2.begin();
        for (; aPIt != aLastPIt; ++aPIt)
        {
          const gp_XYZ aP = *aPIt;
          aPs3.push_back(aP.X());
          aPs3.push_back(aP.Y());
        }
        isCloseds.push_back(aCurve.IsClosed());
      }

      if (!isError)
      {
        const TCollection_AsciiString aNamePrefix = "Section_";
        for (int aSI = 0; aSI < aSCount; ++aSI)
        {
          myCurve->addSectionInternal((aNamePrefix + (aSI + 1)).ToCString(),
            CurveCreator::Spline, isCloseds[aSI], aPs[aSI], CurveCreator_Utils::getRandColor());
        }
      }
    }

    aPolylineName = myEditedObject->GetName();
  }
  else
  {
    aPolylineName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_POLYLINE_NAME" ) );
  }

  aPanel->setPolylineName( aPolylineName );
  aPanel->setCurve( myCurve );

  displayPreview();
}

void HYDROGUI_PolylineOp::abortOperation()
{
  restoreCursor();

  HYDROGUI_PolylineDlg* aPanel = (HYDROGUI_PolylineDlg*)inputPanel();
  if ( aPanel )
    aPanel->setOCCViewer( 0 );
  erasePreview();

  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_PolylineOp::commitOperation()
{
  DEBTRACE("commitOperation");
  if ( isApplyAndClose() )
  {
    restoreCursor();

    HYDROGUI_PolylineDlg* aPanel = (HYDROGUI_PolylineDlg*)inputPanel();
    if ( aPanel )
      aPanel->setOCCViewer( 0 );
  }

  erasePreview();

  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_PolylineOp::createInputPanel() const
{
  HYDROGUI_PolylineDlg* aDlg = new HYDROGUI_PolylineDlg( module(), getName() );
  connect( aDlg, SIGNAL( selectionChanged() ), this, SLOT( onEditorSelectionChanged() ) );
  return aDlg;
}

bool HYDROGUI_PolylineOp::processApply( int& theUpdateFlags,
                                        QString& theErrorMsg,
                                        QStringList& theBrowseObjectsEntries )
{
  DEBTRACE("processApply");
  HYDROGUI_PolylineDlg* aPanel = ::qobject_cast<HYDROGUI_PolylineDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QString aPolylineName = aPanel->getPolylineName().simplified();
  if ( aPolylineName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  if( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != aPolylineName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), aPolylineName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( aPolylineName );
      return false;
    }
  }

  if ( myCurve->getNbSections() <= 0 )
  {
    theErrorMsg = tr( "EMPTY_POLYLINE_DATA" );
    return false;
  }

  Handle(HYDROData_PolylineXY) aPolylineObj;
  if( myIsEdit )
  {
    aPolylineObj = myEditedObject;
    aPolylineObj->RemoveSections();
  }
  else
  {
    aPolylineObj = Handle(HYDROData_PolylineXY)::DownCast( doc()->CreateObject( KIND_POLYLINEXY ) );
  }

  if( aPolylineObj.IsNull() )
    return false;

  aPolylineObj->SetName( aPolylineName );

  for ( int i = 0 ; i < myCurve->getNbSections() ; i++ )
  {
    TCollection_AsciiString aSectName = HYDROGUI_Tool::ToAsciiString( myCurve->getSectionName( i ).c_str() );
    CurveCreator::SectionType aCurveType =  myCurve->getSectionType( i );
    bool aSectClosure = myCurve->isClosed( i );

    HYDROData_PolylineXY::SectionType aSectType = HYDROData_PolylineXY::SECTION_POLYLINE;

    if ( aCurveType == CurveCreator::Spline )
      aSectType = HYDROData_PolylineXY::SECTION_SPLINE;

    aPolylineObj->AddSection( aSectName, aSectType, aSectClosure );

    Quantity_Color aColor = myCurve->getColorSection(i);
    aPolylineObj->SetSectionColor(i, CurveCreator_Utils::colorConv(aColor));

    // Add the points from section
    CurveCreator::Coordinates aCurveCoords = myCurve->getCoords( i );

    if ( aCurveCoords.size() <= 2 )
    {
      theErrorMsg = tr( "NUMBER_OF_SECTION_POINTS_INCORRECT" );
      return false;
    }

    for ( int k = 0 ; k + 1 < aCurveCoords.size() ; k++ )
    {
      HYDROData_PolylineXY::Point aSectPoint;

      aSectPoint.SetX( aCurveCoords.at( k ) );
      k++;
      aSectPoint.SetY( aCurveCoords.at( k ) );

      aPolylineObj->AddPoint( i, aSectPoint );
    }
  }

 //if ( !myIsEdit )
 //{
 //  aPolylineObj->SetWireColor( HYDROData_PolylineXY::DefaultWireColor() );
 //}

  // Update the wire of polyline
  aPolylineObj->Update();
  module()->setIsToUpdate( aPolylineObj );

  // the viewer should be release from the widget before the module update it
  // because it has an opened local context and updated presentation should not be displayed in it
  if ( aPanel )
    aPanel->setOCCViewer( 0 );

  theUpdateFlags = UF_Model;

  // the polyline should be rebuild in all viewers, where it is displayed
  theUpdateFlags |= UF_Viewer | UF_GV_Forced;
  theUpdateFlags |= UF_OCCViewer | UF_OCC_Forced;
  theUpdateFlags |= UF_VTKViewer;

  size_t anActiveViewId = HYDROGUI_Tool::GetActiveGraphicsViewId( module() );
  if ( anActiveViewId == 0 )
  {
    anActiveViewId = HYDROGUI_Tool::GetActiveOCCViewId( module() );
  }

  if( !myIsEdit )
  {
    module()->setObjectVisible( anActiveViewId, aPolylineObj, true );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aPolylineObj );
    theBrowseObjectsEntries.append( anEntry );
  }  

  return true;
}

void HYDROGUI_PolylineOp::onEditorSelectionChanged()
{
  HYDROGUI_PolylineDlg* aPanel = (HYDROGUI_PolylineDlg*)inputPanel();
  if( !aPanel )
    return;
  if( !myCurve )
    return;
  CurveCreator_Displayer* aDisplayer = myCurve->getDisplayer();
  if( !aDisplayer )
    return;
  //QList<int> aSelSections = aPanel->getSelectedSections();
  bool aIsHl = false;
  //if( aSelSections.contains(i) ){
  // TODO
  //aDisplayer->highlight( myCurve->getAISObject(), aIsHl );
  //}
}

void HYDROGUI_PolylineOp::displayPreview()
{
  if( getPreviewManager() )
  {
    if( OCCViewer_Viewer* aViewer = getPreviewManager()->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if( !aCtx.IsNull() )
      {
        CurveCreator_Displayer* aDisplayer = new CurveCreator_Displayer( aCtx, getPreviewZLayer() );
        myCurve->setDisplayer( aDisplayer );
        aDisplayer->display( myCurve->getAISObject( true ), true );
      }
    }
  }
}

void HYDROGUI_PolylineOp::erasePreview()
{
  CurveCreator_Displayer* aDisplayer = myCurve ? myCurve->getDisplayer() : 0;
  if( getPreviewManager() && aDisplayer )
  {
    if( OCCViewer_Viewer* aViewer = getPreviewManager()->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if( !aCtx.IsNull() )
      {
        aDisplayer->eraseAll( true );
      }
    }
  }

  setPreviewManager( NULL );
  if ( myCurve )
  {
    delete myCurve;
    myCurve = NULL;
  }
}

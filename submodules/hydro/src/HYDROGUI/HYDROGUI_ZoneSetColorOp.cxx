// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ZoneSetColorOp.h"

#include "HYDROGUI_Module.h"
#include <QColorDialog>
#include <LightApp_Application.h>
#include <SUIT_Desktop.h>
#include <LightApp_UpdateFlags.h>
#include <HYDROGUI_UpdateFlags.h>
#include <HYDROGUI_Tool2.h>
#include <QApplication>

HYDROGUI_ZoneSetColorOp::HYDROGUI_ZoneSetColorOp( HYDROGUI_Module* theModule )
  : HYDROGUI_Operation( theModule )
{
  setName( tr( "ZONE_SET_COLOR" ) );
}

HYDROGUI_ZoneSetColorOp::~HYDROGUI_ZoneSetColorOp()
{
}

void HYDROGUI_ZoneSetColorOp::startOperation()
{
  HYDROGUI_Operation::startOperation();
  myZone = Handle(HYDROData_Zone)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( !myZone.IsNull() )
    onApply();
  else
    onCancel();
}

bool HYDROGUI_ZoneSetColorOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
  QStringList& theBrowseObjectsEntries )
{
  theUpdateFlags = 0;
  if(myZone)
  {
    QColor aZoneColor = myZone->GetColor(QColor(1,1,1));
    QApplication::restoreOverrideCursor();
    QColor aNewColor = QColorDialog::getColor( aZoneColor );
    myZone->SetColor(aNewColor);	 
    theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced;
    return true;
  }
  return false;
}



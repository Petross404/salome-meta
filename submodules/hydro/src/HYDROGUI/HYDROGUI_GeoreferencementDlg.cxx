// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_GeoreferencementDlg.h"

#include "HYDROGUI_Tool.h"
#include "HYDROGUI_LineEditDoubleValidator.h"

#include <CurveCreator_Utils.hxx>

#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewPort3d.h>

#include <AIS_InteractiveContext.hxx>

#include <QTableWidget>
#include <QHeaderView>
#include <QRadioButton>
#include <QPushButton>
#include <QLineEdit>
#include <QButtonGroup>
#include <QGroupBox>
#include <QLayout>
#include <QMouseEvent>

//! Profile data structre constructor
HYDROGUI_GeoreferencementDlg::ProfileGeoData::ProfileGeoData( 
  const QString& theName,
  const QString& theXg, const QString& theYg, 
  const QString& theXd, const QString& theYd)
{
  this->Name = theName;
  this->isEmpty = theXg.isEmpty() && theYg.isEmpty() &&
                 theXd.isEmpty() && theYd.isEmpty();
  this->isIncomplete = !isEmpty;

  if ( isIncomplete ) {
    bool isOk = false;

    this->Xg= theXg.toDouble( &isOk );
    if ( isOk ) {
      this->Yg = theYg.toDouble( &isOk );
      if ( isOk ) {
        this->Xd = theXd.toDouble( &isOk );
        if ( isOk ) {
          this->Yd = theYd.toDouble( &isOk );
          this->isIncomplete = !isOk;
        }
      }
    }
  }
}

HYDROGUI_GeoreferencementDlg::HYDROGUI_GeoreferencementDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ), myIsModified( false )
{
  // Mode selector (all/selected)
  QGroupBox* aModeGroup = new QGroupBox( tr( "PROFILES" ), this );

  QRadioButton* anAllRB = new QRadioButton( tr( "ALL_MODE" ), aModeGroup );
  QRadioButton* aSelectedRB = new QRadioButton( tr( "SELECTED_MODE" ), aModeGroup );

  myModeButtons = new QButtonGroup( aModeGroup );
  myModeButtons->addButton( anAllRB, AllProfiles );
  myModeButtons->addButton( aSelectedRB, SelectedProfiles );

  QBoxLayout* aModeSelectorLayout = new QVBoxLayout( aModeGroup );
  aModeSelectorLayout->setMargin( 5 );
  aModeSelectorLayout->setSpacing( 5 );
  aModeSelectorLayout->addWidget( anAllRB );
  aModeSelectorLayout->addWidget( aSelectedRB );

  // Update selection button
  myUpdateSelBtn = new QPushButton( mainFrame() );
  myUpdateSelBtn->setText( tr("UPDATE_SELECTION") );
  QBoxLayout* anUpdateSelLayout = new QHBoxLayout( mainFrame() );
  anUpdateSelLayout->addWidget( myUpdateSelBtn );
  anUpdateSelLayout->addStretch();

  // Table
  myTable = new QTableWidget( mainFrame() );
  myTable->setItemDelegate( new HYDROGUI_LineEditDoubleValidator( this ) );
  myTable->verticalHeader()->setVisible( false );
  myTable->setSelectionBehavior( QAbstractItemView::SelectItems );
  myTable->setSelectionMode( QAbstractItemView::SingleSelection );
  myTable->setColumnCount( 5 );
  QStringList aColumnNames;
  aColumnNames << tr( "PROFILE_HEADER" ) << tr( "XG_HEADER" ) << tr( "YG_HEADER" ) << 
                                            tr( "XD_HEADER" ) << tr( "YD_HEADER" );
  myTable->setHorizontalHeaderLabels( aColumnNames );

  // Layout
  addWidget( aModeGroup );
  addLayout( anUpdateSelLayout );
  addWidget( myTable );

  // Connect signals and slots
  connect( myModeButtons, SIGNAL( buttonClicked( int ) ), this, SLOT( onModeActivated( int ) ) );
  connect( myUpdateSelBtn, SIGNAL( clicked() ), this, SIGNAL( updateSelection() ) );
  connect( myTable->model(), SIGNAL( dataChanged ( const QModelIndex&, const QModelIndex& ) ), 
           this, SLOT( onDataChanged() ) );
}

HYDROGUI_GeoreferencementDlg::~HYDROGUI_GeoreferencementDlg()
{
}

void HYDROGUI_GeoreferencementDlg::onModeActivated( int theMode )
{
  myUpdateSelBtn->setEnabled( theMode == SelectedProfiles );
  emit modeActivated( theMode );
}

void HYDROGUI_GeoreferencementDlg::reset()
{
  // Activate the "All" mode
  myModeButtons->button( AllProfiles )->setChecked( true );

  // Clear the table widget
  myTable->setRowCount( 0 );
}

void HYDROGUI_GeoreferencementDlg::setMode( const int theMode )
{
  bool isBlocked = myModeButtons->blockSignals( true );

  QAbstractButton* aModeButton = myModeButtons->button( theMode );
  if ( aModeButton ) {
    aModeButton->setChecked( true );
  }

  myUpdateSelBtn->setEnabled( theMode == SelectedProfiles );

  myModeButtons->blockSignals( isBlocked );
}

void HYDROGUI_GeoreferencementDlg::setData( const ProfilesGeoDataList& theData )
{
  disconnect( myTable->model(), SIGNAL( dataChanged ( const QModelIndex&, const QModelIndex& ) ), 
              this, SLOT( onDataChanged() ) );

  myTable->setRowCount( 0 );

  foreach ( const ProfileGeoData& aGeoData, theData ) {
    // Check the current profile name
    if ( aGeoData.Name.isEmpty() ) {
      continue;
    }

    // Get georeferencement data for the current profile
    QString aXg, anYg, aXd, anYd;
    if ( !aGeoData.isEmpty ) {
      aXg = HYDROGUI_Tool::GetCoordinateString( aGeoData.Xg, false );
      anYg = HYDROGUI_Tool::GetCoordinateString( aGeoData.Yg, false );
      aXd = HYDROGUI_Tool::GetCoordinateString( aGeoData.Xd, false );
      anYd = HYDROGUI_Tool::GetCoordinateString( aGeoData.Yd, false );
    }
    
    // Insert row with the data
    int aRow = myTable->rowCount();
    myTable->insertRow( aRow );

    // "Profile" column
    QTableWidgetItem* aNameItem = new QTableWidgetItem( aGeoData.Name );
    aNameItem->setFlags( aNameItem->flags() & ~Qt::ItemIsEnabled );
    /* Bold font is not used in other tables. Keep the common style.
    QFont aFont = aNameItem->font();
    aFont.setBold( true );
    aNameItem->setFont( aFont );
    */ 
    myTable->setItem( aRow, 0, aNameItem );

    // "Xg" column
    myTable->setItem( aRow, 1, new QTableWidgetItem( aXg ) );

    // "Yg" column
    myTable->setItem( aRow, 2, new QTableWidgetItem( anYg ) );

    // "Xd" column
    myTable->setItem( aRow, 3, new QTableWidgetItem( aXd ) );

    // "Yd" column
    myTable->setItem( aRow, 4, new QTableWidgetItem( anYd ) );
  }

  myTable->resizeColumnToContents( 0 );
  myTable->resizeRowsToContents();

  myIsModified = false;
  
  connect( myTable->model(), SIGNAL( dataChanged ( const QModelIndex&, const QModelIndex& ) ), 
           this, SLOT( onDataChanged() ) );
}

void HYDROGUI_GeoreferencementDlg::getData( ProfilesGeoDataList& theData ) const
{
  // Clear the list
  theData.clear();

  // Fill the map
  bool isOk = false;
  QString aXg, anYg, aXd, anYd;
  for ( int aRow = 0; aRow < myTable->rowCount(); aRow++ ) {
    QString aProfileName = myTable->item( aRow, 0 )->text();

    aXg = myTable->item( aRow, 1 )->text();
    anYg = myTable->item( aRow, 2 )->text();
    aXd = myTable->item( aRow, 3 )->text();
    anYd = myTable->item( aRow, 4 )->text();
   
    theData.append( ProfileGeoData( aProfileName, aXg, anYg, aXd, anYd ) );
  }
}

void HYDROGUI_GeoreferencementDlg::onMousePress( 
  SUIT_ViewWindow* theViewWindow, QMouseEvent* theEvent )
{
  // Check the parameters
  OCCViewer_ViewWindow* anOCCViewWindow = 
    dynamic_cast<OCCViewer_ViewWindow*>(theViewWindow);
  if ( !anOCCViewWindow || (theEvent->button() != Qt::LeftButton) ) {
    return;
  }
  
  // Check for current cell
  int aRow = myTable->currentRow();
  int aColumn = myTable->currentColumn();
  if ( aRow < 0 || aColumn <= 0 ) {
    return;
  }

  // Get the selected point coordinates
  OCCViewer_ViewPort3d* aViewPort = anOCCViewWindow->getViewPort();
  gp_Pnt aPnt = CurveCreator_Utils::ConvertClickToPoint( theEvent->x(), theEvent->y(), 
                                                         aViewPort->getView() );

  // Set the coordinates to the corresponding cells of the table
  int aColumnX = aColumn < 3 ? 1 : 3;
  int aColumnY = aColumnX + 1;
  
  QString aXStr = HYDROGUI_Tool::GetCoordinateString( aPnt.X(), false );
  QString anYStr = HYDROGUI_Tool::GetCoordinateString( aPnt.Y(), false );
  myTable->item( aRow, aColumnX )->setText( aXStr );
  myTable->item( aRow, aColumnY )->setText( anYStr );
}

void HYDROGUI_GeoreferencementDlg::onDataChanged()
{
  myIsModified = true;
}

bool HYDROGUI_GeoreferencementDlg::isModified() const
{
  return myIsModified;
}
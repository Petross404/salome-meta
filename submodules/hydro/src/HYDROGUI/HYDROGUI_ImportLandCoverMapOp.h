// Copyright (C) 2007-2015  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_IMPORTLANDCOVERMAP_H
#define HYDROGUI_IMPORTLANDCOVERMAP_H

#include "HYDROGUI_Operation.h"
#include <vector>
#include <QMap>
#include <QSet>

#include <TopTools_SequenceOfShape.hxx>
#include <HYDROData_ShapeFile.h>

class SUIT_FileDlg;
class HYDROGUI_Shape;
class TopoDS_Face;
#include <HYDROData_LandCoverMap.h>

class HYDROGUI_ImportLandCoverMapOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_ImportLandCoverMapOp( HYDROGUI_Module* theModule );
  virtual ~HYDROGUI_ImportLandCoverMapOp();

protected:
  virtual void startOperation();
  virtual void abortOperation();

  virtual bool processApply( int& theUpdateFlags, QString& theErrorMsg, QStringList& theBrowseObjectsEntries );
  HYDROGUI_InputPanel* createInputPanel() const;
  virtual void onApply();

  void erasePreview();
 
protected slots:
  void onFileSelected();
  void onSelectionChanged( const QStringList& theSelectedNames );
  void onViewerSelectionChanged ();
  void onNext( const int theIndex );

private:
  QMap<QString, HYDROGUI_Shape*> myPolygonName2PrsShape;
  TopTools_SequenceOfShape myPolygonFaces;
  HYDROData_ShapeFile myImporter;
  QString myFileName;
  QStringList myFieldList;
  std::vector<HYDROData_ShapeFile::DBF_AttrValue> myAttrValue;
  QSet<QString> mySetOfAttrValues;
  Handle(HYDROData_LandCoverMap) myLCM;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRSDRIVER_H
#define HYDROGUI_PRSDRIVER_H

#include <HYDROData_Entity.h>

class HYDROGUI_Prs;

/**
 * \class HYDROGUI_PrsDriver
 * \brief Base class of presentation driver, which allows to build a
 *        presentation on a basis of data object.
 */
class HYDROGUI_PrsDriver
{
public:
  /**
   * \brief Constructor.
   */
  HYDROGUI_PrsDriver();

  /**
   * \brief Destructor.
   */
  virtual ~HYDROGUI_PrsDriver();

public:
  /**
   * \brief Virtual method intended to update or create the presentation
   *        on a basis of data object.
   * \param theObj data object
   * \param thePrs presentation
   * \return status of the operation
   */
  virtual bool Update( const Handle(HYDROData_Entity)& theObj,
                       HYDROGUI_Prs*& thePrs );
};

#endif

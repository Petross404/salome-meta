// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_PolylineStyleOp.h>
#include <HYDROGUI_PolylineStyleDlg.h>
#include <HYDROGUI_UpdateFlags.h>
#include <AIS_InteractiveContext.hxx>

HYDROGUI_PolylineStyleOp::HYDROGUI_PolylineStyleOp( HYDROGUI_Module* theModule )
  : HYDROGUI_Operation( theModule )
{
}

HYDROGUI_PolylineStyleOp::~HYDROGUI_PolylineStyleOp()
{
}

HYDROGUI_InputPanel* HYDROGUI_PolylineStyleOp::createInputPanel() const
{
  return new HYDROGUI_PolylineStyleDlg( module() );
}

Handle(AIS_InteractiveContext) getContext( HYDROGUI_Module* theModule );

QList<Handle(HYDROGUI_Arrow)> getSelectedArrows( HYDROGUI_Module* theModule )
{
  QList<Handle(HYDROGUI_Arrow)> arrows;
  Handle(AIS_InteractiveContext) ctx = getContext( theModule );

  AIS_ListOfInteractive objs;
  ctx->DisplayedObjects( objs );
  AIS_ListIteratorOfListOfInteractive it( objs );
  for( ; it.More(); it.Next() )
  {
    Handle(HYDROGUI_Arrow) arrow = Handle(HYDROGUI_Arrow)::DownCast( it.Value() );
    Handle(AIS_InteractiveObject) obj = arrow;
    if( !obj.IsNull() && ctx->IsSelected( obj ) )
      arrows.append( arrow );
  }
  return arrows;
}

void HYDROGUI_PolylineStyleOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myArrows = getSelectedArrows( module() );

  int aCommonType=-1, aCommonSize=-1;
  bool isInit = false;
  foreach( Handle(HYDROGUI_Arrow) arrow, myArrows )
  {
    if( !isInit )
    {
      aCommonType = arrow->GetType();
      aCommonSize = arrow->GetSize();
    }
    else
    {
      if( aCommonType != -1 && aCommonType != arrow->GetType() )
        aCommonType = -1;
      if( aCommonSize != -1 && aCommonSize != arrow->GetSize() )
        aCommonSize = -1;
    }

    isInit = true;
  }

  HYDROGUI_PolylineStyleDlg* dlg = dynamic_cast<HYDROGUI_PolylineStyleDlg*>( inputPanel() );
  dlg->setStyle( aCommonType+1, aCommonSize );
}

bool HYDROGUI_PolylineStyleOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                             QStringList& theBrowseObjectsEntries )
{
  Handle(AIS_InteractiveContext) ctx = getContext( module() );

  int aType, aSize;
  HYDROGUI_PolylineStyleDlg* dlg = dynamic_cast<HYDROGUI_PolylineStyleDlg*>( inputPanel() );
  dlg->getStyle( aType, aSize );
  aType--;
  
  foreach( Handle(HYDROGUI_Arrow) arrow, myArrows )
  {
    if( aType>=0 )
      arrow->SetType( (HYDROGUI_Arrow::Type)aType );
    if( aSize>=0 )
      arrow->SetSize( aSize );
    ctx->Redisplay( arrow, Standard_False );
  }
  theUpdateFlags = UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
  return true;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_VIEWERDLG_H
#define HYDROGUI_VIEWERDLG_H

#include "HYDROGUI_InputPanel.h"
#include <Standard_Handle.hxx>

class AIS_Trihedron;
class AIS_InteractiveContext;
class QLabel;
class SUIT_ViewWindow;
class SUIT_SelectionMgr;
class OCCViewer_Viewer;
class OCCViewer_ViewManager;

class HYDROGUI_ViewerDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_ViewerDlg( HYDROGUI_Module* theModule, const QString& theTitle, bool isSplitter, bool dispTrihedron = true );
  virtual ~HYDROGUI_ViewerDlg();

  Handle(AIS_InteractiveContext) getAISContext();

  OCCViewer_Viewer*              viewer() const;
  OCCViewer_ViewManager*         viewManager() const;
  SUIT_SelectionMgr*             selectionMgr() const;

  virtual bool                   event( QEvent* );
  virtual bool                   eventFilter( QObject*, QEvent* );

protected:
  virtual Handle(AIS_Trihedron)  trihedron();

protected slots:
  void                           onMouseMove( SUIT_ViewWindow*, QMouseEvent* );

private:
  OCCViewer_ViewManager*         myViewManager;
  QLabel*                        myCoordLabel;
};

#endif

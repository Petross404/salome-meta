// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_LandCoverMapOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_Operations.h"
#include "HYDROGUI_LandCoverMapDlg.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_ShapeLandCoverMap.h"
#include "HYDROGUI_OCCSelector.h"

#include <HYDROData_Iterator.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Object.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>

#include <OCCViewer_ViewManager.h>

#include <SalomeApp_Study.h>
#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_DataOwner.h>
#include <SUIT_ViewWindow.h>
#include <SUIT_DataObject.h>

#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <AIS_Shape.hxx>

#include <QApplication>
#include <QMouseEvent>

HYDROGUI_LandCoverMapOp::HYDROGUI_LandCoverMapOp( HYDROGUI_Module* theModule, const int theOperationId )
: HYDROGUI_Operation( theModule ),
  myOperationId( theOperationId ),
  myPreviewPrs( 0 ),
  myPolylineFacePreviewPrs( 0 )
{
  switch( myOperationId )
  {
    case CreateLandCoverMapId:
      setName( tr( "CREATE_LAND_COVER_MAP" ) );
      break;
    case AddLandCoverId:
      setName( tr( "ADD_LAND_COVER" ) );
      break;
    case RemoveLandCoverId:
      setName( tr( "REMOVE_LAND_COVER" ) );
      break;
    case SplitLandCoverId:
      setName( tr( "SPLIT_LAND_COVER" ) );
      break;
    case MergeLandCoverId:
      setName( tr( "MERGE_LAND_COVER" ) );
      break;
    case ChangeLandCoverTypeId:
      setName( tr( "CHANGE_LAND_COVER_TYPE" ) );
      break;
  }  
}

HYDROGUI_LandCoverMapOp::~HYDROGUI_LandCoverMapOp()
{
  closePreview();
}

void HYDROGUI_LandCoverMapOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_LandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->blockSignals( true );

  aPanel->reset();

  // Set name of the created/edited land cover map object
  QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_LAND_COVER_MAP_NAME" ) );
  if ( myOperationId != CreateLandCoverMapId )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_LandCoverMap)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    
    // Construct a list of names of all land cover map objects defined within the data model
    QStringList aLandCoverMapNames;
    HYDROData_Iterator anIterator( doc(), KIND_LAND_COVER_MAP );
    for( ; anIterator.More(); anIterator.Next() )
    {
      Handle(HYDROData_LandCoverMap) aLandCoverObj =
        Handle(HYDROData_LandCoverMap)::DownCast( anIterator.Current() );	
      if ( !aLandCoverObj.IsNull() )
        aLandCoverMapNames.append( aLandCoverObj->GetName() );
    }
    
    //aLandCoverMapNames.sort();
    aPanel->setObjectNames( aLandCoverMapNames );

    if ( myEditedObject.IsNull() )
    {
      if ( !aLandCoverMapNames.empty() )
      {
        anObjectName = aLandCoverMapNames.first();
        if ( !anObjectName.isEmpty())
        {
          Handle(HYDROData_LandCoverMap) anObject =
            Handle(HYDROData_LandCoverMap)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), anObjectName ) );
          if( !anObject.IsNull() )
            myEditedObject = anObject;
        }
      }
    }
    else
      anObjectName = myEditedObject->GetName();
  }  
  aPanel->setObjectName( anObjectName );

  closePreview();
  if ( myOperationId != CreateLandCoverMapId )
    onCreatePreview();

  aPanel->blockSignals( false );

  module()->update( UF_OCCViewer | UF_FitAll );
}

void HYDROGUI_LandCoverMapOp::abortOperation()
{
  closePreview();

  bool aNoActiveOps = module()->getActiveOperations().isEmpty();

  HYDROGUI_Operation::abortOperation();

  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );
  if ( aStudy && !aNoActiveOps )
    module()->update( UF_OCCViewer | UF_FitAll );
}

void HYDROGUI_LandCoverMapOp::commitOperation()
{
  closePreview();

  HYDROGUI_Operation::commitOperation();

  module()->update( UF_OCCViewer | UF_FitAll );
}

HYDROGUI_InputPanel* HYDROGUI_LandCoverMapOp::createInputPanel() const
{
  HYDROGUI_LandCoverMapDlg* aPanel = new HYDROGUI_LandCoverMapDlg( module(), getName(), myOperationId );
  connect( aPanel, SIGNAL( landCoverMapChanged( const QString& ) ),
           this, SLOT( onLandCoverMapChanged( const QString& ) ) );
  connect( aPanel, SIGNAL( polylineFaceChanged() ),
           this, SLOT( onPolylineFaceChanged() ) );
  return aPanel;
}

bool HYDROGUI_LandCoverMapOp::processApply( int& theUpdateFlags,
                                            QString& theErrorMsg,
                                            QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_LandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  // Check name of the created/edited object
  QString anObjectName = aPanel->getObjectName().simplified();
  if ( anObjectName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  if ( myOperationId == CreateLandCoverMapId || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anObjectName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObjectName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObjectName );
      return false;
    }
  }

  Handle(HYDROData_PolylineXY) aPolyline;
  Handle(HYDROData_Object) aFace;

  TopTools_ListOfShape aFacesSelectedInViewer;

  // Get polyline/face selected in combo-box
  if ( myOperationId == CreateLandCoverMapId || myOperationId == AddLandCoverId || myOperationId == SplitLandCoverId )
  {
    Handle(HYDROData_Entity) aPolylineFace = aPanel->getPolylineFace();
    aPolyline = Handle(HYDROData_PolylineXY)::DownCast( aPolylineFace );
    aFace = Handle(HYDROData_Object)::DownCast( aPolylineFace );
    if ( aPolyline.IsNull() && aFace.IsNull() )
    {
      theErrorMsg = tr( "POLYLINE_FACE_NOT_DEFINED" );
      return false;
    }
  }
  // Get face(s) selected in the 3d viewer
  else if ( myOperationId == RemoveLandCoverId || 
            myOperationId == MergeLandCoverId || 
            myOperationId == ChangeLandCoverTypeId )
  {
    if ( myPreviewPrs )
      // Fill in aFacesSelectedInViewer list
      getSelectedShapes( aFacesSelectedInViewer );
  }

  // Get selected Strickler type
  QString aSelectedStricklerType;
  if ( myOperationId == CreateLandCoverMapId || 
       myOperationId == AddLandCoverId || 
       myOperationId == MergeLandCoverId ||
       myOperationId == ChangeLandCoverTypeId )
  {
    aSelectedStricklerType = aPanel->getSelectedStricklerTypeName();
    if ( aSelectedStricklerType.isEmpty() )
    {
      theErrorMsg = tr( "STRICKLER_TYPE_NOT_DEFINED" );
      return false;
    }
  }

  // Create / find the new / edited land cover map object
  Handle(HYDROData_LandCoverMap) aLandCoverMapObj = myOperationId != CreateLandCoverMapId ? myEditedObject :
    Handle(HYDROData_LandCoverMap)::DownCast( doc()->CreateObject( KIND_LAND_COVER_MAP ) );
  if ( aLandCoverMapObj.IsNull() )
  {
    theErrorMsg = tr( "LAND_COVER_MAP_UNDEFINED" );
    return false;
  }

  // Set land cover map name
  aLandCoverMapObj->SetName( anObjectName );
  
  // Add land cover to new / edited land cover map
  if ( myOperationId == CreateLandCoverMapId || myOperationId == AddLandCoverId )
  {
    bool aLandCoverAdded = false;
    if ( !aPolyline.IsNull() )
      aLandCoverAdded = aLandCoverMapObj->Add( aPolyline, aSelectedStricklerType );
    else if ( !aFace.IsNull() )
      aLandCoverAdded = aLandCoverMapObj->Add( aFace, aSelectedStricklerType );
    if ( !aLandCoverAdded )
    {
      theErrorMsg = tr( "LAND_COVER_NOT_ADDED" );
      return false;
    }
  }

  // Remove land cover from edited land cover map
  if ( myOperationId == RemoveLandCoverId )
  {
    bool aLandCoverRemoved = false;
    if ( !aFacesSelectedInViewer.IsEmpty() )
    {
      aLandCoverRemoved = aLandCoverMapObj->Remove( aFacesSelectedInViewer );
      if ( !aLandCoverRemoved )
      {
        theErrorMsg = tr( "LAND_COVER_NOT_REMOVED" );
        return false;
      }
    }
  }

  // Split land cover(s) inside edited land cover map
  if ( myOperationId == SplitLandCoverId )
  {
    bool aLandCoverSplit = false;
    if ( !aPolyline.IsNull() )
      aLandCoverSplit = aLandCoverMapObj->Split( aPolyline );
    else if ( !aFace.IsNull() )
    {
      // Get the complete boundary of the object face as the splitting polyline
      QList<TopoDS_Shape> aBoundShapes;
      QStringList aBoundNames;
      aFace->GetBoundaries( aBoundShapes, aBoundNames );

      for( int i=0, n=aBoundShapes.size(); i<n; i++ )
      {
        TopoDS_Shape aShape = aBoundShapes[i];
        if( aShape.IsNull() )
          continue;

        bool aSplitResult = aLandCoverMapObj->Split( aShape );
        aLandCoverSplit = ( i==0 ? aSplitResult : aLandCoverSplit && aSplitResult );
      }
    }
    if ( !aLandCoverSplit )
    {
      theErrorMsg = tr( "LAND_COVER_NOT_SPLIT" );
      return false;
    }
  }

  // Merge land covers inside edited land cover map
  if ( myOperationId == MergeLandCoverId )
  {
    bool aLandCoverMerged = false;
    if ( !aFacesSelectedInViewer.IsEmpty() )
    {
      aLandCoverMerged = aLandCoverMapObj->Merge( aFacesSelectedInViewer, aSelectedStricklerType );
      if ( !aLandCoverMerged )
      {
        theErrorMsg = tr( "LAND_COVER_NOT_MERGED" );
        return false;
      }
    }
  }

  // Change Strickler type for land cover(s) inside edited land cover map
  if ( myOperationId == ChangeLandCoverTypeId )
  {
    bool aLandCoverChangeType = false;
    if ( !aFacesSelectedInViewer.IsEmpty() )
    {
      aLandCoverChangeType = aLandCoverMapObj->ChangeType( aFacesSelectedInViewer, aSelectedStricklerType );
      if ( !aLandCoverChangeType )
      {
        theErrorMsg = tr( "LAND_COVER_TYPE_NOT_CHANGED" );
        return false;
      }
    }
  }
    
  // Update land cover map object and close preview
  aLandCoverMapObj->Update();

  closePreview();

  // Publish the newly created land cover map in the Object Browser
  module()->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( module() ), aLandCoverMapObj, true );
  if ( myOperationId == CreateLandCoverMapId )
  {
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aLandCoverMapObj );
    theBrowseObjectsEntries.append( anEntry );
  }

  // Update presentation of land cover object in the 3d viewer
  module()->setIsToUpdate( aLandCoverMapObj );
  module()->getOCCDisplayer()->SetToUpdateColorScale();

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  if ( myOperationId == CreateLandCoverMapId )
    module()->enableLCMActions();

  if ( myOperationId == RemoveLandCoverId || myOperationId == MergeLandCoverId || myOperationId == ChangeLandCoverTypeId )
    aPanel->updateSelectedLandCoversLabel( getNbSelected() );

  return true;
}

void HYDROGUI_LandCoverMapOp::onLandCoverMapChanged( const QString& theName )
{
  // If the edited land cover map was changed in the combo-box, update myEditedObject
  if ( myOperationId != CreateLandCoverMapId )
  {
    myEditedObject = Handle(HYDROData_LandCoverMap)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), theName ) );
    if ( !myEditedObject.IsNull() )
    {
      // Show preview of the newly selected land cover map
      closePreview();
      onCreatePreview();
    }
  }
}

void HYDROGUI_LandCoverMapOp::onPolylineFaceChanged()
{
  HYDROGUI_LandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  LightApp_DataOwner* aPolylineFaceDataOwner = NULL;
  Handle(HYDROData_Entity) aPolylineFace = aPanel->getPolylineFace();
  if ( !aPolylineFace.IsNull() )
  {
    // Select chosen polyline/face in the Object Browser, if it is not selected yet
    // (i.e. object was chosen not in the Object Browser or 3d Viewer, but in combo-box)
    aPolylineFaceDataOwner = new LightApp_DataOwner( HYDROGUI_DataObject::dataObjectEntry( aPolylineFace ) );  
    LightApp_SelectionMgr* aSelectionMgr = module()->getApp()->selectionMgr();
    if ( aSelectionMgr )
    {
      bool bIsAlreadySelected = false;
      SUIT_DataOwnerPtrList aSelectedOwners;
      aSelectionMgr->selected( aSelectedOwners );
      foreach( SUIT_DataOwner* aSUITOwner, aSelectedOwners )
      {
        if ( LightApp_DataOwner* anOwner = dynamic_cast<LightApp_DataOwner*>( aSUITOwner ) )
        {
          if ( anOwner->entry() == aPolylineFaceDataOwner->entry() )
          {
            bIsAlreadySelected = true;
            break;
          }
        }
      }
      if ( !bIsAlreadySelected )
      {
        SUIT_DataOwnerPtrList aList( true );
        aList.append( SUIT_DataOwnerPtr( aPolylineFaceDataOwner ) );
        aSelectionMgr->setSelected( aList );
      }
    }

    // Show Preview of selected polyline/face
    Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast( aPolylineFace );
    Handle(HYDROData_Object) aFace = Handle(HYDROData_Object)::DownCast( aPolylineFace );
    if ( !aPolyline.IsNull() || !aFace.IsNull() )
    {
      TopoDS_Shape aTopoDSShape;
      if ( !aPolyline.IsNull() )
        aTopoDSShape = aPolyline->GetShape();
      else
        aTopoDSShape = aFace->GetTopShape();

      OCCViewer_ViewManager* aViewManager = ::qobject_cast<OCCViewer_ViewManager*>( 
        module()->getApp()->getViewManager( OCCViewer_Viewer::Type(), true ) );
      if ( aViewManager )
      {
        if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
        {
          if ( myPolylineFacePreviewPrs )
          {
            delete myPolylineFacePreviewPrs;
            myPolylineFacePreviewPrs = 0;
          }

          size_t aViewerId = (size_t)aViewer;
          if ( !module()->isObjectVisible( aViewerId, aPolylineFace ) )
          {
            Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
            if ( !aCtx.IsNull() )
            {
              myPolylineFacePreviewPrs = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );
              aCtx->ClearSelected(true);

              myPolylineFacePreviewPrs->setBorderColor( Qt::white, false, false );
              myPolylineFacePreviewPrs->setShape( aTopoDSShape, true, true, !aPolyline.IsNull() ? AIS_WireFrame : AIS_Shaded );

              module()->update( UF_OCCViewer | UF_FitAll );
            }       
          }
        }
      }
    }
  }
}

void HYDROGUI_LandCoverMapOp::onCreatePreview()
{
  HYDROGUI_LandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QApplication::setOverrideCursor( Qt::WaitCursor );  

  LightApp_Application* anApp = module()->getApp();
  if ( !getPreviewManager() )
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                       anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager && !myPreviewPrs )
  {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
      {
        if ( myOperationId == RemoveLandCoverId || 
             myOperationId == MergeLandCoverId ||
             myOperationId == ChangeLandCoverTypeId )
        {
          disconnect(aViewManager, SIGNAL(mousePress(SUIT_ViewWindow*, QMouseEvent*)), 
            aViewer, SLOT(onMousePress(SUIT_ViewWindow*, QMouseEvent*)));
          disconnect(aViewManager, SIGNAL(mouseRelease(SUIT_ViewWindow*, QMouseEvent*)), 
            aViewer, SLOT(onMouseRelease(SUIT_ViewWindow*, QMouseEvent*)));        

          connect(aViewManager, SIGNAL(mousePress(SUIT_ViewWindow*, QMouseEvent*)), 
              this, SLOT(onMousePress(SUIT_ViewWindow*, QMouseEvent*)));
          connect(aViewManager, SIGNAL(mouseRelease(SUIT_ViewWindow*, QMouseEvent*)),
            this, SLOT(onMouseRelease(SUIT_ViewWindow*, QMouseEvent*)));

          LightApp_SelectionMgr* aSelectionMgr = module()->getApp()->selectionMgr();
          if ( aSelectionMgr )
          {
            QList<SUIT_Selector*> aSelectorList;
            aSelectionMgr->selectors( aViewManager->getType(), aSelectorList );
            QList<SUIT_Selector*>::iterator anIter, anIterEnd = aSelectorList.end();
            for( anIter = aSelectorList.begin(); anIter != anIterEnd; anIter++ )
            {
              HYDROGUI_OCCSelector* aHydroSelector = dynamic_cast<HYDROGUI_OCCSelector*>( *anIter );
              if ( aHydroSelector )
              {
                disconnect( aHydroSelector->viewer(), SIGNAL( deselection() ), aHydroSelector, SLOT( onDeselection() ) );
                connect( this, SIGNAL( deselection() ), aHydroSelector, SLOT( onDeselection() ) );
              }
            }
          }
        
          connect( this, SIGNAL( selectionChanged() ), this, SLOT( onViewerSelectionChanged() ) );
        }
        else
          connect( aViewer, SIGNAL( selectionChanged() ), this, SLOT( onViewerSelectionChanged() ) );
        myPreviewPrs = new HYDROGUI_ShapeLandCoverMap( module()->getOCCDisplayer(), aCtx, myEditedObject, getPreviewZLayer()/*, theIsScalarMapMode*/ );
      }
    }
  }

  if ( aViewManager && myPreviewPrs && !myEditedObject.IsNull() )
  {
    if ( myOperationId == RemoveLandCoverId || 
         myOperationId == MergeLandCoverId ||
         myOperationId == ChangeLandCoverTypeId )
      myPreviewPrs->setSelectionMode( AIS_Shape::SelectionMode( TopAbs_FACE ) ); 
    myPreviewPrs->update( false, false );
    
    if ( myOperationId == ChangeLandCoverTypeId )
      selectLandCoverInPreview();
  }
  
  module()->update( UF_OCCViewer | UF_FitAll );

  QApplication::restoreOverrideCursor();  
}

void HYDROGUI_LandCoverMapOp::onViewerSelectionChanged()
{
  HYDROGUI_LandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  int aNbSelected = getNbSelected();

  if ( myOperationId == RemoveLandCoverId || myOperationId == ChangeLandCoverTypeId )
    // Enable Apply, Apply and Close buttons only if at least one face (land cover) is selected in the 3d viewer
    aPanel->setApplyEnabled( aNbSelected > 0 );
  else if ( myOperationId == MergeLandCoverId )
    // Enable Apply, Apply and Close buttons only if at least two faces (land covers) are selected in the 3d viewer
    aPanel->setApplyEnabled( aNbSelected > 1 );

  if ( myOperationId == MergeLandCoverId || myOperationId == ChangeLandCoverTypeId )
  {
    if ( aNbSelected == 1 && !myEditedObject.IsNull() )
    {
      TopTools_ListOfShape aFacesSelectedInViewer;
      getSelectedShapes( aFacesSelectedInViewer );
      if ( aFacesSelectedInViewer.Extent() == 1 )
      {
        QString aType = myEditedObject->StricklerType( TopoDS::Face( aFacesSelectedInViewer.First() ) );
        if ( !aType.isEmpty() )
          aPanel->setSelectedStricklerTypeName( aType );
      }
    }
  }

  if ( myOperationId == RemoveLandCoverId || myOperationId == MergeLandCoverId || myOperationId == ChangeLandCoverTypeId )
    aPanel->updateSelectedLandCoversLabel( aNbSelected );
}

void HYDROGUI_LandCoverMapOp::onMousePress(SUIT_ViewWindow* theWindow, QMouseEvent* theEvent)
{
  myStartPnt.setX(theEvent->x()); myStartPnt.setY(theEvent->y());
}

void HYDROGUI_LandCoverMapOp::onMouseRelease(SUIT_ViewWindow* theWindow, QMouseEvent* theEvent)
{
  if (theEvent->button() != Qt::LeftButton) return;
  if (!theWindow->inherits("OCCViewer_ViewWindow")) return;

  OCCViewer_ViewWindow* aView = (OCCViewer_ViewWindow*) theWindow;
  if (!aView )
    return;

  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( !aViewManager )
    return;
  
  OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer();
  if ( !aViewer )
    return;

  Handle(AIS_InteractiveContext) aCtx = getInteractiveContext();
  if ( aCtx.IsNull() )
    return;

  myEndPnt.setX(theEvent->x()); myEndPnt.setY(theEvent->y());

  if (myStartPnt == myEndPnt)
  {
    if ( !aViewer->isPreselectionEnabled() ) {
      Handle(V3d_View) aView3d = aView->getViewPort()->getView();
      if ( !aView3d.IsNull() ) {
        aCtx->MoveTo(myEndPnt.x(), myEndPnt.y(), aView3d, true);
      }
    }

    Handle(StdSelect_ViewerSelector3d) aMainSelector = aCtx->MainSelector();
    if ( aMainSelector.IsNull() )
      return;
    const Standard_Integer aDetectedNb = aMainSelector->NbPicked();
    if ( aDetectedNb == 0 )
    {
      aCtx->ClearSelected( false );
      emit deselection();
    }

    for (Standard_Integer aDetIter = 1; aDetIter <= aDetectedNb; ++aDetIter)
    {
      Handle(SelectMgr_EntityOwner) anOwner = aMainSelector->Picked (aDetIter);
      aCtx->AddOrRemoveSelected( anOwner, Standard_False );
    }
  }
  else
  {
    aCtx->ShiftSelect(myStartPnt.x(), myStartPnt.y(),
                      myEndPnt.x(), myEndPnt.y(),
                      aView->getViewPort()->getView(), Standard_False );
  }

  aCtx->UpdateCurrentViewer();
  emit selectionChanged();  
}

void HYDROGUI_LandCoverMapOp::closePreview()
{
  if ( myPreviewPrs )
  {
    delete myPreviewPrs;
    myPreviewPrs = 0;
  }

  if ( myPolylineFacePreviewPrs )
  {
    delete myPolylineFacePreviewPrs;
    myPolylineFacePreviewPrs = 0;
  }

  HYDROGUI_LandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  if ( myOperationId == RemoveLandCoverId || myOperationId == MergeLandCoverId || myOperationId == ChangeLandCoverTypeId )
    aPanel->setApplyEnabled( false );

  if ( myOperationId == RemoveLandCoverId || 
       myOperationId == MergeLandCoverId ||
       myOperationId == ChangeLandCoverTypeId )
  {
    OCCViewer_ViewManager* aViewManager = getPreviewManager();
    if ( aViewManager )
    {
      if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
      {
        disconnect(aViewManager, SIGNAL(mousePress(SUIT_ViewWindow*, QMouseEvent*)), 
          this, SLOT(onMousePress(SUIT_ViewWindow*, QMouseEvent*)));
        disconnect(aViewManager, SIGNAL(mouseRelease(SUIT_ViewWindow*, QMouseEvent*)),
          this, SLOT(onMouseRelease(SUIT_ViewWindow*, QMouseEvent*)));
        connect(aViewManager, SIGNAL(mousePress(SUIT_ViewWindow*, QMouseEvent*)), 
          aViewer, SLOT(onMousePress(SUIT_ViewWindow*, QMouseEvent*)));
        connect(aViewManager, SIGNAL(mouseRelease(SUIT_ViewWindow*, QMouseEvent*)), 
          aViewer, SLOT(onMouseRelease(SUIT_ViewWindow*, QMouseEvent*)));  

        LightApp_SelectionMgr* aSelectionMgr = module()->getApp()->selectionMgr();
        if ( aSelectionMgr )
        {
          QList<SUIT_Selector*> aSelectorList;
          aSelectionMgr->selectors( aViewManager->getType(), aSelectorList );
          QList<SUIT_Selector*>::iterator anIter, anIterEnd = aSelectorList.end();
          for( anIter = aSelectorList.begin(); anIter != anIterEnd; anIter++ )
          {
            HYDROGUI_OCCSelector* aHydroSelector = dynamic_cast<HYDROGUI_OCCSelector*>( *anIter );
            if ( aHydroSelector )
            {
              disconnect( this, SIGNAL( deselection() ), aHydroSelector, SLOT( onDeselection() ) );
              connect( aHydroSelector->viewer(), SIGNAL( deselection() ), aHydroSelector, SLOT( onDeselection() ) );            
            }
          }
        }
      }
    }
  }
}

Handle(AIS_InteractiveContext) HYDROGUI_LandCoverMapOp::getInteractiveContext()
{
  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  Handle(AIS_InteractiveContext) aCtx = NULL;
  if ( aViewManager ) {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() ) {
      aCtx = aViewer->getAISContext();
    }
  }
  return aCtx;
}

void HYDROGUI_LandCoverMapOp::getSelectedShapes( TopTools_ListOfShape& theSelectedShapes )
{
  Handle(AIS_InteractiveContext) aCtx = getInteractiveContext();
  if ( !aCtx.IsNull() )
  {
    for ( aCtx->InitSelected(); aCtx->MoreSelected(); aCtx->NextSelected() )
    {
      TopoDS_Shape aSelectedShape = aCtx->SelectedShape();
      if ( aSelectedShape.IsNull() )
        continue;

      theSelectedShapes.Append( aSelectedShape );
    }
  }
}

int HYDROGUI_LandCoverMapOp::getNbSelected()
{
  int aNbSelected = 0;

  Handle(AIS_InteractiveContext) aCtx = getInteractiveContext();
  if ( !aCtx.IsNull() )
  {
    for ( aCtx->InitSelected(); aCtx->MoreSelected(); aCtx->NextSelected() )
    {
      TopoDS_Shape aSelectedShape = aCtx->SelectedShape();
      if ( aSelectedShape.IsNull() )
        continue;
      aNbSelected++;
    }
  }

  return aNbSelected;
}

void HYDROGUI_LandCoverMapOp::selectLandCoverInPreview()
{
  if ( myPreviewPrs && !myEditedObject.IsNull() && myEditedObject->GetLCCount() == 1 )
  {
    OCCViewer_ViewManager* aViewManager = getPreviewManager();
    if ( !aViewManager )
      return;
    
    Handle(AIS_InteractiveContext) aCtx = getInteractiveContext();
    if ( aCtx.IsNull() )
      return;
    
    OCCViewer_ViewWindow* aViewWindow = (OCCViewer_ViewWindow*)aViewManager->getActiveView();
    if ( !aViewWindow )
      return;

    OCCViewer_ViewPort3d* aViewPort = aViewWindow->getViewPort();
    if ( !aViewPort )
      return;

    Handle(V3d_View) aView = aViewPort->getView();
    if ( aView.IsNull() )
      return;

    aCtx->ShiftSelect( 0, 0, aViewPort->width(), aViewPort->height(), aView, Standard_False );
    aCtx->UpdateCurrentViewer();
    emit selectionChanged();
  }
}

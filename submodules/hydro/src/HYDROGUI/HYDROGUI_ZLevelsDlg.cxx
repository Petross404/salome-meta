// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ZLevelsDlg.h"

#include "HYDROGUI_OrderedListWidget.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_ListSelector.h"

#include "LightApp_Application.h"
#include "LightApp_SelectionMgr.h"

#include <QCheckBox>
#include <QLayout>
#include <QPushButton>


/**
  Constructor.
  @param theParent the parent widget
*/
HYDROGUI_ZLevelsDlg::HYDROGUI_ZLevelsDlg( QWidget* theParent, HYDROGUI_Module* theModule )
: QDialog( theParent )
{
  // Change windows flags to remove minimize button
  Qt::WindowFlags aFlags = windowFlags();
  aFlags |= Qt::CustomizeWindowHint;
  aFlags &= ~Qt::WindowMinimizeButtonHint;
  aFlags &= ~Qt::WindowMaximizeButtonHint;
  setWindowFlags( aFlags );

  // Dialog title
  setWindowTitle( tr( "CHANGE_LAYER_ORDER" ) );

  // Main layout
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin( 5 );
  aMainLayout->setSpacing( 5 );

  // Ordered list widget
  myListWidget = new HYDROGUI_OrderedListWidget( this );

  // "All objects" check box
  myAllObjects = new QCheckBox( tr( "ALL_OBJECTS" ) );

  // Apply and close buttons
  myApplyAndClose = new QPushButton( tr("APPLY_AND_CLOSE") );
  myApplyAndClose->setDefault( true );
  myApply = new QPushButton( tr("APPLY") );
  myClose = new QPushButton( tr("CLOSE") );
  
  // Layout
  // apply and close buttons
  QHBoxLayout* aDlgButtonsLayout = new QHBoxLayout(); 
  aDlgButtonsLayout->addWidget( myApplyAndClose );
  aDlgButtonsLayout->addWidget( myApply );
  aDlgButtonsLayout->addWidget( myClose );
  aDlgButtonsLayout->addStretch();
  // main
  aMainLayout->addWidget( myListWidget );
  aMainLayout->addWidget( myAllObjects );
  aMainLayout->addLayout( aDlgButtonsLayout );

  // Connections
  connect( myAllObjects, SIGNAL( stateChanged( int ) ), this, SLOT( onStateChanged() ) );

  connect( myApplyAndClose, SIGNAL( clicked() ), this, SIGNAL( applyOrderAndClose() ) );
  connect( myApply, SIGNAL( clicked() ), this, SIGNAL( applyOrder() ) );
  connect( myClose, SIGNAL( clicked() ), this, SLOT( reject() ) );

  // Initialize
  onStateChanged();

  // Create selector
  if ( theModule ) {
    HYDROGUI_ListSelector* aListSelector = 
      new HYDROGUI_ListSelector( myListWidget, theModule->getApp()->selectionMgr() );
    aListSelector->setAutoBlock( true );
  }
}

/**
  Destructor.
*/
HYDROGUI_ZLevelsDlg::~HYDROGUI_ZLevelsDlg()
{
}

/**
  Set the list of objects (which supposed to be ordered by the dialog).
  @param theObjects the list of objects
*/
void HYDROGUI_ZLevelsDlg::setObjects( const HYDROGUI_ListModel::Object2VisibleList& theObjects )
{
  myListWidget->setObjects( theObjects );
}

/**
  Returns the ordered list of objects.
  @return the list of objects
*/
QList<Handle(HYDROData_Entity)> HYDROGUI_ZLevelsDlg::getObjects() const
{
  return myListWidget->getObjects();
}

/**
  Slot called on "All objects" check box state change.
*/
void HYDROGUI_ZLevelsDlg::onStateChanged()
{
  bool isAll = myAllObjects->isChecked();

  QString aToolTip = isAll ? tr( "ALL_OBJECTS_CHECKED_TLT" ) :
                             tr( "ALL_OBJECTS_UNCHECKED_TLT" );
  myAllObjects->setToolTip( aToolTip );

  myListWidget->setHiddenObjectsShown( isAll );
}

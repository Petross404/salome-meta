// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ACTOR_H
#define HYDROGUI_ACTOR_H

#include <GEOM_Actor_patched.h>

class HYDROGUI_Actor : public GEOM_Actor
{
public:
  vtkTypeMacro( HYDROGUI_Actor, GEOM_Actor );
  static HYDROGUI_Actor* New();

  virtual void SetTransform( VTKViewer_Transform* theTransform );

  void SetShape(const TopoDS_Shape& theShape,
                float theDeflection,
                bool theIsVector = false);

  protected:
  virtual void SetMapper(vtkMapper*);

  HYDROGUI_Actor();
  ~HYDROGUI_Actor();
};

#endif

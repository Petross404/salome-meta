// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ExportSinusXOp.h>
#include <HYDROGUI_ExportSinusXDlg.h>
#include <HYDROData_Document.h>
#include <HYDROGUI_UpdateFlags.h>
#include <HYDROGUI_Tool.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Entity.h>
#include <HYDROGUI_ObjListBox.h>
#include <HYDROGUI_Operation.h>
#include "HYDROGUI_Module.h"
#include <HYDROData_SinusX.h>

#include <SUIT_FileDlg.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>

#include <LightApp_Application.h>
#include <QApplication>

HYDROGUI_ExportSinusXOp::HYDROGUI_ExportSinusXOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "EXPORT_SINUSX" ) );
}

HYDROGUI_ExportSinusXOp::~HYDROGUI_ExportSinusXOp()
{
}

void HYDROGUI_ExportSinusXOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_ExportSinusXDlg* aPanel = ::qobject_cast<HYDROGUI_ExportSinusXDlg*>( inputPanel() );
  if ( !aPanel )
    return;
}

HYDROGUI_InputPanel* HYDROGUI_ExportSinusXOp::createInputPanel() const
{
  HYDROGUI_ExportSinusXDlg* aPanel = new HYDROGUI_ExportSinusXDlg( module(), getName() );
  connect( aPanel, SIGNAL( ExportItems() ), SLOT( onExportItems() ) );
  return aPanel;
}

void HYDROGUI_ExportSinusXOp::onExportItems()
{
  QString aFilter( tr("SINUSX_FILTER") ); 
  HYDROGUI_ExportSinusXDlg* aPanel = ::qobject_cast<HYDROGUI_ExportSinusXDlg*>( inputPanel() );
  if ( !aPanel )
    return;
  if (!aPanel->GetSelectedEntities().IsEmpty())
  {
    //Export selected items to SX file
	QString firstName = aPanel->GetSelectedEntities().First()->GetName(); // name of the fist object is used as a proposal for the filename
	firstName = firstName + ".sx";
    QString aFileName = SUIT_FileDlg::getFileName( module()->getApp()->desktop(), firstName, aFilter, tr( "EXPORT_SINUSX" ), false );
    HYDROData_SinusX anExporter;
    anExporter.Export(aFileName, aPanel->GetSelectedEntities());
  }
  else
    SUIT_MessageBox::warning(module()->getApp()->desktop(), tr( "EXPORT_SINUSX" ), tr( "NO_ENTITIES_TO_EXPORT"));
}

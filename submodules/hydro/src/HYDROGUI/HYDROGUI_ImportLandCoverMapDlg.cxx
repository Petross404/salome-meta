// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportLandCoverMapDlg.h"

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QVBoxLayout>
#include <QToolButton>
#include <QCheckBox>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SUIT_FileDlg.h>
#include <HYDROGUI_Module.h>
#include <SUIT_MessageBox.h>
#include <LightApp_Application.h>
#include <QTableWidget>
#include <QHeaderView>
#include <QComboBox>
#include <QStackedWidget>
#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>
#include <QSignalMapper>

HYDROGUI_ImportLandCoverMapDlg::HYDROGUI_ImportLandCoverMapDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_Wizard( theModule, theTitle )
{
  addPage( createPage1() );
  addPage( createPage2() );
  addPage( createPage3() );
  myFirstPageState = false;
  myDbfState = false;
  myUpdateViewerState = false;
}

QWizardPage* HYDROGUI_ImportLandCoverMapDlg::createPage1() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();

  myFileNameGroup = new QGroupBox( tr( "IMPORT_LANDCOVER_MAP_FROM_FILE" ) );

  QLabel* aFileNameLabel = new QLabel( tr( "FILE_NAME" ), myFileNameGroup );

  myFileName = new QLineEdit( myFileNameGroup );
  myFileName->setReadOnly( true );

  QToolButton* aBrowseBtn = new QToolButton( myFileNameGroup );
  aBrowseBtn->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "BROWSE_ICO" ) ) );

  QBoxLayout* aFileNameLayout = new QHBoxLayout( myFileNameGroup );
  aFileNameLayout->setMargin( 5 );
  aFileNameLayout->setSpacing( 5 );
  aFileNameLayout->addWidget( aFileNameLabel );
  aFileNameLayout->addWidget( myFileName );
  aFileNameLayout->addWidget( aBrowseBtn );

  myObjectNameGroup = new QGroupBox( tr( "LANDCOVERMAP_NAME" ) );

  QLabel* aLandcoverNameLabel = new QLabel( tr( "NAME" ), myObjectNameGroup );
  myObjectName = new QLineEdit( myObjectNameGroup );

  QBoxLayout* aLandcoverNameLayout = new QHBoxLayout( myObjectNameGroup );
  aLandcoverNameLayout->setMargin( 5 );
  aLandcoverNameLayout->setSpacing( 5 );
  aLandcoverNameLayout->addWidget( aLandcoverNameLabel );
  aLandcoverNameLayout->addWidget( myObjectName );

  QGroupBox* aPolygonsGroup = new QGroupBox( tr( "FOUND_POLYGONS" ) );
  myPolygonsListWidget = new QListWidget( aPolygonsGroup );
  myPolygonsListWidget->setSelectionMode( QListWidget::ExtendedSelection );
  myPolygonsListWidget->setEditTriggers( QListWidget::NoEditTriggers );
  myPolygonsListWidget->setViewMode( QListWidget::ListMode );
  myPolygonsListWidget->setSortingEnabled( false );

  QBoxLayout* aPolygonsLayout = new QVBoxLayout;
  aPolygonsLayout->addWidget( myPolygonsListWidget );
  aPolygonsGroup->setLayout( aPolygonsLayout );

  QVBoxLayout* aPageLayout = new QVBoxLayout;
  myAttrCheckBox = new QCheckBox( tr("USE_DBF_AS_ST") );
  myAttrCheckBox->setChecked(true);

  // Layout
  aPageLayout->setMargin( 5 );
  aPageLayout->setSpacing( 5 );
  aPageLayout->addWidget( myFileNameGroup );
  aPageLayout->addWidget( myAttrCheckBox );
  aPageLayout->addWidget( myObjectNameGroup );
  aPageLayout->addWidget( aPolygonsGroup );
  aPage->setLayout( aPageLayout );

  // Conections
  connect( myPolygonsListWidget, SIGNAL( itemSelectionChanged() ), this, SLOT( onItemSelectionChanged() ) );
  connect( aBrowseBtn, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
  connect( myAttrCheckBox, SIGNAL(clicked(bool)), this, SLOT(onAttrCBChecked(bool)));

  return aPage;
}

QWizardPage* HYDROGUI_ImportLandCoverMapDlg::createPage2() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  myAvAttrLabel = new QLabel();
  myAvAttrLabel->setText(tr("AV_ATTRS"));

  myDBFAttrListWidget = new QListWidget( );
  myDBFAttrListWidget->setSelectionMode( QListWidget::SingleSelection );
  myDBFAttrListWidget->setEditTriggers( QListWidget::NoEditTriggers );
  myDBFAttrListWidget->setViewMode( QListWidget::ListMode );
  myDBFAttrListWidget->setSortingEnabled( false );

  // Layout
  QVBoxLayout* aPageLayout = new QVBoxLayout;
  aPageLayout->setMargin( 5 );
  aPageLayout->setSpacing( 5 );
  aPageLayout->addWidget( myAvAttrLabel );
  aPageLayout->addWidget( myDBFAttrListWidget );
  aPage->setLayout( aPageLayout );

  return aPage;
}

QWizardPage* HYDROGUI_ImportLandCoverMapDlg::createPage3() {

  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );
  myCorrLabel = new QLabel();
  myCorrLabel->setText(tr ("CORR"));

  myCorrTableWidget = new QTableWidget();

  // Layout
  QVBoxLayout* aPageLayout = new QVBoxLayout;
  aPageLayout->setMargin( 5 );
  aPageLayout->setSpacing( 5 );
  aPageLayout->addWidget( myCorrLabel );
  aPageLayout->addWidget( myCorrTableWidget );
  aPage->setLayout( aPageLayout );
  return aPage;
}


HYDROGUI_ImportLandCoverMapDlg::~HYDROGUI_ImportLandCoverMapDlg()
{
  myPolygonsListWidget->clear();
  myDBFAttrListWidget->clear();
}

void HYDROGUI_ImportLandCoverMapDlg::reset()
{
  myPolygonsListWidget->clear();
  myDBFAttrListWidget->clear();
}

void HYDROGUI_ImportLandCoverMapDlg::setPolygonNames( const QStringList& theNames )
{
  myPolygonsListWidget->clear();
  myPolygonsListWidget->addItems( theNames );
}

void HYDROGUI_ImportLandCoverMapDlg::setAttributeNames( const QStringList& theAttrNames )
{
  myDBFAttrListWidget->clear();
  myDBFAttrListWidget->addItems( theAttrNames );
}

void HYDROGUI_ImportLandCoverMapDlg::setSelectedPolygonNames( const QStringList& theNames )
{
  myPolygonsListWidget->clearSelection();

  foreach( const QString aName, theNames ) {
    QList<QListWidgetItem*> anItems = myPolygonsListWidget->findItems( aName, Qt::MatchExactly );
    if ( anItems.count() == 1 ) {
      anItems.first()->setSelected( true );
    }
  }
}

void HYDROGUI_ImportLandCoverMapDlg::onItemSelectionChanged()
{
  int aCurIndex = getCurrentWizardIndex();
  if (aCurIndex == 0 && getViewerState())
    emit selectionChanged( getSelectedPolygonNames() );
}

QStringList HYDROGUI_ImportLandCoverMapDlg::getSelectedPolygonNames() const
{
  QStringList aSelectedNames;

  QList<QListWidgetItem*> aSelectedItems = myPolygonsListWidget->selectedItems();
  foreach( const QListWidgetItem* anItem, aSelectedItems )
    aSelectedNames << anItem->text();

  return aSelectedNames;
}

void HYDROGUI_ImportLandCoverMapDlg::onBrowse()
{
  QString aFilter( tr( "LANDCOVERMAP_FILTER" ) );
  QString aFileName = SUIT_FileDlg::getFileName( this, "", aFilter, tr( "IMPORT_LANDCOVER_MAP_FROM_FILE" ), true );

  if( !aFileName.isEmpty() )
  {
    setFileName( aFileName );
    emit FileSelected( aFileName );
  }
}

void HYDROGUI_ImportLandCoverMapDlg::setObjectName( const QString& theName )
{
  myObjectName->setText( theName );
  myObjectNameGroup->setEnabled( !theName.isEmpty() || !myFileName->text().isEmpty() );
}

QString HYDROGUI_ImportLandCoverMapDlg::getObjectName() const
{
  return myObjectName->text();
}

void HYDROGUI_ImportLandCoverMapDlg::setFileName( const QString& theFileName )
{
  myFileName->setText( theFileName );

  if ( !myObjectNameGroup->isEnabled() )
    myObjectNameGroup->setEnabled( !theFileName.isEmpty() );
}

QString HYDROGUI_ImportLandCoverMapDlg::getFileName() const
{
  return myFileName->text();
}


void HYDROGUI_ImportLandCoverMapDlg::FillCorrespondenceTable(const QStringList& theFirstColumn,
                                                   const QStringList& theSecondColumn,
                                                   const QVector<int> theDefCBIndices,
                                                   const QVector<QColor> theColors)
{
  int aFCSize = theFirstColumn.size();  // == theDefCBIndices.size() !
  int aSCSize = theSecondColumn.size();

  myStrColors = theColors;

  myCorrTableWidget->setRowCount(aFCSize);
  myCorrTableWidget->setColumnCount(3);
  QTableWidgetItem* aHeader_1 = new QTableWidgetItem(QString(tr("TABLE_H1")), QTableWidgetItem::Type);
  QTableWidgetItem* aHeader_2 = new QTableWidgetItem(QString(tr("TABLE_H2")), QTableWidgetItem::Type);
  QTableWidgetItem* aHeader_3 = new QTableWidgetItem(QString(tr("TABLE_H3")), QTableWidgetItem::Type);
  myCorrTableWidget->setHorizontalHeaderItem(0, aHeader_1);
  myCorrTableWidget->setHorizontalHeaderItem(1, aHeader_2);
  myCorrTableWidget->setHorizontalHeaderItem(2, aHeader_3);
  myCorrTableWidget->horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
  //
  for (int i = 0; i < aFCSize; i++)
  {
    QTableWidgetItem* aTWI = new QTableWidgetItem();
    aTWI->setText(theFirstColumn.at(i));
    aTWI->setFlags(Qt::ItemIsUserCheckable);
    myCorrTableWidget->setItem(i, 0, aTWI);
  }
  //
  QSignalMapper* signalMap = new QSignalMapper();
  for (int i = 0; i < aFCSize; i++)
  {
    QComboBox* aCB = new QComboBox();
    connect(aCB, SIGNAL(currentIndexChanged(int)), signalMap, SLOT(map()));
    signalMap->setMapping(aCB, i);
    aCB->addItems(theSecondColumn);
    aCB->setCurrentIndex(theDefCBIndices[i]);
    myCorrTableWidget->setCellWidget(i, 1, aCB);
  }
  connect(signalMap, SIGNAL(mapped(int)), this, SLOT(onComboBoxColorChanged(int)));

  //
  for (int i = 0; i < aFCSize; i++)
  {
    QTableWidgetItem* aTWI = new QTableWidgetItem;
    aTWI->setBackground(myStrColors[theDefCBIndices[i]]);
    aTWI->setFlags(Qt::NoItemFlags);
    myCorrTableWidget->setItem(i, 2, aTWI);
  }

}

QString HYDROGUI_ImportLandCoverMapDlg::getSelectedFieldName() const
{
  return myDBFAttrListWidget->selectedItems().first()->text();
}

void HYDROGUI_ImportLandCoverMapDlg::setFirstPageState(bool theState)
{
  myFirstPageState = theState;
}

bool HYDROGUI_ImportLandCoverMapDlg::getFirstPageState() const
{
  return myFirstPageState;
}


bool HYDROGUI_ImportLandCoverMapDlg::acceptCurrent() const
{
  //Check the state of the current page
  int aCurrPage = getCurrentWizardIndex();
  switch ( aCurrPage )
  {
    case 0:
    {
      //this method verifies first page
      if (!CheckFirstPageFilling())
        return false;
      break;
    }
    case 1:
    {
      if (myDBFAttrListWidget->selectedItems().empty())
      {
        SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "LCM_IMPORT_WARNING" ), tr ("ATTRS_ISNT_SELECTED"));
        return false;
      }
      break;
    }
    case 2: //last page
    {
      if (this->isPolygonListEmpty())
      {
        SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "LCM_IMPORT_WARNING" ), tr("POLYGONS_ISNT_SELECTED"));
        return false;
      }
      break;
    }
    default:
     return false;
  }
  return true;
}

void HYDROGUI_ImportLandCoverMapDlg::getValAttr2StricklerTypeCorr(QStringList& theAttrValues, QStringList& theST)
{
  int aRowCount = myCorrTableWidget->rowCount();
  for (int i = 0; i < aRowCount; i++)
  {
    QTableWidgetItem* aTWI = myCorrTableWidget->item(i, 0);
    theAttrValues.push_back(aTWI->text());
  }

  for (int i = 0; i < aRowCount; i++)
  {
    QWidget* aW = myCorrTableWidget->cellWidget(i, 1);
    QComboBox* aCB = dynamic_cast<QComboBox*> (aW);
    theST.push_back(aCB->currentText());
  }
}


QVector<int> HYDROGUI_ImportLandCoverMapDlg::getSelectedPolygonIndices() const
{
  QVector<int> aSelectedIndices;

  QModelIndexList aSelectedItems = myPolygonsListWidget->selectionModel()->selectedIndexes();
  foreach( QModelIndex index, aSelectedItems ) {
    aSelectedIndices << index.row();
  }

  return aSelectedIndices;
}

void HYDROGUI_ImportLandCoverMapDlg::onAttrCBChecked( bool theState )
{
  if (theState)
    SetButtonsState ( false );
  else
    SetButtonsState ( true );
}

bool HYDROGUI_ImportLandCoverMapDlg::getAttrCheckBoxState() const
{
  return myAttrCheckBox->isChecked();
}

void HYDROGUI_ImportLandCoverMapDlg::onComboBoxColorChanged(int theInd)
{
  QComboBox* CB = qobject_cast<QComboBox*> (myCorrTableWidget->cellWidget(theInd, 1));
  int aCurIndOfCB = CB->currentIndex();
  myCorrTableWidget->item(theInd, 2)->setBackground(myStrColors[aCurIndOfCB]);
}

void HYDROGUI_ImportLandCoverMapDlg::setDbfState(bool theState)
{
  myDbfState = theState;
}

bool HYDROGUI_ImportLandCoverMapDlg::getDbfState() const
{
  return myDbfState;
}

void HYDROGUI_ImportLandCoverMapDlg::setDbfRecordsNb(int theRecordsNbs)
{
  myDbfRecordsNbs = theRecordsNbs;
}

int HYDROGUI_ImportLandCoverMapDlg::getDbfRecordsNb() const
{
  return myDbfRecordsNbs;
}

int HYDROGUI_ImportLandCoverMapDlg::getCurrentWizardIndex() const
{
  return wizard()->currentIndex();
}

void HYDROGUI_ImportLandCoverMapDlg::setViewerState(bool theState)
{
  myUpdateViewerState = theState;
}

bool HYDROGUI_ImportLandCoverMapDlg::getViewerState() const
{
  return myUpdateViewerState;
}

bool HYDROGUI_ImportLandCoverMapDlg::isPolygonListEmpty() const
{
  return myPolygonsListWidget->selectedItems().empty();
}

bool HYDROGUI_ImportLandCoverMapDlg::CheckFirstPageFilling() const
{
  if (!this->getFirstPageState())
  {
    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "LCM_IMPORT_WARNING" ), tr("FILE_ISNT_CHOSEN"));
    return false;
  }
  if (this->isPolygonListEmpty())
  {
    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "LCM_IMPORT_WARNING" ), tr("POLYGONS_ISNT_SELECTED"));
    return false;
  }
  if (this->getAttrCheckBoxState() && !this->getDbfState())
  {
     SUIT_MessageBox::critical( module()->getApp()->desktop(), tr( "DBF_LOAD_ERROR" ), tr("DBF_LOAD_ERR_MESS"));
     return false;
  }
  if (this->getAttrCheckBoxState() && this->getDbfRecordsNb() == 0)
  {
     SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "DBF_NB_RECORDS_IS_NULL" ), tr("DBF_NB_RECORDS_IS_NULL_MESS"));
     return false;
  }
  return true;
}

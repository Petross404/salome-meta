// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VTKPrsShape.h"

#include <HYDROData_Entity.h>
#include <HYDROData_Channel.h>
#include <HYDROData_Document.h>
#include <HYDROData_DummyObject3D.h>
#include <HYDROData_Image.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_BCPolygon.h>
#include <HYDROData_Obstacle.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Region.h>
#include <HYDROData_Stream.h>
#include <HYDROData_Zone.h>

#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>

#include <gp_Pnt.hxx>

#include <Graphic3d_AspectFillArea3d.hxx>
#include <Graphic3d_MaterialAspect.hxx>

#include <TopoDS.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Face.hxx>

#include <TopExp_Explorer.hxx>

#include <BRep_Builder.hxx>

#include <Precision.hxx>

#include <HYDROGUI_Actor.h>
#include <vtkScalarBarActor.h>

#include <QString>

// Hard-coded value of shape deflection coefficient for VTK viewer
const double VTK_MIN_DEFLECTION = 0.001;


//=======================================================================
// name    : HYDROGUI_VTKPrsShape
// Purpose : Constructor
//=======================================================================
HYDROGUI_VTKPrsShape::HYDROGUI_VTKPrsShape( const Handle(HYDROData_Entity)& theObject )
: HYDROGUI_VTKPrs( theObject ),
myDisplayMode( GEOM_Actor::eWireframe )
{
}

//=======================================================================
// name    : HYDROGUI_VTKPrsShape
// Purpose : Destructor
//=======================================================================
HYDROGUI_VTKPrsShape::~HYDROGUI_VTKPrsShape()
{
}

//================================================================
// Function : compute
// Purpose  : 
//================================================================
void HYDROGUI_VTKPrsShape::compute()
{
  HYDROGUI_VTKPrs::compute();

  if ( !getObject().IsNull() )
  {
    buildShape();

    if ( !myTopoShape.IsNull() )
    {
      HYDROGUI_Actor* anActor = getActor<HYDROGUI_Actor>(this);
      anActor->SetShape( myTopoShape, VTK_MIN_DEFLECTION );
      anActor->setDisplayMode( myDisplayMode );
      anActor->setIO( getIO() );
    }
  }
}


void HYDROGUI_VTKPrsShape::buildShape()
{
  Handle(HYDROData_Entity) anObject = getObject();
  // Try to retrieve information from object
  if ( !anObject.IsNull() )
  {
    Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  
    if ( anObject->IsKind( STANDARD_TYPE(HYDROData_ImmersibleZone) ) )
    {
      Handle(HYDROData_ImmersibleZone) aZoneObj =
        Handle(HYDROData_ImmersibleZone)::DownCast( anObject );

      TopoDS_Shape aZoneShape = aZoneObj->GetTopShape();
      if ( !aZoneShape.IsNull() ) {
        if ( aZoneShape.ShapeType() == TopAbs_FACE ) {
          TopoDS_Face aZoneFace = TopoDS::Face( aZoneShape );
          setFace( aZoneFace, false, false );
        } else {
          myTopoShape = aZoneShape;
        }
      }

      QColor aFillingColor = aZoneObj->GetFillingColor();
      QColor aBorderColor = aZoneObj->GetBorderColor();

      //setFillingColor( aFillingColor, false, false );
      //setBorderColor( aBorderColor, false, false );
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_PolylineXY) ) )
    {
      Handle(HYDROData_PolylineXY) aPolyline =
        Handle(HYDROData_PolylineXY)::DownCast( anObject );

      TopoDS_Shape aPolylineShape = aPolyline->GetShape();

      if ( !aPolylineShape.IsNull() ) {
        if ( aPolylineShape.ShapeType() == TopAbs_WIRE ) {
          TopoDS_Wire aPolylineWire = TopoDS::Wire( aPolylineShape );
          setWire( aPolylineWire, false, false );  
        } else {
          myTopoShape = aPolylineShape;
          myDisplayMode = GEOM_Actor::eWireframe;
        }
      }

      //QColor aWireColor = aPolyline->GetWireColor();
      //setBorderColor( aWireColor, false, false );
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_Polyline3D) ) )
    {
      Handle(HYDROData_Polyline3D) aPolyline =
        Handle(HYDROData_Polyline3D)::DownCast( anObject );

      TopoDS_Shape aPolylineShape = aPolyline->GetShape3D();

      if ( !aPolylineShape.IsNull() ) {
        if ( aPolylineShape.ShapeType() == TopAbs_WIRE ) {
          TopoDS_Wire aPolylineWire = TopoDS::Wire( aPolylineShape );
          setWire( aPolylineWire, false, false );  
        } else {
          myTopoShape = aPolylineShape;
          myDisplayMode = GEOM_Actor::eWireframe;
        }
      }

      QColor aWireColor = aPolyline->GetBorderColor();
      //setBorderColor( aWireColor, false, false );
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_Zone) ) )
    {
      Handle(HYDROData_Zone) aZone =
        Handle(HYDROData_Zone)::DownCast( anObject );

      TopoDS_Face aZoneFace = TopoDS::Face( aZone->GetShape() );

      setFace( aZoneFace, false, false );
      if (aZone->IsMergingNeed() && aZone->GetMergeType() == HYDROData_Zone::Merge_UNKNOWN )
      {
        // Red color for a zone with bathymetry conflict
        //setFillingColor( Qt::red );
      }
      else
      {
        // Generate the filling color for zone
        QStringList anObjectsNames;

        HYDROData_SequenceOfObjects aRefObjects = aZone->GetObjects();
        HYDROData_SequenceOfObjects::Iterator anIter( aRefObjects );
        for ( ; anIter.More(); anIter.Next() )
        {
          Handle(HYDROData_Entity) aRefbject = anIter.Value();
          if ( aRefbject.IsNull() )
            continue;

          QString aRefObjectName = aRefbject->GetName();
          if ( aRefObjectName.isEmpty() )
            continue;

          anObjectsNames.append( aRefObjectName );
        }

        //setFillingColor( HYDROGUI_Tool::GenerateFillingColor( aDocument, aGeomObjectsNames ) );
      }
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_Image) ) )
    {
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_Profile) ) )
    {
      Handle(HYDROData_Profile) aProfile =
        Handle(HYDROData_Profile)::DownCast( anObject );

      TopoDS_Wire aProfileWire;

      if ( aProfile->IsValid() ) {
        TopoDS_Shape aProfileShape = aProfile->GetShape3D();

        if ( !aProfileShape.IsNull() && 
             aProfileShape.ShapeType() == TopAbs_WIRE ) {
          aProfileWire = TopoDS::Wire( aProfileShape );
        }
      }

      setWire( aProfileWire, false, false );  

      QColor aWireColor = aProfile->GetBorderColor();
      //setBorderColor( aWireColor, false, false );
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_Stream) ) ||
              anObject->IsKind( STANDARD_TYPE(HYDROData_Channel) ) ||
              anObject->IsKind( STANDARD_TYPE(HYDROData_Obstacle) ) )
    {
      Handle(HYDROData_Object) aGeomObject =
        Handle(HYDROData_Object)::DownCast( anObject );

      TopoDS_Shape anObjShape = aGeomObject->GetTopShape();

      setShape( anObjShape, false, false );

      QColor aFillingColor = aGeomObject->GetFillingColor();
      QColor aBorderColor = aGeomObject->GetBorderColor();

      //setFillingColor( aFillingColor, false, false );
      //setBorderColor( aBorderColor, false, false );
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_DummyObject3D) ) )
    {
      Handle(HYDROData_DummyObject3D) anObject3D =
        Handle(HYDROData_DummyObject3D)::DownCast( anObject );
      TopoDS_Shape aShape3D = anObject3D->GetShape();

      setShape( aShape3D, false, false );

      QColor aFillingColor = anObject3D->GetFillingColor();
      QColor aBorderColor = anObject3D->GetBorderColor();

      //setFillingColor( aFillingColor, false, false );
      //setBorderColor( aBorderColor, false, false );
    }
    else if ( anObject->IsKind( STANDARD_TYPE(HYDROData_BCPolygon) ) )
    {
      Handle(HYDROData_BCPolygon) aBCObj =
        Handle(HYDROData_BCPolygon)::DownCast( anObject );

      TopoDS_Shape aBCShape = aBCObj->GetTopShape();
      if ( !aBCShape.IsNull() ) 
      {
        if ( aBCShape.ShapeType() == TopAbs_FACE )
        {
          TopoDS_Face aFace = TopoDS::Face( aBCShape );
          setFace( aFace, false, false );
        }
        else 
        {
          myTopoShape = aBCShape;
        }
      }

      QColor aFillingColor = aBCObj->GetFillingColor();
      QColor aBorderColor = aBCObj->GetBorderColor();
    }
 
  }
}

void HYDROGUI_VTKPrsShape::setWire( const TopoDS_Wire& theWire,
                                    const bool         theToDisplay,
                                    const bool         theIsUpdateViewer )
{
  myTopoShape = theWire;
  myDisplayMode = GEOM_Actor::eWireframe;
}

void HYDROGUI_VTKPrsShape::setFaces( const TopoDS_Compound& theWires,
                                     const bool             theToDisplay,
                                     const bool             theIsUpdateViewer )
{
  TopExp_Explorer anExp( theWires, TopAbs_WIRE );
  TopoDS_Compound aCompound;
  BRep_Builder aBuilder;
    aBuilder.MakeCompound( aCompound );

  for ( ; anExp.More(); anExp.Next() ) {
    TopoDS_Wire aWire = TopoDS::Wire( anExp.Current() );
    if ( aWire.IsNull() ) {
      continue;
    }

    BRepBuilderAPI_MakeFace aMakeFace( aWire, Standard_True );
    aMakeFace.Build();
    if( aMakeFace.IsDone() ) {
      aBuilder.Add( aCompound, aMakeFace.Face() );
    }
  }

  myTopoShape = aCompound;
  //myDisplayMode = GEOM_Actor::eShading;
}

void HYDROGUI_VTKPrsShape::setFace( const TopoDS_Wire& theWire,
                                    const bool         theToDisplay,
                                    const bool         theIsUpdateViewer )
{
  BRepBuilderAPI_MakeFace aFaceBuilder( theWire, Standard_True );
  aFaceBuilder.Build();
  if( aFaceBuilder.IsDone() )
  {
    TopoDS_Face aFace = aFaceBuilder.Face();
    setFace( aFace, theToDisplay, theIsUpdateViewer );
  }
}

void HYDROGUI_VTKPrsShape::setFace( const TopoDS_Face& theFace,
                                    const bool         theToDisplay,
                                    const bool         theIsUpdateViewer )
{
  myTopoShape = theFace;
  //myDisplayMode = GEOM_Actor::eShading;
}

void HYDROGUI_VTKPrsShape::setShape( const TopoDS_Shape& theShape,
                                     const bool          theToDisplay,
                                     const bool          theIsUpdateViewer )
{
  myTopoShape = theShape;
  //myDisplayMode = GEOM_Actor::eShading;
}

// Copyright (C) 2007-2015  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportLandCoverMapOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_ImportLandCoverMapDlg.h"
#include "HYDROGUI_Shape.h"
#include <HYDROGUI_ZLayers.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_ShapeFile.h>
#include <HYDROData_Profile.h>
#include <HYDROData_LandCoverMap.h>
#include <HYDROData_StricklerTable.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>
#include <SUIT_ViewManager.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <SalomeApp_Study.h>
#include <LightApp_Application.h>
#include <LightApp_DataOwner.h>
#include <LightApp_Displayer.h>
#include <LightApp_SelectionMgr.h>

#include <BRep_Builder.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS_Wire.hxx>

#include <QSet>
#include <QFile>
#include <QFileInfo>

#include <QApplication>

#define MAX_LCM_NAME_INDEX 1000

HYDROGUI_ImportLandCoverMapOp::HYDROGUI_ImportLandCoverMapOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "IMPORT_LANDCOVER_MAP" ) );
}

HYDROGUI_ImportLandCoverMapOp::~HYDROGUI_ImportLandCoverMapOp()
{
  erasePreview();
  myImporter.Free();
  myAttrValue.clear();
}

void HYDROGUI_ImportLandCoverMapOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  if ( !getPreviewManager() )
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( module()->getApp()->getViewManager( OCCViewer_Viewer::Type(), true ) ) );

  HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->reset();
}

void HYDROGUI_ImportLandCoverMapOp::onFileSelected()
{
  HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;
  
  QString anObjectName = aPanel->getObjectName().simplified();
  anObjectName = aPanel->getFileName();
  if ( !anObjectName.isEmpty() ) 
      anObjectName = QFileInfo( anObjectName ).baseName();

  if ( anObjectName.isEmpty() ) 
    anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_LANDCOVERMAP_NAME" ) );
  aPanel->setObjectName( anObjectName );

  myFileName = aPanel->getFileName();
  if ( myFileName.isEmpty() )
  {
    abort();
    return;
  }

  startDocOperation();    
  QApplication::setOverrideCursor(Qt::WaitCursor);
  
  QStringList aPolygonsList;
  myPolygonFaces.Clear();
  myImporter.Free();

  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );
  if ( !aStudy )
    return;

  erasePreview();

  Handle(AIS_InteractiveContext) aCtx = NULL;

  //Import polygons from SHP file as faces
  //This faces should be added to the new LCM object

  int aStat = myImporter.ImportPolygons(doc(), myFileName, aPolygonsList, myPolygonFaces);
  int aShapeTypeOfFile = myImporter.GetShapeType();
  if (aStat == 1)
  {
    aPanel->setPolygonNames(aPolygonsList);

    LightApp_Application* anApp = module()->getApp();
    if ( !getPreviewManager() )
      setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
    OCCViewer_ViewManager* aViewManager = getPreviewManager();

    if ( aViewManager )
    {
      if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
      {
        aCtx = aViewer->getAISContext();
        connect( aViewer, SIGNAL( selectionChanged() ), this, SLOT( onViewerSelectionChanged() ) );
      }
    }

    for ( int i = 1; i <= myPolygonFaces.Length(); i++ ) 
    {
      TopoDS_Face aFace = TopoDS::Face(myPolygonFaces.Value( i ));
      if ( aViewManager && !aCtx.IsNull() )
      {
        HYDROGUI_Shape* aShape = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );
        //Preview color is grey
        aShape->setFillingColor(QColor(50,50,50), false, false);
        aShape->setBorderColor(QColor(50,50,50), false, false);
        if( !aFace.IsNull() )
          aShape->setShape( aFace);

        myPolygonName2PrsShape.insert( tr( "DEF_POLYGON_NAME" ) + "_" + QString::number(i), aShape);
      }
    }

    if ( !aCtx.IsNull() ) 
    {
      UpdateZLayersOfHilightPresentationsOfDisplayedObjects( aCtx, Graphic3d_ZLayerId_TopOSD );
      aCtx->UpdateCurrentViewer();
    }
    //
    QApplication::restoreOverrideCursor();
    aPanel->setFirstPageState(true); 
    //
    //Try to load DBF-database...
    QString aDBFFileName;
    aDBFFileName = myFileName.simplified().replace( myFileName.simplified().size() - 4, 4, ".dbf");
    bool DBF_Stat = myImporter.DBF_OpenDBF(aDBFFileName);
    // TODO:
    // add MSG BOX if stat is bad
    if (DBF_Stat)
    {
      myFieldList = myImporter.DBF_GetFieldList();
      aPanel->setAttributeNames(myFieldList);
    }
    aPanel->setDbfState(DBF_Stat);
    aPanel->setDbfRecordsNb(myImporter.DBF_GetNbRecords());
  }
  else
  {
    erasePreview();
    aPanel->setPolygonNames(QStringList());
    aPanel->setObjectName("");
    QApplication::restoreOverrideCursor();
    QString aMess = tr( "CANNT_IMPORT_LCM" ) + "\n"; 
    if (aStat == -1)
      aMess += tr( "CANNT_OPEN_SHP" );
    else if (aStat == -2)
      aMess += tr( "CANNT_OPEN_SHX" );
    else 
      aMess += tr ("SHP_TYPEFORMAT_MESS") + myImporter.GetShapeTypeName(aShapeTypeOfFile);
    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "IMPORT_LANDCOVER_MAP" ), aMess);
    commitDocOperation();
    myImporter.Free();
    //abort();
    aPanel->setFirstPageState(false);
  }

}

HYDROGUI_InputPanel* HYDROGUI_ImportLandCoverMapOp::createInputPanel() const
{
  HYDROGUI_InputPanel* aPanel = new HYDROGUI_ImportLandCoverMapDlg( module(), getName() );

  connect( aPanel, SIGNAL( FileSelected( const QString& ) ), SLOT( onFileSelected() ) );
  connect( aPanel, SIGNAL( selectionChanged( const QStringList& ) ), this, SLOT( onSelectionChanged( const QStringList& ) ) );
  connect( aPanel, SIGNAL( Next( const int ) ), SLOT( onNext( const int ) ) );
    
  return aPanel;
}

bool HYDROGUI_ImportLandCoverMapOp::processApply( int& theUpdateFlags,
                                                  QString& theErrorMsg,
                                                  QStringList& theBrowseObjectsEntries )
{

  HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
  if ( !aPanel ) 
    return false;

  myLCM = Handle(HYDROData_LandCoverMap)::DownCast( doc()->CreateObject( KIND_LAND_COVER_MAP ) );
  HYDROData_MapOfFaceToStricklerType aMapFace2ST;

  QStringList aAttrV_T;
  QStringList aSTL;
  aPanel->getValAttr2StricklerTypeCorr(aAttrV_T, aSTL);

  QVector<int> aSelIndices = aPanel->getSelectedPolygonIndices();
  foreach ( int Ind, aSelIndices )
  {
    TopoDS_Shape aShape = myPolygonFaces(Ind + 1);
    if ( aShape.IsNull() ) 
      continue;
    QString aST = "";
    if (aPanel->getAttrCheckBoxState())
    {
      HYDROData_ShapeFile::DBF_AttrValue aDataVal = myAttrValue[Ind];
      int aStricklerTypesInd = aAttrV_T.indexOf(QString(aDataVal.myStrVal));
      aST = aSTL.at(aStricklerTypesInd);
    }
    // else => ST is empty
    aMapFace2ST.Add( TopoDS::Face( aShape ), aST );
  }

  //
  myLCM->StoreLandCovers(aMapFace2ST);

  QString anObjName;
  if ( !aPanel->getFileName().isEmpty() )
    anObjName = aPanel->getObjectName();
  
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();

  //check if name of LCM is already exists
  QSet<QString> aNameList;
  if (aDoc)
  {
    HYDROData_Iterator anIt( aDoc, KIND_LAND_COVER_MAP );
    for( ; anIt.More(); anIt.Next() )
      aNameList << anIt.Current()->GetName();
  }

  QString aNewName = anObjName;
  for ( int i = 1; i < MAX_LCM_NAME_INDEX && aNameList.contains(aNewName); i++)
    aNewName = anObjName + "_" + QString::number(i);

  if( !myLCM.IsNull() ) 
  {
    myLCM->SetName( aNewName );
    myLCM->SetColor( Qt::gray );
    myLCM->Show();
    module()->setIsToUpdate( myLCM );
  }
  
  module()->update( UF_Model | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init );

  erasePreview();
  myImporter.Free();
  myAttrValue.clear();

  return true;
}

void HYDROGUI_ImportLandCoverMapOp::onSelectionChanged( const QStringList& theSelectedNames )
{
  Handle(AIS_InteractiveContext) aCtx = NULL;

  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager ) {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() ) {
      aCtx = aViewer->getAISContext();
    }
  }

  if ( !aCtx.IsNull() ) {
    bool bSelChanged = false;
    foreach ( QString aName, myPolygonName2PrsShape.keys() ) 
    {
      Handle(AIS_InteractiveObject) anObject = 
        myPolygonName2PrsShape.value(aName)->getAISObjects()[0];

      bool isSelected = theSelectedNames.contains( aName );
      if ( ( isSelected && !aCtx->IsSelected( anObject) ) || ( !isSelected && aCtx->IsSelected( anObject) ) )
      {
        aCtx->AddOrRemoveSelected( anObject, Standard_False );
        bSelChanged = true;
      }
    }

    HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
    if ( !aPanel ) 
      return;

    if (bSelChanged && aPanel->getViewerState())
      aCtx->UpdateCurrentViewer();
  }
}


void HYDROGUI_ImportLandCoverMapOp::onViewerSelectionChanged()
{
  HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  int aCurIndex = -1;
  aCurIndex = aPanel->getCurrentWizardIndex();

  if (aCurIndex != 0)
    return;

  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  Handle(AIS_InteractiveContext) aCtx = NULL;
  if ( aViewManager )
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
      aCtx = aViewer->getAISContext();
  
  if ( !aCtx.IsNull() )
  {
    QStringList aSelectedNames;
    foreach ( QString aName, myPolygonName2PrsShape.keys() ) {
      bool isSelected = aCtx->IsSelected( myPolygonName2PrsShape.value(aName)->getAISObjects()[0] );
      if ( isSelected )
        aSelectedNames << aName;
    }
    aPanel->setViewerState(false);
    aPanel->setSelectedPolygonNames( aSelectedNames );
    aPanel->setViewerState(true);
  }
}


void HYDROGUI_ImportLandCoverMapOp::erasePreview()
{
  foreach ( HYDROGUI_Shape* aShape, myPolygonName2PrsShape )
    delete aShape;

  myPolygonName2PrsShape.clear();
}


void HYDROGUI_ImportLandCoverMapOp::abortOperation()
{
  LightApp_Application* anApp = module()->getApp();
  if ( anApp )
    anApp->disconnect( this );

  erasePreview();
  myImporter.Free();
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ImportLandCoverMapOp::onNext( const int theIndex )
{  
  HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  if (theIndex == 2)
  {
    //std::vector<HYDROData_ShapeFile::DBF_AttrValue> myAttrValue;
    int anIndOfSelField = myFieldList.indexOf(aPanel->getSelectedFieldName());
    if (anIndOfSelField == -1)
      return;
    //aPanel->setSecondPageState(true);
    myAttrValue.clear();
    myImporter.DBF_GetAttributeList(anIndOfSelField, myAttrValue ); 

    mySetOfAttrValues.clear();
    for (size_t i = 0; i < myAttrValue.size(); i++)
    {
      HYDROData_ShapeFile::DBF_AttrValue aV = myAttrValue[i];
      mySetOfAttrValues << QString(aV.myStrVal);  //take myStrVal for now..
    }

    //Collect all strickler_types
    QSet<QString> aSTSet;
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    Handle(HYDROData_StricklerTable) DefStricklerTableObj;
    if ( aDoc )
    {
      HYDROData_Iterator It( aDoc, KIND_STRICKLER_TABLE );
      for( ; It.More(); It.Next() )
      {
        Handle(HYDROData_StricklerTable) aStricklerTableObj = Handle(HYDROData_StricklerTable)::DownCast( It.Current() );
        if (DefStricklerTableObj.IsNull())
          DefStricklerTableObj = aStricklerTableObj;
        if ( !aStricklerTableObj.IsNull())
        {
          const QStringList& aStricklerList = aStricklerTableObj->GetTypes();
          foreach (QString aStr, aStricklerList)
            aSTSet << aStr;
        }
      }
    }

    QList<QString> aSetOfAttrValuesList = mySetOfAttrValues.toList();
    QList<QString> aSTSetList = aSTSet.toList();
    QVector<QColor> aSTColors;
    aSTColors.reserve(aSTSetList.size());
    foreach (QString str, aSTSetList)
    {
      QColor col = aDoc->GetAssociatedColor(str, NULL);
      aSTColors.append (col);
    }

    //add an empty Strickler type
    aSTSetList.prepend(""); 
    aSTColors.prepend(QColor(Qt::gray));
    
    QVector<int> aCurCBIndices(aSetOfAttrValuesList.size());
    if (DefStricklerTableObj->GetAttrName().trimmed() == aPanel->getSelectedFieldName().trimmed())
      for (int i = 0; i < aSetOfAttrValuesList.size(); i++)
      {
        QString aST = DefStricklerTableObj->GetType(aSetOfAttrValuesList[i]);
        int anIndOfSt = aSTSetList.indexOf(aST);
        aCurCBIndices[i] = anIndOfSt;
      }
    else
      //TODO add warning ???
      for (int i = 0; i < aSetOfAttrValuesList.size(); i++)
        aCurCBIndices[i] = 0;

    aPanel->FillCorrespondenceTable(aSetOfAttrValuesList, aSTSetList, aCurCBIndices, aSTColors);    
  }

}


void HYDROGUI_ImportLandCoverMapOp::onApply()
{
  HYDROGUI_ImportLandCoverMapDlg* aPanel = ::qobject_cast<HYDROGUI_ImportLandCoverMapDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  if (!aPanel->CheckFirstPageFilling())
    return;
  
  QApplication::setOverrideCursor( Qt::WaitCursor );
  int anUpdateFlags = 0;
  QString anErrorMsg;
  QStringList aBrowseObjectsEntries;

  bool aResult = false;
  try
  {
    aResult = processApply( anUpdateFlags, anErrorMsg, aBrowseObjectsEntries );
  }
  catch (...)
  {
    SUIT_MessageBox::critical( module()->getApp()->desktop(), tr( "LCM_IMPORT_ERROR" ), tr ("CANNT_IMPORT_POLYGONS_FROM_SHP"));
    aResult = false;
  }
  
  QApplication::restoreOverrideCursor();

  if ( aResult )
  {
    module()->update( anUpdateFlags );
    commit();
    browseObjects( aBrowseObjectsEntries );
  }
  else
  {
    myLCM->Remove();
    module()->setObjectRemoved( myLCM );
    abort();
  }
}


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ExportCalculationOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"

#include <HYDROData_CalculationCase.h>

#include <GeometryGUI.h>

#include <SalomeApp_Study.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>

#include <QApplication>

HYDROGUI_ExportCalculationOp::HYDROGUI_ExportCalculationOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "EXPORT_CALCULATION" ) );
}

HYDROGUI_ExportCalculationOp::~HYDROGUI_ExportCalculationOp()
{
}

void HYDROGUI_ExportCalculationOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  onApply();
}

void HYDROGUI_ExportCalculationOp::abortOperation()
{
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ExportCalculationOp::commitOperation()
{
  HYDROGUI_Operation::commitOperation();
}

bool HYDROGUI_ExportCalculationOp::processApply( int& theUpdateFlags,
                                                 QString& theErrorMsg,
                                                 QStringList& theBrowseObjectsEntries)
{
  // Get the selected calculation case
  Handle(HYDROData_CalculationCase) aCalculation = 
    Handle(HYDROData_CalculationCase)::DownCast( 
      HYDROGUI_Tool::GetSelectedObject( module() ) );

  if ( aCalculation.IsNull() ) {
    theErrorMsg = tr( "NULL_DATA_OBJECT" );
    return false;
  }

  bool isOk = false;

  // Get active study
  SalomeApp_Study* aStudy = 
    dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );

  // Export
  myStatMess.clear();
  QString anErrorMsg;
  if ( aStudy ) {
    GEOM::GEOM_Gen_var aGeomEngine = GeometryGUI::GetGeomGen();
    QString anEntry;
    if ( aCalculation->Export( aGeomEngine, anEntry, theErrorMsg, myStatMess ) ) {
      theUpdateFlags = UF_ObjBrowser;
      isOk = true;
    }
  }

  return isOk;
}

void HYDROGUI_ExportCalculationOp::onApply()
{
  QApplication::setOverrideCursor( Qt::WaitCursor );

  int anUpdateFlags = 0;
  QString anErrorMsg;

  bool aResult = false;
  QStringList aBrowseObjectsEntries;

  try {
    aResult = processApply( anUpdateFlags, anErrorMsg, aBrowseObjectsEntries );
  }
  catch ( Standard_Failure& e )
  {
    anErrorMsg = e.GetMessageString();
    aResult = false;
  }
  catch ( ... )
  {
    aResult = false;
  }
  
  QApplication::restoreOverrideCursor();

  if ( aResult ) {
    module()->update( anUpdateFlags );
    commit();
    browseObjects( aBrowseObjectsEntries );

    // Show message box
    SUIT_MessageBox::information( module()->getApp()->desktop(),
                                  tr( "EXPORT_STATUS" ),
                                  tr( "EXPORT_FINISHED") + "\n" + getStatMess() ); 
  } 
  else {
    abort();

    if ( anErrorMsg.isEmpty() )
      anErrorMsg = tr( "EXPORT_DATA_FAILED" );

    anErrorMsg.prepend( tr( "EXPORT_FAILED" ) + "\n" );

    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "EXPORT_STATUS" ),
                               anErrorMsg ); 
  }
}

QString HYDROGUI_ExportCalculationOp::getStatMess()
{
  return myStatMess;
}

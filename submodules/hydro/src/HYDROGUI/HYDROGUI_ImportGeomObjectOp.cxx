// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportGeomObjectOp.h"

#include "HYDROGUI_GeomObjectDlg.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Obstacle.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Iterator.h>

#include <GEOMBase.h>

#include <SalomeApp_Study.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>

#include <QDialog>

HYDROGUI_ImportGeomObjectOp::HYDROGUI_ImportGeomObjectOp( HYDROGUI_Module* theModule,  
                                                          const int theOpType, 
                                                          const int theGEOMOp )
: HYDROGUI_Operation( theModule ),
  myOpType( theOpType ),
  myGEOMOp( theGEOMOp ),
  myGEOMOpName( "" ),
  myIsToShowPanel( true )
{
  if ( myOpType == ImportSelectedAsPolyline ) {
    setName( tr( "IMPORT_GEOM_OBJECT_AS_POLYLINE" ) );
  } else {
    setName( tr( "IMPORT_GEOM_OBJECT_AS_OBSTACLE" ) );
  }
}

HYDROGUI_ImportGeomObjectOp::~HYDROGUI_ImportGeomObjectOp()
{
}

void HYDROGUI_ImportGeomObjectOp::startOperation()
{
  // Get GEOM objects to import
  myGeomObjects.clear();

  if ( myOpType == ImportSelectedAsObstacle ) {
    myGeomObjects = 
      HYDROGUI_Tool::GetSelectedGeomObjects( module(), getObstacleTypes() );
  } else if ( myOpType == ImportSelectedAsPolyline ) {
    myGeomObjects = 
      HYDROGUI_Tool::GetSelectedGeomObjects( module(), getPolylineTypes() );
  }

  // Do not show the panel if more than one GEOM objects are selected
  myIsToShowPanel = myIsToShowPanel && ( myGeomObjects.count() <= 1 );

  HYDROGUI_Operation::startOperation();

  HYDROGUI_GeomObjectDlg* aPanel = 0;

  if ( myIsToShowPanel ) {
    // Get panel
    aPanel = ::qobject_cast<HYDROGUI_GeomObjectDlg*>( inputPanel() );

    if ( aPanel ) {
      // Reset the panel state
      aPanel->reset();

      // Set default name
      updateDefaultName();

      // Pass the existing object names to the panel
      QStringList anExistingNames;
      if ( myOpType == ImportCreatedAsObstacle || 
           myOpType == ImportSelectedAsObstacle ) {
        anExistingNames = HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_OBSTACLE );
      } else if ( myOpType == ImportSelectedAsPolyline ) {
        anExistingNames = HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_POLYLINEXY );
      }

      aPanel->setObjectNames( anExistingNames );
    }
  }

  if ( !aPanel ) {
    onApply();
  }

  // Activate GEOM module operation in case of the corresponding operation type
  if ( myOpType == ImportCreatedAsObstacle && myGEOMOp > 0 ) {
    LightApp_Application* anApp = module()->getApp();
    if ( anApp ) {
      connect( anApp, SIGNAL( operationFinished( const QString&, const QString&, const QStringList& ) ), 
        this, SLOT( onExternalOperationFinished( const QString&, const QString&, const QStringList& ) ) );

      module()->getApp()->activateOperation( "Geometry", myGEOMOp );
    }
  }
}

void HYDROGUI_ImportGeomObjectOp::abortOperation()
{
  LightApp_Application* anApp = module()->getApp();
  if ( anApp ) {
    anApp->disconnect( this );
  }

  closeExternalOperationDlg();

  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ImportGeomObjectOp::commitOperation()
{
  closeExternalOperationDlg();

  HYDROGUI_Operation::commitOperation();
}

bool HYDROGUI_ImportGeomObjectOp::processApply( int& theUpdateFlags,
                                                QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries )
{
  // Get active SalomeApp_Study
  SalomeApp_Study* aStudy = 
    dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );
  if ( !aStudy ) {
    return false;
  }

  // Check that GEOM objects list is not empty
  if ( myGeomObjects.isEmpty() ) {
    theErrorMsg = tr( "NO_GEOM_OBJECT_TO_IMPORT" );
    return false;
  }

  QString anObjectName;
  Handle(HYDROData_Entity) anObjectToEdit;  
  ObjectKind anObjectKind = 
    myOpType == ImportSelectedAsPolyline ? KIND_POLYLINEXY : KIND_OBSTACLE;

  if ( myGeomObjects.count() == 1 ) {
    // Get panel
    HYDROGUI_GeomObjectDlg* aPanel = ::qobject_cast<HYDROGUI_GeomObjectDlg*>( inputPanel() );
    if ( aPanel ) {
      // Check object name
      anObjectName = aPanel->getObjectName().simplified();
      if ( anObjectName.isEmpty() ) {
        theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
        return false;
      }

      // Get object to edit
      QString anEditedName = aPanel->getEditedObjectName().simplified();

      if ( !anEditedName.isEmpty() ) {
        anObjectToEdit = HYDROGUI_Tool::FindObjectByName( module(), anEditedName, anObjectKind );
      }
    }

    if( anObjectToEdit.IsNull() || anObjectToEdit->GetName() != anObjectName ) {
      // check that there are no other objects with the same name in the document
      Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObjectName/*, anObjectKind*/ );
      if( !anObject.IsNull() ) {
        theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObjectName );
        return false;
      }
    }
  }

  bool anIsOk = false;

  // Get the GEOM object as SObject
  foreach ( const QString& anEntry, myGeomObjects ) {
    _PTR(SObject) aSObject( aStudy->studyDS()->FindObjectID( qPrintable(anEntry)) );
    if ( aSObject ) {
      // Get the corresponding TopoDS_Shape
      TopoDS_Shape aShape = GEOMBase::GetShapeFromIOR( aSObject->GetIOR().c_str() );
      if ( aShape.IsNull() ) {
        continue;
      }
      
      // Create/edit an object
      Handle(HYDROData_Entity) anObject; 

      if ( anObjectToEdit.IsNull() ) {
        if ( myOpType == ImportCreatedAsObstacle || myOpType == ImportSelectedAsObstacle ) {
          anObject = doc()->CreateObject( KIND_OBSTACLE );
          Handle(HYDROData_Obstacle) anObstacle = Handle(HYDROData_Obstacle)::DownCast( anObject );
          anObstacle->SetFillingColor( anObstacle->DefaultFillingColor() );
          anObstacle->SetBorderColor( anObstacle->DefaultBorderColor() );
          anObstacle->SetGeomObjectEntry( anEntry.toLatin1().constData() );
        } else if ( myOpType == ImportSelectedAsPolyline ) {
          anObject = doc()->CreateObject( KIND_POLYLINEXY );
          Handle(HYDROData_PolylineXY) aPolylineObj = Handle(HYDROData_PolylineXY)::DownCast( anObject );
          aPolylineObj->SetWireColor( HYDROData_PolylineXY::DefaultWireColor() );
          aPolylineObj->SetGeomObjectEntry( anEntry.toLatin1().constData() );
        }
      } else {
        anObject = anObjectToEdit;
      }

      // Set name
      if ( anObjectName.isEmpty() ) {
        QString aName = QString::fromStdString( aSObject->GetName() );
        anObjectName = HYDROGUI_Tool::GenerateObjectName( 
          module(), aName, QStringList(), true );
      }
      if ( anObject->GetName() != anObjectName ) {
        anObject->SetName( anObjectName );
      }

      anObjectName.clear();

      // Set shape
      if ( myOpType == ImportCreatedAsObstacle || myOpType == ImportSelectedAsObstacle ) {
        Handle(HYDROData_Obstacle) anObstacle = Handle(HYDROData_Obstacle)::DownCast( anObject );
        anObstacle->SetShape3D( aShape );
        anIsOk = true;
      } else if ( myOpType == ImportSelectedAsPolyline ) {
        Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast( anObject );
        anIsOk = aPolyline->ImportShape( aShape, false, NULL );

        /* TODO: check it before start operation
        if ( anIsOk && !aPolyline->IsEditable() )
        {
          anIsOk = SUIT_MessageBox::question( module()->getApp()->desktop(),
                                              tr( "POLYLINE_IS_UNRECOGNIZED_TLT" ),
                                              tr( "POLYLINE_IS_UNRECOGNIZED_MSG" ),
                                              QMessageBox::Yes | QMessageBox::No,
                                              QMessageBox::No ) == QMessageBox::Yes;
          setPrintErrorMessage( anIsOk );
        }
        */
      }

      // Check operation status
      if ( anIsOk ) {
        anObject->Update();
        module()->setIsToUpdate( anObject );
        QString aHydroObjEntry = HYDROGUI_DataObject::dataObjectEntry( anObject );
        theBrowseObjectsEntries.append( aHydroObjEntry );
        theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
      }
    }
  }

  return anIsOk;
}

HYDROGUI_InputPanel* HYDROGUI_ImportGeomObjectOp::createInputPanel() const
{
  HYDROGUI_InputPanel* aPanel = 0;
  if ( myIsToShowPanel ) {
    QString anObjectTypeName = 
      myOpType == ImportSelectedAsPolyline ? tr("DEFAULT_POLYLINE_NAME") :
                  tr("DEFAULT_OBSTACLE_NAME");
    aPanel = new HYDROGUI_GeomObjectDlg( module(), getName(), anObjectTypeName );
  }

  return aPanel;
}

void HYDROGUI_ImportGeomObjectOp::updateDefaultName()
{
  // Get panel
  HYDROGUI_GeomObjectDlg* aPanel = ::qobject_cast<HYDROGUI_GeomObjectDlg*>( inputPanel() );
  if ( !aPanel ) {
    return;
  }

  // Set the current GEOM object name to the panel
  if ( myGeomObjects.count() == 1 ) {
    SalomeApp_Study* aStudy = 
      dynamic_cast<SalomeApp_Study*>( module()->getApp()->activeStudy() );
    if ( aStudy ) {
      QString anEntry = myGeomObjects.first();
      _PTR(SObject) aSObject( aStudy->studyDS()->FindObjectID( qPrintable(anEntry) ) );
      if ( aSObject ) {
        aPanel->setDefaultName( QString::fromStdString(aSObject->GetName()) );
      }
    }
  }
}

/**
 * Called when the operation perfomed by another module is finished.
 * \param theModuleName the name of the module which perfomed the operation
 * \param theOperationName the operation name
 * \param theEntryList the list of the created objects entries
 */
void HYDROGUI_ImportGeomObjectOp::onExternalOperationFinished( 
  const QString& theModuleName, const QString& theOperationName,
  const QStringList& theEntryList )
{
  // Process "Geometry" module operations with non-empty list of created objects only
  if ( theModuleName != "Geometry" || theEntryList.isEmpty() ) {
    return;
  }
  
  // Store the operation name
  myGEOMOpName = theOperationName;

  // Store the geom objects entries list
  myGeomObjects = theEntryList;

  // Update the default name of the HYDRO object
  updateDefaultName();

  // Close the dialog corresponding to the external operation
  closeExternalOperationDlg();
}

void HYDROGUI_ImportGeomObjectOp::closeExternalOperationDlg()
{
  if ( myGEOMOpName.isEmpty() ) {
    return;
  }

  SUIT_Desktop* aDesktop = module()->getApp()->desktop();
  if ( aDesktop ) {
    QList<QDialog*> aDialogs = aDesktop->findChildren<QDialog*>();
    foreach ( QDialog* aDlg, aDialogs ) {
      if ( typeid(*aDlg).name() == myGEOMOpName ) {
        aDlg->close();
        break;
      }
    }
  }
}

QList<GEOM::shape_type> HYDROGUI_ImportGeomObjectOp::getObstacleTypes()
{
  QList<GEOM::shape_type> aTypes;

  aTypes << GEOM::COMPOUND << GEOM::COMPOUND << GEOM::SOLID << 
            GEOM::SHELL << GEOM::FACE << GEOM::SHAPE;

  return aTypes;
}

QList<GEOM::shape_type> HYDROGUI_ImportGeomObjectOp::getPolylineTypes()
{
  QList<GEOM::shape_type> aTypes;

  aTypes << GEOM::WIRE << GEOM::EDGE;

  return aTypes;
}

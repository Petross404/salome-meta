// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_UpdateObjectOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Document.h>
#include <HYDROData_Image.h>

HYDROGUI_UpdateObjectOp::HYDROGUI_UpdateObjectOp( HYDROGUI_Module* theModule,
                                                  const bool       theIsForced )
: HYDROGUI_Operation( theModule ),
  myIsForced( theIsForced )
{
  setName( tr( "UPDATE_IMAGE" ) );
}

HYDROGUI_UpdateObjectOp::~HYDROGUI_UpdateObjectOp()
{
}

void HYDROGUI_UpdateObjectOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  startDocOperation();

  NCollection_Map<Handle(HYDROData_Entity)> aMapOfTreated;

  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( module() );
  for( int anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObject = aSeq.Value( anIndex );
    updateObject( anObject, aMapOfTreated );
  }

  commitDocOperation();

  module()->update( UF_Model | UF_Viewer | UF_GV_Forced | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer );
  commit();
}

void HYDROGUI_UpdateObjectOp::updateObject( const Handle(HYDROData_Entity)& theObject,
                                           NCollection_Map<Handle(HYDROData_Entity)>& theMapOfTreated ) const
{
  if( theObject.IsNull() || theMapOfTreated.Contains( theObject ) )
    return;

  theMapOfTreated.Add( theObject );

  HYDROData_SequenceOfObjects aRefSeq = theObject->GetAllReferenceObjects();
  for ( int i = 1, n = aRefSeq.Length(); i <= n; ++i )
  {
    Handle(HYDROData_Entity) anObject = aRefSeq.Value( i );
    updateObject( anObject, theMapOfTreated );
  }

  if ( !myIsForced && !theObject->IsMustBeUpdated( HYDROData_Entity::Geom_All ) )
    return;

  theObject->Update();
  module()->setIsToUpdate( theObject );
}


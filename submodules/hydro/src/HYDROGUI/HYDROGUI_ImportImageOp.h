// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_IMPORTIMAGEOP_H
#define HYDROGUI_IMPORTIMAGEOP_H

#include "HYDROGUI_Operation.h"
#include "HYDROGUI_ImportImageDlg.h"

#include <HYDROData_Image.h>

#include <QImage>

class GraphicsView_ViewManager;
class SUIT_ViewManager;
class HYDROGUI_PrsImage;

class HYDROGUI_ImportImageOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_ImportImageOp( HYDROGUI_Module* theModule, const bool theIsEdit );
  virtual ~HYDROGUI_ImportImageOp();

protected:
  virtual void               startOperation();
  virtual void               abortOperation();
  virtual void               commitOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool               processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries );
  bool                       isReferenceCorrect() const;

  /**
   * Apply changes done by this operation.
   */
  virtual void               apply();

protected slots:
  void                       onCreatePreview( QImage, HYDROData_Image::ECW_FileInfo* );
  void                       onActivatePointSelection( int );
  void                       onPointCoordChanged( bool, int, bool, int );
  void                       onModeActivated( int );
  void                       onRefImageActivated( const QString& );
  void                       onSetCIsUsed( bool theCIsUsed );

  void                       onLastViewClosed( SUIT_ViewManager* );

  void                       onPointSelected();
  void                       onRefPointSelected();
  void                       onPointSelected( bool theIsRefImage );
  void                       onFilesSelected( const QStringList& );

private:
  void                       closePreview();
  void                       closeView( GraphicsView_ViewManager* &aViewMgr );

  bool                       checkPoints( const QPointF& thePointA,
                                          const QPointF& thePointB,
                                          const QPointF& thePointC,
                                          const bool     theIsByTwoPoints,
                                          const QString& theLineErrMsg,
                                          const QString& thePoinErrMsg,
                                          QString&       theErrorMsg,
                                          const bool     theIsToCheckInvertibles ) const;

  /**
   * Build the local-global points mapping.
   */
  void                       computeTrsfData( HYDROData_Image::TransformationMode theTrsfMode,
                                              bool theIsByTwoPoints,
                                              const QPoint& theLocalPointA,
                                              const QPoint& theLocalPointB,
                                              const QPoint& theLocalPointC,
                                              const QPointF& theGlobalPointA,
                                              const QPointF& theGlobalPointB,
                                              const QPointF& theGlobalPointC,
                                              HYDROGUI_ImportImageDlg::TransformationDataMap& theDataMap ) const;

  /**
   * Set transformation points A,B,C to the image presentation.
   */
  void                       setPresentationTrsfPoints( HYDROGUI_PrsImage* thePrs, 
                                                        bool theIsByTwoPoints,
                                                        const QPoint theLocalPointA, 
                                                        const QPoint theLocalPointB, 
                                                        const QPoint theLocalPointC );

  /*
   * Initialize the combobox in the dialog with the list of available reference images
   */
  void                       getReferenceDataList(
                                 HYDROGUI_ImportImageDlg::PrsPointDataList& theList ) const;

  bool SetNextFile();

private:
  bool                       myIsEdit;
  Handle(HYDROData_Image)    myEditedObject;

  SUIT_ViewManager*          myActiveViewManager;  //!< The previous view to come back to after the operation

  GraphicsView_ViewManager*  myPreviewViewManager; //!< The operation preview window
  GraphicsView_ViewManager*  myRefViewManager;     //!< The reference image view window
  HYDROGUI_PrsImage*         myPreviewPrs;         //!< The loaded image preview presentation
  HYDROGUI_PrsImage*         myRefPreviewPrs;      //!< The reference image presentation

  QImage                     myImage;              //!< The loaded image to import

  int                        myPointType;
  QStringList                myFiles;
  int                        myFileIndex;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ExportImageOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"

#include <HYDROData_Image.h>

#include <LightApp_Application.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>

HYDROGUI_ExportImageOp::HYDROGUI_ExportImageOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "EXPORT_IMAGE" ) );
}

HYDROGUI_ExportImageOp::~HYDROGUI_ExportImageOp()
{
}

void HYDROGUI_ExportImageOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  Handle(HYDROData_Image) anImageObj =
    Handle(HYDROData_Image)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( !anImageObj.IsNull() )
  {
    QImage anImage = anImageObj->Image();
    QTransform aTransform = anImageObj->Trsf();

    // Scale image to it origin imported size
    double aCoefX = aTransform.m11() < 0 ? -1.0 : 1.0;
    double aCoefY = aTransform.m22() < 0 ? -1.0 : 1.0;
    aTransform.scale( aCoefX * ( 1 / aTransform.m11() ), aCoefY * ( 1 / aTransform.m22() ) );

    anImage = anImage.transformed( aTransform, Qt::SmoothTransformation );
    
    // Invert the Y axis direction from down to up
    anImage = anImage.transformed( QTransform::fromScale( 1, -1 ), Qt::SmoothTransformation );
    
    QString aFilter( tr( "IMAGE_FILTER_EXPORT" ) );
    QString aFileName = SUIT_FileDlg::getFileName( module()->getApp()->desktop(),
                                                   "", aFilter, tr( "EXPORT_IMAGE_TO_FILE" ), false );
    if( !aFileName.isEmpty() )
      anImage.save( aFileName );
  }

  commit();
}

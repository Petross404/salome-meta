// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_INPUTPANEL_H
#define HYDROGUI_INPUTPANEL_H

#include <QDockWidget>

class QFrame;
class QPushButton;
class QLayout;
class QCloseEvent;
class QSplitter;

class HYDROGUI_Module;

/**\class HYDROGUI_Dialog
 *\brief The base class representing base input panel for all HYDROGUI panels
 */
class HYDROGUI_InputPanel : public QDockWidget
{
  Q_OBJECT

public:
  HYDROGUI_InputPanel( HYDROGUI_Module* theModule, const QString& theTitle, bool doInitLayout = true, bool isSplitter = false );
  virtual ~HYDROGUI_InputPanel();

  void addWidget( QWidget* theWidget, int theStretch = 0 );
  void insertWidget( QWidget* theWidget, int theIndex, int theStretch = 0 );
  void addLayout( QLayout* theLayout, int theStretch = 0 );
  void addStretch();

  HYDROGUI_Module* module() const;

  bool             isApplyEnabled() const;
  void             setApplyEnabled( bool );

  QSplitter* splitter() const;

signals:
  void panelApplyAndClose();
  void panelApply();
  void panelCancel();
  void panelHelp();

protected slots:
  void onApplyAndClose();
  void onApply();
  void onCancel();
  void onHelp();

protected:
  QFrame*         mainFrame() const;
  QFrame*         buttonFrame() const;
  virtual void    closeEvent ( QCloseEvent * event );

  QPushButton* myCancel;
  QPushButton* myHelp;

private:
  HYDROGUI_Module* myModule;
  QFrame* myMainFrame;
  QFrame* myBtnFrame;
  QPushButton* myApplyAndClose;
  QPushButton* myApply;
  QSplitter* mySplitter;
};

#endif

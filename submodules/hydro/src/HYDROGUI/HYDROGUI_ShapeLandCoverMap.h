// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// REMOVED FROM THE PROJECT, BUT KEPT AS A REFERENCE FILE. TO BE DELETED LATER.

#ifndef HYDROGUI_SHAPE_LANDCOVERMAP_H
#define HYDROGUI_SHAPE_LANDCOVERMAP_H

#include <HYDROGUI_Shape.h>

class HYDROGUI_OCCDisplayer;
class HYDROData_LandCoverMap;

class HYDROGUI_ShapeLandCoverMap : public HYDROGUI_Shape
{
public:
  HYDROGUI_ShapeLandCoverMap( HYDROGUI_OCCDisplayer* theDisplayer,
                              const Handle(AIS_InteractiveContext)& theContext,
                              const Handle(HYDROData_LandCoverMap)& theLandCoverMap,
                              const int                             theZLayer = -1,
                              const bool                            theIsScalarMode = false );
  virtual ~HYDROGUI_ShapeLandCoverMap();

  virtual void              update( bool isUpdateViewer,
                                    bool isDeactivateSelection );

  virtual void display( const bool theIsUpdateViewer = true );
  virtual void erase( const bool theIsUpdateViewer = true );

  virtual void              setVisible( const bool theState,
                                        const bool theIsUpdateViewer = true );

  bool isScalarMapModeEnabled() const;
  /**
   * Enable/disable scalar map coloring mode.
   * @param theIsToEnable if true - scalar map coloring mode willbe enbaled, if false - disabled
   */
  virtual void              setScalarMapModeEnabled( const bool theIsToEnable );

protected:
  virtual void              displayShape( const bool theIsUpdateViewer );
  virtual QList<Handle(AIS_InteractiveObject)> createShape() const;

private:
  HYDROGUI_OCCDisplayer*           myDisplayer;
  bool                             myIsScalarMapMode;
};

#endif

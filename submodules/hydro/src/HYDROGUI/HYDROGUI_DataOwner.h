// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_DATAOWNER_H
#define HYDROGUI_DATAOWNER_H

#include <SUIT_DataOwner.h>

#include <AIS_InteractiveObject.hxx>


/*!
  This class provide data owner objects.
*/
class HYDROGUI_DataOwner : public SUIT_DataOwner
{
public:
    /**
     * Constructor. Initialize by \a AIS_InteractiveObject.
     */
    HYDROGUI_DataOwner( const Handle(AIS_InteractiveObject)& theIO );

    /**
     * Constructor.
     */
    HYDROGUI_DataOwner();

    /**
     * Destructor.
     */
    virtual ~HYDROGUI_DataOwner();

    /**
     * Gets key string, used for data owners comparison.
     * \return the empty string
     */
    virtual QString keyString() const;

    /**
     * Gets SALOME_InteractiveObject.
     * \return the SALOME interactive object
     */
    const Handle(AIS_InteractiveObject)& IO() const;

private:
    Handle(AIS_InteractiveObject) myIO; ///< the AIS interactive object
};

typedef SMART(HYDROGUI_DataOwner) HYDROGUI_DataOwnerPtr;

#endif

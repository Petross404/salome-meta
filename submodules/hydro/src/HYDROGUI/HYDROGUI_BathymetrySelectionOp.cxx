// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_BathymetrySelectionOp.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_BathymetryPrs.h>
#include <HYDROGUI_BathymetryOp.h>
#include <HYDROGUI_ShapeBathymetry.h>
#include <HYDROGUI_Operations.h>
#include <OCCViewer_ViewManager.h>
#include <LightApp_Application.h>
#include <SUIT_Selector.h>
#include <QAction>
#include <QApplication>
#include <QString>
#include <QList>
#include <LightApp_SelectionMgr.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_BathymetrySelectionOp::HYDROGUI_BathymetrySelectionOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ), myIsActive( false )
{
  DEBTRACE("HYDROGUI_BathymetrySelectionOp");
}

HYDROGUI_BathymetrySelectionOp::~HYDROGUI_BathymetrySelectionOp()
{
}

void HYDROGUI_BathymetrySelectionOp::startOperation()
{
  DEBTRACE("startOperation");
  activateSelection( true );
}

void HYDROGUI_BathymetrySelectionOp::abortOperation()
{
  DEBTRACE("abortOperation");
  activateSelection( false );

  module()->action( BathymetrySelectionId )->setChecked( false );
  module()->action( BathymetryTextId )->setChecked( false );
}

bool HYDROGUI_BathymetrySelectionOp::isValid( SUIT_Operation* theOtherOp ) const
{
  DEBTRACE("isValid");
  HYDROGUI_BathymetryOp* aBathOp = dynamic_cast<HYDROGUI_BathymetryOp*>( theOtherOp );
  return ( aBathOp != 0 );
}

OCCViewer_Viewer* getViewer( HYDROGUI_Module* theModule )
{
  LightApp_Application* app = theModule->getApp();
  OCCViewer_ViewManager* mgr = dynamic_cast<OCCViewer_ViewManager*>
    ( app->getViewManager( OCCViewer_Viewer::Type(), true ) );
  return mgr->getOCCViewer();
}

Handle(AIS_InteractiveContext) getContext( HYDROGUI_Module* theModule )
{
  return getViewer( theModule )->getAISContext();
}

QList<Handle(HYDROGUI_BathymetryPrs)> getShownBathymetries( HYDROGUI_Module* theModule )
{
  QList<Handle(HYDROGUI_BathymetryPrs)> baths;
  Handle(AIS_InteractiveContext) ctx = getContext( theModule );

  AIS_ListOfInteractive objs;
  ctx->DisplayedObjects( objs );
  AIS_ListIteratorOfListOfInteractive it( objs );
  for( ; it.More(); it.Next() )
  {
    Handle(HYDROGUI_BathymetryPrs) bath = Handle(HYDROGUI_BathymetryPrs)::DownCast( it.Value() );
    if( !bath.IsNull() )
      baths.append( bath );
  }
  return baths;
}

void HYDROGUI_BathymetrySelectionOp::activateSelection( bool isActive )
{
  DEBTRACE("activateSelection " << isActive);
  if( myIsActive==isActive )
    return;

  getContext( module() )->ClearSelected(true);
  qApp->setOverrideCursor( Qt::WaitCursor );
  getContext( module() )->ClearSelected(true);
  Handle(AIS_InteractiveContext) ctx = getContext( module() );
  QList<Handle(HYDROGUI_BathymetryPrs)> baths = getShownBathymetries( module() );
  if( isActive )
  {
    const int aSelectionMode = 1;  // Cf. AIS_PointCloud 0=selection by points ?
    foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
    {
      DEBTRACE("bathy name: " << bath->GetShape()->getObject()->GetName().toStdString());
      ctx->Deactivate(bath);
      ctx->RemoveFilters();
      ctx->SetSelectionModeActive (bath, aSelectionMode, Standard_True, AIS_SelectionModesConcurrency_Multiple, Standard_False);
      bath->SetAutoHilight( Standard_False ); // True bloque le passage dans hilightSelected...
    }
    ctx->UpdateCurrentViewer();
  }
  else
  {
    foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
    {
      bath->ClearSelected();
      bath->ClearSelectedPoints();
      bath->SetAutoHilight( Standard_True );
      bath->GetShape()->TextLabels( false );
      ctx->Deactivate( bath );
      ctx->SetSelectionModeActive (bath, 2, Standard_True, AIS_SelectionModesConcurrency_Multiple, Standard_False);
    }
  }

  myIsActive = isActive;

  qApp->restoreOverrideCursor();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VisualStateOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Document.h>
#include <HYDROData_VisualState.h>

#include <GraphicsView_Viewer.h>

#include <LightApp_Application.h>

#include <QtxWorkstack.h>

#include <STD_TabDesktop.h>

#include <SUIT_Desktop.h>
#include <SUIT_ViewManager.h>
#include <SUIT_ViewWindow.h>

#include <QApplication>

HYDROGUI_VisualStateOp::HYDROGUI_VisualStateOp( HYDROGUI_Module* theModule,
                                                const bool theIsLoad )
: HYDROGUI_Operation( theModule ),
  myIsLoad( theIsLoad )
{
  setName( theIsLoad ? tr( "LOAD_VISUAL_STATE" ) : tr( "SAVE_VISUAL_STATE" ) );
}

HYDROGUI_VisualStateOp::~HYDROGUI_VisualStateOp()
{
}

void HYDROGUI_VisualStateOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  if( !myIsLoad )
    startDocOperation();

  bool aResult = false;
  if( myIsLoad )
    aResult = loadVisualState();
  else
    aResult = saveVisualState();

  if( aResult )
  {
    if( !myIsLoad )
      commitDocOperation();
    commit();
  }
  else
  {
    if( !myIsLoad )
      abortDocOperation();
    abort();
  }
}

bool HYDROGUI_VisualStateOp::saveVisualState()
{
  LightApp_Application* anApp = module()->getApp();

  Handle(HYDROData_VisualState) aVisualState =
    Handle(HYDROData_VisualState)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( aVisualState.IsNull() )
  {
    // Create new visual state
    aVisualState = Handle(HYDROData_VisualState)::DownCast( doc()->CreateObject( KIND_VISUAL_STATE ) );
    if( aVisualState.IsNull() )
      return false;

    QString aName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_VISUAL_STATE_NAME" ) );
    aVisualState->SetName( aName );
  }

  // Store parameters.
  PropertyMap aPropertyMap;

  // A. Setting unique names for view windows in order to save this view inside
  // workstack's structure (see below). On restore the views with the same names will
  // be placed to the same place inside the workstack's splitters.
  ViewManagerList aViewManagerList;
  anApp->viewManagers( aViewManagerList );
  nameViewWindows( aViewManagerList );

  // B. Store active window.
  if( SUIT_ViewWindow* aViewWindow = anApp->desktop()->activeWindow() )
    setVisualProperty( aPropertyMap, "AP_ACTIVE_VIEW", aViewWindow->objectName() );

  // C. Store view properties.
  size_t aViewerId = 0;
  QListIterator<SUIT_ViewManager*> anIter( aViewManagerList );
  while( anIter.hasNext() )
  {
    if( SUIT_ViewManager* aViewManager = anIter.next() )
    {
      size_t aViewId = (size_t)aViewManager->getViewModel();
      if( SUIT_ViewWindow* aViewWindow = aViewManager->getActiveView() )
      {
        QString aType = aViewManager->getType();
        QString aViewerEntry = QString( "%1_%2" ).arg( aType ).arg( ++aViewerId );
        setVisualProperty( aPropertyMap, "AP_VIEWERS_LIST", aViewerEntry, true );

        setVisualProperty( aPropertyMap, aViewerEntry, aViewWindow->windowTitle(), true );
        setVisualProperty( aPropertyMap, aViewerEntry, aViewWindow->getVisualParameters(), true );

        // C1. Store parameters of presentations.
        HYDROData_SequenceOfObjects aSeq;
        if( aType == GraphicsView_Viewer::Type() )
          HYDROGUI_Tool::GetPrsSubObjects( module(), aSeq );

        for( int anObjIndex = 1, aLength = aSeq.Length(); anObjIndex <= aLength; anObjIndex++ )
        {
          Handle(HYDROData_Entity) anObject = aSeq.Value( anObjIndex );
          if( !anObject.IsNull() )
          {
            // Format: "Name|Visibility[|CoordX|CoordY]"
            QString aParameters = anObject->GetName();

            int aVisibility = (int)( module()->isObjectVisible( aViewId, anObject ) );
            aParameters.append( QString( "|%1" ).arg( aVisibility ) );

            setVisualProperty( aPropertyMap, aViewerEntry, aParameters, true );
          }
        }
      }
    }
  }

  // D. Store split areas.
  if( anApp->desktop()->inherits( "STD_TabDesktop" ) )
  {
    QtxWorkstack* aWorkstack = ( (STD_TabDesktop*)anApp->desktop() )->workstack();
    QByteArray aWorkstackState = aWorkstack->saveState( 0 );
    QString aWorkstackInfo = aWorkstackState.toHex();
    setVisualProperty( aPropertyMap, "AP_WORKSTACK_INFO", aWorkstackInfo );
  }

  // E. Store module preferences.
  //ouv: currently, nothing to do

  QString aState = encodePropertyMap( aPropertyMap );
  //printf( "--- SetState -----------\n" );
  //printf( "%s\n", qPrintable( aState ) );
  //printf( "------------------------\n" );
  aVisualState->SetState( aState.toStdString() );

  module()->update( UF_Model );

  return true;
}

bool HYDROGUI_VisualStateOp::loadVisualState()
{
  LightApp_Application* anApp = module()->getApp();

  Handle(HYDROData_VisualState) aVisualState =
    Handle(HYDROData_VisualState)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( aVisualState.IsNull() )
    return false;

  QString aState = QString::fromStdString( aVisualState->GetState() );
  //printf( "--- GetState -----------\n" );
  //printf( "%s\n", qPrintable( aState ) );
  //printf( "------------------------\n" );
  PropertyMap aPropertyMap = decodePropertyMap( aState );

  // Restore parameters.

  // Preparation. Remove the existing viewers.
  anApp->clearViewManagers();

  // E. Restore module preferences
  //ouv: currently, nothing to do

  // C. Restore view properties (step 1).
  QMap<SUIT_ViewWindow*, QString> aViewParameters;
  int aNbViewers = nbVisualProperties( aPropertyMap, "AP_VIEWERS_LIST" );
  for( int anIndex = 0; anIndex < aNbViewers; anIndex++ )
  {
    QString aViewerEntry = getVisualProperty( aPropertyMap, "AP_VIEWERS_LIST", anIndex );
    QString aType = aViewerEntry.section( '_', 0, -2 );
    QString aViewerId = aViewerEntry.section( '_', -1 ); // unused
    if( SUIT_ViewManager* aViewManager = anApp->createViewManager( aType ) )
    {
      size_t aViewId = (size_t)aViewManager->getViewModel();
      if( SUIT_ViewWindow* aViewWindow = aViewManager->getActiveView() )
      {
        // Wait until the window is really shown. This step fixes MANY bugs.
        //while( !aViewManager->isVisible() )
        //  qApp->processEvents();

        int aNbViewerProps = nbVisualProperties( aPropertyMap, aViewerEntry );

        aViewWindow->setWindowTitle( getVisualProperty( aPropertyMap, aViewerEntry, 0 ) );

        // Parameters of view windows are restoring after the workstack (see below).
        aViewParameters[ aViewWindow ] = getVisualProperty( aPropertyMap, aViewerEntry, 1 );

        // C1. Restore parameters of presentations.
        QMap< QString, QStringList > anObject2ParametersMap;
        for( int aPropIndex1 = 2; aPropIndex1 < aNbViewerProps; aPropIndex1++ )
        {
          QString aProperty = getVisualProperty( aPropertyMap, aViewerEntry, aPropIndex1 );
          QStringList aParameters = aProperty.split( "|" );
          if( aParameters.count() > 1 )
          {
            QString aName = aParameters.front();
            aParameters.pop_front();
            anObject2ParametersMap[ aName ] = aParameters;
          }
        }

        HYDROData_SequenceOfObjects aSeq;
        if( aType == GraphicsView_Viewer::Type() )
          HYDROGUI_Tool::GetPrsSubObjects( module(), aSeq );

        for( int anObjIndex = 1, aLength = aSeq.Length(); anObjIndex <= aLength; anObjIndex++ )
        {
          Handle(HYDROData_Entity) anObject = aSeq.Value( anObjIndex );
          if( !anObject.IsNull() )
          {
            QString aName = anObject->GetName();
            if( anObject2ParametersMap.find( aName ) != anObject2ParametersMap.end() )
            {
              QStringList aParameters = anObject2ParametersMap[ aName ];
              int aParamCount = aParameters.count();
              QVector<bool> anIsOk( aParamCount, false );

              int aParamIndex = -1;
              if( aParamCount - aParamIndex - 1 >= 1 )
              {
                bool anIsVisible = aParameters[ aParamIndex ].toInt( &anIsOk[ ++aParamIndex ] );
                if( anIsOk[ 0 ] )
                  module()->setObjectVisible( aViewId, anObject, anIsVisible );
              }
            }
          }
        }
      }
    }
  }

  // A. Setting unique names for view windows in order to restore positions of view windows inside
  // workstack's structure (see below). During save the same naming algorithm was used,
  // so the same views will get the same names.
  ViewManagerList aViewManagerList;
  anApp->viewManagers( aViewManagerList );
  nameViewWindows( aViewManagerList );

  qApp->processEvents();

  // D. Restore split areas.
  QtxWorkstack* aWorkstack = 0;
  if( anApp->desktop()->inherits( "STD_TabDesktop" ) )
  {
    if( aWorkstack = ((STD_TabDesktop*)anApp->desktop())->workstack() )
    {
      QString aWorkstackInfo = getVisualProperty( aPropertyMap, "AP_WORKSTACK_INFO" );
      QByteArray aWorkstackState = QByteArray::fromHex( aWorkstackInfo.toLatin1() );
      aWorkstack->restoreState( aWorkstackState, 0 );
    }
  }

  qApp->processEvents();

  // C. Restore view properties (step 2).
  // B. Restore active window. (currently doesn't work)
  QString anActiveViewName = getVisualProperty( aPropertyMap, "AP_ACTIVE_VIEW" );
  QMap<SUIT_ViewWindow*, QString>::Iterator aMapIter;
  for( aMapIter = aViewParameters.begin(); aMapIter != aViewParameters.end(); ++aMapIter )
  {
    if( SUIT_ViewWindow* aViewWindow = aMapIter.key() )
    {
      aViewWindow->setVisualParameters( aMapIter.value() );
      if( anActiveViewName == aViewWindow->objectName() )
        if( aWorkstack )
          aWorkstack->setActiveWindow( aViewWindow );
    }
  }

  module()->update( UF_Viewer );

  return true;
}

void HYDROGUI_VisualStateOp::nameViewWindows( const ViewManagerList& theList )
{
  QMap<QString, int> aViewersCounter;
  for( ViewManagerList::const_iterator anIter = theList.begin(); anIter != theList.end(); ++anIter )
  {
    SUIT_ViewManager* aViewManager = *anIter;
    if( !aViewManager )
      continue;

    int aViewCount = aViewManager->getViewsCount();
    QString aType = aViewManager->getType();
    if( !aViewCount )
      continue;

    if( !aViewersCounter.contains( aType ) )
      aViewersCounter.insert( aType, 0 );

    int& aViewerId = aViewersCounter[ aType ];

    QVector<SUIT_ViewWindow*> aViews = aViewManager->getViews();
    for( int i = 0; i < aViewCount; i++ )
    {
      QString aName = QString( "%1_%2_%3" ).arg( aType ).arg( aViewerId ).arg( i );
      aViews[i]->setObjectName( aName );
    }
    aViewerId++;
  }
}

QString HYDROGUI_VisualStateOp::encodePropertyMap( const PropertyMap& thePropertyMap )
{
  QStringList aPropertyDataStringList;
  PropertyMapIterator anIter( thePropertyMap );
  while( anIter.hasNext() )
  {
    QString aPropertyName = anIter.next().key();
    QStringList aPropertyData = anIter.value();

    if( !aPropertyData.isEmpty() )
    {
      aPropertyData.prepend( aPropertyName );
      QString aPropertyDataString = aPropertyData.join( "||" );
      aPropertyDataStringList.append( aPropertyDataString );
    }
  }

  QString aResult = aPropertyDataStringList.join( "|||" );
  return aResult;
}

HYDROGUI_VisualStateOp::PropertyMap
HYDROGUI_VisualStateOp::decodePropertyMap( const QString& theString )
{
  PropertyMap aPropertyMap;
  QStringList aPropertyDataStringList = theString.split( "|||" );
  QStringListIterator anIter( aPropertyDataStringList );
  while( anIter.hasNext() )
  {
    QString aPropertyDataString = anIter.next();
    QStringList aPropertyData = aPropertyDataString.split( "||" );
    if( aPropertyData.count() >= 2 )
    {
      QString aPropertyName = aPropertyData.front();
      aPropertyData.pop_front();
      aPropertyMap[ aPropertyName ] = aPropertyData;
    }
  }
  return aPropertyMap;
}

void HYDROGUI_VisualStateOp::setVisualProperty( PropertyMap& thePropertyMap,
                                                const QString& thePropertyName,
                                                const QString& thePropertyData,
                                                const bool theIsAppend )
{
  QStringList& aPropertyDataList = thePropertyMap[ thePropertyName ];
  if( !theIsAppend )
    aPropertyDataList.clear();
  aPropertyDataList.append( thePropertyData );
}

QString HYDROGUI_VisualStateOp::getVisualProperty( const PropertyMap& thePropertyMap,
                                                   const QString& thePropertyName,
                                                   int theIndex )
{
  PropertyMap::const_iterator anIter = thePropertyMap.find( thePropertyName );
  if( anIter != thePropertyMap.end() )
  {
    const QStringList aPropertyData = anIter.value();
    if( theIndex >= 0 && theIndex < aPropertyData.count() )
      return aPropertyData[ theIndex ];
  }
  return QString();
}

int HYDROGUI_VisualStateOp::nbVisualProperties( const PropertyMap& thePropertyMap,
                                                const QString& thePropertyName )
{
  PropertyMap::const_iterator anIter = thePropertyMap.find( thePropertyName );
  if( anIter != thePropertyMap.end() )
  {
    const QStringList aPropertyData = anIter.value();
    return aPropertyData.count();
  }
  return 0;
}

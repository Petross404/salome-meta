// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#ifndef HYDROGUI_PRSIMAGEFRAME_H
#define HYDROGUI_PRSIMAGEFRAME_H

#include <GraphicsView_Object.h>

#include <QGraphicsEllipseItem>

class HYDROGUI_PrsImage;

/*
  Class       : HYDROGUI_PrsImageFrame
  Description : Presentation for image frame object
*/
class HYDROGUI_PrsImageFrame : public GraphicsView_Object
{
public:
  class UnscaledGraphicsEllipseItem;

  enum AnchorType { Undefined = 0, TopLeft, TopRight, BottomLeft, BottomRight };

  typedef QMap        <int, UnscaledGraphicsEllipseItem*> AnchorMap;
  typedef QMapIterator<int, UnscaledGraphicsEllipseItem*> AnchorMapIterator;

public:
  HYDROGUI_PrsImageFrame( HYDROGUI_PrsImage* thePrsImage );
  virtual ~HYDROGUI_PrsImageFrame();

public:
  // from QGraphicsItem
  virtual QRectF                  boundingRect() const;

  // from GraphicsView_Object
  virtual void                    compute();

  virtual bool                    hasSpecificZValue() const { return true; }

  virtual bool                    isSelectable() const { return false; }
  virtual bool                    isMovable() const { return false; }

public:
  void                            computeAnchorItems();
  void                            updateVisibility();

protected:
  HYDROGUI_PrsImage*              myPrsImage;
  AnchorMap                       myAnchorMap;
};

/*
  Class       : UnscaledGraphicsEllipseItem
  Description : Class for unscaled ellipse item
*/
class HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem : public QGraphicsEllipseItem
{
public:
  UnscaledGraphicsEllipseItem( QGraphicsItem* );
  virtual ~UnscaledGraphicsEllipseItem();

public:
  void           setBasePoint( const QPointF& thePoint ) { myBasePoint = thePoint; }
  const QPointF& getBasePoint() const { return myBasePoint; }

public:
  virtual QRectF boundingRect() const;
  virtual void   paint( QPainter*, const QStyleOptionGraphicsItem*, QWidget* );

private:
  QPointF        myBasePoint;
};

#endif

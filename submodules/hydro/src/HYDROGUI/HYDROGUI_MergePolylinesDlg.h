// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_MergePolylinesDlg_H
#define HYDROGUI_MergePolylinesDlg_H

#include <HYDROGUI_InputPanel.h>
#include <HYDROData_Entity.h>

class QLineEdit;
class QCheckBox;
class HYDROGUI_ObjListBox;

class HYDROGUI_MergePolylinesDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_MergePolylinesDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_MergePolylinesDlg();

  QString                     GetResultName() const;
  HYDROData_SequenceOfObjects selectedPolylines() const;
  void                        setSelectedPolylines( const HYDROData_SequenceOfObjects& );
  void                        setPolylinesFromSelection();

  bool                        IsConnectByNewSegment() const;

private:
  QLineEdit*           myName;
  QCheckBox*           myIsConnect;
  HYDROGUI_ObjListBox* myList;
};

#endif

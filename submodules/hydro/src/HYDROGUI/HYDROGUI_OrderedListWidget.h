// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ORDEREDLISTWIDGET_H
#define HYDROGUI_ORDEREDLISTWIDGET_H

#include "HYDROGUI_ListModel.h"

#include <QWidget>

class QListView;
class QPushButton;

/** 
 * \class HYDROGUI_OrderedListWidget
 * \brief The class representing widget for managing list of objects.
 *
 * The widget represents a list and a set of buttons (top, up, down and bottom)
 * providing the possibility to change the list items order.
 */
class HYDRO_EXPORT HYDROGUI_OrderedListWidget : public QWidget
{
  Q_OBJECT

public:
  HYDROGUI_OrderedListWidget( QWidget* theParent, int theArrowIconSize = 32 );
  virtual ~HYDROGUI_OrderedListWidget();

  void setObjects( const HYDROGUI_ListModel::Object2VisibleList& theObjects );
  HYDROGUI_ListModel::ObjectList getObjects() const;

  void addObject( const HYDROGUI_ListModel::Object2Visible& theObject );
  void removeObjectByName( const QString& theObjectName );

  void setHiddenObjectsShown( const bool theIsToShow );
  void setVisibilityIconShown( const bool theIsToShow );

  void setOrderingEnabled( const bool theIsToEnable );

  QStringList getSelectedEntries() const;
  void setSelectedEntries( const QStringList& theEntries ) const;

  QStringList getSelectedNames() const;
  QStringList getAllNames() const;

  void setBackgroundColor (int theInd, QColor theColor);
  void setBackgroundColor (QString theName, QColor theColor);

  QColor getBackgroundColor (int theInd) const;
  void clearAllBackgroundColors ();

  void undoLastMove();

signals:
  void selectionChanged();
  void orderChanged();

private slots:
  void onMove( int theType );
  void onSelectionChanged();

private:
  HYDROGUI_ListModel* getSourceModel() const;

  QModelIndexList getSelectedIndexes() const;

private:
  QListView*   myList;   ///< the list view
  QPushButton* myTop;    ///< the move on top button
  QPushButton* myUp;     ///< the move up button
  QPushButton* myDown;   ///< the move down button
  QPushButton* myBottom; ///< the move on bottom button

  bool myIsHiddenObjectsShown; ///< defines whether to include hidden objects in the list
  bool myIsVisibilityIconShown; ///< defines whether to show visibility icon (eye icon)
};

#endif

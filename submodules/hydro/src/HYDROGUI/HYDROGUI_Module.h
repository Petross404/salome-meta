// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_MODULE_H
#define HYDROGUI_MODULE_H

#include <HYDROData_Entity.h>

#ifdef WIN32
  #pragma warning ( disable: 4251 )
#endif

#include <LightApp_Module.h>
#include <LightApp_Application.h>
#include <QEvent>
#include <QStack>
#include <HYDROGUI_DataObject.h>

#ifdef WIN32
  #pragma warning ( default: 4251 )
#endif

class QGraphicsSceneMouseEvent;
class GraphicsView_Viewer;
class OCCViewer_Viewer;
class SVTK_Viewer;

class SUIT_ViewWindow;
class SUIT_ViewManager;

class HYDROData_StricklerTable;

class HYDROGUI_DataModel;
class HYDROGUI_Displayer;
class HYDROGUI_OCCDisplayer;
class HYDROGUI_VTKPrsDisplayer;
class HYDROGUI_AbstractDisplayer;
class HYDROGUI_Shape;
class HYDROGUI_VTKPrs;
class HYDROGUI_Operation;
class HYDROGUI_Overview;

/**\class HYDROGUI_Module
 *\brief The class representing the HYDROGUI module
 */
class HYDROGUI_Module : public LightApp_Module
{
  Q_OBJECT

  enum CustomEvent { NewViewEvent = QEvent::User + 100 };
  enum CustomWindows { OverviewWindow = LightApp_Application::WT_User + 100 };

  enum CursorType
  {
    CT_ArrowCursor = 0,
    CT_UpArrowCursor,
    CT_CrossCursor,
    CT_WaitCursor,
    CT_IBeamCursor,
    CT_SizeVerCursor,
    CT_SizeHorCursor,
    CT_SizeBDiagCursor,
    CT_SizeFDiagCursor,
    CT_SizeAllCursor,
    CT_SplitVCursor,
    CT_SplitHCursor,
    CT_PointingHandCursor,
    CT_ForbiddenCursor,
    CT_WhatsThisCursor,
    CT_BusyCursor,
    CT_OpenHandCursor,
    CT_ClosedHandCursor,
    CT_User
  };

public:
  
  enum ViewManagerRole
  {
    VMR_Unknown = 0,
    VMR_General,
    VMR_TransformImage,
    VMR_ReferenceImage,
    VMR_ObserveImage,
    //VMR_PreviewZone, // totally unused, to delete
    VMR_PreviewCaseZones // actually unused, to review
  };
  typedef QPair< SUIT_ViewManager*, ViewManagerRole > ViewManagerInfo;

  typedef QMap        < size_t, ViewManagerInfo > ViewManagerMap;
  typedef QMapIterator< size_t, ViewManagerInfo > ViewManagerMapIterator;

  struct ObjectState
  {
    bool Visibility;
    double Transparency;
    double ZValue;
    ObjectState() : Visibility( false ), Transparency( 1.0 ), ZValue( 0.0 ) {}
  };
  typedef QMap< QString, ObjectState > Entry2ObjectStateMap;
  typedef QMap< size_t, Entry2ObjectStateMap > ViewId2Entry2ObjectStateMap;

  typedef QList<HYDROGUI_Shape*> ListOfShapes;
  typedef QMap<size_t,ListOfShapes> ViewId2ListOfShapes;

  typedef QList<HYDROGUI_VTKPrs*> ListOfVTKPrs;
  typedef QMap<size_t,ListOfVTKPrs> ViewId2ListOfVTKPrs;

  typedef QMap<size_t, Handle(HYDROData_StricklerTable)> ViewId2StricklerTable;

public:
  HYDROGUI_Module();
  virtual ~HYDROGUI_Module();

  int                             getStudyId() const;

  virtual void                    initialize( CAM_Application* );
  virtual void                    windows( QMap<int, int>& ) const;
  virtual void                    viewManagers( QStringList& ) const;

  virtual void                    contextMenuPopup( const QString&, QMenu*, QString& );
  virtual void                    createPreferences();

  virtual void                    update( const int );
  virtual void                    updateCommandsStatus();

  virtual void                    selectionChanged();
  virtual void                    studyClosed( SUIT_Study* );

  SUIT_ViewManager*               getViewManager( const size_t theId ) const;
  HYDROGUI_DataModel*             getDataModel() const;
  HYDROGUI_Displayer*             getDisplayer() const;
  HYDROGUI_OCCDisplayer*          getOCCDisplayer() const;
  HYDROGUI_VTKPrsDisplayer*       getVTKDisplayer() const;

  GraphicsView_Viewer*            getViewer( const size_t theId ) const;
  OCCViewer_Viewer*               getOCCViewer( const size_t theId ) const;
  SVTK_Viewer*                    getVTKViewer( const size_t theId ) const;

  size_t                          getViewManagerId( SUIT_ViewManager* theViewManager );
  ViewManagerRole                 getViewManagerRole( SUIT_ViewManager* theViewManager );
  void                            setViewManagerRole( SUIT_ViewManager* theViewManager,
                                                      const ViewManagerRole theRole );

  bool                            isObjectVisible( const size_t theViewId,
                                                   const Handle(HYDROData_Entity)& theObject ) const;
  void                            setObjectVisible( const size_t theViewId,
                                                    const Handle(HYDROData_Entity)& theObject,
                                                    const bool theState );

  virtual void                    preferencesChanged( const QString&, const QString& );

  void                            resetViewState();

  /** 
   * Set IsToUpdate flag for all presentations of the given object to recompute them during 
   * the next viewer(s) updating.
   * @param theObject the data model object
   * @param theState  the flag to set for object's presentations, it's true by default.
   */
  void                            setIsToUpdate( const Handle(HYDROData_Entity)& theObject,
                                                 const bool theState = true );
  QList<HYDROGUI_Shape*>          getObjectShapes( const size_t  theViewId,
                                                   ObjectKind theKind ) const;
  HYDROGUI_Shape*                 getObjectShape( const size_t                    theViewId,
                                                  const Handle(HYDROData_Entity)& theObject ) const;
  void                            setObjectShape( const size_t                    theViewId,
                                                  const Handle(HYDROData_Entity)& theObject,
                                                  HYDROGUI_Shape*                 theShape );
  void                            removeViewShapes( const size_t                  theViewId );
  void                            removeObjectShape( const size_t                    theViewId,
                                                     const Handle(HYDROData_Entity)& theObject );

  HYDROGUI_VTKPrs*                getObjectVTKPrs( const size_t                    theViewId,
                                                   const Handle(HYDROData_Entity)& theObject ) const;
  void                            setObjectVTKPrs( const size_t                    theViewId,
                                                   const Handle(HYDROData_Entity)& theObject,
                                                   HYDROGUI_VTKPrs*                theShape );
  void                            removeViewVTKPrs( const size_t                   theViewId );
  void                            removeObjectVTKPrs( const size_t                   theViewId,
                                                     const Handle(HYDROData_Entity)& theObject );

  void                            removeObjectVTKPrs( const size_t   theViewId,
                                                      const QString& theEntry );
  void                            removeViewManager( SUIT_ViewManager* );

  /**
   * Update global imposed range of Z values for the given VTK viewer id
   */
  void                            updateVTKZRange( const size_t theViewId, double theRange[] );

  /**
   * Returns true if the object with the given entry can be renamed.
   * @param theEntry the object entry
   */
  virtual bool                    renameAllowed( const QString& theEntry ) const;
  /**
   * Returns true if the object with the given entry is renamed.
   * @param theEntry the object entry
   * @param theName the new name
   */
  virtual bool                    renameObject( const QString& theEntry, const QString& theName );

  QStack<HYDROGUI_Operation*>&    getActiveOperations();
  HYDROGUI_Operation*             activeOperation();

  /**
   * Returns the cursor defined for edition operations in module preferences.
   * \return specific cursor
   */
  QCursor                         getPrefEditCursor() const;

  void                            clearCache();

  bool isDraggable( const SUIT_DataObject* what ) const;

  bool isDropAccepted( const SUIT_DataObject* where ) const;

  void dropObjects( const DataObjectList& what, SUIT_DataObject* where,
                    const int row, Qt::DropAction action );

  /**
   * Returns Strickler table used for Land Cover scalar map coloring in the given view.
   * @param theViewId the view id
   * @return the Strickler table used for scalar map coloring of Land Covers in the given view;
             null - if scalar map coloring is off for the view  
   */
  Handle(HYDROData_StricklerTable) getLandCoverColoringTable( const size_t theViewId ) const;

  /**
   * Set Strickler table to be used for Land Cover scalar map coloring in the given view.
   * @param theViewId the view id
   * @param theTable the Strickler table
   */
  void                             setLandCoverColoringTable( const size_t theViewId,
                                                              const Handle(HYDROData_StricklerTable)& theTable );
  /**
   * Set Land Cover scalar map coloring mode off for the given view.
   * @param theViewId the view id
   */
  void                             setLandCoversScalarMapModeOff( const size_t theViewId );

  /**
   * Check if Land Cover scalar map coloring mode is on in the given view.
   * @param theViewId the view id
   * @return true if the mode is on, false if the mode is off
   */
  bool                             isLandCoversScalarMapModeOn( const size_t theViewId ) const;

  /**
   * Set object as removed.
   * @param theObject the removed object
   */
  void                            setObjectRemoved( const Handle(HYDROData_Entity)& theObject );

  void                            enableLCMActions();

  bool arrowsVisible() const;
  void setArrowsVisible( bool );

protected:
  CAM_DataModel*                  createDataModel();
  void                            setAutoZoomToAllViewManagers(bool bAutoZoom);
  void                            setAutoZoom(SUIT_ViewManager* aVMgr, bool bAutoZoom);
public slots:
  virtual bool                    activateModule( SUIT_Study* );
  virtual bool                    deactivateModule( SUIT_Study* );
  virtual void                    onObjectClicked( SUIT_DataObject*, int );

protected:
  virtual LightApp_Operation*     createOperation( const int ) const;
  virtual bool                    reusableOperation( const int id );

  virtual void                    customEvent( QEvent* );
  virtual bool                    eventFilter( QObject*, QEvent* );

protected slots:
  void                            onOperation();
  void                            onDelete();
  void                            onBathymetrySelection();
  void                            onBathymetryText();


  bool                            onUndo( int theNumActions );
  bool                            onRedo( int theNumActions );

  virtual void                    onViewManagerAdded( SUIT_ViewManager* );
  virtual void                    onViewManagerRemoved( SUIT_ViewManager* );
  virtual void                    onViewCreated( SUIT_ViewWindow* );

  void                            onViewActivated( SUIT_ViewManager* );

  void                            onViewPortMouseEvent( QGraphicsSceneMouseEvent* );

  void                            onMouseMove( SUIT_ViewWindow*, QMouseEvent* );

private:
  void                            updateViewer( HYDROGUI_AbstractDisplayer* theDisplayer, 
                                                const bool theIsInit = false, 
                                                const bool theIsForced = false, 
                                                const bool theDoFitAll = false );

  void                            createSelector( SUIT_ViewManager* viewMgr , bool isDisableGV = false);

  bool                            setUpdateEnabled( const bool theState );
  bool                            isUpdateEnabled() const;

  QStringList                     storeSelection() const;
  void                            restoreSelection( const QStringList& theEntryList );

private:
  void                            createActions();
  void                            createMenus();
  void                            createPopups();
  void                            createToolbars();

  void                            createUndoRedoActions();
  void                            updateUndoRedoControls();

private:
  QAction*                        createAction( const int theId, const QString& theSuffix,
                                                const QString& theImg = QString::null,
                                                const int theKey = 0, const bool isToggle = false,
                                                const QString& theSlot = QString::null );

private:
  HYDROGUI_Displayer*             myDisplayer;
  HYDROGUI_OCCDisplayer*          myOCCDisplayer;
  HYDROGUI_VTKPrsDisplayer*       myVTKDisplayer;

  ViewManagerMap                  myViewManagerMap;
  ViewId2Entry2ObjectStateMap     myObjectStateMap;

  QStack<HYDROGUI_Operation*>     myActiveOperationMap;

  ViewId2ListOfShapes             myShapesMap;
  ViewId2ListOfVTKPrs             myVTKPrsMap;
  bool                            myIsUpdateEnabled;

  QStringList                     myGeomObjectsToImport; ///< entries of GEOM objects to be imported

  ViewId2StricklerTable           myLandCoverColoringMap;

  HYDROGUI_Overview*              myOverview;
};

#endif

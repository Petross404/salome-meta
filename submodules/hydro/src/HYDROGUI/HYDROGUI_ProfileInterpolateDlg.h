// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PROFILEINTERPOLATEDLG_H
#define HYDROGUI_PROFILEINTERPOLATEDLG_H

#include "HYDROGUI_ViewerDlg.h"
#include "HYDROGUI_ObjComboBox.h"
#include <QSet>

class QSpinBox;
class QLineEdit;
class QComboBox;
class QTextEdit;

class HYDROGUI_ProfileInterpolateDlg : public HYDROGUI_ViewerDlg, public HYDROGUI_ObjComboBoxFilter
{
  Q_OBJECT

public:
  HYDROGUI_ProfileInterpolateDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_ProfileInterpolateDlg();

  QString                interpolator() const;
  void                   setInterpolator( const QString& );
  void                   setInterpolators( const QStringList& );

  QString                interpolatorDescription() const;
  void                   setInterpolatorDescription( const QString& );

  QString                interpolatorParameters() const;
  void                   setInterpolatorParameters( const QString& );

  QString                river() const;
  void                   setRiver( const QString& );

  QString                profileStart() const;
  void                   setProfileStart( const QString& );

  QString                profileFinish() const;
  void                   setProfileFinish( const QString& );

  int                    profileNumber() const;
  void                   setProfileNumber( int );

  void                   reset();

  virtual bool           isActive( HYDROGUI_ObjComboBox* ) const;
  virtual bool           isOk( const Handle(HYDROData_Entity)& ) const;

signals:
  void                   riverChanged( const QString& );
  void                   interpolatorChanged( const QString& );
  void                   interpolatorParametersChanged( const QString& );

  void                   profileNumberChanged( int );
  void                   profileStartChanged( const QString& );
  void                   profileFinishChanged( const QString& );

private slots:
  void                   onParametersEditingFinished();
  void                   onRiverChanged( const QString& );
  void                   onInterpolatorIndexChanged( int );
  void                   onProfileChanged( const QString& );

private:
  void                   updateState();
  HYDROGUI_ObjComboBox*  activeProfile() const;

private:
  typedef QSet<QString> RiverProfiles;

private:
  HYDROGUI_ObjComboBox*  myRiver;
  QComboBox*             myIntrp;
  QTextEdit*             myDescr;
  QLineEdit*             myParams;

  HYDROGUI_ObjComboBox*  myProfileStart;
  HYDROGUI_ObjComboBox*  myProfileFinish;
  QSpinBox*              myProfileNumber;

  RiverProfiles          myRiverProfiles;
};

#endif

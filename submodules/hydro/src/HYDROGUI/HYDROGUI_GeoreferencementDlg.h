// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_GEOREFERENCEMENT_H
#define HYDROGUI_GEOREFERENCEMENT_H

#include "HYDROGUI_InputPanel.h"

class SUIT_ViewWindow;

class QGroupBox;
class QButtonGroup;
class QTableWidget;
class QPushButton;

class HYDROGUI_GeoreferencementDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  enum ProfilesMode { AllProfiles, SelectedProfiles };

  struct ProfileGeoData
  {
    double Xg;
    double Yg;
    double Xd;
    double Yd;

    bool isIncomplete, isEmpty;

    QString Name;

    ProfileGeoData( const QString& theName ) :
      Name( theName ), isEmpty( true ), isIncomplete( false ) {}

    ProfileGeoData( const QString& theName,
                    const QString& theXg, const QString& theYg, 
                    const QString& theXd, const QString& theYd );

    ProfileGeoData( const QString& theName,
                    double theXg, double theYg, 
                    double theXd, double theYd )
      : Name( theName ), Xg( theXg ), Yg( theYg ), Xd( theXd ), Yd( theYd ),
      isIncomplete(false), isEmpty(false) {}
  };
  typedef QList<ProfileGeoData> ProfilesGeoDataList;

public:
  HYDROGUI_GeoreferencementDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_GeoreferencementDlg();

  void                       reset();

  void                       setMode( const int theMode );

  void                       setData( const ProfilesGeoDataList& theData );
  void                       getData( ProfilesGeoDataList& theData ) const;

  bool                       isModified() const;

public slots:
  void                       onMousePress( SUIT_ViewWindow*, QMouseEvent* );

protected slots:
  void                       onModeActivated( int );
  void                       onDataChanged();

signals:
  void                       modeActivated( int );
  void                       updateSelection();

private:
  QButtonGroup* myModeButtons;
  QPushButton* myUpdateSelBtn;
  QTableWidget* myTable;

  bool myIsModified;
};

#endif

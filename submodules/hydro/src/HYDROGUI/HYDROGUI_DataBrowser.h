// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_DATABROWSER_H
#define HYDROGUI_DATABROWSER_H

#include <SUIT_DataBrowser.h>
#include <SUIT_TreeModel.h>

class HYDROGUI_Module;

/**\class HYDROGUI_DataBrowser
 *\brief The data model browser widget implementation
 */
class HYDROGUI_DataBrowser : public SUIT_DataBrowser, public SUIT_DataSearcher
{
  Q_OBJECT

public:
  HYDROGUI_DataBrowser( HYDROGUI_Module* theModule, SUIT_DataObject*,
                        QWidget* = 0, bool theLandCover = false );
  ~HYDROGUI_DataBrowser();

  HYDROGUI_Module*         module() const;

  /*!
    Find a data object by the specified entry.
    \param theEntry - Entry of the object.
    \return data object.
  */
  virtual SUIT_DataObject* findObject( const QString& ) const;

  void                     setReadOnly( const bool theIsReadOnly ); 

signals:
  void             dropped( const QList<SUIT_DataObject*>& theList, 
                            SUIT_DataObject* theTargetParent,
                            int theTargetRow, Qt::DropAction theDropAction );
  void             dataChanged();
  void             newRegion();

protected:
  virtual void createPopupMenu( QMenu* );

private:
  HYDROGUI_Module* myModule;
};

#endif

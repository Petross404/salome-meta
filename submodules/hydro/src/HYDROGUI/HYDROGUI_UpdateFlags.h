// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_UPDATEFLAGS_H
#define HYDROGUI_UPDATEFLAGS_H

#include <LightApp_UpdateFlags.h>

/**
 * \enum HYDRO_UpdateFlags
 * Enumeration for update flags. First byte is reserved for LightApp_Module.
 * Modules derived from this model must use other 3 bytes to define their
 * own update flags.
 */
typedef enum
{
  UF_Base          = UF_Forced | UF_Model | UF_Viewer | UF_ObjBrowser | UF_Controls,
  
  UF_GV_Init       = 0x00000020, //!< initial update (used with UF_Viewer)
  UF_GV_Forced     = 0x00000040, //!< to force recomputing all presentations (used with UF_Viewer)
  
  UF_OCCViewer     = 0x00000080, //!< OCC viewer
  UF_OCC_Init      = 0x00000100, //!< initial update (used with UF_OCCViewer)
  UF_OCC_Forced    = 0x00000200, //!< to force recomputing all presentations (used with UF_OCCViewer)
  
  UF_VTKViewer     = 0x00000800, //!< VTK viewer
  UF_VTK_Init      = 0x00001000, //!< initial update (used with UF_VTKViewer)
  UF_VTK_Forced    = 0x00002000, //!< to force recomputing all presentations (used with UF_VTKViewer)
  
  UF_FitAll        = 0x00000400, //!< to do fit all (used with UF_Viewer or UF_OCCViewer)

  UF_All           = UF_Base | UF_GV_Init | UF_GV_Forced | UF_OCCViewer | UF_OCC_Init | UF_OCC_Forced | 
                      UF_VTKViewer | UF_VTK_Init | UF_VTK_Forced//!< all update flags
} HYDRO_UpdateFlags;

#endif

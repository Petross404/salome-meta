// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_StricklerTypeComboBox.h"

#include "HYDROGUI_Module.h"

#include <QLayout>
#include <QLabel>
#include <QComboBox>

#include <HYDROData_Iterator.h>
#include <HYDROData_Document.h>
#include <HYDROData_StricklerTable.h>

HYDROGUI_StricklerTypeComboBox::HYDROGUI_StricklerTypeComboBox( HYDROGUI_Module* theModule,
                                                                const QString& theTitle,
                                                                QWidget* theParent )
: QWidget( theParent ),
  myModule( theModule )
{
  QBoxLayout* base = new QHBoxLayout( this );
  base->setMargin( 0 );

  if ( !theTitle.isEmpty() )
    base->addWidget( new QLabel( theTitle, this ) );    
  myStricklerTypes = new QComboBox( this );  
  myStricklerTypes->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  base->addWidget( myStricklerTypes );

  if ( myModule )
  {
    // Construct a list of unique names of all Strickler types defined within the data model
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    if ( aDoc )
    {
      QStringList aStricklerTypes;
      HYDROData_Iterator anIterator( aDoc, KIND_STRICKLER_TABLE );
      for( ; anIterator.More(); anIterator.Next() )
      {
        Handle(HYDROData_StricklerTable) aStricklerTableObj =
          Handle(HYDROData_StricklerTable)::DownCast( anIterator.Current() );	
        if ( !aStricklerTableObj.IsNull() )
        {
          // Get Strickler table data from the data model
          QStringList aTypes = aStricklerTableObj->GetTypes();
          for ( QStringList::iterator it = aTypes.begin(); it != aTypes.end(); ++it )
          {
            QString aType = *it;
            if ( !aType.isEmpty() && !aStricklerTypes.contains( aType ) )
              aStricklerTypes.append( aType );
          }
        }
      }

      aStricklerTypes.sort();
      setStricklerTypes( aStricklerTypes );
    }
  }
}

HYDROGUI_StricklerTypeComboBox::~HYDROGUI_StricklerTypeComboBox()
{
}

HYDROGUI_Module* HYDROGUI_StricklerTypeComboBox::module() const
{
  return myModule;
}

void HYDROGUI_StricklerTypeComboBox::setStricklerTypes( const QStringList& theTypes )
{
  bool isBlocked = blockSignals( true );

  myStricklerTypes->clear();
  myStricklerTypes->addItems( theTypes );

  blockSignals( isBlocked );
}

void HYDROGUI_StricklerTypeComboBox::setSelectedStricklerTypeName( const QString& theName )
{
  myStricklerTypes->setCurrentIndex( myStricklerTypes->findText( theName ) );
}

QString HYDROGUI_StricklerTypeComboBox::getSelectedStricklerTypeName() const
{
  return myStricklerTypes->currentText();
}

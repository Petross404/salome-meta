// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_BATHPROGRESSINDICATOR_H
#define HYDROGUI_BATHPROGRESSINDICATOR_H

#include <QtxDialog.h>

#include <Message_ProgressIndicator.hxx>

class QLabel;
class QProgressBar;

class HYDROGUI_ZIProgressIndicator : public QtxDialog, public Message_ProgressIndicator
{
  Q_OBJECT

public:
  HYDROGUI_ZIProgressIndicator( QWidget* theParent );
  virtual ~HYDROGUI_ZIProgressIndicator();

  virtual void Show (const Message_ProgressScope&, const Standard_Boolean theForce);

  virtual Standard_Boolean UserBreak();

  virtual void Reset();

protected slots:
  virtual void reject();

private:
  QProgressBar* myQuadTreeBar;
  QLabel* myTriangulationState;
  QProgressBar* mySummaryBar;

  Standard_Real myQuadTreePosition;
  Standard_Boolean myUserBreak;
  int myCount;

public:
  DEFINE_STANDARD_RTTIEXT (HYDROGUI_ZIProgressIndicator, Message_ProgressIndicator)
};

#endif

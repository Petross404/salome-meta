// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_AbstractDisplayer.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"

#include <SUIT_ViewManager.h>
#include <SUIT_ViewModel.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_AbstractDisplayer::HYDROGUI_AbstractDisplayer( HYDROGUI_Module* theModule )
: myModule( theModule )
{
}

HYDROGUI_AbstractDisplayer::~HYDROGUI_AbstractDisplayer()
{
}

bool HYDROGUI_AbstractDisplayer::IsApplicable( const size_t theViewerId ) const
{
  return IsApplicable( myModule->getViewManager( theViewerId ) );
}

bool HYDROGUI_AbstractDisplayer::IsApplicable( const SUIT_ViewManager* theViewMgr ) const
{
  return ( theViewMgr && ( theViewMgr->getType() == GetType() ) );
}

void HYDROGUI_AbstractDisplayer::UpdateAll( const size_t theViewerId,
                                       const bool theIsInit,
                                       const bool theIsForced,
                                       const bool theDoFitAll )
{
  DEBTRACE("UpdateAll");
  if ( theIsInit )
    EraseAll( theViewerId );

  DisplayAll( theViewerId, theIsForced, theDoFitAll );
}

void HYDROGUI_AbstractDisplayer::DisplayAll( const size_t theViewerId,
                                             const bool theIsForced,
                                             const bool theDoFitAll )
{
	DEBTRACE("DisplayAll");
  HYDROData_SequenceOfObjects aSeq;
  HYDROGUI_Tool::GetPrsSubObjects( myModule, aSeq );
  Update( aSeq, theViewerId, theIsForced, theDoFitAll );
}

void HYDROGUI_AbstractDisplayer::Update( const HYDROData_SequenceOfObjects& theObjs,
                                         const size_t                       theViewerId,
                                         const bool                         theIsForced,
                                         const bool                         theDoFitAll )
{
	DEBTRACE("Update");
  // First of all, kill all bad presentations
  purgeObjects( theViewerId );

  // Now dig in the data model
  HYDROData_SequenceOfObjects anObjectsToErase, anObjectsToDisplay;
  SUIT_ViewModel* aViewer = module()->getViewManager( theViewerId )->getViewModel();

  for( int i = 1, n = theObjs.Length(); i <= n; i++ )
  {
    const Handle(HYDROData_Entity)& anObj = theObjs.Value( i );

    if( module()->isObjectVisible( (size_t)aViewer, anObj ) ) 
    {
      anObjectsToDisplay.Append( anObj );
    }
    else
    {
      anObjectsToErase.Append( anObj );
    }
  }

  if( anObjectsToErase.Length() )
    Erase( anObjectsToErase, theViewerId );
  if( anObjectsToDisplay.Length() )
    Display( anObjectsToDisplay, theViewerId, theIsForced, theDoFitAll );
}

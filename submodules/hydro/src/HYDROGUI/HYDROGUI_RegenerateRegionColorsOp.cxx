// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_RegenerateRegionColorsOp.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_DataModel.h>
#include <HYDROGUI_ZoneTool.h>
#include <QList>
#include <LightApp_UpdateFlags.h>
#include <HYDROGUI_UpdateFlags.h>

HYDROGUI_RegenerateRegionColorsOp::HYDROGUI_RegenerateRegionColorsOp( HYDROGUI_Module* theModule )
  : HYDROGUI_Operation( theModule )
{
}

HYDROGUI_RegenerateRegionColorsOp::~HYDROGUI_RegenerateRegionColorsOp()
{
}

void HYDROGUI_RegenerateRegionColorsOp::startOperation()
{
  HYDROGUI_Operation::startOperation();  
  QString outStr;
  if (HYDROGUI_Tool::IsSelectedPartOfCalcCase(module(), myCalcCase, outStr) && 
    outStr == HYDROGUI_DataModel::partitionName( KIND_REGION ))
    onApply();
  else
    onCancel();
}

bool HYDROGUI_RegenerateRegionColorsOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                                      QStringList& theBrowseObjectsEntries )
{
  theUpdateFlags = 0;
  if( myCalcCase )
  {
    const HYDROData_SequenceOfObjects& aRegions = myCalcCase->GetRegions();
    HYDROGUI_ZoneTool::AssignDefaultColors(aRegions, 100, 100);
    theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced;
    return true;
  }
  return false;
}


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRSZONE_H
#define HYDROGUI_PRSZONE_H

#include "HYDROGUI_Prs.h"

/*
  Class       : HYDROGUI_PrsZone
  Description : Presentation for zone object
*/
class HYDROGUI_PrsZone : public HYDROGUI_Prs
{
public:
  HYDROGUI_PrsZone( const Handle(HYDROData_Entity)& theObject );
  virtual ~HYDROGUI_PrsZone();

public:
  void                            setPath( const QPainterPath& thePath );
  QPainterPath                    getPath() const;

  void                            setFillingColor( const QColor& theColor );
  QColor                          getFillingColor() const;

  void                            setBorderColor( const QColor& theColor );
  QColor                          getBorderColor() const;

public:
  // from QGraphicsItem
  virtual QRectF                  boundingRect() const;

  // from GraphicsView_Object
  virtual void                    compute();

  virtual bool                    isMovable() const { return false; }

  virtual bool                    checkHighlight( double theX, double theY, QCursor& theCursor ) const;

  virtual bool                    select( double theX, double theY, const QRectF& theRect );
  virtual void                    unselect();
  virtual void                    setSelected( bool theState );

protected:
  QGraphicsPathItem*              myZoneItem;

  QPainterPath                    myPath;
  QColor                          myFillingColor;
  QColor                          myBorderColor;
};

#endif

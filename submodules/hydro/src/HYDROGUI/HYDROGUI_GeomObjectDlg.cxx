// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_GeomObjectDlg.h"

#include "HYDROGUI_Tool2.h"

#include <SUIT_FileDlg.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QToolButton>
#include <QRadioButton>
#include <QButtonGroup>
#include <QGroupBox>
#include <QLayout>

static QString lastUsedFilter;

HYDROGUI_GeomObjectDlg::HYDROGUI_GeomObjectDlg( HYDROGUI_Module* theModule, const QString& theTitle,
                                                const QString& theObjectTypeName,
                                                const bool theIsToEnableFileSelection )
: HYDROGUI_InputPanel( theModule, theTitle ),
  myFileSelectionEnabled( theIsToEnableFileSelection ),
  myDefaultName( "" )
{
  // Get resource manager
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();

  // File selector
  QGroupBox* aFileNameGroup = 0;
  myFileName = 0;
  if ( myFileSelectionEnabled ) {
    aFileNameGroup = new QGroupBox( tr( "GET_SHAPE_FROM_FILE" ), this );

    QLabel* aFileNameLabel = new QLabel( tr( "FILE_NAME" ), aFileNameGroup );

    myFileName = new QLineEdit( aFileNameGroup );
    myFileName->setReadOnly( true );

    QToolButton* aBrowseBtn = new QToolButton( aFileNameGroup );
    aBrowseBtn->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "BROWSE_ICO" ) ) );

    QBoxLayout* aFileNameLayout = new QHBoxLayout( aFileNameGroup );
    aFileNameLayout->setMargin( 5 );
    aFileNameLayout->setSpacing( 5 );
    aFileNameLayout->addWidget( aFileNameLabel );
    aFileNameLayout->addWidget( myFileName );
    aFileNameLayout->addWidget( aBrowseBtn );

    connect( aBrowseBtn, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
  }

  // Obstacle name
  myNameGroup = new QGroupBox( tr( "OBJECT_NAME" ).arg( theObjectTypeName ), this );

  QLabel* aNameLabel = new QLabel( tr( "NAME" ), myNameGroup );
  myObjectName = new QLineEdit( myNameGroup );

  QBoxLayout* anNameLayout = new QHBoxLayout( myNameGroup );
  anNameLayout->setMargin( 5 );
  anNameLayout->setSpacing( 5 );
  anNameLayout->addWidget( aNameLabel );
  anNameLayout->addWidget( myObjectName );

  // Mode selector (create/edit)
  myModeGroup = new QGroupBox( tr( "MODE" ), this );

  QRadioButton* aNewRB = new QRadioButton( tr( "CREATE_NEW" ), mainFrame() );
  QRadioButton* aModifyRB = new QRadioButton( tr( "MODIFY" ), mainFrame() );

  myModeButtons = new QButtonGroup( mainFrame() );
  myModeButtons->addButton( aNewRB, CreateNewId );
  myModeButtons->addButton( aModifyRB, ModifyExistentId );

  QBoxLayout* aModeSelectorLayout = new QHBoxLayout( myModeGroup );
  aModeSelectorLayout->setMargin( 5 );
  aModeSelectorLayout->setSpacing( 5 );
  aModeSelectorLayout->addWidget( aNewRB );
  aModeSelectorLayout->addWidget( aModifyRB );

  // Existing obstacles selector
  myObjectsGroup = new QGroupBox( tr( "OBJECT_TO_EDIT" ).arg( theObjectTypeName ), this );

  QLabel* anObjectLabel = new QLabel( theObjectTypeName, myObjectsGroup );

  myObjects = new QComboBox( mainFrame() );
  myObjects->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
 
  QBoxLayout* anObjectsSelectorLayout = new QHBoxLayout( myObjectsGroup );
  anObjectsSelectorLayout->setMargin( 5 );
  anObjectsSelectorLayout->setSpacing( 5 );
  anObjectsSelectorLayout->addWidget( anObjectLabel );
  anObjectsSelectorLayout->addWidget( myObjects );

  // Layout
  if ( aFileNameGroup ) {
    addWidget( aFileNameGroup );
  }
  addWidget( myNameGroup );
  addWidget( myModeGroup );
  addWidget( myObjectsGroup );
  addStretch();

  // Connect signals and slots
  connect( myModeButtons, SIGNAL( buttonClicked( int ) ), this, SLOT( onModeActivated( int ) ) );
  connect( myObjects, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onObjectSelectionChanged( ) ) );
}

HYDROGUI_GeomObjectDlg::~HYDROGUI_GeomObjectDlg()
{
}

void HYDROGUI_GeomObjectDlg::setObjectNames( const QStringList& theNames )
{
  bool isBlocked = myObjects->blockSignals( true );
  myObjects->clear();
  myObjects->addItems( theNames );
  myObjects->blockSignals( isBlocked );

  updateControls();
}

void HYDROGUI_GeomObjectDlg::onModeActivated( int theMode )
{
  updateControls();
  updateObjectName();
}

QString HYDROGUI_GeomObjectDlg::getObjectName() const
{
  return myObjectName->text();


  QString aName;

  if ( myModeButtons->checkedId() == ModifyExistentId ) {
    aName = myObjects->currentText();
  }

  return aName;
}

QString HYDROGUI_GeomObjectDlg::getEditedObjectName() const
{
  QString aName;

  if ( myModeButtons->checkedId() == ModifyExistentId ) {
    aName = myObjects->currentText();
  }

  return aName;
}

QString HYDROGUI_GeomObjectDlg::getFileName() const
{
  return myFileName->text();
}

void HYDROGUI_GeomObjectDlg::onBrowse()
{
  SUIT_FileDlg* aFileDlg = new SUIT_FileDlg( this, true );
  aFileDlg->setWindowTitle( tr("IMPORT_OBSTACLE_FROM_FILE") );
  aFileDlg->setNameFilter( tr("OBSTACLE_FILTER") );
  if ( !lastUsedFilter.isEmpty() ) {
    aFileDlg->selectNameFilter( lastUsedFilter );
  }

  if ( aFileDlg->exec() == QDialog::Accepted ) {
    QString aFileName = aFileDlg->selectedFile();
    lastUsedFilter = aFileDlg->selectedNameFilter();

    if ( !aFileName.isEmpty() ) {
      myFileName->setText( aFileName );
      updateObjectName();
    }

    updateControls();
  }
}

void HYDROGUI_GeomObjectDlg::reset()
{
  myDefaultName.clear();
  if ( myFileSelectionEnabled ) {
    myFileName->clear();
  }
  myObjectName->clear();

  // Activate the creation mode
  myModeButtons->button( CreateNewId )->setChecked( true );
  onModeActivated( CreateNewId );

  updateControls();
}

void HYDROGUI_GeomObjectDlg::onObjectSelectionChanged()
{
  updateObjectName();
}

void HYDROGUI_GeomObjectDlg::updateControls()
{
  myNameGroup->setEnabled( !myFileSelectionEnabled || 
                           !myFileName->text().isEmpty() );
  myModeGroup->setEnabled( myNameGroup->isEnabled() && 
                           myObjects->count() > 0 );
  myObjectsGroup->setEnabled( myModeButtons->checkedId() == ModifyExistentId );
}

void HYDROGUI_GeomObjectDlg::updateObjectName()
{
  QString aName;

  // Creation mode
  int aMode = myModeButtons->checkedId();
  if ( aMode == CreateNewId ) {
    if ( myFileSelectionEnabled ) {
      QString aFileName = myFileName->text();
      if ( !aFileName.isEmpty() ) {
        QFileInfo aFileInfo( aFileName );
        aName = HYDROGUI_Tool::GenerateObjectName( 
          module(), aFileInfo.baseName(), QStringList(), true );
      }
    } else {
      aName = HYDROGUI_Tool::GenerateObjectName( 
        module(), getDefaultName(), QStringList(), true );
    }
  } else if ( aMode == ModifyExistentId ) {
      aName = getEditedObjectName();
  }

  myObjectName->setText( aName );
}

void HYDROGUI_GeomObjectDlg::setDefaultName( const QString& theName )
{
  myDefaultName = theName;

  updateObjectName();
}

QString HYDROGUI_GeomObjectDlg::getDefaultName()
{
  return myDefaultName;
}

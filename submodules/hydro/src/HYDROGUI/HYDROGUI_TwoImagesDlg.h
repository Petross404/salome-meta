// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_TWOIMAGESDLG_H
#define HYDROGUI_TWOIMAGESDLG_H

#include "HYDROGUI_InputPanel.h"

class QCheckBox;
class QLabel;
class QLineEdit;
class QRadioButton;
class QFrame;

class HYDROGUI_ColorWidget;
class HYDROGUI_ObjSelector;

class HYDROGUI_TwoImagesDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  enum Mode { TwoFuseImage = 0, TwoCutImage, ImageAndPolyline };

public:
  HYDROGUI_TwoImagesDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_TwoImagesDlg();

  void                       reset();

  void                       setMode( const int theMode, const bool theIsEdit );

  bool                       isModifySelected() const;

  void                       setImageName( const QString& theName );
  QString                    getImageName() const;

  void                       setSelectedObjects( const QString& theName1,
                                                 const QString& theName2 );
  bool                       getSelectedObjects( QString& theName1,
                                                 QString& theName2 ) const;

  void                       setPreselectedObject( const QString& theName );

  void                       setColor( const QColor& theColor );
  QColor                     getColor() const;

signals:
  /** Signal is emitted if the name has already been selected 
   *  in other selector of the same parent widget. 
   * @param theName the selected object name
   */
  void                       alreadySelected( const QString& theName );

protected slots:
  void                       onModifySelectedImage( bool theState );

private:
  /**
   * Returns whether the mode is to select two images, that corresponds to cut or fuse mode
   */
  bool                       isTwoImagesMode() const;

private:
  int                        myMode;
  bool                       myIsEdit;

  QLineEdit*                 myImageName;
  QCheckBox*                 myModifySelectedImage;

  QLabel*                    myImage1Label;
  HYDROGUI_ObjSelector*      myImage1;

  QLabel*                    myImage2Label;
  HYDROGUI_ObjSelector*      myImage2;

  QLabel*                    myPolylineLabel;
  HYDROGUI_ObjSelector*      myPolyline;

  QFrame*                    myBackgroundFrame;
  QRadioButton*              myTransparent;
  QRadioButton*              myColor;
  HYDROGUI_ColorWidget*      myColorBox;
};

#endif

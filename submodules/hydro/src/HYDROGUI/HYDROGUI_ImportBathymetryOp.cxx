// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportBathymetryOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_ImportBathymetryDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROGUI_OCCDisplayer.h>

#include <HYDROData_Bathymetry.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>
#include <SUIT_ViewManager.h>
#include <SVTK_ViewModel.h>

#include <OCCViewer_ViewManager.h>
#include <QFileInfo>
#include <QSet>
#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>
#include <HYDROGUI_ShapeBathymetry.h>


HYDROGUI_ImportBathymetryOp::HYDROGUI_ImportBathymetryOp( HYDROGUI_Module* theModule, 
                                                         const bool theIsEdit  )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit )
{
  setName( theIsEdit ? tr( "EDIT_IMPORTED_BATHYMETRY" ) : tr( "IMPORT_BATHYMETRY" ) );
}

HYDROGUI_ImportBathymetryOp::~HYDROGUI_ImportBathymetryOp()
{
}

void HYDROGUI_ImportBathymetryOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_ImportBathymetryDlg* aPanel = 
    ::qobject_cast<HYDROGUI_ImportBathymetryDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->reset();
  aPanel->SetEditMode(myIsEdit);

  if( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_Bathymetry)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !myEditedObject.IsNull() )
    {
      QString aName = myEditedObject->GetName();
      QStringList aFileNames = myEditedObject->GetFilePaths();
      bool anIsAltitudesInverted = myEditedObject->IsAltitudesInverted();

      aPanel->setFuseIntoOneOptionChecked( true );

      aPanel->setObjectName( aName );
      aPanel->addFileNames( aFileNames ); 
      aPanel->setInvertAltitudes( anIsAltitudesInverted );
      aPanel->setFuseIntoOneOptionEnabled( false );
    }
  }
}

void HYDROGUI_ImportBathymetryOp::abortOperation()
{
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ImportBathymetryOp::commitOperation()
{
  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_ImportBathymetryOp::createInputPanel() const
{
  HYDROGUI_InputPanel* aPanel = new HYDROGUI_ImportBathymetryDlg( module(), getName() );
  
  connect ( aPanel, SIGNAL( FileSelected( const QStringList& ) ), SLOT( onFileSelected() ) );

  return aPanel;
}

bool HYDROGUI_ImportBathymetryOp::processApply( int& theUpdateFlags,
                                                QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_ImportBathymetryDlg* aPanel = 
    ::qobject_cast<HYDROGUI_ImportBathymetryDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QString anObjectName = aPanel->getObjectName().simplified();
  if ( anObjectName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  QStringList aFileNames = aPanel->getFileNames();

  QStringList DummyFileList;
  foreach (QString str, aFileNames)
    DummyFileList << str.simplified();

  aFileNames = DummyFileList;
  DummyFileList.clear();

  if ( aFileNames.isEmpty() )
  {
    theErrorMsg = tr( "EMPTY_FILENAMES" );
    return false;
  }

  QString inexistWarn;

  foreach (QString aFileName, aFileNames )
  {
    QFileInfo aFileInfo( aFileName );
    if ( !aFileInfo.exists() || !aFileInfo.isReadable() )
    {
      inexistWarn += "\n" + aFileName;
      continue;
    }
    DummyFileList << aFileName;
  }

  if (!inexistWarn.isNull())
    SUIT_MessageBox::warning( module()->getApp()->desktop(), 
    tr( "BATHYMETRY_IMPORT_WARNING" ), "Can't read the next files:" + inexistWarn );

  aFileNames = DummyFileList;

  bool isFuseIntoOneOption = aPanel->isFuseIntoOneOptionChecked(); 
  bool anIsInvertAltitudes = aPanel->isInvertAltitudes();

  QString replacemWarn;
  QString UnreadFilesWarn;

  if ( myIsEdit )
  {
    //edit already existing bath
    if (myEditedObject.IsNull())
      return false;
    QStringList anOldFileNames = myEditedObject->GetFilePaths(); 
    //bool anIsInvertAltitudes = aPanel->isInvertAltitudes();
    //myEditedObject->SetAltitudesInverted( anIsInvertAltitudes, false );
    if ( aFileNames.toSet() != anOldFileNames.toSet() )
    {
      myEditedObject->SetAltitudesInverted( anIsInvertAltitudes, true );
      if ( !myEditedObject->ImportFromFiles( aFileNames ) )
      {
        theErrorMsg = tr( "BAD_IMPORTED_BATHYMETRY_FILE" ).arg( aFileNames.join("\n") );
        return false;
      }
    }
    else if ( anIsInvertAltitudes != myEditedObject->IsAltitudesInverted() )
      myEditedObject->SetAltitudesInverted( anIsInvertAltitudes, true );

    myEditedObject->SetName( anObjectName );
    myEditedObject->Update();
  }
  else
  {
    //create the new one
    if (isFuseIntoOneOption)
    {
      Handle(HYDROData_Bathymetry) aBathymetryObj = Handle(HYDROData_Bathymetry)::DownCast( doc()->CreateObject( KIND_BATHYMETRY ) );
      if ( aBathymetryObj.IsNull() )
        return false;
      aBathymetryObj->SetAltitudesInverted( anIsInvertAltitudes, false );
      if ( !aBathymetryObj->ImportFromFiles( aFileNames ) )
      {
        theErrorMsg = tr( "BAD_IMPORTED_BATHYMETRY_FILE" ).arg( aFileNames.join("\n") );
        return false;
      }

      QString aNewObjName;
      if (CheckNameExistingBathy(anObjectName, aNewObjName))
      {
        aBathymetryObj->SetName( aNewObjName );
        replacemWarn += "\n'" + anObjectName + "' => '" + aNewObjName + "'";
      }
      else
        aBathymetryObj->SetName( anObjectName );

      aBathymetryObj->SetName( anObjectName );
      aBathymetryObj->Update();
      QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aBathymetryObj );
      theBrowseObjectsEntries.append( anEntry );
    }
    else //myedit off + non-fuse => import separate files
    {
      bool AtLeastOneWasImported = false;
      foreach (QString filename, aFileNames)
      {
        Handle(HYDROData_Bathymetry) aBathymetryObj = Handle(HYDROData_Bathymetry)::DownCast( doc()->CreateObject( KIND_BATHYMETRY ) );
        if ( aBathymetryObj.IsNull() )
          continue;
        aBathymetryObj->SetAltitudesInverted( anIsInvertAltitudes, false );
        if ( !aBathymetryObj->ImportFromFiles( QStringList(filename) ) )
        {
          UnreadFilesWarn += "\n" + filename;
          continue;
        }

        QString anObjectName = QFileInfo( filename ).baseName();
        QString aNewObjName;
        if (CheckNameExistingBathy(anObjectName, aNewObjName))
        {
          aBathymetryObj->SetName( aNewObjName );
          replacemWarn += "\n'" + anObjectName + "' => '" + aNewObjName + "'";
        }
        else
          aBathymetryObj->SetName( anObjectName );

        AtLeastOneWasImported = true;
        aBathymetryObj->Update();
        QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aBathymetryObj );
        theBrowseObjectsEntries.append( anEntry );
      }
      if (!AtLeastOneWasImported)
      {
        theErrorMsg = tr( "BAD_IMPORTED_BATHYMETRY_FILES" ).arg( aFileNames.join("\n") ); 
        return false;
      }
    }
  }

  if (!UnreadFilesWarn.isNull())
    SUIT_MessageBox::warning( module()->getApp()->desktop(), 
    tr( "BATHYMETRY_IMPORT_WARNING" ), "The next files cannot be imported:" + UnreadFilesWarn );


  if (!replacemWarn.isNull())
    SUIT_MessageBox::warning( module()->getApp()->desktop(), 
    tr( "BATHYMETRY_IMPORT_WARNING" ), "The next objects names are already exist in the document; so the new objects was renamed:" + replacemWarn );

  // Activate VTK viewer and show the bathymetry
  SUIT_ViewManager* aVTKMgr = 0;
  SUIT_ViewManager* aViewMgr = module()->getApp()->activeViewManager();
  // Try to get a VTK viewer as an active or existing one
  if ( aViewMgr )
  {
    if ( aViewMgr->getType() == SVTK_Viewer::Type() )
    {
      aVTKMgr = aViewMgr;
    }
    else
    {
      aVTKMgr = module()->getApp()->viewManager( SVTK_Viewer::Type() );
    }
  }
    
  OCCViewer_ViewManager* mgr = dynamic_cast<OCCViewer_ViewManager*>(aViewMgr);
  OCCViewer_Viewer* occ_viewer = mgr->getOCCViewer();
  int aViewerId = (size_t)(occ_viewer);

  HYDROGUI_Shape* aObjSh = module()->getObjectShape( aViewerId, myEditedObject );
  HYDROGUI_ShapeBathymetry* aBathSh = dynamic_cast<HYDROGUI_ShapeBathymetry*>( aObjSh );
  if (aBathSh)
  {
    aBathSh->update(false, false);
    aBathSh->RescaleDefault();
    module()->getOCCDisplayer()->UpdateColorScale( occ_viewer );
  }

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced |
    UF_VTKViewer | UF_VTK_Forced;

  return true;
}
 
void HYDROGUI_ImportBathymetryOp::onFileSelected()
{
  HYDROGUI_ImportBathymetryDlg* aPanel = 
    ::qobject_cast<HYDROGUI_ImportBathymetryDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->UpdateCheckBoxStates();
}

bool HYDROGUI_ImportBathymetryOp::CheckNameExistingBathy(const QString& InpName, QString& OutputName)
{
  Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), InpName );
  if (anObject.IsNull())
    return false;
  else
  {
    OutputName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_BATHYMETRY_NAME" ) );
    return true;
  }
}



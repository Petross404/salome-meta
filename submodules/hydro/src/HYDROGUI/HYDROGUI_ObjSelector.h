// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_OBJSELECTOR_H
#define HYDROGUI_OBJSELECTOR_H

#include <HYDROData_Object.h>

#include <QAbstractButton>

class QToolButton;
class QLineEdit;
class HYDROGUI_Module;

class HYDROGUI_ObjSelector : public QAbstractButton
{
  Q_OBJECT

public:
  enum ObjectFlags {
    NoFlags        = 0x00000000,
    ClosedPolyline = 0x00000001
  };

public:
  HYDROGUI_ObjSelector( HYDROGUI_Module* theModule,
                        const ObjectKind theObjectKind,
                        QWidget* theParent, 
                        const int theObjectFlags = NoFlags );
  virtual ~HYDROGUI_ObjSelector();

  void Clear();
  void SetChecked( const bool );

  void SetName( const QString& );
  QString GetName() const;

signals:
  /** Signal is emitted if the name has already been selected 
   *  in other selector of the same parent widget. 
   * @param theName the selected object name
   */
  void alreadySelected( const QString& theName );

  void selectionChanged();

protected:
  virtual void paintEvent( QPaintEvent* );
  virtual bool hitButton( const QPoint& thePnt ) const;

protected slots:
  void OnToggled( bool );
  void OnSelectionChanged();

private:
  HYDROGUI_Module* myModule;
  ObjectKind myObjectKind;

  QToolButton* myBtn;
  QLineEdit* myObjName;

  int myObjectFlags;
};

#endif

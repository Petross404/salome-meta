// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_RecognizeContoursOp_H
#define HYDROGUI_RecognizeContoursOp_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_Image.h>

#include <QMap>
#include <QStringList>

class QTemporaryFile;


class HYDROGUI_RecognizeContoursOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_RecognizeContoursOp( HYDROGUI_Module* theModule );
  virtual ~HYDROGUI_RecognizeContoursOp();

protected:
  virtual void startOperation();
  virtual void abortOperation();
  virtual void commitOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool processApply( int& theUpdateFlags, QString& theErrorMsg,
                             QStringList& theBrowseObjectsEntries );

protected slots:
  void onExternalOperationFinished( const QString&, const QString&, 
                                    const QStringList& );

  void onSelectionChanged( const QStringList& theSelectedNames );
  void onViewerSelectionChanged();

private:
  void closeExternalOperationDlg();

  void updateRecognizedPolylines();

  void erasePreview();

  void cleanup();

private:
  Handle(HYDROData_Image) myImage; ///< the image used for contours detection

  QTemporaryFile* myTmpImageFile;  ///< the temporary graphics file

  QStringList myTmpGeomObjects;    ///< the list of temporary GEOM objects entries
  QString myGeomContourEntry;      ///< the GEOM contour object entry
  QString myGEOMOpName;            ///< the name of the called GEOM module operation
  
  QMap<QString, HYDROGUI_Shape*> myPolylineShapes; ///< the detected polylines shapes map (key: name, value: shape)
};

#endif
// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_MergePolylinesOp.h>
#include <HYDROGUI_MergePolylinesDlg.h>
#include <HYDROGUI_UpdateFlags.h>
#include <HYDROData_Document.h>
#include <HYDROData_PolylineOperator.h>

HYDROGUI_MergePolylinesOp::HYDROGUI_MergePolylinesOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "MERGE_POLYLINES" ) );
}

HYDROGUI_MergePolylinesOp::~HYDROGUI_MergePolylinesOp()
{
}

void HYDROGUI_MergePolylinesOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_MergePolylinesDlg* aPanel = 
    ::qobject_cast<HYDROGUI_MergePolylinesDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->setPolylinesFromSelection();
}

HYDROGUI_InputPanel* HYDROGUI_MergePolylinesOp::createInputPanel() const
{
  return new HYDROGUI_MergePolylinesDlg( module(), getName() );
}

bool HYDROGUI_MergePolylinesOp::processApply( int& theUpdateFlags,
                                              QString& theErrorMsg,
                                              QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_MergePolylinesDlg* aPanel = ::qobject_cast<HYDROGUI_MergePolylinesDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QString aName = aPanel->GetResultName();
  bool isConnectByNewSegment = aPanel->IsConnectByNewSegment();
  HYDROData_SequenceOfObjects aPolylinesList = aPanel->selectedPolylines();
  HYDROData_PolylineOperator anOp;
  double aTolerance = 1E-7; //TODO
  anOp.Merge( doc(), aName.toLatin1().data(), aPolylinesList, isConnectByNewSegment, aTolerance );

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
  return true;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportBathymetryDlg.h"

#include "HYDROGUI_Tool.h"

#include <SUIT_FileDlg.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <QListWidget>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QPicture>
#include <QPushButton>
#include <QCheckBox>

HYDROGUI_ImportBathymetryDlg::HYDROGUI_ImportBathymetryDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ), myIsEdit ( false )
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();

  // Import bathymetry from file
  myFileNameGroup = new QGroupBox( tr( "IMPORT_BATHYMETRY_FROM_FILE" ) );

  QLabel* aFileNameLabel = new QLabel( tr( "FILE_NAMES" ), myFileNameGroup );

  myFileNames = new QListWidget( myFileNameGroup );
  myFileNames->setSelectionMode( QAbstractItemView::ExtendedSelection);
  //myFileNames->viewport()->setAttribute( Qt::WA_TransparentForMouseEvents );
  //myFileNames->setFocusPolicy(Qt::FocusPolicy::NoFocus);

  QPushButton* aBrowseBtn = new QPushButton( myFileNameGroup );
  aBrowseBtn->setText("Add files(s)");
  aBrowseBtn->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "BROWSE_ICO" ) ) );

  QPushButton* aRemoveBtn = new QPushButton( myFileNameGroup );
  aRemoveBtn->setText("Remove selected file(s)");

  QBoxLayout* aFileNameLayout = new QVBoxLayout( myFileNameGroup );

  myFuseIntoOne = new QCheckBox();
  myFuseIntoOne->setText("Import as one bathymetry object (fuse the geometry data)");
  myFuseIntoOne->setEnabled( false );

  aFileNameLayout->setMargin( 5 );
  aFileNameLayout->setSpacing( 5 );
  aFileNameLayout->addWidget( aFileNameLabel );  
  aFileNameLayout->addWidget( aBrowseBtn );
  aFileNameLayout->addWidget( aRemoveBtn );
  aFileNameLayout->addWidget( myFileNames );
  aFileNameLayout->addWidget( myFuseIntoOne );

  // Bathymetry name
  myObjectNameGroup = new QGroupBox( tr( "BATHYMETRY_NAME" ) );

  QLabel* aBathymetryNameLabel = new QLabel( tr( "NAME" ), myObjectNameGroup );
  myObjectName = new QLineEdit( myObjectNameGroup );

  QBoxLayout* aBathymetryNameLayout = new QHBoxLayout( myObjectNameGroup );
  aBathymetryNameLayout->setMargin( 5 );
  aBathymetryNameLayout->setSpacing( 5 );
  aBathymetryNameLayout->addWidget( aBathymetryNameLabel );
  aBathymetryNameLayout->addWidget( myObjectName );

  myInvertAltitudes = new QCheckBox( tr( "INVERT_BATHYMETRY_ALTITUDES" ), this );

  // Common
  addWidget( myFileNameGroup );
  addWidget( myObjectNameGroup );
  addWidget( myInvertAltitudes );

  addStretch();

  connect( aBrowseBtn, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
  connect( aRemoveBtn, SIGNAL( clicked() ), this, SLOT( onRemove() ) );
  connect( myFuseIntoOne, SIGNAL( stateChanged(int) ), this, SLOT( onStateChanged(int) ) );
}

HYDROGUI_ImportBathymetryDlg::~HYDROGUI_ImportBathymetryDlg()
{
}

void HYDROGUI_ImportBathymetryDlg::reset()
{
  myFileNames->clear();
  myObjectName->clear();
  myObjectNameGroup->setEnabled( false );
  myFuseIntoOne->setEnabled( false );
}

void HYDROGUI_ImportBathymetryDlg::SetEditMode(bool isEdit)
{
  myIsEdit = isEdit;
}

void HYDROGUI_ImportBathymetryDlg::setObjectName( const QString& theName )
{
  myObjectName->setText( theName );
  bool ObjNameState = myObjectName->isEnabled();
  myObjectNameGroup->setEnabled( !theName.isEmpty() || myFileNames->count() > 0 );
  myObjectName->setEnabled(ObjNameState);
  myFuseIntoOne->setEnabled( myFileNames->count() > 1 );
}

QString HYDROGUI_ImportBathymetryDlg::getObjectName() const
{
  return myObjectName->text();
}

void HYDROGUI_ImportBathymetryDlg::addFileNames( const QStringList& theFileNames )
{
  //myFileNames->clear();
  QSet<QString> exfilenames = getFileNames().toSet();
  QStringList newFileNames;
  foreach (QString str, theFileNames)
    if (!exfilenames.contains(str))
      newFileNames.append(str);
  myFileNames->addItems( newFileNames );
  UpdateCheckBoxStates();
}

void HYDROGUI_ImportBathymetryDlg::removeFileNames( const QList<QListWidgetItem*>& theFileNamesToRem )
{
  foreach (QListWidgetItem* item, theFileNamesToRem)
    delete myFileNames->takeItem(myFileNames->row(item));
  UpdateCheckBoxStates();
}


void HYDROGUI_ImportBathymetryDlg::UpdateCheckBoxStates()
{
  if (myFuseIntoOne->isChecked() || myFileNames->count() == 1 )
  {
    myObjectNameGroup->setEnabled( true );
    myObjectName->setEnabled( true );
  }
  else
  {
    myObjectNameGroup->setEnabled( false );
    myObjectName->setEnabled( false );
  }
  //
  if (myFileNames->count() <= 1 || myIsEdit)
    myFuseIntoOne->setEnabled( false );
  else
    myFuseIntoOne->setEnabled( true );

  QString anObjectName = getObjectName().simplified();
  
  QStringList aFileNames = getFileNames();
  if ( anObjectName.isEmpty() )
  {
    if (!aFileNames.empty())
    {
      anObjectName = aFileNames[0];
      if ( !anObjectName.isEmpty() )
        anObjectName = QFileInfo( anObjectName ).baseName();
    }
    setObjectName( anObjectName );
  }

}

QStringList HYDROGUI_ImportBathymetryDlg::getFileNames() const
{
  QStringList stritems;
  for(int i = 0; i < myFileNames->count(); ++i)
  {
    QListWidgetItem* item = myFileNames->item(i);
    stritems << item->text();
  }
  return stritems;
}

void HYDROGUI_ImportBathymetryDlg::setInvertAltitudes( const bool theIsInvert )
{
  myInvertAltitudes->setChecked( theIsInvert );
}

bool HYDROGUI_ImportBathymetryDlg::isInvertAltitudes() const
{
  return myInvertAltitudes->isChecked();
}

void HYDROGUI_ImportBathymetryDlg::setFuseIntoOneOptionChecked( bool isFIO )
{
  myFuseIntoOne->setChecked( isFIO );
}

bool HYDROGUI_ImportBathymetryDlg::isFuseIntoOneOptionChecked() const
{
  return myFuseIntoOne->isChecked();
}

void HYDROGUI_ImportBathymetryDlg::setFuseIntoOneOptionEnabled(bool enabled)
{
  myFuseIntoOne->setEnabled(enabled);
}

bool HYDROGUI_ImportBathymetryDlg::isFuseIntoOneOptionEnabled() const
{
  return myFuseIntoOne->isEnabled();
}

void HYDROGUI_ImportBathymetryDlg::onBrowse()
{
  QString aFilter( tr( "BATHYMETRY_FILTER" ) );
  QStringList aFileNames = SUIT_FileDlg::getOpenFileNames( this, "", aFilter, tr( "IMPORT_BATHYMETRY_FROM_FILE" ), true );

  if( !aFileNames.isEmpty() )
  {
    addFileNames( aFileNames );
    emit FileSelected( aFileNames );
  }
}

void HYDROGUI_ImportBathymetryDlg::onRemove()
{
  QList<QListWidgetItem*> selected = myFileNames->selectedItems();

  removeFileNames(selected);
}

void HYDROGUI_ImportBathymetryDlg::onStateChanged (int state)
{
  if (getFileNames().count() > 1)
  {
    if (state == Qt::Checked)
    {
      myObjectName->setEnabled(true);
      myObjectNameGroup->setEnabled(true);
    }
    else if (state == Qt::Unchecked)
    {
      myObjectName->setEnabled(false);
      myObjectNameGroup->setEnabled(false);
    }
  }
}



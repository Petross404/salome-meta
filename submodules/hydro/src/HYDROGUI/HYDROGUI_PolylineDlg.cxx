// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PolylineDlg.h"

#include "HYDROGUI_Module.h"
#include <CurveCreator_Widget.h>
#include <CurveCreator_ICurve.hxx>

#include <OCCViewer_ViewModel.h>

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QScrollArea>

HYDROGUI_PolylineDlg::HYDROGUI_PolylineDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ), myName(NULL)
{
  QScrollArea* aScrollView = new QScrollArea( this );
  aScrollView->setFrameStyle( QFrame::NoFrame );
  addWidget( aScrollView );

  QWidget* aContent = new QWidget( aScrollView );
  QVBoxLayout* aCLayout = new QVBoxLayout( aContent );
  aCLayout->setMargin( 0 );

  aScrollView->setWidget( aContent );
  aScrollView->setWidgetResizable( true );

  QHBoxLayout* aNameLayout = new QHBoxLayout();
  QLabel* aNameLabel = new QLabel(tr("POLYLINE_NAME_TLT"), aContent);
  aNameLayout->addWidget(aNameLabel);
  myName = new QLineEdit(aContent);
  aNameLayout->addWidget(myName);

  aCLayout->addLayout(aNameLayout);

  myEditorWidget = new CurveCreator_Widget( aContent, NULL );
  aCLayout->addWidget( myEditorWidget, 3 );

  myAddElementBox = new QGroupBox( tr( "ADD_ELEMENT" ), aContent );
  aCLayout->addWidget( myAddElementBox, 2 );

  QBoxLayout* anAddElementLayout = new QVBoxLayout( myAddElementBox );
  anAddElementLayout->setMargin( 0 );
  anAddElementLayout->setSpacing( 5 );

  connect( myEditorWidget, SIGNAL( selectionChanged() ), this, SIGNAL( selectionChanged() ) );
  connect( myEditorWidget, SIGNAL( subOperationStarted(QWidget*, bool) ), this, SLOT( processStartedSubOperation(QWidget*, bool) ) );
  connect( myEditorWidget, SIGNAL( subOperationFinished(QWidget*) ), this, SLOT( processFinishedSubOperation(QWidget*) ) );

  myAddElementBox->hide();
}

HYDROGUI_PolylineDlg::~HYDROGUI_PolylineDlg()
{
}

void HYDROGUI_PolylineDlg::setOCCViewer( OCCViewer_Viewer* theViewer )
{
  myEditorWidget->setOCCViewer( theViewer );
}

void HYDROGUI_PolylineDlg::processStartedSubOperation( QWidget* theWidget, bool theIsEdit )
{
  myEditorWidget->setEnabled( false );

  myAddElementBox->setTitle( theIsEdit ? tr( "EDIT_ELEMENT" ) : tr( "ADD_ELEMENT" ) );
  QBoxLayout* anAddElementLayout = dynamic_cast<QBoxLayout*>( myAddElementBox->layout() );
  anAddElementLayout->addWidget( theWidget );

  theWidget->show();
  myAddElementBox->show();
}

void HYDROGUI_PolylineDlg::processFinishedSubOperation( QWidget* theWidget )
{
  myEditorWidget->setEnabled( true );

  QBoxLayout* anAddElementLayout = dynamic_cast<QBoxLayout*>( myAddElementBox->layout() );
  anAddElementLayout->removeWidget( theWidget );

  theWidget->hide();
  myAddElementBox->hide();
}

void HYDROGUI_PolylineDlg::reset()
{
}

void HYDROGUI_PolylineDlg::setPolylineName( const QString& theName )
{
  myName->setText(theName);
}

QString HYDROGUI_PolylineDlg::getPolylineName() const
{
  return myName->text();
}

void HYDROGUI_PolylineDlg::setCurve( CurveCreator_ICurve* theCurve )
{
  myEditorWidget->setCurve( theCurve );
}

QList<int> HYDROGUI_PolylineDlg::getSelectedSections()
{
  return myEditorWidget->getSelectedSections();
}

/**
 * Redirect the delete action to editor widget
 */
void HYDROGUI_PolylineDlg::deleteSelected()
{
  myEditorWidget->removeSelected();
}

/**
 * Checks whether there are some to delete
 */
bool HYDROGUI_PolylineDlg::deleteEnabled()
{
  return myEditorWidget->removeEnabled();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef _HYDROGUI_Polyline_HeaderFile
#define _HYDROGUI_Polyline_HeaderFile

#include <AIS_Shape.hxx>
#include <QList>
#include <NCollection_IndexedDataMap.hxx>
#include <Quantity_Color.hxx>
#include <TopTools_ShapeMapHasher.hxx>

class TopoDS_Shape;
class TopoDS_Edge;

class HYDROGUI_Polyline : public AIS_Shape
{
public:
  HYDROGUI_Polyline( const TopoDS_Shape& shape );
  virtual ~HYDROGUI_Polyline();

  virtual void Compute(const Handle(PrsMgr_PresentationManager3d)& aPresentationManager,
				       const Handle(Prs3d_Presentation)& aPresentation,
				       const Standard_Integer aMode = 0);
  
  static QList<Handle(AIS_InteractiveObject)> createPresentations
    ( const TopoDS_Shape& theShape, int theType, int theSize );
  static void update( const QList<Handle(AIS_InteractiveObject)>&, int theType, int theSize );

  NCollection_IndexedDataMap<TopoDS_Shape, Quantity_Color, TopTools_ShapeMapHasher> myShapeToColor;

  bool GetColorOfSubShape(const TopoDS_Shape& SubShape, Quantity_Color& outColor);

public:
  DEFINE_STANDARD_RTTIEXT(HYDROGUI_Polyline, AIS_Shape);

};

class HYDROGUI_Arrow : public AIS_Shape
{
public:
  enum Type { None, Triangle, Cone };

  HYDROGUI_Arrow( const TopoDS_Edge& edge, const Handle(HYDROGUI_Polyline)& polyline );
  virtual ~HYDROGUI_Arrow();

  virtual void Compute( const Handle(PrsMgr_PresentationManager3d)& aPresentationManager,
				                const Handle(Prs3d_Presentation)& aPresentation,
				                const Standard_Integer aMode = 0 );
  
  virtual void BoundingBox (Bnd_Box& theBndBox) override;

  Type GetType() const;
  void SetType( Type );

  int  GetSize() const;
  void SetSize( int );

  const Handle(HYDROGUI_Polyline)& getParentPolyline() const;

public:
  DEFINE_STANDARD_RTTIEXT(HYDROGUI_Arrow, AIS_Shape);

private:
  Type myType;
  int  mySize;
  Handle(HYDROGUI_Polyline) myParentPoly;
};

#endif

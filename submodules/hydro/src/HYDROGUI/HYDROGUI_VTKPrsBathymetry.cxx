// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VTKPrsBathymetry.h"

#include <HYDROData_Entity.h>
#include <HYDROData_Tool.h>

#include <SALOME_Actor.h>
#include <gp_Pnt.hxx>
#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataMapper.h>
#include <vtkVertex.h>
#include <vtkScalarBarActor.h>

#include <QString>

/*! \def Z_MAX
    \brief Maximum Z value used in bathymetry presentation.

    This value is used instead of invalid values.
*/
#define Z_MAX 1

//=======================================================================
// name    : HYDROGUI_VTKPrsBathymetry
// Purpose : Constructor
//=======================================================================
HYDROGUI_VTKPrsBathymetry::HYDROGUI_VTKPrsBathymetry( const Handle(HYDROData_Bathymetry)& theObject )
: HYDROGUI_VTKPrs( theObject )
{
}

//=======================================================================
// name    : HYDROGUI_VTKPrsBathymetry
// Purpose : Destructor
//=======================================================================
HYDROGUI_VTKPrsBathymetry::~HYDROGUI_VTKPrsBathymetry()
{
}

//================================================================
// Function : compute
// Purpose  : 
//================================================================
void HYDROGUI_VTKPrsBathymetry::compute()
{
  HYDROGUI_VTKPrs::compute();

  if ( !getObject().IsNull() )
  {
    Handle(HYDROData_Bathymetry) aBathymetry = Handle(HYDROData_Bathymetry)::DownCast( getObject() );
    if ( !aBathymetry.IsNull() )
    {
      HYDROData_Bathymetry::AltitudePoints anAltPoints = aBathymetry->GetAltitudePoints();
      int aNbPoints = anAltPoints.size();

      HYDROData_Bathymetry::AltitudePoint anAltPnt;
      vtkPoints* aPoints = vtkPoints::New();
      aPoints->SetNumberOfPoints( aNbPoints );

      vtkPolyData* aVertexGrid = vtkPolyData::New();
      aVertexGrid->Allocate( aNbPoints );

      vtkDoubleArray* aZValues = vtkDoubleArray::New();
      aZValues->Allocate( aNbPoints );

      vtkVertex* aVertex = vtkVertex::New();

      double aZ;
      int anInvalidZ = InvalidZValue();
      for (int i = 0; i < aNbPoints; i++ )
      {
        anAltPnt = anAltPoints[i];
        aZ = anAltPnt.Z;
        if ( ValuesLessEquals( aZ, anInvalidZ ) )
        {
          aZ = Z_MAX; // If Z value is invalid then use Z_MAX
        }
        aPoints->InsertPoint( i, anAltPnt.X, anAltPnt.Y, aZ );
        aVertex->GetPointIds()->SetId( 0, i );
        aVertexGrid->InsertNextCell( aVertex->GetCellType(), aVertex->GetPointIds());
        aZValues->InsertNextValue( aZ );
      }

      aVertex->Delete();

      aVertexGrid->SetPoints( aPoints );
      aVertexGrid->GetPointData()->SetScalars( aZValues );
      
      // Update the lookup table range if this bathymetry is out of it
      if ( myLookupTable )
      {
        aZValues->GetRange( myInternalZRange );
        double* aGlobalRange = myLookupTable->GetRange();
        // If the global range is not yet initialized or the current one is out of scope then update the global
        bool anIsUpdated = false;
        if (  ValuesEquals( aGlobalRange[0], anInvalidZ ) || ( aGlobalRange[0] > myInternalZRange[0] ) )
        {
          aGlobalRange[0] = myInternalZRange[0];
          anIsUpdated = true;
        }

        if (  ValuesEquals( aGlobalRange[1], anInvalidZ ) || ( aGlobalRange[1] < myInternalZRange[1] ) )
        {
          aGlobalRange[1] = myInternalZRange[1];
          anIsUpdated = true;
        }

        if ( anIsUpdated )
        {
          myLookupTable->SetRange( aGlobalRange );
          myLookupTable->Build();
        }

        myMapper->SetScalarRange( aGlobalRange );
        myMapper->ScalarVisibilityOn();
        myMapper->SetScalarModeToUsePointData();
        myMapper->SetLookupTable( myLookupTable );
      }

      myMapper->SetInputData( aVertexGrid );
      
      SALOME_Actor* anActor = getActor<SALOME_Actor>(this);
      anActor->SetMapper( myMapper.GetPointer() );
      anActor->setIO( getIO() );
      AddObject( anActor );

      aVertexGrid->Delete();
      aZValues->Delete();
    }
  }
}

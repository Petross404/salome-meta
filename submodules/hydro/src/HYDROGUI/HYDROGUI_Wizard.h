// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_WIZARD_H
#define HYDROGUI_WIZARD_H

#include "HYDROGUI_InputPanel.h"
#include <QWizard>
class QAbstractButton;
class QStackedWidget;
class QWizardPage;

/**\class HYDROGUI_Wizard
 *\brief The base class representing base wizard for HYDROGUI module
 */
class HYDROGUI_Wizard : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_Wizard( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_Wizard();

  QAbstractButton* button( QWizard::WizardButton theBtnId ) const;
  int              addPage( QWizardPage* thePage );
  QStackedWidget*  wizard() const;

  QPushButton*     BackButton();

public slots:
  void             onNext();
  void             onBack();
  
  void             onFirstPage();

signals:
  void             Next( const int );
  void             Back( const int );

protected:

  // Returns the state of current page filing.
  // Reimplement this method in your subclass to verify user input data.
  virtual bool    acceptCurrent() const;

  virtual bool    IsLastPage();

  void            SetButtonsState (bool IsLastPage);

private:
  QStackedWidget* myWizard;
  QPushButton*    myNext;
  QPushButton*    myBack;
  QPushButton*    myFinish;
};

#endif

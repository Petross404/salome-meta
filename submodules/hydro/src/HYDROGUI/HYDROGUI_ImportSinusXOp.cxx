// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportSinusXOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Tool2.h"
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_Iterator.h>

#include <HYDROData_Profile.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>
#include <LightApp_Application.h>

#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

#include <HYDROData_SinusX.h>
#include <HYDROGUI_ImportSinusXDlg.h>


HYDROGUI_ImportSinusXOp::HYDROGUI_ImportSinusXOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "IMPORT_SINUSX" ) );
}

HYDROGUI_ImportSinusXOp::~HYDROGUI_ImportSinusXOp()
{
}

void HYDROGUI_ImportSinusXOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myFileDlg = new SUIT_FileDlg( module()->getApp()->desktop(), true );
  myFileDlg->setWindowTitle( getName() );
  myFileDlg->setFileMode( SUIT_FileDlg::ExistingFiles );
  myFileDlg->setNameFilter( tr("SINUSX_FILTER") );

  connect( myFileDlg, SIGNAL( accepted() ), this, SLOT( onApply() ) );
  connect( myFileDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );

  myFileDlg->exec();
}

void HYDROGUI_ImportSinusXOp::onApply()
{
  if ( !myFileDlg )
  {
    abort();
    return;
  }

  QStringList aFileNames = myFileDlg->selectedFiles();
  bool IsImported = false;

  startDocOperation();

  foreach (QString aFileName, aFileNames) 
  {
    if ( aFileName.isEmpty() )
      continue;

    QString anExt = aFileName.split('.', QString::SkipEmptyParts).back();

    if (anExt == "sx")
    {
      HYDROData_SinusX aSinusXImporter;
      NCollection_Sequence<Handle(HYDROData_Entity)> anEntities;
      QApplication::setOverrideCursor( Qt::WaitCursor );  
      bool ParseStat = aSinusXImporter.OpenAndParse(aFileName);
      QApplication::restoreOverrideCursor();
      if (ParseStat)
      {
        std::vector<HYDROGUI_CurveBlock> aCurveBlocks = aSinusXImporter.GetCurveBlocks();
        QStringList names;
        std::vector<int> types;
        for (int i=0;i<aCurveBlocks.size();i++)
        {
          HYDROGUI_CurveBlock CurveBlock = aCurveBlocks[i];
          types.push_back(CurveBlock.myType);
          names << CurveBlock.myName;
        }
        HYDROGUI_ImportSinusXDlg* aDLG = new HYDROGUI_ImportSinusXDlg( module()->getApp()->desktop(), names, types );
        aDLG->setModal( true );
        aDLG->setWindowTitle(tr("ENTITIES_TO_IMPORT_FROM_SX"));
        //QApplication::restoreOverrideCursor();
        if( aDLG->exec()==QDialog::Accepted ) //??
        {
          QApplication::setOverrideCursor( Qt::WaitCursor );
          std::vector<HYDROData_SinusX::ImportOptions> options = aDLG->GetImportOptions();
          aSinusXImporter.Import(doc(), anEntities, &options);
          UpdateView(anEntities);
          QApplication::restoreOverrideCursor();
          IsImported = true;
        }
      }
    }
  }

  if (IsImported)
  {
    commitDocOperation();
    commit();
  }
  else
    abort();

  module()->update( UF_Model | UF_VTKViewer | UF_OCCViewer );  
  QApplication::restoreOverrideCursor();
}

void HYDROGUI_ImportSinusXOp::UpdateView( NCollection_Sequence<Handle(HYDROData_Entity)>& anEntities)
{
  size_t anActiveViewId = HYDROGUI_Tool::GetActiveGraphicsViewId( module() );
  if ( anActiveViewId == 0 )
    anActiveViewId = HYDROGUI_Tool::GetActiveViewId( module() );

  for (int i = 1; i <= anEntities.Size() ; i++)
  {
    anEntities(i)->Update();
    module()->setObjectVisible( anActiveViewId, anEntities(i), true );
    module()->setIsToUpdate( anEntities(i) );
  }
}
 

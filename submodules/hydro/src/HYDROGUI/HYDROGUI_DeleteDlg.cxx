// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DeleteDlg.h"

#include <SUIT_MessageBox.h>

#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

HYDROGUI_DeleteDlg::HYDROGUI_DeleteDlg( QWidget* theParent )
: QtxDialog( theParent, false, true, QtxDialog::YesNo )
{
  setWindowTitle( tr( "DELETE_OBJECTS" ) );
  setButtonPosition( Left, Yes );
  setButtonPosition( Right, No );

  QLabel* anIconLabelLabel = new QLabel( mainFrame() );
  anIconLabelLabel->setPixmap( SUIT_MessageBox::standardIcon( QMessageBox::Question ) );
  anIconLabelLabel->setScaledContents( false );
  anIconLabelLabel->setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed ) );

  myObjectsLabel = new QLabel( tr( "CONFIRM_DELETION" ), mainFrame() );
  myObjects = new QTextEdit( mainFrame() );
  myObjects->setReadOnly( true );
  myObjects->setFocusPolicy( Qt::NoFocus );

  QGridLayout* aLayout = new QGridLayout( mainFrame() );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );
  aLayout->addWidget( anIconLabelLabel, 0, 0 );
  aLayout->addWidget( myObjectsLabel, 0, 1 );
  aLayout->addWidget( myObjects, 1, 0, 1, 2 );

  if ( QPushButton* aYesBtn = ::qobject_cast<QPushButton*>( button( Yes ) ) )
  {
    setFocusProxy( aYesBtn );
    aYesBtn->setAutoDefault( true );
    aYesBtn->setDefault( true );
  }
  if ( QPushButton* aNoBtn = ::qobject_cast<QPushButton*>( button( No ) ) )
  {
    aNoBtn->setAutoDefault( true );
  }

  setMinimumSize( 275, 250 );
}

HYDROGUI_DeleteDlg::~HYDROGUI_DeleteDlg()
{
}

void HYDROGUI_DeleteDlg::setObjectsToRemove( const QStringList& theObjects )
{
  myObjectsLabel->setText( tr( "CONFIRM_DELETION" ).arg( theObjects.length() ) );

  QStringList anObjects;

  QStringListIterator anIter( theObjects );
  while ( anIter.hasNext() )
  {
    QString anObject = anIter.next();
    anObjects.append( anObject.prepend( "- " ) );
  }

  myObjects->setPlainText( anObjects.join( "\n" ) );
}

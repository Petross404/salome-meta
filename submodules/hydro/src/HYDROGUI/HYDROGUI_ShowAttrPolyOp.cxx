// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ShowAttrPolyOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"

#include <HYDROData_Object.h>
#include <HYDROData_IPolyline.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_PolyAttrDlg.h>
#include <HYDROData_PolylineXY.h>
#include <SUIT_MessageBox.h>

HYDROGUI_ShowAttrPolyOp::HYDROGUI_ShowAttrPolyOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ),
  myPolyAttrDlg( 0 )
{
  setName( tr( "SHOWATTR_POLYLINE" ) );
}

HYDROGUI_ShowAttrPolyOp::~HYDROGUI_ShowAttrPolyOp()
{
}

void HYDROGUI_ShowAttrPolyOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects(module());
  Handle(HYDROData_PolylineXY) aPolyXY = Handle(HYDROData_PolylineXY)::DownCast(aSeq(1));  
  QStringList aDbflist;
  if (aPolyXY->GetDBFInfo(aDbflist))
  {
    myPolyAttrDlg = new HYDROGUI_PolyAttrDlg( module()->getApp()->desktop(), aDbflist );
    myPolyAttrDlg->setModal( true );
    myPolyAttrDlg->setWindowTitle(getName());
    myPolyAttrDlg->exec();
  }
  else
    SUIT_MessageBox::information( module()->getApp()->desktop(), getName(), tr("SHOWATTR_POLY_NO_ATTR")); 

  HYDROGUI_Operation::abort();
}


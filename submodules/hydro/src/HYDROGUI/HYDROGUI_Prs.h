// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRS_H
#define HYDROGUI_PRS_H

#include <HYDROData_Entity.h>

#include <GraphicsView_Object.h>

/*
  Class       : HYDROGUI_Prs
  Description : Base class for all HYDRO presentation
*/
class HYDROGUI_Prs : public GraphicsView_Object
{
public:
  HYDROGUI_Prs( const Handle(HYDROData_Entity)& theObject );
  virtual ~HYDROGUI_Prs();

public:
  Handle(HYDROData_Entity)        getObject() const { return myObject; }

  bool                            getIsToUpdate() const { return myIsToUpdate; }
  void                            setIsToUpdate( bool theState ) { myIsToUpdate = theState; }

public:
  // from GraphicsView_Object
  virtual void                    addTo( GraphicsView_ViewPort* theViewPort );

protected:
  QCursor*                        getHighlightCursor() const { return myHighlightCursor; }

private:
  Handle(HYDROData_Entity)        myObject;
  bool                            myIsToUpdate;

private:
  QCursor*                        myHighlightCursor;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_VTKPRSDISPLAYER_H
#define HYDROGUI_VTKPRSDISPLAYER_H

#include "HYDROGUI_AbstractDisplayer.h"
#include <vtkSmartPointer.h>
#include <vtkScalarBarActor.h>
#include <QMap>

class HYDROGUI_VTKPrsDriver;

/**
 * \class HYDROGUI_VTKPrsDisplayer
 * \brief Class intended to create, display and update the presentations in VTK viewer.
 */
class HYDROGUI_VTKPrsDisplayer : public HYDROGUI_AbstractDisplayer
{
public:
  /**
   * \brief Constructor.
   * \param theModule module object
   */
  HYDROGUI_VTKPrsDisplayer( HYDROGUI_Module* theModule );

  /**
   * \brief Destructor.
   */
  virtual ~HYDROGUI_VTKPrsDisplayer();

  /**
   * \brief Force the specified objects to be updated.
   * \param theObjs sequence of objects to update
   * \param theViewerId viewer identifier
   */
  void             SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                const size_t theViewerId );

  /**
   * \brief Get the applicable viewer type.
   */
  virtual QString  GetType() const;

  /**
   * \brief Set the range of Z values for the color legend bar.
   */
  void             SetZRange( const size_t theViewerId, double theRange[] );

  /**
   * \brief Get the range of Z values for the color legend bar.
   */
  double*          GetZRange( const size_t theViewerId ) const;

  /**
   * \brief Delete scalar bar for the given viewer id.
   * \param theViewerId viewer identifier
   */
  void             EraseScalarBar( const size_t theViewerId, const bool theIsDelete = false );

protected:

  virtual void     DisplayAll( const size_t theViewerId,
                               const bool theIsForced,
                               const bool theDoFitAll );

  /**
   * \brief Erase all viewer objects.
   * \param theViewerId viewer identifier
   */
  void             EraseAll( const size_t theViewerId );

  /**
   * \brief Erase the specified viewer objects.
   * \param theObjs sequence of objects to erase
   * \param theViewerId viewer identifier
   */
  void             Erase( const HYDROData_SequenceOfObjects& theObjs,
                          const size_t theViewerId );

  /**
   * \brief Display the specified viewer objects.
   * \param theObjs sequence of objects to display
   * \param theViewerId viewer identifier
   * \param theIsForced flag used to update all objects, including the unchanged ones
   * \param theDoFitAll flag used to fit the view to all visible objects; do not fit by default
   */
  void             Display( const HYDROData_SequenceOfObjects& theObjs,
                            const size_t theViewerId,
                            const bool theIsForced,
                            const bool theDoFitAll );

protected:
  /**
   * \brief Purge all invalid objects in the viewer.
   * \param theViewerId viewer identifier
   */
  void             purgeObjects( const size_t theViewerId );

private:

  /**
   * \brief Create a new scalar bar for the given view id.
   * \param theViewId view identifier
   */
  void             createScalarBar( const size_t theViewId );

  /**
   * \brief Get the presentation driver for the specified data object.
   * \param theObj data object
   */
  HYDROGUI_VTKPrsDriver* getDriver( const size_t theViewId, const Handle(HYDROData_Entity)& theObj );

  HYDROGUI_VTKPrsDriver*       myDriver;
  HYDROGUI_VTKPrsDriver*       myShapeDriver;

  QMap<size_t, vtkSmartPointer<vtkScalarBarActor> > myScalarBars; //!< Colors legend presentations
};

#endif

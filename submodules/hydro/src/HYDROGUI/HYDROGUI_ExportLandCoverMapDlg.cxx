// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ExportLandCoverMapDlg.h"

#include <SUIT_MessageBox.h>

#include <QCheckBox>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QVBoxLayout>

HYDROGUI_ExportLandCoverMapDlg::HYDROGUI_ExportLandCoverMapDlg( QWidget* theParent, bool IsLinear, const QStringList& theAttrItems )
  : QtxDialog( theParent, false, true, QtxDialog::OKCancel )
{
  setWindowTitle( tr( "EXPORT_LANDCOVERMAP" ) );
  setButtonPosition( Left, OK );
  setButtonPosition( Right, Cancel );

  myAttrCheckBox = new QCheckBox( tr( "WRITE_ST_AS_ATTRS_TO_DBF" ), mainFrame() );
  myAttrCheckBox->setChecked(false);
  myAvFields = new QComboBox( mainFrame() );
  myAvFields->setEnabled(false);
  myAvFields->addItems( theAttrItems );

  QVBoxLayout* aSketchLayout = new QVBoxLayout( mainFrame() );
  aSketchLayout->setMargin( 0 );
  aSketchLayout->setSpacing( 5 );

  QGridLayout* aLayout = new QGridLayout();
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );
  aLayout->addWidget( myAttrCheckBox, 0, 0 );
  aLayout->addWidget( myAvFields, 1, 0, 1, 2 );

  if(!IsLinear)
  {
    myDiscrLabel = new QLabel( tr( "LCM_DISCR_LABEL" ), mainFrame() );
    myDeflSpinBox = new QDoubleSpinBox( mainFrame() );
    myDeflSpinBox->setRange(0.001, 2); 
    myDeflSpinBox->setDecimals(3);
    myDeflSpinBox->setSingleStep(0.001);
    myDeflLabel = new QLabel ( tr( "LCM_DEFL_LABEL" ), mainFrame() );
    aLayout->addWidget( myDiscrLabel, 2, 0 );
    aLayout->addWidget( myDeflLabel, 3, 0, 1, 1 );
    aLayout->addWidget( myDeflSpinBox, 3, 1, 2, 2 );
  }
  else
  {
    myDeflSpinBox = NULL;
    myDeflLabel = NULL;
    myDiscrLabel = NULL;
  }

  myWarningLabel = new QLabel( tr( "ATTRS_NOT_WRITTEN_TO_DBF" ), mainFrame() );
  myWarningLabel->setStyleSheet("QLabel { font: italic; color : blue; }");
  
  aSketchLayout->addLayout( aLayout );
  aSketchLayout->addWidget( myWarningLabel, 1, Qt::AlignHCenter );
  aSketchLayout->addStretch(1);

  setMinimumSize( 300, 100 );

  connect( myAttrCheckBox, SIGNAL(clicked(bool)), this, SLOT(onAttrCBChecked(bool)));
  connect( myAvFields, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onAvFieldsChanged(const QString&)));

}

HYDROGUI_ExportLandCoverMapDlg::~HYDROGUI_ExportLandCoverMapDlg()
{
}

void HYDROGUI_ExportLandCoverMapDlg::onAttrCBChecked( bool theState )
{
  if (theState)
    myAvFields->setEnabled(true);
  else
    myAvFields->setEnabled(false);

  myWarningLabel->setVisible( !theState || getCurrentItem().isEmpty() );
}

void HYDROGUI_ExportLandCoverMapDlg::onAvFieldsChanged(const QString& theText)
{
  myWarningLabel->setVisible( theText.isEmpty() );
}

QString HYDROGUI_ExportLandCoverMapDlg::getCurrentItem() const
{
  return myAvFields->currentText();
}

bool HYDROGUI_ExportLandCoverMapDlg::getAttrCheckBoxState()
{
  return myAttrCheckBox->isChecked();
}

double HYDROGUI_ExportLandCoverMapDlg::getDeflValue() const
{
  if (myDeflSpinBox)
    return myDeflSpinBox->value();
  else 
    return -1; //value is not needed
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_SubmersibleOp.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_Module.h>

HYDROGUI_SubmersibleOp::HYDROGUI_SubmersibleOp( HYDROGUI_Module* theModule, bool isSubmersible )
  : HYDROGUI_Operation( theModule ), myIsSubmersible( isSubmersible )
{
}

HYDROGUI_SubmersibleOp::~HYDROGUI_SubmersibleOp()
{
}

void HYDROGUI_SubmersibleOp::startOperation()
{
  HYDROGUI_Operation::startOperation();
  myObject = Handle(HYDROData_Object)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( myObject.IsNull() )
    onCancel();
  else
    onApply();
}

bool HYDROGUI_SubmersibleOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries )
{
  bool isOK = !myObject.IsNull();
  theUpdateFlags = 0;
  if( isOK )
  {
    myObject->SetIsSubmersible( myIsSubmersible );
    theUpdateFlags = 0;
  }
  return isOK;
}


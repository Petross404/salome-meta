// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VTKPrs.h"

#include "HYDROGUI_DataObject.h"
#include <HYDROData_IAltitudeObject.h>
#include <vtkMapper.h>

//=======================================================================
// name    : HYDROGUI_VTKPrs
// Purpose : Constructor
//=======================================================================
HYDROGUI_VTKPrs::HYDROGUI_VTKPrs( const Handle(HYDROData_Entity)& theObject ) 
: SVTK_Prs( "" ),
  myObject( theObject ), 
  myIsToUpdate( false )
{
  // Define IO for actors to be added:
  QString anEntry = HYDROGUI_DataObject::dataObjectEntry( theObject );
  myIO = new SALOME_InteractiveObject(
    anEntry.toLatin1(), QString::number( theObject->GetKind() ).toLatin1(), theObject->GetName().toLatin1() );
  myZRange[0] = HYDROData_IAltitudeObject::GetInvalidAltitude();
  myZRange[1] = HYDROData_IAltitudeObject::GetInvalidAltitude();
}

//=======================================================================
// name    : HYDROGUI_VTKPrs
// Purpose : Destructor
//=======================================================================
HYDROGUI_VTKPrs::~HYDROGUI_VTKPrs()
{
}

//=======================================================================
// name    : compute
// Purpose : Compute the presentation
//=======================================================================
void HYDROGUI_VTKPrs::compute()
{
  setIsToUpdate( false );
}

//=======================================================================
// name    : setZRange
// Purpose : Compute the presentation
//=======================================================================
void HYDROGUI_VTKPrs::setZRange( double theRange[] )
{
  myZRange[0] = theRange[0];
  myZRange[1] = theRange[1];
  vtkMapper* aMapper = mapper();
  if ( aMapper )
  {
    mapper()->SetScalarRange( myZRange );
  }
}

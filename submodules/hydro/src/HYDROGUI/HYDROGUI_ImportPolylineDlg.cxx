// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportPolylineDlg.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>

HYDROGUI_ImportPolylineDlg::HYDROGUI_ImportPolylineDlg( QWidget* theParent, 
  const NCollection_Sequence<Handle(HYDROData_Entity)>& PolyEnts )
  : QDialog( theParent )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin(5);
  aMainLayout->setSpacing(5);

  QLabel* gLabel = new QLabel();
  gLabel->setText(tr("POLYLINES_3D_IN_FILES"));

  myPolylineTable = new QTableWidget(PolyEnts.Size(), 2);

  myPolyEnts = PolyEnts;

  QStringList header;
  header << tr("POLYLINE_NAME") << tr("FULL_IMPORT");
  myPolylineTable->setHorizontalHeaderLabels(header);
  myPolylineTable->setShowGrid(true);

  for (int i = 1; i <= myPolyEnts.Size(); i++)
  {
    QTableWidgetItem* item = new QTableWidgetItem(myPolyEnts(i)->GetName());
    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
    myPolylineTable->setItem(i-1, 0, item);

    QTableWidgetItem* itemCheckbox = new QTableWidgetItem("");
    itemCheckbox->setFlags(itemCheckbox->flags() ^ Qt::ItemIsEditable);
    itemCheckbox->setCheckState(Qt::Unchecked);
    myPolylineTable->setItem(i-1, 1, itemCheckbox);
  }

  aMainLayout->addWidget(gLabel);
  aMainLayout->addWidget(myPolylineTable);

  QPushButton* CheckAllButton = new QPushButton( tr("CHECK_ALL"), this );
  QPushButton* UncheckAllButton = new QPushButton( tr("UNCHECK_ALL"), this );

  QPushButton* anOkButton = new QPushButton( tr("OK"), this );
  anOkButton->setDefault( true ); 

  aMainLayout->addWidget( CheckAllButton );
  aMainLayout->addWidget( UncheckAllButton );
  aMainLayout->addWidget( anOkButton );

  connect( anOkButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( CheckAllButton, SIGNAL( clicked() ), this, SLOT( onCheckAll() ) );
  connect( UncheckAllButton, SIGNAL( clicked() ), this, SLOT( onUncheckAll() ) );

  setLayout(aMainLayout);
}

HYDROGUI_ImportPolylineDlg::~HYDROGUI_ImportPolylineDlg()
{
}

void HYDROGUI_ImportPolylineDlg::onCheckAll()
{
  for (int i = 1; i <= myPolyEnts.Size(); i++)
  {
    QTableWidgetItem* itemCheckbox = myPolylineTable->item(i-1, 1);
    itemCheckbox->setCheckState(Qt::Checked);
  }
}

void HYDROGUI_ImportPolylineDlg::onUncheckAll()
{
  for (int i = 1; i <= myPolyEnts.Size(); i++)
  {
    QTableWidgetItem* itemCheckbox = myPolylineTable->item(i-1, 1);
    itemCheckbox->setCheckState(Qt::Unchecked);
  }
}

NCollection_Sequence<Handle(HYDROData_Entity)> HYDROGUI_ImportPolylineDlg::GetCheckedPolylines() const
{
  NCollection_Sequence<Handle(HYDROData_Entity)> checkedPolys;
  for (int i = 1; i <= myPolyEnts.Size(); i++)
  {
    QTableWidgetItem* itemCheckbox = myPolylineTable->item(i-1, 1);
    if (itemCheckbox->checkState() == Qt::CheckState::Checked)
      checkedPolys.Append(myPolyEnts(i));
  }
  return checkedPolys;
}





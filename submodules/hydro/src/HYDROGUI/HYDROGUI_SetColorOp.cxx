// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_SetColorOp.h"

#include "HYDROGUI_ColorDlg.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Object.h>
#include <HYDROData_IPolyline.h>
#include <HYDROData_LandCoverMap.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>

HYDROGUI_SetColorOp::HYDROGUI_SetColorOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ),
  myColorDlg( 0 )
{
  setName( tr( "SET_COLOR" ) );
}

HYDROGUI_SetColorOp::~HYDROGUI_SetColorOp()
{
}

bool HYDROGUI_SetColorOp::CanObjectBeColored( const Handle(HYDROData_Entity)& theObject,
                                              HYDROGUI_Module* theModule )
{
  if ( theObject.IsNull() )
    return false;

  bool isCanBeColored = 
    theObject->IsKind( STANDARD_TYPE(HYDROData_Object) ) ||
    theObject->IsKind( STANDARD_TYPE(HYDROData_IPolyline) );

  return isCanBeColored;
}

void HYDROGUI_SetColorOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  // Get the selected object
  //myEditedObject = Handle(HYDROData_Object)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  myEditedObject = HYDROGUI_Tool::GetSelectedObject( module() );
  if ( !CanObjectBeColored( myEditedObject, module() ) )
  {
    abort();
    return;
  }

  bool anIsOneColor = false;

  // Get colors from the object
  QColor aFirstColor, aSecondColor;
  if ( myEditedObject->IsKind( STANDARD_TYPE(HYDROData_Object) ) )
  {
    Handle(HYDROData_Object) aGeomObject =
      Handle(HYDROData_Object)::DownCast( myEditedObject );

    if ( myEditedObject->GetKind() == KIND_POLYLINE ||
         myEditedObject->GetKind() == KIND_PROFILE )
    {
      aFirstColor = aGeomObject->GetBorderColor();
      anIsOneColor = true;
    }
    else
    {
      aFirstColor = aGeomObject->GetFillingColor();
      aSecondColor = aGeomObject->GetBorderColor();
    }
  }
  else if ( myEditedObject->IsKind( STANDARD_TYPE(HYDROData_IPolyline) ) )
  {
    Handle(HYDROData_IPolyline) aPolyObject =
      Handle(HYDROData_IPolyline)::DownCast( myEditedObject );

    //aFirstColor = aPolyObject->GetWireColor();

    if (aPolyObject->NbSections() > 0)
      aPolyObject->GetSectionColor(0, aFirstColor);
    else
      aFirstColor = aPolyObject->GetWireColor();

    anIsOneColor = true;
  }
  else if ( myEditedObject->IsKind( STANDARD_TYPE(HYDROData_LandCoverMap) ) )
  {
    Handle(HYDROData_LandCoverMap) aLandCoverMap =
      Handle(HYDROData_LandCoverMap)::DownCast( myEditedObject );

    /* TODO
    aFirstColor = aLandCoverMap->GetFillingColor();
    aSecondColor = aLandCoverMap->GetBorderColor();
    */
  }

  // Create color dialog
  myColorDlg = new HYDROGUI_ColorDlg( module()->getApp()->desktop(), anIsOneColor );
  myColorDlg->setModal( true );
  myColorDlg->setWindowTitle( getName() );

  // Set colors from the object
  myColorDlg->setFirstColor( aFirstColor );
  myColorDlg->setSecondColor( aSecondColor );

  // Connect the dialog to operation slots
  connect( myColorDlg, SIGNAL( accepted() ), this, SLOT( onApply()  ) );
  connect( myColorDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );

  // Show the dialog
  myColorDlg->exec();
}

bool HYDROGUI_SetColorOp::processApply( int& theUpdateFlags,
                                        QString& theErrorMsg,
                                        QStringList& theBrowseObjectsEntries )
{
  if ( !myColorDlg || myEditedObject.IsNull() )
    return false;

  QColor aFirstColor = myColorDlg->getFirstColor();
  QColor aSecondColor = myColorDlg->getSecondColor();

  if ( myEditedObject->IsKind( STANDARD_TYPE(HYDROData_Object) ) )
  {
    Handle(HYDROData_Object) aGeomObject =
      Handle(HYDROData_Object)::DownCast( myEditedObject );

    if ( myEditedObject->GetKind() == KIND_POLYLINE ||
         myEditedObject->GetKind() == KIND_PROFILE  )
    {
      aGeomObject->SetBorderColor( aFirstColor );
    }
    else
    {
      aGeomObject->SetFillingColor( aFirstColor );
      aGeomObject->SetBorderColor( aSecondColor );
    }
  }
  else if ( myEditedObject->IsKind( STANDARD_TYPE(HYDROData_IPolyline) ) )
  {
    Handle(HYDROData_IPolyline) aPolyObject =
      Handle(HYDROData_IPolyline)::DownCast( myEditedObject );

    //aPolyObject->SetWireColor( aFirstColor );
    int nbSec = aPolyObject->NbSections();
    if (nbSec > 0)
    {
      for (int i = 0; i < nbSec; i++)
        aPolyObject->SetSectionColor(i, aFirstColor);
    }
    else
      aPolyObject->SetWireColor( aFirstColor );
  }
  else if ( myEditedObject->IsKind( STANDARD_TYPE(HYDROData_LandCoverMap) ) )
  {
    Handle(HYDROData_LandCoverMap) aLandCoverMap =
      Handle(HYDROData_LandCoverMap)::DownCast( myEditedObject );

    /* TODO
    aLandCoverMap->SetFillingColor( aFirstColor );
    aLandCoverMap->SetBorderColor( aSecondColor );
    */
  }

  module()->setIsToUpdate( myEditedObject );

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
  
  return true;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <Python.h>

#include "HYDROGUI_Module.h"

#include "HYDROGUI.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_Displayer.h"
#include "HYDROGUI_GVSelector.h"
#include "HYDROGUI_InputPanel.h"
#include "HYDROGUI_ObjSelector.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_OCCSelector.h"
#include "HYDROGUI_Operations.h"
#include "HYDROGUI_Operation.h"
#include "HYDROGUI_PrsImage.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_VTKPrs.h"
#include "HYDROGUI_VTKPrsDisplayer.h"
#include "HYDROGUI_AbstractDisplayer.h"
#include "HYDROGUI_PolylineOp.h"
#include "HYDROGUI_SetColorOp.h"
#include "HYDROGUI_ImportGeomObjectOp.h"
#include "HYDROGUI_ShowHideOp.h"
#include "HYDROGUI_Overview.h"
#include <HYDROGUI_ProfileDlg.h>
#include <HYDROGUI_SIProgressIndicator.h>
#include <HYDROGUI_ZIProgressIndicator.h>
#include <HYDROData_Tool.h>
#include <HYDROData_Image.h>
#include <HYDROData_Stream.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Lambert93.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_ArtificialObject.h>
#include <HYDROData_NaturalObject.h>

#include <HYDROData_OperationsFactory.h>

#include <CurveCreator_Utils.hxx>

#include <GraphicsView_ViewFrame.h>
#include <GraphicsView_ViewManager.h>
#include <GraphicsView_ViewPort.h>
#include <GraphicsView_Viewer.h>

#include <ImageComposer_CutOperator.h>
#include <ImageComposer_CropOperator.h>
#include <ImageComposer_FuseOperator.h>

#include <LightApp_Application.h>
#include <LightApp_DataOwner.h>
#include <LightApp_GVSelector.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_UpdateFlags.h>

#include <SalomeApp_Study.h>

#include <OCCViewer_ViewFrame.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <SALOME_Event.h>

#include <SUIT_DataBrowser.h>
#include <SUIT_DataObject.h>
#include <SUIT_ViewManager.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Desktop.h>
#include <SUIT_Study.h>
#include <SUIT_Session.h>

#include <SVTK_ViewManager.h>
#include <SVTK_ViewModel.h>
#include <SVTK_ViewWindow.h>
#include <SVTK_Selector.h>

#include <OCCViewer_ViewPort3d.h>

#include <GEOMUtils.hxx>
#include <GeometryGUI.h>

#include <SALOMEDS_wrap.hxx>

#include <QAction>
#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QMouseEvent>
#include <QStatusBar>
#include <QCursor>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

static size_t ViewManagerId = 1;

extern "C" HYDRO_EXPORT CAM_Module* createModule()
{
  return new HYDROGUI_Module();
}

extern "C" HYDRO_EXPORT char* getModuleVersion()
{
  return (char*)HYDRO_VERSION;
}

HYDROGUI_Module::HYDROGUI_Module()
: LightApp_Module( "HYDRO" ),
  myDisplayer( 0 ),
  myOCCDisplayer( 0 ),
  myIsUpdateEnabled( true ),
  myOverview( 0 )
{
  DEBTRACE("HYDROGUI_Module::HYDROGUI_Module()");
}

HYDROGUI_Module::~HYDROGUI_Module()
{
  DEBTRACE("HYDROGUI_Module::~HYDROGUI_Module()");
}

int HYDROGUI_Module::getStudyId() const
{
  DEBTRACE("HYDROGUI_Module::getStudyId()");
  LightApp_Application* anApp = getApp();
  return anApp ? anApp->activeStudy()->id() : 0;
}

void HYDROGUI_Module::initialize( CAM_Application* theApp )
{
  DEBTRACE("HYDROGUI_Module::initialize");
  LightApp_Module::initialize( theApp );

  createActions();
  createUndoRedoActions();
  createMenus();
  createPopups();
  createToolbars();

  setMenuShown( false );
  setToolShown( false );

  myDisplayer = new HYDROGUI_Displayer( this );
  myOCCDisplayer = new HYDROGUI_OCCDisplayer( this );
  myVTKDisplayer = new HYDROGUI_VTKPrsDisplayer( this );

  //HYDROData_Tool::SetSIProgress( new HYDROGUI_SIProgressIndicator( theApp->desktop() ) );
  //HYDROData_Tool::SetZIProgress( new HYDROGUI_ZIProgressIndicator( theApp->desktop() ) );
}

bool HYDROGUI_Module::activateModule( SUIT_Study* theStudy )
{
  DEBTRACE("HYDROGUI_Module::activateModule");
  bool aRes = LightApp_Module::activateModule( theStudy );

  LightApp_Application* anApp = getApp();
  SUIT_Desktop* aDesktop = anApp->desktop();

  getApp()->setEditEnabled( false ); // hide SalomeApp copy/paste actions

  setMenuShown( true );
  setToolShown( true );

  // import Python module that manages HYDRO plugins (need to be here because SalomePyQt API uses active module)
  PyGILState_STATE gstate = PyGILState_Ensure();
  PyObject* pluginsmanager = PyImport_ImportModuleNoBlock((char*)"salome_pluginsmanager");
  if ( !pluginsmanager ) {
    PyErr_Print();
  }
  else {
    PyObject* result =
      PyObject_CallMethod(pluginsmanager, (char*)"initialize", (char*)"isss", 1, "hydro",
                          tr("MEN_DESK_HYDRO").toUtf8().data(),
                          tr("Python plugins").toUtf8().data());
    if ( !result )
      PyErr_Print();
    Py_XDECREF(result);
  }
  PyGILState_Release(gstate);
  // end of GEOM plugins loading

#ifndef DISABLE_PYCONSOLE
  aDesktop->tabifyDockWidget( HYDROGUI_Tool::WindowDock( anApp->getWindow( LightApp_Application::WT_PyConsole ) ),
                              HYDROGUI_Tool::WindowDock( anApp->getWindow( LightApp_Application::WT_LogWindow ) ) );
#endif

  // Remove defunct view managers from the map.
  // It's essential to do this before "update( UF_All )" call!
  QList<size_t> anObsoleteIds;
  ViewManagerList anAllViewManagers = anApp->viewManagers();
  ViewManagerList aHydroViewManagers; // view managers created inside the HYDRO module
  ViewManagerMapIterator anIter( myViewManagerMap );
  while( anIter.hasNext() ) {
    size_t anId = anIter.next().key();
    const ViewManagerInfo& anInfo = anIter.value();

    aHydroViewManagers << anInfo.first;

    if ( !anAllViewManagers.contains( anInfo.first ) ) {
      anObsoleteIds << anId;
    }
  }
  foreach ( const size_t anId, anObsoleteIds ) {
    myViewManagerMap.remove( anId );
    myObjectStateMap.remove( anId );
    myShapesMap.remove( anId );
    myVTKPrsMap.remove( anId );
  }
  // Replace the default selector for all view managers.
  // Add view managers created outside of HYDRO module to the map.
  foreach ( SUIT_ViewManager* aViewManager, anAllViewManagers ) {
    createSelector( aViewManager ); // replace the default selector
    if ( !aHydroViewManagers.contains( aViewManager ) ) {
      ViewManagerInfo anInfo( aViewManager, VMR_General );
      myViewManagerMap.insert( ViewManagerId++, anInfo );
    }
  }

  update( UF_All );

  updateCommandsStatus();

  HYDROGUI_Tool::setOCCActionShown( this, OCCViewer_ViewWindow::MaximizedId, false );

  ViewManagerList anOCCViewManagers;
  anApp->viewManagers( OCCViewer_Viewer::Type(), anOCCViewManagers );

  foreach ( SUIT_ViewManager* aViewManager, anOCCViewManagers )
  {
    connect( aViewManager, SIGNAL( mouseMove( SUIT_ViewWindow*, QMouseEvent* ) ),
             this, SLOT( onMouseMove( SUIT_ViewWindow*, QMouseEvent* ) ) );
    connect( aViewManager, SIGNAL( activated( SUIT_ViewManager* ) ),
             this, SLOT( onViewActivated( SUIT_ViewManager* ) ) );

    OCCViewer_ViewManager* occ_mgr = dynamic_cast<OCCViewer_ViewManager*>( aViewManager );
    if( occ_mgr )
    {
      occ_mgr->setChainedOperations( true );//TODO: via preferences
    }

    foreach( SUIT_ViewWindow* aViewWindow, aViewManager->getViews() )
    {
      OCCViewer_ViewFrame* aViewFrame = dynamic_cast<OCCViewer_ViewFrame*>( aViewWindow );
      if ( aViewFrame && aViewFrame->getViewPort() )
      {
        aViewFrame->getViewPort()->installEventFilter( this );
      }
    }
  }

  preferencesChanged( "HYDRO", "zoom_shutoff" );

  preferencesChanged( "HYDRO", "chained_panning" );

  // Load GEOM data
  SalomeApp_Study* aStudy =
    dynamic_cast<SalomeApp_Study*>( getApp()->activeStudy() );
  if ( aStudy ) {
    SALOMEDS::Study_var aDSStudy = GeometryGUI::getStudyServant();
    GEOM::GEOM_Gen_var aGeomEngine = GeometryGUI::GetGeomGen();
    if ( !aGeomEngine->_is_nil() && !aGeomEngine->_is_nil() ) {
      SALOMEDS::StudyBuilder_var aStudyBuilder = aDSStudy->NewBuilder();
      SALOMEDS::SComponent_wrap GEOM_var = aDSStudy->FindComponent( "GEOM" );
      if( !GEOM_var->_is_nil() ) {
        aStudyBuilder->LoadWith( GEOM_var, aGeomEngine );
      }
    }
  }


//  SUIT_DataBrowser* ob = getApp()->objectBrowser();
//  SUIT_AbstractModel* treeModel = dynamic_cast<SUIT_AbstractModel*>( ob->model() );
//  treeModel->setAppropriate( SUIT_DataObject::VisibilityId, Qtx::Toggled );

  return aRes;
}

bool HYDROGUI_Module::deactivateModule( SUIT_Study* theStudy )
{
  DEBTRACE("HYDROGUI_Module::deactivateModule");
  /* Issues ## 68, 88.
  ViewManagerMapIterator anIter( myViewManagerMap );
  while( anIter.hasNext() )
    if( SUIT_ViewManager* aViewManager = anIter.next().value().first )
      getApp()->removeViewManager( aViewManager );
  myViewManagerMap.clear();
  */

  ViewManagerList anOCCViewManagers;
  getApp()->viewManagers( OCCViewer_Viewer::Type(), anOCCViewManagers );
  foreach ( SUIT_ViewManager* aViewManager, anOCCViewManagers )
  {
    disconnect( aViewManager, SIGNAL( mouseMove( SUIT_ViewWindow*, QMouseEvent* ) ),
                this, SLOT( onMouseMove( SUIT_ViewWindow*, QMouseEvent* ) ) );
    disconnect( aViewManager, SIGNAL( activated( SUIT_ViewManager* ) ),
                this, SLOT( onViewActivated( SUIT_ViewManager* ) ) );

    OCCViewer_ViewManager* occ_mgr = dynamic_cast<OCCViewer_ViewManager*>( aViewManager );
    if( occ_mgr )
    {
      occ_mgr->setChainedOperations( false );
      setAutoZoom( occ_mgr, true );
    }
  }

  /* Issues ## 68, 88.
  myObjectStateMap.clear();
  myShapesMap.clear();
  myVTKPrsMap.clear();
  */

  // clear the status bar
  SUIT_Desktop* aDesktop = getApp()->desktop();
  if ( aDesktop && aDesktop->statusBar() ) {
    aDesktop->statusBar()->clearMessage();
  }

  // clear the data model's list of copying objects
  HYDROGUI_DataModel::changeCopyingObjects( HYDROData_SequenceOfObjects() );

  setMenuShown( false );
  setToolShown( false );

  getApp()->setEditEnabled( true ); // show SalomeApp copy/paste actions

  HYDROGUI_Tool::setOCCActionShown( this, OCCViewer_ViewWindow::MaximizedId, true );

  myActiveOperationMap.clear();

  myViewManagerMap.clear();

  bool ret = LightApp_Module::deactivateModule( theStudy );
  return ret;
}

void HYDROGUI_Module::studyClosed(SUIT_Study* theStudy)
{
    DEBTRACE("HYDROGUI_Module::studyClosed");
    HYDROGUI_Operation::myIsClear = false; // force clear of Zlayers on next study
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    if (!aDoc.IsNull())
      aDoc->Close();
}

void HYDROGUI_Module::windows( QMap<int, int>& theMap ) const
{
  DEBTRACE("HYDROGUI_Module::windows");
  static bool inWindows = false;
  if( inWindows )
    return;

  theMap.clear();
  theMap.insert( LightApp_Application::WT_LogWindow,     Qt::BottomDockWidgetArea );
#ifndef DISABLE_PYCONSOLE
  theMap.insert( LightApp_Application::WT_PyConsole,     Qt::BottomDockWidgetArea );
#endif
  theMap.insert( LightApp_Application::WT_ObjectBrowser, Qt::LeftDockWidgetArea   );
  theMap.insert( OverviewWindow, Qt::LeftDockWidgetArea );

  inWindows = true;
  LightApp_Application* app = getApp();
  if( app && app->getWindow( OverviewWindow )==0 )
  {
    const_cast<HYDROGUI_Module*>( this )->myOverview =
      new HYDROGUI_Overview( tr( "OVERVIEW" ), 0, app->desktop() );
    app->insertDockWindow( OverviewWindow, myOverview );
    app->placeDockWindow( OverviewWindow, Qt::LeftDockWidgetArea );
  }
  inWindows = false;
}

void HYDROGUI_Module::viewManagers( QStringList& theTypesList ) const
{
  DEBTRACE("HYDROGUI_Module::viewManagers");
  theTypesList << GraphicsView_Viewer::Type() << OCCViewer_Viewer::Type();
}

void HYDROGUI_Module::contextMenuPopup( const QString& theClient,
                                        QMenu* theMenu,
                                        QString& theTitle )
{
  HYDROGUI_DataModel* aModel = getDataModel();

  bool anIsObjectBrowser = theClient == getApp()->objectBrowser()->popupClientType();
  bool anIsGraphicsView = theClient == GraphicsView_Viewer::Type();
  bool anIsOCCView = theClient == OCCViewer_Viewer::Type();
  bool anIsVTKView = theClient == SVTK_Viewer::Type();
  if( !anIsObjectBrowser && !anIsGraphicsView && !anIsOCCView && !anIsVTKView )
    return;

  size_t anActiveViewId = HYDROGUI_Tool::GetActiveViewId( this );

  bool anIsSelectedDataObjects = false;
  bool anIsVisibleInSelection = false;
  bool anIsHiddenInSelection = false;

  bool anIsImage = false;
  bool anIsImportedImage = false;
  bool anIsImageHasRefs = false;
  bool anIsFusedImage = false;
  bool anIsCutImage = false;
  bool anIsSplitImage = false;
  bool anIsMustObjectBeUpdated = false;
  bool anIsPolyline = false;
  bool anIsPolyline3D = false;
  bool anIsProfile = false;
  bool anIsValidProfile = false;
  bool anAllAreProfiles = false;
  bool anIsBathymetry = false;
  bool anIsCalculation = false;
  bool anIsImmersibleZone = false;
  bool anIsVisualState = false;
  bool anIsRegion = false;
  bool anIsZone = false;
  bool anIsObstacle = false;
  bool anIsStricklerTable = false;
  bool anIsLandCoverMap = false;
  bool anIsStream = false;
  bool anIsChannel = false;
  bool anIsDigue = false;
  bool anIsDummyObject3D = false;
  bool anIsGroup = false;
  bool anIsObjectCanBeColored = false;
  bool isRoot = false;
  bool isStreamHasBottom = false;
  bool anIsBCPolygon = false;

  SUIT_SelectionMgr* aSelectionMgr = getApp()->selectionMgr();
  SUIT_DataOwnerPtrList anOwners;
  aSelectionMgr->selected( anOwners );
  if( anIsObjectBrowser && anOwners.size()==1 )
  {
    QString anEntry = anOwners[0]->keyString();
    LightApp_Study* aStudy = dynamic_cast<LightApp_Study*>( getApp()->activeStudy() );
    if( aStudy )
      isRoot = aStudy->isComponent( anEntry );
  }

  // Check the selected GEOM objects (take into account the Object Browser only)
  if ( anIsObjectBrowser ) {
    QList<GEOM::shape_type> anObstacleTypes =
      HYDROGUI_ImportGeomObjectOp::getObstacleTypes();
    QList<GEOM::shape_type> aPolylineTypes =
      HYDROGUI_ImportGeomObjectOp::getPolylineTypes();

    bool isCanBeImportedAsObstacle =
      !HYDROGUI_Tool::GetSelectedGeomObjects( this, anObstacleTypes ).isEmpty();
    bool isCanBeImportedAsPolyline =
      !HYDROGUI_Tool::GetSelectedGeomObjects( this, aPolylineTypes ).isEmpty();

    // Add import as obstacle action
    if ( isCanBeImportedAsObstacle ) {
      theMenu->addAction( action( ImportGeomObjectAsObstacleId ) );
    }
    // Add import as polyline action
    if ( isCanBeImportedAsPolyline ) {
      theMenu->addAction( action( ImportGeomObjectAsPolylineId ) );
    }
    // Add separator
    if ( isCanBeImportedAsObstacle || isCanBeImportedAsPolyline ) {
      theMenu->addSeparator();
    }
  }

  // check the selected data model objects
  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( this );
  int aNbOfSelectedProfiles = 0;
  for( Standard_Integer anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObject = aSeq.Value( anIndex );
    if( !anObject.IsNull() )
    {
      anIsSelectedDataObjects = true;

      bool aVisibility = isObjectVisible( anActiveViewId, anObject );
      anIsVisibleInSelection |= aVisibility;
      anIsHiddenInSelection |= !aVisibility;

      if ( anObject->CanBeUpdated() && anObject->IsMustBeUpdated( HYDROData_Entity::Geom_All ) )
      {
        anIsMustObjectBeUpdated = true;
      }

      ObjectKind anObjectKind = anObject->GetKind();
      if( anObjectKind == KIND_IMAGE )
      {
        anIsImage = true;
        Handle(HYDROData_Image) anImage = Handle(HYDROData_Image)::DownCast( anObject );
        if( !anImage.IsNull() )
        {
          anIsImportedImage = anImage->HasLocalPoints();
          anIsImageHasRefs = anImage->HasReferences();
          if( HYDROData_OperationsFactory* aFactory = HYDROData_OperationsFactory::Factory() )
          {
            if( ImageComposer_Operator* anOperator = aFactory->Operator( anImage ) )
            {
              QString anOperatorName = anOperator->name();
              if( anOperatorName == ImageComposer_FuseOperator::Type() )
                anIsFusedImage = true;
              else if( anOperatorName == ImageComposer_CutOperator::Type() )
                anIsCutImage = true;
              else if( anOperatorName == ImageComposer_CropOperator::Type() )
                anIsSplitImage = true;
            }
          }
        }
      }
      else if( anObjectKind == KIND_POLYLINEXY )
        anIsPolyline = true;
      else if( anObjectKind == KIND_POLYLINE )
        anIsPolyline3D = true;
      else if( anObjectKind == KIND_PROFILE )
      {
        anIsProfile = true;
        aNbOfSelectedProfiles++;

        Handle(HYDROData_Profile) aProfile =
          Handle(HYDROData_Profile)::DownCast( anObject );
        if( !aProfile.IsNull() && aProfile->IsValid() ) {
          anIsValidProfile = true;
        }
      }
      else if( anObjectKind == KIND_CALCULATION )
        anIsCalculation = true;
      else if( anObjectKind == KIND_IMMERSIBLE_ZONE )
        anIsImmersibleZone = true;
      else if( anObjectKind == KIND_VISUAL_STATE )
        anIsVisualState = true;
      else if( anObjectKind == KIND_REGION )
        anIsRegion = true;
      else if( anObjectKind == KIND_ZONE )
        anIsZone = true;
      else if( anObjectKind == KIND_BATHYMETRY )
        anIsBathymetry = true;
      else if( anObjectKind == KIND_OBSTACLE )
        anIsObstacle = true;
      else if( anObjectKind == KIND_STRICKLER_TABLE )
        anIsStricklerTable = true;
      else if( anObjectKind == KIND_LAND_COVER_MAP )
        anIsLandCoverMap = true;
      else if (anObjectKind == KIND_BC_POLYGON)
        anIsBCPolygon = true;
      else if( anObjectKind == KIND_STREAM )
      {
        anIsStream = true;
        Handle(HYDROData_Stream) aStream =
          Handle(HYDROData_Stream)::DownCast( anObject );
        if ( !aStream.IsNull() )
          isStreamHasBottom = !aStream->GetBottomPolyline().IsNull();
      }
      else if( anObjectKind == KIND_CHANNEL )
        anIsChannel = true;
      else if( anObjectKind == KIND_DIGUE )
        anIsDigue = true;
      else if( anObjectKind == KIND_DUMMY_3D )
        anIsDummyObject3D = true;
      else if( anObjectKind == KIND_SHAPES_GROUP || anObjectKind == KIND_SPLIT_GROUP )
        anIsGroup = true;
    }

    if ( !anIsObjectCanBeColored )
      anIsObjectCanBeColored = HYDROGUI_SetColorOp::CanObjectBeColored( anObject, this );
  }

  // Check if all selected objects are profiles
  anAllAreProfiles = ( aNbOfSelectedProfiles > 0 ) &&
    ( aNbOfSelectedProfiles == aSeq.Length() );

  // check the selected partitions
  if( !anIsSelectedDataObjects && anIsObjectBrowser )
  {
    ObjectKind aSelectedPartition = HYDROGUI_Tool::GetSelectedPartition( this );
    if( aSelectedPartition != KIND_UNKNOWN )
    {
      switch( aSelectedPartition )
      {
      case KIND_IMAGE:
        theMenu->addAction( action( ImportImageId ) );
        break;
      case KIND_BATHYMETRY:
        theMenu->addAction( action( ImportBathymetryId ) );
        break;
      case KIND_ARTIFICIAL_OBJECT:
        theMenu->addAction( action( CreateChannelId ) );
        theMenu->addAction( action( CreateDigueId ) );
        break;
      case KIND_NATURAL_OBJECT:
        theMenu->addAction( action( CreateImmersibleZoneId ) );
        theMenu->addAction( action( CreateStreamId ) );
        break;
      case KIND_OBSTACLE:
        theMenu->addAction( action( ImportObstacleFromFileId ) );
        theMenu->addAction( action( CreateBoxId ) );
        theMenu->addAction( action( CreateCylinderId ) );
        break;
      case KIND_STRICKLER_TABLE:
        theMenu->addAction( action( ImportStricklerTableFromFileId ) );
        break;
      case KIND_LAND_COVER_MAP:
        theMenu->addAction( action( CreateLandCoverMapId ) );
        theMenu->addAction( action( ImportLandCoverMapId ) );
        break;
      case KIND_CALCULATION:
        theMenu->addAction( action( CreateCalculationId ) );
        break;
      case KIND_POLYLINEXY:
        theMenu->addAction( action( ImportPolylineId ) );
        theMenu->addAction( action( CreatePolylineId ) );
        break;
      case KIND_POLYLINE:
        theMenu->addAction( action( CreatePolyline3DId ) );
        theMenu->addAction( action( ImportPolylineId ) );
        break;
      case KIND_PROFILE:
        theMenu->addAction( action( CreateProfileId ) );
        theMenu->addAction( action( ImportProfilesId ) );
        theMenu->addAction( action( AllGeoreferencementId ) );
        break;
      case KIND_VISUAL_STATE:
        theMenu->addAction( action( SaveVisualStateId ) );
        break;
      case KIND_BC_POLYGON:
        theMenu->addAction( action( ImportBCPolygonId ) );
        break;
      }
      theMenu->addSeparator();
    }
    else
    {
      Handle(HYDROData_CalculationCase) aCalcCase;
      QString outStr;
      HYDROGUI_Tool::IsSelectedPartOfCalcCase(this, aCalcCase, outStr);
      if (outStr == HYDROGUI_DataModel::partitionName( KIND_REGION ))
        theMenu->addAction( action( RegenerateRegionColorsId ) );
    }
  }

  if( anIsSelectedDataObjects )
  {
    if ( anIsMustObjectBeUpdated )
    {
      theMenu->addAction( action( UpdateObjectId ) );
      theMenu->addSeparator();
    }
    else
    {
      theMenu->addAction( action( ForcedUpdateObjectId ) );
      theMenu->addSeparator();
    }


    if( aSeq.Length() == 1 )
    {
      if( anIsImage )
      {
        if( anIsImportedImage )
          theMenu->addAction( action( EditImportedImageId ) );
        else if( anIsImageHasRefs )
        {
          if( anIsFusedImage )
            theMenu->addAction( action( EditFusedImageId ) );
          else if( anIsCutImage )
            theMenu->addAction( action( EditCutImageId ) );
          else if( anIsSplitImage )
            theMenu->addAction( action( EditSplitImageId ) );
        }

        //RKV: BUG#98: theMenu->addAction( action( ObserveImageId ) );
        theMenu->addAction( action( ExportImageId ) );
        theMenu->addSeparator();

        if( anIsImageHasRefs )
        {
          theMenu->addAction( action( RemoveImageRefsId ) );
          theMenu->addSeparator();
        }

        theMenu->addAction( action( FuseImagesId ) );
        theMenu->addAction( action( CutImagesId ) );
        theMenu->addAction( action( SplitImageId ) );
        theMenu->addSeparator();
        theMenu->addAction( action( RecognizeContoursId ) );
        theMenu->addSeparator();
      }
      else if( anIsBathymetry )
      {
        theMenu->addAction( action( EditImportedBathymetryId ) );
        theMenu->addAction( action( BathymetryBoundsId ) );
        theMenu->addSeparator();
      }
      else if( anIsPolyline )
      {
        theMenu->addAction( action( EditPolylineId ) );
        theMenu->addSeparator();
        theMenu->addAction( action( SplitPolylinesId ) );
        theMenu->addAction( action( MergePolylinesId ) );
        theMenu->addSeparator();
        theMenu->addAction( action( ShowAttrPolylinesId ) );
        theMenu->addSeparator();
      }
      else if( anIsPolyline3D )
      {
        theMenu->addAction( action( EditPolyline3DId ) );
        theMenu->addSeparator();
      }
      else if( anIsProfile )
      {
        theMenu->addAction( action( EditProfileId ) );
        theMenu->addAction( action( SelectedGeoreferencementId ) );
        theMenu->addSeparator();
      }
      else if( anIsCalculation )
      {
        theMenu->addAction( action( EditCalculationId ) );
        theMenu->addAction( action( ExportCalculationId ) );
        theMenu->addAction( action( CompleteCalculationId ) );
        theMenu->addSeparator();
      }
      else if( anIsImmersibleZone )
      {
        theMenu->addAction( action( EditImmersibleZoneId ) );
        theMenu->addSeparator();
      }
      else if( anIsStream )
      {
        theMenu->addAction( action( EditStreamId ) );
        if ( action( RiverBottomContextId ) )
        {
          theMenu->addAction( action( RiverBottomContextId ) );
          action( RiverBottomContextId )->setEnabled( !isStreamHasBottom );
        }
        theMenu->addAction( action( ProfileInterpolateId ) );
        theMenu->addSeparator();
      }
      else if( anIsChannel )
      {
        theMenu->addAction( action( EditChannelId ) );
        theMenu->addSeparator();
      }
      else if( anIsDigue )
      {
        theMenu->addAction( action( EditDigueId ) );
        theMenu->addSeparator();
      }
      else if( anIsObstacle )
      {
        theMenu->addAction( action( TranslateObstacleId ) );
        theMenu->addSeparator();
      }
      else if( anIsStricklerTable )
      {
        theMenu->addAction( action( EditStricklerTableId ) );
        theMenu->addAction( action( ExportStricklerTableFromFileId ) );
        theMenu->addAction( action( DuplicateStricklerTableId ) );
        theMenu->addSeparator();

        Handle(HYDROData_StricklerTable) aTable =
          Handle(HYDROData_StricklerTable)::DownCast( aSeq.First() );
        QString aCurrentTable =
          HYDROGUI_DataObject::dataObjectEntry( getLandCoverColoringTable( anActiveViewId ) );
        bool isUsed = aCurrentTable == HYDROGUI_DataObject::dataObjectEntry( aTable );

        if ( !isUsed && !getObjectShapes( anActiveViewId, KIND_LAND_COVER_MAP ).isEmpty() ) {
          theMenu->addAction( action( LandCoverScalarMapModeOnId ) );
          theMenu->addSeparator();
        }
      }
      else if( anIsLandCoverMap )
      {
        theMenu->addAction( action( AddLandCoverId ) );
        theMenu->addAction( action( RemoveLandCoverId ) );
        theMenu->addSeparator();
        theMenu->addAction( action( SplitLandCoverId ) );
        theMenu->addAction( action( MergeLandCoverId ) );
        theMenu->addAction( action( ChangeLandCoverTypeId ) );
        theMenu->addSeparator();
        //
        theMenu->addAction( action( ExportToShapeFileID ) );
        theMenu->addSeparator();
      }
      else if( anIsVisualState && anIsObjectBrowser )
      {
        theMenu->addAction( action( SaveVisualStateId ) );
        theMenu->addAction( action( LoadVisualStateId ) );
        theMenu->addSeparator();
      }
      else if (anIsZone)
        theMenu->addAction( action( ZoneSetColorId ) );
      else if (anIsBCPolygon)
        theMenu->addAction( action( SetBoundaryTypePolygonId ) );

      if ( anIsStream || anIsChannel || anIsDigue || anIsObstacle )
      {
        theMenu->addAction( action( PolylineExtractionId ) );
        theMenu->addSeparator();
      }

      // Add set color action for geometrical objects
      if ( anIsObjectCanBeColored )
      {
        theMenu->addAction( action( SetColorId ) );
        theMenu->addSeparator();
      }

      // Add transparency action for land cover map objects
      if ( anIsLandCoverMap )
      {
        theMenu->addAction( action( SetTransparencyId ) );
        theMenu->addSeparator();
      }
    }
    else if ( anAllAreProfiles )
    {
      theMenu->addAction( action( EditProfileId ) );
      theMenu->addAction( action( SelectedGeoreferencementId ) );
      theMenu->addSeparator();
    }

    bool isPoly = anIsPolyline || anIsPolyline3D;
    if (isPoly  && !anIsLandCoverMap)
      theMenu->addAction( action( ExportToShapeFileID ) );

    // Add copy action
    QAction* aCopyAction = action( CopyId );
    if( aCopyAction && aCopyAction->isEnabled() ) {
      theMenu->addAction( action( CopyId ) );
      theMenu->addSeparator();
    }

    // Add delete action
    if( !anIsDummyObject3D )
      theMenu->addAction( action( DeleteId ) );

    theMenu->addSeparator();

    if( anIsImage || anIsPolyline || anIsPolyline3D ||
      anIsImmersibleZone || anIsZone || anIsRegion ||
      anIsBathymetry || anIsObstacle || anIsStream ||
      anIsChannel || anIsDigue || anIsDummyObject3D ||
      anIsValidProfile || anIsGroup || anIsLandCoverMap ||
      anIsBCPolygon)
    {
      if( anIsHiddenInSelection )
        theMenu->addAction( action( ShowId ) );
      theMenu->addAction( action( ShowOnlyId ) );
      if( anIsVisibleInSelection )
        theMenu->addAction( action( HideId ) );
      theMenu->addSeparator();
    }
  }

  if ( anIsOCCView )
  {
    SUIT_Operation* anOp = application()->activeStudy()->activeOperation();
    HYDROGUI_PolylineOp* aPolylineOp = dynamic_cast<HYDROGUI_PolylineOp*>( anOp );
    if ( aPolylineOp && aPolylineOp->deleteEnabled() )
      theMenu->addAction( action( DeleteId ) );

    theMenu->addSeparator();
    theMenu->addAction( action( SetZLevelId ) );
    theMenu->addSeparator();

    if ( isLandCoversScalarMapModeOn( anActiveViewId ) ) {
      theMenu->addAction( action( LandCoverScalarMapModeOffId ) );
      theMenu->addSeparator();
    }
  }

  if( anIsObjectBrowser || anIsGraphicsView || anIsOCCView || anIsVTKView )
  {
    theMenu->addAction( action( ShowAllId ) );
    theMenu->addAction( action( HideAllId ) );
    theMenu->addSeparator();
  }

  if ( anIsOCCView || anIsVTKView )
  {
    theMenu->addSeparator();
    theMenu->addAction( action( CopyViewerPositionId ) );
  }

  if( isRoot )
    theMenu->addAction( action( EditLocalCSId ) );

  if( anIsObjectBrowser && anOwners.size()==1 )
  {
    if( aSeq.Size() > 0 )
    {
      Handle( HYDROData_Entity ) aFirstEnt = aSeq.First();
      Handle(HYDROData_Object) anObject;
      Handle(HYDROData_ArtificialObject) anAObject = Handle( HYDROData_ArtificialObject )::DownCast(aFirstEnt);
      Handle(HYDROData_NaturalObject) aNObject = Handle( HYDROData_NaturalObject )::DownCast(aFirstEnt);

      if (!anAObject.IsNull())
        anObject = anAObject;
      if (!aNObject.IsNull())
        anObject = aNObject;

      if( !anObject.IsNull() )
      {
        theMenu->addSeparator();
        bool IsSubmersible = anObject->IsSubmersible();
        if (!IsSubmersible)
        {
          theMenu->addAction( action( SubmersibleId ) );
          action( SubmersibleId )->setCheckable(true);
          action( SubmersibleId )->setChecked(true);
        }
        else
        {
          theMenu->addAction( action( UnSubmersibleId ) );
          action( UnSubmersibleId )->setCheckable(true);
          action( UnSubmersibleId )->setChecked(true);
        }
      }
    }
  }

  theMenu->addSeparator();
  QAction* a = action( ShowHideArrows );
  a->setText( arrowsVisible() ? tr( "HIDE_ARROWS" ) : tr( "SHOW_ARROWS" ) );
  theMenu->addAction( a );
}

void HYDROGUI_Module::createPreferences()
{
  DEBTRACE("HYDROGUI_Module::createPreferences");
  int genTab = addPreference( tr( "PREF_TAB_GENERAL" ) );
  int CursorGroup = addPreference( tr( "PREF_GROUP_CURSOR" ), genTab );

  int typeOfCursor = addPreference( tr( "PREF_TYPE_OF_CURSOR" ), CursorGroup,
                                    LightApp_Preferences::Selector, "preferences", "type_of_cursor" );

  // Set property cursor type
  QList<QVariant> aCursorTypeIndicesList;
  QList<QVariant> aCursorTypeIconsList;

  SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();
  for ( int i = CT_ArrowCursor; i < CT_User; i++ ) {
    QString icoFile = QString( "ICON_CURSOR_%1" ).arg( i+1 );
    QPixmap pixmap = resMgr->loadPixmap( "HYDRO", tr( qPrintable( icoFile ) ) );
    aCursorTypeIndicesList << i;
    aCursorTypeIconsList << pixmap;
  }

  setPreferenceProperty( typeOfCursor, "indexes", aCursorTypeIndicesList );
  setPreferenceProperty( typeOfCursor, "icons",   aCursorTypeIconsList );

  int viewerGroup = addPreference( tr( "PREF_GROUP_VIEWER" ), genTab );
  addPreference( tr( "PREF_VIEWER_AUTO_FITALL" ), viewerGroup,
                 LightApp_Preferences::Bool, "HYDRO", "auto_fit_all" );

  addPreference( tr( "PREF_VIEWER_ZOOM_SHUTOFF" ), viewerGroup, LightApp_Preferences::Bool, "HYDRO", "zoom_shutoff" );

  addPreference( tr( "PREF_VIEWER_CHAINED_PANNING" ), viewerGroup, LightApp_Preferences::Bool, "HYDRO", "chained_panning" );

  int StricklerTableGroup = addPreference( tr( "PREF_GROUP_STRICKLER_TABLE" ), genTab );
  int defaultStricklerCoef = addPreference( tr( "PREF_DEFAULT_STRICKLER_COEFFICIENT" ), StricklerTableGroup,
                                            LightApp_Preferences::DblSpin, "preferences", "default_strickler_coefficient" );
  setPreferenceProperty( defaultStricklerCoef, "precision", 2 );
  setPreferenceProperty( defaultStricklerCoef, "min", 0.00 );
  setPreferenceProperty( defaultStricklerCoef, "max", 1000000.00 );
  setPreferenceProperty( defaultStricklerCoef, "step", 0.01 );

  int polylinesGroup = addPreference( tr( "PREF_GROUP_POLYLINES" ), genTab );
  int polylineArrow = addPreference( tr( "PREF_POLYLINE_ARROW" ), polylinesGroup,
    LightApp_Preferences::Selector, "polyline", "arrow_type" );

  QList<QVariant> arrow_types;
  arrow_types.append( tr( "No" ) );
  arrow_types.append( tr( "Triangle" ) );
  arrow_types.append( tr( "Cone" ) );
  setPreferenceProperty( polylineArrow, "strings", arrow_types );

  QList<QVariant> indices;
  indices.append( 0 );
  indices.append( 1 );
  indices.append( 2 );
  setPreferenceProperty( polylineArrow, "indexes", indices );
  setPreferenceProperty( polylineArrow, "ids", indices );

  int polylineSize = addPreference( tr( "PREF_POLYLINE_ARROW_SIZE" ), polylinesGroup,
    LightApp_Preferences::IntSpin, "polyline", "arrow_size" );
}

void HYDROGUI_Module::preferencesChanged( const QString& theSection, const QString& thePref )
{
    DEBTRACE("HYDROGUI_Module::preferencesChanged");
    SUIT_ResourceMgr* resMgr = application()->resourceMgr();
    if ( theSection == "preferences" && thePref == "default_strickler_coefficient" )
    {

        Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
        if ( resMgr && !aDoc.IsNull() )
            aDoc->SetDefaultStricklerCoefficient( resMgr->doubleValue( theSection, thePref, 0 ) );
    }
    else if( theSection == "polyline" )
    {
      int aType = -1;
      int aSize = -1;
      if( resMgr )
      {
        resMgr->value( "polyline", "arrow_type", aType );
        resMgr->value( "polyline", "arrow_size", aSize );
      }
      //Update polylines
      ViewManagerMap::const_iterator it = myViewManagerMap.begin(), last = myViewManagerMap.end();
      for( ; it!=last; it++ )
      {
        size_t aViewerId = it.key();
        OCCViewer_ViewManager* aMgr = dynamic_cast<OCCViewer_ViewManager*>( it.value().first );
        if( aMgr )
          getOCCDisplayer()->UpdatePolylines( aViewerId, aType, aSize );
      }
    }
    else if (theSection == "HYDRO" && thePref == "zoom_shutoff")
    {
      bool aZoomShutoff = resMgr->booleanValue( "HYDRO", "zoom_shutoff" );
      setAutoZoomToAllViewManagers(!aZoomShutoff);
    }
    else if (theSection == "HYDRO" && thePref == "chained_panning")
    {
      bool aChainedPan = resMgr->booleanValue( "HYDRO", "chained_panning" );
      if (!aChainedPan)
        resetViewState();
      ViewManagerList aViewManagers = getApp()->viewManagers();
      foreach (SUIT_ViewManager* aVMgr, aViewManagers)
      {
        OCCViewer_ViewManager* anOCCViewMgr = dynamic_cast<OCCViewer_ViewManager*>( aVMgr );
        if (anOCCViewMgr)
          anOCCViewMgr->setChainedOperations( aChainedPan );
      }

      QList<QDockWidget*> docW = getApp()->desktop()->findChildren<QDockWidget*>();
      foreach (QDockWidget* qw, docW)
      {
        HYDROGUI_ProfileDlg* pdlg = dynamic_cast<HYDROGUI_ProfileDlg*>(qw);
        if (pdlg)
          pdlg->viewManager()->setChainedOperations(aChainedPan);
      }
    }
    else
      LightApp_Module::preferencesChanged( theSection, thePref );
}

QCursor HYDROGUI_Module::getPrefEditCursor() const
{
  int aCursorType = SUIT_Session::session()->resourceMgr()->integerValue("preferences", "type_of_cursor", (int)CT_CrossCursor );
  if ( aCursorType >= Qt::BlankCursor)
    aCursorType++;
  QCursor aCursor = QCursor( Qt::CursorShape(aCursorType) );
  return aCursor;
}

void HYDROGUI_Module::update( const int flags )
{
  DEBTRACE("HYDROGUI_Module::update");
  if ( !isUpdateEnabled() )
    return;

  QApplication::setOverrideCursor( Qt::WaitCursor );

  // To prevent calling this method recursively
  // from one of the methods called below
  setUpdateEnabled( false );

  // store selected objects
  QStringList aSelectedEntries = storeSelection();

  bool aDoFitAll = flags & UF_FitAll;
  if ( aDoFitAll )
  {
    SUIT_ResourceMgr* aResMgr = getApp()->resourceMgr();
    aDoFitAll = aResMgr->booleanValue( "HYDRO", "auto_fit_all", false );
  }

  if( ( flags & UF_Viewer ) )
    updateViewer( getDisplayer(), flags & UF_GV_Init, flags & UF_GV_Forced, aDoFitAll );

  if( ( flags & UF_OCCViewer ) )
    updateViewer( getOCCDisplayer(), flags & UF_OCC_Init, flags & UF_OCC_Forced, aDoFitAll );

  if( ( flags & UF_VTKViewer ) )
    updateViewer( getVTKDisplayer(), flags & UF_VTK_Init, flags & UF_VTK_Forced, aDoFitAll );

  if( ( flags & UF_Model ) && getDataModel() && getApp() )
  {
    getDataModel()->update( getStudyId() );

    // Temporary workaround to prevent breaking
    // the selection in the object browser.
    // Note: processEvents() should be called after updateGV(),
    // otherwise the application crashes from time to time.
    //RKV: qApp->processEvents();
    SUIT_ResourceMgr* aResMgr = getApp()->resourceMgr();
    bool isResizeOnExpandItem = aResMgr->booleanValue( "ObjectBrowser", "resize_on_expand_item", false );
    SUIT_DataBrowser* anObjectBrowser = getApp()->objectBrowser();
    if ( isResizeOnExpandItem && anObjectBrowser ) {
      anObjectBrowser->setResizeOnExpandItem( false ); // MZN: ISSUE #280
    }
    getApp()->updateObjectBrowser( true );
    if ( isResizeOnExpandItem && anObjectBrowser ) {
      anObjectBrowser->setResizeOnExpandItem( true ); // MZN: ISSUE #280
    }
  }

  // Object browser is currently updated by using UF_Model flag
  if( ( flags & UF_ObjBrowser ) && ((flags & UF_Model) == 0) && getApp() )
    getApp()->updateObjectBrowser( true );

  if( ( flags & UF_Controls ) && getApp() )
    getApp()->updateActions();

  // restore selected objects
  restoreSelection( aSelectedEntries );

  setUpdateEnabled( true );

  preferencesChanged( "HYDRO", "zoom_shutoff" );

  preferencesChanged( "HYDRO", "chained_panning" );

  QApplication::restoreOverrideCursor();
}

void HYDROGUI_Module::updateCommandsStatus()
{
  DEBTRACE("HYDROGUI_Module::updateCommandsStatus");
  LightApp_Module::updateCommandsStatus();

  updateUndoRedoControls();

  action( CopyId )->setEnabled( getDataModel()->canCopy() );
  action( PasteId )->setEnabled( getDataModel()->canPaste() );
}

void HYDROGUI_Module::selectionChanged()
{
  DEBTRACE("HYDROGUI_Module::selectionChanged");
  LightApp_Module::selectionChanged();
  updateCommandsStatus();
}

HYDROGUI_DataModel* HYDROGUI_Module::getDataModel() const
{
  //DEBTRACE("HYDROGUI_Module::getDataModel()");
  return (HYDROGUI_DataModel*)dataModel();
}

HYDROGUI_Displayer* HYDROGUI_Module::getDisplayer() const
{
  //DEBTRACE("HYDROGUI_Module::getDisplayer()");
  return myDisplayer;
}

HYDROGUI_OCCDisplayer* HYDROGUI_Module::getOCCDisplayer() const
{
  DEBTRACE("HYDROGUI_Module::getOCCDisplayer");
  return myOCCDisplayer;
}

HYDROGUI_VTKPrsDisplayer* HYDROGUI_Module::getVTKDisplayer() const
{
  DEBTRACE("HYDROGUI_Module::getVTKDisplayer");
  return myVTKDisplayer;
}

SUIT_ViewManager* HYDROGUI_Module::getViewManager( const size_t theId ) const
{
  DEBTRACE("HYDROGUI_Module::getViewManager " << theId);
  if( myViewManagerMap.contains( theId ) )
  {
    return myViewManagerMap[ theId ].first;
  }
  return NULL;
}

GraphicsView_Viewer* HYDROGUI_Module::getViewer( const size_t theId ) const
{
  DEBTRACE("HYDROGUI_Module::getViewer " << theId);
  if( myViewManagerMap.contains( theId ) )
  {
    ViewManagerInfo anInfo = myViewManagerMap[ theId ];
    GraphicsView_ViewManager* aViewManager =
      dynamic_cast<GraphicsView_ViewManager*>( anInfo.first );
    if( aViewManager )
      return aViewManager->getViewer();
  }
  return NULL;
}

OCCViewer_Viewer* HYDROGUI_Module::getOCCViewer( const size_t theId ) const
{
  DEBTRACE("HYDROGUI_Module::getOCCViewer " << theId);
  if( myViewManagerMap.contains( theId ) )
  {
    ViewManagerInfo anInfo = myViewManagerMap[ theId ];
    OCCViewer_ViewManager* aViewManager =
      ::qobject_cast<OCCViewer_ViewManager*>( anInfo.first );
    if( aViewManager )
      return aViewManager->getOCCViewer();
  }
  return NULL;
}

SVTK_Viewer* HYDROGUI_Module::getVTKViewer( const size_t theId ) const
{
  DEBTRACE("HYDROGUI_Module::getVTKViewer "  << theId);
  if( myViewManagerMap.contains( theId ) )
  {
    ViewManagerInfo anInfo = myViewManagerMap[ theId ];
    SVTK_ViewManager* aViewManager =
      ::qobject_cast<SVTK_ViewManager*>( anInfo.first );
    if( aViewManager )
      return dynamic_cast<SVTK_Viewer*>( aViewManager->getViewModel() );
  }
  return NULL;
}

size_t HYDROGUI_Module::getViewManagerId( SUIT_ViewManager* theViewManager )
{
  DEBTRACE("HYDROGUI_Module::getViewManagerId");
  ViewManagerMapIterator anIter( myViewManagerMap );
  while( anIter.hasNext() )
  {
    size_t anId = anIter.next().key();
    const ViewManagerInfo& anInfo = anIter.value();
    if( anInfo.first == theViewManager )
      return anId;
  }
  return 0;
}

HYDROGUI_Module::ViewManagerRole HYDROGUI_Module::getViewManagerRole( SUIT_ViewManager* theViewManager )
{
  DEBTRACE("HYDROGUI_Module::getViewManagerRole");
  size_t anId = getViewManagerId( theViewManager );
  if( anId == 0 )
  {
    const ViewManagerInfo& anInfo = myViewManagerMap[ anId ];
    return anInfo.second;
  }
  return VMR_Unknown;
}

void HYDROGUI_Module::setViewManagerRole( SUIT_ViewManager* theViewManager,
                                          const ViewManagerRole theRole )
{
  DEBTRACE("HYDROGUI_Module::setViewManagerRole");
  size_t anId = getViewManagerId( theViewManager );
  if( anId != 0 )
  {
    ViewManagerInfo& anInfo = myViewManagerMap[ anId ];
    anInfo.second = theRole;
  }
}

bool HYDROGUI_Module::isObjectVisible( const size_t theViewId,
                                       const Handle(HYDROData_Entity)& theObject ) const
{
  if( theObject.IsNull() )
    return false;

  if( theViewId == 0 )
  {
    //search in all
    foreach( size_t aViewId, myObjectStateMap.keys() )
    {
      if( isObjectVisible( aViewId, theObject ) )
        return true;
    }
    return false;
  }

  ViewId2Entry2ObjectStateMap::const_iterator anIter1 = myObjectStateMap.find( theViewId );
  if( anIter1 != myObjectStateMap.end() )
  {
    const Entry2ObjectStateMap& aEntry2ObjectStateMap = anIter1.value();
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( theObject );

    Entry2ObjectStateMap::const_iterator anIter2 = aEntry2ObjectStateMap.find( anEntry );
    if( anIter2 != aEntry2ObjectStateMap.end() )
    {
      const ObjectState& anObjectState = anIter2.value();
      return anObjectState.Visibility;
    }
  }
  return false;
}

void HYDROGUI_Module::setObjectVisible( const size_t theViewId,
                                        const Handle(HYDROData_Entity)& theObject,
                                        const bool theState )
{
  DEBTRACE("setObjectVisible, theViewId: " << theViewId << " " << theState);
  if( !theObject.IsNull() )
  {
	DEBTRACE("myObjectStateMap.size: " << myObjectStateMap.size());
	QString anEntry = HYDROGUI_DataObject::dataObjectEntry( theObject );
	DEBTRACE("anEntry: " << anEntry.toStdString());
	if (myObjectStateMap.find(theViewId) == myObjectStateMap.end())
	{
		DEBTRACE("theViewId is not a valid key for myObjectStateMap");
	}
    if (theViewId)
    {
        Entry2ObjectStateMap& aEntry2ObjectStateMap = myObjectStateMap[ theViewId ]; // created OK if it does not exist
        DEBTRACE("myObjectStateMap key created");

        ObjectState& anObjectState = aEntry2ObjectStateMap[ anEntry ];
        anObjectState.Visibility = theState;
    }

    HYDROGUI_DataObject* hydroObject = getDataModel()->getDataObject( theObject );
    if ( hydroObject )
    {
        DEBTRACE("hydroObject " << hydroObject);
        SUIT_AbstractModel* treeModel = dynamic_cast<SUIT_AbstractModel*>( getApp()->objectBrowser()->model() );
        if ( treeModel )
        {
          DEBTRACE("treeModel " << treeModel);
          QString id = hydroObject->text( hydroObject->customData( Qtx::IdType ).toInt() );
          Qtx::VisibilityState visState = treeModel->visibilityState( id );
          DEBTRACE("id " << id << " visState "<< visState);
          if ( visState != Qtx::UnpresentableState )
            treeModel->setVisibilityState( id, theState ? Qtx::ShownState : Qtx::HiddenState );
        }
    }

    if ( theObject->GetKind() == KIND_BATHYMETRY && theState ) {
      setLandCoversScalarMapModeOff( theViewId );
    } else if ( theObject->GetKind() == KIND_LAND_COVER_MAP && theState ) {
      getOCCDisplayer()->SetToUpdateColorScale();
    }
  }
}

void HYDROGUI_Module::setIsToUpdate( const Handle(HYDROData_Entity)& theObject,
                                     const bool theState )
{
  DEBTRACE("HYDROGUI_Module::setIsToUpdate");
  if( !theObject.IsNull() )
  {
    // Process OCC shapes
    ViewId2ListOfShapes::const_iterator aShapesMapIter( myShapesMap.begin() );
    while( aShapesMapIter != myShapesMap.end() )
    {
      const ListOfShapes& aShapesList = aShapesMapIter.value();
      foreach ( HYDROGUI_Shape* aShape, aShapesList )
      {
        if ( aShape && IsEqual( aShape->getObject(), theObject ) )
        {
          aShape->setIsToUpdate( theState );
        }
      }
      aShapesMapIter++;
    }
    // Process VTK shapes
    ViewId2ListOfVTKPrs::const_iterator aVTKPrsMapIter( myVTKPrsMap.begin() );
    while( aVTKPrsMapIter != myVTKPrsMap.end() )
    {
      const ListOfVTKPrs& aShapesList = aVTKPrsMapIter.value();
      foreach ( HYDROGUI_VTKPrs* aShape, aShapesList )
      {
        if ( aShape && IsEqual( aShape->getObject(), theObject ) )
        {
          aShape->setIsToUpdate( theState );
        }
      }
      aVTKPrsMapIter++;
    }
  }
}

/////////////////// OCC SHAPES PROCESSING
QList<HYDROGUI_Shape*> HYDROGUI_Module::getObjectShapes( const size_t  theViewId,
                                                         ObjectKind theKind ) const
{
  QList<HYDROGUI_Shape*> aResult;

  if ( myShapesMap.contains( theViewId ) )
  {
    const ListOfShapes& aViewShapes = myShapesMap.value( theViewId );
    foreach ( HYDROGUI_Shape* aShape, aViewShapes )
    {
      if( aShape && aShape->getObject()->GetKind()==theKind )
        aResult.append( aShape );
    }
  }
  return aResult;
}

HYDROGUI_Shape* HYDROGUI_Module::getObjectShape( const size_t                    theViewId,
                                                 const Handle(HYDROData_Entity)& theObject ) const
{
  HYDROGUI_Shape* aResShape = NULL;
  if( theObject.IsNull() )
    return aResShape;

  if ( myShapesMap.contains( theViewId ) )
  {
    const ListOfShapes& aViewShapes = myShapesMap.value( theViewId );
    foreach ( HYDROGUI_Shape* aShape, aViewShapes )
    {
      if ( !aShape || !IsEqual( aShape->getObject(), theObject ) )
        continue;

      aResShape = aShape;
      break;
    }
  }

  return aResShape;
}

void HYDROGUI_Module::setObjectShape( const size_t                    theViewId,
                                      const Handle(HYDROData_Entity)& theObject,
                                      HYDROGUI_Shape*                 theShape )
{
  if( theObject.IsNull() )
    return;

  ListOfShapes& aViewShapes = myShapesMap[ theViewId ];
  aViewShapes.append( theShape );
}

void HYDROGUI_Module::removeObjectShape( const size_t                    theViewId,
                                         const Handle(HYDROData_Entity)& theObject )
{
  if ( !myShapesMap.contains( theViewId ) )
    return;

  ListOfShapes& aViewShapes = myShapesMap[ theViewId ];
  Handle(HYDROData_Entity) anObject;
  for ( int i = 0; i < aViewShapes.length(); )
  {
    HYDROGUI_Shape* aShape = aViewShapes.at( i );
    anObject = aShape->getObject();
    if ( aShape && (!anObject.IsNull()) && IsEqual( anObject, theObject ) )
    {
      DEBTRACE("delete shape :" << aShape->getObject()->GetName().toStdString());
      delete aShape;
      aViewShapes.removeAt( i );
      continue;
    }

    ++i;
  }
}

void HYDROGUI_Module::removeViewShapes( const size_t theViewId )
{
  if ( !myShapesMap.contains( theViewId ) )
    return;

  const ListOfShapes& aViewShapes = myShapesMap.value( theViewId );
  for ( int i = 0, n = aViewShapes.length(); i < n; ++i )
  {
    HYDROGUI_Shape* aShape = aViewShapes.at( i );
    if ( aShape )
      delete aShape;
  }

  myShapesMap.remove( theViewId );
}
/////////////////// END OF OCC SHAPES PROCESSING

/////////////////// VTKPrs PROCESSING
HYDROGUI_VTKPrs* HYDROGUI_Module::getObjectVTKPrs( const size_t                  theViewId,
                                                 const Handle(HYDROData_Entity)& theObject ) const
{
  HYDROGUI_VTKPrs* aResShape = NULL;
  if( theObject.IsNull() )
    return aResShape;

  if ( myVTKPrsMap.contains( theViewId ) )
  {
    const ListOfVTKPrs& aViewShapes = myVTKPrsMap.value( theViewId );
    foreach ( HYDROGUI_VTKPrs* aShape, aViewShapes )
    {
      if ( !aShape || !IsEqual( aShape->getObject(), theObject ) )
        continue;

      aResShape = aShape;
      break;
    }
  }

  return aResShape;
}

void HYDROGUI_Module::setObjectVTKPrs( const size_t                    theViewId,
                                       const Handle(HYDROData_Entity)& theObject,
                                       HYDROGUI_VTKPrs*                 theShape )
{
  if( theObject.IsNull() )
    return;

  if( theShape && theShape->needScalarBar() )
  {
    // Compute the new global Z range from the added presentation and the old global Z range.
    double* aGlobalRange = getVTKDisplayer()->GetZRange( theViewId );
    double* aRange = theShape->getInternalZRange();
    bool anIsUpdate = false;
    if ( aRange[0] < aGlobalRange[0] )
    {
      aGlobalRange[0] = aRange[0];
      anIsUpdate = true;
    }
    if ( aRange[1] > aGlobalRange[1] )
    {
      aGlobalRange[1] = aRange[1];
      anIsUpdate = true;
    }

    //if ( anIsUpdate )
    //{
      updateVTKZRange( theViewId, aGlobalRange );
    //}
  }

  ListOfVTKPrs& aViewShapes = myVTKPrsMap[ theViewId ];
  aViewShapes.append( theShape );
}

void HYDROGUI_Module::removeObjectVTKPrs( const size_t   theViewId,
                                          const QString& theEntry )
{
  if ( !myVTKPrsMap.contains( theViewId ) )
    return;

  ListOfVTKPrs& aViewShapes = myVTKPrsMap[ theViewId ];
  Handle(HYDROData_Entity) anObject;
  QString anEntryRef;
  for ( int i = 0; i < aViewShapes.length(); )
  {
    HYDROGUI_VTKPrs* aShape = aViewShapes.at( i );
    anObject = aShape->getObject();
    anEntryRef = HYDROGUI_DataObject::dataObjectEntry( anObject );
    if ( aShape && (!anObject.IsNull()) && ( anEntryRef == theEntry ) )
    {
      delete aShape;
      aViewShapes.removeAt( i );
      continue;
    }

    ++i;
  }

  // Invalidate global Z range
  double anInvalidRange[2] = { HYDROGUI_VTKPrs::InvalidZValue(), HYDROGUI_VTKPrs::InvalidZValue() };
  getVTKDisplayer()->SetZRange( theViewId, anInvalidRange );
}

void HYDROGUI_Module::removeObjectVTKPrs( const size_t                    theViewId,
                                          const Handle(HYDROData_Entity)& theObject )
{
  if ( !myVTKPrsMap.contains( theViewId ) )
    return;

  ListOfVTKPrs& aViewShapes = myVTKPrsMap[ theViewId ];
  Handle(HYDROData_Entity) anObject;
  for ( int i = 0; i < aViewShapes.length(); )
  {
    HYDROGUI_VTKPrs* aShape = aViewShapes.at( i );
    anObject = aShape->getObject();
    if ( aShape && (!anObject.IsNull()) && IsEqual( anObject, theObject ) )
    {
      delete aShape;
      aViewShapes.removeAt( i );
      continue;
    }

    ++i;
  }

  // Invalidate global Z range
  double anInvalidRange[2] = { HYDROGUI_VTKPrs::InvalidZValue(), HYDROGUI_VTKPrs::InvalidZValue() };
  getVTKDisplayer()->SetZRange( theViewId, anInvalidRange );
}

void HYDROGUI_Module::removeViewVTKPrs( const size_t theViewId )
{
  if ( !myVTKPrsMap.contains( theViewId ) )
    return;

  const ListOfVTKPrs& aViewShapes = myVTKPrsMap.value( theViewId );
  for ( int i = 0, n = aViewShapes.length(); i < n; ++i )
  {
    HYDROGUI_VTKPrs* aShape = aViewShapes.at( i );
    if ( aShape )
      delete aShape;
  }

  myVTKPrsMap.remove( theViewId );
}

void HYDROGUI_Module::updateVTKZRange( const size_t theViewId, double theRange[] )
{
  if ( myVTKPrsMap.contains( theViewId ) )
  {
    // For the given viewer id update all VTK presentations ...
    const ListOfVTKPrs& aViewShapes = myVTKPrsMap.value( theViewId );
    HYDROGUI_VTKPrs* aShape;
    for ( int i = 0, n = aViewShapes.length(); i < n; ++i )
    {
      aShape = aViewShapes.at( i );
      if ( aShape && aShape->needScalarBar() )
      {
        aShape->setZRange( theRange );
      }
    }
  }
  // ... and update the global color legend scalar bar.
  getVTKDisplayer()->SetZRange( theViewId, theRange );
}
/////////////////// END OF VTKPrs PROCESSING

void HYDROGUI_Module::clearCache()
{
    DEBTRACE("HYDROGUI_Module::clearCache");
    myObjectStateMap.clear();
}

CAM_DataModel* HYDROGUI_Module::createDataModel()
{
  DEBTRACE("HYDROGUI_Module::createDataModel");
  return new HYDROGUI_DataModel( this );
}

void HYDROGUI_Module::customEvent( QEvent* e )
{
  int aType = e->type();
  if ( aType == NewViewEvent )
  {
    SALOME_CustomEvent* ce = ( SALOME_CustomEvent* )e;
    if( GraphicsView_ViewFrame* aViewFrame = ( GraphicsView_ViewFrame* )ce->data() )
    {
      if( GraphicsView_Viewer* aViewer = dynamic_cast<GraphicsView_Viewer*>( aViewFrame->getViewer() ) )
      {
        SUIT_ViewManager* aViewManager = aViewer->getViewManager();
        ViewManagerRole aRole = getViewManagerRole( aViewManager );

        if( GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort() )
        {
          if( aRole != VMR_TransformImage && aRole != VMR_ReferenceImage )
            aViewPort->scale( 1, -1 ); // invert the Y axis direction from down to up

          aViewPort->setInteractionFlag( GraphicsView_ViewPort::TraceBoundingRect );
          aViewPort->setInteractionFlag( GraphicsView_ViewPort::ImmediateContextMenu );
          aViewPort->setInteractionFlag( GraphicsView_ViewPort::ImmediateSelection );

          //ouv: temporarily commented
          //aViewPort->setViewLabelPosition( GraphicsView_ViewPort::VLP_BottomLeft, true );
        }

        if( aRole != VMR_TransformImage && aRole != VMR_ReferenceImage )
          update( UF_Viewer );

        aViewer->activateTransform( GraphicsView_Viewer::FitAll );
      }
    }
  }
}

bool HYDROGUI_Module::eventFilter( QObject* theObj, QEvent* theEvent )
{
  QEvent::Type aType = theEvent->type();
  if( theObj->inherits( "GraphicsView_ViewFrame" ) )
  {
    if( aType == QEvent::Show )
    {
      SALOME_CustomEvent* e = new SALOME_CustomEvent( NewViewEvent );
      e->setData( theObj );
      QApplication::postEvent( this, e );
      theObj->removeEventFilter( this );
    }
  }
  else if ( theObj->inherits( "OCCViewer_ViewPort" ) )
  {
    if( aType == QEvent::Leave )
    {
      SUIT_Desktop* aDesktop = getApp()->desktop();
      if ( aDesktop && aDesktop->statusBar() ) {
        aDesktop->statusBar()->clearMessage();
      }
    }
  }
  else if ( theObj->inherits( "SVTK_ViewWindow" ) )
  {
    if( aType == QEvent::Leave )
    {
      SUIT_Desktop* aDesktop = getApp()->desktop();
      if ( aDesktop && aDesktop->statusBar() ) {
        aDesktop->statusBar()->clearMessage();
      }
    }
  }

  return LightApp_Module::eventFilter( theObj, theEvent );
}

void HYDROGUI_Module::onViewManagerAdded( SUIT_ViewManager* theViewManager )
{
  DEBTRACE("HYDROGUI_Module::onViewManagerAdded");
  LightApp_Module::onViewManagerAdded( theViewManager );

  if( theViewManager->getType() == GraphicsView_Viewer::Type() )
  {
    connect( theViewManager, SIGNAL( viewCreated( SUIT_ViewWindow* ) ),
             this, SLOT( onViewCreated( SUIT_ViewWindow* ) ) );
  }
  else if( theViewManager->getType() == OCCViewer_Viewer::Type() )
  {
    OCCViewer_ViewManager* mgr = dynamic_cast<OCCViewer_ViewManager*>( theViewManager );
    //mgr->setChainedOperations( true );

    connect( theViewManager, SIGNAL( viewCreated( SUIT_ViewWindow* ) ),
             this, SLOT( onViewCreated( SUIT_ViewWindow* ) ) );
    connect( theViewManager, SIGNAL( mouseMove( SUIT_ViewWindow*, QMouseEvent* ) ),
             this, SLOT( onMouseMove( SUIT_ViewWindow*, QMouseEvent* ) ) );
    connect( theViewManager, SIGNAL( activated( SUIT_ViewManager* ) ),
             this, SLOT( onViewActivated( SUIT_ViewManager* ) ) );
  }
  else if( theViewManager->getType() == SVTK_Viewer::Type() )
  {
    connect( theViewManager, SIGNAL( viewCreated( SUIT_ViewWindow* ) ),
             this, SLOT( onViewCreated( SUIT_ViewWindow* ) ) );
    connect( theViewManager, SIGNAL( mouseMove( SUIT_ViewWindow*, QMouseEvent* ) ),
             this, SLOT( onMouseMove( SUIT_ViewWindow*, QMouseEvent* ) ) );
  }

  createSelector( theViewManager ); // replace the default selector

  ViewManagerInfo anInfo( theViewManager, VMR_General );
  myViewManagerMap.insert( ViewManagerId++, anInfo );
}

void HYDROGUI_Module::removeViewManager( SUIT_ViewManager* theViewManager)
{
  DEBTRACE("removeViewManager")

  size_t anId = getViewManagerId( theViewManager );
  if( anId != 0 )
  {
    bool isDisableGVSelector = theViewManager->getType() == GraphicsView_Viewer::Type();
    createSelector( theViewManager, isDisableGVSelector ); // replace the default selector

    OCCViewer_ViewManager* anOCCViewManager =
      ::qobject_cast<OCCViewer_ViewManager*>( myViewManagerMap[ anId ].first );
    if ( anOCCViewManager )
    {
      OCCViewer_Viewer* anOCCViewer = anOCCViewManager->getOCCViewer();
      if ( anOCCViewer ) {
        size_t aViewerId = (size_t)anOCCViewer;
        removeViewShapes( aViewerId );
        setLandCoversScalarMapModeOff( aViewerId );
      }
    }

    if ( getVTKDisplayer()->IsApplicable( theViewManager ) )
    {
      SVTK_Viewer* aVTKViewer = getVTKViewer( anId );
      if ( aVTKViewer )
      {
        getVTKDisplayer()->EraseScalarBar( anId, true );
        removeViewShapes( (size_t)aVTKViewer );
      }
    }

    myViewManagerMap.remove( anId );
  }
}

void HYDROGUI_Module::onViewManagerRemoved( SUIT_ViewManager* theViewManager )
{
  DEBTRACE("HYDROGUI_Module::onViewManagerRemoved");
  removeViewManager(theViewManager);
  LightApp_Module::onViewManagerRemoved( theViewManager );

}

void HYDROGUI_Module::onViewCreated( SUIT_ViewWindow* theViewWindow )
{
  DEBTRACE("HYDROGUI_Module::onViewCreated");
  if( theViewWindow && theViewWindow->inherits( "GraphicsView_ViewFrame" ) )
  {
	DEBTRACE("theViewWindow->inherits( 'GraphicsView_ViewFrame' )");
    if( GraphicsView_ViewFrame* aViewFrame = dynamic_cast<GraphicsView_ViewFrame*>( theViewWindow ) )
    {
      DEBTRACE("---");
      aViewFrame->installEventFilter( this );

      GraphicsView_ViewPort* aViewPort = aViewFrame->getViewPort();
      aViewPort->setInteractionFlag( GraphicsView_ViewPort::GlobalWheelScaling );

      connect( aViewPort, SIGNAL( vpMouseEvent( QGraphicsSceneMouseEvent* ) ),
               this, SLOT( onViewPortMouseEvent( QGraphicsSceneMouseEvent* ) ) );
    }
  }
  else if( theViewWindow && theViewWindow->inherits( "OCCViewer_ViewFrame" ) )
  {
	DEBTRACE("theViewWindow->inherits( 'OCCViewer_ViewFrame' )");
    if( OCCViewer_ViewFrame* aViewFrame = dynamic_cast<OCCViewer_ViewFrame*>( theViewWindow ) )
    {
      DEBTRACE("---");
      aViewFrame->onTopView();

      HYDROGUI_Tool::setOCCActionShown( aViewFrame, OCCViewer_ViewWindow::MaximizedId, false );
      OCCViewer_ViewPort3d* aViewPort = aViewFrame->getViewPort();
      if ( aViewPort ) {
        aViewPort->installEventFilter( this );
      }
    }
  }
  else if( theViewWindow && theViewWindow->inherits( "SVTK_ViewWindow" ) )
  {
	DEBTRACE("theViewWindow->inherits( 'SVTK_ViewWindow' )");
    if( SVTK_ViewWindow* aViewFrame = dynamic_cast<SVTK_ViewWindow*>( theViewWindow ) )
    {
      DEBTRACE("---");
      aViewFrame->installEventFilter( this );
    }
  }
}

void HYDROGUI_Module::onViewPortMouseEvent( QGraphicsSceneMouseEvent* theEvent )
{
  /* ouv: currently unused
  if( GraphicsView_ViewPort* aViewPort = qobject_cast<GraphicsView_ViewPort*>( sender() ) )
  {
    SUIT_ViewManager* aViewManager = 0;

    QObject* aParent = aViewPort;
    while( aParent = aParent->parent() )
    {
      if( GraphicsView_ViewFrame* aViewFrame = dynamic_cast<GraphicsView_ViewFrame*>( aParent ) )
      {
        if( GraphicsView_Viewer* aViewer = aViewFrame->getViewer() )
        {
          aViewManager = aViewer->getViewManager();
          break;
        }
      }
    }

    if( !aViewManager )
      return;

    double aMouseX = theEvent->scenePos().x();
    double aMouseY = theEvent->scenePos().y();

    ViewManagerRole aRole = getViewManagerRole( aViewManager );
    if( aRole == VMR_General )
    {
      int aXDeg = 0, aYDeg = 0;
      int aXMin = 0, aYMin = 0;
      double aXSec = 0, aYSec = 0;
      HYDROData_Lambert93::secToDMS( aMouseX, aXDeg, aXMin, aXSec );
      HYDROData_Lambert93::secToDMS( aMouseY, aYDeg, aYMin, aYSec );

      QString aDegSymbol( QChar( 0x00B0 ) );
      QString aXStr = QString( "%1%2 %3' %4\"" ).arg( aXDeg ).arg( aDegSymbol ).arg( aXMin ).arg( aXSec );
      QString aYStr = QString( "%1%2 %3' %4\"" ).arg( aYDeg ).arg( aDegSymbol ).arg( aYMin ).arg( aYSec );

      aViewPort->setViewLabelText( QString( "X: %1\nY: %2" ).arg( aXStr ).arg( aYStr ) );
    }
    else if( aRole == VMR_TransformImage )
      aViewPort->setViewLabelText( QString( "X: %1\nY: %2" ).arg( (int)aMouseX ).arg( (int)aMouseY ) );
  }
  */
}

void HYDROGUI_Module::updateViewer( HYDROGUI_AbstractDisplayer* theDisplayer,
                                    const bool theIsInit,
                                    const bool theIsForced,
                                    const bool theDoFitAll )
{
  DEBTRACE("HYDROGUI_Module::updateViewer");
  QList<size_t> aViewManagerIdList;

  // currently, all views are updated
  ViewManagerMapIterator anIter( myViewManagerMap );
  while( anIter.hasNext() )
  {
    SUIT_ViewManager* aViewManager = anIter.next().value().first;

    if ( theDisplayer->IsApplicable( aViewManager ) )
    {
      int anId = anIter.key();
      aViewManagerIdList.append( anId );
    }
  }

  QListIterator<size_t> anIdIter( aViewManagerIdList );
  while( anIdIter.hasNext() )
  {
    theDisplayer->UpdateAll( anIdIter.next(), theIsInit, theIsForced, theDoFitAll );
    myOverview->setTopView();
  }
}

void HYDROGUI_Module::createSelector( SUIT_ViewManager* theViewManager, bool isDisableGV )
{
  DEBTRACE("HYDROGUI_Module::createSelector " << isDisableGV);
  if( !theViewManager )
    return;

  LightApp_SelectionMgr* aSelectionMgr = getApp()->selectionMgr();
  if( !aSelectionMgr )
    return;

  QString aViewType = theViewManager->getType();
  DEBTRACE("aViewType " << aViewType.toStdString());
  if( aViewType != GraphicsView_Viewer::Type() &&
      aViewType != OCCViewer_Viewer::Type())
    return;

  QList<SUIT_Selector*> aSelectorList;
  aSelectionMgr->selectors( aViewType, aSelectorList );

  // disable all alien selectors
  bool isGV = false;
  bool isOCC = false;
  QList<SUIT_Selector*>::iterator anIter, anIterEnd = aSelectorList.end();
  for( anIter = aSelectorList.begin(); anIter != anIterEnd; anIter++ )
  {
    SUIT_Selector* aSelector = *anIter;
    bool isOk = false;
    if (aSelector)
    {
    	DEBTRACE("Selector " << aSelector->type().toStdString());
    	if (dynamic_cast<HYDROGUI_GVSelector*>( aSelector ))
    	{
    		isOk = !isDisableGV;
    		isGV = true;
    		DEBTRACE("HYDROGUI_GVSelector " << isOk);
    	}
    	else if (dynamic_cast<SVTK_Selector*>( aSelector ))
    	{
    		isOk = true;
    		DEBTRACE("SVTK_Selector");
    	}
    	else if (dynamic_cast<HYDROGUI_OCCSelector*>( aSelector ))
    	{
    		isOk = true;
    		isOCC = true;
    		DEBTRACE("HYDROGUI_OCCSelector");
    	}
    	if (isOk)
		{
		  DEBTRACE("Selector enabled " << aSelector->type().toStdString());
		  aSelector->setEnabled( true );
		}
    	else
		{
		  DEBTRACE("Selector disabled " << aSelector->type().toStdString());
		  aSelector->setEnabled( false );
		}
    }
  }

  if ( aViewType == GraphicsView_Viewer::Type() )
  {
    GraphicsView_ViewManager* aViewManager =
      ::qobject_cast<GraphicsView_ViewManager*>( theViewManager );
    if( aViewManager )
    {
      HYDROGUI_GVSelector* sel = new HYDROGUI_GVSelector( this, aViewManager->getViewer(), aSelectionMgr );
      DEBTRACE("new HYDROGUI_GVSelector " << sel << " " << sel->type().toStdString());
      sel->setEnabled( true );
    }
  }
  else if ( aViewType == OCCViewer_Viewer::Type() )
  {
    OCCViewer_ViewManager* aViewManager =
      ::qobject_cast<OCCViewer_ViewManager*>( theViewManager );
    if( aViewManager )
    {
      HYDROGUI_OCCSelector* sel =  new HYDROGUI_OCCSelector( this, aViewManager->getOCCViewer(), aSelectionMgr );
      DEBTRACE("new HYDROGUI_OCCSelector " << sel << " " << sel->type().toStdString());
      sel->setEnabled( true );
    }
  }
}

bool HYDROGUI_Module::setUpdateEnabled( const bool theState )
{
  DEBTRACE("HYDROGUI_Module::setUpdateEnabled");
  bool aPrevState = myIsUpdateEnabled;
  myIsUpdateEnabled = theState;
  return aPrevState;
}

bool HYDROGUI_Module::isUpdateEnabled() const
{
  return myIsUpdateEnabled;
}

QStringList HYDROGUI_Module::storeSelection() const
{
  DEBTRACE("HYDROGUI_Module::storeSelection");
  QStringList anEntryList;
  if( LightApp_SelectionMgr* aSelectionMgr = getApp()->selectionMgr() )
  {
    SUIT_DataOwnerPtrList aList( true );
    aSelectionMgr->selected( aList );

    SUIT_DataOwnerPtrList::iterator anIter;
    for( anIter = aList.begin(); anIter != aList.end(); anIter++ )
    {
      const LightApp_DataOwner* anOwner =
        dynamic_cast<const LightApp_DataOwner*>( (*anIter).operator->() );
      if( anOwner )
        anEntryList.append( anOwner->entry() );
    }
  }
  return anEntryList;
}

void HYDROGUI_Module::restoreSelection( const QStringList& theEntryList )
{
  DEBTRACE("HYDROGUI_Module::restoreSelection");
  if( LightApp_SelectionMgr* aSelectionMgr = getApp()->selectionMgr() )
  {
    SUIT_DataOwnerPtrList aList( true );
    for( int anIndex = 0, aSize = theEntryList.size(); anIndex < aSize; anIndex++ )
      aList.append( SUIT_DataOwnerPtr( new LightApp_DataOwner( theEntryList[ anIndex ] ) ) );
    aSelectionMgr->setSelected( aList );
  }
}

void HYDROGUI_Module::onMouseMove( SUIT_ViewWindow* theViewWindow, QMouseEvent* )
{
  double X, Y, Z;
  bool doShow = false;
  HYDROGUI_Displayer* aDisplayer = getDisplayer();
  if ( aDisplayer )
    aDisplayer->SaveCursorViewPosition( theViewWindow );
    doShow = aDisplayer->GetCursorViewCoordinates( theViewWindow, X, Y, Z );

  if ( doShow )
  {
    // Show the coordinates in the status bar
    SUIT_Desktop* aDesktop = getApp()->desktop();
    if ( aDesktop && aDesktop->statusBar() )
    {
      gp_Pnt aWPnt( X, Y, Z );
      HYDROData_Document::Document()->Transform( aWPnt, false );
      double WX = aWPnt.X(), WY = aWPnt.Y();

      QString aXStr = HYDROGUI_Tool::GetCoordinateString( X, true );
      QString anYStr = HYDROGUI_Tool::GetCoordinateString( Y, true );
      QString aWXStr = HYDROGUI_Tool::GetCoordinateString( WX, true );
      QString aWYStr = HYDROGUI_Tool::GetCoordinateString( WY, true );
      QString aMsg = tr( "COORDINATES_INFO" );
      aMsg = aMsg.arg( aXStr ).arg( anYStr ).arg( aWXStr ).arg( aWYStr );
      aDesktop->statusBar()->showMessage( aMsg );
    }
  }
}

/**
 * Returns stack of active operations;
 */
QStack<HYDROGUI_Operation*>& HYDROGUI_Module::getActiveOperations()
{
  DEBTRACE("HYDROGUI_Module::getActiveOperations");
  return myActiveOperationMap;
}

/**
 * Returns the module active operation. If the active operation is show/hide,
 * the method returns the previous operation if it is.
 */
HYDROGUI_Operation* HYDROGUI_Module::activeOperation()
{
  HYDROGUI_Operation* anOp = !myActiveOperationMap.empty() ? myActiveOperationMap.top() : 0;

  if ( dynamic_cast<HYDROGUI_ShowHideOp*>( anOp ) )
  {
    QVectorIterator<HYDROGUI_Operation*> aVIt( myActiveOperationMap );
    aVIt.toBack();
    aVIt.previous(); // skip the top show/hide operation
    anOp = aVIt.hasPrevious() ? aVIt.previous() : 0;
  }

  return anOp;
}

/*!
 * \brief Virtual public slot
 *
 * This method is called after the object inserted into data view to update their visibility state
 * This is default implementation
 */
void HYDROGUI_Module::onObjectClicked( SUIT_DataObject* theObject, int theColumn )
{
  if ( !isActiveModule() )
      return;

  HYDROGUI_DataObject* hydroObject = dynamic_cast<HYDROGUI_DataObject*>( theObject );

  // change visibility of object
  if ( !hydroObject || theColumn != SUIT_DataObject::VisibilityId )
      return;

  SUIT_AbstractModel* treeModel = dynamic_cast<SUIT_AbstractModel*>( getApp()->objectBrowser()->model() );

  QString id = theObject->text( theObject->customData( Qtx::IdType ).toInt() );
  Qtx::VisibilityState visState = treeModel->visibilityState( id );
  if ( visState == Qtx::UnpresentableState )
      return;

  visState = visState == Qtx::ShownState ? Qtx::HiddenState : Qtx::ShownState;
  treeModel->setVisibilityState( id, visState );

  bool vis = visState == Qtx::ShownState;
  if ( vis == isObjectVisible( HYDROGUI_Tool::GetActiveViewId( this ), hydroObject->modelObject() ) )
      return;

  setObjectVisible( HYDROGUI_Tool::GetActiveViewId( this ), hydroObject->modelObject(), vis );

  update( UF_OCCViewer | UF_VTKViewer | ( visState == Qtx::ShownState ? UF_FitAll : 0 ) );
}

bool HYDROGUI_Module::isDraggable( const SUIT_DataObject* what ) const
{
  return true;
}

bool HYDROGUI_Module::isDropAccepted( const SUIT_DataObject* where ) const
{

  return true;
}

void HYDROGUI_Module::dropObjects( const DataObjectList& what, SUIT_DataObject* where,
                                   const int row, Qt::DropAction action )
{
  if ( action != Qt::CopyAction && action != Qt::MoveAction )
    return;

  if (row == -1)
    return;

  if (where->level() < 2 )
    return;

  DataObjectList::ConstIterator it = what.constBegin();
  for (;it != what.constEnd();++it)
  {
    if ((*it)->parent() != where)
      return;
  }

  it = what.constBegin();

  int i=0;
  for (;it != what.constEnd();++it)
  {
    SUIT_DataObject* objWhat = *it;

    DataObjectList objInSect = where->children();

    //std::list<SUIT_DataObject*> t1 = where->children().toStdList(); //debug
    int ind = objInSect.indexOf(objWhat);
    if (ind != -1)
    {
      HYDROGUI_DataModel* aModel = getDataModel();
      int pos = -1;
      if (ind >= row)
      {
        pos = row + i;
        i++;
      }
      else
        pos = row - 1;
      where->moveChildPos(objWhat, pos);
      //std::list<SUIT_DataObject*> t2 = where->children().toStdList(); //debug
    }
  }

  getApp()->updateObjectBrowser(true);

}

Handle(HYDROData_StricklerTable) HYDROGUI_Module::getLandCoverColoringTable( const size_t theViewId ) const
{
  Handle(HYDROData_StricklerTable) aTable;

  if ( myLandCoverColoringMap.contains( theViewId ) ) {
    aTable = myLandCoverColoringMap.value( theViewId );
  }

  return aTable;
}

void HYDROGUI_Module::setLandCoverColoringTable( const size_t theViewId,
                                                 const Handle(HYDROData_StricklerTable)& theTable )
{
  if ( !theTable.IsNull() && theViewId ) {
    myLandCoverColoringMap.insert( theViewId, theTable );
  }
}

void HYDROGUI_Module::setLandCoversScalarMapModeOff( const size_t theViewId )
{
  myLandCoverColoringMap.remove( theViewId );
}

bool HYDROGUI_Module::isLandCoversScalarMapModeOn( const size_t theViewId ) const
{
  return myLandCoverColoringMap.contains( theViewId );
}

void HYDROGUI_Module::setObjectRemoved( const Handle(HYDROData_Entity)& theObject )
{
  if ( theObject.IsNull() || !theObject->IsRemoved() ) {
    return;
  }

  if ( theObject->GetKind() == KIND_STRICKLER_TABLE ) {
    Handle(HYDROData_StricklerTable) aTable =
      Handle(HYDROData_StricklerTable)::DownCast( theObject );
    QMutableMapIterator<size_t, Handle(HYDROData_StricklerTable)> anIter( myLandCoverColoringMap );
    while ( anIter.hasNext() ) {
      if ( HYDROGUI_DataObject::dataObjectEntry( anIter.next().value() ) ==
           HYDROGUI_DataObject::dataObjectEntry( aTable ) ) {
        anIter.remove();
      }
    }
  }
}

void HYDROGUI_Module::onViewActivated( SUIT_ViewManager* theMgr )
{
  DEBTRACE("HYDROGUI_Module::onViewActivated");
  if( !theMgr )
    return;

  SUIT_ViewWindow* wnd = theMgr->getActiveView();
  OCCViewer_ViewFrame* occwnd = dynamic_cast<OCCViewer_ViewFrame*>( wnd );
  if( !occwnd )
    return;

  myOverview->setMainView( occwnd );
}

void HYDROGUI_Module::setAutoZoomToAllViewManagers(bool bAutoZoom)
{
  ViewManagerList aViewManagers = getApp()->viewManagers();
  foreach (SUIT_ViewManager* aVMgr, aViewManagers)
    setAutoZoom(aVMgr, bAutoZoom);
}

void HYDROGUI_Module::setAutoZoom(SUIT_ViewManager* aVMgr, bool bAutoZoom)
{
  if (!aVMgr)
    return;
  QVector<SUIT_ViewWindow*> aViews = aVMgr->getViews();
  foreach (SUIT_ViewWindow* aView, aViews)
  {
    if (aView)
    {
      OCCViewer_ViewFrame* anOCCViewFrame = dynamic_cast<OCCViewer_ViewFrame*>( aView );
      if (anOCCViewFrame)
      {
        anOCCViewFrame->setAutomaticZoom(bAutoZoom);
        for (int i = OCCViewer_ViewFrame::MAIN_VIEW; i<=OCCViewer_ViewFrame::TOP_RIGHT; i++)
        {
          OCCViewer_ViewWindow* aV = anOCCViewFrame->getView(i);
          if (aV)
            aV->setAutomaticZoom(bAutoZoom);
        }
      }
      OCCViewer_ViewWindow* anOCCViewWindow = dynamic_cast<OCCViewer_ViewWindow*>( aView );
      if (anOCCViewWindow)
        anOCCViewWindow->setAutomaticZoom(bAutoZoom);
    }
  }
}


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_RIVERBOTTOMDLG_H
#define HYDROGUI_RIVERBOTTOMDLG_H

#include "HYDROGUI_InputPanel.h"

#include "HYDROGUI_ObjComboBox.h"

class QComboBox;

class HYDROGUI_RiverBottomDlg : public HYDROGUI_InputPanel, public HYDROGUI_ObjComboBoxFilter
{
  Q_OBJECT

public:
  HYDROGUI_RiverBottomDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_RiverBottomDlg();

  void                       reset();

  QString                    getRiverName() const;
  void                       setRiverName( const QString& );

  virtual bool               isOk( const Handle(HYDROData_Entity)& ) const;

private slots:
  void                       onRiverChanged( const QString& );

private:
  void                       updateState();

private:
  HYDROGUI_ObjComboBox*      myRivers;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_StreamDlg.h"
#include "HYDROGUI_ListSelector.h"
#include "HYDROGUI_OrderedListWidget.h"
#include <HYDROData_Profile.h>

#ifndef LIGHT_MODE
  #include "HYDROGUI_Module.h"
  #include "HYDROGUI_Tool2.h"
  #include <LightApp_Application.h>
  #include <LightApp_SelectionMgr.h>
#endif

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QTextEdit>
#include <QRadioButton>

HYDROGUI_StreamDlg::HYDROGUI_StreamDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  // Stream name
  myObjectNameGroup = new QGroupBox( tr( "STREAM_NAME" ), mainFrame() );

  myObjectName = new QLineEdit( myObjectNameGroup );

  QBoxLayout* aNameLayout = new QHBoxLayout( myObjectNameGroup );
  aNameLayout->setMargin( 5 );
  aNameLayout->setSpacing( 5 );
  aNameLayout->addWidget( new QLabel( tr( "NAME" ), myObjectNameGroup ) );
  aNameLayout->addWidget( myObjectName );

  // Stream parameters
  QGroupBox* aParamGroup = new QGroupBox( tr( "STREAM_PARAMETERS" ), mainFrame() );

  myAxes = new QComboBox( aParamGroup );
  myAxes->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myLeftBanks = new QComboBox( aParamGroup );
  myLeftBanks->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myRightBanks = new QComboBox( aParamGroup );
  myRightBanks->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myProfilePoints = new QSpinBox();
  myProfilePoints->setRange(3, 99999);
  myProfilePoints->setValue(100);

  myDDZ = new QDoubleSpinBox( aParamGroup );
  myDDZ->setRange( 0.01, 100 );
  myDDZ->setSingleStep( 0.1 );
  myDDZ->setValue( 0.1 );
  myDDZ->setDecimals( 2 );
  mySpatialStep = new QDoubleSpinBox( aParamGroup );
  mySpatialStep->setRange( 0.1, 9999 );
  mySpatialStep->setSingleStep( 0.1 );
  mySpatialStep->setValue( 1 );
  mySpatialStep->setDecimals( 2 );
  
  myModeButton = new QRadioButton( tr( "VIA_DTM" ), myObjectNameGroup );
  myModeButton->setAutoExclusive(true);
  myModeButton2 = new QRadioButton( tr( "VIA_LISM" ), myObjectNameGroup );
  myModeButton2->setAutoExclusive(true);
  myModeButton->setChecked(true);
  myModeButton2->setChecked(false);
  
  QGridLayout* aParam1Layout = new QGridLayout();

  aParam1Layout->addWidget( myModeButton, 0, 0 );
  aParam1Layout->addWidget( myModeButton2, 0, 1 );

  myDDZLabel = new QLabel( tr( "STREAM_DDZ" ) );
  aParam1Layout->setMargin( 0 );
  aParam1Layout->addWidget( new QLabel( tr( "STREAM_HYDRAULIC_AXIS" ) ), 1, 0 );
  aParam1Layout->addWidget( myAxes, 1, 1 );
  aParam1Layout->addWidget( myDDZLabel, 2, 0 );
  aParam1Layout->addWidget( myDDZ, 2, 1 );
  aParam1Layout->addWidget( new QLabel( tr( "STREAM_SPATIAL_STEP" ) ), 3, 0 );
  aParam1Layout->addWidget( mySpatialStep, 3, 1 );

  ///line. intr method widgets
  myLeftBanksLabel = new QLabel( tr( "STREAM_LEFT_BANK" ) );
  myRightBanksLabel = new QLabel( tr( "STREAM_RIGHT_BANK" ) );
  myProfilePointsLabel =  new QLabel( tr( "STREAM_PROFILE_POINTS" ) );

  aParam1Layout->addWidget( myLeftBanksLabel, 4, 0 );
  aParam1Layout->addWidget( myLeftBanks, 4, 1 );
  aParam1Layout->addWidget( myRightBanksLabel, 5, 0 );
  aParam1Layout->addWidget( myRightBanks, 5, 1 );
  aParam1Layout->addWidget( myProfilePointsLabel, 6, 0 );
  aParam1Layout->addWidget( myProfilePoints, 6, 1 );

  myProfiles = new HYDROGUI_OrderedListWidget( aParamGroup, 0 );
  myProfiles->setHiddenObjectsShown(true);
  myProfiles->setVisibilityIconShown(false);
  myProfiles->setContentsMargins(QMargins());
  myProfiles->setOrderingEnabled( false );
 
  myAddButton = new QPushButton( aParamGroup );
  myAddButton->setText( tr("ADD") );
  myRemoveButton = new QPushButton( aParamGroup );
  myRemoveButton->setText( tr("REMOVE") );
  QBoxLayout* aButtonsLayout = new QHBoxLayout();
  aButtonsLayout->addWidget( myAddButton );
  aButtonsLayout->addWidget( myRemoveButton );
  aButtonsLayout->addStretch();
 
  QBoxLayout* aParamLayout = new QVBoxLayout();
  aParamLayout->setMargin( 5 );
  aParamLayout->setSpacing( 5 );
  aParamLayout->addLayout( aParam1Layout );
  aParamLayout->addWidget( myProfiles );
  aParamLayout->addLayout( aButtonsLayout );
  
  aParamGroup->setLayout( aParamLayout );

  // Warnings
  QGroupBox* aWarnGroup = new QGroupBox( tr( "STREAM_WARNINGS" ), mainFrame() );
  myWarnText = new QTextEdit(); 
  myWarnText->setReadOnly(true);
  QBoxLayout* aWarnLayout = new QVBoxLayout();
  aWarnLayout->addWidget( myWarnText );  
  aWarnGroup->setLayout( aWarnLayout );

  // Common
  addWidget( myObjectNameGroup );
  addWidget( aParamGroup );
  addWidget( aWarnGroup );
  addStretch();

  // Create selector
  if ( module() ) {
    HYDROGUI_ListSelector* aListSelector = 
#ifdef LIGHT_MODE
      new HYDROGUI_ListSelector( myProfiles, 0 );
#else
      new HYDROGUI_ListSelector( myProfiles, module()->getApp()->selectionMgr() );
#endif
    aListSelector->setAutoBlock( true );
  }

  // Connect signals and slots
  connect( myAxes, SIGNAL( currentIndexChanged( const QString & ) ), this, SIGNAL( AxisChanged( const QString& ) ) );
  connect( myAddButton, SIGNAL( clicked() ), this, SIGNAL( AddProfiles() ) );
  connect( myRemoveButton, SIGNAL( clicked() ), this, SLOT( onRemoveProfiles() ) );
  connect( myDDZ, SIGNAL( valueChanged (double) ), this, SLOT (onDDZValueChanged(double)));
  connect( mySpatialStep, SIGNAL( valueChanged (double) ), this, SLOT (onSSValueChanged(double)));
  ///
  connect( myLeftBanks, SIGNAL( currentIndexChanged( const QString & ) ), this, SIGNAL( LeftBankChanged( const QString& ) ) );
  connect( myRightBanks, SIGNAL( currentIndexChanged( const QString & ) ), this, SIGNAL( RightBankChanged( const QString& ) ) );
  connect( myProfilePoints, SIGNAL( valueChanged (int) ), this, SIGNAL (ProfilePointsChanged(int)));

  connect( myModeButton, SIGNAL( toggled( bool ) ), this, SIGNAL( ModeChanged( bool ) ) );
  connect( myModeButton, SIGNAL( toggled( bool ) ), this, SLOT( ModeChangedDlg( bool ) ) );

  ModeChangedDlg(true);

  //myModeButton->setChecked(false);
}

HYDROGUI_StreamDlg::~HYDROGUI_StreamDlg()
{
}

void HYDROGUI_StreamDlg::reset()
{
  bool isBlocked = blockSignals( true );

  myObjectName->clear();

  myAxes->clear();
  myLeftBanks->clear();
  myRightBanks->clear();

  myProfiles->setObjects( HYDROGUI_ListModel::Object2VisibleList() );

  myAddButton->setEnabled( true );
  myRemoveButton->setEnabled( true );

  blockSignals( isBlocked );
}

QList<Handle(HYDROData_Entity)> HYDROGUI_StreamDlg::getProfiles()
{
  return myProfiles->getObjects();
}

void HYDROGUI_StreamDlg::setObjectName( const QString& theName )
{
  myObjectName->setText( theName );
}

QString HYDROGUI_StreamDlg::getObjectName() const
{
  return myObjectName->text();
}

void HYDROGUI_StreamDlg::setAxisNames( const QStringList& theAxises )
{
  bool isBlocked = blockSignals( true );

  myAxes->clear();
  myAxes->addItems( theAxises );

  blockSignals( isBlocked );
}

void HYDROGUI_StreamDlg::setLeftBankNames( const QStringList& theBanks )
{
  bool isBlocked = blockSignals( true );

  myLeftBanks->clear();
  myLeftBanks->addItems( theBanks );

  blockSignals( isBlocked );
}

void HYDROGUI_StreamDlg::setRightBankNames( const QStringList& theBanks )
{
  bool isBlocked = blockSignals( true );

  myRightBanks->clear();
  myRightBanks->addItems( theBanks );

  blockSignals( isBlocked );
}

void HYDROGUI_StreamDlg::setAxisName( const QString& theName )
{
  bool isBlocked = blockSignals( true );

  int aNewId = myAxes->findText( theName );
  if ( aNewId != myAxes->currentIndex() ) {
    myAxes->setCurrentIndex( aNewId );
  }
  //myAddButton->setEnabled( myAxes->currentIndex() > -1 );

  blockSignals( isBlocked );
}

void HYDROGUI_StreamDlg::setLeftBankName( const QString& theName )
{
  bool isBlocked = blockSignals( true );

  int aNewId = myLeftBanks->findText( theName );
  if ( aNewId != myLeftBanks->currentIndex() ) {
    myLeftBanks->setCurrentIndex( aNewId );
  }
  //myAddButton->setEnabled( myAxes->currentIndex() > -1 );

  blockSignals( isBlocked );
}

void HYDROGUI_StreamDlg::setRightBankName( const QString& theName )
{
  bool isBlocked = blockSignals( true );

  int aNewId = myRightBanks->findText( theName );
  if ( aNewId != myRightBanks->currentIndex() ) {
    myRightBanks->setCurrentIndex( aNewId );
  }
  //myAddButton->setEnabled( myAxes->currentIndex() > -1 );

  blockSignals( isBlocked );
}


QString HYDROGUI_StreamDlg::getAxisName() const
{
  return myAxes->currentText();
}

QString HYDROGUI_StreamDlg::getLeftBankName() const
{
  return myLeftBanks->currentText();
}

QString HYDROGUI_StreamDlg::getRightBankName() const
{
  return myRightBanks->currentText();
}

void HYDROGUI_StreamDlg::setProfiles( const QStringList& theProfiles )
{
  bool isBlocked = blockSignals( true );

  myProfiles->setUpdatesEnabled( false );
  
  HYDROGUI_ListModel::Object2VisibleList aProfiles;
#ifndef LIGHT_MODE
  foreach ( const QString& aProfileName, theProfiles )
  {
    Handle(HYDROData_Profile) anObject = Handle(HYDROData_Profile)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aProfileName ) );
    if ( !anObject.IsNull() )
    {
      aProfiles.append( HYDROGUI_ListModel::Object2Visible( anObject, true ) );
    }
  }
#endif

  myProfiles->setObjects( aProfiles );


  myRemoveButton->setEnabled( myProfiles->getObjects().count() > 0 );

  myProfiles->setUpdatesEnabled( true );

  blockSignals( isBlocked );
}

void HYDROGUI_StreamDlg::onRemoveProfiles()
{
  QStringList aSelectedProfiles = myProfiles->getSelectedNames();

  emit RemoveProfiles( aSelectedProfiles );
}

void HYDROGUI_StreamDlg::onDDZValueChanged(double d)
{
  emit DDZValueChanged( d );
}

void HYDROGUI_StreamDlg::onSSValueChanged(double d)
{
  emit SSValueChanged( d );
}


void HYDROGUI_StreamDlg::setDDZ( const double theDDZ )
{
  myDDZ->setValue( theDDZ );
}

double HYDROGUI_StreamDlg::getDDZ() const
{
  return myDDZ->value();
}

void HYDROGUI_StreamDlg::setSpatialStep( const double theSpatialStep )
{
  mySpatialStep->setValue( theSpatialStep );
}

double HYDROGUI_StreamDlg::getSpatialStep() const
{
  return mySpatialStep->value();
}

void HYDROGUI_StreamDlg::setNbProfilePoints( const int theNbPoints )
{
  myProfilePoints->setValue( theNbPoints );
}

int HYDROGUI_StreamDlg::getNbProfilePoints() const
{
  return myProfilePoints->value();
}


void HYDROGUI_StreamDlg::setBackgroundColorForProfileList (int theInd, QColor theColor)
{
  myProfiles->setBackgroundColor(theInd, theColor);
}

void HYDROGUI_StreamDlg::setBackgroundColorForProfileList (QString name, QColor theColor)
{
  myProfiles->setBackgroundColor(name, theColor);
}

QColor HYDROGUI_StreamDlg::getBackgroundColorForProfileList (int theInd) const
{
  return myProfiles->getBackgroundColor(theInd);
}

void HYDROGUI_StreamDlg::clearAllBackgroundColorsForProfileList ()
{
  myProfiles->clearAllBackgroundColors();
}

void HYDROGUI_StreamDlg::addWarning( const QString& theWarnMess )
{
  myWarnText->append( theWarnMess );
}

void HYDROGUI_StreamDlg::clearWarnings()
{
  myWarnText->clear();
}


void HYDROGUI_StreamDlg::ModeChangedDlg(bool mode)
{
  //if ( signalsBlocked() )
  //  return;
  if (mode)
  {
    myProfilePoints->setVisible(false);
    myProfilePointsLabel->setVisible(false);

    myLeftBanks->setVisible(false);
    myLeftBanksLabel->setVisible(false);

    myRightBanks->setVisible(false);
    myRightBanksLabel->setVisible(false);

    myDDZ->setVisible(true);
    myDDZLabel->setVisible(true);
  }
  else
  {
    myProfilePoints->setVisible(true);
    myProfilePointsLabel->setVisible(true);

    myLeftBanks->setVisible(true);
    myLeftBanksLabel->setVisible(true);

    myRightBanks->setVisible(true);
    myRightBanksLabel->setVisible(true);

    myDDZ->setVisible(false);
    myDDZLabel->setVisible(false);
  }

}

int HYDROGUI_StreamDlg::getMode()
{
  bool isCh = myModeButton->isChecked();
  return isCh ? 0 : 1;
}

void HYDROGUI_StreamDlg::setMode(int mode)
{
  bool checkState = mode == 0;
  myModeButton->setChecked(checkState);
  myModeButton2->setChecked(!checkState);
}




// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportImageDlg.h"

#include "HYDROGUI_PrsImage.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Module.h"

#include <HYDROData_Lambert93.h>
#include <HYDROData_Image.h>

#include <LightApp_Application.h>
#include <SUIT_FileDlg.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Session.h>

#include <QtxDoubleSpinBox.h>
#include <QtxIntSpinBox.h>

#include <QButtonGroup>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QPainter>
#include <QPicture>
#include <QPushButton>
#include <QRadioButton>
#include <QToolButton>
#include <QCheckBox>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_ImportImageDlg::HYDROGUI_ImportImageDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ),
  myIsInitialized( false )
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();

  // Import image from file
  myFileNameGroup = new QGroupBox( tr( "IMPORT_IMAGE_FROM_FILE" ), this );

  QLabel* aFileNameLabel = new QLabel( tr( "FILE_NAME" ), myFileNameGroup );

  myFileName = new QLineEdit( myFileNameGroup );
  myFileName->setReadOnly( true );

  myBrowseBtn = new QToolButton( myFileNameGroup );
  myBrowseBtn->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "BROWSE_ICO" ) ) );

  QBoxLayout* aFileNameLayout = new QHBoxLayout( myFileNameGroup );
  aFileNameLayout->setMargin( 5 );
  aFileNameLayout->setSpacing( 5 );
  aFileNameLayout->addWidget( aFileNameLabel );
  aFileNameLayout->addWidget( myFileName );
  aFileNameLayout->addWidget( myBrowseBtn );

  // Image name
  myImageNameGroup = new QGroupBox( tr( "IMAGE_NAME" ), this );

  QLabel* anImageNameLabel = new QLabel( tr( "NAME" ), myImageNameGroup );
  myImageName = new QLineEdit( myImageNameGroup );

  QBoxLayout* anImageNameLayout = new QHBoxLayout( myImageNameGroup );
  anImageNameLayout->setMargin( 5 );
  anImageNameLayout->setSpacing( 5 );
  anImageNameLayout->addWidget( anImageNameLabel );
  anImageNameLayout->addWidget( myImageName );

  // Transform image
  myTransformGroup = new QGroupBox( tr( "TRANSFORM_IMAGE" ), this );

  // Make a pixmap for arrow
  QPixmap anArrowPixmap = aResMgr->loadPixmap( "HYDRO", tr( "ARROW_RIGHT_ICO" ) );

  QPicture anArrowPicture;
  anArrowPicture.setBoundingRect( QRect( QPoint( 0, 0 ), anArrowPixmap.size() ) );

  QPainter aPainter;
  aPainter.begin( &anArrowPicture );
  aPainter.drawPixmap( 0, 0, anArrowPixmap );
  aPainter.end();

  QRadioButton* aManualGeodesicBtn = new QRadioButton( tr( "MANUALLY_GEODESIC" ), myTransformGroup );
  QRadioButton* aManualCartesianBtn = new QRadioButton( tr( "MANUALLY_LAMBERT93" ), myTransformGroup );
  QRadioButton* aCartesianFromFileBtn = new QRadioButton( tr( "LAMBERT93_FROM_FILE" ), myTransformGroup );
  QRadioButton* aRefImageBtn = new QRadioButton( tr( "BY_REFERENCE_IMAGE" ), myTransformGroup );

  myModeGroup = new QButtonGroup( myTransformGroup );
  myModeGroup->addButton( aManualGeodesicBtn, HYDROData_Image::ManualGeodesic );
  myModeGroup->addButton( aManualCartesianBtn, HYDROData_Image::ManualCartesian );
  myModeGroup->addButton( aCartesianFromFileBtn, HYDROData_Image::CartesianFromFile );
  myModeGroup->addButton( aRefImageBtn, HYDROData_Image::ReferenceImage );

  myRefImage = new QComboBox( myTransformGroup );

  QVBoxLayout* aTransformLayout = new QVBoxLayout( myTransformGroup );
  aTransformLayout->setMargin( 5 );
  aTransformLayout->setSpacing( 5 );

  QGridLayout* aModeLayout = new QGridLayout( myTransformGroup );
  aModeLayout->setMargin( 0 );
  aModeLayout->setSpacing( 5 );
  aModeLayout->addWidget( aManualGeodesicBtn,   0, 0 );
  aModeLayout->addWidget( aManualCartesianBtn, 1, 0 );
  aModeLayout->addWidget( aCartesianFromFileBtn, 2, 0 );
  aModeLayout->addWidget( aRefImageBtn,        3, 0 );
  aModeLayout->addWidget( myRefImage,          3, 1 );
  aModeLayout->setColumnStretch( 1, 1 );

  aTransformLayout->addLayout( aModeLayout );

  // Manual input widget
  QWidget* aManualInputGroup = new QWidget( myTransformGroup );
  QGridLayout* aManualInputLayout = new QGridLayout( aManualInputGroup );
  aManualInputLayout->setMargin( 5 );
  aManualInputLayout->setSpacing( 5 );

  QLabel* aImageCSLabel = new QLabel( tr( "IMAGE_CS" ), aManualInputGroup );
  QLabel* aGeodesicLabel = new QLabel( tr( "GEODESIC" ), aManualInputGroup );
  QLabel* aLambertLabel = new QLabel( tr( "LAMBERT93" ), aManualInputGroup );
  QLabel* aRefImageLabel = new QLabel( tr( "REFERENCE_IMAGE_CS" ), aManualInputGroup );

  aManualInputLayout->addWidget( aImageCSLabel,  0, 1 );
  aManualInputLayout->addWidget( aGeodesicLabel, 0, 2, 1, 6 );
  aManualInputLayout->addWidget( aLambertLabel,  0, 2, 1, 6 );
  aManualInputLayout->addWidget( aRefImageLabel, 0, 2, 1, 6 );

  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    QString aPointStr;
    switch( aPointType )
    {
      case HYDROGUI_PrsImage::PointA: aPointStr = tr( "ACTIVATE_POINT_A_SELECTION" ); break;
      case HYDROGUI_PrsImage::PointB: aPointStr = tr( "ACTIVATE_POINT_B_SELECTION" ); break;
      case HYDROGUI_PrsImage::PointC: aPointStr = tr( "ACTIVATE_POINT_C_SELECTION" ); break;
    }
    QPushButton* aPointBtn = new QPushButton( aPointStr, myTransformGroup );
    aPointBtn->setCheckable( true );

    QLabel* aPointXLabel = new QLabel( "X", aManualInputGroup );
    QLabel* aPointYLabel = new QLabel( "Y", aManualInputGroup );

    QtxIntSpinBox* aPointX = new QtxIntSpinBox( 0, 0, 1, aManualInputGroup ); // max is updated later
    QtxIntSpinBox* aPointY = new QtxIntSpinBox( 0, 0, 1, aManualInputGroup ); // max is updated later

    QLabel* aPointArrowLabel = new QLabel( aManualInputGroup );
    aPointArrowLabel->setPicture( anArrowPicture );

    QLabel* aPointXDegLabel = new QLabel( QChar( 0x00B0 ), aManualInputGroup );
    QLabel* aPointYDegLabel = new QLabel( QChar( 0x00B0 ), aManualInputGroup );
    QLabel* aPointXMinLabel = new QLabel( "'", aManualInputGroup );
    QLabel* aPointYMinLabel = new QLabel( "'", aManualInputGroup );
    QLabel* aPointXSecLabel = new QLabel( "\"", aManualInputGroup );
    QLabel* aPointYSecLabel = new QLabel( "\"", aManualInputGroup );
    QLabel* aPointLatLabel = new QLabel( tr( "POINT_LATITUDE" ), aManualInputGroup );
    QLabel* aPointLonLabel = new QLabel( tr( "POINT_LONGITUDE" ), aManualInputGroup );
    myGeodesicLabels << aPointXDegLabel << aPointYDegLabel
                    << aPointXMinLabel << aPointYMinLabel
                    << aPointXSecLabel << aPointYSecLabel
                    << aPointLonLabel << aPointLatLabel;

    QtxIntSpinBox* aPointXDeg = new QtxIntSpinBox( -179, 179, 1, aManualInputGroup );
    QtxIntSpinBox* aPointYDeg = new QtxIntSpinBox( 0, 89, 1, aManualInputGroup );
    QtxIntSpinBox* aPointXMin = new QtxIntSpinBox( 0, 59, 1, aManualInputGroup );
    QtxIntSpinBox* aPointYMin = new QtxIntSpinBox( 0, 59, 1, aManualInputGroup );
    QtxDoubleSpinBox* aPointXSec = new QtxDoubleSpinBox( 0, 59.9999, 1, 4, 4, aManualInputGroup );
    QtxDoubleSpinBox* aPointYSec = new QtxDoubleSpinBox( 0, 59.9999, 1, 4, 4, aManualInputGroup );

    QtxDoubleSpinBox* aCartPointX = new QtxDoubleSpinBox( 0, 1e8, 1, 2, 2, aManualInputGroup );
    QtxDoubleSpinBox* aCartPointY = new QtxDoubleSpinBox( 0, 1e8, 1, 2, 2, aManualInputGroup );

    QtxIntSpinBox* aRefPointX = new QtxIntSpinBox( 0, 0, 1, aManualInputGroup ); // max is updated later
    QtxIntSpinBox* aRefPointY = new QtxIntSpinBox( 0, 0, 1, aManualInputGroup ); // max is updated later

    int aRow = 4 * aPointType;
    if ( aPointType == HYDROGUI_PrsImage::PointC )
    {
      myPointCEnabler = new QCheckBox( aManualInputGroup );
      aManualInputLayout->addWidget( myPointCEnabler, aRow,    0, 1, 2, Qt::AlignHCenter );
      aManualInputLayout->addWidget( aPointBtn,       aRow,    2, 1, 8 );
    }
    else
    {
      aManualInputLayout->addWidget( aPointBtn,       aRow,    0, 1, 10 );
    }

    aManualInputLayout->addWidget( aPointXLabel,     aRow + 1, 0 );
    aManualInputLayout->addWidget( aPointX,          aRow + 1, 1 );
    aManualInputLayout->addWidget( aPointArrowLabel, aRow + 1, 2, 2, 1 );
    aManualInputLayout->addWidget( aPointXDeg,       aRow + 1, 3 );
    aManualInputLayout->addWidget( aPointXDegLabel,  aRow + 1, 4 );
    aManualInputLayout->addWidget( aPointXMin,       aRow + 1, 5 );
    aManualInputLayout->addWidget( aPointXMinLabel,  aRow + 1, 6 );
    aManualInputLayout->addWidget( aPointXSec,       aRow + 1, 7 );
    aManualInputLayout->addWidget( aPointXSecLabel,  aRow + 1, 8 );
    aManualInputLayout->addWidget( aPointLonLabel,   aRow + 1, 9 );

    aManualInputLayout->addWidget( aCartPointX,      aRow + 1, 3, 1, 6 );
    aManualInputLayout->addWidget( aRefPointX,       aRow + 1, 3, 1, 6 );

    aManualInputLayout->addWidget( aPointYLabel,     aRow + 2, 0 );
    aManualInputLayout->addWidget( aPointY,          aRow + 2, 1 );
    aManualInputLayout->addWidget( aPointYDeg,       aRow + 2, 3 );
    aManualInputLayout->addWidget( aPointYDegLabel,  aRow + 2, 4 );
    aManualInputLayout->addWidget( aPointYMin,       aRow + 2, 5 );
    aManualInputLayout->addWidget( aPointYMinLabel,  aRow + 2, 6 );
    aManualInputLayout->addWidget( aPointYSec,       aRow + 2, 7 );
    aManualInputLayout->addWidget( aPointYSecLabel,  aRow + 2, 8 );
    aManualInputLayout->addWidget( aPointLatLabel,   aRow + 2, 9 );

    aManualInputLayout->addWidget( aCartPointY,      aRow + 2, 3, 1, 6 );
    aManualInputLayout->addWidget( aRefPointY,       aRow + 2, 3, 1, 6 );

    if( aPointType != HYDROGUI_PrsImage::PointC )
    {
      QFrame* aLine = new QFrame( aManualInputGroup );
      aLine->setFrameShape( QFrame::HLine );
      aLine->setFrameShadow( QFrame::Sunken );
      aManualInputLayout->addWidget( aLine, aRow + 3, 0, 1, 10 );
    }

    myPointBtnMap[ aPointType ] = aPointBtn;
    myPointXMap[ aPointType ] = aPointX;
    myPointYMap[ aPointType ] = aPointY;
    myPointXDegMap[ aPointType ] = aPointXDeg;
    myPointYDegMap[ aPointType ] = aPointYDeg;
    myPointXMinMap[ aPointType ] = aPointXMin;
    myPointYMinMap[ aPointType ] = aPointYMin;
    myPointXSecMap[ aPointType ] = aPointXSec;
    myPointYSecMap[ aPointType ] = aPointYSec;

    myCartPointXMap[ aPointType ] = aCartPointX;
    myCartPointYMap[ aPointType ] = aCartPointY;

    myRefPointXMap[ aPointType ] = aRefPointX;
    myRefPointYMap[ aPointType ] = aRefPointY;

    connect( aPointBtn, SIGNAL( toggled( bool ) ), this, SLOT( onPointBtnToggled( bool ) ) );

    connect( aPointX, SIGNAL( valueChanged( int ) ), this, SLOT( onPointCoordChanged( int ) ) );
    connect( aPointY, SIGNAL( valueChanged( int ) ), this, SLOT( onPointCoordChanged( int ) ) );

    connect( aPointXDeg, SIGNAL( valueChanged( int ) ), this, SLOT( onGeodesicCoordChanged() ) );
    connect( aPointYDeg, SIGNAL( valueChanged( int ) ), this, SLOT( onGeodesicCoordChanged() ) );
    connect( aPointXMin, SIGNAL( valueChanged( int ) ), this, SLOT( onGeodesicCoordChanged() ) );
    connect( aPointYMin, SIGNAL( valueChanged( int ) ), this, SLOT( onGeodesicCoordChanged() ) );
    connect( aPointXSec, SIGNAL( valueChanged( double ) ), this, SLOT( onGeodesicCoordChanged() ) );
    connect( aPointYSec, SIGNAL( valueChanged( double ) ), this, SLOT( onGeodesicCoordChanged() ) );

    connect( aCartPointX, SIGNAL( valueChanged( double ) ), this, SLOT( onCartesianCoordChanged() ) );
    connect( aCartPointY, SIGNAL( valueChanged( double ) ), this, SLOT( onCartesianCoordChanged() ) );

    connect( aRefPointX, SIGNAL( valueChanged( int ) ), this, SLOT( onPointCoordChanged( int ) ) );
    connect( aRefPointY, SIGNAL( valueChanged( int ) ), this, SLOT( onPointCoordChanged( int ) ) );
  }

  // Connect checkbox enabling point C
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointBtnMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointXMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointYMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointXDegMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointYDegMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointXMinMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointYMinMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointXSecMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myPointYSecMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myCartPointXMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myCartPointYMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myRefPointXMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );
  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), myRefPointYMap[ HYDROGUI_PrsImage::PointC ], SLOT( setEnabled( bool ) ) );

  connect( myPointCEnabler, SIGNAL( toggled( bool ) ), SLOT( onSetCIsUsed( bool ) ) );

  aManualInputLayout->setColumnStretch( 1, 1 ); // double
  aManualInputLayout->setColumnStretch( 3, 1 ); // degrees
  aManualInputLayout->setColumnStretch( 5, 1 ); // minutes
  aManualInputLayout->setColumnStretch( 7, 2 ); // seconds (double with 4 digits)

  // Image georeferencement file widget
  QWidget* aFromFileInputGroup = new QWidget( myTransformGroup );
  QBoxLayout* aFromFileInputLayout = new QHBoxLayout( aFromFileInputGroup );
  aFromFileInputLayout->setMargin( 5 );
  aFromFileInputLayout->setSpacing( 5 );
 
  QLabel* aGeoFileNameLabel = new QLabel( tr( "FILE_NAME" ), aFromFileInputGroup );

  myGeoFileName = new QLineEdit( aFromFileInputGroup );
  myGeoFileName->setReadOnly( true );

  QToolButton* aGeoBrowseBtn = new QToolButton( aFromFileInputGroup );
  aGeoBrowseBtn->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "BROWSE_ICO" ) ) );

  aFromFileInputLayout->addWidget( aGeoFileNameLabel );
  aFromFileInputLayout->addWidget( myGeoFileName );
  aFromFileInputLayout->addWidget( aGeoBrowseBtn );

  // Widgets connections
  connect( aManualCartesianBtn, SIGNAL( toggled( bool ) ), aLambertLabel, SLOT( setVisible ( bool ) ) );
  connect( aManualCartesianBtn, SIGNAL( toggled( bool ) ), aGeodesicLabel, SLOT( setHidden ( bool ) ) );
  connect( aManualCartesianBtn, SIGNAL( toggled( bool ) ), aRefImageLabel, SLOT( setHidden ( bool ) ) );
  connect( aManualCartesianBtn, SIGNAL( toggled( bool ) ), aFromFileInputGroup, SLOT( setHidden ( bool ) ) );
  connect( aManualGeodesicBtn, SIGNAL( toggled( bool ) ), aGeodesicLabel, SLOT( setVisible ( bool ) ) );
  connect( aManualGeodesicBtn, SIGNAL( toggled( bool ) ), aLambertLabel, SLOT( setHidden ( bool ) ) );
  connect( aManualGeodesicBtn, SIGNAL( toggled( bool ) ), aRefImageLabel, SLOT( setHidden ( bool ) ) );
  connect( aManualGeodesicBtn, SIGNAL( toggled( bool ) ), aFromFileInputGroup, SLOT( setHidden ( bool ) ) );
  connect( aRefImageBtn, SIGNAL( toggled( bool ) ), aRefImageLabel, SLOT( setVisible ( bool ) ) );
  connect( aRefImageBtn, SIGNAL( toggled( bool ) ), aGeodesicLabel, SLOT( setHidden ( bool ) ) );
  connect( aRefImageBtn, SIGNAL( toggled( bool ) ), aLambertLabel, SLOT( setHidden ( bool ) ) );
  connect( aRefImageBtn, SIGNAL( toggled( bool ) ), aFromFileInputGroup, SLOT( setHidden ( bool ) ) );
  connect( aCartesianFromFileBtn, SIGNAL( toggled( bool ) ), aFromFileInputGroup, SLOT( setVisible ( bool ) ) );
  connect( aCartesianFromFileBtn, SIGNAL( toggled( bool ) ), aManualInputGroup, SLOT( setHidden ( bool ) ) );

  // Input widgets
  aTransformLayout->addWidget( aManualInputGroup );
  aTransformLayout->addWidget( aFromFileInputGroup );

  // Common
  addWidget( myFileNameGroup );
  addWidget( myImageNameGroup );
  addWidget( myTransformGroup );
  addStretch();

  connect( myBrowseBtn, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );

  connect( myModeGroup, SIGNAL( buttonClicked( int ) ),
           this, SLOT( onModeActivated( int ) ) );

  connect( aGeoBrowseBtn, SIGNAL( clicked() ), this, SLOT( onGeoBrowse() ) );

  connect( myRefImage, SIGNAL( activated( const QString& ) ),
           this, SLOT( onRefImageActivated( const QString& ) ) );

  //setTransformationMode( HYDROData_Image::ManualCartesian );

  setMinimumWidth( 350 );
}

HYDROGUI_ImportImageDlg::~HYDROGUI_ImportImageDlg()
{
}

void HYDROGUI_ImportImageDlg::onSetCIsUsed( bool theCIsUsed )
{
  if ( !theCIsUsed  && myPointBtnMap[ HYDROGUI_PrsImage::PointC ]->isChecked() )
  {
    // Turn on point A selection if point C selection has been activated and we disable point C.
    myPointBtnMap[ HYDROGUI_PrsImage::PointA ]->toggle();
  }
  emit setCIsUsed( theCIsUsed );
}

void HYDROGUI_ImportImageDlg::setIsEdit( const bool theIsEdit )
{
  myFileNameGroup->setVisible( !theIsEdit );
  myImageNameGroup->setEnabled( theIsEdit );
  myTransformGroup->setEnabled( theIsEdit );
}

void HYDROGUI_ImportImageDlg::reset()
{
  myFileName->clear();
  myImageName->clear();
  myImageNameGroup->setEnabled( false );
  myGeoFileName->clear();
  bool isPBlocked = blockSignalsPoints( true );
  bool isGBlocked = blockSignalsGeodesic( true );
  bool isCBlocked = blockSignalsCartesian( true );
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    QPushButton* aBtn = myPointBtnMap[ aPointType ];
    bool anIsBlocked = aBtn->blockSignals( true );
    aBtn->setChecked( false );
    aBtn->blockSignals( anIsBlocked );

    clearSpins( myPointXMap[ aPointType ] );
    clearSpins( myPointYMap[ aPointType ] );
    clearSpins( myPointXDegMap[ aPointType ] );
    clearSpins( myPointYDegMap[ aPointType ] );
    clearSpins( myPointXMinMap[ aPointType ] );
    clearSpins( myPointYMinMap[ aPointType ] );
    clearSpins( myPointXSecMap[ aPointType ] );
    clearSpins( myPointYSecMap[ aPointType ] );

    clearSpins( myRefPointXMap[ aPointType ] );
    clearSpins( myRefPointYMap[ aPointType ] );

    clearSpins( myCartPointXMap[ aPointType ] );
    clearSpins( myCartPointYMap[ aPointType ] );
  }
  blockSignalsPoints( isPBlocked );
  blockSignalsGeodesic( isGBlocked );
  blockSignalsCartesian( isCBlocked );
  
  // Emulate turning off C point usage
  myPointCEnabler->blockSignals( true );
  
  myPointCEnabler->setChecked( false );
  myPointBtnMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointXMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointYMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointXDegMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointYDegMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointXMinMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointYMinMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointXSecMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myPointYSecMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myCartPointXMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myCartPointYMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myRefPointXMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  myRefPointYMap[ HYDROGUI_PrsImage::PointC ]->setEnabled( false );
  onSetCIsUsed( false );

  myPointCEnabler->blockSignals( false );
  
  myTransformGroup->setEnabled( false );
  myRefImage->clear();

  setTransformationMode( HYDROData_Image::ManualCartesian );

  myPrsPointDataList.clear();

  myIsInitialized = false;
}

void HYDROGUI_ImportImageDlg::setImageName( const QString& theName )
{
  myImageName->setText( theName );
}

QString HYDROGUI_ImportImageDlg::getImageName() const
{
  return myImageName->text();
}

void HYDROGUI_ImportImageDlg::setRefImageName( const QString& theName )
{
  myRefImage->setCurrentIndex( myRefImage->findText( theName ) );
}

QString HYDROGUI_ImportImageDlg::getRefImageName() const
{
  return myRefImage->currentText();
}

QString HYDROGUI_ImportImageDlg::getFileName() const
{
  return myFileName->text();
}

void HYDROGUI_ImportImageDlg::setFileName( const QString& theName )
{
  myFileName->setText( theName );
  myImageNameGroup->setEnabled( true );
  myTransformGroup->setEnabled( true );
}

QString HYDROGUI_ImportImageDlg::getGeoreferencementFileName() const
{
  return myGeoFileName->text();
}

void HYDROGUI_ImportImageDlg::setImageSize( const QSize& theSize,
                                            const bool theIsRefImage )
{
  int aWidth = theSize.width();
  int aHeight = theSize.height();
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    if( theIsRefImage )
    {
      myRefPointXMap[ aPointType ]->setRange( 0, aWidth );
      myRefPointYMap[ aPointType ]->setRange( 0, aHeight );
    }
    else
    {
      myPointXMap[ aPointType ]->setRange( 0, aWidth );
      myPointYMap[ aPointType ]->setRange( 0, aHeight );
    }
  }
}

void HYDROGUI_ImportImageDlg::setTransformationMode( const int theMode )
{
  myModeGroup->button( theMode )->setChecked( true );
  onModeActivated( theMode );
}

int HYDROGUI_ImportImageDlg::getTransformationMode() const
{
  return myModeGroup->checkedId();
}

void HYDROGUI_ImportImageDlg::setByTwoPoints( const bool theIsByTwoPoints )
{
  myPointCEnabler->setChecked( !theIsByTwoPoints );
}

bool HYDROGUI_ImportImageDlg::isByTwoPoints() const
{
  return !myPointCEnabler->isChecked();
}

void HYDROGUI_ImportImageDlg::setTransformationDataMap( const TransformationDataMap& theMap,
                                                        const bool theIsOnlyInput,
                                                        const bool theIsRefImage )
{
  DEBTRACE("setTransformationDataMap " << theIsOnlyInput << " " << theIsRefImage);
  blockSignalsGeodesic( true );
  blockSignalsCartesian( true );
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    if( theMap.contains( aPointType ) )
    {
      const TransformationData& aData = theMap[ aPointType ];

      QtxIntSpinBox* aPointX = theIsRefImage ? myRefPointXMap[ aPointType ] : myPointXMap[ aPointType ];
      QtxIntSpinBox* aPointY = theIsRefImage ? myRefPointYMap[ aPointType ] : myPointYMap[ aPointType ];
      aPointX->setValue( aData.ImagePoint.x() );
      aPointY->setValue( aData.ImagePoint.y() );
      DEBTRACE("aPointX " << aData.ImagePoint.x() << " aPointY " << aData.ImagePoint.y());

      if( !theIsOnlyInput && !theIsRefImage )
      {
        QPointF aLPoint = aData.GeodesicPoint;
        int aXDeg = 0, aYDeg = 0;
        int aXMin = 0, aYMin = 0;
        double aXSec = 0, aYSec = 0;
        HYDROData_Lambert93::degToDMS( aLPoint.x(), aXDeg, aXMin, aXSec );
        HYDROData_Lambert93::degToDMS( aLPoint.y(), aYDeg, aYMin, aYSec );

        myPointXDegMap[ aPointType ]->setValue( aXDeg );
        myPointYDegMap[ aPointType ]->setValue( aYDeg );
        myPointXMinMap[ aPointType ]->setValue( aXMin );
        myPointYMinMap[ aPointType ]->setValue( aYMin );
        myPointXSecMap[ aPointType ]->setValue( aXSec );
        myPointYSecMap[ aPointType ]->setValue( aYSec );

        QPointF aCPoint = aData.CartesianPoint;
        myCartPointXMap[ aPointType ]->setValue( aCPoint.x() );
        myCartPointYMap[ aPointType ]->setValue( aCPoint.y() );
      }
    }
  }
  blockSignalsGeodesic( false );
  blockSignalsCartesian( false );
}

bool HYDROGUI_ImportImageDlg::getTransformationDataMap( TransformationDataMap& theMap,
                                                        const bool theIsRefImage ) const
{
  theMap.clear();
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    bool anIsOk[10];
    for( int i = 0; i < 10; i++ )
      anIsOk[ i ] = true;

    QtxIntSpinBox* aPointX = theIsRefImage ? myRefPointXMap[ aPointType ] : myPointXMap[ aPointType ];
    QtxIntSpinBox* aPointY = theIsRefImage ? myRefPointYMap[ aPointType ] : myPointYMap[ aPointType ];
    int aX1 = aPointX->text().toInt( &anIsOk[0] );
    int aY1 = aPointY->text().toInt( &anIsOk[1] );

    int aXDeg = 0, aYDeg = 0;
    int aXMin = 0, aYMin = 0;
    double aXSec = 0, aYSec = 0;
    double aXCart = 0, aYCart = 0;
    if( !theIsRefImage )
    {
      aXDeg = myPointXDegMap[ aPointType ]->text().toInt( &anIsOk[2] );
      aYDeg = myPointYDegMap[ aPointType ]->text().toInt( &anIsOk[3] );
      aXMin = myPointXMinMap[ aPointType ]->text().toInt( &anIsOk[4] );
      aYMin = myPointYMinMap[ aPointType ]->text().toInt( &anIsOk[5] );
      aXSec = myPointXSecMap[ aPointType ]->text().toDouble( &anIsOk[6] );
      aYSec = myPointYSecMap[ aPointType ]->text().toDouble( &anIsOk[7] );

      aXCart = myCartPointXMap[ aPointType ]->text().toDouble( &anIsOk[8] );
      aYCart = myCartPointYMap[ aPointType ]->text().toDouble( &anIsOk[9] );
    }

    for( int i = 0; i < 10; i++ )
      if( !anIsOk[ i ] )
        return false;

    double aX2 = 0, aY2 = 0;
    HYDROData_Lambert93::DMSToDeg( aXDeg, aXMin, aXSec, aX2 );
    HYDROData_Lambert93::DMSToDeg( aYDeg, aYMin, aYSec, aY2 );

    TransformationData aData( QPoint( aX1, aY1 ), QPointF( aX2, aY2 ), QPointF( aXCart, aYCart ) );
    theMap[ aPointType ] = aData;
  }
  return true;
}

void HYDROGUI_ImportImageDlg::setPrsPointDataList( const PrsPointDataList& theList )
{
  myPrsPointDataList = theList;

  if( !myPrsPointDataList.isEmpty() )
  {
    myModeGroup->button( HYDROData_Image::ReferenceImage )->setEnabled( true );

    myRefImage->clear();
    myRefImage->addItem( "" ); // first empty item

    PrsPointDataListIterator anIter( myPrsPointDataList );
    while( anIter.hasNext() )
      myRefImage->addItem( anIter.next().first );
  }
  else
    myModeGroup->button( HYDROData_Image::ReferenceImage )->setEnabled( false );
}

void HYDROGUI_ImportImageDlg::initializePointSelection()
{
  if( myIsInitialized )
    return;

  myIsInitialized = true;

  myPointBtnMap[ HYDROGUI_PrsImage::PointA ]->setChecked( true );

  double aCartX0 = LAMBERT_X0;
  double aCartY0 = LAMBERT_Y0;

  blockSignalsCartesian( true );

  myCartPointXMap[ HYDROGUI_PrsImage::PointA ]->setValue( aCartX0 );
  myCartPointYMap[ HYDROGUI_PrsImage::PointA ]->setValue( aCartY0 + IMG_DELTA );

  myCartPointXMap[ HYDROGUI_PrsImage::PointB ]->setValue( aCartX0 + IMG_DELTA );
  myCartPointYMap[ HYDROGUI_PrsImage::PointB ]->setValue( aCartY0 + IMG_DELTA );

  myCartPointXMap[ HYDROGUI_PrsImage::PointC ]->setValue( aCartX0 );
  myCartPointYMap[ HYDROGUI_PrsImage::PointC ]->setValue( aCartY0 );

  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
    onCartesianCoordChanged( aPointType );

  blockSignalsCartesian( false );
}

void HYDROGUI_ImportImageDlg::ECW_initializePointSelection(HYDROData_Image::ECW_FileInfo* theECW_FileInfo)
{
  if( myIsInitialized )
    return;

  myIsInitialized = true;

  myPointBtnMap[ HYDROGUI_PrsImage::PointA ]->setChecked( true );
  myPointBtnMap[ HYDROGUI_PrsImage::PointB ]->setChecked( true );
  myPointBtnMap[ HYDROGUI_PrsImage::PointC ]->setChecked( true );

  setByTwoPoints(false);

  HYDROData_Image::ECW_CellUnits Units = theECW_FileInfo->myCellSizeUnits;

  if (Units == HYDROData_Image::ECW_CellUnits::ECW_CellUnits_Deg)
  {
    blockSignalsGeodesic( true );
    double Xa, Ya, Xb, Yb, Xc, Yc;
    Xa = theECW_FileInfo->myOriginX;
    Ya = theECW_FileInfo->myOriginY;
    Xb = theECW_FileInfo->myOriginX;    
    Yb = theECW_FileInfo->myOriginY + theECW_FileInfo->myCellIncrementY * theECW_FileInfo->myYSize;
    Xc = theECW_FileInfo->myOriginX + theECW_FileInfo->myCellIncrementX * theECW_FileInfo->myXSize;    
    Yc = theECW_FileInfo->myOriginY;

    int Deg = 0, Min = 0;
    double Sec = 0.0;
    //
    HYDROData_Lambert93::degToDMS( Xa, Deg, Min, Sec );
    myPointXDegMap[HYDROGUI_PrsImage::PointA]->setValue(Deg);
    myPointXMinMap[HYDROGUI_PrsImage::PointA]->setValue(Min);
    myPointXSecMap[HYDROGUI_PrsImage::PointA]->setValue(Sec);
    HYDROData_Lambert93::degToDMS( Ya, Deg, Min, Sec );
    myPointYDegMap[HYDROGUI_PrsImage::PointA]->setValue(Deg);
    myPointYMinMap[HYDROGUI_PrsImage::PointA]->setValue(Min);
    myPointYSecMap[HYDROGUI_PrsImage::PointA]->setValue(Sec);
    //
    HYDROData_Lambert93::degToDMS( Xb, Deg, Min, Sec );
    myPointXDegMap[HYDROGUI_PrsImage::PointB]->setValue(Deg);
    myPointXMinMap[HYDROGUI_PrsImage::PointB]->setValue(Min);
    myPointXSecMap[HYDROGUI_PrsImage::PointB]->setValue(Sec);
    HYDROData_Lambert93::degToDMS( Yb, Deg, Min, Sec );
    myPointYDegMap[HYDROGUI_PrsImage::PointB]->setValue(Deg);
    myPointYMinMap[HYDROGUI_PrsImage::PointB]->setValue(Min);
    myPointYSecMap[HYDROGUI_PrsImage::PointB]->setValue(Sec);
    //
    HYDROData_Lambert93::degToDMS( Xc, Deg, Min, Sec );
    myPointXDegMap[HYDROGUI_PrsImage::PointC]->setValue(Deg);
    myPointXMinMap[HYDROGUI_PrsImage::PointC]->setValue(Min);
    myPointXSecMap[HYDROGUI_PrsImage::PointC]->setValue(Sec);
    HYDROData_Lambert93::degToDMS( Yc, Deg, Min, Sec );
    myPointYDegMap[HYDROGUI_PrsImage::PointC]->setValue(Deg);
    myPointYMinMap[HYDROGUI_PrsImage::PointC]->setValue(Min);
    myPointYSecMap[HYDROGUI_PrsImage::PointC]->setValue(Sec);
    //
    for( int aPointType = HYDROGUI_PrsImage::PointA; aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
      onGeodesicCoordChanged( aPointType );
    blockSignalsGeodesic( false );
    setTransformationMode( HYDROData_Image::ManualGeodesic );
  }
  if (Units == HYDROData_Image::ECW_CellUnits::ECW_CellUnits_Meters ||
    Units == HYDROData_Image::ECW_CellUnits::ECW_CellUnits_Feet )
  {
    double Coeff = 1;
    if (Units == HYDROData_Image::ECW_CellUnits::ECW_CellUnits_Feet)
      Coeff = 0.3048; //feets
    blockSignalsCartesian( true );
    double X1 = 0, Y1 = 0, X2 = 0, Y2 = 0, X3 = 0, Y3 = 0; 
    X1 = theECW_FileInfo->myOriginX;
    Y1 = theECW_FileInfo->myOriginY;
    X2 = theECW_FileInfo->myOriginX;    
    Y2 = theECW_FileInfo->myOriginY + Coeff * theECW_FileInfo->myCellIncrementY * theECW_FileInfo->myXSize;
    X3 = theECW_FileInfo->myOriginX + Coeff * theECW_FileInfo->myCellIncrementX * theECW_FileInfo->myYSize;    
    Y3 = theECW_FileInfo->myOriginY;
    
    //extract to A-point (top-left), B-point (bottom-left), C-Point (top-right)
    double Xa = 0, Ya = 0, Xb = 0, Yb = 0, Xc = 0, Yc = 0;
    Xa = Min(X1, Min(X2, X3));
    Ya = Max(Y1, Max(Y2, Y3));
    Xb = Max(X1, Max(X2, X3));
    Yb = Ya;
    Xc = Xa;
    Yc = Min(Y1, Min(Y2, Y3));

    myCartPointXMap[HYDROGUI_PrsImage::PointA]->setValue(Xa);
    myCartPointYMap[HYDROGUI_PrsImage::PointA]->setValue(Ya);
    //
    myCartPointXMap[HYDROGUI_PrsImage::PointB]->setValue(Xb);
    myCartPointYMap[HYDROGUI_PrsImage::PointB]->setValue(Yb);
    //
    myCartPointXMap[HYDROGUI_PrsImage::PointC]->setValue(Xc);
    myCartPointYMap[HYDROGUI_PrsImage::PointC]->setValue(Yc);
    //
    for( int aPointType = HYDROGUI_PrsImage::PointA; aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
      onCartesianCoordChanged( aPointType );
    blockSignalsCartesian( false );
    setTransformationMode( HYDROData_Image::ManualCartesian );
  }
}


HYDROGUI_ImportImageDlg::TransformationData HYDROGUI_ImportImageDlg::ComputeTrsfData(
  const int      theMode,
  const QPoint&  theLocalPoint,
  const QPointF& theGlobalPoint )
{
  TransformationData aResTrsfData;
  aResTrsfData.ImagePoint = theLocalPoint;

  double arx = theGlobalPoint.x();
  double ary = theGlobalPoint.y();
  if ( theMode == HYDROData_Image::ManualGeodesic )
  {
    // Geodesic to Cartesian
    double aXCart = 0, aYCart = 0;
    // Interpreting arY as attitude and arX as longitude
    HYDROData_Lambert93::toXY( ary, arx, aXCart, aYCart );

    aResTrsfData.GeodesicPoint = theGlobalPoint;
    aResTrsfData.CartesianPoint = QPointF( aXCart, aYCart );
  }
  else if ( theMode == HYDROData_Image::ManualCartesian )
  {
    // Cartesian to Geodesic
    double aLonDeg = 0, aLatDeg = 0;
    HYDROData_Lambert93::toGeo( arx, ary, aLatDeg, aLonDeg );

    aResTrsfData.CartesianPoint = theGlobalPoint;
    aResTrsfData.GeodesicPoint = QPointF( aLonDeg, aLatDeg );
  }

  return aResTrsfData;
}

void HYDROGUI_ImportImageDlg::onBrowse()
{
  QString aFilter( tr( "IMAGE_FILTER_IMPORT" ) );
  QStringList aFileNamesList = SUIT_FileDlg::getOpenFileNames( this, "", aFilter, tr( "IMPORT_IMAGE_FROM_FILE" ), true );
  emit filesSelected( aFileNamesList );
}

void HYDROGUI_ImportImageDlg::ActivateFile( const QString& theFileName, bool isEnableFilesChoice )
{
  myFileName->setEnabled( isEnableFilesChoice );
  myBrowseBtn->setEnabled( isEnableFilesChoice );

  if( !theFileName.isEmpty() )
  {
    QFileInfo aFI( theFileName );
    QImage anImage;
    HYDROData_Image::ECW_FileInfo* theECWInfo = NULL;
    if (aFI.suffix().toLower() == "ecw")
    {
      theECWInfo = new HYDROData_Image::ECW_FileInfo;
      HYDROData_Image::OpenECW(theFileName.toLatin1().data(), anImage, theECWInfo);
    }
    else
      anImage = QImage(theFileName);
    if( anImage.isNull() )
    {
      QString aTitle = QObject::tr( "INPUT_VALID_DATA" );
      QString aMessage = QObject::tr( "FILE_CAN_NOT_BE_IMPORTED" ).
        arg( theFileName ).arg( QFileInfo( theFileName ).suffix() );
      SUIT_MessageBox::warning( module()->getApp()->desktop(), aTitle, aMessage );
    }
    else
    {
      setFileName( theFileName );
      setImageName( "" );
      emit createPreview( anImage, theECWInfo );
    }
  }
}

void HYDROGUI_ImportImageDlg::onGeoBrowse()
{
  QString aFilter( tr( "IMAGE_GEOREFERENCEMENT_FILTER" ) );
  QString aGeoFileName = SUIT_FileDlg::getFileName( this, "", aFilter, tr( "IMPORT_GEO_DATA_FROM_FILE" ), true );
  if( !aGeoFileName.isEmpty() ) {
    myGeoFileName->setText( aGeoFileName );
  }
}

void HYDROGUI_ImportImageDlg::onModeActivated( int theMode )
{
  bool anIsManualGeodesic = theMode == HYDROData_Image::ManualGeodesic;
  bool anIsManualCartesian = theMode == HYDROData_Image::ManualCartesian;
  bool anIsRefImage = theMode == HYDROData_Image::ReferenceImage;

  myRefImage->setEnabled( anIsRefImage );

  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    myPointXDegMap[ aPointType ]->setVisible( anIsManualGeodesic );
    myPointYDegMap[ aPointType ]->setVisible( anIsManualGeodesic );
    myPointXMinMap[ aPointType ]->setVisible( anIsManualGeodesic );
    myPointYMinMap[ aPointType ]->setVisible( anIsManualGeodesic );
    myPointXSecMap[ aPointType ]->setVisible( anIsManualGeodesic );
    myPointYSecMap[ aPointType ]->setVisible( anIsManualGeodesic );

    myCartPointXMap[ aPointType ]->setVisible( anIsManualCartesian );
    myCartPointYMap[ aPointType ]->setVisible( anIsManualCartesian );

    myRefPointXMap[ aPointType ]->setVisible( anIsRefImage );
    myRefPointYMap[ aPointType ]->setVisible( anIsRefImage );
  }

  QListIterator<QLabel*> anIter( myGeodesicLabels );
  while( anIter.hasNext() )
    anIter.next()->setVisible( anIsManualGeodesic );

  emit modeActivated( theMode );
}

void HYDROGUI_ImportImageDlg::onRefImageActivated( const QString& theName )
{
  emit refImageActivated( theName );
}

void HYDROGUI_ImportImageDlg::onPointBtnToggled( bool theState )
{
  int aPointType = HYDROGUI_PrsImage::None;
  if( theState )
  {
    QMapIterator<int, QPushButton*> anIter( myPointBtnMap );
    while( anIter.hasNext() )
    {
      int aBtnPointType = anIter.next().key();
      QPushButton* aBtn = anIter.value();
      if( aBtn == sender() )
        aPointType = aBtnPointType;
      else
      {
        bool anIsBlocked = aBtn->blockSignals( true );
        aBtn->setChecked( false );
        aBtn->blockSignals( anIsBlocked );
      }
    }
  }
  emit activatePointSelection( aPointType );
}

void HYDROGUI_ImportImageDlg::onPointCoordChanged( int theValue )
{
  QObject* aSender = sender();
  if( !aSender )
    return;

  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    if( aSender == myPointXMap[ aPointType ] ||
        aSender == myPointYMap[ aPointType ] )
    {
      bool anIsY = aSender == myPointYMap[ aPointType ];
      emit pointCoordChanged( false, aPointType, anIsY, theValue );
    }
    else if( aSender == myRefPointXMap[ aPointType ] ||
             aSender == myRefPointYMap[ aPointType ] )
    {
      bool anIsY = aSender == myRefPointYMap[ aPointType ];
      emit pointCoordChanged( true, aPointType, anIsY, theValue );
    }
  }
}

void HYDROGUI_ImportImageDlg::onGeodesicCoordChanged()
{
  QObject* aSender = sender();
  if( !aSender )
    return;

  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    if( aSender == myPointXDegMap[ aPointType ] ||
        aSender == myPointYDegMap[ aPointType ] ||
        aSender == myPointXMinMap[ aPointType ] ||
        aSender == myPointYMinMap[ aPointType ] ||
        aSender == myPointXSecMap[ aPointType ] ||
        aSender == myPointYSecMap[ aPointType ] )
    {
      onGeodesicCoordChanged( aPointType );
      return;
    }
  }
}

void HYDROGUI_ImportImageDlg::onGeodesicCoordChanged( const int thePointType )
{
  bool anIsOk[6];
  for( int i = 0; i < 6; i++ )
    anIsOk[ i ] = true;

  int aXDeg = myPointXDegMap[ thePointType ]->text().toInt( &anIsOk[0] );
  int aYDeg = myPointYDegMap[ thePointType ]->text().toInt( &anIsOk[1] );
  int aXMin = myPointXMinMap[ thePointType ]->text().toInt( &anIsOk[2] );
  int aYMin = myPointYMinMap[ thePointType ]->text().toInt( &anIsOk[3] );
  double aXSec = myPointXSecMap[ thePointType ]->text().toDouble( &anIsOk[4] );
  double aYSec = myPointYSecMap[ thePointType ]->text().toDouble( &anIsOk[5] );

  for( int i = 0; i < 6; i++ )
    if( !anIsOk[ i ] )
      return;

  double aLonDeg = 0, aLatDeg = 0;
  HYDROData_Lambert93::DMSToDeg( aXDeg, aXMin, aXSec, aLonDeg );
  HYDROData_Lambert93::DMSToDeg( aYDeg, aYMin, aYSec, aLatDeg );

  double aXCart = 0, aYCart = 0;
  HYDROData_Lambert93::toXY( aLatDeg, aLonDeg, aXCart, aYCart );

  blockSignalsCartesian( true );

  myCartPointXMap[ thePointType ]->setValue( aXCart );
  myCartPointYMap[ thePointType ]->setValue( aYCart );

  blockSignalsCartesian( false );
}

void HYDROGUI_ImportImageDlg::onCartesianCoordChanged()
{
  QObject* aSender = sender();
  if( !aSender )
    return;

  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    if( aSender == myCartPointXMap[ aPointType ] ||
        aSender == myCartPointYMap[ aPointType ] )
    {
      onCartesianCoordChanged( aPointType );
      return;
    }
  }
}

void HYDROGUI_ImportImageDlg::onCartesianCoordChanged( const int thePointType )
{
  bool anIsOk[2];
  for( int i = 0; i < 2; i++ )
    anIsOk[ i ] = true;

  double aXCart = myCartPointXMap[ thePointType ]->text().toDouble( &anIsOk[0] );
  double aYCart = myCartPointYMap[ thePointType ]->text().toDouble( &anIsOk[1] );

  for( int i = 0; i < 2; i++ )
    if( !anIsOk[ i ] )
      return;

  double aLonDeg = 0, aLatDeg = 0;
  HYDROData_Lambert93::toGeo( aXCart, aYCart, aLatDeg, aLonDeg );

  int aXDeg = 0, aYDeg = 0;
  int aXMin = 0, aYMin = 0;
  double aXSec = 0, aYSec = 0;
  HYDROData_Lambert93::degToDMS( aLonDeg, aXDeg, aXMin, aXSec );
  HYDROData_Lambert93::degToDMS( aLatDeg, aYDeg, aYMin, aYSec );

  blockSignalsGeodesic( true );

  myPointXDegMap[ thePointType ]->setValue( aXDeg );
  myPointYDegMap[ thePointType ]->setValue( aYDeg );
  myPointXMinMap[ thePointType ]->setValue( aXMin );
  myPointYMinMap[ thePointType ]->setValue( aYMin );
  myPointXSecMap[ thePointType ]->setValue( aXSec );
  myPointYSecMap[ thePointType ]->setValue( aYSec );

  blockSignalsGeodesic( false );
}

void HYDROGUI_ImportImageDlg::clearSpins( QAbstractSpinBox* theSpin )
{
  if ( dynamic_cast<QtxIntSpinBox*>( theSpin ) )
  {
    QtxIntSpinBox* aSpin = dynamic_cast<QtxIntSpinBox*>( theSpin );
    aSpin->setValue( aSpin->minimum() );
  }
  else if ( dynamic_cast<QtxDoubleSpinBox*>( theSpin ) )
  {
    QtxDoubleSpinBox* aDblSpin = dynamic_cast<QtxDoubleSpinBox*>( theSpin );
    aDblSpin->setValue( aDblSpin->minimum() );
  }

  theSpin->clear();
}

bool HYDROGUI_ImportImageDlg::blockSignalsPoints( const bool theState )
{
  bool isBlocked = false;
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    isBlocked = 
    myPointXMap[ aPointType ]->blockSignals( theState ) || isBlocked;
    myPointYMap[ aPointType ]->blockSignals( theState );

    myRefPointXMap[ aPointType ]->blockSignals( theState );
    myRefPointYMap[ aPointType ]->blockSignals( theState );
  }
  return isBlocked;
}

bool HYDROGUI_ImportImageDlg::blockSignalsGeodesic( const bool theState )
{
  bool isBlocked = false;
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    isBlocked = 
    myPointXDegMap[ aPointType ]->blockSignals( theState ) || isBlocked;
    myPointXMinMap[ aPointType ]->blockSignals( theState );
    myPointXSecMap[ aPointType ]->blockSignals( theState );
    myPointYDegMap[ aPointType ]->blockSignals( theState );
    myPointYMinMap[ aPointType ]->blockSignals( theState );
    myPointYSecMap[ aPointType ]->blockSignals( theState );
  }
  return isBlocked;
}

bool HYDROGUI_ImportImageDlg::blockSignalsCartesian( const bool theState )
{
  bool isBlocked = false;
  for( int aPointType = HYDROGUI_PrsImage::PointA;
       aPointType <= HYDROGUI_PrsImage::PointC; aPointType++ )
  {
    isBlocked = 
    myCartPointXMap[ aPointType ]->blockSignals( theState ) || isBlocked;
    myCartPointYMap[ aPointType ]->blockSignals( theState );
  }
  return isBlocked;
}

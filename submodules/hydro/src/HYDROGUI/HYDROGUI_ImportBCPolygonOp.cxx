// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportBCPolygonOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Tool2.h"
#include <HYDROData_PolylineXY.h>

#include <HYDROData_BCPolygon.h>
#include <HYDROGUI_DataObject.h>

#include <HYDROGUI_ImportPolylineOp.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>
#include <LightApp_Application.h>

#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <SUIT_MessageBox.h>


HYDROGUI_ImportBCPolygonOp::HYDROGUI_ImportBCPolygonOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "IMPORT_BC_POLYGON" ) );
}

HYDROGUI_ImportBCPolygonOp::~HYDROGUI_ImportBCPolygonOp()
{
}

void HYDROGUI_ImportBCPolygonOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myFileDlg = new SUIT_FileDlg( module()->getApp()->desktop(), true );
  myFileDlg->setWindowTitle( getName() );
  myFileDlg->setFileMode( SUIT_FileDlg::ExistingFiles );
  myFileDlg->setNameFilter( tr("BC_POLYGON_FILTER") );

  connect( myFileDlg, SIGNAL( accepted() ), this, SLOT( onApply() ) );
  connect( myFileDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );

  myFileDlg->exec();
}


bool HYDROGUI_ImportBCPolygonOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries )
{
  if ( !myFileDlg )
  {
    //abort();
    return false;
  }

  QStringList aFileNames = myFileDlg->selectedFiles();
  
  QApplication::setOverrideCursor( Qt::WaitCursor );  
  //startDocOperation();

   NCollection_Sequence<Handle(HYDROData_Entity)> importedEnts = 
     HYDROGUI_ImportPolylineOp::ImportPolyOp(aFileNames, doc(), module(), 
     HYDROData_ShapeFile::ImportShapeType_Polygon);

   for (int i=1;i<=importedEnts.Size();i++ )
   {
      Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast(importedEnts(i));
      if (aPolylineXY.IsNull())
        continue;
      TopoDS_Shape aTopSh = HYDROData_BCPolygon::generateTopShape(aPolylineXY);
      if (aTopSh.IsNull())
        continue;

      //get boundary type from DBF
      QStringList aDBFTableList;
      aPolylineXY->GetDBFInfo(aDBFTableList);
      QVector<QString> aDBFTable = aDBFTableList.toVector();
      int nbs = aDBFTable.size();
      QString aBTypeStr;
      for (int i = 0; i < nbs / 2; i++)
        if (aDBFTable[i] == "type")
        {
          aBTypeStr = aDBFTable[nbs / 2 + i];
          break;
        }

      int aBTypeVal = 0;
      bool Ok = false;
      aBTypeVal = aBTypeStr.toInt(&Ok);
      if (!Ok)
        aBTypeVal = 0;

      QString aPolyXYName = aPolylineXY->GetName();
      Handle(HYDROData_BCPolygon) aBCObj = Handle(HYDROData_BCPolygon)::DownCast( doc()->CreateObject( KIND_BC_POLYGON ) );
      aBCObj->SetName( "BP_" + aPolyXYName );
      aBCObj->SetBoundaryType(aBTypeVal);
  
      aBCObj->SetFillingColor( aBCObj->DefaultFillingColor() );
      aBCObj->SetBorderColor( aBCObj->DefaultBorderColor() );
      aBCObj->SetPolyline( aPolylineXY );
      aBCObj->Update(aTopSh);

      module()->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( module() ), aBCObj, true );
      QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aBCObj );
      theBrowseObjectsEntries.append( anEntry );
      module()->setIsToUpdate( aBCObj );
   }
 
  if (!aFileNames.empty())
  {
    //commitDocOperation();
    //commit();
    //module()->update( UF_Model | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init | UF_OCCViewer );
    theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init;
  }
  //else
  //  abort();
  
  QApplication::restoreOverrideCursor();
  return true;
}

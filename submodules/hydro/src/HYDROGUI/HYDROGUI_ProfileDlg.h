// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PROFILEDLG_H
#define HYDROGUI_PROFILEDLG_H

#include "HYDROGUI_ViewerDlg.h"

#include <AIS_InteractiveContext.hxx>
#include <vector>

class CurveCreator_Widget;
class CurveCreator_ICurve;
class OCCViewer_ViewManager;
class SUIT_ViewWindow;
class QGroupBox;
class QLineEdit;
class QLabel;
class QListWidget;
class HYDROGUI_CurveCreatorProfile;
class HYDROData_SequenceOfObjects; 
class QListWidgetItem;

class HYDROGUI_ProfileDlg : public HYDROGUI_ViewerDlg
{
  Q_OBJECT

public:
  HYDROGUI_ProfileDlg( HYDROGUI_Module* theModule, const QString& theTitle, bool theSingleProfileMode );
  virtual ~HYDROGUI_ProfileDlg();

  void setProfileName( const QString& theName );
  void addProfileName( const QString& theName, const QColor& theColor );
  void eraseProfile( int index );
  QStringList getProfileNames() const;
	
  void  setProfile( CurveCreator_ICurve* theProfile );
  void  setProfilesPointer(std::vector<HYDROGUI_CurveCreatorProfile*>* theProfilesPointer);
  void  switchToFirstProfile();
  //void  SetSingleProfileMode(bool SingleMode);
  //bool  GetSingleProfileMode() const;
  void  SwitchToProfile(int theIndex);
  void  BlockProfileNameSignals(bool state);
  int   GetProfileSelectionIndex();

  void  reset();

  QList<int> getSelectedSections();

  void  deleteSelected();
  bool  deleteEnabled();

protected slots:
  void processStartedSubOperation( QWidget*, bool );
  void processFinishedSubOperation( QWidget* );
  void ProfileNameChanged(QString);
  void onProfileIndexChanged();
  void onAddBtnPressed(bool);
  void onRemoveBtnPressed(bool);
  void onSetColorBtnPressed(bool);
  void onProfileNameChanged(QListWidgetItem* item);

signals:
  void                  createPreview( QString );
  void                  selectionChanged();
  void                  widgetCreated(QWidget*);
  void                  subOperationStarted(QWidget*);
  void                  subOperationFinished(QWidget*);
  void                  AddProfiles();
  void                  RemoveProfile(int);

protected:
  virtual Handle(AIS_Trihedron) trihedron();

private:
  QLineEdit*             myName;
  QListWidget*           myProfileNames;
  QPushButton*           myAddProfBtn;
  QPushButton*           myRemProfBtn; 
  QPushButton*           mySetColorProfBtn;
public:
  CurveCreator_Widget*   myEditorWidget;
  QGroupBox*             myAddElementBox;
  std::vector<HYDROGUI_CurveCreatorProfile*>* myProfilesPointer;
private:
  bool                   myIsEdit;
  QString                myCurrentName;
};

#endif

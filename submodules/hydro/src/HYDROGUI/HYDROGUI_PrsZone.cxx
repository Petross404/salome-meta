// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PrsZone.h"

#include <QBrush>
#include <QPen>

//=======================================================================
// name    : HYDROGUI_PrsZone
// Purpose : Constructor
//=======================================================================
HYDROGUI_PrsZone::HYDROGUI_PrsZone( const Handle(HYDROData_Entity)& theObject )
: HYDROGUI_Prs( theObject ),
  myZoneItem( 0 ),
  myFillingColor( Qt::transparent ),
  myBorderColor( Qt::transparent )
{
}

//=======================================================================
// name    : HYDROGUI_PrsZone
// Purpose : Destructor
//=======================================================================
HYDROGUI_PrsZone::~HYDROGUI_PrsZone()
{
}

//================================================================
// Function : setPath
// Purpose  : 
//================================================================
void HYDROGUI_PrsZone::setPath( const QPainterPath& thePath )
{
  myPath = thePath;
}

//================================================================
// Function : getPath
// Purpose  : 
//================================================================
QPainterPath HYDROGUI_PrsZone::getPath() const
{
  return myPath;
}

//================================================================
// Function : setFillingColor
// Purpose  : 
//================================================================
void HYDROGUI_PrsZone::setFillingColor( const QColor& theColor )
{
  myFillingColor = theColor;
}

//================================================================
// Function : getFillingColor
// Purpose  : 
//================================================================
QColor HYDROGUI_PrsZone::getFillingColor() const
{
  return myFillingColor;
}

//================================================================
// Function : setBorderColor
// Purpose  : 
//================================================================
void HYDROGUI_PrsZone::setBorderColor( const QColor& theColor )
{
  myBorderColor = theColor;
}

//================================================================
// Function : getBorderColor
// Purpose  : 
//================================================================
QColor HYDROGUI_PrsZone::getBorderColor() const
{
  return myBorderColor;
}

//================================================================
// Function : boundingRect
// Purpose  : 
//================================================================
QRectF HYDROGUI_PrsZone::boundingRect() const
{
  return myZoneItem->boundingRect();
}

//================================================================
// Function : compute
// Purpose  : 
//================================================================
void HYDROGUI_PrsZone::compute()
{
  if( !myZoneItem )
  {
    myZoneItem = new QGraphicsPathItem( this );
    addToGroup( myZoneItem );
  }
  
  QPainterPath aPath( myPath );
  //aPath.setFillRule( Qt::WindingFill );
  aPath.setFillRule( Qt::OddEvenFill ); // ouv: for correct drawing the paths with holes

  myZoneItem->setPath( aPath );
  myZoneItem->setBrush( QBrush( myFillingColor ) );
  myZoneItem->setPen( QPen( myBorderColor ) );
}

//================================================================
// Function : checkHighlight
// Purpose  : 
//================================================================
bool HYDROGUI_PrsZone::checkHighlight( double theX, double theY, QCursor& theCursor ) const
{
  // to do
  return false;
}

//================================================================
// Function : select
// Purpose  : 
//================================================================
bool HYDROGUI_PrsZone::select( double theX, double theY, const QRectF& theRect )
{
  return GraphicsView_Object::select( theX, theY, theRect );
}

//================================================================
// Function : unselect
// Purpose  : 
//================================================================
void HYDROGUI_PrsZone::unselect()
{
  GraphicsView_Object::unselect();

  // ouv: tmp
  QPen aPen = myZoneItem->pen();
  aPen.setColor( Qt::black );
  aPen.setWidth( 1 );
  myZoneItem->setPen( aPen );
}

//================================================================
// Function : setSelected
// Purpose  : 
//================================================================
void HYDROGUI_PrsZone::setSelected( bool theState )
{
  GraphicsView_Object::setSelected( theState );

  // ouv: tmp
  QPen aPen = myZoneItem->pen();
  aPen.setColor( theState ? Qt::red : Qt::black );
  aPen.setWidth( theState ? 2 : 1 );
  myZoneItem->setPen( aPen );
}

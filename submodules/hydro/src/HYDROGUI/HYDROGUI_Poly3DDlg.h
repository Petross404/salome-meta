// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_POLY3DDLG_H
#define HYDROGUI_POLY3DDLG_H

#include "HYDROGUI_InputPanel.h"

class QCheckBox;
class QLabel;
class QLineEdit;

class HYDROGUI_ObjSelector;

class HYDROGUI_Poly3DDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_Poly3DDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_Poly3DDlg();

  void                       reset();

  void                       setMode( const bool theIsEdit );

  void                       setResultName( const QString& theName );
  QString                    getResultName() const;

  void                       setSelectedObjects( const QString& theNamePoly,
                                                 const QString& theNameProfile,
                                                 const QString& theNameBath );
  bool                       getSelectedObjects( QString& theNamePoly,
                                                 QString& theNameProfile,
                                                 QString& theNameBath ) const;

  void                       setPreselectedObject( const QString& theName );

protected slots:
  void OnSelectionChanged();

private:
  bool                       myIsEdit;

  QLineEdit*                 myName;

  QLabel*                    myPolylineLabel;
  HYDROGUI_ObjSelector*      myPolyline;

  QLabel*                    myProfileLabel;
  HYDROGUI_ObjSelector*      myProfile;

  QLabel*                    myBathLabel;
  HYDROGUI_ObjSelector*      myBath;
};

#endif

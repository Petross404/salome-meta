// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ShapeImage.h>

#include <HYDROGUI_Tool.h>
#include <HYDROGUI_ImagePrs.h>

#include <HYDROData_Image.h>
#include <HYDROData_Document.h>

#include <gp_Pnt.hxx>
#include <QMessageBox>
#include <QApplication>
#include <QDesktopWidget>

HYDROGUI_ShapeImage::HYDROGUI_ShapeImage( const Handle(AIS_InteractiveContext)& theContext,
                                          const Handle(HYDROData_Image)&         theImage,
                                          const int                             theZLayer )
: HYDROGUI_Shape( theContext, theImage, theZLayer )
{
}

HYDROGUI_ShapeImage::~HYDROGUI_ShapeImage()
{
}

void HYDROGUI_ShapeImage::update( bool theIsUpdateViewer, bool isDeactivateSelection )
{
  setIsToUpdate( false );

  buildShape();

  HYDROGUI_Shape::update( theIsUpdateViewer, isDeactivateSelection );
}

QList<Handle(AIS_InteractiveObject)> HYDROGUI_ShapeImage::createShape() const
{
  QList<Handle(AIS_InteractiveObject)> shapes;

  Handle(HYDROGUI_ImagePrs) aPrs;

  Handle(HYDROData_Image) anImageObj = Handle(HYDROData_Image)::DownCast( getObject() );
  if ( !anImageObj.IsNull() )
  {
	QString aMsg;
    aPrs = new HYDROGUI_ImagePrs( imagePixMap( anImageObj, aMsg ), imageContour( anImageObj ) );
	if( !aMsg.isEmpty() )
		QMessageBox::warning( qApp->desktop(), "Warning", aMsg ); 
    shapes.append( aPrs );
  }

  return shapes;
}

Handle(Image_PixMap) HYDROGUI_ShapeImage::imagePixMap( const Handle(HYDROData_Image)& theImageObj, QString& theMessage ) const
{
    Handle(Image_PixMap) aPix;
    if ( !theImageObj.IsNull() )
    {
        QTransform aTrsf = theImageObj->Trsf();
        QImage anImage = theImageObj->Image();
//        anImage = anImage.transformed( QTransform::fromScale( -1, -1 ) * aTrsf, Qt::SmoothTransformation );

        if ( !anImage.isNull() )
        {
            // Workaround: Scale the texture image to the nearest width multiple 4 due to the CASCADE bug 23813
            int aTrsfWidth = anImage.width();
            int aDelta = aTrsfWidth % 4;
            if ( aDelta > 0 )
                aTrsfWidth += ( 4 - aDelta );
      
			const int maxWidth = 7000;
			if( aTrsfWidth>maxWidth )
			{
				aTrsfWidth = maxWidth;
				theMessage = QString( "The size of image is scaled to %0" ).arg( aTrsfWidth );
			}
            anImage = anImage.scaledToWidth( aTrsfWidth );
			int pix = anImage.width() * anImage.height();

            aPix = HYDROGUI_Tool::Pixmap( anImage );
        }
    }
    return aPix;
}

QPolygonF HYDROGUI_ShapeImage::imageContour( const Handle(HYDROData_Image)& theImageObj ) const
{
    QPolygonF aContour;
    if ( !theImageObj.IsNull() )
    {
        QTransform aTrsf = theImageObj->Trsf();
        QImage anImage = theImageObj->Image();

        QPolygonF aPolygon = QPolygonF() << QPointF( 0, 0 ) << QPointF( anImage.width(), 0 ) 
                                         << QPointF( anImage.width(), anImage.height() ) << QPointF( 0, anImage.height() );
        aPolygon = aTrsf.map( aPolygon );

        Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
        for ( QPolygonF::iterator it = aPolygon.begin(); it != aPolygon.end(); ++it )
        {
            gp_Pnt aPnt( (*it).x(), (*it).y(), 0 );
            aDoc->Transform( aPnt, true );
            aContour.append( QPointF( aPnt.X(), aPnt.Y() ) );
        }
    }
    return aContour;
}

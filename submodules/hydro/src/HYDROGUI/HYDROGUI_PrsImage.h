// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRSIMAGE_H
#define HYDROGUI_PRSIMAGE_H

#include "HYDROGUI_Prs.h"

class HYDROGUI_PrsImageFrame;

/*
  Class       : HYDROGUI_PrsImage
  Description : Presentation for image object
*/
class HYDROGUI_PrsImage : public HYDROGUI_Prs
{
public:
  enum TransformationPointType { None = 0, PointA, PointB, PointC };
  struct TransformationPoint
  {
    TransformationPoint() : Point( 0, 0 ), Caption( "" ), GroupItem( 0 ) {}

    QPoint              Point;
    QString             Caption;
    QGraphicsItemGroup* GroupItem;
  };
  typedef QMap        < int, TransformationPoint > TransformationPointMap;
  typedef QMapIterator< int, TransformationPoint > TransformationPointMapIterator;

public:
  HYDROGUI_PrsImage( const Handle(HYDROData_Entity)& theObject );
  virtual ~HYDROGUI_PrsImage();

public:
  void                            setImage( const QImage& theImage,
                                            const bool theCompute = false );
  QImage                          getImage() const;

  void                            setCaption( const QString& theCaption );
  QString                         getCaption() const;

  void                            setIsTransformationPointPreview( const bool theState );
  bool                            getIsTransformationPointPreview() const;

  void                            setIsByTwoPoints( const bool theIsByTwoPoints );
  bool                            getIsByTwoPoints() const;

  void                            setTransformationPointType( const int thePointType );

  void                            setTransformationPointMap( const TransformationPointMap& theMap );
  const TransformationPointMap&   getTransformationPointMap() const { return myTransformationPointMap; }

  void                            updateTransformationPoint( const int thePointType,
                                                             const bool theIsY,
                                                             const int theValue );

  /**
   * Set shape of the transformation point cursor.
   * @param theCursorShape a cursor shape to be set
   */
  void                            setTransformationPointCursorShape(Qt::CursorShape theCursorShape);

public:
  // from QGraphicsItem
  virtual QRectF                  boundingRect() const;

  // from GraphicsView_Object
  virtual void                    compute();

  virtual void                    addTo( GraphicsView_ViewPort* theViewPort );
  virtual void                    removeFrom( GraphicsView_ViewPort* theViewPort );

  virtual bool                    isMovable() const { return false; }

  virtual bool                    checkHighlight( double theX, double theY, QCursor& theCursor ) const;

  virtual bool                    select( double theX, double theY, const QRectF& theRect );
  virtual void                    unselect();
  virtual void                    setSelected( bool theState );

protected:
  void                            computeTransformationPoints( const bool theObligatoryInit = false );

  QGraphicsItemGroup*             createPointItem( const QString& theCaption,
                                                   const QColor& theColor );

  /**
   * Create presentations for transformation points A, B and C or update the presentation
   * parameters if it is already created
   */
  void                            initTrsfPoints( const int thePointType );
  /**
   * Show/hide a transformation point if necessary.
   */
  void                            updateTrsfPoint( const int thePointType );

protected:
  QCursor*                        getTransformationPointCursor() const { return myTransformationPointCursor; }

protected:
  QImage                          myImage;

  QGraphicsPixmapItem*            myPixmapItem;
  QGraphicsSimpleTextItem*        myCaptionItem;
  HYDROGUI_PrsImageFrame*         myPrsImageFrame;

  bool                            myIsTransformationPointPreview;
  bool                            myIsByTwoPoints;
  int                             myTransformationPointType;
  TransformationPointMap          myTransformationPointMap;

private:
  QCursor*                        myTransformationPointCursor;
};

#endif

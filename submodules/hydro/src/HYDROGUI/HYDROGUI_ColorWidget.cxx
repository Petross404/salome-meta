// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ColorWidget.h"

#include <QApplication>
#include <QColorDialog>

HYDROGUI_ColorWidget::HYDROGUI_ColorWidget( QWidget* parent )
: QFrame( parent )
{
  setFrameShape( QFrame::Box );
  //setFixedSize( 40, 20 );
  //commented for correct layout management
  //(otherwise the column with color widget can not be resized
  //and all free space is arranged on sides
}

HYDROGUI_ColorWidget::~HYDROGUI_ColorWidget()
{
}

QColor HYDROGUI_ColorWidget::color() const
{
  QPalette aPalette = palette();
  return aPalette.color( QPalette::Window );
}

void HYDROGUI_ColorWidget::setColor( const QColor& c )
{
  QPalette aPalette = palette();
  aPalette.setColor( QPalette::Window, c );
  setAutoFillBackground( true );
  setPalette( aPalette );
}

void HYDROGUI_ColorWidget::resetColor()
{
  QPalette aDefPalette = QApplication::palette();
  QColor aColor = aDefPalette.color( QPalette::Window );
  setColor( aColor );
}

void HYDROGUI_ColorWidget::mouseDoubleClickEvent( QMouseEvent* )
{
  QColor c = QColorDialog::getColor( color(), this );
  if( c.isValid() )
  {
    setColor( c );
    emit colorChanged( c );
  }
}
/*
int HYDROGUI_ColorWidget::intColor() const
{
  return HYDROGUI_Tools::color2int( color() );
}

void HYDROGUI_ColorWidget::setColor( const int col )
{
  setColor( HYDROGUI_Tools::int2color( col ) );
}
*/
/*void HYDROGUI_ColorWidget::setRandColor()
{
  setColor( HYDROGUI_Tools::randColor() );
}*/

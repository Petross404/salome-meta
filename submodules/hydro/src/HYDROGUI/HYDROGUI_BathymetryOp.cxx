// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_BathymetryOp.h>
#include <HYDROGUI_Operations.h>
#include <HYDROGUI_BathymetryPrs.h>
#include <HYDROGUI_ShapeBathymetry.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_OCCDisplayer.h>
#include <LightApp_Application.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewWindow.h>
#include <QtxDoubleSpinBox.h>
#include <SUIT_Desktop.h>
#include <SUIT_SelectionMgr.h>
#include <QLayout>
#include <QPushButton>
#include <QApplication>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_BathymetryLimitsDlg::HYDROGUI_BathymetryLimitsDlg( QWidget* theParent )
  : QDialog( theParent )
{
  QGridLayout* layout = new QGridLayout();
  setLayout( layout );

  layout->addWidget( new QLabel( tr( "Min value" ) ), 0, 0 );
  layout->addWidget( new QLabel( tr( "Max value" ) ), 1, 0 );

  myMin = new QtxDoubleSpinBox( this );
  myMin->setRange( -1000, 1000 );
  myMax = new QtxDoubleSpinBox( this );
  myMax->setRange( -1000, 1000 );

  layout->addWidget( myMin, 0, 1 );
  layout->addWidget( myMax, 1, 1 );

  QFrame* aBtnFrame = new QFrame( this );
  QHBoxLayout* aBtnLayout = new QHBoxLayout( aBtnFrame );
  layout->addWidget( aBtnFrame, 2, 0, 1, 2 );

  QPushButton* ok = new QPushButton( tr( "OK" ), this );
  QPushButton* cancel = new QPushButton( tr( "Cancel" ), this );
  aBtnLayout->addWidget( ok );
  aBtnLayout->addWidget( cancel );

  connect( ok, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( cancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
}

HYDROGUI_BathymetryLimitsDlg::~HYDROGUI_BathymetryLimitsDlg()
{
}

double HYDROGUI_BathymetryLimitsDlg::GetMin() const
{
  return myMin->value();
}

double HYDROGUI_BathymetryLimitsDlg::GetMax() const
{
  return myMax->value();
}

void HYDROGUI_BathymetryLimitsDlg::SetMinMax( double theMin, double theMax )
{
  myMin->setValue( theMin );
  myMax->setValue( theMax );
}





HYDROGUI_BathymetryOp::HYDROGUI_BathymetryOp( HYDROGUI_Module* theModule, int theMode )
: HYDROGUI_Operation( theModule ), myMode( theMode ), myIsActivate( false )
{
}

HYDROGUI_BathymetryOp::~HYDROGUI_BathymetryOp()
{
}

OCCViewer_Viewer* getViewer( HYDROGUI_Module* theModule );
Handle(AIS_InteractiveContext) getContext( HYDROGUI_Module* theModule );
QList<Handle(HYDROGUI_BathymetryPrs)> getShownBathymetries( HYDROGUI_Module* theModule );

void HYDROGUI_BathymetryOp::startOperation()
{
  activate( true );
}

void HYDROGUI_BathymetryOp::commitOperation()
{
  //if( myMode!=BathymetryTextId )
  activate( false );
}

void HYDROGUI_BathymetryOp::abortOperation()
{
  activate( false );
}

OCCViewer_ViewWindow* HYDROGUI_BathymetryOp::activeViewWindow() const
{
  LightApp_Application* app = module()->getApp();
  OCCViewer_ViewManager* mgr = dynamic_cast<OCCViewer_ViewManager*>
    ( app->getViewManager( OCCViewer_Viewer::Type(), true ) );
  return dynamic_cast<OCCViewer_ViewWindow*>( mgr->getActiveView() );
}

void HYDROGUI_BathymetryOp::activate( bool isActivate )
{
  DEBTRACE("activate " << isActivate);
  if( myIsActivate==isActivate )
    return;

  myIsActivate = isActivate;
  QList<Handle(HYDROGUI_BathymetryPrs)> baths = getShownBathymetries( module() );
  bool isUpdateCS = false;

  if( myMode!=BathymetryRescaleUserId )
    qApp->setOverrideCursor( Qt::WaitCursor );

  switch( myMode )
  {
  case BathymetryTextId:
    {
      DEBTRACE("BathymetryTextId");
      foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
        bath->GetShape()->TextLabels( isActivate, bath==baths.last() );
      //commit();
      if( isActivate )
        connect( selectionMgr(), SIGNAL( selectionChanged() ), this, SLOT( onSelectionChanged() ) );
      else
        disconnect( selectionMgr(), SIGNAL( selectionChanged() ), this, SLOT( onSelectionChanged() ) );
      break;
    }

  case BathymetryRescaleSelectionId:
    {
      if( isActivate )
      {
        foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
          bath->GetShape()->RescaleBySelection();
        isUpdateCS = true;
      }
      commit();
      break;
    }

  case BathymetryRescaleVisibleId:
    {
      if( isActivate )
      {
        foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
          bath->GetShape()->RescaleByVisible( activeViewWindow() );
        isUpdateCS = true;
      }
      commit();
      break;
    }

  case BathymetryRescaleUserId:
    {
      if( isActivate )
      {
        double min=0, max=0, lmin=0, lmax=0;
        bool first = true;
        foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
        {
          bath->GetShape()->GetRange( lmin, lmax );
          if( first || lmin < min )
            min = lmin;
          if( first || lmax > max )
            max = lmax;
          first = false;
        }

        HYDROGUI_BathymetryLimitsDlg dlg( module()->getApp()->desktop() );
        dlg.SetMinMax( min, max );
        if( dlg.exec()==QDialog::Accepted )
        {
          qApp->setOverrideCursor( Qt::WaitCursor );

          min = dlg.GetMin();
          max = dlg.GetMax();
          foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
            bath->GetShape()->Rescale( min, max );

          isUpdateCS = true;
          commit();
        }
        else
          abort();
        break;
      }
    }

  case BathymetryRescaleDefaultId:
    {
      if( isActivate )
      {
        foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
          bath->GetShape()->RescaleDefault();
        isUpdateCS = true;
      }
      commit();
      break;
    }
  }

  if( isUpdateCS )
    module()->getOCCDisplayer()->UpdateColorScale( getViewer( module() ) );

  qApp->restoreOverrideCursor();
}

void HYDROGUI_BathymetryOp::onSelectionChanged()
{
  DEBTRACE("onSelectionChanged");
  QList<Handle(HYDROGUI_BathymetryPrs)> baths = getShownBathymetries( module() );
  bool isUpdateCS = false;

  qApp->setOverrideCursor( Qt::WaitCursor );
  if( myMode==BathymetryTextId )
  {
    foreach( Handle(HYDROGUI_BathymetryPrs) bath, baths )
      bath->GetShape()->TextLabels( true, bath==baths.last() );
  }

  qApp->restoreOverrideCursor();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DeleteOp.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_DeleteDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_ProfileOp.h"

#include <HYDROData_Object.h>
#include <HYDROData_Document.h>

#include <LightApp_Application.h>

#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Study.h>

#include <QSet>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_DeleteOp::HYDROGUI_DeleteOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "DELETE" ) );
}

HYDROGUI_DeleteOp::~HYDROGUI_DeleteOp()
{
}

void HYDROGUI_DeleteOp::startOperation()
{
  DEBTRACE("startOperation");
  QList<SUIT_Operation*> operations = study()->operations();
  int n = operations.size();
  if( n>=2 )
  {
    SUIT_Operation* anOtherOp = operations[n-2];
    HYDROGUI_ProfileOp* aProfileOp = dynamic_cast<HYDROGUI_ProfileOp*>( anOtherOp );
    if( aProfileOp )
    {
      aProfileOp->deleteSelected();
      abort();
      return;
    }
  }

  HYDROGUI_Operation::startOperation();

  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( module() );
  if( aSeq.IsEmpty() )
  {
    abort();
    return;
  }

  bool anIsCanRemove = true;

  QStringList anObjNames;
  for( Standard_Integer anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObject = aSeq.Value( anIndex );
    if( anObject.IsNull() )
      continue;

    if ( !anObject->CanRemove() )
    {
      anIsCanRemove = false;
      break;
    }

    anObjNames.append( anObject->GetName() );
    DEBTRACE("obj name " << anObject->GetName().toStdString());
  }

  if ( !anIsCanRemove )
  {
    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "DELETE_OBJECTS" ), tr( "DELETE_OBJECTS_IMPOSIBLE" ) );
    abort();
    return;
  }

  anObjNames.removeDuplicates();

  // check the back-references
  QMap<QString,HYDROData_SequenceOfObjects> aBackObjects =
    HYDROGUI_Tool::GetObjectsBackReferences( doc(), anObjNames );

  QMap<QString,HYDROData_SequenceOfObjects>::const_iterator anIt = aBackObjects.begin(),
                                                            aLast = aBackObjects.end();
  QStringList aRefList;
  for ( ; anIt != aLast; anIt++ ) {
    QStringList aSeqList;
    HYDROData_SequenceOfObjects::Iterator anIter( anIt.value() );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Entity) anEntity = anIter.Value();
      if ( anEntity.IsNull() )
        continue;
      aSeqList.append( QString( tr( "DELETE_OBJECT_NAME" ) ).arg( anEntity->GetName() ) );
    }
    aRefList.append( QString( tr( "DELETE_OBJECT_IS_USED_FOR" ) ).arg( anIt.key() )
                                                          .arg( aSeqList.join( ", " ) ) );
  }

  //QString aNamesList = anObjNames.join( "\n" );
  if ( !aBackObjects.isEmpty() )
  {
    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "DELETE_OBJECTS" ),
                               tr( "DELETED_OBJECTS_HAS_BACKREFERENCES" ).arg( aRefList.join( "\n" ) ) );
    abort();
    return;
  }

  QString aWarningMsg;

  HYDROGUI_DeleteDlg aDeleteDlg( module()->getApp()->desktop() );
  aDeleteDlg.setObjectsToRemove( anObjNames );
  bool isLastStricklerRemoved = false;
  if ( aDeleteDlg.exec() != HYDROGUI_DeleteDlg::Accepted )
  {
    abort();
    return;
  }
  else
  {
    QStringList aTableNames = 
      HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_STRICKLER_TABLE, false );
    aTableNames += anObjNames;
    if ( aTableNames.toSet().size() == anObjNames.size() )
    {
      aWarningMsg = tr("DELETE_LAST_TABLE_WRN");
      isLastStricklerRemoved = true;
    }
  }

  if ( !aWarningMsg.isEmpty() ) {
    int anAnswer = SUIT_MessageBox::warning( module()->getApp()->desktop(),
                                             tr("WARNING"), aWarningMsg,
                                              QMessageBox::Yes | QMessageBox::No,
                                              QMessageBox::No );
    if ( anAnswer != QMessageBox::Yes ) {
      abort();
      return;
    }
  }

  startDocOperation();

  for( Standard_Integer anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObject = aSeq.Value( anIndex );

    if ( !anObject.IsNull() && !anObject->IsRemoved() ) {
      anObject->Remove();
      module()->setObjectRemoved( anObject );
    }
  }

  if( isLastStricklerRemoved )
  {
    HYDROGUI_DataModel* aModel = dynamic_cast<HYDROGUI_DataModel*>( module()->dataModel() );
    aModel->createDefaultStricklerTable( doc(), 0 );
  }

  commitDocOperation();

  module()->update( UF_Model | UF_Viewer | UF_OCCViewer | UF_VTKViewer );
  commit();

  module()->enableLCMActions();
}

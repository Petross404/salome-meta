// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_OrderedListWidget.h"

#include "HYDROGUI_ListModel.h"

#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <QLayout>
#include <QListView>
#include <QPushButton>
#include <QSignalMapper>
#include <QSortFilterProxyModel>


/**
  Constructor.
  @param theParent the parent widget
  @param theIconSize the icon size for arrow buttons
*/
HYDROGUI_OrderedListWidget::HYDROGUI_OrderedListWidget( QWidget* theParent, int theArrowIconSize )
: QWidget( theParent )
{
  // Main layout
  QHBoxLayout* aMainLayout = new QHBoxLayout( this );
  aMainLayout->setMargin( 0 );
  aMainLayout->setSpacing( 5 );

  // List view
  myList = new QListView( this );
  myList->setSelectionMode( QAbstractItemView::ExtendedSelection );
  // enable drag and drop
  myList->setDragEnabled( true );
  myList->setAcceptDrops( true );
  // configure drag and drop
  myList->setDropIndicatorShown( true );
  myList->setDragDropMode( QAbstractItemView::InternalMove );

  // Set the custom model
  HYDROGUI_ListModel* aModel = new HYDROGUI_ListModel();
  QSortFilterProxyModel* aFilteredModel = new QSortFilterProxyModel();
  aFilteredModel->setSourceModel( aModel );
  aFilteredModel->setFilterKeyColumn( 0 );
  aFilteredModel->setFilterRole( HYDROGUI_VisibleRole );

  myList->setModel( aFilteredModel );

  // Add list to the main layout
  aMainLayout->addWidget( myList );

  // Buttons top, up, down, bottom
  if ( theArrowIconSize > 0 ) {
    SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
    myTop = new QPushButton( this );
    myTop->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "ARROW_TOP_ICO" ) ) );
    myTop->setIconSize( QSize( theArrowIconSize, theArrowIconSize ) );
    myTop->setToolTip( tr( "TOP_TLT" ) );
    myUp = new QPushButton( this );
    myUp->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "ARROW_UP_ICO" ) ) );
    myUp->setIconSize( myTop->iconSize() );
    myUp->setToolTip( tr( "UP_TLT" ) );
    myDown = new QPushButton( this );
    myDown->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "ARROW_DOWN_ICO" ) ) );
    myDown->setIconSize( myTop->iconSize() );
    myDown->setToolTip( tr( "DOWN_TLT" ) );
    myBottom = new QPushButton( this );
    myBottom->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "ARROW_BOTTOM_ICO" ) ) );
    myBottom->setIconSize( myTop->iconSize() );
    myBottom->setToolTip( tr( "BOTTOM_TLT" ) );

    // Add buttons to the main layout
    QVBoxLayout* aListButtonsLayout = new QVBoxLayout();
    aListButtonsLayout->addWidget( myTop );
    aListButtonsLayout->addWidget( myUp );
    aListButtonsLayout->addWidget( myDown );
    aListButtonsLayout->addWidget( myBottom );
    aListButtonsLayout->addStretch();
    
    aMainLayout->addLayout( aListButtonsLayout );

    // Buttons connections
    QSignalMapper* aSignalMapper = new QSignalMapper( this );
    aSignalMapper->setMapping( myTop, HYDROGUI_ListModel::Top );
    aSignalMapper->setMapping( myUp, HYDROGUI_ListModel::Up );
    aSignalMapper->setMapping( myDown, HYDROGUI_ListModel::Down );
    aSignalMapper->setMapping( myBottom, HYDROGUI_ListModel::Bottom );
    connect( myTop, SIGNAL( clicked() ), aSignalMapper, SLOT( map() ) );
    connect( myUp, SIGNAL( clicked() ), aSignalMapper, SLOT( map() ) );
    connect( myDown, SIGNAL( clicked() ), aSignalMapper, SLOT( map() ) );
    connect( myBottom, SIGNAL( clicked() ), aSignalMapper, SLOT( map() ) );
    connect( aSignalMapper, SIGNAL( mapped( int ) ), this, SLOT( onMove( int ) ) );
  } else {
    myTop = myUp = myDown = myBottom = 0;
  }

  connect ( myList->selectionModel(), SIGNAL( selectionChanged( QItemSelection, QItemSelection ) ), 
            this, SIGNAL( selectionChanged() ) );
  
  connect ( myList->selectionModel(), SIGNAL( selectionChanged( QItemSelection, QItemSelection ) ), 
            this, SLOT( onSelectionChanged() ) );

  // Initialize
  setHiddenObjectsShown( true );
  setVisibilityIconShown( true );
}

void HYDROGUI_OrderedListWidget::onSelectionChanged()
{
  QStringList selectedEntries = getSelectedEntries();

}

/**
  Destructor.
*/
HYDROGUI_OrderedListWidget::~HYDROGUI_OrderedListWidget()
{
}

/**
  Set the list of objects (which supposed to be ordered by the widget).
  @param theObjects the list of pairs (object; visibility)
*/
void HYDROGUI_OrderedListWidget::setObjects( const HYDROGUI_ListModel::Object2VisibleList& theObjects )
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    aModel->setObjects( theObjects );
  }
}

void HYDROGUI_OrderedListWidget::setBackgroundColor (int theInd, QColor theColor)
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    aModel->setBackgroundColor( theInd, theColor );
  }
}

void HYDROGUI_OrderedListWidget::setBackgroundColor (QString theName, QColor theColor)
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if (aModel)
  {
    QList<Handle(HYDROData_Entity)> anObjects = aModel->getObjects();
    int ind = -1;
    int cur_ind = 0;
    foreach (Handle(HYDROData_Entity) ent, anObjects)
    {
      if (ent->GetName() == theName)
      {
        ind = cur_ind;
        break;
      }
      cur_ind++;
    }
    if (ind != -1)
      aModel->setBackgroundColor( ind, theColor );
  }
}

void HYDROGUI_OrderedListWidget::clearAllBackgroundColors ()
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    aModel->clearAllBackgroundColors( );
  }
}


 QColor HYDROGUI_OrderedListWidget::getBackgroundColor (int theInd) const
 {
   HYDROGUI_ListModel* aModel = getSourceModel();
   if( aModel ) {
     return aModel->getBackgroundColor( theInd );
   }
 }

/**
  Returns the ordered list of objects.
  @return the list of objects
*/
QList<Handle(HYDROData_Entity)> HYDROGUI_OrderedListWidget::getObjects() const
{
  QList<Handle(HYDROData_Entity)> anObjects;

  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    anObjects = aModel->getObjects();
  }

  return anObjects;
}

/**
  Add the object to the end of the list.
  @param theObjects the pair (object; visibility)
*/
void HYDROGUI_OrderedListWidget::addObject( const HYDROGUI_ListModel::Object2Visible& theObject )
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    aModel->addObject(theObject);
  }
}

/**
  Remove the object from the list.
  @param theObjectName the name of the object to remove
*/
void HYDROGUI_OrderedListWidget::removeObjectByName( const QString& theObjectName )
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    aModel->removeObjectByName(theObjectName);
  }
}

/**
  Set whether the hidden objects are presented in the list.
  @param theIsToShow if true - the hidden objects will be shown in the list
*/
void HYDROGUI_OrderedListWidget::setHiddenObjectsShown( const bool theIsToShow )
{
  myIsHiddenObjectsShown = theIsToShow; 

  QSortFilterProxyModel* aFilterModel = dynamic_cast<QSortFilterProxyModel*>( myList->model() );
  QString anExpr = theIsToShow ? "true|false" : "true";
  aFilterModel->setFilterRegExp( anExpr );
}

/**
  Set whether the visibility icon (eye icon) are presented in the list.
  @param theIsToShow if true - the eye icon will be shown in the list
*/
void HYDROGUI_OrderedListWidget::setVisibilityIconShown( const bool theIsToShow )
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel ) {
    aModel->setDecorationEnabled( theIsToShow );
  }
}

/**
  Get entries of the selected objects.
  @return the list of entries
*/
QStringList HYDROGUI_OrderedListWidget::getSelectedEntries() const
{ 
  QStringList anEntries;

  QSortFilterProxyModel* aFilterModel = 
    dynamic_cast<QSortFilterProxyModel*>( myList->model() );
  if( aFilterModel ) {
    HYDROGUI_ListModel* aSourceModel = 
      dynamic_cast<HYDROGUI_ListModel*>( aFilterModel->sourceModel() );
    if ( aSourceModel ) {
      QModelIndexList aSelectedIndexes = myList->selectionModel()->selectedIndexes();
      foreach ( const QModelIndex& anIndex, aSelectedIndexes ) {
        QModelIndex aSourceIndex = aFilterModel->mapToSource( anIndex );
        QString anEntry = aSourceModel->data( aSourceIndex, HYDROGUI_EntryRole ).toString();
        anEntries << anEntry;
      }
    }
  }

  return anEntries;
}

/**
  Set objects with the given entries selected (other objects will deselected).
  @param theEntries the list of entries
*/
void HYDROGUI_OrderedListWidget::setSelectedEntries( const QStringList& theEntries ) const
{
  QSortFilterProxyModel* aFilterModel = dynamic_cast<QSortFilterProxyModel*>( myList->model() );
  if( !aFilterModel ) {
    return;
  }
  HYDROGUI_ListModel* aSourceModel = dynamic_cast<HYDROGUI_ListModel*>( aFilterModel->sourceModel() );
  if ( !aSourceModel ) {
    return;
  }
  QItemSelectionModel* aSelectionModel = myList->selectionModel();
  if ( !aSelectionModel ) {
    return;
  }
  
  for ( int aRow = 0 ; aRow < aSourceModel->rowCount() ; aRow++ ) {
    QModelIndex anIndex = aSourceModel->index( aRow, 0 );
    if ( !anIndex.isValid() ) {
      continue;
    }

    QString anEntry = aSourceModel->data( anIndex, HYDROGUI_EntryRole ).toString();

    bool isToSelect = theEntries.contains( anEntry );
    QModelIndex aProxyModelIndex = aFilterModel->mapFromSource( anIndex );
    QItemSelectionModel::SelectionFlags aSelectionFlags =
      isToSelect ? QItemSelectionModel::Select : QItemSelectionModel::Deselect;
    aSelectionModel->select( aProxyModelIndex, aSelectionFlags );
  }
}

/**
  Get names of the selected objects.
  @return the list of names
*/
QStringList HYDROGUI_OrderedListWidget::getSelectedNames() const
{
  QStringList aNames;

  QSortFilterProxyModel* aFilterModel = 
    dynamic_cast<QSortFilterProxyModel*>( myList->model() );
  if ( aFilterModel ) {
    HYDROGUI_ListModel* aSourceModel = 
      dynamic_cast<HYDROGUI_ListModel*>( aFilterModel->sourceModel() );
    if ( aSourceModel ) {
      QModelIndexList aSelectedIndexes = myList->selectionModel()->selectedIndexes();
      foreach ( const QModelIndex& anIndex, aSelectedIndexes ) {
        QModelIndex aSourceIndex = aFilterModel->mapToSource( anIndex );
        QString anEntry = aSourceModel->data( aSourceIndex, Qt::DisplayRole ).toString();
        aNames << anEntry;
      }
    }
  }

  return aNames;
}

/**
  Get names of all objects.
  @return the list of names
*/
QStringList HYDROGUI_OrderedListWidget::getAllNames() const
{
  QStringList aNames;


  foreach ( const Handle(HYDROData_Entity)& anObject, getObjects() ) {
    aNames << anObject->GetName();
  }

  return aNames;
}

/**
  Slot called on top, up, down and bottom button click.
  @param theType the move operation type
*/
void HYDROGUI_OrderedListWidget::onMove( int theType )
{
  bool isMoved = false;

  QSortFilterProxyModel* aFilterModel = dynamic_cast<QSortFilterProxyModel*>( myList->model() );
  if( aFilterModel ) {
    HYDROGUI_ListModel* aModel = dynamic_cast<HYDROGUI_ListModel*>( aFilterModel->sourceModel() );
    if( aModel ) {
      QModelIndexList aSelectedIndexes = myList->selectionModel()->selectedIndexes();
      QModelIndexList aSelectedSourceIndexes;
      foreach ( const QModelIndex& anIndex, aSelectedIndexes ) {
        aSelectedSourceIndexes << aFilterModel->mapToSource( anIndex );
      }
      QList<int> aSelectedIds = aModel->getIds( aSelectedSourceIndexes );
      isMoved = aModel->move( aSelectedIds, ( HYDROGUI_ListModel::OpType )theType, 
                              !myIsHiddenObjectsShown );
    }
  }

  if ( isMoved )
  {
    emit orderChanged();
  }
}

/**
  Returns the list source model.
  @return the source model
*/
HYDROGUI_ListModel* HYDROGUI_OrderedListWidget::getSourceModel() const
{
  HYDROGUI_ListModel* aSourceModel = 0;

  QSortFilterProxyModel* aFilterModel = dynamic_cast<QSortFilterProxyModel*>( myList->model() );
  if( aFilterModel ) {
    aSourceModel = dynamic_cast<HYDROGUI_ListModel*>( aFilterModel->sourceModel() );
  }

  return aSourceModel;
}

/**
  Enable/disable ordering (i.e. enable/disable arrow buttons and drag and drop).
  @param theIsToEnable if true - ordering will bw enabled
*/
void HYDROGUI_OrderedListWidget::setOrderingEnabled( const bool theIsToEnable )
{
  // enable/disable arrow buttons
  if ( myTop && myUp && myDown && myBottom ) {
    myTop->setEnabled( theIsToEnable );
    myUp->setEnabled( theIsToEnable );
    myDown->setEnabled( theIsToEnable );
    myBottom->setEnabled( theIsToEnable );
  }

  // enable/disable drag and drop
  myList->setDragEnabled( theIsToEnable );
}

void HYDROGUI_OrderedListWidget::undoLastMove()
{
  HYDROGUI_ListModel* aModel = getSourceModel();
  if( aModel )
    aModel->undoLastMove();
}

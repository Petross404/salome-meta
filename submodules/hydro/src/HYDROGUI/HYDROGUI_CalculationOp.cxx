// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_CalculationOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_CalculationDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Zone.h"
#include "HYDROGUI_Region.h"

#include <HYDROData_PolylineXY.h>
#include <HYDROData_ShapesGroup.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_Object.h>
#include <HYDROData_Tool.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_BCPolygon.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_DataOwner.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>
#include <SUIT_DataBrowser.h>

#include <QApplication>
#include <QKeySequence>
#include <QShortcut>
#include <QPushButton>
#include <BRep_Builder.hxx>
#include <TopoDS.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>

#include <HYDROData_CompleteCalcCase.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"


HYDROGUI_CalculationOp::HYDROGUI_CalculationOp( HYDROGUI_Module* theModule, bool theIsEdit, bool IsComplete )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit ),
  myActiveViewManager( NULL ),
  myPreviewViewManager( NULL ),
  myShowGeomObjects( true ),
  myShowLandCoverMap( false ),
  myShowZones( false ),
  myIsComplete (IsComplete)
{
  QString aDlgName;
  if (myIsComplete)
    aDlgName =  tr( "COMPLETE_CALCULATION" );
  else
    aDlgName = myIsEdit ? tr( "EDIT_CALCULATION" ) : tr( "CREATE_CALCULATION" );
  setName( aDlgName );
}

HYDROGUI_CalculationOp::~HYDROGUI_CalculationOp()
{
  closePreview();
}

void HYDROGUI_CalculationOp::startOperation()
{
  HYDROGUI_Operation::startOperation();
  
  // Begin transaction
  startDocOperation();

  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  SUIT_DataBrowser* aOb = ((LightApp_Application*)module()->application())->objectBrowser();
  QList<QShortcut*> aShortcuts = aOb->findChildren<QShortcut*>();
  QShortcut* aShortcut;
  foreach( aShortcut, aShortcuts )
  {
    if ( aShortcut->key() == 
      QKeySequence(((LightApp_Application*)module()->application())->objectBrowser()->shortcutKey( 
      SUIT_DataBrowser::RenameShortcut ) ) )
    {
      aShortcut->setEnabled( false );
    }
  }

  aPanel->reset();
  QStringList aList;
  QStringList anEntryList;
  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetGeometryObjects( module() );
  getNamesAndEntries( aSeq, aList, anEntryList );
  
  QStringList List1;
  QStringList EList1;

  //add intersection polylines
  AddInterPolylinesToList(aList, anEntryList);
  
  aPanel->setAllGeomObjects( aList, anEntryList );

  // Get all polylines
  aList.clear();
  anEntryList.clear();
  HYDROData_Iterator anIter( doc(), KIND_POLYLINEXY );
  Handle(HYDROData_PolylineXY) aPolylineObj;
  QString aPolylineName;
  for ( ; anIter.More(); anIter.Next() )
  {
    aPolylineObj = Handle(HYDROData_PolylineXY)::DownCast( anIter.Current() );

    if ( !aPolylineObj.IsNull() && aPolylineObj->IsClosed(false) )
    { 
//      // Check the polyline shape
//      TopoDS_Shape aPolylineShape = aPolylineObj->GetShape();
//      if ( !aPolylineShape.IsNull() && aPolylineShape.ShapeType() == TopAbs_WIRE ) {
        aPolylineName = aPolylineObj->GetName();
        if ( !aPolylineName.isEmpty() )
        {
          aList.append( aPolylineName );
          anEntryList.append( HYDROGUI_DataObject::dataObjectEntry( aPolylineObj ) );
        }
//      }
    }
  }
  aPanel->setPolylineNames( aList, anEntryList );

  QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_CALCULATION_CASE_NAME" ) );

  myEditedObject.Nullify();

  if ( myIsEdit )
  {
    myEditedObject = Handle(HYDROData_CalculationCase)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if ( !myEditedObject.IsNull() )
    {
      aPanel->setMode( myEditedObject->GetAssignmentMode() );
      anObjectName = myEditedObject->GetName();
      aPolylineObj = myEditedObject->GetBoundaryPolyline();
      if ( aPolylineObj.IsNull() )
      {
        aPanel->setBoundary( QString() );
      }
      else
      {
        aPolylineName = aPolylineObj->GetName();
        aPanel->setBoundary( aPolylineName );
      }

      aSeq = myEditedObject->GetGeometryObjects();      
      HYDROData_SequenceOfObjects anInterPolyList = myEditedObject->GetInterPolyObjects();
      aSeq.Append(anInterPolyList);

      getNamesAndEntries( aSeq, aList, anEntryList );
      if (!myIsComplete)
        aPanel->includeGeomObjects( aList );
      else 
      {
        //aPanel->includeGeomObjects( aList );
        aPanel->setAlreadyAddedGeomObjects( aList );
        //myIncObjAtStart = aSeq;
        aPanel->hideAvailableGeomObjects( aList );
        aPanel->setGeomOrderingEnabled(true);
      }

      // set rules
      setRules( HYDROData_CalculationCase::DataTag_CustomRules );      
    }
  }
  else
  {
    myEditedObject =
      Handle(HYDROData_CalculationCase)::DownCast( doc()->CreateObject( KIND_CALCULATION ) );
    myEditedObject->SetName( anObjectName );
    myEditedObject->SetAssignmentMode( (HYDROData_CalculationCase::AssignmentMode)aPanel->getMode() );
  }

  aPanel->setObjectName( anObjectName );
  aPanel->setEditedObject( myEditedObject );

  setGeomObjectsVisible( true );

  createPreview( false );
}

void HYDROGUI_CalculationOp::AddInterPolylinesToList(QStringList& theList, QStringList& theEntryList)
{
  HYDROData_Iterator anIter( doc(), KIND_POLYLINEXY );
  Handle(HYDROData_PolylineXY) aPolylineObj;
  QString aPolylineName;
  for ( ; anIter.More(); anIter.Next() )
  {
    aPolylineObj = Handle(HYDROData_PolylineXY)::DownCast( anIter.Current() );
    if ( !aPolylineObj.IsNull())
    { 
      aPolylineName = aPolylineObj->GetName();
      if ( !aPolylineName.isEmpty() && !theList.contains(aPolylineName) )
      {
        theList.append( aPolylineName );
        theEntryList.append(HYDROGUI_DataObject::dataObjectEntry( aPolylineObj ));
      }
    }
  }
}

void HYDROGUI_CalculationOp::getNamesAndEntries( const HYDROData_SequenceOfObjects& theSeq, 
                                                QStringList& theNames, QStringList& theEntries ) const
{
 
  theNames.clear();
  theEntries.clear();
  HYDROData_SequenceOfObjects::Iterator anIter( theSeq );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anEntity = anIter.Value();
    //if ( !HYDROData_Tool::IsGeometryObject( anEntity ) )
    //  continue;

    theNames.append( anEntity->GetName() );
    theEntries.append( HYDROGUI_DataObject::dataObjectEntry( anEntity ) );
  }
}

void HYDROGUI_CalculationOp::abortOperation()
{
  closePreview();
  // Abort transaction
  abortDocOperation();
  HYDROGUI_Operation::abortOperation();
  module()->getApp()->updateObjectBrowser();
}

void HYDROGUI_CalculationOp::commitOperation()
{
  closePreview();
  // Commit transaction
  commitDocOperation();
  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_CalculationOp::createInputPanel() const
{
  HYDROGUI_CalculationDlg* aPanel = new HYDROGUI_CalculationDlg( module(), getName(), myIsComplete );

  // Connect signals and slots
  connect( aPanel, SIGNAL( changeMode( int ) ), SLOT( onChangeMode( int ) ) );  
  connect( aPanel, SIGNAL( addObjects() ), SLOT( onAddObjects() ) );
  connect( aPanel, SIGNAL( removeObjects() ), SLOT( onRemoveObjects() ) );
  connect( aPanel, SIGNAL( addGroups() ), SLOT( onAddGroups() ) );
  connect( aPanel, SIGNAL( removeGroups() ), SLOT( onRemoveGroups() ) );

  connect( aPanel, SIGNAL( addBoundaryPolygons() ), SLOT( onAddBoundaryPolygons() ) );
  connect( aPanel, SIGNAL( removeBoundaryPolygons() ), SLOT( onRemoveBoundaryPolygons() ) );

  connect( aPanel, SIGNAL( orderChanged( bool& ) ), SLOT( onOrderChanged( bool& ) ) );

  connect( aPanel, SIGNAL( ruleChanged( bool& ) ), SLOT( onRuleChanged( bool& ) ) );

  connect( aPanel, SIGNAL( Next( const int ) ), SLOT( onNext( const int ) ) );
  connect( aPanel, SIGNAL( Back( const int ) ), SLOT( onHideZones( const int ) ) );
  //connect( aPanel, SIGNAL( clicked( SUIT_DataObject* ) ), SLOT( onSelected( SUIT_DataObject* ) ) );
  connect( aPanel, SIGNAL( setMergeType( int, QString& ) ), SLOT( onSetMergeType( int, QString& ) ) );
  connect( aPanel, SIGNAL( moveZones( SUIT_DataObject*, const QList<SUIT_DataObject*>& ) ),
    SLOT( onMoveZones( SUIT_DataObject*, const QList<SUIT_DataObject*>& ) ) );
  connect( aPanel, SIGNAL( createRegion( const QList<SUIT_DataObject*>& ) ),
    SLOT( onCreateRegion( const QList<SUIT_DataObject*>& ) ) );
  connect( aPanel, SIGNAL( clickedInZonesBrowser( SUIT_DataObject* ) ),
    SLOT( onClickedInZonesBrowser( SUIT_DataObject* ) ) );
  connect( aPanel, SIGNAL( objectsSelected() ), 
           SLOT( onObjectsSelected() ) );
  connect( aPanel, SIGNAL( landCoverMapSelected( const QString & ) ), 
           SLOT( onLandCoverMapSelected( const QString & ) ) );
  connect( aPanel, SIGNAL( boundarySelected( const QString & ) ), 
    SLOT( onBoundarySelected( const QString & ) ) );
  connect( aPanel, SIGNAL( StricklerTableSelected( const QString & ) ), 
    SLOT( onStricklerTableSelected( const QString & ) ) );

  connect( aPanel, SIGNAL( regenerateColors() ), this, 
    SLOT( onRegenerateColors() ) );

  return aPanel;
}

void HYDROGUI_CalculationOp::onBoundarySelected ( const QString & theObjName )
{
  bool anIsToUpdateViewer = false;

  // Remove the old boundary from the operation viewer
  Handle(HYDROData_PolylineXY) aPrevPolyline = 
    myEditedObject->GetBoundaryPolyline();
  if ( !aPrevPolyline.IsNull() )
  {
    setObjectVisibility( aPrevPolyline, false );
    anIsToUpdateViewer = true;
  }

  // Set the selected boundary polyline to the calculation case
  Handle(HYDROData_PolylineXY) aNewPolyline = Handle(HYDROData_PolylineXY)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), theObjName, KIND_POLYLINEXY ) );
  myEditedObject->SetBoundaryPolyline( aNewPolyline );

  if ( myPreviewViewManager )
  {
    OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer();
    if ( aViewer )
    {
      if ( !aNewPolyline.IsNull() )
      {
        Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
        if ( !aCtx.IsNull() )
        {
          setObjectVisibility( aNewPolyline, true );
          anIsToUpdateViewer = true;
        }
      }

      if ( anIsToUpdateViewer )
        module()->update( UF_OCCViewer );
    }
  }
}

void HYDROGUI_CalculationOp::onStricklerTableSelected ( const QString & theObjName )
{
  bool anIsToUpdateViewer = false;

  // Remove old presentation of land cover map from the operation viewer  
  Handle(HYDROData_LandCoverMap) aLandCoverMap = myEditedObject->GetLandCoverMap();
  if ( !aLandCoverMap.IsNull() )
  {
    setObjectVisibility( aLandCoverMap, false );
    anIsToUpdateViewer = true;
  }

  // Set the selected Strickler table to the calculation case
  Handle(HYDROData_StricklerTable) aNewStricklerTable = Handle(HYDROData_StricklerTable)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), theObjName, KIND_STRICKLER_TABLE ) );
  myEditedObject->SetStricklerTable( aNewStricklerTable );

  if ( myPreviewViewManager )
  {
    OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer();
    if ( aViewer )
    {
      if ( !aNewStricklerTable.IsNull() )
      {
        Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
        if ( !aCtx.IsNull() )
        {
          if ( !aLandCoverMap.IsNull() )
          {
            setObjectVisibility( aLandCoverMap, true );
            anIsToUpdateViewer = true;
          }          
        }
      }

      if ( anIsToUpdateViewer )
        module()->update( UF_OCCViewer );
    }
  }
}

void HYDROGUI_CalculationOp::onObjectsSelected()
{
  DEBTRACE("onObjectsSelected");
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );

  QStringList aSelectedObjs = aPanel->getSelectedGeomObjects();
  QMap<QString, bool> aSelectedObjsMap;
  foreach( QString aName, aSelectedObjs )
    aSelectedObjsMap[aName] = true;


  // Select the appropriate geometry object shape in the viewer
  selectionMgr()->clearSelected();

  // Unhighlight all objects except selected
  HYDROGUI_Shape* aShape = 0, *aLastShape = 0;
  Handle(HYDROData_Entity) anEntity;
  HYDROData_SequenceOfObjects aSeq = myEditedObject->GetGeometryObjects();
  HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
  bool isSelected;
  QString aName;
  for ( ; anIter.More(); anIter.Next() )
  {
    anEntity = anIter.Value();
    if ( !anEntity.IsNull() )
    {
      aShape = module()->getObjectShape( HYDROGUI_Module::VMR_PreviewCaseZones, anEntity );
      if ( aShape )
      {
        aName = anEntity->GetName();
        isSelected = aSelectedObjsMap.contains( aName );
        aShape->highlight( isSelected, false );
        aShape->update( false, false );
        aLastShape = aShape;
      }
    }
  }
  if( aLastShape )
    aLastShape->update( true, false );
}

void HYDROGUI_CalculationOp::onLandCoverMapSelected( const QString & theObjName )
{
  bool anIsToUpdateViewer = false;
  
  // Remove old presentation of land cover map from the operation viewer  
  Handle(HYDROData_LandCoverMap) aPrevLandCoverMap = myEditedObject->GetLandCoverMap();
  if ( !aPrevLandCoverMap.IsNull() )
  {
    setObjectVisibility( aPrevLandCoverMap, false );
    anIsToUpdateViewer = true;
  }

  // Select the appropriate land cover map shape in the viewer
  selectionMgr()->clearSelected();

  // Set the selected land cover map to the calculation case
  Handle(HYDROData_LandCoverMap) aNewLandCoverMap = Handle(HYDROData_LandCoverMap)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), theObjName, KIND_LAND_COVER_MAP ) );
  myEditedObject->SetLandCoverMap( aNewLandCoverMap );

  createPreview( true );
}

void HYDROGUI_CalculationOp::onClickedInZonesBrowser( SUIT_DataObject* theItem )
{
  DEBTRACE("onClickedInZonesBrowser");
  HYDROGUI_Region* aRegionItem = dynamic_cast<HYDROGUI_Region*>(theItem);
  HYDROGUI_Zone* aZoneItem;
  selectionMgr()->clearSelected();
  if ( aRegionItem )
  {
    DEBTRACE("Region");
    // Select a region in preview
    SUIT_DataOwnerPtrList aList( true );
    DataObjectList aZones = aRegionItem->children();
    for ( int i = 0; i < aZones.length(); i++ )
    {
      aZoneItem = dynamic_cast<HYDROGUI_Zone*>(aZones.at(i));
      if ( aZoneItem )
      {
        aList.append( SUIT_DataOwnerPtr( new LightApp_DataOwner( aZoneItem->entry() ) ) );
      }
    }
    selectionMgr()->setSelected( aList );
  }
  else
  {
    // select a single zone
    aZoneItem = dynamic_cast<HYDROGUI_Zone*>(theItem);
    if ( aZoneItem )
    {
      DEBTRACE("Zone");
      SUIT_DataOwnerPtrList aList( true );
      aList.append( SUIT_DataOwnerPtr( new LightApp_DataOwner( aZoneItem->entry() ) ) );
      selectionMgr()->setSelected( aList );
    }
  }
}

void HYDROGUI_CalculationOp::onMoveZones( SUIT_DataObject* theRegionItem,
                                          const QList<SUIT_DataObject*>& theZonesList )
{
  HYDROGUI_Region* aRegion = dynamic_cast<HYDROGUI_Region*>(theRegionItem);
  if ( aRegion )
  {
    QList<HYDROGUI_Zone*> aZonesList;
    HYDROGUI_Zone* aZone;
    // Get a list of dropped zones
    for ( int i = 0; i < theZonesList.length(); i++ )
    {
      aZone = dynamic_cast<HYDROGUI_Zone*>( theZonesList.at( i ) );
      if ( aZone )
      {
        aZonesList.append( aZone );
      }
    }
    if ( aZonesList.length() > 0 )
    {
      aRegion->addZones( aZonesList );
      HYDROGUI_CalculationDlg* aPanel = 
        ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
      if ( aPanel )
        aPanel->refreshZonesBrowser();
      createPreview( false, false, false );
    }
  }
}

void HYDROGUI_CalculationOp::onCreateRegion( const QList<SUIT_DataObject*>& theZonesList )
{
  if ( createRegion( theZonesList ) )
  {
    HYDROGUI_CalculationDlg* aPanel = 
      ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
    if ( aPanel )
    {
      aPanel->refreshZonesBrowser();
    }
    createPreview( false, false, false  );
  }
}

void HYDROGUI_CalculationOp::onSetMergeType( int theMergeType, QString& theMergeObjectName )
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( aPanel )
  {
    HYDROGUI_Zone* aZone = aPanel->getCurrentZone();
    if ( aZone )
    {
      aZone->setMergeType( theMergeType, theMergeObjectName );
      HYDROGUI_Shape* aShape = module()->getObjectShape( HYDROGUI_Module::VMR_PreviewCaseZones, aZone->modelObject() );
      if ( aShape )
      {
        aShape->update( true, false );
      }
    }
    aPanel->refreshZonesBrowser();
  }
}

void HYDROGUI_CalculationOp::onAddObjects()
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  // Add geometry objects selected in the module browser to the calculation case
  QStringList aSelectedList = aPanel->getSelectedAvailableGeomObjects();
  if ( aSelectedList.isEmpty() || !confirmRegionsChange() )
    return;

  QStringList anAddedList;
  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_Object) anObject = Handle(HYDROData_Object)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at( i ) ) );
    if ( anObject.IsNull() )
      continue;

    if ( myEditedObject->AddGeometryObject( anObject ) )
      anAddedList.append( anObject->GetName() );
  }

  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_PolylineXY) aPoly = Handle(HYDROData_PolylineXY)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at( i ) ) );
    if ( aPoly.IsNull() )
      continue;

    if ( myEditedObject->AddInterPoly( aPoly ) )
      anAddedList.append( aPoly->GetName() );
  }

  if ( !anAddedList.isEmpty() )
  {
    aPanel->includeGeomObjects( anAddedList );
    createPreview( false );
  }
}

void HYDROGUI_CalculationOp::onRemoveObjects()
{
  // Remove selected objects from the calculation case
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QStringList aSelectedList = aPanel->getSelectedGeomObjects();

  if (myIsComplete)
  {
    QSet<QString> selMap = aSelectedList.toSet();
  }


  if ( aSelectedList.isEmpty() || !confirmRegionsChange() )
    return;

  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_Object) anObject = Handle(HYDROData_Object)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at(i) ) );
    if ( anObject.IsNull() )
      continue;

    setObjectVisibility( anObject, false );
    myEditedObject->RemoveGeometryObject( anObject );
  }

  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_PolylineXY) aPoly = Handle(HYDROData_PolylineXY)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at(i) ) );
    if ( aPoly.IsNull() )
      continue;

    setObjectVisibility( aPoly, false );
    myEditedObject->RemoveInterPolyObject ( aPoly ); 
  }

  module()->update( UF_OCCViewer );
  aPanel->excludeGeomObjects( aSelectedList );
}

bool HYDROGUI_CalculationOp::confirmRegionsChange() const
{

  // Check if the case is already modified or not
  bool isConfirmed = myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d );

  if ( !isConfirmed )
  {
  
   if (myIsComplete)
   {
     //SUIT_MessageBox::information(module()->getApp()->desktop(),
     //                         tr( "REGIONS_CHANGED" ),
     //                         tr( "COMPLETE_OP_WILL_BE_PERFORMED" ));
     return true;
   }

    // If not modified check if the case has already defined regions with zones
    HYDROData_SequenceOfObjects aSeq = myEditedObject->GetRegions();
    if ( aSeq.Length() > 0 )
    {
      // If there are already defined zones then ask a user to confirm zones recalculation
      isConfirmed = ( SUIT_MessageBox::question( module()->getApp()->desktop(),
                               tr( "REGIONS_CHANGED" ),
                               tr( "CONFIRM_SPLITTING_ZONES_RECALCULATION_REGIONS" ),
                               QMessageBox::Yes | QMessageBox::No,
                               QMessageBox::No ) == QMessageBox::Yes );
    }
    else
    {
      isConfirmed = true; // No regions - no zones - nothing to recalculate
    }
  }
  return isConfirmed;
}

bool HYDROGUI_CalculationOp::confirmOrderChange() const
{
  // Check if the case is already modified or not
  bool isConfirmed = myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d );
  if ( !isConfirmed )
  {
    // If not modified check if the case has already defined regions with zones
    HYDROData_SequenceOfObjects aSeq = myEditedObject->GetRegions();
    if ( aSeq.Length() > 0 )
    {
      // If there are already defined zones then ask a user to confirm zones recalculation
      isConfirmed = ( SUIT_MessageBox::question( module()->getApp()->desktop(),
                               tr( "ORDER_CHANGED" ),
                               tr( "CONFIRM_SPLITTING_ZONES_RECALCULATION_REGIONS" ),
                               QMessageBox::Yes | QMessageBox::No,
                               QMessageBox::No ) == QMessageBox::Yes );
    }
    else
    {
      isConfirmed = true; // No regions - no zones - nothing to recalculate
    }
  }
  return isConfirmed;
}

bool HYDROGUI_CalculationOp::confirmRuleChange() const
{
  // Check if the case is already modified or not
  bool isConfirmed = myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d );
  if ( !isConfirmed )
  {
    // If not modified check if the case has already defined regions with zones
    HYDROData_SequenceOfObjects aSeq = myEditedObject->GetRegions();
    if ( aSeq.Length() > 0 )
    {
      // If there are already defined zones then ask a user to confirm zones recalculation
      isConfirmed = ( SUIT_MessageBox::question( module()->getApp()->desktop(),
                               tr( "RULE_CHANGED" ),
                               tr( "CONFIRM_SPLITTING_ZONES_RECALCULATION_REGIONS" ),
                               QMessageBox::Yes | QMessageBox::No,
                               QMessageBox::No ) == QMessageBox::Yes );
    }
    else
    {
      isConfirmed = true; // No regions - no zones - nothing to recalculate
    }
  }
  return isConfirmed;
}

bool HYDROGUI_CalculationOp::confirmModeChange() const
{
  // Check if the case is already modified or not
  bool isConfirmed = myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d );
  if ( !isConfirmed )
  {
    // If not modified check if the case has already defined regions with zones
    HYDROData_SequenceOfObjects aSeq = myEditedObject->GetRegions();
    if ( aSeq.Length() > 0 )
    {
      // If there are already defined zones then ask a user to confirm zones recalculation
      isConfirmed = ( SUIT_MessageBox::question( module()->getApp()->desktop(),
                               tr( "MODE_CHANGED" ),
                               tr( "CONFIRM_SPLITTING_ZONES_RECALCULATION_MODE" ),
                               QMessageBox::Yes | QMessageBox::No,
                               QMessageBox::No ) == QMessageBox::Yes );
    }
    else
    {
      isConfirmed = true; // No regions - no zones - nothing to recalculate
    }
  }
  return isConfirmed;
}

bool HYDROGUI_CalculationOp::confirmContinueWithWarning( const HYDROData_Warning& theWarning ) const
{
  HYDROData_WarningType aType = theWarning.Type;
  if ( aType == WARN_OK ) {
    return true;
  }

  QString aTitle;
  QString aMsg;
  switch ( aType )
  {
    case WARN_EMPTY_REGIONS:
      aTitle = tr( "EMPTY_REGIONS" );
      aMsg = tr( "CONFIRM_CONTINUE_WITH_OBJECTS_NOT_INCLUDED_TO_REGION" ).arg( theWarning.Data );
      break;
    default:
      aTitle = tr( "WARNING" );
      aMsg = theWarning.Data;
  }


  int anAnswer = SUIT_MessageBox::warning( module()->getApp()->desktop(),
                                           aTitle, aMsg,
                                           QMessageBox::Yes | QMessageBox::No,
                                           QMessageBox::No );

  return ( anAnswer == QMessageBox::Yes );
}

bool HYDROGUI_CalculationOp::processApply( int&     theUpdateFlags,
                                           QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  if( !myIsEdit )
  {
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( myEditedObject );
    theBrowseObjectsEntries.append( anEntry );
  }

  // For manual mode priority rules are redundant
  if ( aPanel->getMode() == HYDROData_CalculationCase::MANUAL ) {
    myEditedObject->ClearRules( HYDROData_CalculationCase::DataTag_CustomRules, false );
  }
 
  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init;

  return true;
}

void HYDROGUI_CalculationOp::onApply()
{
  // Check warnings
  HYDROData_Warning aWarning = myEditedObject->GetLastWarning();
  if ( aWarning.Type != WARN_OK ) {
    if ( !confirmContinueWithWarning( aWarning ) ) {
      // Go back to the first page
      HYDROGUI_CalculationDlg* aPanel = 
        ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
      if ( aPanel ) {
        aPanel->onFirstPage();
      }
      return;
    }
  }

  QApplication::setOverrideCursor( Qt::WaitCursor );

  int anUpdateFlags = 0;
  QString anErrorMsg;
  QStringList aBrowseObjectsEntries;

  bool aResult = false;
  
  try
  {
    aResult = processApply( anUpdateFlags, anErrorMsg, aBrowseObjectsEntries );
  }
  catch ( Standard_Failure& e )
  {
    anErrorMsg = e.GetMessageString();
    aResult = false;
  }
  catch ( ... )
  {
    aResult = false;
  }
  
  QApplication::restoreOverrideCursor();

  if ( aResult )
  {
    module()->update( anUpdateFlags );
    commit();
    browseObjects( aBrowseObjectsEntries );
  }
  else
  {
    abort();
    QString aMsg = tr( "INPUT_VALID_DATA" );
    if( !anErrorMsg.isEmpty() )
      aMsg.prepend( anErrorMsg + "\n" );
    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "INSUFFICIENT_INPUT_DATA" ),
                               aMsg ); 
  }
}
#include <HYDROData_CompleteCalcCase.h>
void HYDROGUI_CalculationOp::onNext( const int theIndex )
{
  if( theIndex==1 && !myIsComplete)
  {
    setAvailableGroups();
  }
  else if( theIndex==2 && !myIsComplete)
  {
    setAvailableBoundaryPolygons();
  }
  else if( theIndex==3 && !myIsComplete)
  {
    // Land cover map panel
     HYDROGUI_CalculationDlg* aPanel = 
      ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
    if ( !aPanel )
      return;

    setLandCoverMapVisible( true );

    QStringList aList;
    QStringList anEntryList;

    // Get all land cover map objects to fill in combo-box
    Handle(HYDROData_LandCoverMap) aLandCoverMapObj;
    QString aLandCoverMapName;

    aList.clear();
    anEntryList.clear();
    HYDROData_Iterator anIter( doc(), KIND_LAND_COVER_MAP );
    for ( ; anIter.More(); anIter.Next() )
    {
      aLandCoverMapObj = Handle(HYDROData_LandCoverMap)::DownCast( anIter.Current() );

      if ( !aLandCoverMapObj.IsNull() )
      { 
        aLandCoverMapName = aLandCoverMapObj->GetName();
        if ( !aLandCoverMapName.isEmpty() )
        {
          aList.append( aLandCoverMapName );
          anEntryList.append( HYDROGUI_DataObject::dataObjectEntry( aLandCoverMapObj ) );
        }
      }
    }
    aPanel->setLandCoverMapsNames( aList, anEntryList );
    aLandCoverMapObj = myEditedObject->GetLandCoverMap();
    if ( !aList.isEmpty() )
    {
      if ( aLandCoverMapObj.IsNull() )
        aPanel->setLandCoverMap( aList.at( 0 ), false );
      else if ( myIsEdit )
        aPanel->setLandCoverMap( aList.at( 0 ), true );
      else
        aPanel->setLandCoverMap( aList.at( aList.indexOf( aLandCoverMapObj->GetName() ) ), true );
    }

    // Get all Strickler table objects to fill in combo-box
    Handle(HYDROData_StricklerTable) aStricklerTableObj;
    QString aStricklerTableName;

    aList.clear();
    anEntryList.clear();
    anIter = HYDROData_Iterator( doc(), KIND_STRICKLER_TABLE );
    for ( ; anIter.More(); anIter.Next() )
    {
      aStricklerTableObj = Handle(HYDROData_StricklerTable)::DownCast( anIter.Current() );

      if ( !aStricklerTableObj.IsNull() )
      { 
        aStricklerTableName = aStricklerTableObj->GetName();
        if ( !aStricklerTableName.isEmpty() )
        {
          aList.append( aStricklerTableName );
          anEntryList.append( HYDROGUI_DataObject::dataObjectEntry( aStricklerTableObj ) );
        }        
      }
    }
    aPanel->setStricklerTableNames( aList, anEntryList );
    //@ASL: bool anUpdateState = myEditedObject->IsMustBeUpdated();
    if ( !aList.isEmpty() )
      aPanel->setStricklerTable( aList.at( 0 ), false );
    //@ASL: myEditedObject->SetToUpdate( anUpdateState );

    if ( !myEditedObject.IsNull() )
    {
      if ( myIsEdit )
      {      
        // Select the certain Strickler table object in combo-box
        aStricklerTableObj = myEditedObject->GetStricklerTable();
        if ( aStricklerTableObj.IsNull() )
        {
          aPanel->setStricklerTable( QString() );
        }
        else
        {
          aStricklerTableName = aStricklerTableObj->GetName();
          aPanel->setStricklerTable( aStricklerTableName );
        }

        // Select the certain land cover map object in combo-box
        if ( aLandCoverMapObj.IsNull() )
        {
          aPanel->setLandCoverMap( QString() );
        }
        else
        {
          aLandCoverMapName = aLandCoverMapObj->GetName();
          aPanel->setLandCoverMap( aLandCoverMapName );
        }
      }
    }

    closePreview( false );
    createPreview( true );
  }
  else if( theIndex==4 || myIsComplete)
  {
    HYDROGUI_CalculationDlg* aPanel = 
      ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
    if ( !aPanel )
      return;

    QApplication::setOverrideCursor( Qt::WaitCursor );

    setGeomObjectsVisible( false );
    setLandCoverMapVisible( false );

    QString aNewCaseName = aPanel->getObjectName();
    QString anOldCaseName = myEditedObject->GetName();
    bool isNameChanged = anOldCaseName != aNewCaseName;
  
    bool anIsToUpdateOb = isNameChanged;

    // At first we must to update the case name because of 
    // automatic names generation for regions and zones
    myEditedObject->SetName( aNewCaseName );
    
    // Zones: set parameters for automatic mode
    int aMode = aPanel->getMode();
    if ( aMode == HYDROData_CalculationCase::AUTOMATIC )
    {
      // Set objects in the specified order
      if( myEditedObject->IsMustBeUpdated(  HYDROData_Entity::Geom_2d ) )
      {
        myEditedObject->RemoveGeometryObjects();
        QStringList aGeomObjNames;
        if (myIsComplete) //erase objects ?
          aGeomObjNames << aPanel->getAlreadyAddedGeomObjects();
        aGeomObjNames << aPanel->getAllGeomObjects();
        foreach ( const QString& aName, aGeomObjNames )
        {
          Handle(HYDROData_Object) anObject = Handle(HYDROData_Object)::DownCast( 
            HYDROGUI_Tool::FindObjectByName( module(), aName ) );
          if ( anObject.IsNull() )
          {
            continue;
          }
          myEditedObject->AddGeometryObject( anObject );
        }

        if (!myIsComplete)
        {
          // Clear priority rules
          //@ASL if ( myEditedObject->GetRulesCount() > 0 ) {
          myEditedObject->ClearRules( HYDROData_CalculationCase::DataTag_CustomRules, true );
          //@ASL }
          // Set priority rules
          foreach ( const HYDROData_CustomRule& aRule, aPanel->getRules() ) {
            myEditedObject->AddRule( aRule.Object1, aRule.Priority,
              aRule.Object2, aRule.MergeType,
              HYDROData_CalculationCase::DataTag_CustomRules );
          }
        }
      }
    }
    aPanel->setEditZonesEnabled( aMode == HYDROData_CalculationCase::MANUAL );

    if (myIsComplete)
      aPanel->BackButton()->setEnabled(false); //force disable; to prevent moving to the first page

    if ( myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_All ) )
    {
      myShowZones = true;
      
      if (myIsComplete)
      {
        //abortDocOperation();
        //module()->getApp()->updateObjectBrowser();
        QList<Handle(HYDROData_Entity)> newGeomObj = aPanel->getGeometryObjects(false);
        //QSet<Handle(HYDROData_Entity)> includedObjAtStartSet = myIncObjAtStart.toSet();
        NCollection_Sequence<Handle(HYDROData_Entity)> theNewObjects;
        foreach (Handle(HYDROData_Entity) obj, newGeomObj)
          theNewObjects.Append(obj);


        bool is_int = false;
        bool UseOrigNamingOfNewRegions = true;
        NCollection_Sequence<Handle(HYDROData_Region)> theNewRegions;
        QSet<QString> newRegionEntries;
        HYDROData_CompleteCalcCase::AddObjects(doc(), myEditedObject, theNewObjects, UseOrigNamingOfNewRegions, is_int, theNewRegions);
        if (is_int)        
          SUIT_MessageBox::information(module()->getApp()->desktop(),
          tr( "COMPLETE_CASE" ),
          tr( "COMPLETE_CASE_INTERSECTION_DETECTED" ));

        myEditedObject->ClearChanged();
        for (int k=1;k<=theNewRegions.Size();k++)
        {
          QString anEntry = HYDROGUI_DataObject::dataObjectEntry( theNewRegions(k) );
          newRegionEntries.insert(anEntry);
        }
        AssignDefaultZonesColors(&newRegionEntries);
      }
      else
      {
        myEditedObject->Update();
        AssignDefaultZonesColors(NULL);
      }
      

      //aPanel->setEditedObject( myEditedObject );
      aPanel->refreshZonesBrowser();
    
      closePreview( false );
      createPreview( false );

      anIsToUpdateOb = true;
    }
    else
    {
      // Show zones
      setZonesVisible( true );

      if ( isNameChanged ) {
        module()->getDataModel()->updateObjectTree( myEditedObject );
      }
    }

    QApplication::restoreOverrideCursor();
  }  
}

void HYDROGUI_CalculationOp::onHideZones( const int theIndex )
{
  if (myIsComplete)
  {
    //theIndex == 0
  //  setGeomObjectsVisible( true );
  // // HYDROGUI_CalculationDlg* aPanel = ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  // // QList<Handle(HYDROData_Entity)> geomObjList =  aPanel->getGeometryObjects(false);
  // // foreach ( Handle(HYDROData_Entity) geomObj, geomObjList)
  // //   setObjectVisibility( geomObj, true );
  //
  //  setZonesVisible( false );
  //  closePreview( false );
  //  createPreview( false );
  }
  else
  {
    if( theIndex==1 )
    {
      setGeomObjectsVisible( true );

      closePreview( false );
      createPreview( false );
    }
    if( theIndex==3 )
    {
      setLandCoverMapVisible( true );

      closePreview( false );
      createPreview( true );

      // Hide zones
      setZonesVisible( false );
    }
    else if( theIndex==4 )
    {
      AssignDefaultZonesColors();

      closePreview( false );
      createPreview( false );

      // Show zones
      setZonesVisible( true );
    }
  }
}

void HYDROGUI_CalculationOp::setZonesVisible( bool theIsVisible )
{
  myShowZones = theIsVisible;
  HYDROData_SequenceOfObjects aRegions = myEditedObject->GetRegions();
  HYDROData_SequenceOfObjects::Iterator aRegionsIter( aRegions );
  HYDROData_SequenceOfObjects aZones;
  Handle(HYDROData_Region) aRegion;
  if ( myPreviewViewManager ) 
  {
    if ( OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
      {
        for ( ; aRegionsIter.More(); aRegionsIter.Next() )
        {
          aRegion = Handle(HYDROData_Region)::DownCast( aRegionsIter.Value() );
          if ( !aRegion.IsNull() )
          {
            aZones = aRegion->GetZones();
            HYDROData_SequenceOfObjects::Iterator aZonesIter( aZones );
            for ( ; aZonesIter.More(); aZonesIter.Next() )
            {
              setObjectVisibility( aZonesIter.Value(), theIsVisible );
            }
          }
        }
      }

      module()->update( UF_OCCViewer );
    }
  }
}

void HYDROGUI_CalculationOp::setGeomObjectsVisible( bool theIsVisible )
{
  myShowGeomObjects = theIsVisible;

  HYDROData_SequenceOfObjects aSeq = myEditedObject->GetGeometryObjects();

  HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
  for ( ; anIter.More(); anIter.Next() ) {
    setObjectVisibility( anIter.Value(), theIsVisible );
  }
}

void HYDROGUI_CalculationOp::setLandCoverMapVisible( bool theIsVisible )
{
  myShowLandCoverMap = theIsVisible;
  setObjectVisibility( myEditedObject->GetLandCoverMap(), theIsVisible );  
}

void HYDROGUI_CalculationOp::AssignDefaultZonesColors(const QSet<QString>* theRegionsEntriesToColored)
{
  HYDROData_SequenceOfObjects aRegions = myEditedObject->GetRegions();
  HYDROData_SequenceOfObjects::Iterator aRegionsIter( aRegions );
  HYDROData_SequenceOfObjects aZones;
  Handle(HYDROData_Region) aRegion;
  if ( myPreviewViewManager ) 
  {
    if ( OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
      {     
        for ( ; aRegionsIter.More(); aRegionsIter.Next() )
        {
          aRegion = Handle(HYDROData_Region)::DownCast( aRegionsIter.Value() );
          QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aRegion );

          if (theRegionsEntriesToColored && !theRegionsEntriesToColored->contains(anEntry))
            continue;

          if ( !aRegion.IsNull() )
          {
            aZones = aRegion->GetZones();
            HYDROData_SequenceOfObjects::Iterator aZonesIter( aZones );
            for ( ; aZonesIter.More(); aZonesIter.Next() )
            {
              // Zone
              Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( aZonesIter.Value() );
              if ( !aZone.IsNull() )
              {
                QColor aFillingColor = HYDROData_Tool::GenerateRandColor();
                while (aFillingColor == Qt::red)
                  aFillingColor = HYDROData_Tool::GenerateRandColor();
                
                aZone->SetColor(aFillingColor);
              }
            }
          }
        }
      }
    }
  }
}

void HYDROGUI_CalculationOp::setRules( HYDROData_CalculationCase::DataTag theDataTag )
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  HYDROData_ListOfRules aRules;
  Handle(HYDROData_Entity) anObject1, anObject2;
  HYDROData_PriorityType aPriority;
  HYDROData_Zone::MergeType aMergeType;
  HYDROData_CalculationCase::DataTag aDataTag = HYDROData_CalculationCase::DataTag_CustomRules;
  for ( int anIndex = 0; ; anIndex++ )
  {
    if ( myEditedObject->GetRule( anIndex, anObject1, aPriority, anObject2, aMergeType, theDataTag ) ) {
      HYDROData_CustomRule aRule;
      aRule.Object1 = anObject1;
      aRule.Object2 = anObject2;
      aRule.Priority = aPriority;
      aRule.MergeType = aMergeType;

      aRules << aRule;
    }
    else
      break;
  }

  if ( theDataTag == HYDROData_CalculationCase::DataTag_CustomRules )
    aPanel->setRules( aRules );  
}

bool HYDROGUI_CalculationOp::createRegion( const QList<SUIT_DataObject*>& theZonesList )
{
  bool aRetValue = false;

  QList<HYDROGUI_Zone*> aZonesList;
  HYDROGUI_Zone* aZone;
  // Get a list of dropped zones
  for ( int i = 0; i < theZonesList.length(); i++ )
  {
    aZone = dynamic_cast<HYDROGUI_Zone*>( theZonesList.at( i ) );
    if ( aZone )
    {
      aZonesList.append( aZone );
    }
  }
  if ( aZonesList.length() > 0 )
  {
    module()->getDataModel()->createNewRegion( myEditedObject, aZonesList );
    aRetValue = true;
  }
   
  return aRetValue;
}

void HYDROGUI_CalculationOp::createPreview( const bool theLandCoverMap, bool fitAllFlag, bool onTopViewAndFit )
{
  DEBTRACE("createPreview " << theLandCoverMap << " " << fitAllFlag << " " << onTopViewAndFit);
  LightApp_Application* anApp = module()->getApp();
  HYDROData_SequenceOfObjects aSeq;
  if ( theLandCoverMap && myShowLandCoverMap )
  {
    aSeq.Append( myEditedObject->GetLandCoverMap() );
  }
  else if ( !theLandCoverMap && myShowGeomObjects )
  {
    HYDROData_SequenceOfObjects aSeqGO = myEditedObject->GetGeometryObjects();
    HYDROData_SequenceOfObjects aSeqP = myEditedObject->GetInterPolyObjects();
    aSeq.Append( aSeqGO );
    aSeq.Append( aSeqP );
  }

  Handle(HYDROData_Entity) anEntity;

  if ( myShowZones )
  {
    // Gather zones for displaying
    HYDROData_SequenceOfObjects aRegions = myEditedObject->GetRegions();
    HYDROData_SequenceOfObjects::Iterator aRegionsIter( aRegions );
    HYDROData_SequenceOfObjects aZones;
    Handle(HYDROData_Region) aRegion;
    for ( ; aRegionsIter.More(); aRegionsIter.Next() )
    {
      anEntity = aRegionsIter.Value();
      if ( !anEntity.IsNull() )
      {
        aRegion = Handle(HYDROData_Region)::DownCast( anEntity );
        if ( !aRegion.IsNull() )
        {
          aZones = aRegion->GetZones();
          aSeq.Append( aZones );
        }
      }
    }
  }

  // Get a boundary polyline if any
  if (Handle(HYDROData_PolylineXY) aBPoly = myEditedObject->GetBoundaryPolyline())
    aSeq.Append( aBPoly );

  module()->removeViewShapes( HYDROGUI_Module::VMR_PreviewCaseZones );

  if ( !myActiveViewManager )
  {
    if ( aSeq.IsEmpty() )
      return;

    myActiveViewManager = anApp->activeViewManager();
  }

  if ( !myPreviewViewManager )
  {
    myPreviewViewManager = ::qobject_cast<OCCViewer_ViewManager*>( 
      anApp->createViewManager( OCCViewer_Viewer::Type() ) );
    DEBTRACE("  createViewManager " << myPreviewViewManager);
    if ( myPreviewViewManager )
    {
      connect( myPreviewViewManager, SIGNAL( lastViewClosed( SUIT_ViewManager* ) ),
               this, SLOT( onLastViewClosed( SUIT_ViewManager* ) ) );

      module()->setViewManagerRole( myPreviewViewManager, HYDROGUI_Module::VMR_PreviewCaseZones );
      myPreviewViewManager->setTitle( tr( "PREVIEW_CASE_ZONES" ) );
    }
  }

  if ( !myPreviewViewManager )
    return;
  restoreOCCViewerSelection(myPreviewViewManager);

  if ( OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer() )
  {
    Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
    if ( !aCtx.IsNull() )
    {
      HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
      for ( ; anIter.More(); anIter.Next() )
      {
        const Handle(HYDROData_Entity)& ent = anIter.Value();
        if (!ent.IsNull())
          setObjectVisibility( ent, true );
      }

      //Process the draw events for viewer
      QApplication::processEvents( QEventLoop::ExcludeUserInputEvents );
    }

    int UpdateFlags = UF_OCCViewer;
    if (fitAllFlag)
      UpdateFlags |= UF_FitAll;
    module()->update( UpdateFlags );

    OCCViewer_ViewWindow* vw = (OCCViewer_ViewWindow*)myPreviewViewManager->getActiveView();
    if ( onTopViewAndFit && vw )
      vw->onTopView();
  }
}

void HYDROGUI_CalculationOp::setObjectVisibility( Handle(HYDROData_Entity) theEntity, const bool theIsVisible )
{
  if ( theEntity.IsNull() || !myPreviewViewManager ) {
    return;
  }

  OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer();
  if ( aViewer ) {
    module()->setObjectVisible( (size_t)aViewer, theEntity, theIsVisible );
  }
}

void HYDROGUI_CalculationOp::onLastViewClosed( SUIT_ViewManager* theViewManager )
{
  closePreview();
}

void HYDROGUI_CalculationOp::closePreview( bool theRemoveViewManager )
{
  SUIT_DataBrowser* aOb = ((LightApp_Application*)module()->application())->objectBrowser();
  QList<QShortcut*> aShortcuts = aOb->findChildren<QShortcut*>();
  QShortcut* aShortcut;
  foreach( aShortcut, aShortcuts )
  {
    if ( aShortcut->key() == 
      QKeySequence( ((LightApp_Application*)module()->application())->objectBrowser()->shortcutKey( 
      SUIT_DataBrowser::RenameShortcut ) ) )
    {
      aShortcut->setEnabled( true );
    }
  }


  if( myPreviewViewManager )
  {
    // Hide all the displayed objects in the preview view
    OCCViewer_Viewer* aViewer = myPreviewViewManager->getOCCViewer();
    if ( aViewer ) {
      size_t aViewId = (size_t)aViewer;
      HYDROData_Iterator anIterator( doc() );
      for( ; anIterator.More(); anIterator.Next() ) {
        Handle(HYDROData_Entity) anObject = anIterator.Current();
        if( !anObject.IsNull() ) {
          module()->setObjectVisible( aViewId, anObject, false );
        }
      }
    }

    if ( theRemoveViewManager )
    {
      disconnect( myPreviewViewManager, SIGNAL( lastViewClosed( SUIT_ViewManager* ) ),
                  this, SLOT( onLastViewClosed( SUIT_ViewManager* ) ) );

      module()->getApp()->removeViewManager( myPreviewViewManager ); // myPreviewViewManager is deleted here
      myPreviewViewManager = NULL;
    }
  }

  if( myActiveViewManager && theRemoveViewManager )
  {
    HYDROGUI_Tool::SetActiveViewManager( module(), myActiveViewManager );
    myActiveViewManager = NULL;
  }
}

void HYDROGUI_CalculationOp::setAvailableGroups()
{
  HYDROGUI_CalculationDlg* aPanel = 
      ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );

  HYDROData_SequenceOfObjects aSeq = myEditedObject->GetGeometryGroups();
  QStringList aList, anEntryList;
  getNamesAndEntries( aSeq, aList, anEntryList );

  QStringList aGroupsNames;

  HYDROData_SequenceOfObjects anObjs = myEditedObject->GetGeometryObjects();
  for( int anIndex = 1, aLength = anObjs.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Object) anObj = Handle(HYDROData_Object)::DownCast( anObjs.Value( anIndex ) );
    HYDROData_SequenceOfObjects aGroups = anObj->GetGroups();
    for( int aGIndex = 1, aGLength = aGroups.Length(); aGIndex <= aGLength; aGIndex++ )
    {
      Handle(HYDROData_ShapesGroup) aGroup = Handle(HYDROData_ShapesGroup)::DownCast( aGroups.Value( aGIndex ) );
      aGroupsNames.append( aGroup->GetName() );
    }
  }
  if( myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) ) {
    for( int anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ ) {
      Handle(HYDROData_ShapesGroup) aGeomGroup =
        Handle(HYDROData_ShapesGroup)::DownCast( aSeq.Value( anIndex ) );
      if ( !aGeomGroup.IsNull() && !aGroupsNames.contains( aGeomGroup->GetName() ) ) {
        myEditedObject->RemoveGeometryGroup( aGeomGroup );
      }
    }
  }

  aPanel->setAvailableGroups( aGroupsNames );
  aPanel->includeGroups( aList );

  //@ASL: bool isUpdated = myEditedObject->IsMustBeUpdated();
}

void HYDROGUI_CalculationOp::onAddGroups()
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  // Add geometry objects selected in the module browser to the calculation case
  QStringList aSelectedList = aPanel->getSelectedAvailableGroups();
  if ( aSelectedList.isEmpty() || !confirmRegionsChange() )
    return;

  QStringList anAddedList;
  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_ShapesGroup) aGroup = Handle(HYDROData_ShapesGroup)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at( i ) ) );
    if ( aGroup.IsNull() )
      continue;

    if ( myEditedObject->AddGeometryGroup( aGroup ) )
      anAddedList.append( aGroup->GetName() );
  }

  if ( !anAddedList.isEmpty() )
  {
    aPanel->includeGroups( anAddedList );
  }
}

void HYDROGUI_CalculationOp::onRemoveGroups()
{
  // Remove selected objects from the calculation case
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QStringList aSelectedList = aPanel->getSelectedGroups();
  if ( aSelectedList.isEmpty() || !confirmRegionsChange() )
    return;

  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_ShapesGroup) aGroup = Handle(HYDROData_ShapesGroup)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at(i) ) );
    if ( aGroup.IsNull() )
      continue;

    myEditedObject->RemoveGeometryGroup( aGroup );
  }

  aPanel->excludeGroups( aSelectedList );
}


void HYDROGUI_CalculationOp::setAvailableBoundaryPolygons()
{
  HYDROGUI_CalculationDlg* aPanel = ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );

  HYDROData_Iterator anIter( doc(), KIND_BC_POLYGON );
  Handle(HYDROData_BCPolygon) aBCP;
  QStringList aNames;
  QVector<int> aTypes; 
  for (;anIter.More(); anIter.Next())
  {
    aBCP = Handle(HYDROData_BCPolygon)::DownCast( anIter.Current() );
    aNames.append(aBCP->GetName());
    aTypes.append(aBCP->GetBoundaryType());
  }
  HYDROData_SequenceOfObjects aBCPSeq = myEditedObject->GetBoundaryPolygons();

  QStringList aListCut, aListIS;
  for (int i=1; i<=aBCPSeq.Size();i++)
  {
    int type = Handle(HYDROData_BCPolygon)::DownCast(aBCPSeq(i))->GetBoundaryType();
    if (type == 1)
      aListCut << aBCPSeq(i)->GetName();
    else if (type == 2 || type == 3)
      aListIS << aBCPSeq(i)->GetName();
  }

  aPanel->setAvailableBoundaryPolygons( aNames, aTypes );
  aPanel->includeBoundaryPolygons( aListCut );
  aPanel->includeISBoundaryPolygons( aListIS );
}

void HYDROGUI_CalculationOp::onRemoveBoundaryPolygons()
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QStringList aSelectedListCut = aPanel->getSelectedBoundaryPolygons();
  QStringList aSelectedListIS = aPanel->getSelectedISBoundaryPolygons();

  if ( aSelectedListCut.isEmpty() && aSelectedListIS.isEmpty() )
    return;

  for (int i = 0; i < aSelectedListCut.length(); i++)
  {
    Handle(HYDROData_BCPolygon) aBCPoly = Handle(HYDROData_BCPolygon)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedListCut.at(i) ) );
    if ( aBCPoly.IsNull() )
      continue;
    myEditedObject->RemoveBoundaryPolygon( aBCPoly );
  }

  for (int i = 0; i < aSelectedListIS.length(); i++)
  {
    Handle(HYDROData_BCPolygon) aBCPoly = Handle(HYDROData_BCPolygon)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedListIS.at(i) ) );
    if ( aBCPoly.IsNull() )
      continue;
    myEditedObject->RemoveBoundaryPolygon( aBCPoly );
  }

  aPanel->excludeBoundaryPolygons( aSelectedListCut );
  aPanel->excludeISBoundaryPolygons( aSelectedListIS );
}

 
void HYDROGUI_CalculationOp::onAddBoundaryPolygons()
{
  HYDROGUI_CalculationDlg* aPanel = ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QStringList aSelectedList = aPanel->getSelectedAvailableBoundaryPolygons();
  if ( aSelectedList.isEmpty())
    return;

  QStringList anAddedListCut, anAddedListIS;
  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_BCPolygon) aBCPoly = Handle(HYDROData_BCPolygon)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at( i ) ) );
    if ( aBCPoly.IsNull() )
      continue;
   
   if ( myEditedObject->AddBoundaryPolygon( aBCPoly ) )
   {
     QString aName = aBCPoly->GetName();
     int type = aBCPoly->GetBoundaryType(); 
     if (type == 1)
       anAddedListCut.append(aName);
     else if (type == 2 || type == 3)
       anAddedListIS.append(aName);
   }

  }

  if ( !anAddedListCut.isEmpty() )
    aPanel->includeBoundaryPolygons(anAddedListCut);
  if ( !anAddedListIS.isEmpty() )
    aPanel->includeISBoundaryPolygons(anAddedListIS);
}

void HYDROGUI_CalculationOp::onChangeMode( int theMode )
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  if ( !confirmModeChange() ) {
    aPanel->setMode( myEditedObject->GetAssignmentMode() );
    return;
  }

  myEditedObject->SetAssignmentMode( (HYDROData_CalculationCase::AssignmentMode)theMode );
  aPanel->setMode( theMode );
}

void HYDROGUI_CalculationOp::onOrderChanged( bool& isConfirmed )
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  isConfirmed = confirmOrderChange();
  if( isConfirmed )
    myEditedObject->Changed( HYDROData_Entity::Geom_2d );
}

void HYDROGUI_CalculationOp::onRuleChanged( bool& isConfirmed )
{
  HYDROGUI_CalculationDlg* aPanel = 
    ::qobject_cast<HYDROGUI_CalculationDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  isConfirmed = confirmRuleChange();
  if( isConfirmed )
    myEditedObject->Changed( HYDROData_Entity::Geom_2d );
}

void HYDROGUI_CalculationOp::onRegenerateColors()
{
  // For geometry zones
  AssignDefaultZonesColors();
  setZonesVisible( false );
  setZonesVisible( true );
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Shape.h>
#include <HYDROGUI_Tool.h>
#include <HYDROGUI_Polyline.h>
#include <HYDROGUI_AISShape.h>

#include <HYDROData_Channel.h>
#include <HYDROData_Document.h>
#include <HYDROData_DummyObject3D.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_BCPolygon.h>
#include <HYDROData_Obstacle.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_Profile.h>
#include <HYDROData_ShapesGroup.h>
#include <HYDROData_Stream.h>
#include <HYDROData_Zone.h>
#include <HYDROData_LandCoverMap.h>

#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <Prs3d_IsoAspect.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <TopExp_Explorer.hxx>
#include <TopExp.hxx>

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_Shape::HYDROGUI_Shape( const Handle(AIS_InteractiveContext)& theContext,
                                const Handle(HYDROData_Entity)&       theObject,
                                const int                             theZLayer )
: myContext( theContext ),
  myObject( theObject ),
  myZLayer( theZLayer ),
  myIsHighlight( false ),
  myFillingColor( Qt::transparent ),
  myBorderColor( Qt::black ),
  myHighlightColor( Qt::white ),
  myIsToUpdate( false ),
  myIsVisible( true ),
  myDisplayMode( AIS_Shaded ),
  mySelectionMode( AIS_Shape::SelectionMode( TopAbs_SHAPE ) )
{
}

HYDROGUI_Shape::~HYDROGUI_Shape()
{
  erase( false );
}

Handle(AIS_InteractiveContext) HYDROGUI_Shape::getContext() const
{
  return myContext;
}

Handle(HYDROData_Entity) HYDROGUI_Shape::getObject() const
{
  return myObject;
}

TopoDS_Shape HYDROGUI_Shape::getTopoShape() const
{
  return myTopoShape;
}

bool HYDROGUI_Shape::getIsToUpdate() const
{
  return myIsToUpdate;
}

void HYDROGUI_Shape::setIsToUpdate( bool theState )
{
  myIsToUpdate = theState;
}

bool HYDROGUI_Shape::isVisible() const
{
  return myIsVisible;
}

QList<Handle(AIS_InteractiveObject)> HYDROGUI_Shape::getAISObjects() const
{
  return myShapes;
}

void HYDROGUI_Shape::display( const bool theIsUpdateViewer )
{
  if ( myContext.IsNull() || myShapes.empty() || !isVisible() )
    return;

  displayShape( theIsUpdateViewer );
}

void HYDROGUI_Shape::erase( const bool theIsUpdateViewer )
{
  if ( myContext.IsNull() || myShapes.empty() )
    return;

  eraseShape( theIsUpdateViewer );
}

void HYDROGUI_Shape::update( bool isUpdateViewer,
                             bool isDeactivateSelection )

{
  DEBTRACE("update " << isUpdateViewer << " " << isDeactivateSelection);
  setIsToUpdate( false );

  if ( myContext.IsNull() )
    return;

  // Try to retrieve information from object
  if ( !myObject.IsNull() )
  {
    Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();

    if ( myObject->IsKind( STANDARD_TYPE(HYDROData_ImmersibleZone) ) )
    {
      Handle(HYDROData_ImmersibleZone) aZoneObj =
        Handle(HYDROData_ImmersibleZone)::DownCast( myObject );

      TopoDS_Shape aZoneShape = aZoneObj->GetTopShape();
      if ( !aZoneShape.IsNull() ) {
        if ( aZoneShape.ShapeType() == TopAbs_FACE ) {
          TopoDS_Face aZoneFace = TopoDS::Face( aZoneShape );
          setFace( aZoneFace, false, false, "" );
        } else {
          myTopoShape = aZoneShape;
          //TODO: myDisplayMode = myTextureFileName.isEmpty() ? AIS_Shaded : AIS_Shaded+2;
          myDisplayMode = AIS_Shaded;

          buildShape();
          updateShape( false, false );
        }
      }

      QColor aFillingColor = aZoneObj->GetFillingColor();
      QColor aBorderColor = aZoneObj->GetBorderColor();

      setFillingColor( aFillingColor, false, false );
      setBorderColor( aBorderColor, false, false );
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_PolylineXY) ) )
    {
      Handle(HYDROData_PolylineXY) aPolyline =
        Handle(HYDROData_PolylineXY)::DownCast( myObject );

      TopoDS_Shape aPolylineShape = aPolyline->GetShape();

      if ( !aPolylineShape.IsNull() )
      {
        if ( aPolylineShape.ShapeType() == TopAbs_WIRE )
        {
          TopoDS_Wire aPolylineWire = TopoDS::Wire( aPolylineShape );
          setWire( aPolylineWire, false, false );
        }
        else
        {
          myTopoShape = aPolylineShape;
          // Set shading mode to avoid that hilight presentation is equal to "normal" object presentation.
          // Note that hilight presentation is always to be on top ( i.e. in the top Z layer ).
          myDisplayMode = AIS_Shaded;

          buildShape();
          updateShape( false, false );
        }
      }

      //QColor aWireColor = aPolyline->GetWireColor();
      std::vector<QColor> aSectColors;
      //TODO backward comp. with old aWireColor??
      int nbSec = aPolyline->NbSections();
      for (int i = 0; i < nbSec; i++)
      {
        QColor aColor;
        aPolyline->GetSectionColor(i, aColor);
        aSectColors.push_back(aColor);
      }

      //
      //setBorderColor( aWireColor, false, false );

      foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
      {
        if( !myTopoShape.IsNull() )
        {
          Handle(HYDROGUI_Polyline) aPShape = Handle(HYDROGUI_Polyline)::DownCast( aShape );
          bool WireOrCmp = myTopoShape.ShapeType() == TopAbs_WIRE || myTopoShape.ShapeType() == TopAbs_COMPOUND;
          if ( !aPShape.IsNull() && WireOrCmp)
          {
            TopTools_IndexedMapOfShape MW;
            TopExp::MapShapes(myTopoShape, TopAbs_WIRE, MW);
            if (MW.Extent() == nbSec)
            {
              for (int i=0;i<nbSec;i++)
              {
                const TopoDS_Shape& CW = MW(i+1);
                Quantity_Color aFColor( getQuantityColorVal( aSectColors[i].red() ),
                  getQuantityColorVal( aSectColors[i].green() ),
                  getQuantityColorVal( aSectColors[i].blue() ),
                  Quantity_TOC_RGB );
                aPShape->myShapeToColor.Add(CW, aFColor);
              }
            }
          }
        }
      }

    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_Polyline3D) ) )
    {
      Handle(HYDROData_Polyline3D) aPolyline =
        Handle(HYDROData_Polyline3D)::DownCast( myObject );

      TopoDS_Shape aPolylineShape = aPolyline->GetShape3D();

      if ( !aPolylineShape.IsNull() ) {
        if ( aPolylineShape.ShapeType() == TopAbs_WIRE ) {
          TopoDS_Wire aPolylineWire = TopoDS::Wire( aPolylineShape );
          setWire( aPolylineWire, false, false );
        } else {
          myTopoShape = aPolylineShape;
          // Set shading mode to avoid that hilight presentation is equal to "normal" object presentation.
          // Note that hilight presentation is always to be on top ( i.e. in the top Z layer ).
          myDisplayMode = AIS_Shaded;

          buildShape();
          updateShape( false, false );
        }
      }

      QColor aWireColor = aPolyline->GetBorderColor();
      setBorderColor( aWireColor, false, false );
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_Zone) ) )
    {
      Handle(HYDROData_Zone) aZone =
        Handle(HYDROData_Zone)::DownCast( myObject );

      TopoDS_Face aZoneFace = TopoDS::Face( aZone->GetShape() );

      setFace( aZoneFace, false, false, "" );
      if( aZone->IsMergingNeed() && aZone->GetMergeType() == HYDROData_Zone::Merge_UNKNOWN )
      {
        // Red color for a zone with bathymetry conflict
        setFillingColor( Qt::red );
      }
      else
      {
		    // Set the filling color for zone
        setFillingColor( aZone->GetColor( Qt::darkBlue ) );
      }
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_Profile) ) )
    {
      Handle(HYDROData_Profile) aProfile =
        Handle(HYDROData_Profile)::DownCast( myObject );

      TopoDS_Wire aProfileWire;

      if ( aProfile->IsValid() ) {
        TopoDS_Shape aProfileShape = aProfile->GetShape3D();

        if ( !aProfileShape.IsNull() &&
             aProfileShape.ShapeType() == TopAbs_WIRE ) {
          aProfileWire = TopoDS::Wire( aProfileShape );
        }
      }

      setWire( aProfileWire, false, false );

      QColor aWireColor = aProfile->GetBorderColor();
      setBorderColor( aWireColor, false, false );
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_Stream) ) ||
              myObject->IsKind( STANDARD_TYPE(HYDROData_Channel) ) ||
              myObject->IsKind( STANDARD_TYPE(HYDROData_Obstacle) ) )
    {
      Handle(HYDROData_Object) aGeomObject =
        Handle(HYDROData_Object)::DownCast( myObject );

      TopoDS_Shape anObjShape = aGeomObject->GetTopShape();

      setShape( anObjShape, false, false );

      QColor aFillingColor = aGeomObject->GetFillingColor();
      QColor aBorderColor = aGeomObject->GetBorderColor();

      setFillingColor( aFillingColor, false, false );
      setBorderColor( aBorderColor, false, false );
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_DummyObject3D) ) )
    {
      Handle(HYDROData_DummyObject3D) anObject3D =
        Handle(HYDROData_DummyObject3D)::DownCast( myObject );
      TopoDS_Shape aShape3D = anObject3D->GetShape();

      setShape( aShape3D, false, false );

      QColor aFillingColor = anObject3D->GetFillingColor();
      QColor aBorderColor = anObject3D->GetBorderColor();

      setFillingColor( aFillingColor, false, false );
      setBorderColor( aBorderColor, false, false );
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_ShapesGroup) ) )
    {
      Handle(HYDROData_ShapesGroup) aShapesGroup =
        Handle(HYDROData_ShapesGroup)::DownCast( myObject );

      TopTools_SequenceOfShape aShapes;
      aShapesGroup->GetShapes( aShapes );

      TopoDS_Compound aCompound;
      BRep_Builder aCompoundBuilder;
      aCompoundBuilder.MakeCompound( aCompound );

      for ( int i = 1, n = aShapes.Length(); i <= n; ++i )
      {
        const TopoDS_Shape& aShape = aShapes.Value( i );
        aCompoundBuilder.Add( aCompound, aShape );
      }

      setShape( aCompound, false, false );
    }
    else if ( myObject->IsKind( STANDARD_TYPE(HYDROData_BCPolygon) ) )
    {
      Handle(HYDROData_BCPolygon) aBCObj = Handle(HYDROData_BCPolygon)::DownCast( myObject );

      TopoDS_Shape aBCShape = aBCObj->GetTopShape();
      if ( !aBCShape.IsNull() )
      {
        if ( aBCShape.ShapeType() == TopAbs_FACE )
        {
          TopoDS_Face aBCFace = TopoDS::Face( aBCShape );
          setFace( aBCFace, false, false, "" );
        }
        else
        {
          myTopoShape = aBCShape;
          myDisplayMode = AIS_Shaded;
          buildShape();
          updateShape( false, false );
        }
      }

      QColor aFillingColor = aBCObj->GetFillingColor();
      QColor aBorderColor = aBCObj->GetBorderColor();
      aFillingColor.setAlpha(175);

      setFillingColor( aFillingColor, false, false );
      setBorderColor( aBorderColor, false, false );
    }

  }

  if ( myShapes.empty() || !isVisible() )
    return;

  displayShape( isUpdateViewer );

  if (isDeactivateSelection)
    foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
      myContext->Deactivate( aShape );
}

void HYDROGUI_Shape::setVisible( const bool theState,
                                 const bool theIsUpdateViewer )
{
  DEBTRACE("setVisible")
  myIsVisible = theState;

  if ( myShapes.empty() )
    return;

  if ( ( myIsVisible && myContext->IsDisplayed( myShapes[0] ) ) ||
       ( !myIsVisible && !myContext->IsDisplayed( myShapes[0] ) ) )
    return;

  if ( myIsVisible ) {
    displayShape( theIsUpdateViewer );
  }
  else
    eraseShape( theIsUpdateViewer );
}

void HYDROGUI_Shape::highlight( bool theIsHighlight, bool isUpdateViewer )
{
  DEBTRACE("highlight " << theIsHighlight << " " << isUpdateViewer);
  if ( myIsHighlight == theIsHighlight )
    return;

  myIsHighlight = theIsHighlight;

  if ( myContext.IsNull() || myShapes.empty() )
    return;

  colorShapeBorder( getActiveColor() );
  displayShape( isUpdateViewer );
}

bool HYDROGUI_Shape::isHighlighted() const
{
  DEBTRACE("isHighlighted " << myIsHighlight);
  return myIsHighlight;
}

void HYDROGUI_Shape::setWire( const TopoDS_Wire& theWire,
                              const bool         theToDisplay,
                              const bool         theIsUpdateViewer )
{
  myTopoShape = theWire;
  // To avoid that hilight presentation is equal to "normal" object presentation.
  // Note that hilight presentation is always to be on top ( i.e. in the top Z layer ).
  myDisplayMode = AIS_Shaded;

  buildShape();
  updateShape( theToDisplay, theIsUpdateViewer );
}

void HYDROGUI_Shape::setFaces( const TopoDS_Compound& theWires,
                               const bool             theToDisplay,
                               const bool             theIsUpdateViewer )
{
  TopExp_Explorer anExp( theWires, TopAbs_WIRE );
  TopoDS_Compound aCompound;
  BRep_Builder aBuilder;
    aBuilder.MakeCompound( aCompound );

  for ( ; anExp.More(); anExp.Next() ) {
    TopoDS_Wire aWire = TopoDS::Wire( anExp.Current() );
    if ( aWire.IsNull() ) {
      continue;
    }

    BRepBuilderAPI_MakeFace aMakeFace( aWire, Standard_True );
    aMakeFace.Build();
    if( aMakeFace.IsDone() ) {
      aBuilder.Add( aCompound, aMakeFace.Face() );
    }
  }

  myTopoShape = aCompound;
  myDisplayMode = AIS_Shaded;

  buildShape();
  updateShape( theToDisplay, theIsUpdateViewer );
}

void HYDROGUI_Shape::setFace( const TopoDS_Wire& theWire,
                              const bool         theToDisplay,
                              const bool         theIsUpdateViewer,
                              const QString&     theTextureFileName )
{
  BRepBuilderAPI_MakeFace aFaceBuilder( theWire, Standard_True );
  aFaceBuilder.Build();
  if( aFaceBuilder.IsDone() )
  {
    TopoDS_Face aFace = aFaceBuilder.Face();
    setFace( aFace, theToDisplay, theIsUpdateViewer, theTextureFileName );
  }
}

void HYDROGUI_Shape::setFace( const TopoDS_Face& theFace,
                              const bool         theToDisplay,
                              const bool         theIsUpdateViewer,
                              const QString&     theTextureFileName )
{
  myTopoShape = theFace;
  myDisplayMode = theTextureFileName.isEmpty() ? AIS_Shaded : AIS_Shaded+2;
  //Note: AIS_Shaded+2 is the same as AIS_ExactHLR
  //TODO: it would be more suitable to use TexturedShape mode from GEOM_AISShape

  buildShape();
  updateShape( theToDisplay, theIsUpdateViewer );
}

void HYDROGUI_Shape::setShape( const TopoDS_Shape& theShape,
                               const bool          theToDisplay,
                               const bool          theIsUpdateViewer,
                               const int           theDisplayMode )
{
  myTopoShape = theShape;
  myDisplayMode = theDisplayMode;

  buildShape();
  updateShape( theToDisplay, theIsUpdateViewer );
}

void HYDROGUI_Shape::setFillingColor( const QColor& theColor,
                                      const bool    theToDisplay,
                                      const bool    theIsUpdateViewer )
{
  myFillingColor = theColor;
  updateShape( theToDisplay, theIsUpdateViewer );
}

QColor HYDROGUI_Shape::getFillingColor() const
{
  return myFillingColor;
}

void HYDROGUI_Shape::setBorderColor( const QColor& theColor,
                                     const bool    theToDisplay,
                                     const bool    theIsUpdateViewer )
{
  myBorderColor = theColor;
  updateShape( theToDisplay, theIsUpdateViewer );
}

QColor HYDROGUI_Shape::getBorderColor() const
{
  return myBorderColor;
}

void HYDROGUI_Shape::setHighlightColor( const QColor& theColor )
{
  myHighlightColor = theColor;
}

QColor HYDROGUI_Shape::getHighlightColor() const
{
  return myHighlightColor;
}

void HYDROGUI_Shape::setZLayer( const int theZLayer )
{
  if ( myZLayer == theZLayer )
    return;

  myZLayer = theZLayer;
  if( isVisible() && !myContext.IsNull() && myZLayer >= 0 )
    foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
      myContext->SetZLayer( aShape, myZLayer );
}

QList<Handle(AIS_InteractiveObject)> HYDROGUI_Shape::createShape() const
{
  QList<Handle(AIS_InteractiveObject)> shapes;
  if( myTopoShape.IsNull() ) {
    return shapes;
  }

  TopAbs_ShapeEnum aShapeType = myTopoShape.ShapeType();
  bool IsWireEdgeCompound = aShapeType==TopAbs_COMPOUND;
  if (IsWireEdgeCompound) {
    TopoDS_Iterator itr(myTopoShape);
    while (itr.More() && IsWireEdgeCompound) {
      if (itr.Value().ShapeType() != TopAbs_WIRE && itr.Value().ShapeType() != TopAbs_EDGE)
        IsWireEdgeCompound = false;
      itr.Next();
    }
  }

  if ( aShapeType==TopAbs_EDGE || aShapeType==TopAbs_WIRE || IsWireEdgeCompound)
  {
    SUIT_ResourceMgr* aResMgr = 0;
    SUIT_Session* aSession = SUIT_Session::session();
    if ( aSession )
      aResMgr = SUIT_Session::session()->resourceMgr();

    int aType = -1;
    int aSize = -1;

    if( myShapes.length()>1 )
    {
      Handle(HYDROGUI_Arrow) arrow = Handle(HYDROGUI_Arrow)::DownCast( myShapes[1] );
      if( !arrow.IsNull() )
      {
        aType = arrow->GetType();
        aSize = arrow->GetSize();
      }
    }
    if( aResMgr && ( aType<0 || aSize < 0 ) )
    {
      aResMgr->value( "polyline", "arrow_type", aType );
      aResMgr->value( "polyline", "arrow_size", aSize );
    }

    shapes.append( HYDROGUI_Polyline::createPresentations( myTopoShape, aType, aSize ) );
  }
  else
  {
    shapes.append( new HYDROGUI_AISShape( myTopoShape ) );
  }
  return shapes;
}

void HYDROGUI_Shape::buildShape()
{
  // Erase previously created shape
  erase(false);

  myShapes = createShape();
  if( myShapes.empty() )
    return;

  foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
  {
    Handle(AIS_Shape) anAISShape = Handle(AIS_Shape)::DownCast( aShape );
    if( !anAISShape.IsNull() )
      anAISShape ->SetAngleAndDeviation( 0.001 );

    if ( !myObject.IsNull() )
      aShape->SetOwner( myObject );

    if ( !myObject.IsNull() && !myObject->IsKind( STANDARD_TYPE(HYDROData_LandCoverMap) ) )
      aShape->SetTransparency( 0 );
    aShape->SetDisplayMode( (AIS_DisplayMode)myDisplayMode );

    // Init default params for shape
    const Handle(Prs3d_Drawer)& anAttributes = aShape->Attributes();
    if ( !anAttributes.IsNull() )
    {
      Handle(Prs3d_IsoAspect) anIsoAspect = anAttributes->UIsoAspect();
      if ( !anIsoAspect.IsNull() ) {
        anIsoAspect->SetNumber( 0 );
        anAttributes->SetUIsoAspect( anIsoAspect );
      }

      anIsoAspect = anAttributes->VIsoAspect();
      if ( !anIsoAspect.IsNull() )
      {
        anIsoAspect->SetNumber( 0 );
        anAttributes->SetVIsoAspect( anIsoAspect );
      }

      if ( myDisplayMode == AIS_Shaded )
      {
        aShape->SetMaterial( Graphic3d_NOM_PLASTIC );
      }
      else if ( myDisplayMode == AIS_WireFrame )
      {
        anAttributes->SetWireDraw( true );
      }
    }
  }
}

void HYDROGUI_Shape::updateShape( const bool theToDisplay,
                                  const bool theIsUpdateViewer )
{
  DEBTRACE("updateShape " << theToDisplay << " " << theIsUpdateViewer);
  foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
  {
    const Handle(Prs3d_Drawer)& anAttributes = aShape->Attributes();
    if ( !anAttributes.IsNull() )
    {
      if ( myDisplayMode == AIS_Shaded && myFillingColor.isValid() )
      {
        Quantity_Color aFillingColor( getQuantityColorVal( myFillingColor.red() ),
                                      getQuantityColorVal( myFillingColor.green() ),
                                      getQuantityColorVal( myFillingColor.blue() ),
                                      Quantity_TOC_RGB );

        aShape->SetColor( aFillingColor );
        aShape->SetTransparency( 1 - getQuantityColorVal( myFillingColor.alpha() ) );
      }
      else if ( myDisplayMode == AIS_WireFrame )
      {
      }

      // Coloring borders
    }
  }

  colorShapeBorder( myBorderColor );
  if ( !theToDisplay || !isVisible() || myContext.IsNull() )
    return;

  displayShape( theIsUpdateViewer );
}

void HYDROGUI_Shape::displayShape( const bool theIsUpdateViewer )
{
  DEBTRACE("displayShape " << theIsUpdateViewer << " " << mySelectionMode << " AIScontext " << myContext.get());

  foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
  {
    if ( mySelectionMode >= 0 )
      // Display object with selection
        myContext->Display( aShape, Standard_False );
    else
    {
        // Display object without selection
        myContext->Display( aShape, myDisplayMode, -1, Standard_False, Standard_False );
    }
  }

  if ( mySelectionMode >= 0 )
  {
    foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
      myContext->Activate( aShape, mySelectionMode, Standard_True );
  }

  if ( myZLayer >= 0 )
  {
    foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
      myContext->SetZLayer( aShape, myZLayer );
  }

  if ( theIsUpdateViewer ) {
    myContext->UpdateCurrentViewer();
  }
}

void HYDROGUI_Shape::eraseShape( const bool theIsUpdateViewer )
{
  foreach( Handle(AIS_InteractiveObject) aShape, myShapes )
    myContext->Erase( aShape, Standard_False );

  if( theIsUpdateViewer )
    myContext->UpdateCurrentViewer();
}

QColor HYDROGUI_Shape::getActiveColor() const
{
  return isHighlighted() ? myHighlightColor : myBorderColor;
}

double HYDROGUI_Shape::getQuantityColorVal( const int theColorVal )
{
  return theColorVal == 0 ? 0 : ( (double)theColorVal / 255 );
}

void HYDROGUI_Shape::colorShapeBorder( const QColor& theColor )
{
  foreach( Handle(AIS_InteractiveObject) aShape1, myShapes )
  {
    const Handle(Prs3d_Drawer)& anAttributes = aShape1->Attributes();
    if ( anAttributes.IsNull() )
      continue;

    Quantity_Color aBorderColor( getQuantityColorVal( theColor.red() ),
                                 getQuantityColorVal( theColor.green() ),
                                 getQuantityColorVal( theColor.blue() ),
                                 Quantity_TOC_RGB );

    if( !myTopoShape.IsNull() )
    {
      if ( myTopoShape.ShapeType() == TopAbs_WIRE ) // Note that we display polylines in shaded mode
      {
        aShape1->SetColor( aBorderColor );
      }
      else if ( myDisplayMode == AIS_Shaded )
      {
        Handle(HYDROGUI_AISShape) aShape = Handle(HYDROGUI_AISShape)::DownCast( aShape1 );
        if( !aShape.IsNull() )
          aShape->SetBorderColor( aBorderColor );
      }
      else if ( myDisplayMode == AIS_WireFrame )
      {
        aShape1->SetColor( aBorderColor );
      }
    }
  }
}

void HYDROGUI_Shape::setDisplayMode( int theDisplayMode )
{
  myDisplayMode = theDisplayMode;
}

void HYDROGUI_Shape::setSelectionMode( int theSelectionMode )
{
  mySelectionMode = theSelectionMode;
}

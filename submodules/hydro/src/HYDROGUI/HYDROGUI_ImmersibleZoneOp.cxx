// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImmersibleZoneOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_ImmersibleZoneDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_DataObject.h"

#include <HYDROData_Bathymetry.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_PolylineXY.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>

#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <QApplication>
//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_ImmersibleZoneOp::HYDROGUI_ImmersibleZoneOp( HYDROGUI_Module* theModule,
                                                      const bool theIsEdit )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit ),
  myPreviewPrs( 0 )
{
  setName( theIsEdit ? tr( "EDIT_IMMERSIBLE_ZONE" ) : tr( "CREATE_IMMERSIBLE_ZONE" ) );
}

HYDROGUI_ImmersibleZoneOp::~HYDROGUI_ImmersibleZoneOp()
{
  closePreview();
}

void HYDROGUI_ImmersibleZoneOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_ImmersibleZoneDlg* aPanel = ::qobject_cast<HYDROGUI_ImmersibleZoneDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->blockSignals( true );

  aPanel->reset();

  QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_IMMERSIBLE_ZONE_NAME" ) );

  QString     aSelectedPolyline, aSelectedBathymetry;
  QStringList aSelectedBathymetries;

  if ( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_ImmersibleZone)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !myEditedObject.IsNull() )
    {
      anObjectName = myEditedObject->GetName();

      Handle(HYDROData_PolylineXY) aRefPolyline = myEditedObject->GetPolyline();
      if ( !aRefPolyline.IsNull() )
        aSelectedPolyline = aRefPolyline->GetName();

      Handle(HYDROData_IAltitudeObject) aRefAltitude = myEditedObject->GetAltitudeObject();
      if ( !aRefAltitude.IsNull() )
        aSelectedBathymetry = aRefAltitude->GetName();
    }
  }

  aPanel->setObjectName( anObjectName );
  aPanel->setPolylineNames( HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_POLYLINEXY ) );
  aPanel->setAdditionalParams( HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_BATHYMETRY ) );

  aPanel->blockSignals( false );

  aPanel->setPolylineName( aSelectedPolyline );
  aPanel->setSelectedAdditionalParamName( aSelectedBathymetry );
}

void HYDROGUI_ImmersibleZoneOp::abortOperation()
{
  closePreview();

  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ImmersibleZoneOp::commitOperation()
{
  closePreview();

  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_ImmersibleZoneOp::createInputPanel() const
{
  HYDROGUI_ImmersibleZoneDlg* aPanel = new HYDROGUI_ImmersibleZoneDlg( module(), getName() );
  connect( aPanel, SIGNAL( CreatePreview( const QString& ) ),
           this,   SLOT( onCreatePreview( const QString& ) ) );
  return aPanel;
}

bool HYDROGUI_ImmersibleZoneOp::processApply( int& theUpdateFlags,
                                              QString& theErrorMsg,
                                              QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_ImmersibleZoneDlg* aPanel = ::qobject_cast<HYDROGUI_ImmersibleZoneDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QString anObjectName = aPanel->getObjectName().simplified();
  if ( anObjectName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  if( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anObjectName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObjectName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObjectName );
      return false;
    }
  }

  Handle(HYDROData_PolylineXY) aZonePolyline;
  Handle(HYDROData_Bathymetry) aZoneBathymetry;

  QString aPolylineName = aPanel->getPolylineName();
  if ( !aPolylineName.isEmpty() )
  {
    aZonePolyline = Handle(HYDROData_PolylineXY)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aPolylineName, KIND_POLYLINEXY ) );
  }

  QString aBathymetryName = aPanel->getSelectedAdditionalParamName();
  if ( !aBathymetryName.isEmpty() )
  {
    aZoneBathymetry = Handle(HYDROData_Bathymetry)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aBathymetryName, KIND_BATHYMETRY ) );
  }


  if ( HYDROData_ImmersibleZone::generateTopShape( aZonePolyline ).IsNull() )
  {
    theErrorMsg = tr( "ZONE_OBJECT_CANNOT_BE_CREATED" );
    return false;
  }

  Handle(HYDROData_ImmersibleZone) aZoneObj = myIsEdit ? myEditedObject :
    Handle(HYDROData_ImmersibleZone)::DownCast( doc()->CreateObject( KIND_IMMERSIBLE_ZONE ) );

  aZoneObj->SetName( anObjectName );

  if ( !myIsEdit )
  {
    aZoneObj->SetFillingColor( aZoneObj->DefaultFillingColor() );
    aZoneObj->SetBorderColor( aZoneObj->DefaultBorderColor() );
  }

  bool isPolylineChanged = true;
  if (aZonePolyline && aZoneObj->GetPolyline() && (aZoneObj->GetPolyline()->GetName() == aZonePolyline->GetName()))
  {
      isPolylineChanged =false;
      DEBTRACE("polyline unchanged");
  }
  else
  {
      if (aZonePolyline) DEBTRACE(aZonePolyline->GetName().toStdString());
      if (aZoneObj->GetPolyline()) DEBTRACE(aZoneObj->GetPolyline()->GetName().toStdString());
      aZoneObj->SetPolyline( aZonePolyline );
  }
  bool isBathymetryChanged = true;
  if (aZoneBathymetry && aZoneObj->GetAltitudeObject() && IsEqual(aZoneObj->GetAltitudeObject(), aZoneBathymetry))
  {
      isBathymetryChanged = false;
      DEBTRACE("bathymetry unchanged");
  }
  else aZoneObj->SetAltitudeObject( aZoneBathymetry );
  aZoneObj->Update();

  closePreview();

  if( !myIsEdit )
  {
    module()->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( module() ), aZoneObj, true );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aZoneObj );
    theBrowseObjectsEntries.append( anEntry );
  }

  module()->setIsToUpdate( aZoneObj );

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  if (isBathymetryChanged)
  {
    DEBTRACE("bathymetry changed on " << anObjectName.toStdString());
    // Find immersibles objects referenced by all calculation case zones and check if they correspond to the one modified 
    HYDROData_SequenceOfObjects zoneList = doc()->CollectAllObjects();
    for (int i=1; i<=zoneList.Size(); i++ )
    {
        //DEBTRACE("    --- entity: " << zoneList(i)->GetName().toStdString() << " " << zoneList(i)->GetKind());
        Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( zoneList(i) );
        if (aZone.IsNull())
            continue;
        DEBTRACE("  zone tested: " << aZone->GetName().toStdString());
        HYDROData_SequenceOfObjects aSeq = aZone->GetObjects();
        HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
        for ( ; anIter.More(); anIter.Next() )
        {
            Handle(HYDROData_Entity) aMergeObject;
            Handle(HYDROData_Object) aRefGeomObj = Handle(HYDROData_Object)::DownCast( anIter.Value() );
            if ( !aRefGeomObj.IsNull() )
            {
                DEBTRACE("    aRefGeomObj " << aRefGeomObj->GetName().toStdString());
                if (aRefGeomObj->GetName() == anObjectName)
                {
                     DEBTRACE("      BINGO! modify altitude object in zone");
                     aMergeObject = aRefGeomObj->GetAltitudeObject();
                     aZone->SetMergeObject( aMergeObject );
                     break;
                }
            }            
        }
      
    }
  }
  return true;
}

void HYDROGUI_ImmersibleZoneOp::onCreatePreview( const QString& thePolylineName )
{
  HYDROGUI_ImmersibleZoneDlg* aPanel = ::qobject_cast<HYDROGUI_ImmersibleZoneDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QApplication::setOverrideCursor( Qt::WaitCursor );
  TopoDS_Shape aZoneShape;

  Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), thePolylineName, KIND_POLYLINEXY ) );
  if ( !aPolyline.IsNull() )
  {
    aZoneShape = HYDROData_ImmersibleZone::generateTopShape( aPolyline );
    if( aZoneShape.IsNull() )
      printErrorMessage( tr( "ZONE_OBJECT_CANNOT_BE_CREATED" ) );
  }

  LightApp_Application* anApp = module()->getApp();
  if ( !getPreviewManager() )
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                       anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager && !myPreviewPrs )
  {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
        myPreviewPrs = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );
    }
  }

  if ( aViewManager && myPreviewPrs )
  {
    QColor aFillingColor = Qt::darkBlue;
    QColor aBorderColor = Qt::transparent;
    if ( !myEditedObject.IsNull() ) {
      aFillingColor = myEditedObject->GetFillingColor();
      aBorderColor = myEditedObject->GetBorderColor();
    }

    TopoDS_Face aFace;
    if( !aZoneShape.IsNull() )
      aFace = TopoDS::Face( aZoneShape );
    myPreviewPrs->setFace( aFace, true, true, "" );
    myPreviewPrs->setFillingColor( aFillingColor, false, false );
    myPreviewPrs->setBorderColor( aBorderColor, false, false );
  }

  QApplication::restoreOverrideCursor();
}

void HYDROGUI_ImmersibleZoneOp::closePreview()
{
  if( myPreviewPrs )
  {
    delete myPreviewPrs;
    myPreviewPrs = 0;
  }
}

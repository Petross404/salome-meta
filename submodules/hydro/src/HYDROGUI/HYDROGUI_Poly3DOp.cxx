// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Poly3DOp.h"

#include "HYDROGUI_Module.h"
#include <HYDROGUI_DataObject.h>
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Poly3DDlg.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Document.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_Profile.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Bathymetry.h>

#include <HYDROData_OperationsFactory.h>

HYDROGUI_Poly3DOp::HYDROGUI_Poly3DOp( HYDROGUI_Module* theModule,
                                            const bool theIsEdit )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit ),
  myEditedObject( 0 )
{
  setName( theIsEdit ? tr( "EDIT_POLYLINE_3D" ) : tr( "CREATE_POLYLINE_3D" ) );
}

HYDROGUI_Poly3DOp::~HYDROGUI_Poly3DOp()
{
}

HYDROGUI_InputPanel* HYDROGUI_Poly3DOp::createInputPanel() const
{
  return new HYDROGUI_Poly3DDlg( module(), getName() );
}

void HYDROGUI_Poly3DOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_Poly3DDlg* aPanel = (HYDROGUI_Poly3DDlg*)inputPanel();
  aPanel->reset();
  aPanel->setMode( myIsEdit );

  QString aPoly3DName;
  if( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_Polyline3D)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !myEditedObject.IsNull() )
      aPoly3DName = myEditedObject->GetName();
  }
  else
  {
    aPoly3DName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_POLYLINE_3D_NAME" ) );
  }
  aPanel->setResultName( aPoly3DName );

  QString aPolyName, aProfileName, aBathName;
  if( myIsEdit && !myEditedObject.IsNull() )
  {
    Handle(HYDROData_ProfileUZ) aProfileUZ = myEditedObject->GetProfileUZ();
    if( !aProfileUZ.IsNull() )
    {
      Handle(HYDROData_Entity) aProfile = aProfileUZ->GetFatherObject();
      if ( !aProfile.IsNull() )
        aProfileName = aProfile->GetName();
    }

    Handle(HYDROData_Entity) aPoly = myEditedObject->GetPolylineXY();
    if( !aPoly.IsNull() )
      aPolyName = aPoly->GetName();

    Handle(HYDROData_IAltitudeObject) anAltitudeObj = myEditedObject->GetAltitudeObject();
    if( !anAltitudeObj.IsNull() )
      aBathName = anAltitudeObj->GetName();

    aPanel->setSelectedObjects( aPolyName, aProfileName, aBathName );
  }
  else if( !myIsEdit )
  {
    Handle(HYDROData_Profile) aSelectedProfile =
      Handle(HYDROData_Profile)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !aSelectedProfile.IsNull() )
    {
      QString aSelectedName = aSelectedProfile->GetName();
      aPanel->setPreselectedObject( aSelectedName );
    }
  }
}

bool HYDROGUI_Poly3DOp::processApply( int& theUpdateFlags,
                                      QString& theErrorMsg,
                                      QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_Poly3DDlg* aPanel = dynamic_cast<HYDROGUI_Poly3DDlg*>( inputPanel() );

  QString aResultName = aPanel->getResultName();
  if( aResultName.isEmpty() )
    return false;

  QString aPolyName, aProfileName, aBathName;
  if( !aPanel->getSelectedObjects( aPolyName, aProfileName, aBathName ) )
    return false;

  if( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != aResultName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), aResultName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( aResultName );
      return false;
    }
  }

  Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( 
    HYDROGUI_Tool::FindObjectByName( module(), aProfileName, KIND_PROFILE ) );
  Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast( 
    HYDROGUI_Tool::FindObjectByName( module(), aPolyName, KIND_POLYLINEXY ) );
  Handle(HYDROData_Bathymetry) aBath = Handle(HYDROData_Bathymetry)::DownCast( 
    HYDROGUI_Tool::FindObjectByName( module(), aBathName, KIND_BATHYMETRY ) );

  if ( aPolyline.IsNull() || ( aProfile.IsNull() && aBath.IsNull() ) )
    return false;

  Handle(HYDROData_Polyline3D) aResult;
  if( myIsEdit )
  {
    aResult = myEditedObject;
  }
  else
  {
    aResult = Handle(HYDROData_Polyline3D)::DownCast( doc()->CreateObject( KIND_POLYLINE ) );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aResult );
    theBrowseObjectsEntries.append( anEntry );
  }

  if( aResult.IsNull() )
    return false;

  aResult->SetName( aResultName );

  aResult->SetPolylineXY( aPolyline, false );
  if ( !aProfile.IsNull() )
  {
    Handle(HYDROData_ProfileUZ) aProfileUZ = aProfile->GetProfileUZ();
    if( aProfileUZ.IsNull() )
      return false;
    
    aResult->SetProfileUZ( aProfileUZ );
  }
  else
  {
    aResult->SetAltitudeObject( aBath );
  }

  if( !myIsEdit )
  {
    aResult->SetBorderColor( aResult->DefaultBorderColor() );
  }

  aResult->Update();

  if( !myIsEdit )
  {
    size_t aViewId = HYDROGUI_Tool::GetActiveOCCViewId( module() );
    module()->setObjectVisible( aViewId, aPolyline, false );
    module()->setObjectVisible( aViewId, aProfile, false );
    module()->setObjectVisible( aViewId, aResult, true );
  }
  module()->setIsToUpdate( aResult );

  theUpdateFlags = UF_Model | UF_Viewer | UF_GV_Forced | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
  return true;
}

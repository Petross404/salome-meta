// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Operation.h"

#include "HYDROGUI_InputPanel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_Shape.h"

#include <HYDROData_Document.h>
#include <HYDROData_Iterator.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>

#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Study.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>

#include <QApplication>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

bool HYDROGUI_Operation::myIsClear = false;

HYDROGUI_Operation::HYDROGUI_Operation( HYDROGUI_Module* theModule )
: LightApp_Operation(),
  myModule( theModule ),
  myPanel( 0 ),
  myIsPrintErrorMessage( true ),
  myIsTransactionOpened( false ),
  myPreviewManager( 0 ),
  myPreviewZLayer( -1 ),
  myIsApplyAndClose( true )
{
  DEBTRACE("HYDROGUI_Operation");
  connect( this, SIGNAL( helpContextModule( const QString&, const QString&,
                                            const QString& ) ),
           theModule->application(), SLOT( onHelpContextModule( const QString&,
                                            const QString&, const QString& ) ) );
}

HYDROGUI_Operation::~HYDROGUI_Operation()
{
  DEBTRACE("~HYDROGUI_Operation");
}

void HYDROGUI_Operation::setName( const QString& theName )
{
  myName = theName;
}

const QString& HYDROGUI_Operation::getName() const
{
  return myName;
}

HYDROGUI_InputPanel* HYDROGUI_Operation::inputPanel() const
{
  DEBTRACE("inputPanel");
  if( !myPanel )
  {
    ( ( HYDROGUI_Operation* )this )->myPanel = createInputPanel();
    if (myPanel)
    {
		connect( myPanel, SIGNAL( panelApplyAndClose() ), this, SLOT( onApplyAndClose() ) );
		connect( myPanel, SIGNAL( panelApply() ),  this, SLOT( onApply() ) );
		connect( myPanel, SIGNAL( panelCancel() ), this, SLOT( onCancel() ) );
		connect( myPanel, SIGNAL( panelHelp() ), this, SLOT( onHelp() ) );
    }
  }
  return myPanel;
}

SUIT_SelectionMgr* HYDROGUI_Operation::selectionMgr() const
{
  return myModule->getApp()->selectionMgr();
}

HYDROGUI_Module* HYDROGUI_Operation::module() const
{
  return myModule;
}

/**
  * Returns Z layer of the operation preview.
   \ returns a layer position
 */
int HYDROGUI_Operation::getPreviewZLayer() const
{
  return myPreviewZLayer;
}

/**
 * Update Z layer for the operation preview.
   \param theLayer a layer position
 */
void HYDROGUI_Operation::updatePreviewZLayer( int theLayer )
{
  setPreviewZLayer( theLayer );

  HYDROGUI_Shape* aPreview = getPreviewShape();
  if ( aPreview )
    aPreview->setZLayer( getPreviewZLayer() );
}

/**
 * Set Z layer for the operation preview.
 \param theLayer a layer position
 */
void HYDROGUI_Operation::setPreviewZLayer( int theLayer )
{
  if ( theLayer != myPreviewZLayer )
    myPreviewZLayer = theLayer;
}

/**
 * Returns a shape preview of the operation
 */
HYDROGUI_Shape* HYDROGUI_Operation::getPreviewShape() const
{
  return 0;
}

/**
 * Return the operation preview manager
 */
OCCViewer_ViewManager* HYDROGUI_Operation::getPreviewManager()
{
  return myPreviewManager;
}

/**
 * Set the preview manager
 */
void HYDROGUI_Operation::setPreviewManager( OCCViewer_ViewManager* theManager )
{
  //No good: preview Z layer could be used by usual presentations
  //if ( !theManager && myPreviewManager )
  //  module()->getOCCDisplayer()->RemoveZLayer( myPreviewManager, getPreviewZLayer() );

  myPreviewManager = theManager;
  // the manager can be null on abort Operation and we need to clear ZLayers on first call, when on new document (second study without SALOME restart)
  OCCViewer_ViewManager* aViewManager = myPreviewManager;
  if ( !aViewManager )
    aViewManager = dynamic_cast<OCCViewer_ViewManager*>( module()->getApp()->getViewManager( OCCViewer_Viewer::Type(), true ) );
  DEBTRACE("aViewManager, myPreviewManager " << aViewManager << " " << myPreviewManager);

  if (!myIsClear)
    {
      module()->getOCCDisplayer()->RemoveZLayer(aViewManager, 0, true);
      myIsClear = true;
    }
  if ( myPreviewManager )
    setPreviewZLayer( module()->getOCCDisplayer()->AddPreviewZLayer( myPreviewManager ) );
}

void HYDROGUI_Operation::setCursor()
{
  if ( myPreviewManager )
  {
    QVector<SUIT_ViewWindow*> winList = myPreviewManager->getViews();
    for ( QVector<SUIT_ViewWindow*>::iterator it = winList.begin(); it != winList.end(); ++it )
    {
      OCCViewer_ViewWindow* occWin = ::qobject_cast<OCCViewer_ViewWindow*>( *it );
      if ( occWin )
      {
        OCCViewer_ViewPort3d* vp = occWin->getViewPort();
        if ( vp )
        {
          // Save old cursor
          myCursor = vp->cursor();
          // Set specific cursor chosen in preferences
          QCursor aCursor = module()->getPrefEditCursor();
          vp->setDefaultCursor( aCursor.shape() );
          vp->setCursor( *vp->getDefaultCursor() );
        }
      }
    }
  }
}

void HYDROGUI_Operation::restoreCursor()
{
  if ( myPreviewManager )
  {
    QVector<SUIT_ViewWindow*> winList = myPreviewManager->getViews();
    for ( QVector<SUIT_ViewWindow*>::iterator it = winList.begin(); it != winList.end(); ++it )
    {
      OCCViewer_ViewWindow* occWin = ::qobject_cast<OCCViewer_ViewWindow*>( *it );
      if ( occWin )
      {
        OCCViewer_ViewPort3d* vp = occWin->getViewPort();
        if ( vp )
        {
          // Restore old cursor
          vp->setDefaultCursor( myCursor.shape() );
          vp->setCursor( myCursor );
        }
      }
    }
  }
}

void HYDROGUI_Operation::setIsApplyAndClose( const bool theFlag )
{
  myIsApplyAndClose = theFlag;
}

bool HYDROGUI_Operation::isApplyAndClose() const
{
  return myIsApplyAndClose;
}

void HYDROGUI_Operation::apply()
{
  QApplication::setOverrideCursor( Qt::WaitCursor );

  startDocOperation();

  int anUpdateFlags = 0;
  QString anErrorMsg;

  bool aResult = false;
  QStringList aBrowseObjectsEntries;

  try
  {
    aResult = processApply( anUpdateFlags, anErrorMsg, aBrowseObjectsEntries );
  }
  catch ( Standard_Failure& e )
  {
    if (&e)
      anErrorMsg = e.GetMessageString();
    else
      anErrorMsg = "failure unknown: catch ( Standard_Failure ) does not give access to failure!";
    aResult = false;
  }
  catch ( ... )
  {
    aResult = false;
  }
  
  QApplication::restoreOverrideCursor();

  if ( aResult )
  {
    module()->update( anUpdateFlags );
    commitDocOperation();
    commit();
    browseObjects( aBrowseObjectsEntries, myIsApplyAndClose );

    if ( !myIsApplyAndClose && inputPanel() )
      start();
  }
  else if( !anErrorMsg.isEmpty() )
  {
    // Abort document opeartion only if requested
    if ( isToAbortOnApply() )
      abortDocOperation();

    printErrorMessage( anErrorMsg );
 
    // If the operation has no input panel - do abort
    if ( !inputPanel() ) {
      abort();
    }
  } 
}

void HYDROGUI_Operation::startOperation()
{
  LightApp_Operation::startOperation();
  myModule->getActiveOperations().push( this );

  if( myIsApplyAndClose && inputPanel() )
  {
    myModule->getApp()->desktop()->addDockWidget( Qt::RightDockWidgetArea, inputPanel() );
    inputPanel()->show();
  }
}

void HYDROGUI_Operation::abortOperation()
{
  LightApp_Operation::abortOperation();
  closeInputPanel();
}

void HYDROGUI_Operation::commitOperation()
{
  LightApp_Operation::commitOperation();
  if ( myIsApplyAndClose )
    closeInputPanel();
}

void HYDROGUI_Operation::stopOperation()
{
  LightApp_Operation::stopOperation();

  // pop the operation from the cached map of active operations
  QStack<HYDROGUI_Operation*>& anOperations = myModule->getActiveOperations();
  if ( !anOperations.empty() ) {
    if ( anOperations.top() == this )
      anOperations.pop();
    else
    {
      // find in the stack the current operation and remove it from the stack
      QVectorIterator<HYDROGUI_Operation*> aVIt( anOperations );
      aVIt.toBack();
      aVIt.previous(); // skip the top show/hide operation
      while ( aVIt.hasPrevious() )
      {
        HYDROGUI_Operation* anOp = aVIt.previous();
        if ( anOp == this )
          anOperations.remove( anOperations.lastIndexOf( anOp ) );
      }
    }
  }
  // release the preview manager with removing the added preview Z layer
  setPreviewManager( 0 );
}

void HYDROGUI_Operation::setDialogActive( const bool active )
{
  LightApp_Operation::setDialogActive( active );
  if( myPanel )
  {
    if( active )
    {
      myPanel->show();
    }
  }
}

HYDROGUI_InputPanel* HYDROGUI_Operation::createInputPanel() const
{
  return NULL;
}

void HYDROGUI_Operation::closeInputPanel()
{
  if( myPanel )
  {
    myModule->getApp()->desktop()->removeDockWidget( myPanel );
    delete myPanel;
    myPanel = 0;
  }
}

bool HYDROGUI_Operation::processApply( int& theUpdateFlags,
                                       QString& theErrorMsg,
                                       QStringList& theBrowseObjectsEntries )
{
  return false;
}

void HYDROGUI_Operation::processCancel()
{
}

void HYDROGUI_Operation::startDocOperation()
{
  // Open transaction in the model document only if it not
  // already opened by other operation (intended for nested operations)
  if ( !doc()->IsOperation() )
  {
    doc()->StartOperation();
    myIsTransactionOpened = true;
  }
}

void HYDROGUI_Operation::abortDocOperation()
{
  // Abort transaction in the model document only if it was 
  // opened by this operation (intended for nested operations)
  if ( myIsTransactionOpened && doc()->IsOperation() )
  {
    doc()->AbortOperation();
    myIsTransactionOpened = false;
  }
}

void HYDROGUI_Operation::commitDocOperation()
{
  // Commit transaction in the model document only if it was 
  // opened by this operation (intended for nested operations)
  if ( myIsTransactionOpened && doc()->IsOperation() )
  {
    doc()->CommitOperation( HYDROGUI_Tool::ToExtString( getName() ) );
    myIsTransactionOpened = false;
  }
}

Handle(HYDROData_Document) HYDROGUI_Operation::doc() const
{
  return HYDROData_Document::Document();
}

void HYDROGUI_Operation::onApplyAndClose()
{
  myIsApplyAndClose = true;
  apply();
  restoreOCCViewerSelection();
}

void HYDROGUI_Operation::onApply()
{
  myIsApplyAndClose = false;
  apply();
  restoreOCCViewerSelection();
}

void HYDROGUI_Operation::setPrintErrorMessage( const bool theIsPrint )
{
  myIsPrintErrorMessage = theIsPrint;
}

void HYDROGUI_Operation::printErrorMessage( const QString& theErrorMsg )
{
  if ( myIsPrintErrorMessage )
  {
    QString aMsg = tr( "INPUT_VALID_DATA" );
    if( !theErrorMsg.isEmpty() )
      aMsg.prepend( theErrorMsg + "\n" );
    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "INSUFFICIENT_INPUT_DATA" ),
                               aMsg );

  }
  
  myIsPrintErrorMessage = true;
}

void HYDROGUI_Operation::onCancel()
{
  processCancel();
  abort();
  if ( myPanel )
    abortOperation();
  myIsApplyAndClose = true;
  restoreOCCViewerSelection();
}

void HYDROGUI_Operation::restoreOCCViewerSelection(OCCViewer_ViewManager* theViewManager)
{
  DEBTRACE("restoreOCCViewerSelection " << theViewManager);
  LightApp_Application* anApp = module()->getApp();
  LightApp_SelectionMgr* sm = anApp->selectionMgr();
  if(sm)
  {
	  sm->clearFilters(); // see GEOM_Displayer::GlobalSelection
  }
  OCCViewer_ViewManager* aViewManager = theViewManager;
  if (!aViewManager)
    aViewManager = dynamic_cast<OCCViewer_ViewManager*>( anApp->getViewManager( OCCViewer_Viewer::Type(), true ) );
  if( aViewManager )
  {
	if( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
	{
	  Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
	  if( !aCtx.IsNull() )
	  {
		  DEBTRACE("aViewManager " << aViewManager << " AIScontext " << aCtx.get());
		  aCtx->Deactivate();
		  aCtx->Activate(0);
		  aCtx->SetAutomaticHilight( Standard_True );
		  aCtx->RemoveFilters();
	  }
	}
  }
}
void HYDROGUI_Operation::onHelp()
{
   emit helpContextModule( getHelpComponent(), getHelpFile(), getHelpContext() );
}

QString HYDROGUI_Operation::getHelpComponent() const
{
   return module()->moduleName();
}

QString HYDROGUI_Operation::getHelpFile() const
{
  QString aFileName = ((myName.isEmpty())? operationName() : myName);
  aFileName = aFileName.toLower();
  aFileName = aFileName.replace(QRegExp("\\s"), "_").append(".html");
  aFileName.remove( "create_" );
  aFileName.remove( "edit_" );
  return aFileName;
}

QString HYDROGUI_Operation::getHelpContext() const
{
   return QString();
}

void HYDROGUI_Operation::browseObjects( const QStringList& theBrowseObjectsEntries,
                                        const bool theIsApplyAndClose/* = true*/ )
{
  bool isOptimizedBrowse = true;
  module()->getApp()->browseObjects( theBrowseObjectsEntries, theIsApplyAndClose, isOptimizedBrowse );
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_CALCULATIONOP_H
#define HYDROGUI_CALCULATIONOP_H

#include "HYDROGUI_Operation.h"

#include "HYDROData_SplitToZonesTool.h"
#include "HYDROGUI_Shape.h"

#include <HYDROData_CalculationCase.h>
#include <HYDROData_Region.h>

class SUIT_ViewManager;
class OCCViewer_ViewManager;
class HYDROGUI_CalculationDlg;
class SUIT_DataObject;

class HYDROGUI_CalculationOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_CalculationOp( HYDROGUI_Module* theModule, bool theIsEdit, bool IsComplete );
  virtual ~HYDROGUI_CalculationOp();

protected:
  virtual void                    startOperation();
  virtual void                    abortOperation();
  virtual void                    commitOperation();

  virtual HYDROGUI_InputPanel*    createInputPanel() const;

  virtual void                    onApply();
  virtual bool                    processApply( int& theUpdateFlags, QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries );

  void setAvailableGroups();

  void setAvailableBoundaryPolygons();

protected slots:

  /**
   * Change the creation mode.
   * @param theMode the mode to set
   */
  void                            onChangeMode( int theMode );

  /**
   * Add geometry objects selected in the module browser to the calculation case.
   */
  void                            onAddObjects();
  /**
   * Remove selected objects from the calculation case.
   */
  void                            onRemoveObjects();

  /**
   * Add geometry groups selected in the module browser to the calculation case.
   */
  void                            onAddGroups();
  /**
   * Remove selected groups from the calculation case.
   */
  void                            onRemoveGroups();

  void                            onAddBoundaryPolygons();

  void                            onRemoveBoundaryPolygons();

  /**
   * Set the given bathymetry/type merge type to the current zone.
   */
  void                            onSetMergeType( int theMergeType, QString& theMergeObjectName );
  /**
   * Selected zones are moved to the existing region.
   */
  void                            onMoveZones( SUIT_DataObject* theRegionItem, 
                                               const QList<SUIT_DataObject*>& theZonesList);
  /**
   * Selected zones are moved to the new region to be created.
   */
  void                            onCreateRegion( const QList<SUIT_DataObject*>& theZonesList );
  /**
   * Case objects must be split to zones if the case has been modified or new.
   */
  void                            onNext( const int );
  void                            onLastViewClosed( SUIT_ViewManager* );
  /**
   * The zone is selected in the browser on the second page of the wizard.
   */
  void                            onClickedInZonesBrowser( SUIT_DataObject* theItem );
  /**
   * Slot called on back button click.
   */
  void                            onHideZones( const int );
  /** 
   * Geometry object is selected in the list on the first wizard page
   */
  void                            onObjectsSelected();
  /** 
   * Land cover map is selected in the list on the third wizard page
   */
  void                            onLandCoverMapSelected( const QString & theObjName );
  /** 
   * Boundary polyline is selected in the list on the first wizard page
   */
  void                            onBoundarySelected( const QString & theObjName );
  /** 
   * Strickler table name is selected in the list on the third wizard page
   */
  void                            onStricklerTableSelected( const QString & theObjName );

  void onOrderChanged( bool& isConfirmed );

  void onRuleChanged( bool& isConfirmed );

  void onRegenerateColors();

private:
  void                            createPreview( const bool theLandCoverMap, bool fitAllFlag = true, bool onTopViewAndFit = true);
  void                            closePreview( bool theRemoveViewManager = true );
  void                            setObjectVisibility( Handle(HYDROData_Entity) theEntity, const bool theIsVisible );
  void                            setZonesVisible( bool theIsVisible );
  void                            setGeomObjectsVisible( bool theIsVisible );
  void                            setLandCoverMapVisible( bool theIsVisible );
  void                            getNamesAndEntries( const HYDROData_SequenceOfObjects& theSeq, 
                                                      QStringList& theNames, QStringList& theEntries ) const;

  /**
   * Internal method that used to assign unique default colors for zones
   */
  void                            AssignDefaultZonesColors(const QSet<QString>* theRegionsEntriesToColored = NULL);
  void                            setRules( HYDROData_CalculationCase::DataTag theDataTag );

  bool                            createRegion( const QList<SUIT_DataObject*>& theZonesList );

  void                            AddInterPolylinesToList(QStringList& theList, QStringList& theEntryList);

  bool confirmRegionsChange() const;
  bool confirmModeChange() const;
  bool confirmOrderChange() const;
  bool confirmRuleChange() const;
  bool confirmContinueWithWarning( const HYDROData_Warning& theWarning ) const;

private:
  bool                            myIsEdit;
  bool                            myShowZones;
  bool                            myShowGeomObjects;
  bool                            myShowLandCoverMap;
  Handle(HYDROData_CalculationCase) myEditedObject;

  SUIT_ViewManager*               myActiveViewManager;
  OCCViewer_ViewManager*          myPreviewViewManager;

  bool myIsComplete;
};

#endif



// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_CalculationDlg.h"

#include "HYDROGUI_ObjSelector.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_DataBrowser.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_ListSelector.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_NameValidator.h"
#include "HYDROGUI_Region.h"
#include "HYDROGUI_Zone.h"
#include "HYDROGUI_OrderedListWidget.h"
#include "HYDROGUI_PriorityWidget.h"
#include "HYDROGUI_PriorityTableModel.h"

#include <HYDROData_Document.h>
#include <HYDROData_Entity.h>

#include <CAM_Application.h>
#include <LightApp_DataObject.h>

#include <SUIT_DataObject.h>
#include <SUIT_FileDlg.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SUIT_Study.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>

#include <QButtonGroup>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QListWidget>
#include <QPicture>
#include <QPushButton>
#include <QRadioButton>
#include <QSplitter>
#include <QTableWidget>
#include <QToolButton>
#include <QTreeView>
#include <QWizardPage>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"


HYDROGUI_CalculationDlg::HYDROGUI_CalculationDlg( HYDROGUI_Module* theModule, const QString& theTitle, bool IsComplete )
: HYDROGUI_Wizard( theModule, theTitle )
{
  myIsComplete = IsComplete;
  myAlreadyAddedGeomObjects = NULL;
  myPolylineName = NULL;
  myLandCoverMapName = NULL;
  myStricklerTableName = NULL;
  myAvailableGeomObjects = NULL;
  addPage( createObjectsPage() );
  if (!IsComplete)
  {
    addPage( createGroupsPage() );
    addPage( createBoundaryPolygonsPage() );
    addPage( createLandCoverMapPage() );
  }
  addPage( createZonesPage() );
}

HYDROGUI_CalculationDlg::~HYDROGUI_CalculationDlg()
{
}

void HYDROGUI_CalculationDlg::reset()
{
  myObjectName->clear();
  HYDROGUI_ListModel::Object2VisibleList anObject2VisibleList;
  myGeomObjects->setObjects(anObject2VisibleList);
  if (myAlreadyAddedGeomObjects)
    myAlreadyAddedGeomObjects->clear();
  if (myPolylineName)
    myPolylineName->clear();
  if (myLandCoverMapName)
    myLandCoverMapName->clear();
  if (myStricklerTableName)
    myStricklerTableName->clear();
  if (myAvailableGeomObjects)
    myAvailableGeomObjects->clear();

  // Activate the automatic mode
  setMode( HYDROData_CalculationCase::AUTOMATIC );

  // Reset the priority widget state
  QList<Handle(HYDROData_Entity)> anObjects;
  myPriorityWidget->setObjects( anObjects );
}

QWizardPage* HYDROGUI_CalculationDlg::createObjectsPage() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  // Splitter
  mySplitter = new QSplitter(Qt::Vertical);

  // Top of the page
  QWidget* aTopContainer = new QWidget;
   
  // calculation name
  myObjectName = new QLineEdit( aPage );
  myValidator = new HYDROGUI_NameValidator(module(), myObjectName);
  myObjectName->setValidator( myValidator );

  connect( myValidator, SIGNAL( emptyName() ), SLOT( onEmptyName() ) );
  connect( myValidator, SIGNAL( alreadyExists( QString ) ), SLOT( onAlreadyExists( QString ) ) );

  // polyline name
  myPolylineName = new QComboBox( aPage );
  connect( myPolylineName, SIGNAL( activated( const QString & ) ), 
    SIGNAL( boundarySelected( const QString & ) ) );

  // names labels
  QLabel* aNameLabel = new QLabel( tr( "NAME" ), aPage );
  QLabel* aLimitsLabel = new QLabel( tr( "LIMITS" ), aPage );

  // mode selector (auto/manual)
  QGroupBox* aModeGroup = new QGroupBox( tr( "MODE" ) );

  QRadioButton* aManualRB = new QRadioButton( tr( "MANUAL" ), mainFrame() );
  QRadioButton* anAutoRB = new QRadioButton( tr( "AUTO" ), mainFrame() );

  myModeButtons = new QButtonGroup( mainFrame() );
  myModeButtons->addButton( anAutoRB, HYDROData_CalculationCase::AUTOMATIC );
  myModeButtons->addButton( aManualRB, HYDROData_CalculationCase::MANUAL );
  
  QBoxLayout* aModeSelectorLayout = new QHBoxLayout;
  aModeSelectorLayout->setMargin( 5 );
  aModeSelectorLayout->setSpacing( 5 );
  aModeSelectorLayout->addWidget( anAutoRB );
  aModeSelectorLayout->addWidget( aManualRB );
  aModeGroup->setLayout( aModeSelectorLayout );

  // geometry objects
  QLabel* anObjectsLabel = new QLabel( tr( "CALCULATION_REFERENCE_OBJECTS" ) );
  myGeomObjects = new HYDROGUI_OrderedListWidget( aPage, 16 );
  myGeomObjects->setHiddenObjectsShown(true);
  myGeomObjects->setVisibilityIconShown(false);
  myGeomObjects->setContentsMargins(QMargins());

  if (myIsComplete)
  {
    myAlreadyAddedGeomObjects = new QListWidget( aPage );
    myAlreadyAddedGeomObjects->setSelectionMode( QListWidget::ExtendedSelection );
    myAlreadyAddedGeomObjects->setEditTriggers( QListWidget::NoEditTriggers );
    myAlreadyAddedGeomObjects->setViewMode( QListWidget::ListMode );
  }
  else
  {
    myAlreadyAddedGeomObjects = NULL;
  }
 
  // included geometry objects
  QLabel* anIncludedLabel = new QLabel( tr( "INCLUDED_OBJECTS" ) );
  myAvailableGeomObjects = new QListWidget( aPage );
  myAvailableGeomObjects->setSelectionMode( QListWidget::ExtendedSelection );
  myAvailableGeomObjects->setEditTriggers( QListWidget::NoEditTriggers );
  myAvailableGeomObjects->setViewMode( QListWidget::ListMode );
  myAvailableGeomObjects->setSortingEnabled( true );

  // buttons
  QFrame* aBtnsFrame = new QFrame;
  QVBoxLayout* aBtnsLayout = new QVBoxLayout( aBtnsFrame );
  aBtnsLayout->setMargin( 5 );
  aBtnsLayout->setSpacing( 5 );
  aBtnsFrame->setLayout( aBtnsLayout );
  QPushButton* anAddBtn = new QPushButton( tr("INCLUDE"), aBtnsFrame );
  QPushButton* aRemoveBtn = new QPushButton( tr("EXCLUDE"), aBtnsFrame );

  // fill the butons frame with two buttons
  aBtnsLayout->addWidget( anAddBtn );
  aBtnsLayout->addWidget( aRemoveBtn );
  aBtnsLayout->addStretch( 1 );
  
  // top of the page layout
  
  // objects frame
  QFrame* anObjectsFrame = new QFrame( aPage );
  anObjectsFrame->setFrameStyle( QFrame::Panel | QFrame::Raised );
  QGridLayout* anObjsLayout = new QGridLayout( anObjectsFrame );
  anObjsLayout->setMargin( 5 );
  anObjsLayout->setSpacing( 5 );
  anObjectsFrame->setLayout( anObjsLayout );
  
  // fill the objects frame with two lists, two labels and with buttons frame
  anObjsLayout->addWidget( anObjectsLabel, 0, 0, Qt::AlignHCenter );
  anObjsLayout->addWidget( anIncludedLabel, 0, 2, Qt::AlignHCenter );
  if (myIsComplete)
  {
    QLabel* anAlreadyIncludedLabel = new QLabel( tr( "ALREADY_INCLUDED_OBJECTS" ) );
    anObjsLayout->addWidget( anAlreadyIncludedLabel, 2, 2, Qt::AlignHCenter );
    anObjsLayout->addWidget( myAvailableGeomObjects, 1, 0, 3, 1 );
  }
  else
    anObjsLayout->addWidget( myAvailableGeomObjects, 1, 0 );

  anObjsLayout->addWidget( aBtnsFrame, 1, 1, Qt::AlignHCenter );
  anObjsLayout->addWidget( myGeomObjects, 1, 2 );
  if (myIsComplete)
  {
    anObjsLayout->addWidget( myAlreadyAddedGeomObjects, 3, 2 );  
  }
  
  // fill the top of the page
  QGridLayout* aTopLayout = new QGridLayout;
  aTopLayout->setMargin( 5 );
  aTopLayout->setSpacing( 5 );
  aTopLayout->setVerticalSpacing( 10 );
  aTopLayout->addWidget( aNameLabel,     0, 0, Qt::AlignHCenter );
  aTopLayout->addWidget( myObjectName,   0, 1 );
  aTopLayout->addWidget( aLimitsLabel,   1, 0, Qt::AlignHCenter );
  aTopLayout->addWidget( myPolylineName, 1, 1 );
  aTopLayout->addWidget( aModeGroup, 2, 0, 1, 2 );
  aTopLayout->addWidget( anObjectsFrame, 3, 0, 1, 2 );

  aTopContainer->setLayout( aTopLayout );

  // add the top of the page to the splitter
  mySplitter->insertWidget(0, aTopContainer);
  mySplitter->setStretchFactor(0, 2);

  // Bottom of the page
  myPriorityWidget = new HYDROGUI_PriorityWidget( mainFrame() );

  QGroupBox* aPriorityGroup = new QGroupBox( tr( "PRIORITY" ) );
  QBoxLayout* aPriorityLayout = new QHBoxLayout;
  aPriorityLayout->setMargin( 5 );
  aPriorityLayout->setSpacing( 5 );
  aPriorityLayout->addWidget( myPriorityWidget );
  aPriorityGroup->setLayout( aPriorityLayout );

  // add the bottom of the page to the splitter
  mySplitter->insertWidget(1, aPriorityGroup);
  mySplitter->setStretchFactor(1, 1);

  // Page layout
  QVBoxLayout* aPageLayout = new QVBoxLayout;
  aPageLayout->setMargin( 5 );
  aPageLayout->setSpacing( 5 );
  aPageLayout->addWidget( mySplitter );

  aPage->setLayout( aPageLayout );

  if (myIsComplete)
  {
    aLimitsLabel->setEnabled(false);
    myPolylineName->setEnabled(false);
    aModeGroup->setEnabled(false);
    myPriorityWidget->setEnabled(false);
  }

  // Create selector
  if ( module() ) {
    HYDROGUI_ListSelector* aListSelector = 
      new HYDROGUI_ListSelector( myGeomObjects, module()->getApp()->selectionMgr() );
    aListSelector->setAutoBlock( true );
  }

  // Connections
  connect( myModeButtons, SIGNAL( buttonClicked( int ) ), SIGNAL( changeMode( int ) ) );
  connect( anAddBtn, SIGNAL( clicked() ), SIGNAL( addObjects() ) );
  connect( aRemoveBtn, SIGNAL( clicked() ), SIGNAL( removeObjects() ) );

  connect( myGeomObjects, SIGNAL( orderChanged() ), SLOT( onOrderChanged() ) );

  connect( myPriorityWidget, SIGNAL( ruleChanged() ), SLOT( onRuleChanged() ) );

  return aPage;
}

QWizardPage* HYDROGUI_CalculationDlg::createGroupsPage() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  myGroups = new QListWidget( aPage );
  myGroups->setSelectionMode( QListWidget::ExtendedSelection );
  myGroups->setEditTriggers( QListWidget::NoEditTriggers );
  myGroups->setViewMode( QListWidget::ListMode );
  myGroups->setSortingEnabled( true );

  myAvailableGroups = new QListWidget( aPage );
  myAvailableGroups->setSelectionMode( QListWidget::ExtendedSelection );
  myAvailableGroups->setEditTriggers( QListWidget::NoEditTriggers );
  myAvailableGroups->setViewMode( QListWidget::ListMode );
  myAvailableGroups->setSortingEnabled( true );

  connect( myGroups, SIGNAL( itemSelectionChanged() ), 
    SIGNAL( groupsSelected() ) );

  QFrame* aGroupsFrame = new QFrame( aPage );
  QGridLayout* aGroupsLayout = new QGridLayout( aGroupsFrame );
  aGroupsLayout->setMargin( 5 );
  aGroupsLayout->setSpacing( 5 );
  aGroupsFrame->setLayout( aGroupsLayout );

  QFrame* aBtnsFrame = new QFrame( aGroupsFrame );
  QVBoxLayout* aBtnsLayout = new QVBoxLayout( aBtnsFrame );
  aBtnsLayout->setMargin( 5 );
  aBtnsLayout->setSpacing( 5 );
  aBtnsFrame->setLayout( aBtnsLayout );
  QPushButton* anAddBtn = new QPushButton( tr("INCLUDE"), aBtnsFrame );
  QPushButton* aRemoveBtn = new QPushButton( tr("EXCLUDE"), aBtnsFrame );

  // Fill the butons frame with two buttons
  aBtnsLayout->addWidget( anAddBtn );
  aBtnsLayout->addWidget( aRemoveBtn );
  aBtnsLayout->addStretch( 1 );

  QLabel* anIncludedLabel = new QLabel( tr( "INCLUDED_GROUPS" ), aGroupsFrame );
  QLabel* anAvailableLabel = new QLabel( tr( "AVAILABLE_GROUPS" ), aGroupsFrame );

  // Fill the objects frame with two lists, two labels and with buttons frame
  aGroupsLayout->addWidget( anAvailableLabel, 0, 0, Qt::AlignHCenter );
  aGroupsLayout->addWidget( anIncludedLabel, 0, 2, Qt::AlignHCenter );
  aGroupsLayout->addWidget( myAvailableGroups, 1, 0 );
  aGroupsLayout->addWidget( aBtnsFrame, 1, 1, Qt::AlignHCenter );
  aGroupsLayout->addWidget( myGroups, 1, 2 );

  // Fill the page
  QGridLayout* aPageLayout = new QGridLayout( aPage );
  aPageLayout->setMargin( 5 );
  aPageLayout->setSpacing( 5 );
  aPageLayout->setVerticalSpacing( 10 );
  aPageLayout->addWidget( aGroupsFrame, 0, 0 );

  aPage->setLayout( aPageLayout );

  connect( anAddBtn, SIGNAL( clicked() ), SIGNAL( addGroups() ) );
  connect( aRemoveBtn, SIGNAL( clicked() ), SIGNAL( removeGroups() ) );

  return aPage;
}

QWizardPage* HYDROGUI_CalculationDlg::createBoundaryPolygonsPage() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  myBoundaryPolygons = new QListWidget( aPage );
  myBoundaryPolygons->setSelectionMode( QListWidget::ExtendedSelection );
  myBoundaryPolygons->setEditTriggers( QListWidget::NoEditTriggers );
  myBoundaryPolygons->setViewMode( QListWidget::ListMode );
  myBoundaryPolygons->setSortingEnabled( true );

  myISBoundaryPolygons = new QListWidget( aPage );
  myISBoundaryPolygons->setSelectionMode( QListWidget::ExtendedSelection );
  myISBoundaryPolygons->setEditTriggers( QListWidget::NoEditTriggers );
  myISBoundaryPolygons->setViewMode( QListWidget::ListMode );
  myISBoundaryPolygons->setSortingEnabled( true );

  myAvailableBoundaryPolygons = new QListWidget( aPage );
  myAvailableBoundaryPolygons->setSelectionMode( QListWidget::ExtendedSelection );
  myAvailableBoundaryPolygons->setEditTriggers( QListWidget::NoEditTriggers );
  myAvailableBoundaryPolygons->setViewMode( QListWidget::ListMode );
  myAvailableBoundaryPolygons->setSortingEnabled( true );

  //connect( myGroups, SIGNAL( itemSelectionChanged() ), 
  //  SIGNAL( groupsSelected() ) ); //TODO

  QFrame* aBPFrame = new QFrame( aPage );
  QGridLayout* aBPLayout = new QGridLayout( aBPFrame );
  aBPLayout->setMargin( 5 );
  aBPLayout->setSpacing( 5 );
  aBPFrame->setLayout( aBPLayout );

  QFrame* aBtnsFrame = new QFrame( aBPFrame );
  QVBoxLayout* aBtnsLayout = new QVBoxLayout( aBtnsFrame );
  aBtnsLayout->setMargin( 5 );
  aBtnsLayout->setSpacing( 5 );
  aBtnsFrame->setLayout( aBtnsLayout );
  QPushButton* anAddBtn = new QPushButton( tr("INCLUDE"), aBtnsFrame );
  QPushButton* aRemoveBtn = new QPushButton( tr("EXCLUDE"), aBtnsFrame );

  // Fill the butons frame with two buttons
  aBtnsLayout->addWidget( anAddBtn );
  aBtnsLayout->addWidget( aRemoveBtn );
  aBtnsLayout->addStretch( 1 );

  QLabel* anIncludedLabelCut = new QLabel( tr( "INCLUDED_BOUNDARY_POLYGONS_CUT" ), aBPFrame );

  QLabel* anAvailableLabel = new QLabel( tr( "AVAILABLE_BOUNDARY_POLYGONS" ), aBPFrame );
  
  QLabel* anIncludedLabelIS = new QLabel( tr( "INCLUDED_BOUNDARY_POLYGONS_INCSEL" ) );

  // Fill the objects frame with two lists, two labels and with buttons frame
  aBPLayout->addWidget( anAvailableLabel, 0, 0, Qt::AlignHCenter );
  aBPLayout->addWidget( anIncludedLabelCut, 0, 2, Qt::AlignHCenter );

  aBPLayout->addWidget( myAvailableBoundaryPolygons, 1, 0, 3, 1 );
  aBPLayout->addWidget( aBtnsFrame, 1, 1, Qt::AlignHCenter );
  aBPLayout->addWidget( myBoundaryPolygons, 1, 2 );

  aBPLayout->addWidget( anIncludedLabelIS, 2, 2 );

  aBPLayout->addWidget( myISBoundaryPolygons, 3, 2 );


  // Fill the page
  QGridLayout* aPageLayout = new QGridLayout( aPage );
  aPageLayout->setMargin( 5 );
  aPageLayout->setSpacing( 5 );
  aPageLayout->setVerticalSpacing( 10 );
  aPageLayout->addWidget( aBPFrame, 0, 0 );

  aPage->setLayout( aPageLayout );

  connect( anAddBtn, SIGNAL( clicked() ), SIGNAL( addBoundaryPolygons() ) );
  connect( aRemoveBtn, SIGNAL( clicked() ), SIGNAL( removeBoundaryPolygons() ) );

  return aPage;
}


QWizardPage* HYDROGUI_CalculationDlg::createLandCoverMapPage() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  // Top of the page
  QWidget* aTopContainer = new QWidget;

  // Combo-box to choose land cover map object
  QLabel* aLandCoverMapLabel = new QLabel( tr( "LAND_COVER_MAP" ), aPage );
  myLandCoverMapName = new QComboBox( aPage );
  myLandCoverMapName->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  connect( myLandCoverMapName, SIGNAL( activated( const QString & ) ), 
                               SIGNAL( landCoverMapSelected( const QString & ) ) );
  // Combo-box to choose Strickler table name
  QLabel* aStricklerTableLabel = new QLabel( tr( "STRICKLER_TABLE" ), aPage );
  myStricklerTableName = new QComboBox( aPage );
  myStricklerTableName->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  connect( myStricklerTableName, SIGNAL( activated( const QString & ) ), 
                                 SIGNAL( StricklerTableSelected( const QString & ) ) );

  // Fill the top layout of the page
  QGridLayout* aGridLayout = new QGridLayout;
  aGridLayout->setMargin( 5 );
  aGridLayout->setSpacing( 5 );
  aGridLayout->setVerticalSpacing( 10 );
  aGridLayout->addWidget( aLandCoverMapLabel,   0, 0 );
  aGridLayout->addWidget( myLandCoverMapName,   0, 1 );
  aGridLayout->addWidget( aStricklerTableLabel, 1, 0 );
  aGridLayout->addWidget( myStricklerTableName, 1, 1 );

  QVBoxLayout* aTopLayout = new QVBoxLayout;
  aTopLayout->setMargin( 5 );
  aTopLayout->setSpacing( 5 );
  aTopLayout->addLayout( aGridLayout );
  aTopLayout->addStretch( 1 );
  
  aTopContainer->setLayout( aTopLayout );

  aPage->setLayout( aTopLayout );

  return aPage;
}

QWizardPage* HYDROGUI_CalculationDlg::createZonesPage() {
  QWizardPage* aPage = new QWizardPage( mainFrame() );
  QFrame* aFrame = new QFrame( aPage );

  QGridLayout* aLayout = new QGridLayout( aPage );

  QLabel* aResultsOnGeomObjectsLabel = new QLabel( tr( "RESULTS_ON_GEOMETRY_OBJECTS" ), aFrame );
  
  myBrowser = new HYDROGUI_DataBrowser( module(), NULL, aPage );
  myBrowser->setAutoOpenLevel( 3 );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  aLayout->addWidget( aResultsOnGeomObjectsLabel, 0, 0 );
  aLayout->addWidget( myBrowser, 1, 0, 1, 2 );

  myBathymetryLabel = new QLabel( tr( "BATHYMETRY" ), aFrame );
  myBathymetryChoice = new QComboBox( aFrame );

  myBathymetryChoice->setVisible( false );
  myBathymetryLabel->setVisible( false );

  aLayout->addWidget( myBathymetryLabel, 2, 0 );
  aLayout->addWidget( myBathymetryChoice, 2, 1 );

  QPushButton* aRegenerateBtn = new QPushButton( tr( "REGENERATE_COLORS" ), this );
  aLayout->addWidget( aRegenerateBtn, 3, 0 );

  aPage->setLayout( aLayout );

  connect( myBrowser, SIGNAL( dataChanged() ), SLOT( onDataChanged() ) );
  connect( myBrowser, SIGNAL( clicked( SUIT_DataObject* ) ), SIGNAL( clickedInZonesBrowser( SUIT_DataObject* ) ) );
  connect( myBrowser, SIGNAL( clicked( SUIT_DataObject* ) ), SLOT( onSelected( SUIT_DataObject* ) ) );
  connect( myBathymetryChoice, SIGNAL( activated( int ) ), SLOT( onMergeTypeSelected( int ) ) );
  connect( myBrowser, 
      SIGNAL( dropped( const QList<SUIT_DataObject*>&, SUIT_DataObject*, int, Qt::DropAction ) ),
      SLOT( onZonesDropped( const QList<SUIT_DataObject*>&, SUIT_DataObject*, int, Qt::DropAction ) ) );
  connect( myBrowser, SIGNAL( newRegion() ), this, SLOT( OnNewRegion() ) );
  connect( aRegenerateBtn, SIGNAL( clicked() ), this, SIGNAL( regenerateColors() ) );
  return aPage;
}

bool HYDROGUI_CalculationDlg::acceptCurrent() const
{
  QString anErrorMsg;

  if ( false /*myGeomObjects->count() == 0*/ )
  {
    anErrorMsg = tr( "EMPTY_GEOMETRY_OBJECTS" );
  }

  if ( !anErrorMsg.isEmpty() )
  {
    anErrorMsg += "\n" + tr( "INPUT_VALID_DATA" );
    
    QString aTitle = tr( "INSUFFICIENT_INPUT_DATA" );
    SUIT_MessageBox::critical( module()->getApp()->desktop(), aTitle, anErrorMsg );
  }

  return anErrorMsg.isEmpty();
}

void HYDROGUI_CalculationDlg::onEmptyName()
{
  QString aTitle = tr( "INSUFFICIENT_INPUT_DATA" );
  QString aMessage = tr( "INCORRECT_OBJECT_NAME" ) + "\n" + tr( "INPUT_VALID_DATA" );
  SUIT_MessageBox::critical( module()->getApp()->desktop(), aTitle, aMessage );
}

void HYDROGUI_CalculationDlg::onAlreadyExists( QString theName )
{
  QString aTitle = tr( "INSUFFICIENT_INPUT_DATA" );
  QString aMessage = QObject::tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( theName ) +
                     "\n" + tr( "INPUT_VALID_DATA" );
  SUIT_MessageBox::critical( module()->getApp()->desktop(), aTitle, aMessage );
}

void HYDROGUI_CalculationDlg::onZonesDropped( const QList<SUIT_DataObject*>& theList, 
    SUIT_DataObject* theTargetParent, int theTargetRow, Qt::DropAction theDropAction )
{
  QList<SUIT_DataObject*> aZonesList;
  HYDROGUI_Zone* aZone;
  // Get a list of dropped zones
  for ( int i = 0; i < theList.length(); i++ )
  {
    aZone = dynamic_cast<HYDROGUI_Zone*>( theList.at( i ) );
    if ( aZone )
    {
      aZonesList.append( aZone );
    }
  }
  if ( aZonesList.length() > 0 )
  {
    // Get the target region
    HYDROGUI_NamedObject* aRegionsRoot = dynamic_cast<HYDROGUI_NamedObject*>(theTargetParent);
    if ( aRegionsRoot )
    {
      // Create a new region
      emit createRegion( aZonesList );
    }
    else
    {
      HYDROGUI_Region* aRegion = dynamic_cast<HYDROGUI_Region*>(theTargetParent);
      if ( aRegion )
      {
        emit moveZones( theTargetParent, aZonesList );
      }
    }
  }
}

void HYDROGUI_CalculationDlg::OnNewRegion()
{
  emit createRegion( myBrowser->getSelected() );
}

void HYDROGUI_CalculationDlg::onMergeTypeSelected( int theIndex )
{
  int aType = myBathymetryChoice->itemData( theIndex ).toInt();
  QString aText = myBathymetryChoice->itemText( theIndex );
  emit setMergeType( aType, aText );
}

void HYDROGUI_CalculationDlg::onSelected( SUIT_DataObject* theObject )
{
  DEBTRACE("onSelected");
  bool doShow = false;
  HYDROGUI_Zone* aZone = dynamic_cast<HYDROGUI_Zone*>( theObject );
  if ( aZone )
  {
    doShow = aZone->isMergingNeed();
  }

  if ( doShow )
  {
    // Fill the merge type combo box
    bool prevBlock = myBathymetryChoice->blockSignals( true );
    myCurrentZone = aZone;
    myBathymetryChoice->clear();
    myBathymetryChoice->addItem( tr("MERGE_UNKNOWN"), HYDROData_Zone::Merge_UNKNOWN );
    myBathymetryChoice->addItem( tr("MERGE_ZMIN"), HYDROData_Zone::Merge_ZMIN );
    myBathymetryChoice->addItem( tr("MERGE_ZMAX"), HYDROData_Zone::Merge_ZMAX );
    QStringList aList = aZone->getObjects();
    for ( int i = 0; i < aList.length(); i++ )
    {
      myBathymetryChoice->addItem( aList.at( i ), HYDROData_Zone::Merge_Object );
    }
    // Select the current choice if any
    int aCurIndex = 0;
    switch ( aZone->getMergeType() )
    {
      case HYDROData_Zone::Merge_ZMIN:
        aCurIndex = 1;
        break;
      case HYDROData_Zone::Merge_ZMAX:
        aCurIndex = 2;
        break;
      case HYDROData_Zone::Merge_Object:
        aCurIndex = 3 + aList.indexOf( aZone->text( HYDROGUI_DataObject::AltitudeObjId ) );
        break;
      default:
        aCurIndex = 0; // Select unknown by default
    }
    myBathymetryChoice->setCurrentIndex( aCurIndex );
    myBathymetryChoice->blockSignals( prevBlock );
  }

  myBathymetryChoice->setVisible( doShow );
  myBathymetryChoice->setEnabled( getMode() == HYDROData_CalculationCase::MANUAL );
  myBathymetryLabel->setVisible( doShow );
}

void HYDROGUI_CalculationDlg::setObjectName( const QString& theName )
{
  myObjectName->setText( theName );
}

QString HYDROGUI_CalculationDlg::getObjectName() const
{
  return myObjectName->text();
}

void moveItems( QListWidget* theSource, QListWidget* theDest, const QStringList& theObjects )
{
  QList<QListWidgetItem*> aFoundItems;
  int anIdx;
  QListWidgetItem* anItem;

  for ( int i = 0, n = theObjects.length(); i < n; ++i )
  {
    QString anObjName = theObjects.at( i );
    aFoundItems = theSource->findItems( anObjName, Qt::MatchExactly );
    for ( anIdx = 0; anIdx < aFoundItems.length(); anIdx++ )
    {
      anItem = aFoundItems.at( anIdx );
      // Remove this object from available objects list
      anItem = theSource->takeItem( theSource->row( anItem ) );
      // Add the item to the included objects list
      theDest->addItem( anItem );
    }
  }
}

void HYDROGUI_CalculationDlg::includeGeomObjects( const QStringList& theObjects )
{
  QList<QListWidgetItem*> aFoundItems;
  foreach ( const QString& anObjName, theObjects ) {
    // Hide the object in the available objects list
    aFoundItems = myAvailableGeomObjects->findItems( anObjName, Qt::MatchExactly );
    foreach ( QListWidgetItem* anItem, aFoundItems ) {
      anItem->setHidden( true );
    }

    // Add the object to the list of included objects
    Handle(HYDROData_Entity) anObject = 
      HYDROGUI_Tool::FindObjectByName( module(), anObjName );
    myGeomObjects->addObject( HYDROGUI_ListModel::Object2Visible( anObject, true ) );
  }

  if (!myIsComplete)
    myPriorityWidget->setObjects( getGeometryObjects() );
}

void HYDROGUI_CalculationDlg::hideAvailableGeomObjects( const QStringList& theObjects )
{
  QList<QListWidgetItem*> aFoundItems;
  foreach ( const QString& anObjName, theObjects ) 
  {
    aFoundItems = myAvailableGeomObjects->findItems( anObjName, Qt::MatchExactly );
    foreach ( QListWidgetItem* anItem, aFoundItems ) 
      anItem->setHidden( true );
  }
}

void HYDROGUI_CalculationDlg::setAlreadyAddedGeomObjects( const QStringList& theObjects )
{
  myAlreadyAddedGeomObjects->addItems(theObjects);
}

 QStringList HYDROGUI_CalculationDlg::getAlreadyAddedGeomObjects()
{
  QStringList list;
  for (int i = 0; i < myAlreadyAddedGeomObjects->count(); i++)
    list << myAlreadyAddedGeomObjects->item(i)->text();
  return list;
}

void HYDROGUI_CalculationDlg::excludeGeomObjects( const QStringList& theObjects )
{
  QList<QListWidgetItem*> aFoundItems;
  foreach ( const QString& anObjName, theObjects ) {
    // Set visible the object in the available objects list
    aFoundItems = myAvailableGeomObjects->findItems( anObjName, Qt::MatchExactly );
    foreach ( QListWidgetItem* anItem, aFoundItems ) {
      anItem->setHidden( false );
    }

    // Remove the object from the list of included objects
    myGeomObjects->removeObjectByName( anObjName );
  }

  myPriorityWidget->setObjects( getGeometryObjects() );
}

void HYDROGUI_CalculationDlg::setBoundary( const QString& theObjName )
{
  bool isBlocked = myPolylineName->blockSignals( true );
  myPolylineName->setCurrentIndex( myPolylineName->findText( theObjName ) );
  myPolylineName->blockSignals( isBlocked );
}

void HYDROGUI_CalculationDlg::setPolylineNames( const QStringList& theObjects, const QStringList& theObjectsEntries )
{
  myPolylineName->clear();
  myPolylineName->addItem( "", "" ); // No boundary item

  for ( int i = 0, n = theObjects.length(); i < n; ++i )
  {
    myPolylineName->addItem( theObjects.at( i ), theObjectsEntries.at( i ) );
  }
}

void HYDROGUI_CalculationDlg::setLandCoverMapsNames( const QStringList& theObjects, const QStringList& theObjectsEntries )
{
  myLandCoverMapName->clear();

  for ( int i = 0, n = theObjects.length(); i < n; ++i )
  {
    myLandCoverMapName->addItem( theObjects.at( i ), theObjectsEntries.at( i ) );
  }
}

void HYDROGUI_CalculationDlg::setStricklerTableNames( const QStringList& theObjects, const QStringList& theObjectsEntries )
{
  myStricklerTableName->clear();

  for ( int i = 0, n = theObjects.length(); i < n; ++i )
  {
    myStricklerTableName->addItem( theObjects.at( i ), theObjectsEntries.at( i ) );
  }
}

void HYDROGUI_CalculationDlg::setAllGeomObjects( const QStringList& theObjects, const QStringList& theObjectsEntries )
{
  myAvailableGeomObjects->clear();

  for ( int i = 0, n = theObjects.length(); i < n; ++i )
  {
    QString anObjName = theObjects.at( i );

    QListWidgetItem* aListItem = new QListWidgetItem( anObjName, myAvailableGeomObjects );
    aListItem->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable );
    aListItem->setData( Qt::UserRole, theObjectsEntries.at( i ) );
  }
}

QStringList getSelected( QListWidget* theWidget )
{
  QStringList aResList;
  QList<QListWidgetItem*> aList = theWidget->selectedItems();
  for ( int i = 0, n = aList.length(); i < n; ++i )
  {
    aResList.append( aList.at( i )->text() );
  }
  return aResList;
}

QStringList HYDROGUI_CalculationDlg::getSelectedGeomObjects() const
{
  return myGeomObjects->getSelectedNames();
}

QStringList HYDROGUI_CalculationDlg::getAllGeomObjects() const
{
  return myGeomObjects->getAllNames();
}

QStringList HYDROGUI_CalculationDlg::getSelectedAvailableGeomObjects() const
{
  return getSelected( myAvailableGeomObjects );
}

void HYDROGUI_CalculationDlg::setEditedObject( const Handle(HYDROData_CalculationCase) theCase )
{
  myEditedObject = theCase;
  myValidator->setEditedObject( myEditedObject );

  // Build the calculation case subtree
  module()->getDataModel()->buildCaseTree( myBrowser->root(), myEditedObject);

  myBrowser->updateTree();
  myBrowser->openLevels();
  myBrowser->adjustColumnsWidth();
  myBrowser->setAutoUpdate( true );
  myBrowser->setUpdateModified( true );
}

HYDROGUI_Zone* HYDROGUI_CalculationDlg::getCurrentZone() const
{
  return myCurrentZone;
}

void HYDROGUI_CalculationDlg::refreshZonesBrowser()
{
  SUIT_DataObject* aRoot = myBrowser->root();
  module()->getDataModel()->updateObjectTree( myEditedObject );
  module()->getDataModel()->buildCaseTree( aRoot, myEditedObject );
  myBrowser->updateTree( aRoot );
}

void HYDROGUI_CalculationDlg::onDataChanged()
{
  SUIT_DataObject* aRoot = myBrowser->root();
  module()->getDataModel()->buildCaseTree( aRoot, myEditedObject );
  myBrowser->updateTree( aRoot );
}

void HYDROGUI_CalculationDlg::setAvailableGroups( const QStringList& theGroups )
{
  myAvailableGroups->clear();
  myGroups->clear();
  foreach( QString aGroup, theGroups )
    myAvailableGroups->addItem( aGroup );
}

QStringList HYDROGUI_CalculationDlg::getSelectedGroups() const
{
  return getSelected( myGroups );
}

QStringList HYDROGUI_CalculationDlg::getSelectedAvailableGroups() const
{
  return getSelected( myAvailableGroups );
}

void HYDROGUI_CalculationDlg::includeGroups( const QStringList& theObjects )
{
  moveItems( myAvailableGroups, myGroups, theObjects );
}

void HYDROGUI_CalculationDlg::excludeGroups( const QStringList& theObjects )
{
  moveItems( myGroups, myAvailableGroups, theObjects );
}

///

void HYDROGUI_CalculationDlg::setAvailableBoundaryPolygons( const QStringList& theBoundaryPolygons, 
  const QVector<int>& theTypes )
{
  myAvailableBoundaryPolygons->clear();
  myBoundaryPolygons->clear();
  myISBoundaryPolygons->clear();
  QString iconStr;
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  int i = 0;
  foreach( QString aBP, theBoundaryPolygons )
  {
    int aBT = theTypes[i];    
    i++;
    if (aBT == 1)
      iconStr = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE1_ICO").toLatin1());
    else if (aBT == 2)
      iconStr = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE2_ICO").toLatin1());
    else if (aBT == 3)
      iconStr = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE3_ICO").toLatin1());
    else
      continue;
    QPixmap aPM = aResMgr->loadPixmap( "HYDRO", iconStr );
    QListWidgetItem* item = new QListWidgetItem(QIcon(aPM), aBP);
    myAvailableBoundaryPolygons->addItem(item);
  }
}

QStringList HYDROGUI_CalculationDlg::getSelectedBoundaryPolygons() const
{
  return getSelected( myBoundaryPolygons );
}

QStringList HYDROGUI_CalculationDlg::getSelectedISBoundaryPolygons() const
{
  return getSelected( myISBoundaryPolygons );
}

QStringList HYDROGUI_CalculationDlg::getSelectedAvailableBoundaryPolygons() const
{
  return getSelected( myAvailableBoundaryPolygons );
}

void HYDROGUI_CalculationDlg::includeBoundaryPolygons( const QStringList& theObjects )
{
  moveItems( myAvailableBoundaryPolygons, myBoundaryPolygons, theObjects );
}

void HYDROGUI_CalculationDlg::includeISBoundaryPolygons( const QStringList& theObjects )
{
  moveItems( myAvailableBoundaryPolygons, myISBoundaryPolygons, theObjects );
}

void HYDROGUI_CalculationDlg::excludeBoundaryPolygons( const QStringList& theObjects )
{
  moveItems( myBoundaryPolygons, myAvailableBoundaryPolygons, theObjects );
}

void HYDROGUI_CalculationDlg::excludeISBoundaryPolygons( const QStringList& theObjects )
{
  moveItems( myISBoundaryPolygons, myAvailableBoundaryPolygons, theObjects );
}

/**
  Get creation mode.
  @param theMode the mode
*/
int HYDROGUI_CalculationDlg::getMode() const
{
  return myModeButtons->checkedId();
}

/**
  Set creation mode.
  @param theMode the mode
*/
void HYDROGUI_CalculationDlg::setMode( int theMode )
{
  bool isBlocked = myModeButtons->blockSignals( true );
  myModeButtons->button( theMode )->setChecked( true );
  myModeButtons->blockSignals( isBlocked );

  bool isAuto = ( theMode == HYDROData_CalculationCase::AUTOMATIC );

  myGeomObjects->setOrderingEnabled( isAuto );
  QWidget* aWidget = mySplitter->widget( 1 );
  if ( aWidget ) {
    aWidget->setVisible( isAuto );
  }
}

void HYDROGUI_CalculationDlg::setGeomOrderingEnabled( int enabled )
{
  myGeomObjects->setOrderingEnabled( enabled );
}

/**
  Enable/disable zones drag'n'drop and renaming.
  @param theIsEnabled if true - zones drag'n'drop and renaming will be enabled
*/
void HYDROGUI_CalculationDlg::setEditZonesEnabled( const bool theIsEnabled )
{
  myBrowser->setReadOnly( !theIsEnabled );
}

/**
  Get included geometry objects.
  @return the list of geometry objects
 */
QList<Handle(HYDROData_Entity)> HYDROGUI_CalculationDlg::getGeometryObjects(bool GeomObjOnly)
{
  QList<Handle(HYDROData_Entity)> anEntities = myGeomObjects->getObjects();
  QList<Handle(HYDROData_Entity)> anObjects;

  foreach ( Handle(HYDROData_Entity) anEntity, anEntities ) 
  {
    if (GeomObjOnly)
    {
      Handle(HYDROData_Object) anObj = Handle(HYDROData_Object)::DownCast( anEntity );
      if ( anObj.IsNull() ) {
        continue;
      }      
      anObjects << anObj;
    }
    else
      anObjects << anEntity;
  }

  return anObjects;
}

/**
  Get rules.
  @return the list of rules
 */
HYDROData_ListOfRules HYDROGUI_CalculationDlg::getRules() const
{
  return myPriorityWidget->getRules();
}

/**
  Set rules.
  @param theRules the list of rules
 */
void  HYDROGUI_CalculationDlg::setRules( const HYDROData_ListOfRules& theRules ) const
{
  myPriorityWidget->setRules( theRules );
}

/**
  Slot called when objects order is changed.
 */
void HYDROGUI_CalculationDlg::onOrderChanged()
{
  bool isConfirmed = true;
  emit orderChanged( isConfirmed );
  if( isConfirmed )
    myPriorityWidget->setObjects( getGeometryObjects() );
  else
    myGeomObjects->undoLastMove();
}

/**
  Slot called when priority rule for geometry objects is changed.
 */
void HYDROGUI_CalculationDlg::onRuleChanged()
{
  bool isConfirmed = true;
  emit ruleChanged( isConfirmed );
  if( !isConfirmed )
    myPriorityWidget->undoLastChange();
}

void HYDROGUI_CalculationDlg::setStricklerTable( const QString& theStricklerTableName, bool theBlockSignals )
{
  bool isBlocked;
  if ( theBlockSignals )
    isBlocked = myStricklerTableName->blockSignals( true );
  
  myStricklerTableName->setCurrentIndex( myStricklerTableName->findText( theStricklerTableName ) );

  if ( theBlockSignals )
    myStricklerTableName->blockSignals( isBlocked );
  else
    emit StricklerTableSelected( theStricklerTableName );
}

void HYDROGUI_CalculationDlg::setLandCoverMap( const QString& theLandCoverMapName, bool theBlockSignals )
{
  bool isBlocked;
  if ( theBlockSignals )
    isBlocked = myLandCoverMapName->blockSignals( true );
  
  myLandCoverMapName->setCurrentIndex( myLandCoverMapName->findText( theLandCoverMapName ) );

  if ( theBlockSignals )
    myLandCoverMapName->blockSignals( isBlocked );
  else
    emit landCoverMapSelected( theLandCoverMapName );
}

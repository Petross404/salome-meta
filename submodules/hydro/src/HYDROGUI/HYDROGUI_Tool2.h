// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_TOOL2_H
#define HYDROGUI_TOOL2_H

#include <HYDROData_Entity.h>
#include <GraphicsView_Defs.h>
#include <QStringList>

// IDL includes
#include <SALOMEconfig.h>
#include CORBA_SERVER_HEADER(GEOM_Gen)
#endif

class HYDROGUI_Module;
class HYDROGUI_Prs;
class SUIT_ViewManager;
class GraphicsView_ViewPort;
class OCCViewer_ViewFrame;
class HYDROData_Document;
class HYDROData_CalculationCase;

namespace HYDROGUI_Tool
{
  int GetActiveStudyId();

  void SetActiveViewManager( HYDROGUI_Module* theModule,
                             SUIT_ViewManager* theViewManager );

  void GetPrsSubObjects( HYDROGUI_Module* theModule,
                         HYDROData_SequenceOfObjects& theSeq );

  HYDROGUI_Prs* GetPresentation( const Handle(HYDROData_Entity)& theObj,
                                 const GraphicsView_ObjectList& theObjects );

  GraphicsView_ObjectList GetPrsList( GraphicsView_ViewPort* theViewPort );

  HYDROData_SequenceOfObjects GetSelectedObjects( HYDROGUI_Module* theModule );

  bool IsObjectHasPresentation( const Handle(HYDROData_Entity)& theObject,
                                const QString&                 theViewerType = "" );

  Handle(HYDROData_Entity) GetSelectedObject( HYDROGUI_Module* theModule );

  HYDROData_SequenceOfObjects GetGeometryObjects( HYDROGUI_Module* theModule );

  ObjectKind GetSelectedPartition( HYDROGUI_Module* theModule );

  bool IsSelectedPartOfCalcCase( HYDROGUI_Module* theModule, Handle(HYDROData_CalculationCase)& theOutCalCase,
                                 QString& theOutPart);

  Handle(HYDROData_Entity) FindObjectByName( HYDROGUI_Module* theModule,
                                             const QString& theName,
                                             const ObjectKind theObjectKind = KIND_UNKNOWN );

  HYDROData_SequenceOfObjects FindObjectsByNames( HYDROGUI_Module*   theModule,
                                                  const QStringList& theNames,
                                                  const ObjectKind   theObjectKind = KIND_UNKNOWN );

  QString GenerateObjectName( HYDROGUI_Module*   theModule,
                              const QString&     thePrefix,
                              const QStringList& theUsedNames = QStringList(),
                              const bool         theIsTryToUsePurePrefix = false );

  size_t GetActiveViewId( HYDROGUI_Module* theModule,
                          const QString&   theViewId = QString() );

  size_t GetActiveGraphicsViewId( HYDROGUI_Module* theModule );

  size_t GetActiveOCCViewId( HYDROGUI_Module* theModule );

  QList<size_t> GetGraphicsViewIdList( HYDROGUI_Module* theModule );

  QList<size_t> GetOCCViewIdList( HYDROGUI_Module* theModule );

  void setOCCActionShown( OCCViewer_ViewFrame* theViewFrame,
                          const int theActionId,
                          const bool isShown );

  void setOCCActionShown( HYDROGUI_Module* theModule,
                          const int theActionId,
                          const bool isShown );

  QColor GenerateFillingColor( HYDROGUI_Module* theModule, const QStringList& theZoneNames );

  QColor GenerateFillingColor( const Handle(HYDROData_Document)& theDoc,
                               const QStringList& theZoneNames );

  QStringList GetSelectedGeomObjects( HYDROGUI_Module* theModule,
                                      QList<GEOM::shape_type> theTypes );

  void DeleteGeomObjects( HYDROGUI_Module* theModule, const QStringList& theEntries );
};

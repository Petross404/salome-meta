// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Displayer.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Prs.h"
#include "HYDROGUI_PrsImageDriver.h"
#include "HYDROGUI_PrsPolylineDriver.h"
#include "HYDROGUI_PrsZoneDriver.h"
#include "HYDROGUI_Tool2.h"

#include <CurveCreator_Utils.hxx>

#include <LightApp_Application.h>
#include <SVTK_ViewWindow.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>
#include <SUIT_ViewManager.h>

#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkWorldPointPicker.h>
#include <vtkCamera.h>

#include <GraphicsView_Viewer.h>
#include <GraphicsView_ViewPort.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

const double LOCAL_SELECTION_TOLERANCE = 0.0001;

HYDROGUI_Displayer::HYDROGUI_Displayer( HYDROGUI_Module* theModule )
: HYDROGUI_AbstractDisplayer( theModule ),
  myXPosition( -1 ), myYPosition( -1 ), myIsPositionSaved( false )
{
}

HYDROGUI_Displayer::~HYDROGUI_Displayer()
{
}

void HYDROGUI_Displayer::SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                      const size_t theViewerId )
{
  GraphicsView_Viewer* aViewer = module()->getViewer( theViewerId );
  if( !aViewer )
    return;

  GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort();
  if( !aViewPort )
    return;

  GraphicsView_ObjectList anObjectList = aViewPort->getObjects();
  for( int i = 1, n = theObjs.Length(); i <= n; i++ )
  {
    Handle(HYDROData_Entity) anObj = theObjs.Value( i );
    if( anObj.IsNull() )
      continue;

    if( HYDROGUI_Prs* aPrs = HYDROGUI_Tool::GetPresentation( anObj, anObjectList ) )
      aPrs->setIsToUpdate( true );
  }
}

void HYDROGUI_Displayer::EraseAll( const size_t theViewerId )
{
  GraphicsView_Viewer* aViewer = module()->getViewer( theViewerId );
  if( !aViewer )
    return;

  GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort();
  if( !aViewPort )
    return;

  GraphicsView_ObjectListIterator anIter( HYDROGUI_Tool::GetPrsList( aViewPort ) );
  while( anIter.hasNext() )
  {
    if( GraphicsView_Object* anObject = anIter.next() )
    {
      aViewPort->removeItem( anObject );
      delete anObject;
    }
  }
}

void HYDROGUI_Displayer::Erase( const HYDROData_SequenceOfObjects& theObjs,
                                const size_t theViewerId )
{
  GraphicsView_Viewer* aViewer = module()->getViewer( theViewerId );
  if( !aViewer )
    return;

  GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort();
  if( !aViewPort )
    return;

  HYDROGUI_DataModel* aModel = (HYDROGUI_DataModel*)module()->dataModel();
  if( aModel ) 
  {
    GraphicsView_ObjectList anObjectList = HYDROGUI_Tool::GetPrsList( aViewPort );
    for( int i = 1, n = theObjs.Length(); i <= n; i++ )
    {
      // the object may be null or dead
      const Handle(HYDROData_Entity)& anObj = theObjs.Value( i );
      if( HYDROGUI_Prs* aPrs = HYDROGUI_Tool::GetPresentation( anObj, anObjectList ) )
      {
        aViewPort->removeItem( aPrs );
        anObjectList.removeAll( aPrs );
        delete aPrs;
      }
    }
  }
}

void HYDROGUI_Displayer::Display( const HYDROData_SequenceOfObjects& theObjs,
                                  const size_t theViewerId,
                                  const bool theIsForced,
                                  const bool theDoFitAll)
{
  GraphicsView_Viewer* aViewer = module()->getViewer( theViewerId );
  if( !aViewer )
    return;

  GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort();
  if( !aViewPort )
    return;

  bool anIsDisplayed = false;
  GraphicsView_ObjectList anObjectList = aViewPort->getObjects();
  for( int i = 1, n = theObjs.Length(); i <= n; i++ )
  {
    Handle(HYDROData_Entity) anObj = theObjs.Value( i );
    if( anObj.IsNull() )
      continue;

    HYDROGUI_Prs* aPrs = HYDROGUI_Tool::GetPresentation( anObj, anObjectList );

    bool anIsInserted = ( aPrs != 0 );
    if( !aPrs || aPrs->getIsToUpdate() || theIsForced )
    {
      if( HYDROGUI_PrsDriver* aDriver = getDriver( anObj ) )
      {
        if( aDriver->Update( anObj, aPrs ) && aPrs && !anIsInserted )
        {
          DEBTRACE("Display, addItem");
          aViewPort->addItem( aPrs );
        }
      }
    }

    if( aPrs )
    {
      bool anIsVisible = module()->isObjectVisible( (size_t)aViewer, anObj );
      aPrs->setVisible( anIsVisible );
    }
  }

  aViewPort->onBoundingRectChanged(); // specific of HYDRO module
  if ( theDoFitAll )
  {
    aViewPort->fitAll();
  }
}

void HYDROGUI_Displayer::purgeObjects( const size_t theViewerId )
{
  DEBTRACE("purgeObjects");
  GraphicsView_Viewer* aViewer = module()->getViewer( theViewerId );
  if( !aViewer )
    return;

  GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort();
  if( !aViewPort )
    return;

  GraphicsView_ObjectListIterator anIter( HYDROGUI_Tool::GetPrsList( aViewPort ) );
  while( anIter.hasNext() )
  {
    if( HYDROGUI_Prs* aPrs = dynamic_cast<HYDROGUI_Prs*>( anIter.next() ) )
    {
      Handle(HYDROData_Entity) anObject = aPrs->getObject();
      if( !anObject.IsNull() && anObject->IsRemoved() )
      {
        DEBTRACE("removeItem");
        aViewPort->removeItem( aPrs );
        delete aPrs;
      }
    }
  }
}

HYDROGUI_PrsDriver* HYDROGUI_Displayer::getDriver( const Handle(HYDROData_Entity)& theObj )
{
  HYDROGUI_PrsDriver* aDriver = NULL;
  ObjectKind aKind = theObj->GetKind();
  PrsDriversMap::iterator anIter = myPrsDriversMap.find( aKind );
  if( anIter != myPrsDriversMap.end() )
    aDriver = anIter.value();
  else 
  {
    switch( aKind )
    {
      case KIND_IMAGE:
        aDriver = new HYDROGUI_PrsImageDriver();
        break;
      case KIND_POLYLINEXY:
        aDriver = new HYDROGUI_PrsPolylineDriver();
        break;
      case KIND_ZONE:
        aDriver = new HYDROGUI_PrsZoneDriver();
        break;
      default:
        break;
    }

    if ( aDriver )
      myPrsDriversMap[ aKind ] = aDriver;
  }

  return aDriver;
}

QString HYDROGUI_Displayer::GetType() const
{
  return GraphicsView_Viewer::Type();
}

void HYDROGUI_Displayer::SaveCursorViewPosition( SUIT_ViewWindow* theViewWindow )
{
  myIsPositionSaved = false;
  myXPosition = 0;
  myYPosition = 0;

  SUIT_ViewWindow* aViewWindow = theViewWindow;
  if ( !theViewWindow ) {
    SUIT_ViewManager* aViewMgr = module()->getApp()->activeViewManager();
    aViewWindow = aViewMgr ? aViewMgr->getActiveView() : 0;
    if ( !aViewWindow )
      return;
  }
  
  OCCViewer_ViewWindow* anOCCViewWindow = 
    dynamic_cast<OCCViewer_ViewWindow*>( aViewWindow );
  if ( anOCCViewWindow ) {
    // Get the selected point coordinates
    OCCViewer_ViewPort3d* aViewPort = anOCCViewWindow->getViewPort();
    if ( aViewPort ) {
      QPoint aViewPos = aViewPort->mapFromGlobal( QCursor::pos() );
      myXPosition = aViewPos.x();
      myYPosition = aViewPos.y();
      myIsPositionSaved = true;
    }
  }
  else {
    SVTK_ViewWindow* aVTKViewWindow = 
      dynamic_cast<SVTK_ViewWindow*>(aViewWindow);
    if ( aVTKViewWindow ) {
      vtkRenderer* aRen = aVTKViewWindow->getRenderer();
      if ( aRen )
      {
        vtkCamera* aCamera = aRen->GetActiveCamera();
        double* aNormal = aCamera->GetViewPlaneNormal();
        vtkRenderWindowInteractor* anInteractor = aVTKViewWindow->getInteractor();
        if ( anInteractor )
        {
          anInteractor->GetLastEventPosition( myXPosition, myYPosition );
          myIsPositionSaved = true;
        }
      }
    }
  }
  if (!myIsPositionSaved)
    int aValue = 0;
}

bool HYDROGUI_Displayer::GetCursorViewCoordinates( SUIT_ViewWindow* theViewWindow,
                                                   double& theXCoordinate,
                                                   double& theYCoordinate,
                                                   double& theZCoordinate )
{
  theXCoordinate = 0;
  theYCoordinate = 0;
  theZCoordinate = 0;
  bool doShow = false;
  if ( !theViewWindow || !myIsPositionSaved )
    return doShow;
  
  OCCViewer_ViewWindow* anOCCViewWindow = 
    dynamic_cast<OCCViewer_ViewWindow*>(theViewWindow);
  if ( anOCCViewWindow ) {
    // Get the selected point coordinates
    OCCViewer_ViewPort3d* aViewPort = anOCCViewWindow->getViewPort();
    if ( !aViewPort ) {
      return doShow;
    }
    gp_Pnt aPnt = CurveCreator_Utils::ConvertClickToPoint( myXPosition, myYPosition,
                                                           aViewPort->getView() );
    theXCoordinate = aPnt.X();
    theYCoordinate = aPnt.Y();
    doShow = true;
  } 
  else
  {
    SVTK_ViewWindow* aVTKViewWindow = 
      dynamic_cast<SVTK_ViewWindow*>(theViewWindow);
    if ( aVTKViewWindow ) {
      vtkRenderer* aRen = aVTKViewWindow->getRenderer();
      if ( aRen )
      {
        vtkCamera* aCamera = aRen->GetActiveCamera();
        double* aNormal = aCamera->GetViewPlaneNormal();
        myPicker->Pick( myXPosition, myYPosition, 0, aRen );
        double* aCoords = myPicker->GetPickPosition();
        /////////////////////// Use the same algorithm as for OCC
        double X, Y, Z;
        double aXp, aYp, aZp;
        double Vx, Vy, Vz;
        X = aCoords[0];
        Y = aCoords[1];
        Z = aCoords[2];
        Vx = aNormal[0];
        Vy = aNormal[1];
        Vz = aNormal[2];
        Standard_Real aPrec = LOCAL_SELECTION_TOLERANCE;
        if ( fabs( Vz ) > aPrec ) {
          double aT = -Z/Vz;
          aXp = X + aT*Vx;
          aYp = Y + aT*Vy;
          aZp = Z + aT*Vz;
        }
        else { // Vz = 0 - the eyed plane is orthogonal to Z plane - XOZ, or YOZ
          aXp = aYp = aZp = 0;
          if ( fabs( Vy ) < aPrec ) // Vy = 0 - the YOZ plane
            aYp = Y;
          else if ( fabs( Vx ) < aPrec ) // Vx = 0 - the XOZ plane
            aXp = X;
        }
        /////////////////////////
        theXCoordinate = aXp;
        theYCoordinate = aYp;
        doShow = true;
      }
    } 
  }
  return doShow;
}

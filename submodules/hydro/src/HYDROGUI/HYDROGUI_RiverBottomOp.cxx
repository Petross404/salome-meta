// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_RiverBottomOp.h"

#include "HYDROGUI_RiverBottomDlg.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_River.h>
#include <HYDROData_Iterator.h>

/*
#include <HYDROData_Profile.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <TopoDS.hxx>
*/

HYDROGUI_RiverBottomOp::HYDROGUI_RiverBottomOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "FIND_STREAM_BOTTOM" ) );
}

HYDROGUI_RiverBottomOp::~HYDROGUI_RiverBottomOp()
{
}

void HYDROGUI_RiverBottomOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  if ( isApplyAndClose() )
  {
    HYDROGUI_RiverBottomDlg* aPanel = ::qobject_cast<HYDROGUI_RiverBottomDlg*>( inputPanel() );

    aPanel->reset();
  }
}

void HYDROGUI_RiverBottomOp::abortOperation()
{
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_RiverBottomOp::commitOperation()
{
  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_RiverBottomOp::createInputPanel() const
{
  HYDROGUI_RiverBottomDlg* aPanel = new HYDROGUI_RiverBottomDlg( module(), getName() );
  return aPanel;
}

bool HYDROGUI_RiverBottomOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_RiverBottomDlg* aPanel = ::qobject_cast<HYDROGUI_RiverBottomDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  Handle(HYDROData_Stream) aRiver = riverObject( aPanel->getRiverName() );
  if ( aRiver.IsNull() )
  {
    theErrorMsg = tr( "INCORRECT_STREAM_OBJECT" );
    return false;
  }

  startDocOperation();

  aRiver->GenerateBottomPolyline();
  aRiver->Update();

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  commitDocOperation();

  return true;
}
/*
Handle(HYDROData_RiverBottom) HYDROGUI_RiverBottomOp::createNewObject()
{
  return Handle(HYDROData_RiverBottom)::DownCast( doc()->CreateObject( KIND_RIVERBOTTOM ) );
}
*/

Handle(HYDROData_Stream) HYDROGUI_RiverBottomOp::riverObject( const QString& theName ) const
{
    return Handle(HYDROData_Stream)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), theName ) );
}
QStringList HYDROGUI_RiverBottomOp::riverNames( bool all ) const
{
    QStringList aNames;
/*
    for ( HYDROData_Iterator it( HYDROData_Document::Document( module()->getStudyId() ), KIND_RIVER ); it.More(); it.Next() )
    {
        it.Current();
        if ( !aRiver.IsNull() && ( all || !aRiver->HasBottom() )
            aNames.append( aRiver->GetName() );
    }
*/
    return aNames;
}

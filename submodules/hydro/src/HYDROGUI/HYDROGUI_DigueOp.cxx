// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DigueOp.h"

#include "HYDROGUI_DigueDlg.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Shape.h"

#include <HYDROData_Document.h>
#include <HYDROData_Digue.h>

HYDROGUI_DigueOp::HYDROGUI_DigueOp( HYDROGUI_Module* theModule,
                                    const bool theIsEdit )
: HYDROGUI_ChannelOp( theModule, theIsEdit )
{
  setName( theIsEdit ? tr( "EDIT_DIGUE" ) : tr( "CREATE_DIGUE" ) );
}

HYDROGUI_DigueOp::~HYDROGUI_DigueOp()
{
}

void HYDROGUI_DigueOp::startOperation()
{
  HYDROGUI_ChannelOp::startOperation();

  HYDROGUI_DigueDlg* aPanel = ::qobject_cast<HYDROGUI_DigueDlg*>( inputPanel() );

  if ( !myIsEdit || myEditedObject.IsNull() )
  {
    QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_DIGUE_NAME" ) );
    aPanel->setObjectName( anObjectName );
  }
}

bool HYDROGUI_DigueOp::processApply( int& theUpdateFlags,
                                     QString& theErrorMsg,
                                     QStringList& theBrowseObjectsEntries )
{
  if ( !HYDROGUI_ChannelOp::processApply( theUpdateFlags, theErrorMsg, theBrowseObjectsEntries ) )
    return false;

  if ( !myIsEdit )
  {
    myEditedObject->SetFillingColor( myEditedObject->DefaultFillingColor() );
    myEditedObject->SetBorderColor( myEditedObject->DefaultBorderColor() );
  }

  return true;
}

HYDROGUI_InputPanel* HYDROGUI_DigueOp::createInputPanel() const
{
  HYDROGUI_DigueDlg* aPanel = new HYDROGUI_DigueDlg( module(), getName() );
  connect( aPanel, SIGNAL( CreatePreview() ), this, SLOT( onCreatePreview() ) );
  return aPanel;
}

Handle(HYDROData_Channel) HYDROGUI_DigueOp::createNewObject() const
{
  return Handle(HYDROData_Digue)::DownCast( doc()->CreateObject( KIND_DIGUE ) );
}


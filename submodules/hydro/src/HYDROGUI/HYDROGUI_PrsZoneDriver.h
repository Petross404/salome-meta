// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRSZONEDRIVER_H
#define HYDROGUI_PRSZONEDRIVER_H

#include <HYDROGUI_PrsDriver.h>

/**
 * \class HYDROGUI_PrsZoneDriver
 * \brief Presentation driver for zone objects.
 */
class HYDROGUI_PrsZoneDriver : public HYDROGUI_PrsDriver
{
public:
  /**
   * \brief Constructor.
   */
  HYDROGUI_PrsZoneDriver();

  /**
   * \brief Destructor.
   */
  virtual ~HYDROGUI_PrsZoneDriver();

public:
  /**
   * \brief Update or create the polyline presentation on a basis of data object.
   * \param theObj data object
   * \param thePrs presentation
   * \return status of the operation
   */
  virtual bool Update( const Handle(HYDROData_Entity)& theObj,
                       HYDROGUI_Prs*& thePrs );
};

#endif

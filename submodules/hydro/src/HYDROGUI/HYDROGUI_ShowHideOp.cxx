// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ShowHideOp.h"

#include <GraphicsView_Viewer.h>

#include "HYDROGUI_Module.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_Operations.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Iterator.h>
#include <HYDROData_Entity.h>
#include <HYDROData_Region.h>

#include <LightApp_Application.h>
#include <LightApp_Displayer.h>

#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewManager.h>

#include <SVTK_ViewModel.h>

#include <SUIT_ViewManager.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_ShowHideOp::HYDROGUI_ShowHideOp( HYDROGUI_Module* theModule, int theId )
: HYDROGUI_Operation( theModule ),
  myId( theId )
{
  DEBTRACE("HYDROGUI_ShowHideOp " << theId);
  QString aName;
  switch( myId )
  {
    case ShowId:     aName = tr( "SHOW" ); break;
    case ShowOnlyId: aName = tr( "SHOW_ONLY" ); break;
    case ShowAllId:  aName = tr( "SHOW_ALL" ); break;
    case HideId:     aName = tr( "HIDE" ); break;
    case HideAllId:  aName = tr( "HIDE_ALL" ); break;
    default: break;
  }
  setName( aName );
}

HYDROGUI_ShowHideOp::~HYDROGUI_ShowHideOp()
{
}

void HYDROGUI_ShowHideOp::startOperation()
{
  DEBTRACE("startOperation");
  HYDROGUI_Operation::startOperation();

  HYDROGUI_Module* aModule = module();

  size_t aViewId = HYDROGUI_Tool::GetActiveViewId( aModule );

  int anUpdateFlags = 0;

  SUIT_ViewManager* aVTKMgr           = NULL;
  OCCViewer_ViewManager* anOCCManager = NULL;

  SUIT_ViewManager* aViewMgr = aModule->getApp()->activeViewManager();
  if ( aViewMgr )
  {
    if ( aViewMgr->getType() == GraphicsView_Viewer::Type() )
    {
      anUpdateFlags |= UF_Viewer;
    }
    else if ( aViewMgr->getType() == OCCViewer_Viewer::Type() )
    {
      anUpdateFlags |= UF_OCCViewer;
      anOCCManager = ::qobject_cast<OCCViewer_ViewManager*>( aViewMgr );
    }
    else if ( aViewMgr->getType() == SVTK_Viewer::Type() )
    {
      anUpdateFlags |= UF_VTKViewer;
    }
  }

  // for all objects
  if( myId == ShowOnlyId || myId == ShowAllId || myId == HideAllId )
  {
    bool aVisibility = myId == ShowAllId;
    HYDROData_Iterator anIterator( doc() );
    for( ; anIterator.More(); anIterator.Next() )
    {
      Handle(HYDROData_Entity) anObject = anIterator.Current();
      if( !anObject.IsNull() )
        aModule->setObjectVisible( aViewId, anObject, aVisibility );
    }

    // For occ viewer we do the additional step to hide objects from other modules
    if ( anOCCManager != NULL && !aVisibility )
      LightApp_Displayer().EraseAll( true );
  }

  bool isFoundImage = false;
  // for selected objects
  if( myId == ShowId || myId == ShowOnlyId || myId == HideId )
  {
    HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( aModule );
  
    bool aVisibility = myId == ShowId || myId == ShowOnlyId || myId == ShowAllId;
    Handle( HYDROData_Entity ) anObject;
    for( Standard_Integer anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
    {
      anObject = aSeq.Value( anIndex );
      if( !anObject.IsNull() )
      {
        if ( anObject->GetKind() == KIND_IMAGE )
          isFoundImage = true;

        aModule->setObjectVisible( aViewId, anObject, aVisibility );
        if ( anObject->GetKind() == KIND_REGION )
        {
          Handle( HYDROData_Region ) aRegion = Handle( HYDROData_Region )::DownCast( anObject );
          if ( !aRegion.IsNull() )
          {
            HYDROData_SequenceOfObjects aZonesSeq = aRegion->GetZones();
            for( Standard_Integer aZoneIdx = 1, aNbZones = aZonesSeq.Length(); aZoneIdx <= aNbZones; aZoneIdx++ )
            {
              anObject = aZonesSeq.Value( aZoneIdx );
              if( !anObject.IsNull() )
              {
                aModule->setObjectVisible( aViewId, anObject, aVisibility );
              }
            }
          }
        }
        else if ( anObject->GetKind() == KIND_BATHYMETRY && aVisibility )
        {
          if ( anUpdateFlags & UF_OCCViewer )
          {
            aModule->setObjectVisible( aViewId, anObject, aVisibility );
          }
          else if ( !(anUpdateFlags & UF_VTKViewer) )
          {
            // Activate VTK viewer if show a bathymetry
            aVTKMgr = aModule->getApp()->viewManager( SVTK_Viewer::Type() );
            if ( !aVTKMgr )
            {
              aVTKMgr = aModule->getApp()->createViewManager( SVTK_Viewer::Type() );
            }
            if ( aVTKMgr )
            {
              aModule->setObjectVisible( (size_t)aVTKMgr->getViewModel(), anObject, aVisibility );
            }
          }
        }
      }
    }
  }

  if ( myId == ShowOnlyId || myId == ShowId || myId == ShowAllId )
  {
    if( isFoundImage || myId == ShowAllId )
      anUpdateFlags |= UF_FitAll;
  }

  // Set VTK viewer active if show a bathymetry
  if ( aVTKMgr )
  {
    anUpdateFlags |= UF_VTKViewer;
    aVTKMgr->setShown( true );
  }

  aModule->update( anUpdateFlags );
  commit();
}

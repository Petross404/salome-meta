// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ABSTRACT_DISPLAYER_H
#define HYDROGUI_ABSTRACT_DISPLAYER_H

#include <HYDROData_Entity.h>

class HYDROGUI_Module;
class SUIT_ViewManager;

/**
 * \class HYDROGUI_DataModel
 * \brief Class intended to create, display and update the presentations.
 */
class HYDROGUI_AbstractDisplayer
{
public:
  /**
   * \brief Constructor.
   * \param theModule module object
   */
  HYDROGUI_AbstractDisplayer( HYDROGUI_Module* theModule );

  /**
   * \brief Destructor.
   */
  virtual ~HYDROGUI_AbstractDisplayer();

public:
  /**
   * \brief Check if this displayer is applicable to the given view manager.
   * The view manager method getType is used.
   * \param theViewerId viewer identifier
   */
  virtual bool     IsApplicable( const size_t theViewerId ) const;

  /**
   * \brief Check if this displayer is applicable to the given view manager.
   * The view manager method getType is used.
   * \param theViewMgr the view manager to check
   */
  virtual bool     IsApplicable( const SUIT_ViewManager* theViewMgr ) const;

  /**
   * \brief Update all objects in the viewer.
   * \param theViewerId viewer identifier
   * \param theIsInit flag used for initial update
   * \param theIsForced flag used to update all objects, including the unchanged ones
   */
  virtual void     UpdateAll( const size_t theViewerId,
                              const bool theIsInit,
                              const bool theIsForced,
                              const bool theDoFitAll );

  /**
   * \brief Force the specified objects to be updated.
   * \param theObjs sequence of objects to update
   * \param theViewerId viewer identifier
   */
  virtual void     SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                const size_t theViewerId ) = 0;

  /**
   * \brief Get the applicable viewer type.
   */
  virtual QString  GetType() const = 0;

  HYDROGUI_Module* module() const {return myModule;}

protected:
  /**
   * \brief Update and display all objects in the viewer.
   * \param theViewerId viewer identifier
   * \param theIsForced flag used to update all objects, including the unchanged ones
   */
  virtual void     DisplayAll( const size_t theViewerId,
                               const bool theIsForced,
                               const bool theDoFitAll );

  /**
   * \brief Update the specified viewer objects.
   * \param theObjs sequence of objects to update
   * \param theViewerId viewer identifier
   * \param theIsForced flag used to update all objects, including the unchanged ones
   * \param theDoFitAll flag used to fit the view to all visible objects; do not fit by default
   */
  virtual void     Update( const HYDROData_SequenceOfObjects& theObjs,
                           const size_t theViewerId,
                           const bool theIsForced,
                           const bool theDoFitAll );

  /**
   * \brief Erase all viewer objects.
   * \param theViewerId viewer identifier
   */
  virtual void     EraseAll( const size_t theViewerId ) = 0;

  /**
   * \brief Erase the specified viewer objects.
   * \param theObjs sequence of objects to erase
   * \param theViewerId viewer identifier
   */
  virtual void     Erase( const HYDROData_SequenceOfObjects& theObjs,
                          const size_t theViewerId ) = 0;

  /**
   * \brief Display the specified viewer objects.
   * \param theObjs sequence of objects to display
   * \param theViewerId viewer identifier
   * \param theIsForced flag used to update all objects, including the unchanged ones
   * \param theDoFitAll flag used to fit the view to all visible objects; do not fit by default
   */
  virtual void     Display( const HYDROData_SequenceOfObjects& theObjs,
                            const size_t theViewerId,
                            const bool theIsForced,
                            const bool theDoFitAll ) = 0;

protected:
  /**
   * \brief Purge all invalid objects in the viewer.
   * \param theViewerId viewer identifier
   */
  virtual void     purgeObjects( const size_t theViewerId ) = 0;

  //HYDROGUI_Module* module() const {return myModule;}

private:
  HYDROGUI_Module* myModule;
};

#endif

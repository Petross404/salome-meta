// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_LANDCOVERMAP_PRS_H
#define HYDROGUI_LANDCOVERMAP_PRS_H

#include <HYDROData_LandCoverMap.h>
#include <HYDROData_StricklerTable.h>
#include <AIS_ColoredShape.hxx>
#include <AIS_ColorScale.hxx>


class HYDROGUI_LandCoverMapPrs : public AIS_ColoredShape
{
public:
  DEFINE_STANDARD_RTTIEXT( HYDROGUI_LandCoverMapPrs, AIS_ColoredShape );

  HYDROGUI_LandCoverMapPrs( const Handle(HYDROData_LandCoverMap)& );
  virtual ~HYDROGUI_LandCoverMapPrs();

  Handle(HYDROData_LandCoverMap) GetLandCoverMap() const;
  void SetLandCoverMap( const Handle(HYDROData_LandCoverMap)& );

  Handle(AIS_ColorScale) GetColorScale() const;
  void SetColorScale( const Handle(AIS_ColorScale)& );

  Handle(HYDROData_StricklerTable) GetTable() const;
  void SetTable( const Handle(HYDROData_StricklerTable)& );

  virtual void Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                        const Handle(Prs3d_Presentation)& thePresentation,
                        const Standard_Integer theMode );

  virtual void HilightSelected( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                                const SelectMgr_SequenceOfOwner& theOwners );

  Quantity_Color GetColor( const QString& theStricklerType ) const;

  void UpdateColors();

protected:
  void SetHilightAttributes( const Quantity_Color& theEdgesColor );

private:
  Handle(HYDROData_LandCoverMap)   myLCMap;
  Handle(AIS_ColorScale)           myColorScale;
  Handle(HYDROData_StricklerTable) myTable;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_TwoImagesDlg.h"

#include "HYDROGUI_ColorWidget.h"
#include "HYDROGUI_ObjSelector.h"

#include <QCheckBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QRadioButton>

HYDROGUI_TwoImagesDlg::HYDROGUI_TwoImagesDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ),
  myMode( TwoFuseImage ),
  myIsEdit( false )
{
  // Image name
  QGroupBox* anImageNameGroup = new QGroupBox( tr( "IMAGE_NAME" ) );

  QLabel* anImageNameLabel = new QLabel( tr( "NAME" ), anImageNameGroup );
  myImageName = new QLineEdit( anImageNameGroup );

  myModifySelectedImage = new QCheckBox( tr( "MODIFY_SELECTED_IMAGE" ) );

  QGridLayout* anImageNameLayout = new QGridLayout( anImageNameGroup );
  anImageNameLayout->setMargin( 5 );
  anImageNameLayout->setSpacing( 5 );
  anImageNameLayout->addWidget( anImageNameLabel,      0, 0 );
  anImageNameLayout->addWidget( myImageName,           0, 1 );
  anImageNameLayout->addWidget( myModifySelectedImage, 1, 0, 1, 2 );

  // Parameters
  QGroupBox* aParamGroup = new QGroupBox( tr( "PARAMETERS" ) );

  myImage1Label = new QLabel( tr( "IMAGE_1" ), aParamGroup );
  myImage1 = new HYDROGUI_ObjSelector( theModule, KIND_IMAGE, aParamGroup );

  myImage2Label = new QLabel( tr( "IMAGE_2" ), aParamGroup );
  myImage2 = new HYDROGUI_ObjSelector( theModule, KIND_IMAGE, aParamGroup );

  myPolylineLabel = new QLabel( tr( "POLYLINE" ), aParamGroup );
  myPolyline = new HYDROGUI_ObjSelector( theModule, KIND_POLYLINEXY, aParamGroup,
    HYDROGUI_ObjSelector::ClosedPolyline);

  myBackgroundFrame = new QFrame( aParamGroup );
  QLabel* aBackgroundLabel = new QLabel( tr( "BACKGROUND" ), myBackgroundFrame );
  myTransparent = new QRadioButton( tr( "TRANSPARENT" ), myBackgroundFrame );
  myTransparent->setChecked( true );
  myColor = new QRadioButton( tr( "COLOR" ), myBackgroundFrame );
  myColorBox = new HYDROGUI_ColorWidget( myBackgroundFrame );

  QGridLayout* aBackgroundLayout = new QGridLayout( myBackgroundFrame );
  aBackgroundLayout->setMargin( 0 );
  aBackgroundLayout->setSpacing( 5 );
  aBackgroundLayout->addWidget( aBackgroundLabel, 0, 0, 2, 1 );
  aBackgroundLayout->addWidget( myTransparent,    0, 1 );
  aBackgroundLayout->addWidget( myColor,          1, 1 );
  aBackgroundLayout->addWidget( myColorBox,       1, 2 );

  QGridLayout* aParamLayout = new QGridLayout( aParamGroup );
  aParamLayout->setMargin( 5 );
  aParamLayout->setSpacing( 5 );
  aParamLayout->addWidget( myImage1Label,    0, 0 );
  aParamLayout->addWidget( myImage1,         0, 1 );
  aParamLayout->addWidget( myImage2Label,    1, 0 );
  aParamLayout->addWidget( myImage2,         1, 1 );
  aParamLayout->addWidget( myPolylineLabel,  1, 0 );
  aParamLayout->addWidget( myPolyline,       1, 1 );
  aParamLayout->addWidget( myBackgroundFrame, 2, 0, 1, 2 );

  // Common
  addWidget( anImageNameGroup );
  addWidget( aParamGroup );
  addStretch();

  connect( myModifySelectedImage, SIGNAL( toggled( bool ) ),
           this, SLOT( onModifySelectedImage( bool ) ) );
  connect( myTransparent, SIGNAL( toggled( bool ) ), 
    myColorBox, SLOT( setDisabled( bool ) ) );
  connect( myImage1, SIGNAL( alreadySelected( const QString& ) ),
    this, SIGNAL( alreadySelected( const QString& ) ) );
  connect( myImage2, SIGNAL( alreadySelected( const QString& ) ),
    this, SIGNAL( alreadySelected( const QString& ) ) );

  setMode( myMode, myIsEdit );
}

HYDROGUI_TwoImagesDlg::~HYDROGUI_TwoImagesDlg()
{
}

void HYDROGUI_TwoImagesDlg::reset()
{
  myImageName->clear();
  myModifySelectedImage->setChecked( false );
  myImage1->Clear();
  myImage2->Clear();
  myPolyline->Clear();
  myTransparent->setChecked( true );
  myColorBox->resetColor();
  myColorBox->setDisabled( true );
}

void HYDROGUI_TwoImagesDlg::setMode( const int theMode, const bool theIsEdit )
{
  myMode = theMode;
  myIsEdit = theIsEdit;

  bool anIsTwoImages = isTwoImagesMode();

  myBackgroundFrame->setVisible( theMode != TwoCutImage );

  myModifySelectedImage->setVisible( !anIsTwoImages && !myIsEdit );

  myImage1Label->setText( anIsTwoImages ? tr( "IMAGE_1" ) : tr( "IMAGE" ) );

  myImage2Label->setVisible( anIsTwoImages );
  myImage2->setVisible( anIsTwoImages );
  myPolylineLabel->setVisible( !anIsTwoImages );
  myPolyline->setVisible( !anIsTwoImages );
}

bool HYDROGUI_TwoImagesDlg::isModifySelected() const
{
  return myModifySelectedImage->isChecked();
}

void HYDROGUI_TwoImagesDlg::setImageName( const QString& theName )
{
  myImageName->setText( theName );
}

QString HYDROGUI_TwoImagesDlg::getImageName() const
{
  return myImageName->text();
}

void HYDROGUI_TwoImagesDlg::setSelectedObjects( const QString& theName1,
                                                const QString& theName2 )
{
  myImage1->SetName( theName1 );
  if( isTwoImagesMode() )
    myImage2->SetName( theName2 );
  else
    myPolyline->SetName( theName2 );
}

bool HYDROGUI_TwoImagesDlg::getSelectedObjects( QString& theName1,
                                                QString& theName2 ) const
{
  theName1 = myImage1->GetName();
  if( isTwoImagesMode() )
    theName2 = myImage2->GetName();
  else
    theName2 = myPolyline->GetName();
  return !theName1.isEmpty() && !theName2.isEmpty();
}

void HYDROGUI_TwoImagesDlg::setPreselectedObject( const QString& theName )
{
  myImage1->SetName( theName );

  HYDROGUI_ObjSelector* aSelector = isTwoImagesMode() ? myImage2 : myPolyline;
  aSelector->SetChecked( true );
  aSelector->SetName( QString() );
}

void HYDROGUI_TwoImagesDlg::setColor( const QColor& theColor )
{
  if( theColor.alpha() == 0 ) { // transparent
    myTransparent->setChecked( true );
    myColorBox->resetColor();
  }
  else {
    myColor->setChecked( true );
    myColorBox->setColor( theColor );
  }
}

QColor HYDROGUI_TwoImagesDlg::getColor() const
{
  QColor aColor( 255, 255, 255, 0 ); // transparent
  if( myColor->isChecked() )
    aColor = myColorBox->color();
  return aColor;
}

void HYDROGUI_TwoImagesDlg::onModifySelectedImage( bool theState )
{
  myImageName->setEnabled( !theState );
}

bool HYDROGUI_TwoImagesDlg::isTwoImagesMode() const
{
  return myMode == TwoFuseImage || myMode == TwoCutImage;
}

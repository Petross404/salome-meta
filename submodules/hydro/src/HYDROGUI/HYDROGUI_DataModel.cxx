// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DataModel.h"

#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Zone.h"
#include "HYDROGUI_Region.h"

#include <HYDROData_Bathymetry.h>
#include <HYDROData_CalculationCase.h>
#include <HYDROGUI_DataModelSync.h>
#include <HYDROData_Document.h>
#include <HYDROData_DummyObject3D.h>
#include <HYDROData_Image.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Profile.h>
#include <HYDROData_VisualState.h>
#include <HYDROData_Region.h>
#include <HYDROData_Zone.h>
#include <HYDROData_Obstacle.h>
#include <HYDROData_Channel.h>
#include <HYDROData_Digue.h>
#include <HYDROData_River.h>
#include <HYDROData_Stream.h>
#include <HYDROData_StricklerTable.h>
#include <HYDROData_LandCoverMap.h>
#include <HYDROData_BCPolygon.h>

#include <CAM_Module.h>
#include <CAM_Study.h>

#include <LightApp_Application.h>
#include <LightApp_DataObject.h>
#include <LightApp_Study.h>

#include <SUIT_DataBrowser.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Study.h>
#include <SUIT_Tools.h>
#include <SUIT_TreeSync.h>
#include <SUIT_DataObjectIterator.h>

#include <HYDROData_Document.h>

#include <TDF_Delta.hxx>
#include <TDF_ListIteratorOfDeltaList.hxx>

#include <QApplication>
#include <QDir>

// #define DEB_GROUPS 1
#ifdef DEB_GROUPS
#include <HYDROData_ShapesGroup.h>
#endif

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

static HYDROData_SequenceOfObjects myCopyingObjects;

const int ENTRY_COLUMN = 2;


HYDROGUI_DataModel::HYDROGUI_DataModel( CAM_Module* theModule )
: LightApp_DataModel( theModule )
{
  DEBTRACE("HYDROGUI_DataModel");
  update( module()->application()->activeStudy()->id() );
}

HYDROGUI_DataModel::~HYDROGUI_DataModel()
{
  DEBTRACE("~HYDROGUI_DataModel");
}

bool HYDROGUI_DataModel::create( CAM_Study* theStudy )
{
  DEBTRACE("create");
    bool status = LightApp_DataModel::create( theStudy );
    if ( status )
        updateDocument();
    return status;
}

bool HYDROGUI_DataModel::open( const QString& theURL,
                               CAM_Study* theStudy,
                               QStringList theFileList )
{
  DEBTRACE("open");
  LightApp_DataModel::open( theURL, theStudy, theFileList );
  const int aStudyId = theStudy->id();

  Data_DocError res = DocError_UnknownProblem;
  if( theFileList.count() >= 2 )
  {
    QString aTmpDir = theFileList[0];
    QString aDataFileName = theFileList[1];
    QString aStatesFileName = theFileList.count() == 3 ? theFileList[2] : "";

    myStudyURL = theURL;
    QString aDataFullPath = SUIT_Tools::addSlash( aTmpDir ) + aDataFileName;
    QString aStatesFullPath = aStatesFileName.isEmpty() ? "" : SUIT_Tools::addSlash( aTmpDir ) + aStatesFileName;

    try
    {
      res = HYDROData_Document::Load( (char*)aDataFullPath.toLatin1().constData() );
    }
    catch(...)
    {
      res = DocError_UnknownProblem;
    }

    if ( res != DocError_OK )
    {
      module()->application()->putInfo( tr( "LOAD_ERROR" ) );
      return false;
    }

    if ( !aStatesFullPath.isEmpty() )
    {
      QFile aFile( aStatesFullPath );
      if( aFile.open( QFile::ReadOnly ) )
      {
        myStates = aFile.readAll();
        aFile.close();
      }
    }

    updateDocument();
  }

  // if the document open was successful, the data model update happens
  // in the set mode of the module
  if ( res == DocError_OK )
    update( aStudyId );

  return true;
}

bool HYDROGUI_DataModel::save( QStringList& theFileList )
{
  DEBTRACE("save");
  if( !module()->application()->activeStudy() )
    return false;

  LightApp_DataModel::save( theFileList );

  QString aTmpDir;
  SUIT_ResourceMgr* resMgr = module()->application()->resourceMgr();
  bool isMultiFile = false;
  if ( resMgr )
    isMultiFile = resMgr->booleanValue( "Study", "multi_file", false );

  // save module data to temporary files
  LightApp_Study* aStudy = dynamic_cast<LightApp_Study*>( module()->application()->activeStudy() );
  aTmpDir = aStudy->GetTmpDir( myStudyURL.toLatin1().constData(), isMultiFile ).c_str();

  // save OCAF data to a temporary file
  QString aDataFileName = SUIT_Tools::file( myStudyURL, false ) + "_HYDRO.cbf";
  QString aDataFullPath = aTmpDir + aDataFileName;
  Data_DocError res = getDocument()->Save( (char*)aDataFullPath.toLatin1().constData() );
  if( res != DocError_OK )
  {
    module()->application()->putInfo( tr( "SAVE_ERROR" ) );
    return false;
  }

  // save tree state data to a temporary file
  LightApp_Application* anApp = dynamic_cast<LightApp_Application*>( module()->application() );
  QByteArray aStatesData = anApp->objectBrowser()->getOpenStates( ENTRY_COLUMN );
  QString aStatesFileName = SUIT_Tools::file( myStudyURL, false ) + "_HYDRO_tree_states.txt";
  QString aStatesFullPath = aTmpDir + aStatesFileName;
  QFile aFile( aStatesFullPath );
  if( aFile.open( QFile::WriteOnly ) )
  {
    aFile.write( aStatesData );
    aFile.close();
  }

  // add temporary files to the list
  theFileList.append( aTmpDir );
  theFileList.append( aDataFileName );
  theFileList.append( aStatesFileName );

  return true;
}

bool HYDROGUI_DataModel::saveAs( const QString& theURL,
                                 CAM_Study*,
                                 QStringList& theFileList )
{
  DEBTRACE("saveAs");
  myStudyURL = theURL;
  return save( theFileList );
}

bool HYDROGUI_DataModel::close()
{
  DEBTRACE("close");
  HYDROGUI_Module* aModule = dynamic_cast<HYDROGUI_Module*>( module() );
  if ( aModule )
      aModule->clearCache();
  return true;
}

bool HYDROGUI_DataModel::dumpPython( const QString& theURL,
                                     CAM_Study*     theStudy,
                                     bool           isMultiFile,
                                     QStringList&   theListOfFiles )
{
  DEBTRACE("dumpPython");
  LightApp_DataModel::dumpPython( theURL, theStudy, isMultiFile, theListOfFiles );

  int aStudyId = theStudy->id();

  LightApp_Study* aStudy = ::qobject_cast<LightApp_Study*>( theStudy );
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( aDocument.IsNull() || !aStudy )
    return false;

  QString aDir = aStudy->GetTmpDir( theURL.toLatin1().constData(), isMultiFile ).c_str();
  QString aFileToExport = aDir + QString( QDir::separator() ) + "HYDRO.py";

  bool aRes = aDocument->DumpToPython( aFileToExport, isMultiFile );
  if ( aRes )
  {
    theListOfFiles.append( aDir );
    theListOfFiles.append( aFileToExport );
  }

  return aRes;
}

bool HYDROGUI_DataModel::isModified() const
{
  return getDocument()->IsModified();
}

bool HYDROGUI_DataModel::isSaved() const
{
  return true;
}

void HYDROGUI_DataModel::update( const int theStudyId )
{
  DEBTRACE("update");
  LightApp_Application* anApp = dynamic_cast<LightApp_Application*>( module()->application() );
  if( !anApp )
    return;

  SUIT_DataObject* aStudyRoot = anApp->activeStudy()->root();
  if( !aStudyRoot )
    return;

  // create a new root object
  CAM_DataObject* aNewRootObj = new CAM_DataObject();

  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if( aDocument.IsNull() )
    return;

  // Create root objects:

  // IMAGES
  LightApp_DataObject* anImageRootObj = createObject( aNewRootObj, tr( partitionName( KIND_IMAGE ).toLatin1() ) );

  // BATHYMETRY
  LightApp_DataObject* aBathymetryRootObj = createObject( aNewRootObj, tr( partitionName( KIND_BATHYMETRY ).toLatin1() ) );

  // POLYLINES
  LightApp_DataObject* aPolylineRootObj = createObject( aNewRootObj, tr( partitionName( KIND_POLYLINEXY ).toLatin1() ) );

  // PROFILES
  LightApp_DataObject* aProfileRootObj = createObject( aNewRootObj, tr( partitionName( KIND_PROFILE ).toLatin1() ) );

  // POLYLINES 3D
  LightApp_DataObject* aPolyline3DRootObj = createObject( aNewRootObj, tr( partitionName( KIND_POLYLINE ).toLatin1() ) );

  // NATURAL OBJECTS
  LightApp_DataObject* aNaturalObjectsRootObj = createObject( aNewRootObj, tr( partitionName( KIND_NATURAL_OBJECT ).toLatin1() ) );

  // ARTIFICIAL OBJECTS
  LightApp_DataObject* anArtificialObjectsRootObj = createObject( aNewRootObj, tr( partitionName( KIND_ARTIFICIAL_OBJECT ).toLatin1() ) );

  // OBSTACLES
  LightApp_DataObject* anObstaclesRootObj = createObject( aNewRootObj, tr( partitionName( KIND_OBSTACLE ).toLatin1() ) );

  //BC Polygons
  LightApp_DataObject* aBCPolygonRootObj = createObject( aNewRootObj, tr( partitionName( KIND_BC_POLYGON ).toLatin1() ) );

  // STRICKLER TABLES
  LightApp_DataObject* aStricklerTablesRootObj = createObject( aNewRootObj, tr( partitionName( KIND_STRICKLER_TABLE ).toLatin1() ) );

  // LAND COVER MAPS
  LightApp_DataObject* aLandCoversRootObj = createObject( aNewRootObj, tr( partitionName( KIND_LAND_COVER_MAP ).toLatin1() ) );

  // CALCULATION CASES
  LightApp_DataObject* aCalculRootObj = createObject( aNewRootObj, tr( partitionName( KIND_CALCULATION ).toLatin1() ) );

  // VISUAL STATES
  LightApp_DataObject* aVisualStateRootObj = createObject( aNewRootObj, tr( partitionName( KIND_VISUAL_STATE ).toLatin1() ) );


  int aNoStricklerTableObj = 0;

  HYDROData_Iterator anIterator( aDocument, KIND_UNKNOWN );
  std::vector<Handle(HYDROData_Entity)> ents;

  for( ; anIterator.More(); anIterator.Next() )
    ents.push_back(anIterator.Current());

  for (int i = 0; i< ents.size();i++)
  {
    LightApp_DataObject* obj = 0;
    Handle(HYDROData_Entity) anObj = ents[i];

    if ( !anObj.IsNull() )
    {
      switch ( anObj->GetKind() ) {
        case KIND_IMAGE:
        {
          Handle(HYDROData_Image) anImageObj =
            Handle(HYDROData_Image)::DownCast( anObj );
          if( !anImageObj.IsNull() ) {
            obj = createObject( anImageRootObj, anImageObj );
          }

          break;
        }
        case KIND_BATHYMETRY:
        {
          Handle(HYDROData_Bathymetry) aBathymetryObj =
            Handle(HYDROData_Bathymetry)::DownCast( anObj );
          if( !aBathymetryObj.IsNull() ) {
            obj = createObject( aBathymetryRootObj, aBathymetryObj );
          }

          break;
        }
        case KIND_CHANNEL:
        {
          Handle(HYDROData_Channel) aChannelObj =
            Handle(HYDROData_Channel)::DownCast( anObj );
          if( !aChannelObj.IsNull() ) {
            obj = createObject( anArtificialObjectsRootObj, aChannelObj );
          }

          break;
        }
        case KIND_DIGUE:
        {
          Handle(HYDROData_Digue) aDigueObj =
            Handle(HYDROData_Digue)::DownCast( anObj );
          if( !aDigueObj.IsNull() ) {
            obj = createObject( anArtificialObjectsRootObj, aDigueObj );
          }

          break;
        }
        case KIND_IMMERSIBLE_ZONE:
        {
          Handle(HYDROData_ImmersibleZone) anImmersibleZoneObj =
            Handle(HYDROData_ImmersibleZone)::DownCast( anObj );
          if( !anImmersibleZoneObj.IsNull() ) {
            obj = createObject( aNaturalObjectsRootObj, anImmersibleZoneObj );
          }

          break;
        }
        case KIND_RIVER:
        {
          Handle(HYDROData_River) aRiverObj =
            Handle(HYDROData_River)::DownCast( anObj );
          if( !aRiverObj.IsNull() ) {
            obj = createObject( aNaturalObjectsRootObj, aRiverObj );
          }

          break;
        }
        case KIND_STREAM:
        {
          Handle(HYDROData_Stream) aStreamObj =
            Handle(HYDROData_Stream)::DownCast( anObj );
          if( !aStreamObj.IsNull() ) {
            obj = createObject( aNaturalObjectsRootObj, aStreamObj );
          }

          break;
        }
        case KIND_OBSTACLE:
        {
          Handle(HYDROData_Obstacle) anObstacleObj =
            Handle(HYDROData_Obstacle)::DownCast( anObj );
          if( !anObstacleObj.IsNull() ) {
            obj = createObject( anObstaclesRootObj, anObstacleObj );
          }

          break;
        }
		case KIND_STRICKLER_TABLE:
        {
          Handle(HYDROData_StricklerTable) aStricklerTableObj =
            Handle(HYDROData_StricklerTable)::DownCast( anObj );
          if( !aStricklerTableObj.IsNull() ) {
            obj = createObject( aStricklerTablesRootObj, aStricklerTableObj );
          }
		  aNoStricklerTableObj++;

          break;
        }
        case KIND_LAND_COVER_MAP:
        {
          Handle(HYDROData_LandCoverMap) aLandCoverMapObj =
            Handle(HYDROData_LandCoverMap)::DownCast( anObj );
          if( !aLandCoverMapObj.IsNull() ) {
            obj = createObject( aLandCoversRootObj, aLandCoverMapObj );
          }

          break;
        }
        case KIND_CALCULATION:
        {
          Handle(HYDROData_CalculationCase) aCalculObj =
            Handle(HYDROData_CalculationCase)::DownCast( anObj );
          if( !aCalculObj.IsNull() ) {
            obj = createObject( aCalculRootObj, aCalculObj );
          }

          break;
        }
        case KIND_POLYLINEXY:
        {
          Handle(HYDROData_PolylineXY) aPolylineObj =
            Handle(HYDROData_PolylineXY)::DownCast( anObj );
          if( !aPolylineObj.IsNull() ) {
            obj = createObject( aPolylineRootObj, aPolylineObj );
          }

          break;
        }
        case KIND_POLYLINE:
        {
          Handle(HYDROData_Polyline3D) aPolylineObj =
            Handle(HYDROData_Polyline3D)::DownCast( anObj );
          if( !aPolylineObj.IsNull() ) {
            obj = createObject( aPolyline3DRootObj, aPolylineObj );
          }

          break;
        }
        case KIND_PROFILE:
        {
          Handle(HYDROData_Profile) aProfileObj =
            Handle(HYDROData_Profile)::DownCast( anObj );
          if( !aProfileObj.IsNull() ) {
            obj = createObject( aProfileRootObj, aProfileObj );
          }

          break;
        }
        case KIND_VISUAL_STATE:
        {
          Handle(HYDROData_VisualState) aVisualStateObj =
            Handle(HYDROData_VisualState)::DownCast( anObj );
          if( !aVisualStateObj.IsNull() ) {
            obj = createObject( aVisualStateRootObj, aVisualStateObj );
          }

          break;
        }
        case KIND_BC_POLYGON:
        {
          Handle(HYDROData_BCPolygon) aBCPolygonObj =
            Handle(HYDROData_BCPolygon)::DownCast( anObj );
          if( !aBCPolygonObj.IsNull() ) {
            obj = createObject( aBCPolygonRootObj, aBCPolygonObj );
          }

          break;
        }
      }
    }
  }

  // Create default Strickler table object
  if ( aNoStricklerTableObj == 0 )
    createDefaultStricklerTable( aDocument, aStricklerTablesRootObj );

  //if( SUIT_DataBrowser* anObjectBrowser = anApp->objectBrowser() )
  //{
  //  anObjectBrowser->setAutoOpenLevel( 3 );
  //  anObjectBrowser->openLevels();
  //}

  SUIT_DataObject* aRoot = root();

  if (aRoot)
  {
    std::map<std::string, SUIT_DataObject*> entry2ObjNewRoot;
    SUIT_DataObjectIterator::DetourType dt = SUIT_DataObjectIterator::DepthLeft;
    for ( SUIT_DataObjectIterator it( aNewRootObj, dt ); it.current(); ++it )
    {
      LightApp_DataObject* aCurObjN = dynamic_cast<LightApp_DataObject*>(it.current());
      if (aCurObjN)
        entry2ObjNewRoot[aCurObjN->entry().toStdString()] = it.current();
    }

    for ( SUIT_DataObjectIterator it( aRoot, dt ); it.current(); ++it )
    {
      LightApp_DataObject* aCurObjO = dynamic_cast<LightApp_DataObject*>(it.current());
      if (aCurObjO && aCurObjO->childCount() > 0)
      {
        std::string entry = aCurObjO->entry().toStdString();
        SUIT_DataObject* newNode = entry2ObjNewRoot[entry];
        if (newNode && aCurObjO->childCount() > 0)
        {
          DataObjectList newchildren;
          newNode->children(newchildren);
          //new root - remove children
          std::map<std::string, SUIT_DataObject*> newNode2Entries;
          for ( DataObjectList::const_iterator it = newchildren.begin(); it != newchildren.end(); ++it )
          {
            SUIT_DataObject* cc = *it;
            LightApp_DataObject* obj = dynamic_cast<LightApp_DataObject*>(cc);
            newNode2Entries[obj->entry().toStdString()] = cc;
            newNode->removeChild(cc);
          }
          //
          std::set<SUIT_DataObject*> objtemp;

          DataObjectList oldchildren;
          aCurObjO->children(oldchildren);
          for ( DataObjectList::const_iterator it = oldchildren.begin(); it != oldchildren.end(); ++it )
          {
            SUIT_DataObject* old_ch = *it;
            std::string entr = dynamic_cast<LightApp_DataObject*>(old_ch)->entry().toStdString();
            if (newNode2Entries.count(entr) > 0)
            {
              SUIT_DataObject* obj = newNode2Entries[entr];
              newNode->appendChild(obj);
              objtemp.insert(obj);
            }
          }
          //
          for ( DataObjectList::const_iterator it = newchildren.begin(); it != newchildren.end(); ++it )
          {
            SUIT_DataObject* ch = *it;
            if (objtemp.count(ch) == 0)
              newNode->appendChild(ch);
          }
          { //IF DEBUG
            //add. check
            DataObjectList newchildren2;
            newNode->children(newchildren2);
            std::set<std::string> entries2, entries1;
            for ( DataObjectList::const_iterator it = newchildren2.begin(); it != newchildren2.end(); ++it )
              entries2.insert((dynamic_cast<LightApp_DataObject*>(*it))->entry().toStdString());
            for ( DataObjectList::const_iterator it = newchildren.begin(); it != newchildren.end(); ++it )
              entries1.insert((dynamic_cast<LightApp_DataObject*>(*it))->entry().toStdString());
            assert(entries1 == entries2);
          }
        }
      }
    }
  }

  HYDROGUI_DataModelSync aSync( aNewRootObj );
  bool isNewDoc = aRoot==0;
  if( isNewDoc )
    aRoot = createRootModuleObject( aStudyRoot );
  ::synchronize < suitPtr, suitPtr, HYDROGUI_DataModelSync >
    ( aNewRootObj, aRoot, aSync );

  SUIT_DataBrowser* ob = anApp->objectBrowser();

  if ( !myStates.isEmpty() )
  {
    ob->updateTree();
    ob->setAutoOpenLevel( 1 );
    //ob->setOpenStates( myStates, ENTRY_COLUMN );
    myStates.clear();
  }
}

HYDROGUI_DataObject* HYDROGUI_DataModel::getDataObject( const Handle(HYDROData_Entity)& theModelObject )
{
  HYDROGUI_DataObject* aGuiObj = dynamic_cast<HYDROGUI_DataObject*>(
    findObject( HYDROGUI_DataObject::dataObjectEntry( theModelObject ) ) );
  return aGuiObj;
}

HYDROGUI_DataObject* HYDROGUI_DataModel::getReferencedDataObject( HYDROGUI_DataObject* theObject )
{
  return NULL; // to do if necessary
}

SUIT_DataObject* HYDROGUI_DataModel::findObject( const QString& theEntry ) const
{
  LightApp_Application* anApp = dynamic_cast<LightApp_Application*>( module()->application() );
  return anApp ? anApp->findObject( theEntry ) : 0;
}

void HYDROGUI_DataModel::update( LightApp_DataObject* theObject,
                                 LightApp_Study* theStudy )
{
  if( !theStudy )
    theStudy = dynamic_cast<LightApp_Study*>( module()->application()->activeStudy()) ;
  if( theStudy )
    update( theStudy->id() );
}

CAM_DataObject* HYDROGUI_DataModel::createRootModuleObject( SUIT_DataObject* theParent )
{
  CAM_ModuleObject* aRootObj = createModuleObject( theParent );
  aRootObj->setDataModel( this );
  setRoot( aRootObj );
  return aRootObj;
}

void HYDROGUI_DataModel::updateModel()
{
  DEBTRACE("updateModel");
  HYDROGUI_Module* aModule = dynamic_cast<HYDROGUI_Module*>( module() );
  if( aModule )
    update( aModule->getStudyId() );
}

Handle(HYDROData_Entity) HYDROGUI_DataModel::objectByEntry( const QString& theEntry,
                                                            const ObjectKind theObjectKind )
{
  QString anEntry = theEntry;
  if( anEntry.indexOf( "_" ) != -1 ) // reference object
    anEntry = anEntry.section( "_", -1 );
  //DEBTRACE("objectByEntry: " << anEntry.toStdString() << " kind: " << theObjectKind);
  Handle(HYDROData_Document) aDocument = getDocument();
  if( !aDocument.IsNull() )
  {
    HYDROData_Iterator anIterator( aDocument, theObjectKind );
    for( ; anIterator.More(); anIterator.Next() )
    {
      Handle(HYDROData_Entity) anObject = anIterator.Current();
      if( !anObject.IsNull() )
      {
        QString anEntryRef = HYDROGUI_DataObject::dataObjectEntry( anObject );
        //DEBTRACE("  anEntryRef " << anEntryRef.toStdString());
        if( anEntryRef == anEntry )
        {
          //DEBTRACE("  found!");
          return anObject;
        }
      }
    }
  }
  return NULL;
}

bool HYDROGUI_DataModel::canUndo() const
{
  return getDocument()->CanUndo();
}

bool HYDROGUI_DataModel::canRedo() const
{
  return getDocument()->CanRedo();
}

QStringList HYDROGUI_DataModel::undoNames() const
{
  QStringList aNames;
  for( TDF_ListIteratorOfDeltaList anIter( getDocument()->GetUndos() ); anIter.More(); anIter.Next() )
    aNames.prepend( HYDROGUI_Tool::ToQString( anIter.Value()->Name() ) );
  return aNames;
}

QStringList HYDROGUI_DataModel::redoNames() const
{
  QStringList aNames;
  for( TDF_ListIteratorOfDeltaList anIter( getDocument()->GetRedos() ); anIter.More(); anIter.Next() )
    aNames.append( HYDROGUI_Tool::ToQString( anIter.Value()->Name() ) );
  return aNames;
}

void HYDROGUI_DataModel::clearUndos()
{
  getDocument()->ClearUndos();
}

void HYDROGUI_DataModel::clearRedos()
{
  getDocument()->ClearRedos();
}

bool HYDROGUI_DataModel::undo()
{
  try
  {
    getDocument()->Undo();
  }
  catch ( Standard_Failure )
  {
    return false;
  }
  return true;
}

bool HYDROGUI_DataModel::redo()
{
  try
  {
    getDocument()->Redo();
  }
  catch ( Standard_Failure )
  {
    return false;
  }
  return true;
}

bool HYDROGUI_DataModel::canCopy()
{
  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( (HYDROGUI_Module*)module() );
  bool isCanCopy = !aSeq.IsEmpty();

  for ( Standard_Integer anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ ) {
    Handle(HYDROData_Entity) anObject = aSeq.Value( anIndex );
    if( !anObject.IsNull() ) {
      ObjectKind aKind = anObject->GetKind();
      bool isUnrecognized = aKind <= KIND_UNKNOWN || aKind > KIND_LAST;
      bool isChildObject = aKind == KIND_DUMMY_3D ||
                           aKind == KIND_ZONE ||
                           aKind == KIND_SHAPES_GROUP ||
                           aKind == KIND_SPLIT_GROUP;
      if ( isUnrecognized || isChildObject ) {
        isCanCopy = false;
        break;
      }
    }
  }

  return isCanCopy;
}

bool HYDROGUI_DataModel::canPaste()
{
  for( int anIndex = 1, aLength = myCopyingObjects.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObject = myCopyingObjects.Value( anIndex );
    if( !anObject.IsNull() && !anObject->IsRemoved() )
      return true;
  }
  return false;
}

bool HYDROGUI_DataModel::copy()
{
  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( (HYDROGUI_Module*)module() );
  changeCopyingObjects( aSeq );
  return true;
}

bool HYDROGUI_DataModel::paste()
{
  bool anIsChanged = false;
  for( int anIndex = 1, aLength = myCopyingObjects.Length(); anIndex <= aLength; anIndex++ )
  {
    Handle(HYDROData_Entity) anObject = myCopyingObjects.Value( anIndex );
    if( !anObject.IsNull() && !anObject->IsRemoved() )
    {
      ObjectKind aKind = anObject->GetKind();
      Handle(HYDROData_Entity) aClone = getDocument()->CreateObject( aKind );
      if( !aClone.IsNull() )
      {
        anObject->CopyTo( aClone, true );
        anIsChanged = true;

        // remove Z layer
        aClone->RemoveZLevel();
      }
    }
  }
  return anIsChanged;
}

void HYDROGUI_DataModel::changeCopyingObjects( const HYDROData_SequenceOfObjects& theSeq )
{
  myCopyingObjects.Assign( theSeq );
}

QString HYDROGUI_DataModel::partitionName( const ObjectKind theObjectKind )
{
  switch( theObjectKind )
  {
    case KIND_IMAGE:             return "IMAGES";
    case KIND_POLYLINE:          return "POLYLINES_3D";
    case KIND_POLYLINEXY:        return "POLYLINES";
    case KIND_PROFILE:           return "PROFILES";
    case KIND_VISUAL_STATE:      return "VISUAL_STATES";
    case KIND_BATHYMETRY:        return "BATHYMETRIES";
    case KIND_CALCULATION:       return "CALCULATION_CASES";
    case KIND_OBSTACLE:          return "OBSTACLES";
    case KIND_ARTIFICIAL_OBJECT: return "ARTIFICIAL_OBJECTS";
    case KIND_NATURAL_OBJECT:    return "NATURAL_OBJECTS";
    case KIND_STRICKLER_TABLE:   return "STRICKLER_TABLES";
    case KIND_LAND_COVER_MAP:    return "LAND_COVER_MAPS";
    case KIND_REGION:            return "REGIONS";
    case KIND_BC_POLYGON:        return "BOUNDARY_POLYGONS";
    default: break;
  }
  return QString();
}

Handle(HYDROData_Document) HYDROGUI_DataModel::getDocument() const
{
  int aStudyId = module()->application()->activeStudy()->id();
  return HYDROData_Document::Document();
}

LightApp_DataObject* HYDROGUI_DataModel::createObject( SUIT_DataObject*         theParent,
                                                       Handle(HYDROData_Entity) theModelObject,
                                                       const QString&           theParentEntry,
                                                       const bool               theIsBuildTree )
{
  HYDROGUI_DataObject* aResObj = new HYDROGUI_DataObject( theParent, theModelObject, theParentEntry );

  const ObjectKind aKind = theModelObject->GetKind();
  bool visibility = aKind == KIND_IMAGE || aKind == KIND_POLYLINEXY || aKind == KIND_POLYLINE ||
                    aKind == KIND_SHAPES_GROUP || aKind == KIND_SPLIT_GROUP || aKind == KIND_ZONE ||
                    aKind == KIND_IMMERSIBLE_ZONE || aKind == KIND_REGION || aKind == KIND_BATHYMETRY ||
                    aKind == KIND_OBSTACLE || aKind == KIND_STREAM || aKind == KIND_CHANNEL ||
                    aKind == KIND_DIGUE || aKind == KIND_DUMMY_3D || aKind == KIND_LAND_COVER_MAP ||
                    aKind == KIND_BC_POLYGON;
  if ( !visibility )
  {
    Handle(HYDROData_Profile) aProfObj = Handle(HYDROData_Profile)::DownCast( theModelObject );
    visibility = !aProfObj.IsNull() && aProfObj->IsValid();
  }

  if ( aKind == KIND_REGION )
  {
      QString an = aResObj->name();
      int a = 0;
  }

  if ( visibility )
  {
    setObjectVisibilityState( theModelObject, aResObj );
  }

  if ( theIsBuildTree )
  {
    buildObjectTree( theParent, aResObj, theParentEntry );
  }

  return aResObj;
}

LightApp_DataObject* HYDROGUI_DataModel::buildObject( SUIT_DataObject*     theParent,
                                                      HYDROGUI_DataObject* theObject,
                                                      const QString&       theParentEntry,
                                                      const bool           theIsBuildTree,
                                                      const bool           theIsInOperation )
{
  if ( theIsBuildTree )
  {
    buildObjectTree( theParent, theObject, theParentEntry, theIsInOperation );
  }
  return theObject;
}

LightApp_DataObject* HYDROGUI_DataModel::createZone( SUIT_DataObject*       theParent,
                                                     Handle(HYDROData_Zone) theModelObject,
                                                     const QString&         theParentEntry,
                                                     const bool             theIsBuildTree,
                                                     const bool             theIsInOperation )
{
  HYDROGUI_Zone* aZone = new HYDROGUI_Zone( theParent, theModelObject, theParentEntry, theIsInOperation );
  LightApp_DataObject* aDataObj = buildObject( theParent, aZone, theParentEntry, theIsBuildTree, theIsInOperation );

  setObjectVisibilityState( theModelObject, aZone );

  return aDataObj;
}

LightApp_DataObject* HYDROGUI_DataModel::createRegion( SUIT_DataObject*         theParent,
                                                       Handle(HYDROData_Region) theModelObject,
                                                       const QString&           theParentEntry,
                                                       const bool               theIsBuildTree,
                                                       const bool               theIsInOperation )
{
  return buildObject( theParent, new HYDROGUI_Region( theParent, theModelObject, theParentEntry, theIsInOperation ),
    theParentEntry, theIsBuildTree, theIsInOperation );
}

void HYDROGUI_DataModel::createDefaultStricklerTable( const Handle(HYDROData_Document)& theDocument,
                                                      LightApp_DataObject*              theParent )
{
  // Create default Strickler table object
  Handle(HYDROData_StricklerTable) aStricklerTableObj =
    Handle(HYDROData_StricklerTable)::DownCast( theDocument->CreateObject(KIND_STRICKLER_TABLE) );
  if ( !aStricklerTableObj.IsNull() )
  {
    SUIT_ResourceMgr* resMgr = module()->application()->resourceMgr();
    QString defTablePath = resMgr->path( "resources", module()->name(), tr( "DEFAULT_STRICKLER_TABLE_FILE" ) );
    aStricklerTableObj->Import( defTablePath );
	// Set name
    QString aStricklerTableName;
    if ( aStricklerTableObj->GetName().isEmpty() )
    {
      HYDROGUI_Module* aModule = dynamic_cast<HYDROGUI_Module*>( module() );
      if ( aModule )
        aStricklerTableName = HYDROGUI_Tool::GenerateObjectName( aModule, tr( "DEFAULT_STRICKLER_TABLE_NAME" ) );
    }
    if ( aStricklerTableObj->GetName() != aStricklerTableName )
      aStricklerTableObj->SetName( aStricklerTableName );

    aStricklerTableObj->Update();

    LightApp_DataObject* obj = createObject( theParent, aStricklerTableObj );
  }
}

LightApp_DataObject* HYDROGUI_DataModel::createObject( SUIT_DataObject* theParent,
                                                       const QString&   theName,
                                                       const QString&   theParentEntry )
{
  return new HYDROGUI_NamedObject( theParent, theName, theParentEntry );
}

void HYDROGUI_DataModel::buildObjectPartition( SUIT_DataObject*                   theObject,
                                               const HYDROData_SequenceOfObjects& theObjects,
                                               const QString&                     thePartName,
                                               const bool                         theIsCreateEmpty )
{
  if ( theObjects.IsEmpty() && !theIsCreateEmpty )
    return;

  HYDROGUI_DataObject* aGuiObj = dynamic_cast<HYDROGUI_DataObject*>( theObject );
  if ( !aGuiObj )
    return;

  LightApp_DataObject* aPartSect =
    createObject( aGuiObj, thePartName, aGuiObj->entry() );

  HYDROData_SequenceOfObjects::Iterator anIter( theObjects );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObj = anIter.Value();
    if( !anObj.IsNull() && !anObj->IsRemoved() )
      createObject( aPartSect, anObj, aGuiObj->entry(), false );
  }
}

void HYDROGUI_DataModel::buildObjectTree( SUIT_DataObject* theParent,
                                          SUIT_DataObject* theObject,
                                          const QString&   theParentEntry,
                                          const bool       theIsInOperation )
{
  HYDROGUI_DataObject* aGuiObj = dynamic_cast<HYDROGUI_DataObject*>( theObject );
  if ( !aGuiObj )
    return;

  Handle(HYDROData_Entity) aDataObj = aGuiObj->modelObject();
  if ( aDataObj.IsNull() )
    return;

  if ( aDataObj->IsKind( STANDARD_TYPE(HYDROData_Object) ) )
  {
    Handle(HYDROData_Object) aGeomObj =
      Handle(HYDROData_Object)::DownCast( aDataObj );

    Handle(HYDROData_DummyObject3D) anObject3D = aGeomObj->GetObject3D();
    if ( !anObject3D.IsNull() )
      createObject( aGuiObj, anObject3D, "", false );

#ifdef DEB_GROUPS
    HYDROData_SequenceOfObjects anObjGroups = aGeomObj->GetGroups();
    buildObjectPartition( aGuiObj, anObjGroups, tr( "OBJECT_GROUPS" ), false );
#endif
  }

  ObjectKind anObjectKind = aDataObj->GetKind();

  if ( anObjectKind == KIND_IMAGE )
  {
    Handle(HYDROData_Image) anImageObj =
      Handle(HYDROData_Image)::DownCast( aDataObj );
    for ( int anIndex = 0, aNbRef = anImageObj->NbReferences(); anIndex < aNbRef; anIndex++ )
    {
      Handle(HYDROData_Entity) aRefObj = anImageObj->Reference( anIndex );
      if ( !aRefObj.IsNull() && !aRefObj->IsRemoved() )
        createObject( aGuiObj, aRefObj, aGuiObj->entry(), false );
    }
  }
  else if ( anObjectKind == KIND_IMMERSIBLE_ZONE )
  {
    Handle(HYDROData_ImmersibleZone) aZoneObj =
      Handle(HYDROData_ImmersibleZone)::DownCast( aDataObj );

    LightApp_DataObject* aPolylineSect =
      createObject( aGuiObj, tr( "ZONE_POLYLINE" ), aGuiObj->entry() );

    Handle(HYDROData_PolylineXY) aPolyline = aZoneObj->GetPolyline();
    if ( !aPolyline.IsNull() && !aPolyline->IsRemoved() )
      createObject( aPolylineSect, aPolyline, aGuiObj->entry(), false );

    LightApp_DataObject* aBathSect =
      createObject( aGuiObj, tr( "ZONE_BATHYMETRY" ), aGuiObj->entry() );

    Handle(HYDROData_IAltitudeObject) anAltitudeObj = aZoneObj->GetAltitudeObject();
    if ( !anAltitudeObj.IsNull() && !anAltitudeObj->IsRemoved() )
      createObject( aBathSect, anAltitudeObj, aGuiObj->entry(), false );
  }
  else if ( anObjectKind == KIND_POLYLINE )
  {
    Handle(HYDROData_Polyline3D) aPolyline3D =
      Handle(HYDROData_Polyline3D)::DownCast( aDataObj );

    LightApp_DataObject* aPolylineSect =
      createObject( aGuiObj, tr( "POLYLINE3D_POLYLINE" ), aGuiObj->entry() );

    Handle(HYDROData_PolylineXY) aPolylineXY = aPolyline3D->GetPolylineXY();
    if ( !aPolylineXY.IsNull() && !aPolylineXY->IsRemoved() )
      createObject( aPolylineSect, aPolylineXY, aGuiObj->entry(), false );

    LightApp_DataObject* aProfileSect =
      createObject( aGuiObj, tr( "POLYLINE3D_PROFILE" ), aGuiObj->entry() );

    Handle(HYDROData_ProfileUZ) aProfileUZ = aPolyline3D->GetProfileUZ();
    if ( aProfileUZ.IsNull() || aProfileUZ->IsRemoved() )
      aProfileUZ = aPolyline3D->GetChildProfileUZ( false );

    if ( !aProfileUZ.IsNull() && !aProfileUZ->IsRemoved() )
    {
      Handle(HYDROData_Profile) aProfile =
        Handle(HYDROData_Profile)::DownCast( aProfileUZ->GetFatherObject() );
      if ( !aProfile.IsNull() && !aProfile->IsRemoved() )
        createObject( aProfileSect, aProfile, aGuiObj->entry(), false );
    }

    LightApp_DataObject* aBathSect =
      createObject( aGuiObj, tr( "POLYLINE3D_BATHYMETRY" ), aGuiObj->entry() );

    Handle(HYDROData_IAltitudeObject) anAltitudeObj = aPolyline3D->GetAltitudeObject();
    if ( !anAltitudeObj.IsNull() && !anAltitudeObj->IsRemoved() )
      createObject( aBathSect, anAltitudeObj, aGuiObj->entry(), false );
  }
  else if ( anObjectKind == KIND_CALCULATION )
  {
    Handle(HYDROData_CalculationCase) aCaseObj =
      Handle(HYDROData_CalculationCase)::DownCast( aDataObj );

    LightApp_DataObject* aPolylineSect =
      createObject( aGuiObj, tr( "CASE_BOUNDARY" ), aGuiObj->entry() );

    Handle(HYDROData_PolylineXY) aPolyline = aCaseObj->GetBoundaryPolyline();
    if ( !aPolyline.IsNull() && !aPolyline->IsRemoved() )
      createObject( aPolylineSect, aPolyline, aGuiObj->entry(), false );

    LightApp_DataObject* aCaseAOSect =
      createObject( aGuiObj, tr( partitionName( KIND_ARTIFICIAL_OBJECT ).toLatin1() ),
                    aGuiObj->entry() );
    LightApp_DataObject* aCaseNOSect =
      createObject( aGuiObj, tr( partitionName( KIND_NATURAL_OBJECT ).toLatin1() ),
                    aGuiObj->entry() );

    HYDROData_SequenceOfObjects aSeq = aCaseObj->GetGeometryObjects();
    HYDROData_SequenceOfObjects::Iterator aGOIter( aSeq );
    Handle(HYDROData_Entity) anEntity;
    Handle(HYDROData_ArtificialObject) anAObject;
    Handle(HYDROData_NaturalObject) aNObject;
    for ( ; aGOIter.More(); aGOIter.Next() )
    {
      anEntity = aGOIter.Value();
      if ( anEntity.IsNull() )
        continue;
      anAObject = Handle(HYDROData_ArtificialObject)::DownCast( anEntity );
      if ( !anAObject.IsNull() )
        createObject( aCaseAOSect, anAObject, aGuiObj->entry(), false );
      else
      {
        aNObject = Handle(HYDROData_NaturalObject)::DownCast( anEntity );
        if ( !aNObject.IsNull() )
          createObject( aCaseNOSect, aNObject, aGuiObj->entry(), false );
      }
    }

    LightApp_DataObject* aBoundaryPolygonSect =
      createObject( aGuiObj, tr( "CASE_BOUNDARY_POLYGONS" ), aGuiObj->entry() );
    HYDROData_SequenceOfObjects aBCPolygons = aCaseObj->GetBoundaryPolygons();
    for (int i = 1; i <= aBCPolygons.Size(); i++ )
      createObject( aBoundaryPolygonSect, aBCPolygons(i), aGuiObj->entry(), false );

    LightApp_DataObject* aLandCoverMapSect =
      createObject( aGuiObj, tr( "CASE_LAND_COVER_MAP" ), aGuiObj->entry() );

    Handle(HYDROData_LandCoverMap) aLandCoverMap = aCaseObj->GetLandCoverMap();
    if ( !aLandCoverMap.IsNull() && !aLandCoverMap->IsRemoved() )
      createObject( aLandCoverMapSect, aLandCoverMap, aGuiObj->entry(), false );

    LightApp_DataObject* aCaseRegionsSect =
      createObject( aGuiObj, tr( "CASE_REGIONS" ), aGuiObj->entry() );

    HYDROData_SequenceOfObjects aCaseRegions = aCaseObj->GetRegions();
    HYDROData_SequenceOfObjects::Iterator anIter( aCaseRegions );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Region) aCaseRegion =
        Handle(HYDROData_Region)::DownCast( anIter.Value() );
      if( !aCaseRegion.IsNull() && !aCaseRegion->IsRemoved() )
        createRegion( aCaseRegionsSect, aCaseRegion, "", true, theIsInOperation );
    }

#ifdef DEB_GROUPS
    HYDROData_SequenceOfObjects aCalcGroups = aCaseObj->GetGeometryGroups();
    buildObjectPartition( aGuiObj, aCalcGroups, tr( "OBJECT_GROUPS" ), false );

    HYDROData_SequenceOfObjects aCalcSplitGroups = aCaseObj->GetSplitGroups();
    buildObjectPartition( aGuiObj, aCalcSplitGroups, tr( "CASE_SPLIT_GROUPS" ), false );
#endif

  }
  else if ( anObjectKind == KIND_REGION )
  {
    Handle(HYDROData_Region) aRegionObj =
      Handle(HYDROData_Region)::DownCast( aDataObj );

    HYDROData_SequenceOfObjects aRegionZones = aRegionObj->GetZones();
    HYDROData_SequenceOfObjects::Iterator anIter( aRegionZones );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Zone) aRegionZone =
        Handle(HYDROData_Zone)::DownCast( anIter.Value() );
      if( !aRegionZone.IsNull() && !aRegionZone->IsRemoved() )
        createZone( aGuiObj, aRegionZone, "", true, theIsInOperation );
    }
  }
  else if ( anObjectKind == KIND_PROFILE )
  {
    Handle(HYDROData_Profile) aProfileObj =
      Handle(HYDROData_Profile)::DownCast( aDataObj );

    aGuiObj->setIsValid( aProfileObj->IsValid() );
  }
  else if ( anObjectKind == KIND_CHANNEL || anObjectKind == KIND_DIGUE )
  {
    Handle(HYDROData_Channel) aChannelObj =
      Handle(HYDROData_Channel)::DownCast( aDataObj );

    LightApp_DataObject* aGuideLineSect =
      createObject( aGuiObj, tr( "CHANNEL_GUIDE_LINE" ), aGuiObj->entry() );
    Handle(HYDROData_Polyline3D) aGuideLine = aChannelObj->GetGuideLine();
    if ( !aGuideLine.IsNull() && !aGuideLine->IsRemoved() ) {
      createObject( aGuideLineSect, aGuideLine, aGuiObj->entry(), false );
    }

    LightApp_DataObject* aProfileSect =
      createObject( aGuiObj, tr( "CHANNEL_PROFILE" ), aGuiObj->entry() );
    Handle(HYDROData_Profile) aProfile = aChannelObj->GetProfile();
    if ( !aProfile.IsNull() && !aProfile->IsRemoved() ) {
      createObject( aProfileSect, aProfile, aGuiObj->entry(), false );
    }
  }
  else if ( anObjectKind == KIND_STREAM )
  {
    Handle(HYDROData_Stream) aStreamObj =
      Handle(HYDROData_Stream)::DownCast( aDataObj );

    LightApp_DataObject* aHydraulicAxisSect =
      createObject( aGuiObj, tr( "STREAM_HYDRAULIC_AXIS" ), aGuiObj->entry() );
    Handle(HYDROData_PolylineXY) aHydraulicAxis = aStreamObj->GetHydraulicAxis();
    if ( !aHydraulicAxis.IsNull() && !aHydraulicAxis->IsRemoved() ) {
      createObject( aHydraulicAxisSect, aHydraulicAxis, aGuiObj->entry(), false );
    }

    HYDROData_SequenceOfObjects aProfiles = aStreamObj->GetProfiles();
    buildObjectPartition( aGuiObj, aProfiles, tr( "STREAM_PROFILES" ), true );

    Handle(HYDROData_Polyline3D) aBottomPolyline = aStreamObj->GetBottomPolyline();
    if ( !aBottomPolyline.IsNull() && !aBottomPolyline->IsRemoved() ) {
      createObject( aGuiObj, aBottomPolyline, aGuiObj->entry(), false );
    }
  }
  else if ( anObjectKind == KIND_LAND_COVER_MAP )
  {
    Handle(HYDROData_LandCoverMap) aLandCoverMapObj =
      Handle(HYDROData_LandCoverMap)::DownCast( aDataObj );

    /*TODO: reference objects of the land cover map
    HYDROData_SequenceOfObjects aPolylines = aLandCoverMapObj->GetPolylines();
    buildObjectPartition( aGuiObj, aPolylines, tr( "LAND_COVER_POLYLINES" ), true );*/
  }
  else if ( anObjectKind == KIND_BC_POLYGON )
  {
    Handle(HYDROData_BCPolygon) aBCObj =
      Handle(HYDROData_BCPolygon)::DownCast( aDataObj );

    LightApp_DataObject* aPolylineSect =
      createObject( aGuiObj, tr( "BC_POLYGON_POLYLINE" ), aGuiObj->entry() );

    Handle(HYDROData_PolylineXY) aPolyline = aBCObj->GetPolyline();
    if ( !aPolyline.IsNull() && !aPolyline->IsRemoved() )
      createObject( aPolylineSect, aPolyline, aGuiObj->entry(), false );
  }
  HYDROGUI_Module* aModule = dynamic_cast<HYDROGUI_Module*>( module() );
  if( aModule )
    aModule->enableLCMActions();

}

void HYDROGUI_DataModel::buildCaseTree( SUIT_DataObject* theParent, Handle(HYDROData_CalculationCase) theCase )
{
  if ( !theCase.IsNull() )
  {
    if ( theParent )
    {
      // Remove previous objects tree
        DataObjectList aList;
        theParent->children( aList );
        QListIterator<SUIT_DataObject*> anIter( aList );
        while( anIter.hasNext() )
          removeChild( theParent, anIter.next() );
    }

    new HYDROGUI_DropTargetObject( theParent, tr( "NEW_REGION" ), "", true );

    HYDROData_SequenceOfObjects aCaseRegions = theCase->GetRegions();
    HYDROData_SequenceOfObjects::Iterator anIter( aCaseRegions );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Region) aCaseRegion =
        Handle(HYDROData_Region)::DownCast( anIter.Value() );
      if( !aCaseRegion.IsNull() && !aCaseRegion->IsRemoved() )
        createRegion( theParent, aCaseRegion, "", true, true );
    }
  }
}

void HYDROGUI_DataModel::updateObjectTree( Handle(HYDROData_Entity)& theObj )
{
  if ( !theObj.IsNull() )
  {
    HYDROGUI_DataObject* aGuiObj = dynamic_cast<HYDROGUI_DataObject*>(
      findObject( HYDROGUI_DataObject::dataObjectEntry( theObj ) ) );
    if ( aGuiObj )
    {
      // Remove previous objects tree
      DataObjectList aList;
      aGuiObj->children( aList );
      QListIterator<SUIT_DataObject*> anIter( aList );
      while( anIter.hasNext() )
        removeChild( aGuiObj, anIter.next() );

      // Rebuild the subtree
      QString aParentEntry;
      HYDROGUI_DataObject* aParent = dynamic_cast<HYDROGUI_DataObject*>( aGuiObj->parent() );
      if ( aParent )
      {
        aParentEntry = aParent->entry();
      }
      buildObjectTree( aParent, aGuiObj, aParentEntry, aGuiObj->isInOperation() );
    }
    else
    {
      // workaround for the bug in SalomeApp_Study::findObjectByEntry - it can't find LightApp_DataObjects
      HYDROGUI_Module* aModule = dynamic_cast<HYDROGUI_Module*>( module() );
      if( aModule )
      {
        aModule->getApp()->updateObjectBrowser();
      }
    }
  }
}

void HYDROGUI_DataModel::removeChild( SUIT_DataObject* theParent,
                                      SUIT_DataObject* theChild )
{
  SUIT_DataObject* aSubChild = theChild->firstChild();
  for( ; aSubChild; aSubChild = aSubChild->nextBrother() )
    removeChild( theChild, aSubChild );
  theParent->removeChild( theChild );
}

SUIT_DataObject* HYDROGUI_DataModel::findChildByName( const SUIT_DataObject* theFather,
                                                      const QString& theName )
{
  SUIT_DataObject* aChild = theFather->firstChild();
  while( aChild )
  {
    if( aChild->name() == theName )
      return aChild; // found
    aChild = aChild->nextBrother();
  }
  return NULL; // not found
}

bool HYDROGUI_DataModel::createNewRegion( Handle(HYDROData_CalculationCase) theCase,
                                         const QList<HYDROGUI_Zone*>& theZonesList )
{
  bool isOk = !theCase.IsNull();
  if ( isOk )
  {
    Handle(HYDROData_Region) aRegion;
    Handle(HYDROData_Zone) aZone;
    for (int i = 0; i < theZonesList.length(); i++ )
    {
      aZone = Handle(HYDROData_Zone)::DownCast( theZonesList.at(i)->modelObject() );
      if ( !aZone.IsNull() )
      {
        if ( aRegion.IsNull() )
        {
          aRegion = theCase->AddNewRegion( aZone );
          isOk = !aRegion.IsNull();
        }
        else
        {
          if ( !( aRegion->AddZone( aZone ) ) )
          {
            isOk = false;
          }
        }
      }
    }
  }
  return isOk;
}

bool HYDROGUI_DataModel::rename( Handle(HYDROData_Entity) theEntity, const QString& theName )
{
  if ( theName.isEmpty() )
    return false;

  try
  {
    getDocument()->StartOperation();
    theEntity->SetName( theName );
    getDocument()->CommitOperation( HYDROGUI_Tool::ToExtString( tr("RENAME_TO").arg( theName ) ) );
    module()->application()->activeStudy()->Modified();
  }
  catch ( Standard_Failure )
  {
    getDocument()->AbortOperation();
    return false;
  }
  return true;
}

void HYDROGUI_DataModel::updateDocument()
{
  DEBTRACE("updateDocument");
    // Sets the default strickler coefficient from preferences to document.
    Handle(HYDROData_Document) aDoc = getDocument();
    SUIT_ResourceMgr* resMgr = module()->application()->resourceMgr();
    if ( resMgr && !aDoc.IsNull() )
      aDoc->SetDefaultStricklerCoefficient( resMgr->doubleValue( "preferences", "default_strickler_coefficient", 0 ) );
}

void HYDROGUI_DataModel::setObjectVisibilityState( Handle(HYDROData_Entity) theModelObject,
                                                   HYDROGUI_DataObject* theObject )
{
  SUIT_AbstractModel* treeModel = 0;
  LightApp_Application* app = dynamic_cast<LightApp_Application*>( module()->application() );
  if ( app )
    treeModel = dynamic_cast<SUIT_AbstractModel*>( app->objectBrowser()->model() );

  if ( treeModel )
  {
    HYDROGUI_Module* aModule = dynamic_cast<HYDROGUI_Module*>( module() );
    bool isVisible = aModule->isObjectVisible( 0, theModelObject );
    Qtx::VisibilityState aVisState = isVisible ? Qtx::ShownState : Qtx::HiddenState;
    treeModel->setVisibilityState( theObject->text( theObject->customData( Qtx::IdType ).toInt() ), aVisState, false );
  }
}

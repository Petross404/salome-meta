// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ZIProgressIndicator.h"
#include "HYDROGUI_Tool.h"

#include "HYDROData_Tool.h"

#include <QLabel>
#include <QProgressBar>
#include <QVBoxLayout>
#include <QApplication>
//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROGUI_ZIProgressIndicator, Message_ProgressIndicator)

HYDROGUI_ZIProgressIndicator::HYDROGUI_ZIProgressIndicator( QWidget* theParent )
: QtxDialog( theParent, true, false, QtxDialog::Cancel ), myUserBreak(Standard_False),
  myQuadTreePosition(0)
{
  setWindowTitle( tr( "BATHYMETRY_INTERPOLATION_TLT" ) );

  QVBoxLayout* aLayout = new QVBoxLayout( mainFrame() );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  myQuadTreeBar = new QProgressBar( mainFrame() );
  myTriangulationState = new QLabel( tr( "PENDING" ), mainFrame() );
  mySummaryBar = new QProgressBar( mainFrame() );

  QString aBold("<b>%1</b>");

  aLayout->addWidget( new QLabel( aBold.arg( tr( "QUADTREE_STAGE" ) ) ) );
  aLayout->addWidget( myQuadTreeBar );
  aLayout->addSpacing( 10 );
  aLayout->addWidget( new QLabel( aBold.arg( tr( "TRIANGULATION_STAGE" ) ) ) );
  aLayout->addWidget( myTriangulationState );
  aLayout->addSpacing( 10 );
  aLayout->addWidget( new QLabel( aBold.arg( tr( "EXPLORATION_STAGE" ) ) ) );
  aLayout->addWidget( mySummaryBar );

  setButtonText( Cancel, tr( "CANCEL" ) );
  setButtonPosition( Center, Cancel );
  setMinimumWidth( 350 );
}

HYDROGUI_ZIProgressIndicator::~HYDROGUI_ZIProgressIndicator()
{
}

void HYDROGUI_ZIProgressIndicator::Show(const Message_ProgressScope& theScope, const Standard_Boolean theForce)
{
  //DEBTRACE("...");
  Standard_Boolean isUserBreak = UserBreak();
  Standard_Real aPosition = GetPosition();
  myCount++;
  if (theForce)
      DEBTRACE("aPosition=" << aPosition << " myCount:" <<myCount);

  if ( !theForce ) {
    // quadtree
    QString aName = HYDROGUI_Tool::ToQString( theScope.Name() );
    DEBTRACE("ScopeName="<< aName);
    myQuadTreePosition = theScope.GetPortion();
  } else {
    if ( !isUserBreak ) {
      myQuadTreeBar->setValue( int( myQuadTreePosition * 100 ) );
      mySummaryBar->setValue( int( aPosition * 100 ) );
      HYDROData_Tool::ExecStatus aStatus = HYDROData_Tool::GetTriangulationStatus();
      if ( aStatus == HYDROData_Tool::Running ) {
        myTriangulationState->setText( tr("IN_PROGRESS") );
      } else if ( aStatus == HYDROData_Tool::Finished ) {
        myTriangulationState->setText( tr("COMPLETED") );
      }
    }

    bool isFinished = aPosition >= 1 || ( isUserBreak );
    if ( isFinished && theForce ) { // theForce == true : call from main thread, Qt display safe
      if ( result() != Accepted ) {
        QDialog::accept();
      }
    } else if ( !isVisible() && theForce ) { // theForce == true : call from main thread, Qt display safe
      open();
    }


    QApplication::processEvents();
  }

  return;
}

Standard_Boolean HYDROGUI_ZIProgressIndicator::UserBreak()
{
  return myUserBreak;
}

void HYDROGUI_ZIProgressIndicator::Reset()
{
  Message_ProgressIndicator::Reset();
  myTriangulationState->setText( tr( "PENDING" ) );
  setButtonText( Cancel, tr("CANCEL") );
  setButtonEnabled( true, Cancel );
  myUserBreak = Standard_False;
  myCount = 0;
}

void HYDROGUI_ZIProgressIndicator::reject()
{
  myUserBreak = Standard_True;
  setButtonText( Cancel, tr("CANCELLING") );
  setButtonEnabled( false, Cancel );
  QApplication::processEvents();
}
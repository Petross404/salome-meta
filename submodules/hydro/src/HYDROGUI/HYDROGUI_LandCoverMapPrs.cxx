// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_LandCoverMapPrs.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Tool.h>
#include <AIS_DisplayMode.hxx>
#include <Prs3d_IsoAspect.hxx>
#include <SelectMgr_SequenceOfOwner.hxx>
#include <StdPrs_WFShape.hxx>
#include <StdSelect_BRepOwner.hxx>
#include <TopoDS_Face.hxx>
#include <QColor>
#include <QString>

const Quantity_Color EDGES_COLOR = Quantity_NOC_SLATEGRAY;
const int HILIGHT_ISO_NB = 10;

IMPLEMENT_STANDARD_RTTIEXT( HYDROGUI_LandCoverMapPrs, AIS_ColoredShape )

HYDROGUI_LandCoverMapPrs::HYDROGUI_LandCoverMapPrs( const Handle(HYDROData_LandCoverMap)& theMap )
  : AIS_ColoredShape( theMap->GetShape() )
{
  SetLandCoverMap( theMap );
  SetAutoHilight( Standard_False );
  SetHilightAttributes( EDGES_COLOR );
}

HYDROGUI_LandCoverMapPrs::~HYDROGUI_LandCoverMapPrs()
{
}

Handle(HYDROData_LandCoverMap) HYDROGUI_LandCoverMapPrs::GetLandCoverMap() const
{
  return myLCMap;
}

void HYDROGUI_LandCoverMapPrs::SetLandCoverMap( const Handle(HYDROData_LandCoverMap)& theMap )
{
  myLCMap = theMap;
  UpdateColors();
}

void HYDROGUI_LandCoverMapPrs::UpdateColors()
{
  SetMaterial( Graphic3d_NOM_PLASTIC );
  if( myLCMap.IsNull() )
  {
    Set( TopoDS_Shape() );
    return;
  }
  
  Set( myLCMap->GetShape() );
  HYDROData_LandCoverMap::Explorer anIt( myLCMap );
  for( ; anIt.More(); anIt.Next() )
  {
    TopoDS_Face aFace = anIt.Face();
    QString aStricklerType = anIt.StricklerType();
    Quantity_Color aColor = GetColor( aStricklerType );
    SetCustomColor( aFace, aColor );
    SetCustomWidth( aFace, 1.0 );
  } 
  SetTransparency( myLCMap->GetTransparency() );
}

Handle(AIS_ColorScale) HYDROGUI_LandCoverMapPrs::GetColorScale() const
{
  return myColorScale;
}

void HYDROGUI_LandCoverMapPrs::SetColorScale( const Handle(AIS_ColorScale)& theColorScale )
{
  myColorScale = theColorScale;
  double aMin = 0, aMax = 0;
  if( myTable.IsNull() )
  {
    //TODO: go through all Strickler tables in the document to get the global range
  }
  else
    myTable->GetCoefficientRange( aMin, aMax );

  myColorScale->SetRange( aMin, aMax );
  UpdateColors();
}

Handle(HYDROData_StricklerTable) HYDROGUI_LandCoverMapPrs::GetTable() const
{
  return myTable;
}

void HYDROGUI_LandCoverMapPrs::SetTable( const Handle(HYDROData_StricklerTable)& theTable )
{
  myTable = theTable;
}

Quantity_Color HYDROGUI_LandCoverMapPrs::GetColor( const QString& theStricklerType ) const
{
  Quantity_Color aResult;

  if( !myColorScale.IsNull() && !myTable.IsNull() )
  {
    double aCoeff = myTable->Get( theStricklerType, 0.0 );
    Standard_Boolean isOK = myColorScale->FindColor( aCoeff, aResult );
    if( isOK )
      return aResult;
    else
      return Quantity_Color();
  }

  QColor aColor = HYDROData_Document::Document()->GetAssociatedColor(theStricklerType, myTable);

  if (aColor.isValid())
    return HYDROData_Tool::toOccColor(aColor);
  else
    return Quantity_Color();
}

void HYDROGUI_LandCoverMapPrs::Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                                        const Handle(Prs3d_Presentation)& thePresentation,
                                        const Standard_Integer theMode )
{
  thePresentation->Clear();

  myDrawer->UIsoAspect()->SetNumber( 0 );
  myDrawer->VIsoAspect()->SetNumber( 0 );
  myDrawer->LineAspect()->SetColor( EDGES_COLOR );
  myDrawer->FaceBoundaryAspect()->SetColor( EDGES_COLOR );
  myDrawer->FreeBoundaryAspect()->SetColor( EDGES_COLOR );

  switch( theMode )
  {
  case AIS_WireFrame:
  case AIS_Shaded:
    AIS_ColoredShape::Compute( thePresentationManager, thePresentation, theMode );
    break;
  }

  if( theMode==AIS_Shaded )
    StdPrs_WFShape::Add( thePresentation, Shape(), myDrawer );
}

void HYDROGUI_LandCoverMapPrs::HilightSelected( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                                                const SelectMgr_SequenceOfOwner& theOwners )
{
  Handle(Prs3d_Presentation) aSelectPrs = GetSelectPresentation( thePresentationManager );

  SetHilightAttributes( EDGES_COLOR );

  for( int i=1, n=theOwners.Length(); i<=n; i++ )
  {
    Handle(StdSelect_BRepOwner) anOwner = Handle(StdSelect_BRepOwner)::DownCast( theOwners.Value( i ) );
    if( !anOwner.IsNull() )
      StdPrs_WFShape::Add( aSelectPrs, anOwner->Shape(), HilightAttributes() );
  }

  HilightAttributes()->UIsoAspect()->SetNumber( 0 );
  HilightAttributes()->VIsoAspect()->SetNumber( 0 );

  aSelectPrs->SetDisplayPriority( 9 );
  aSelectPrs->Display();
}

void HYDROGUI_LandCoverMapPrs::SetHilightAttributes( const Quantity_Color& theEdgesColor )
{
  if (HilightAttributes().IsNull())
    AIS_ColoredShape::SetHilightAttributes(new Prs3d_Drawer());
  HilightAttributes()->UIsoAspect()->SetNumber( HILIGHT_ISO_NB );
  HilightAttributes()->UIsoAspect()->SetColor( theEdgesColor );
  HilightAttributes()->VIsoAspect()->SetNumber( HILIGHT_ISO_NB );
  HilightAttributes()->VIsoAspect()->SetColor( theEdgesColor );
  HilightAttributes()->LineAspect()->SetColor( theEdgesColor );
  HilightAttributes()->FaceBoundaryAspect()->SetColor( theEdgesColor );
  HilightAttributes()->FreeBoundaryAspect()->SetColor( theEdgesColor );
}

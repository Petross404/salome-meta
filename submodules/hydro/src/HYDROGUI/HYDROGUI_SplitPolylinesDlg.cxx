// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_SplitPolylinesDlg.h>
#include <HYDROGUI_ObjComboBox.h>
#include <HYDROGUI_ObjListBox.h>
#include <HYDROGUI_Tool2.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>
#include <CurveCreator_Utils.hxx>
#include <SUIT_ViewWindow.h>
#include <QtxDoubleSpinBox.h>
#include <QFrame>
#include <QLabel>
#include <QGridLayout>
#include <QTabWidget>
#include <QMouseEvent>
#include <gp_Pnt2d.hxx>

const double MIN_COORD = -1000000;
const double MAX_COORD =  1000000;
const double STEP      =  10;

HYDROGUI_SplitPolylinesDlg::HYDROGUI_SplitPolylinesDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ), myOCCViewer( 0 )
{
  QFrame* aFrame = new QFrame( mainFrame() );
  addWidget( aFrame, 1 );

  QGridLayout* aLayout = new QGridLayout( aFrame );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  myTab = new QTabWidget( aFrame );
  aLayout->addWidget( myTab, 1, 0, 1, 2 );

  QFrame* aPointPage = new QFrame();
  myMainPolyline1 = new HYDROGUI_ObjComboBox( theModule, tr( "POLYLINE" ), KIND_POLYLINEXY, aPointPage );
  myX = new QtxDoubleSpinBox( aPointPage );
  myX->setRange( MIN_COORD, MAX_COORD );
  myX->setSingleStep( STEP );
  myY = new QtxDoubleSpinBox( aPointPage );
  myY->setRange( MIN_COORD, MAX_COORD );
  myY->setSingleStep( STEP );
  
  QGridLayout* aPointPageLayout = new QGridLayout( aPointPage );
  aPointPageLayout->addWidget( myMainPolyline1, 0, 0, 1, 3 );
  aPointPageLayout->addWidget( new QLabel( tr( "POINT" ) ), 1, 0 );
  aPointPageLayout->addWidget( myX, 1, 1 );
  aPointPageLayout->addWidget( myY, 1, 2 );
  aPointPageLayout->setColumnStretch( 1, 1 );
  aPointPageLayout->setColumnStretch( 2, 1 );
  aPointPageLayout->setRowStretch( 2, 1 );
  myTab->addTab( aPointPage, tr( "BY_POINT" ) );

  QFrame* aToolPage = new QFrame();
  myMainPolyline2 = new HYDROGUI_ObjComboBox( theModule, tr( "POLYLINE" ), KIND_POLYLINEXY, aToolPage );
  myToolPolyline = new HYDROGUI_ObjComboBox( theModule, tr( "TOOL_POLYLINE" ), KIND_POLYLINEXY, aToolPage );

  QGridLayout* aToolPageLayout = new QGridLayout( aToolPage );
  aToolPageLayout->addWidget( myMainPolyline2, 0, 0 );
  aToolPageLayout->addWidget( myToolPolyline, 1, 0 );
  aToolPageLayout->setRowStretch( 2, 1 );
  myTab->addTab( aToolPage, tr( "BY_TOOL" ) );

  QFrame* aSplitPage = new QFrame();
  myPolylines = new HYDROGUI_ObjListBox( theModule, tr( "POLYLINES" ), KIND_POLYLINEXY, aSplitPage );
  QGridLayout* aSplitPageLayout = new QGridLayout( aSplitPage );
  aSplitPageLayout->addWidget( myPolylines, 0, 0 );
  myTab->addTab( aSplitPage, tr( "COMPLETE_SPLIT" ) );

  connect( myX, SIGNAL( valueChanged( double ) ), this, SIGNAL( pointMoved() ) );
  connect( myY, SIGNAL( valueChanged( double ) ), this, SIGNAL( pointMoved() ) );
  connect( myTab, SIGNAL( currentChanged( int ) ), this, SIGNAL( modeChanged() ) );
  connect( myMainPolyline1, SIGNAL( objectSelected( const QString& ) ), this, SIGNAL( selectionChanged() ) );
  connect( myMainPolyline2, SIGNAL( objectSelected( const QString& ) ), this, SIGNAL( selectionChanged() ) );
  connect( myToolPolyline, SIGNAL( objectSelected( const QString& ) ), this, SIGNAL( selectionChanged() ) );
  connect( myPolylines, SIGNAL( selectionChanged() ), this, SIGNAL( selectionChanged() ) );
}

HYDROGUI_SplitPolylinesDlg::~HYDROGUI_SplitPolylinesDlg()
{
}

HYDROGUI_SplitPolylinesDlg::Mode HYDROGUI_SplitPolylinesDlg::GetMode() const
{
  return ( Mode )myTab->currentIndex();
}

Handle( HYDROData_PolylineXY ) HYDROGUI_SplitPolylinesDlg::GetMainPolyline() const
{
  switch( GetMode() )
  {
  case ByPoint:
    if( !myMainPolyline1->GetObject().IsNull() )
      return Handle( HYDROData_PolylineXY )::DownCast( myMainPolyline1->GetObject() );
  case ByTool:
    if( !myMainPolyline2->GetObject().IsNull() )
      return Handle( HYDROData_PolylineXY )::DownCast( myMainPolyline2->GetObject() );
  default:
    return Handle( HYDROData_PolylineXY )();
  }
  return Handle( HYDROData_PolylineXY )();
}

Handle( HYDROData_PolylineXY ) HYDROGUI_SplitPolylinesDlg::GetToolPolyline() const
{
  if( GetMode()==ByTool )
    return Handle( HYDROData_PolylineXY )::DownCast( myToolPolyline->GetObject() );
  else
    return Handle( HYDROData_PolylineXY )();
}

gp_Pnt2d HYDROGUI_SplitPolylinesDlg::GetPoint() const
{
  return gp_Pnt2d( myX->value(), myY->value() );
}

HYDROData_SequenceOfObjects HYDROGUI_SplitPolylinesDlg::GetPolylines() const
{
  if( GetMode()==Split )
    return myPolylines->selectedObjects();
  else
    return HYDROData_SequenceOfObjects();
}

void HYDROGUI_SplitPolylinesDlg::setPolylinesFromSelection()
{
  myMainPolyline1->reset();
  myMainPolyline2->reset();
  myToolPolyline->reset();
  myPolylines->reset();

  Handle( HYDROData_Entity ) anObject = HYDROGUI_Tool::GetSelectedObject( module() );
  if( anObject.IsNull() || anObject->GetKind() != KIND_POLYLINEXY )
    return;

  QString aName = anObject->GetName();
  myMainPolyline1->setObjectName( aName );
  myMainPolyline2->setObjectName( aName );
  myPolylines->setObjectsFromSelection();
}

void HYDROGUI_SplitPolylinesDlg::setOCCViewer( OCCViewer_Viewer* theViewer )
{
  if( !theViewer )
    return;
  
  myOCCViewer = theViewer;
  OCCViewer_ViewManager* aViewManager = dynamic_cast<OCCViewer_ViewManager*>( myOCCViewer->getViewManager() );
  disconnect( aViewManager, SIGNAL( mousePress( SUIT_ViewWindow*, QMouseEvent* ) ),
              this, SLOT( onMousePress( SUIT_ViewWindow*, QMouseEvent* ) ) );
  connect( aViewManager, SIGNAL( mousePress( SUIT_ViewWindow*, QMouseEvent* ) ),
           this, SLOT( onMousePress( SUIT_ViewWindow*, QMouseEvent* ) ) );
}

void HYDROGUI_SplitPolylinesDlg::onMousePress( SUIT_ViewWindow* theWindow, QMouseEvent* theEvent )
{
  gp_Pnt aPnt = CurveCreator_Utils::ConvertClickToPoint( theEvent->x(), theEvent->y(), getViewPort()->getView() );
  myX->setValue( aPnt.X() );
  myY->setValue( aPnt.Y() );
}

OCCViewer_ViewPort3d* HYDROGUI_SplitPolylinesDlg::getViewPort() const
{
  OCCViewer_ViewPort3d* aViewPort = 0;
  if ( myOCCViewer )
    aViewPort = dynamic_cast<OCCViewer_ViewWindow*>( myOCCViewer->getViewManager()->getActiveView() )->getViewPort();
    
  return aViewPort;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_IMPORTGEOMOBJECTOP_H
#define HYDROGUI_IMPORTGEOMOBJECTOP_H

#include "HYDROGUI_Operation.h"

#include <QStringList>

// IDL includes
#include <SALOMEconfig.h>
#include CORBA_SERVER_HEADER(GEOM_Gen)

class HYDROGUI_InputPanel;

class HYDROGUI_ImportGeomObjectOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  enum OperationType { ImportCreatedAsObstacle, 
                       ImportSelectedAsObstacle,
                       ImportSelectedAsPolyline };

public:
  HYDROGUI_ImportGeomObjectOp( HYDROGUI_Module* theModule,
                               const int theOpType,
                               const int theGEOMOp = -1 );
  virtual ~HYDROGUI_ImportGeomObjectOp();

  static QList<GEOM::shape_type> getObstacleTypes();
  static QList<GEOM::shape_type> getPolylineTypes();

protected:
  virtual void               startOperation();
  virtual void               abortOperation();
  virtual void               commitOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool               processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries );

protected slots:
  void                       onExternalOperationFinished( const QString&, const QString&, 
                                                          const QStringList& );

private:
  void updateDefaultName();
  void closeExternalOperationDlg();

private:
  QStringList myGeomObjects; ///< the list of GEOM object entries
  bool myIsToShowPanel; ///< indicates if obstacle panel to be shown
  int  myOpType; ///< operation type (to import selected GEOM objects or the objects just created by GEOM )
  int  myGEOMOp; ///< GEOM module operation to be called to create GEOM object for import
  QString myGEOMOpName; ///< the name of the called GEOM module operation
};

#endif
// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_OPERATION_H
#define HYDROGUI_OPERATION_H

#include <LightApp_Operation.h>
#include <Standard_DefineHandle.hxx>
#include <QCursor>

class HYDROGUI_Module;
class HYDROGUI_InputPanel;
class HYDROGUI_Shape;

class SUIT_SelectionMgr;
class OCCViewer_ViewManager;

class HYDROData_Document;
class HYDROData_Object;

class HYDROGUI_Operation : public LightApp_Operation
{
  Q_OBJECT

public:

  HYDROGUI_Operation( HYDROGUI_Module* theModule );
  virtual ~HYDROGUI_Operation();

  static bool                         myIsClear;

public:

  void                                setName( const QString& theName );
  const QString&                      getName() const;

  HYDROGUI_InputPanel*                inputPanel() const;
  SUIT_SelectionMgr*                  selectionMgr() const;
  HYDROGUI_Module*                    module() const;

  int                                 getPreviewZLayer() const;
  virtual void                        updatePreviewZLayer( int theLayer );

signals:
  void                                helpContextModule( const QString&,
                                                         const QString&,
                                                         const QString& );

protected:
  virtual void                        startOperation();
  virtual void                        abortOperation();
  virtual void                        commitOperation();
  virtual void                        stopOperation();
  virtual void                        setDialogActive( const bool );

  virtual HYDROGUI_InputPanel*        createInputPanel() const;
  virtual void                        closeInputPanel();

  virtual bool                        processApply( int& theUpdateFlags, QString& theErrorMsg,
                                                    QStringList& theBrowseObjectsEntries );
  virtual void                        processCancel();

  void                                startDocOperation();
  void                                abortDocOperation();
  void                                commitDocOperation();

  Handle(HYDROData_Document)          doc() const;

  virtual bool                        isToAbortOnApply() const { return true; }

  void                                printErrorMessage( const QString& theErrorMsg );
  void                                setPrintErrorMessage( const bool theIsPrint );

  void                                browseObjects( const QStringList& theBrowseObjectsEntries,
                                                     const bool theIsApplyAndClose = true );

protected slots:

  virtual void                        onApplyAndClose();
  virtual void                        onApply();
  virtual void                        onCancel();
  virtual void                        onHelp();

protected:

  QString                             getHelpComponent() const;
  QString                             getHelpFile() const;
  QString                             getHelpContext() const;

  virtual void                        setPreviewZLayer( int theLayer );
  virtual HYDROGUI_Shape*             getPreviewShape() const;
  OCCViewer_ViewManager*              getPreviewManager();
  void                                setPreviewManager( OCCViewer_ViewManager* theManager );

  /**
   * Set specific cursor chosen in preferences for edition operations.
   */
  virtual void                        setCursor();
  /**
   * Restore the default cursor.
   */
  virtual void                        restoreCursor();

  /**
   * Set flag indicating, if 'Apply and Close' button is clicked on input panel.
   * @param theFlag a flag value to be set
   */
  virtual void                        setIsApplyAndClose( const bool theFlag );
  /**
   * Get flag indicating, if 'Apply and Close' button is clicked on input panel.
   * \return a flag value
   */
  virtual bool                        isApplyAndClose() const;

  /**
   * Apply changes done by this operation.
   */
  virtual void                        apply();

  virtual void                        restoreOCCViewerSelection(OCCViewer_ViewManager* theViewManager = 0);

private:

  HYDROGUI_Module*                    myModule;
  HYDROGUI_InputPanel*                myPanel;
  OCCViewer_ViewManager*              myPreviewManager;
  QString                             myName;
  bool                                myIsPrintErrorMessage;
  bool                                myIsTransactionOpened;
  int                                 myPreviewZLayer;

  QCursor                             myCursor;

  bool                                myIsApplyAndClose;
};

#endif

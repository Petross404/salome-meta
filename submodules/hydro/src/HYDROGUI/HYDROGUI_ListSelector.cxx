// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ListSelector.h"

#include "HYDROGUI_OrderedListWidget.h"

#ifdef TEST_MODE
  #include <SUIT_DataOwner.h>
  class TestOwner : public SUIT_DataOwner
  {
  public:
    TestOwner( const QString& entry ) { myEntry = entry; }
    virtual ~TestOwner() {}

    QString keyString() const { return myEntry; }
    QString entry() const { return myEntry; }

  private:
    QString myEntry;
  };
  #define OWNER_CLASS TestOwner

#else
  #include <LightApp_DataOwner.h>
  #define OWNER_CLASS LightApp_DataOwner
#endif


HYDROGUI_ListSelector::HYDROGUI_ListSelector( HYDROGUI_OrderedListWidget* theListWidget,
                                              SUIT_SelectionMgr* theSelectionMgr )
: SUIT_Selector( theSelectionMgr, theListWidget ),
  myListWidget( theListWidget )
{
  if ( myListWidget ) {
    connect( myListWidget, SIGNAL( selectionChanged() ), this, SLOT( onSelectionChanged() ) );
  }
}

/**
  Destructor.
*/
HYDROGUI_ListSelector::~HYDROGUI_ListSelector()
{
}

/*!
  Get selector type.
  @return the selector type
*/
QString HYDROGUI_ListSelector::type() const
{ 
  return "ListSelector"; 
}

/**
*/
void HYDROGUI_ListSelector::getSelection( SUIT_DataOwnerPtrList& theList ) const
{
  QStringList aSelectedEntries = myListWidget->getSelectedEntries();

  foreach ( const QString& anEntry, aSelectedEntries ) {
    if ( !anEntry.isEmpty() ) {
      theList.append( SUIT_DataOwnerPtr( new OWNER_CLASS( anEntry ) ) );
    }
  }
}

/**
*/
void HYDROGUI_ListSelector::setSelection( const SUIT_DataOwnerPtrList& theList )
{
  if ( !myListWidget ) {
    return;
  }

  QStringList aSelectedEntries;
  SUIT_DataOwnerPtrList::const_iterator anIt = theList.begin();
  for ( ; anIt != theList.end(); ++anIt ) {
    const OWNER_CLASS* anOwner = dynamic_cast<const OWNER_CLASS*>( (*anIt).operator->() );
    if ( anOwner ) {
      aSelectedEntries << anOwner->entry();
    }
  }
 
  myListWidget->setSelectedEntries( aSelectedEntries );
}

/**
  Called when the list selection is changed.
*/
void HYDROGUI_ListSelector::onSelectionChanged()
{
  selectionChanged();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_StreamOp.h"

#include "HYDROGUI_Module.h"
#include <HYDROGUI_DataObject.h>
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_StreamDlg.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_OCCDisplayer.h"

#include <HYDROData_Document.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Profile.h>
#include <HYDROData_DTM.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>
#include <QColor>
#include <QApplication>


#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>
#include <SVTK_ViewManager.h>
#include <SVTK_ViewModel.h>
#include <gp_Ax1.hxx>
#include <gp_Ax2.hxx>
#include <gp_Ax3.hxx>
#include <gp_Vec.hxx>
#include <gp_Pnt.hxx>
#include <gp_Pln.hxx>
#include <gp.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <QSet>
#include <HYDROData_StreamLinearInterpolation.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

void insertProfileInToOrder( const QString& theProfileName,
                             const double&  theProfilePar,
                             QStringList&   theProfiles,
                             QList<double>& theProfileParams )
{
  DEBTRACE("insertProfileInToOrder");
  bool anIsInserted = false;
  for ( int k = 0; k < theProfileParams.length(); ++k )
  {
    const double& aParam = theProfileParams.value( k );
    if ( theProfilePar < aParam )
    {
      theProfiles.insert( k, theProfileName );
      theProfileParams.insert( k, theProfilePar );
      anIsInserted = true;
      break;
    }
  }
  
  if ( !anIsInserted )
  {
    theProfiles << theProfileName;
    theProfileParams << theProfilePar;
  }
}

void HYDROGUI_StreamOp::hideBathy(HYDROGUI_Module *theModule)
{
    DEBTRACE("HYDROGUI_StreamOp::hideBathy");
    Handle(HYDROData_Stream) editedObject =
    Handle(HYDROData_Stream)::DownCast(HYDROGUI_Tool::GetSelectedObject(theModule));
    if (!editedObject.IsNull())
    {
        QString bathyName = editedObject->GetBathyName();
        DEBTRACE("bathyName " << bathyName.toStdString());
        if (!bathyName.isEmpty())
        {
            Handle(HYDROData_Entity) aBathyObject = HYDROGUI_Tool::FindObjectByName(theModule, bathyName);
            if (!aBathyObject.IsNull())
            {
                DEBTRACE("Hide " << bathyName.toStdString());
                int anUpdateFlags = UF_OCCViewer;
                size_t aViewerId = HYDROGUI_Tool::GetActiveOCCViewId(theModule);
                theModule->setObjectVisible(aViewerId, aBathyObject, false);
                theModule->update( anUpdateFlags );
            }
        }
    }
}

HYDROGUI_StreamOp::HYDROGUI_StreamOp(HYDROGUI_Module *theModule, bool theIsEdit) :
        HYDROGUI_Operation(theModule), myIsEdit(theIsEdit), myPreviewPrs( NULL)
{
    DEBTRACE("HYDROGUI_StreamOp");
    setName(theIsEdit ? tr("EDIT_STREAM") : tr("CREATE_STREAM"));
}

HYDROGUI_StreamOp::~HYDROGUI_StreamOp()
{
  DEBTRACE("~HYDROGUI_StreamOp");
  erasePreview();
}

void HYDROGUI_StreamOp::startOperation()
{
  DEBTRACE("startOperation");
  HYDROGUI_Operation::startOperation();
  int mode = 0; //DTM mode by def

  if ( !myIsEdit || isApplyAndClose() )
    myEditedObject.Nullify();
  myHydAxis.clear();
  myProfiles.clear();
  myProfileParams.clear();

  // Get panel and reset its state
  HYDROGUI_StreamDlg* aPanel = (HYDROGUI_StreamDlg*)inputPanel();
  aPanel->reset();

  QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_STREAM_NAME" ) );
  if ( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject =
        Handle(HYDROData_Stream)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if ( !myEditedObject.IsNull() )
    {          
      mode = myEditedObject->GetInterpolationMethod();
      anObjectName = myEditedObject->GetName();
      DEBTRACE("anObjectName " << anObjectName.toStdString());
      //aPanel->setMode(mode);

      // Hydraulic axis
      Handle(HYDROData_PolylineXY) aHydraulicAxis = myEditedObject->GetHydraulicAxis();
      
      TopoDS_Face aPlane;
      HYDROData_Stream::BuildRefFace( aPlane ) ;
      if ( !aHydraulicAxis.IsNull() )
        myHydAxis = aHydraulicAxis->GetName();

      // Stream profiles
      HYDROData_SequenceOfObjects aStreamProfiles = myEditedObject->GetProfiles();
      for ( int i = 1, n = aStreamProfiles.Length(); i <= n; ++i )
      {
        Handle(HYDROData_Profile) aProfile = 
          Handle(HYDROData_Profile)::DownCast( aStreamProfiles.Value( i ) );
        if ( aProfile.IsNull() )
          continue;

        QString aProfileName = aProfile->GetName();        
        myProfiles      << aProfileName;

        if (!aHydraulicAxis.IsNull())
        {
          Standard_Real aProfilePar = 0.0;
          HYDROData_Stream::HasIntersection( aHydraulicAxis, aProfile, aPlane, aProfilePar );
          myProfileParams << aProfilePar;
        }
      }      
    }
  }

  // Update the panel
  // set the edited object name
  aPanel->setObjectName( anObjectName );

  // set the existing 2D polylines names to the panel
  aPanel->setAxisNames( HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_POLYLINEXY ) );

  aPanel->setLeftBankNames( HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_POLYLINEXY ) );
  aPanel->setRightBankNames( HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_POLYLINEXY ) );

  bool isBlocked = aPanel->blockSignals( true );
  if (myIsEdit)
  {
    if (mode == 0)
    {
      aPanel->setDDZ( myEditedObject->GetDDZ() );
    }
    else
    {
      aPanel->setNbProfilePoints( myEditedObject->GetNbProfilePoints() );
      aPanel->setLeftBankName(myEditedObject->GetLeftBank()->GetName());
      aPanel->setRightBankName(myEditedObject->GetRightBank()->GetName());
    }
    aPanel->setSpatialStep( myEditedObject->GetSpatialStep() );
  }  
  
  aPanel->setMode(mode);
  aPanel->blockSignals( isBlocked );

  // synchronize the panel state with the edited object state
  updatePanelData();
  // Create preview
  createPreview();
}

void HYDROGUI_StreamOp::abortOperation()
{
  DEBTRACE("abortOperation");
  erasePreview();
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_StreamOp::commitOperation()
{
  DEBTRACE("commitOperation");
  erasePreview();
  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_StreamOp::createInputPanel() const
{
  DEBTRACE("createInputPanel");
  HYDROGUI_StreamDlg* aPanel = new HYDROGUI_StreamDlg( module(), getName() );

  connect( aPanel, SIGNAL( AddProfiles() ), this, SLOT( onAddProfiles() ) );
  connect( aPanel, SIGNAL( RemoveProfiles( const QStringList& ) ), 
           this, SLOT( onRemoveProfiles( const QStringList& ) ) );
  connect( aPanel, SIGNAL( AxisChanged( const QString& ) ), 
           this, SLOT( onAxisChanged( const QString& ) ) );

  connect( aPanel, SIGNAL( DDZValueChanged( double ) ),  this, SLOT( onDDZValueChanged( double ) ) );
  connect( aPanel, SIGNAL( SSValueChanged( double ) ),  this, SLOT( onSSValueChanged( double ) ) );

  connect( aPanel, SIGNAL( LeftBankChanged( const QString& ) ), 
    this, SLOT( onLeftBankChanged( const QString& ) ) );

  connect( aPanel, SIGNAL( RightBankChanged( const QString& ) ), 
    this, SLOT( onRightBankChanged( const QString& ) ) );

  connect( aPanel, SIGNAL( ProfilePointsChanged( int ) ),  this, SLOT( onProfilePointsChanged( int ) ) );

  connect( aPanel, SIGNAL( ModeChanged( bool ) ), this, SLOT( onModeChanged( bool ) ) );

  return aPanel;
}

void HYDROGUI_StreamOp::apply()
{
  DEBTRACE("apply");
  QApplication::setOverrideCursor( Qt::WaitCursor );
  if (myEditedObject)
  {
      QString anObjectName = myEditedObject->GetName();
      DEBTRACE("anObjectName " << anObjectName.toStdString());
  }

  startDocOperation();

  int anUpdateFlags = 0;
  QString anErrorMsg;

  bool aResult = false;
  QStringList aBrowseObjectsEntries;

  try
  {
    aResult = processApply( anUpdateFlags, anErrorMsg, aBrowseObjectsEntries );
  }
  catch ( Standard_Failure& e )
  {
    anErrorMsg = e.GetMessageString();
    aResult = false;
  }
  catch ( ... )
  {
    aResult = false;
  }
  
  QApplication::restoreOverrideCursor();

  if ( aResult )
  {
    module()->update( anUpdateFlags );
    commitDocOperation();
    commit();
    browseObjects( aBrowseObjectsEntries );
  }
  else
  {
    myEditedObject->Remove();
    module()->setObjectRemoved( myEditedObject );  

    if ( isToAbortOnApply() )
      abortDocOperation();

    abort();
    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "CREATE_STREAM_ERROR" ),
                               anErrorMsg ); 

  } 
}
//#include <HYDROData_StreamLinearInterpolation.h>
bool HYDROGUI_StreamOp::processApply( int& theUpdateFlags,
                                      QString& theErrorMsg,
                                      QStringList& theBrowseObjectsEntries )
{
  DEBTRACE("processApply");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  // Check whether the object name is not empty
  QString anObjectName = aPanel->getObjectName().simplified();
  if ( anObjectName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  // Check that there are no other objects with the same name in the document
  if ( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anObjectName ) )
  {
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObjectName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObjectName );
      return false;
    }
  }

  if ( myEditedObject.IsNull() ) // Create new data model object
    myEditedObject = Handle(HYDROData_Stream)::DownCast( doc()->CreateObject( KIND_STREAM ) );

  Handle(HYDROData_PolylineXY) aHydAxis = Handle(HYDROData_PolylineXY)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), myHydAxis, KIND_POLYLINEXY ) );
 //if ( aHydAxis.IsNull() )
 //{
 //  theErrorMsg = tr( "AXIS_NOT_DEFINED" );
 //  return false;
 //}

  // Check if at least 2 profiles is set
  HYDROData_SequenceOfObjects aRefProfiles;
  for ( int i = 0; i < myProfiles.length(); ++i )
  {
    QString aProfileName = myProfiles.value( i );

    Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aProfileName, KIND_PROFILE ) );
    if ( !aProfile.IsNull() )
      aRefProfiles.Append( aProfile );
  }

  if ( aRefProfiles.Length() < 2 )
  {
    theErrorMsg = tr( "PROFILES_NOT_DEFINED" );
    return false;
  }

  int InterpMethod = aPanel->getMode();

  myEditedObject->SetInterpolationMethod(InterpMethod);

  if (InterpMethod == 0)
  {
    //DTM
    bool ToOrder = !aHydAxis.IsNull();
    myEditedObject->SetProfiles (aRefProfiles, ToOrder) ;
    myEditedObject->SetName( anObjectName );

    if (!aHydAxis.IsNull())
      myEditedObject->SetHydraulicAxis( aHydAxis );
    myEditedObject->SetProfiles( aRefProfiles, false );
    myEditedObject->SetDDZ( aPanel->getDDZ() );
    myEditedObject->SetSpatialStep( aPanel->getSpatialStep() );
  }
  else if (InterpMethod == 1)
  {
    //LISM
    myEditedObject->SetProfiles (aRefProfiles, false) ;
    myEditedObject->SetName( anObjectName );
    if (!aHydAxis.IsNull())
      myEditedObject->SetHydraulicAxis( aHydAxis );
    Handle(HYDROData_PolylineXY) aLeftBank = Handle(HYDROData_PolylineXY)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aPanel->getLeftBankName(), KIND_POLYLINEXY ) );
    Handle(HYDROData_PolylineXY) aRightBank = Handle(HYDROData_PolylineXY)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aPanel->getRightBankName(), KIND_POLYLINEXY ) );
    myEditedObject->SetLeftBank(aLeftBank);
    myEditedObject->SetRightBank(aRightBank);

    myEditedObject->SetNbProfilePoints( aPanel->getNbProfilePoints() );
    myEditedObject->SetHaxStep( aPanel->getSpatialStep() );
  }

  if ( myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) )
  {
    myEditedObject->Update();
    //NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>> warnings;
    //myEditedObject->GetWarnings(warnings);
    //QString totalWarning;
   //for (NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>::Iterator it(warnings); it.More(); it.Next())
   //{
   //  const QSet<QString>& V = it.Value();
   //  if (V.empty())
   //    continue;
   //  const Handle(HYDROData_Profile)& K = it.Key();
   //  QString ProfName = K->GetName();
   //  foreach (QString str, V)
   //  {
   //    totalWarning + "Profile: " + ProfName + ":  " + str + "\n";    
   //  }
   //}
   //if (totalWarning != "")
   //  SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "WARNING" ), totalWarning);
  }

  if ( !myIsEdit )
  {
    myEditedObject->SetFillingColor( myEditedObject->DefaultFillingColor() );
    myEditedObject->SetBorderColor( myEditedObject->DefaultBorderColor() );
  }

  // Erase preview
  erasePreview();

  // Show the object in case of creation mode of the operation
  if( !myIsEdit ) {
    module()->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( module() ), myEditedObject, true );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( myEditedObject );
    theBrowseObjectsEntries.append( anEntry );
  }

  module()->setIsToUpdate( myEditedObject );

  // Set update flags
  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  return true;
}

void HYDROGUI_StreamOp::createPreview()
{
  DEBTRACE("createPreview");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  int mode = aPanel->getMode();

  LightApp_Application* anApp = module()->getApp();
  if ( !getPreviewManager() )
  {
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
      anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  }

  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager && !myPreviewPrs )
  {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
      {
        myPreviewPrs = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );

        QColor aFillingColor = Qt::green;
        QColor aBorderColor = Qt::transparent;
        if ( !myEditedObject.IsNull() )
        {
          aFillingColor = myEditedObject->GetFillingColor();
          aBorderColor = myEditedObject->GetBorderColor();
        }

        myPreviewPrs->setFillingColor( aFillingColor, false, false );
        myPreviewPrs->setBorderColor( aBorderColor, false, false );
      }
    }
  }

  if ( !aViewManager || !myPreviewPrs )
    return;

  if (mode == 0)
  {
    Handle(HYDROData_PolylineXY) aHydAxis = Handle(HYDROData_PolylineXY)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), myHydAxis, KIND_POLYLINEXY ) );

    HYDROData_SequenceOfObjects aRefProfiles;
    //std::vector<Handle(HYDROData_Profile)> aRefProfiles;
    int plen = myProfiles.length();
    for ( int i = 0; i < plen; ++i )
    {
      QString aProfileName = myProfiles.value( i );

      Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast(
        HYDROGUI_Tool::FindObjectByName( module(), aProfileName, KIND_PROFILE ) );
      if ( !aProfile.IsNull() )
        aRefProfiles.Append( aProfile );
    }

    HYDROData_Stream::PrsDefinition aPrsDef;

    TopoDS_Shape Out3dPres;
    TopoDS_Shape Out2dPres;
    TopoDS_Shape OutLeftB;
    TopoDS_Shape OutRightB;
    TopoDS_Shape OutInlet;
    TopoDS_Shape OutOutlet;

    double ddz = aPanel->getDDZ();
    double ss = aPanel->getSpatialStep();

    std::set<int> InvInd;

#ifdef _DEBUG
    const int MAX_POINTS_IN_PREVIEW = 50000;
#else
    const int MAX_POINTS_IN_PREVIEW = 500000;
#endif

    HYDROData_Bathymetry::AltitudePoints points;

    bool ProjStat = true;
    NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>> warnings;
    bool ToEstimateWarnings = false;
    HYDROData_DTM::CreateProfilesFromDTM( aRefProfiles, ddz, ss, points, Out3dPres, Out2dPres, OutLeftB, OutRightB,
      OutInlet, OutOutlet, true, true, InvInd, MAX_POINTS_IN_PREVIEW, ProjStat, warnings, ToEstimateWarnings );

    aPanel->clearAllBackgroundColorsForProfileList();
    // for (std::set<int>::const_iterator it = InvInd.begin(); it != InvInd.end(); it++)
    //   aPanel->setBackgroundColorForProfileList(*it, QColor(Qt::yellow));

    aPrsDef.myInlet = OutInlet;
    aPrsDef.myOutlet = OutOutlet;
    aPrsDef.myLeftBank = OutLeftB;
    aPrsDef.myRightBank = OutRightB;
    if (ProjStat)
      aPrsDef.myPrs2D = Out2dPres;
    aPrsDef.myPrs3D = Out3dPres;

    aPanel->clearWarnings();
    if (!ProjStat)
      aPanel->addWarning(tr("STREAM_PROJECTION_FAILED"));

    QVector<QString> warningsStr;

    for (NCollection_DataMap<Handle(HYDROData_Profile), QSet<QString>>::Iterator it(warnings); it.More(); it.Next())
    {
      QSet<QString> V = it.Value();
      if (V.empty())
        continue;   
      const Handle(HYDROData_Profile)& K = it.Key();
      QString ProfName = K->GetName();
      foreach (QString str, V)
      {
        warningsStr.append(ProfName + ":  " + str);
        //aPanel->addWarning(ProfName + ":  " + str);    
      }

      aPanel->setBackgroundColorForProfileList(ProfName, QColor(Qt::yellow));
    }

    std::sort(warningsStr.begin(), warningsStr.end());
    for (int k=0;k<warningsStr.size();k++)
      aPanel->addWarning(warningsStr[k]);

    myPreviewPrs->setShape( aPrsDef.myPrs2D );
  }
  else //mode == 1
  {
    aPanel->clearAllBackgroundColorsForProfileList();
    aPanel->clearWarnings();
    int nbProfPoints = aPanel->getNbProfilePoints();
    double ss = aPanel->getSpatialStep();
    QString axisName = aPanel->getAxisName();
    QString leftBankName = aPanel->getLeftBankName();
    QString rightBankName = aPanel->getRightBankName();
    QList<Handle(HYDROData_Entity)> listProf = aPanel->getProfiles();
    HYDROData_SequenceOfObjects seqProf;
    foreach (Handle(HYDROData_Entity) ent, listProf)
      seqProf.Append(ent);

    Handle(HYDROData_PolylineXY) axis = Handle(HYDROData_PolylineXY)::DownCast(  HYDROGUI_Tool::FindObjectByName( module(), axisName, KIND_POLYLINEXY ) );
    Handle(HYDROData_PolylineXY) LB = Handle(HYDROData_PolylineXY)::DownCast(  HYDROGUI_Tool::FindObjectByName( module(), leftBankName, KIND_POLYLINEXY ) );
    Handle(HYDROData_PolylineXY) RB = Handle(HYDROData_PolylineXY)::DownCast(  HYDROGUI_Tool::FindObjectByName( module(), rightBankName, KIND_POLYLINEXY ) );
    HYDROData_Bathymetry::AltitudePoints outBathypoints;

    std::vector<std::string> warnings;
    HYDROData_Stream::PrsDefinition prsDef;

    HYDROData_StreamLinearInterpolation::Perform(seqProf, 500, 1, axis, LB, RB, outBathypoints, true, true, prsDef, &warnings); 
    //TODO for prs3d??
    myPreviewPrs->setShape( prsDef.myPrs2D );
    for (int i=0;i<warnings.size();i++)
      aPanel->addWarning(warnings[i].c_str());
  }
}

void HYDROGUI_StreamOp::erasePreview()
{
  DEBTRACE("erasePreview");
  if( myPreviewPrs )
  {
    delete myPreviewPrs;
    myPreviewPrs = 0;
  }
}

void HYDROGUI_StreamOp::onAddProfiles()
{
  DEBTRACE("onAddProfiles");
  //TODO skip if mode == 1??
  Handle(HYDROData_PolylineXY) aHydAxis = Handle(HYDROData_PolylineXY)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), myHydAxis, KIND_POLYLINEXY ) );
  //if ( aHydAxis.IsNull() )
   // return;

  TopoDS_Face aPlane;
  HYDROData_Stream::BuildRefFace( aPlane );

  // Get the current profiles list
  QStringList aCurrentProfiles = myProfiles;
    
  // Get the selected profiles ( in the Object Browser )
  QStringList anInvalidProfiles;
  QStringList anExistingProfiles;
  QStringList aHasNoIntersectionProfiles;
  QStringList aVerifiedProfiles;

  HYDROData_SequenceOfObjects aSelectedProfiles = HYDROGUI_Tool::GetSelectedObjects( module() ); 

  for( int i = 1, n = aSelectedProfiles.Length(); i <= n; i++ )
  {
    Handle(HYDROData_Profile) aProfile = 
      Handle(HYDROData_Profile)::DownCast( aSelectedProfiles.Value( i ) );
    if ( aProfile.IsNull() )
      continue;

    QString aProfileName = aProfile->GetName();
    Standard_Real aProfilePar = 0.0;

    // Check the profile, if all is ok - add it to the list
    if ( !aProfile->IsValid() )
    {
      // check whether the profile is valid
      anInvalidProfiles << aProfileName;
    }
    else if ( aCurrentProfiles.contains( aProfileName ) )
    {
      // check whether the profile is already added
      anExistingProfiles << aProfileName;
    }
    else if ( !HYDROData_Stream::HasIntersection( aHydAxis, aProfile, aPlane, aProfilePar ) )
    {
      // check whether the profile has intersection
      aHasNoIntersectionProfiles << aProfileName;
    }
    else
    {
      // Insert profile in correct place
      // if hidr axis is null => the params (myProfileParams) will be igrored. So ordering will be the same as in the aSelectedProfiles
      insertProfileInToOrder( aProfileName, aProfilePar, myProfiles, myProfileParams );
      aVerifiedProfiles << aProfileName;
    }
  }
 
  // Show message box with the ignored profiles
  if ( !anInvalidProfiles.isEmpty() ||
       !anExistingProfiles.isEmpty() ||
       !aHasNoIntersectionProfiles.isEmpty() )
  {
    QString aMessage = tr( "IGNORED_PROFILES" );
    if ( !anInvalidProfiles.isEmpty() )
    {
      aMessage.append( "\n\n" );
      aMessage.append( tr("INVALID_PROFILES").arg( anInvalidProfiles.join( "\n" ) ) );
    }
    if ( !anExistingProfiles.isEmpty() )
    {
      aMessage.append( "\n\n" );
      aMessage.append( tr("EXISTING_PROFILES").arg( anExistingProfiles.join( "\n" ) ) );
    }
    if ( !aHasNoIntersectionProfiles.isEmpty() )
    {
      aMessage.append( "\n\n" );
      aMessage.append( tr("NOT_INTERSECTED_PROFILES").arg( aHasNoIntersectionProfiles.join( "\n" ) ) );
    }

    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "WARNING" ), aMessage );
  }

  if ( aVerifiedProfiles.isEmpty() )
    return;

  // Update the panel
  updatePanelData();

  // Update preview
  createPreview();
}

void HYDROGUI_StreamOp::onRemoveProfiles( const QStringList& theProfilesToRemove )
{
  DEBTRACE("onRemoveProfiles");
  QStringList aToRemove = theProfilesToRemove;
  
  aToRemove.removeDuplicates();
  if ( aToRemove.isEmpty() )
    return;

  bool isRemoved = false;

  // Remove profiles
  for ( int i = 0; i < aToRemove.length(); ++i )
  {
    const QString& aProfileName = aToRemove.value( i );

    int aProfileId = myProfiles.indexOf( aProfileName );
    if ( aProfileId < 0 )
      continue;

    myProfiles.removeAt( aProfileId );
    myProfileParams.removeAt( aProfileId );
    isRemoved = true;
  }

  if ( isRemoved )
  {
    // Update the panel
    updatePanelData();

    // Update preview
    createPreview();
  }
}

void HYDROGUI_StreamOp::onDDZValueChanged( double d )
{
  DEBTRACE("onDDZValueChanged");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if (!aPanel)
    return;
  if (aPanel->getMode() == 0)
    createPreview();
}

void HYDROGUI_StreamOp::onSSValueChanged( double d )
{
  DEBTRACE("onSSValueChanged");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if (!aPanel)
    return;
   createPreview();
}

void HYDROGUI_StreamOp::onAxisChanged( const QString& theNewAxis )
{
  DEBTRACE("onAxisChanged");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if (!aPanel)
    return;
  // Get axis object   
  Handle(HYDROData_PolylineXY) aNewAxis = Handle(HYDROData_PolylineXY)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), theNewAxis, KIND_POLYLINEXY ) );

  // Prepare data for intersection check
  TopoDS_Face aPlane;
  HYDROData_Stream::BuildRefFace( aPlane );
  //{
  //  SUIT_MessageBox::critical( module()->getApp()->desktop(), 
  //                            tr( "BAD_SELECTED_POLYLINE_TLT" ),
  //                            tr( "BAD_SELECTED_POLYLINE_MSG" ).arg( theNewAxis ) );
  //  // To restore the old axis
  //  updatePanelData();
  //  return;
  //}

  QStringList   aNewProfiles;
  QList<double> aNewProfileParams;
  QStringList   aHasNoIntersectionProfiles;

  // Get list of profiles which do not intersect the axis
  for ( int i = 0; i < myProfiles.length(); ++i )
  {
    QString aProfileName = myProfiles.value( i );

    Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aProfileName, KIND_PROFILE ) );
    if ( aProfile.IsNull() )
      continue;
      
    Standard_Real aProfilePar = 0.0;
    if ( HYDROData_Stream::HasIntersection( aNewAxis, aProfile, aPlane, aProfilePar ) )
    {
      // Insert profile in correct place
      insertProfileInToOrder( aProfileName, aProfilePar, aNewProfiles, aNewProfileParams );
    }
    else
    {
      aHasNoIntersectionProfiles << aProfile->GetName();
    }
  }

  // If there are profiles which don't intersect the new axis - show confirmation message box
  bool isConfirmed = true;
  if ( !aHasNoIntersectionProfiles.isEmpty() )
  {
    SUIT_MessageBox::StandardButtons aButtons = 
      SUIT_MessageBox::Yes | SUIT_MessageBox::No;

    QString aMsg = aHasNoIntersectionProfiles.join( "\n" );
    isConfirmed = SUIT_MessageBox::question( module()->getApp()->desktop(),
                                             tr( "CONFIRMATION" ),
                                             tr( "CONFIRM_AXIS_CHANGE" ).arg( aMsg ),
                                             aButtons, 
                                             SUIT_MessageBox::Yes) == SUIT_MessageBox::Yes;
  }

  // Check if the user has confirmed axis change
  if ( !isConfirmed )
  {
    // To restore the old axis
    updatePanelData();
  }
  else
  {
    // Update data
    myHydAxis = theNewAxis;
    myProfiles = aNewProfiles;
    myProfileParams = aNewProfileParams;

    // Update the panel
    updatePanelData();

    // Update preview
    createPreview();
  }
}

void HYDROGUI_StreamOp::onLeftBankChanged( const QString& theNewAxis )
{
  DEBTRACE("onLeftBankChanged");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if (!aPanel)
    return;
  if (aPanel->getMode() == 1)
    createPreview();
}

void HYDROGUI_StreamOp::onRightBankChanged( const QString& theNewAxis )
{
  DEBTRACE("onRightBankChanged");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if (!aPanel)
    return;
  if (aPanel->getMode() == 1)
    createPreview();
}

void HYDROGUI_StreamOp::onProfilePointsChanged( int d )
{
  DEBTRACE("onProfilePointsChanged");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if (!aPanel)
    return;
  if (aPanel->getMode() == 1)
    createPreview();
}

void HYDROGUI_StreamOp::onModeChanged( bool mode )
{
  DEBTRACE("onModeChanged");
  createPreview();
}

void HYDROGUI_StreamOp::updatePanelData()
{
  DEBTRACE("updatePanelData");
  HYDROGUI_StreamDlg* aPanel = ::qobject_cast<HYDROGUI_StreamDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->setAxisName( myHydAxis );
  aPanel->setProfiles( myProfiles );

}

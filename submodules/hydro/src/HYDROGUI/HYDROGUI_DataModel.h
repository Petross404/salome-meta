// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_DATAMODEL_H
#define HYDROGUI_DATAMODEL_H

#include <HYDROData_Document.h>
#include <HYDROData_Entity.h>
#include <HYDROData_Zone.h>
#include <HYDROData_Region.h>
#include <HYDROData_CalculationCase.h>

#include <QMap>
#include <QList>
#include <LightApp_DataModel.h>

#include <SUIT_TreeModel.h>

class CAM_DataObject;
class SUIT_DataObject;
class HYDROGUI_DataObject;
class HYDROGUI_Zone;

/**
 * \class HYDROGUI_DataModel
 * \brief The class representing the HYDROGUI data model
 */
class HYDROGUI_DataModel : public LightApp_DataModel, public SUIT_DataSearcher
{
public:
  /**
   * Constructor.
   * \param theModule module object
   */
  HYDROGUI_DataModel( CAM_Module* theModule );
  virtual ~HYDROGUI_DataModel();

  /**
   * Creates the document into the data model. Reimplemented.
   */
  virtual bool create( CAM_Study* );

  /**
   * Open the document into the data model. Reimplemented.
   * \param theURL opened study path
   * \param theStudy object study
   * \param theFileList list of opened files for this model.
   */
  virtual bool open( const QString& theURL,
                     CAM_Study* theStudy,
                     QStringList theFileList );

  /**
   * Saves the document. Reimplemented.
   * \param returned theFileList list of saved files of this model.
   */
  virtual bool save( QStringList& theFileList );

  /**
   * Saves as the document. Reimplemented.
   * \param theURL saved study path
   * \param theStudy object study
   * \param returned theFileList list of saved files of this model.
   */
  virtual bool saveAs( const QString& theURL,
                       CAM_Study* theStudy,
                       QStringList& theFileList );

  /**
   * Close the model and remove data. Reimplemented.
   */
  virtual bool close();

  /**
   * Dump study data to Python script. Reimplemented.
   */
  virtual bool                        dumpPython( const QString& theURL,
                                                  CAM_Study*     theStudy,
                                                  bool           isMultiFile,
                                                  QStringList&   theListOfFiles );

  /**
   * Returns modification status. Reimplemented.
   * \return boolean value of modification status
   */
  virtual bool isModified() const;

  /**
   * Returns saving status. Reimplemented.
   * \return true if document has saved files on disc.
   */
  virtual bool isSaved() const;

  /**
   * Updates the internal structure of data object tree.
   * \param theStudyId study identifier
   */
  virtual void update( const int theStudyId );

  /**
   * Returns data object corresponding to the model object.
   * \param the data model object
   * \return the only one object referenced to the given data model object, or null if not found
   */
  virtual HYDROGUI_DataObject* getDataObject( const Handle(HYDROData_Entity)& theModelObject );

  /**
   * Returns a data object referenced to the given data object.
   * \param the data object
   * \return the object referenced to the given object, or null if not found
   */
  virtual HYDROGUI_DataObject* getReferencedDataObject( HYDROGUI_DataObject* theObject );

  /**
   * Finds the object by entry
   * \param theEntry an object entry
   */
  virtual SUIT_DataObject* findObject( const QString& theEntry ) const;

  /**
   * Updates the internal structure of data object tree starting from specified data object \a obj.
   * \param theObject start data object
   * \param theStudy study object
   */
  virtual void update( LightApp_DataObject* theObject = 0,
                       LightApp_Study* theStudy = 0 );

  /**
   * Creates a module object and set is a root for the model
   */
  CAM_DataObject* createRootModuleObject( SUIT_DataObject* theParent );

  /**
   * Create a new region in the given calculation case containing given zones. 
   */
  bool createNewRegion( Handle(HYDROData_CalculationCase) theCase,
                        const QList<HYDROGUI_Zone*>& theZonesList );

  /**
   * Correct an internal model object according to the current document mode
   */
  void updateModel();

  /**
   * Find a data object by the specified entry and kind
   */
  Handle(HYDROData_Entity) objectByEntry( const QString& theEntry,
                                          const ObjectKind theObjectKind = KIND_UNKNOWN );

  /**
   * Check if it is possible to perform 'undo' operation
   */
  bool canUndo() const;

  /**
   * Check if it is possible to perform 'redo' operation
   */
  bool canRedo() const;

  /**
   * Returns the list of names of available 'undo' actions
   */
  QStringList undoNames() const;

  /**
   * Returns the list of names of available 'redo' actions
   */
  QStringList redoNames() const;

  /**
   * Clear the list of stored 'undo' actions
   */
  void clearUndos();

  /**
   * Clear the list of stored 'redo' actions
   */
  void clearRedos();

  /**
   * Perform the 'undo' operation
   */
  bool undo();

  /**
   * Perform the 'redo' operation
   */
  bool redo();

  /**
   * Check if it is possible to perform 'copy' operation
   */
  bool canCopy();

  /**
   * Check if it is possible to perform 'paste' operation
   */
  bool canPaste();

  /**
   * Perform the 'copy' operation
   */
  bool copy();

  /**
   * Perform the 'paste' operation
   */
  bool paste();

  /**
   * Rename the object
   */
  bool rename( Handle(HYDROData_Entity) theEntity, const QString& theName );

  /**
   * Creates the Calculation Case subtree for usage within an operation dialog.
   * \param theParent a created object will be appended as a child of this GUI object
   * \param theCase the calculation case model object
   * \param theLandCover if true - land cover regions will be represented in the tree
   */
  void buildCaseTree( SUIT_DataObject*                  theParent,
                      Handle(HYDROData_CalculationCase) theCase );

  /**
   * Updates the object subtree.
   * \param theObj the data model entity
   */
  void updateObjectTree( Handle(HYDROData_Entity)& theObj );

  /**
   * Update the sequence of the objects to be copied
   */
  static void changeCopyingObjects( const HYDROData_SequenceOfObjects& );

  /**
   * Returns name of the partition containing the objects of the specified kind
   * \param theObjectKind kind of objects
   * \return partition name
   */
  static QString partitionName( const ObjectKind theObjectKind );

  /**
   * Creates the default Strickler table object: both GUI data object and corresponding model object
   * \param theDocument a document into which created object will be added
   * \param theParent a created object will be appended as a child of this GUI object
   */
  void                 createDefaultStricklerTable( const Handle(HYDROData_Document)& theDocument,
                                                    LightApp_DataObject*              theParent );

protected:
  /**
   * Returns the document for the current study
   */
  Handle(HYDROData_Document) getDocument() const;

  /**
   * Creates the GUI data object according to the model object.
   * \param theParent a created object will be appended as a child of this object
   * \param theModelObject model object
   * \param theParentEntry entry of parent object
   */
  LightApp_DataObject* createObject( SUIT_DataObject*         theParent,
                                     Handle(HYDROData_Entity) theModelObject,
                                     const QString&           theParentEntry = QString(),
                                     const bool               theIsBuildTree = true );

  /**
   * Creates the GUI data object without corresponding model object: just by name
   * \param theParent a created object will be appended as a child of this object
   * \param theName name of this object
   * \param theParentEntry entry of parent object
   */
  LightApp_DataObject* createObject( SUIT_DataObject* theParent,
                                     const QString&   theName,
                                     const QString&   theParentEntry = QString() );

  /**
   * Build object tree if the flag theIsBuildTree is true. 
   * This is a conditional wrapper for buildObjectTree method.
   * \param theParent a created object will be appended as a child of this object
   * \param theObject the GUI object
   * \param theParentEntry the entry of parent object
   * \param theIsBuildTree if true then build the subtree of the GUI object
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  LightApp_DataObject* buildObject( SUIT_DataObject*     theParent,
                                    HYDROGUI_DataObject* theObject,
                                    const QString&       theParentEntry,
                                    const bool           theIsBuildTree,
                                    const bool           theIsInOperation = false );

  /**
   * Build object tree if the flag theIsBuildTree is true. 
   * \param theParent a created object will be appended as a child of this GUI object
   * \param theModelObject the data model zone object
   * \param theParentEntry the entry of parent object
   * \param theIsBuildTree if true then build the subtree of the GUI object
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  LightApp_DataObject* createZone( SUIT_DataObject*       theParent,
                                   Handle(HYDROData_Zone) theModelObject,
                                   const QString&         theParentEntry,
                                   const bool             theIsBuildTree ,
                                   const bool             theIsInOperation = false  );

  /**
   * Build object tree if the flag theIsBuildTree is true. 
   * \param theParent a created object will be appended as a child of this GUI object
   * \param theModelObject the data model region object
   * \param theParentEntry the entry of parent object
   * \param theIsBuildTree if true then build the subtree of the GUI object
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  LightApp_DataObject* createRegion( SUIT_DataObject*         theParent,
                                     Handle(HYDROData_Region) theModelObject,
                                     const QString&           theParentEntry,
                                     const bool               theIsBuildTree ,
                                     const bool               theIsInOperation = false );
  /**
   * Build partition for object.
   * \param theObject gui object for which the partition will be build
   * \param theObjects sequence of builded objects
   * \param thePartName name of created partition
   * \param theIsCreateEmpty if true then partition will be created in any case
   */
  void                 buildObjectPartition( SUIT_DataObject*                   theObject,
                                             const HYDROData_SequenceOfObjects& theObjects,
                                             const QString&                     thePartName,
                                             const bool                         theIsCreateEmpty );

  /**
   * Build tree of a model object.
   * \param theParent a created object will be appended as a child of this object
   * \param theObject gui object for which the tree will be build
   * \param theParentEntry entry of parent object
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  void                 buildObjectTree( SUIT_DataObject* theParent,
                                        SUIT_DataObject* theObject,
                                        const QString&   theParentEntry = QString(),
                                        const bool       theIsInOperation = false );

  /**
   * Removes data object from the tree.
   * \param theParent an object will be removed from this parent.
   * \param theChild the removed object.
   */
  void removeChild( SUIT_DataObject* theParent,
                    SUIT_DataObject* theChild );

  /**
   * Returns the first child of the object with the specified name
   * \param theFather object that contain the searched object in children
   * \param theName name f the searched data object
   * \returns NULL if not found
   */
  static SUIT_DataObject* findChildByName( const SUIT_DataObject* theFather,
                                           const QString& theName );

  void updateDocument();

  /**
   * Set object visibility state.
   * \param theModelObject the data model object
   * \param theDataObject the GUI object
   */
  void setObjectVisibilityState( Handle(HYDROData_Entity) theModelObject,
                                 HYDROGUI_DataObject* theObject );
                                 

protected:
  QString myStudyURL; ///< the saved/opened document URL
  QByteArray myStates;
};

#endif 

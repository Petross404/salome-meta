// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_TranslateObstacleOp.h"

#include "HYDROGUI_TranslateObstacleDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_ShapesTool.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>


HYDROGUI_TranslateObstacleOp::HYDROGUI_TranslateObstacleOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ), 
  myPreviewPrs( NULL )
{
  setName( tr( "TRANSLATE_OBSTACLE" ) );
}

HYDROGUI_TranslateObstacleOp::~HYDROGUI_TranslateObstacleOp()
{
  erasePreview();
}

void HYDROGUI_TranslateObstacleOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  // Get panel and reset its state
  HYDROGUI_TranslateObstacleDlg* aPanel = (HYDROGUI_TranslateObstacleDlg*)inputPanel();
  aPanel->reset();

  // Get the edited object
  if ( isApplyAndClose() )
    myEditedObject = Handle(HYDROData_Obstacle)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if ( myEditedObject.IsNull() ) {
    abort();
    return;
  }

  // Set the edited object name to the panel
  aPanel->setName( myEditedObject->GetName() );

  // Create preview
  createPreview();
}

void HYDROGUI_TranslateObstacleOp::abortOperation()
{
  erasePreview();
  abortDocOperation();

  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_TranslateObstacleOp::commitOperation()
{
  erasePreview();

  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_TranslateObstacleOp::createInputPanel() const
{
  HYDROGUI_TranslateObstacleDlg* aPanel = new HYDROGUI_TranslateObstacleDlg( module(), getName() );

  connect( aPanel, SIGNAL( argumentsChanged() ), this, SLOT( onArgumentsChanged() ) );

  return aPanel;
}

bool HYDROGUI_TranslateObstacleOp::processApply( int& theUpdateFlags,
                                                 QString& theErrorMsg,
                                                 QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_TranslateObstacleDlg* aPanel = ::qobject_cast<HYDROGUI_TranslateObstacleDlg*>( inputPanel() );
  if ( !aPanel || myEditedObject.IsNull() ) {
    return false;
  }

  // Erase preview
  erasePreview();

  // Get the translated shape
  double aDx = aPanel->getDx();
  double aDy = aPanel->getDy();
  double aDz = aPanel->getDz();

  // Translate the obstacle
  myEditedObject->Translate( aDx, aDy, aDz );
  myEditedObject->Update();

  module()->setIsToUpdate( myEditedObject );

  // Set update flags
  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  return true;
}

void HYDROGUI_TranslateObstacleOp::createPreview()
{
  HYDROGUI_TranslateObstacleDlg* aPanel = (HYDROGUI_TranslateObstacleDlg*)inputPanel();
  if ( myEditedObject.IsNull() || !aPanel )
    return;

  // Create preview presentation if necessary
  if ( !myPreviewPrs ) {
    LightApp_Application* anApp = module()->getApp();

    if ( !getPreviewManager() )
      setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                         anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
    OCCViewer_ViewManager* aViewManager = getPreviewManager();
    if ( aViewManager ) {
      if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() ) {
        Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
        if ( !aCtx.IsNull() ) {
          myPreviewPrs = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );
          myPreviewPrs->setFillingColor( myEditedObject->GetFillingColor(), false, false );
          myPreviewPrs->setBorderColor( myEditedObject->GetBorderColor(), false, false );
        }
      }
    }
  }
  
  // Set the translated shape to the preview presentation
  if ( myPreviewPrs )
  {
    double aDx = aPanel->getDx();
    double aDy = aPanel->getDy();
    double aDz = aPanel->getDz();
    TopoDS_Shape anOriShape = myEditedObject->GetShape3D();

    TopoDS_Shape aTranslatedShape = HYDROData_ShapesTool::Translated( anOriShape, aDx, aDy, aDz );
    myPreviewPrs->setShape( aTranslatedShape );
  }
}

void HYDROGUI_TranslateObstacleOp::erasePreview()
{
  if( myPreviewPrs ) {
    delete myPreviewPrs;
    myPreviewPrs = 0;
  }
}

void HYDROGUI_TranslateObstacleOp::onArgumentsChanged()
{
  // Update preview
  createPreview();
}


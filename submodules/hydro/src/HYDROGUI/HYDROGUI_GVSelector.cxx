// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_GVSelector.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Prs.h"
#include "HYDROGUI_Tool2.h"

#include <GraphicsView_Object.h>
#include <GraphicsView_Selector.h>
#include <GraphicsView_ViewPort.h>
#include <GraphicsView_Viewer.h>

#include <LightApp_DataOwner.h>

HYDROGUI_GVSelector::HYDROGUI_GVSelector( HYDROGUI_Module* theModule,
                                          GraphicsView_Viewer* theViewer,
                                          SUIT_SelectionMgr* theSelMgr )
: LightApp_GVSelector( theViewer, theSelMgr ),
  myModule( theModule )
{
}

HYDROGUI_GVSelector::~HYDROGUI_GVSelector()
{
}

void HYDROGUI_GVSelector::getSelection( SUIT_DataOwnerPtrList& theList ) const
{
  if( GraphicsView_ViewPort* aViewPort = myViewer->getActiveViewPort() )
  {
    for( aViewPort->initSelected(); aViewPort->moreSelected(); aViewPort->nextSelected() )
    {
      if( HYDROGUI_Prs* aPrs = dynamic_cast<HYDROGUI_Prs*>( aViewPort->selectedObject() ) )
      {
        QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aPrs->getObject() );
        theList.append( new LightApp_DataOwner( anEntry ) );
      }
    }
  }
}

void HYDROGUI_GVSelector::setSelection( const SUIT_DataOwnerPtrList& theList )
{
  HYDROGUI_DataModel* aModel = myModule->getDataModel();
  if( GraphicsView_ViewPort* aViewPort = myViewer->getActiveViewPort() )
  {
    aViewPort->clearSelected();
    SUIT_DataOwnerPtrList::const_iterator it;
    GraphicsView_ObjectList anObjectList = aViewPort->getObjects();
    for ( it = theList.begin(); it != theList.end(); ++it )
    {
      LightApp_DataOwner* anOwner = dynamic_cast<LightApp_DataOwner*>( (*it).operator->() );
      if ( anOwner )
      {
        QString anEntry = anOwner->entry();
        Handle(HYDROData_Entity) aModelObject = aModel->objectByEntry( anEntry );
        if( !aModelObject.IsNull() )
        {
          if( HYDROGUI_Prs* aPrs = HYDROGUI_Tool::GetPresentation( aModelObject, anObjectList ) )
            aViewPort->setSelected( aPrs );
        }
      }
    }
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_OCCDisplayer.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include <HYDROGUI_ShapeImage.h>
#include <HYDROGUI_ShapeBathymetry.h>
#include <HYDROGUI_ShapeLandCoverMap.h>
#include "HYDROGUI_Operation.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_ZLayers.h"
#include "HYDROGUI_Polyline.h"

#include <HYDROData_Bathymetry.h>
#include <HYDROData_Image.h>
#include <HYDROData_LandCoverMap.h>
#include <HYDROData_StricklerTable.h>

#include <AIS_InteractiveContext.hxx>
#include <AIS_ListIteratorOfListOfInteractive.hxx>
#include <AIS_ListOfInteractive.hxx>
#include <AIS_ColorScale.hxx>

#include <TColStd_SequenceOfInteger.hxx>

#include <LightApp_Application.h>
#include <SUIT_Study.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_OCCDisplayer::HYDROGUI_OCCDisplayer( HYDROGUI_Module* theModule )
: HYDROGUI_AbstractDisplayer( theModule )
{
  DEBTRACE("HYDROGUI_OCCDisplayer");
  myToUpdateColorScale = false;
}

HYDROGUI_OCCDisplayer::~HYDROGUI_OCCDisplayer()
{
}

void HYDROGUI_OCCDisplayer::SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                         const size_t                       theViewerId )
{
  OCCViewer_Viewer* aViewer = module()->getOCCViewer( theViewerId );
  if( !aViewer )
    return;

  for ( int i = 1, n = theObjs.Length(); i <= n; i++ )
  {
    Handle(HYDROData_Entity) anObj = theObjs.Value( i );
    if( anObj.IsNull() )
      continue;

    HYDROGUI_Shape* anObjShape = module()->getObjectShape( (size_t)aViewer, anObj );
    if ( !anObjShape )
      continue;

    anObjShape->setIsToUpdate( true );
  }
}

int HYDROGUI_OCCDisplayer::AddPreviewZLayer( OCCViewer_ViewManager* theMgr )
{
  DEBTRACE("AddPreviewZLayer");
  int aLayer = -1;
  OCCViewer_Viewer* aViewer = theMgr->getOCCViewer();
  if ( !aViewer )
    return aLayer;

  aLayer = CreateTopZLayer( aViewer->getViewer3d() );

  // Hilight presentation should be on top
  Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
  if( !aCtx.IsNull() ) {
    int aTopLayer = CreateTopZLayer( aViewer->getViewer3d() );
    if ( aTopLayer > 0 ) {
      UpdateZLayersOfHilightPresentationsOfDisplayedObjects( aCtx, aTopLayer );
    }
  }

  return aLayer;
}

void HYDROGUI_OCCDisplayer::RemoveZLayer( OCCViewer_ViewManager* theMgr,
                                          const int theLayer,
                                          bool isClearAll)
{
  DEBTRACE("RemoveZLayer " << theLayer << " " << isClearAll);
  if ( theLayer < 0 )
    return;

  if ( !theMgr )
    return;

  OCCViewer_Viewer* aViewer = theMgr->getOCCViewer();
  if ( !aViewer )
    return;

  // Get existing Z layers
  TColStd_SequenceOfInteger anExistingZLayers;
  aViewer->getViewer3d()->GetAllZLayers( anExistingZLayers );
  int aNbLayers = anExistingZLayers.Length();

  if (isClearAll)
    {
      for ( int i = 1; i <= aNbLayers; i++ )
        {
          int val = anExistingZLayers.Value( i );
          if (val > 0)
            aViewer->getViewer3d()->RemoveZLayer( val );
        }
    }
  else
    if ( theLayer < aNbLayers )
      aViewer->getViewer3d()->RemoveZLayer( theLayer );
}

void HYDROGUI_OCCDisplayer::EraseAll( const size_t theViewerId )
{
  OCCViewer_Viewer* aViewer = module()->getOCCViewer( theViewerId );
  if( !aViewer )
    return;

  module()->removeViewShapes( (size_t)aViewer );
  UpdateColorScale( aViewer );
}

void HYDROGUI_OCCDisplayer::Erase( const HYDROData_SequenceOfObjects& theObjs,
                                   const size_t                       theViewerId )
{
  DEBTRACE("Erase " << theObjs.Length() << " " << theViewerId);
  OCCViewer_Viewer* aViewer = module()->getOCCViewer( theViewerId );
  if( !aViewer )
    return;

  for ( int i = 1, n = theObjs.Length(); i <= n; i++ )
  {
    Handle(HYDROData_Entity) anObj = theObjs.Value( i );
    if( anObj.IsNull() )
      continue;
    DEBTRACE("Erasing objects...");
    module()->removeObjectShape( (size_t)aViewer, anObj );
  }
  aViewer->update();
  if ( !module()->isLandCoversScalarMapModeOn( (size_t)aViewer ) ) {
    UpdateColorScale( aViewer );
  }
}

HYDROGUI_Shape* HYDROGUI_OCCDisplayer::createShape( const size_t                          theViewerId,
                                                    const Handle(AIS_InteractiveContext)& theContext,
                                                    const Handle(HYDROData_Entity)&       theObject )
{
  DEBTRACE("createShape " << theViewerId);
  HYDROGUI_Shape* aResShape = NULL;
  if ( theContext.IsNull() || theObject.IsNull() )
    return aResShape;

  if ( !HYDROGUI_Tool::IsObjectHasPresentation( theObject, OCCViewer_Viewer::Type() ) )
    return aResShape;

  if( theObject->IsKind( STANDARD_TYPE( HYDROData_Image ) ) )
    aResShape = new HYDROGUI_ShapeImage( theContext, Handle(HYDROData_Image)::DownCast( theObject ) );
  else if( theObject->IsKind( STANDARD_TYPE( HYDROData_Bathymetry ) ) )
    aResShape = new HYDROGUI_ShapeBathymetry( this, theContext, Handle(HYDROData_Bathymetry)::DownCast( theObject ) );
  else if( theObject->IsKind( STANDARD_TYPE( HYDROData_LandCoverMap ) ) ) {
    bool isScalarMode = module()->isLandCoversScalarMapModeOn( theViewerId );
    aResShape = new HYDROGUI_ShapeLandCoverMap( this, theContext, Handle(HYDROData_LandCoverMap)::DownCast( theObject ), -1, isScalarMode );
  }
  else
    aResShape = new HYDROGUI_Shape( theContext, theObject );

  module()->setObjectShape( theViewerId, theObject, aResShape );

  return aResShape;
}

void HYDROGUI_OCCDisplayer::Display( const HYDROData_SequenceOfObjects& theObjs,
                                     const size_t                       theViewerId,
                                     const bool                         theIsForced,
                                     const bool theDoFitAll )
{
  // Get OCC viewer by id
  OCCViewer_Viewer* aViewer = module()->getOCCViewer( theViewerId );
  if( !aViewer )
    return;

  // Get interactive context
  Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
  if( aCtx.IsNull() )
    return;

  // Get the document
  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
  if ( !aDoc )
    return;

  // Assign Z layer indexes to the objects
  aDoc->Show( theObjs );

  // Sort objects by display order ( needed for Z layers assignment only )
  HYDROData_SequenceOfObjects anUnorderedToDisplay = theObjs;
  HYDROData_SequenceOfObjects anOrderedToDisplay;
  HYDROData_SequenceOfObjects anAllOrderedObjects = aDoc->GetObjectsLayerOrder();

  HYDROData_SequenceOfObjects::Iterator anAllOrderedIter( anAllOrderedObjects );
  for ( ; anAllOrderedIter.More(); anAllOrderedIter.Next() ) {
    QString anOrderedEntry =
      HYDROGUI_DataObject::dataObjectEntry( anAllOrderedIter.Value() );

    HYDROData_SequenceOfObjects::Iterator aToDisplayIter( anUnorderedToDisplay );
    for ( ; aToDisplayIter.More(); aToDisplayIter.Next() ) {
      Handle(HYDROData_Entity) anObjToDisplay = aToDisplayIter.Value();
      QString anEntry = HYDROGUI_DataObject::dataObjectEntry( anObjToDisplay );
      if ( anEntry == anOrderedEntry ) {
        anOrderedToDisplay.Prepend( anObjToDisplay );
        anUnorderedToDisplay.Remove( aToDisplayIter );
        break;
      }
    }
  }

  // Get 3d viewer
  Handle(V3d_Viewer) aViewer3d = aViewer->getViewer3d();

  // Display objects:
  HYDROGUI_ZLayersIterator aZLayersIt( aViewer->getViewer3d() );
  if ( !aZLayersIt.More() ) {
    aZLayersIt.Next();
  }

  // 1. Display the ordered objects:
  HYDROData_SequenceOfObjects::Iterator anOrderedIter( anOrderedToDisplay );
  for ( ; anOrderedIter.More(); anOrderedIter.Next() ) {
    Handle(HYDROData_Entity) anObj = anOrderedIter.Value();
    if ( Display( anObj, aViewer, theIsForced ) ) {
      // set Z layer ( one Z layer for each ordered object )
      int aZLayerId = aZLayersIt.LayerId();
      SetZLayer( aViewer, anObj, aZLayerId );
      SetZLayerSettings( aViewer3d, aZLayerId, true );
      aZLayersIt.Next();
    }
  }

  // 2. Display the unordered objects:
  bool isDisplayed = false;
  int anUnorderedZLayerId = aZLayersIt.LayerId();
  HYDROData_SequenceOfObjects::Iterator anUnorderedIter( anUnorderedToDisplay );
  for ( ; anUnorderedIter.More(); anUnorderedIter.Next() ) {
    Handle(HYDROData_Entity) anObj = anUnorderedIter.Value();
    if ( Display( anObj, aViewer, theIsForced) ) {
      // set Z layer ( one Z layer for all unordered objects )
      SetZLayer( aViewer, anObj, anUnorderedZLayerId );
      if ( !isDisplayed ) {
        SetZLayerSettings( aViewer3d, anUnorderedZLayerId, false );
      }
      isDisplayed = true;
    }
  }

  // 3. Update the top Z layer index
  if ( isDisplayed ) {
    aZLayersIt.Next();
  }

  // Update Z layer of the active operation
  int aPreviewZLayerId = aZLayersIt.LayerId();

  HYDROGUI_Module* aModule = module();
  SUIT_Operation* anOp = aModule->activeOperation();
  HYDROGUI_Operation* aHOp = anOp ? dynamic_cast<HYDROGUI_Operation*>( anOp ) : 0;
  if ( aHOp && aHOp->getPreviewZLayer() >= 0 ) {
    aHOp->updatePreviewZLayer( aPreviewZLayerId );
    aZLayersIt.Next();
  }

  // Update Z layer of hilight presentations
  int aHilightLayer = aZLayersIt.TopLayer();
  UpdateZLayersOfHilightPresentationsOfDisplayedObjects( aCtx, aHilightLayer );

  // Fit all / update selection
  if ( theDoFitAll ) {
    OCCViewer_ViewManager* aViewManager
      = ::qobject_cast<OCCViewer_ViewManager*>( aViewer->getViewManager() );
    if ( aViewManager ) {
      OCCViewer_ViewWindow* aViewWindow =
        ::qobject_cast<OCCViewer_ViewWindow*>( aViewManager->getActiveView() );
      if ( aViewWindow ) {
        aViewWindow->onFitAll();
      }
    }
  }
  else if ( !aCtx.IsNull() ) { // TODO: determine if this code is necessary (added as a fix for issue# 359)
    aCtx->UpdateSelected(true);
  }

  UpdateColorScale( aViewer );
}

void HYDROGUI_OCCDisplayer::purgeObjects( const size_t theViewerId )
{
  OCCViewer_Viewer* aViewer = module()->getOCCViewer( theViewerId );
  if( !aViewer )
    return;

  Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
  if( aCtx.IsNull() )
    return;

  AIS_ListOfInteractive aDisplayedObjects;
  aCtx->DisplayedObjects( aDisplayedObjects );

  AIS_ListIteratorOfListOfInteractive aListIter( aDisplayedObjects );
  for ( ; aListIter.More(); aListIter.Next() )
  {
    Handle(AIS_InteractiveObject) aPrsObj = aListIter.Value();
    if ( aPrsObj.IsNull() )
      continue;

    Handle(HYDROData_Entity) anOwnerObj =
      Handle(HYDROData_Entity)::DownCast( aPrsObj->GetOwner() );
    if ( !anOwnerObj.IsNull() && anOwnerObj->IsRemoved() )
      module()->removeObjectShape( (size_t)aViewer, anOwnerObj );
  }
  UpdateColorScale( aViewer );
}

QString HYDROGUI_OCCDisplayer::GetType() const
{
  return OCCViewer_Viewer::Type();
}

bool HYDROGUI_OCCDisplayer::Display( const Handle(HYDROData_Entity)& theObject,
                                     const OCCViewer_Viewer* theViewer,
                                     const bool theIsForced )
{
  DEBTRACE("Display");
  bool aRes = false;

  if ( theObject.IsNull() || theObject->IsRemoved() || !theViewer ) {
    return aRes;
  }

  // Get interactive context
  Handle(AIS_InteractiveContext) aCtx = theViewer->getAISContext();
  if( aCtx.IsNull() ) {
    return aRes;
  }

  // Viewer id
  size_t aViewerId = (size_t)theViewer;

  // Object shape
  HYDROGUI_Shape* anObjShape = module()->getObjectShape( aViewerId, theObject );
  // create if needed
  if ( !anObjShape ) {
    anObjShape = createShape( aViewerId, aCtx, theObject );
    if ( anObjShape ) {
      anObjShape->setIsToUpdate( true );
    }
  }

  // Process the shape
  if ( anObjShape ) {
    // update if needed
    if ( anObjShape->getIsToUpdate() || theIsForced ) {
      anObjShape->update( false, false );
    }

    // Set visibility
    bool anIsVisible = module()->isObjectVisible( aViewerId, theObject );
    anObjShape->setVisible( anIsVisible, false );

    aRes = true;
  }

  return aRes;
}

void HYDROGUI_OCCDisplayer::SetZLayer( const OCCViewer_Viewer* theViewer,
                                       const Handle(HYDROData_Entity)& theObject,
                                       const int theZLayerId )
{
  if ( !theViewer /*|| ( theZLayerId < 0 )*/ ) {
    return;
  }

  // Get interactive context
  Handle(AIS_InteractiveContext) aCtx = theViewer->getAISContext();
  if( aCtx.IsNull() ) {
    return;
  }

  // Get viewer id
  size_t aViewerId = (size_t)theViewer;

  // Get object shape
  HYDROGUI_Shape* anObjShape = module()->getObjectShape( aViewerId, theObject );

  // Set Z layer
  if ( anObjShape )
  {
    QList<Handle(AIS_InteractiveObject)> shapes = anObjShape->getAISObjects();
    foreach( Handle(AIS_InteractiveObject) shape, shapes )
      aCtx->SetZLayer( shape, theZLayerId );
  }
}

void HYDROGUI_OCCDisplayer::SetToUpdateColorScale()
{
  myToUpdateColorScale = true;
}

void HYDROGUI_OCCDisplayer::UpdateColorScale( const OCCViewer_Viewer* theViewer )
{
  if( !myToUpdateColorScale || !theViewer )
    return;

  OCCViewer_ViewWindow* aWnd = dynamic_cast<OCCViewer_ViewWindow*>( theViewer->getViewManager()->getActiveView() );
  Handle(V3d_View) aView = aWnd->getViewPort()->getView();


  HYDROGUI_Module* aModule = module();
  size_t aViewerId = (size_t)theViewer;//TODO: check if viewer id is correct
  bool isLandCoverColoringOn = aModule->isLandCoversScalarMapModeOn( aViewerId );

  QList<HYDROGUI_Shape*> aLandCoverMapShapes = aModule->getObjectShapes( aViewerId, KIND_LAND_COVER_MAP );
  QList<HYDROGUI_Shape*> aBathShapes = aModule->getObjectShapes( aViewerId, KIND_BATHYMETRY );

  bool isDisplayColorScale = false;
  foreach (HYDROGUI_Shape* shape, aLandCoverMapShapes)
  {
    if (aModule->isObjectVisible(aViewerId, shape->getObject()))
    {
      isDisplayColorScale = true;
      break;
    }
  }
  if (!isDisplayColorScale)
    foreach (HYDROGUI_Shape* shape, aBathShapes)
    {
      if (aModule->isObjectVisible(aViewerId, shape->getObject()))
      {
        isDisplayColorScale = true;
        break;
      }
    }

  Standard_Real aColorScaleMin = 0, aColorScaleMax = 1;

  // Get range
  Handle(HYDROData_StricklerTable) aTable;
  QStringList aTableTypes;
  if ( isLandCoverColoringOn ) {
    aTable = module()->getLandCoverColoringTable( aViewerId );
    if ( !aTable.IsNull() ) {
      // TODO: non-empty title leads to buggy behaviour
      // aColorScaleTitle = TCollection_ExtendedString( aTable->GetName().toLatin1().constData() );
      aTable->GetCoefficientRange( aColorScaleMin, aColorScaleMax );
      aTableTypes = aTable->GetTypes();
    }
  } else {
    Standard_Real aMin, aMax;
    bool isFirst = true;
    foreach( HYDROGUI_Shape* aShape, aBathShapes )
    {
      HYDROGUI_ShapeBathymetry* aBathShape = dynamic_cast<HYDROGUI_ShapeBathymetry*>( aShape );
      if( !aBathShape || !aBathShape->isVisible() )
        continue;

      aBathShape->GetRange( aMin, aMax );

      if( isFirst || aMin < aColorScaleMin )
        aColorScaleMin = aMin;
      if( isFirst || aMax > aColorScaleMax )
        aColorScaleMax = aMax;

      isFirst = false;
    }
  }

  Handle(AIS_ColorScale) aColorScale = GetColorScale( aViewerId );
  Handle(AIS_InteractiveContext) aCtx = theViewer->getAISContext();
  if( isDisplayColorScale )
  {
    if( !aColorScale.IsNull() )
    {
      // Set color scale title
      TCollection_ExtendedString aColorScaleTitle = ""; //TODO
      aColorScale->SetTitle( aColorScaleTitle );

      // Set color scale range
      aColorScale->SetRange( aColorScaleMin, aColorScaleMax );

      aColorScale->SetToUpdate();

      if ( !isLandCoverColoringOn ) {
        foreach( HYDROGUI_Shape* aShape, aBathShapes ) {
          HYDROGUI_ShapeBathymetry* aBathShape = dynamic_cast<HYDROGUI_ShapeBathymetry*>( aShape );
          if( !aBathShape || !aBathShape->isVisible() )
            continue;

          aBathShape->UpdateWithColorScale( aColorScale );
        }
      }

      if ( !aCtx.IsNull()/* && !aCtx->IsDisplayed( aColorScale ) */) {
	if ( !aCtx->IsDisplayed( aColorScale ) ) {
	  aCtx->Display( aColorScale, Standard_False );
	}

	aCtx->Update( aColorScale, true );
      }
    }
  }
  else
  {
    if ( !aCtx.IsNull() && aCtx->IsDisplayed( aColorScale ) ) {
      aCtx->Erase( aColorScale, true );
    }
  }

  foreach( HYDROGUI_Shape* aShape, aLandCoverMapShapes ) {
    HYDROGUI_ShapeLandCoverMap* aLandCoverMapShape =
      dynamic_cast<HYDROGUI_ShapeLandCoverMap*>( aShape );

    if ( !aLandCoverMapShape || !aLandCoverMapShape->isVisible() ) {
      continue;
    }

    Handle(HYDROData_LandCoverMap) aLandCoverMap =
      Handle(HYDROData_LandCoverMap)::DownCast( aLandCoverMapShape->getObject() );

    if ( aLandCoverMap.IsNull() ) {
      continue;
    }

    bool isScalarMode = aLandCoverMapShape->isScalarMapModeEnabled();
    if( isScalarMode != isLandCoverColoringOn )
    {
      aLandCoverMapShape->setScalarMapModeEnabled( isLandCoverColoringOn );
      theViewer->getAISContext()->Redisplay( aLandCoverMapShape->getAISObjects()[0], Standard_False );
    }
  }

  myToUpdateColorScale = false;
}

Handle(AIS_ColorScale) HYDROGUI_OCCDisplayer::GetColorScale( const size_t theViewerId )
{
  Handle(AIS_ColorScale) aColorScale;

  aColorScale = myColorScales.value( theViewerId, aColorScale );
  if ( aColorScale.IsNull() ) {
    // Create color scale
    aColorScale = new AIS_ColorScale();

    // Set properties
    Standard_Integer anXPos = 50; //TODO
    Standard_Integer anYPos = 100; //TODO
    Standard_Integer aWidth = 100; //TODO
    Standard_Integer aHeight = 350; //TODO

    Standard_Integer aTextHeight = 14; //TODO
    Standard_Integer aNbIntervals = 20; //TODO

    aColorScale->SetTransformPersistence( Graphic3d_TMF_2d, gp_Pnt( -1, -1, 0 ) );

    aColorScale->SetXPosition( anXPos );
    aColorScale->SetYPosition( anYPos );
    aColorScale->SetSize( aWidth, aHeight );

    aColorScale->SetTextHeight( aTextHeight );
    aColorScale->SetNumberOfIntervals( aNbIntervals );

    // Put into the map
    myColorScales.insert( theViewerId, aColorScale );
  }

  return aColorScale;
}

void HYDROGUI_OCCDisplayer::UpdatePolylines( size_t theViewerId, int theType, int theSize )
{
  OCCViewer_Viewer* aViewer = module()->getOCCViewer( theViewerId );
  if( !aViewer )
    return;

  // Get interactive context
  Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
  if( aCtx.IsNull() )
    return;

  AIS_ListOfInteractive objs;
  aCtx->DisplayedObjects( objs );
  AIS_ListOfInteractive::const_iterator it = objs.begin(), last = objs.end();
  for( ; it!=last; it++ )
  {
    Handle(HYDROGUI_Arrow) arr = Handle(HYDROGUI_Arrow)::DownCast( *it );
    if( !arr.IsNull() )
    {
      if( theType>=0 )
        arr->SetType( (HYDROGUI_Arrow::Type)theType );
      if( theSize>=0 )
        arr->SetSize( theSize );
      aCtx->Redisplay( arr, Standard_False );
    }
  }
  aCtx->UpdateCurrentViewer();
}

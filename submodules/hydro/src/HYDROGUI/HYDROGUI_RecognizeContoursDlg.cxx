// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_RecognizeContoursDlg.h"

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QVBoxLayout>

/**
 * Constructor.
 * \param theModule the module
 * \param theTitle the panel title
 */
HYDROGUI_RecognizeContoursDlg::HYDROGUI_RecognizeContoursDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  // Original image
  QGroupBox* anImageGroup = new QGroupBox( tr( "ORIGINAL_IMAGE" ), mainFrame() );
  QLabel* aNameLabel = new QLabel( tr("NAME") );
  myImageName = new QLineEdit;
  myImageName->setReadOnly( true );

  QHBoxLayout* anImageLayout = new QHBoxLayout();
  anImageLayout->addWidget( aNameLabel );
  anImageLayout->addWidget( myImageName );
  anImageGroup->setLayout( anImageLayout );

  // List of recognized polylines
  QGroupBox* aPolylinesGroup = new QGroupBox( tr( "RECOGNIZED_POLYLINES" ), mainFrame() );
  myPolylines = new QListWidget( aPolylinesGroup );
  myPolylines->setSelectionMode( QListWidget::ExtendedSelection );
  myPolylines->setEditTriggers( QListWidget::NoEditTriggers );
  myPolylines->setViewMode( QListWidget::ListMode );
  myPolylines->setSortingEnabled( false );

  QBoxLayout* aPolylinesLayout = new QVBoxLayout;
  aPolylinesLayout->addWidget( myPolylines );
  aPolylinesGroup->setLayout( aPolylinesLayout );

  // Layout
  addWidget( anImageGroup );
  addWidget( aPolylinesGroup );

  // Conections
  connect( myPolylines, SIGNAL( itemSelectionChanged() ), this, SLOT( onItemSelectionChanged() ) );
}

/**
 * Destructor.
 */
HYDROGUI_RecognizeContoursDlg::~HYDROGUI_RecognizeContoursDlg()
{
}

/**
 * Set the image name.
 * \param theName the name
 */
void HYDROGUI_RecognizeContoursDlg::setImageName( const QString& theName )
{
  myImageName->setText( theName );
}

/**
 * Reset the panel.
 */
void HYDROGUI_RecognizeContoursDlg::reset()
{
  myImageName->clear();
  myPolylines->clear();
}

/**
 * Refill the list of polylines names.
 * \param theNames the list of names
 */
void HYDROGUI_RecognizeContoursDlg::setPolylineNames( const QStringList& theNames )
{
  myPolylines->clear();
  myPolylines->addItems( theNames );
}

/**
 * Remove the given polyline names from the list.
 * \param theNames the list of names
 */
void HYDROGUI_RecognizeContoursDlg::removePolylineNames( const QStringList& theNames )
{
  QList<QListWidgetItem*> aFoundItems;

  foreach ( const QString& aName, theNames ) {
    aFoundItems = myPolylines->findItems( aName, Qt::MatchExactly );
    foreach ( QListWidgetItem* anItem, aFoundItems ) {
      anItem = myPolylines->takeItem( myPolylines->row( anItem ) );
      delete anItem;
    }
  }
}

/**
 * Select the given polyline names in the list.
 * \param theNames the list of names
 */
void HYDROGUI_RecognizeContoursDlg::setSelectedPolylineNames( const QStringList& theNames )
{
  myPolylines->clearSelection();

  foreach( const QString aName, theNames ) {
    QList<QListWidgetItem*> anItems = myPolylines->findItems( aName, Qt::MatchExactly );
    if ( anItems.count() == 1 ) {
      anItems.first()->setSelected( true );
    }
  }
}

/**
 * Slot called when polyline names selection changed.
 */
void HYDROGUI_RecognizeContoursDlg::onItemSelectionChanged()
{
  emit selectionChanged( getSelectedtPolylineNames() );
}

/**
 * Get the list of selected polyline names.
 * \return the list of names
 */ 
QStringList HYDROGUI_RecognizeContoursDlg::getSelectedtPolylineNames() const
{
  QStringList aSelectedNames;

  QList<QListWidgetItem*> aSelectedItems = myPolylines->selectedItems();
  foreach( const QListWidgetItem* anItem, aSelectedItems ) {
    aSelectedNames << anItem->text();
  }

  return aSelectedNames;
}
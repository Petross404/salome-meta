// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_SHAPE_H
#define HYDROGUI_SHAPE_H

#include <HYDROData_Entity.h>

#include <AIS_InteractiveContext.hxx>
#include <TopoDS_Shape.hxx>

#include <QColor>

class TopoDS_Compound;
class TopoDS_Face;
class TopoDS_Wire;

class HYDROGUI_Shape
{
public:
  HYDROGUI_Shape( const Handle(AIS_InteractiveContext)& theContext,
                  const Handle(HYDROData_Entity)&       theObject,
                  const int                             theZLayer = -1 );
  virtual ~HYDROGUI_Shape();

public:
  virtual void               display( const bool theIsUpdateViewer = true );
  virtual void               erase( const bool theIsUpdateViewer = true );

  virtual void               highlight( bool theIsHighlight, bool isUpdateViewer );
  virtual bool               isHighlighted() const;

  Handle(AIS_InteractiveContext) getContext() const;
  Handle(HYDROData_Entity)   getObject() const;
  TopoDS_Shape               getTopoShape() const;

  virtual void               update( bool isUpdateViewer,
                                     bool isDeactivateSelection );

  virtual bool               getIsToUpdate() const;
  virtual void               setIsToUpdate( bool theState );

  virtual bool               isVisible() const;
  virtual void               setVisible( const bool theState,
                                         const bool theIsUpdateViewer = true );

  virtual void               setWire( const TopoDS_Wire& theWire,
                                      const bool         theToDisplay = true,
                                      const bool         theIsUpdateViewer = true );

  virtual void               setFaces( const TopoDS_Compound& theWires,
                                       const bool             theToDisplay = true,
                                       const bool             theIsUpdateViewer = true );

  virtual void               setFace( const TopoDS_Wire& theWire,
                                      const bool         theToDisplay,
                                      const bool         theIsUpdateViewer,
                                      const QString&     theTextureFileName );

  virtual void               setFace( const TopoDS_Face& theFace,
                                      const bool         theToDisplay,
                                      const bool         theIsUpdateViewer,
                                      const QString&     theTextureFileName );

  virtual void               setShape( const TopoDS_Shape& theShape,
                                       const bool          theToDisplay = true,
                                       const bool          theIsUpdateViewer = true,
                                       const int           theDisplayMode = AIS_Shaded );

  virtual void               setFillingColor( const QColor& theColor,
                                              const bool    theToDisplay = true,
                                              const bool    theIsUpdateViewer = true );
  virtual QColor             getFillingColor() const;

  virtual void               setBorderColor( const QColor& theColor,
                                             const bool    theToDisplay = true,
                                             const bool    theIsUpdateViewer = true );
  virtual QColor             getBorderColor() const;

  virtual void               setHighlightColor( const QColor& theColor );
  virtual QColor             getHighlightColor() const;

  void                       setZLayer( const int theZLayer );
  void                       setSelectionMode( int theSelectionMode );

  QList<Handle(AIS_InteractiveObject)> getAISObjects() const;

protected:
  virtual void               buildShape();
  virtual void               updateShape( const bool theToDisplay      = true,
                                          const bool theIsUpdateViewer = true );
  virtual void               displayShape( const bool theIsUpdateViewer );
  virtual void               eraseShape( const bool theIsUpdateViewer );

  virtual QColor             getActiveColor() const;
  virtual QList<Handle(AIS_InteractiveObject)> createShape() const;

  void setDisplayMode( int theDisplayMode );

private:
  static double              getQuantityColorVal( const int theColorVal );
  void                       colorShapeBorder( const QColor& theColor );

private:
  Handle(AIS_InteractiveContext) myContext;
  Handle(HYDROData_Entity)       myObject;
  QList<Handle(AIS_InteractiveObject)>  myShapes;

  int                            myZLayer;

  bool                           myIsToUpdate;
  bool                           myIsVisible;

  bool                           myIsHighlight;
  TopoDS_Shape                   myTopoShape;
  int                            myDisplayMode;
  int                            mySelectionMode;
  
  QColor                         myFillingColor;
  QColor                         myBorderColor;
  QColor                         myHighlightColor;
};

#endif


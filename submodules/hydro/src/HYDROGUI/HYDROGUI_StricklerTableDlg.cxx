// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_StricklerTableDlg.h>
#include <HYDROGUI_LineEditDoubleValidator.h>
#include <HYDROGUI_Tool.h>
#include <HYDROData_StricklerTable.h>
#include <SUIT_FileDlg.h>
#ifndef LIGHT_MODE
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#endif
#include <QBoxLayout>
#include <QColorDialog>
#include <QGroupBox>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QTableWidget>
#include <QToolButton>

HYDROGUI_ColorDelegate::HYDROGUI_ColorDelegate( QWidget* theParent )
{
}

HYDROGUI_ColorDelegate::~HYDROGUI_ColorDelegate()
{
}

void HYDROGUI_ColorDelegate::paint( QPainter* thePainter, const QStyleOptionViewItem& theOption,
                                    const QModelIndex& theIndex ) const
{
  QColor aColor = theIndex.data( Qt::BackgroundColorRole ).value<QColor>();
  thePainter->fillRect( theOption.rect, aColor );
}

QWidget* HYDROGUI_ColorDelegate::createEditor( QWidget* theParent,
                                               const QStyleOptionViewItem& theOption,
                                               const QModelIndex& theIndex ) const
{
  QColor aColor = theIndex.data( Qt::BackgroundColorRole ).value<QColor>();
  QColor aNewColor = QColorDialog::getColor( aColor );
  if( aNewColor.isValid() )
  {
    QAbstractItemModel* aModel = const_cast<QAbstractItemModel*>( theIndex.model() );
    QVariant aValue = qVariantFromValue( aNewColor );
    aModel->setData( theIndex, aValue, Qt::BackgroundColorRole );
  }
  return 0;
}

void HYDROGUI_ColorDelegate::setEditorData( QWidget* theEditor, const QModelIndex& theIndex ) const
{
}

void HYDROGUI_ColorDelegate::setModelData( QWidget* theEditor, QAbstractItemModel* theModel,
                                           const QModelIndex& theIndex ) const
{
}

QSize	HYDROGUI_ColorDelegate::sizeHint( const QStyleOptionViewItem& theOption, const QModelIndex& theIndex ) const
{
  return theOption.rect.size();
}



const int COLUMNS_COUNT = 4;


HYDROGUI_StricklerTableDlg::HYDROGUI_StricklerTableDlg( HYDROGUI_Module* theModule, const QString& theTitle, int theType )
: HYDROGUI_InputPanel( theModule, theTitle ),
  myType( theType )
{
    QString fileGroupTitle = theType == Export ? tr( "EXPORT_STRICKLER_TABLE_FILE" ) : tr( "IMPORT_STRICKLER_TABLE_FILE" );

    // Import Strickler table from file
    QGroupBox* aFileNameGroup = new QGroupBox( fileGroupTitle, this );
    aFileNameGroup->setVisible( theType != Edit );

    QLabel* aFileNameLabel = new QLabel( tr( "FILE_NAME" ), aFileNameGroup );

    myFileName = new QLineEdit( aFileNameGroup );
    myFileName->setReadOnly( true );

    QToolButton* aBrowseBtn = new QToolButton( aFileNameGroup );
#ifndef LIGHT_MODE
    SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
    aBrowseBtn->setIcon( aResMgr->loadPixmap( "HYDRO", tr( "BROWSE_ICO" ) ) );
#endif

    QBoxLayout* aFileNameLayout = new QHBoxLayout( aFileNameGroup );
    aFileNameLayout->setMargin( 5 );
    aFileNameLayout->setSpacing( 5 );
    aFileNameLayout->addWidget( aFileNameLabel );
    aFileNameLayout->addWidget( myFileName );
    aFileNameLayout->addWidget( aBrowseBtn );

    // Strickler table name
    QGroupBox* aNameGroup = new QGroupBox( tr( "STRICKLER_TABLE_NAME" ), this );

    QLabel* anImageNameLabel = new QLabel( tr( "NAME" ), aNameGroup );
    myName = new QLineEdit( aNameGroup );

    QGroupBox* aAttrNameGroup = new QGroupBox( tr( "STRICKLER_TABLE_ATTR_NAME" ), this );
    aAttrNameGroup->setVisible( theType == Edit );
    QLabel* aAttrNameLabel = new QLabel( tr( "ATTR_NAME" ), aAttrNameGroup );
    myAttrName = new QLineEdit( aAttrNameGroup );

    QBoxLayout* anImageNameLayout = new QHBoxLayout( aNameGroup );
    anImageNameLayout->setMargin( 5 );
    anImageNameLayout->setSpacing( 5 );
    anImageNameLayout->addWidget( anImageNameLabel );
    anImageNameLayout->addWidget( myName );

    QBoxLayout* anAttrNameLayout = new QHBoxLayout( aAttrNameGroup );
    anAttrNameLayout->setMargin( 5 );
    anAttrNameLayout->setSpacing( 5 );
    anAttrNameLayout->addWidget( aAttrNameLabel );
    anAttrNameLayout->addWidget( myAttrName );

    // Strickler table
    QGroupBox* aTableGroup = new QGroupBox( tr( "STRICKLER_TABLE_TABLE" ), this );
    aTableGroup->setVisible( theType == Edit );

    // Main layout
    QVBoxLayout* aTableLayout = new QVBoxLayout( aTableGroup );
    aTableLayout->setMargin( 5 );
    aTableLayout->setSpacing( 5 );

    // Buttons
    myAddBtn = new QToolButton;
    myAddBtn->setText( tr( "ADD" ) );
    myRemoveBtn = new QToolButton;
    myRemoveBtn->setText( tr( "REMOVE" ) );
    myClearBtn = new QToolButton;
    myClearBtn->setText( tr( "CLEAR_ALL" ) );

    // Table
    myTable = new QTableWidget( mainFrame() );
    myTable->setItemDelegateForColumn( 1, new HYDROGUI_LineEditDoubleValidator( this ) );
    myTable->setItemDelegateForColumn( 3, new HYDROGUI_ColorDelegate( this ) );
    myTable->setEditTriggers( QAbstractItemView::DoubleClicked |
        QAbstractItemView::SelectedClicked |
        QAbstractItemView::EditKeyPressed );

    myTable->setColumnCount( COLUMNS_COUNT );

    QStringList aColumnNames;
    aColumnNames << tr( "STRICKLER_TYPE" ) << tr( "STRICKLER_COEFFICIENT" ) << tr( "ATTR_VALUE" ) << tr( "COLOR" );
    myTable->setHorizontalHeaderLabels( aColumnNames );

    myTable->horizontalHeader()->setStretchLastSection( false );
    myTable->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Interactive );
    myTable->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Interactive );
    myTable->horizontalHeader()->setSectionResizeMode( 2, QHeaderView::Interactive );
    myTable->horizontalHeader()->setSectionResizeMode( 3, QHeaderView::Interactive );
    myTable->verticalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );

    const int default_size = 50;
    myTable->horizontalHeader()->setMinimumSectionSize( default_size  );
    myTable->horizontalHeader()->resizeSection( 0, 300 );
    myTable->horizontalHeader()->resizeSection( 1, 75 );
    myTable->horizontalHeader()->resizeSection( 2, 75 );
    myTable->horizontalHeader()->resizeSection( 3, 75 );

    // Layout
    // buttons
    QHBoxLayout* aButtonsLayout = new QHBoxLayout();
    aButtonsLayout->addWidget( myAddBtn );
    aButtonsLayout->addWidget( myRemoveBtn );
    aButtonsLayout->addStretch( 1 );
    aButtonsLayout->addWidget( myClearBtn );

    // main
    aTableLayout->addLayout( aButtonsLayout );
    aTableLayout->addWidget( myTable );

    // Common
    addWidget( aFileNameGroup );
    addWidget( aNameGroup );
    addWidget( aAttrNameGroup );
    addWidget( aTableGroup );

    // Update controls
    updateControls();

    // Connections
    connect( aBrowseBtn, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
    connect( myAddBtn, SIGNAL( clicked() ), this, SLOT( onAddCoefficient() ) );
    connect( myRemoveBtn, SIGNAL( clicked() ), this, SLOT( onRemoveCoefficient() ) );
    connect( myClearBtn, SIGNAL( clicked() ), this, SLOT( onClearCoefficients() ) );
    connect( myTable->selectionModel(), SIGNAL( selectionChanged( QItemSelection, QItemSelection ) ),
        this, SLOT( onSelectionChanged() ) );

    setMinimumWidth( 450 );

    if ( theType != Edit )
        addStretch();
}

HYDROGUI_StricklerTableDlg::~HYDROGUI_StricklerTableDlg()
{
}

void HYDROGUI_StricklerTableDlg::reset()
{
    myFileName->clear();

    myName->clear();
}

QString HYDROGUI_StricklerTableDlg::getFileName() const
{
    return myFileName->text();
}

void HYDROGUI_StricklerTableDlg::setFileName( const QString& theName )
{
    myFileName->setText( theName );
}

void HYDROGUI_StricklerTableDlg::setTableName( const QString& theName )
{
    myName->setText(theName);
}

QString HYDROGUI_StricklerTableDlg::getTableName() const
{
    return myName->text();
}

bool HYDROGUI_StricklerTableDlg::isTableNameReadOnly() const
{
    return myName->isReadOnly();
}

void HYDROGUI_StricklerTableDlg::setTableNameReadOnly( bool on )
{
    myName->setReadOnly( on );
}

void HYDROGUI_StricklerTableDlg::getGuiData( Handle(HYDROData_StricklerTable)& theTable ) const
{
  if( theTable.IsNull() )
    return;

  theTable->SetAttrName( myAttrName->text() );
  theTable->Clear();
  for ( int i = 0; i < myTable->rowCount(); i++ )
  {
    QTableWidgetItem* typeItem = myTable->item( i, 0 );
    QString aType = typeItem->data( Qt::DisplayRole ).toString();

    QTableWidgetItem* coeffItem = myTable->item( i, 1 );
    QString aCoeffStr = coeffItem->data( Qt::DisplayRole ).toString();
    double aCoeff = aCoeffStr.toDouble();

    QTableWidgetItem* attrValueItem = myTable->item( i, 2 );
    QString anAttrValue = attrValueItem->data( Qt::DisplayRole ).toString();

    QTableWidgetItem* colorItem = myTable->item( i, 3 );
    QColor aColor = colorItem->backgroundColor();

    theTable->Set( aType, aCoeff );
    theTable->SetAttrValue( aType, anAttrValue );
    theTable->SetColor( aType, aColor );
  }
}

void HYDROGUI_StricklerTableDlg::setGuiData( const Handle(HYDROData_StricklerTable)& theTable )
{
  myAttrName->setText( theTable->GetAttrName() );

  if( theTable.IsNull() )
  {
    myTable->setRowCount( 0 );
    return;
  }

  QStringList aTypes = theTable->GetTypes();
  myTable->setRowCount( 0 );
  foreach( QString aType, aTypes )
  {
    // Check the current Strickler type
    if( aType.isEmpty() )
      continue;

    // Get Strickler data for the current Strickler type
    QString aCoefficient = HYDROGUI_Tool::GetCoordinateString( theTable->Get( aType, 0.0 ), false );
    QString anAttrValue = theTable->GetAttrValue( aType );
    QColor aColor = theTable->GetColor( aType );

    // Insert row with the data
    int aRow = myTable->rowCount();
    myTable->insertRow( aRow );

    // "Type" column
    QTableWidgetItem* aTypeItem = new QTableWidgetItem( aType );
    myTable->setItem( aRow, 0, aTypeItem );

    // "Coefficient" column
    myTable->setItem( aRow, 1, new QTableWidgetItem( aCoefficient ) );

    // "Attribute value" column
    myTable->setItem( aRow, 2, new QTableWidgetItem( anAttrValue ) );

    // "Color" column
    QTableWidgetItem* anItem = new QTableWidgetItem();
    anItem->setBackgroundColor( aColor );
    myTable->setItem( aRow, 3, anItem );
  }

  //myTable->resizeColumnToContents( 0 );
  myTable->resizeRowsToContents();

  updateControls();
}

void HYDROGUI_StricklerTableDlg::updateControls()
{
    bool isTableNotEmpty = myTable->rowCount() > 0;
    myClearBtn->setEnabled( isTableNotEmpty );
    onSelectionChanged();
}

void HYDROGUI_StricklerTableDlg::removeRows( const QList<int> theRows )
{
    QList<int> aSortedRows = theRows;
    qSort( aSortedRows );

    int aRowToRemove = -1;
    int aNbRemoved = 0;
    foreach ( int aRow, aSortedRows ) {
        aRowToRemove = aRow - aNbRemoved;
        if ( myTable->model()->removeRow( aRowToRemove ) ) {
            aNbRemoved++;
        }
    }

    if ( aNbRemoved > 0 )
        updateControls();
}

void HYDROGUI_StricklerTableDlg::onBrowse()
{
    QString aFilter( tr( "STRICKLER_TABLE_FILTER" ) );
    QString aFileName = SUIT_FileDlg::getFileName( this, "", aFilter, myFileName->parent()->property( "title" ).toString(), myType == Import );
    if ( !aFileName.isEmpty() )
    {
        setFileName( aFileName );
        emit fileSelected( aFileName );
    }
}

/**
Add the new default constructed Strickler coefficient.
*/
void HYDROGUI_StricklerTableDlg::onAddCoefficient()
{
    int aRow = myTable->rowCount();
    myTable->insertRow( aRow );

    for( int i=0; i<COLUMNS_COUNT; i++ )
    {
      myTable->setItem( aRow, i, new QTableWidgetItem() );
    }

    // Set default type (=> generate unique type name) and coefficient
    //...

    updateControls();
}

/**
Remove the selected Strickler coefficient.
*/
void HYDROGUI_StricklerTableDlg::onRemoveCoefficient()
{
    QList<int> aRows;
    QModelIndexList aSelectedIndexes = myTable->selectionModel()->selectedIndexes();
    foreach ( const QModelIndex& anIndex, aSelectedIndexes ) {
      int aRowIndex = anIndex.row();
      if ( !aRows.contains( aRowIndex ) )
        aRows << aRowIndex;
    }

    removeRows( aRows );
}

/**
Clear all Strickler coefficients.
*/
void HYDROGUI_StricklerTableDlg::onClearCoefficients()
{
    QList<int> aRows;
    for ( int i=0; i< myTable->rowCount(); i++ ) {
        aRows << i;
    }

    removeRows( aRows );
}

/**
Slot called on table selection change.
*/
void HYDROGUI_StricklerTableDlg::onSelectionChanged()
{
    QModelIndexList aSelectedIndexes = myTable->selectionModel()->selectedIndexes();
    myRemoveBtn->setEnabled( aSelectedIndexes.count() > 0 );
}

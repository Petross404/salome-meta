// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_ImmersibleZoneOp_H
#define HYDROGUI_ImmersibleZoneOp_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_ImmersibleZone.h>

class HYDROGUI_Shape;

class HYDROGUI_ImmersibleZoneOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_ImmersibleZoneOp( HYDROGUI_Module* theModule, const bool theIsEdit );
  virtual ~HYDROGUI_ImmersibleZoneOp();

protected:
  virtual void               startOperation();
  virtual void               abortOperation();
  virtual void               commitOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool               processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries );

  virtual HYDROGUI_Shape*   getPreviewShape() const { return myPreviewPrs; };

protected slots:
  void                       onCreatePreview( const QString& thePolylineName );

private:
  void                       closePreview();

private:
  bool                             myIsEdit;
  Handle(HYDROData_ImmersibleZone) myEditedObject;

  HYDROGUI_Shape*                  myPreviewPrs;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_STRICKLERTABLEDLG_H
#define HYDROGUI_STRICKLERTABLEDLG_H

#include "HYDROGUI_InputPanel.h"
#include <QAbstractItemDelegate>
#include <Standard_Type.hxx>

class HYDROData_StricklerTable;

class QGroupBox;
class QLineEdit;
class QToolButton;
class QTableWidget;
class QtxFilePath;

class HYDROGUI_StricklerTableDlg : public HYDROGUI_InputPanel
{
    Q_OBJECT

public:
    enum { Edit, Import, Export };

public:
    HYDROGUI_StricklerTableDlg( HYDROGUI_Module* theModule, const QString& theTitle, int theType );
    virtual ~HYDROGUI_StricklerTableDlg();

    void                       reset();

    QString                    getFileName() const;
    void                       setFileName( const QString& theName );

    void                       setTableName( const QString& theName );
    QString                    getTableName() const;

    bool                       isTableNameReadOnly() const;
    void                       setTableNameReadOnly( bool );

    void                       getGuiData( Handle(HYDROData_StricklerTable)& theTable ) const;
    void                       setGuiData( const Handle(HYDROData_StricklerTable)& theTable );

protected:
    void                       updateControls();
    void                       removeRows( const QList<int> theRows );

protected Q_SLOTS:
    void                       onBrowse();

    void                       onAddCoefficient();
    void                       onRemoveCoefficient();
    void                       onClearCoefficients();

    void                       onSelectionChanged();

Q_SIGNALS:
    void                       fileSelected( const QString& theFileName );

private:
  friend class test_HYDROData_StricklerTable;

private:
  int                        myType;
  QLineEdit*                 myFileName;   //!< Source Strickler table file name input field
  QLineEdit*                 myName;       //!< The Strickler table name input field
  QLineEdit*                 myAttrName;   //!< The Strickler table attribute's name input field
  QTableWidget*              myTable;      //!< The table of Strickler coefficients
  QToolButton*               myAddBtn;     //!< The add Strickler coefficient button
  QToolButton*               myRemoveBtn;  //!< The remove Strickler coefficient button
  QToolButton*               myClearBtn;   //!< The clear all Strickler coefficients button
};

class HYDROGUI_ColorDelegate : public QAbstractItemDelegate
{
public:
  HYDROGUI_ColorDelegate( QWidget* theParent );
  virtual ~HYDROGUI_ColorDelegate();

  virtual void paint( QPainter* thePainter, const QStyleOptionViewItem& theOption,
                      const QModelIndex& theIndex ) const;

  virtual QWidget* createEditor( QWidget* theParent,
                                 const QStyleOptionViewItem& theOption,
                                 const QModelIndex& theIndex ) const;

  virtual void setEditorData( QWidget* theEditor, const QModelIndex& theIndex ) const;

  virtual void setModelData( QWidget* theEditor, QAbstractItemModel* theModel,
                             const QModelIndex& theIndex ) const;

  virtual QSize	sizeHint( const QStyleOptionViewItem& theOption, const QModelIndex& theIndex ) const;
};

#endif

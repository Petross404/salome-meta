// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_TwoImagesOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_TwoImagesDlg.h"
#include "HYDROGUI_UpdateFlags.h"
#include <HYDROGUI_DataObject.h>

#include <HYDROData_Document.h>
#include <HYDROData_Image.h>

#include <HYDROData_OperationsFactory.h>

#include <ImageComposer_CutOperator.h>
#include <ImageComposer_CropOperator.h>
#include <ImageComposer_FuseOperator.h>

#include <LightApp_Application.h>
#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>

HYDROGUI_TwoImagesOp::HYDROGUI_TwoImagesOp( HYDROGUI_Module* theModule,
                                            const int theType,
                                            const bool theIsEdit )
: HYDROGUI_Operation( theModule ),
  myType( theType ),
  myIsEdit( theIsEdit ),
  myEditedObject( 0 )
{
  QString aName;
  switch( myType )
  {
    case Fuse: aName = theIsEdit ? tr( "EDIT_FUSED_IMAGE" ) : tr( "FUSE_IMAGES" ); break;
    case Cut: aName = theIsEdit ? tr( "EDIT_CUT_IMAGE" ) : tr( "CUT_IMAGES" ); break;
    case Split: aName = theIsEdit ? tr( "EDIT_SPLIT_IMAGE" ) : tr( "SPLIT_IMAGE" ); break;
    default: break;
  }
  setName( aName );
}

HYDROGUI_TwoImagesOp::~HYDROGUI_TwoImagesOp()
{
}

HYDROGUI_InputPanel* HYDROGUI_TwoImagesOp::createInputPanel() const
{
  return new HYDROGUI_TwoImagesDlg( module(), getName() );
}

void HYDROGUI_TwoImagesOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_TwoImagesDlg* aPanel = (HYDROGUI_TwoImagesDlg*)inputPanel();
  aPanel->reset();

  int aMode;
  if( myType == Fuse )
    aMode = HYDROGUI_TwoImagesDlg::TwoFuseImage;
  if ( myType == Cut )
    aMode = HYDROGUI_TwoImagesDlg::TwoCutImage;
  else if( myType == Split )
    aMode = HYDROGUI_TwoImagesDlg::ImageAndPolyline;
  aPanel->setMode( aMode, myIsEdit );

  QString anImageName;
  if( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_Image)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !myEditedObject.IsNull() )
      anImageName = myEditedObject->GetName();
  }
  else
  {
    QString aPrefix;
    switch( myType )
    {
      case Fuse: aPrefix = tr( "FUSE" ); break;
      case Cut: aPrefix = tr( "CUT" ); break;
      case Split: aPrefix = tr( "SPLIT" ); break;
      default: break;
    }
    anImageName = HYDROGUI_Tool::GenerateObjectName( module(), aPrefix );
  }
  aPanel->setImageName( anImageName );

  QString aSelectedName1, aSelectedName2;
  if( myIsEdit && !myEditedObject.IsNull() )
  {
    if( myEditedObject->NbReferences() > 0 )
    {
      Handle(HYDROData_Entity) anObject1 = myEditedObject->Reference( 0 );
      if( !anObject1.IsNull() )
        aSelectedName1 = anObject1->GetName();
    }
    if( myEditedObject->NbReferences() > 1 )
    {
      Handle(HYDROData_Entity) anObject2 = myEditedObject->Reference( 1 );
      if( !anObject2.IsNull() )
        aSelectedName2 = anObject2->GetName();
    }
    aPanel->setSelectedObjects( aSelectedName1, aSelectedName2 );

    HYDROData_OperationsFactory* aFactory = HYDROData_OperationsFactory::Factory();
    if( ImageComposer_Operator* anOperator = aFactory->Operator( myEditedObject ) )
    {
      QColor aColor;
      anOperator->getArgs( aColor );
      aPanel->setColor( aColor );
    }
  }
  else if( !myIsEdit )
  {
    Handle(HYDROData_Image) aSelectedImage =
      Handle(HYDROData_Image)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !aSelectedImage.IsNull() )
    {
      QString aSelectedName = aSelectedImage->GetName();
      aPanel->setPreselectedObject( aSelectedName );
    }
  }
  connect( aPanel, SIGNAL( alreadySelected( const QString& ) ), SLOT( onAlreadySelected( const QString& ) ) );
}

void HYDROGUI_TwoImagesOp::onAlreadySelected( const QString& theName )
{
  QString aTitle = tr( "INSUFFICIENT_INPUT_DATA" );
  QString aMessage = tr( "OBJECT_ALREADY_SELECTED" ).arg( theName );
  SUIT_MessageBox::critical( module()->getApp()->desktop(), aTitle, aMessage );
}

bool HYDROGUI_TwoImagesOp::processApply( int& theUpdateFlags,
                                         QString& theErrorMsg,
                                         QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_TwoImagesDlg* aPanel = dynamic_cast<HYDROGUI_TwoImagesDlg*>( inputPanel() );

  bool anIsModifySelected = myType == Split && aPanel->isModifySelected();

  QString anImageName = aPanel->getImageName();
  if( !anIsModifySelected && anImageName.isEmpty() )
    return false;

  QString aSelectedName1, aSelectedName2;
  if( !aPanel->getSelectedObjects( aSelectedName1, aSelectedName2 ) )
    return false;

  if( !anIsModifySelected &&
      ( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anImageName ) ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anImageName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anImageName );
      return false;
    }
  }

  Handle(HYDROData_Entity) anObject1 =
    HYDROGUI_Tool::FindObjectByName( module(), aSelectedName1, KIND_UNKNOWN ) ;
  Handle(HYDROData_Entity) anObject2 =
    HYDROGUI_Tool::FindObjectByName( module(), aSelectedName2, KIND_UNKNOWN );
  if( anObject1.IsNull() || anObject2.IsNull() )
    return false;

  HYDROData_OperationsFactory* aFactory = HYDROData_OperationsFactory::Factory();

  Handle(HYDROData_Image) aResult;
  ImageComposer_Operator* anOperator = 0;
  if( myIsEdit )
  {
    aResult = myEditedObject;
    aResult->ClearReferences();
    anOperator = aFactory->Operator( aResult );
  }
  else
  {
    QString anOperatorName;
    switch( myType )
    {
      case Fuse:  anOperatorName = ImageComposer_FuseOperator::Type(); break;
      case Cut:   anOperatorName = ImageComposer_CutOperator::Type(); break;
      case Split: anOperatorName = ImageComposer_CropOperator::Type(); break;
      default: break;
    }

    anOperator = aFactory->Operator( anOperatorName );

    aResult = aFactory->CreateImage( doc(), anOperator );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aResult );
    theBrowseObjectsEntries.append( anEntry );
  }

  if( aResult.IsNull() || !anOperator )
    return false;

  // Setting the operator arguments 
  anOperator->setArgs( aPanel->getColor() );
  aResult->SetArgs( anOperator->getBinArgs() );

  aResult->SetName( anImageName );
  aResult->AppendReference( anObject1 );
  aResult->AppendReference( anObject2 );

  aResult->Update();

  if( anIsModifySelected )
  {
    Handle(HYDROData_Image) aSelectedImage = Handle(HYDROData_Image)::DownCast( anObject1 );
    if( !aSelectedImage.IsNull() )
    {
      aSelectedImage->SetIsSelfSplit( true );
      aSelectedImage->SetImage( aResult->Image() );
      aSelectedImage->SetTrsf( aResult->Trsf() );
      aResult->Remove();
    }
  }

  if( !myIsEdit && !anIsModifySelected )
  {
    size_t aViewId = HYDROGUI_Tool::GetActiveGraphicsViewId( module() );
    module()->setObjectVisible( aViewId, anObject1, false );
    module()->setObjectVisible( aViewId, anObject2, false );
    module()->setObjectVisible( aViewId, aResult, true );
  }

  theUpdateFlags = UF_Model | UF_Viewer | UF_GV_Forced | UF_OCCViewer | UF_OCC_Forced;
  return true;
}

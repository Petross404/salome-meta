// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_TranslateObstacleDlg.h"

#include <QtxDoubleSpinBox.h>

#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QGridLayout>

#include <limits>

HYDROGUI_TranslateObstacleDlg::HYDROGUI_TranslateObstacleDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  // Obstacle name
  QGroupBox* aNameGroup = new QGroupBox( tr( "OBSTACLE_NAME" ), this );

  QLabel* aNameLabel = new QLabel( tr( "NAME" ), aNameGroup );
  myName = new QLineEdit( aNameGroup );
  myName->setEnabled( false );
  
  QBoxLayout* aNameLayout = new QHBoxLayout( aNameGroup );
  aNameLayout->setMargin( 5 );
  aNameLayout->setSpacing( 5 );
  aNameLayout->addWidget( aNameLabel );
  aNameLayout->addWidget( myName );

  // Translation arguments
  QGroupBox* anArgGroup = new QGroupBox( tr( "TRANSLATION_ARGUMENTS" ), this );

  QLabel* aDxLabel = new QLabel( tr( "DX" ), anArgGroup );
  myDxSpin = new QtxDoubleSpinBox( -1e+15, +1e+15, 10, anArgGroup );

  QLabel* aDyLabel = new QLabel( tr( "DY" ), anArgGroup );
  myDySpin = new QtxDoubleSpinBox( -1e+15, +1e+15, 10, anArgGroup );

  QLabel* aDzLabel = new QLabel( tr( "DZ" ), anArgGroup );
  myDzSpin = new QtxDoubleSpinBox( -1e+15, +1e+15, 10, anArgGroup );

  QGridLayout* anArgLayout = new QGridLayout( anArgGroup );
  anArgLayout->setMargin( 5 );
  anArgLayout->setSpacing( 5 );
  
  anArgLayout->addWidget( aDxLabel, 0, 0 );
  anArgLayout->addWidget( myDxSpin, 0, 1 );
  anArgLayout->addWidget( aDyLabel, 1, 0 );
  anArgLayout->addWidget( myDySpin, 1, 1 );
  anArgLayout->addWidget( aDzLabel, 2, 0 );
  anArgLayout->addWidget( myDzSpin, 2, 1 );

  // Layout
  addWidget( aNameGroup );
  addWidget( anArgGroup );
  addStretch();

  // Connect signals and slots
  connect( myDxSpin, SIGNAL( valueChanged( double ) ), this, SIGNAL( argumentsChanged() ) );
  connect( myDySpin, SIGNAL( valueChanged( double ) ), this, SIGNAL( argumentsChanged() ) );
  connect( myDzSpin, SIGNAL( valueChanged( double ) ), this, SIGNAL( argumentsChanged() ) );
}

HYDROGUI_TranslateObstacleDlg::~HYDROGUI_TranslateObstacleDlg()
{
}

void HYDROGUI_TranslateObstacleDlg::reset()
{
  myName->clear();

  myDxSpin->setValue( 0 );
  myDySpin->setValue( 0 );
  myDzSpin->setValue( 0 );
}

void HYDROGUI_TranslateObstacleDlg::setName( const QString& theName )
{
  myName->setText( theName );
}

double HYDROGUI_TranslateObstacleDlg::getDx() const
{
  return myDxSpin->value();
}

double HYDROGUI_TranslateObstacleDlg::getDy() const
{
  return myDySpin->value();
}

double HYDROGUI_TranslateObstacleDlg::getDz() const
{
  return myDzSpin->value();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_IMPORTIMAGEDLG_H
#define HYDROGUI_IMPORTIMAGEDLG_H

#include "HYDROGUI_InputPanel.h"
#include <HYDROData_Image.h>

#include <QMap>

#define LAMBERT_X0 700000
#define LAMBERT_Y0 6600000
#define LAMBERT_LONG 3
#define LAMBERT_LATT 46.5
#define IMG_DELTA 500

class QButtonGroup;
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QCheckBox;

class QtxDoubleSpinBox;
class QtxIntSpinBox;
class QAbstractSpinBox;
class QToolButton;

class HYDROGUI_ImportImageDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  struct TransformationData
  {
    QPoint  ImagePoint;
    QPointF GeodesicPoint;
    QPointF CartesianPoint;

    TransformationData() {}

    TransformationData( const QPoint& theImagePoint ) :
      ImagePoint( theImagePoint ) {}

    TransformationData( const QPoint&  theImagePoint,
                        const QPointF& theGeodesicPoint,
                        const QPointF& theCartesianPoint ) :
      ImagePoint( theImagePoint ),
      GeodesicPoint( theGeodesicPoint ),
      CartesianPoint( theCartesianPoint ) {}
  };
  typedef QMap< int, TransformationData > TransformationDataMap;

  typedef QPair< QString, TransformationDataMap > PrsPointData;
  typedef QList< PrsPointData >                   PrsPointDataList;
  typedef QListIterator< PrsPointData >           PrsPointDataListIterator;

public:
  HYDROGUI_ImportImageDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_ImportImageDlg();

  void                       setIsEdit( const bool theIsEdit );
  void                       reset();

  void                       setImageName( const QString& theName );
  QString                    getImageName() const;

  void                       setRefImageName( const QString& theName );
  QString                    getRefImageName() const;

  QString                    getFileName() const;
  void                       setFileName( const QString& theName );
  QString                    getGeoreferencementFileName() const;

  void                       setImageSize( const QSize& theSize,
                                           const bool theIsRefImage = false );

  void                       setTransformationMode( const int theMode );
  int                        getTransformationMode() const;

  void                       setByTwoPoints( const bool theIsByTwoPoints );
  bool                       isByTwoPoints() const;

  void                       setTransformationDataMap( const TransformationDataMap& theMap,
                                                       const bool theIsOnlyInput = false,
                                                       const bool theIsRefImage = false );
  bool                       getTransformationDataMap( TransformationDataMap& theMap,
                                                       const bool theIsRefImage = false ) const;

  void                       setPrsPointDataList( const PrsPointDataList& theList );

  void                       initializePointSelection();


  void                       ECW_initializePointSelection(HYDROData_Image::ECW_FileInfo* theECW_FileInfo);


  void                       ActivateFile( const QString& theFileName, bool isEnableFilesChoice );

public:

  static TransformationData  ComputeTrsfData( const int      theMode,
                                              const QPoint&  theLocalPoint,
                                              const QPointF& theGlobalPoint );

protected slots:
  void                       onBrowse();
  void                       onGeoBrowse();
  void                       onModeActivated( int );
  void                       onRefImageActivated( const QString& );
  void                       onPointBtnToggled( bool );
  void                       onPointCoordChanged( int );
  void                       onSetCIsUsed( bool theCIsUsed );

  void                       onGeodesicCoordChanged();
  void                       onGeodesicCoordChanged( const int thePointType );

  void                       onCartesianCoordChanged();
  void                       onCartesianCoordChanged( const int thePointType );

private:
  void                       clearSpins( QAbstractSpinBox* theSpin );
  bool                       blockSignalsPoints( const bool theState );
  bool                       blockSignalsGeodesic( const bool theState );
  bool                       blockSignalsCartesian( const bool theState );

signals:
  void                       createPreview( QImage, HYDROData_Image::ECW_FileInfo* );
  void                       activatePointSelection( int );
  void                       pointCoordChanged( bool theIsRef,
                                                int thePointType,
                                                bool theIsY,
                                                int theValue );
  void                       modeActivated( int );
  void                       refImageActivated( const QString& );
  void                       setCIsUsed( bool theIsByTwoPoints );
  void                       filesSelected( const QStringList& );

private:
  QGroupBox*                   myFileNameGroup;    //!< The group for the source image file selection
  QLineEdit*                   myFileName;         //!< Source image file name input field
  QToolButton*                 myBrowseBtn;

  QGroupBox*                   myImageNameGroup;   //!< The group for the image name input field
  QLineEdit*                   myImageName;        //!< The image name input field

  QGroupBox*                   myTransformGroup;   //!< The group of input contols for points definition

  QButtonGroup*                myModeGroup;        //!< The group for the input mode selector
  QComboBox*                   myRefImage;         //!< Reference image selector

  QLineEdit*                   myGeoFileName;      //!< Image georeferencement file name input field

  QList<QLabel*>               myGeodesicLabels;   //!< Labels for geodesic coords input fields

  QMap<int, QPushButton*>      myPointBtnMap;      //!< A,B,C points selection modes activators
  QMap<int, QtxIntSpinBox*>    myPointXMap;        //!< X coord on the image
  QMap<int, QtxIntSpinBox*>    myPointYMap;        //!< Y coord on the image
  QMap<int, QtxIntSpinBox*>    myPointXDegMap;     //!< Longitude degrees
  QMap<int, QtxIntSpinBox*>    myPointYDegMap;     //!< Latitude degrees
  QMap<int, QtxIntSpinBox*>    myPointXMinMap;     //!< Longitude minutes
  QMap<int, QtxIntSpinBox*>    myPointYMinMap;     //!< Latitude minutes
  QMap<int, QtxDoubleSpinBox*> myPointXSecMap;     //!< Longitude seconds
  QMap<int, QtxDoubleSpinBox*> myPointYSecMap;     //!< Latitude seconds

  QMap<int, QtxDoubleSpinBox*> myCartPointXMap;    //!< Lambert93 (cartesian) X coord (m)
  QMap<int, QtxDoubleSpinBox*> myCartPointYMap;    //!< Lambert93 (cartesian) Y coord (m)

  QMap<int, QtxIntSpinBox*>    myRefPointXMap;     //!< X coord on the reference image
  QMap<int, QtxIntSpinBox*>    myRefPointYMap;     //!< Y coord on the reference image

  QCheckBox*                   myPointCEnabler;    //!< Checkbox for enabling definition of the point C

  PrsPointDataList             myPrsPointDataList; //!< Points presentations

  bool                         myIsInitialized;    //!< True if a point selection is activated
};

#endif

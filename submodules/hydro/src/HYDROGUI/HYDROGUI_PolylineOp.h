// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_POLYLINEOP_H
#define HYDROGUI_POLYLINEOP_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_PolylineXY.h>

class CurveCreator_Curve;

class HYDROGUI_PolylineOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_PolylineOp( HYDROGUI_Module* theModule, bool isEdit );
  virtual ~HYDROGUI_PolylineOp();

  void                       deleteSelected();
  bool                       deleteEnabled();

  virtual void               updatePreviewZLayer( int theLayer );

protected:
  virtual void               startOperation();
  virtual void               abortOperation();
  virtual void               commitOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool               processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries );

protected slots:
  void                       onEditorSelectionChanged();

private:
  void                       displayPreview();
  void                       erasePreview();

private:
  bool                         myIsEdit;
  Handle(HYDROData_PolylineXY) myEditedObject;
  CurveCreator_Curve*          myCurve;
};

#endif

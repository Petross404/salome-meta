// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_AISTRIHEDRON_H
#define HYDROGUI_AISTRIHEDRON_H

#include <AIS_Trihedron.hxx>
#include <Prs3d_LineAspect.hxx>

#include <gp_Pnt.hxx>
#include <gp_Dir.hxx>

/**
 * \class HYDROGUI_AISTrihedron
 * \brief AIS trihedron is redefined in order to show U text on the OX axis
 * and Z text on the OY axis.
 */

class HYDROGUI_AISTrihedron : public AIS_Trihedron
{
public:
  /**
   * The struct to contain axis parameters. Save them in order to do not extract them
   * on each compute call
   */
  struct AxisParameters
  {
    gp_Pnt myPfirst; // the first point of the axis
    gp_Pnt myPlast;  // the last point of the axis
    Standard_Real myVal; // the axis length
    Handle(Prs3d_LineAspect) myLineAspect; // the line aspect
    gp_Dir myDir; // the axis direction
    Standard_CString myText; // the axis text. This is the main parameter,
                             // that leads to the trihedron redefine
  };

public:
  HYDROGUI_AISTrihedron( const Handle(Geom_Axis2Placement)& thePlacement );

  ~HYDROGUI_AISTrihedron() {}

  /**
   * The static method to create filled trihedron. It makes some additional settings
   * to the trihedron: resize and set color to the axis
   */
  static Handle(AIS_Trihedron) createTrihedron( double theSize );

  /**
   * Updates the internal axis parameters. Should be called after the axis parameters change
   * e.g. axis resize
   */
  void SetAxis2Placement();

  /**
   * AIS trihedron compute method. It removes the parent compute functionality.
   * It add to the presentation only oX, oY axis with the corrected text, that contains
   * "U"/"Z" labels
   * \param thePresentationManager the prs manager
   * \param thePresentationManager the presentation
   * \param theMode the mode
   */
  void Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                const Handle(Prs3d_Presentation)& thePresentation,
                const Standard_Integer theMode);

  /**
   * AIS trihedron compute method. It is empty. We do not apply the transformation.
   * \param thePresentationManager the prs manager
   * \param theTransformation the transformation
   * \param thePresentationManager the presentation
   */
  void Compute( const Handle(Graphic3d_Camera)& theProjector,
                const Handle(TopLoc_Datum3D)& theTransformation,
                const Handle(Prs3d_Presentation)& thePresentation );

  /**
   * AIS trihedron compute selection method. It is empty. We do not select the trihedron
   * \param theSelection the selection
   * \param theMode the mode
   */
  void ComputeSelection( const Handle(SelectMgr_Selection)& theSelection,
                         const Standard_Integer theMode );

private:
  /**
   * It adds the presentation to the DsgPrs_XYZAxisPresentation. It should be called in
   * compute for displayed axis
   * \param thePresentation the 3D presentation
   * \param theParams the axis params, such as text, direction, stand and last points
   */
  void ComputeAxis( const Handle(Prs3d_Presentation)& thePresentation,
                    const AxisParameters& theParams );
  void ComputeFields();

  AxisParameters myUParams; // the parameters for U axis
  AxisParameters myZParams; // the parameters for Z axis
};


#endif

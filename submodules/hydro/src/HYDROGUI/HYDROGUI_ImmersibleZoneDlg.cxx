// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImmersibleZoneDlg.h"

#include "HYDROGUI_Tool.h"

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>

HYDROGUI_ImmersibleZoneDlg::HYDROGUI_ImmersibleZoneDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_BasicZoneDlg( theModule, theTitle,
                         tr( "ZONE_NAME" ), tr( "NAME" ),
                         tr( "ZONE_PARAMETERS" ), tr( "ZONE_BATHYMETRY" ) )
{
  myPolylines = new QComboBox( myPolylineFrame );
  myPolylines->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  QBoxLayout* aPolyLayout = new QHBoxLayout( myPolylineFrame );
  aPolyLayout->setMargin( 0 );
  aPolyLayout->setSpacing( 5 );
  aPolyLayout->addWidget( new QLabel( tr( "ZONE_POLYLINE" ), myPolylineFrame ) );
  aPolyLayout->addWidget( myPolylines );
  
  QBoxLayout* aParamLayout = new QVBoxLayout( myParamGroup );
  aParamLayout->setMargin( 5 );
  aParamLayout->setSpacing( 5 );
  aParamLayout->addWidget( myPolylineFrame );

  // Connect signals and slots
  connect( myPolylines, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onZoneDefChanged() ) );
}

HYDROGUI_ImmersibleZoneDlg::~HYDROGUI_ImmersibleZoneDlg()
{
}

void HYDROGUI_ImmersibleZoneDlg::reset()
{
  bool isBlocked = blockSignals( true );

  HYDROGUI_BasicZoneDlg::reset(); 

  myPolylines->clear();
  
  blockSignals( isBlocked );

  onZoneDefChanged();
}

void HYDROGUI_ImmersibleZoneDlg::setPolylineNames( const QStringList& thePolylines )
{
  bool isBlocked = blockSignals( true );

  myPolylines->clear();
  myPolylines->addItems( thePolylines );

  blockSignals( isBlocked );
}

void HYDROGUI_ImmersibleZoneDlg::setPolylineName( const QString& theName )
{
  int aNewIdx = myPolylines->findText( theName );
  if ( aNewIdx != myPolylines->currentIndex() )
  {
    myPolylines->setCurrentIndex( aNewIdx );
  }
  else
  {
    onZoneDefChanged();
  }
}

QString HYDROGUI_ImmersibleZoneDlg::getPolylineName() const
{
  return myPolylines->currentText();
}

void HYDROGUI_ImmersibleZoneDlg::onZoneDefChanged()
{
  if ( signalsBlocked() )
    return;

  QString aPolylineName = getPolylineName();
  emit CreatePreview( aPolylineName );
}

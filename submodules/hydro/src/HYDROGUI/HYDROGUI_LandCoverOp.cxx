// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_LandCoverOp.h"

#include "HYDROGUI_LandCoverDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_DataObject.h"

#include <HYDROData_PolylineXY.h>
#include <HYDROData_Document.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_StricklerTable.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <LightApp_Application.h>

#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>

#include <TopoDS.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>

#include <QApplication>

HYDROGUI_LandCoverOp::HYDROGUI_LandCoverOp( HYDROGUI_Module* theModule,
                                            const bool theIsEdit )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit ),
  myPreviewPrs( 0 )
{
  setName( theIsEdit ? tr( "EDIT_LAND_COVER" ) : tr( "CREATE_LAND_COVER" ) );
}

HYDROGUI_LandCoverOp::~HYDROGUI_LandCoverOp()
{
  closePreview();
}

void HYDROGUI_LandCoverOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_LandCoverDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  aPanel->blockSignals( true );

  aPanel->reset();

  QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_LAND_COVER_NAME" ) );

  HYDROGUI_ListModel::Object2VisibleList aSelectedPolylines;
  QString     aSelectedStricklerType;

  if ( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_LandCover)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if ( !myEditedObject.IsNull() )
    {
      anObjectName = myEditedObject->GetName();

      HYDROData_SequenceOfObjects aRefPolylines = myEditedObject->GetPolylines();
      for ( int i = aRefPolylines.Lower(); i <= aRefPolylines.Upper(); i++ ) {
        Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast( aRefPolylines.Value( i ) );
        if ( !aPolyline.IsNull() ) {
          aSelectedPolylines.append( HYDROGUI_ListModel::Object2Visible( aPolyline, true ) );
        }
      }
    }

    aSelectedStricklerType = myEditedObject->GetStricklerType();
  }

  aPanel->setObjectName( anObjectName );
  // Construct a list of unique names of all Strickler types defined within the data model
  QStringList aStricklerTypes;
  HYDROData_Iterator anIterator( doc(), KIND_STRICKLER_TABLE );
  for( ; anIterator.More(); anIterator.Next() )
  {
    Handle(HYDROData_StricklerTable) aStricklerTableObj =
      Handle(HYDROData_StricklerTable)::DownCast( anIterator.Current() );	
    if ( !aStricklerTableObj.IsNull() )
    {
      // Get Strickler table data from the data model
      QStringList aTypes = aStricklerTableObj->GetTypes();
      for ( QStringList::iterator it = aTypes.begin(); it != aTypes.end(); ++it )
      {
        QString aType = *it;
        if ( !aType.isEmpty() && !aStricklerTypes.contains( aType ) )
          aStricklerTypes.append( aType );
      }
    }
  }

  aStricklerTypes.sort();
  aPanel->setAdditionalParams( aStricklerTypes );

  aPanel->blockSignals( false );

  aPanel->includePolylines( aSelectedPolylines );
  aPanel->setSelectedAdditionalParamName( aSelectedStricklerType );
}

void HYDROGUI_LandCoverOp::abortOperation()
{
  closePreview();

  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_LandCoverOp::commitOperation()
{
  closePreview();

  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_LandCoverOp::createInputPanel() const
{
  HYDROGUI_LandCoverDlg* aPanel = new HYDROGUI_LandCoverDlg( module(), getName() );
  connect( aPanel, SIGNAL( CreatePreview( const QStringList& ) ),
           this,   SLOT( onCreatePreview( const QStringList& ) ) );
  connect( aPanel, SIGNAL( addPolylines() ), SLOT( onAddPolylines() ) );
  connect( aPanel, SIGNAL( removePolylines() ), SLOT( onRemovePolylines() ) );
  return aPanel;
}

bool HYDROGUI_LandCoverOp::processApply( int& theUpdateFlags,
                                         QString& theErrorMsg,
                                         QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_LandCoverDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QString anObjectName = aPanel->getObjectName().simplified();
  if ( anObjectName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  if( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anObjectName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObjectName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObjectName );
      return false;
    }
  }

  HYDROData_SequenceOfObjects aZonePolylines;
  QString aStricklerType;

  QStringList aSelectedPolylineNames = aPanel->getPolylineNames();
  QStringList::const_iterator anIt = aSelectedPolylineNames.begin(), aLast = aSelectedPolylineNames.end();
  for( ; anIt!=aLast; anIt++ )
  {
    QString aPolylineName = *anIt;
    if ( !aPolylineName.isEmpty() )
    {
      aZonePolylines.Append( Handle(HYDROData_PolylineXY)::DownCast(
        HYDROGUI_Tool::FindObjectByName( module(), aPolylineName, KIND_POLYLINEXY ) ) );
    }
  }

  if ( aZonePolylines.IsEmpty() )
  {
    theErrorMsg = tr( "POLYLINES_NOT_DEFINED" );
    return false;
  }

  QString aSelectedStricklerType = aPanel->getSelectedAdditionalParamName();
  
  TCollection_AsciiString anError;
  if ( HYDROData_LandCover::buildShape( aZonePolylines, anError ).IsNull() )
  {
    if ( !anError.IsEmpty() ) {
      theErrorMsg = HYDROGUI_Tool::ToQString( anError );
    } else {
      theErrorMsg = tr( "LAND_COVER_OBJECT_CANNOT_BE_CREATED" );
    }
    return false;
  }
  
  Handle(HYDROData_LandCover) aZoneObj = myIsEdit ? myEditedObject :
    Handle(HYDROData_LandCover)::DownCast( doc()->CreateObject( KIND_LAND_COVER ) );

  aZoneObj->SetName( anObjectName );

  if ( !myIsEdit )
  {    
    aZoneObj->SetFillingColor( HYDROData_LandCover::DefaultFillingColor() );
    aZoneObj->SetBorderColor( HYDROData_LandCover::DefaultBorderColor() );
  }

  aZoneObj->SetPolylines( aZonePolylines );
  aZoneObj->SetStricklerType( aSelectedStricklerType );
  aZoneObj->Update();

  closePreview();

  if( !myIsEdit )
  {
    module()->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( module() ), aZoneObj, true );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aZoneObj );
    theBrowseObjectsEntries.append( anEntry );
  }

  module()->setIsToUpdate( aZoneObj );
  module()->getOCCDisplayer()->SetToUpdateColorScale();

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  return true;
}

void HYDROGUI_LandCoverOp::onCreatePreview( const QStringList& thePolylineNames )
{
  HYDROGUI_LandCoverDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QApplication::setOverrideCursor( Qt::WaitCursor );  

  HYDROData_SequenceOfObjects aZonePolylines;
  QStringList::const_iterator anIt = thePolylineNames.begin(), aLast = thePolylineNames.end();
  for( ; anIt!=aLast; anIt++ )
  {
    QString aPolylineName = *anIt;
    Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aPolylineName, KIND_POLYLINEXY ) );
    if ( !aPolyline.IsNull() )
      aZonePolylines.Append( aPolyline );
  }
  
  TCollection_AsciiString anError;
  TopoDS_Shape aZoneShape = HYDROData_LandCover::buildShape( aZonePolylines, anError );  

  LightApp_Application* anApp = module()->getApp();
  if ( !getPreviewManager() )
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                       anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager && !myPreviewPrs )
  {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
        myPreviewPrs = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );
    }
  }

  if ( aViewManager && myPreviewPrs )
  {
    QColor aFillingColor = HYDROData_LandCover::DefaultFillingColor();
    QColor aBorderColor = HYDROData_LandCover::DefaultBorderColor();
    if ( !myEditedObject.IsNull() ) {
      aFillingColor = myEditedObject->GetFillingColor();
      aBorderColor = myEditedObject->GetBorderColor();
    }

    myPreviewPrs->setFillingColor( aFillingColor, false, false );
    myPreviewPrs->setBorderColor( aBorderColor, false, false );

    if( !aZoneShape.IsNull() )
      myPreviewPrs->setShape( aZoneShape );
  }

  QApplication::restoreOverrideCursor();
}

void HYDROGUI_LandCoverOp::onAddPolylines()
{
  HYDROGUI_LandCoverDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  // Add polylines selected in the module browser
  Handle(HYDROData_PolylineXY) aPolyXY;
  HYDROGUI_ListModel::Object2VisibleList aSelectedPolylines;
  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( module() );

  if ( aSeq.IsEmpty() || !confirmPolylinesChange() )
    return;

  for( int anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
  {
    aPolyXY = Handle(HYDROData_PolylineXY)::DownCast( aSeq.Value( anIndex ));
    if (!aPolyXY.IsNull())
      aSelectedPolylines.append( HYDROGUI_ListModel::Object2Visible( aPolyXY, true ) );
  }
  
  if ( aPanel->includePolylines( aSelectedPolylines ) )
  {
    closePreview();
    onCreatePreview( aPanel->getPolylineNames() );
  }  
}

void HYDROGUI_LandCoverOp::onRemovePolylines()
{
  // Remove selected polylines from the calculation case
  HYDROGUI_LandCoverDlg* aPanel = ::qobject_cast<HYDROGUI_LandCoverDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QStringList aSelectedList = aPanel->getSelectedPolylineNames();
  if ( aSelectedList.isEmpty() || !confirmPolylinesChange() )
    return;
  
  HYDROGUI_ListModel::Object2VisibleList aSelectedPolylines;
  for (int i = 0; i < aSelectedList.length(); i++)
  {
    Handle(HYDROData_PolylineXY) anObject = Handle(HYDROData_PolylineXY)::DownCast( 
      HYDROGUI_Tool::FindObjectByName( module(), aSelectedList.at(i) ) );
    if ( anObject.IsNull() )
      continue;

    aSelectedPolylines.append( HYDROGUI_ListModel::Object2Visible( anObject, true ) );
  }

  module()->update( UF_OCCViewer );
  
  if ( aPanel->excludePolylines( aSelectedPolylines ) )
  {
    closePreview();
    onCreatePreview( aPanel->getPolylineNames() );
  }
}

void HYDROGUI_LandCoverOp::closePreview()
{
  if( myPreviewPrs )
  {
    delete myPreviewPrs;
    myPreviewPrs = 0;
  }
}

bool HYDROGUI_LandCoverOp::confirmPolylinesChange() const
{
  if ( myEditedObject.IsNull() )
    return true;

  // Check if the land cover object is already modified or not
  bool isConfirmed = myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d );
  if ( !isConfirmed )
  {
    // If not modified check if the land cover has already defined polylines
    HYDROData_SequenceOfObjects aSeq = myEditedObject->GetPolylines();
    if ( aSeq.Length() > 0 )
    {
      // If there are already defined polylines then ask a user to confirm land cover recalculation
      isConfirmed = ( SUIT_MessageBox::question( module()->getApp()->desktop(),
                      tr( "POLYLINES_CHANGED" ),
                      tr( "CONFIRM_LAND_COVER_RECALCULATION" ),
                      QMessageBox::Yes | QMessageBox::No,
                      QMessageBox::No ) == QMessageBox::Yes );
    }
    else
    {
      isConfirmed = true; // No polylines - nothing to recalculate
    }
  }
  return isConfirmed;
}

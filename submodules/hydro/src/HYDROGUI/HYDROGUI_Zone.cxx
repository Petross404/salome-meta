// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Zone.h"

#include <HYDROData_Zone.h>
#include <HYDROData_Object.h>
#include <HYDROData_IAltitudeObject.h>

#include <SUIT_DataObject.h>
#include <QSet>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_Zone::HYDROGUI_Zone( SUIT_DataObject* theParent, 
                                          Handle(HYDROData_Zone) theData,
                                          const QString& theParentEntry,
                                          const bool theIsInOperation )
: HYDROGUI_DataObject( theParent, theData, theParentEntry, theIsInOperation ), CAM_DataObject( theParent )
{
}

QString HYDROGUI_Zone::text( const int theColumnId ) const
{
  QString aRes;
  if( !modelObject().IsNull() )
  {
    switch ( theColumnId )
    {
      case RefObjectId:
        // Get Ref.Object name
        aRes = getRefObjectNames();
        break;
      case AltitudeObjId:
        // Get altitude/land cover object name
        aRes = getObjectName();
        break;
      default:
        aRes = LightApp_DataObject::text( theColumnId );
    }
  }
  return aRes;
}

QString HYDROGUI_Zone::getRefObjectNames() const
{
  QString aRes;
  Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
  if ( !aZone.IsNull() )
  {
    HYDROData_SequenceOfObjects aSeq = aZone->GetObjects();
    HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Entity) aRefGeomObj = anIter.Value();
      if ( !aRefGeomObj.IsNull() )
      {
        // Get Ref.Object name
        aRes += aRefGeomObj->GetName() + ", ";
      }
    }
  }
  if ( aRes.length() > 1 )
  {
    aRes.remove( aRes.length() - 2, 2 );
  }
  return aRes;
}

QString HYDROGUI_Zone::getObjectName() const
{
  QString aRes;
  Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
  if ( !aZone.IsNull() )
  {
    HYDROData_SequenceOfObjects aSeq = aZone->GetObjects();
    bool isMergingNeed = aZone->IsMergingNeed();
    if ( ( isMergingNeed && aZone->GetMergeType() == HYDROData_Zone::Merge_UNKNOWN ) 
      || ( aSeq.Length() == 1 ) || ( !isMergingNeed ) )
    {
      // Collect all used altitudes names when merging is necessary
      // or just get the name of altitude of a single geometry object
      // or just get the name of a single altitude
      HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
      QSet<QString> aNamesSet;
      QString aName;
      for ( ; anIter.More(); anIter.Next() )
      {
        aName.clear();
        Handle(HYDROData_Object) aRefGeomObj =
          Handle(HYDROData_Object)::DownCast( anIter.Value() );
        if ( !aRefGeomObj.IsNull() )
        {
          // Get altitude object name
          Handle(HYDROData_IAltitudeObject) anAltitudeObj = aRefGeomObj->GetAltitudeObject();
          if ( !anAltitudeObj.IsNull() )
          {
            aName = anAltitudeObj->GetName();
            if ( !isMergingNeed )
            {
              // Get the first geometry object's altitude name and go out
              aRes = aName;
              break;
            }
          }
        }

        if ( !aName.isEmpty() && !aNamesSet.contains( aName ) )
        {
          aRes += aName + ", ";
          aNamesSet.insert( aName );
        }
      }
      // Remove the last comma if necessary
      if ( isMergingNeed && ( aRes.length() > 1 ) )
      {
        aRes.remove( aRes.length() - 2, 2 );
      }
    }
    else
    {
      switch( aZone->GetMergeType() )
      {
        case HYDROData_Zone::Merge_ZMIN:    // The minimum values
          aRes = QObject::tr( "MERGE_ZMIN" );
          break;
        case HYDROData_Zone::Merge_ZMAX:    // The maximum values
          aRes = QObject::tr( "MERGE_ZMAX" );
          break;
        case HYDROData_Zone::Merge_Object:   // Only one altitude/land cover will be taken into account
        {
          Handle(HYDROData_Entity) aMergeObj = aZone->GetMergeObject();
          if ( !aMergeObj.IsNull() )
            aRes = aMergeObj->GetName();
          break;
        }
        default:
          aRes = QObject::tr( "MERGE_UNKNOWN" );
      }
    }
  }
  return aRes;
}

bool HYDROGUI_Zone::isMergingNeed() const
{
  bool aRes = false;
  if( !modelObject().IsNull() )
  {
    Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
    if ( !aZone.IsNull() ) 
    {
      aRes = aZone->IsMergingNeed();
    }
  }
  return aRes;
}

QColor HYDROGUI_Zone::color( const ColorRole theColorRole, const int theColumnId ) const
{
  // Implement red color for altitude conflicts in case creation dialog
  QColor aRes;
  Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
  if ( !aZone.IsNull() )
  {
    if ( ( aZone->IsMergingNeed() && aZone->GetMergeType() == HYDROData_Zone::Merge_UNKNOWN ) )
    {
      switch( theColorRole )
      {
        case Text:            // editor foreground (text) color
        case Foreground:      // foreground (text) color
          aRes = Qt::red;
          break;
        case HighlightedText: // highlighted foreground (text) color
          aRes = Qt::black;
          break;
        case Base:            // editor background color
        case Background:      // background color
        case Highlight:       // highlight background color
        default:
          aRes = Qt::red;
      }
    }
  }
  if ( !aRes.isValid() )
  {
    aRes = HYDROGUI_DataObject::color( theColorRole, theColumnId );
  }
  return aRes;
}

QStringList HYDROGUI_Zone::getObjects() const
{
  QStringList aRes;
  Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
  if ( !aZone.IsNull() )
  {
    HYDROData_SequenceOfObjects aSeq = aZone->GetObjects();
    // Collect all used altitudes/land cover names when merging is necessary
    // or just get the name of altitude/land cover of a single object
    HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(HYDROData_Object) aRefGeomObj =
        Handle(HYDROData_Object)::DownCast( anIter.Value() );
      if ( !aRefGeomObj.IsNull() )
      {
        Handle(HYDROData_IAltitudeObject) anAltitudeObj = aRefGeomObj->GetAltitudeObject();
        if ( !anAltitudeObj.IsNull() && !aRes.contains( anAltitudeObj->GetName() ) )
          aRes.append( anAltitudeObj->GetName() );
      } else {
        aRes.append( anIter.Value()->GetName() );
      }
    }
  }
  return aRes;
}

HYDROData_Zone::MergeType HYDROGUI_Zone::getMergeType() const
{
  HYDROData_Zone::MergeType aRes = HYDROData_Zone::Merge_UNKNOWN;
  Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
  if ( !aZone.IsNull() )
  {
    aRes = aZone->GetMergeType();
  }
  return aRes;
}

void HYDROGUI_Zone::setMergeType( int theMergeType, QString theMergeObjectName )
{
  DEBTRACE("HYDROGUI_Zone::setMergeType " << theMergeType << " " << theMergeObjectName.toStdString());
  Handle(HYDROData_Zone) aZone = Handle(HYDROData_Zone)::DownCast( modelObject() );
  if ( !aZone.IsNull() )
  {
    HYDROData_Zone::MergeType aMergeType = 
      ( HYDROData_Zone::MergeType )theMergeType;
    aZone->SetMergeType( aMergeType );
    if ( aMergeType == HYDROData_Zone::Merge_Object )
    {
      // Find an altitude/land cover object by the given name and set it as the zone's merge altitude/land cover
      HYDROData_SequenceOfObjects aSeq = aZone->GetObjects();
      HYDROData_SequenceOfObjects::Iterator anIter( aSeq );
      for ( ; anIter.More(); anIter.Next() )
      {
        Handle(HYDROData_Entity) aMergeObject;

        Handle(HYDROData_Object) aRefGeomObj =
          Handle(HYDROData_Object)::DownCast( anIter.Value() );
        if ( !aRefGeomObj.IsNull() )
        {
          // Get altitude object
          DEBTRACE("aRefGeomObj " << aRefGeomObj->GetName().toStdString());
          aMergeObject = aRefGeomObj->GetAltitudeObject();
        }

        if ( !aMergeObject.IsNull() && theMergeObjectName == aMergeObject->GetName() )
        {
          aZone->SetMergeObject( aMergeObject );
          break;
        }
      }
    }
  }
}

/*!
  \brief Check if this object is can't be renamed in place

  \param id column id
  \return \c true if the item can be renamed by the user in place (e.g. in the Object browser)
*/
bool HYDROGUI_Zone::renameAllowed( const int id ) const
{
  if ( id == NameId && isInOperation() )
  {
    return true;
  }
  return HYDROGUI_DataObject::renameAllowed( id );
}

///*!
//  \brief Set name of this object.
//
//  \return \c true if rename operation finished successfully, \c false otherwise.
//*/
//bool HYDROGUI_Zone::setName(const QString& theName)
//{
//  if ( isInOperation() )
//  {
//    bool aRes = false;
//    if ( !theName.isEmpty() )
//    {
//      Handle(HYDROData_Entity) anEntity = modelObject();
//      CAM_Module* aModule = module();
//      if( anEntity->GetName() != theName && aModule )
//      {
//        // check that there are no other objects with the same name in the document
//        Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( aModule, theName );
//        if ( anObject.IsNull() )
//        {
//          anEntity->SetName( theName );
//          aRes = true;
//        }
//        else
//        {
//          // Inform the user that the name is already used
//          QString aTitle = QObject::tr( "INSUFFICIENT_INPUT_DATA" );
//          QString aMessage = QObject::tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( theName );
//          SUIT_MessageBox::critical( getApp()->desktop(), aTitle, aMessage );
//        }
//      }
//    }
//  }
//  else
//  {
//    aRes = HYDROGUI_DataObject::setName( theName );
//  }
//  return aRes;
//}

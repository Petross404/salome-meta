// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ProfileInterpolateOp.h>

#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_ProfileInterpolateDlg.h"

#include <HYDROData_Stream.h>
#include <HYDROData_Profile.h>
#include <HYDROData_Document.h>
#include <HYDROData_ProfileUZ.h>
#include <HYDROData_IPolyline.h>
#include <HYDROData_PolylineXY.h>
#include <HYDROData_InterpolatorsFactory.h>
#include <HYDROData_IProfilesInterpolator.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_UpdateFlags.h>

#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewPort3d.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_AISSelector.h>

#include <AIS_ListOfInteractive.hxx>
#include <AIS_ListIteratorOfListOfInteractive.hxx>

#include <BRep_Builder.hxx>
#include <TopoDS_Compound.hxx>

#include <QMessageBox>

HYDROGUI_ProfileInterpolateOp::HYDROGUI_ProfileInterpolateOp( HYDROGUI_Module* theModule )
    : HYDROGUI_Operation( theModule )
{
    setName( tr( "PROFILE_INTERPOLATION" ) );
}

HYDROGUI_ProfileInterpolateOp::~HYDROGUI_ProfileInterpolateOp()
{
}

void HYDROGUI_ProfileInterpolateOp::startOperation()
{
    HYDROGUI_Operation::startOperation();

    if ( isApplyAndClose() )
    {
      HYDROGUI_ProfileInterpolateDlg* aPanel = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
      if ( aPanel )
      {
        aPanel->reset();
        aPanel->setInterpolators( interpolators() );
      }
    }
}

void HYDROGUI_ProfileInterpolateOp::abortOperation()
{
    if ( !myPreview.IsNull() )
    {
        HYDROGUI_ProfileInterpolateDlg* aDlg = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
        if ( aDlg && !aDlg->getAISContext().IsNull() )
        {
            aDlg->getAISContext()->Remove( myPreview, false );
            myPreview.Nullify();
        }
    }

    HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ProfileInterpolateOp::commitOperation()
{
    if ( !myPreview.IsNull() )
    {
        HYDROGUI_ProfileInterpolateDlg* aDlg = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
        if ( aDlg && !aDlg->getAISContext().IsNull() )
        {
            aDlg->getAISContext()->Remove( myPreview, false );
            myPreview.Nullify();
        }
    }

    HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_ProfileInterpolateOp::createInputPanel() const
{
    HYDROGUI_ProfileInterpolateDlg* aDlg = new HYDROGUI_ProfileInterpolateDlg( module(), getName() );

    connect( aDlg, SIGNAL( riverChanged( const QString& ) ), this, SLOT( onRiverChanged( const QString& ) ) );
    connect( aDlg, SIGNAL( interpolatorChanged( const QString& ) ), this, SLOT( onInterpolatorChanged( const QString& ) ) );
    connect( aDlg, SIGNAL( interpolatorParametersChanged( const QString& ) ), this, SLOT( updatePreview() ) );
    connect( aDlg, SIGNAL( profileNumberChanged( int ) ), this, SLOT( updatePreview() ) );
    connect( aDlg, SIGNAL( profileStartChanged( const QString& ) ), this, SLOT( updatePreview() ) );
    connect( aDlg, SIGNAL( profileFinishChanged( const QString& ) ), this, SLOT( updatePreview() ) );

    return aDlg;
}

bool HYDROGUI_ProfileInterpolateOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                                  QStringList& theBrowseObjectsEntries )
{
    HYDROGUI_ProfileInterpolateDlg* aDlg = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
    if ( !aDlg )
        return false;

    QString errMessage;
    HYDROData_IProfilesInterpolator* anIterp = interpolator( aDlg->interpolator() );
    if ( anIterp )
    {
        updateInterpolator( anIterp );
        Handle(HYDROData_Stream) aRiver = Handle(HYDROData_Stream)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), aDlg->river(), KIND_STREAM ) );
        if ( !aRiver.IsNull() )
        {
            startDocOperation();
            aRiver->Interpolate( anIterp );
            if ( anIterp->GetErrorCode() == OK )
                commitDocOperation();
            else
            {
                errMessage = tr( "CALCULATE_ERROR" ), QString( HYDROGUI_Tool::ToQString( anIterp->GetErrorMessage() ) );
                abortDocOperation();
            }
        }
        else
            errMessage = tr( "CANT_GET_STREAM_OBJECT" ).arg( aDlg->river() );

        if ( !errMessage.isEmpty() )
            QMessageBox::critical( aDlg->topLevelWidget(), tr( "INTERPOLATION_ERROR" ), errMessage, QMessageBox::Ok );
        else
            commit();
    }

    bool res = errMessage.isEmpty();

    if ( res )
        theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

    return res;
}

void HYDROGUI_ProfileInterpolateOp::updatePreview()
{
    HYDROGUI_ProfileInterpolateDlg* aDlg = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
    if ( !aDlg )
        return;

    Handle(AIS_InteractiveContext) aCtx = aDlg->getAISContext();
    if ( aCtx.IsNull() )
        return;

    if ( !myPreview.IsNull() )
        aCtx->Remove( myPreview, false );

    myPreview.Nullify();

    HYDROData_IProfilesInterpolator* anIterp = interpolator( aDlg->interpolator() );
    if ( anIterp )
    {
        updateInterpolator( anIterp );
        anIterp->Calculate();
        if ( anIterp->GetErrorCode() == OK )
        {
            TopoDS_Shape aShape = previewShape( anIterp );
            if ( !aShape.IsNull() )
            {
                myPreview = new AIS_Shape( aShape );
                myPreview->SetColor( Quantity_NOC_RED );
                aCtx->Display( myPreview, 1, -1, false );
            }
        }
    }

    aCtx->UpdateCurrentViewer();
}

void HYDROGUI_ProfileInterpolateOp::updateInterpolator( HYDROData_IProfilesInterpolator* theInt )
{
    HYDROGUI_ProfileInterpolateDlg* aDlg = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
    if ( !aDlg || !theInt )
        return;

    theInt->Reset();
    theInt->SetResultProfilesNumber( aDlg->profileNumber() );
    theInt->SetProfiles( profile( aDlg->profileStart() ), profile( aDlg->profileFinish() ) );
    ParamsList aParams = parameters( aDlg->interpolatorParameters() );
    for ( ParamsList::Iterator it = aParams.begin(); it != aParams.end(); ++it ) {
        theInt->SetParameter( HYDROGUI_Tool::ToAsciiString( (*it).first ), 
                              HYDROGUI_Tool::ToAsciiString( (*it).second ) );
    }
}

HYDROData_Profile::ProfilePoints HYDROGUI_ProfileInterpolateOp::profile( const QString& theName ) const
{
    HYDROData_Profile::ProfilePoints aPoints;
    Handle(HYDROData_Profile) aProf = Handle(HYDROData_Profile)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), theName, KIND_PROFILE ) );
    if ( !aProf.IsNull() )
    {
        aPoints = aProf->GetProfilePoints();
    }
    return aPoints;
}

HYDROGUI_ProfileInterpolateOp::ParamsList HYDROGUI_ProfileInterpolateOp::parameters( const QString& theStr ) const
{
    // Regular expression for parsing parameters string of kind: <param_name> <param_value> <param_name> <param_value> ..."
    // QRegExp rx( "([A-Za-z_\\d]+)\\s+([A-Za-z_\\d]+)\\s*" );

    // Regular expression for parsing parameters string of kind: <param_name> = <param_value> <param_name> = <param_value> ..."
    QRegExp rx( "([A-Za-z_\\d]+)\\s*=\\s*([A-Za-z_\\d]+)\\s*" );

    ParamsList aParamList;
    for ( int i = 0; i >= 0; )
    {
        i = rx.indexIn( theStr, i );
        if ( i >= 0  )
        {
            aParamList.append( QPair<QString, QString>( rx.cap( 1 ), rx.cap( 2 ) ) );
            i += rx.matchedLength();
        }
    }

    return aParamList;
}

QString HYDROGUI_ProfileInterpolateOp::parameters( const ParamsList& theParamList ) const
{
    QStringList paramList;
    for ( ParamsList::const_iterator it = theParamList.begin(); it != theParamList.end(); ++it )
        paramList.append( QString( "%1 = %2" ).arg( (*it).first ).arg( (*it).second ) );
    return paramList.join( " " );
}

QStringList HYDROGUI_ProfileInterpolateOp::interpolators() const
{
    HYDROData_InterpolatorsFactory* anIFactory = 0;
    Handle(HYDROData_Document) aDoc = doc();
    if ( !aDoc.IsNull() )
        anIFactory = aDoc->GetInterpolatorsFactory();

    QStringList aNames;
    if ( anIFactory )
    {
      NCollection_Sequence<TCollection_AsciiString> iNames = anIFactory->GetInterpolatorNames();
      for ( int i = 1, n = iNames.Size(); i <= n; ++i ) {
        const TCollection_AsciiString& anInterpName = iNames.Value( i );
        aNames.append( HYDROGUI_Tool::ToQString( anInterpName ) );
      }
    }

    return aNames;
}

HYDROData_IProfilesInterpolator* HYDROGUI_ProfileInterpolateOp::interpolator( const QString& theName ) const
{
    HYDROData_InterpolatorsFactory* anIFactory = 0;
    Handle(HYDROData_Document) aDoc = doc();
    if ( !aDoc.IsNull() )
        anIFactory = aDoc->GetInterpolatorsFactory();

    HYDROData_IProfilesInterpolator* aRes = 0;
    if ( anIFactory )
        aRes = anIFactory->GetInterpolator( HYDROGUI_Tool::ToAsciiString( theName ) );
    return aRes;
}

TopoDS_Shape HYDROGUI_ProfileInterpolateOp::previewShape( HYDROData_IProfilesInterpolator* theInterp ) const
{
    TopoDS_Compound aPreviewShape;
    if ( theInterp )
    {
        BRep_Builder aBuilder;
        aBuilder.MakeCompound( aPreviewShape );
        for ( int i = 0; i < theInterp->GetCalculatedProfilesNumber(); i++ )
        {
            NCollection_Sequence<gp_XYZ> pointSeq = theInterp->GetResultProfilePoints( i );

            TopoDS_Shape aWire = HYDROData_PolylineXY::BuildWire( HYDROData_IPolyline::SECTION_SPLINE, false, pointSeq );
            if ( !aWire.IsNull() )
                aBuilder.Add( aPreviewShape, aWire );
        }
    }

    return aPreviewShape;
}

void HYDROGUI_ProfileInterpolateOp::onInterpolatorChanged( const QString& theInterpName )
{
    HYDROGUI_ProfileInterpolateDlg* aPanel = dynamic_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
    HYDROData_IProfilesInterpolator* anInterp = interpolator( theInterpName );
    if ( !aPanel || !anInterp )
        return;

    aPanel->setInterpolatorDescription( HYDROGUI_Tool::ToQString( anInterp->GetDescription() ) );

    updatePreview();
}

void HYDROGUI_ProfileInterpolateOp::onRiverChanged( const QString& theRiver )
{
    HYDROGUI_ProfileInterpolateDlg* aDlg = ::qobject_cast<HYDROGUI_ProfileInterpolateDlg*>( inputPanel() );
    if ( !aDlg )
        return;

    Handle(AIS_InteractiveContext) aCtx = aDlg->getAISContext();
    if ( !aCtx.IsNull() )
    {
        AIS_ListOfInteractive aList;
        aCtx->DisplayedObjects( aList );
        for ( AIS_ListIteratorOfListOfInteractive it( aList ); it.More(); it.Next() )
        {
            Handle(AIS_Shape) anObj = Handle(AIS_Shape)::DownCast( it.Value() );
            if ( !anObj.IsNull() )
                aCtx->Remove( anObj, false );
        }

        Handle(HYDROData_Stream) aStream = Handle(HYDROData_Stream)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), theRiver, KIND_STREAM ) );
        if ( !aStream.IsNull() )
        {
            HYDROData_SequenceOfObjects aSeq = aStream->GetProfiles();
            for ( int i = aSeq.Lower(); i <= aSeq.Upper(); i++ )
            {
                Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( aSeq.Value( i ) );
                if ( !aProfile.IsNull() )
                {
                    Handle(AIS_Shape) aPrs = new AIS_Shape( aProfile->GetShape3D() );
                    aPrs->SetOwner( aProfile );
                    aPrs->SetColor( Quantity_NOC_BLACK );
                    aCtx->Display( aPrs, 1, 0, false );
                }
            }
            OCCViewer_ViewManager* vm = aDlg->viewManager();
            if ( vm )
            {
                QVector<SUIT_ViewWindow*> winList = vm->getViews();
                for ( QVector<SUIT_ViewWindow*>::iterator it = winList.begin(); it != winList.end(); ++it )
                {
                    OCCViewer_ViewWindow* occWin = ::qobject_cast<OCCViewer_ViewWindow*>( *it );
                    if ( occWin )
                    {
                        OCCViewer_ViewPort3d* vp = occWin->getViewPort();
                        if ( vp )
                            vp->fitAll();
                    }
                }
            }
        }
    }
    updatePreview();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_VTKPrsBathymetry_H
#define HYDROGUI_VTKPrsBathymetry_H

#include "HYDROGUI_VTKPrs.h"

#include <HYDROData_Bathymetry.h>

#include <vtkScalarsToColors.h>
#include <vtkWeakPointer.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>

/*
  Class       : HYDROGUI_VTKPrsBathymetry
  Description : Presentation for Bathymetry object
*/
class HYDROGUI_VTKPrsBathymetry : public HYDROGUI_VTKPrs
{
public:
  HYDROGUI_VTKPrsBathymetry( const Handle(HYDROData_Bathymetry)& theObject );
  virtual ~HYDROGUI_VTKPrsBathymetry();

  virtual void                     compute();
  virtual bool                     needScalarBar() { return true; }

  //! Get the range of colored 
  void setLookupTable( vtkScalarsToColors* theTable ) { myLookupTable = theTable; }

protected:
  virtual vtkMapper*               mapper() { return myMapper.GetPointer(); }

private:
  vtkWeakPointer< vtkScalarsToColors > myLookupTable;
  vtkNew< vtkPolyDataMapper >          myMapper;
};

#endif

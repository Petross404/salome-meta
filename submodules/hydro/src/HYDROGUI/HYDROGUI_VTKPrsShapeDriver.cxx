// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VTKPrsShapeDriver.h"

#include "HYDROGUI_VTKPrsShape.h"

#include <HYDROData_Entity.h>

HYDROGUI_VTKPrsShapeDriver::HYDROGUI_VTKPrsShapeDriver( vtkScalarBarActor* theScalarBar )
{
  myScalarBar = theScalarBar;
}

HYDROGUI_VTKPrsShapeDriver::~HYDROGUI_VTKPrsShapeDriver()
{
}

bool HYDROGUI_VTKPrsShapeDriver::Update( const Handle(HYDROData_Entity)& theObj,
                                         HYDROGUI_VTKPrs*& thePrs )
{
  HYDROGUI_VTKPrsDriver::Update( theObj, thePrs );

  if( theObj.IsNull() )
    return false;

  if( !thePrs )
    thePrs = new HYDROGUI_VTKPrsShape( theObj );

  HYDROGUI_VTKPrsShape* aPrsShape = (HYDROGUI_VTKPrsShape*)thePrs;
  aPrsShape->compute();

  return true;
}

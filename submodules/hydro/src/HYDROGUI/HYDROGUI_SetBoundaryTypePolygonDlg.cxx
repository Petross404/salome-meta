// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_SetBoundaryTypePolygonDlg.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QFrame>
#include <QLabel>
#include <QFrame>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <QListWidgetItem>

HYDROGUI_SetBoundaryTypePolygonDlg::HYDROGUI_SetBoundaryTypePolygonDlg( QWidget* theParent, int theType )
  : QDialog( theParent )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin( 5 );
  aMainLayout->setSpacing( 5 );

  QFrame* aBTFrame = new QFrame( this );
  QLabel* aBoundaryLabel = new QLabel( tr( "BOUNDARY_TYPE" ), aBTFrame );
  myBTBox = new QComboBox( aBTFrame );
  myBTBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding ) );

  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  QStringList Types;
  Types << QObject::tr( QString("HYDRO_BC_POLYGON_TYPE1_ICO").toLatin1()) <<
    QObject::tr( QString("HYDRO_BC_POLYGON_TYPE2_ICO").toLatin1()) <<
    QObject::tr( QString("HYDRO_BC_POLYGON_TYPE3_ICO").toLatin1());

  myBTBox->addItem(QIcon( aResMgr->loadPixmap( "HYDRO", Types[0])), tr( "CUT_TOOL" ));
  myBTBox->addItem(QIcon( aResMgr->loadPixmap( "HYDRO", Types[1])), tr( "INCLUDE_TOOL" ));
  myBTBox->addItem(QIcon( aResMgr->loadPixmap( "HYDRO", Types[2])), tr( "SELECTION_TOOL" ));

  if (theType != 1 && theType != 2 && theType != 3)
    theType = -1;
  myBTBox->setCurrentIndex(theType - 1);

  QBoxLayout* aColorLayout = new QHBoxLayout( aBTFrame );
  aColorLayout->setMargin( 10 );
  aColorLayout->setSpacing( 5 );
  aColorLayout->addWidget( aBoundaryLabel );
  aColorLayout->addWidget( myBTBox );

  aMainLayout->addWidget( aBTFrame );

  // Buttons
  QPushButton* anOkButton = new QPushButton( tr( "OK" ), this );
  anOkButton->setDefault( true ); 
  QPushButton* aCancelButton = new QPushButton( tr( "CANCEL" ), this );
  aCancelButton->setAutoDefault( true );

  QHBoxLayout* aButtonsLayout = new QHBoxLayout;
  aButtonsLayout->setMargin( 5 );
  aButtonsLayout->setSpacing( 5 );
  aButtonsLayout->addWidget( anOkButton );
  aButtonsLayout->addStretch();
  aButtonsLayout->addWidget( aCancelButton );

  aMainLayout->addStretch();
  aMainLayout->addLayout( aButtonsLayout );

  setLayout( aMainLayout );  

  connect( anOkButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( aCancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
}

int HYDROGUI_SetBoundaryTypePolygonDlg::GetCurrentType()
{
  return myBTBox->currentIndex();
}

HYDROGUI_SetBoundaryTypePolygonDlg::~HYDROGUI_SetBoundaryTypePolygonDlg()
{
}



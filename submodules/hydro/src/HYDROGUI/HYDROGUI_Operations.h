// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_OPERATIONS_H
#define HYDROGUI_OPERATIONS_H

enum OperationId
{
  FirstId = 0,
  
  SaveVisualStateId,
  LoadVisualStateId,
  
  UndoId,
  RedoId,

  CopyId,
  PasteId,

  UpdateObjectId,
  ForcedUpdateObjectId,

  ImportImageId,
  EditImportedImageId,
  ObserveImageId,
  ExportImageId,
  RemoveImageRefsId,

  CreatePolylineId,
  EditPolylineId,
  
  CreatePolyline3DId,
  EditPolyline3DId,
  
  ImportProfilesId,
  CreateProfileId,
  EditProfileId,
  AllGeoreferencementId,
  SelectedGeoreferencementId,

  ImportBathymetryId,
  EditImportedBathymetryId,
  BathymetryBoundsId,

  CreateImmersibleZoneId,
  EditImmersibleZoneId,

  CreateStreamId,
  EditStreamId,

  CreateChannelId,
  EditChannelId,

  CreateDigueId,
  EditDigueId,

  CreateCalculationId,
  EditCalculationId,
  ExportCalculationId,
  CompleteCalculationId,

  FuseImagesId,
  EditFusedImageId,

  CutImagesId,
  EditCutImageId,

  SplitImageId,
  EditSplitImageId,

  ImportObstacleFromFileId,
  ImportGeomObjectAsObstacleId,
  ImportGeomObjectAsPolylineId,
  CreateBoxId,
  CreateCylinderId,
  TranslateObstacleId,

  CopyViewerPositionId,

  DeleteId,

  ShowId,
  ShowOnlyId,
  ShowAllId,
  HideId,
  HideAllId,

  SetColorId,
  SetTransparencyId,
  SetZLevelId,
  EditLocalCSId,

  RiverBottomId,
  RiverBottomContextId,
  ProfileInterpolateId,

  RecognizeContoursId,
  SubmersibleId,
  UnSubmersibleId,
  ImportPolylineId,
  ImportSinusXId,
  ExportSinusXId,
  ImportBCPolygonId, 
  MeasurementToolId, 
  
  ExportToShapeFileID,
  ImportLandCoverMapId,

  ImportStricklerTableFromFileId,
  ExportStricklerTableFromFileId,
  EditStricklerTableId,
  DuplicateStricklerTableId,

  CreateLandCoverMapId,
  
  AddLandCoverId,
  RemoveLandCoverId,
  SplitLandCoverId,
  MergeLandCoverId,
  ChangeLandCoverTypeId,

  PolylineExtractionId,
  SplitPolylinesId,
  MergePolylinesId,

  ShowAttrPolylinesId,

  LandCoverScalarMapModeOnId,
  LandCoverScalarMapModeOffId,

  BathymetrySelectionId,
  BathymetryTextId,
  BathymetryRescaleSelectionId,
  BathymetryRescaleVisibleId,
  BathymetryRescaleUserId,
  BathymetryRescaleDefaultId,
  RegenerateRegionColorsId,
  ShowHideArrows,
  ZoneSetColorId,
  SetBoundaryTypePolygonId
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PolyAttrDlg.h"
#include <QVBoxLayout>
#include <QPushButton>

HYDROGUI_PolyAttrDlg::HYDROGUI_PolyAttrDlg( QWidget* theParent, const QStringList& theDBFData )
  : QDialog( theParent )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin(5);
  aMainLayout->setSpacing(5);
  int nbcol = theDBFData.size()/2;
  myDBFDataTable = new QTableWidget(1, nbcol);
  //myDBFDataTable->setEditTriggers(QAbstractItemView:NoEditTriggers);

  QStringList header;
  for (int i = 0; i < nbcol; i++)
    header << theDBFData[i];
  myDBFDataTable->setHorizontalHeaderLabels(header);
  myDBFDataTable->setShowGrid(true);

  for (int i = theDBFData.size()/2; i < theDBFData.size(); i++)
  {
    QTableWidgetItem* item = new QTableWidgetItem(theDBFData[i]);
    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
    myDBFDataTable->setItem(0, i-theDBFData.size()/2, item);
  }

  aMainLayout->addWidget(myDBFDataTable);

  QPushButton* anOkButton = new QPushButton( tr("OK"), this );
  anOkButton->setDefault( true ); 
  aMainLayout->addWidget( anOkButton );

  connect( anOkButton, SIGNAL( clicked() ), this, SLOT( reject() ) );

  setLayout(aMainLayout);  

}

HYDROGUI_PolyAttrDlg::~HYDROGUI_PolyAttrDlg()
{
}



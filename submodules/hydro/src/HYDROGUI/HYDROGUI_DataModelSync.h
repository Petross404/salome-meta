// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_DATA_MODEL_SYNC_HEADER
#define HYDROGUI_DATA_MODEL_SYNC_HEADER

#include <QList>

class SUIT_DataObject;

typedef SUIT_DataObject* suitPtr;

class HYDROGUI_DataModelSync
{
public:
  HYDROGUI_DataModelSync( SUIT_DataObject* theRoot );
  ~HYDROGUI_DataModelSync();

  void     deleteItemWithChildren( const suitPtr& ) const;
  suitPtr  nullSrc() const;
  suitPtr  nullTrg() const;
  QList<suitPtr> children( const suitPtr& ) const;
  suitPtr  parent( const suitPtr& ) const;
  void     updateItem( const suitPtr&, const suitPtr& ) const;
  bool     isEqual( const suitPtr&, const suitPtr& ) const;
  suitPtr  createItem( const suitPtr&, const suitPtr&, const suitPtr& ) const;

private:
  SUIT_DataObject* myRoot;
};

#endif

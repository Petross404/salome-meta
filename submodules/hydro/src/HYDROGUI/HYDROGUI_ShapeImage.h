// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_SHAPE_IMAGE_H
#define HYDROGUI_SHAPE_IMAGE_H

#include <HYDROGUI_Shape.h>

#include <QPolygonF>

class HYDROData_Image;

class HYDROGUI_ShapeImage : public HYDROGUI_Shape
{
public:
  HYDROGUI_ShapeImage( const Handle(AIS_InteractiveContext)& theContext,
                       const Handle(HYDROData_Image)&        theImage,
                       const int                             theZLayer = -1 );
  virtual ~HYDROGUI_ShapeImage();

  virtual void                         update( bool isUpdateViewer, bool isDeactivateSelection );

protected:
  virtual QList<Handle(AIS_InteractiveObject)> createShape() const;
  Handle(Image_PixMap)                  imagePixMap( const Handle(HYDROData_Image)&, QString& theMessage ) const;
  QPolygonF                             imageContour( const Handle(HYDROData_Image)& ) const;
};

#endif

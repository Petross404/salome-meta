// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_IMAGEPRS_H
#define HYDROGUI_IMAGEPRS_H

#include <AIS_InteractiveObject.hxx>

#include <Image_PixMap.hxx>

#include <QPolygonF>

class Font_FTFont;
class HYDROGUI_ImagePrs;

class HYDROGUI_ImagePrs : public AIS_InteractiveObject
{
public:
    HYDROGUI_ImagePrs();
    HYDROGUI_ImagePrs( const Handle(Image_PixMap)&, const QPolygonF& );
    virtual ~HYDROGUI_ImagePrs();

    QPolygonF              GetContour() const;
    void                   SetContour( const QPolygonF& );

    Handle(Image_PixMap)   GetImage() const;
    void                   SetImage( const Handle(Image_PixMap)& );

    virtual void           Compute( const Handle(PrsMgr_PresentationManager3d)&,
                                    const Handle(Prs3d_Presentation)&, const Standard_Integer = 0 );
    virtual void           ComputeSelection( const Handle(SelectMgr_Selection)&, const Standard_Integer );

    DEFINE_STANDARD_RTTIEXT(HYDROGUI_ImagePrs, AIS_InteractiveObject)

private:
    gp_Pnt                 convert( const QPointF& ) const;

private:
    Handle(Image_PixMap)   myImage;
    QPolygonF              myContour;
};

#endif

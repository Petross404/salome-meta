// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_H
#define HYDROGUI_H

#ifdef HYDROGUI_STATIC
  #define HYDRO_EXPORT
#else
  #if defined HYDROGUI_EXPORTS
    #if defined WNT
      #define HYDRO_EXPORT   __declspec( dllexport )
    #else
      #define HYDRO_EXPORT
    #endif
  #else
    #if defined WNT
      #define HYDRO_EXPORT   __declspec( dllimport )
    #else
      #define HYDRO_EXPORT
    #endif
  #endif
#endif

#ifdef WNT
#pragma warning ( disable: 4251 )
#pragma warning ( disable: 4267 )
#pragma warning ( disable: 4311 )
#pragma warning ( disable: 4312 )
#endif

#if defined ( _DEBUG ) || defined ( DEBUG )
#include <assert.h>
#define HYDRO_VERIFY(x)        (assert(x))
#define HYDRO_ASSERT(x)        (assert(x))
#else
#define HYDRO_VERIFY(x)        (x)
#define HYDRO_ASSERT(x)
#endif

#define HYDRO_VERSION "0.1"

#endif


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_OBJCOMBOBOX_H
#define HYDROGUI_OBJCOMBOBOX_H

#include <HYDROData_Entity.h>
#include <QComboBox>

class HYDROGUI_Module;
class HYDROGUI_ObjComboBoxFilter;
class SUIT_SelectionMgr;

class HYDROGUI_ObjComboBox : public QWidget
{
    Q_OBJECT

public:
    HYDROGUI_ObjComboBox( HYDROGUI_Module* theModule, const QString& theTitle, const ObjectKind& = KIND_UNKNOWN, QWidget* = 0 );
    virtual ~HYDROGUI_ObjComboBox();

    HYDROGUI_Module*            module() const;

    ObjectKind                  objectType() const;

    HYDROGUI_ObjComboBoxFilter* objectFilter() const;
    void                        setObjectFilter( HYDROGUI_ObjComboBoxFilter* );

    QString                     selectedObject() const;
    void                        setSelectedObject( const QString& );

    void                        reset();

    Handle( HYDROData_Entity )  GetObject() const;

signals:
    void                        objectSelected( const QString& );

private slots:
    void                        onSelectionChanged();
    void                        onIndexChanged( int );

    void                        updateSelection();

protected:
    QStringList                 objectNames( HYDROData_SequenceOfObjects& ) const;
    SUIT_SelectionMgr*          selectionMgr() const;

private:
    ObjectKind                  myType;
    QComboBox*                  myObject;
    HYDROGUI_Module*            myModule;
    HYDROGUI_ObjComboBoxFilter* myFilter;
    HYDROData_SequenceOfObjects myObjects;
};

class HYDROGUI_ObjComboBoxFilter
{
public:
    HYDROGUI_ObjComboBoxFilter() {};
    virtual ~HYDROGUI_ObjComboBoxFilter() {};

    virtual bool isActive( HYDROGUI_ObjComboBox* ) const { return true; };
    virtual bool isOk( const Handle(HYDROData_Entity)& ) const = 0;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_VTK_PRS_SHAPE_H
#define HYDROGUI_VTK_PRS_SHAPE_H

#include "HYDROGUI_VTKPrs.h"

#include <HYDROData_Entity.h>

#include <vtkScalarsToColors.h>
#include <vtkWeakPointer.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>

#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Compound.hxx>

#include <QColor>
#include <QString>

class TopoDS_Shape;

/*
  Class       : HYDROGUI_VTKPrsShape
  Description : Presentation for HYDRO object on the base of OCC shape
*/
class HYDROGUI_VTKPrsShape : public HYDROGUI_VTKPrs
{
public:
  HYDROGUI_VTKPrsShape( const Handle(HYDROData_Entity)& theObject );
  virtual ~HYDROGUI_VTKPrsShape();

  virtual void compute();

  //! Get the range of colored 
  void setLookupTable( vtkScalarsToColors* theTable ) { myLookupTable = theTable; }

protected:
  virtual vtkMapper*               mapper() { return myMapper.GetPointer(); }

  void buildShape();

  virtual void               setWire( const TopoDS_Wire& theWire,
                                      const bool         theToDisplay = true,
                                      const bool         theIsUpdateViewer = true );

  virtual void               setFaces( const TopoDS_Compound& theWires,
                                       const bool             theToDisplay = true,
                                       const bool             theIsUpdateViewer = true );

  virtual void               setFace( const TopoDS_Wire& theWire,
                                      const bool         theToDisplay = true,
                                      const bool         theIsUpdateViewer = true );

  virtual void               setFace( const TopoDS_Face& theFace,
                                      const bool         theToDisplay = true,
                                      const bool         theIsUpdateViewer = true );

  virtual void               setShape( const TopoDS_Shape& theShape,
                                       const bool          theToDisplay = true,
                                       const bool          theIsUpdateViewer = true );
private:
  vtkWeakPointer< vtkScalarsToColors > myLookupTable;
  vtkNew< vtkPolyDataMapper >          myMapper;
  TopoDS_Shape                         myTopoShape;
  int                                  myDisplayMode;
};

#endif

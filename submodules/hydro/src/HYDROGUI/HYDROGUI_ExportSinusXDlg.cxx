// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ExportSinusXDlg.h>
#include <HYDROGUI_Module.h>
#include <QtxDoubleSpinBox.h>
#include <QLayout>
#include <QLabel>
#include <QStackedWidget>
#include <QPushButton>
#include "HYDROGUI_Module.h"
#include <CAM_Application.h>
#include <SUIT_Desktop.h>
#include <QListWidget>
#include <HYDROGUI_ObjListBox.h>


HYDROGUI_ExportSinusXDlg::HYDROGUI_ExportSinusXDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle, false )
{
  QFrame* aFrame = new QFrame( mainFrame() );
  addWidget( aFrame, 1 );

  QGridLayout* aLayout = new QGridLayout( aFrame );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  QList<ObjectKind> anAllowedTypesToSave;
  anAllowedTypesToSave.append(KIND_BATHYMETRY);   
  anAllowedTypesToSave.append(KIND_POLYLINEXY);
  anAllowedTypesToSave.append(KIND_PROFILE);

  myList = new HYDROGUI_ObjListBox( theModule, tr( "OBJECTS_TO_EXPORT" ), anAllowedTypesToSave, mainFrame() );
  aLayout->addWidget( myList, 0, 0 );

  myExportBtn = new QPushButton( tr( "EXPORT" ) );

  QHBoxLayout* aBtnsLayout = qobject_cast<QHBoxLayout*>(buttonFrame()->layout());
  aBtnsLayout->addWidget( myExportBtn, 0 );
  aBtnsLayout->addWidget( myCancel, 0 );
  aBtnsLayout->addStretch( 1 );
  aBtnsLayout->addWidget( myHelp, 0 );

  connect( myExportBtn, SIGNAL( clicked() ), SIGNAL( ExportItems() ) );

}

HYDROGUI_ExportSinusXDlg::~HYDROGUI_ExportSinusXDlg()
{
}


HYDROData_SequenceOfObjects HYDROGUI_ExportSinusXDlg::GetSelectedEntities()
{
  return myList->selectedObjects();
}
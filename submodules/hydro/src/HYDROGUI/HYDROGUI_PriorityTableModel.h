// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PriorityTableModel_H
#define HYDROGUI_PriorityTableModel_H

#include <HYDROGUI.h>
#include <HYDROData_PriorityQueue.h>

#include <QAbstractTableModel>
#include <QStringList>


class HYDROGUI_Module;


/** 
 * \class HYDROGUI_PriorityTableModel
 * \brief The class representing custom table model for the priority rules
 */
class HYDRO_EXPORT HYDROGUI_PriorityTableModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  HYDROGUI_PriorityTableModel( QObject* theParent = 0 );
  virtual ~HYDROGUI_PriorityTableModel();

  virtual Qt::ItemFlags	flags( const QModelIndex & theIndex ) const;

  virtual QVariant data( const QModelIndex &theIndex, int theRole = Qt::DisplayRole ) const;  
  virtual bool setData( const QModelIndex & theIndex, const QVariant & theValue, int theRole = Qt::EditRole );

  virtual int rowCount( const QModelIndex &theParent = QModelIndex() ) const;
  virtual int columnCount( const QModelIndex &theParent = QModelIndex() ) const;

  virtual QVariant headerData( int theSection,
                               Qt::Orientation theOrientation,
                               int theRole = Qt::DisplayRole ) const;

  virtual bool removeRows( int theRow, int theCount, const QModelIndex & theParent = QModelIndex() );
  
  bool removeRows( const QList<int> theRows );
  bool removeAll();

  void setObjects( const QList<Handle(HYDROData_Entity)>& theObjects );

  void setRules( const HYDROData_ListOfRules& theObjects );
  HYDROData_ListOfRules getRules() const;

  bool createNewRule();
  bool canCreateNewRule() const;

  void setColumnCount( int theColumnCount );

  void undoLastChange();
  
protected:
  bool isUsed( const Handle(HYDROData_Entity)& theObj1, 
               const Handle(HYDROData_Entity)& theObj2 ) const;

  QStringList getAvailablePairs( const Handle(HYDROData_Entity)& theObject ) const;
  QStringList getAvailableObjectNames() const;

  QString priorityToString( const int thePriority ) const;
  QString mergeTypeToString( const int theMergeType ) const;

signals:
  void showError( const QString& theMsg );
  void ruleChanged();

private:
  const ObjectKind getObjectsKind() const;

  friend class test_HYDROGUI_PriorityTableModel;

  HYDROGUI_Module* myModule;

  HYDROData_ListOfRules myRules, myPrevRules;
  QList<Handle(HYDROData_Entity)> myObjects;

  int myColumnCount;
};

#endif
// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VTKPrsBathymetryDriver.h"

#include "HYDROGUI_VTKPrsBathymetry.h"

#include <HYDROData_Bathymetry.h>

HYDROGUI_VTKPrsBathymetryDriver::HYDROGUI_VTKPrsBathymetryDriver( vtkScalarBarActor* theScalarBar )
{
  myScalarBar = theScalarBar;
}

HYDROGUI_VTKPrsBathymetryDriver::~HYDROGUI_VTKPrsBathymetryDriver()
{
}

bool HYDROGUI_VTKPrsBathymetryDriver::Update( const Handle(HYDROData_Entity)& theObj,
                                         HYDROGUI_VTKPrs*& thePrs )
{
  HYDROGUI_VTKPrsDriver::Update( theObj, thePrs );

  if( theObj.IsNull() )
    return false;

  Handle(HYDROData_Bathymetry) aBathymetry = Handle(HYDROData_Bathymetry)::DownCast( theObj );
  if( aBathymetry.IsNull() )
    return false;

  if( !thePrs )
    thePrs = new HYDROGUI_VTKPrsBathymetry( aBathymetry );

  HYDROGUI_VTKPrsBathymetry* aPrsBathymetry = (HYDROGUI_VTKPrsBathymetry*)thePrs;
  // Update global colors table during compute if necessary
  aPrsBathymetry->setLookupTable( myScalarBar->GetLookupTable() );
  aPrsBathymetry->compute();

  return true;
}

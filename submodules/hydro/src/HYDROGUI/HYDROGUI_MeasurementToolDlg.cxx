// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_MeasurementToolDlg.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>
#include <CurveCreator_Utils.hxx>
#include <SUIT_ViewWindow.h>
#include <QMouseEvent>
#include <gp_Pnt.hxx>
#include <QTableWidget>
#include <QGroupBox>
#include <QFormLayout>
#include <QLabel>
#include <QComboBox>

#include <QRadioButton>
#include <QStringList>


HYDROGUI_MeasurementToolDlg::HYDROGUI_MeasurementToolDlg( QWidget* theParent )
  : QDialog( theParent ), myExit (false)
{  
  QFormLayout* aMLayout = new QFormLayout( this );

  mySwitcherLD = new QRadioButton(tr("LINEAR_DISTANCE"));
  QRadioButton* aSwitcherPD = new QRadioButton(tr("POLYLINE_DISTANCE"));

  myPolylinesCBox = new QComboBox( this );

  aMLayout->addRow(mySwitcherLD);
  aMLayout->addRow(aSwitcherPD, myPolylinesCBox);

  myTotalDst = new QLineEdit( this );
  myTotalDst->setReadOnly(true);

  aMLayout->addRow(new QLabel(tr("TOTAL_DST")), myTotalDst);

  QPushButton* aClearButton = new QPushButton( tr("CLEAR") );

  QPushButton* aCloseButton = new QPushButton( tr("CLOSE"), this );

  QGroupBox* aPolyLenGroup = new QGroupBox(tr("POLYLINES_LENGTH"));
  QVBoxLayout* aPolyLayout = new QVBoxLayout( aPolyLenGroup );

  aMLayout->addRow(aClearButton, aCloseButton);
  ////
  connect( aClearButton, SIGNAL( clicked() ), this, SLOT( onClear () ) );
  connect( aCloseButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( this, SIGNAL( rejected() ), this, SLOT( onExit () ) );
  connect( mySwitcherLD, SIGNAL( toggled(bool) ), this, SLOT( onSetMode (bool) ) );
  connect( myPolylinesCBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onClear () ) );

  mySwitcherLD->setChecked(true);

  setLayout(aMLayout);
}

HYDROGUI_MeasurementToolDlg::~HYDROGUI_MeasurementToolDlg()
{
}

void HYDROGUI_MeasurementToolDlg::setTotalDst(QString strVal)
{
  myTotalDst->setText(strVal);
}

bool HYDROGUI_MeasurementToolDlg::IsLDChecked() const
{
  return mySwitcherLD->isChecked();
}

void HYDROGUI_MeasurementToolDlg::setPolylineNames(QStringList thePolyNames)
{
  myPolylinesCBox->clear();
  foreach (QString aName, thePolyNames)
    myPolylinesCBox->addItem(aName);
}

int HYDROGUI_MeasurementToolDlg::currentSelectedPolyline() const
{
  return myPolylinesCBox->currentIndex();
}

bool HYDROGUI_MeasurementToolDlg::GetExitFlag() const
{
  return myExit;
}

void HYDROGUI_MeasurementToolDlg::onExit()
{
  myExit = true;
  emit doExit();
}

void HYDROGUI_MeasurementToolDlg::onClear()
{
  emit clearLine();
}

void HYDROGUI_MeasurementToolDlg::onSetMode(bool mode)
{
  emit onClear();
  if (mode)
    myPolylinesCBox->setDisabled(true);
  else
    myPolylinesCBox->setDisabled(false);
}




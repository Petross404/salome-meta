// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PriorityWidget.h"

#include "HYDROGUI_PriorityTableModel.h"

#include "HYDROData_PriorityQueue.h"

#include <SUIT_MessageBox.h>

#include <QComboBox>
#include <QHeaderView>
#include <QLayout>
#include <QTableView>
#include <QToolButton>


/**
  Constructor.
  @param theParent the parent object
 */
HYDROGUI_PriorityWidget::Delegate::Delegate( QObject* theParent )
 : QStyledItemDelegate( theParent )
{
}

/**
 */
QWidget* HYDROGUI_PriorityWidget::Delegate::createEditor(
  QWidget* theParent, const QStyleOptionViewItem& theOption,
  const QModelIndex& theIndex ) const
{
  QComboBox* aComboBox = new QComboBox( theParent );

  if ( theIndex.column() == 1 || theIndex.column() == 3 ) {
    QMap<QString, QVariant> aMap = theIndex.data( Qt::UserRole ).toMap();
    foreach ( QString aText, aMap.keys() ) {
      aComboBox->addItem( aText, aMap.value( aText ).toInt() );
    }
  } else if ( theIndex.column() == 0 || theIndex.column() ==  2 ) {
    QStringList anObjNames = theIndex.data( Qt::UserRole ).toStringList();
    aComboBox->addItems( anObjNames );
  }

  connect( aComboBox, SIGNAL( activated( int ) ), this, SLOT( finishEditing() ) );

  return aComboBox;
}

/**
 */
void HYDROGUI_PriorityWidget::Delegate::setEditorData(
  QWidget* theEditor, const QModelIndex& theIndex ) const
{
  QComboBox* aComboBox = dynamic_cast<QComboBox*>( theEditor );

  if ( aComboBox ) {
    int anIndex = -1;

    if ( theIndex.column() == 0 || theIndex.column() == 2 ) {
      QString aText = theIndex.data( Qt::EditRole ).toString();
      anIndex = aComboBox->findText( aText );
    } else {
      QVariant aData = theIndex.data( Qt::EditRole ).toInt();
      anIndex = aComboBox->findData( aData );
    }

    if ( anIndex >= 0 ) {
      aComboBox->setCurrentIndex( anIndex );
    }
  }
}

/**
 */
void HYDROGUI_PriorityWidget::Delegate::setModelData(
  QWidget* theEditor, QAbstractItemModel* theModel, const QModelIndex& theIndex) const
{
  QComboBox* aComboBox = dynamic_cast<QComboBox*>( theEditor );

  if ( aComboBox ) {
    int aColumn = theIndex.column();
    if ( aColumn == 0 || aColumn == 2 ) {
      QString aCurrentText = theIndex.data( Qt::EditRole ).toString();
      QString aNewText = aComboBox->currentText();
      if ( aNewText != aCurrentText ) {
        theModel->setData( theIndex, aNewText );
      }
    } else {
      theModel->setData( theIndex, aComboBox->itemData( aComboBox->currentIndex() ) );
    }
  }
}

/**
 Emit signal indicating that the user has finished editing.
 */
void HYDROGUI_PriorityWidget::Delegate::finishEditing()
{
  QWidget* anEditor = qobject_cast<QWidget*>( sender() );
  if ( anEditor ) {
    emit commitData( anEditor );
    emit closeEditor( anEditor );
  }
}


/**
  Constructor.
  @param theParent the parent widget
 */
HYDROGUI_PriorityWidget::HYDROGUI_PriorityWidget( QWidget* theParent )
: QWidget( theParent )
{
  // Main layout
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin( 0 );
  aMainLayout->setSpacing( 5 );

  // Buttons
  myAdd = new QToolButton;
  myAdd->setText( tr( "ADD" ) );
  myRemove = new QToolButton;
  myRemove->setText( tr( "REMOVE" ) );
  myClear = new QToolButton;
  myClear->setText( tr( "CLEAR_ALL" ) );

  // Table view
  myTable = new QTableView( this );
  myTable->setItemDelegate( new Delegate( this ) );
  myTable->setEditTriggers( QAbstractItemView::DoubleClicked |
                            QAbstractItemView::SelectedClicked |
                            QAbstractItemView::EditKeyPressed );

  // Set the custom model
  HYDROGUI_PriorityTableModel* aModel = new HYDROGUI_PriorityTableModel();
  myTable->setModel( aModel );

  // Set resize mode
  myTable->horizontalHeader()->setStretchLastSection( false);
  myTable->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );
  myTable->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::ResizeToContents );
  myTable->horizontalHeader()->setSectionResizeMode( 2, QHeaderView::Stretch );
  myTable->horizontalHeader()->setSectionResizeMode( 3, QHeaderView::ResizeToContents );

  myTable->verticalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );

  // Layout
  // buttons
  QHBoxLayout* aButtonsLayout = new QHBoxLayout();
  aButtonsLayout->addWidget( myAdd );
  aButtonsLayout->addWidget( myRemove );
  aButtonsLayout->addStretch( 1 );
  aButtonsLayout->addWidget( myClear );

  // main
  aMainLayout->addLayout( aButtonsLayout );
  aMainLayout->addWidget( myTable );

  // Update controls
  updateControls();

  // Connections
  connect( myAdd, SIGNAL( clicked() ), this, SLOT( onAddRule() ) );
  connect( myRemove, SIGNAL( clicked() ), this, SLOT( onRemoveRule() ) );
  connect( myClear, SIGNAL( clicked() ), this, SLOT( onClearRules() ) );

  connect ( myTable->selectionModel(), SIGNAL( selectionChanged( QItemSelection, QItemSelection ) ),
            this, SLOT( onSelectionChanged() ) );

  connect( aModel, SIGNAL( showError( const QString& ) ), this, SLOT( onShowError( const QString& ) ) );
  connect( aModel, SIGNAL( ruleChanged() ), this, SIGNAL( ruleChanged() ) );
}

/**
  Destructor.
 */
HYDROGUI_PriorityWidget::~HYDROGUI_PriorityWidget()
{
}

/**
  Add the new default constructed rule.
 */
void HYDROGUI_PriorityWidget::onAddRule()
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if ( aModel ) {
    if (aModel->createNewRule()) {
      updateControls();
      emit ruleChanged();
    }
  }
}

/**
  Remove the selected rule.
 */
void HYDROGUI_PriorityWidget::onRemoveRule()
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );

  if (aModel) {
    QList<int> aRows;

    QModelIndexList aSelectedIndexes = myTable->selectionModel()->selectedIndexes();
    foreach ( const QModelIndex& anIndex, aSelectedIndexes ) {
      int aRowIndex = anIndex.row();
      if ( !aRows.contains( aRowIndex ) )
        aRows << aRowIndex;
    }

    if ( aModel->removeRows( aRows ) ) {
      updateControls();
      emit ruleChanged();
    }
  }
}

/**
  Clear all rules.
 */
void HYDROGUI_PriorityWidget::onClearRules()
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if ( aModel && aModel->removeAll() ) {
    updateControls();
    emit ruleChanged();
  }
}

/**
  Set objects which could be used for rules definition.
  @param theObjects the ordered list of objects
 */
void HYDROGUI_PriorityWidget::setObjects( const QList<Handle(HYDROData_Entity)>& theObjects )
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if( aModel ) {
    aModel->setObjects( theObjects );
    updateControls();
  }
}

/**
  Get rules.
  @return the list of rules
 */
HYDROData_ListOfRules HYDROGUI_PriorityWidget::getRules() const
{
  HYDROData_ListOfRules aRules;

  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if( aModel ) {
    aRules = aModel->getRules();
  }

  return aRules;
}

/**
  Set rules.
  @param theRules the list of rules
*/
void HYDROGUI_PriorityWidget::setRules( const HYDROData_ListOfRules& theRules )
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if( aModel ) {
    aModel->setRules( theRules );
    updateControls();
  }
}

/**
  Get table view.
  @return the table view
 */
QTableView* HYDROGUI_PriorityWidget::getTable() const
{
  return myTable;
}

/**
 Slot called on table selection change.
*/
void HYDROGUI_PriorityWidget::onSelectionChanged()
{
  QModelIndexList aSelectedIndexes = myTable->selectionModel()->selectedIndexes();
  myRemove->setEnabled( aSelectedIndexes.count() > 0 );
}

/**
 Update GUI controls state.
 */
void HYDROGUI_PriorityWidget::updateControls()
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if( aModel ) {
    myAdd->setEnabled( aModel->canCreateNewRule() );
    bool isTableNotEmpty = aModel->rowCount() > 0;
    myClear->setEnabled( isTableNotEmpty );
  }
  onSelectionChanged();
}

/**
 Show error message.
 */
void HYDROGUI_PriorityWidget::onShowError( const QString& theMsg ) {
  SUIT_MessageBox::warning( this, tr( "INCORRECT_INPUT" ), theMsg );
}

/**
 Undo last change in priority rules table.
 */
void HYDROGUI_PriorityWidget::undoLastChange()
{
  HYDROGUI_PriorityTableModel* aModel =
    dynamic_cast<HYDROGUI_PriorityTableModel*>( myTable->model() );
  if ( aModel )
    aModel->undoLastChange();
}

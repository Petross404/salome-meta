// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_VISUALSTATEOP_H
#define HYDROGUI_VISUALSTATEOP_H

#include "HYDROGUI_Operation.h"

#include <STD_Application.h>

class HYDROGUI_VisualStateOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_VisualStateOp( HYDROGUI_Module* theModule, const bool theIsLoad );
  virtual ~HYDROGUI_VisualStateOp();

protected:
  virtual void               startOperation();

protected:
  bool                       saveVisualState();
  bool                       loadVisualState();

private:
  static void                nameViewWindows( const ViewManagerList& lst );

  typedef QMap        < QString, QStringList > PropertyMap;
  typedef QMapIterator< QString, QStringList > PropertyMapIterator;
  static QString             encodePropertyMap( const PropertyMap& thePropertyMap );
  static PropertyMap         decodePropertyMap( const QString& theString );

  static void                setVisualProperty( PropertyMap& thePropertyMap,
                                                const QString& thePropertyName,
                                                const QString& thePropertyData,
                                                const bool theIsAppend = false );

  static QString             getVisualProperty( const PropertyMap& thePropertyMap,
                                                const QString& thePropertyName,
                                                int theIndex = 0 );

  static int                 nbVisualProperties( const PropertyMap& thePropertyMap,
                                                 const QString& thePropertyName );

private:
  bool                       myIsLoad;
};

#endif

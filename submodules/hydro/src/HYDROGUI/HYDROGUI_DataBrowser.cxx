// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DataBrowser.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_DataObject.h"

#include <LightApp_Application.h>
#include <LightApp_OBSelector.h>
#include <LightApp_SelectionMgr.h>
#include <QHeaderView>
#include <QtxSearchTool.h>
#include <QtxTreeView.h>
#include <SUIT_DataObject.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_SelectionMgr.h>
#include <SUIT_DataObjectIterator.h>

#define VISIBILITY_COLUMN_WIDTH 25


#include <SUIT_Selector.h>
#include <SUIT_DataOwner.h>

#include <QObject>
#include <QShortcut>
#include <QMenu>

class SUIT_DataBrowser;
class LightApp_DataObject;


#include "LightApp_DataOwner.h"
#include "LightApp_DataObject.h"
#include "LightApp_Application.h"
#include <SUIT_DataBrowser.h>
#include <SUIT_Session.h>
#include <SUIT_DataObjectIterator.h>
#include <QTime>
#include <time.h>


// The selector is redefined in order to correct the selection in the browser.
// The main modification is to call fillEntries without the selector modified
// time compare. The modified time is result of the clock() method.
// On Linux, the method clock() returns the same values with some delay. So, it is possible,
// that time has the same value, but the browser has already other objects.
// So, the obsole entries can be in the saved entries by the filled method.
// May be it will be improved in the latest version of GUI_SRC.
// This redefinition is done for tag V7_3_0 of GUI_SRC.
class HYDROGUI_OBSelector : public LightApp_OBSelector
{
public:
  HYDROGUI_OBSelector( SUIT_DataBrowser*, SUIT_SelectionMgr* );
  virtual ~HYDROGUI_OBSelector();

protected:
  virtual void       getSelection( SUIT_DataOwnerPtrList& ) const;
  virtual void       setSelection( const SUIT_DataOwnerPtrList& );

private:
  void               fillEntries( QMap<QString, LightApp_DataObject*>& );

private:
  SUIT_DataOwnerPtrList               mySelectedList;
  QMap<QString, LightApp_DataObject*> myEntries;
};

HYDROGUI_OBSelector::HYDROGUI_OBSelector( SUIT_DataBrowser* ob, SUIT_SelectionMgr* mgr )
: LightApp_OBSelector( ob, mgr )
{
}

/*!
  \brief Destructor.
*/
HYDROGUI_OBSelector::~HYDROGUI_OBSelector()
{
}

/*!
  \brief Get list of currently selected objects.
  \param theList list to be filled with the selected objects owners
  \ This method is necessary to fill the cach containter mySelectedList
  \ It is the same as in LightApp_OBSelector
*/
void HYDROGUI_OBSelector::getSelection( SUIT_DataOwnerPtrList& theList ) const
{
  SUIT_DataBrowser* aBrowser = browser();

  if ( mySelectedList.count() == 0 ) {
    SUIT_Session* session = SUIT_Session::session();
    SUIT_Application* sapp = session ? session->activeApplication() : 0;
    LightApp_Application* app = dynamic_cast<LightApp_Application*>( sapp );
    if( !app || !aBrowser )
      return;

    DataObjectList objlist;
    aBrowser->getSelected( objlist );
    HYDROGUI_OBSelector* that = (HYDROGUI_OBSelector*)this;
    QListIterator<SUIT_DataObject*> it( objlist );
    while ( it.hasNext() ) {
      LightApp_DataObject* obj = dynamic_cast<LightApp_DataObject*>( it.next() );
      if ( obj && app->checkDataObject( obj) ) {
#ifndef DISABLE_SALOMEOBJECT
        Handle(SALOME_InteractiveObject) aSObj = new SALOME_InteractiveObject
          ( obj->entry().toLatin1().constData(),
            obj->componentDataType().toLatin1().constData(),
            obj->name().toLatin1().constData() );
        LightApp_DataOwner* owner = new LightApp_DataOwner( aSObj  );
#else
        LightApp_DataOwner* owner = new LightApp_DataOwner( obj->entry() );
#endif
        that->mySelectedList.append( SUIT_DataOwnerPtr( owner ) );
      }
    }
  }
  theList = mySelectedList;
}

/*!
  \brief Set selection.
  \param theList list of the object owners to be set selected
  \ It is the same as in LightApp_OBSelector. The difference is in the row with
  \ the modification time check.
*/
void HYDROGUI_OBSelector::setSelection( const SUIT_DataOwnerPtrList& theList )
{
  SUIT_DataBrowser* aBrowser = browser();
  if ( !aBrowser )
    return;

  // this is the difference to LightApp_OBSelector. For this, this class is redefined
  //if( myEntries.count() == 0 || myModifiedTime < aBrowser->getModifiedTime() )
  {
    fillEntries( myEntries );
  }

  DataObjectList objList;
  for ( SUIT_DataOwnerPtrList::const_iterator it = theList.begin();
        it != theList.end(); ++it ) {
    const LightApp_DataOwner* owner = dynamic_cast<const LightApp_DataOwner*>( (*it).operator->() );

    if ( owner && myEntries.contains( owner->entry() ) )
      objList.append( myEntries[owner->entry()] );
  }

  aBrowser->setSelected( objList );
  mySelectedList.clear();
}

/*!
  \brief Fill map of the data objects currently shown in the Object Browser.
  \param entries map to be filled
  \ It is the same as in LightApp_OBSelector
*/
void HYDROGUI_OBSelector::fillEntries( QMap<QString, LightApp_DataObject*>& entries )
{
  entries.clear();

  SUIT_DataBrowser* aBrowser = browser();
  if ( !aBrowser )
    return;

  for ( SUIT_DataObjectIterator it( aBrowser->root(),
                                    SUIT_DataObjectIterator::DepthLeft ); it.current(); ++it ) {
    LightApp_DataObject* obj = dynamic_cast<LightApp_DataObject*>( it.current() );
    if ( obj )
      entries.insert( obj->entry(), obj );
  }

  setModified();
}


HYDROGUI_DataBrowser::HYDROGUI_DataBrowser( HYDROGUI_Module* theModule,
                                            SUIT_DataObject* theRoot,
                                            QWidget* theParent,
                                            bool theLandCover/* = false*/)
: SUIT_DataBrowser( theRoot, theParent ), myModule( theModule )
{
  SUIT_ResourceMgr* resMgr = theModule->getApp()->resourceMgr();

  if ( ( !theRoot ) && theModule )
  {
    // Initialize the root with the module data model
    setRoot( new CAM_ModuleObject( theModule->dataModel(), NULL ) );
  }

  setSortMenuEnabled( true );
  setAutoUpdate( true );
  setUpdateModified( true );

  if ( resMgr->hasValue( "ObjectBrowser", "auto_hide_search_tool" ) )
    searchTool()->enableAutoHide( resMgr->booleanValue( "ObjectBrowser", "auto_hide_search_tool" ) );

  setWindowTitle( tr( "OBJECT_BROWSER" ) );
  connect( this, SIGNAL( requestUpdate() ), theModule->getApp(), SLOT( onRefresh() ) );

  QString EntryCol = QObject::tr( "ENTRY_COLUMN" );
  QString RefObjCol = tr( "REF_OBJECT_COLUMN" );
  QString AltitudeCol;
  if ( theLandCover )
    AltitudeCol = tr( "LAND_COVER_COLUMN" );
  else
    AltitudeCol = tr( "ALTITUDE_COLUMN" );

  SUIT_AbstractModel* treeModel = dynamic_cast<SUIT_AbstractModel*>( model() );
  //RKV: treeModel->setSearcher( theModule->getApp() );
  treeModel->setSearcher( this ); //RKV
  treeModel->registerColumn( 0, EntryCol, LightApp_DataObject::EntryId );
  treeModel->setAppropriate( EntryCol, Qtx::Toggled );
  treeModel->registerColumn( 0, RefObjCol, HYDROGUI_DataObject::RefObjectId );
  treeModel->setAppropriate( RefObjCol, Qtx::Toggled );
  treeModel->registerColumn( 0, AltitudeCol, HYDROGUI_DataObject::AltitudeObjId );
  treeModel->setAppropriate( AltitudeCol, Qtx::Toggled );

  // Mantis issue 0020136: Drag&Drop in OB
  SUIT_ProxyModel* proxyModel = dynamic_cast<SUIT_ProxyModel*>(treeModel);
  if ( proxyModel ) {
    connect( proxyModel,
      SIGNAL( dropped( const QList<SUIT_DataObject*>&, SUIT_DataObject*, int, Qt::DropAction ) ),
      SIGNAL( dropped( const QList<SUIT_DataObject*>&, SUIT_DataObject*, int, Qt::DropAction ) ) );

    //// Connect signal emitted after editing for updating after objects renaming
    SUIT_TreeModel* aMiniModel = dynamic_cast<SUIT_TreeModel*>( proxyModel->sourceModel() );
    if ( aMiniModel )
    {
      connect( aMiniModel, SIGNAL( dataChanged( const QModelIndex &, const QModelIndex & ) ),
        SIGNAL( dataChanged() ) );
    }

    // Do updating also in the module's main object browser.
    if ( theModule )
    {
      SUIT_DataBrowser* aModulBrowser = theModule->getApp()->objectBrowser();
      if ( aModulBrowser )
      {
        SUIT_ProxyModel* aPModel = dynamic_cast<SUIT_ProxyModel*>(aModulBrowser->model());
        if ( aPModel )
        {
          SUIT_TreeModel* aModel = dynamic_cast<SUIT_TreeModel*>(aPModel->sourceModel());
          //connect( proxyModel, SIGNAL( dataChanged( const QModelIndex &, const QModelIndex & ) ),
          //  aPModel, SIGNAL( dataChanged( const QModelIndex &, const QModelIndex & ) ) );
          //connect( proxyModel, SIGNAL( dataChanged( const QModelIndex &, const QModelIndex & ) ),
          //  aModel, SIGNAL( dataChanged( const QModelIndex &, const QModelIndex & ) ) );
          connect( proxyModel, SIGNAL( modelUpdated() ), aModel, SIGNAL( modelUpdated() ) );
        }
      }
    }
  }

  // temporary commented
  /*
  OB_ListView* ob_list = dynamic_cast<OB_ListView*>( const_cast<QListView*>( listView() ) );
  if( ob_list )
    ob_list->setColumnMaxWidth( 0, theModule->getApp()->desktop()->width()/4 );

  setFilter( new LightApp_OBFilter( theModule->getApp()->selectionMgr() ) );
  */

  // Create OBSelector
  new HYDROGUI_OBSelector( this, theModule->getApp()->selectionMgr() );

  treeView()->header()->setSectionResizeMode(SUIT_DataObject::VisibilityId, QHeaderView::Fixed);
  treeView()->header()->moveSection(SUIT_DataObject::NameId,SUIT_DataObject::VisibilityId);
  treeView()->setColumnWidth(SUIT_DataObject::VisibilityId, VISIBILITY_COLUMN_WIDTH);
  treeView()->hideColumn( SUIT_DataObject::VisibilityId );
  treeView()->hideColumn( LightApp_DataObject::EntryId );
  connectPopupRequest( theModule->getApp(), SLOT( onConnectPopupRequest( SUIT_PopupClient*, QContextMenuEvent* ) ) );
}

HYDROGUI_DataBrowser::~HYDROGUI_DataBrowser()
{
}

SUIT_DataObject* HYDROGUI_DataBrowser::findObject( const QString& theEntry ) const
{
  LightApp_DataObject* aCurObj;
  for ( SUIT_DataObjectIterator it( root(), SUIT_DataObjectIterator::DepthLeft ); it.current(); ++it ) {
    aCurObj = dynamic_cast<LightApp_DataObject*>( it.current() );
    if ( aCurObj && aCurObj->entry() == theEntry )
      return aCurObj;
  }
  return NULL;
}

/*!
  \brief Switch read only mode for the Object Browser.
  \param theIsReadOnly if true - read only mode will be turned on
*/
void HYDROGUI_DataBrowser::setReadOnly( const bool theIsReadOnly )
{
  //TODO: to be reimplemented

  // Enable/disable edit triggers
  foreach ( QTreeView* aView, findChildren<QTreeView*>() ) {
    aView->setDragEnabled ( !theIsReadOnly );
    aView->setEditTriggers ( theIsReadOnly ?
                             QAbstractItemView::NoEditTriggers :
                             QAbstractItemView::DoubleClicked );
  }

  // Enable/disable rename shortcut
  QList<QShortcut*> aShortcuts = findChildren<QShortcut*>();
  QShortcut* aShortcut;
  foreach( aShortcut, aShortcuts ) {
    if ( aShortcut->key() == QKeySequence( shortcutKey( RenameShortcut ) ) ) {
      aShortcut->setEnabled( !theIsReadOnly );
    }
  }
}

void HYDROGUI_DataBrowser::createPopupMenu( QMenu* theMenu )
{
  theMenu->clear();
  DataObjectList aSelection = getSelected();
  bool isOnlyZones = aSelection.size() > 0;
  foreach( SUIT_DataObject* anObj, aSelection )
  {
    HYDROGUI_DataObject* aHydroObj = dynamic_cast<HYDROGUI_DataObject*>( anObj );
    if( aHydroObj )
    {
      Handle(HYDROData_Entity) aModelObj = aHydroObj->modelObject();
      if( !aModelObj.IsNull() )
      {
        isOnlyZones = aModelObj->GetKind()==KIND_ZONE;
        if( !isOnlyZones )
          break;

        SUIT_DataObject* aParentObj = aHydroObj->parent();
        if ( aParentObj )
        {
          isOnlyZones = aParentObj->childCount() > 1;
          if( !isOnlyZones )
            break;
        }
      }
      else
      {
        isOnlyZones = false;
        break;
      }
    }
    else
    {
      isOnlyZones = false;
      break;
    }
  }

  if( isOnlyZones )
    theMenu->addAction( tr( "ZONE_TO_NEW_REGION" ), this, SIGNAL( newRegion() ) );
}

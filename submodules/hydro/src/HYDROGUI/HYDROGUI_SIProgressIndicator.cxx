// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_SIProgressIndicator.h"

#include <QProgressBar>
#include <QVBoxLayout>
#include <QApplication>
//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

IMPLEMENT_STANDARD_RTTIEXT(HYDROGUI_SIProgressIndicator, Message_ProgressIndicator)

HYDROGUI_SIProgressIndicator::HYDROGUI_SIProgressIndicator( QWidget* theParent )
: QtxDialog( theParent, true, false, QtxDialog::Cancel ), myUserBreak(Standard_False)
{
  setWindowTitle( tr( "STRICKLER_INTERPOLATION_TLT" ) );

  QVBoxLayout* aLayout = new QVBoxLayout( mainFrame() );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  myBar = new QProgressBar( mainFrame() );

  aLayout->addWidget( myBar );

  setButtonText( Cancel, tr("CANCEL") );
  setButtonPosition( Center, Cancel );
  setMinimumWidth( 350 );
}

HYDROGUI_SIProgressIndicator::~HYDROGUI_SIProgressIndicator()
{
}

void HYDROGUI_SIProgressIndicator::Show(const Message_ProgressScope& theScope, const Standard_Boolean theForce)
{
  //DEBTRACE("...");
  Standard_Real aPosition = GetPosition();
  myCount++;
  if (theForce)
      DEBTRACE("aPosition=" << aPosition << " myCount:" <<myCount);
  Standard_Boolean isUserBreak = UserBreak();

  bool isFinished = aPosition >= 1 || ( isUserBreak );
  if ( isFinished && theForce ) { // theForce == true : call from main thread, Qt display safe
    if ( result() != Accepted ) {
      QDialog::accept();
    }
  } else if (!isVisible() && theForce) { // theForce == true : call from main thread, Qt display safe
    open();
  }

  if ( theForce ) {
    if ( !isUserBreak ) {
      int aNbSteps = myBar->maximum() - myBar->minimum();
      int aValue = int( aPosition * aNbSteps );
      myBar->setValue(aValue);
    }

    QApplication::processEvents();
  }

  return;
}

Standard_Boolean HYDROGUI_SIProgressIndicator::UserBreak()
{
  return myUserBreak;
}

void HYDROGUI_SIProgressIndicator::Reset()
{
  Message_ProgressIndicator::Reset();
  setButtonText( Cancel, tr("CANCEL") );
  setButtonEnabled( true, Cancel );
  myUserBreak = Standard_False;
  myCount = 0;
}

void HYDROGUI_SIProgressIndicator::reject()
{
  myUserBreak = Standard_True;
  setButtonText( Cancel, tr("CANCELLING") );
  setButtonEnabled( false, Cancel );
  QApplication::processEvents();
}
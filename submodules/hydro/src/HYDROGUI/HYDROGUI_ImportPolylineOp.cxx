// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportPolylineOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Tool2.h"
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_ShapeFile.h>
#include <HYDROData_Tool.h>

#include <HYDROData_Profile.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>
#include <LightApp_Application.h>

#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <SUIT_MessageBox.h>
#include <gp_XY.hxx>
#include <HYDROGUI_ImportPolylineDlg.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_Operation.h>

HYDROGUI_ImportPolylineOp::HYDROGUI_ImportPolylineOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "IMPORT_POLYLINE" ) );
}

HYDROGUI_ImportPolylineOp::~HYDROGUI_ImportPolylineOp()
{
}

void HYDROGUI_ImportPolylineOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myFileDlg = new SUIT_FileDlg( module()->getApp()->desktop(), true );
  myFileDlg->setWindowTitle( getName() );
  myFileDlg->setFileMode( SUIT_FileDlg::ExistingFiles );
  myFileDlg->setNameFilter( tr("POLYLINE_FILTER") );

  connect( myFileDlg, SIGNAL( accepted() ), this, SLOT( onApply() ) );
  connect( myFileDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );

  myFileDlg->exec();
}

NCollection_Sequence<Handle(HYDROData_Entity)> HYDROGUI_ImportPolylineOp::ImportPolyOp(
  const QStringList& aFileNames, Handle(HYDROData_Document) theDocument,
  HYDROGUI_Module* module, HYDROData_ShapeFile::ImportShapeType theShapeTypesToImport)
{
  NCollection_Sequence<Handle(HYDROData_Entity)> importedEntities;
  foreach (QString aFileName, aFileNames) 
  {
    if ( aFileName.isEmpty() )
      continue;

    QString anExt = aFileName.split('.', QString::SkipEmptyParts).back();
    anExt.toLower();
    bool importXY = false;
    if (anExt == "xyz")
    {
      importXY = SUIT_MessageBox::question( module->getApp()->desktop(),
        tr( "IMPORT_POLYLINE" ),
        tr( "IMPORT_POLYLINE_XY_PART_ONLY" ),
        QMessageBox::Yes | QMessageBox::No, 
        SUIT_MessageBox::Yes) == SUIT_MessageBox::Yes;
    }
    if (anExt == "shp")
    {
      QApplication::setOverrideCursor( Qt::WaitCursor );
      HYDROData_ShapeFile anImporter;
      NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*> theEntitiesToSHPObj;
      NCollection_Sequence<Handle(HYDROData_Entity)> PolyEnt;
      int aShapeTypeOfFile = -1;
      int aStat = anImporter.OpenAndParse(aFileName);
      QApplication::restoreOverrideCursor();
      if (aStat == 1)
      {
        QApplication::setOverrideCursor( Qt::WaitCursor );
        aStat = anImporter.ImportPolylines(theDocument, aFileName, theEntitiesToSHPObj, theShapeTypesToImport); 
        QApplication::restoreOverrideCursor();
        for (int k=1;k<=theEntitiesToSHPObj.Extent();k++)
        {
          PolyEnt.Append(theEntitiesToSHPObj.FindKey(k));
        }
      if (aStat == 1 || aStat == 2)
      {
        //try to import DBF
        QString aDBFFileName;
        aDBFFileName = aFileName.simplified().replace( aFileName.simplified().size() - 4, 4, ".dbf");
        bool DBF_Stat = anImporter.DBF_OpenDBF(aDBFFileName);
        if (DBF_Stat)
        {
          QStringList aFieldList = anImporter.DBF_GetFieldList();
          int nbRecords = anImporter.DBF_GetNbRecords();
            assert (theEntitiesToSHPObj.Extent() == nbRecords);
            if (theEntitiesToSHPObj.Extent() == nbRecords)
          {
            int indNameAttrFound = -1;
            int k = 0;
            bool bUseNameAttrFound = false;
            for (QStringList::iterator it = aFieldList.begin(); it != aFieldList.end(); it++, k++)
              if (QString::compare(*it, "name", Qt::CaseInsensitive) == 0)
              {
                indNameAttrFound = k;
                break;
              }

              if (indNameAttrFound != -1)
                bUseNameAttrFound = SUIT_MessageBox::question( module->getApp()->desktop(),
                               tr( "IMPORT_POLYLINE" ),
                               tr( "IMPORT_POLYLINE_USE_NAME_ATTR" ),
                               QMessageBox::Yes | QMessageBox::No, 
                               SUIT_MessageBox::Yes) == SUIT_MessageBox::Yes;
  
            std::vector<std::vector<HYDROData_ShapeFile::DBF_AttrValue>> anAttrVV;
            for (int i = 0; i < aFieldList.size(); i++)
            {
              std::vector<HYDROData_ShapeFile::DBF_AttrValue> anAttrV;
              anAttrVV.push_back(anAttrV);
              anImporter.DBF_GetAttributeList(i, anAttrVV[i] );
            }

            int indNULL = 1;
                for (int i = 1; i <= theEntitiesToSHPObj.Extent(); i++)
            {
                  Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast( theEntitiesToSHPObj.FindKey(i) );
              QStringList aDBFinfo;
              aDBFinfo << aFieldList; //first, the table header
              for (int j = 0; j < aFieldList.size(); j++)
              {
                QString attr;
                const HYDROData_ShapeFile::DBF_AttrValue& attrV = anAttrVV[j][i-1];
                if (attrV.myFieldType == HYDROData_ShapeFile::DBF_FieldType_String)
                  aDBFinfo << attrV.myStrVal;
                else if (attrV.myFieldType == HYDROData_ShapeFile::DBF_FieldType_Integer)
                  aDBFinfo << QString::number(attrV.myIntVal);
                else if (attrV.myFieldType == HYDROData_ShapeFile::DBF_FieldType_Double)
                  aDBFinfo << QString::number(attrV.myDoubleVal);
                else 
                  aDBFinfo << "";
              }
              assert (aDBFinfo.size() / 2 == aFieldList.size());
              aPolylineXY->SetDBFInfo(aDBFinfo);
              if (bUseNameAttrFound)
              {
                QString nameOfPoly = aDBFinfo[aDBFinfo.size() / 2 + indNameAttrFound];
                if (!nameOfPoly.isEmpty())
                  aPolylineXY->SetName(nameOfPoly);
                else
                {
                  aPolylineXY->SetName("null_name_" + QString::number(indNULL));
                  indNULL++;
                }
              }
            }
          }
        }        
      }

        if (anImporter.GetShapeType() == 13) //Polyline 3D
        {
          HYDROGUI_ImportPolylineDlg* aDLG = new HYDROGUI_ImportPolylineDlg( module->getApp()->desktop(), PolyEnt );
          aDLG->setModal( true );
          aDLG->setWindowTitle(tr("IMPORT_POLYLINE"));
          //QApplication::restoreOverrideCursor();
          if( aDLG->exec()==QDialog::Accepted )
          {
            NCollection_Sequence<Handle(HYDROData_Entity)> CheckedPolylines = aDLG->GetCheckedPolylines();
            NCollection_IndexedDataMap<Handle(HYDROData_Entity), SHPObject*> CheckedEntitiesToSHPObj;
            for (int k=1;k<=CheckedPolylines.Size();k++)
            {
              SHPObject* const* SHPObkP = theEntitiesToSHPObj.Seek(CheckedPolylines(k));
              if (SHPObkP != NULL)
                CheckedEntitiesToSHPObj.Add(CheckedPolylines(k), *SHPObkP);
            }
            NCollection_Sequence<Handle(HYDROData_Entity)> theEntitiesPoly3D;
            anImporter.ImportPolylines3D(theDocument, CheckedEntitiesToSHPObj, theEntitiesPoly3D, theShapeTypesToImport);          
            PolyEnt.Append(theEntitiesPoly3D);
          }
        }
        anImporter.Free();
      }
      if (aStat == 1)
        UpdateView(module, PolyEnt);
      else if (aStat == 2)
      {
        UpdateView(module, PolyEnt);
        if (theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType_All) //if other flag = > no need to show this messagebox
          SUIT_MessageBox::information(module->getApp()->desktop(), 
            tr( "IMPORT_POLYLINE" ), tr ("POLYGON_IMPORTED_AS_POLYLINE"));
      }
      else
      {
        QString aMess;
        if (theShapeTypesToImport == HYDROData_ShapeFile::ImportShapeType::ImportShapeType_Polygon)
          aMess += tr ("POLYLINE_IMPORT_FAILED_AS_POLYGON") + ";\n";
        else
          aMess += tr ("POLYLINE_IMPORT_FAILED_AS_POLYLINE") + ";\n";

        if (aStat == -1)
          aMess += tr ("CANT_OPEN_SHP");
        else if (aStat == -2)
          aMess += tr ("CANT_OPEN_SHX");
        else 
          aMess += tr ("SHAPE_TYPE_IS") + anImporter.GetShapeTypeName(aShapeTypeOfFile);
        SUIT_MessageBox::warning( module->getApp()->desktop(), tr( "IMPORT_POLYLINE" ), aMess);
      }
      importedEntities.Append(PolyEnt);
    }
    else if ( anExt == "xy" || (importXY && anExt == "xyz"))
    {
      if (!HYDROData_Tool::importPolylineFromXYZ(aFileName, theDocument, true, importedEntities))
        SUIT_MessageBox::warning( module->getApp()->desktop(), tr( "IMPORT_POLYLINE" ), tr( "NO_ONE_POLYLINE_IMPORTED" ));
    }
    else if (anExt == "xyz")
    {
      if (!HYDROData_Tool::importPolylineFromXYZ(aFileName, theDocument, false, importedEntities))
        SUIT_MessageBox::warning( module->getApp()->desktop(), tr( "IMPORT_POLYLINE" ), tr( "NO_ONE_POLYLINE_IMPORTED" ));
    }
  }
  return importedEntities;
}

void HYDROGUI_ImportPolylineOp::onApply()
{
  if ( !myFileDlg )
  {
    abort();
    return;
  }

  QStringList aFileNames = myFileDlg->selectedFiles();
  
  //QApplication::setOverrideCursor( Qt::WaitCursor );  
  startDocOperation();

  ImportPolyOp(aFileNames, doc(), module(), HYDROData_ShapeFile::ImportShapeType_All);
 
  if (!aFileNames.empty())
  {
    commitDocOperation();
    commit();
    module()->update( UF_Model | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init | UF_OCCViewer );
  }
  else
    abort();
  
  QApplication::restoreOverrideCursor();
}

void HYDROGUI_ImportPolylineOp::UpdateView( HYDROGUI_Module* module, NCollection_Sequence<Handle(HYDROData_Entity)>& anEntities)
{
  size_t anActiveViewId = HYDROGUI_Tool::GetActiveGraphicsViewId( module );
  if ( anActiveViewId == 0 )
    anActiveViewId = HYDROGUI_Tool::GetActiveOCCViewId( module );

  for (int i = 1; i <= anEntities.Size() ; i++)
  {
    anEntities(i)->Update();
    module->setObjectVisible( anActiveViewId, anEntities(i), true );
    module->setIsToUpdate( anEntities(i) );
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_CurveCreator_Profile_HeaderFile
#define HYDROGUI_CurveCreator_Profile_HeaderFile

#include "CurveCreator_Curve.hxx"
#include <Quantity_Color.hxx>

/**
 *  The CurveCreator_Curve object is represented as one or more sets of
 *  connected points; thus CurveCreator_Curve object can contain several
 *  not connected curves (polylines or b-splines), each such curve has two
 *  only ends start and end points in other words non-manifold curves
 *  are not supported.
 */
class HYDROGUI_CurveCreatorProfile : public CurveCreator_Curve
{

public:

  //! Constructor of the curve.
  HYDROGUI_CurveCreatorProfile();

  //! Destructor.
  virtual ~HYDROGUI_CurveCreatorProfile();

public:

  /***********************************************/
  /***           Section methods               ***/
  /***********************************************/

  //! For internal use only! Undo/Redo are not used here.
  virtual bool clearInternal();

  //! For internal use only! Undo/Redo are not used here.
  virtual bool joinInternal( const std::list<int>& theSections );

  //! For internal use only! Undo/Redo are not used here.
  virtual bool moveSectionInternal( const int theISection,
                                    const int theNewIndex);
  //! Move section to new position in list
  virtual bool moveSection( const int theISection,
                            const int theNewIndex );
  //! Join list of sections to one section (join all if the list is empty)
  // The first section in the list is a leader, another sections are joined to it
  virtual bool join( const std::list<int>& theSections );

  //! For internal use only! Undo/Redo are not used here.
  virtual int addSectionInternal( const std::string &theName, 
                                  const CurveCreator::SectionType theType,
                                  const bool theIsClosed,
                                  const CurveCreator::Coordinates &thePoints);
  //! Add a new section.
  virtual int addSection( const std::string &theName, 
                           const CurveCreator::SectionType theType,
                           const bool theIsClosed );
  //! Add a new section.
  virtual int addSection( const std::string &theName, 
                           const CurveCreator::SectionType theType,
                           const bool theIsClosed,
                           const CurveCreator::Coordinates &thePoints);
  
  
  //! For internal use only! Undo/Redo are not used here.
  virtual bool removeSectionInternal( const int theISection );

  //! Removes the given sections.
  virtual bool removeSection( const int theISection );

  
  //! For internal use only! Undo/Redo are not used here.
  virtual bool setClosedInternal( const int theISection, 
                                  const bool theIsClosed );
  /**
   *  Set "closed" flag of the specified section (all sections if
   *  \a theISection is -1).
   */
  virtual bool setClosed( const int theISection, 
                          const bool theIsClosed );

  /***********************************************/
  /***           Point methods                 ***/
  /***********************************************/

  //! For internal use only! Undo/Redo are not used here.
  virtual bool addPointsInternal( const CurveCreator::SectionsMap &theSectionsMap );
  /**
   *  Add one point to the specified section starting from the given theIPnt index
   *  (or at the end of points if \a theIPnt is -1).
   */
  //! For internal use only! Undo/Redo are not used here.
  virtual bool setPointInternal( const CurveCreator::SectionsMap &theSectionsMap );

  /**
   *  Add one point to the specified section starting from the given theIPnt index.
   *  Contrary to CurveCreator_Curve::addPoints(...) if \a theIPnt is -1 the right index
   *  will be calculated automatically in accordance with X coordinate value of the point.
   */
  virtual bool addPoints( const CurveCreator::Coordinates &theCoords,
                          const int theISection,
                          const int theIPnt = -1 );
  /**
   * Indicates whether the points can be sorted.
   */
  virtual bool canPointsBeSorted();

  virtual Handle(TColgp_HArray1OfPnt) GetDifferentPoints( int theISection = -1 ) const;

protected:
  /**
   * Converts the list of custom point position objects into a list of point indices
   * \param thePoints an source list
   * \param theConvPoints a converted list
   */
  void convert( const CurveCreator::PosPointsList& thePoints,
                std::list<int>& theConvPoints );

  //virtual void constructAISObject();

  Quantity_Color myCurveColor;

};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Module.h>
#include <HYDROGUI_ProfileOp.h>
#include <HYDROGUI_ProfileDlg.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_UpdateFlags.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROData_Document.h>
#include <HYDROData_Profile.h>
#include <HYDROGUI_CurveCreatorProfile.h>
#include <HYDROGUI_DeleteOp.h>
#include <HYDROData_Tool.h>
#include <CurveCreator_Displayer.hxx>
#include <HYDROData_Entity.h>
#include <QSet>
#include <set>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_UpdateFlags.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Desktop.h>
#include <OCCViewer_AISSelector.h>

#include <Precision.hxx>

//static int ZValueIncrement = 0;

static void ProfileUZToCurveCrProfile(const Handle(HYDROData_ProfileUZ)& aProfileUZ, 
  HYDROGUI_CurveCreatorProfile* outProfile)
{
  CurveCreator::Coordinates aCurveCoords;
  CurveCreator::SectionsMap aSectionsMap;

  HYDROData_ProfileUZ::PointsList aSectPointsList = aProfileUZ->GetPoints();
  CurveCreator::PosPointsList aPoints;
  for ( int k = 1, aNbPoints = aSectPointsList.Size(); k <= aNbPoints; ++k )
  {
    const HYDROData_ProfileUZ::Point& aSectPoint = aSectPointsList.Value( k );
    aCurveCoords.clear();
    aCurveCoords.push_back( aSectPoint.X() );
    aCurveCoords.push_back( aSectPoint.Y() );

    CurveCreator_PosPoint* aPosPoint = new CurveCreator_PosPoint( k, aCurveCoords );
    aPoints.push_back( aPosPoint );
  }

  aSectionsMap[0] = aPoints;
  outProfile->addPointsInternal( aSectionsMap );

  HYDROData_ProfileUZ::SectionType aSectType = aProfileUZ->GetSectionType( 0 );

  CurveCreator::SectionType aCurveType = CurveCreator::Polyline;
  if( aSectType == HYDROData_ProfileUZ::SECTION_SPLINE )
    aCurveType = CurveCreator::Spline;

  outProfile->setSectionType( 0, aCurveType );
}

static int CurveCrProfileToHProfile(const HYDROGUI_CurveCreatorProfile* outProfile, 
  Handle(HYDROData_Profile) theProfileObj,
  const QString theProfileName,
  bool IsEdit)
{
  if( theProfileObj.IsNull() )
    return 0;

  Handle(HYDROData_ProfileUZ) aProfileUZ = theProfileObj->GetProfileUZ();
  if ( aProfileUZ.IsNull() )
    return 0;

  theProfileObj->SetName(theProfileName);

  HYDROData_ProfileUZ::PointsList aProfileParamPoints;

  Handle(TColgp_HArray1OfPnt) aCurveCoords = outProfile->GetDifferentPoints( 0 );
  if ( aCurveCoords.IsNull() || aCurveCoords->Size() <= 2 )
    return -1;

  for ( int k = aCurveCoords->Lower(); k <= aCurveCoords->Upper() ; k++ )
  {
    HYDROData_ProfileUZ::Point aProfileParamPoint;

    aProfileParamPoint.SetX( aCurveCoords->Value( k ).X() );
    aProfileParamPoint.SetY( aCurveCoords->Value( k ).Y() );

    aProfileParamPoints.Append( aProfileParamPoint );
  }
  theProfileObj->SetParametricPoints( aProfileParamPoints );

  HYDROData_ProfileUZ::SectionType aSectType = HYDROData_ProfileUZ::SECTION_POLYLINE;
  if ( outProfile->getSectionType( 0 ) == CurveCreator::Spline )
    aSectType = HYDROData_ProfileUZ::SECTION_SPLINE;

  aProfileUZ->SetSectionType( 0, aSectType );

  if ( !IsEdit )
    theProfileObj->SetBorderColor( theProfileObj->DefaultBorderColor() );

  // At first we update the child u,z profile object
  aProfileUZ->Changed( HYDROData_Entity::Geom_2d );
  aProfileUZ->Update();

  // And now we update our edited object
  theProfileObj->Update();
  return 1;
}

HYDROGUI_ProfileOp::HYDROGUI_ProfileOp( HYDROGUI_Module* theModule, bool theIsEdit )
: HYDROGUI_Operation( theModule ), myIsEdit(theIsEdit), 
   myDisplayer (NULL)
{
  setName( theIsEdit ? tr( "EDIT_PROFILE" ) : tr( "CREATE_PROFILE" ) );
}

HYDROGUI_ProfileOp::~HYDROGUI_ProfileOp()
{
  erasePreview();
}

/**
 * Redirect the delete action to input panel
 */
void HYDROGUI_ProfileOp::deleteSelected()
{
  HYDROGUI_ProfileDlg* aPanel = (HYDROGUI_ProfileDlg*)inputPanel();
  aPanel->deleteSelected();
}

/**
 * Checks whether there are some to delete
 */
#include <CurveCreator_Widget.h>
bool HYDROGUI_ProfileOp::deleteEnabled()
{
  HYDROGUI_ProfileDlg* aPanel = (HYDROGUI_ProfileDlg*)inputPanel();
  return aPanel->deleteEnabled();
}

void HYDROGUI_ProfileOp::startOperation()
{
  if (!myProfiles.empty())
  {
    for (int i = 0; i < myProfiles.size(); i++)
    {
      delete myProfiles[i];
      myProfiles[i] = NULL;
    }
    myProfiles.clear();
  }

  if (!myIsEdit)
    myEditedObjects.Clear();

  if (myCurveToProfile.IsEmpty())
    myCurveToProfile.Clear();

  if( myIsEdit && isApplyAndClose() )
    myEditedObjects = HYDROGUI_Tool::GetSelectedObjects(module());

  int lenP = myEditedObjects.Length();  
  myProfiles.resize(lenP == 0 ? 1 : myEditedObjects.Length());
  for (int i = 0; i < myProfiles.size(); i++)
    myProfiles[i] = new HYDROGUI_CurveCreatorProfile();

  //mySingleProfileMode = myEditedObjects.IsEmpty();  
  HYDROGUI_Operation::startOperation();

  HYDROGUI_ProfileDlg* aPanel = (HYDROGUI_ProfileDlg*)inputPanel();
  aPanel->myEditorWidget->setCurve(NULL);
  aPanel->reset();
  setPreviewManager( aPanel->viewManager() );
  setCursor();

  //aPanel->SetSingleProfileMode(mySingleProfileMode); 
  QMap<HYDROGUI_CurveCreatorProfile*, QColor> CurveToColor;
  if( lenP )
  {
    for (int i = 1; i <= lenP; i++)
    {
      Handle(HYDROData_Profile) aCProfile = Handle(HYDROData_Profile)::DownCast(myEditedObjects(i)); 
      QString aProfileName;
      if( !aCProfile.IsNull() )
      {
        Handle(HYDROData_ProfileUZ) aProfileUZ = aCProfile->GetProfileUZ( false );
        if ( !aProfileUZ.IsNull() )
        {
          HYDROGUI_CurveCreatorProfile* CP = new HYDROGUI_CurveCreatorProfile();
          myProfiles[i-1] = CP;
          ProfileUZToCurveCrProfile(aProfileUZ, CP);
          myCurveToProfile.Bind(CP, aCProfile);
        }
      }
    }
    int ext = myCurveToProfile.Extent(); //ext should be equal to lenP
    QVector<QColor> PColors;
    if (myIsEdit)
      HYDROData_Tool::GenerateRepeatableRandColors(ext, PColors);
    else
      PColors << QColor(0,0,255); //default color

    for (int i = 0; i < myProfiles.size(); i++)
    {
      HYDROGUI_CurveCreatorProfile* CC = myProfiles[i];
      const Handle(HYDROData_Profile)& CP = myCurveToProfile.Find(CC);
      QColor CurCol = PColors[i]; 
      CP->GetProfileColor(CurCol);
      CurveToColor[CC] = CurCol;
      const QString& profName = CP->GetName();
      const QColor& PColor = CurCol;
      if (myIsEdit)
        aPanel->addProfileName(profName, PColor);
      else
        aPanel->setProfileName(profName);
    }
  }

  if (!myIsEdit)
  {
    QString aProfileName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_PROFILE_NAME" ) );
    aPanel->setProfileName( aProfileName );
  }

  if (!myProfiles.empty())
  {
    aPanel->setProfile( myProfiles[0] );
    aPanel->setProfilesPointer( &myProfiles );
  }
  displayPreviews(CurveToColor, 0, myProfiles.size(), true, true );
}

void HYDROGUI_ProfileOp::onAddProfiles()
{
  if( !myIsEdit )
    return;

  QSet<QString> edObjNamesMap;
  for (int i = 1; i <= myEditedObjects.Length(); i++)
    edObjNamesMap.insert( myEditedObjects(i)->GetName());

  HYDROData_SequenceOfObjects aSelectedObj = HYDROGUI_Tool::GetSelectedObjects( module() );;
  int ExistingProfLen = myEditedObjects.Length();
  for (int i = 1; i <= aSelectedObj.Length(); i++)
  {
    Handle(HYDROData_Profile) CurProf = Handle(HYDROData_Profile)::DownCast(aSelectedObj.Value(i));
    if (CurProf.IsNull())
      continue;
    if (!edObjNamesMap.contains(CurProf->GetName()))
      myEditedObjects.Append(CurProf);
  }

  int NewLen = myEditedObjects.Length();
  bool IsNewPoly = NewLen - ExistingProfLen;
  if (aSelectedObj.IsEmpty())
  {
    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "PROFILEOP_WARNING" ), tr ("PROFILES_ARE_NOT_SELECTED") );
    return;
  }
  if (!IsNewPoly)
  {
    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "PROFILEOP_WARNING" ), tr ("PROFILES_ALREADY_PRESENT") );
    return;
  }
  myProfiles.resize(myEditedObjects.Length());
  for (int i = myProfiles.size() - 1; i < myEditedObjects.Length(); i++)
    myProfiles[i] = new HYDROGUI_CurveCreatorProfile();
    
  HYDROGUI_ProfileDlg* aPanel = (HYDROGUI_ProfileDlg*)inputPanel();  
  QMap<HYDROGUI_CurveCreatorProfile*, QColor> CurveToColor;
  if( IsNewPoly )
  {
    //TODO move to ext func!
    for (int i = ExistingProfLen + 1; i <= NewLen; i++)
    {
      Handle(HYDROData_Profile) aCProfile = Handle(HYDROData_Profile)::DownCast(myEditedObjects(i)); 

      QString aProfileName;
      if( !aCProfile.IsNull() )
      {
        Handle(HYDROData_ProfileUZ) aProfileUZ = aCProfile->GetProfileUZ( false );
        if ( !aProfileUZ.IsNull() )
        {
          HYDROGUI_CurveCreatorProfile* CP = new HYDROGUI_CurveCreatorProfile();
          myProfiles[i-1] = CP;
          ProfileUZToCurveCrProfile(aProfileUZ, CP);
          myCurveToProfile.Bind(CP, aCProfile);            
        }
      }
    }
    //int ext = myCurveToProfile.Extent(); //ext should be equal to lenP
    QVector<QColor> PColors;
    HYDROData_Tool::GenerateRepeatableRandColors(NewLen - ExistingProfLen, PColors);

    for (int i = ExistingProfLen; i < NewLen; i++)
    {
      HYDROGUI_CurveCreatorProfile* CC = myProfiles[i];
      const Handle(HYDROData_Profile)& CP = myCurveToProfile.Find(CC);
      QColor CurCol = PColors[i-ExistingProfLen]; 
      CP->GetProfileColor(CurCol);
      CurveToColor[CC] = CurCol;
      const QString& profName = CP->GetName();
      const QColor& PColor = CurCol;
      if (myIsEdit)
        aPanel->addProfileName(profName, PColor);
      else
        aPanel->setProfileName(profName);
    }
  }
  displayPreviews(CurveToColor, ExistingProfLen, NewLen, false, false);
}

void HYDROGUI_ProfileOp::abortOperation()
{
  erasePreview();
  restoreCursor();

  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ProfileOp::commitOperation()
{
  erasePreview();
  restoreCursor();

  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_ProfileOp::createInputPanel() const
{
  HYDROGUI_ProfileDlg* aDlg = new HYDROGUI_ProfileDlg( module(), getName(), myIsEdit );
  connect( aDlg, SIGNAL( AddProfiles() ), this, 
    SLOT( onAddProfiles() ) );
  connect( aDlg, SIGNAL( RemoveProfile(int) ), this, 
    SLOT( onRemoveProfile(int) ) );  
  return aDlg;
}


bool HYDROGUI_ProfileOp::processApply( int& theUpdateFlags,
                                       QString& theErrorMsg,
                                       QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_ProfileDlg* aPanel = ::qobject_cast<HYDROGUI_ProfileDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QStringList aProfileNames = aPanel->getProfileNames();
  QVector<QString> aProfileNamesFiltered;
  int i = 0;
 // QSet<QString> edObjStrMap = aProfileNames.toSet();
  QSet<QString> ObjStrMapNE ;
  HYDROData_SequenceOfObjects allobj = doc()->CollectAllObjects();
  for (int i=1;i<=allobj.Size();i++ )
    ObjStrMapNE.insert(allobj(i)->GetName());
  for (int i=1; i<=myEditedObjects.Size();i++)
    ObjStrMapNE.remove(myEditedObjects(i)->GetName());
  bool warn = false;
  QString title = tr( "PROFILEOP_WARNING" );
  QString mes = tr("PROFILE_RENAMING_NOTIF") + "\n";
  foreach (QString profName, aProfileNames)
  {
    i++;
    if( !myIsEdit || ObjStrMapNE.contains(profName) || profName.isEmpty() )
    {
      QString newName = HYDROData_Tool::GenerateObjectName(doc(), "Profile");
      mes += profName + " => " + newName + "\n";
      profName = newName;
      warn = true;
    }
    aProfileNamesFiltered.append(profName);
  }
  if (myIsEdit && warn)
#ifndef TEST_MODE
    SUIT_MessageBox::warning( module()->getApp()->desktop(), title, mes );
#endif
  //  
  theUpdateFlags = UF_Model;
  if (myIsEdit)
  {
    for (int i = 1; i <= myEditedObjects.Size(); i++)
    {
      Handle(HYDROData_Profile) HProf = Handle(HYDROData_Profile)::DownCast(myEditedObjects(i));
      int stat = CurveCrProfileToHProfile(myProfiles[i-1], HProf, aProfileNamesFiltered[i-1], true);
      HProf->SetProfileColor(HYDROData_Tool::toQtColor(myProfiles[i-1]->getColorSection(0)));
      if (stat == 0)
        continue;
      else if (stat == -1)
      {    
        theErrorMsg = tr( "NUMBER_OF_PROFILE_POINTS_INCORRECT" ); //TODO resolve this
        continue;
      }    
      module()->setIsToUpdate( HProf );
    }
    theUpdateFlags |= UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
  }
  else
  {
    Handle(HYDROData_Profile) aNewProfileObj = Handle(HYDROData_Profile)::DownCast( doc()->CreateObject( KIND_PROFILE ) ); 
    int stat = CurveCrProfileToHProfile(myProfiles[0], aNewProfileObj, aProfileNamesFiltered[0], false);
    aNewProfileObj->SetProfileColor(HYDROData_Tool::toQtColor(myProfiles[0]->getColorSection(0)));
    if (stat == 0)
      return false;
    else if (stat == -1)
    {    
      theErrorMsg = tr( "NUMBER_OF_PROFILE_POINTS_INCORRECT" );
      return false;
    }    
    module()->setIsToUpdate( aNewProfileObj );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aNewProfileObj );
    theBrowseObjectsEntries.append( anEntry );
  }

  return true;
}

void HYDROGUI_ProfileOp::displayPreviews(const QMap<HYDROGUI_CurveCreatorProfile*, QColor>& CurveToColor,
  int firstIndProf, int lastIndProf, bool createNewDisplayer, bool SwitchToFirstProf)
{
  HYDROGUI_ProfileDlg* aPanel = dynamic_cast<HYDROGUI_ProfileDlg*>( inputPanel() );
  if( aPanel )
  {
    Handle(AIS_InteractiveContext) aCtx = aPanel->getAISContext();
    if( !aCtx.IsNull() )
    {
      if (myDisplayer)
      {
        //delete myDisplayer;
        //myDisplayer = NULL;
      }
      if (createNewDisplayer)
        myDisplayer = new CurveCreator_Displayer( aCtx );
      for (int i = firstIndProf; i < lastIndProf; i++ )
      {
        HYDROGUI_CurveCreatorProfile* CC = myProfiles[i];
        QColor QCurCol = QColor(0,0,255); //def color
        if (myIsEdit)
          QCurCol = CurveToColor[CC];
        Quantity_Color CurOCCCol = HYDROData_Tool::toOccColor(QCurCol);
        CC->setDisplayer( myDisplayer );
        CC->myPointAspectColor = CurOCCCol;
        CC->setColorSectionInternal(0, CurOCCCol);
        CC->myLineWidth = 1;
        myDisplayer->display( CC->getAISObject( true ), true );
      }
    }
    if (SwitchToFirstProf && myProfiles.size() > 1)
      aPanel->switchToFirstProfile();
  }
}

void HYDROGUI_ProfileOp::erasePreview()
{
  HYDROGUI_ProfileDlg* aPanel = dynamic_cast<HYDROGUI_ProfileDlg*>( inputPanel() );
  //CurveCreator_Displayer* aDisplayer = myProfiles[0] ? myProfile[0]->getDisplayer() : 0;
  if( aPanel && myDisplayer )
  {
    Handle(AIS_InteractiveContext) aCtx = aPanel->getAISContext();
    if( !aCtx.IsNull() )
    {
      myDisplayer->eraseAll( true );
    }
  }
  myCurveToProfile.Clear();
}

void HYDROGUI_ProfileOp::onRemoveProfile(int index)
{
  if (index >= myProfiles.size() )
    return;

  if (!myIsEdit)
    return;

  HYDROGUI_CurveCreatorProfile* CP = myProfiles[index];
  myProfiles.erase (myProfiles.begin()+index);
  myEditedObjects.Remove(index+1);
  myCurveToProfile.UnBind(CP);

  HYDROGUI_ProfileDlg* aPanel = (HYDROGUI_ProfileDlg*)inputPanel();
  //aPanel->reset();
  //setPreviewManager( aPanel->viewManager() );
  //setCursor();

  aPanel->BlockProfileNameSignals(true);
  aPanel->eraseProfile(index);

  Handle(AIS_InteractiveContext) aCtx = aPanel->getAISContext();
  if( !aCtx.IsNull() && myDisplayer)
    myDisplayer->erase( CP->getAISObject(false), true);
  CP->SetEraseAllState(false);
  delete CP;
  int selectedInd = aPanel->GetProfileSelectionIndex();
  if (selectedInd > -1)
    aPanel->SwitchToProfile(selectedInd);
  aPanel->BlockProfileNameSignals(false);
}

bool HYDROGUI_ProfileOp::isValid( SUIT_Operation* theOtherOp ) const
{
  HYDROGUI_DeleteOp* aDelOp = dynamic_cast<HYDROGUI_DeleteOp*>( theOtherOp );
  if( aDelOp )
    return true;
  else
    return false;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_VTKPrsDisplayer.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_VTKPrs.h"
#include "HYDROGUI_VTKPrsBathymetryDriver.h"
#include "HYDROGUI_VTKPrsShapeDriver.h"
#include "HYDROGUI_Tool.h"

#include "HYDROData_Tool.h"
#include <SVTK_ViewModel.h>
#include <SVTK_ViewWindow.h>
#include <SALOME_ListIO.hxx>
//#include <SALOME_ListIteratorOfListIO.hxx>
#include <SALOME_InteractiveObject.hxx>
#include <SUIT_ViewManager.h>
#include <SUIT_Accel.h>

#include <vtkLookupTable.h>
#include <vtkRenderer.h>
#include <vtkTextProperty.h>
#include <vtkWindow.h>
#include <vtkActor2DCollection.h>

#include <QVector>

#define NB_COLORS 32

// Saturation of blue
//#define HUE_START 0.69 
//#define HUE_END   0.41
//#define SATURATION_START 1.0 
//#define SATURATION_END   0.4

#define HUE_START 0.7
#define HUE_END   0.0 
#define SATURATION_START 1.0 
#define SATURATION_END   1.0

HYDROGUI_VTKPrsDisplayer::HYDROGUI_VTKPrsDisplayer( HYDROGUI_Module* theModule )
: HYDROGUI_AbstractDisplayer( theModule ), myDriver( NULL ), myShapeDriver( NULL )
{
}

HYDROGUI_VTKPrsDisplayer::~HYDROGUI_VTKPrsDisplayer()
{
}

void HYDROGUI_VTKPrsDisplayer::SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                      const size_t theViewerId )
{
  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( !aViewer )
  {
    HYDROGUI_VTKPrs* anObjShape;
    for ( int i = 1, n = theObjs.Length(); i <= n; i++ )
    {
      Handle(HYDROData_Entity) anObj = theObjs.Value( i );
      if( !anObj.IsNull() )
      {
        anObjShape = module()->getObjectVTKPrs( (size_t)aViewer, anObj );
        if ( anObjShape )
        {
          anObjShape->setIsToUpdate( true );
        }
      }
    }
  }
}

void HYDROGUI_VTKPrsDisplayer::DisplayAll( const size_t theViewerId,
                                           const bool theIsForced,
                                           const bool theDoFitAll )
{
  HYDROGUI_AbstractDisplayer::DisplayAll( theViewerId, theIsForced, theDoFitAll );

  bool isEraseScalarBar = true;

  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( aViewer )
  {
    SALOME_ListIO aListIO;
    aViewer->GetVisible( aListIO );
    
    HYDROGUI_VTKPrs* aPrs;
    SALOME_ListIteratorOfListIO anIter( aListIO );
    for( ; anIter.More(); anIter.Next() )
    {
      Handle(SALOME_InteractiveObject) aPrsObj = anIter.Value();
      if ( !aPrsObj.IsNull() )
      {
        Handle(HYDROData_Entity) anObj = 
          module()->getDataModel()->objectByEntry( aPrsObj->getEntry() );
        aPrs = module()->getObjectVTKPrs( (size_t)aViewer, anObj );
        if( aPrs->needScalarBar() )
        {
          isEraseScalarBar = false;
          break;
        }
      }
    }
  }

  if( isEraseScalarBar )
    EraseScalarBar( theViewerId );
}

void HYDROGUI_VTKPrsDisplayer::EraseAll( const size_t theViewerId )
{
  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( aViewer )
  {
    aViewer->EraseAll( 0, true );
    module()->removeViewVTKPrs( (size_t)aViewer );
    EraseScalarBar( theViewerId );
  }
}

void HYDROGUI_VTKPrsDisplayer::EraseScalarBar( const size_t theViewerId, const bool theIsDelete )
{
  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( aViewer )
  {
    if ( myScalarBars.contains( (size_t)aViewer ) )
    {
      SUIT_ViewManager* aViewMgr = dynamic_cast<SUIT_ViewManager*>( aViewer->getViewManager() );
      if ( aViewMgr && aViewMgr->getViewsCount() > 0 )
      {
        SVTK_ViewWindow* aView = dynamic_cast<SVTK_ViewWindow*>( aViewMgr->getActiveView() );
        if ( aView )
        {
          vtkScalarBarActor* aScalarBar = myScalarBars[ (size_t)aViewer ];
          if ( aView->getRenderer()->HasViewProp( aScalarBar ) )
          {
            aView->getRenderer()->RemoveActor2D( aScalarBar );
          }
        }
      }
      if ( theIsDelete )
      {
        myScalarBars.remove( (size_t)aViewer );
      }
    }
  }
}

void HYDROGUI_VTKPrsDisplayer::Erase( const HYDROData_SequenceOfObjects& theObjs,
                                const size_t theViewerId )
{
  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( aViewer )
  {
    HYDROGUI_VTKPrs* aPrs;
    for ( int i = 1, n = theObjs.Length(); i <= n; i++ )
    {
      Handle(HYDROData_Entity) anObj = theObjs.Value( i );
      if( anObj.IsNull() )
        continue;

      aPrs = module()->getObjectVTKPrs( (size_t)aViewer, anObj );
      if ( aPrs )
      {
        aViewer->Erase( aPrs, true );
      }
      if ( anObj->IsRemoved() )
      {
        module()->removeObjectVTKPrs( (size_t)aViewer, anObj );
      }
    }
  }
}

void HYDROGUI_VTKPrsDisplayer::Display( const HYDROData_SequenceOfObjects& theObjs,
                                        const size_t theViewerId,
                                        const bool theIsForced,
                                        const bool theDoFitAll)
{
  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( aViewer )
  {
    // Hide colors legend bar
    SVTK_ViewWindow* aView = dynamic_cast<SVTK_ViewWindow*>(
      aViewer->getViewManager()->getActiveView() );
    vtkScalarBarActor* aScalarBar = 0;
    if ( aView )
    {
      if ( !myScalarBars.contains( (size_t)aViewer ) )
      {
        createScalarBar( (size_t)aViewer );
      }
      aScalarBar = myScalarBars[ (size_t)aViewer ];

      if ( aView->getRenderer()->HasViewProp( aScalarBar ) )
      {
        aView->getRenderer()->RemoveActor2D( aScalarBar );
      }
    }

    // Invalidate global Z range
    double anInvalidRange[2] = { HYDROGUI_VTKPrs::InvalidZValue(), HYDROGUI_VTKPrs::InvalidZValue() };
    SetZRange( (size_t)aViewer, anInvalidRange );

    int anInvalidZ = HYDROGUI_VTKPrs::InvalidZValue();
    bool isChanged = false;
    bool isScalarBarNeeded = false;
    HYDROGUI_VTKPrs* aPrs;
    for ( int i = 1, n = theObjs.Length(); i <= n; i++ )
    {
      Handle(HYDROData_Entity) anObj = theObjs.Value( i );
      if( !anObj.IsNull() )
      {
        bool anIsVisible = module()->isObjectVisible( (size_t)aViewer, anObj );
        aPrs = module()->getObjectVTKPrs( (size_t)aViewer, anObj );

        bool anIsInserted = ( aPrs != 0 );
        if( anIsVisible && ( !aPrs || aPrs->getIsToUpdate() || theIsForced ) )
        {
          // Erase the presentation in the view because of the problem with GEOM_Actor upadting on second SetShape.
          if ( aPrs )
          {
            aViewer->Erase( aPrs, true );
          }
          // Update the presentation
          if( HYDROGUI_VTKPrsDriver* aDriver = getDriver( (size_t)aViewer, anObj ) )
          {
            if( aDriver->Update( anObj, aPrs ) && aPrs && !anIsInserted )
            {
              module()->setObjectVTKPrs( (size_t)aViewer, anObj, aPrs );
            }
          }
        }

        if( aPrs )
        {
          if ( anIsVisible )
          {
            if ( aPrs->needScalarBar() )
            {
              // Extend the global Z range if necessary
              double* aGlobalRange = GetZRange( (size_t)aViewer );
              double* aRange = aPrs->getInternalZRange();
              bool anIsUpdate = false;
              if ( aRange[0] < aGlobalRange[0] || ValuesEquals( aGlobalRange[0], anInvalidZ ) )
              {
                aGlobalRange[0] = aRange[0];
                anIsUpdate = true;
              }
              if ( aRange[1] > aGlobalRange[1] || ValuesEquals( aGlobalRange[1], anInvalidZ ) )
              {
                aGlobalRange[1] = aRange[1];
                anIsUpdate = true;
              }

              if ( anIsUpdate )
              {
                module()->updateVTKZRange( (size_t)aViewer, aGlobalRange );
              }
            }

            aViewer->Display( aPrs );
            isScalarBarNeeded = isScalarBarNeeded || aPrs->needScalarBar();

          }
          else
          {
            aViewer->Erase( aPrs );
          }
          isChanged = true;
        }
      }
    }

    if ( aView ) 
    {
      if ( isChanged && isScalarBarNeeded && aScalarBar )
      {
        // Show colors legend bar
          aView->getRenderer()->AddActor2D( aScalarBar );
      }

      // Refresh the view
      if ( theDoFitAll )
      {
        // Repaint is done inside OnFitAll()
        aView->onAccelAction( SUIT_Accel::ZoomFit );
      } 
      else if ( isChanged )
      {
        aView->Repaint( true );
      }
    }
  }
}

void HYDROGUI_VTKPrsDisplayer::purgeObjects( const size_t theViewerId )
{
  bool doEraseScalarBar = false;

  SVTK_Viewer* aViewer = module()->getVTKViewer( theViewerId );
  if( aViewer )
  {
    SALOME_ListIO aListIO;
    aViewer->GetVisible( aListIO );
    
    HYDROGUI_VTKPrs* aPrs;
    SALOME_ListIteratorOfListIO anIter( aListIO );
    for( ; anIter.More(); anIter.Next() )
    {
      Handle(SALOME_InteractiveObject) aPrsObj = anIter.Value();
      if ( !aPrsObj.IsNull() )
      {
        Handle(HYDROData_Entity) anOwnerObj = 
          module()->getDataModel()->objectByEntry( aPrsObj->getEntry() );
        if ( anOwnerObj.IsNull() )
        {
          // We found an unknown IO presented in the viewer. 
          // Remove such an orphan presentation from all the views of the viewer.
          SUIT_ViewManager* aViewMgr = dynamic_cast<SUIT_ViewManager*>( aViewer->getViewManager() );
          if ( aViewMgr && aViewMgr->getViewsCount() > 0 )
          {
            QVector<SUIT_ViewWindow*> aViews = aViewMgr->getViews();
            foreach ( SUIT_ViewWindow* aView, aViews )
            {
              SVTK_ViewWindow* aVTKView = dynamic_cast<SVTK_ViewWindow*>( aView );
              if ( aVTKView )
              {
                aVTKView->Erase( aPrsObj, false );
              }
            }
          }
          // Remove the object presentation from the module's cache
          module()->removeObjectVTKPrs( (size_t)aViewer, aPrsObj->getEntry() );
          doEraseScalarBar = true;
        }
        else if ( anOwnerObj->IsRemoved() )
        {
          aPrs = module()->getObjectVTKPrs( (size_t)aViewer, anOwnerObj );
          if ( aPrs )
          {
            aViewer->Erase( aPrs );
          }
          else
          {
          }
          module()->removeObjectVTKPrs( (size_t)aViewer, anOwnerObj );
          doEraseScalarBar = true;
        }
      }
    }

    if ( doEraseScalarBar )
    {
      EraseScalarBar( theViewerId );
    }
  }
}

HYDROGUI_VTKPrsDriver* HYDROGUI_VTKPrsDisplayer::getDriver( const size_t theViewId, const Handle(HYDROData_Entity)& theObj )
{
  HYDROGUI_VTKPrsDriver* aDriver = NULL;
  ObjectKind aKind = theObj->GetKind();
  if( theObj->GetKind() == KIND_BATHYMETRY )
  {
    if ( !myDriver )
    {
      myDriver = new HYDROGUI_VTKPrsBathymetryDriver( myScalarBars[ theViewId ] );
    }
    aDriver = myDriver;
  }
  else
  {
    if ( !myShapeDriver )
    {
      myShapeDriver = new HYDROGUI_VTKPrsShapeDriver( myScalarBars[ theViewId ] );
    }
    aDriver = myShapeDriver;
  }

  return aDriver;
}

QString HYDROGUI_VTKPrsDisplayer::GetType() const
{
  return SVTK_Viewer::Type();
}

void HYDROGUI_VTKPrsDisplayer::SetZRange( const size_t theViewId, double theRange[] )
{
  myScalarBars[ theViewId ]->GetLookupTable()->SetRange( theRange );
}

double* HYDROGUI_VTKPrsDisplayer::GetZRange( const size_t theViewId ) const
{
  return myScalarBars[ theViewId ]->GetLookupTable()->GetRange();
}

void HYDROGUI_VTKPrsDisplayer::createScalarBar( const size_t theViewId )
{
  if ( !myScalarBars.contains( theViewId ) )
  {
    // The invalid value is used to identify the case when the table range is not initialized yet.
    double anInvalidValue = HYDROGUI_VTKPrs::InvalidZValue();
    vtkLookupTable* aTable = vtkLookupTable::New();
    aTable->SetHueRange( HUE_START, HUE_END );
    aTable->SetSaturationRange( SATURATION_START, SATURATION_END );
    aTable->SetTableRange( anInvalidValue, anInvalidValue );
    aTable->SetValueRange( 1.0, 1.0 );
    aTable->SetAlphaRange( 1.0, 1.0 );
    aTable->SetNumberOfColors( NB_COLORS );
    aTable->Build();
    vtkSmartPointer<vtkScalarBarActor> aScalarBar = vtkScalarBarActor::New();
    aScalarBar->SetLookupTable( aTable );
    aScalarBar->SetNumberOfLabels( NB_COLORS * 0.75 );
    aScalarBar->SetWidth( aScalarBar->GetWidth() / 1.5 );
    aScalarBar->SetTextureGridWidth( aScalarBar->GetTextureGridWidth() * 4. );
    aScalarBar->SetTitle( "  " );
    // The call of SetTitle() with dummy string is a workaround necessary
    // to avoid the problem with uninitialized variables in VTK scalar bar actor
    // which leads to incorrect (very big) size of the VTK scalar bar presentation
    aTable->Delete();
    myScalarBars.insert( theViewId, aScalarBar );
  }
}

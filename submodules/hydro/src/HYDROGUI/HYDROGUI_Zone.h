// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ZONE_H
#define HYDROGUI_ZONE_H

#include "HYDROGUI_DataObject.h"

#include <HYDROData_Zone.h>

#include <QString>
#include <QColor>

/**
 * \class HYDROGUI_Zone
 * \brief Browser item presenting a zone, used for object browser tree creation.
 *
 * This is an Object Browser item that contains reference to a zone data structure 
 * element inside.
 */
class HYDROGUI_Zone : public HYDROGUI_DataObject
{
public:
  /**
   * Constructor.
   * \param theParent parent data object
   * \param theData reference to the corresponding object from data structure
   * \param theParentEntry entry of the parent data object (for reference objects)
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  HYDROGUI_Zone( SUIT_DataObject*       theParent,
                 Handle(HYDROData_Zone) theData,
                 const QString&         theParentEntry,
                 const bool             theIsInOperation = false );
    
  /**
   * Returns the text for the specified column.
   */
  QString     text( const int = NameId ) const;

  /**
   * Returns the color for the specified column.
   */
  QColor      color( const ColorRole theColorRole, const int theColumnId = NameId ) const;

  /**
   * Return true because zones are draggable.
   */
  bool        isDraggable() const { return true; }

  bool        renameAllowed( const int = NameId ) const;
  //bool        setName( const QString& );

  /**
   * Returns true if it is a zone which needs merge of bathymetries.
   */
  bool        isMergingNeed() const;

  /**
   * Returns the list of source object names.
   */
  QStringList getObjects() const;

  /**
   * Returns the merging type for conflict altidudes.
   */
  HYDROData_Zone::MergeType getMergeType() const;

  /**
   * Set the merging type for conflict altidudes/types. 
   * If the type is Merge_Object then use the second parameter to set the merge bathymetry/land cover.
   */
  void        setMergeType( int theMergeType, QString theMergeObjectName = QString() );

private:
  QString getRefObjectNames() const;
  QString getObjectName() const;
};
#endif

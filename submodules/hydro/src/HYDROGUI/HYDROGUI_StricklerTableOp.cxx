// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_StricklerTableOp.h"

#include "HYDROGUI_StricklerTableDlg.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_Operations.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Document.h>

#include <LightApp_UpdateFlags.h>

#include <QFileInfo>

HYDROGUI_StricklerTableOp::HYDROGUI_StricklerTableOp( HYDROGUI_Module* theModule, int theType )
    : HYDROGUI_Operation( theModule ), 
    myType( theType )
{
    setName( isEdit() ? tr( "EDIT_STRICKLER_TABLE" ) :
                        ( isImport() ? tr( "IMPORT_STRICKLER_TABLE" ) : tr( "EXPORT_STRICKLER_TABLE" ) ) );
}

HYDROGUI_StricklerTableOp::~HYDROGUI_StricklerTableOp()
{
}

void HYDROGUI_StricklerTableOp::startOperation()
{   
    HYDROGUI_Operation::startOperation();

    HYDROGUI_StricklerTableDlg* aPanel = (HYDROGUI_StricklerTableDlg*)inputPanel();
    aPanel->reset();

    if ( !isImport() && isApplyAndClose() )
        myObject = Handle(HYDROData_StricklerTable)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );

    if ( isEdit() )
    {
        if ( !myObject.IsNull() )
        {
            // Edit selected Strickler table
            aPanel->setTableName( myObject->GetName() );
            aPanel->setGuiData( myObject );
        }
    }
    else if ( isExport() )
    {
        if ( !myObject.IsNull() )
            aPanel->setTableName( myObject->GetName() );
    }
    aPanel->setTableNameReadOnly( isExport() );
}

void HYDROGUI_StricklerTableOp::abortOperation()
{
    HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_StricklerTableOp::commitOperation()
{
    HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_StricklerTableOp::createInputPanel() const
{
    int type = isEdit() ? HYDROGUI_StricklerTableDlg::Edit : ( isImport() ? HYDROGUI_StricklerTableDlg::Import : HYDROGUI_StricklerTableDlg::Export );
    HYDROGUI_StricklerTableDlg* aPanel = new HYDROGUI_StricklerTableDlg( module(), getName(), type );
    connect( aPanel, SIGNAL( fileSelected( const QString& ) ), SLOT( onFileSelected() ) );
    return aPanel;
}

bool HYDROGUI_StricklerTableOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
    QStringList& theBrowseObjectsEntries )
{
    HYDROGUI_StricklerTableDlg* aPanel = ::qobject_cast<HYDROGUI_StricklerTableDlg*>( inputPanel() );
    if ( !aPanel )
        return false;

    QString aFilePath;
    if( !isEdit() )
    {
        aFilePath = aPanel->getFileName();
        if ( aFilePath.isEmpty() )
        {
            theErrorMsg = tr( "SELECT_STRICKLER_TABLE_FILE" ).arg( aFilePath );
            return false;
        }
    }

    QString aStricklerTableName = aPanel->getTableName().simplified();
    if ( aStricklerTableName.isEmpty() )
    {
        theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
        return false;
    }

    if ( isImport() || ( isEdit() && !myObject.IsNull() && myObject->GetName() != aStricklerTableName ) )
    {
        // check that there are no other objects with the same name in the document
        Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), aStricklerTableName );
        if( !anObject.IsNull() )
        {
            theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( aStricklerTableName );
            return false;
        }
    }

    Handle(HYDROData_StricklerTable) aStricklerTableObj;
    if ( isImport() )
    {
        aStricklerTableObj = Handle(HYDROData_StricklerTable)::DownCast( doc()->CreateObject( KIND_STRICKLER_TABLE ) );
        QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aStricklerTableObj );
        theBrowseObjectsEntries.append( anEntry );
    }
    else
        aStricklerTableObj = myObject;

    if ( aStricklerTableObj.IsNull() )
        return false;

    if ( isExport() )
    {
        bool res = false;
        QString aFilePath = aPanel->getFileName().simplified();
        if ( !aFilePath.isEmpty() )
            res = aStricklerTableObj->Export( aFilePath );
        return res;
    }

    startDocOperation();

    aStricklerTableObj->SetName( aStricklerTableName );

    if( isEdit() )
    {
        // Get data from input panel's table and save it into data model object
      aPanel->getGuiData( aStricklerTableObj );
    }
    else
    {
        // Import data from Strickler table file into data model object
        aStricklerTableObj->Import( aFilePath );
    }

    aStricklerTableObj->Update();

    commitDocOperation();

    if( !isEdit() )
    {
        QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aStricklerTableObj );
        theBrowseObjectsEntries.append( anEntry );
    } else {
      module()->getOCCDisplayer()->SetToUpdateColorScale();
    }

    theUpdateFlags |= UF_ObjBrowser | UF_OCCViewer;

    return true;
}

void HYDROGUI_StricklerTableOp::onFileSelected()
{
    HYDROGUI_StricklerTableDlg* aPanel = 
        ::qobject_cast<HYDROGUI_StricklerTableDlg*>( inputPanel() );
    if ( !aPanel )
        return;

    QString anStricklerTableName = aPanel->getTableName().simplified();
    if ( anStricklerTableName.isEmpty() )
    {
        anStricklerTableName = aPanel->getFileName();
        if ( !anStricklerTableName.isEmpty() ) {
            anStricklerTableName = QFileInfo( anStricklerTableName ).baseName();
        }

        if ( anStricklerTableName.isEmpty() ) {
            anStricklerTableName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_STRICKLER_TABLE_NAME" ) );
        }
        aPanel->setTableName( anStricklerTableName );
    }
}

bool HYDROGUI_StricklerTableOp::isEdit() const
{
    return myType == EditStricklerTableId;
}

bool HYDROGUI_StricklerTableOp::isImport() const
{
    return myType == ImportStricklerTableFromFileId;
}

bool HYDROGUI_StricklerTableOp::isExport() const
{
    return myType == ExportStricklerTableFromFileId;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ObserveImageOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_PrsImage.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Image.h>

#include <LightApp_Application.h>

#include <GraphicsView_ViewManager.h>
#include <GraphicsView_Viewer.h>

HYDROGUI_ObserveImageOp::HYDROGUI_ObserveImageOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "OBSERVE_IMAGE" ) );
}

HYDROGUI_ObserveImageOp::~HYDROGUI_ObserveImageOp()
{
}

void HYDROGUI_ObserveImageOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  Handle(HYDROData_Image) anImageObj =
    Handle(HYDROData_Image)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( !anImageObj.IsNull() )
  {
    QImage anImage = anImageObj->Image();
    QTransform aTransform = anImageObj->Trsf();

    HYDROGUI_PrsImage* aPrs = new HYDROGUI_PrsImage( anImageObj );
    aPrs->setImage( anImage );
    aPrs->setTransform( aTransform );
    aPrs->compute();

    LightApp_Application* anApp = module()->getApp();
    GraphicsView_ViewManager* aViewManager =
      dynamic_cast<GraphicsView_ViewManager*>( anApp->createViewManager( GraphicsView_Viewer::Type() ) );
    if( aViewManager )
    {
      module()->setViewManagerRole( aViewManager, HYDROGUI_Module::VMR_ObserveImage );
      aViewManager->setTitle( anImageObj->GetName() );
      if( GraphicsView_Viewer* aViewer = aViewManager->getViewer() )
        module()->setObjectVisible( (size_t)aViewer, anImageObj, true );
    }
  }

  module()->update( UF_Viewer );
  commit();
}

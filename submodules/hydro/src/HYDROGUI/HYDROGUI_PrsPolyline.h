// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRSPOLYLINE_H
#define HYDROGUI_PRSPOLYLINE_H

#include "HYDROGUI_Prs.h"

/*
  Class       : HYDROGUI_PrsPolyline
  Description : Presentation for polyline object
*/
class HYDROGUI_PrsPolyline : public HYDROGUI_Prs
{
public:
  HYDROGUI_PrsPolyline( const Handle(HYDROData_Entity)& theObject );
  virtual ~HYDROGUI_PrsPolyline();

public:
  void                            setPath( const QPainterPath& thePath );
  QPainterPath                    getPath() const;

public:
  // from QGraphicsItem
  virtual QRectF                  boundingRect() const;

  // from GraphicsView_Object
  virtual void                    compute();

  virtual bool                    isMovable() const { return false; }

  virtual bool                    checkHighlight( double theX, double theY, QCursor& theCursor ) const;

  virtual bool                    select( double theX, double theY, const QRectF& theRect );
  virtual void                    unselect();
  virtual void                    setSelected( bool theState );

protected:
  QGraphicsPathItem*              myPolylineItem;

  QPainterPath                    myPath;
};

#endif

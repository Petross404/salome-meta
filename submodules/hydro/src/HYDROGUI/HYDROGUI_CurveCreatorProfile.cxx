// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_CurveCreatorProfile.h>
#include <CurveCreator_Displayer.hxx>
#include <CurveCreator_Section.hxx>
#include <QVector>
#include <TopoDS_Shape.hxx>
#include <CurveCreator_Utils.hxx>
#include <AIS_Shape.hxx>
#include <Prs3d_PointAspect.hxx>

HYDROGUI_CurveCreatorProfile::HYDROGUI_CurveCreatorProfile()
: CurveCreator_Curve( CurveCreator::Dim2d )
{
  CurveCreator_Section *aSection = new CurveCreator_Section;
  aSection->myName     = getUniqSectionName();
  aSection->myType     = CurveCreator::Polyline;
  aSection->myIsClosed = false;
  myCurveColor = Quantity_NOC_RED;

  mySections.push_back( aSection );
  mySkipSorting = true;
}

HYDROGUI_CurveCreatorProfile::~HYDROGUI_CurveCreatorProfile()
{
}

bool HYDROGUI_CurveCreatorProfile::moveSectionInternal( const int theISection,
                                                        const int theNewIndex )
{
  return false;
}

bool HYDROGUI_CurveCreatorProfile::moveSection( const int theISection,
                                                const int theNewIndex )
{
  return false;
}

bool HYDROGUI_CurveCreatorProfile::clearInternal()
{
  // erase curve from the viewer
  if( myDisplayer )
    myDisplayer->eraseAll( true );

  // Delete all allocated data.
  CurveCreator_Section* aSection = (CurveCreator_Section*)getSection( 0 );
  if ( aSection )
    aSection->myPoints.clear();

  return true;
}

bool HYDROGUI_CurveCreatorProfile::joinInternal( const std::list<int>& theSections )
{
  return false;
}

bool HYDROGUI_CurveCreatorProfile::join( const std::list<int>& theSections )
{
  return false;
}

int HYDROGUI_CurveCreatorProfile::addSectionInternal( const std::string&               theName, 
                                                      const CurveCreator::SectionType  theType,
                                                      const bool                       theIsClosed, 
                                                      const CurveCreator::Coordinates& thePoints )
{
  return -1;
}

int HYDROGUI_CurveCreatorProfile::addSection( const std::string&              theName,
                                              const CurveCreator::SectionType theType,
                                              const bool                      theIsClosed )
{
  return -1;
}

int HYDROGUI_CurveCreatorProfile::addSection( const std::string&               theName,
                                              const CurveCreator::SectionType  theType,
                                              const bool                       theIsClosed, 
                                              const CurveCreator::Coordinates& thePoints )
{
  return -1;
}

bool HYDROGUI_CurveCreatorProfile::removeSectionInternal( const int theISection )
{
  return false;
}
  
bool HYDROGUI_CurveCreatorProfile::removeSection( const int theISection )
{
  return false;
}

bool HYDROGUI_CurveCreatorProfile::setClosedInternal( const int theISection, const bool theIsClosed )
{
  return false;
}

bool HYDROGUI_CurveCreatorProfile::setClosed( const int theISection, const bool theIsClosed )
{
  return false;
}

bool HYDROGUI_CurveCreatorProfile::addPointsInternal( const CurveCreator::SectionsMap &theSectionsMap )
{
  bool res = false;

  CurveCreator_Section* aSection = (CurveCreator_Section*)getSection( 0 );
  if( !aSection )
    return res;

  CurveCreator::SectionsMap::const_iterator anIt = theSectionsMap.begin();
  for ( ; anIt != theSectionsMap.end(); anIt++ )
  {
    int anISection = anIt->first;
    if( anISection != 0 )
      continue;

    const CurveCreator::PosPointsList& aSectionPoints = anIt->second;

    CurveCreator::PosPointsList::const_iterator aPntIt = aSectionPoints.begin();
    for( ; aPntIt != aSectionPoints.end(); aPntIt++ )
    {
      const CurveCreator::Coordinates& aNewCoords = (*aPntIt)->myCoords;
      CurveCreator::Coordinates::const_iterator aNewPntIt = aNewCoords.begin();
      for( ; aNewPntIt != aNewCoords.end(); aNewPntIt++ )
      {
        const CurveCreator::TypeCoord& aCoordU = *aNewPntIt++;
        if ( aNewPntIt == aNewCoords.end() )
          break;
        
        const CurveCreator::TypeCoord& aCoordZ = *aNewPntIt;

        CurveCreator::TypeCoord aC = aCoordU - 1;
        if ( !aSection->myPoints.empty() )
          aC = *(aSection->myPoints.end() - 2);

        if ( aSection->myPoints.empty() || aCoordU >= aC )
        {
          aSection->myPoints.push_back( aCoordU );
          aSection->myPoints.push_back( aCoordZ );
        }
        else if ( aCoordU < aSection->myPoints.front() )
        {
          aSection->myPoints.push_front( aCoordZ );
          aSection->myPoints.push_front( aCoordU );
        }
        else
        {
          CurveCreator::Coordinates::iterator aRefPntIt = aSection->myPoints.begin();
          for( ; aRefPntIt != aSection->myPoints.end(); aRefPntIt++ )
          {
            const CurveCreator::TypeCoord& aRefCoordU = *aRefPntIt++;
            if ( aCoordU < aRefCoordU )
              break;
          }

          aSection->myPoints.insert( aRefPntIt - 1, aNewPntIt - 1, aNewPntIt + 1 );
        }
      }
    }

    res = true;
  }

  if ( res )
    redisplayCurve(false);

  return res;
}

//! For internal use only! Undo/Redo are not used here.
bool HYDROGUI_CurveCreatorProfile::setPointInternal( const CurveCreator::SectionsMap &theSectionsMap )
{
  bool aRes = false;

  if ( mySkipSorting ) {
    return CurveCreator_Curve::setPointInternal( theSectionsMap );
  }

  int anISection = 0;
  CurveCreator_Section* aSection = (CurveCreator_Section*)getSection( anISection );
  if( !aSection )
    return aRes;

  CurveCreator::SectionsMap::const_iterator anIt = theSectionsMap.begin();
  if ( anIt == theSectionsMap.end() )
    return aRes;

  const CurveCreator::PosPointsList& aSectionPoints = anIt->second;

  std::list<int> aConvPoints;
  convert( aSectionPoints, aConvPoints );
  removeSectionPoints( anISection, aConvPoints );

  aRes = addPointsInternal( theSectionsMap );
  if ( aRes )
    redisplayCurve(false);

  return aRes;
}

void HYDROGUI_CurveCreatorProfile::convert( const CurveCreator::PosPointsList& thePoints,
                                            std::list<int>& theConvPoints )
{
  theConvPoints.clear();

  CurveCreator::PosPointsList::const_iterator aPntIt = thePoints.begin(),
                                              aPntLast = thePoints.end();
  for( ; aPntIt != aPntLast; aPntIt++ )
  {
    int anIPnt = (*aPntIt)->myID;
    theConvPoints.push_back( anIPnt );
  }
}

bool HYDROGUI_CurveCreatorProfile::canPointsBeSorted()
{
  return false;
}

/**
 *  Add one point to the specified section starting from the given theIPnt index
 *  (or at the end of points if \a theIPnt is -1).
 */
bool HYDROGUI_CurveCreatorProfile::addPoints( const CurveCreator::Coordinates& theCoords,
                                              const int theISection,
                                              const int theIPnt )
{
  int anIPnt = theIPnt;

  if ( anIPnt == - 1 && theCoords.size() > 1 ) {
    CurveCreator::Coordinates aCoords;
    for ( int i = 0, aNb = getNbPoints( theISection ); i < aNb; i++ ) {
      aCoords = getPoint( theISection, i );
      if ( aCoords.size() < 2 ) {
        continue;
      }

      if ( theCoords[0] < aCoords[0] ) {
        anIPnt = i;
        break;
      }
    }
  }
  
  return CurveCreator_Curve::addPoints( theCoords, theISection, anIPnt );
}

bool ULess( const gp_Pnt& p1, const gp_Pnt& p2 )
{
  return p1.X() < p2.X();
}

Handle(TColgp_HArray1OfPnt) HYDROGUI_CurveCreatorProfile::GetDifferentPoints( int theISection ) const
{
  Handle(TColgp_HArray1OfPnt) points = CurveCreator_Curve::GetDifferentPoints( theISection );
  if( points.IsNull() )
    return points;

  QVector<gp_Pnt> vpoints( points->Size() );
  for( int i=points->Lower(), j=0, n=points->Upper(); i<=n; i++, j++ )
    vpoints[j] = points->Value( i );

  qSort( vpoints.begin(), vpoints.end(), ULess );

  for( int i=points->Lower(), j=0, n=points->Upper(); i<=n; i++, j++ )
    points->SetValue( i, vpoints[j] );

  return points;
}
/*
void HYDROGUI_CurveCreatorProfile::constructAISObject()
{
  TopoDS_Shape aShape;
  CurveCreator_Utils::constructShape( this, aShape );
  myAISShape = new AIS_Shape( aShape );  
  myAISShape->SetColor( myCurveColor );
  myAISShape->SetWidth( myLineWidth );
  Handle(Prs3d_PointAspect) anAspect = myAISShape->Attributes()->PointAspect();
  anAspect->SetScale( 3.0 );
  anAspect->SetTypeOfMarker(Aspect_TOM_O_POINT);
  anAspect->SetColor(myPointAspectColor);
  myAISShape->Attributes()->SetPointAspect( anAspect );
}*/


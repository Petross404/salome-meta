// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_POLYLINEDLG_H
#define HYDROGUI_POLYLINEDLG_H

#include "HYDROGUI_InputPanel.h"

class QGroupBox;
class QLineEdit;
class CurveCreator_Widget;
class CurveCreator_ICurve;
class OCCViewer_Viewer;

class HYDROGUI_PolylineDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_PolylineDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_PolylineDlg();

  void setOCCViewer( OCCViewer_Viewer* theViewer );

  void  setPolylineName( const QString& theName );
  QString getPolylineName() const;
	
  void  setCurve( CurveCreator_ICurve* theCurve );

  void  reset();

  QList<int> getSelectedSections();

  void  deleteSelected();
  bool  deleteEnabled();

protected slots:
  void processStartedSubOperation( QWidget*, bool );
  void processFinishedSubOperation( QWidget* );

signals:
  void                  createPreview( QString );
  void                  selectionChanged();
  void                  widgetCreated(QWidget*);
  void                  subOperationStarted(QWidget*);
  void                  subOperationFinished(QWidget*);
private:
  QLineEdit*            myName;
  CurveCreator_Widget*  myEditorWidget;
  QGroupBox*            myAddElementBox;
};

#endif

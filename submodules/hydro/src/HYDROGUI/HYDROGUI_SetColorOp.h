// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_SETCOLOROP_H
#define HYDROGUI_SETCOLOROP_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_Entity.h>

class HYDROGUI_ColorDlg;

class HYDROGUI_SetColorOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_SetColorOp( HYDROGUI_Module* theModule );
  virtual ~HYDROGUI_SetColorOp();


  static bool CanObjectBeColored( const Handle(HYDROData_Entity)& theObject,
                                  HYDROGUI_Module* theModule );

protected:
  virtual void startOperation();

  virtual bool processApply( int& theUpdateFlags, QString& theErrorMsg,
                             QStringList& theBrowseObjectsEntries );

private:
  HYDROGUI_ColorDlg* myColorDlg;

  Handle(HYDROData_Entity) myEditedObject;
};

#endif
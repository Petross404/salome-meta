// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_SetTransparencyOp.h"

#include "HYDROGUI_TransparencyDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>

/**
  Constructor.
  @param theModule the module
*/
HYDROGUI_SetTransparencyOp::HYDROGUI_SetTransparencyOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ),
  myDlg( NULL )
{
  setName( tr( "SET_TRANSPARENCY" ) );
}

/**
  Destructor.
*/
HYDROGUI_SetTransparencyOp::~HYDROGUI_SetTransparencyOp()
{
}

void HYDROGUI_SetTransparencyOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myEditedObject = Handle(HYDROData_LandCoverMap)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if ( !myEditedObject.IsNull() )
  {
    myDlg = new HYDROGUI_TransparencyDlg( module()->getApp()->desktop() );
    myDlg->setModal( true );
    myDlg->setWindowTitle( getName() );
    myDlg->setTransparency( myEditedObject->GetTransparency() );

    connect( myDlg, SIGNAL( applyAndClose() ), this, SLOT( onApplyAndClose() ) );
    connect( myDlg, SIGNAL( apply() ), this, SLOT( onApply() ) );
    connect( myDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );

    myDlg->exec();
  }  
}

bool HYDROGUI_SetTransparencyOp::processApply( int& theUpdateFlags,
                                               QString& theErrorMsg,
                                               QStringList& theBrowseObjectsEntries )
{
  if ( !myDlg || myEditedObject.IsNull() )
    return false;
  
  myEditedObject->SetTransparency( myDlg->getTransparency() );

  module()->setIsToUpdate( myEditedObject );
  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
  
  return true;
}

void HYDROGUI_SetTransparencyOp::processCancel()
{
  // Delete the dialog
  if ( myDlg ) {
    delete myDlg;
    myDlg = 0;
  }
}

void HYDROGUI_SetTransparencyOp::onApplyAndClose()
{
  HYDROGUI_Operation::onApplyAndClose();
  if ( myDlg )
    myDlg->reject();
}

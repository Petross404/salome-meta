// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_OCCSelector.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_DataObject.h"
#include "HYDROGUI_DataOwner.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Shape.h"

#include <AIS_ListOfInteractive.hxx>
#include <AIS_ListIteratorOfListOfInteractive.hxx>

#include <LightApp_DataOwner.h>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_OCCSelector::HYDROGUI_OCCSelector( HYDROGUI_Module*   theModule,
                                            OCCViewer_Viewer*  theViewer,
                                            SUIT_SelectionMgr* theSelMgr )
: LightApp_OCCSelector( theViewer, theSelMgr ),
  myModule( theModule )
{
}

HYDROGUI_OCCSelector::~HYDROGUI_OCCSelector()
{
}

void HYDROGUI_OCCSelector::getSelection( SUIT_DataOwnerPtrList& aList ) const
{
  DEBTRACE("getSelection");
  OCCViewer_Viewer* aViewer = viewer();
  if ( !aViewer )
    return;

  Handle(AIS_InteractiveContext) aContext = aViewer->getAISContext();
  //bool isLocalContext = aContext->HasOpenedContext();

  AIS_ListOfInteractive aSelList;
  aViewer->getSelectedObjects( aSelList );
  DEBTRACE("  aSelList.Size(): " << aSelList.Size());
  for ( AIS_ListIteratorOfListOfInteractive anIt( aSelList ); anIt.More(); anIt.Next() )
    if ( !anIt.Value().IsNull() )
    {
      //if ( !isLocalContext ) {
        QString anEntry = entry( anIt.Value() );
        DEBTRACE("  anEntry: " << anEntry.toStdString());
        if ( !anEntry.isEmpty() ) {
          aList.append( SUIT_DataOwnerPtr( new LightApp_DataOwner( entry( anIt.Value() ) ) ) );
        } else {
          aList.append( SUIT_DataOwnerPtr( new HYDROGUI_DataOwner( anIt.Value() ) ) );
        }
      //}
    }
  // add externally selected objects
  SUIT_DataOwnerPtrList::const_iterator anExtIter;
  for(anExtIter = mySelectedExternals.begin(); anExtIter != mySelectedExternals.end(); anExtIter++) {
	DEBTRACE("  anExternal");
    aList.append(*anExtIter);
  }
}

void HYDROGUI_OCCSelector::setSelection( const SUIT_DataOwnerPtrList& aList )
{
  DEBTRACE("setSelection");
  OCCViewer_Viewer* aViewer = viewer();
  if ( !aViewer )
    return;

  size_t aViewerId = (size_t)aViewer;

  Handle(AIS_InteractiveContext) aContext = aViewer->getAISContext();
  if ( aContext.IsNull() ) {
    return;
  }

  AIS_ListOfInteractive aSelList;

  /*QMap<QString, Handle(AIS_InteractiveObject)> aDisplayed;
  AIS_ListOfInteractive aDispList;
  aContext->DisplayedObjects( aDispList );

  for ( AIS_ListIteratorOfListOfInteractive it( aDispList ); it.More(); it.Next() )
  {
    QString entryStr = entry( it.Value() );
    if ( !entryStr.isEmpty() )
      aDisplayed.insert( entryStr, it.Value() );
  }*/

  mySelectedExternals.clear();
  for ( SUIT_DataOwnerPtrList::const_iterator itr = aList.begin(); itr != aList.end(); ++itr )
  {
    const LightApp_DataOwner* owner = dynamic_cast<const LightApp_DataOwner*>( (*itr).operator->() );
    if ( owner )   //&& aDisplayed.contains( owner->entry() ) )
    {
      QString anEntry = owner->entry();
      DEBTRACE("  anEntry: " << anEntry.toStdString());
      Handle(HYDROData_Entity) anEntity = myModule->getDataModel()->objectByEntry( anEntry );
      if( !anEntity.IsNull() )
      {
    	DEBTRACE("aViewerId, anEntity " << aViewerId << " " << anEntity.get());
        HYDROGUI_Shape* aShape = myModule->getObjectShape( aViewerId, anEntity );
        if( aShape && aShape->isVisible() )
        {
          foreach( Handle(AIS_InteractiveObject) obj, aShape->getAISObjects() )
            aSelList.Append( obj );
        }
      }
    }
    else {
      const HYDROGUI_DataOwner* hydroOwner = dynamic_cast<const HYDROGUI_DataOwner*>( (*itr).operator->() );
      DEBTRACE("  hydroOwner: " << hydroOwner);
      if ( hydroOwner && !hydroOwner->IO().IsNull() ) {
        aSelList.Append( hydroOwner->IO() );
      } else
        mySelectedExternals.append(*itr);
    }
  }
  DEBTRACE("aSelList.Size() " << aSelList.Size());
  aViewer->unHighlightAll( false );
  aViewer->setObjectsSelected( aSelList );
}

QString HYDROGUI_OCCSelector::entry( const Handle(AIS_InteractiveObject)& anAIS ) const
{
  QString aRes;

  if ( anAIS.IsNull() || !anAIS->HasOwner() )
    return aRes;

  Handle(HYDROData_Entity) anObj =
    Handle(HYDROData_Entity)::DownCast( anAIS->GetOwner() );
  if ( !anObj.IsNull() )
    aRes = HYDROGUI_DataObject::dataObjectEntry( anObj );

  return aRes;
}

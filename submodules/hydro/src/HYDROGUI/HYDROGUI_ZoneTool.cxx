// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ZoneTool.h>
#include <HYDROData_Zone.h>
#include <QColor>

int randHue( int theHue )
{
  if( theHue>=0 && theHue<360 )
    return theHue;
  else
    return rand()%360;
}

int randValue( int theMin, int theMax )
{
  return theMin + rand()%(theMax-theMin);
}

void AssignDefaultColor( const Handle(HYDROData_Zone)& theZone,
                         int theHue, int theSMargin, int theBMargin )
{
  int aHue = randHue( theHue );
  int aSat = randValue( theSMargin, 255 );
  int aBri = randValue( theBMargin, 255-theBMargin );

  QColor aColor = QColor::fromHsl( aHue, aSat, aBri );
  theZone->SetColor( aColor );
}

namespace HYDROGUI_ZoneTool { 

void AssignDefaultColors( const QList<Handle(HYDROData_Region)>& theRegions, int theSMargin, int theBMargin )
{
  foreach( Handle(HYDROData_Region) region, theRegions )
    AssignDefaultColors( region, theSMargin, theBMargin );
}

void AssignDefaultColors( const Handle(HYDROData_Region)& theRegion, int theSMargin, int theBMargin )
{
  int aHue = randHue( -1 );
  HYDROData_SequenceOfObjects zones = theRegion->GetZones();
  HYDROData_SequenceOfObjects::Iterator zonesIt( zones );
  for( ; zonesIt.More(); zonesIt.Next() )
  {
    Handle(HYDROData_Zone) aZone =
      Handle(HYDROData_Zone)::DownCast( zonesIt.Value() );
    AssignDefaultColor( aZone, aHue, theSMargin, theBMargin );
  }
}

void AssignDefaultColors( const HYDROData_SequenceOfObjects& theRegions, int theSMargin, int theBMargin )
{
  for ( int i = 1; i <= theRegions.Length(); i++ )
    AssignDefaultColors( Handle(HYDROData_Region)::DownCast(theRegions(i)), theSMargin, theBMargin );
}

};

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ObjSelector.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"

#include <HYDROData_PolylineXY.h>

#include <GraphicsView_Object.h>

#include <LightApp_Application.h>
#include <LightApp_GVSelector.h>
#include <LightApp_SelectionMgr.h>

#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>

#include <QLayout>
#include <QLineEdit>
#include <QToolButton>

HYDROGUI_ObjSelector::HYDROGUI_ObjSelector( HYDROGUI_Module* theModule,
                                            const ObjectKind theObjectKind,
                                            QWidget* theParent,
                                            const int theObjectFlags)
: QAbstractButton( theParent ),
  myObjectKind( theObjectKind ),
  myModule( theModule ),
  myObjectFlags( theObjectFlags )
{
  QHBoxLayout* aLayout = new QHBoxLayout( this );
  aLayout->setMargin( 0 );
  aLayout->setSpacing( 5 );
  myBtn = new QToolButton( this );
  myBtn->setCheckable( true );
  myBtn->setChecked( false );
  myObjName = new QLineEdit( this );
  myObjName->setReadOnly( true );
  aLayout->addWidget( myBtn, 0 );
  aLayout->addWidget( myObjName, 1 );

  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  myBtn->setIcon( QIcon( aResMgr->loadPixmap( "HYDRO", tr( "SELECT_ICO" ) ) ) );

  SUIT_SelectionMgr* aSelMgr = theModule->getApp()->selectionMgr();

  connect( myBtn, SIGNAL( toggled( bool ) ), this, SLOT( OnToggled( bool ) ) );
  connect( aSelMgr, SIGNAL( selectionChanged() ), this, SLOT( OnSelectionChanged() ) );
}

HYDROGUI_ObjSelector::~HYDROGUI_ObjSelector()
{
}

void HYDROGUI_ObjSelector::paintEvent( QPaintEvent* )
{
}

bool HYDROGUI_ObjSelector::hitButton( const QPoint& thePnt ) const
{
  return false;
}

void HYDROGUI_ObjSelector::OnToggled( bool isChecked )
{
  if( !isChecked )
    return;

  QList<HYDROGUI_ObjSelector*> aSelectors = parentWidget()->findChildren<HYDROGUI_ObjSelector*>();
  foreach( HYDROGUI_ObjSelector* aSelector, aSelectors )
    if( aSelector != this )
      aSelector->myBtn->setChecked( false );

  if( isChecked )
    OnSelectionChanged();
}

void HYDROGUI_ObjSelector::OnSelectionChanged()
{
  if( !myBtn->isChecked() )
    return;

  QString anObjName;
  Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::GetSelectedObject( myModule );
  if( !anObject.IsNull() )
    if( myObjectKind == KIND_UNKNOWN || myObjectKind == anObject->GetKind() )
    {
      if ( myObjectKind == KIND_POLYLINEXY && ( myObjectFlags & ClosedPolyline ) ) {
        // check if the polyline is closed
        Handle(HYDROData_PolylineXY) aPolylineObj = 
          Handle(HYDROData_PolylineXY)::DownCast( anObject );
        if ( !aPolylineObj.IsNull() && aPolylineObj->IsClosed() ) {
          anObjName = aPolylineObj->GetName();
        }
      } else {
        anObjName = anObject->GetName();
      }

      // Check if the same object has not been selected in other selectors of the same parent widget.
      if ( !anObjName.isEmpty() )
      {
        QList<HYDROGUI_ObjSelector*> aSelectors = parentWidget()->findChildren<HYDROGUI_ObjSelector*>();
        foreach( HYDROGUI_ObjSelector* aSelector, aSelectors )
        {
          if( aSelector != this  && ( aSelector->GetName() == anObjName ) )
          {
            // Forbid selection of the same object
            emit alreadySelected( anObjName );
            return;
          }
        }
      }
    }

  SetName( anObjName );
}

void HYDROGUI_ObjSelector::SetName( const QString& theName )
{
  myObjName->setText( theName );
  emit selectionChanged();
}

QString HYDROGUI_ObjSelector::GetName() const
{
  return myObjName->text();
}

void HYDROGUI_ObjSelector::Clear()
{
  myObjName->clear();
  myBtn->setChecked( false );
}

void HYDROGUI_ObjSelector::SetChecked( const bool theState )
{
  myBtn->setChecked( theState );
}

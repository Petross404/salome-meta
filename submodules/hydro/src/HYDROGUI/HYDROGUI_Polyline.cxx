// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Polyline.h>
#include <BRepAdaptor_Curve.hxx>
#include <BRepBndLib.hxx>
#include <BRep_Tool.hxx>
#include <GCPnts_AbscissaPoint.hxx>
#include <GCPnts_QuasiUniformDeflection.hxx>
#include <Graphic3d_ArrayOfPolylines.hxx>
#include <Prs3d_Arrow.hxx>
#include <Prs3d_LineAspect.hxx>
#include <Prs3d_Point.hxx>
#include <Prs3d_Root.hxx>
#include <TopExp.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Vertex.hxx>

IMPLEMENT_STANDARD_RTTIEXT(HYDROGUI_Polyline, AIS_Shape)


HYDROGUI_Polyline::HYDROGUI_Polyline(const TopoDS_Shape& shape)
  : AIS_Shape(shape)
{
}

HYDROGUI_Polyline::~HYDROGUI_Polyline()
{
}

Handle( Graphic3d_ArrayOfPolylines ) BuildEdgePresentation( const TopoDS_Edge& theEdge, double theDeviation )
{
  BRepAdaptor_Curve aCurveAdaptor( theEdge );
  GCPnts_QuasiUniformDeflection aPnts( aCurveAdaptor, theDeviation );

  Handle( Graphic3d_ArrayOfPolylines ) anArray;
  if( !aPnts.IsDone() )
    return anArray;

  int n = aPnts.NbPoints();
  anArray = new Graphic3d_ArrayOfPolylines( n );
  for( int i=1; i<=n; i++ )
    anArray->AddVertex( aPnts.Value( i ) );

  return anArray;
}

bool HYDROGUI_Polyline::GetColorOfSubShape(const TopoDS_Shape& SubShape, Quantity_Color& outColor)
{
  for (int i=1; i<=myShapeToColor.Extent();i++)
  {
    const TopoDS_Shape& aCurShape = myShapeToColor.FindKey(i);
    TopTools_IndexedMapOfShape aSubShMap;
    TopExp::MapShapes(aCurShape, aSubShMap);
    if (aSubShMap.Contains(SubShape))
    {
      outColor = myShapeToColor(i);
      return true;
    }
  }
  return false;
}

void HYDROGUI_Polyline::Compute(const Handle(PrsMgr_PresentationManager3d)& aPresentationManager,
                            const Handle(Prs3d_Presentation)& aPrs,
                            const Standard_Integer aMode)
{
  //AIS_Shape::Compute(aPresentationManager, aPrs, aMode);
  // return;

  aPrs->Clear();
  Handle(Graphic3d_Group) aGroup = Prs3d_Root::CurrentGroup( aPrs );
  Quantity_Color aColor = Attributes()->LineAspect()->Aspect()->Color();
  Aspect_TypeOfLine aType = Attributes()->LineAspect()->Aspect()->Type();
  Standard_Real anWidth = Attributes()->LineAspect()->Aspect()->Width();
  anWidth =2;
  Handle(Graphic3d_AspectLine3d) anAspect = new Graphic3d_AspectLine3d( aColor, aType, anWidth );

  Bnd_Box BB;
  BRepBndLib::AddClose(myshape, BB);
  double xmin, xmax, ymin, ymax, zmin, zmax;
  double devCoeff = 0.05;
  if (!BB.IsVoid())
  {
    BB.Get(xmin, ymin, zmin, xmax, ymax, zmax); //ignore Z coord
    double minSide = Min(Abs(xmax - xmin), Abs(ymax - ymin));
    devCoeff = minSide > 50 ? 0.05 : minSide / 3000;
  }

  TopExp_Explorer Exp ( myshape, TopAbs_WIRE );
  for ( ; Exp.More(); Exp.Next() )
  {
    TopoDS_Shape W = Exp.Current();
    TopExp_Explorer Exp1 (W, TopAbs_EDGE );
    Quantity_Color aWCol = aColor; //from drawer
    if (myShapeToColor.Contains(W))
    {
      Quantity_Color aWCol = myShapeToColor.FindFromKey(W);
      anAspect = new Graphic3d_AspectLine3d( aWCol, aType, anWidth );
    }
    for ( ; Exp1.More(); Exp1.Next() )
    {
      TopoDS_Edge anEdge = TopoDS::Edge( Exp1.Current() );
      Handle( Graphic3d_ArrayOfPolylines ) anArray = BuildEdgePresentation( anEdge, devCoeff);
      if( !anArray.IsNull() )
      {
        aGroup->SetPrimitivesAspect( anAspect );
        aGroup->AddPrimitiveArray( anArray );
      }
    }
  }
}

QList<Handle(AIS_InteractiveObject)> HYDROGUI_Polyline::createPresentations
  ( const TopoDS_Shape& theShape, int theType, int theSize )
{
  QList<Handle(AIS_InteractiveObject)> shapes;

  // 1. Main shape
  Handle(HYDROGUI_Polyline) aMPoly = new HYDROGUI_Polyline( theShape );
  shapes.append( aMPoly );

  // 2. Shapes for direction arrows on edges
  TopExp_Explorer Exp ( theShape, TopAbs_EDGE );
  for ( ; Exp.More(); Exp.Next() )
  {
    TopoDS_Edge anEdge = TopoDS::Edge(Exp.Current());
    if ( !anEdge.IsNull() )
    {
      Handle(HYDROGUI_Arrow) arrow = new HYDROGUI_Arrow( anEdge, aMPoly );
      if( theType>=0 )
        arrow->SetType( (HYDROGUI_Arrow::Type)theType );
      if( theSize>=0 )
        arrow->SetSize( theSize );
      shapes.append( arrow );
    }
  }

  return shapes;
}

void HYDROGUI_Polyline::update( const QList<Handle(AIS_InteractiveObject)>& theObjects, int theType, int theSize )
{
  foreach( Handle(AIS_InteractiveObject) obj, theObjects )
  {
    Handle(HYDROGUI_Arrow) arrow = Handle(HYDROGUI_Arrow)::DownCast( obj );
    if( !arrow.IsNull() )
    {
      if( theType>=0 )
        arrow->SetType( (HYDROGUI_Arrow::Type)theType );
      if( theSize>=0 )
        arrow->SetSize( theSize );
    }
  }
}




IMPLEMENT_STANDARD_RTTIEXT(HYDROGUI_Arrow, AIS_Shape)


  HYDROGUI_Arrow::HYDROGUI_Arrow( const TopoDS_Edge& edge, const Handle(HYDROGUI_Polyline)& polyline )
  : AIS_Shape( edge ), myType( Cone ), mySize( 35 ), myParentPoly (polyline)
{
}

HYDROGUI_Arrow::~HYDROGUI_Arrow()
{
}

HYDROGUI_Arrow::Type HYDROGUI_Arrow::GetType() const
{
  return myType;
}

void HYDROGUI_Arrow::SetType( HYDROGUI_Arrow::Type theType )
{
  myType = theType;
}

int HYDROGUI_Arrow::GetSize() const
{
  return mySize;
}

void HYDROGUI_Arrow::SetSize( int theSize )
{
  mySize = qMax( theSize, 0 );
}

const Handle(HYDROGUI_Polyline)& HYDROGUI_Arrow::getParentPolyline() const
{
  return myParentPoly;
}

void HYDROGUI_Arrow::BoundingBox (Bnd_Box& theBndBox)
{
  //Nothing to change, we consider arrow as object with empty bounding box
}

void HYDROGUI_Arrow::Compute( const Handle(PrsMgr_PresentationManager3d)& aPresentationManager,
				                      const Handle(Prs3d_Presentation)& aPrs,
				                      const Standard_Integer aMode )
{
  aPrs->Clear();
  if( myType==None )
    return;

  TopoDS_Edge anEdge = TopoDS::Edge( myshape );
  if( anEdge.IsNull() )
    return;

  BRepAdaptor_Curve anAdaptor( anEdge );
  double curveLen = GCPnts_AbscissaPoint::Length( anAdaptor, anAdaptor.FirstParameter(), anAdaptor.LastParameter() );
  double arrowLen;
  if( mySize==0 )
    // if size==0, then the arrow length is proportional to curve length
    arrowLen = curveLen/10;
  else
  {
    //arrowLen = qMin( curveLen/10, (double)mySize );
    arrowLen = (double)mySize;
  }

  double t = ( anAdaptor.FirstParameter() + anAdaptor.LastParameter() ) / 2;
  gp_Pnt P, P1;
  gp_Vec V;
  anAdaptor.D1( t, P, V );

  bool isFixed = mySize>0;

  if( isFixed )
  {
    gp_Trsf tr;
    tr.SetTranslation( -gp_Vec( gp_Pnt(), P ) );
    aPrs->SetTransformation( new TopLoc_Datum3D( tr ) );

    Handle(Graphic3d_TransformPers) tp = new Graphic3d_TransformPers( Graphic3d_TMF_ZoomPers, P );
    SetTransformPersistence( tp );

    P1 = gp_Pnt();
  }
  else
  {
    aPrs->SetTransformation( Handle(TopLoc_Datum3D)() );
    SetTransformPersistence( Handle(Graphic3d_TransformPers)() );
    P1 = P;
  }

  Handle(Graphic3d_Group) aGroup = Prs3d_Root::CurrentGroup( aPrs );
  Quantity_Color aColor = Attributes()->LineAspect()->Aspect()->Color();
  Aspect_TypeOfLine aType = Attributes()->LineAspect()->Aspect()->Type();
  Standard_Real anWidth = Attributes()->LineAspect()->Aspect()->Width();
  anWidth = 1;

  const Handle(HYDROGUI_Polyline)& aParentPoly = getParentPolyline();
  aParentPoly->GetColorOfSubShape(anEdge, aColor);

  if( myType==Cone )
  {
    Handle(Graphic3d_AspectLine3d) anAspect = new Graphic3d_AspectLine3d( aColor, aType, anWidth );
    aGroup->SetPrimitivesAspect( anAspect );

    Prs3d_Arrow::Draw( aGroup, P1, V, M_PI/180.*12., arrowLen );
  }
  else
  {
    Graphic3d_MaterialAspect m( Graphic3d_NOM_PLASTIC );
    Handle(Graphic3d_AspectFillArea3d) anAspect = new Graphic3d_AspectFillArea3d
      ( Aspect_IS_SOLID, aColor, aColor, Aspect_TOL_SOLID, 1, m, m );
    aGroup->SetPrimitivesAspect( anAspect );

    gp_Vec d = -V.Normalized() * arrowLen;
    const double k = 5.0;
    gp_Vec n( -d.Y()/k, d.X()/k, 0 );

    gp_Pnt Pa = P1.Translated( d );
    gp_Pnt P2 = Pa.Translated( n );
    gp_Pnt P3 = Pa.Translated( -n );

    Handle(Graphic3d_ArrayOfTriangles) prim = new Graphic3d_ArrayOfTriangles(3);
    prim->AddVertex( P1 );
    prim->AddVertex( P2 );
    prim->AddVertex( P3 );

    aGroup->AddPrimitiveArray( prim );
  }
}

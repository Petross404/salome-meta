// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_GeoreferencementOp.h"

#include "HYDROGUI_GeoreferencementDlg.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Profile.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Entity.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ViewWindow.h>

#include <OCCViewer_ViewManager.h>

#include <gp_XY.hxx>

HYDROGUI_GeoreferencementOp::HYDROGUI_GeoreferencementOp( HYDROGUI_Module* theModule, const int theInitialMode )
: HYDROGUI_Operation( theModule ),
  myInitialMode( theInitialMode )
{
  setName( tr( "PROFILES_GEOREFERENCEMENT" ) );
}

HYDROGUI_GeoreferencementOp::~HYDROGUI_GeoreferencementOp()
{
}

void HYDROGUI_GeoreferencementOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_GeoreferencementDlg* aPanel = 
    ::qobject_cast<HYDROGUI_GeoreferencementDlg*>( inputPanel() );
  if ( !aPanel ) {
    return;
  }

  aPanel->reset();
  setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                       module()->getApp()->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  setCursor();

  if ( myInitialMode == All ) {
    onModeActivated( HYDROGUI_GeoreferencementDlg::AllProfiles );
  } else if ( myInitialMode == Selected ) {
    onModeActivated( HYDROGUI_GeoreferencementDlg::SelectedProfiles );
  }

  LightApp_Application* anApp = module()->getApp();
  OCCViewer_ViewManager* aViewManager =
    dynamic_cast<OCCViewer_ViewManager*>( anApp->getViewManager( OCCViewer_Viewer::Type(), false ) );
  if ( aViewManager ) {
    connect( aViewManager, SIGNAL( mousePress( SUIT_ViewWindow*, QMouseEvent* ) ),
             aPanel, SLOT( onMousePress( SUIT_ViewWindow*, QMouseEvent* ) ), 
             Qt::UniqueConnection );
  }

  connect( anApp->desktop(), SIGNAL( windowActivated( SUIT_ViewWindow* ) ),
           this, SLOT( onWindowActivated( SUIT_ViewWindow* ) ), Qt::UniqueConnection );
}

void HYDROGUI_GeoreferencementOp::abortOperation()
{
  LightApp_Application* anApp = module()->getApp();
  if ( anApp && anApp->desktop() ) {
    anApp->desktop()->disconnect( this );
  }

  restoreCursor();
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_GeoreferencementOp::commitOperation()
{
  LightApp_Application* anApp = module()->getApp();
  if ( anApp && anApp->desktop() ) {
    anApp->desktop()->disconnect( this );
  }

  restoreCursor();
  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_GeoreferencementOp::createInputPanel() const
{
  HYDROGUI_InputPanel* aPanel = new HYDROGUI_GeoreferencementDlg( module(), getName() );
  connect( aPanel, SIGNAL( modeActivated( int ) ), SLOT( onModeActivated( int ) ) );
  connect( aPanel, SIGNAL( updateSelection() ), SLOT( onUpdateSelection() ) );

  return aPanel;
}

bool HYDROGUI_GeoreferencementOp::processApply( int& theUpdateFlags,
                                                QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries )
{
  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init;

  return store( theErrorMsg );
}

void HYDROGUI_GeoreferencementOp::onModeActivated( const int theActualMode )
{
  QString anErrorMsg;

  // Get the panel
  HYDROGUI_GeoreferencementDlg* aPanel = 
    ::qobject_cast<HYDROGUI_GeoreferencementDlg*>( inputPanel() );
  if ( !aPanel ) {
    return;
  }

  aPanel->setMode( theActualMode == HYDROGUI_GeoreferencementDlg::AllProfiles ?
                   HYDROGUI_GeoreferencementDlg::SelectedProfiles :
                   HYDROGUI_GeoreferencementDlg::AllProfiles);

  // Store the dialog data to the data model
  bool isToStore = false;
  // Check if modifications exists
  if ( aPanel->isModified() ) {
    // Show confirmation dialog
    SUIT_MessageBox::StandardButtons aButtons = 
      SUIT_MessageBox::Yes | SUIT_MessageBox::No;

    isToStore = SUIT_MessageBox::question( module()->getApp()->desktop(),
                                           tr( "CONFIRMATION" ),
                                           tr( "CONFIRM_STORE" ),
                                           aButtons, 
                                           SUIT_MessageBox::Yes) == SUIT_MessageBox::Yes;
  }
  // Store modifications if needed
  if ( isToStore )
  {
    startDocOperation();
    if ( !store( anErrorMsg ) )
    {
      abortDocOperation();

      anErrorMsg.append( "\n" + tr( "INPUT_VALID_DATA" ) );
      SUIT_MessageBox::critical( module()->getApp()->desktop(),
                                 tr( "INSUFFICIENT_INPUT_DATA" ),
                                 anErrorMsg );
      return;
    }
    else
    {
      commitDocOperation();
    }
  }

  aPanel->setMode( theActualMode );

  // Get the list of profiles
  HYDROData_SequenceOfObjects aSeqOfProfiles;
  
  if( theActualMode == HYDROGUI_GeoreferencementDlg::AllProfiles ) {
    HYDROData_Iterator aProfilesIter( doc(), KIND_PROFILE );
    while ( aProfilesIter.More() ) {
      aSeqOfProfiles.Append( aProfilesIter.Current() );
      aProfilesIter.Next();
    }
  } else if ( theActualMode == HYDROGUI_GeoreferencementDlg::SelectedProfiles ) {
    aSeqOfProfiles = HYDROGUI_Tool::GetSelectedObjects( module() );
  }

  // Set the profiles to the panel
  setPanelData( aSeqOfProfiles );
}

void HYDROGUI_GeoreferencementOp::onWindowActivated( SUIT_ViewWindow* theViewWindow )
{
  if ( !theViewWindow ) {
    return;
  }

  OCCViewer_ViewManager* aViewManager =
    dynamic_cast<OCCViewer_ViewManager*>( theViewWindow->getViewManager() );

  if ( aViewManager ) {
    HYDROGUI_GeoreferencementDlg* aPanel = 
      ::qobject_cast<HYDROGUI_GeoreferencementDlg*>( inputPanel() );
    if ( aPanel ) {
      connect( aViewManager, SIGNAL( mousePress( SUIT_ViewWindow*, QMouseEvent* ) ),
               aPanel, SLOT( onMousePress( SUIT_ViewWindow*, QMouseEvent* ) ),
               Qt::UniqueConnection);
    }
  }
}

bool HYDROGUI_GeoreferencementOp::store( QString& theErrorMsg )
{
  // Clear the error string
  theErrorMsg.clear();
  
  HYDROGUI_Module* aModule = module();

  // Get the panel
  HYDROGUI_GeoreferencementDlg* aPanel = 
    ::qobject_cast<HYDROGUI_GeoreferencementDlg*>( inputPanel() );
  if ( !aPanel ) {
    return false;
  }

  // Get georeferencement data from the panel
  HYDROGUI_GeoreferencementDlg::ProfilesGeoDataList aGeoDataList;
  aPanel->getData( aGeoDataList );
  if ( aGeoDataList.empty() ) {
    return true;
  }

  // Check the data validity
  foreach ( const HYDROGUI_GeoreferencementDlg::ProfileGeoData& aGeoData, aGeoDataList ) {
    if ( aGeoData.isIncomplete ) {
      theErrorMsg = tr( "INCOMPLETE_DATA" ).arg( aGeoData.Name );
      return false;
    }
  }

  // Store the data in the data model
  foreach ( const HYDROGUI_GeoreferencementDlg::ProfileGeoData& aGeoData, aGeoDataList ) {
    Handle(HYDROData_Profile) aProfile = 
      Handle(HYDROData_Profile)::DownCast(
        HYDROGUI_Tool::FindObjectByName( module(), aGeoData.Name, KIND_PROFILE ) );
    if ( !aProfile.IsNull() ) {
      if ( !aGeoData.isEmpty ) {
        if ( !aProfile->IsValid() ) // Show the profile after it became valid
          aModule->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( aModule ), aProfile, true );

        aProfile->SetLeftPoint( gp_XY( aGeoData.Xg, aGeoData.Yg ), false );
        aProfile->SetRightPoint( gp_XY( aGeoData.Xd, aGeoData.Yd ), false );
      } else {
        aProfile->Invalidate();
        aModule->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( aModule ), aProfile, false );
      }

      aProfile->Update();
    }
  }

  aModule->update( UF_Model | UF_OCCViewer | UF_OCC_Forced );

  return true;
}

void HYDROGUI_GeoreferencementOp::onUpdateSelection()
{
  // Get the selected profiles
  HYDROData_SequenceOfObjects aSeqOfProfiles = 
    HYDROGUI_Tool::GetSelectedObjects( module() );

  // Set them to the dialog
  setPanelData( aSeqOfProfiles );
}

void HYDROGUI_GeoreferencementOp::setPanelData( 
  const HYDROData_SequenceOfObjects& theProfiles )
{
  // Get the panel
  HYDROGUI_GeoreferencementDlg* aPanel = 
    ::qobject_cast<HYDROGUI_GeoreferencementDlg*>( inputPanel() );
  if ( !aPanel ) {
    return;
  }

  // Get georeferencement data from the data model
  HYDROGUI_GeoreferencementDlg::ProfilesGeoDataList aData;

  HYDROData_SequenceOfObjects::Iterator anIter( theProfiles );
  for ( ; anIter.More(); anIter.Next() ) {
    Handle(HYDROData_Profile) aProfile =
        Handle(HYDROData_Profile)::DownCast( anIter.Value() );
    if ( aProfile.IsNull() ) {
      continue;
    }

    HYDROGUI_GeoreferencementDlg::ProfileGeoData aGeoData( aProfile->GetName() );

    gp_XY aFirstPoint, aLastPoint;
    if ( aProfile->GetLeftPoint( aFirstPoint, false ) &&
         aProfile->GetRightPoint( aLastPoint, false ) ) {
      aGeoData = 
        HYDROGUI_GeoreferencementDlg::ProfileGeoData( aGeoData.Name, 
                                                      aFirstPoint.X(), aFirstPoint.Y(),
                                                      aLastPoint.X(), aLastPoint.Y() );
    }
   
    aData.append( aGeoData );
  }

  // Set the collected data to the dialog
  aPanel->setData( aData );
}
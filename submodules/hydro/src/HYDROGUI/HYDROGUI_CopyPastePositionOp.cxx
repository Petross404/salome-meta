// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_CopyPastePositionOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_Displayer.h"
#include "HYDROGUI_Tool.h"

#include <SUIT_ViewManager.h>
#include <LightApp_Application.h>

#include <QClipboard>
#include <QApplication>

HYDROGUI_CopyPastePositionOp::HYDROGUI_CopyPastePositionOp( HYDROGUI_Module* theModule,
                                                            const bool theIsPaste )
: HYDROGUI_Operation( theModule ),
  myIsPaste( theIsPaste )
{
  setName( tr( "COPY_PASTE_VIEW_POSITION" ) );
}

HYDROGUI_CopyPastePositionOp::~HYDROGUI_CopyPastePositionOp()
{
}

void HYDROGUI_CopyPastePositionOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  if( !myIsPaste )
  {
    QString aResult;
    HYDROGUI_Module* aModule = module();
    HYDROGUI_Displayer* aDisplayer = aModule->getDisplayer();
    if ( aDisplayer ) {
      SUIT_ViewManager* aViewMgr = aModule->getApp()->activeViewManager();
      SUIT_ViewWindow* aViewWindow = aViewMgr ? aViewMgr->getActiveView() : 0;
      double aX, aY, aZ;
      if ( aDisplayer->GetCursorViewCoordinates( aViewWindow, aX, aY, aZ ) ) {
        QString aXStr = HYDROGUI_Tool::GetCoordinateString( aX, false );
        QString anYStr = HYDROGUI_Tool::GetCoordinateString( aY, false );
        aResult = tr( "%1,%2" ).arg( aXStr ).arg( anYStr );
      }
    }
    if ( !aResult.isEmpty() ) {
      QClipboard* aClBoard = QApplication::clipboard();
      aClBoard->clear();
      QApplication::clipboard()->setText( aResult );
    }
  }
  commit();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_InputPanel.h>
#ifndef LIGHT_MODE
  #include <HYDROGUI_Module.h>
  #include <CAM_Application.h>
  #include <SUIT_Desktop.h>
#endif

#include <QFrame>
#include <QLayout>
#include <QPushButton>
#include <QDir>
#include <QSplitter>

HYDROGUI_InputPanel::HYDROGUI_InputPanel( HYDROGUI_Module* theModule, const QString& theTitle,
                                          bool doInitLayout, bool isSplitter )
#ifdef LIGHT_MODE
: QDockWidget( 0 ),
#else
: QDockWidget( theModule->application()->desktop() ),
#endif
  myModule( theModule )
{
  setFloating( false );
  setWindowTitle( theTitle );
  setAllowedAreas( Qt::RightDockWidgetArea );


  QFrame* aFrame = new QFrame( this );
  setWidget( aFrame );
  QVBoxLayout* aLayout = new QVBoxLayout( aFrame );
  
  myMainFrame = new QFrame( aFrame );
  QBoxLayout* aMainLayout = new QVBoxLayout( myMainFrame );
  aMainLayout->setMargin( 0 );
  aMainLayout->setSpacing( 5 );
  if( isSplitter )
  {
    mySplitter = new QSplitter( myMainFrame );
    mySplitter->setOrientation( Qt::Vertical );
    aMainLayout->addWidget( mySplitter, 1 );
  }
  else
    mySplitter = 0;
    
  aLayout->addWidget( myMainFrame, 1 );

  myBtnFrame = new QFrame( aFrame );
  aLayout->addWidget( myBtnFrame, 0 );

  QHBoxLayout* aBtnsLayout = new QHBoxLayout( myBtnFrame );
  aBtnsLayout->setMargin( 5 );
  aBtnsLayout->setSpacing( 5 );

  myCancel = new QPushButton( tr( "CLOSE" ), myBtnFrame );
  myHelp = new QPushButton( tr( "HELP" ), myBtnFrame );

  if ( doInitLayout ) {
    myApplyAndClose = new QPushButton( tr( "APPLY_AND_CLOSE" ), myBtnFrame );
    myApply = new QPushButton( tr( "APPLY" ), myBtnFrame );

    aBtnsLayout->addWidget( myApplyAndClose, 0 );
    aBtnsLayout->addWidget( myApply, 0 );
    aBtnsLayout->addWidget( myCancel, 0 );
    aBtnsLayout->addStretch( 1 );
    aBtnsLayout->addWidget( myHelp, 0 );

    connect( myApplyAndClose,  SIGNAL( clicked() ), this, SLOT( onApplyAndClose()  ) );
    connect( myApply,  SIGNAL( clicked() ), this, SLOT( onApply()  ) );
  }
  connect( myCancel, SIGNAL( clicked() ), this, SLOT( onCancel() ) );
  connect( myHelp,   SIGNAL( clicked() ), this, SLOT( onHelp()   ) );
}

HYDROGUI_InputPanel::~HYDROGUI_InputPanel()
{
}

HYDROGUI_Module* HYDROGUI_InputPanel::module() const
{
  return myModule;
}

bool HYDROGUI_InputPanel::isApplyEnabled() const
{
  return myApply->isEnabled();
}

void HYDROGUI_InputPanel::setApplyEnabled( bool on )
{
  myApplyAndClose->setEnabled( on );
  myApply->setEnabled( on );
}

void HYDROGUI_InputPanel::onApplyAndClose()
{
  emit panelApplyAndClose();
}

void HYDROGUI_InputPanel::onApply()
{
  emit panelApply(); 
}

void HYDROGUI_InputPanel::onCancel()
{
  emit panelCancel();
}

void HYDROGUI_InputPanel::onHelp()
{
  emit panelHelp();
}

void HYDROGUI_InputPanel::closeEvent ( QCloseEvent * event )
{
  emit panelCancel();
}

void HYDROGUI_InputPanel::insertWidget( QWidget* theWidget, int theIndex, int theStretch )
{
  if( mySplitter )
  {
    mySplitter->insertWidget( theIndex, theWidget );
    mySplitter->setStretchFactor( theIndex, theStretch );
  }
  else
  {
    QBoxLayout* aMainLayout = dynamic_cast<QBoxLayout*>( myMainFrame->layout() );
    aMainLayout->insertWidget( theIndex, theWidget, theStretch );
  }
}

void HYDROGUI_InputPanel::addWidget( QWidget* theWidget, int theStretch )
{
  if( mySplitter )
  {
    int s = mySplitter->count();
    mySplitter->addWidget( theWidget );
    mySplitter->setStretchFactor( s, theStretch );
  }
  else
  {
    QBoxLayout* aMainLayout = dynamic_cast<QBoxLayout*>( myMainFrame->layout() );
    aMainLayout->addWidget( theWidget, theStretch );
  }
}

void HYDROGUI_InputPanel::addLayout( QLayout* theLayout, int theStretch )
{
  if( mySplitter )
  {
  }
  else
  {
    QBoxLayout* aMainLayout = dynamic_cast<QBoxLayout*>( myMainFrame->layout() );
    aMainLayout->addLayout( theLayout, theStretch );
  }
}

void HYDROGUI_InputPanel::addStretch()
{
  if( mySplitter )
  {
  }
  else
  {
    QBoxLayout* aMainLayout = dynamic_cast<QBoxLayout*>( myMainFrame->layout() );
    aMainLayout->addStretch();
  }
}

QFrame* HYDROGUI_InputPanel::mainFrame() const
{
  return myMainFrame;
}

QFrame* HYDROGUI_InputPanel::buttonFrame() const
{
  return myBtnFrame;
}

QSplitter* HYDROGUI_InputPanel::splitter() const
{
  return mySplitter;
}

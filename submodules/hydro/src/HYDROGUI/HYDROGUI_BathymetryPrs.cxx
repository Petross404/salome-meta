// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_BathymetryPrs.h>
#include <HYDROGUI_ShapeBathymetry.h>
#include <HYDROGUI_Tool.h>

#include <Prs3d_Root.hxx>
#include <Prs3d_LineAspect.hxx>
#include <Prs3d_Text.hxx>
#include <Prs3d_TextAspect.hxx>
#include <Select3D_SensitiveBox.hxx>
#include <Select3D_SensitivePoint.hxx>
#include <Graphic3d_ArrayOfPolylines.hxx>
#include <Graphic3d_ArrayOfPoints.hxx>
#include <Image_PixMap.hxx>
#include <Graphic3d_MarkerImage.hxx>
#include <TColStd_HPackedMapOfInteger.hxx>
#include <TColStd_PackedMapOfInteger.hxx>
#include <unistd.h>

#include <QImage>
#include <QVector>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

const int BATH_HIGHLIGHT_MODE = 10;

HYDROGUI_BathymetryPrs::HYDROGUI_BathymetryPrs( const HYDROGUI_ShapeBathymetry* theShape )
  : myShape( theShape )
{
  DEBTRACE("HYDROGUI_BathymetryPrs");
  SetHilightMode( BATH_HIGHLIGHT_MODE );
  SetAutoHilight( Standard_True );
}

HYDROGUI_BathymetryPrs::~HYDROGUI_BathymetryPrs()
{
}

HYDROGUI_ShapeBathymetry* HYDROGUI_BathymetryPrs::GetShape() const
{
  return const_cast<HYDROGUI_ShapeBathymetry*>( myShape );
}

void HYDROGUI_BathymetryPrs::UpdateBound()
{
  DEBTRACE("UpdateBound");
  Handle(Graphic3d_ArrayOfPoints) points = GetPoints();
  myBound.SetVoid();
  if( !points.IsNull() )
  {
    int aLower = 1;
    int anUpper = points->VertexNumber();

    for( int i = aLower; i <= anUpper; i++ )
    {
      gp_Pnt p = points->Vertice( i );
      if( i==aLower )
        myBound.Set( p );
      else
        myBound.Update( p.X(), p.Y(), p.Z() );
    }
  }
}

void HYDROGUI_BathymetryPrs::SetPoints( const Handle(TColgp_HArray1OfPnt)&     theCoords,
                                        const Handle(Quantity_HArray1OfColor)& theColors,
                                        const Handle(TColgp_HArray1OfDir)&     theNormals )
{
  AIS_PointCloud::SetPoints( theCoords, theColors, theNormals );
  //UpdateBound();
}

void HYDROGUI_BathymetryPrs::Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                                      const Handle(Prs3d_Presentation)& thePresentation,
                                      const Standard_Integer theMode )
{
  DEBTRACE("Compute " << theMode);
//  thePresentation->Clear();
//  Handle(Graphic3d_Group) aGroup = Prs3d_Root::CurrentGroup (thePresentation);

  if( theMode== BATH_HIGHLIGHT_MODE ) // highlight ==> draw bounding box
  {
    AIS_PointCloud::Compute( thePresentationManager, thePresentation, AIS_PointCloud::DM_BndBox );
  }
  else if (theMode == 0)
  {
    DEBTRACE("non highlight mode");  // display the nodes as points with color (HYDROGUI_ShapeBathymetry::UpdateWithColorScale)
    AIS_PointCloud::Compute( thePresentationManager, thePresentation, AIS_PointCloud::DM_Points );

    QImage qtMark( 4, 4, QImage::Format_RGB888 ); // points marks as 4x4 rectangles
    Handle(Image_PixMap) aMark = HYDROGUI_Tool::Pixmap( qtMark );
    DEBTRACE("pnt dims " << aMark->SizeX() << " " << aMark->SizeY());
    Handle(Graphic3d_MarkerImage) gMark = new Graphic3d_MarkerImage(aMark);
    Handle(Graphic3d_AspectMarker3d) anAspect = new Graphic3d_AspectMarker3d( Aspect_TOM_USERDEFINED, Quantity_NOC_WHITE, 1.0 );
    anAspect->SetMarkerImage(gMark);

    DEBTRACE("nb groups " << thePresentation->NumberOfGroups()); // find the group corresponding to the points, change the aspect
    const Graphic3d_SequenceOfGroup& grps = thePresentation->Groups();
    Graphic3d_SequenceOfGroup::Iterator anIter( grps );
    for ( ; anIter.More(); anIter.Next() )
    {
      Handle(Graphic3d_Group) aGrp = anIter.Value();
      DEBTRACE("grp type: " << aGrp->get_type_name());
      Handle(Graphic3d_Aspects) asp = aGrp->Aspects();
      if (!asp.IsNull())
        {
          DEBTRACE("aspect: " << asp->get_type_name());
          DEBTRACE("asp->LineType() " << asp->LineType());
          if (asp->LineType() != Aspect_TOL_DASH)       // find the group which does not correspond to the bounding box
            aGrp->SetGroupPrimitivesAspect( anAspect );
        }
    }

    Handle(Graphic3d_ArrayOfPoints) points = GetPoints();
    if (!points.IsNull())
      DEBTRACE("nb points " << points->ItemNumber());

    if( !myTextIndices.empty() && !points.IsNull() ) // points selected ==> altitude labels
    {
      DEBTRACE("myTextIndices.size() " << myTextIndices.size());
      char aBuf[1024];
      Handle(Prs3d_TextAspect) anAspect = new Prs3d_TextAspect();

      Handle(Graphic3d_Group) aGroup = Prs3d_Root::CurrentGroup (thePresentation);
      foreach( int index, myTextIndices )
      {
        gp_Pnt p = points->Vertice( index );
        sprintf( aBuf, "%.2f", p.Z() );
        Prs3d_Text::Draw( aGroup, anAspect, aBuf, p );
      }
    }
  }
  else
    DEBTRACE("Do nothing");
}

void HYDROGUI_BathymetryPrs::ComputeSelection( const Handle(SelectMgr_Selection)& theSelection,
                                               const Standard_Integer theMode )
{
  DEBTRACE("ComputeSelection " << theMode);
  switch (theMode)
  {
    case 0:
    {
      DEBTRACE("Points selection");
      AIS_PointCloud::ComputeSelection( theSelection, SM_Points);
      break;
    }
    case 1:
    {
      DEBTRACE("Subset of points detection");
      AIS_PointCloud::ComputeSelection( theSelection, SM_SubsetOfPoints );
      break;
    }
    default:
    {
      DEBTRACE("Bounding box selection");
      AIS_PointCloud::ComputeSelection( theSelection, SM_BndBox);
      break;
    }
  }

  if( theMode == 1 )
  {
    Handle(Graphic3d_ArrayOfPoints) points = GetPoints();
    int n = points.IsNull() ? 0 : points->VertexNumber();
    for( int i=1; i<=n; i++ )
    {
      gp_Pnt p = points->Vertice( i );
      Handle(HYDROGUI_BathymetryPointOwner) anOwner = new HYDROGUI_BathymetryPointOwner( this, i );
      Handle(Select3D_SensitivePoint) aSensitivePoint = new Select3D_SensitivePoint( anOwner, p );
      theSelection->Add( aSensitivePoint );
    }
  }
}

int HYDROGUI_BathymetryPrs::NbPoints() const
{
  Handle(Graphic3d_ArrayOfPoints) points = GetPoints();
  int n = points.IsNull() ? 0 : points->VertexNumber();
  return n;
}

gp_Pnt HYDROGUI_BathymetryPrs::GetPoint( int theIndex ) const
{
  Handle(Graphic3d_ArrayOfPoints) points = GetPoints();
  if( points.IsNull() )
    return gp_Pnt();
  else
    return points->Vertice( theIndex );
}

void HYDROGUI_BathymetryPrs::AddPoint( const Handle(Graphic3d_ArrayOfPoints)& thePoints,
                                       const Handle(SelectMgr_EntityOwner)& theOwner )
{
  DEBTRACE("AddPoint");
  Handle(HYDROGUI_BathymetryPointOwner) anOwner = Handle(HYDROGUI_BathymetryPointOwner)::DownCast( theOwner );
  if( anOwner.IsNull() )
    return;
  DEBTRACE("index " << anOwner->GetIndex());
  gp_Pnt p = GetPoint( anOwner->GetIndex() );
  DEBTRACE("p: " << p.X() << " " << p.Y() << " " << p.Z());
  thePoints->AddVertex( p );
}

void HYDROGUI_BathymetryPrs::HilightOwnerWithColor( const Handle(PrsMgr_PresentationManager3d)& thePM,
                                                    const Handle(Prs3d_Drawer)& theColor,
                                                    const Handle(SelectMgr_EntityOwner)& theOwner )
{
  DEBTRACE("HilightOwnerWithColor"); // appel si AIS_PointCloud::ComputeSelection( theSelection, SM_Points);
  Handle(AIS_PointCloudOwner) anOwner = Handle(AIS_PointCloudOwner)::DownCast( theOwner );
  if (anOwner.IsNull())
    return;
  DEBTRACE("---"); // jamais appelé : on n'a pas de AIS_PointCloudOwner
}

void HYDROGUI_BathymetryPrs::HilightSelected( const Handle(PrsMgr_PresentationManager3d)& thePM,
                                              const SelectMgr_SequenceOfOwner& theOwners )
{
  DEBTRACE("HilightSelected " << theOwners.Size()); // appel si AIS_PointCloud::ComputeSelection( theSelection, SM_SubsetOfPoints );
  //AIS_PointCloud::HilightSelected(thePM, theOwners); // Non, fige la machine !
  if (theOwners.Size() == 0)
    return;
  Handle(SelectMgr_EntityOwner) owner = theOwners.First();
  Handle(AIS_PointCloudOwner) anOwner = Handle(AIS_PointCloudOwner)::DownCast( owner );
  if (!anOwner.IsNull())
  {
    DEBTRACE("immediateMode: " << thePM->IsImmediateModeOn()); // semble sans effet
    const Handle(TColStd_HPackedMapOfInteger)& aSMap = anOwner->SelectedPoints();
    DEBTRACE("selectedPoints.Extent() :"  << aSMap->Map().Extent());
    const Handle(TColStd_HPackedMapOfInteger)& aDMap = anOwner->DetectedPoints();
    DEBTRACE("detectedPoints.Extent() :"  << aDMap->Map().Extent());
  }

  Handle(Prs3d_Presentation) aSelectPrs = GetSelectPresentation( thePM );
  aSelectPrs->SetZLayer( Graphic3d_ZLayerId_Topmost );
  aSelectPrs->Clear();

  //mySelectedPoints.clear();
  mySelectedPoints.reserve(theOwners.Size());
  Handle(Graphic3d_ArrayOfPoints) points = new Graphic3d_ArrayOfPoints( theOwners.Size() );
  for( int i=theOwners.Lower(); i<=theOwners.Upper(); i++ )
    {
    AddPoint( points, theOwners.Value( i ) );
    Handle(HYDROGUI_BathymetryPointOwner) anOwner = Handle(HYDROGUI_BathymetryPointOwner)::DownCast( theOwners.Value( i ) );
    if (!anOwner.IsNull())
      mySelectedPoints.append(anOwner->GetIndex());
    }
  DEBTRACE("points selected " << points->ItemNumber());
  Handle(Graphic3d_Group) aGroup = Prs3d_Root::CurrentGroup( aSelectPrs );
  //Handle(Graphic3d_Group) aGroup = aSelectPrs->NewGroup();
  Handle(Graphic3d_AspectMarker3d) anAspect = new Graphic3d_AspectMarker3d( Aspect_TOM_X, Quantity_NOC_WHITE, 1.0 );
  aGroup->SetGroupPrimitivesAspect( anAspect );
  aGroup->AddPrimitiveArray( points );

  aSelectPrs->SetDisplayPriority(5);
  aSelectPrs->Display();
  //sleep(5);
}

void HYDROGUI_BathymetryPrs::ClearSelected()
{
  DEBTRACE("ClearSelected");
  Handle(Prs3d_Presentation) aSelectPrs = GetSelectPresentation( NULL );  
  if( !aSelectPrs.IsNull() )
    aSelectPrs->Clear(); 
  //mySelectedPoints.clear();
}

void HYDROGUI_BathymetryPrs::SetTextLabels( const QVector<int>& theTextIndices )
{
  DEBTRACE("SetTextLabels");
  myTextIndices = theTextIndices;
}


HYDROGUI_BathymetryPointOwner::HYDROGUI_BathymetryPointOwner
  ( const Handle(HYDROGUI_BathymetryPrs)& theBathymetry, int theIndex )
  : SelectMgr_EntityOwner( Handle(SelectMgr_SelectableObject)::DownCast( theBathymetry ), 0 ),
    myIndex( theIndex )
{
}

HYDROGUI_BathymetryPointOwner::~HYDROGUI_BathymetryPointOwner()
{
}

Standard_Boolean HYDROGUI_BathymetryPointOwner::IsAutoHilight() const
{
  return Standard_False;
}

int HYDROGUI_BathymetryPointOwner::GetIndex() const
{
  return myIndex;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_TOOL_H
#define HYDROGUI_TOOL_H

#include <HYDROData_Entity.h>
#include <QString>

class TCollection_AsciiString;
class HYDROData_Document;
class Image_PixMap;
class TCollection_HAsciiString;
class TCollection_HExtendedString;
class QDockWidget;
class QImage;
class QWidget;

namespace HYDROGUI_Tool
{
  QString ToQString( const TCollection_AsciiString& );
  QString ToQString( const TCollection_ExtendedString& );
  QString ToQString( const Handle(TCollection_HAsciiString)& );
  QString ToQString( const Handle(TCollection_HExtendedString)& );
  TCollection_AsciiString ToAsciiString( const QString& );
  TCollection_ExtendedString ToExtString( const QString& );
  Handle(TCollection_HAsciiString) ToHAsciiString( const QString& );
  Handle(TCollection_HExtendedString) ToHExtString( const QString& );

  QString GetTempDir( const bool theToCreate );

  QDockWidget* WindowDock( QWidget* wid );

  QStringList FindExistingObjectsNames( const Handle(HYDROData_Document)& theDoc,
                                        const ObjectKind theObjectKind,
                                        bool isCheckValidProfile = false );
  QString GetCoordinateString( const double theNumber, bool isInLocale );

  Handle(Image_PixMap) Pixmap( const QImage& theImage );

  void GetObjectReferences( const Handle(HYDROData_Entity)& theObj,
                            HYDROData_SequenceOfObjects& theRefObjects,
                            QStringList& theRefNames );
  HYDROData_SequenceOfObjects GetObjectBackReferences( const Handle(HYDROData_Entity)& theObj );
  QMap<QString,HYDROData_SequenceOfObjects> GetObjectsBackReferences
    ( const Handle(HYDROData_Document)& theDocument, const QStringList& theObjectNames );
};

#endif

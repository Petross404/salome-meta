// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ProfileInterpolateDlg.h"

#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_ObjComboBox.h"
#include "HYDROGUI_OCCSelector.h"

#include <OCCViewer_ViewPort3d.h>
#include <OCCViewer_Utilities.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewFrame.h>
#include <LightApp_Application.h>

#include <SUIT_Tools.h>
#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QGroupBox>
#include <QComboBox>
#include <QGridLayout>

#include <HYDROData_Stream.h>
#include <HYDROData_Profile.h>

HYDROGUI_ProfileInterpolateDlg::HYDROGUI_ProfileInterpolateDlg( HYDROGUI_Module* theModule, const QString& theTitle )
    : HYDROGUI_ViewerDlg( theModule, theTitle, false )
{
    QWidget* main = new QGroupBox( mainFrame() );
    QGridLayout* base = new QGridLayout( main );

    addWidget( main );

    base->addWidget( new QLabel( tr( "STREAM_OBJECT" ), main ), 0, 0, 1, 1 );
    base->addWidget( myRiver = new HYDROGUI_ObjComboBox( theModule, QString::null, KIND_STREAM, main ), 0, 1, 1, 1 );

    base->addWidget( new QLabel( tr( "INTERPOLATOR" ), main ), 1, 0, 1, 1 );
    base->addWidget( myIntrp = new QComboBox( main ), 1, 1, 1, 1 );

    base->addWidget( new QLabel( tr( "DESCRIPTION" ), main ), 2, 0, 1, 1 );
    base->addWidget( myDescr = new QTextEdit( main ), 2, 1, 1, 1 );
    myDescr->setReadOnly( true );

    base->addWidget( new QLabel( tr( "PROFILE_1" ), main ), 3, 0, 1, 1 );
    base->addWidget( myProfileStart = new HYDROGUI_ObjComboBox( theModule, QString::null, KIND_PROFILE, main ), 3, 1, 1, 1 );
    myProfileStart->setObjectFilter( this );

    base->addWidget( new QLabel( tr( "PROFILE_2" ), main ), 4, 0, 1, 1 );
    base->addWidget( myProfileFinish = new HYDROGUI_ObjComboBox( theModule, QString::null, KIND_PROFILE, main ), 4, 1, 1, 1 );
    myProfileFinish->setObjectFilter( this );

    base->addWidget( new QLabel( tr( "NUMBER_OF_PROFILES" ), main ), 5, 0, 1, 1 );
    base->addWidget( myProfileNumber = new QSpinBox( main ), 5, 1, 1, 1 );
    myProfileNumber->setRange( 1, 1000 );

    base->addWidget( new QLabel( tr( "PARAMETERS" ), main ), 6, 0, 1, 1 );
    base->addWidget( myParams = new QLineEdit( main ), 6, 1, 1, 1 );

    connect( myRiver, SIGNAL( objectSelected( const QString& ) ), this, SLOT( onRiverChanged( const QString& ) ) );
    connect( myIntrp, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onInterpolatorIndexChanged( int ) ) );

    connect( myProfileStart, SIGNAL( objectSelected( const QString& ) ), this, SLOT( onProfileChanged( const QString& ) ) );
    connect( myProfileFinish, SIGNAL( objectSelected( const QString& ) ), this, SLOT( onProfileChanged( const QString& ) ) );

    connect( myProfileStart, SIGNAL( objectSelected( const QString& ) ), this, SIGNAL( profileStartChanged( const QString& ) ) );
    connect( myProfileFinish, SIGNAL( objectSelected( const QString& ) ), this, SIGNAL( profileFinishChanged( const QString& ) ) );

    connect( myProfileNumber, SIGNAL( valueChanged( int ) ), this, SIGNAL( profileNumberChanged( int ) ) );
    connect( myParams, SIGNAL( editingFinished() ), this, SLOT( onParametersEditingFinished() ) );

    new HYDROGUI_OCCSelector( module(), viewer(), selectionMgr() );

    updateState();
}

HYDROGUI_ProfileInterpolateDlg::~HYDROGUI_ProfileInterpolateDlg()
{
}

QString HYDROGUI_ProfileInterpolateDlg::interpolator() const
{
    return myIntrp->currentText();
}

void HYDROGUI_ProfileInterpolateDlg::setInterpolator( const QString& theInterp )
{
    int idx = myIntrp->findText( theInterp );
    if ( idx >= 0 )
        myIntrp->setCurrentIndex( idx );
}

void HYDROGUI_ProfileInterpolateDlg::setInterpolators( const QStringList& theList )
{
    myIntrp->clear();
    myIntrp->addItems( theList );
}

QString HYDROGUI_ProfileInterpolateDlg::interpolatorDescription() const
{
    return myDescr->toPlainText();
}

void HYDROGUI_ProfileInterpolateDlg::setInterpolatorDescription( const QString& theText )
{
    myDescr->setText( theText );
}

QString HYDROGUI_ProfileInterpolateDlg::interpolatorParameters() const
{
    return myParams->text();
}

void HYDROGUI_ProfileInterpolateDlg::setInterpolatorParameters( const QString& theLine )
{
    myParams->setText( theLine );
}

QString HYDROGUI_ProfileInterpolateDlg::river() const
{
    return myRiver->selectedObject();
}

void HYDROGUI_ProfileInterpolateDlg::setRiver( const QString& theName )
{
    myRiver->setSelectedObject( theName );
}

QString HYDROGUI_ProfileInterpolateDlg::profileStart() const
{
    return myProfileStart->selectedObject();
}

void HYDROGUI_ProfileInterpolateDlg::setProfileStart( const QString& theName )
{
    myProfileStart->setSelectedObject( theName );
}

QString HYDROGUI_ProfileInterpolateDlg::profileFinish() const
{
    return myProfileFinish->selectedObject();
}

void HYDROGUI_ProfileInterpolateDlg::setProfileFinish( const QString& theName )
{
    myProfileFinish->setSelectedObject( theName );
}

int HYDROGUI_ProfileInterpolateDlg::profileNumber() const
{
    return myProfileNumber->value();
}

void HYDROGUI_ProfileInterpolateDlg::setProfileNumber( int theNum )
{
    myProfileNumber->setValue( theNum );
}

bool HYDROGUI_ProfileInterpolateDlg::isActive( HYDROGUI_ObjComboBox* selector ) const
{
    return selector == activeProfile();
}

bool HYDROGUI_ProfileInterpolateDlg::isOk( const Handle(HYDROData_Entity)& theEntity ) const
{
    if ( theEntity.IsNull() )
        return false;

    bool res = true;
    if ( theEntity->GetKind() == KIND_PROFILE )
    {
        if ( myRiverProfiles.isEmpty() )
        {
            Handle(HYDROData_Stream) aStream = Handle(HYDROData_Stream)::DownCast( HYDROGUI_Tool::FindObjectByName( module(), river(), KIND_STREAM ) );
            if ( !aStream.IsNull() )
            {
                HYDROData_SequenceOfObjects aProfiles = aStream->GetProfiles();
                HYDROGUI_ProfileInterpolateDlg* that = (HYDROGUI_ProfileInterpolateDlg*)this;
                for ( int i = aProfiles.Lower(); i <= aProfiles.Upper(); i++ )
                    that->myRiverProfiles.insert( aProfiles.Value( i )->GetName() );
            }
        }
        res = myRiverProfiles.contains( theEntity->GetName() );
    }
    return res;
}

void HYDROGUI_ProfileInterpolateDlg::reset()
{
    myRiverProfiles.clear();

    myRiver->reset();
    myProfileStart->reset();
    myProfileFinish->reset();
    myParams->clear();
    myDescr->clear();
}

void HYDROGUI_ProfileInterpolateDlg::onParametersEditingFinished()
{
    emit interpolatorParametersChanged( myParams->text() );
}

void HYDROGUI_ProfileInterpolateDlg::onRiverChanged( const QString& theName )
{
    myRiverProfiles.clear();

    myProfileStart->reset();
    myProfileFinish->reset();

    updateState();

    emit riverChanged( theName );
}

void HYDROGUI_ProfileInterpolateDlg::onInterpolatorIndexChanged( int theIdx )
{
    emit interpolatorChanged( myIntrp->itemText( theIdx ) );
}

void HYDROGUI_ProfileInterpolateDlg::onProfileChanged( const QString& )
{
    updateState();
}

void HYDROGUI_ProfileInterpolateDlg::updateState()
{
    setApplyEnabled( !river().isEmpty() && !profileStart().isEmpty() && !profileFinish().isEmpty() && profileStart() != profileFinish() );
}

HYDROGUI_ObjComboBox* HYDROGUI_ProfileInterpolateDlg::activeProfile() const
{
    HYDROGUI_ObjComboBox* profile = myProfileFinish;
    if ( myProfileStart->selectedObject().isEmpty() )
        profile = myProfileStart;
    else if ( myProfileFinish->selectedObject().isEmpty() )
        profile = myProfileFinish;
    else if ( SUIT_Tools::isParent( myProfileStart->parentWidget()->focusWidget(), myProfileStart ) )
        profile = myProfileStart;
    else if ( SUIT_Tools::isParent( myProfileFinish->parentWidget()->focusWidget(), myProfileFinish ) )
        profile = myProfileFinish;
    return profile;
}

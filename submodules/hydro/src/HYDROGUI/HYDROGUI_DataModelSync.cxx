// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_DataModelSync.h>
#include <HYDROGUI_DataObject.h>
#include <typeinfo>

HYDROGUI_DataModelSync::HYDROGUI_DataModelSync( SUIT_DataObject* theRoot )
  : myRoot( theRoot )
{
}

HYDROGUI_DataModelSync::~HYDROGUI_DataModelSync()
{
}

void HYDROGUI_DataModelSync::deleteItemWithChildren( const suitPtr& theSuitPtr ) const
{
  if( !theSuitPtr )
    return;

  theSuitPtr->setAutoDeleteChildren( true );
  theSuitPtr->deleteLater();
}

suitPtr HYDROGUI_DataModelSync::nullSrc() const
{
  return 0;
}

suitPtr HYDROGUI_DataModelSync::nullTrg() const
{
  return 0;
}

QList<suitPtr> HYDROGUI_DataModelSync::children( const suitPtr& theSuitPtr ) const
{
  QList<suitPtr> aChildren;
  if( theSuitPtr )
  {
    DataObjectList anObjList;
    theSuitPtr->children( anObjList );
    foreach( SUIT_DataObject* anObj, anObjList )
      aChildren.append( anObj );
  }
  return aChildren;
}

suitPtr HYDROGUI_DataModelSync::parent( const suitPtr& theSuitPtr ) const
{
  return theSuitPtr ? theSuitPtr->parent() : 0;
}

void HYDROGUI_DataModelSync::updateItem( const suitPtr& theSrcPtr, const suitPtr& theTrgPtr ) const
{
  HYDROGUI_DataObject* aDataObj = dynamic_cast<HYDROGUI_DataObject*>( theTrgPtr );
  if( aDataObj )
    aDataObj->updateBy( theSrcPtr );

  HYDROGUI_NamedObject* aNamedObj = dynamic_cast<HYDROGUI_NamedObject*>( theTrgPtr );
  if( aNamedObj )
    aNamedObj->updateBy( theSrcPtr );
}

bool HYDROGUI_DataModelSync::isEqual( const suitPtr& theSrcPtr, const suitPtr& theTrgPtr ) const
{
  if( theSrcPtr==myRoot )
    return true;

  if( theSrcPtr==0 )
    return theTrgPtr==0;

  if( theTrgPtr==0 )
    return theSrcPtr==0;

  QString aSrcClass = typeid( *theSrcPtr ).name();
  QString aTrgClass = typeid( *theTrgPtr ).name();
  return aSrcClass==aTrgClass;
}

suitPtr HYDROGUI_DataModelSync::createItem( const suitPtr& theSrcPtr,
                                            const suitPtr& theParent,
                                            const suitPtr& theAfter ) const
{
  if( theParent )
  {
    int aPos = theParent->childPos( theAfter );
    if( aPos>=0 )
      theParent->insertChild( theSrcPtr, aPos+1 );
    else
      theParent->appendChild( theSrcPtr );
  }
  return theSrcPtr;
}


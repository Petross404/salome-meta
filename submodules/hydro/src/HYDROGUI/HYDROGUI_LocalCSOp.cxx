// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_LocalCSOp.h>
#include <HYDROGUI_LocalCSDlg.h>
#include <HYDROData_Document.h>
#include <HYDROGUI_UpdateFlags.h>
#include <gp_Pnt2d.hxx>

HYDROGUI_LocalCSOp::HYDROGUI_LocalCSOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "EDIT_LOCAL_CS" ) );
}

HYDROGUI_LocalCSOp::~HYDROGUI_LocalCSOp()
{
}

void HYDROGUI_LocalCSOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_LocalCSDlg* aPanel = ::qobject_cast<HYDROGUI_LocalCSDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  double aLX, aLY;
  doc()->GetLocalCS( aLX, aLY );
  aPanel->SetLocalCS( aLX, aLY );
}

HYDROGUI_InputPanel* HYDROGUI_LocalCSOp::createInputPanel() const
{
  return new HYDROGUI_LocalCSDlg( module(), getName() );
}

bool HYDROGUI_LocalCSOp::processApply( int& theUpdateFlags,
                                       QString& theErrorMsg,
                                       QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_LocalCSDlg* aPanel = ::qobject_cast<HYDROGUI_LocalCSDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  double aLX = aPanel->GetLocalX();
  double aLY = aPanel->GetLocalY();
  doc()->SetLocalCS( aLX, aLY );

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  return true;
}

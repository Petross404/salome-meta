// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_AISTrihedron.h"

#include <Geom_Axis2Placement.hxx>
#include <Prs3d_DatumAspect.hxx>
#include <Prs3d_LineAspect.hxx>
#include <Prs3d_Presentation.hxx>
#include <Prs3d_Drawer.hxx>
#include <DsgPrs_XYZAxisPresentation.hxx>
#include <gp_Ax2.hxx>
#include <Quantity_Length.hxx>

HYDROGUI_AISTrihedron::HYDROGUI_AISTrihedron( const Handle(Geom_Axis2Placement)& thePlacement )
 : AIS_Trihedron(thePlacement)
{
  ComputeFields();
}

Handle(AIS_Trihedron) HYDROGUI_AISTrihedron::createTrihedron( double theSize )
{
  Handle(Geom_Axis2Placement) anAxis = new Geom_Axis2Placement( gp::XOY() );
  Handle(HYDROGUI_AISTrihedron) aTrihedron = new HYDROGUI_AISTrihedron( anAxis );
  aTrihedron->SetInfiniteState( Standard_True );

  Quantity_Color aCol( 193/255., 205/255., 193/255., Quantity_TOC_RGB );
  aTrihedron->SetArrowColor( aCol.Name() );
  aTrihedron->SetSize( theSize );

  Handle(Prs3d_Drawer) aDrawer = aTrihedron->Attributes();
  if ( aDrawer->HasOwnDatumAspect() ) {
    Handle(Prs3d_DatumAspect) aDaspect = aDrawer->DatumAspect();
    aDaspect->FirstAxisAspect()->SetColor( Quantity_Color( 1.0, 0.0, 0.0, Quantity_TOC_RGB ) );
    aDaspect->SecondAxisAspect()->SetColor( Quantity_Color( 0.0, 1.0, 0.0, Quantity_TOC_RGB ) );
    aDaspect->ThirdAxisAspect()->SetColor( Quantity_Color( 0.0, 0.0, 1.0, Quantity_TOC_RGB ) );
  }

  aTrihedron->SetAxis2Placement();
  return aTrihedron;
}

void HYDROGUI_AISTrihedron::SetAxis2Placement()
{
  ComputeFields();
}

void HYDROGUI_AISTrihedron::Compute( const Handle(PrsMgr_PresentationManager3d)& /*thePresentationManager*/,
                                     const Handle(Prs3d_Presentation)& thePresentation,
                                     const Standard_Integer theMode)
{
  thePresentation->Clear();
  thePresentation->SetInfiniteState( Standard_True );

  ComputeAxis(thePresentation, myUParams);
  ComputeAxis(thePresentation, myZParams);
}

void HYDROGUI_AISTrihedron::ComputeAxis( const Handle(Prs3d_Presentation)& thePresentation,
                                         const AxisParameters& theParams )
{
  DsgPrs_XYZAxisPresentation::Add( thePresentation, theParams.myLineAspect,
                                   myDrawer->ArrowAspect(), myDrawer->TextAspect(),
                                   theParams.myDir, theParams.myVal, theParams.myText,
                                   theParams.myPfirst, theParams.myPlast );
}

void HYDROGUI_AISTrihedron::Compute(const Handle(Graphic3d_Camera)& aProjector,
                                    const Handle(TopLoc_Datum3D)& aTransformation,
                                    const Handle(Prs3d_Presentation)& aPresentation)
{
}

void HYDROGUI_AISTrihedron::ComputeSelection( const Handle(SelectMgr_Selection)& aSelection,
                                              const Standard_Integer aMode )
{
}

void HYDROGUI_AISTrihedron::ComputeFields()
{
  gp_Ax2 anAx2 = Component()->Ax2();

  Handle(Prs3d_DatumAspect) DA = myDrawer->DatumAspect();
  gp_Pnt Orig = anAx2.Location();
  Quantity_Length xo,yo,zo,x,y,z;
  Orig.Coord(xo,yo,zo);

  gp_Dir oX = anAx2.XDirection();
  oX.Coord(x,y,z);
  myUParams.myPfirst.SetCoord(xo,yo,zo);
  myUParams.myVal = DA->FirstAxisLength();
  myUParams.myDir = oX;
  myUParams.myText = "U";
  myUParams.myLineAspect = DA->FirstAxisAspect();
  x = xo + x*myUParams.myVal;
  y = yo + y*myUParams.myVal;
  z = zo + z*myUParams.myVal;
  myUParams.myPlast.SetCoord(x,y,z);

  gp_Dir oY = anAx2.YDirection();
  oY.Coord(x,y,z);
  myZParams.myPfirst.SetCoord(xo,yo,zo);
  myZParams.myVal = DA->SecondAxisLength();
  myZParams.myDir = oY;
  myZParams.myText = "Z";
  myZParams.myLineAspect = DA->SecondAxisAspect();
  x = xo + x*myZParams.myVal;
  y = yo + y*myZParams.myVal;
  z = zo + z*myZParams.myVal;
  myZParams.myPlast.SetCoord(x,y,z);
}

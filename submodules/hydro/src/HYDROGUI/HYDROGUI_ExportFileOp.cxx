// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ExportFileOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_ExportLandCoverMapDlg.h"
#include <HYDROData_PolylineXY.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_LandCoverMap.h>

#include <HYDROData_StricklerTable.h>

#include <HYDROData_Profile.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>
#include <LightApp_Application.h>

#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <SUIT_MessageBox.h>
#include <TopoDS.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS_Wire.hxx>
#include <TopoDS_Vertex.hxx>
#include <TopoDS_Edge.hxx>
#include <BRep_Tool.hxx>
#include <Precision.hxx>
#include <Geom_Curve.hxx>
#include <Geom_Line.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <Geom_TrimmedCurve.hxx>
#include <HYDROData_ShapeFile.h>
#include <SUIT_Desktop.h>
#include <SalomeApp_Study.h>
#include <HYDROData_Iterator.h>

#include <LightApp_Application.h>
#include <QSet>

HYDROGUI_ExportFileOp::HYDROGUI_ExportFileOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "EXPORT_TO_SHAPE_FILE" ) );
}

HYDROGUI_ExportFileOp::~HYDROGUI_ExportFileOp()
{
}

void HYDROGUI_ExportFileOp::startOperation()
{
  HYDROGUI_Operation::startOperation();
  
  QString aFilter( tr("SHP_FILTER") ); 
  
  Handle(HYDROData_PolylineXY) aPolyXY;
  Handle(HYDROData_Polyline3D) aPoly3D;
  NCollection_Sequence<Handle(HYDROData_PolylineXY)> aPolyXYSeq;
  NCollection_Sequence<Handle(HYDROData_Polyline3D)> aPoly3DSeq;
  //
  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( module() );
  for( int anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
  {
    aPolyXY = Handle(HYDROData_PolylineXY)::DownCast( aSeq.Value( anIndex ));
    if (!aPolyXY.IsNull())
      aPolyXYSeq.Append(aPolyXY);

    aPoly3D = Handle(HYDROData_Polyline3D)::DownCast( aSeq.Value( anIndex ));
    if (!aPoly3D.IsNull())
      aPoly3DSeq.Append(aPoly3D);
  }

  if (!aPolyXYSeq.IsEmpty() && !aPoly3DSeq.IsEmpty())
    SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "EXPORT_TO_SHAPE_FILE" ), tr("ERROR_POLYLINES_OF_DIFF_KIND"));
  else
  {
    QString aName = "";
    if (aPolyXYSeq.Size() == 1 && aPoly3DSeq.IsEmpty())
      aName = aPolyXYSeq(1)->GetName();
    if (aPoly3DSeq.Size() == 1 && aPolyXYSeq.IsEmpty())
      aName = aPoly3DSeq(1)->GetName();
    QString aFileName = SUIT_FileDlg::getFileName( module()->getApp()->desktop(), aName, aFilter, tr( "EXPORT_TO_SHAPE_FILE" ), false );
    if (!aFileName.isEmpty())
    {
      QStringList aNonExpList;
      HYDROData_ShapeFile anExporter;
      Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
      if (!aPolyXYSeq.IsEmpty() || !aPoly3DSeq.IsEmpty())
        //Export polylines
        anExporter.Export(aDoc, aFileName, aPolyXYSeq, aPoly3DSeq, aNonExpList);
      else
      {
        //Export polygons
        //Extract all attribute names from all strickler tables
        QSet<QString> anAttrNames;
        //use QSet to store attribute names. Yet it's not so good if the document contains two strickler types
        //with the same name
        if (aDoc)
        {
          HYDROData_Iterator anIt( aDoc, KIND_STRICKLER_TABLE );
          for( ; anIt.More(); anIt.Next() )
          {
            Handle(HYDROData_StricklerTable) aStricklerTableObj = Handle(HYDROData_StricklerTable)::DownCast( anIt.Current() );
            if ( !aStricklerTableObj.IsNull())
              anAttrNames << aStricklerTableObj->GetAttrName();
          }
        }
        else
          return; 

        QStringList SortedListOfAttr = anAttrNames.toList();
        SortedListOfAttr.sort();
        //
        Handle(HYDROData_LandCoverMap) aLCM = Handle(HYDROData_LandCoverMap)::DownCast( aSeq(1) );
        bool IsLinear = aLCM->CheckLinear();
        HYDROGUI_ExportLandCoverMapDlg aDlg( module()->getApp()->desktop(), IsLinear, SortedListOfAttr);
        if ( aDlg.exec() == HYDROGUI_ExportLandCoverMapDlg::Accepted )
        { 
          //In our case :  aSeq.Size() == 1
          //Export of multiple landcover maps into the one shp-file is disallowed.
          QString aCItem = aDlg.getCurrentItem();
          Handle(HYDROData_StricklerTable) aStricklerTableObj;
          {
            HYDROData_Iterator anIt( aDoc, KIND_STRICKLER_TABLE );
            for( ; anIt.More(); anIt.Next() )
            {
              aStricklerTableObj = Handle(HYDROData_StricklerTable)::DownCast( anIt.Current() );
              if ( !aStricklerTableObj.IsNull() && aStricklerTableObj->GetAttrName() == aCItem)
                break;
            }
          }

          //export shape-data
          anExporter.Export(aDoc, aFileName, aLCM, aNonExpList, false, !IsLinear, aDlg.getDeflValue());
          QString aDBFFileName = aFileName.replace( ".shp", ".dbf", Qt::CaseInsensitive);
          //Even if attribute-checkbox is unchecked, the .dbf-file should be removed. 
          //otherwise it may be used with wrong .shp-file. This is an incorrect behaivor.
          remove (aDBFFileName.toStdString().c_str());
          bool bToSaveAttrInfo = aDlg.getAttrCheckBoxState() && !aDlg.getCurrentItem().isEmpty();
          if (bToSaveAttrInfo)
          {
            //export attribute info
            QStringList anAttrValues;
            QStringList aStricklerTypes;
            if (aNonExpList.empty())
            {
              HYDROData_LandCoverMap::Explorer aLCMIt( aLCM );
              for( ; aLCMIt.More(); aLCMIt.Next() )
              {
                QString aST = aLCMIt.StricklerType();
                anAttrValues << aStricklerTableObj->GetAttrValue(aST);
                aStricklerTypes << aST;
              }
            }
            aLCM->ExportDBF(aDBFFileName, aCItem, anAttrValues, aStricklerTypes);
          }
        }
        else
        {
          abort();
          return;
        }

      }

      if (!aNonExpList.empty())
      {
        QString aMessage = tr("CANNOT_BE_EXPORTED") + "\n";
        foreach (QString anNonExpEntName, aNonExpList)
          aMessage += anNonExpEntName + "\n"; 
        SUIT_MessageBox::warning( module()->getApp()->desktop(), tr( "EXPORT_TO_SHAPE_FILE" ), aMessage);
      }
      commit();
    }
    else
      abort();
  }

}


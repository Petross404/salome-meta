// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ZLayers.h>
#include <PrsMgr_PresentableObject.hxx>
#include <PrsMgr_Presentations.hxx>
#include <TColStd_SequenceOfInteger.hxx>
#include <Graphic3d_ZLayerId.hxx>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

void SetZLayerForPrs( const Handle(PrsMgr_Presentation)& thePrs, int theLayerId );

void SetPrsZLayer( const Handle(PrsMgr_PresentableObject)& thePresentableObject,
                   const int theMode, const int theLayerId )
{
  PrsMgr_Presentations& aPresentations = thePresentableObject->Presentations();
  for (Standard_Integer aIdx = 1; aIdx <= aPresentations.Length (); aIdx++)
  {
    Handle(PrsMgr_Presentation) aPrs = aPresentations (aIdx);
    if ( aPrs->Mode() == theMode )
    {
      SetZLayerForPrs( aPrs, theLayerId );
    }
  }
}

void SetZLayerSettings( const Handle(V3d_Viewer)& theViewer3d, int theLayerId, bool theIsOrdered )
{
  if ( theViewer3d.IsNull() /*|| theLayerId < 0*/ )
  {
    return;
  }

  Graphic3d_ZLayerSettings aSettings = theViewer3d->ZLayerSettings( theLayerId );
  // enable depth write
  aSettings.EnableSetting( Graphic3d_ZLayerDepthWrite );
  // disable depth clear
  aSettings.DisableSetting( Graphic3d_ZLayerDepthClear );
  if ( theIsOrdered ) {
    // disable depth test
    aSettings.DisableSetting( Graphic3d_ZLayerDepthTest );
    // disable depth offset
    aSettings.DisableSetting( Graphic3d_ZLayerDepthOffset );
  } else {
    // enable depth test
    aSettings.EnableSetting( Graphic3d_ZLayerDepthTest );
    // set depth offset
    aSettings.SetDepthOffsetPositive();
  }

  // set new settings
  theViewer3d->SetZLayerSettings( theLayerId, aSettings );
}

int CreateTopZLayer( const Handle(V3d_Viewer)& theViewer3d )
{
  DEBTRACE("CreateTopZLayer");
  int aTopZLayer = Graphic3d_ZLayerId_Top;

  if ( theViewer3d ) //&& !theViewer3d->AddZLayer( aTopZLayer ) ) {
    {
      try
      {
         theViewer3d->AddZLayer( aTopZLayer );
      }
      catch (...)
      {
        DEBTRACE("Add ZLayer not possible");
        aTopZLayer = Graphic3d_ZLayerId_Top;
      }
    }
  DEBTRACE("  aTopZLayer: " << aTopZLayer);
  return aTopZLayer;
}


HYDROGUI_ZLayersIterator::HYDROGUI_ZLayersIterator( const Handle(V3d_Viewer)& theViewer )
  : myIndex( 0 ), myNewZLayer( Graphic3d_ZLayerId_Top ), myViewer( theViewer )
{
  Init( theViewer );
}

HYDROGUI_ZLayersIterator::~HYDROGUI_ZLayersIterator()
{
}

void HYDROGUI_ZLayersIterator::Init( const Handle(V3d_Viewer)& theViewer )
{
  TColStd_SequenceOfInteger anExistingZLayers;
  theViewer->GetAllZLayers( anExistingZLayers );

  int n = anExistingZLayers.Length();
  for( int i=1; i<=n; i++ ) {
    int aLayerId = anExistingZLayers( i );
    if ( aLayerId >= 0 ) {
      myZLayers.push_back( aLayerId );
    }
  }

  myIndex = 0;
}

bool HYDROGUI_ZLayersIterator::More() const
{
  return myIndex < (int)myZLayers.size() || myNewZLayer >= 0;
}

void HYDROGUI_ZLayersIterator::Next()
{
  myIndex++;

  if( myIndex >= (int)myZLayers.size() &&
      !myViewer.IsNull() ) {
    myNewZLayer = CreateTopZLayer( myViewer );
  }
}

int HYDROGUI_ZLayersIterator::TopLayer() const
{
  int aTopLayer = Graphic3d_ZLayerId_Top;

  if ( myNewZLayer >= 0 ) {
    aTopLayer = myNewZLayer;
  } else if ( !myZLayers.empty() ) {
    aTopLayer = myZLayers.back();
  }

  return aTopLayer;
}


int HYDROGUI_ZLayersIterator::LayerId() const
{
  if( myIndex < (int)myZLayers.size() )
    return myZLayers[myIndex];
  else
    return myNewZLayer;
}

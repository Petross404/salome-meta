// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PrsPolyline.h"

#include <QPen>

//=======================================================================
// name    : HYDROGUI_PrsPolyline
// Purpose : Constructor
//=======================================================================
HYDROGUI_PrsPolyline::HYDROGUI_PrsPolyline( const Handle(HYDROData_Entity)& theObject )
: HYDROGUI_Prs( theObject ),
  myPolylineItem( 0 )
{
}

//=======================================================================
// name    : HYDROGUI_PrsPolyline
// Purpose : Destructor
//=======================================================================
HYDROGUI_PrsPolyline::~HYDROGUI_PrsPolyline()
{
}

//================================================================
// Function : setPath
// Purpose  : 
//================================================================
void HYDROGUI_PrsPolyline::setPath( const QPainterPath& thePath )
{
  myPath = thePath;
}

//================================================================
// Function : getPath
// Purpose  : 
//================================================================
QPainterPath HYDROGUI_PrsPolyline::getPath() const
{
  return myPath;
}

//================================================================
// Function : boundingRect
// Purpose  : 
//================================================================
QRectF HYDROGUI_PrsPolyline::boundingRect() const
{
  return myPolylineItem->boundingRect();
}

//================================================================
// Function : compute
// Purpose  : 
//================================================================
void HYDROGUI_PrsPolyline::compute()
{
  if( !myPolylineItem )
  {
    myPolylineItem = new QGraphicsPathItem( this );
    addToGroup( myPolylineItem );
  }
  myPolylineItem->setPath( myPath );
}

//================================================================
// Function : checkHighlight
// Purpose  : 
//================================================================
bool HYDROGUI_PrsPolyline::checkHighlight( double theX, double theY, QCursor& theCursor ) const
{
  // to do
  return false;
}

//================================================================
// Function : select
// Purpose  : 
//================================================================
bool HYDROGUI_PrsPolyline::select( double theX, double theY, const QRectF& theRect )
{
  return GraphicsView_Object::select( theX, theY, theRect );
}

//================================================================
// Function : unselect
// Purpose  : 
//================================================================
void HYDROGUI_PrsPolyline::unselect()
{
  GraphicsView_Object::unselect();

  // ouv: tmp
  QPen aPen = myPolylineItem->pen();
  aPen.setColor( Qt::black );
  aPen.setWidth( 1 );
  myPolylineItem->setPen( aPen );
}

//================================================================
// Function : setSelected
// Purpose  : 
//================================================================
void HYDROGUI_PrsPolyline::setSelected( bool theState )
{
  GraphicsView_Object::setSelected( theState );

  // ouv: tmp
  QPen aPen = myPolylineItem->pen();
  aPen.setColor( theState ? Qt::red : Qt::black );
  aPen.setWidth( theState ? 2 : 1 );
  myPolylineItem->setPen( aPen );
}

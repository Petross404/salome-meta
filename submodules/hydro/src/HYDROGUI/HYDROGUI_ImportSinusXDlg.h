// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//



#ifndef HYDROGUI_IMPORTSINUSXDLG_H
#define HYDROGUI_IMPORTSINUSXDLG_H

#include <QTableWidget>
#include <QDialog>
#include <NCollection_Sequence.hxx>
#include <HYDROData_Entity.h>
#include <HYDROData_SinusX.h>

class HYDROGUI_ImportSinusXDlg : public QDialog
{
  Q_OBJECT

public:

  HYDROGUI_ImportSinusXDlg( QWidget* theParent, const QStringList& theNames,
     const std::vector<int>& theTypes);
  virtual ~HYDROGUI_ImportSinusXDlg();

  std::vector<HYDROData_SinusX::ImportOptions> GetImportOptions() const;

private slots:
  void onCheckChecked(int r, int c);

  //private slots:
//  void onCheckAll();
//  void onUncheckAll();
  
private:
  QTableWidget* myTable;
  //NCollection_Sequence<Handle(HYDROData_Entity)> myPolyEnts;
};

#endif

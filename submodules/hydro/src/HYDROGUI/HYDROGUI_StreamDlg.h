// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_StreamDlg_H
#define HYDROGUI_StreamDlg_H

#include "HYDROGUI_InputPanel.h"
#include <HYDROData_Entity.h>

class HYDROGUI_OrderedListWidget;

class QComboBox;
class QGroupBox;
class QLineEdit;
class QListWidget;
class QPushButton;
class QDoubleSpinBox;
class QTextEdit;
class QRadioButton;
class QSpinBox;
class QLabel;

class HYDROGUI_StreamDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_StreamDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_StreamDlg();

  void                       reset();

  void                       setObjectName( const QString& theName );
  QString                    getObjectName() const;

  void                       setAxisNames( const QStringList& theAxises );
  void                       setLeftBankNames( const QStringList& theAxises );
  void                       setRightBankNames( const QStringList& theAxises );

  void                       setAxisName( const QString& thePolyline );
  void                       setLeftBankName( const QString& theName );
  void                       setRightBankName( const QString& theName );

  QString                    getAxisName() const;
  QString                    getLeftBankName() const;
  QString                    getRightBankName() const;

  void                       setProfiles( const QStringList& theProfiles );

  void                       setDDZ( const double );
  double                     getDDZ() const;

  void                       setSpatialStep( const double );
  double                     getSpatialStep() const;

  void                       setNbProfilePoints( const int );
  int                        getNbProfilePoints() const;  

  void                       addWarning( const QString& theWarnMess );
  void                       clearWarnings();

  QList<Handle(HYDROData_Entity)> getProfiles();

  void                       setBackgroundColorForProfileList (int theInd, QColor theColor);
  void                       setBackgroundColorForProfileList (QString name, QColor theColor);
  QColor                     getBackgroundColorForProfileList (int theInd) const;
  void                       clearAllBackgroundColorsForProfileList ();

  int                        getMode();
  void                       setMode(int mode);


signals:
  void                       AddProfiles();
  void                       RemoveProfiles( const QStringList& );
  void                       AxisChanged( const QString& );
  void                       DDZValueChanged (double d);
  void                       SSValueChanged (double d);

  void                       LeftBankChanged( const QString& ) ;
  void                       RightBankChanged( const QString& );
  void                       ProfilePointsChanged (int);
  void                       ModeChanged(bool);

private slots:
  void                       onRemoveProfiles();
  void                       onDDZValueChanged(double d);
  void                       onSSValueChanged(double d);
  void                       ModeChangedDlg(bool);



private:
  QGroupBox*                  myObjectNameGroup;
  QLineEdit*                  myObjectName;
  QDoubleSpinBox*             myDDZ;
  QDoubleSpinBox*             mySpatialStep;
  QSpinBox*                   myProfilePoints;

  QComboBox*                  myAxes;

  QComboBox*                  myLeftBanks;
  QComboBox*                  myRightBanks;

  HYDROGUI_OrderedListWidget* myProfiles;
  QPushButton*                myRemoveButton;
  QPushButton*                myAddButton;
  QTextEdit*                  myWarnText;
  QRadioButton*               myModeButton;
  QRadioButton*               myModeButton2;

  QLabel*                     myDDZLabel;
  QLabel*                     myLeftBanksLabel;
  QLabel*                     myRightBanksLabel;
  QLabel*                     myProfilePointsLabel;

};

#endif

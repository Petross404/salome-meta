// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Operations.h"

#include "HYDROGUI_CopyPasteOp.h"
#include "HYDROGUI_CalculationOp.h"
#include "HYDROGUI_ChannelOp.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_CopyPastePositionOp.h"
#include "HYDROGUI_DeleteOp.h"
#include "HYDROGUI_DigueOp.h"
#include "HYDROGUI_ExportImageOp.h"
#include "HYDROGUI_ExportFileOp.h"
#include "HYDROGUI_ImportImageOp.h"
#include "HYDROGUI_ImportPolylineOp.h"
#include "HYDROGUI_ImportSinusXOp.h"
#include "HYDROGUI_ImportBathymetryOp.h"
#include "HYDROGUI_ImmersibleZoneOp.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_ObserveImageOp.h"
#include "HYDROGUI_PolylineOp.h"
#include "HYDROGUI_Poly3DOp.h"
#include "HYDROGUI_ProfileOp.h"
#include "HYDROGUI_RemoveImageRefsOp.h"
#include "HYDROGUI_ShowHideOp.h"
#include "HYDROGUI_StreamOp.h"
#include "HYDROGUI_TwoImagesOp.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_UpdateObjectOp.h"
#include "HYDROGUI_VisualStateOp.h"
#include "HYDROGUI_ImmersibleZoneOp.h"
#include "HYDROGUI_MeasurementToolOp.h"
#include "HYDROGUI_ImportGeomObjectOp.h"
#include "HYDROGUI_ImportObstacleFromFileOp.h"
#include "HYDROGUI_TranslateObstacleOp.h"
#include "HYDROGUI_ExportCalculationOp.h"
#include "HYDROGUI_ImportProfilesOp.h"
#include "HYDROGUI_GeoreferencementOp.h"
#include "HYDROGUI_SetColorOp.h"
#include "HYDROGUI_BathymetryBoundsOp.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_ZLevelsOp.h"
#include "HYDROGUI_LocalCSOp.h"
#include "HYDROGUI_RiverBottomOp.h"
#include "HYDROGUI_ProfileInterpolateOp.h"
#include "HYDROGUI_RecognizeContoursOp.h"
#include <HYDROGUI_ImportBCPolygonOp.h>
#include "HYDROGUI_SubmersibleOp.h"
#include "HYDROGUI_StricklerTableOp.h"
#include "HYDROGUI_DuplicateOp.h"
#include "HYDROGUI_LandCoverMapOp.h"
#include "HYDROGUI_PolylineExtractionOp.h"
#include "HYDROGUI_ExportSinusXOp.h"
#include "HYDROGUI_MergePolylinesOp.h"
#include "HYDROGUI_SplitPolylinesOp.h"
#include "HYDROGUI_LandCoverColoringOp.h"
#include "HYDROGUI_SetTransparencyOp.h"
#include "HYDROGUI_ImportLandCoverMapOp.h"
#include "HYDROGUI_BathymetrySelectionOp.h"
#include "HYDROGUI_BathymetryOp.h"
#include "HYDROGUI_RegenerateRegionColorsOp.h"
#include "HYDROGUI_PolylineStyleOp.h"
#include "HYDROGUI_ZoneSetColorOp.h"
#include <HYDROGUI_ShowAttrPolyOp.h>
#include <HYDROGUI_SetBoundaryTypePolygonOp.h>
#include <HYDROData_Document.h>
#include <HYDROData_Obstacle.h>
#include <HYDROData_SplitToZonesTool.h>
#include <HYDROData_Iterator.h>

#include <GeometryGUI.h>
#include <GeometryGUI_Operations.h>
#include <GEOMBase.h>

#include <SalomeApp_Study.h>

#include <LightApp_Application.h>

#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewFrame.h>

#include <QtxListAction.h>
#include <QtxActionToolMgr.h>

#include <SUIT_Desktop.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SUIT_MessageBox.h>

#include <QAction>
#include <QApplication>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

QAction* HYDROGUI_Module::createAction( const int theId, const QString& theSuffix, const QString& theImg,
                                        const int theKey, const bool isToggle, const QString& theSlot )
{
  QString aSlot = theSlot;
  if( aSlot.isEmpty() )
    aSlot = SLOT( onOperation() );
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  std::string anImg = theImg.toStdString();
  QPixmap aPixmap = theImg.isEmpty() ? QPixmap() : aResMgr->loadPixmap( "HYDRO", tr( anImg.c_str() ) );
  std::string aMenu    = ( "MEN_" + theSuffix ).toStdString();
  std::string aDesktop = ( "DSK_" + theSuffix ).toStdString();
  std::string aToolbar = ( "STB_" + theSuffix ).toStdString();
  std::string aSlotStr = aSlot.toStdString();
  return LightApp_Module::createAction( theId, tr( aDesktop.c_str() ), aPixmap,
    tr( aMenu.c_str() ), tr( aToolbar.c_str() ),
		theKey, application()->desktop(), isToggle, this, aSlotStr.c_str() );
}

void HYDROGUI_Module::createActions()
{
  createAction( SaveVisualStateId, "SAVE_VISUAL_STATE" );
  createAction( LoadVisualStateId, "LOAD_VISUAL_STATE" );

  createAction( CopyId, "COPY", "", Qt::CTRL + Qt::Key_C );
  createAction( PasteId, "PASTE", "", Qt::CTRL + Qt::Key_V );

  createAction( UpdateObjectId, "UPDATE_OBJECT" );
  createAction( ForcedUpdateObjectId, "FORCED_UPDATE_OBJECT" );

  createAction( ImportImageId, "IMPORT_IMAGE", "IMPORT_IMAGE_ICO", Qt::CTRL + Qt::Key_I );
  createAction( EditImportedImageId, "EDIT_IMPORTED_IMAGE", "EDIT_IMPORTED_IMAGE_ICO" );
  createAction( ObserveImageId, "OBSERVE_IMAGE", "OBSERVE_IMAGE_ICO" );
  createAction( ExportImageId, "EXPORT_IMAGE", "EXPORT_IMAGE_ICO" );
  createAction( RemoveImageRefsId, "REMOVE_IMAGE_REFERENCE", "REMOVE_IMAGE_REFERENCE_ICO" );
  
  createAction( ImportPolylineId, "IMPORT_POLYLINE", "IMPORT_POLYLINE_ICO" );
  createAction( ImportSinusXId, "IMPORT_SINUSX", "IMPORT_SINUSX_ICO" );
  createAction( ExportSinusXId, "EXPORT_SINUSX", "EXPORT_SINUSX_ICO" );
  createAction( ImportLandCoverMapId, "IMPORT_LANDCOVER_MAP", "IMPORT_LANDCOVER_MAP_ICO" );

  createAction( MeasurementToolId, "MEASUREMENT_TOOL", "MEASUREMENT_TOOL_ICO" );

  createAction( ImportBCPolygonId, "IMPORT_BC_POLYGON", "IMPORT_BC_POLYGON_ICO" );

  createAction( CreatePolylineId, "CREATE_POLYLINE", "CREATE_POLYLINE_ICO" );
  createAction( EditPolylineId, "EDIT_POLYLINE", "EDIT_POLYLINE_ICO" ); 

  createAction( CreatePolyline3DId, "CREATE_POLYLINE_3D", "CREATE_POLYLINE_3D_ICO" );
  createAction( EditPolyline3DId, "EDIT_POLYLINE_3D", "EDIT_POLYLINE_3D_ICO" ); 

  createAction( CreateProfileId, "CREATE_PROFILE", "CREATE_PROFILE_ICO" );
  createAction( ImportProfilesId, "IMPORT_PROFILES", "IMPORT_PROFILES_ICO" );
  createAction( EditProfileId, "EDIT_PROFILE", "EDIT_PROFILE_ICO" ); 
  createAction( AllGeoreferencementId, "GEOREFERENCEMENT", "GEOREFERENCEMENT_ICO" ); 
  createAction( SelectedGeoreferencementId, "GEOREFERENCEMENT", "GEOREFERENCEMENT_ICO" ); 
  
  createAction( ImportBathymetryId, "IMPORT_BATHYMETRY", "IMPORT_BATHYMETRY_ICO", Qt::CTRL + Qt::Key_B );
  createAction( EditImportedBathymetryId, "EDIT_IMPORTED_BATHYMETRY", "EDIT_IMPORTED_BATHYMETRY_ICO" );
  createAction( BathymetryBoundsId, "BATHYMETRY_BOUNDS", "BATHYMETRY_BOUNDS_ICO" );
  createAction( BathymetrySelectionId, "BATHYMETRY_SELECTION", "BATHYMETRY_SELECTION_ICO",
    0, true, SLOT( onBathymetrySelection() ) );

  createAction( BathymetryTextId, "BATHYMETRY_TEXT", "BATHYMETRY_TEXT_ICO",
    0, true, SLOT( onBathymetryText() ) );

  createAction( BathymetryRescaleSelectionId, "BATHYMETRY_RESCALE_SELECTION", "BATHYMETRY_RESCALE_SELECTION_ICO" );
  createAction( BathymetryRescaleVisibleId, "BATHYMETRY_RESCALE_VISIBLE", "BATHYMETRY_RESCALE_VISIBLE_ICO" );
  createAction( BathymetryRescaleUserId, "BATHYMETRY_RESCALE_USER", "BATHYMETRY_RESCALE_USER_ICO" );
  createAction( BathymetryRescaleDefaultId, "BATHYMETRY_RESCALE_DEFAULT", "BATHYMETRY_RESCALE_DEFAULT_ICO" );

  createAction( CreateImmersibleZoneId, "CREATE_IMMERSIBLE_ZONE", "CREATE_IMMERSIBLE_ZONE_ICO" );
  createAction( EditImmersibleZoneId, "EDIT_IMMERSIBLE_ZONE", "EDIT_IMMERSIBLE_ZONE_ICO" );

  createAction( CreateStreamId, "CREATE_STREAM", "CREATE_STREAM_ICO" );
  createAction( EditStreamId, "EDIT_STREAM", "EDIT_STREAM_ICO" );

  createAction( CreateChannelId, "CREATE_CHANNEL", "CREATE_CHANNEL_ICO" );
  createAction( EditChannelId, "EDIT_CHANNEL", "EDIT_CHANNEL_ICO" );

  createAction( CreateDigueId, "CREATE_DIGUE", "CREATE_DIGUE_ICO" );
  createAction( EditDigueId, "EDIT_DIGUE", "EDIT_DIGUE_ICO" );

  createAction( ImportStricklerTableFromFileId, "IMPORT_STRICKLER_TABLE", "IMPORT_STRICKLER_TABLE_ICO" );
  createAction( ExportStricklerTableFromFileId, "EXPORT_STRICKLER_TABLE", "EXPORT_STRICKLER_TABLE_ICO" );
  createAction( EditStricklerTableId, "EDIT_STRICKLER_TABLE", "EDIT_STRICKLER_TABLE_ICO" );
  createAction( DuplicateStricklerTableId, "DUPLICATE_STRICKLER_TABLE", "DUPLICATE_STRICKLER_TABLE_ICO" );

  createAction( CreateLandCoverMapId, "CREATE_LAND_COVER_MAP", "CREATE_LAND_COVER_MAP_ICO" );
  
  createAction( AddLandCoverId, "ADD_LAND_COVER", "ADD_LAND_COVER_ICO" );
  createAction( RemoveLandCoverId, "REMOVE_LAND_COVER", "REMOVE_LAND_COVER_ICO" );
  createAction( SplitLandCoverId, "SPLIT_LAND_COVER", "SPLIT_LAND_COVER_ICO" );
  createAction( MergeLandCoverId, "MERGE_LAND_COVER", "MERGE_LAND_COVER_ICO" );
  createAction( ChangeLandCoverTypeId, "CHANGE_LAND_COVER_TYPE", "CHANGE_LAND_COVER_TYPE_ICO" );

  createAction( ImportObstacleFromFileId, "IMPORT_OBSTACLE_FROM_FILE", "IMPORT_OBSTACLE_FROM_FILE_ICO" );
  createAction( ImportGeomObjectAsObstacleId, "IMPORT_GEOM_OBJECT_AS_OBSTACLE", "IMPORT_GEOM_OBJECT_ICO" );
  createAction( ImportGeomObjectAsPolylineId, "IMPORT_GEOM_OBJECT_AS_POLYLINE", "IMPORT_GEOM_OBJECT_ICO" );
  createAction( CreateBoxId, "CREATE_BOX", "CREATE_BOX_ICO" );
  createAction( CreateCylinderId, "CREATE_CYLINDER", "CREATE_CYLINDER_ICO" );
  createAction( TranslateObstacleId, "TRANSLATE_OBSTACLE" );

  createAction( CreateCalculationId, "CREATE_CALCULATION", "CREATE_CALCULATION_ICO" );
  createAction( EditCalculationId, "EDIT_CALCULATION", "EDIT_CALCULATION_ICO" );
  createAction( ExportCalculationId, "EXPORT_CALCULATION", "EXPORT_CALCULATION_ICO" );

  createAction( CompleteCalculationId, "COMPLETE_CALCULATION", "COMPLETE_CALCULATION_ICO" );

  createAction( FuseImagesId, "FUSE_IMAGES", "FUSE_IMAGES_ICO" );
  createAction( EditFusedImageId, "EDIT_FUSED_IMAGE", "EDIT_FUSED_IMAGE_ICO" );

  createAction( CutImagesId, "CUT_IMAGES", "CUT_IMAGES_ICO" );
  createAction( EditCutImageId, "EDIT_CUT_IMAGE", "EDIT_CUT_IMAGE_ICO" );

  createAction( SplitImageId, "SPLIT_IMAGE", "SPLIT_IMAGE_ICO" );
  createAction( EditSplitImageId, "EDIT_SPLIT_IMAGE", "EDIT_SPLIT_IMAGE_ICO" );

  createAction( RecognizeContoursId, "RECOGNIZE_CONTOURS", "RECOGNIZE_CONTOURS_ICO" );

  createAction( CopyViewerPositionId, "COPY_VIEWER_POSITION", "" );

  createAction( DeleteId, "DELETE", "", Qt::Key_Delete, false,
                SLOT( onDelete() ) );

  createAction( SetColorId, "COLOR" );
  createAction( SetTransparencyId, "TRANSPARENCY" );
  createAction( SetZLevelId, "ZLEVEL" );
  createAction( EditLocalCSId, "EDIT_LOCAL_CS" );

  createAction( ShowId, "SHOW" );
  createAction( ShowOnlyId, "SHOW_ONLY" );
  createAction( ShowAllId, "SHOW_ALL" );
  createAction( HideId, "HIDE" );
  createAction( HideAllId, "HIDE_ALL" );

  createAction( RiverBottomId, "CREATE_STREAM_BOTTOM", "CREATE_STREAM_BOTTOM_ICO" );
  createAction( RiverBottomContextId, "CREATE_STREAM_BOTTOM", "CREATE_STREAM_BOTTOM_ICO" );
  createAction( ProfileInterpolateId, "PROFILE_INTERPOLATE", "PROFILE_INTERPOLATE_ICO" );

  createAction( SubmersibleId, "SUBMERSIBLE", "SUBMERSIBLE_ICO" );
  createAction( UnSubmersibleId, "UNSUBMERSIBLE", "HYDRO_UNSUBMERSIBLE16_ICO" );
  createAction( ExportToShapeFileID, "EXPORT_TO_SHAPE_FILE", "EXPORT_TO_SHAPE_FILE_ICO" );

  createAction( PolylineExtractionId, "POLYLINE_EXTRACTION" );
  createAction( SplitPolylinesId, "SPLIT_POLYLINES", "SPLIT_POLYLINES_ICO" );
  createAction( MergePolylinesId, "MERGE_POLYLINES", "MERGE_POLYLINES_ICO" );

  createAction( ShowAttrPolylinesId, "SHOWATTR_POLYLINES");

  createAction( LandCoverScalarMapModeOnId, "LC_SCALARMAP_COLORING_ON" );
  createAction( LandCoverScalarMapModeOffId, "LC_SCALARMAP_COLORING_OFF" );

  createAction( RegenerateRegionColorsId, "REGENERATE_REGION_COLORS" );
  createAction( ZoneSetColorId, "ZONE_SET_COLOR" );
  createAction( ShowHideArrows, "SHOW_HIDE_ARROWS" );
  createAction( SetBoundaryTypePolygonId, "SET_BOUNDARY_TYPE_POLYGON" );
}

void HYDROGUI_Module::createMenus()
{
  int aFileMenu = createMenu( tr( "MEN_DESK_FILE" ), -1, -1, 0 );
  int aCustomPos = 5; // to insert custom actions after "Save as" and before "Preferences"
  createMenu( SaveVisualStateId, aFileMenu, aCustomPos );
  createMenu( separator(), aFileMenu, -1, aCustomPos );

  int anEditMenu = createMenu( tr( "MEN_DESK_EDIT" ), -1, -1, 5 );
  createMenu( UndoId, anEditMenu );
  createMenu( RedoId, anEditMenu );
  createMenu( separator(), anEditMenu );
  createMenu( CopyId, anEditMenu );
  createMenu( PasteId, anEditMenu );

  int aHydroMenu = 6; // Edit menu id == 5, View menu id == 10
  int aHydroId = createMenu( tr( "MEN_DESK_HYDRO" ), -1, -1, aHydroMenu );
  createMenu( ImportSinusXId, aHydroId, -1, -1 );
  createMenu( ExportSinusXId, aHydroId, -1, -1 );  
  
  createMenu( ImportBathymetryId, aHydroId, -1, -1 );
  createMenu( ImportStricklerTableFromFileId, aHydroId, -1, -1 );
  createMenu( ImportBCPolygonId, aHydroId, -1, -1 );
  createMenu( CreatePolyline3DId, aHydroId, -1, -1 );
  createMenu( CreateImmersibleZoneId, aHydroId, -1, -1 );
  createMenu( CreateChannelId, aHydroId, -1, -1 );
  createMenu( CreateDigueId, aHydroId, -1, -1 );

  createMenu( MeasurementToolId, aHydroId, -1, -1 );

  int aNewImageId = createMenu( tr( "MEN_DESK_IMAGE" ), aHydroId, -1 );
  createMenu( ImportImageId, aNewImageId, -1, -1 );
  createMenu( separator(), aNewImageId );
  createMenu( FuseImagesId, aNewImageId, -1, -1 );
  createMenu( CutImagesId, aNewImageId, -1, -1 );
  createMenu( SplitImageId, aNewImageId, -1, -1 );

  int aNewPolylineId = createMenu( tr( "MEN_DESK_POLYLINE" ), aHydroId, -1 );
  createMenu( ImportPolylineId, aNewPolylineId, -1, -1 );
  createMenu( CreatePolylineId, aNewPolylineId, -1, -1 );
  createMenu( separator(), aNewPolylineId );
  createMenu( SplitPolylinesId, aNewPolylineId, -1, -1 );
  createMenu( MergePolylinesId, aNewPolylineId, -1, -1 );

  int aNewProfileId = createMenu( tr( "MEN_DESK_PROFILE" ), aHydroId, -1 );
  createMenu( CreateProfileId, aNewProfileId, -1, -1 );
  createMenu( ImportProfilesId, aNewProfileId, -1, -1 );
  createMenu( separator(), aNewProfileId );
  createMenu( AllGeoreferencementId, aNewProfileId, -1, -1 );

  int aStreamMenuId = createMenu( tr( "MEN_DESK_STREAM" ), aHydroId, -1 );
  createMenu( CreateStreamId, aStreamMenuId, -1, -1 );
  createMenu( separator(), aStreamMenuId );
  createMenu( RiverBottomId, aStreamMenuId, -1, -1 );
  createMenu( ProfileInterpolateId, aStreamMenuId, -1, -1 );

  int anObstacleMenuId = createMenu( tr( "MEN_DESK_OBSTACLE" ), aHydroId, -1 );
  createMenu( ImportObstacleFromFileId, anObstacleMenuId, -1, -1 );
  createMenu( separator(), anObstacleMenuId );
  createMenu( CreateBoxId, anObstacleMenuId, -1, -1 );
  createMenu( CreateCylinderId, anObstacleMenuId, -1, -1 );

  int aLandCoverMapMenuId = createMenu( tr( "MEN_DESK_LAND_COVER_MAP" ), aHydroId, -1 );
  createMenu( ImportLandCoverMapId, aLandCoverMapMenuId, -1, -1 );
  createMenu( CreateLandCoverMapId, aLandCoverMapMenuId, -1, -1 );
  createMenu( separator(), aLandCoverMapMenuId );
  createMenu( AddLandCoverId, aLandCoverMapMenuId, -1, -1 );
  createMenu( RemoveLandCoverId, aLandCoverMapMenuId, -1, -1 );
  createMenu( separator(), aLandCoverMapMenuId );
  createMenu( SplitLandCoverId, aLandCoverMapMenuId, -1, -1 );
  createMenu( MergeLandCoverId, aLandCoverMapMenuId, -1, -1 );
  createMenu( ChangeLandCoverTypeId, aLandCoverMapMenuId, -1, -1 );
  
  createMenu( CreateCalculationId, aHydroId, -1, -1 );
  createMenu( separator(), aHydroId );
  createMenu( EditLocalCSId, aHydroId, -1, -1 );
  createMenu( separator(), aHydroId );
}

void HYDROGUI_Module::createPopups()
{
}

void HYDROGUI_Module::createToolbars()
{
  int aToolBar = createTool( tr( "MEN_DESK_HYDRO" ) );
  createTool( UndoId, aToolBar );
  createTool( RedoId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( ImportImageId, aToolBar );
  createTool( ImportPolylineId, aToolBar );
  createTool( ImportSinusXId, aToolBar );
  createTool( ExportSinusXId, aToolBar );
  createTool( ImportLandCoverMapId, aToolBar );

  createTool( ImportBCPolygonId, aToolBar );
  createTool( MeasurementToolId, aToolBar );

  createTool( ImportBathymetryId, aToolBar );
  createTool( CreatePolylineId, aToolBar );
  createTool( CreatePolyline3DId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( CreateProfileId, aToolBar );
  createTool( ImportProfilesId, aToolBar );
  createTool( AllGeoreferencementId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( CreateChannelId, aToolBar );
  createTool( CreateDigueId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( CreateImmersibleZoneId, aToolBar );
  createTool( CreateStreamId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( ImportObstacleFromFileId, aToolBar );
  createTool( CreateBoxId, aToolBar );
  createTool( CreateCylinderId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( ImportStricklerTableFromFileId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( CreateLandCoverMapId, aToolBar );
  createTool( AddLandCoverId, aToolBar );
  createTool( RemoveLandCoverId, aToolBar );
  createTool( SplitLandCoverId, aToolBar );
  createTool( MergeLandCoverId, aToolBar );
  createTool( ChangeLandCoverTypeId, aToolBar );
  enableLCMActions();

  createTool( separator(), aToolBar );
  createTool( CreateCalculationId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( FuseImagesId, aToolBar );
  createTool( CutImagesId, aToolBar );
  createTool( SplitImageId, aToolBar );

  createTool( separator(), aToolBar );
  createTool( BathymetrySelectionId, aToolBar );
  createTool( BathymetryTextId, aToolBar );
  createTool( BathymetryRescaleSelectionId, aToolBar );
  createTool( BathymetryRescaleVisibleId, aToolBar );
  createTool( BathymetryRescaleUserId, aToolBar );
  createTool( BathymetryRescaleDefaultId, aToolBar );
}

void HYDROGUI_Module::createUndoRedoActions()
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();

  QtxListAction* anEditUndo = new QtxListAction( tr( "MEN_UNDO" ),
    aResMgr->loadPixmap( "HYDRO", tr( "UNDO_ICO" ) ), tr( "DSK_UNDO" ),
    Qt::CTRL + Qt::Key_Z, application()->desktop() );
    
  QtxListAction* anEditRedo = new QtxListAction( tr( "MEN_REDO" ),
    aResMgr->loadPixmap( "HYDRO", tr( "REDO_ICO" ) ), tr( "DSK_REDO" ),
    Qt::CTRL + Qt::Key_Y, application()->desktop() );
  
  registerAction( UndoId, anEditUndo );
  registerAction( RedoId, anEditRedo );

  anEditUndo->setComment( tr( "STB_UNDO" ) );
  anEditRedo->setComment( tr( "STB_REDO" ) );

  connect( anEditUndo, SIGNAL( triggered( int ) ), this, SLOT( onUndo( int ) ) );
  connect( anEditRedo, SIGNAL( triggered( int ) ), this, SLOT( onRedo( int ) ) );
}

void HYDROGUI_Module::updateUndoRedoControls()
{
  HYDROGUI_DataModel* aModel = getDataModel();

  QtxListAction* aUndoAction = (QtxListAction*)action( UndoId );
  QtxListAction* aRedoAction = (QtxListAction*)action( RedoId );

  bool aCanUndo = aModel->canUndo();
  bool aCanRedo = aModel->canRedo();

  if( aCanUndo )
    aUndoAction->addNames( aModel->undoNames() );
  aUndoAction->setEnabled( aCanUndo );

  if( aCanRedo )
    aRedoAction->addNames( aModel->redoNames() );
  aRedoAction->setEnabled( aCanRedo );
}

void HYDROGUI_Module::enableLCMActions()
{
  HYDROData_Iterator anIt( HYDROData_Document::Document(), KIND_LAND_COVER_MAP );
  bool anEnableTools = anIt.More();

  QtxListAction* anAction;
  anAction = (QtxListAction*)action( AddLandCoverId );
  if ( anAction ) anAction->setEnabled( anEnableTools );
  anAction = (QtxListAction*)action( RemoveLandCoverId );
  if ( anAction ) anAction->setEnabled( anEnableTools );
  anAction = (QtxListAction*)action( SplitLandCoverId );
  if ( anAction ) anAction->setEnabled( anEnableTools );
  anAction = (QtxListAction*)action( MergeLandCoverId );
  if ( anAction ) anAction->setEnabled( anEnableTools );
  anAction = (QtxListAction*)action( ChangeLandCoverTypeId );
  if ( anAction ) anAction->setEnabled( anEnableTools );
}

void HYDROGUI_Module::resetViewState()
{
  DEBTRACE("HYDROGUI_Module::resetViewState");
  OCCViewer_ViewManager* mgr = dynamic_cast<OCCViewer_ViewManager*>(getApp()->viewManager( OCCViewer_Viewer::Type()));
  if( mgr )
  {
    foreach( SUIT_ViewWindow* wnd, mgr->getViews() )
    {
      OCCViewer_ViewFrame* vf = dynamic_cast<OCCViewer_ViewFrame*>( wnd );
      if( vf )
      {
        for( int i=OCCViewer_ViewFrame::MAIN_VIEW; i<=OCCViewer_ViewFrame::TOP_RIGHT; i++ )
        {
          OCCViewer_ViewWindow* iwnd = vf->getView(i);
          if( iwnd )
            iwnd->resetState();
        }
      }
      else
      {
        OCCViewer_ViewWindow* ownd = dynamic_cast<OCCViewer_ViewWindow*>( wnd );
        if( ownd )
          ownd->resetState();
      }
    }
  }
}

void HYDROGUI_Module::onOperation()
{
  DEBTRACE("HYDROGUI_Module::onOperation");
  const QAction* anAction = dynamic_cast<const QAction*>( sender() );
  int anId = actionId( anAction );
  if( anId >= 0 )
  {
    // --- bathymetry from stream interpolation must be hidden before edition to avoid aborts after on hide show
    if (anId == EditStreamId)
        HYDROGUI_StreamOp::hideBathy(this);
    resetViewState();
    startOperation( anId );
  }

  if( anId==ShowHideArrows )
  {
    setArrowsVisible( !arrowsVisible() );
  }
}

bool HYDROGUI_Module::arrowsVisible() const
{
  int aType;
  SUIT_ResourceMgr* resMgr = application()->resourceMgr();
  if( resMgr )
  {
    resMgr->value( "polyline", "arrow_type", aType );
    return aType>0;
  }
  else
    return false;
}

void HYDROGUI_Module::setArrowsVisible( bool isVisible )
{
  SUIT_ResourceMgr* resMgr = application()->resourceMgr();

  int aType=-1;
  if( !resMgr )
    return;

  if( isVisible )
  {
    if( resMgr->hasValue( "polyline", "prev_arrow_type" ) )
      resMgr->value( "polyline", "prev_arrow_type", aType );

    if( aType<=0 )
      aType = 1;
    resMgr->setValue( "polyline", "arrow_type", aType );
  }
  else
  {
    resMgr->value( "polyline", "arrow_type", aType );
    resMgr->setValue( "polyline", "prev_arrow_type", aType );
    resMgr->setValue( "polyline", "arrow_type", 0 );
  }
  preferencesChanged( "polyline", "arrow_type" );
}

void HYDROGUI_Module::onDelete()
{
  SUIT_Operation* anOp = application()->activeStudy()->activeOperation();
  HYDROGUI_PolylineOp* aPolylineOp = dynamic_cast<HYDROGUI_PolylineOp*>( anOp );
  if ( aPolylineOp && aPolylineOp->deleteEnabled() )
    aPolylineOp->deleteSelected();
  else
    startOperation( DeleteId );
}

bool HYDROGUI_Module::onUndo( int theNumActions )
{
  QApplication::setOverrideCursor( Qt::WaitCursor );
  bool anIsOk = true;
  HYDROGUI_DataModel* aModel = getDataModel();
  if( aModel )
  {
    while( theNumActions > 0 )
    {
      if( !aModel->undo() )
      {
        anIsOk = false;
        break;
      }
      theNumActions--;
    }
    update( UF_All );
  }
  QApplication::restoreOverrideCursor();
  return anIsOk;
}

bool HYDROGUI_Module::onRedo( int theNumActions )
{
  QApplication::setOverrideCursor( Qt::WaitCursor );
  bool anIsOk = true;
  HYDROGUI_DataModel* aModel = getDataModel();
  if( aModel )
  {
    while( theNumActions > 0 )
    {
      if( !aModel->redo() )
      {
        anIsOk = false;
        break;
      }
      theNumActions--;
    }
    update( UF_All );
  }
  QApplication::restoreOverrideCursor();
  return anIsOk;
}

LightApp_Operation* HYDROGUI_Module::createOperation( const int theId ) const
{
  DEBTRACE("HYDROGUI_Module::createOperation "<< theId);
  LightApp_Operation* anOp = 0;
  HYDROGUI_Module* aModule = const_cast<HYDROGUI_Module*>( this );
  switch( theId )
  {
  case SaveVisualStateId:
  case LoadVisualStateId:
    anOp = new HYDROGUI_VisualStateOp( aModule, theId == LoadVisualStateId );
    break;
  case CopyId:
  case PasteId:
    anOp = new HYDROGUI_CopyPasteOp( aModule, theId == PasteId );
    break;
  case ImportImageId:
  case EditImportedImageId:
    anOp = new HYDROGUI_ImportImageOp( aModule, theId == EditImportedImageId );
    break;
  case ImportPolylineId:
    anOp = new HYDROGUI_ImportPolylineOp( aModule/*, theId == EditImportedPolylineId*/ );
    break;
  case ImportSinusXId:
    anOp = new HYDROGUI_ImportSinusXOp( aModule );
    break;
  case ExportSinusXId:
    anOp = new HYDROGUI_ExportSinusXOp( aModule );
    break;
  case ImportBCPolygonId:
    anOp = new HYDROGUI_ImportBCPolygonOp( aModule );
    break;
  case MeasurementToolId:
    anOp = new HYDROGUI_MeasurementToolOp( aModule );
    break;
  case ObserveImageId:
    anOp = new HYDROGUI_ObserveImageOp( aModule );
    break;
  case ExportImageId:
    anOp = new HYDROGUI_ExportImageOp( aModule );
    break;
  case UpdateObjectId:
  case ForcedUpdateObjectId:
    anOp = new HYDROGUI_UpdateObjectOp( aModule, theId == ForcedUpdateObjectId );
    break;
  case ExportToShapeFileID:
    anOp = new HYDROGUI_ExportFileOp( aModule );
    break;
  case ImportLandCoverMapId:
    anOp = new HYDROGUI_ImportLandCoverMapOp( aModule );
    break;
  case RemoveImageRefsId:
    anOp = new HYDROGUI_RemoveImageRefsOp( aModule );
    break;
  case CreatePolyline3DId:
  case EditPolyline3DId:
    anOp = new HYDROGUI_Poly3DOp( aModule, theId == EditPolyline3DId );
    break;
  case CreatePolylineId:
  case EditPolylineId:
    anOp = new HYDROGUI_PolylineOp( aModule, theId == EditPolylineId );
    break;
  case CreateProfileId:
  case EditProfileId:
    anOp = new HYDROGUI_ProfileOp( aModule, theId == EditProfileId );
    break;
  case ProfileInterpolateId:
    anOp = new HYDROGUI_ProfileInterpolateOp( aModule );
    break;
  case ImportProfilesId:
    anOp = new HYDROGUI_ImportProfilesOp( aModule ) ;
    break;
  case AllGeoreferencementId:
    anOp = new HYDROGUI_GeoreferencementOp( aModule, HYDROGUI_GeoreferencementOp::All ) ;
    break;
  case SelectedGeoreferencementId:
    anOp = new HYDROGUI_GeoreferencementOp( aModule, HYDROGUI_GeoreferencementOp::Selected ) ;
    break;
  case ImportBathymetryId:
  case EditImportedBathymetryId:
    anOp = new HYDROGUI_ImportBathymetryOp( aModule, theId == EditImportedBathymetryId  );
    break;
  case BathymetryBoundsId:
    anOp = new HYDROGUI_BathymetryBoundsOp( aModule );
    break;
  case BathymetrySelectionId:
    anOp = new HYDROGUI_BathymetrySelectionOp( aModule );
    break;
  case BathymetryTextId:
  case BathymetryRescaleSelectionId:
  case BathymetryRescaleVisibleId:
  case BathymetryRescaleUserId:
  case BathymetryRescaleDefaultId:
    anOp = new HYDROGUI_BathymetryOp( aModule, theId );
    break;

  case CreateImmersibleZoneId:
  case EditImmersibleZoneId:
    anOp = new HYDROGUI_ImmersibleZoneOp( aModule, theId == EditImmersibleZoneId );
    break;
  case CreateStreamId:
  case EditStreamId:
    anOp = new HYDROGUI_StreamOp( aModule, theId == EditStreamId );
    break;
  case CreateChannelId:
  case EditChannelId:
    anOp = new HYDROGUI_ChannelOp( aModule, theId == EditChannelId );
    break;
  case CreateDigueId:
  case EditDigueId:
    anOp = new HYDROGUI_DigueOp( aModule, theId == EditDigueId );
    break;
  case ImportStricklerTableFromFileId:
  case ExportStricklerTableFromFileId:
  case EditStricklerTableId:
    anOp = new HYDROGUI_StricklerTableOp( aModule, theId );
    break;
  case CreateLandCoverMapId:
  case AddLandCoverId:
  case RemoveLandCoverId:
  case SplitLandCoverId:
  case MergeLandCoverId:
  case ChangeLandCoverTypeId:
    anOp = new HYDROGUI_LandCoverMapOp( aModule, theId );
    break;
  case DuplicateStricklerTableId:
    anOp = new HYDROGUI_DuplicateOp( aModule );
    break;
  case CreateCalculationId:
  case EditCalculationId:
    anOp = new HYDROGUI_CalculationOp( aModule, theId == EditCalculationId, false );
    break;

  case CompleteCalculationId:
    anOp = new HYDROGUI_CalculationOp( aModule, true, true);
    break;

  case ExportCalculationId:
    anOp = new HYDROGUI_ExportCalculationOp( aModule );
    break;
  case FuseImagesId:
  case EditFusedImageId:
    anOp = new HYDROGUI_TwoImagesOp( aModule, HYDROGUI_TwoImagesOp::Fuse, theId == EditFusedImageId );
    break;
  case CutImagesId:
  case EditCutImageId:
    anOp = new HYDROGUI_TwoImagesOp( aModule, HYDROGUI_TwoImagesOp::Cut, theId == EditCutImageId );
    break;
  case SplitImageId:
  case EditSplitImageId:
    anOp = new HYDROGUI_TwoImagesOp( aModule, HYDROGUI_TwoImagesOp::Split, theId == EditSplitImageId );
    break;
  case ImportObstacleFromFileId:
    anOp = new HYDROGUI_ImportObstacleFromFileOp( aModule );
    break;
  case ImportGeomObjectAsObstacleId:
    anOp = new HYDROGUI_ImportGeomObjectOp( aModule, HYDROGUI_ImportGeomObjectOp::ImportSelectedAsObstacle );
    break;
  case ImportGeomObjectAsPolylineId:
    anOp = new HYDROGUI_ImportGeomObjectOp( aModule, HYDROGUI_ImportGeomObjectOp::ImportSelectedAsPolyline );
    break;
  case CreateBoxId:
    anOp = new HYDROGUI_ImportGeomObjectOp( aModule, 
      HYDROGUI_ImportGeomObjectOp::ImportCreatedAsObstacle, GEOMOp::OpBox );
    break;
  case CreateCylinderId:
    anOp = new HYDROGUI_ImportGeomObjectOp( aModule, 
      HYDROGUI_ImportGeomObjectOp::ImportCreatedAsObstacle, GEOMOp::OpCylinder );
    break;
  case TranslateObstacleId:
    anOp = new HYDROGUI_TranslateObstacleOp( aModule );
    break;
  case CopyViewerPositionId:
    anOp = new HYDROGUI_CopyPastePositionOp( aModule, false );
    break;
  case DeleteId:
    anOp = new HYDROGUI_DeleteOp( aModule );
    break;
  case SetColorId:
    anOp = new HYDROGUI_SetColorOp( aModule );
    break;
  case SetTransparencyId:
    anOp = new HYDROGUI_SetTransparencyOp( aModule );
    break;
  case SetZLevelId:
    anOp = new HYDROGUI_ZLevelsOp( aModule );
    break;
  case EditLocalCSId:
    anOp = new HYDROGUI_LocalCSOp( aModule );
    break;
  case RiverBottomId:
  case RiverBottomContextId:
    anOp = new HYDROGUI_RiverBottomOp( aModule );
    break;
  case RecognizeContoursId:
    anOp = new HYDROGUI_RecognizeContoursOp( aModule );
    break;
  case ShowId:
  case ShowOnlyId:
  case ShowAllId:
  case HideId:
  case HideAllId:
    anOp = new HYDROGUI_ShowHideOp( aModule, theId );
    break;
  case SubmersibleId:
    anOp = new HYDROGUI_SubmersibleOp( aModule, true );
    break;
  case UnSubmersibleId:
    anOp = new HYDROGUI_SubmersibleOp( aModule, false );
    break;
  case PolylineExtractionId:
    anOp = new HYDROGUI_PolylineExtractionOp( aModule );
    break;
  case SplitPolylinesId:
    anOp = new HYDROGUI_SplitPolylinesOp( aModule );
    break;
  case MergePolylinesId:
    anOp = new HYDROGUI_MergePolylinesOp( aModule );
    break;
  case ShowAttrPolylinesId:
    anOp = new HYDROGUI_ShowAttrPolyOp( aModule );
    break;
  case SetBoundaryTypePolygonId:
    anOp = new HYDROGUI_SetBoundaryTypePolygonOp( aModule );
    break;
  case LandCoverScalarMapModeOnId:
  case LandCoverScalarMapModeOffId:
    anOp = new HYDROGUI_LandCoverColoringOp( aModule, theId );
    break;
  case RegenerateRegionColorsId:
    anOp = new HYDROGUI_RegenerateRegionColorsOp( aModule );
    break;
  case ZoneSetColorId:
    anOp = new HYDROGUI_ZoneSetColorOp( aModule );
    break;
  }

  if( !anOp )
    anOp = LightApp_Module::createOperation( theId );

  return anOp;
}

bool HYDROGUI_Module::reusableOperation( const int id )
{
  if ( id == ImportGeomObjectAsObstacleId ||
       id == ImportGeomObjectAsPolylineId ) {
    return false;
  }

  return LightApp_Module::reusableOperation( id );
}

/**
 * Returns true if the object with the given entry can be renamed.
 * @param theEntry the object entry
 */
bool HYDROGUI_Module::renameAllowed( const QString& theEntry ) const
{
  // Allow to rename all HYDRO objects
  Handle(HYDROData_Entity) anEntity = getDataModel()->objectByEntry( theEntry );
  return !anEntity.IsNull();
}
/**
 * Returns true if the object with the given entry is renamed.
 * @param theEntry the object entry
 * @param theName the new name
 */
bool HYDROGUI_Module::renameObject( const QString& theEntry, const QString& theName )
{
  Handle(HYDROData_Entity) anEntity = getDataModel()->objectByEntry( theEntry );
  bool aRes = false;
  if ( !anEntity.IsNull() )
  {
    HYDROGUI_DataModel* aModel = getDataModel();
    if( aModel )
    {
      if( anEntity->GetName() != theName )
      {
        // check that there are no other objects with the same name in the document
        Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( this, theName );
        if ( anObject.IsNull() )
        {
          SUIT_Operation* anOp = application()->activeStudy()->activeOperation();
          if ( anOp && anOp->inherits( "HYDROGUI_CalculationOp" ) )
          {
            anEntity->SetName( theName );
            aRes = true;
          }
          else
          {
            aRes = aModel->rename( anEntity, theName );
          }
        }
        else
        {
          // Inform the user that the name is already used
          QString aTitle = QObject::tr( "INSUFFICIENT_INPUT_DATA" );
          QString aMessage = QObject::tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( theName );
          SUIT_MessageBox::critical( getApp()->desktop(), aTitle, aMessage );
        }
      }
    }
  }
  return aRes;
}

void HYDROGUI_Module::onBathymetrySelection()
{
  QAction* a = qobject_cast<QAction*>( sender() );
  if( !a )
    return;

  bool isChecked = a->isChecked();
  if( isChecked )
    startOperation( BathymetrySelectionId );
  else
  {
    LightApp_Operation* op = operation( BathymetryTextId );
    if( op )
      op->abort();

    op = operation( BathymetrySelectionId );
    if( op )
      op->abort();
  }
}

void HYDROGUI_Module::onBathymetryText()
{
  QAction* a = qobject_cast<QAction*>( sender() );
  if( !a )
    return;

  bool isChecked = a->isChecked();
  if( isChecked )
    startOperation( BathymetryTextId );
  else
  {
    HYDROGUI_BathymetryOp* op = dynamic_cast<HYDROGUI_BathymetryOp*>( operation( BathymetryTextId ) );
    if( op )
      op->commit();
  }
}

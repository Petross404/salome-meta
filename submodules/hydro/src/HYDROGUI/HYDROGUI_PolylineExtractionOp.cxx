// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_PolylineExtractionOp.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_UpdateFlags.h>
#include <HYDROData_Document.h>
#include <HYDROData_PolylineOperator.h>

HYDROGUI_PolylineExtractionOp::HYDROGUI_PolylineExtractionOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "POLYLINE_EXTRACTION" ) );
}

HYDROGUI_PolylineExtractionOp::~HYDROGUI_PolylineExtractionOp()
{
}

void HYDROGUI_PolylineExtractionOp::startOperation()
{
    HYDROGUI_Operation::startOperation();

    int anUpdateFlags = UF_ObjBrowser | UF_Model;
    HYDROGUI_Module* aModule = module();

    HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects( aModule );
    if ( aSeq.IsEmpty() )
        return;

    startDocOperation();
    HYDROData_PolylineOperator anOp;
    for ( Standard_Integer anIndex = 1, aLength = aSeq.Length(); anIndex <= aLength; anIndex++ )
    {
      Handle(HYDROData_Object) anObject = Handle(HYDROData_Object)::DownCast( aSeq.Value( anIndex ) );
      if( !anObject.IsNull() )
        anOp.Extract( doc(), anObject );      
    }

    commitDocOperation();
    aModule->update( anUpdateFlags );
    commit();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_GeomObjectDlg_H
#define HYDROGUI_GeomObjectDlg_H

#include "HYDROGUI_InputPanel.h"

class QLineEdit;
class QGroupBox;
class QComboBox;
class QButtonGroup;

class HYDROGUI_GeomObjectDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

  enum CreationMode { CreateNewId, ModifyExistentId };

public:
  HYDROGUI_GeomObjectDlg( HYDROGUI_Module* theModule, const QString& theTitle,
                          const QString& theObjectTypeName,
                          const bool theIsToEnableFileSelection = false );
  virtual ~HYDROGUI_GeomObjectDlg();

  void                       reset();

  QString                    getObjectName() const;

  void                       setObjectNames( const QStringList& theObstacles );
  QString                    getEditedObjectName() const;

  QString                    getFileName() const;

  QString                    getDefaultName();
  void                       setDefaultName( const QString& theName );

protected slots:
  void                       onModeActivated( int );
  void                       onObjectSelectionChanged();
  void                       onBrowse();

private:
  void                       updateControls();
  void                       updateObjectName();

private:
  QLineEdit*                 myFileName;
  
  QGroupBox*                 myNameGroup;
  QLineEdit*                 myObjectName;

  QGroupBox*                 myModeGroup;
  QButtonGroup*              myModeButtons;

  QGroupBox*                 myObjectsGroup;
  QComboBox*                 myObjects;

  bool                       myFileSelectionEnabled;
  QString                    myDefaultName;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ColorDlg.h"

#include "HYDROGUI_ColorWidget.h"
#include "HYDROGUI_Tool.h"

#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QRadioButton>
#include <QPushButton>

HYDROGUI_ColorDlg::HYDROGUI_ColorDlg( QWidget* theParent, bool theIsOneColor )
: QDialog( theParent ),
  myIsOneColor( theIsOneColor )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin( 5 );
  aMainLayout->setSpacing( 5 );

  if ( !theIsOneColor )
  {
    // Filling color
    QFrame* aFillingFrame = new QFrame( this );
    QLabel* aFillingLabel = new QLabel( tr( "FILLING_COLOR" ), aFillingFrame );
    myFillingTransparent = new QRadioButton( tr( "TRANSPARENT" ), aFillingFrame );
    myFillingTransparent->setChecked( true );
    myFillingColor = new QRadioButton( tr( "COLOR" ), aFillingFrame );
    myFillingColorBox = new HYDROGUI_ColorWidget( aFillingFrame );

    QGridLayout* aFillingLayout = new QGridLayout( aFillingFrame );
    aFillingLayout->setMargin( 5 );
    aFillingLayout->setSpacing( 5 );
    aFillingLayout->addWidget( aFillingLabel, 0, 0, 2, 1 );
    aFillingLayout->addWidget( myFillingTransparent, 0, 1 );
    aFillingLayout->addWidget( myFillingColor,       1, 1 );
    aFillingLayout->addWidget( myFillingColorBox,    1, 2 );

    // Border color
    myBorderColorGroup = new QGroupBox( tr( "BORDER_COLOR" ), this );
    myBorderColorGroup->setCheckable( true );

    myBorderColorBox = new HYDROGUI_ColorWidget( myBorderColorGroup );

    QBoxLayout* aBorderColorLayout = new QHBoxLayout( myBorderColorGroup );
    aBorderColorLayout->setMargin( 5 );
    aBorderColorLayout->setSpacing( 5 );
    aBorderColorLayout->addWidget( new QLabel( tr( "COLOR" ), myBorderColorGroup ) );
    aBorderColorLayout->addWidget( myBorderColorBox );

    aMainLayout->addWidget( aFillingFrame );
    aMainLayout->addWidget( myBorderColorGroup );

    connect( myFillingTransparent, SIGNAL( toggled( bool ) ), 
             myFillingColorBox, SLOT( setDisabled( bool ) ) );
  }
  else
  {
    QFrame* aColorFrame = new QFrame( this );
    QLabel* aColorLabel = new QLabel( tr( "OBJECT_COLOR" ), aColorFrame );
    myColorBox = new HYDROGUI_ColorWidget( aColorFrame );
    myColorBox->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding ) ); 
    myColorBox->setFixedHeight( 20 );

    QBoxLayout* aColorLayout = new QHBoxLayout( aColorFrame );
    aColorLayout->setMargin( 10 );
    aColorLayout->setSpacing( 5 );
    aColorLayout->addWidget( aColorLabel );
    aColorLayout->addWidget( myColorBox );

    aMainLayout->addWidget( aColorFrame );
  }

  // Buttons
  QPushButton* anOkButton = new QPushButton( tr( "OK" ), this );
  anOkButton->setDefault( true ); 
  QPushButton* aCancelButton = new QPushButton( tr( "CANCEL" ), this );
  aCancelButton->setAutoDefault( true );

  QHBoxLayout* aButtonsLayout = new QHBoxLayout;
  aButtonsLayout->setMargin( 5 );
  aButtonsLayout->setSpacing( 5 );
  aButtonsLayout->addWidget( anOkButton );
  aButtonsLayout->addStretch();
  aButtonsLayout->addWidget( aCancelButton );

  // Common
  aMainLayout->addStretch();
  aMainLayout->addLayout( aButtonsLayout );

  setLayout( aMainLayout );  

  // Connect signals and slots
  connect( anOkButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( aCancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );

  setFixedSize( 300, theIsOneColor ? 90 : 150 );
}

HYDROGUI_ColorDlg::~HYDROGUI_ColorDlg()
{
}

void HYDROGUI_ColorDlg::setFirstColor( const QColor& theColor )
{
  if ( !myIsOneColor )
  {
    if( theColor.alpha() == 0 ) // transparent
      myFillingTransparent->setChecked( true );
    else
      myFillingColor->setChecked( true );

    myFillingColorBox->setColor( theColor );
  }
  else
  {
    myColorBox->setColor( theColor );
  }
}

QColor HYDROGUI_ColorDlg::getFirstColor() const
{
  QColor aColor;
  if ( !myIsOneColor )
  {
    aColor = QColor( 255, 255, 255, 0 ); // transparent
    if( myFillingColor->isChecked() )
      aColor = myFillingColorBox->color();
  }
  else
  {
    aColor = myColorBox->color();
  }

  return aColor;
}

void HYDROGUI_ColorDlg::setSecondColor( const QColor& theColor )
{
  if ( myIsOneColor )
    return;

  bool isTransparent = theColor.alpha() == 0;
  myBorderColorGroup->setChecked( !isTransparent );
  myBorderColorBox->setColor( !isTransparent ? theColor : QColor( Qt::black ) );
}

QColor HYDROGUI_ColorDlg::getSecondColor() const
{
  QColor aColor( Qt::transparent ); // transparent
 
  if ( myIsOneColor )
    return aColor;

  if( myBorderColorGroup->isChecked() )
    aColor = myBorderColorBox->color();
  
  return aColor;
}



// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DuplicateOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"

HYDROGUI_DuplicateOp::HYDROGUI_DuplicateOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "DUPLICATE" ) );
}


HYDROGUI_DuplicateOp::~HYDROGUI_DuplicateOp()
{
}

void HYDROGUI_DuplicateOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_DataModel* aModel = module()->getDataModel();

  bool anIsOk = false;
  int aFlags = 0;

  // Copy object
  anIsOk = aModel->copy();
  aFlags = UF_Controls;
  if( !anIsOk )
  {
    abort();
    return;
  }

  // Paste object
  startDocOperation();

  anIsOk = aModel->paste();
  aFlags = UF_Controls | UF_Model;

  if( anIsOk )
    commitDocOperation();
  else
    abortDocOperation();

  if( anIsOk )
  {
    module()->update( aFlags );
    commit();
  }
  else
    abort();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_OCCDISPLAYER_H
#define HYDROGUI_OCCDISPLAYER_H

#include "HYDROGUI_AbstractDisplayer.h"

class HYDROGUI_Shape;
class AIS_InteractiveContext;
class AIS_ColorScale;
class OCCViewer_ViewManager;
class OCCViewer_Viewer;

/**
 * \class HYDROGUI_OCCDisplayer
 * \brief Class intended to create, display and update the presentations on OCC viewer.
 */
class HYDROGUI_OCCDisplayer : public HYDROGUI_AbstractDisplayer
{
public:
  /**
   * \brief Constructor.
   * \param theModule module object
   */
  HYDROGUI_OCCDisplayer( HYDROGUI_Module* theModule );

  /**
   * \brief Destructor.
   */
  virtual ~HYDROGUI_OCCDisplayer();

public:
  /**
   * \brief Force the specified objects to be updated.
   * \param theObjs sequence of objects to update
   * \param theViewerId viewer identifier
   */
  void                            SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                               const size_t theViewerId );

  /**
   * \brief Get the applicable viewer type.
   */
  virtual QString  GetType() const;

  /**
   * \brief Add the z layer for preview and returns its index.
   * \param theMgr OCC view manager
   */
  int                             AddPreviewZLayer( OCCViewer_ViewManager* theMgr );

  /**
   * \brief Removes the z layer.
   * \param theMgr OCC view manager
   * \param theLayer a layer index
   */
  void                            RemoveZLayer( OCCViewer_ViewManager* theMgr,
                                                const int theLayer,
                                                bool isClearAll = false);

  void SetToUpdateColorScale();

  /**
   * \brief Get color scale for the viewer.
   * \param theViewerId viewer identifier
   * \return the color scale 
   */
  Handle(AIS_ColorScale) GetColorScale( const size_t theViewerId );


  void UpdatePolylines( size_t theViewerId, int theType, int theSize );

  void UpdateColorScale( const OCCViewer_Viewer* );

protected:
  /**
   * \brief Erase all viewer objects.
   * \param theViewerId viewer identifier
   */
  void                            EraseAll( const size_t theViewerId );

  /**
   * \brief Erase the specified viewer objects.
   * \param theObjs sequence of objects to erase
   * \param theViewerId viewer identifier
   */
  void                            Erase( const HYDROData_SequenceOfObjects& theObjs,
                                         const size_t theViewerId );

  /**
   * \brief Display the specified viewer objects.
   * \param theObjs sequence of objects to display
   * \param theViewerId viewer identifier
   * \param theIsForced flag used to update all objects, including the unchanged ones
   * \param theDoFitAll flag used to fit the view to all visible objects; do not fit by default
   */
  void                            Display( const HYDROData_SequenceOfObjects& theObjs,
                                           const size_t theViewerId,
                                           const bool theIsForced,
                                           const bool theDoFitAll );
  /**
   * \brief Purge all invalid objects in the viewer.
   * \param theViewerId viewer identifier
   */
  void                            purgeObjects( const size_t theViewerId );
  
private:
  /**
   * \brief Creates new OCC shape.
   * \param theViewerId viewer identifier
   * \param theContext context of OCC viewer
   * \param theObject data model object
   * \return pointer to new created shape
   */
  HYDROGUI_Shape*                 createShape( const size_t                          theViewerId,
                                               const Handle(AIS_InteractiveContext)& theContext,
                                               const Handle(HYDROData_Entity)&       theObject );

   /**
   * \brief Display the specified object.
   * \param theObject the object to display
   * \param theViewer the viewer for displaying
   * \param theIsForced the flag used to update the object shape
   * \return true in case of success
   */
  bool                            Display( const Handle(HYDROData_Entity)& theObject,
                                           const OCCViewer_Viewer* theViewer,
                                           const bool theIsForced );

   /**
   * \brief Set Z layer to the shape corresponding to the HYDRO data object.
   * \param theViewer the viewer for Z layer setting
   * \param theObject the HYDRO object
   * \param theZLayerId the Z layer ID
   */
  void                            SetZLayer( const OCCViewer_Viewer* theViewer,
                                             const Handle(HYDROData_Entity)& theObject, 
                                             const int theZLayerId );

private:
  bool myToUpdateColorScale;
  
  QMap<size_t, Handle(AIS_ColorScale)> myColorScales;
};

#endif


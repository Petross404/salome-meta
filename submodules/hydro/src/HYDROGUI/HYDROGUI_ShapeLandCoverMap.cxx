// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ShapeLandCoverMap.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_LandCoverMapPrs.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Module.h"

#include <LightApp_Application.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewWindow.h>
#include <OCCViewer_ViewPort3d.h>

#include <HYDROData_LandCoverMap.h>

#include <AIS_InteractiveContext.hxx>

HYDROGUI_ShapeLandCoverMap::HYDROGUI_ShapeLandCoverMap( HYDROGUI_OCCDisplayer* theDisplayer,
                                                        const Handle(AIS_InteractiveContext)& theContext,
                                                        const Handle(HYDROData_LandCoverMap)& theLandCoverMap,
                                                        const int                             theZLayer,
                                                        const bool                            theIsScalarMode )
: HYDROGUI_Shape( theContext, theLandCoverMap, theZLayer ),
  myDisplayer( theDisplayer ),
  myIsScalarMapMode( theIsScalarMode )
{
  setFillingColor( QColor(), false, false );
}

HYDROGUI_ShapeLandCoverMap::~HYDROGUI_ShapeLandCoverMap()
{
#ifndef LIGHT_MODE
  if( myDisplayer )
    myDisplayer->SetToUpdateColorScale();
#endif
}

void HYDROGUI_ShapeLandCoverMap::update( bool theIsUpdateViewer, bool isDeactivateSelection )
{
  setIsToUpdate( false );

  Handle(HYDROData_LandCoverMap) aLandCoverMapObj =
    Handle(HYDROData_LandCoverMap)::DownCast( getObject() );

  TopoDS_Shape aLandCoverMapShape = aLandCoverMapObj->GetShape();
  if ( !aLandCoverMapShape.IsNull() ) {
    setShape( aLandCoverMapShape, false, false );
  }

  Handle(HYDROGUI_LandCoverMapPrs) aLandCoverMapPrs = 
    Handle(HYDROGUI_LandCoverMapPrs)::DownCast( getAISObjects()[0] );
  if ( !aLandCoverMapPrs.IsNull() )
    aLandCoverMapPrs->UpdateColors();

  HYDROGUI_Shape::update( theIsUpdateViewer, isDeactivateSelection );
}

bool HYDROGUI_ShapeLandCoverMap::isScalarMapModeEnabled() const
{
  return myIsScalarMapMode;
}

void HYDROGUI_ShapeLandCoverMap::setScalarMapModeEnabled( const bool theIsToEnable )
{
  myIsScalarMapMode = theIsToEnable;
  update( false, false );
}

void HYDROGUI_ShapeLandCoverMap::setVisible( const bool theState,
                                             const bool theIsUpdateViewer )
{
  HYDROGUI_Shape::setVisible( theState, theIsUpdateViewer );
  myDisplayer->SetToUpdateColorScale();
}

void HYDROGUI_ShapeLandCoverMap::displayShape( const bool theIsUpdateViewer )
{
  HYDROGUI_Shape::displayShape( theIsUpdateViewer );
  myDisplayer->SetToUpdateColorScale();
}

QList<Handle(AIS_InteractiveObject)> HYDROGUI_ShapeLandCoverMap::createShape() const
{
  QList<Handle(AIS_InteractiveObject)> shapes;

  Handle(HYDROData_LandCoverMap) aLandCoverMap = Handle(HYDROData_LandCoverMap)::DownCast( getObject() );
  if ( !aLandCoverMap.IsNull() )
  {
    Handle(HYDROGUI_LandCoverMapPrs) aLandCoverMapPrs = new HYDROGUI_LandCoverMapPrs( aLandCoverMap );    
    if ( myIsScalarMapMode )
    {
      LightApp_Application* anApp = myDisplayer->module()->getApp();
      OCCViewer_ViewManager* aViewManager = ::qobject_cast<OCCViewer_ViewManager*>( 
        anApp->getViewManager( OCCViewer_Viewer::Type(), true ) );
      if ( aViewManager )
      {
        if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
        {
          size_t aViewerId = (size_t)aViewer;

          // Set Strickler table
          Handle(HYDROData_StricklerTable) aTable = 
            myDisplayer->module()->getLandCoverColoringTable( aViewerId );
          if( !aTable.IsNull() )
            aLandCoverMapPrs->SetTable( aTable );

          // Set color scale
          aLandCoverMapPrs->SetColorScale( myDisplayer->GetColorScale( aViewerId) );
        }
      }
    }
    shapes.append( aLandCoverMapPrs );
  }
  return shapes;
}

void HYDROGUI_ShapeLandCoverMap::display( const bool theIsUpdateViewer )
{
  HYDROGUI_Shape::display( theIsUpdateViewer );
  myDisplayer->SetToUpdateColorScale();
}

void HYDROGUI_ShapeLandCoverMap::erase( const bool theIsUpdateViewer )
{
  HYDROGUI_Shape::erase( theIsUpdateViewer );
  myDisplayer->SetToUpdateColorScale();
}

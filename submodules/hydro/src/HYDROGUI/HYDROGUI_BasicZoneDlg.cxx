// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_BasicZoneDlg.h"

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>

HYDROGUI_BasicZoneDlg::HYDROGUI_BasicZoneDlg( HYDROGUI_Module* theModule, const QString& theTitle,
                                              const QString& theLabel1, const QString& theLabel2,
                                              const QString& theLabel3, const QString& theLabel4 )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  // Zone name
  myObjectNameGroup = new QGroupBox( theLabel1, mainFrame() );

  myObjectName = new QLineEdit( myObjectNameGroup );

  QBoxLayout* aNameLayout = new QHBoxLayout( myObjectNameGroup );
  aNameLayout->setMargin( 5 );
  aNameLayout->setSpacing( 5 );
  aNameLayout->addWidget( new QLabel( theLabel2, myObjectNameGroup ) );
  aNameLayout->addWidget( myObjectName );

  // Main parameters: group-box and frame
  myParamGroup = new QGroupBox( theLabel3, mainFrame() );

  myPolylineFrame = new QFrame( myParamGroup );

  // Additional parameter defined with help of combo-box control
  myAdditionalParamsGroup = new QGroupBox( theLabel4, mainFrame() );

  myAdditionalParams = new QComboBox( myPolylineFrame );
  myAdditionalParams->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  QBoxLayout* aBathLayout = new QHBoxLayout( myAdditionalParamsGroup );
  aBathLayout->setMargin( 5 );
  aBathLayout->setSpacing( 5 );
  aBathLayout->addWidget( myAdditionalParams );

  // Common
  addWidget( myObjectNameGroup );
  addWidget( myParamGroup );
  addWidget( myAdditionalParamsGroup );

  addStretch();  
}

HYDROGUI_BasicZoneDlg::~HYDROGUI_BasicZoneDlg()
{
}

void HYDROGUI_BasicZoneDlg::reset()
{
  myObjectName->clear();
  myAdditionalParams->clear();  
}

void HYDROGUI_BasicZoneDlg::setObjectName( const QString& theName )
{
  myObjectName->setText( theName );
}

QString HYDROGUI_BasicZoneDlg::getObjectName() const
{
  return myObjectName->text();
}

void HYDROGUI_BasicZoneDlg::setAdditionalParams( const QStringList& thePrams )
{
  bool isBlocked = blockSignals( true );

  myAdditionalParams->clear();
  myAdditionalParams->addItems( thePrams );

  blockSignals( isBlocked );
}

void HYDROGUI_BasicZoneDlg::setSelectedAdditionalParamName( const QString& theParam )
{
  int aNewIdx = myAdditionalParams->findText( theParam );
  myAdditionalParams->setCurrentIndex( aNewIdx );
}

QString HYDROGUI_BasicZoneDlg::getSelectedAdditionalParamName() const
{
  return myAdditionalParams->currentText();
}

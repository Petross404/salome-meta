// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Actor.h>
#include <GEOM_PainterPolyDataMapper.h>
#include <GEOM_DeviceActor.h>
#include <GEOM_VertexSource.h>
#include <GEOM_EdgeSource.h>
#include <GEOM_WireframeFace.h>
#include <GEOM_ShadingFace.h>
#include <VTKViewer_TransformFilter.h>
#include <vtkAppendPolyData.h>
#include <VTKViewer_Transform.h>

HYDROGUI_Actor::HYDROGUI_Actor()
{ 
  myVertexActor->GetDeviceActor()->SetMapper( myVertexActor->GetDeviceActor()->GetMapper() );
  myIsolatedEdgeActor->GetDeviceActor()->SetMapper( myIsolatedEdgeActor->GetDeviceActor()->GetMapper() );
  myOneFaceEdgeActor->GetDeviceActor()->SetMapper( myOneFaceEdgeActor->GetDeviceActor()->GetMapper() );
  mySharedEdgeActor->GetDeviceActor()->SetMapper( mySharedEdgeActor->GetDeviceActor()->GetMapper() );
  myWireframeFaceActor->GetDeviceActor()->SetMapper( myWireframeFaceActor->GetDeviceActor()->GetMapper() );
  myShadingFaceActor->SetInput(myShadingFaceSource->GetOutputPort(),false); 
  myShadingFaceActor->GetDeviceActor()->SetMapper( myShadingFaceActor->GetDeviceActor()->GetMapper() );
  GEOM_Actor::myHighlightActor->GetDeviceActor()->SetMapper( GEOM_Actor::myHighlightActor->GetDeviceActor()->GetMapper() );
} 
 
 
HYDROGUI_Actor::~HYDROGUI_Actor() 
{ 
} 

HYDROGUI_Actor* HYDROGUI_Actor::New()
{
  HYDROGUI_Actor* anObject = new HYDROGUI_Actor(); 
  anObject->SetMapper( anObject->myPolyDataMapper.Get()); 
  return anObject; 
}

void HYDROGUI_Actor::SetMapper( vtkMapper* theMapper )
{
  SALOME_Actor::SetMapper( theMapper );
}

void HYDROGUI_Actor::SetTransform( VTKViewer_Transform* theTransform )
{
  Superclass::SetTransform(theTransform);
  myVertexActor       ->GetDeviceActor()->SetTransform( theTransform );
  myIsolatedEdgeActor ->GetDeviceActor()->SetTransform( theTransform );
  myOneFaceEdgeActor  ->GetDeviceActor()->SetTransform( theTransform );
  mySharedEdgeActor   ->GetDeviceActor()->SetTransform( theTransform );
  myWireframeFaceActor->GetDeviceActor()->SetTransform( theTransform );
  myShadingFaceActor  ->GetDeviceActor()->SetTransform( theTransform );
  GEOM_Actor::myHighlightActor->GetDeviceActor()->SetTransform( theTransform );
}

void HYDROGUI_Actor::SetShape( const TopoDS_Shape& theShape,
                               float theDeflection,
                               bool theIsVector )
{
  GEOM_Actor::SetShape( theShape, theDeflection, theIsVector ); 

  if( myIsolatedEdgeSource->IsEmpty() )
    myIsolatedEdgeActor->GetDeviceActor()->SetInfinitive( true );

  if( myOneFaceEdgeSource->IsEmpty() )
    myOneFaceEdgeActor->GetDeviceActor()->SetInfinitive( true );

  if( mySharedEdgeSource->IsEmpty() )
    mySharedEdgeActor->GetDeviceActor()->SetInfinitive( true );

  if( myWireframeFaceSource->IsEmpty() )
    myWireframeFaceActor->GetDeviceActor()->SetInfinitive( true );

  if( myShadingFaceSource->IsEmpty() )
  {
    myShadingFaceActor->GetDeviceActor()->SetInfinitive( true );
  }
  else
  {
    myShadingFaceSource->Update();
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_LineEditDoubleValidator.h"

#include <QLineEdit>

HYDROGUI_LineEditDoubleValidator::HYDROGUI_LineEditDoubleValidator( QObject* theParent )
 : QItemDelegate( theParent )
{
}

QWidget* HYDROGUI_LineEditDoubleValidator::createEditor( 
  QWidget* theParent, const QStyleOptionViewItem& theOption,
  const QModelIndex& theIndex ) const
{
  QWidget* anEditor = 0;

  if ( theIndex.column() > 0 ) {
    QLineEdit* aLineEdit = new QLineEdit( theParent );
    QDoubleValidator* aDoubleValidator = new QDoubleValidator();
    aDoubleValidator->setNotation( QDoubleValidator::StandardNotation );
    aDoubleValidator->setDecimals( 2 );
    aLineEdit->setValidator( aDoubleValidator );
    anEditor = aLineEdit;
  } else {
    anEditor = QItemDelegate::createEditor( theParent, theOption, theIndex );
  }

  return anEditor;
}

void HYDROGUI_LineEditDoubleValidator::setEditorData( 
  QWidget* theEditor, const QModelIndex& theIndex ) const
{
  if ( QLineEdit* aLineEdit = dynamic_cast<QLineEdit*>( theEditor ) ) {
    aLineEdit->setText( theIndex.data( Qt::EditRole ).toString() );
  } else {
    QItemDelegate::setEditorData( theEditor, theIndex );
  }
}

void HYDROGUI_LineEditDoubleValidator::setModelData( 
  QWidget* theEditor, QAbstractItemModel* theModel, const QModelIndex& theIndex) const
{
  if ( QLineEdit* aLineEdit = dynamic_cast<QLineEdit*>( theEditor ) ) {
    theModel->setData( theIndex, aLineEdit->text() );
  } else {
    QItemDelegate::setModelData( theEditor, theModel, theIndex );
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ObjListBox.h>
#include <HYDROGUI_ObjComboBox.h>
#include <HYDROGUI_Tool2.h>
#include <QGridLayout>
#include <QListWidget>
#include <QLabel>
#include <QPushButton>

HYDROGUI_ObjListBox::HYDROGUI_ObjListBox( HYDROGUI_Module* theModule, const QString& theTitle, const ObjectKind& theType, QWidget* theParent )
: QWidget( theParent ),
  myModule( theModule ),
  myFilter( 0 )
{
  myTypes.append( theType );
  Init(theTitle);
}

HYDROGUI_ObjListBox::HYDROGUI_ObjListBox( HYDROGUI_Module* theModule, const QString& theTitle, const QList<ObjectKind>& theTypes, QWidget* theParent )
: QWidget( theParent ),
  myTypes( theTypes ),
  myModule( theModule ),
  myFilter( 0 )
{
  Init(theTitle);
}

void HYDROGUI_ObjListBox::Init(const QString& theTitle)
{
  QGridLayout* aBase = new QGridLayout( this );
  aBase->setMargin( 0 );

  QPushButton* anInclude = new QPushButton( tr( "INCLUDE" ), this );
  QPushButton* anExclude = new QPushButton( tr( "EXCLUDE" ), this );

  aBase->addWidget( anInclude, 0, 0 );
  aBase->addWidget( anExclude, 0, 1 );
  if( !theTitle.isEmpty() )
    aBase->addWidget( new QLabel( theTitle, this ), 1, 0 );
  aBase->addWidget( myList = new QListWidget( this ), 1, 1, 1, 2 );
  aBase->setColumnStretch( 0, 0 );
  aBase->setColumnStretch( 1, 0 );
  aBase->setColumnStretch( 2, 1 );

  myList->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred ) );
  myList->setSelectionMode( QListWidget::ExtendedSelection );

  connect( anInclude, SIGNAL( clicked() ), this, SLOT( OnInclude() ) );
  connect( anExclude, SIGNAL( clicked() ), this, SLOT( OnExclude() ) );
}

HYDROGUI_ObjListBox::~HYDROGUI_ObjListBox()
{
}

HYDROGUI_Module* HYDROGUI_ObjListBox::module() const
{
    return myModule;
}

QList<ObjectKind> HYDROGUI_ObjListBox::objectTypes() const
{
    return myTypes;
}

HYDROGUI_ObjComboBoxFilter* HYDROGUI_ObjListBox::objectFilter() const
{
    return myFilter;
}

void HYDROGUI_ObjListBox::setObjectFilter( HYDROGUI_ObjComboBoxFilter* filter )
{
    myFilter = filter;
}

void HYDROGUI_ObjListBox::reset()
{
  myList->clear();
  mySelection.Clear();
  emit selectionChanged();
}

void HYDROGUI_ObjListBox::setSelectedObjects( const HYDROData_SequenceOfObjects& theObjects )
{
  reset();
  Append( theObjects );
}

void HYDROGUI_ObjListBox::setObjectsFromSelection()
{
  setSelectedObjects( HYDROGUI_Tool::GetSelectedObjects( module() ) );
}

HYDROData_SequenceOfObjects HYDROGUI_ObjListBox::selectedObjects() const
{
  return mySelection;
}

void HYDROGUI_ObjListBox::OnInclude()
{
  Append( HYDROGUI_Tool::GetSelectedObjects( module() ) );
  emit selectionChanged();
}

void HYDROGUI_ObjListBox::OnExclude()
{
  QList<QListWidgetItem*> aSelection = myList->selectedItems();
  foreach( QListWidgetItem* anItem, aSelection )
  {
    QString itemText = anItem->text();
    for (int i=mySelection.Lower();i<=mySelection.Upper();i++)
    {
      QString name = mySelection(i)->GetName();
      if (itemText == name)
      {
        mySelection.Remove(i);
        break;
      }
    }
    delete anItem;
  }
  emit selectionChanged();
}

void HYDROGUI_ObjListBox::Append( const HYDROData_SequenceOfObjects& theObjects )
{
  for( int i=theObjects.Lower(), n=theObjects.Upper(); i<=n; i++ )
  {
    Handle( HYDROData_Entity ) anObject = theObjects.Value( i );
    bool isOK = !anObject.IsNull() && myTypes.contains(anObject->GetKind());
    if( myFilter && isOK )
      isOK = myFilter->isOk( anObject );

    if( isOK )
    {
      QString aName = anObject->GetName();
      if (myList->findItems(aName, Qt::MatchExactly).size() == 0)
      {
        myList->addItem( aName );
        mySelection.Append( anObject );
      }
    }
  }
}

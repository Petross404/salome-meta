// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_LocalCSDlg.h>
#include <QtxDoubleSpinBox.h>
#include <QLayout>
#include <QLabel>

const double RANGE = 1E+10;
const double STEP = 1.00;
const double PREC = 2;

HYDROGUI_LocalCSDlg::HYDROGUI_LocalCSDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  QFrame* aFrame = new QFrame( mainFrame() );
  addWidget( aFrame, 1 );

  QGridLayout* aLayout = new QGridLayout( aFrame );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  myLX = new QtxDoubleSpinBox( 0, RANGE, STEP, PREC, PREC, aFrame );
  myLX->setValue( 0.0 );
  myLY = new QtxDoubleSpinBox( 0, RANGE, STEP, PREC, PREC, aFrame );
  myLY->setValue( 0.0 );

  aLayout->addWidget( new QLabel( tr( "LX" ), aFrame ), 0, 0 );
  aLayout->addWidget( myLX, 0, 1 );
  aLayout->addWidget( new QLabel( tr( "LY" ), aFrame ), 1, 0 );
  aLayout->addWidget( myLY, 1, 1 );
  aLayout->setColumnStretch( 0, 0 );
  aLayout->setColumnStretch( 1, 1 );
  aLayout->setRowStretch( 2, 1 );
}

HYDROGUI_LocalCSDlg::~HYDROGUI_LocalCSDlg()
{
}

double HYDROGUI_LocalCSDlg::GetLocalX() const
{
  return myLX->value();
}

double HYDROGUI_LocalCSDlg::GetLocalY() const
{
  return myLY->value();
}

void HYDROGUI_LocalCSDlg::SetLocalCS( double theLX, double theLY )
{
  myLX->setValue( theLX );
  myLY->setValue( theLY );
}


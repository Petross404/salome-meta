// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_DataModel.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_Prs.h>
#include <HYDROData_Document.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Tool.h>

#include <GeometryGUI.h>
#include <GEOMBase.h>

#include <SalomeApp_Study.h>
#include <LightApp_Application.h>
#include <LightApp_DataOwner.h>
#include <LightApp_SelectionMgr.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewFrame.h>
#include <SVTK_ViewModel.h>
#include <GraphicsView_Viewer.h>
#include <GraphicsView_ViewPort.h>
#include <STD_TabDesktop.h>
#include <SUIT_Session.h>
#include <SUIT_ViewManager.h>
#include <SUIT_ViewWindow.h>
#include <QtxActionToolMgr.h>
#include <QtxWorkstack.h>

int HYDROGUI_Tool::GetActiveStudyId()
{
  if( SUIT_Session* aSession = SUIT_Session::session() )
    if( SUIT_Application* anApp = aSession->activeApplication() )
      if( SUIT_Study* aStudy = anApp->activeStudy() )
        return aStudy->id();
  return 0;
}

static bool GetCalCaseSubT(const QString& aHydroPref, 
  const QString& anEntry, 
  const QString& SubTPostFix,
  HYDROGUI_DataModel* aModel,
  Handle(HYDROData_CalculationCase)& theOutCalCase)
{
  int aFiB = anEntry.lastIndexOf(SubTPostFix, -1, Qt::CaseInsensitive);
  if (aFiB != -1)
  {
    QString RightTruncEntry = anEntry.left(anEntry.length() - SubTPostFix.length());
    Handle(HYDROData_CalculationCase) CalCase = 
      Handle(HYDROData_CalculationCase)::DownCast (aModel->objectByEntry( RightTruncEntry, KIND_CALCULATION ));
    if (CalCase)
    {
      theOutCalCase = CalCase;
      return true;
    }
    else
      return false;
  }
  return false;
}

void HYDROGUI_Tool::SetActiveViewManager( HYDROGUI_Module* theModule,
                                          SUIT_ViewManager* theViewManager )
{
  if( theViewManager )
    if( SUIT_ViewWindow* aViewWindow = theViewManager->getActiveView() )
      if( STD_TabDesktop* aTabDesktop = dynamic_cast<STD_TabDesktop*>( theModule->getApp()->desktop() ) )
        if( QtxWorkstack* aWorkstack = aTabDesktop->workstack() )
          aWorkstack->setActiveWindow( aViewWindow );
}


bool HYDROGUI_Tool::IsObjectHasPresentation( const Handle(HYDROData_Entity)& theObject,
                                             const QString&                  theViewerType )
{
  if ( theObject.IsNull() )
    return false;

  ObjectKind anObjectKind = theObject->GetKind();
  if ( theViewerType.isEmpty() || theViewerType == OCCViewer_Viewer::Type() )
  {
    if ( anObjectKind == KIND_IMAGE ||
         anObjectKind == KIND_POLYLINEXY ||
         anObjectKind == KIND_POLYLINE ||
         anObjectKind == KIND_IMMERSIBLE_ZONE ||
         anObjectKind == KIND_REGION ||
         anObjectKind == KIND_ZONE ||
         anObjectKind == KIND_OBSTACLE ||
         anObjectKind == KIND_PROFILE ||
         anObjectKind == KIND_STREAM ||
         anObjectKind == KIND_CHANNEL ||
         anObjectKind == KIND_DIGUE ||
         anObjectKind == KIND_DUMMY_3D || 
         anObjectKind == KIND_BATHYMETRY ||
         anObjectKind == KIND_BC_POLYGON ||
         anObjectKind == KIND_LAND_COVER_MAP
#ifdef DEB_GROUPS
         || anObjectKind == KIND_SHAPES_GROUP ||
         anObjectKind == KIND_SPLIT_GROUP
#endif
         )
    {
      return true;
    }
  }
  
  if ( theViewerType.isEmpty() || theViewerType == SVTK_Viewer::Type() )
  {
    if ( anObjectKind == KIND_BATHYMETRY )
      return true;
  }

  if ( theViewerType.isEmpty() || theViewerType == GraphicsView_Viewer::Type() )
  {
    if ( anObjectKind == KIND_IMAGE ||
         anObjectKind == KIND_POLYLINEXY )
      return true;
  }

  return false;
}

void HYDROGUI_Tool::GetPrsSubObjects( HYDROGUI_Module* theModule,
                                      HYDROData_SequenceOfObjects& theSeq )
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if( !aDocument.IsNull() )
  {
    HYDROData_Iterator anIterator( aDocument );
    for( ; anIterator.More(); anIterator.Next() )
    {
      Handle(HYDROData_Entity) anObject = anIterator.Current();
      if ( !IsObjectHasPresentation( anObject ) )
        continue;

      theSeq.Append( anObject );
    }
  }
}

HYDROGUI_Prs* HYDROGUI_Tool::GetPresentation( const Handle(HYDROData_Entity)& theObj,
                                              const GraphicsView_ObjectList& theObjects )
{
  if( !theObj.IsNull() )
  {
    GraphicsView_ObjectListIterator anIter( theObjects );
    while( anIter.hasNext() )
    {
      if( HYDROGUI_Prs* aPrs = dynamic_cast<HYDROGUI_Prs*>( anIter.next() ) )
      {
        Handle(HYDROData_Entity) anObj = aPrs->getObject();
        if( IsEqual( anObj, theObj ) )
          return aPrs;
      }
    }
  }
  return NULL;
}

GraphicsView_ObjectList HYDROGUI_Tool::GetPrsList( GraphicsView_ViewPort* theViewPort )
{
  GraphicsView_ObjectList aList;
  if( theViewPort )
  {
    GraphicsView_ObjectListIterator anIter( theViewPort->getObjects() );
    while( anIter.hasNext() )
      if( HYDROGUI_Prs* aPrs = dynamic_cast<HYDROGUI_Prs*>( anIter.next() ) )
        aList.append( aPrs );
  }
  return aList;
}



HYDROData_SequenceOfObjects HYDROGUI_Tool::GetSelectedObjects( HYDROGUI_Module* theModule )
{
  HYDROData_SequenceOfObjects aSeq;

  HYDROGUI_DataModel* aModel = theModule->getDataModel();

  SUIT_SelectionMgr* aSelectionMgr = theModule->getApp()->selectionMgr();
  SUIT_DataOwnerPtrList anOwners;
  aSelectionMgr->selected( anOwners );

  QStringList aCollectedNameList; // to avoid duplication
  foreach( SUIT_DataOwner* aSUITOwner, anOwners )
  {
    if( LightApp_DataOwner* anOwner = dynamic_cast<LightApp_DataOwner*>( aSUITOwner ) )
    {
      Handle(HYDROData_Entity) anObject = aModel->objectByEntry( anOwner->entry() );
      if( !anObject.IsNull() )
      {
        QString aName = anObject->GetName();
        if( !aCollectedNameList.contains( aName ) )
        {
          aSeq.Append( anObject );
          aCollectedNameList.append( aName );
        }
      }
    }
  }
  return aSeq;
}

Handle(HYDROData_Entity) HYDROGUI_Tool::GetSelectedObject( HYDROGUI_Module* theModule )
{
  HYDROData_SequenceOfObjects aSeq = GetSelectedObjects( theModule );
  if( !aSeq.IsEmpty() )
    return aSeq.First();
  return NULL;
}

HYDROData_SequenceOfObjects HYDROGUI_Tool::GetGeometryObjects( HYDROGUI_Module* theModule )
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();

  HYDROData_SequenceOfObjects aResSeq;

  HYDROData_Iterator anIter( aDocument );
  for ( ; anIter.More(); anIter.Next() )
  {
    Handle(HYDROData_Entity) anObj = anIter.Current();
    if ( !HYDROData_Tool::IsGeometryObject( anObj ) )
      continue;

    aResSeq.Append( anObj );
  }
  
  return aResSeq;
}

ObjectKind HYDROGUI_Tool::GetSelectedPartition( HYDROGUI_Module* theModule )
{
  HYDROGUI_DataModel* aModel = theModule->getDataModel();

  SUIT_SelectionMgr* aSelectionMgr = theModule->getApp()->selectionMgr();
  SUIT_DataOwnerPtrList anOwners;
  aSelectionMgr->selected( anOwners );

  if( anOwners.size() != 1 )
    return KIND_UNKNOWN;

  if( LightApp_DataOwner* anOwner = dynamic_cast<LightApp_DataOwner*>( anOwners.first().operator->() ) )
  {
    QString anEntry = anOwner->entry();
    QString aPrefix = HYDROGUI_DataObject::entryPrefix();
    if( anEntry.left( aPrefix.length() ) == aPrefix )
    {
      anEntry.remove( aPrefix );
      for( ObjectKind anObjectKind = KIND_UNKNOWN + 1; anObjectKind <= KIND_LAST; anObjectKind++ )
        if( HYDROGUI_DataModel::tr( HYDROGUI_DataModel::partitionName( anObjectKind ).toLatin1() ) == anEntry )
          return anObjectKind;
    }
  }
  return KIND_UNKNOWN;
}

bool HYDROGUI_Tool::IsSelectedPartOfCalcCase( HYDROGUI_Module* theModule, Handle(HYDROData_CalculationCase)& theOutCalCase,
  QString& theOutPart)
{
  HYDROGUI_DataModel* aModel = theModule->getDataModel();
  SUIT_SelectionMgr* aSelectionMgr = theModule->getApp()->selectionMgr();
  SUIT_DataOwnerPtrList anOwners;
  aSelectionMgr->selected( anOwners );

  if( anOwners.size() != 1 )
    return false;

  LightApp_DataOwner* anOwner = dynamic_cast<LightApp_DataOwner*>( anOwners.first().operator->() );

  if( anOwner )
  {
    QString anEntry = anOwner->entry();
    QString aHydroPref = "_" + HYDROGUI_DataObject::entryPrefix();
    //
    QString aPostFixBoundary = aHydroPref + HYDROGUI_DataModel::tr("CASE_BOUNDARY");
    if (GetCalCaseSubT(anEntry, anEntry, aPostFixBoundary, aModel, theOutCalCase ))
    {
      theOutPart = HYDROGUI_DataModel::tr("CASE_BOUNDARY");
      theOutPart.toUpper();
      return true;
    }
    //
    QString aPostFixAO = aHydroPref + 
      HYDROGUI_DataModel::tr( HYDROGUI_DataModel::partitionName( KIND_ARTIFICIAL_OBJECT ).toLatin1().constData());
    if (GetCalCaseSubT(anEntry, anEntry, aPostFixAO, aModel, theOutCalCase ))
    {
      aPostFixAO.remove(0, aHydroPref.length());
      theOutPart = aPostFixAO;
      return true;
    }
    //
    QString aPostFixNO = aHydroPref + 
      HYDROGUI_DataModel::tr( HYDROGUI_DataModel::partitionName( KIND_NATURAL_OBJECT ).toLatin1().constData());
    if (GetCalCaseSubT(anEntry, anEntry, aPostFixNO, aModel, theOutCalCase ))
    {
      aPostFixNO.remove(0, aHydroPref.length());
      theOutPart = aPostFixNO;
      return true;
    }
    //
    QString aPostFixLCM = aHydroPref + 
      HYDROGUI_DataModel::tr( HYDROGUI_DataModel::partitionName( KIND_LAND_COVER_MAP ).toLatin1().constData());
    if (GetCalCaseSubT(anEntry, anEntry, aPostFixLCM, aModel, theOutCalCase ))
    {
      aPostFixLCM.remove(0, aHydroPref.length());
      theOutPart = aPostFixLCM;
      return true;
    }
    //
    QString aPostFixReg = aHydroPref + 
      HYDROGUI_DataModel::tr( HYDROGUI_DataModel::partitionName( KIND_REGION ).toLatin1().constData());
    if (GetCalCaseSubT(anEntry, anEntry, aPostFixReg, aModel, theOutCalCase ))
    {
      aPostFixReg.remove(0, aHydroPref.length());
      theOutPart = aPostFixReg;
      return true;
    }
    //
  }
  return false;
}

QStringList HYDROGUI_Tool::GetSelectedGeomObjects( HYDROGUI_Module* theModule,
                                                   QList<GEOM::shape_type> theTypes )
{
  QStringList anEntryList;

  // Get active SalomeApp_Study
  SalomeApp_Study* aStudy = NULL;
  if ( theModule && theModule->getApp() ) {
    aStudy = dynamic_cast<SalomeApp_Study*>( theModule->getApp()->activeStudy() );
  }
  if ( !aStudy ) {
    return anEntryList;
  }

  // Get selection
  SUIT_SelectionMgr* aSelectionMgr = theModule->getApp()->selectionMgr();
  SUIT_DataOwnerPtrList anOwners;
  aSelectionMgr->selected( anOwners );

  // Check if the selected objects belong to GEOM and have a shape
  foreach( SUIT_DataOwner* aSUITOwner, anOwners )
  {
    if( LightApp_DataOwner* anOwner = dynamic_cast<LightApp_DataOwner*>( aSUITOwner ) )
    {
      QString anEntry = anOwner->entry();
      _PTR(SObject) aSObject( aStudy->studyDS()->FindObjectID(qPrintable(anEntry)) );
      if (aSObject) {
         _PTR(SComponent) aSComponent = aSObject->GetFatherComponent();
        if ( aSComponent && aSComponent->ComponentDataType() == "GEOM" ) {
          GEOM::GEOM_Object_var aGeomObj = 
            GEOMBase::GetObjectFromIOR( aSObject->GetIOR().c_str() );

          if ( !aGeomObj->_is_nil() && aGeomObj->IsShape() && 
               theTypes.contains( aGeomObj->GetShapeType() ) ) {
            anEntryList << anEntry;
          }
        }
      }
    }
  }

  return anEntryList;
}

Handle(HYDROData_Entity) HYDROGUI_Tool::FindObjectByName( HYDROGUI_Module* theModule,
                                                          const QString&   theName,
                                                          const ObjectKind theObjectKind )
{
  Handle(HYDROData_Entity) aResObj;
  
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( !aDocument.IsNull() )
    aResObj = aDocument->FindObjectByName( theName, theObjectKind );
  
  return aResObj;
}

HYDROData_SequenceOfObjects HYDROGUI_Tool::FindObjectsByNames( HYDROGUI_Module*   theModule,
                                                               const QStringList& theNames,
                                                               const ObjectKind   theObjectKind )
{
  HYDROData_SequenceOfObjects aResSeq;

  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  if ( !aDocument.IsNull() )
    aResSeq = aDocument->FindObjectsByNames( theNames, theObjectKind );

  return aResSeq;
}

QString HYDROGUI_Tool::GenerateObjectName( HYDROGUI_Module*   theModule,
                                           const QString&     thePrefix,
                                           const QStringList& theUsedNames,
                                           const bool         theIsTryToUsePurePrefix)
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  return HYDROData_Tool::GenerateObjectName( aDocument, thePrefix, theUsedNames, theIsTryToUsePurePrefix );
}

size_t HYDROGUI_Tool::GetActiveViewId( HYDROGUI_Module* theModule,
                                       const QString&   theViewId )
{
  size_t aViewId = 0;
  SUIT_ViewManager* aViewMgr = theModule->getApp()->activeViewManager();
  if( !aViewMgr || ( !theViewId.isEmpty() && aViewMgr->getType() != theViewId ) )
    return aViewId;

  if( SUIT_ViewModel* aViewer = aViewMgr->getViewModel() )
    aViewId = (size_t)aViewer;
  return aViewId;
}

size_t HYDROGUI_Tool::GetActiveGraphicsViewId( HYDROGUI_Module* theModule )
{
  return GetActiveViewId( theModule, GraphicsView_Viewer::Type() );
}

size_t HYDROGUI_Tool::GetActiveOCCViewId( HYDROGUI_Module* theModule )
{
  return GetActiveViewId( theModule, OCCViewer_Viewer::Type() );
}

QList<size_t> getViewIdList( HYDROGUI_Module* theModule,
                             const QString&   theViewId )
{
  QList<size_t> aList;
  ViewManagerList aViewMgrs;
  theModule->getApp()->viewManagers( theViewId, aViewMgrs );
  QListIterator<SUIT_ViewManager*> anIter( aViewMgrs );
  while( anIter.hasNext() )
  {
    if( SUIT_ViewManager* aViewMgr = anIter.next() )
    {
      if( SUIT_ViewModel* aViewer = aViewMgr->getViewModel() )
        aList.append( (size_t)aViewer );
    }
  }
  return aList;
}

QList<size_t> HYDROGUI_Tool::GetGraphicsViewIdList( HYDROGUI_Module* theModule )
{
  return getViewIdList( theModule, GraphicsView_Viewer::Type() );
}

QList<size_t> HYDROGUI_Tool::GetOCCViewIdList( HYDROGUI_Module* theModule )
{
  return getViewIdList( theModule, OCCViewer_Viewer::Type() );
}

void HYDROGUI_Tool::setOCCActionShown( OCCViewer_ViewFrame* theViewFrame,
                                       const int theActionId,
                                       const bool isShown )
{
  if ( !theViewFrame )
    return;

  OCCViewer_ViewWindow* aView = theViewFrame->getView( OCCViewer_ViewFrame::MAIN_VIEW );
  if ( aView ) {
    aView->toolMgr()->setShown( theActionId, isShown );
    if ( theActionId == OCCViewer_ViewWindow::MaximizedId )
      theViewFrame->onMaximizedView( aView, true );
  }
}

void HYDROGUI_Tool::setOCCActionShown( HYDROGUI_Module* theModule,
                                       const int theActionId,
                                       const bool isShown )
{
  QList<size_t> aList;
  ViewManagerList aViewMgrs;
  theModule->getApp()->viewManagers( OCCViewer_Viewer::Type(), aViewMgrs );
  QListIterator<SUIT_ViewManager*> anIter( aViewMgrs );
  while( anIter.hasNext() )
  {
    if( SUIT_ViewManager* aViewMgr = anIter.next() )
    {
      OCCViewer_ViewFrame* aViewFrame = dynamic_cast<OCCViewer_ViewFrame*>
                                                           ( aViewMgr->getActiveView() );
      if ( aViewFrame )
        setOCCActionShown( aViewFrame, theActionId, isShown );
    }
  }
}



QColor HYDROGUI_Tool::GenerateFillingColor( HYDROGUI_Module*   theModule,
                                            const QStringList& theZoneNames )
{
  Handle(HYDROData_Document) aDocument = HYDROData_Document::Document();
  return GenerateFillingColor( aDocument, theZoneNames );
}

QColor HYDROGUI_Tool::GenerateFillingColor( const Handle(HYDROData_Document)& theDoc,
                                            const QStringList&                theZoneNames )
{
  QColor aFillingColor( Qt::darkBlue );

  int aCounter = 0;
  int aR = 0, aG = 0, aB = 0;
  QStringListIterator aZoneNameIter( theZoneNames );
  while( aZoneNameIter.hasNext() )
  {
    const QString& aZoneName = aZoneNameIter.next();
    Handle(HYDROData_ImmersibleZone) aRefZone = 
      Handle(HYDROData_ImmersibleZone)::DownCast( theDoc->FindObjectByName( aZoneName, KIND_IMMERSIBLE_ZONE ) );
    if( !aRefZone.IsNull() )
    {
      QColor aRefColor = aRefZone->GetFillingColor();
      aR += aRefColor.red();
      aG += aRefColor.green();
      aB += aRefColor.blue();
      aCounter++;
    }
  }
  
  if( aCounter > 0 )
  {
    aFillingColor = QColor( aR / aCounter, aG / aCounter, aB / aCounter );
  }

  return aFillingColor;
}



void HYDROGUI_Tool::DeleteGeomObjects( HYDROGUI_Module* theModule, const QStringList& theEntries )
{
  QStringList anEntryList;

  // Get active SalomeApp_Study
  SalomeApp_Study* aStudy = NULL;
  if ( theModule && theModule->getApp() ) {
    aStudy = dynamic_cast<SalomeApp_Study*>( theModule->getApp()->activeStudy() );
  }
  if ( !aStudy ) {
    return;
  }
 
  // Get GEOM engine
  GEOM::GEOM_Gen_var aGeomEngine = GeometryGUI::GetGeomGen();
  if ( aGeomEngine->_is_nil() ) {
    return;
  }
  
  // Delete GEOM objects
  _PTR(StudyBuilder) aStudyBuilder( aStudy->studyDS()->NewBuilder() );
  foreach ( const QString anEntry, theEntries ) {
    _PTR(SObject) aSObject( aStudy->studyDS()->FindObjectID( qPrintable(anEntry) ) );
    if ( aSObject ) {
      GEOM::GEOM_Object_var aGeomObj = 
        GEOMBase::GetObjectFromIOR( aSObject->GetIOR().c_str() );

      if ( !aGeomObj->_is_nil() ) {
        aGeomEngine->RemoveObject( aGeomObj );
      }

      aStudyBuilder->RemoveObject( aSObject );
    }
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_MergePolylinesDlg.h>
#include <HYDROGUI_ObjListBox.h>
#include <QFrame>
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QCheckBox>

HYDROGUI_MergePolylinesDlg::HYDROGUI_MergePolylinesDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  QFrame* aFrame = new QFrame( mainFrame() );
  addWidget( aFrame, 1 );

  QGridLayout* aLayout = new QGridLayout( aFrame );
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );

  myName = new QLineEdit( mainFrame() );
  aLayout->addWidget( new QLabel( tr( "RESULT_NAME" ) ), 0, 0 );
  aLayout->addWidget( myName, 0, 1 );

  myIsConnect = new QCheckBox( tr( "IS_CONNECT" ), mainFrame() );
  aLayout->addWidget( myIsConnect, 1, 0, 1, 2 );

  myList = new HYDROGUI_ObjListBox( theModule, tr( "POLYLINES" ), KIND_POLYLINEXY, mainFrame() );
  aLayout->addWidget( myList, 2, 0, 1, 2 );
}

HYDROGUI_MergePolylinesDlg::~HYDROGUI_MergePolylinesDlg()
{
}

HYDROData_SequenceOfObjects HYDROGUI_MergePolylinesDlg::selectedPolylines() const
{
  return myList->selectedObjects();
}

void HYDROGUI_MergePolylinesDlg::setSelectedPolylines( const HYDROData_SequenceOfObjects& theObjects )
{
  return myList->setSelectedObjects( theObjects );
}

void HYDROGUI_MergePolylinesDlg::setPolylinesFromSelection()
{
  return myList->setObjectsFromSelection();
}

QString HYDROGUI_MergePolylinesDlg::GetResultName() const
{
  return myName->text();
}

bool HYDROGUI_MergePolylinesDlg::IsConnectByNewSegment() const
{
  return myIsConnect->isChecked();
}

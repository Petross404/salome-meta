// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ListModel_H
#define HYDROGUI_ListModel_H

#include <HYDROGUI.h>
#include <HYDROData_Entity.h>
#include <QAbstractListModel>
#include <QPixmap>
#include <QMap>
#include <QColor>

const int HYDROGUI_VisibleRole = Qt::UserRole + 1;
const int HYDROGUI_EntryRole   = Qt::UserRole + 2;

/**
 * \class HYDROGUI_ListModel
 * \brief The class representing custom list model for the Z levels
 */
class HYDRO_EXPORT HYDROGUI_ListModel : public QAbstractListModel
{
public:
  enum OpType { Top, Up, Down, Bottom, DragAndDrop };

  typedef QPair<Handle(HYDROData_Entity), bool> Object2Visible;
  typedef QList<Object2Visible> Object2VisibleList;
  typedef QList<Handle(HYDROData_Entity)> ObjectList;

public:
  HYDROGUI_ListModel( QObject* theParent = 0 );
  virtual ~HYDROGUI_ListModel();

  virtual QVariant data( const QModelIndex &theIndex, int theRole = Qt::DisplayRole ) const;

  virtual int rowCount( const QModelIndex &theParent = QModelIndex() ) const;

  virtual QVariant headerData( int theSection,
                               Qt::Orientation theOrientation,
                               int theRole = Qt::DisplayRole ) const;
  virtual Qt::ItemFlags flags( const QModelIndex& theIndex ) const;
  virtual QMimeData* mimeData( const QModelIndexList& theIndexes ) const;
  virtual QStringList mimeTypes() const;
  virtual bool dropMimeData( const QMimeData* theData, Qt::DropAction theAction,
                             int theRow, int theColumn, const QModelIndex& theParent );
  virtual Qt::DropActions supportedDropActions() const;
  virtual Qt::DropActions supportedDragActions() const;

  QList<int> getIds( const QModelIndexList& theIndexes, bool theIsToSort = true ) const;

  void setObjects( const Object2VisibleList& theObjects );
  ObjectList getObjects() const;

  void addObject( const Object2Visible& theObject );
  void removeObjectByName( const QString& theObjectName );

  bool move( const int theItem, const OpType theType, bool theIsVisibleOnly,
             const int theDropItem = -1 );
  bool move( const QList<int>& theItems, const OpType theType, bool theIsVisibleOnly,
             const int theDropItem = -1 );

  void setDecorationEnabled( const bool theIsToEnable );

  void undoLastMove();

  void setBackgroundColor(int theInd, const QColor theColor);
  QColor getBackgroundColor(int theInd) const;
  void clearAllBackgroundColors ();

protected:
  bool isObjectVisible( int theIndex ) const;
  bool isDragAndDropAllowed( const QList<int>& theItems, const int theDropItem ) const;

private:
  friend class test_HYDROGUI_ListModel;

  Object2VisibleList myObjects, myPrevObjects;
  QPixmap myEmpty, myEye;
  QMap<int, QColor> myColoredRow;

  bool myIsDecorationEnabled;
};

#endif

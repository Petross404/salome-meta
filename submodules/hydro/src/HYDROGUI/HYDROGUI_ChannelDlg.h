// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_ChannelDlg_H
#define HYDROGUI_ChannelDlg_H

#include "HYDROGUI_InputPanel.h"

class QComboBox;
class QGroupBox;
class QLineEdit;
class QCheckBox;
class QtxDoubleSpinBox;
class QLabel;
class QRadioButton;

class HYDROGUI_ChannelDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_ChannelDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_ChannelDlg();

  void                       reset();

  void                       setObjectName( const QString& theName );
  QString                    getObjectName() const;

  void                       setGuideLineNames( const QStringList& theGuideLines );
  void                       setGuideLineName( const QString& theGuideLine );
  QString                    getGuideLineName() const;

  void                       setProfileNames( const QStringList& theProfiles );
  void                       setProfileName( const QString& theProfile );
  QString                    getProfileName() const;

  double                     getEquiDistance() const;
  void                       setEquiDistance( double );
  
  bool                       getInvertDirection() const;
  void                       setInvertDirection( bool isChecked );

  bool                       getProfileMode();
  void                       setProfileMode(bool);

  
  double                     getLCValue() const;
  void                       setLCValue( double );
  
  double                     getDeltaZValue() const;
  void                       setDeltaZValue( double );
  
  double                     getCoteZValue() const;
  void                       setCoteZValue( double );

signals:
  void                       CreatePreview();

private slots:
  void                       onChannelDefChanged();
  void                       onProfileModeChanged(bool);

protected:

  QGroupBox*                 myObjectNameGroup;
  QLineEdit*                 myObjectName;

  QComboBox*                 myGuideLines;
  QComboBox*                 myProfiles;

  QLabel*                    myProfileLabel;
  QLabel*                    myLCValLabel;
  QLabel*                    myDeltaZValLabel;
  QLabel*                    myCoteZValLabel;

  QtxDoubleSpinBox*          myEquiDistance;
  QCheckBox*                 myInvertDirection;

  QtxDoubleSpinBox*          myLCVal;  
  QtxDoubleSpinBox*          myDeltaZVal;
  QtxDoubleSpinBox*          myCoteZVal;

  QRadioButton*              myProfileModeButton;
  QRadioButton*              myProfileModeButton2;

};

#endif

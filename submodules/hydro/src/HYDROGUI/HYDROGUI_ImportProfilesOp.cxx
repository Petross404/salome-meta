// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportProfilesOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_Tool.h"

#include <HYDROData_Profile.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>
#include <SUIT_MessageBox.h>

#include <QApplication>
#include <QFileInfo>

HYDROGUI_ImportProfilesOp::HYDROGUI_ImportProfilesOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
  setName( tr( "IMPORT_PROFILES" ) );
}

HYDROGUI_ImportProfilesOp::~HYDROGUI_ImportProfilesOp()
{
}

void HYDROGUI_ImportProfilesOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myFileDlg = new SUIT_FileDlg( module()->getApp()->desktop(), true );
  myFileDlg->setWindowTitle( getName() );
  myFileDlg->setFileMode( SUIT_FileDlg::ExistingFiles );
  myFileDlg->setNameFilter( tr("PROFILE_FILTER") );

  connect( myFileDlg, SIGNAL( accepted() ), this, SLOT( onApply() ) );
  connect( myFileDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );

  myFileDlg->exec();
}

void HYDROGUI_ImportProfilesOp::onApply()
{
  if ( !myFileDlg )
  {
    abort();
    return;
  }

  // Get the selected files
  QStringList aFileNames = myFileDlg->selectedFiles();
  if ( aFileNames.isEmpty() )
  {
    abort();
    return;
  }

  QApplication::setOverrideCursor( Qt::WaitCursor );
  QStringList aBrowseObjectsEntries;
  //TODO: to implement the addition of imported profiles' entries to the list

  startDocOperation();

  // Import each of the selected files, fill the list of bad imported files
  QString aBadImportedFiles;
  int aTotalNbImported = 0;

  foreach ( const QString& aFileName, aFileNames )
  {
    NCollection_Sequence<int> aBadProfilesIds;
    TCollection_AsciiString anAsciiFileName = HYDROGUI_Tool::ToAsciiString( aFileName );
    
    int aNbImported = HYDROData_Profile::ImportFromFile( doc(), anAsciiFileName, aBadProfilesIds );
    if ( aNbImported == 0 || !aBadProfilesIds.IsEmpty() )
    {
      aBadImportedFiles += QFileInfo( aFileName ).fileName();
      if ( !aBadProfilesIds.IsEmpty() && aNbImported > 0 )
      {
        aBadImportedFiles += ", " + tr( "BAD_PROFILE_IDS" ) + " : ";
        for ( int i = 1, n = aBadProfilesIds.Length(); i <= n; ++i )
          aBadImportedFiles += QString::number( aBadProfilesIds.Value( i ) ) + ", ";
        aBadImportedFiles.remove( aBadImportedFiles.length() - 2, 2 );
      }
      aBadImportedFiles += ";\n";
    }

    aTotalNbImported += aNbImported;
  }

  if ( aTotalNbImported == 0 )
  {
    QApplication::restoreOverrideCursor();
  
    SUIT_MessageBox::critical( module()->getApp()->desktop(),
                               tr( "BAD_IMPORTED_PROFILES_FILES_TLT" ),
                               tr( "NO_ONE_PROFILE_IMPORTED" ) );

    QApplication::setOverrideCursor( Qt::WaitCursor );

    abortDocOperation();
    abort();
  }
  else
  {
    if ( !aBadImportedFiles.isEmpty() )
    {
      QApplication::restoreOverrideCursor();

      aBadImportedFiles = aBadImportedFiles.simplified();
      SUIT_MessageBox::warning( module()->getApp()->desktop(),
                                tr( "BAD_IMPORTED_PROFILES_FILES_TLT" ),
                                tr( "BAD_IMPORTED_PROFILES_FILES" ).arg( aBadImportedFiles ) );

      QApplication::setOverrideCursor( Qt::WaitCursor );
    }

    commitDocOperation();
    commit();

    module()->update( UF_Model | UF_VTKViewer | UF_VTK_Forced | UF_VTK_Init );
    browseObjects( aBrowseObjectsEntries );
  }

  QApplication::restoreOverrideCursor();
}


// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_COLORWIDGET_H
#define HYDROGUI_COLORWIDGET_H

#include <QFrame>

/** \class HYDROGUI_ColorWidget
 * \brief The class representing widget for color selection
 */
class HYDROGUI_ColorWidget : public QFrame
{
  Q_OBJECT

public:
  /** Constructor 
   * \param parent - parent widget
   */
  HYDROGUI_ColorWidget( QWidget* parent );
  /** Destructor */
  virtual ~HYDROGUI_ColorWidget();

  /** \return color value */
  QColor color() const;
  /** Sets color value */
  void   setColor( const QColor& );
  /** Resets the default color value */
  void   resetColor();
  /** \return color value in the integer form */
  //int    intColor() const;
  /** Sets color value in the integer form */
  //void   setColor( const int );
  /** Sets a randomized color as current value */
  //void   setRandColor();

signals:
  /** is emitted when color is changed */
  void colorChanged( const QColor& );

protected:
  /** mouse double click event handler, activates the standard color dialog for color choice */
  virtual void mouseDoubleClickEvent( QMouseEvent* );
};

#endif

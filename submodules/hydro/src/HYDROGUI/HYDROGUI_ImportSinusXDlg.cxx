// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportSinusXDlg.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <HYDROData_SinusX.h>
#include <assert.h>
#include <QHeaderView>
#include <QSizePolicy>

HYDROGUI_ImportSinusXDlg::HYDROGUI_ImportSinusXDlg( QWidget* theParent, const QStringList& theNames,
  const std::vector<int>& theTypes )
  : QDialog( theParent )
{
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin(5);
  aMainLayout->setSpacing(5);
  this->resize(600, 500);

  QLabel* gLabel = new QLabel();
  gLabel->setText(tr("ENTITIES_TO_IMPORT"));

  assert (theNames.size() == theTypes.size());
  myTable = new QTableWidget(theNames.size(), 5);

  QStringList header;
  header << tr("NAME") << tr("TYPE") << tr("AS_BATHY") << tr("AS_POLYXY") << tr("AS_PROFILE");
  myTable->setHorizontalHeaderLabels(header);

  myTable->setShowGrid(true);
  myTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  for (int i = 0; i < theNames.size(); i++)
  {
    QTableWidgetItem* item_name = new QTableWidgetItem(theNames[i]);
    item_name->setFlags(item_name->flags() ^ Qt::ItemIsEditable);
    myTable->setItem(i, 0, item_name);

    QString TypeName;
    if (theTypes[i] == 1)
      TypeName = tr("XYZ_CURVE");
    else if (theTypes[i] == 2)
      TypeName = tr("XYZ_PROFILE");
    else if (theTypes[i] == 3)
      TypeName = tr("ISOCONTOUR");
    else if (theTypes[i] == 4)
      TypeName = tr("SCATTER");

    QTableWidgetItem* item_type = new QTableWidgetItem(TypeName);
    item_type->setFlags(item_type->flags() ^ Qt::ItemIsEditable);
    myTable->setItem(i, 1, item_type);

    QTableWidgetItem* itemCheckbox1 = new QTableWidgetItem(""); //as bathy
    itemCheckbox1->setFlags(itemCheckbox1->flags() ^ Qt::ItemIsEditable);
    itemCheckbox1->setCheckState(Qt::Checked);
    myTable->setItem(i, 2, itemCheckbox1);

    QTableWidgetItem* itemCheckbox2 = new QTableWidgetItem("");  ; //as polyXY
    itemCheckbox2->setFlags(itemCheckbox2->flags() ^ Qt::ItemIsEditable);
    itemCheckbox2->setCheckState(Qt::Checked);
    myTable->setItem(i, 3, itemCheckbox2);

    QTableWidgetItem* itemCheckbox3 = new QTableWidgetItem("");  ; //as profile
    itemCheckbox3->setFlags(itemCheckbox3->flags() ^ Qt::ItemIsEditable);
    itemCheckbox3->setCheckState(Qt::Checked);
    myTable->setItem(i, 4, itemCheckbox3);

    if (theTypes[i] == 1 || theTypes[i] == 3) ///XYZ curve 
    {
      itemCheckbox1->setFlags(item_type->flags() & ~Qt::ItemIsEnabled); //disable bathy
      itemCheckbox1->setCheckState(Qt::Unchecked);
    }
    else if (theTypes[i] == 2)
    {
      itemCheckbox1->setFlags(item_type->flags() & ~Qt::ItemIsEnabled); 
      itemCheckbox2->setFlags(item_type->flags() & ~Qt::ItemIsEnabled); 
      itemCheckbox1->setCheckState(Qt::Unchecked);
      itemCheckbox2->setCheckState(Qt::Unchecked);
    }
    else if (theTypes[i] == 4)
    {
      //as bathy only
      itemCheckbox2->setFlags(item_type->flags() & ~Qt::ItemIsEnabled); 
      itemCheckbox3->setFlags(item_type->flags() & ~Qt::ItemIsEnabled); 
      itemCheckbox2->setCheckState(Qt::Unchecked);
      itemCheckbox3->setCheckState(Qt::Unchecked);
    }
  }

  aMainLayout->addWidget(gLabel);
  aMainLayout->addWidget(myTable);

  //QPushButton* CheckAllButton = new QPushButton( tr("CHECK_ALL"), this );
  //QPushButton* UncheckAllButton = new QPushButton( tr("UNCHECK_ALL"), this );

  QPushButton* anOkButton = new QPushButton( tr("OK"), this );
  anOkButton->setDefault( true ); 

  //aMainLayout->addWidget( CheckAllButton );
  //aMainLayout->addWidget( UncheckAllButton );
  aMainLayout->addWidget( anOkButton );

  connect( anOkButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
  //connect( CheckAllButton, SIGNAL( clicked() ), this, SLOT( onCheckAll() ) );
  //connect( UncheckAllButton, SIGNAL( clicked() ), this, SLOT( onUncheckAll() ) );

  connect(myTable, SIGNAL(cellChanged(int,int)), this, SLOT(onCheckChecked(int, int)));

  setLayout(aMainLayout);
}

HYDROGUI_ImportSinusXDlg::~HYDROGUI_ImportSinusXDlg()
{
}

void HYDROGUI_ImportSinusXDlg::onCheckChecked(int r, int c)
{
  ///if it's "as POLYLINE" column and now it's unchecked => disable "as PROFILE" checkbox
  if (c == 3 && myTable->item(r, 3)->checkState() == Qt::Unchecked) 
  {
    QString text = myTable->item(r, 1)->text();
    if (text == tr("XYZ_CURVE") || text == tr("ISOCONTOUR"))
      myTable->item(r, 4)->setCheckState(Qt::Unchecked);
  }
}

std::vector<HYDROData_SinusX::ImportOptions> HYDROGUI_ImportSinusXDlg::GetImportOptions() const
{
  std::vector<HYDROData_SinusX::ImportOptions> options; 
  int size = myTable->rowCount();
  for (int i = 0; i < size; i++)
  {
    HYDROData_SinusX::ImportOptions option;
    if (myTable->item(i, 2)->checkState() == Qt::CheckState::Checked)
      option.ImportAsBathy = true;
    if (myTable->item(i, 3)->checkState() == Qt::CheckState::Checked)
      option.ImportAsPolyXY = true;
    if (myTable->item(i, 4)->checkState() == Qt::CheckState::Checked)
      option.ImportAsProfile = true;
    options.push_back(option);
  }
  return options;
}





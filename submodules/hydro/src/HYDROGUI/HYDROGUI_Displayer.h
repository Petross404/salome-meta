// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_DISPLAYER_H
#define HYDROGUI_DISPLAYER_H

#include "HYDROGUI_AbstractDisplayer.h"

#include <QMap>
#include <QPoint>
#include <vtkNew.h>

class HYDROGUI_PrsDriver;
class SUIT_ViewWindow;
class vtkWorldPointPicker;

/**
 * \class HYDROGUI_Displayer
 * \brief Class intended to create, display and update the presentations in 2D graphics viewer.
 */
class HYDROGUI_Displayer : public HYDROGUI_AbstractDisplayer
{
public:
  /**
   * \brief Constructor.
   * \param theModule module object
   */
  HYDROGUI_Displayer( HYDROGUI_Module* theModule );

  /**
   * \brief Destructor.
   */
  virtual ~HYDROGUI_Displayer();

public:
  /**
   * \brief Force the specified objects to be updated.
   * \param theObjs sequence of objects to update
   * \param theViewerId viewer identifier
   */
  void             SetToUpdate( const HYDROData_SequenceOfObjects& theObjs,
                                const size_t theViewerId );

  /**
   * \brief Get the applicable viewer type.
   */
  virtual QString  GetType() const;

  /*
   * Get the current cursor view position and save it in the displayer
   * to be used in the obtaining the view coordinates of the cursor.
   * \param theViewWindow a view window. If a view window is null, an active view is used
   * only OCC and VTK views are processed
   */
  void             SaveCursorViewPosition( SUIT_ViewWindow* theViewWindow = 0 );
  /**
   * \brief Get the coodinates from the view window, projected on XOY plane
   * \param theViewWindow a view window
   * \param theXCoordinate a X coordinate
   * \param theXCoordinate an Y coordinate
   * \param theXCoordinate a Z coordinate, has a zero value because of the plane
   * \return true if the coordinates are got
   */
  bool             GetCursorViewCoordinates( SUIT_ViewWindow* theViewWindow,
                                             double& theXCoordinate,
                                             double& theYCoordinate,
                                             double& theZCoordinate );
protected:
  /**
   * \brief Erase all viewer objects.
   * \param theViewerId viewer identifier
   */
  void             EraseAll( const size_t theViewerId );

  /**
   * \brief Erase the specified viewer objects.
   * \param theObjs sequence of objects to erase
   * \param theViewerId viewer identifier
   */
  void             Erase( const HYDROData_SequenceOfObjects& theObjs,
                          const size_t theViewerId );

  /**
   * \brief Display the specified viewer objects.
   * \param theObjs sequence of objects to display
   * \param theViewerId viewer identifier
   * \param theIsForced flag used to update all objects, including the unchanged ones
   * \param theDoFitAll flag used to fit the view to all visible objects; do not fit by default
   */
  void             Display( const HYDROData_SequenceOfObjects& theObjs,
                            const size_t theViewerId,
                            const bool theIsForced,
                            const bool theDoFitAll );

protected:
  /**
   * \brief Purge all invalid objects in the viewer.
   * \param theViewerId viewer identifier
   */
  void             purgeObjects( const size_t theViewerId );

private:
  /**
   * \brief Get the presentation driver for the specified data object.
   * \param theObj data object
   */
  HYDROGUI_PrsDriver*             getDriver( const Handle(HYDROData_Entity)& theObj );

private:
  typedef QMap< ObjectKind, HYDROGUI_PrsDriver* > PrsDriversMap;
  PrsDriversMap                   myPrsDriversMap;
  vtkNew<vtkWorldPointPicker>     myPicker;
  int                             myXPosition;
  int                             myYPosition;
  bool                            myIsPositionSaved;
};

#endif

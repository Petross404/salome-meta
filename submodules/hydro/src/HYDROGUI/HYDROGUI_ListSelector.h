// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_LISTSELECTOR_H
#define HYDROGUI_LISTSELECTOR_H

#include <SUIT_Selector.h>

#include <QObject>

class HYDROGUI_OrderedListWidget;


/**
 * \class HYDROGUI_ListSelector
 * \brief The special selector for ordered list of objects.
 */
class HYDROGUI_ListSelector : public QObject, public SUIT_Selector
{
  Q_OBJECT

public:
  HYDROGUI_ListSelector( HYDROGUI_OrderedListWidget* theListWidget,
                         SUIT_SelectionMgr* theSelectionMgr );
  virtual ~HYDROGUI_ListSelector();

  virtual QString    type() const;

protected:
  virtual void      getSelection( SUIT_DataOwnerPtrList& ) const;
  virtual void      setSelection( const SUIT_DataOwnerPtrList& );

private slots:
  void              onSelectionChanged();

private:
  HYDROGUI_OrderedListWidget* myListWidget; ///< the ordered list widget
};

#endif

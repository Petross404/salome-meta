// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_StreamOp_H
#define HYDROGUI_StreamOp_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_Stream.h>
#include <QStringList>

class HYDROGUI_Shape;

class HYDROGUI_StreamOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_StreamOp( HYDROGUI_Module* theModule, bool isEdit );
  virtual ~HYDROGUI_StreamOp();

  static void hideBathy(HYDROGUI_Module* theModule);

protected:

  virtual void                 startOperation();
  virtual void                 abortOperation();
  virtual void                 commitOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool                 processApply( int& theUpdateFlags, QString& theErrorMsg,
                                             QStringList& theBrowseObjectsEntries );

  virtual bool                 isToAbortOnApply() const { return false; }

  virtual void                 apply();

private slots:
  void                         onAddProfiles();
  void                         onRemoveProfiles( const QStringList& );
  void                         onAxisChanged( const QString& );
  void                         onDDZValueChanged( double d );
  void                         onSSValueChanged( double d );
  void                         onLeftBankChanged( const QString& );
  void                         onRightBankChanged( const QString& );
  void                         onProfilePointsChanged( int ); 
  void                         onModeChanged( bool ); 



private:
  void                         createPreview();
  void                         erasePreview();
  void                         updatePanelData();

private:
  bool                         myIsEdit;
  Handle(HYDROData_Stream)     myEditedObject;

  HYDROGUI_Shape*              myPreviewPrs;
  
  QString                      myHydAxis;
  QStringList                  myProfiles;
  QList<double>                myProfileParams;
  //QString                      myLeftBank;
  //QString                      myRightBank;

};

#endif

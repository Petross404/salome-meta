// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Overview.h>
#include <OCCViewer_ViewPort3d.h>
#include <OCCViewer_ViewFrame.h>
#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewManager.h>
#include <QtxRubberBand.h>
#include <QApplication>
#include <QPainter>
#include <QMouseEvent>
#include <QLayout>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

class HYDROGUI_OverviewBand : public QtxPolyRubberBand
{
public:
  HYDROGUI_OverviewBand( HYDROGUI_Overview* theOverview );
  virtual ~HYDROGUI_OverviewBand();

  void initGeometry( const QPolygon& );
  void update( bool isFromMain );
  QPoint center() const;
  int    count() const;

  void   drag( const QPoint&, bool isStart );
  bool   isDrag() const;
  void   dragging( const QPoint& );
  bool   contains( const QPoint& ) const;
  QRect  boundingRect() const;

protected:
  virtual void paintEvent( QPaintEvent* );

private:
  HYDROGUI_Overview* myOverview;
  bool               myIsDrag;
  QPolygon           myStartPoints;
  QPoint             myStartPnt;
};


HYDROGUI_OverviewBand::HYDROGUI_OverviewBand( HYDROGUI_Overview* theOverview )
  : QtxPolyRubberBand( theOverview->getViewPort( false ) ),
    myOverview( theOverview ), myIsDrag( false )
{
}

HYDROGUI_OverviewBand::~HYDROGUI_OverviewBand()
{
}

bool isEmpty( const QPolygon& thePoly )
{
  QSize s = thePoly.boundingRect().size();
  if( s.width() < 2 || s.height() < 2 )
    return true;
  else
    return false;
}

void HYDROGUI_OverviewBand::initGeometry( const QPolygon& thePoly )
{
  int w2 = myOverview->width()*2;
  int h2 = myOverview->height()*2;

  QPolygon aPoly;
  for( int i=0, n=thePoly.size(); i<n; i++ )
  {
    QPoint p = thePoly.point( i );
    if( p.x() < -w2 )
      p.setX( -w2 );
    if( p.x() > w2 )
      p.setX( w2 );
    if( p.y() < -h2 )
      p.setY( -h2 );
    if( p.y() > h2 )
      p.setY( h2 );
  }

  QtxPolyRubberBand::initGeometry( thePoly );
  if( isEmpty( thePoly ) )
    hide();
  else
    show();
}

QRect HYDROGUI_OverviewBand::boundingRect() const
{
  return myPoints.boundingRect();
}

QPoint HYDROGUI_OverviewBand::center() const
{
  QPoint c;
  int n = myPoints.size()-1;
  if( n==0 )
    return QPoint();

  for( int i=0; i<n; i++ )
    c += myPoints.point( i );

  c = c/n;
  return c;
}

void HYDROGUI_OverviewBand::drag( const QPoint& thePoint, bool isStart )
{
  DEBTRACE("drag");
  if( myIsDrag==isStart )
    return;

  if( isStart )
  {
    myStartPoints = myPoints;
    /*if( contains( thePoint ) )
      myStartPnt = thePoint;
    else*/
      myStartPnt = center();
    myIsDrag = true;
    dragging( thePoint );
  }
  else
  {
    dragging( thePoint );
    myIsDrag = false;
  }
}

bool HYDROGUI_OverviewBand::isDrag() const
{
  return myIsDrag;
}

void HYDROGUI_OverviewBand::dragging( const QPoint& thePoint )
{
  DEBTRACE("dragging");
  int n = myPoints.size();
  QPoint delta = thePoint - myStartPnt;
  for( int i=0; i<n; i++ )
    myPoints.setPoint( i, myStartPoints.point( i ) + delta );
  initGeometry( myPoints );
  update( false );
}

bool HYDROGUI_OverviewBand::contains( const QPoint& thePoint ) const
{
  return myPoints.containsPoint( thePoint, Qt::OddEvenFill );
}

int HYDROGUI_OverviewBand::count() const
{
  return myPoints.size();
}

void HYDROGUI_OverviewBand::update( bool isFromMain )
{
  OCCViewer_ViewPort3d* main = myOverview->getViewPort( true );
  if( isFromMain )
  {
    int w = main->width();
    int h = main->height();

    QPolygon poly;
    QPoint p1 = myOverview->fromMain( 0, 0 );
    QPoint p2 = myOverview->fromMain( w, 0 );
    QPoint p3 = myOverview->fromMain( w, h );
    QPoint p4 = myOverview->fromMain( 0, h );
    poly.append( p1 );
    poly.append( p2 );
    poly.append( p3 );
    poly.append( p4 );
    poly.append( p1 );
    initGeometry( poly );
  }
  else
  {
    OCCViewer_ViewPort3d* overview = myOverview->getViewPort( false );
    QPoint c = center();
    double x1, y1, z1, x2, y2, z2;
    main->getView()->Convert( main->width()/2, main->height()/2, x1, y1, z1 );
    DEBTRACE("x1, y1, z1 " << x1 << " " << y1 << " " << z1);
    // Patch for OCCT 6.9.1, on 7.0.0 the moving of point to plane XY is not necessary
//    gp_Dir dm = main->getView()->Camera()->Direction();
//    double t1 = -z1/dm.Z();
//    x1 += dm.X()*t1;
//    y1 += dm.Y()*t1;
//    z1 += dm.Z()*t1;
//    DEBTRACE("x1, y1, z1 " << x1 << " " << y1 << " " << z1);

    overview->getView()->Convert( c.x(), c.y(), x2, y2, z2 );
    gp_Dir dov = overview->getView()->Camera()->Direction();
    double t2 = -z2/dov.Z();
    x2 += dov.X()*t2;
    y2 += dov.Y()*t2;
    z2 += dov.Z()*t2;
    DEBTRACE("x2, y2, z2 " << x2 << " " << y2 << " " << z2);

    gp_Trsf aTrsf;
    aTrsf.SetTranslation( gp_Pnt( x1, y1, z1 ), gp_Pnt( x2, y2, z2 ) );

    // Temporary patch for bug in OCCT 6.9.1
//    Handle(Graphic3d_Camera) cam = main->getView()->Camera();
//    gp_Dir u = cam->Up(), nu = u.Transformed (aTrsf);
//    gp_Pnt e = cam->Eye(), ne = e.Transformed (aTrsf);
//    gp_Pnt cen = cam->Center(), ncen = cen.Transformed (aTrsf);
//
//    if (!nu.IsEqual (u, 0.0))
//    {
//      cam->SetUp(nu);
//      DEBTRACE("nu " << nu.X() << " "  << nu.Y() << " "  << nu.Z());
//    }
//
//    if (!ne.IsEqual (e, 0.0))
//    {
//      cam->SetEye(ne);
//      DEBTRACE("ne " << ne.X() << " "  << ne.Y() << " "  << ne.Z())
//    }
//
//    if (!ncen.IsEqual(cen, 0.0))
//    {
//      cam->SetCenter (ncen);
//      DEBTRACE("ncen " << ncen.X() << " "  << ncen.Y() << " "  << ncen.Z())
//    }

    //version for new OCCT:
    main->getView()->Camera()->Transform( aTrsf );
    main->repaint();
  }
}

void HYDROGUI_OverviewBand::paintEvent( QPaintEvent* thePaintEvent )
{
  QPainter painter( this );
  painter.setRenderHint( QPainter::Antialiasing );

  static QColor aColor = Qt::red;
  static int aWidth = 2;
  static QPen aPen( QBrush( aColor ), aWidth, Qt::DashDotLine );

  painter.setPen( aPen );
  painter.drawPolygon( myPoints );
}

//////////////

HYDROGUI_Overview::HYDROGUI_Overview( const QString& theTitle, int theMargin, QWidget* theParent )
  : QFrame( theParent ), myMargin( theMargin ),
    myMainView( 0 ), myViewPort( 0 ), myBand( 0 )
{
  setWindowTitle( theTitle );
  myLayout = new QGridLayout( this );
  myLayout->setMargin( 0 );
  myLayout->setSpacing( 0 );
  myLayout->setRowStretch( 0, 1 );
  myLayout->setColumnStretch( 0, 1 );
}

HYDROGUI_Overview::~HYDROGUI_Overview()
{
  //delete myViewPort;
}

QImage HYDROGUI_Overview::dump() const
{
  if( !myViewPort )
    return QImage();

  Handle(V3d_View) view = myViewPort->getView();
  if( view.IsNull() )
    return QImage();

  int aWidth = myViewPort->width();
  int aHeight = myViewPort->height();

  Image_PixMap aPix;

#if OCC_VERSION_LARGE >= 0x07020000
  view->ToPixMap( aPix,aWidth, aHeight,Graphic3d_BT_RGBA );
  QImage anImage( aPix.Data(), aWidth, aHeight, QImage::Format_ARGB32 );
  anImage = anImage.mirrored();
  anImage = anImage.rgbSwapped();
#else
  view->ToPixMap(aPix, aWidth, aHeight, Graphic3d_BT_RGB);
  QImage anImage( aWidth, aHeight, QImage::Format_ARGB32 );
  for ( int i = 0; i < aWidth; i++ ) {
    for ( int j = 0; j < aHeight; j++ ) {
      Quantity_Color pixel = aPix.PixelColor( i, j ).GetRGB();
      QColor color = QColor::fromRgbF( pixel.Red(), pixel.Green(), pixel.Blue() );
      anImage.setPixelColor( i, j, color );
    }
  }
  if ( aPix.IsTopDown() )
    anImage = anImage.mirrored();
#endif
  if( myBand && myBand->isVisible() )
  {
    QPixmap aPixmap = QPixmap::fromImage( anImage );
    QPoint p = myBand->boundingRect().topLeft();
    myBand->render( &aPixmap, p );
    anImage = aPixmap.toImage();
  }
  return anImage;
}

OCCViewer_ViewPort3d* HYDROGUI_Overview::getViewPort( bool isMain ) const
{
  if ( isMain) 
  {
    if (myMainView!=NULL)
      return myMainView->getViewPort();
    else
      return NULL;
  }
  else
    return myViewPort;
}

void HYDROGUI_Overview::setMainView( OCCViewer_ViewFrame* theMainView )
{
  myMainView = theMainView;
  if( !myMainView )
    return;
  DEBTRACE("setMainView");

  OCCViewer_ViewWindow* aMainView = myMainView->getView( OCCViewer_ViewFrame::MAIN_VIEW );
  connect( aMainView, SIGNAL( vpTransformationFinished( OCCViewer_ViewWindow::OperationType ) ),
           this,      SLOT( OnTransformationAfterOp( OCCViewer_ViewWindow::OperationType ) ) );
  connect( aMainView->getViewPort(), SIGNAL( vpResizeEvent( QResizeEvent* ) ),
           this,       SLOT( OnResizeEvent( QResizeEvent* ) ) );
  connect( aMainView->getViewPort(), SIGNAL( vpTransformed( OCCViewer_ViewPort* ) ),
           this,       SLOT( OnTransformation() ) ); 

  connect( myMainView, SIGNAL(destroyed()),  this,  SLOT( onMainViewDestr() ) );

  if( !myViewPort )
  {
    myViewPort = new OCCViewer_ViewPort3d( this, myMainView->getViewPort()->getViewer(), V3d_ORTHOGRAPHIC );

    if( myViewPort )
    {
      myViewPort->setBackgroundColor( myMainView->getViewPort()->backgroundColor() );

      connect( myViewPort, SIGNAL(destroyed()),  this,  SLOT( onViewPortDestr() ) );
      connect( myViewPort, SIGNAL( vpMouseEvent( QMouseEvent* ) ), 
              this,       SLOT( OnMouseEvent( QMouseEvent* ) ) );
      connect( myViewPort, SIGNAL( vpResizeEvent( QResizeEvent* ) ),
              this,       SLOT( OnResizeEvent( QResizeEvent* ) ) );

      myLayout->addWidget( myViewPort, 0, 0 );
    }
  }

//#if defined(TEST_MODE) || defined(_DEBUG)
//qApp->installEventFilter( this );
//#endif

  qApp->processEvents();

  setTopView();

  qApp->processEvents();

  if( !myBand )
    myBand = new HYDROGUI_OverviewBand( this );

  myBand->update( true );
}

void HYDROGUI_Overview::setTopView()
{
  if( !myViewPort )
    return;

  Handle(V3d_View) aView3d = myViewPort->getView();
  if( !aView3d.IsNull() )
    aView3d->SetProj( V3d_Zpos );
  myViewPort->fitAll();

  // Apply margins for internal area in the view port
  if( myMargin>0 )
  {
    QRect aRect( -myMargin, -myMargin, myViewPort->width()+2*myMargin, myViewPort->height()+2*myMargin );
    myViewPort->fitRect( aRect );
  }

  if( myBand )
    myBand->update( true );
}

void HYDROGUI_Overview::OnTransformationAfterOp( OCCViewer_ViewWindow::OperationType theOp )
{
  if( myViewPort && theOp>=OCCViewer_ViewWindow::WINDOWFIT )
  {
    myViewPort->fitAll();
  }
  if( theOp==OCCViewer_ViewWindow::FITSELECTION )
  {
    CustomFitSelection();
  }
  OnTransformation();
}

void HYDROGUI_Overview::OnTransformation()
{
  if( myBand )
    myBand->update( true );
}

QPoint HYDROGUI_Overview::fromMain( int xp, int yp ) const
{
  if( !myMainView || !myViewPort || !myViewPort->isVisible() )
    return QPoint();

  const double EPS = 1E-2;

  Handle(V3d_View) aMain = myMainView->getViewPort()->getView();
  Handle(V3d_View) aXY = myViewPort->getView();

  // View coordinates to 3d (world) coordinates
  double x, y, z;
  aMain->Convert( xp, yp, x, y, z );

  // Moving to the XY plane
  gp_Vec aDir = aMain->Camera()->Direction();
  if( fabs( aDir.Z() )<EPS )
    return QPoint();

  double t = -z/aDir.Z();
  x += t * aDir.X();
  y += t * aDir.Y();
  z += t * aDir.Z();

  // 3d (world) coordinates to view coordinates in the overview
  aXY->Convert( x, y, z, xp, yp );

  return QPoint( xp, yp );
}

void HYDROGUI_Overview::OnMouseEvent( QMouseEvent* theEvent )
{
  QPoint mp = theEvent->pos();
  if( !myBand )
    return;

  switch( theEvent->type() )
  {
  case QEvent::MouseButtonPress:
    myBand->drag( mp, true );
    break;

  case QEvent::MouseButtonRelease:
    myBand->drag( mp, false );
    break;

  case QEvent::MouseMove:
    if( myBand->isDrag() )
      myBand->dragging( mp );
    break;
  }
}

bool HYDROGUI_Overview::eventFilter( QObject* theObject, QEvent* theEvent )
{
//#if defined(TEST_MODE) || defined(_DEBUG)
  if (theEvent->type() == 12) // paint
  {
    DEBTRACE("eventFilter " << theEvent->type() << " object " << theObject << " " << theObject->objectName().toStdString());
  }

//  switch( theEvent->type() )
//  {
//  case QEvent::MouseMove:
//    {
//      QPoint mp = ((QMouseEvent*)theEvent)->pos();
//      //mp = getViewPort(false)->mapFromGlobal(mp);
//      QString coords = QString( "(%0, %1)" ).arg( mp.x() ).arg( mp.y() );
//      std::string scoords = coords.toStdString();
//      qDebug( scoords.c_str() );
//    }
//    break;
//  }
//#endif
  return QFrame::eventFilter( theObject, theEvent );
}

void HYDROGUI_Overview::OnResizeEvent( QResizeEvent* )
{
  if( myBand )
    myBand->update( true );
}


void HYDROGUI_Overview::onMainViewDestr()
{
  myMainView = NULL;
  if (myViewPort == NULL)
    return;
  Handle(V3d_View) ov = myViewPort->getView();
  ov->View()->Deactivate();
  delete myViewPort; //this will delete myBand
  myViewPort = NULL;
  myBand = NULL;
}

void HYDROGUI_Overview::onViewPortDestr()
{
  myViewPort = NULL;
}


void HYDROGUI_Overview::CustomFitSelection() const
{
  DEBTRACE("CustomFitSelection");
  OCCViewer_ViewPort3d* main = getViewPort( true );
  if( !main )
    return;

  int w = main->width();
  int h = main->height();

  Bnd_Box bounding = BoundingForSelection();
  if( bounding.IsVoid() )
    return;

  Standard_Real xmin, ymin, zmin, xmax, ymax, zmax;
  bounding.Get( xmin, ymin, zmin, xmax, ymax, zmax );

  QList<QPoint> points;
  Standard_Integer xp, yp;
  main->getView()->Convert( xmin, ymin, zmin, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmax, ymin, zmin, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmin, ymax, zmin, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmax, ymax, zmin, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmin, ymin, zmax, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmax, ymin, zmax, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmin, ymax, zmax, xp, yp ); points.append( QPoint( xp, yp ) );
  main->getView()->Convert( xmax, ymax, zmax, xp, yp ); points.append( QPoint( xp, yp ) );

  int xpmin, ypmin, xpmax, ypmax;
  bool isFirst = true;
  foreach( QPoint p, points )
  {
    int x = p.x(), y = p.y();
    if( isFirst || x<xpmin )
      xpmin = x;
    if( isFirst || x>xpmax )
      xpmax = x;
    if( isFirst || y<ypmin )
      ypmin = y;
    if( isFirst || y>ypmax )
      ypmax = y;

    isFirst = false;
  }

  const int margin = 5;
  QRect r( xpmin-margin, ypmin-margin, xpmax-xpmin+2*margin, ypmax-ypmin+2*margin );
  main->fitRect( r );
}

Handle(AIS_InteractiveContext) HYDROGUI_Overview::context() const
{
  if( myMainView )
  {
    SUIT_ViewModel* vm = myMainView->getViewManager()->getViewModel();
    OCCViewer_Viewer* viewer = dynamic_cast<OCCViewer_Viewer*>( vm );
    if( viewer )
      return viewer->getAISContext();
  }
  return Handle(AIS_InteractiveContext)();
}

typedef NCollection_DataMap<Handle(SelectMgr_SelectableObject), Handle(SelectMgr_IndexedMapOfOwner)> AIS_MapOfObjectOwners1;
  typedef NCollection_DataMap<Handle(SelectMgr_SelectableObject), Handle(SelectMgr_IndexedMapOfOwner)>::Iterator AIS_MapIteratorOfMapOfObjectOwners1;
Bnd_Box HYDROGUI_Overview::BoundingForSelection() const
{
  DEBTRACE("BoundingForSelection");
  Handle(AIS_InteractiveContext) c = context();

  Bnd_Box aBndSelected;

  AIS_MapOfObjectOwners1 anObjectOwnerMap;
  for (c->InitSelected(); c->MoreSelected(); c->NextSelected())
  {
    const Handle(SelectMgr_EntityOwner)& anOwner = c->SelectedOwner();
    Handle(AIS_InteractiveObject) anObj = Handle(AIS_InteractiveObject)::DownCast(anOwner->Selectable());
    if (anObj->IsInfinite())
    {
      continue;
    }

    if (anOwner == anObj->GlobalSelOwner())
    {
      Bnd_Box aTmpBnd;
      anObj->BoundingBox (aTmpBnd);
      aBndSelected.Add (aTmpBnd);
    }
    else
    {
      Handle(SelectMgr_IndexedMapOfOwner) anOwnerMap;
      if (!anObjectOwnerMap.Find (anOwner->Selectable(), anOwnerMap))
      {
        anOwnerMap = new SelectMgr_IndexedMapOfOwner();
        anObjectOwnerMap.Bind (anOwner->Selectable(), anOwnerMap);
      }

      anOwnerMap->Add (anOwner);
    }
  }

  for (AIS_MapIteratorOfMapOfObjectOwners1 anIter (anObjectOwnerMap); anIter.More(); anIter.Next())
  {
    const Handle(SelectMgr_SelectableObject) anObject = anIter.Key();
    Bnd_Box aTmpBox = anObject->BndBoxOfSelected (anIter.ChangeValue());
    aBndSelected.Add (aTmpBox);
  }

  anObjectOwnerMap.Clear();

  return aBndSelected;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_SetBoundaryTypePolygonOp.h"

#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"

#include <HYDROData_Object.h>
#include <HYDROData_BCPolygon.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_SetBoundaryTypePolygonDlg.h>
#include <SUIT_MessageBox.h>
#include <HYDROGUI_UpdateFlags.h>

HYDROGUI_SetBoundaryTypePolygonOp::HYDROGUI_SetBoundaryTypePolygonOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ),
  mySetBTDlg( 0 )
{
  setName( tr( "SET_BOUNDARY_TYPE_POLYGON" ) );
}

HYDROGUI_SetBoundaryTypePolygonOp::~HYDROGUI_SetBoundaryTypePolygonOp()
{
}

void HYDROGUI_SetBoundaryTypePolygonOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROData_SequenceOfObjects aSeq = HYDROGUI_Tool::GetSelectedObjects(module());
  myBCP = Handle(HYDROData_BCPolygon)::DownCast(aSeq(1)); 

  int bType = myBCP->GetBoundaryType();

  mySetBTDlg = new HYDROGUI_SetBoundaryTypePolygonDlg( module()->getApp()->desktop(), bType );
  mySetBTDlg->setModal( true );
  mySetBTDlg->setWindowTitle(getName());

  // Connect the dialog to operation slots
  connect( mySetBTDlg, SIGNAL( accepted() ), this, SLOT( onApply()  ) );
  connect( mySetBTDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );
  
  mySetBTDlg->exec();
}

 bool HYDROGUI_SetBoundaryTypePolygonOp::processApply( int& theUpdateFlags,
                                        QString& theErrorMsg,
                                        QStringList& theBrowseObjectsEntries )
{
  if ( !mySetBTDlg )
    return false;

  int aType = mySetBTDlg->GetCurrentType();

  myBCP->SetBoundaryType( aType + 1 );
  myBCP->SetFillingColor( myBCP->DefaultFillingColor() );
  myBCP->SetBorderColor( myBCP->DefaultBorderColor() );
    
  module()->setIsToUpdate( myBCP );

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer | UF_VTK_Forced;  

  return true;
}

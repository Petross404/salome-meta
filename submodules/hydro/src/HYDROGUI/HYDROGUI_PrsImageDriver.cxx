// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PrsImageDriver.h"

#include "HYDROGUI_PrsImage.h"

#include <HYDROData_Image.h>

HYDROGUI_PrsImageDriver::HYDROGUI_PrsImageDriver()
:HYDROGUI_PrsDriver()
{
}

HYDROGUI_PrsImageDriver::~HYDROGUI_PrsImageDriver()
{
}

bool HYDROGUI_PrsImageDriver::Update( const Handle(HYDROData_Entity)& theObj,
                                      HYDROGUI_Prs*& thePrs )
{
  HYDROGUI_PrsDriver::Update( theObj, thePrs );

  if( theObj.IsNull() )
    return false;

  Handle(HYDROData_Image) anImage = Handle(HYDROData_Image)::DownCast( theObj );
  if( anImage.IsNull() )
    return false;

  if( !thePrs )
    thePrs = new HYDROGUI_PrsImage( theObj );

  HYDROGUI_PrsImage* aPrsImage = (HYDROGUI_PrsImage*)thePrs;

  aPrsImage->setName( anImage->GetName() );
  aPrsImage->setImage( anImage->Image() );
  aPrsImage->setTransform( anImage->Trsf() );

  aPrsImage->compute();

  return true;
}

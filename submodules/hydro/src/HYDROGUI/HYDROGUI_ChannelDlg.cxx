// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ChannelDlg.h"

#include "HYDROGUI_Tool.h"

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QtxDoubleSpinBox.h>
#include <QRadioButton>

HYDROGUI_ChannelDlg::HYDROGUI_ChannelDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle )
{
  // Channel name
  myObjectNameGroup = new QGroupBox( tr( "CHANNEL_NAME" ), mainFrame() );

  myObjectName = new QLineEdit( myObjectNameGroup );

  QBoxLayout* aNameLayout = new QHBoxLayout( myObjectNameGroup );
  aNameLayout->setMargin( 5 );
  aNameLayout->setSpacing( 5 );
  aNameLayout->addWidget( new QLabel( tr( "NAME" ), myObjectNameGroup ) );
  aNameLayout->addWidget( myObjectName );

  // Channel parameters
  QGroupBox* aParamGroup = new QGroupBox( tr( "CHANNEL_PARAMETERS" ), mainFrame() );

  //myGuideLines = new HYDROGUI_ObjComboSelector( theModule, KIND_POLYLINE, aParamGroup );
  myGuideLines = new QComboBox( aParamGroup );
  myGuideLines->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  //myProfiles = new HYDROGUI_ObjComboSelector( theModule, KIND_PROFILE, aParamGroup );
  myProfiles = new QComboBox( aParamGroup );
  myProfiles->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  myEquiDistance = new QtxDoubleSpinBox( aParamGroup );
  myEquiDistance->setRange( 0.01, 9999 );
  myEquiDistance->setValue( 1.0 );
  myEquiDistance->setSingleStep( 1.0 );

  //for digue
  myLCVal = new QtxDoubleSpinBox( aParamGroup );
  myLCVal->setRange( 0.0, 999999 );
  myLCVal->setValue( 50 );
  myLCVal->setSingleStep( 0.1 );

  myDeltaZVal = new QtxDoubleSpinBox( aParamGroup );
  myDeltaZVal->setRange( 0.0, 999999 );
  myDeltaZVal->setValue( 5.0 );
  myDeltaZVal->setSingleStep( 0.1 );

  myCoteZVal = new QtxDoubleSpinBox( aParamGroup );
  myCoteZVal->setRange( 0.0, 999999 );
  myCoteZVal->setValue( 1.0 );
  myCoteZVal->setSingleStep( 0.1 );
  //END for digue

  myInvertDirection = new QCheckBox(aParamGroup);
  myInvertDirection->setChecked(false);

  myProfileLabel = new QLabel( tr( "CHANNEL_PROFILE" ), aParamGroup);

  myProfileModeButton = new QRadioButton( tr( "VIA_PROFILE" ), myObjectNameGroup );
  myProfileModeButton2 = new QRadioButton( tr( "PREDEF" ), myObjectNameGroup );

  QGridLayout* aParamsLayout = new QGridLayout( aParamGroup );
  aParamsLayout->setMargin( 5 );
  aParamsLayout->setSpacing( 5 );
  aParamsLayout->addWidget( new QLabel( tr( "CHANNEL_GUIDE_LINE" ), aParamGroup ), 0, 0 );
  aParamsLayout->addWidget( myGuideLines, 0, 1 );
    
  aParamsLayout->addWidget( myProfileModeButton, 1, 0 );
  aParamsLayout->addWidget( myProfileModeButton2, 1, 1 );

  aParamsLayout->addWidget( myProfileLabel, 2, 0 );
  aParamsLayout->addWidget( myProfiles, 2, 1 );

  myLCValLabel = new QLabel( tr( "LC_VALUE" ), aParamGroup);
  myDeltaZValLabel = new QLabel( tr( "DELTAZ_VALUE" ), aParamGroup);
  myCoteZValLabel = new QLabel( tr( "COTEZ_VALUE" ), aParamGroup);

  aParamsLayout->addWidget( myLCValLabel, 3, 0 );
  aParamsLayout->addWidget( myLCVal, 3, 1 );
  aParamsLayout->addWidget( myDeltaZValLabel, 4, 0 );
  aParamsLayout->addWidget( myDeltaZVal, 4, 1 );
  aParamsLayout->addWidget( myCoteZValLabel, 5, 0 );
  aParamsLayout->addWidget( myCoteZVal, 5, 1 );

  aParamsLayout->addWidget( new QLabel( tr( "EQUI_DISTANCE" ), aParamGroup ), 6, 0 );
  aParamsLayout->addWidget( myEquiDistance, 6, 1 );
  aParamsLayout->addWidget( new QLabel( tr( "INVERT_DIRECTION" ), aParamGroup ), 7, 0 );
  aParamsLayout->addWidget( myInvertDirection, 7, 1 );

  // Common
  addWidget( myObjectNameGroup );
  addWidget( aParamGroup );

  addStretch();

  // Connect signals and slots
  connect( myGuideLines, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onChannelDefChanged() ) );
  connect( myProfiles, SIGNAL(  currentIndexChanged( int ) ), this, SLOT( onChannelDefChanged() ) );
  
  connect( myLCVal, SIGNAL( valueChanged( double ) ), this, SLOT( onChannelDefChanged() ) );
  connect( myDeltaZVal, SIGNAL(  valueChanged( double ) ), this, SLOT( onChannelDefChanged() ) );
  connect( myCoteZVal, SIGNAL( valueChanged( double ) ), this, SLOT( onChannelDefChanged() ) );

  connect( myProfileModeButton, SIGNAL( toggled( bool ) ), this, SLOT( onProfileModeChanged( bool ) ) );
  myProfileModeButton->setChecked(true);
}

HYDROGUI_ChannelDlg::~HYDROGUI_ChannelDlg()
{
}

void HYDROGUI_ChannelDlg::reset()
{
  bool isBlocked = blockSignals( true );

  myObjectName->clear();

  myGuideLines->clear();
  myProfiles->clear();

  blockSignals( isBlocked );

  onChannelDefChanged();
}

void HYDROGUI_ChannelDlg::setObjectName( const QString& theName )
{
  myObjectName->setText( theName );
}

QString HYDROGUI_ChannelDlg::getObjectName() const
{
  return myObjectName->text();
}

void HYDROGUI_ChannelDlg::setGuideLineNames( const QStringList& theGuideLines )
{
  bool isBlocked = blockSignals( true );

  myGuideLines->clear();
  myGuideLines->addItems( theGuideLines );

  blockSignals( isBlocked );
}

void HYDROGUI_ChannelDlg::setGuideLineName( const QString& theGuideLine )
{
  int aNewIdx = myGuideLines->findText( theGuideLine );
  if ( aNewIdx != myGuideLines->currentIndex() )
  {
    myGuideLines->setCurrentIndex( aNewIdx );
  }
  else
  {
    onChannelDefChanged();
  }
}

QString HYDROGUI_ChannelDlg::getGuideLineName() const
{
  return myGuideLines->currentText();
}

void HYDROGUI_ChannelDlg::setProfileNames( const QStringList& theProfiles )
{
  bool isBlocked = blockSignals( true );

  myProfiles->clear();
  myProfiles->addItems( theProfiles );

  blockSignals( isBlocked );
}

void HYDROGUI_ChannelDlg::setProfileName( const QString& theProfile )
{
  int aNewIdx = myProfiles->findText( theProfile );
  if ( aNewIdx != myProfiles->currentIndex() )
  {
    myProfiles->setCurrentIndex( aNewIdx );
  }
  else
  {
    onChannelDefChanged();
  }
}

QString HYDROGUI_ChannelDlg::getProfileName() const
{
  return myProfiles->currentText();
}

void HYDROGUI_ChannelDlg::onChannelDefChanged()
{
  if ( signalsBlocked() )
    return;

  emit CreatePreview();
}

void HYDROGUI_ChannelDlg::onProfileModeChanged(bool mode)
{
  if ( signalsBlocked() )
    return;
  if (mode)
  {
    myProfiles->setVisible(true);
    myProfileLabel->setVisible(true);
    myLCValLabel->setVisible(false);
    myLCVal->setVisible(false);
    myDeltaZValLabel->setVisible(false);
    myDeltaZVal->setVisible(false);
    myCoteZValLabel->setVisible(false);
    myCoteZVal->setVisible(false);
  }
  else
  {
    myProfiles->setVisible(false);
    myProfileLabel->setVisible(false);
    myLCValLabel->setVisible(true);
    myLCVal->setVisible(true);
    myDeltaZValLabel->setVisible(true);
    myDeltaZVal->setVisible(true);
    myCoteZValLabel->setVisible(true);
    myCoteZVal->setVisible(true);
  }
  emit CreatePreview();
}

bool HYDROGUI_ChannelDlg::getProfileMode()
{
  return myProfileModeButton->isChecked();
}

void HYDROGUI_ChannelDlg::setProfileMode(bool mode)
{
  myProfileModeButton->setChecked(mode);
  myProfileModeButton2->setChecked(!mode);
}

double HYDROGUI_ChannelDlg::getEquiDistance() const
{
  return myEquiDistance->value();
}

void HYDROGUI_ChannelDlg::setEquiDistance( double theValue )
{
  myEquiDistance->setValue( theValue );
}

bool HYDROGUI_ChannelDlg::getInvertDirection() const
{
  return myInvertDirection->isChecked();
}

void HYDROGUI_ChannelDlg::setInvertDirection( bool isChecked )
{
  myInvertDirection->setChecked(isChecked);
}

double HYDROGUI_ChannelDlg::getLCValue() const
{
  return myLCVal->value();
}

void  HYDROGUI_ChannelDlg::setLCValue( double val )
{
  myLCVal->setValue( val );
}  

double HYDROGUI_ChannelDlg::getDeltaZValue() const
{
  return myDeltaZVal->value();
}

void HYDROGUI_ChannelDlg::setDeltaZValue( double val )
{
  myDeltaZVal->setValue( val );
}
double HYDROGUI_ChannelDlg::getCoteZValue() const
{
  return myCoteZVal->value();
}

void HYDROGUI_ChannelDlg::setCoteZValue( double val )
{
  myCoteZVal->setValue( val );
}
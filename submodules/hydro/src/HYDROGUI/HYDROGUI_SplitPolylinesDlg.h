// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_SplitPolylinesDlg_H
#define HYDROGUI_SplitPolylinesDlg_H

#include "HYDROGUI_InputPanel.h"
#include <HYDROData_PolylineXY.h>

class QTabWidget;
class QtxDoubleSpinBox;
class HYDROGUI_ObjComboBox;
class HYDROGUI_ObjListBox;
class gp_Pnt2d;
class OCCViewer_Viewer;
class SUIT_ViewWindow;
class OCCViewer_ViewPort3d;

class HYDROGUI_SplitPolylinesDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  enum Mode { ByPoint, ByTool, Split };

public:
  HYDROGUI_SplitPolylinesDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_SplitPolylinesDlg();

  Mode                           GetMode() const; 
  Handle( HYDROData_PolylineXY ) GetMainPolyline() const;
  Handle( HYDROData_PolylineXY ) GetToolPolyline() const;
  gp_Pnt2d                       GetPoint() const;
  HYDROData_SequenceOfObjects    GetPolylines() const;

  void setPolylinesFromSelection();
  void setOCCViewer( OCCViewer_Viewer* theViewer );

signals:
  void modeChanged();
  void pointMoved();
  void selectionChanged();

private slots:
  void onMousePress( SUIT_ViewWindow*, QMouseEvent* );

private:
  OCCViewer_ViewPort3d* getViewPort() const;

private:
  QTabWidget*           myTab;
  QtxDoubleSpinBox*     myX;
  QtxDoubleSpinBox*     myY;
  HYDROGUI_ObjComboBox* myMainPolyline1;
  HYDROGUI_ObjComboBox* myMainPolyline2;
  HYDROGUI_ObjComboBox* myToolPolyline;
  HYDROGUI_ObjListBox*  myPolylines;
  OCCViewer_Viewer*     myOCCViewer;
};

#endif

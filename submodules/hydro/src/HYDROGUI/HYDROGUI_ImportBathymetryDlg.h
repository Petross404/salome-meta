// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_IMPORTBATHYMETRYDLG_H
#define HYDROGUI_IMPORTBATHYMETRYDLG_H

#include "HYDROGUI_InputPanel.h"

#include <QMap>
#include <QList>

class QGroupBox;
class QLineEdit;
class QCheckBox;
class QListWidget;
class QStringList;
class QListWidgetItem;

class HYDROGUI_ImportBathymetryDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_ImportBathymetryDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_ImportBathymetryDlg();

  void                       reset();

  void                       setObjectName( const QString& theName );
  QString                    getObjectName() const;

  //void                       setFileNames( const QStringList& theFileName );
  QStringList                getFileNames() const;
  void                       addFileNames( const QStringList& theFileNames );
  void                       removeFileNames( const QList<QListWidgetItem*>& theFileNamesToRem );
  void                       UpdateCheckBoxStates();

  void                       setInvertAltitudes( const bool theIsInvert );
  bool                       isInvertAltitudes() const;

  void                       setFuseIntoOneOptionChecked( bool isFIO );
  bool                       isFuseIntoOneOptionChecked() const;
  void                       setFuseIntoOneOptionEnabled(bool enabled);
  bool                       isFuseIntoOneOptionEnabled() const;

  void                       SetEditMode(bool isEdit);


signals:
  void                       FileSelected( const QStringList& theFileName );

protected slots:
  void                       onBrowse();
  void                       onRemove();
  void                       onStateChanged (int state);

private:
  QGroupBox*                 myFileNameGroup;
  QListWidget*               myFileNames;

  QCheckBox*                 myInvertAltitudes;

  QCheckBox*                 myFuseIntoOne;

  QGroupBox*                 myObjectNameGroup;
  QLineEdit*                 myObjectName;

  bool                       myIsEdit;
};

#endif

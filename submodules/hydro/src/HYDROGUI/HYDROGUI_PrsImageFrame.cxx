// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PrsImageFrame.h"

#include "HYDROGUI_PrsImage.h"

#include <QPainter>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

#define FRAME_Z_VALUE   1000
#define ANCHOR_RADIUS   3
#define EPSILON         1e-6

//=======================================================================
// name    : HYDROGUI_PrsImageFrame
// Purpose : Constructor
//=======================================================================
HYDROGUI_PrsImageFrame::HYDROGUI_PrsImageFrame( HYDROGUI_PrsImage* thePrsImage )
: GraphicsView_Object( thePrsImage ),
  myPrsImage( thePrsImage )
{
}

//=======================================================================
// name    : HYDROGUI_PrsImageFrame
// Purpose : Destructor
//=======================================================================
HYDROGUI_PrsImageFrame::~HYDROGUI_PrsImageFrame()
{
}

//================================================================
// Function : boundingRect
// Purpose  : 
//================================================================
QRectF HYDROGUI_PrsImageFrame::boundingRect() const
{
  QRectF aRect;
  AnchorMapIterator anIter( myAnchorMap );
  while( anIter.hasNext() )
  {
    if( QGraphicsEllipseItem* anAnchorItem = anIter.next().value() )
    {
      QRectF anAnchorRect = anAnchorItem->boundingRect();
      if( !anAnchorRect.isNull() )
      {
        if( aRect.isNull() )
          aRect = anAnchorRect;
        else
          aRect |= anAnchorRect;
      }
    }
  }
  return aRect;
}

//================================================================
// Function : compute
// Purpose  : 
//================================================================
void HYDROGUI_PrsImageFrame::compute()
{
  DEBTRACE("compute");
  if( myAnchorMap.isEmpty() )
  {
    for( int aType = TopLeft; aType <= BottomRight; aType++ )
    {
      UnscaledGraphicsEllipseItem* anAnchorItem = new UnscaledGraphicsEllipseItem( this );
      anAnchorItem->setBrush( QBrush( Qt::white ) );
      myAnchorMap.insert( aType, anAnchorItem );
      addToGroup( anAnchorItem );
    }
  }

  setZValue( FRAME_Z_VALUE );

  computeAnchorItems();
//  QTransform transform;
//  transform.scale(-0.5, 1.0);
//  this->setTransform(transform, false);
  updateVisibility();
}

//================================================================
// Function : computeAnchorItems
// Purpose  : 
//================================================================
void HYDROGUI_PrsImageFrame::computeAnchorItems()
{
  if( !myPrsImage )
    return;

  QRectF aRect = myPrsImage->boundingRect();

  QMap<int, QPointF> anAnchorPointMap;
  anAnchorPointMap[ TopLeft ] = aRect.topLeft();
  anAnchorPointMap[ TopRight ] = aRect.topRight();
  anAnchorPointMap[ BottomLeft ] = aRect.bottomLeft();
  anAnchorPointMap[ BottomRight ] = aRect.bottomRight();

  qreal ar = ANCHOR_RADIUS;
  QMapIterator<int, QPointF> anIter( anAnchorPointMap );
  while( anIter.hasNext() )
  {
    int anAnchorType = anIter.next().key();
    const QPointF& anAnchorPoint = anIter.value();

    QRectF anAnchorRect( anAnchorPoint - QPointF( ar, ar ), QSizeF( ar * 2, ar * 2 ) );
    myAnchorMap[ anAnchorType ]->setRect( anAnchorRect );
    myAnchorMap[ anAnchorType ]->setBasePoint( anAnchorPoint );
  }
}

//================================================================
// Function : updateVisibility
// Purpose  : 
//================================================================
void HYDROGUI_PrsImageFrame::updateVisibility()
{
  if (myPrsImage)
    DEBTRACE("updateVisibility " << myPrsImage->isSelected());
  setVisible( myPrsImage && myPrsImage->isSelected() );
}

//=======================================================================
// name    : HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem
// Purpose : Constructor
//=======================================================================
HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem::UnscaledGraphicsEllipseItem( QGraphicsItem* theParent )
: QGraphicsEllipseItem( theParent )
{
}

//=======================================================================
// name    : HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem
// Purpose : Destructor
//=======================================================================
HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem::~UnscaledGraphicsEllipseItem()
{
}

//================================================================
// Function : boundingRect
// Purpose  : 
//================================================================
QRectF HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem::boundingRect() const
{
  QRectF aRect = QGraphicsEllipseItem::boundingRect();

  GraphicsView_Object* aParent = dynamic_cast<GraphicsView_Object*>( parentItem() );
  if( !aParent )
    return aRect;

  // take into account a transformation of the base image item
  double aXScale = 1.0;
  double aYScale = 1.0;
  if( QGraphicsItem* aGrandParent = aParent->parentItem() )
  {
    QTransform aTransform = aGrandParent->transform();
    QLineF aLine( 0, 0, 1, 1 );
    aLine = aTransform.map( aLine );
    aXScale = aLine.dx();
    aYScale = aLine.dy();
  }

  QTransform aViewTransform = aParent->getViewTransform();
  double aScale = aViewTransform.m11(); // same as m22(), viewer specific
  if( fabs( aScale ) < EPSILON || fabs( aXScale ) < EPSILON || fabs( aYScale ) < EPSILON)
    return aRect;

  QPointF aCenter = aRect.center();
  double aWidth = aRect.width() / aScale / aXScale;
  double aHeight = aRect.height() / aScale / aYScale;

  aRect = QRectF( aCenter.x() - aWidth / 2, aCenter.y() - aHeight / 2, aWidth, aHeight );
  return aRect;
}

//================================================================
// Function : GenerateTranslationOnlyTransform
// Purpose  : 
//================================================================
static QTransform GenerateTranslationOnlyTransform( const QTransform &theOriginalTransform,
                                                    const QPointF &theTargetPoint )
{
  qreal dx = theOriginalTransform.m11() * theTargetPoint.x() - theTargetPoint.x() +
             theOriginalTransform.m21() * theTargetPoint.y() +
             theOriginalTransform.m31();
  qreal dy = theOriginalTransform.m22() * theTargetPoint.y() - theTargetPoint.y() +
             theOriginalTransform.m12() * theTargetPoint.x() +
             theOriginalTransform.m32();
  return QTransform::fromTranslate( dx, dy );
}

//================================================================
// Function : paint
// Purpose  : 
//================================================================
void HYDROGUI_PrsImageFrame::UnscaledGraphicsEllipseItem::paint(
  QPainter* thePainter,
  const QStyleOptionGraphicsItem* theOption,
  QWidget* theWidget )
{
  thePainter->save();
  thePainter->setTransform( GenerateTranslationOnlyTransform( thePainter->transform(),
                                                              myBasePoint ) );
  QGraphicsEllipseItem::paint( thePainter, theOption, theWidget );
  thePainter->restore();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ChannelOp.h"

#include "HYDROGUI_DataModel.h"
#include <HYDROGUI_DataObject.h>
#include "HYDROGUI_ChannelDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Shape.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Iterator.h>
#include <HYDROData_Polyline3D.h>
#include <HYDROData_Profile.h>

#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewModel.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>
#include <HYDROData_ChannelAltitude.h>

#include <TopoDS.hxx>

HYDROGUI_ChannelOp::HYDROGUI_ChannelOp( HYDROGUI_Module* theModule,
                                       const bool theIsEdit )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit ),
  myPreviewPrs( 0 )
{
  setName( theIsEdit ? tr( "EDIT_CHANNEL" ) : tr( "CREATE_CHANNEL" ) );
}

HYDROGUI_ChannelOp::~HYDROGUI_ChannelOp()
{
  erasePreview();
}

void HYDROGUI_ChannelOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_ChannelDlg* aPanel = ::qobject_cast<HYDROGUI_ChannelDlg*>( inputPanel() );

  aPanel->blockSignals( true );

  aPanel->reset();

  if ( isApplyAndClose() )
    myEditedObject.Nullify();

  QString aSelectedGuideLine, aSelectedProfile;
  bool profile_mode = true;


  QString anObjectName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_CHANNEL_NAME" ) );
  if ( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject =
        Handle(HYDROData_Channel)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if ( !myEditedObject.IsNull() )
    {
      anObjectName = myEditedObject->GetName();

      Handle(HYDROData_Polyline3D) aRefGuideLine = myEditedObject->GetGuideLine();
      if ( !aRefGuideLine.IsNull() )
        aSelectedGuideLine = aRefGuideLine->GetName();

      Handle(HYDROData_Profile) aRefProfile = myEditedObject->GetProfile();
      if ( !aRefProfile.IsNull() )
        aSelectedProfile = aRefProfile->GetName();

      profile_mode = myEditedObject->GetProfileMode();
    }
  }

  // collect information about existing 3d polylines
  QStringList aGuideLines;
  HYDROData_Iterator aPolylinesIt( doc(), KIND_POLYLINE );
  for ( ; aPolylinesIt.More(); aPolylinesIt.Next() )
  {
    Handle(HYDROData_Polyline3D) aPolyline3d = 
      Handle(HYDROData_Polyline3D)::DownCast( aPolylinesIt.Current() );
    if( !aPolyline3d.IsNull() )
    {
      TopoDS_Shape aShape = aPolyline3d->GetShape3D();
      if( aShape.ShapeType()==TopAbs_WIRE )
      {
        TopoDS_Wire aWire = TopoDS::Wire( aShape );
        if( !aWire.Closed() )
          aGuideLines.append( aPolyline3d->GetName() );
      }
    }
  }

  // collect information about existing profiles
  QStringList aProfiles = HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_PROFILE, false );

  aPanel->setObjectName( anObjectName );

  aPanel->setGuideLineNames( aGuideLines );
  aPanel->setProfileNames( aProfiles );

  aPanel->setGuideLineName( aSelectedGuideLine );
  aPanel->setProfileName( aSelectedProfile );

  if( !myEditedObject.IsNull() )
  {
    aPanel->setEquiDistance( myEditedObject->GetEquiDistance() );
    if (!profile_mode) //predef
    {
      aPanel->setLCValue( myEditedObject->GetLCValue() );
      aPanel->setCoteZValue( myEditedObject->GetCoteZValue() );
      aPanel->setDeltaZValue( myEditedObject->GetDeltaZValue() );
    }
  }
  else
    aPanel->setEquiDistance( 1.0 );

  if( !myEditedObject.IsNull() )
  {
    bool invDirection = false;
    Handle(HYDROData_IAltitudeObject) anObjAltitude = myEditedObject->GetAltitudeObject();
    Handle(HYDROData_ChannelAltitude) aChannelAlt = Handle(HYDROData_ChannelAltitude)::DownCast(anObjAltitude);
    if (!aChannelAlt.IsNull())
      invDirection = aChannelAlt->GetInvertDirection();
    aPanel->setInvertDirection( invDirection );
  }
  else
    aPanel->setInvertDirection( false );

  aPanel->blockSignals( false );
  aPanel->setProfileMode(profile_mode);

  onCreatePreview();
}

void HYDROGUI_ChannelOp::abortOperation()
{
  erasePreview();
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ChannelOp::commitOperation()
{
  erasePreview();
  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_ChannelOp::createInputPanel() const
{
  HYDROGUI_ChannelDlg* aPanel = new HYDROGUI_ChannelDlg( module(), getName() );
  connect( aPanel, SIGNAL( CreatePreview() ), this,   SLOT( onCreatePreview() ) );
  return aPanel;
}

bool HYDROGUI_ChannelOp::processApply( int& theUpdateFlags,
                                       QString& theErrorMsg,
                                       QStringList& theBrowseObjectsEntries )
{
  HYDROGUI_ChannelDlg* aPanel = ::qobject_cast<HYDROGUI_ChannelDlg*>( inputPanel() );
  if ( !aPanel )
    return false;

  QString anObjectName = aPanel->getObjectName().simplified();
  if ( anObjectName.isEmpty() )
  {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  if ( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anObjectName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObjectName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObjectName );
      return false;
    }
  }

  
  if ( myEditedObject.IsNull() )
    myEditedObject = createNewObject(); // Create new data model object

  myEditedObject->SetName( anObjectName );

  if ( !myIsEdit )
  {
    myEditedObject->SetFillingColor( myEditedObject->DefaultFillingColor() );
    myEditedObject->SetBorderColor( myEditedObject->DefaultBorderColor() );
  }

  QString aGuideLineName = aPanel->getGuideLineName();

  bool profileMode = aPanel->getProfileMode();
  QString aProfileName;
  double lc=0, deltaz=0, cotez=0;
  
  if (profileMode)
  {
    aProfileName = aPanel->getProfileName();
  }
  else
  {
    lc = aPanel->getLCValue();
    deltaz = aPanel->getDeltaZValue();
    cotez = aPanel->getCoteZValue();
  }


  /*if ( aGuideLineName.isEmpty() || aProfileName.isEmpty() )
  {
    myEditedObject->RemoveGuideLine();
    myEditedObject->RemoveProfile();
  }
  else*/
  {
    Handle(HYDROData_Polyline3D) aGuideLine = Handle(HYDROData_Polyline3D)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aGuideLineName, KIND_POLYLINE ) );
    if ( aGuideLine.IsNull() )
    {
      theErrorMsg = tr( "GUIDE_LINE_IS_NOT_SELECTED" );
      return false;
    }

    myEditedObject->RemoveGuideLine();
    myEditedObject->SetGuideLine( aGuideLine );

    myEditedObject->SetProfileMode(profileMode);

    if (profileMode)
    {
      Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast(
        HYDROGUI_Tool::FindObjectByName( module(), aProfileName, KIND_PROFILE ) );
      if ( aProfile.IsNull() )
      {
        theErrorMsg = tr( "PROFILE_IS_NOT_SELECTED" );
        return false;
      }
      myEditedObject->RemoveProfile();
      myEditedObject->SetProfile( aProfile );
      myEditedObject->SetLCValue(0);
      myEditedObject->SetDeltaZValue(0);
      myEditedObject->SetCoteZValue(0);
    }
    else
    {
      myEditedObject->RemoveProfile();
      myEditedObject->SetLCValue(lc);
      myEditedObject->SetDeltaZValue(deltaz);
      myEditedObject->SetCoteZValue(cotez);
    }

    myEditedObject->SetEquiDistance( aPanel->getEquiDistance() );
    ///
    Handle(HYDROData_IAltitudeObject) anObjAltitude = myEditedObject->GetAltitudeObject();
    Handle(HYDROData_ChannelAltitude) aChannelAlt = Handle(HYDROData_ChannelAltitude)::DownCast(anObjAltitude);
    if (!aChannelAlt.IsNull())
      aChannelAlt->SetInvertDirection(aPanel->getInvertDirection());
  }

  if ( myEditedObject->IsMustBeUpdated( HYDROData_Entity::Geom_2d ) )
    myEditedObject->Update();

  erasePreview();

  if( !myIsEdit )
  {
    module()->setObjectVisible( HYDROGUI_Tool::GetActiveOCCViewId( module() ), myEditedObject, true );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( myEditedObject );
    theBrowseObjectsEntries.append( anEntry );
  }

  module()->setIsToUpdate( myEditedObject );

  theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;

  return true;
}

Handle(HYDROData_Channel) HYDROGUI_ChannelOp::createNewObject() const
{
  return Handle(HYDROData_Channel)::DownCast( doc()->CreateObject( KIND_CHANNEL ) );
}

void HYDROGUI_ChannelOp::onCreatePreview()
{
  HYDROGUI_ChannelDlg* aPanel = ::qobject_cast<HYDROGUI_ChannelDlg*>( inputPanel() );
  if ( !aPanel )
    return;

  QString aGuideLineName = aPanel->getGuideLineName();
  QString aProfileName = aPanel->getProfileName();
  if ( aGuideLineName.isEmpty() || aProfileName.isEmpty() )
  {
    erasePreview();
    return;
  }

  LightApp_Application* anApp = module()->getApp();
  
  if ( !getPreviewManager() )
  {
    setPreviewManager( ::qobject_cast<OCCViewer_ViewManager*>( 
                       anApp->getViewManager( OCCViewer_Viewer::Type(), true ) ) );
  }

  OCCViewer_ViewManager* aViewManager = getPreviewManager();
  if ( aViewManager && !myPreviewPrs )
  {
    if ( OCCViewer_Viewer* aViewer = aViewManager->getOCCViewer() )
    {
      Handle(AIS_InteractiveContext) aCtx = aViewer->getAISContext();
      if ( !aCtx.IsNull() )
      {
        myPreviewPrs = new HYDROGUI_Shape( aCtx, NULL, getPreviewZLayer() );

        QColor aFillingColor = Qt::blue;
        QColor aBorderColor = Qt::transparent;
        if ( !myEditedObject.IsNull() )
        {
          aFillingColor = myEditedObject->GetFillingColor();
          aBorderColor = myEditedObject->GetBorderColor();
        }

        myPreviewPrs->setFillingColor( aFillingColor, false, false );
        myPreviewPrs->setBorderColor( aBorderColor, false, false );
      }
    }
  }

  if ( !aViewManager || !myPreviewPrs )
    return;

  Handle(HYDROData_Polyline3D) aGuideLine = Handle(HYDROData_Polyline3D)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), aGuideLineName, KIND_POLYLINE ) );

  Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), aProfileName, KIND_PROFILE ) );

  bool aProfMode = aPanel->getProfileMode();
  HYDROData_Channel::PrsDefinition aPrsDef;
  bool stat = false;
  double eqDist = aPanel->getEquiDistance();
  if (aProfMode)
    stat = HYDROData_Channel::CreatePresentations( aGuideLine, aProfile, aPrsDef, eqDist);
  else
  {
    double lc = aPanel->getLCValue();
    double deltaz = aPanel->getDeltaZValue();
    double cotez = aPanel->getCoteZValue();
    stat = HYDROData_Channel::CreatePresentations( aGuideLine, lc, deltaz, cotez, aPrsDef, eqDist);
  }
  if ( !stat  )
  {
    erasePreview();
    return;
  }

  myPreviewPrs->setShape( aPrsDef.myPrs2D );
}

void HYDROGUI_ChannelOp::erasePreview()
{
  if( myPreviewPrs )
  {
    delete myPreviewPrs;
    myPreviewPrs = 0;
  }
}

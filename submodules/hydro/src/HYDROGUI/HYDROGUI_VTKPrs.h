// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_VTKPrs_H
#define HYDROGUI_VTKPrs_H

#include <HYDROData_Entity.h>
#include <HYDROData_AltitudeObject.h>

#include <SALOME_InteractiveObject.hxx>
#include <SVTK_Prs.h>
#include <vtkActorCollection.h>

class vtkMapper;

template <class ActorType> ActorType* getActor(SVTK_Prs* thePrs);

/**
 * Get a VTK actor of the given presentation. Create a new one if there is no actor yet.
 */
template <class ActorType>
ActorType* getActor(SVTK_Prs* thePrs)
{
  ActorType* anActor = 0;
  vtkActorCollection* aContent = thePrs->GetObjects();
  if ( aContent )
  {
    // Remove old actor because of the problem of GEOM_Actor updating.
    //anActor = dynamic_cast<ActorType*>( aContent->GetLastActor() );
    aContent->RemoveAllItems();
  }
  if ( !anActor )
  {
    anActor = ActorType::New();
    thePrs->AddObject( anActor );
    anActor->Delete();
  }
  return anActor;
}


/*
  Class       : HYDROGUI_VTKPrs
  Description : Base class for all HYDRO presentation in VTK viewer
*/
class HYDROGUI_VTKPrs : public SVTK_Prs
{
public:
  HYDROGUI_VTKPrs( const Handle(HYDROData_Entity)& theObject );
  virtual ~HYDROGUI_VTKPrs();

  virtual void                     compute();
  virtual bool                     needScalarBar() { return false; }

  static double InvalidZValue() { return HYDROData_AltitudeObject::GetInvalidAltitude(); }

public:
  Handle(HYDROData_Entity)         getObject() const { return myObject; }
  Handle(SALOME_InteractiveObject) getIO() const { return myIO; }

  bool                             getIsToUpdate() const { return myIsToUpdate; }
  void                             setIsToUpdate( bool theState ) { myIsToUpdate = theState; }
  /**
   * \brief Set the range of Z values for the color mapping.
   */
  virtual void                     setZRange( double theRange[] );
  /**
   * \brief Get the range of Z values for the color mapping.
   */
  virtual double*                 getZRange() { return myZRange; }
  /**
   * \brief Get an actual Z values range of the presented object.
   */
  virtual double*                 getInternalZRange() { return myInternalZRange; }

protected:
  virtual vtkMapper*               mapper() { return 0; }

  double                           myInternalZRange[2]; //!< Actual Z values range of the presented object

private:
  Handle(HYDROData_Entity)         myObject;
  Handle(SALOME_InteractiveObject) myIO;
  bool                             myIsToUpdate;
  double                           myZRange[2];         //!< Imposed Z values range for colors mapping
};

#endif

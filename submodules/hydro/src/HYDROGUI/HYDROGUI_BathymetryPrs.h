// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_BATHYMETRY_PRS_H
#define HYDROGUI_BATHYMETRY_PRS_H

#include <AIS_PointCloud.hxx>
#include <SelectMgr_EntityOwner.hxx>
#include <Bnd_Box.hxx>
#include <QVector>

class HYDROGUI_ShapeBathymetry;

class HYDROGUI_BathymetryPrs : public AIS_PointCloud
{
public:
  HYDROGUI_BathymetryPrs( const HYDROGUI_ShapeBathymetry* );
  virtual ~HYDROGUI_BathymetryPrs();

  virtual void SetPoints( const Handle(TColgp_HArray1OfPnt)&     theCoords,
                          const Handle(Quantity_HArray1OfColor)& theColors = NULL,
                          const Handle(TColgp_HArray1OfDir)&     theNormals = NULL) Standard_OVERRIDE;

  int NbPoints() const;
  gp_Pnt GetPoint( int theIndex ) const;

  virtual void HilightOwnerWithColor( const Handle(PrsMgr_PresentationManager3d)& thePM,
                                      const Handle(Prs3d_Drawer)& theColor,
                                      const Handle(SelectMgr_EntityOwner)& theOwner );

  virtual void HilightSelected( const Handle(PrsMgr_PresentationManager3d)& thePM,
                                const SelectMgr_SequenceOfOwner& theOwners );

  virtual void ClearSelected();
  virtual void ClearSelectedPoints() {mySelectedPoints.clear();};

  void SetTextLabels( const QVector<int>& );

  HYDROGUI_ShapeBathymetry* GetShape() const;

  QVector<int> getSelectedPoints() {return mySelectedPoints;};

protected:
  virtual void Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                        const Handle(Prs3d_Presentation)& thePresentation,
                        const Standard_Integer theMode = 0 ) Standard_OVERRIDE;
  virtual void ComputeSelection( const Handle(SelectMgr_Selection)& theSelection,
                                 const Standard_Integer theMode ) Standard_OVERRIDE;

  void UpdateBound();
  void AddPoint( const Handle(Graphic3d_ArrayOfPoints)&, const Handle(SelectMgr_EntityOwner)& );

private:
  const HYDROGUI_ShapeBathymetry* myShape;
  Bnd_Box myBound;
  QVector<int> myTextIndices;
  QVector<int> mySelectedPoints;
};

class HYDROGUI_BathymetryPointOwner : public SelectMgr_EntityOwner
{
public:
  HYDROGUI_BathymetryPointOwner( const Handle(HYDROGUI_BathymetryPrs)&, int theIndex );
  virtual ~HYDROGUI_BathymetryPointOwner();

  virtual Standard_Boolean IsAutoHilight() const;
  int GetIndex() const;

private:
  int myIndex;
};

#endif

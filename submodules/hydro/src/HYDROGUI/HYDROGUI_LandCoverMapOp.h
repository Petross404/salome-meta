// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_LANDCOVERMAPOP_H
#define HYDROGUI_LANDCOVERMAPOP_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_LandCoverMap.h>

#include <AIS_InteractiveContext.hxx>

class SUIT_ViewWindow;
class QMouseEvent;

class HYDROGUI_LandCoverMapOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_LandCoverMapOp( HYDROGUI_Module* theModule, const int theOperationId );
  virtual ~HYDROGUI_LandCoverMapOp();
    
protected:
  virtual void                   startOperation();
  virtual void                   abortOperation();
  virtual void                   commitOperation();

  virtual HYDROGUI_InputPanel*   createInputPanel() const;

  virtual bool                   processApply( int& theUpdateFlags, QString& theErrorMsg,
                                               QStringList& theBrowseObjectsEntries );

  virtual HYDROGUI_Shape*        getPreviewShape() const { return myPreviewPrs; };

signals:
  void                           selectionChanged();
  void                           deselection();

protected slots:
  void                           onLandCoverMapChanged( const QString& theName );
  void                           onPolylineFaceChanged();
  void                           onCreatePreview();
  void                           onViewerSelectionChanged();
  void                           onMousePress(SUIT_ViewWindow*, QMouseEvent*);
  void                           onMouseRelease(SUIT_ViewWindow*, QMouseEvent*);

private:
  void                           closePreview();

  Handle(AIS_InteractiveContext) getInteractiveContext();
  void                           getSelectedShapes( TopTools_ListOfShape& theSelectedShapes );
  int                            getNbSelected();
  void                           selectLandCoverInPreview();

private:
  int                             myOperationId;
  Handle(HYDROData_LandCoverMap)  myEditedObject;

  HYDROGUI_Shape*                 myPreviewPrs;
  HYDROGUI_Shape*                 myPolylineFacePreviewPrs;

  QPoint                          myStartPnt, myEndPnt;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_AISShape.h>
#include <PrsMgr_PresentationManager.hxx>
#include <AIS_DisplayMode.hxx>
#include <Prs3d_IsoAspect.hxx>
#include <Prs3d_LineAspect.hxx>
#include <StdPrs_WFShape.hxx>

IMPLEMENT_STANDARD_RTTIEXT(HYDROGUI_AISShape, AIS_Shape)

HYDROGUI_AISShape::HYDROGUI_AISShape( const TopoDS_Shape& theShape )
  : AIS_Shape( theShape ),
    myBorderColor( Quantity_NOC_BLACK )
{
}

HYDROGUI_AISShape::~HYDROGUI_AISShape()
{
}

Quantity_Color HYDROGUI_AISShape::GetBorderColor() const
{
  return myBorderColor;
}

void HYDROGUI_AISShape::SetBorderColor( const Quantity_Color& theBorderColor )
{
  myBorderColor = theBorderColor;
  Redisplay( Standard_True );
}

void HYDROGUI_AISShape::Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                                 const Handle(Prs3d_Presentation)& thePresentation,
                                 const Standard_Integer theMode )
{
  thePresentation->Clear();

  myDrawer->FaceBoundaryAspect()->SetColor( myBorderColor );
  myDrawer->FreeBoundaryAspect()->SetColor( myBorderColor );
  myDrawer->UnFreeBoundaryAspect()->SetColor( myBorderColor );
  myDrawer->LineAspect()->SetColor( myBorderColor );
  myDrawer->SeenLineAspect()->SetColor( myBorderColor );
  myDrawer->WireAspect()->SetColor( myBorderColor );
  myDrawer->UIsoAspect()->SetColor( myBorderColor );
  myDrawer->VIsoAspect()->SetColor( myBorderColor );

  switch( theMode )
  {
  case AIS_WireFrame:
  case AIS_Shaded:
    AIS_Shape::Compute( thePresentationManager, thePresentation, theMode );
    break;
  }

  if( theMode==AIS_Shaded )
    StdPrs_WFShape::Add( thePresentation, Shape(), myDrawer );
}

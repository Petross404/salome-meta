// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PrsImage.h"

#include "HYDROGUI_PrsImageFrame.h"

#include <GraphicsView_ViewPort.h>

#include <QCursor>
#include <QApplication>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

//=======================================================================
// name    : HYDROGUI_PrsImage
// Purpose : Constructor
//=======================================================================
HYDROGUI_PrsImage::HYDROGUI_PrsImage( const Handle(HYDROData_Entity)& theObject )
: HYDROGUI_Prs( theObject ),
  myPixmapItem( 0 ),
  myCaptionItem( 0 ),
  myPrsImageFrame( 0 ),
  myIsTransformationPointPreview( false ),
  myIsByTwoPoints( true ),
  myTransformationPointType( None )
{
  DEBTRACE("HYDROGUI_PrsImage");
  myTransformationPointCursor = new QCursor( Qt::CrossCursor );
}

//=======================================================================
// name    : HYDROGUI_PrsImage
// Purpose : Destructor
//=======================================================================
HYDROGUI_PrsImage::~HYDROGUI_PrsImage()
{
  DEBTRACE("~HYDROGUI_PrsImage");
  if( myTransformationPointCursor )
  {
    delete myTransformationPointCursor;
    myTransformationPointCursor = 0;
  }
}

//================================================================
// Function : setImage
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setImage( const QImage& theImage, const bool theCompute )
{
  myImage = theImage;
  if ( theCompute ) {
    compute();
    computeTransformationPoints( true );
  }
}

//================================================================
// Function : getImage
// Purpose  : 
//================================================================
QImage HYDROGUI_PrsImage::getImage() const
{
  return myImage;
}

//================================================================
// Function : setCaption
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setCaption( const QString& theCaption )
{
  if( myCaptionItem )
  {
    myCaptionItem->setText( theCaption );
    myCaptionItem->setVisible( !theCaption.isEmpty() );
  }
}

//================================================================
// Function : getCaption
// Purpose  : 
//================================================================
QString HYDROGUI_PrsImage::getCaption() const
{
  if( myCaptionItem )
    return myCaptionItem->text();
  return QString();
}

//================================================================
// Function : setIsTransformationPointPreview
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setIsTransformationPointPreview( const bool theState )
{
  myIsTransformationPointPreview = theState;
}

//================================================================
// Function : getIsTransformationPointPreview
// Purpose  : 
//================================================================
bool HYDROGUI_PrsImage::getIsTransformationPointPreview() const
{
  return myIsTransformationPointPreview;
}

//================================================================
// Function : setTransformationPointType
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setTransformationPointType( const int thePointType )
{
  myTransformationPointType = thePointType;
  if( thePointType != None )
    computeTransformationPoints();
}

//================================================================
// Function : setTransformationPointMap
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setTransformationPointMap( const TransformationPointMap& theMap )
{
  myTransformationPointMap = theMap;
  if( !theMap.isEmpty() )
    computeTransformationPoints();
}

//================================================================
// Function : updateTransformationPoint
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::updateTransformationPoint( const int thePointType,
                                                   const bool theIsY,
                                                   const int theValue )
{
  DEBTRACE("updateTransformationPoint " << thePointType << " theIsY " << theIsY << " theValue " << theValue);
  if( myTransformationPointMap.find( thePointType ) != myTransformationPointMap.end() )
  {
    TransformationPoint& aTransformationPoint = myTransformationPointMap[ thePointType ];
    QPoint& aPoint = aTransformationPoint.Point;
    theIsY ? aPoint.setY( theValue ) : aPoint.setX( theValue );
    computeTransformationPoints();
  }
}

//================================================================
// Function : setTransformationPointCursorShape
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setTransformationPointCursorShape(Qt::CursorShape theCursorShape)
{
  if ( myTransformationPointCursor )
    myTransformationPointCursor->setShape(theCursorShape);
}

//================================================================
// Function : boundingRect
// Purpose  : 
//================================================================
QRectF HYDROGUI_PrsImage::boundingRect() const
{
  return myPixmapItem->boundingRect();
}

//================================================================
// Function : compute
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::compute()
{
  DEBTRACE("compute");
  if( !myPixmapItem )
  {
    myPixmapItem = new QGraphicsPixmapItem( this );
    addToGroup( myPixmapItem );
  }
  if( !myCaptionItem )
  {
    myCaptionItem = new QGraphicsSimpleTextItem( this );

    QFont aFont = myCaptionItem->font();
    aFont.setPointSize( 14 );
    myCaptionItem->setFont( aFont );

    addToGroup( myCaptionItem );
  }
  if( !myPrsImageFrame )
  {
    myPrsImageFrame = new HYDROGUI_PrsImageFrame( this );
    addToGroup( myPrsImageFrame );
  }

  myPixmapItem->setPixmap( QPixmap::fromImage( myImage ) );

  myCaptionItem->setPos( 0, -30 );
  myCaptionItem->setVisible( false );

//   QTransform transform;
//   transform.scale(1.0, -1.0);
//   this->setTransform(transform, false);
//   //this->setViewTransform(transform);

  myPrsImageFrame->compute();
}

//================================================================
// Function : addTo
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::addTo( GraphicsView_ViewPort* theViewPort )
{
  HYDROGUI_Prs::addTo( theViewPort );
  theViewPort->addItem( myPrsImageFrame );
}

//================================================================
// Function : removeFrom
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::removeFrom( GraphicsView_ViewPort* theViewPort )
{
  HYDROGUI_Prs::removeFrom( theViewPort );
  theViewPort->removeItem( myPrsImageFrame );
}

//================================================================
// Function : checkHighlight
// Purpose  : 
//================================================================
bool HYDROGUI_PrsImage::checkHighlight( double theX, double theY, QCursor& theCursor ) const
{
  QRect aRect = myPixmapItem->boundingRect().toRect();
  QPolygon aPolygon = sceneTransform().mapToPolygon( aRect );
  if( aPolygon.containsPoint( QPoint( theX, theY ), Qt::OddEvenFill ) )
  {
    if( myIsTransformationPointPreview )
    {
      if( myTransformationPointType != None )
        theCursor = *getTransformationPointCursor();
    }
    else
      theCursor = *getHighlightCursor();
    return true;
  }
  return false;
}

//================================================================
// Function : select
// Purpose  : 
//================================================================
bool HYDROGUI_PrsImage::select( double theX, double theY, const QRectF& theRect )
{
  if( myIsTransformationPointPreview )
  {
    if( myTransformationPointType == None || !theRect.isEmpty() )
      return false;

    QPoint aPos = pos().toPoint();

    TransformationPoint& aTransformationPoint = myTransformationPointMap[ myTransformationPointType ];
    aTransformationPoint.Point = QPoint( (int)theX, (int)theY ) - aPos;
    computeTransformationPoints();
    return true;
  }

  bool anIsSelected = HYDROGUI_Prs::select( theX, theY, theRect );
  myPrsImageFrame->updateVisibility();
  return anIsSelected;
}

//================================================================
// Function : unselect
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::unselect()
{
  HYDROGUI_Prs::unselect();
  if( !myIsTransformationPointPreview )
    myPrsImageFrame->updateVisibility();
}

//================================================================
// Function : setSelected
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setSelected( bool theState )
{
  HYDROGUI_Prs::setSelected( theState );
  if( !myIsTransformationPointPreview )
    myPrsImageFrame->updateVisibility();
}

//================================================================
// Function : computeTransformationPoints
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::computeTransformationPoints( const bool theObligatoryInit )
{
  {
    for( int aPointType = PointA; aPointType <= PointC; aPointType++ )
    {
      if( myTransformationPointMap.isEmpty() || theObligatoryInit )
        initTrsfPoints( aPointType );
      // Show/hide the point if necessary
      updateTrsfPoint( aPointType );

    }
  }
}

QGraphicsItemGroup* HYDROGUI_PrsImage::createPointItem( const QString& theCaption,
                                                        const QColor& theColor )
{
  QGraphicsEllipseItem* aPointItem = new QGraphicsEllipseItem( this );
  aPointItem->setPen( QPen( theColor ) );
  aPointItem->setBrush( QBrush( theColor ) );

  double aRadius = 3;
  QRectF aRect( -QPointF( aRadius, aRadius ), QSizeF( aRadius * 2 + 1, aRadius * 2 + 1 ) );
  aPointItem->setRect( aRect );
  aPointItem->setPos( QPointF( 0, 0 ) );

  QGraphicsSimpleTextItem* aCaptionItem = aCaptionItem = new QGraphicsSimpleTextItem( theCaption, this );
  aCaptionItem->setPen( QPen( theColor ) );
  aCaptionItem->setBrush( QBrush( theColor ) );
  QFont aFont = aCaptionItem->font();
  aFont.setPointSize( qApp->font().pointSize() );
  aCaptionItem->setFont( aFont );
  aCaptionItem->setPos( QPointF( -aRadius * 2, aRadius * 2 ) );

  QGraphicsItemGroup* aGroupItem = new QGraphicsItemGroup( this );
  aGroupItem->addToGroup( aPointItem );
  aGroupItem->addToGroup( aCaptionItem );
  aGroupItem->setVisible( false );
//  QTransform transform;
//  transform.scale(1.0, -1.0); // retourne le point et sa lettre
//  aGroupItem->setTransform(transform, true);
  aGroupItem->setFlag( QGraphicsItem::ItemIgnoresTransformations );

  return aGroupItem;
}

//================================================================
// Function : initTrsfPoints
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::initTrsfPoints( const int thePointType )
{
  QPoint aPoint;
  QString aCaption;
  QColor aColor = Qt::black;

  int aWidth = myImage.width();
  int aHeight = myImage.height();
  switch( thePointType )
  {
    case PointA:
      aPoint = QPoint( 0, 0 );
      aCaption = "A";
      aColor = Qt::darkRed;
      break;
    case PointB:
      aPoint = QPoint( aWidth, 0 );
      aCaption = "B";
      aColor = Qt::darkGreen;
      break;
    case PointC:
      aPoint = QPoint( 0, aHeight );
      aCaption = "C";
      aColor = Qt::darkBlue;
      break;
  }

  TransformationPoint aTransformationPoint;
  if ( myTransformationPointMap.contains( thePointType ) )
    aTransformationPoint = myTransformationPointMap[thePointType];

  aTransformationPoint.Point = aPoint;
  aTransformationPoint.Caption = aCaption;
  if ( !aTransformationPoint.GroupItem )
    aTransformationPoint.GroupItem = createPointItem( aCaption, aColor );
  aTransformationPoint.GroupItem->setPos( aPoint );

  myTransformationPointMap[ thePointType ] = aTransformationPoint;
}

//================================================================
// Function : updateTrsfPoint
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::updateTrsfPoint( const int thePointType )
{
  // If image is transformed only by two points then the point C is invisible
  bool anIsPointVisible = myIsTransformationPointPreview && ( 
    ( !myIsByTwoPoints ) || ( myIsByTwoPoints && ( thePointType != PointC ) ) );

  TransformationPoint& aTransformationPoint = myTransformationPointMap[ thePointType ];

  const QPointF& aPoint = aTransformationPoint.Point;
  aTransformationPoint.GroupItem->setPos( aPoint );
  aTransformationPoint.GroupItem->setVisible( anIsPointVisible );
}

//================================================================
// Function : getIsByTwoPoints
// Purpose  : 
//================================================================
bool HYDROGUI_PrsImage::getIsByTwoPoints() const
{
  return myIsByTwoPoints;
}

//================================================================
// Function : setIsByTwoPoints
// Purpose  : 
//================================================================
void HYDROGUI_PrsImage::setIsByTwoPoints( const bool theIsByTwoPoints )
{
  myIsByTwoPoints = theIsByTwoPoints;
  if ( myTransformationPointMap.contains( PointC ) )
  {
    // Show/hide the point C if necessary
    updateTrsfPoint( PointC );
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ListModel.h"

#include "HYDROGUI_DataObject.h"

#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <QMimeData>

const QString OBJ_LIST_MIME_TYPE = "application/hydro.objects.list";


/**
  Constructor.
  @param theParent the parent object
*/
HYDROGUI_ListModel::HYDROGUI_ListModel( QObject* theParent )
 : QAbstractListModel( theParent ), myIsDecorationEnabled( true )
{
  // Get resource manager
  SUIT_ResourceMgr* aResMgr = 0;
  SUIT_Session* aSession = SUIT_Session::session();
  if ( aSession ) {
    aResMgr = SUIT_Session::session()->resourceMgr();
  }

  // Define eye icon and empty icon
  myEmpty = QPixmap( 16, 16 );
  myEmpty.fill( Qt::transparent );
  if ( aResMgr ) {
    myEye = aResMgr->loadPixmap( "HYDRO", tr( "EYE_ICO" ) );
  } else {
    myEye = QPixmap( 16, 16 );
    myEye.fill( Qt::black );
  }
}

/**
  Destructor.
*/
HYDROGUI_ListModel::~HYDROGUI_ListModel()
{
}

void HYDROGUI_ListModel::setBackgroundColor(int theInd, QColor theColor)
{
  myColoredRow[theInd] = theColor;
}

void HYDROGUI_ListModel::clearAllBackgroundColors()
{
  myColoredRow.clear();
}

QColor HYDROGUI_ListModel::getBackgroundColor(int theInd) const
{
  if (myColoredRow.count( theInd ))
    return myColoredRow[theInd];
  else
    return QColor();
}

/**
*/
QVariant HYDROGUI_ListModel::data( const QModelIndex &theIndex, int theRole ) const
{
  QVariant aVariant;

  int aRow = theIndex.row();
  int aColumn = theIndex.column();

  switch( theRole )
  {
  case Qt::DisplayRole:
    {
      if( aColumn==0 && aRow >=0 && aRow < myObjects.count() )
        return myObjects.at( aRow ).first->GetName();
      else
        return QVariant();
    }
    break;
  case Qt::BackgroundRole:
    {
      if( aColumn==0 && aRow >=0 && aRow < myObjects.count() && myColoredRow.contains(aRow))
      {
        QBrush aBackgr(myColoredRow[aRow]);
        return aBackgr;
      }
      else
        return QVariant();
    }

  case Qt::DecorationRole:
    {
      if( myIsDecorationEnabled &&
          aColumn==0 && aRow >=0 && aRow < myObjects.count() ) {
        bool isVisible = isObjectVisible( aRow );
        if( isVisible )
          return myEye;
        else
          return myEmpty;
      }
      return QVariant();
    }
    break;

  case HYDROGUI_VisibleRole:
    {
      bool isVisible = isObjectVisible( aRow );
      return QVariant( isVisible ).toString();
    }
    break;
  case HYDROGUI_EntryRole:
    {
      if( aColumn==0 && aRow >=0 && aRow < myObjects.count() ) {
        aVariant = HYDROGUI_DataObject::dataObjectEntry( myObjects.at( aRow ).first );
      }
    }
    break;
  }

  return aVariant;
}

/**
*/
int HYDROGUI_ListModel::rowCount( const QModelIndex &theParent ) const
{
  return myObjects.count();
}

/**
  Set objects list.
  @param theObjects the list of pairs (object; object visibility)
*/
void HYDROGUI_ListModel::setObjects( const Object2VisibleList& theObjects )
{
  beginResetModel();
  myObjects = theObjects;
  endResetModel();
}

/**
  Get objects list.
  @return the list of objects ordered according to the model
*/
HYDROGUI_ListModel::ObjectList HYDROGUI_ListModel::getObjects() const
{
  ObjectList anObjects;

  foreach( const Object2Visible& anItem, myObjects ) {
    anObjects << anItem.first;
  }

  return anObjects;
}

/**
  Add the object to the end of the list.
  @param theObjects the pair (object; visibility)
*/
void HYDROGUI_ListModel::addObject( const Object2Visible& theObject )
{
  beginResetModel();
  myObjects << theObject;
  endResetModel();
}

/**
  Remove the object from the list.
  @param theObjectName the name of the object to remove
*/
void HYDROGUI_ListModel::removeObjectByName( const QString& theObjectName )
{
  Object2Visible anItem;
  foreach( anItem, myObjects ) {
    if ( anItem.first->GetName() == theObjectName ) {
      break;
    }
  }

  beginResetModel();
  myObjects.removeAll(anItem);
  endResetModel();
}


/**
  Check if the object is visible.
  @param theIndex the object index
  @return true if the object is visible, false - otherwise
*/
bool HYDROGUI_ListModel::isObjectVisible( int theIndex ) const
{
  bool isVisible = false;

  if ( theIndex >= 0 && theIndex < myObjects.count() ) {
    isVisible = myObjects.at( theIndex ).second;
  }

  return isVisible;
}

/**
*/
QVariant HYDROGUI_ListModel::headerData( int theSection,
                                         Qt::Orientation theOrientation,
                                         int theRole ) const
{
  if( theOrientation==Qt::Horizontal && theRole==Qt::DisplayRole )
  {
    switch( theSection )
    {
    case 0:
      return tr( "VISIBLE" );
    case 1:
      return tr( "OBJECT_NAME" );
    };
  }
  return QVariant();
}

/**
*/
Qt::ItemFlags HYDROGUI_ListModel::flags( const QModelIndex& theIndex ) const
{
  Qt::ItemFlags aDefaultFlags = QAbstractListModel::flags( theIndex );
  if( theIndex.isValid() )
    return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | aDefaultFlags;
  else
    return Qt::ItemIsDropEnabled | aDefaultFlags;
}

/**
*/
QMimeData* HYDROGUI_ListModel::mimeData( const QModelIndexList& theIndexes ) const
{
  QMimeData* aMimeData = new QMimeData();
  QByteArray anEncodedData;
  QDataStream aStream( &anEncodedData, QIODevice::WriteOnly );

  QList<int> anIdsList = getIds( theIndexes, true );
  foreach( int anId, anIdsList )
    aStream << anId;

  aMimeData->setData( OBJ_LIST_MIME_TYPE, anEncodedData );
  return aMimeData;
}

/**
*/
QStringList HYDROGUI_ListModel::mimeTypes() const
{
  QStringList aTypes;
  aTypes << OBJ_LIST_MIME_TYPE;
  return aTypes;
}

/**
*/
bool HYDROGUI_ListModel::dropMimeData( const QMimeData* theData, Qt::DropAction theAction,
                                       int theRow, int theColumn, const QModelIndex& theParent )
{
  if( theAction == Qt::IgnoreAction)
    return true;

  if( !theData->hasFormat( OBJ_LIST_MIME_TYPE ))
    return false;

  if( theColumn > 0 )
    return false;

  // TODO: to disable drop between items use: int aDropItemId = theParent.row();
  int aDropItemId = theParent.isValid() ? theParent.row() : theRow;

  QByteArray anEncodedData = theData->data( OBJ_LIST_MIME_TYPE );
  QDataStream aStream( &anEncodedData, QIODevice::ReadOnly );
  QList<int> anIdsList;
  while( !aStream.atEnd() )
  {
    int anId;
    aStream >> anId;
    anIdsList << anId;
  }

  move( anIdsList, DragAndDrop, false, aDropItemId ); //TODO set visibility?
  return true;
}

/**
*/
Qt::DropActions HYDROGUI_ListModel::supportedDropActions() const
{
  return Qt::MoveAction | Qt::CopyAction;
}

/**
*/
Qt::DropActions HYDROGUI_ListModel::supportedDragActions() const
{
  // Set the supported drag actions for the items in the model
  return Qt::MoveAction | Qt::CopyAction;
}

/**
  Get list of ids by the list model indexes.
  @param theIsToSort defines if the list of ids should be sorted in ascending order
  @return the list of ids
*/
QList<int> HYDROGUI_ListModel::getIds( const QModelIndexList& theIndexes,
                                       bool theIsToSort ) const
{
  QList<int> anIds;
  foreach( const QModelIndex& anIndex, theIndexes ) {
    anIds << anIndex.row();
  }

  if ( theIsToSort ) {
    qSort( anIds );
  }

  return anIds;
}

/**
  Move the item.
  @param theItem the item id to move
  @param theType the move operation type
  @param theIsVisibleOnly indicates if do move relating to the visible objects only
  @param theDropItem the drop item id ( used for drag and drop obly )
  @return true in case of success
*/
bool HYDROGUI_ListModel::move( const int theItem, const OpType theType,
                               bool theIsVisibleOnly, const int theDropItem )
{
  bool aRes = false;
  if ( theItem < 0 || theItem >= myObjects.count() ) {
    return aRes;
  }

  int aDestinationIndex = -1;
  bool isInsertBefore = false;

  switch ( theType ) {
    case Up:
      isInsertBefore = true;
      if ( theItem > 0 ) {
        aDestinationIndex = theItem - 1;
        if ( theIsVisibleOnly ) {
          while ( aDestinationIndex >= 0 && !isObjectVisible( aDestinationIndex ) ) {
            aDestinationIndex--;
          }
        }
      }
      break;
    case Down:
      if ( theItem < myObjects.count() - 1 ) {
        aDestinationIndex = theItem + 1;
        if ( theIsVisibleOnly ) {
          while ( aDestinationIndex < myObjects.count() && !isObjectVisible( aDestinationIndex ) ) {
            aDestinationIndex++;
          }
        }
      }
      break;
    case Top:
      isInsertBefore = true;
      if ( theItem > 0 ) {
        aDestinationIndex = 0;
      }
      break;
    case Bottom:
      if ( theItem < myObjects.count() - 1 ) {
        aDestinationIndex = myObjects.count() - 1;
      }
      break;
    case DragAndDrop:
      if ( theItem > theDropItem ) {
        isInsertBefore = true;
        aDestinationIndex = theDropItem;
      } else {
        aDestinationIndex = theDropItem - 1;
      }
      break;
  }

  if ( aDestinationIndex >= 0 && aDestinationIndex < myObjects.count() ) {
    int aDestinationRow = isInsertBefore ? aDestinationIndex : aDestinationIndex + 1;
    if ( beginMoveRows( QModelIndex(), theItem, theItem, QModelIndex(), aDestinationRow ) ) {
      myPrevObjects = myObjects;
      myObjects.move( theItem, aDestinationIndex );
      endMoveRows();
      aRes = true;
    }
  }

  return aRes;
}

/**
  Move the items.
  @param theItems the list of item ids to move
  @param theType the move operation type
  @param theIsVisibleOnly indicates if do move relating to the visible objects only
  @param theDropItem the drop item id ( used for drag and drop obly )
  @return true in case of success
*/
bool HYDROGUI_ListModel::move( const QList<int>& theItems, const OpType theType,
                               bool theIsVisibleOnly, const int theDropItem )
{
  bool aRes = true;

  QListIterator<int> anIt( theItems );
  int aDragShift = 0;

  switch ( theType ) {
    case Top:
    case Down:
      // reverse order
      anIt.toBack();
      while ( anIt.hasPrevious() ) {
        int anId = anIt.previous();
        if ( theType == Top ) {
          anId += aDragShift;
          aDragShift++;
        }
        if ( !move( anId, theType, theIsVisibleOnly, theDropItem ) ) {
          aRes = false;
          break;
        }
      }
      break;
    case Bottom:
    case Up:
      // direct order
      while ( anIt.hasNext() ) {
        int anId = anIt.next();
        if ( theType == Bottom ) {
          anId -= aDragShift;
          aDragShift++;
        }
        if ( !move( anId, theType, theIsVisibleOnly, theDropItem ) ) {
          aRes = false;
          break;
        }
      }
      break;
    case DragAndDrop:
      // direct order
      aRes = isDragAndDropAllowed( theItems, theDropItem );
      if ( aRes ) {
        int aDropShift = 0;
        int aDropItem = theDropItem;
        while ( anIt.hasNext() ) {
          int anId = anIt.next();
          aDropItem = theDropItem + aDropShift;
          if ( anId > aDropItem ) {
            aDragShift = 0;
            aDropShift++;
          } else {
            anId -= aDragShift;
            if ( ( aDropItem - anId ) != 1 ) {
              aDragShift++;
            }
          }
          move( anId, theType, theIsVisibleOnly, aDropItem );
        }
      }
      break;
    default:
      aRes = false;
  }

  return aRes;
}

/**
  Check if drag and drop operation allowed.
  @param theItems the list of dragged item ids
  @param theDropItem the drop item id
  @return true if drag and drop allowed
*/
bool HYDROGUI_ListModel::isDragAndDropAllowed( const QList<int>& theItems,
                                               const int theDropItem ) const
{
  bool isAllowed = false;

  if ( theDropItem >= 0 &&
       // TODO: to disable drop between items use: theDropItem < myObjects.count()
       theDropItem <= myObjects.count() &&
       !theItems.empty() && theItems.count() < myObjects.count() &&
       !theItems.contains( theDropItem )) {
    isAllowed = true;
  }

  return isAllowed;
}

/**
  Enable/disable decoration (eye icon).
  @param theIsToEnable if true - the decoration will be enabled
*/
void HYDROGUI_ListModel::setDecorationEnabled( const bool theIsToEnable )
{
  myIsDecorationEnabled = theIsToEnable;
}

void HYDROGUI_ListModel::undoLastMove()
{
  beginResetModel();
  myObjects = myPrevObjects;
  endResetModel();
}

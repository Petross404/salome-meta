// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ZLEVELSPANEL_H
#define HYDROGUI_ZLEVELSPANEL_H

#include "HYDROGUI_ListModel.h"

#include <QDialog>

class HYDROGUI_OrderedListWidget;
class HYDROGUI_Module;

class QCheckBox;
class QPushButton;


/** 
 * \class HYDROGUI_ZLevelsDlg
 * \brief The class representing dialog for managing Z levels
 */
class HYDRO_EXPORT HYDROGUI_ZLevelsDlg : public QDialog
{
  Q_OBJECT

public:
  HYDROGUI_ZLevelsDlg( QWidget* theParent, HYDROGUI_Module* theModule  );
  virtual ~HYDROGUI_ZLevelsDlg();

  void setObjects( const HYDROGUI_ListModel::Object2VisibleList& theObjects );
  HYDROGUI_ListModel::ObjectList getObjects() const;

signals:
  void applyOrderAndClose();
  void applyOrder();

private slots:
  void onStateChanged();

private:
  HYDROGUI_OrderedListWidget* myListWidget; ///< the ordered list widget
  QCheckBox* myAllObjects;         ///< the show all objects button
  QPushButton* myApplyAndClose;    ///< the apply changes and close dialog button
  QPushButton* myApply;            ///< the apply changes button  
  QPushButton* myClose;            ///< the close dialog button
};

#endif

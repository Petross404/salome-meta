// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_ZLayers.h>
#include <AIS_InteractiveContext.hxx>
#include <AIS_InteractiveObject.hxx>
#include <AIS_ListOfInteractive.hxx>
#include <AIS_ListIteratorOfListOfInteractive.hxx>
#include <AIS_Trihedron.hxx>
#include <PrsMgr_PresentationManager3d.hxx>
#include <Basics_OCCTVersion.hxx>

void UpdateZLayersOfHilightPresentationsOfDisplayedObjects( const Handle(AIS_InteractiveContext)& theContext,
                                                            int theLayer )
{
  AIS_ListOfInteractive aDisplayedObjects;
  theContext->DisplayedObjects( aDisplayedObjects );

  AIS_ListIteratorOfListOfInteractive aListIter( aDisplayedObjects );
  for ( ; aListIter.More(); aListIter.Next() )
  {
    Handle(AIS_InteractiveObject) aPrsObj = aListIter.Value();
    if( !aPrsObj.IsNull() && !aPrsObj->IsKind( STANDARD_TYPE(AIS_Trihedron) ) )
    {
      int aMode = aPrsObj->HasHilightMode() ? aPrsObj->HilightMode() : 0;
#if OCC_VERSION_LARGE < 0x07010000
      Quantity_NameOfColor aStyle = Quantity_NOC_YELLOW;
#else
      Handle(Graphic3d_HighlightStyle) aStyle = new Graphic3d_HighlightStyle( /*Aspect_TOHM_COLOR, Quantity_NOC_YELLOW*/ );
      aStyle->SetMethod(Aspect_TOHM_COLOR);
      aStyle->SetColor(Quantity_NOC_YELLOW);
#endif
      theContext->MainPrsMgr()->Color( aPrsObj, aStyle, aMode );
      SetPrsZLayer( aPrsObj, aMode, theLayer );
      theContext->MainPrsMgr()->Unhighlight( aPrsObj, aMode );
    }
  }
}

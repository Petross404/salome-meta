// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_TWOIMAGESOP_H
#define HYDROGUI_TWOIMAGESOP_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_Image.h>

class HYDROGUI_TwoImagesOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  enum OperationType { Fuse, Cut, Split };

public:
  HYDROGUI_TwoImagesOp( HYDROGUI_Module* theModule, const int theType, const bool theIsEdit );
  virtual ~HYDROGUI_TwoImagesOp();

protected:
  virtual void               startOperation();

  virtual HYDROGUI_InputPanel* createInputPanel() const;

  virtual bool               processApply( int& theUpdateFlags, QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries );

protected slots:
  /** Show warning if the name has already been selected 
   *  in other selector of the same parent widget. 
   * @param theName the selected object name
   */
  void                       onAlreadySelected( const QString& theName );

private:
  int                        myType;
  bool                       myIsEdit;
  Handle(HYDROData_Image)    myEditedObject;
};

#endif

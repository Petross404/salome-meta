// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_PRIORITYWIDGET_H
#define HYDROGUI_PRIORITYWIDGET_H

#include <HYDROGUI.h>

#include <HYDROData_Entity.h>
#include <HYDROData_PriorityQueue.h>

#include <QStyledItemDelegate>
#include <QWidget>

class QTableView;
class QToolButton;

/** 
 * \class HYDROGUI_PriorityWidget
 * \brief The class representing widget for tuning of the objects priorities and bathymetry conflicts
 *
 * The widget represents a table and a set of buttons (add and remove)
 * providing the possibility to define priority rules.
 */
class HYDRO_EXPORT HYDROGUI_PriorityWidget : public QWidget
{
  Q_OBJECT

  class Delegate;

public:
  HYDROGUI_PriorityWidget( QWidget* theParent );
  virtual ~HYDROGUI_PriorityWidget();

  void setObjects( const QList<Handle(HYDROData_Entity)>& theObjects );

  HYDROData_ListOfRules getRules() const;
  void setRules( const HYDROData_ListOfRules& theRules );

  QTableView* getTable() const;

  void undoLastChange();

protected:
  void updateControls();

signals:
  void ruleChanged();

protected slots:
  void onAddRule();
  void onRemoveRule();
  void onClearRules();

  void onSelectionChanged();

  void onShowError( const QString& theMsg );

private:
  QTableView*  myTable;  ///< the table view
  QToolButton* myAdd;    ///< the add rule button
  QToolButton* myRemove; ///< the remove rule button
  QToolButton* myClear;  ///< the clear all rules button
};

/** 
 * \class HYDROGUI_PriorityWidget::Delegate
 * \brief The class representing custom item delegate (combobox)
 */
class HYDROGUI_PriorityWidget::Delegate : public QStyledItemDelegate
{
  Q_OBJECT

public:
  Delegate( QObject* theParent = 0 );
  
  QWidget* createEditor( QWidget*, const QStyleOptionViewItem&,
                         const QModelIndex& ) const;
  
  void setEditorData( QWidget*, const QModelIndex& ) const;
  void setModelData( QWidget*, QAbstractItemModel*, const QModelIndex& ) const;

protected slots:
  void finishEditing();
};

#endif

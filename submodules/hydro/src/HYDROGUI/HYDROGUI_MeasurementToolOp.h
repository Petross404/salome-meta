// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_MEASUREMENTTOOLOP_H
#define HYDROGUI_MEASUREMENTTOOLOP_H

#include "HYDROGUI_Operation.h"

#include <HYDROData_Entity.h>
#include <TopoDS_Wire.hxx>
#include <AIS_Shape.hxx>

class HYDROGUI_MeasurementToolDlg;
class QMouseEvent;
class SUIT_ViewWindow;
class HYDROData_PolylineXY;
class BRepLib_MakePolygon;

class HYDROGUI_MeasurementToolOp : public HYDROGUI_Operation
{
  Q_OBJECT

public:
  HYDROGUI_MeasurementToolOp( HYDROGUI_Module* theModule );
  virtual ~HYDROGUI_MeasurementToolOp();

protected:
  virtual void startOperation();
  virtual void abortOperation();

protected slots:
  void onMousePress(SUIT_ViewWindow*, QMouseEvent*);
  void onClearLine();
  void onExit();

private:
  void eraseLine();

private:

  Handle(AIS_Shape) myAISLineM;
  BRepLib_MakePolygon* myLineMaker; 
  HYDROGUI_MeasurementToolDlg* myMeasDlg;
  double myDist;
  TopoDS_Wire myFW;
  double myFu;
  std::vector<Handle(HYDROData_PolylineXY)> myPolyObjs;

};

#endif
// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_LANDCOVERMAPDLG_H
#define HYDROGUI_LANDCOVERMAPDLG_H

#include <HYDROGUI_InputPanel.h>
#include <HYDROGUI_LandCoverArgsFilter.h>

class HYDROGUI_StricklerTypeComboBox;

class QGroupBox;
class QLineEdit;
class QLabel;

class HYDROGUI_LandCoverMapDlg : public HYDROGUI_InputPanel
{
  Q_OBJECT

public:
  HYDROGUI_LandCoverMapDlg( HYDROGUI_Module* theModule, const QString& theTitle, const int theOperationId );
  virtual ~HYDROGUI_LandCoverMapDlg();

  virtual void             reset();

  void                     setObjectNames( const QStringList& theNames );
  void                     setObjectName( const QString& theName );
  QString                  getObjectName() const;

  QString                  getPolylineFaceName() const;
  void                     setPolylineFaceName( const QString& );
  Handle(HYDROData_Entity) getPolylineFace() const;

  void                     setSelectedStricklerTypeName( const QString& theName );
  QString                  getSelectedStricklerTypeName() const;

  void                     updateSelectedLandCoversLabel( int theNbSelected );

signals:
  void                     landCoverMapChanged( const QString& theName );
  void                     polylineFaceChanged();

private slots:
  void                     onLandCoverMapChanged();
  void                     onPolylineFaceChanged();

private:
  void                     updateState( bool theInitialConfigure = false );

private:
  int                             myOperationId;
  QGroupBox*                      myObjectNameGroup;  
  QLineEdit*                      myObjectNameCreate;
  QComboBox*                      myObjectNameEdit;

  QGroupBox*                      myParamGroup;
  QLabel*                         myPolylinesFacesLabel;
  HYDROGUI_ObjComboBox*           myPolylinesFaces;
  QLabel*                         myStricklerTypesLabel;
  HYDROGUI_StricklerTypeComboBox* myStricklerTypes;
  QLabel*                         mySelectedLandCoversLabel;
  HYDROGUI_LandCoverArgsFilter    myFilter;
};

#endif

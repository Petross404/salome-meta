// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_AISSHAPE_H
#define HYDROGUI_AISSHAPE_H

#include <AIS_Shape.hxx>

class HYDROGUI_AISShape : public AIS_Shape
{
public:
  DEFINE_STANDARD_RTTIEXT(HYDROGUI_AISShape, AIS_Shape);

  HYDROGUI_AISShape( const TopoDS_Shape& );
  virtual ~HYDROGUI_AISShape();

  Quantity_Color GetBorderColor() const;
  void SetBorderColor( const Quantity_Color& theBorderColor );

  virtual void Compute( const Handle(PrsMgr_PresentationManager3d)& thePresentationManager,
                        const Handle(Prs3d_Presentation)& thePresentation, 
                        const Standard_Integer theMode );

private:
  Quantity_Color myBorderColor;
};

#endif

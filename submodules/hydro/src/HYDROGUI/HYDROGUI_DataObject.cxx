// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_DataObject.h"

#include <HYDROData_DummyObject3D.h>
#include <HYDROData_ImmersibleZone.h>
#include <HYDROData_Object.h>
#include <HYDROData_ArtificialObject.h>
#include <HYDROData_NaturalObject.h>
#include <HYDROData_BCPolygon.h>

#include <TDF_Tool.hxx>

#include <SUIT_DataObject.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Session.h>
#include <SUIT_Operation.h>
#include <CAM_Module.h>
#include <CAM_Application.h>
#include <SUIT_Study.h>

#include <QPixmap>
#include <QPainter>

HYDROGUI_DataObject::HYDROGUI_DataObject( SUIT_DataObject* theParent,
                                          Handle(HYDROData_Entity) theData,
                                          const QString& theParentEntry,
                                          const bool theIsInOperation )
: CAM_DataObject( theParent ),
#ifndef TEST_MODE
  LightApp_DataObject( theParent ),
#endif
  myData( theData ),
  myParentEntry( theParentEntry ),
  myIsValid( true ),
  myIsInOperation( theIsInOperation )
{
}

QString HYDROGUI_DataObject::entry() const
{
  QString anEntry = HYDROGUI_DataObject::dataObjectEntry( modelObject() );
  if( !refEntry().isEmpty() )
    anEntry.prepend( myParentEntry + "_" );
  return anEntry;
}

QString HYDROGUI_DataObject::refEntry() const
{
  if( !myParentEntry.isEmpty() )
    return HYDROGUI_DataObject::dataObjectEntry( modelObject() );
  return QString();
}

QString HYDROGUI_DataObject::name() const
{
  if( !myData.IsNull() )
    return myData->GetName();
  return QString();
}

QFont HYDROGUI_DataObject::font( const int theId ) const
{
  QFont aFont = PARENT::font( theId );
  if( theId == NameId )
  {
    Handle(HYDROData_Entity) aDataObject = modelObject();
    if( !aDataObject.IsNull() && aDataObject->IsMustBeUpdated( HYDROData_Entity::Geom_All ) )
    {
      aFont.setItalic( true );
      aFont.setBold( true );
    }
  }
  return aFont;
}

QColor HYDROGUI_DataObject::color( const ColorRole theRole, const int theId ) const
{
  QColor aColor;

  if ( !isValid() ) {
    switch ( theRole )
    {
      case Text:
      case Foreground:
      case Highlight:
        aColor = Qt::red; // red
      break;
      case HighlightedText:
        // text color for the highlighted item
        aColor = Qt::white;   // white
      break;

      default:
        break;
    }
  }

  if ( !aColor.isValid() )
  {
    Handle(HYDROData_Entity) aDataObject = modelObject();
    if( !aDataObject.IsNull() && aDataObject->IsMustBeUpdated( HYDROData_Entity::Geom_All ) )
    {
      switch ( theRole )
      {
        case Text:
        case Foreground:
        case Highlight:
          aColor = Qt::blue;    // color for objects which need updating
        break;
        case HighlightedText:
          // text color for the highlighted item
          aColor = Qt::white;   // white
        break;

        default:
          break;
      }
    }
  }

  if ( !aColor.isValid() ) {
    aColor = PARENT::color( theRole, theId );
  }

  return aColor;
}

QPixmap HYDROGUI_DataObject::icon( const int theId ) const
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  if ( theId == NameId )
  {
    QString anIcon;
    Handle(HYDROData_Entity) aDataObject = modelObject();
    Handle(HYDROData_Object) anObject;
    Handle(HYDROData_ArtificialObject) anAObject = Handle( HYDROData_ArtificialObject )::DownCast(aDataObject);
    Handle(HYDROData_NaturalObject) aNObject = Handle( HYDROData_NaturalObject )::DownCast(aDataObject);

    if (!anAObject.IsNull())
      anObject = anAObject;
    if (!aNObject.IsNull())
      anObject = aNObject;

    if( aDataObject.IsNull() )
    {
      anIcon = QObject::tr( "HYDRO_TYPE0_ICO" ); // KIND_UNKNOWN
    }
    else
    {
      QString aNeedUpdate( aDataObject->IsMustBeUpdated( HYDROData_Entity::Geom_All ) ? "M_" : "" );

      int anObjectKind = (int)aDataObject->GetKind();
      bool IsUnsImmZone = false;
      if ( anObjectKind == KIND_DUMMY_3D )
      {
        Handle(HYDROData_DummyObject3D) anObject3D =
          Handle(HYDROData_DummyObject3D)::DownCast( aDataObject );

        Handle(HYDROData_Object) aFatherObj = anObject3D->GetObject();
        if ( !aFatherObj.IsNull() )
          anObjectKind = aFatherObj->GetKind();
        anIcon = QObject::tr( QString("HYDRO_%1TYPE%2_ICO").arg( aNeedUpdate ).arg( anObjectKind ).toLatin1() );
      }
      else if ( !anObject.IsNull() )
      {
        bool IsSubm = anObject->IsSubmersible();
        QString anIcon1 = QObject::tr( QString("HYDRO_%1TYPE%2_ICO").arg( aNeedUpdate ).arg( anObjectKind ).toLatin1() );
        QString anIcon2;
        if (IsSubm)
          anIcon2 = QObject::tr( QString("HYDRO_SUBMERSIBLE16_ICO").toLatin1());
        else
          anIcon2 = QObject::tr( QString("HYDRO_UNSUBMERSIBLE16_ICO").toLatin1());

        QPixmap qpm1 = aResMgr->loadPixmap( "HYDRO", anIcon1 );
        QPixmap qpm2 = aResMgr->loadPixmap( "HYDRO", anIcon2 );
        QPixmap qpmD(32,16);
        qpmD.fill(QColor(0,0,0));
        QPainter painter;
        painter.begin(&qpmD);
        painter.drawPixmap(0, 0, qpm1);
        painter.drawPixmap(16, 0, qpm2);
        painter.end();
        return qpmD;
      }
      else if (anObjectKind == KIND_BC_POLYGON)
      {
        Handle(HYDROData_BCPolygon) aBCObj = Handle(HYDROData_BCPolygon)::DownCast( aDataObject );
        int aBT = aBCObj->GetBoundaryType();
        if (aBT == 1)
          anIcon = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE1_ICO").toLatin1());
        else if (aBT == 2)
          anIcon = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE2_ICO").toLatin1());
        else if (aBT == 3)
          anIcon = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE3_ICO").toLatin1());
        else
          anIcon = QObject::tr( QString("HYDRO_BC_POLYGON_TYPE_UNDEF_ICO").toLatin1());
      }
      else
        anIcon = QObject::tr( QString("HYDRO_%1TYPE%2_ICO").arg( aNeedUpdate ).arg( anObjectKind ).toLatin1() );
    }

    return aResMgr->loadPixmap( "HYDRO", anIcon );
  }
  return PARENT::icon( theId );
}

QString HYDROGUI_DataObject::dataObjectEntry( const Handle(HYDROData_Entity)& theObject,
                                              const bool theWithPrefix )
{
  QString aEntryStr = QString::null;
  if( !theObject.IsNull() )
  {
    TCollection_AsciiString aLabEntr;
    TDF_Tool::Entry( theObject->Label(), aLabEntr );
    aEntryStr = aLabEntr.ToCString();
    if( theWithPrefix )
      aEntryStr.prepend( HYDROGUI_DataObject::entryPrefix() );
  }
  return aEntryStr;
}

void HYDROGUI_DataObject::setIsValid( const bool theIsValid )
{
  myIsValid = theIsValid;
}

bool HYDROGUI_DataObject::isValid() const
{
  return myIsValid;
}

bool HYDROGUI_DataObject::renameAllowed( const int theColumnId ) const
{
  bool aRes = false;
  if ( theColumnId == NameId && module())
  {
    SUIT_Operation* anOp = module()->application()->activeStudy()->activeOperation();
    if ( anOp && anOp->inherits( "HYDROGUI_CalculationOp" ) )
    {
      aRes = isInOperation();
    }
    else
    {
      aRes = !anOp;
    }
  }
  else
  {
    aRes = PARENT::renameAllowed( theColumnId );
  }
  return aRes;
}

void HYDROGUI_DataObject::updateBy( SUIT_DataObject* theObj )
{
  HYDROGUI_DataObject* aDataObj = dynamic_cast<HYDROGUI_DataObject*>( theObj );
  if( !aDataObj )
    return;

  myData = aDataObj->myData;
  myParentEntry = aDataObj->myParentEntry;
  myIsValid = aDataObj->myIsValid;
  myIsInOperation = aDataObj->myIsInOperation;
  myIcon = aDataObj->myIcon;
  setModified( true );
}

HYDROGUI_NamedObject::HYDROGUI_NamedObject( SUIT_DataObject* theParent,
                                            const QString&   theName,
                                            const QString&   theParentEntry,
                                            const bool       theIsInOperation  )
: CAM_DataObject( theParent ),
#ifndef TEST_MODE
  LightApp_DataObject( theParent ),
#endif
  myName( theName ),
  myParentEntry( theParentEntry ),
  myIsInOperation( theIsInOperation )
{
}

QString HYDROGUI_NamedObject::entry() const
{
  QString anEntry = HYDROGUI_DataObject::entryPrefix() + name();
  if( !myParentEntry.isEmpty() )
    anEntry.prepend( myParentEntry + "_" );
  return anEntry;
}

QString HYDROGUI_NamedObject::name() const
{
  return myName.toUpper();
}

QPixmap HYDROGUI_NamedObject::icon( const int theId ) const
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  if ( theId == NameId )
  {
    return aResMgr->loadPixmap( "HYDRO", QObject::tr( "PARTITION_ICO" ) );
  }
  return PARENT::icon( theId );
}

QFont HYDROGUI_NamedObject::font( const int theId ) const
{
  QFont aFont = PARENT::font( theId );
  if( theId == NameId )
  {
  if ( dynamic_cast<CAM_ModuleObject*>( parent() ) )
    {
      aFont.setBold( true );
    }
  }
  return aFont;
}

void HYDROGUI_NamedObject::updateBy( SUIT_DataObject* theObj )
{
  HYDROGUI_NamedObject* aNamedObj = dynamic_cast<HYDROGUI_NamedObject*>( theObj );
  if( !aNamedObj )
    return;

  myName = aNamedObj->myName;
  myParentEntry = aNamedObj->myParentEntry;
  myIcon = aNamedObj->myIcon;
  myIsInOperation = aNamedObj->myIsInOperation;
  setModified( true );
}






HYDROGUI_DropTargetObject::HYDROGUI_DropTargetObject( SUIT_DataObject* theParent,
                                            const QString&   theName,
                                            const QString&   theParentEntry,
                                            const bool       theIsInOperation  )
: HYDROGUI_NamedObject( theParent, theName, theParentEntry, theIsInOperation ),
  CAM_DataObject( theParent )
{
}

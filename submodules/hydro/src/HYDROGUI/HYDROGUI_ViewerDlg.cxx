// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ViewerDlg.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_AISTrihedron.h"

#include <CurveCreator_Widget.h>
#include <CurveCreator_ICurve.hxx>
#include <CurveCreator_Utils.hxx>

#include <OCCViewer_ViewPort3d.h>
#include <OCCViewer_Utilities.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewFrame.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>

#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMouseEvent>

HYDROGUI_ViewerDlg::HYDROGUI_ViewerDlg( HYDROGUI_Module* theModule, const QString& theTitle, bool isSplitter, bool dispTrihedron )
: HYDROGUI_InputPanel( theModule, theTitle, true, isSplitter )
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
#ifndef TEST_MODE
  SUIT_Study* aStudy = theModule ? theModule->application()->activeStudy() : 0;
#else
  SUIT_Study* aStudy = 0;
#endif
  myViewManager = new OCCViewer_ViewManager( aStudy, 0 );
  bool IsChainedPan = aResMgr->booleanValue( "HYDRO", "chained_panning" );
  myViewManager->setChainedOperations(IsChainedPan);
  OCCViewer_Viewer* aViewer = new OCCViewer_Viewer( dispTrihedron );

  aViewer->setBackground( OCCViewer_ViewFrame::TOP_LEFT,
                          aResMgr->backgroundValue( "OCCViewer", "xz_background", aViewer->background(OCCViewer_ViewFrame::TOP_LEFT) ) );
  aViewer->setBackground( OCCViewer_ViewFrame::TOP_RIGHT,
                          aResMgr->backgroundValue( "OCCViewer", "yz_background", aViewer->background(OCCViewer_ViewFrame::TOP_RIGHT) ) );
  aViewer->setBackground( OCCViewer_ViewFrame::BOTTOM_LEFT,
                          aResMgr->backgroundValue( "OCCViewer", "xy_background", aViewer->background(OCCViewer_ViewFrame::BOTTOM_LEFT) ) );
  aViewer->setBackground( OCCViewer_ViewFrame::BOTTOM_RIGHT,
                          aResMgr->backgroundValue( "OCCViewer", "background", aViewer->background(OCCViewer_ViewFrame::MAIN_VIEW) ) );

  aViewer->setTrihedronSize( aResMgr->doubleValue( "3DViewer", "trihedron_size", aViewer->trihedronSize() ),
                             aResMgr->booleanValue( "3DViewer", "relative_size", aViewer->trihedronRelative() ));
  aViewer->setInteractionStyle( aResMgr->integerValue( "3DViewer", "navigation_mode", aViewer->interactionStyle() ) );
  aViewer->setZoomingStyle( aResMgr->integerValue( "3DViewer", "zooming_mode", aViewer->zoomingStyle() ) );
  aViewer->enablePreselection( aResMgr->booleanValue( "OCCViewer", "enable_preselection", aViewer->isPreselectionEnabled() ) );
  aViewer->enableSelection( aResMgr->booleanValue( "OCCViewer", "enable_selection", aViewer->isSelectionEnabled() ) );

  myViewManager->setViewModel( aViewer );// custom view model, which extends SALOME_View interface

  //aViewer->enableMultiselection( false );

  SUIT_ViewWindow* aViewWin = myViewManager->createViewWindow();
  aViewer->setStaticTrihedronDisplayed( false );
  /*
  Handle(AIS_Trihedron) aTrihedron =
      HYDROGUI_AISTrihedron::createTrihedron( aResMgr->doubleValue( "3DViewer", "trihedron_size", aViewer->trihedronSize() ) );
  Handle(AIS_InteractiveContext) anAISContext = aViewer->getAISContext();
  if ( !anAISContext.IsNull() )
  {
    anAISContext->Display( aTrihedron );
    anAISContext->Deactivate( aTrihedron );
  }
  */
  addWidget( aViewWin, 1 );

  // Coordinates
  connect( myViewManager, SIGNAL( mouseMove( SUIT_ViewWindow*, QMouseEvent* ) ),
           this, SLOT( onMouseMove( SUIT_ViewWindow*, QMouseEvent* ) ) );

  if ( aViewWin )
  {
    OCCViewer_ViewFrame* aViewFrame = dynamic_cast<OCCViewer_ViewFrame*>( aViewWin );
    if ( aViewFrame && aViewFrame->getViewPort() )
      aViewFrame->getViewPort()->installEventFilter( this );
  }

  myCoordLabel = new QLabel( mainFrame() );
  addWidget( myCoordLabel, 0 );
}

HYDROGUI_ViewerDlg::~HYDROGUI_ViewerDlg()
{
  myViewManager->closeAllViews();
  delete myViewManager;
}

bool HYDROGUI_ViewerDlg::event( QEvent* e )
{
    if ( e->type() == QEvent::Polish )
    {
        //addWidget( myCoordLabel->parentWidget(), 4 );

        Handle(AIS_Trihedron) aTrihedron = trihedron();
        Handle(AIS_InteractiveContext) anAISContext = getAISContext();
        if ( !anAISContext.IsNull() && !aTrihedron.IsNull() )
        {
            viewer()->setTrihedronShown( false );
            anAISContext->Display( aTrihedron, true );
            anAISContext->Deactivate( aTrihedron );
        }
    }

    return HYDROGUI_InputPanel::event( e );
}

Handle(AIS_InteractiveContext) HYDROGUI_ViewerDlg::getAISContext()
{
  OCCViewer_Viewer* aViewer = (OCCViewer_Viewer*)myViewManager->getViewModel();
  return aViewer ? aViewer->getAISContext() : 0;
}

OCCViewer_Viewer* HYDROGUI_ViewerDlg::viewer() const
{
    return ::qobject_cast<OCCViewer_Viewer*>( myViewManager->getViewModel() );
}

OCCViewer_ViewManager* HYDROGUI_ViewerDlg::viewManager() const
{
    return myViewManager;
}

SUIT_SelectionMgr* HYDROGUI_ViewerDlg::selectionMgr() const
{
#ifdef TEST_MODE
  static SUIT_SelectionMgr* mgr = new SUIT_SelectionMgr();
  return mgr;
#else
    SUIT_SelectionMgr* aSelMgr = 0;
    if ( module() )
    {
        LightApp_Application* app = module()->getApp();
        if ( app )
            aSelMgr = app->selectionMgr();
    }
    return aSelMgr;
#endif
}

void HYDROGUI_ViewerDlg::onMouseMove( SUIT_ViewWindow* theViewWindow, QMouseEvent* theEvent )
{
  OCCViewer_ViewWindow* anOCCViewWindow = 
    dynamic_cast<OCCViewer_ViewWindow*>(theViewWindow);
  if ( anOCCViewWindow && anOCCViewWindow->getViewPort() )
  {
    gp_Pnt aPnt = CurveCreator_Utils::ConvertClickToPoint( theEvent->x(), theEvent->y(), anOCCViewWindow->getViewPort()->getView() );

    // Show the coordinates
    QString aX = HYDROGUI_Tool::GetCoordinateString( aPnt.X(), true );
    QString anY = HYDROGUI_Tool::GetCoordinateString( aPnt.Y(), true );
    myCoordLabel->setText( tr("UZ_COORDINATES_INFO").arg( aX ).arg( anY ) );
  }
}

bool HYDROGUI_ViewerDlg::eventFilter( QObject* theObj, QEvent* theEvent )
{
  if ( theObj->inherits( "OCCViewer_ViewPort" ) )
  {
    if ( theEvent->type() == QEvent::Leave )
      myCoordLabel->clear();

    return false;
  }

  return HYDROGUI_InputPanel::eventFilter( theObj, theEvent );
}

Handle(AIS_Trihedron) HYDROGUI_ViewerDlg::trihedron()
{
    return Handle(AIS_Trihedron)();
}

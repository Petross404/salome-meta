// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_LandCoverArgsFilter.h>
#include <HYDROGUI_Operations.h>
#include <HYDROData_ArtificialObject.h>
#include <HYDROData_NaturalObject.h>
#include <HYDROData_PolylineXY.h>

HYDROGUI_LandCoverArgsFilter::HYDROGUI_LandCoverArgsFilter( int theOperationId )
  : myOperationId( theOperationId )
{
}

HYDROGUI_LandCoverArgsFilter::~HYDROGUI_LandCoverArgsFilter()
{
}

bool HYDROGUI_LandCoverArgsFilter::isOk( const Handle(HYDROData_Entity)& theEntity ) const
{
  if( theEntity.IsNull() )
    return false;

  ObjectKind aKind = theEntity->GetKind();
  if( aKind==KIND_POLYLINEXY )
  {
    Handle(HYDROData_PolylineXY) aPolylineXY = Handle(HYDROData_PolylineXY)::DownCast( theEntity );
    switch( myOperationId )
    {
    case CreateLandCoverMapId:
    case AddLandCoverId:
      return aPolylineXY->IsClosed();  // only closed polylines are allowed
    case SplitLandCoverId:
      return true;  // any polylines are allowed
    default:
      return false;
    }
  }

  Handle(HYDROData_NaturalObject) aNaturalObject = Handle(HYDROData_NaturalObject)::DownCast( theEntity );
  Handle(HYDROData_ArtificialObject) anArtificialObject = Handle(HYDROData_ArtificialObject)::DownCast( theEntity );
  bool isOK = !aNaturalObject.IsNull() || !anArtificialObject.IsNull();
  return isOK;
}

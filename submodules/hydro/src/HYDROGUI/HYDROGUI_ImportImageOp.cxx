// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportImageOp.h"

#include "HYDROGUI_DataModel.h"
#include <HYDROGUI_DataObject.h>
#include "HYDROGUI_ImportImageDlg.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_PrsImage.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Iterator.h>
#include <HYDROData_Tool.h>

#include <GraphicsView_ViewManager.h>
#include <GraphicsView_ViewPort.h>
#include <GraphicsView_Viewer.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <STD_TabDesktop.h>
#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>
#include <SUIT_ViewWindow.h>
#include <QtxWorkstack.h>
#include <QApplication>
#include <QFileInfo>

//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"

HYDROGUI_ImportImageOp::HYDROGUI_ImportImageOp( HYDROGUI_Module* theModule,
                                                const bool theIsEdit )
: HYDROGUI_Operation( theModule ),
  myIsEdit( theIsEdit ),
  myEditedObject( 0 ),
  myActiveViewManager( 0 ),
  myPreviewViewManager( 0 ),
  myRefViewManager( 0 ),
  myPreviewPrs( 0 ),
  myRefPreviewPrs( 0 ),
  myPointType( HYDROGUI_PrsImage::None )
{
  DEBTRACE("HYDROGUI_ImportImageOp");
  setName( theIsEdit ? tr( "EDIT_IMPORTED_IMAGE" ) : tr( "IMPORT_IMAGE" ) );
}

HYDROGUI_ImportImageOp::~HYDROGUI_ImportImageOp()
{
  DEBTRACE("~HYDROGUI_ImportImageOp");
}

void HYDROGUI_ImportImageOp::startOperation()
{
  DEBTRACE("startOperation");
  HYDROGUI_Operation::startOperation();

  myEditedObject = 0;
  myActiveViewManager = 0;
  myPreviewViewManager = 0;
  myRefViewManager = 0;
  myPreviewPrs = 0;
  myRefPreviewPrs = 0;
  myPointType = HYDROGUI_PrsImage::None;
  HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();
  aPanel->reset();
  aPanel->setIsEdit( myIsEdit );

  QString aRefImageName;

  if( myIsEdit )
  {
    if ( isApplyAndClose() )
      myEditedObject = Handle(HYDROData_Image)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if( !myEditedObject.IsNull() )
    {
      QImage anImage = myEditedObject->Image();
      bool anIsByTwoPoints = myEditedObject->IsByTwoPoints();

      QPoint aLocalPointA, aLocalPointB, aLocalPointC;
      myEditedObject->GetLocalPoints( aLocalPointA, aLocalPointB, aLocalPointC );

      // Create the edited image preview presentation in the viewer
      onCreatePreview( anImage, NULL ); //TODO

      // Set transformation local points A,B,C to the image preview presentation
      setPresentationTrsfPoints( myPreviewPrs, anIsByTwoPoints, aLocalPointA, 
        aLocalPointB, aLocalPointC );

      // Build the existing image local and global points mapping 
      // according to the current transformation mode.
      HYDROData_Image::TransformationMode aTrsfMode = myEditedObject->GetTrsfMode();
      QPointF aTrsfPointA, aTrsfPointB, aTrsfPointC;
      HYDROGUI_ImportImageDlg::TransformationDataMap aDataMap;
      if ( aTrsfMode == HYDROData_Image::ReferenceImage )
      {
        // Compute global points using the transformation matrix of the reference image
        Handle(HYDROData_Image) aRefImage = myEditedObject->GetTrsfReferenceImage();
        if ( !aRefImage.IsNull() )
        {
          QTransform aRefTrsf = aRefImage->Trsf(); // The reference image transformation matrix
          aTrsfPointA = aRefTrsf.map( aLocalPointA ); // Compute the global point A
          aTrsfPointB = aRefTrsf.map( aLocalPointB ); // Compute the global point B
          if ( !anIsByTwoPoints )
          {
            aTrsfPointC = aRefTrsf.map( aLocalPointC ); // Compute the global point C if used
          }
          // Build the local-global points map
          // Use the reference image transformation mode for interpreting global points
          computeTrsfData( aRefImage->GetTrsfMode(), anIsByTwoPoints, aLocalPointA, aLocalPointB, aLocalPointC,
            aTrsfPointA, aTrsfPointB, aTrsfPointC, aDataMap );
        }
      }
      else
      {
        // Get global points from the edited image
        myEditedObject->GetGlobalPoints( aTrsfMode, aTrsfPointA, aTrsfPointB, aTrsfPointC );
        // Build the local-global points map
        computeTrsfData( aTrsfMode, anIsByTwoPoints, aLocalPointA, aLocalPointB, aLocalPointC,
          aTrsfPointA, aTrsfPointB, aTrsfPointC, aDataMap );
      }

      // Initialize the dialog mode, local and global coordinates 
      // except coordinates on the reference image
      aPanel->setTransformationMode( aTrsfMode );
      aPanel->setTransformationDataMap( aDataMap );
      aPanel->setByTwoPoints( anIsByTwoPoints );

      // Set points of the reference image
      if ( aTrsfMode == HYDROData_Image::ReferenceImage )
      {
        Handle(HYDROData_Image) aRefImage;
        myEditedObject->GetReferencePoints( aRefImage,
                                         aTrsfPointA, aTrsfPointB, aTrsfPointC );
        if ( !aRefImage.IsNull() )
        {
          aRefImageName = aRefImage->GetName();

          // Create the reference image presentation in the viewer
          onRefImageActivated( aRefImageName );

          // Set transformation points A,B,C to the reference image presentation
          setPresentationTrsfPoints( myRefPreviewPrs, anIsByTwoPoints, aTrsfPointA.toPoint(), 
            aTrsfPointB.toPoint(), aTrsfPointC.toPoint() );

          // Prepare A, B, C points on the reference image
          HYDROGUI_ImportImageDlg::TransformationDataMap aRefDataMap;
          aRefDataMap[ HYDROGUI_PrsImage::PointA ] = 
            HYDROGUI_ImportImageDlg::TransformationData( aTrsfPointA.toPoint() );
          aRefDataMap[ HYDROGUI_PrsImage::PointB ] = 
            HYDROGUI_ImportImageDlg::TransformationData( aTrsfPointB.toPoint() );
          if ( !anIsByTwoPoints )
            aRefDataMap[ HYDROGUI_PrsImage::PointC ] = 
            HYDROGUI_ImportImageDlg::TransformationData( aTrsfPointC.toPoint() );

          // Initialize the dialog's A, B, C points coordinates of the reference image
          aPanel->setTransformationDataMap( aRefDataMap, true, true );
        }
      }
    }
  }

  HYDROGUI_ImportImageDlg::PrsPointDataList aPrsPointDataList;
  getReferenceDataList( aPrsPointDataList );
  aPanel->setPrsPointDataList( aPrsPointDataList );
  // Select the current reference image in the dialog combobox
  aPanel->setRefImageName( aRefImageName );
}

void HYDROGUI_ImportImageOp::setPresentationTrsfPoints( HYDROGUI_PrsImage* thePrs, 
                                                        bool theIsByTwoPoints,
                                                        const QPoint theLocalPointA, 
                                                        const QPoint theLocalPointB, 
                                                        const QPoint theLocalPointC )
{
  DEBTRACE("setPresentationTrsfPoints");
  // Set transformation points A,B,C to the image presentation
  if( thePrs )
  {
    HYDROGUI_PrsImage::TransformationPointMap aPointMap =
      thePrs->getTransformationPointMap();
    if( !aPointMap.isEmpty() )
    {
      aPointMap[ HYDROGUI_PrsImage::PointA ].Point = theLocalPointA;
      aPointMap[ HYDROGUI_PrsImage::PointB ].Point = theLocalPointB;
      if ( !theIsByTwoPoints )
        aPointMap[ HYDROGUI_PrsImage::PointC ].Point = theLocalPointC;

      thePrs->setIsByTwoPoints( theIsByTwoPoints );
      thePrs->setTransformationPointMap( aPointMap );
    }
  }
}

void HYDROGUI_ImportImageOp::getReferenceDataList(
                        HYDROGUI_ImportImageDlg::PrsPointDataList& theList ) const
{
  DEBTRACE("getReferenceDataList");
  // Collect information about existing images and initialize the combobox 
  // reference image selector in the dialog.
  HYDROData_Iterator anIterator( doc(), KIND_IMAGE );
  for( ; anIterator.More(); anIterator.Next() )
  {
    Handle(HYDROData_Image) anImageObj = Handle(HYDROData_Image)::DownCast( anIterator.Current() );
    if( !anImageObj.IsNull() )
    {
      if( myIsEdit && IsEqual( anImageObj, myEditedObject ) )
        continue;

      QPoint aLocalPointA, aLocalPointB, aLocalPointC;
      anImageObj->GetLocalPoints( aLocalPointA, aLocalPointB, aLocalPointC );
      
      HYDROData_Image::TransformationMode aImgTrsfMode;
      QPointF aTrsfPointA, aTrsfPointB, aTrsfPointC;
      anImageObj->GetGlobalPoints( aImgTrsfMode,
                                   aTrsfPointA, aTrsfPointB, aTrsfPointC );

      HYDROGUI_ImportImageDlg::TransformationDataMap aDataMap;
      computeTrsfData( aImgTrsfMode, false, aLocalPointA, aLocalPointB, aLocalPointC,
        aTrsfPointA, aTrsfPointB, aTrsfPointC, aDataMap );

      HYDROGUI_ImportImageDlg::PrsPointData aPrsPointData( anImageObj->GetName(), aDataMap );
      theList.append( aPrsPointData );
    }
  }
}

void HYDROGUI_ImportImageOp::computeTrsfData( HYDROData_Image::TransformationMode theTrsfMode,
                                              bool theIsByTwoPoints,
                                              const QPoint& theLocalPointA,
                                              const QPoint& theLocalPointB,
                                              const QPoint& theLocalPointC,
                                              const QPointF& theGlobalPointA,
                                              const QPointF& theGlobalPointB,
                                              const QPointF& theGlobalPointC,
                                              HYDROGUI_ImportImageDlg::TransformationDataMap& theDataMap ) const
{
  DEBTRACE("computeTrsfData");
  // Build the local-global points map
  HYDROGUI_ImportImageDlg::TransformationDataMap aDataMap;
  theDataMap[ HYDROGUI_PrsImage::PointA ] = 
    HYDROGUI_ImportImageDlg::ComputeTrsfData( theTrsfMode, theLocalPointA, theGlobalPointA );
  theDataMap[ HYDROGUI_PrsImage::PointB ] =
    HYDROGUI_ImportImageDlg::ComputeTrsfData( theTrsfMode, theLocalPointB, theGlobalPointB );
  if ( !theIsByTwoPoints )
  {
    theDataMap[ HYDROGUI_PrsImage::PointC ] =
      HYDROGUI_ImportImageDlg::ComputeTrsfData( theTrsfMode, theLocalPointC, theGlobalPointC );
  }
}

void HYDROGUI_ImportImageOp::abortOperation()
{
  DEBTRACE("abortOperation");
  closePreview();
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ImportImageOp::commitOperation()
{
  DEBTRACE("commitOperation");
  if ( isApplyAndClose() )
    closePreview();

  HYDROGUI_Operation::commitOperation();
}

HYDROGUI_InputPanel* HYDROGUI_ImportImageOp::createInputPanel() const
{
  DEBTRACE("createInputPanel");
  HYDROGUI_InputPanel* aPanel = new HYDROGUI_ImportImageDlg( module(), getName() );
  connect( aPanel, SIGNAL( createPreview( QImage, HYDROData_Image::ECW_FileInfo* ) ), 
    SLOT( onCreatePreview( QImage, HYDROData_Image::ECW_FileInfo* ) ) );
  connect( aPanel, SIGNAL( activatePointSelection( int ) ), SLOT( onActivatePointSelection( int ) ) );
  connect( aPanel, SIGNAL( pointCoordChanged( bool, int, bool, int ) ), 
                     SLOT( onPointCoordChanged( bool, int, bool, int ) ) );
  connect( aPanel, SIGNAL( modeActivated( int ) ), SLOT( onModeActivated( int ) ) );
  connect( aPanel, SIGNAL( refImageActivated( const QString& ) ),
                     SLOT( onRefImageActivated( const QString& ) ) );
  connect( aPanel, SIGNAL( setCIsUsed( bool ) ), SLOT( onSetCIsUsed( bool ) ) );
  connect( aPanel, SIGNAL( filesSelected( const QStringList& ) ),
                   SLOT( onFilesSelected( const QStringList& ) ) );
  return aPanel;
}

void HYDROGUI_ImportImageOp::onSetCIsUsed( bool theCIsUsed )
{
  DEBTRACE("onSetCIsUsed");
  if ( myPreviewPrs )
  {
    myPreviewPrs->setIsByTwoPoints( !theCIsUsed );
  }
  if ( myRefPreviewPrs )
  {
    myRefPreviewPrs->setIsByTwoPoints( !theCIsUsed );
  }
}

bool HYDROGUI_ImportImageOp::checkPoints( const QPointF& thePointA,
                                          const QPointF& thePointB,
                                          const QPointF& thePointC,
                                          const bool     theIsByTwoPoints,
                                          const QString& theLineErrMsg,
                                          const QString& thePoinErrMsg,
                                          QString&       theErrorMsg,
                                          const bool     theIsToCheckInvertibles ) const
{
  DEBTRACE("checkPoints");
  double xa = thePointA.x();
  double ya = thePointA.y();
  double xb = thePointB.x();
  double yb = thePointB.y();
  double xc = thePointC.x();
  double yc = thePointC.y();

  if ( !theIsByTwoPoints )
  {
    // check that three input points don't belong to a single line
    if ( ValuesEquals( ( yb - ya ) * ( xc - xa ), ( yc - ya ) * ( xb - xa ) ) )
    {
      theErrorMsg = theLineErrMsg;
      return false;
    }

    if ( theIsToCheckInvertibles )
    {
      QTransform aTransform1( xa, ya, 1, xb, yb, 1, xc, yc, 1 );

      bool anIsInvertible = false;
      QTransform aTransform1Inverted = aTransform1.inverted( &anIsInvertible );
      if( !anIsInvertible )
      {
        theErrorMsg = tr( "TRANSFORMATION_MATRIX_CANNOT_BE_COMPUTED" );
        return false;
      }
    }
  }
  else 
  {
    // check that two points are not identical
    if ( ValuesEquals( xa, xb ) && ValuesEquals( ya, yb ) )
    {
      theErrorMsg = thePoinErrMsg;
      return false;
    }
  }

  return true;
}

bool HYDROGUI_ImportImageOp::processApply( int& theUpdateFlags,
                                           QString& theErrorMsg,
                                           QStringList& theBrowseObjectsEntries )
{
  DEBTRACE("processApply");
  HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();

  if( !myIsEdit )
  {
    QString aFilePath = aPanel->getFileName();
    if( aFilePath.isEmpty() )
    {
      theErrorMsg = tr( "SELECT_IMAGE_FILE" ).arg( aFilePath );
      return false;
    }
  }

  QString anImageName = aPanel->getImageName();
  if( anImageName.isEmpty() )
  {
    theErrorMsg = tr( "SELECT_IMAGE_NAME" ).arg( anImageName );
    return false;
  }

  if( !myIsEdit || ( !myEditedObject.IsNull() && myEditedObject->GetName() != anImageName ) )
  {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anImageName );
    if( !anObject.IsNull() )
    {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anImageName );
      return false;
    }
  }

  HYDROData_Image::TransformationMode aTransformationMode = 
    (HYDROData_Image::TransformationMode)aPanel->getTransformationMode();
  
  QPoint aPointA, aPointB, aPointC;
  QPointF aTrsfPointA, aTrsfPointB, aTrsfPointC( INT_MIN, INT_MIN );
  Handle(HYDROData_Image) aRefImageObj;

  if ( aTransformationMode != HYDROData_Image::CartesianFromFile ) {
    HYDROGUI_ImportImageDlg::TransformationDataMap aMap;
    if( !aPanel->getTransformationDataMap( aMap ) )
      return false;

    bool anIsByTwoPoints = aPanel->isByTwoPoints();

    aPointA = aMap[ HYDROGUI_PrsImage::PointA ].ImagePoint;
    aPointB = aMap[ HYDROGUI_PrsImage::PointB ].ImagePoint;
    aPointC = anIsByTwoPoints ? QPoint( INT_MIN, INT_MIN ) :
                                aMap[ HYDROGUI_PrsImage::PointC ].ImagePoint;

    // first, we check correctness of image points
    if ( !checkPoints( aPointA, aPointB, aPointC, anIsByTwoPoints, 
                       tr( "POINTS_A_B_C_BELONG_TO_SINGLE_LINE" ),
                       tr( "POINTS_A_B_ARE_IDENTICAL" ),
                       theErrorMsg, true ) )
      return false;

    if ( aTransformationMode == HYDROData_Image::ReferenceImage )
    {
      QString aRefImageName = aPanel->getRefImageName();
      if( aRefImageName.isEmpty() )
      {
        theErrorMsg = tr( "REFERENCE_IMAGE_IS_NOT_SELECTED" );
        return false;
      }

      HYDROGUI_ImportImageDlg::TransformationDataMap aRefMap;
      if( !aPanel->getTransformationDataMap( aRefMap, true ) )
        return false;

      aRefImageObj = Handle(HYDROData_Image)::DownCast(
        HYDROGUI_Tool::FindObjectByName( module(), aRefImageName, KIND_IMAGE ) );
      if( aRefImageObj.IsNull() ) {
        return false;
      }
      else if ( !isReferenceCorrect() ) {
        aRefImageObj->RemoveAllReferences();
      }

      aTrsfPointA = aRefMap[ HYDROGUI_PrsImage::PointA ].ImagePoint;
      aTrsfPointB = aRefMap[ HYDROGUI_PrsImage::PointB ].ImagePoint;
      if ( !anIsByTwoPoints )
      aTrsfPointC = aRefMap[ HYDROGUI_PrsImage::PointC ].ImagePoint;

      // the same check of correctness for the reference points
      if ( !checkPoints( aTrsfPointA, aTrsfPointB, aTrsfPointC, anIsByTwoPoints, 
                         tr( "REFERENCE_POINTS_A_B_C_BELONG_TO_SINGLE_LINE" ),
                         tr( "REFERENCE_POINTS_A_B_ARE_IDENTICAL" ),
                         theErrorMsg, false ) )
        return false;
    }
    else
    {
      if ( aTransformationMode == HYDROData_Image::ManualGeodesic )
      {
        aTrsfPointA = aMap[ HYDROGUI_PrsImage::PointA ].GeodesicPoint;
        aTrsfPointB = aMap[ HYDROGUI_PrsImage::PointB ].GeodesicPoint;
        if ( !anIsByTwoPoints )
          aTrsfPointC = aMap[ HYDROGUI_PrsImage::PointC ].GeodesicPoint;
      }
      else
      {
        aTrsfPointA = aMap[ HYDROGUI_PrsImage::PointA ].CartesianPoint;
        aTrsfPointB = aMap[ HYDROGUI_PrsImage::PointB ].CartesianPoint;
        if ( !anIsByTwoPoints )
          aTrsfPointC = aMap[ HYDROGUI_PrsImage::PointC ].CartesianPoint;
      }
    }
  }

  Handle(HYDROData_Image) anImageObj;
  if( myIsEdit )
    anImageObj = myEditedObject;
  else
  {
    anImageObj = Handle(HYDROData_Image)::DownCast( doc()->CreateObject( KIND_IMAGE ) );
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( anImageObj );
    theBrowseObjectsEntries.append( anEntry );
  }

  if( anImageObj.IsNull() )
    return false;

  anImageObj->SetName( anImageName );
  anImageObj->SetImage( myImage );
  
  if ( aTransformationMode == HYDROData_Image::CartesianFromFile ) {
    QString aGeoreferencementFileName = aPanel->getGeoreferencementFileName();
    if ( aGeoreferencementFileName.isEmpty() ) {
      return false;
    }

    QPoint aLocalPointA( 0, 0 ),
           aLocalPointB( anImageObj->Image().width(), 0 ), 
           aLocalPointC( INT_MIN, INT_MIN );
    anImageObj->SetLocalPoints( aLocalPointA, aLocalPointB, aLocalPointC, false );
    if ( !anImageObj->SetGlobalPointsFromFile( aGeoreferencementFileName ) ) {
      theErrorMsg = tr( "CANT_LOAD_GEOREFERENCEMENT_FILE" );
      return false;
    }
  } else {
    anImageObj->SetLocalPoints( aPointA, aPointB, aPointC, false );

    if ( aTransformationMode == HYDROData_Image::ReferenceImage )
    {
      anImageObj->SetReferencePoints( aRefImageObj,
                                    aTrsfPointA, aTrsfPointB, aTrsfPointC );
    }
    else
    {
      anImageObj->SetGlobalPoints( aTransformationMode,
                                 aTrsfPointA, aTrsfPointB, aTrsfPointC );
    }
  }

  if( !myIsEdit )
  {
    // Set imported file name for image
    QString aFilePath = aPanel->getFileName();
    anImageObj->SetFilePath( aFilePath );
  }
   
  // must be done after all checks and before calling SetVisible() method below
  if ( isApplyAndClose() )
  {
    //DEBTRACE("closePreview");
    closePreview();
  }

  if( !myIsEdit )
    module()->setObjectVisible( HYDROGUI_Tool::GetActiveGraphicsViewId( module() ), anImageObj, true );

  anImageObj->Update();

  theUpdateFlags = UF_Model;
  if ( isApplyAndClose() )
    theUpdateFlags |= UF_Viewer | UF_GV_Forced | UF_OCCViewer | UF_OCC_Forced;

  if( isApplyAndClose() )
  {
    commitDocOperation(); // to save the modifications in the data model
    return true;
  }

  if( SetNextFile() )
  {
    theErrorMsg = "";
    module()->updateObjBrowser();
    return false;         // and to continue the operation
  }

  /*if( myFiles.count() > 1 )
  {
    setIsApplyAndClose( true );
  }*/
  return true;
}

bool HYDROGUI_ImportImageOp::isReferenceCorrect() const
{
  DEBTRACE("isReferenceCorrect");
  bool isCorrect = true;

  if( myIsEdit && !myEditedObject.IsNull() )
  {
    HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();
    Handle(HYDROData_Image) aRefImageObj = Handle(HYDROData_Image)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aPanel->getRefImageName(), KIND_IMAGE ) );
    if( !aRefImageObj.IsNull() )
    {
      bool isFoundEdited = false;
      HYDROData_SequenceOfObjects aRefSeq = aRefImageObj->GetAllReferenceObjects();
      for ( int i = 1, n = aRefSeq.Length(); i <= n && !isFoundEdited; ++i )
      {
        Handle(HYDROData_Entity) anObject = aRefSeq.Value( i );
        isFoundEdited = anObject->GetName() == myEditedObject->GetName();
      }
      isCorrect = !isFoundEdited;
    }
  }
  return isCorrect;
}

void HYDROGUI_ImportImageOp::apply()
{
  DEBTRACE("apply");
  HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();

  bool aCanApply = isReferenceCorrect();
  if ( !aCanApply && !myEditedObject.IsNull() )
  {
    Handle(HYDROData_Image) aRefImageObj = Handle(HYDROData_Image)::DownCast(
      HYDROGUI_Tool::FindObjectByName( module(), aPanel->getRefImageName(), KIND_IMAGE ) );
    if ( !aRefImageObj.IsNull() )
      aCanApply = SUIT_MessageBox::question( module()->getApp()->desktop(),
                    tr( "CORRECT_INPUT_DATA" ), tr( "CONFIRM_REMOVE_REFERENCE_FROM_IMAGE" ).
                    arg( aRefImageObj->GetName() ).arg( myEditedObject->GetName() ),
                    QMessageBox::Yes | QMessageBox::No, QMessageBox::No ) == QMessageBox::Yes;
  }
  if ( aCanApply )
    HYDROGUI_Operation::apply();
  else {
    aPanel->setRefImageName( "" );
    onRefImageActivated( aPanel->getRefImageName() );
  }
}

void HYDROGUI_ImportImageOp::onCreatePreview( QImage theImage, HYDROData_Image::ECW_FileInfo* theFileInfo )
{
  DEBTRACE("onCreatePreview");
  HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();

  myImage = theImage;

  if( myPreviewPrs ) // if the image is changed by choosing another file
  {
    myPreviewPrs->setImage( myImage, true );
    if ( sender() ) // reset the previous presentation settings
    {
      QString aFileName = aPanel->getFileName();

      aPanel->reset();
      aPanel->setIsEdit( myIsEdit );
      // restore the file name
      aPanel->setFileName( aFileName );
      // fill the reference list
      HYDROGUI_ImportImageDlg::PrsPointDataList aPrsPointDataList;
      getReferenceDataList( aPrsPointDataList );
      aPanel->setPrsPointDataList( aPrsPointDataList );
    }
    if( myPreviewViewManager )
    {
      if( GraphicsView_Viewer* aViewer = myPreviewViewManager->getViewer() )
      {
        if( GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort() )
        {
          DEBTRACE("myPreviewViewManager exists");
//          QTransform transform;
//          transform.scale(1.0, -1.0);
//          aViewPort->setTransform(transform, false);
          aViewPort->onBoundingRectChanged();
          aViewPort->fitAll();
        }
      }
    }
  }
  else
  {
    LightApp_Application* anApp = module()->getApp();

    myActiveViewManager = anApp->activeViewManager();

    myPreviewPrs = new HYDROGUI_PrsImage( myIsEdit ? myEditedObject : 0 );
    myPreviewPrs->setImage( myImage, true );

    myPreviewViewManager =
      dynamic_cast<GraphicsView_ViewManager*>( anApp->createViewManager( GraphicsView_Viewer::Type() ) );
    if( myPreviewViewManager )
    {
      DEBTRACE("myPreviewViewManager created " << myPreviewViewManager);
      connect( myPreviewViewManager, SIGNAL( lastViewClosed( SUIT_ViewManager* ) ),
               this, SLOT( onLastViewClosed( SUIT_ViewManager* ) ) );

      module()->setViewManagerRole( myPreviewViewManager, HYDROGUI_Module::VMR_TransformImage );
      myPreviewViewManager->setTitle( tr( "TRANSFORM_IMAGE" ) );
      if( GraphicsView_Viewer* aViewer = myPreviewViewManager->getViewer() )
      {
        if( GraphicsView_ViewPort* aViewPort = aViewer->getActiveViewPort() )
        {
          DEBTRACE("aViewPort " << aViewPort);
          //aViewPort->setMousePositionEnabled( true ); //ouv: temporarily commented

          QTransform transform;
          transform.scale(1.0, -1.0);
          aViewPort->setTransform(transform, false);
          aViewPort->addItem( myPreviewPrs );
          aViewPort->fitAll();
          
          if ( myEditedObject ) {
            size_t aViewId = (size_t)aViewer;
            module()->setObjectVisible( aViewId, myEditedObject, true );
          }
          
          myPreviewPrs->setIsTransformationPointPreview( true );
          myPreviewPrs->setTransformationPointCursorShape( module()->getPrefEditCursor().shape() );
        }
        connect( aViewer, SIGNAL( selectionChanged( GV_SelectionChangeStatus ) ),
                 this, SLOT( onPointSelected() ) );
      }
    }
  }

  // Set the image name in the dialog
  QString anImageName = aPanel->getImageName().simplified();
  // If edit mode and the name was not set yet then get from the edited object
  if( myIsEdit && anImageName.isEmpty() )
  {
    if( !myEditedObject.IsNull() )
      anImageName = myEditedObject->GetName();
  }
  // If the name was not set then initialize it from the selected file name
  if ( anImageName.isEmpty() )
  {
    anImageName = aPanel->getFileName();
    if ( !anImageName.isEmpty() ) {
        anImageName = QFileInfo( anImageName ).baseName();
    }
    // If no file name then generate a new image name
    if ( anImageName.isEmpty() ) {
      anImageName = HYDROGUI_Tool::GenerateObjectName( module(), tr( "DEFAULT_IMAGE_NAME" ) );
    }
  }
  aPanel->setImageName( anImageName );

  aPanel->setImageSize( myImage.size() );

  if (!theFileInfo)
    aPanel->initializePointSelection();
  else
     aPanel->ECW_initializePointSelection(theFileInfo);
  onPointSelected( false );
  onSetCIsUsed( !aPanel->isByTwoPoints() );
}

void HYDROGUI_ImportImageOp::onActivatePointSelection( int thePointType )
{
  DEBTRACE("onActivatePointSelection");
  myPointType = thePointType;
  if( myPreviewPrs )
    myPreviewPrs->setTransformationPointType( thePointType );
  if( myRefPreviewPrs )
    myRefPreviewPrs->setTransformationPointType( thePointType );
}

void HYDROGUI_ImportImageOp::onPointCoordChanged( bool theIsRef,
                                                  int thePointType,
                                                  bool theIsY,
                                                  int theValue )
{
  DEBTRACE("onPointCoordChanged");
  if( !theIsRef && myPreviewPrs )
    myPreviewPrs->updateTransformationPoint( thePointType, theIsY, theValue );
  else if( theIsRef && myRefPreviewPrs )
    myRefPreviewPrs->updateTransformationPoint( thePointType, theIsY, theValue );
}

void HYDROGUI_ImportImageOp::onModeActivated( int theMode )
{
  DEBTRACE("onModeActivated " << theMode);
  HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();

  QString aRefImageName;
  if( theMode == HYDROData_Image::ReferenceImage )
  {
    aRefImageName = aPanel->getRefImageName();
    if( aRefImageName.isEmpty() )
      return; // do nothing in this case to avoid visual moving of preview prs
    onRefImageActivated( aRefImageName );
  }
  else
  {
    if( myRefViewManager )
    {
      closeView( myRefViewManager );
      myRefViewManager = 0;
    }
  }
}

void HYDROGUI_ImportImageOp::onRefImageActivated( const QString& theName )
{
  DEBTRACE("onRefImageActivated " << theName.toStdString());
  if( theName.isEmpty() ) {
    if( myRefViewManager )
      closeView( myRefViewManager );
    myRefViewManager = 0;
    return;
  }

  GraphicsView_ViewPort* aViewPort = 0;

  LightApp_Application* anApp = module()->getApp();
  ///// Get a view port for the reference image preview
  if( myRefViewManager )
  {
    if( GraphicsView_Viewer* aViewer = myRefViewManager->getViewer() )
    {
      aViewPort = aViewer->getActiveViewPort();
    }
  }
  else
  {
    anApp = module()->getApp();
    // Init reference image preview
    myRefViewManager =
      dynamic_cast<GraphicsView_ViewManager*>( anApp->createViewManager( GraphicsView_Viewer::Type() ) );
    if( myRefViewManager )
    {
      DEBTRACE("myRefViewManager created " << myRefViewManager);
      connect( myRefViewManager, SIGNAL( lastViewClosed( SUIT_ViewManager* ) ),
               this, SLOT( onLastViewClosed( SUIT_ViewManager* ) ) );

      module()->setViewManagerRole( myRefViewManager, HYDROGUI_Module::VMR_ReferenceImage );
      myRefViewManager->setTitle( tr( "REFERENCE_IMAGE" ) );
      if( GraphicsView_Viewer* aViewer = myRefViewManager->getViewer() )
      {
        aViewPort = aViewer->getActiveViewPort();
        connect( aViewer, SIGNAL( selectionChanged( GV_SelectionChangeStatus ) ),
                 this, SLOT( onRefPointSelected() ) );
      }
    }
  }

  if( !aViewPort )
    return;
  QTransform transform;
  transform.scale(1.0, -1.0);
  aViewPort->setTransform(transform, false);

  size_t aViewId = (size_t)myRefViewManager->getViewer();

  // Remove the old presentation of the reference image if any
  if( myRefPreviewPrs )
  {
    module()->setObjectVisible( aViewId, myRefPreviewPrs->getObject(), false );

    myRefPreviewPrs->setCaption( QString() );
    aViewPort->removeItem( myRefPreviewPrs );

    delete myRefPreviewPrs;
    myRefPreviewPrs = 0;
  }

  // Create a new reference image presentation
  QImage anImage;
  Handle(HYDROData_Image) anImageObj = Handle(HYDROData_Image)::DownCast(
    HYDROGUI_Tool::FindObjectByName( module(), theName, KIND_IMAGE ) );
  HYDROGUI_ImportImageDlg* aPanel = (HYDROGUI_ImportImageDlg*)inputPanel();
  if( !anImageObj.IsNull() )
  {
    anImage = anImageObj->Image();

    myRefPreviewPrs = new HYDROGUI_PrsImage( anImageObj );
    myRefPreviewPrs->setImage( anImage, true );

    myRefPreviewPrs->setIsTransformationPointPreview( true );
    myRefPreviewPrs->setTransformationPointType( myPointType );

    myRefPreviewPrs->setIsByTwoPoints( aPanel->isByTwoPoints() );

    myRefPreviewPrs->setTransformationPointCursorShape( module()->getPrefEditCursor().shape() );

    // Add the new reference image presentation to the appropriate view
    aViewPort->addItem( myRefPreviewPrs );

    module()->setObjectVisible( aViewId, anImageObj, true );
  }

  aViewPort->fitAll();

  // Split views horizontally
  if( anApp->desktop()->inherits( "STD_TabDesktop" ) )
  {
    qApp->processEvents();
    QtxWorkstack* aWorkstack = ( (STD_TabDesktop*)anApp->desktop() )->workstack();
    aViewPort->activateWindow();
    aViewPort->show();
    aViewPort->setFocus(Qt::ActiveWindowFocusReason);
    aWorkstack->splitHorizontal();
  }

  // Initialize the dialog
  aPanel->setImageSize( anImage.size(), true );
  aPanel->initializePointSelection();
  onPointSelected( true );
}

void HYDROGUI_ImportImageOp::onLastViewClosed( SUIT_ViewManager* theViewManager )
{
  DEBTRACE("onLastViewClosed");
  closePreview();
}

void HYDROGUI_ImportImageOp::onPointSelected()
{
  DEBTRACE("onPointSelected");
  onPointSelected( myRefPreviewPrs && myRefPreviewPrs->isSelected() );
}

void HYDROGUI_ImportImageOp::onRefPointSelected()
{
  DEBTRACE("onRefPointSelected");
  onPointSelected( true );
}

void HYDROGUI_ImportImageOp::onPointSelected( bool theIsRefImage )
{
  DEBTRACE("onPointSelected " << theIsRefImage);
  HYDROGUI_PrsImage* aPrs = theIsRefImage ? myRefPreviewPrs : myPreviewPrs;
  if( !aPrs )
    return;

  HYDROGUI_ImportImageDlg::TransformationDataMap aDataMap;

  const HYDROGUI_PrsImage::TransformationPointMap& aPointMap =
    aPrs->getTransformationPointMap();
  
  HYDROGUI_PrsImage::TransformationPointMapIterator anIter( aPointMap );
  while( anIter.hasNext() )
  {
    int aPointType = anIter.next().key();
    const HYDROGUI_PrsImage::TransformationPoint& aTransformationPoint = anIter.value();
    const QPoint& aPoint = aTransformationPoint.Point;

    HYDROGUI_ImportImageDlg::TransformationData aData( aPoint, QPointF(), QPointF() );
    aDataMap[ aPointType ] = aData;
  }

  ( (HYDROGUI_ImportImageDlg*)inputPanel() )->setTransformationDataMap( aDataMap, true, theIsRefImage );
}

void HYDROGUI_ImportImageOp::closePreview()
{
  DEBTRACE("closePreview");
  closeView( myPreviewViewManager );
  myPreviewViewManager = 0;
  closeView( myRefViewManager );
  myRefViewManager = 0;

  if( myActiveViewManager )
    HYDROGUI_Tool::SetActiveViewManager( module(), myActiveViewManager );
}

void HYDROGUI_ImportImageOp::closeView( GraphicsView_ViewManager* &aViewMgr )
{
  DEBTRACE("closeView " << aViewMgr);
  if( aViewMgr )
  {
    GraphicsView_ViewPort* aViewPort = 0;
    if( GraphicsView_Viewer* aViewer = aViewMgr->getViewer() )
    {
      aViewPort = aViewer->getActiveViewPort();
    }
    disconnect( aViewMgr, SIGNAL( lastViewClosed( SUIT_ViewManager* ) ),
                this, SLOT( onLastViewClosed( SUIT_ViewManager* ) ) );

    // Nullify appropriate presentation pointer
    HYDROGUI_PrsImage* aPrs = 0;
    switch ( module()->getViewManagerRole( aViewMgr ) )
    {
      case HYDROGUI_Module::VMR_ReferenceImage:
        DEBTRACE("  myRefPreviewPrs = 0");
        aPrs = myRefPreviewPrs;
        myRefPreviewPrs = 0;
        break;
      case HYDROGUI_Module::VMR_TransformImage:
        DEBTRACE("  myPreviewPrs = 0");
        aPrs = myPreviewPrs;
        myPreviewPrs = 0;
    }

    // Remove the appropriate presentation from the view
    if( aPrs )
    {
      if( aViewPort )
      {
        aViewPort->removeItem( aPrs );
      }
      delete aPrs;
    }

    // Delete the view
    module()->removeViewManager(aViewMgr); // clean HYDROGUI module structure
    aViewMgr->getActiveView()->hide();
    module()->getApp()->removeViewManager( aViewMgr ); // aViewMgr is deleted here
    aViewMgr = 0;
    DEBTRACE("end of closeView");
  }
}

void HYDROGUI_ImportImageOp::onFilesSelected( const QStringList& theFileNames )
{
  DEBTRACE("onFilesSelected");
  myFiles = theFileNames;
  myFileIndex = -1;
  SetNextFile();
}

bool HYDROGUI_ImportImageOp::SetNextFile()
{
  DEBTRACE("SetNextFile");
  myFileIndex++;
  bool isEnabledEdit = myFiles.count()==1 || myFileIndex==myFiles.count();
  bool isValid = ( myFileIndex>=0 && myFileIndex<myFiles.count() );
  QString aFile = isValid ? myFiles[myFileIndex] : "";

  HYDROGUI_ImportImageDlg* aPanel = dynamic_cast<HYDROGUI_ImportImageDlg*>( inputPanel() );
  aPanel->ActivateFile( aFile, isEnabledEdit );
  return isValid;
}

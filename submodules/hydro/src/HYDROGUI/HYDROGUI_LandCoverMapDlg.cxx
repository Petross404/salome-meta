// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_LandCoverMapDlg.h"

#include "HYDROGUI_Operations.h"
#include "HYDROGUI_StricklerTypeComboBox.h"

#include <HYDROData_Object.h>
#include <HYDROData_PolylineXY.h>

#include <OCCViewer_ViewManager.h>

#include <LightApp_Application.h>
#include <LightApp_SelectionMgr.h>
#include <LightApp_DataOwner.h>

#include <SUIT_DataObject.h>

#include <QLineEdit>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>

HYDROGUI_LandCoverMapDlg::HYDROGUI_LandCoverMapDlg( HYDROGUI_Module* theModule,
                                                    const QString& theTitle,
                                                    const int theOperationId )
: HYDROGUI_InputPanel( theModule, theTitle ),
  myOperationId( theOperationId ),
  myFilter( theOperationId )
{
  // Land Cover Map name
  myObjectNameGroup = new QGroupBox( tr( "LAND_COVER_MAP_NAME" ), mainFrame() );

  myObjectNameCreate = new QLineEdit( myObjectNameGroup );
  myObjectNameEdit = new QComboBox( myObjectNameGroup );
  myObjectNameEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );

  QBoxLayout* aNameLayout = new QHBoxLayout( myObjectNameGroup );
  aNameLayout->setMargin( 5 );
  aNameLayout->setSpacing( 5 );
  aNameLayout->addWidget( new QLabel( tr( "NAME" ), myObjectNameGroup ) );
  aNameLayout->addWidget( myObjectNameCreate );
  aNameLayout->addWidget( myObjectNameEdit );

  myParamGroup = new QGroupBox( tr( "LAND_COVER_MAP_PARAMETERS" ), mainFrame() );
  QGridLayout* aParamLayout = new QGridLayout( myParamGroup );
  aParamLayout->setMargin( 5 );
  aParamLayout->setSpacing( 10 );
  
  // Polyline/Face name
  myPolylinesFacesLabel = new QLabel( tr( "LAND_COVER_MAP_POLYLINE_FACE" ) );
  aParamLayout->addWidget( myPolylinesFacesLabel, 0, 0, 1, 1 );
  aParamLayout->addWidget( myPolylinesFaces = new HYDROGUI_ObjComboBox( theModule, "", KIND_UNKNOWN, myParamGroup ), 0, 1, 1, 1 );
  myPolylinesFaces->setObjectFilter( &myFilter );

  // Strickler type name
  myStricklerTypesLabel = new QLabel( tr( "LAND_COVER_MAP_STRICKLER_TYPE" ), myParamGroup );
  myStricklerTypes = new HYDROGUI_StricklerTypeComboBox( theModule, "", myParamGroup );
  aParamLayout->addWidget( myStricklerTypesLabel, 1, 0, 1, 1 );
  aParamLayout->addWidget( myStricklerTypes, 1, 1, 1, 1 );

  // Label indicated the number of selected land covers
  QString aDefaultLabel( "0" );
  mySelectedLandCoversLabel = new QLabel( tr( "LAND_COVER_MAP_SELECTED_FACES" ) + aDefaultLabel, myParamGroup );  
  mySelectedLandCoversLabel->setStyleSheet("QLabel { font: italic; color : red; }");

  // Common
  addWidget( myObjectNameGroup );
  addWidget( myParamGroup );
  addWidget( mySelectedLandCoversLabel );

  addStretch();

  // Connect signals and slots
  connect( myObjectNameEdit, SIGNAL( currentIndexChanged( int ) ), this, SLOT( onLandCoverMapChanged() ) );
  connect( myPolylinesFaces, SIGNAL( objectSelected( const QString& ) ), this, SLOT( onPolylineFaceChanged() ) );

  updateState( true );

}

HYDROGUI_LandCoverMapDlg::~HYDROGUI_LandCoverMapDlg()
{
}

void HYDROGUI_LandCoverMapDlg::reset()
{
  bool isBlocked = blockSignals( true );

  myPolylinesFaces->reset();

  blockSignals( isBlocked );

  updateState();
}

void HYDROGUI_LandCoverMapDlg::setObjectNames( const QStringList& theNames )
{
  bool isBlocked = blockSignals( true );

  myObjectNameEdit->clear();
  myObjectNameEdit->addItems( theNames );

  blockSignals( isBlocked );
}

void HYDROGUI_LandCoverMapDlg::setObjectName( const QString& theName )
{
  if ( myObjectNameCreate->isVisible() )
    myObjectNameCreate->setText( theName );
  else
    myObjectNameEdit->setCurrentIndex( myObjectNameEdit->findText( theName ) );

  updateState();
}

QString HYDROGUI_LandCoverMapDlg::getObjectName() const
{
  if ( myObjectNameCreate->isVisible() )
    return myObjectNameCreate->text();
  return myObjectNameEdit->currentText();
}

QString HYDROGUI_LandCoverMapDlg::getPolylineFaceName() const
{
  return myPolylinesFaces->selectedObject();
}

void HYDROGUI_LandCoverMapDlg::setPolylineFaceName( const QString& theName )
{
  myPolylinesFaces->setSelectedObject( theName );
}

Handle(HYDROData_Entity) HYDROGUI_LandCoverMapDlg::getPolylineFace() const
{
  return myPolylinesFaces->GetObject();
}

void HYDROGUI_LandCoverMapDlg::setSelectedStricklerTypeName( const QString& theName )
{
  myStricklerTypes->setSelectedStricklerTypeName( theName );
}

QString HYDROGUI_LandCoverMapDlg::getSelectedStricklerTypeName() const
{
  return myStricklerTypes->getSelectedStricklerTypeName();
}

void HYDROGUI_LandCoverMapDlg::updateSelectedLandCoversLabel( int theNbSelected )
{
  QString aLabel;
  aLabel.setNum( theNbSelected );
  if ( theNbSelected == 0 )
    mySelectedLandCoversLabel->setStyleSheet("QLabel { font: italic; color : red; }");
  else
    mySelectedLandCoversLabel->setStyleSheet("QLabel { font: italic; color : black; }");
  mySelectedLandCoversLabel->setText( tr( "LAND_COVER_MAP_SELECTED_FACES" ) + aLabel );
}

void HYDROGUI_LandCoverMapDlg::onLandCoverMapChanged()
{
  if ( signalsBlocked() )
    return;

  reset();

  emit landCoverMapChanged( getObjectName() );
}

void HYDROGUI_LandCoverMapDlg::onPolylineFaceChanged()
{
  updateState();

  emit polylineFaceChanged();
}

void HYDROGUI_LandCoverMapDlg::updateState( bool theInitialConfigure )
{
  if ( theInitialConfigure )
  {
    myObjectNameCreate->setVisible( myOperationId == CreateLandCoverMapId );
    myObjectNameEdit->setVisible( myOperationId != CreateLandCoverMapId );

    myParamGroup->setVisible( myOperationId != RemoveLandCoverId );

    bool aShowPolylinesFacesCtrls = ( myOperationId == CreateLandCoverMapId ||
                                      myOperationId == AddLandCoverId ||
                                      myOperationId == SplitLandCoverId );
    myPolylinesFacesLabel->setVisible( aShowPolylinesFacesCtrls );
    myPolylinesFaces->setVisible( aShowPolylinesFacesCtrls );

    bool aShowStricklerTypesCtrls = ( myOperationId == CreateLandCoverMapId ||
                                      myOperationId == AddLandCoverId ||
                                      myOperationId == MergeLandCoverId ||
                                      myOperationId == ChangeLandCoverTypeId );
    myStricklerTypesLabel->setVisible( aShowStricklerTypesCtrls );
    myStricklerTypes->setVisible( aShowStricklerTypesCtrls );

    bool aShowSelectedLandCoversLabel = ( myOperationId == RemoveLandCoverId ||
                                          myOperationId == MergeLandCoverId ||
                                          myOperationId == ChangeLandCoverTypeId );
    mySelectedLandCoversLabel->setVisible( aShowSelectedLandCoversLabel );
  }
  else
  {
    bool anEmptyObjectName = getObjectName().isEmpty();

    bool anEmptyPolylineFaceName = false;
    if ( myPolylinesFaces->isVisible() )
      anEmptyPolylineFaceName = getPolylineFaceName().isEmpty();

    setApplyEnabled( !anEmptyObjectName && !anEmptyPolylineFaceName );
  }
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Prs.h"

#include <GraphicsView_ViewPort.h>

#include <QCursor>

//=======================================================================
// name    : HYDROGUI_Prs
// Purpose : Constructor
//=======================================================================
HYDROGUI_Prs::HYDROGUI_Prs( const Handle(HYDROData_Entity)& theObject )
: GraphicsView_Object(),
  myObject( theObject ),
  myIsToUpdate( false )
{
  myHighlightCursor = new QCursor( Qt::PointingHandCursor );
}

//=======================================================================
// name    : HYDROGUI_Prs
// Purpose : Destructor
//=======================================================================
HYDROGUI_Prs::~HYDROGUI_Prs()
{
  if( myHighlightCursor )
  {
    delete myHighlightCursor;
    myHighlightCursor = 0;
  }
}

//================================================================
// Function : addTo
// Purpose  : 
//================================================================
void HYDROGUI_Prs::addTo( GraphicsView_ViewPort* theViewPort )
{
  GraphicsView_Object::addTo( theViewPort );

  double aZValue = 0;
  GraphicsView_ObjectListIterator anIter( theViewPort->getObjects() );
  while( anIter.hasNext() )
  {
    if( HYDROGUI_Prs* aPrs = dynamic_cast<HYDROGUI_Prs*>( anIter.next() ) )
    {
      double aZValueRef = aPrs->zValue();
      aZValue = qMax( aZValue, aZValueRef );
    }
  }
  setZValue( aZValue + 1 );
}

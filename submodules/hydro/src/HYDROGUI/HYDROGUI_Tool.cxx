// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_Tool.h>
#include <HYDROData_Document.h>
#include <HYDROData_Iterator.h>
#include <HYDROData_Profile.h>
#include <TCollection_ExtendedString.hxx>
#include <TCollection_HAsciiString.hxx>
#include <TCollection_HExtendedString.hxx>
#include <Image_PixMap.hxx>
#include <QDir>
#include <QDockWidget>
#include <QFile>
#include <QFileInfo>
#include <QLocale>
#include <QStringList>
#include <QTextCodec>
#include <QWidget>

// Definition of this id allows to use 'latin1' (Qt alias for 'ISO-8859-1')
// encoding instead of default 'System'
#define USE_LATIN1_ENCODING

// #define DEB_GROUPS 1

QString HYDROGUI_Tool::ToQString( const TCollection_AsciiString& src )
{
#ifdef USE_LATIN1_ENCODING
  QTextCodec* codec = QTextCodec::codecForName( "latin1" ); // alias for ISO-8859-1
#else
  QTextCodec* codec = QTextCodec::codecForLocale();
#endif
  QString res;
  if ( !src.IsEmpty() )
    res = codec ? codec->toUnicode( (char*)src.ToCString(), src.Length() ) :
      QString( (char*)src.ToCString() );
  return res;
}

QString HYDROGUI_Tool::ToQString( const TCollection_ExtendedString& src )
{
  return QString( (QChar*)src.ToExtString(), src.Length() );
}

QString HYDROGUI_Tool::ToQString( const Handle(TCollection_HAsciiString)& src )
{
  if( src.IsNull() )
    return QString();
  else
    return ToQString( src->String() );
}

QString HYDROGUI_Tool::ToQString( const Handle(TCollection_HExtendedString)& src )
{
  if( src.IsNull() )
    return QString();
  return ToQString( src->String() );
}

TCollection_AsciiString HYDROGUI_Tool::ToAsciiString( const QString& src )
{
  TCollection_AsciiString res;
  if( !src.isNull() )
  {
#ifdef USE_LATIN1_ENCODING
    QTextCodec* codec = QTextCodec::codecForName( "latin1" ); // alias for ISO-8859-1
#else
    QTextCodec* codec = QTextCodec::codecForLocale();
#endif
    if( codec )
    {
      QByteArray str = codec->fromUnicode( src );
      res = TCollection_AsciiString( (Standard_CString)str.constData() );
    }
    else
      res = TCollection_AsciiString( src.toLatin1().data() );
  }
  return res;
}

TCollection_ExtendedString HYDROGUI_Tool::ToExtString( const QString& src )
{
  if( src.isEmpty() )
    return TCollection_ExtendedString();

  Standard_Integer len = src.length();
  Standard_ExtString extStr = new Standard_ExtCharacter[ ( len + 1 ) * 2 ];
  memcpy( (void*)extStr, src.unicode(), len * 2 );
  ((short*)extStr)[ len ] = 0;

  TCollection_ExtendedString trg( extStr );
  delete [] extStr;
  return trg;
}

Handle(TCollection_HAsciiString) HYDROGUI_Tool::ToHAsciiString( const QString& src )
{
  return new TCollection_HAsciiString( ToAsciiString( src ) );
}

Handle(TCollection_HExtendedString) HYDROGUI_Tool::ToHExtString( const QString& src )
{
  return new TCollection_HExtendedString( ToExtString( src ) );
}

QString HYDROGUI_Tool::GetTempDir( const bool theToCreate )
{
  QString aRes;

  char* tmpdir = getenv ( "HYDRO_TMP_DIR" );
  if ( tmpdir )
  {
    // try to create folder if it does not exist
    QFileInfo fi( tmpdir );
    if ( !fi.exists() && theToCreate )
    {
      if ( QDir().mkdir( tmpdir ) )
        QFile::setPermissions( tmpdir, (QFile::Permissions)0x4FFFF );
       QFileInfo fi( tmpdir );
       if ( !fi.exists() || !fi.isWritable() )
         tmpdir = 0;
    }
  }
  if ( !tmpdir )
    tmpdir = getenv ( "TEMP" );
  if ( !tmpdir )
    tmpdir = getenv ( "TMP" );
  if ( !tmpdir )
  {
#ifdef WNT
    tmpdir = "C:\\";
#else
    tmpdir = strdup( "/tmp" );
#endif
  }
  aRes = tmpdir;
  
  QFileInfo fi( aRes );
  if ( !fi.exists() || !fi.isWritable() )
    aRes = QString::null;

  return aRes;
}

void HYDROGUI_Tool::GetObjectReferences( const Handle(HYDROData_Entity)& theObj,
                                         HYDROData_SequenceOfObjects& theRefObjects,
                                         QStringList& theRefNames )
{
  if( theObj.IsNull() )
    return;

  HYDROData_SequenceOfObjects anAllRefObjects = theObj->GetAllReferenceObjects();
  theRefObjects.Append( anAllRefObjects );

  for( int i = 1, n = anAllRefObjects.Length(); i <= n; ++i )
  {
    Handle(HYDROData_Entity) aRefObj = theRefObjects.Value( i );
    if( aRefObj.IsNull() || aRefObj->IsRemoved() )
      continue;

    QString aRefObjectName = aRefObj->GetName();
    if( theRefNames.contains( aRefObjectName ) )
      continue;

    theRefObjects.Append( aRefObj );
    theRefNames.append( aRefObjectName );

    GetObjectReferences( aRefObj, theRefObjects, theRefNames );
  }
}

HYDROData_SequenceOfObjects HYDROGUI_Tool::GetObjectBackReferences( const Handle(HYDROData_Entity)& theObj )
{
  if( theObj.IsNull() )
    return HYDROData_SequenceOfObjects();

  QString anObjName = theObj->GetName();

  Handle(HYDROData_Document) aDoc = HYDROData_Document::Document( theObj->Label() );
  QMap<QString,HYDROData_SequenceOfObjects> aMapOfBackRefs =
    GetObjectsBackReferences( aDoc, QStringList() << anObjName );

  return aMapOfBackRefs[ anObjName ];
}

QMap<QString,HYDROData_SequenceOfObjects> HYDROGUI_Tool::GetObjectsBackReferences(
  const Handle(HYDROData_Document)& theDocument, const QStringList& theObjectNames )
{
  QMap<QString,HYDROData_SequenceOfObjects> aResMap;

  if( theObjectNames.isEmpty() )
    return aResMap;

  if( theDocument.IsNull() )
    return aResMap;

  HYDROData_Iterator anIterator( theDocument );
  for( ; anIterator.More(); anIterator.Next() )
  {
    Handle(HYDROData_Entity) anObject = anIterator.Current();
    if( anObject.IsNull() || anObject->IsRemoved() )
      continue;

    QString anObjectName = anObject->GetName();
    if ( theObjectNames.contains( anObjectName ) )
      continue;

    HYDROData_SequenceOfObjects aRefObjects = anObject->GetAllReferenceObjects();
    for ( int i = 1, n = aRefObjects.Length(); i <= n; ++i )
    {
      Handle(HYDROData_Entity) aRefObject = aRefObjects.Value( i );
      if( aRefObject.IsNull() || aRefObject->IsRemoved() )
        continue;

      QString aRefObjectName = aRefObject->GetName();
      if ( !theObjectNames.contains( aRefObjectName ) )
        continue;

      aResMap[ aRefObjectName ].Append( anObject );
    }
  }

  return aResMap;
}

QDockWidget* HYDROGUI_Tool::WindowDock( QWidget* wid )
{
  if ( !wid )
    return 0;

  QDockWidget* dock = 0;
  QWidget* w = wid->parentWidget();
  while ( w && !dock )
  {
    dock = ::qobject_cast<QDockWidget*>( w );
    w = w->parentWidget();
  }
  return dock;
}

QStringList HYDROGUI_Tool::FindExistingObjectsNames( const Handle(HYDROData_Document)& theDoc, 
                                                     const ObjectKind theObjectKind,
                                                     bool isCheckValidProfile )
{
  QStringList aNames;

  HYDROData_Iterator anIter( theDoc, theObjectKind );
  for ( ; anIter.More(); anIter.Next() ) {
    Handle(HYDROData_Entity) anObject = anIter.Current();

    bool isOK = !anObject.IsNull();
    
    if( isOK && isCheckValidProfile )
    {
      Handle(HYDROData_Profile) aProfile = Handle(HYDROData_Profile)::DownCast( anObject );
      if( !aProfile.IsNull() && !aProfile->IsValid() )
        isOK = false;
    }
      
    if( isOK )
      aNames.append( anObject->GetName() );
  }

  return aNames;
}

QString HYDROGUI_Tool::GetCoordinateString( const double theNumber, bool isInLocale )
{
  if( isInLocale )
  {
    static QLocale aLocale( QLocale::English, QLocale::France );
    return aLocale.toString( theNumber, 'f', 2 );
  }
  else
    return QString::number( theNumber, 'f', 2 );
}

Handle(Image_PixMap) HYDROGUI_Tool::Pixmap( const QImage& theImage )
{
    Handle(Image_PixMap) pix;
    if ( theImage.isNull() || theImage.format() == QImage::Format_Invalid )
        return pix;

    Handle(Image_PixMap) tmpPix = new Image_PixMap();
    tmpPix->SetTopDown( false );
    QImage anImage = theImage.mirrored();
    if ( !anImage.hasAlphaChannel() && anImage.allGray() )
    {
        tmpPix->InitTrash( Image_PixMap::ImgGray, anImage.width(), anImage.height(), anImage.width() );
        for ( int r = 0; r < anImage.height(); r++ )
        {
            Standard_Byte* aRowData = tmpPix->ChangeRow( anImage.height() - r - 1 );
            for  ( int p = 0; p < anImage.width(); p++ )
                aRowData[p] = qRed( anImage.pixel( p, r ) );
        }
    }
    else
    {
        Image_PixMap::ImgFormat aFormat;
        if ( anImage.hasAlphaChannel() )
        {
          if ( anImage.format() != QImage::Format_RGBA8888 )
                anImage = anImage.convertToFormat( QImage::Format_RGBA8888 );
            aFormat = Image_PixMap::ImgRGBA;
        }
        else
        {
            if ( anImage.format() != QImage::Format_RGB888 )
                anImage = anImage.convertToFormat( QImage::Format_RGB888 );
            aFormat = Image_PixMap::ImgRGB;
        }

        tmpPix->InitWrapper( aFormat, (Standard_Byte*)anImage.bits(), anImage.width(), anImage.height(), anImage.bytesPerLine() );
    }

    if ( !tmpPix.IsNull() )
    {
        pix = new Image_PixMap();
        pix->InitCopy( *tmpPix.operator->() );
        pix->SetTopDown( tmpPix->IsTopDown() );
    }

    return pix;
}


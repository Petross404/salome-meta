// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ImportObstacleFromFileOp.h"

#include "HYDROGUI_GeomObjectDlg.h"
#include <HYDROGUI_DataObject.h>
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Obstacle.h>

#include <GEOMBase.h>

#include <SalomeApp_Study.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>
#include <SUIT_FileDlg.h>

static QString lastUsedFilter;

HYDROGUI_ImportObstacleFromFileOp::HYDROGUI_ImportObstacleFromFileOp( HYDROGUI_Module* theModule, 
                                                                      const bool theIsToShowPanel )
: HYDROGUI_Operation( theModule ),
  myIsToShowPanel ( theIsToShowPanel ),
  myFileDlg( 0 )
{
  setName( tr( "IMPORT_OBSTACLE_FROM_FILE" ) );
}

HYDROGUI_ImportObstacleFromFileOp::~HYDROGUI_ImportObstacleFromFileOp()
{
}

void HYDROGUI_ImportObstacleFromFileOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  // Get panel
  HYDROGUI_GeomObjectDlg* aPanel = ::qobject_cast<HYDROGUI_GeomObjectDlg*>( inputPanel() );

  if ( aPanel ) {
    // Reset the panel state
    aPanel->reset();

    // Pass the existing obstacle names to the panel
    QStringList anObstacleNames = 
      HYDROGUI_Tool::FindExistingObjectsNames( doc(), KIND_OBSTACLE );

    aPanel->setObjectNames( anObstacleNames );
  } else {
    myFileDlg = new SUIT_FileDlg( module()->getApp()->desktop(), true );
    myFileDlg->setWindowTitle( getName() );
    myFileDlg->setNameFilter( tr("OBSTACLE_FILTER") );
    if ( !lastUsedFilter.isEmpty() ) {
      myFileDlg->selectNameFilter( lastUsedFilter );
    }

    connect( myFileDlg, SIGNAL( accepted() ),      this, SLOT( onApply()  ) );
    connect( myFileDlg, SIGNAL( rejected() ),      this, SLOT( onCancel() ) );

    myFileDlg->exec();
  }
}

void HYDROGUI_ImportObstacleFromFileOp::abortOperation()
{
  HYDROGUI_Operation::abortOperation();
}

void HYDROGUI_ImportObstacleFromFileOp::commitOperation()
{
  HYDROGUI_Operation::commitOperation();
}

bool HYDROGUI_ImportObstacleFromFileOp::processApply( int& theUpdateFlags,
                                                      QString& theErrorMsg,
                                                      QStringList& theBrowseObjectsEntries )
{
  QString aFileName;
  QString anObstacleName;
  Handle(HYDROData_Obstacle) anObstacle;

  // Get panel
  HYDROGUI_GeomObjectDlg* aPanel = ::qobject_cast<HYDROGUI_GeomObjectDlg*>( inputPanel() );
  if ( aPanel ) {
    // Get file name and obstacle name defined by the user
    aFileName = aPanel->getFileName();
      
    QString anEditedName = aPanel->getEditedObjectName().simplified();

    // Get obstacle to edit
    if ( !anEditedName.isEmpty() ) {
      anObstacle = Handle(HYDROData_Obstacle)::DownCast(
        HYDROGUI_Tool::FindObjectByName( module(), anEditedName, KIND_OBSTACLE ) );
    }
  } else if ( myFileDlg ) {
    // Get file name and file filter defined by the user
    aFileName = myFileDlg->selectedFile();
    lastUsedFilter = myFileDlg->selectedNameFilter();
  }

  // Check the file name
  if ( aFileName.isEmpty() ) {
    return false;
  }
  QFileInfo aFileInfo( aFileName );
  if ( !aFileInfo.exists() || !aFileInfo.isReadable() ) {
    theErrorMsg = tr( "FILE_NOT_EXISTS_OR_CANT_BE_READ" ).arg( aFileName );
    return false;
  }

  // Check obstacle name
  anObstacleName = aPanel->getObjectName().simplified();
  if ( anObstacleName.isEmpty() ) {
    theErrorMsg = tr( "INCORRECT_OBJECT_NAME" );
    return false;
  }

  if( anObstacle.IsNull() || anObstacle->GetName() != anObstacleName ) {
    // check that there are no other objects with the same name in the document
    Handle(HYDROData_Entity) anObject = HYDROGUI_Tool::FindObjectByName( module(), anObstacleName );
    if( !anObject.IsNull() ) {
      theErrorMsg = tr( "OBJECT_EXISTS_IN_DOCUMENT" ).arg( anObstacleName );
      return false;
    }
  }

  bool anIsOk = false;
 
  // If the obstacle for edit is null - create new obstacle object
  if ( anObstacle.IsNull() ) {
    anObstacle = Handle(HYDROData_Obstacle)::DownCast( doc()->CreateObject(KIND_OBSTACLE) );

    anObstacle->SetFillingColor( anObstacle->DefaultFillingColor() );
    anObstacle->SetBorderColor( anObstacle->DefaultBorderColor() );
  }

  if ( !anObstacle.IsNull() ) {
    if ( anObstacle->ImportFromFile( aFileName ) ) {
      // Set name
      if ( anObstacleName.isEmpty() ) {
        anObstacleName = HYDROGUI_Tool::GenerateObjectName( 
          module(), aFileInfo.baseName(), QStringList(), true );
      }
      if ( anObstacle->GetName() != anObstacleName ) {
        anObstacle->SetName( anObstacleName );
      }

      anObstacle->Update();

      // Set operation status
      anIsOk = true;
      module()->setIsToUpdate( anObstacle );
      theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
      QString anEntry = HYDROGUI_DataObject::dataObjectEntry( anObstacle );
      theBrowseObjectsEntries.append( anEntry );


    } else {
      theErrorMsg = tr( "BAD_IMPORTED_OBSTACLE_FILE" ).arg( aFileName );
    }
  }
  
  return anIsOk;
}

HYDROGUI_InputPanel* HYDROGUI_ImportObstacleFromFileOp::createInputPanel() const
{
  HYDROGUI_InputPanel* aPanel = 0;
  if ( myIsToShowPanel ) {
    aPanel = new HYDROGUI_GeomObjectDlg( module(), getName(), 
                                         tr( "DEFAULT_OBSTACLE_NAME" ), true );
  }

  return aPanel;
}

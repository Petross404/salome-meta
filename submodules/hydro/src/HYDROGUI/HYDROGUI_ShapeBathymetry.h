// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_SHAPE_BATHYMETRY_H
#define HYDROGUI_SHAPE_BATHYMETRY_H

#include <HYDROGUI_Shape.h>
#include <TColgp_HArray1OfPnt.hxx>
#include <Quantity_HArray1OfColor.hxx>
#include <QVector>

class HYDROData_Bathymetry;
class AIS_ColorScale;
class HYDROGUI_OCCDisplayer;
class OCCViewer_ViewWindow;

class HYDROGUI_ShapeBathymetry : public HYDROGUI_Shape
{
public:
  HYDROGUI_ShapeBathymetry( HYDROGUI_OCCDisplayer* theDisplayer,
                            const Handle(AIS_InteractiveContext)& theContext,
                            const Handle(HYDROData_Bathymetry)&    theBathymetry,
                            const int                             theZLayer = -1 );
  virtual ~HYDROGUI_ShapeBathymetry();

  void GetRange( double& theMin, double& theMax ) const;
  void UpdateWithColorScale( const Handle(AIS_ColorScale)& );

  // Re-scale by visible points
  void RescaleByVisible( OCCViewer_ViewWindow* );
  // Re-scale by selected points
  void RescaleBySelection();
  // Custom (user) re-scale 
  void Rescale( double theMin, double theMax );
  // Default re-scale (by all points)
  void RescaleDefault();

  void TextLabels( bool isOn, bool isUpdateCurrentViewer=true );

  virtual void display( const bool theIsUpdateViewer = true );
  virtual void erase( const bool theIsUpdateViewer = true );

  virtual void               update( bool isUpdateViewer,
                                     bool isDeactivateSelection );

  virtual void               setVisible( const bool theState,
                                         const bool theIsUpdateViewer = true );

  void Build();

protected:
  virtual QList<Handle(AIS_InteractiveObject)> createShape() const;
  virtual void displayShape( const bool theIsUpdateViewer );

  void setToUpdateColorScale( bool isChanged );
  void Rescale( const QVector<int>& theIndices, bool isForcedAll );

  QVector<int> selected() const;

private:
  HYDROGUI_OCCDisplayer* myDisplayer;
  Handle(TColgp_HArray1OfPnt)     myCoords;
  Handle(Quantity_HArray1OfColor) myColors;

  double myMin;
  double myMax;
  bool myRangeInitialized;
  QVector<int> mySelectedPoints;
};

#endif

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_Poly3DDlg.h"

#include "HYDROGUI_ObjSelector.h"

#include <QCheckBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>

HYDROGUI_Poly3DDlg::HYDROGUI_Poly3DDlg( HYDROGUI_Module* theModule, const QString& theTitle )
: HYDROGUI_InputPanel( theModule, theTitle ),
  myIsEdit( false )
{
  // Profile name
  QGroupBox* aNameGroup = new QGroupBox( tr( "POLYLINE_3D_NAME" ) );

  QLabel* aNameLabel = new QLabel( tr( "NAME" ), aNameGroup );
  myName = new QLineEdit( aNameGroup );

  QGridLayout* aNameLayout = new QGridLayout( aNameGroup );
  aNameLayout->setMargin( 5 );
  aNameLayout->setSpacing( 5 );
  aNameLayout->addWidget( aNameLabel,       0, 0 );
  aNameLayout->addWidget( myName,           0, 1 );

  // Parameters
  QGroupBox* aParamGroup = new QGroupBox( tr( "PARAMETERS" ) );

  myPolylineLabel = new QLabel( tr( "POLYLINE" ), aParamGroup );
  myPolyline = new HYDROGUI_ObjSelector( theModule, KIND_POLYLINEXY, aParamGroup );

  myProfileLabel = new QLabel( tr( "PROFILE" ), aParamGroup );
  myProfile = new HYDROGUI_ObjSelector( theModule, KIND_PROFILE, aParamGroup );
  
  myBathLabel = new QLabel( tr( "BATH" ), aParamGroup );
  myBath = new HYDROGUI_ObjSelector( theModule, KIND_BATHYMETRY, aParamGroup );

  QGridLayout* aParamLayout = new QGridLayout( aParamGroup );
  aParamLayout->setMargin( 5 );
  aParamLayout->setSpacing( 5 );
  aParamLayout->addWidget( myPolylineLabel,  0, 0 );
  aParamLayout->addWidget( myPolyline,       0, 1 );
  aParamLayout->addWidget( myProfileLabel,   1, 0 );
  aParamLayout->addWidget( myProfile,        1, 1 );
  aParamLayout->addWidget( myBathLabel,      2, 0 );
  aParamLayout->addWidget( myBath,           2, 1 );

  // Common
  addWidget( aNameGroup );
  addWidget( aParamGroup );
  addStretch();

  setMode( myIsEdit );

  connect( myProfile, SIGNAL( selectionChanged() ), this, SLOT( OnSelectionChanged() ) );
  connect( myBath, SIGNAL( selectionChanged() ), this, SLOT( OnSelectionChanged() ) );
}

HYDROGUI_Poly3DDlg::~HYDROGUI_Poly3DDlg()
{
}

void HYDROGUI_Poly3DDlg::reset()
{
  myName->clear();
  myProfile->Clear();
  myPolyline->Clear();
}

void HYDROGUI_Poly3DDlg::setMode( const bool theIsEdit )
{
  myIsEdit = theIsEdit;
}

void HYDROGUI_Poly3DDlg::setResultName( const QString& theName )
{
  myName->setText( theName );
}

QString HYDROGUI_Poly3DDlg::getResultName() const
{
  return myName->text();
}

void HYDROGUI_Poly3DDlg::setSelectedObjects( const QString& theNamePoly,
                                             const QString& theNameProfile,
                                             const QString& theNameBath )
{
  myPolyline->SetName( theNamePoly );
  myProfile->SetName( theNameProfile );
  myBath->SetName( theNameBath );
}

bool HYDROGUI_Poly3DDlg::getSelectedObjects( QString& theNamePoly,
                                             QString& theNameProfile,
                                             QString& theNameBath ) const
{
  theNamePoly = myPolyline->GetName();
  theNameProfile = myProfile->GetName();
  theNameBath = myBath->GetName();
  return !theNamePoly.isEmpty() && ( !theNameProfile.isEmpty() || !theNameBath.isEmpty() );
}

void HYDROGUI_Poly3DDlg::setPreselectedObject( const QString& theName )
{
  myProfile->SetName( theName );
  myPolyline->SetChecked( true );
  myPolyline->SetName( QString() );
}

void HYDROGUI_Poly3DDlg::OnSelectionChanged()
{
  if( sender()==myProfile && !myProfile->GetName().isEmpty() )
    myBath->Clear();
  if( sender()==myBath && !myBath->GetName().isEmpty() )
    myProfile->Clear();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_LandCoverColoringOp.h"

#include "HYDROGUI_Module.h"
#include "HYDROGUI_OCCDisplayer.h"
#include "HYDROGUI_Operations.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"

#include <HYDROData_Iterator.h>
#include <HYDROData_Bathymetry.h>
#include <HYDROData_StricklerTable.h>

#include <LightApp_Application.h>

#include <OCCViewer_ViewModel.h>
#include <OCCViewer_ViewManager.h>


HYDROGUI_LandCoverColoringOp::HYDROGUI_LandCoverColoringOp( HYDROGUI_Module* theModule, int theId )
: HYDROGUI_Operation( theModule ),
  myId( theId )
{
  QString aName;
  switch( myId )
  {
    case LandCoverScalarMapModeOnId:  aName = tr( "LC_SCALARMAP_COLORING_ON" ); break;
    case LandCoverScalarMapModeOffId: aName = tr( "LC_SCALARMAP_COLORING_OFF" ); break;
    default: break;
  }
  setName( aName );
}

HYDROGUI_LandCoverColoringOp::~HYDROGUI_LandCoverColoringOp()
{
}

void HYDROGUI_LandCoverColoringOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  HYDROGUI_Module* aModule = module();

  LightApp_Application* anApp = module()->getApp();
  OCCViewer_ViewManager* aViewManager =
    dynamic_cast<OCCViewer_ViewManager*>( anApp->getViewManager( OCCViewer_Viewer::Type(), false ) );

  size_t aViewId = (size_t)aViewManager->getViewModel();

  Handle(HYDROData_StricklerTable) aTable;

  if ( myId == LandCoverScalarMapModeOnId ) {
    aTable = Handle(HYDROData_StricklerTable)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
    if ( !aTable.IsNull() && aViewId) {
      aModule->setLandCoverColoringTable( aViewId, aTable );
    }
  } else if ( myId == LandCoverScalarMapModeOffId ) {
    aModule->setLandCoversScalarMapModeOff( aViewId );
  }
    
  // Hide bathymetries
  HYDROData_Iterator anIterator( doc(), KIND_BATHYMETRY );
  for( ; anIterator.More(); anIterator.Next() ) {
    Handle(HYDROData_Bathymetry) aBath =
      Handle(HYDROData_Bathymetry)::DownCast( anIterator.Current() );	
    if ( !aBath.IsNull() && aModule->isObjectVisible( aViewId, aBath ) ) {
      aModule->setObjectVisible( aViewId, aBath, false );
    }
  }

  aModule->getOCCDisplayer()->SetToUpdateColorScale();
  aModule->update( UF_OCCViewer );
  commit();
}

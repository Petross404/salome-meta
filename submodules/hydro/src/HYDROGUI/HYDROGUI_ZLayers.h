// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_ZLAYERS_H
#define HYDROGUI_ZLAYERS_H

#include <vector>
#include <V3d_Viewer.hxx>

class PrsMgr_PresentableObject;
class AIS_InteractiveContext;

class HYDROGUI_ZLayersIterator
{
public:
  HYDROGUI_ZLayersIterator( const Handle(V3d_Viewer)& );
  ~HYDROGUI_ZLayersIterator();

  void Init( const Handle(V3d_Viewer)& );
  bool More() const;
  void Next();
  int LayerId() const;
  int TopLayer() const;

private:
  std::vector<int>  myZLayers;
  int               myIndex;
  int               myNewZLayer;
  Handle(V3d_Viewer) myViewer;
};

void SetPrsZLayer( const Handle(PrsMgr_PresentableObject)& thePresentableObject,
                   const int theMode, const int theLayerId );
void UpdateZLayersOfHilightPresentationsOfDisplayedObjects( const Handle(AIS_InteractiveContext)&, int theLayer );
void SetZLayerSettings( const Handle(V3d_Viewer)&, int theLayerId, bool theIsOrdered );
int  CreateTopZLayer( const Handle(V3d_Viewer)& );

#endif

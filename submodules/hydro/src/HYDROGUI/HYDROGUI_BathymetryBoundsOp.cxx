// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <HYDROGUI_BathymetryBoundsOp.h>
#include <HYDROGUI_Tool2.h>
#include <HYDROGUI_UpdateFlags.h>
#include <HYDROGUI_Module.h>
#include <HYDROGUI_DataObject.h>
#include <HYDROData_PolylineXY.h>

HYDROGUI_BathymetryBoundsOp::HYDROGUI_BathymetryBoundsOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule )
{
}

HYDROGUI_BathymetryBoundsOp::~HYDROGUI_BathymetryBoundsOp()
{
}

HYDROGUI_InputPanel* HYDROGUI_BathymetryBoundsOp::createInputPanel() const
{
  return 0;
}

void HYDROGUI_BathymetryBoundsOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  myBath = Handle(HYDROData_Bathymetry)::DownCast( HYDROGUI_Tool::GetSelectedObject( module() ) );
  if( myBath.IsNull() )
    onCancel();
  else
    onApply();
}

bool HYDROGUI_BathymetryBoundsOp::processApply( int& theUpdateFlags, QString& theErrorMsg,
                                                QStringList& theBrowseObjectsEntries )
{
  Handle(HYDROData_PolylineXY) aPolyline = myBath->CreateBoundaryPolyline();
  bool isOK = !aPolyline.IsNull();
  theUpdateFlags = 0;
  if( isOK ) {
    module()->setIsToUpdate( myBath );
    theUpdateFlags = UF_Model | UF_OCCViewer | UF_OCC_Forced | UF_VTKViewer;
    
    QString anEntry = HYDROGUI_DataObject::dataObjectEntry( aPolyline );
    theBrowseObjectsEntries.append( anEntry );
  }
  else
    theErrorMsg = tr( "CANNOT_CREATE_BOUNDARY_POLYLINE" );
  return isOK;
}

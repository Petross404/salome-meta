// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#ifndef HYDROGUI_REGION_H
#define HYDROGUI_REGION_H

#include "HYDROGUI_DataObject.h"

#include <HYDROData_Region.h>

#include <QString>
#include <QColor>

class HYDROGUI_Zone;

/**
 * \class HYDROGUI_Region
 * \brief Browser item presenting a zone, used for object browser tree creation.
 *
 * This is an Object Browser item that contains reference to a zone data structure 
 * element inside.
 */
class HYDROGUI_Region : public HYDROGUI_DataObject
{
public:
  /**
   * Constructor.
   * \param theParent parent data object
   * \param theData reference to the corresponding object from data structure
   * \param theParentEntry entry of the parent data object (for reference objects)
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  HYDROGUI_Region( SUIT_DataObject*         theParent,
                   Handle(HYDROData_Region) theData,
                   const QString&           theParentEntry,
                   const bool               theIsInOperation = false );

  bool isDropAccepted() const { return true; }

  /**
   * Add zones to the region. 
   */
  bool addZones( const QList<HYDROGUI_Zone*>& theZonesList );
};
#endif

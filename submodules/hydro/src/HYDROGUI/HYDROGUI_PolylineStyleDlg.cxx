// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PolylineStyleDlg.h"
#include <QFrame>
#include <QLayout>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QGroupBox>

HYDROGUI_PolylineStyleDlg::HYDROGUI_PolylineStyleDlg( HYDROGUI_Module* theModule )
  : HYDROGUI_InputPanel( theModule, tr( "POLYLINE_STYLE" ) )
{
  QGroupBox* group = new QGroupBox( tr( "POLYLINE_STYLE" ), mainFrame() );

  QGridLayout* lay = new QGridLayout( group );

  myShape = new QComboBox( mainFrame() );
  myShape->addItem( "???" );
  myShape->addItem( tr( "No" ) );
  myShape->addItem( tr( "Triangle" ) );
  myShape->addItem( tr( "Cone" ) );

  mySize = new QSpinBox( mainFrame() );
  mySize->setRange( -1, 50 );
  mySize->setSpecialValueText( "   " );

  lay->addWidget( new QLabel( tr( "PREF_POLYLINE_ARROW" ) ), 0, 0 );
  lay->addWidget( myShape, 0, 1 );
  lay->addWidget( new QLabel( tr( "PREF_POLYLINE_ARROW_SIZE" ) ), 1, 0 );
  lay->addWidget( mySize, 1, 1 );
  lay->setRowStretch( 2, 1 );

  addWidget( group );
}

HYDROGUI_PolylineStyleDlg::~HYDROGUI_PolylineStyleDlg()
{
}

void HYDROGUI_PolylineStyleDlg::getStyle( int& theShape, int& theSize ) const
{
  theShape = myShape->currentIndex();
  theSize = mySize->value();
}

void HYDROGUI_PolylineStyleDlg::setStyle( int theShape, int theSize )
{
  myShape->setCurrentIndex( theShape );
  mySize->setValue( theSize );
}


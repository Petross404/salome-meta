// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_DATAOBJECT_H
#define HYDROGUI_DATAOBJECT_H

#include <HYDROData_Entity.h>

#ifdef WIN32
  #pragma warning( disable: 4250 )
  #pragma warning( disable: 4251 )
#endif

#ifdef TEST_MODE
  #include <CAM_DataObject.h>
  enum { 
    EntryId = CAM_DataObject::VisibilityId + 1,    //!< entry column
    RefEntryId                     //!< reference entry column
  };
  #define PARENT CAM_DataObject
#else
  #include <LightApp_DataObject.h>
  #define PARENT LightApp_DataObject
#endif

#include <QString>
#include <QMap>
#include <QPixmap>
#include <QColor>

#ifdef WIN32
  #pragma warning( default: 4250 )
  #pragma warning( default: 4251 )
#endif

/**
 * \class HYDROGUI_DataObject
 * \brief Module data object, used for object browser tree creation.
 *
 * This is an Object Browser object that contains reference to data structure 
 * element inside.
 * This class inherits CAM_DataObject virtually, so it is necessary to call in the class
 * constructor the CAM object constructor manually for the correct initialization
 */
class HYDROGUI_DataObject : public PARENT
{
public:
  //! Column id
  enum { 
    RefObjectId = RefEntryId + 1,    //!< Ref.Object column
    AltitudeObjId                    //!< Altitude column
  };

  //! Role
  enum {
    IsInOperationRole = Qtx::AppropriateRole + 100 //!< Filter value role
  };

  /**
   * Constructor.
   * \param theParent parent data object
   * \param theData reference to the corresponding object from data structure
   * \param theParentEntry entry of the parent data object (for reference objects)
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  HYDROGUI_DataObject( SUIT_DataObject*         theParent,
                       Handle(HYDROData_Entity) theData,
                       const QString&           theParentEntry,
                       const bool               theIsInOperation = false );
    
  /**
   * Returns the unique object identifier string.
   */
  virtual QString entry() const;

  /**
   * Returns the entry of the referenced object.
   */
  virtual QString refEntry() const;

  /**
   * Returns the name of object.
   */
  virtual QString name() const;

  /**
   * Returns the font of displayed object name.
   */
  virtual QFont font( const int = SUIT_DataObject::NameId ) const;

  /**
   * Returns the object color.
   */
  virtual QColor color( const ColorRole, const int = NameId ) const;

  /**
   * Returns the object icon.
   */
  virtual QPixmap icon( const int = NameId ) const;

  /**
   * Returns true if renaming is allowed for the object.
   */
  virtual bool    renameAllowed( const int = NameId ) const;

  /**
   * Returns the model data object.
   */
  const Handle(HYDROData_Entity)& modelObject() const { return myData; }

  /**
   * Redefines the object.
   */
  void setObject( const Handle(HYDROData_Entity)& theObject ) { myData = theObject; }

  /**
   * Returns the entry prefix for all HYDRO data objects.
   */
  static QString entryPrefix() { return QString( "HYDRO:" ); }

  /**
   * Returns the full entry for the specified data object.
   */
  static QString dataObjectEntry( const Handle(HYDROData_Entity)& theObject,
                                  const bool theWithPrefix = true );
  /**
   * Sets the validity flag: if object is valid or not.
   * \param theIsValid is true for valid objects, false for invalid
   */
  void setIsValid( const bool theIsValid );

  /**
   * Returns the validity flag: is object valid or not
   * \return false if object is not valid
   */
  bool isValid() const;

  /**
   * Returns the usage within active operation flag: 
   * is the object is used in the local tree of an active operation dialog or not.
   * \return false if the object is used in the main object browser tree
   */
  bool isInOperation() const { return myIsInOperation; }

  void updateBy( SUIT_DataObject* );

protected:
  Handle(HYDROData_Entity) myData; ///< object from data model
  QString myParentEntry;
  bool myIsValid; ///< indicates if the object is valid
  bool myIsInOperation; ///< indicates if the object is used within an active operation
  QPixmap myIcon;
};

/**
 * \class HYDROGUI_NamedObject
 * \brief Module data object, used for object browser tree creation.
 *
 * It contains only name inside, without additional data: it is just information
 * or grouping object in the Object Browser.
 * This class inherits CAM_DataObject virtually, so it is necessary to call in the class
 * constructor the CAM object constructor manually for the correct initialization
 */
class HYDROGUI_NamedObject : public virtual PARENT
{
public:
  /**
   * Constructor.
   * \param theParent parent data object
   * \param theName displayed name
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  HYDROGUI_NamedObject( SUIT_DataObject* theParent,
                        const QString&   theName,
                        const QString&   theParentEntry,
                        const bool       theIsInOperation = false );
    
  /**
   * Returns the unique object identifier string.
   */
  virtual QString entry() const;

  /**
   * Returns the name of object.
   */
  virtual QString name() const;

  /**
   * Returns the font of displayed object name.
   */
  virtual QFont font( const int = SUIT_DataObject::NameId ) const;

  /**
   * Returns the object icon.
   */
  virtual QPixmap icon( const int = NameId ) const;

  /**
   * Returns the usage within active operation flag: 
   * is the object is used in the local tree of an active operation dialog or not.
   * \return false if the object is used in the main object browser tree
   */
  bool isInOperation() const { return myIsInOperation; }

  void updateBy( SUIT_DataObject* );

private:
  QString myName; ///< name in the OB
  QString myParentEntry;
  QPixmap myIcon;
  bool myIsInOperation; ///< indicates if the object is used within an active operation
};

/**
 * \brief Module data object, used for dropping items in the object browser.
 *
 * It inherits NamedObject with only difference - it accepts dropping.
 */
class HYDROGUI_DropTargetObject : public HYDROGUI_NamedObject
{
public:
  /**
   * Constructor.
   * \param theParent parent data object
   * \param theName displayed name
   * \param theIsInOperation if true then the tree is used for a browser within an operation, it is false by default
   */
  HYDROGUI_DropTargetObject( SUIT_DataObject* theParent,
                             const QString&   theName,
                             const QString&   theParentEntry,
                             const bool       theIsInOperation = false );
    
  bool isDropAccepted() const { return true; }
};

#endif

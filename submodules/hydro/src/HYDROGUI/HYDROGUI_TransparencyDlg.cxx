// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_TransparencyDlg.h"

#include <QtxDoubleSpinBox.h>

#include <QLabel>
#include <QLayout>
#include <QPushButton>

const double RANGE = 1;
const double STEP = 0.1;
const double PREC = 2;

/**
  Constructor.
  @param theParent the parent widget
*/
HYDROGUI_TransparencyDlg::HYDROGUI_TransparencyDlg( QWidget* theParent )
  : QDialog( theParent )
{
  myTransparency = new QtxDoubleSpinBox( 0, RANGE, STEP, PREC, PREC, this );
  myTransparency->setValue( 0.5 );

  // Apply and close buttons
  myApplyAndClose = new QPushButton( tr("APPLY_AND_CLOSE") );
  myApplyAndClose->setDefault( true );
  myApply = new QPushButton( tr("APPLY") );
  myClose = new QPushButton( tr("CLOSE") );

  // Layout
  // Spin-box layout
  QHBoxLayout* aLayout = new QHBoxLayout();
  aLayout->setMargin( 5 );
  aLayout->setSpacing( 5 );
  aLayout->addWidget( new QLabel( tr( "TRANSPARENCY" ), this ) );
  aLayout->addWidget( myTransparency );
  // Apply and close buttons layout
  QHBoxLayout* aDlgButtonsLayout = new QHBoxLayout(); 
  aDlgButtonsLayout->addWidget( myApplyAndClose );
  aDlgButtonsLayout->addWidget( myApply );
  aDlgButtonsLayout->addWidget( myClose );
  aDlgButtonsLayout->addStretch();
  // Main layout
  QVBoxLayout* aMainLayout = new QVBoxLayout( this );
  aMainLayout->setMargin( 5 );
  aMainLayout->setSpacing( 5 );
  aMainLayout->addLayout( aLayout );
  aMainLayout->addLayout( aDlgButtonsLayout );

  // Connections
  connect( myApplyAndClose, SIGNAL( clicked() ), this, SIGNAL( applyAndClose() ) );
  connect( myApply, SIGNAL( clicked() ), this, SIGNAL( apply() ) );
  connect( myClose, SIGNAL( clicked() ), this, SLOT( reject() ) );  

  setFixedSize( 300, 90 );
}

/**
  Destructor.
*/
HYDROGUI_TransparencyDlg::~HYDROGUI_TransparencyDlg()
{
}

void HYDROGUI_TransparencyDlg::setTransparency( const double& theValue )
{
  myTransparency->setValue( theValue );
}

double HYDROGUI_TransparencyDlg::getTransparency() const
{
  return myTransparency->value();
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_ImportLandCoverMapDlg_H
#define HYDROGUI_ImportLandCoverMapDlg_H

#include "HYDROGUI_Wizard.h"
#include <qvector.h>

class QListWidget;
class QLineEdit;
class QGroupBox;
class QComboBox;
class QLabel;
class QTableWidget;
class QCheckBox;


class HYDROGUI_ImportLandCoverMapDlg : public HYDROGUI_Wizard
{
  Q_OBJECT

public:
  HYDROGUI_ImportLandCoverMapDlg( HYDROGUI_Module* theModule, const QString& theTitle );
  virtual ~HYDROGUI_ImportLandCoverMapDlg();

  void                  reset();

  void                  setPolygonNames( const QStringList& theNames );
  void                  setSelectedPolygonNames( const QStringList& theNames );
  void                  setAttributeNames( const QStringList& theAttrNames );

  QStringList           getSelectedPolygonNames() const;
  QString               getSelectedFieldName() const;

  void                  setObjectName( const QString& theName );
  QString               getObjectName() const;

  void                  setFileName( const QString& theFileName );
  QString               getFileName() const;

  void                  setFirstPageState(bool theState);
  bool                  getFirstPageState() const;
  bool                  getAttrCheckBoxState() const;
  void                  getValAttr2StricklerTypeCorr(QStringList& theAttrValues, QStringList& theST);
  QVector<int>          getSelectedPolygonIndices() const;
  bool                  isPolygonListEmpty() const;

  bool                  CheckFirstPageFilling() const;

  void                  setDbfState(bool theState);
  bool                  getDbfState() const;

  void                  setDbfRecordsNb(int theRecordsNbs);
  int                   getDbfRecordsNb() const;

  int                   getCurrentWizardIndex() const;

  void                  setViewerState(bool theState);
  bool                  getViewerState() const;
  

  void                  FillCorrespondenceTable(const QStringList& theFirstColumn, 
                                                const QStringList& theSecondColumn,
                                                const QVector<int> theDefCBIndices,
                                                const QVector<QColor> theColors);
signals:
  void                  FileSelected( const QString& theFileName );
  void                  selectionChanged( const QStringList& );

protected slots:
  void                  onBrowse();
  void                  onItemSelectionChanged();
  void                  onAttrCBChecked(bool theState);
  void                  onComboBoxColorChanged(int theInd);

protected:
  bool                  acceptCurrent() const;

private:

  QWizardPage*          createPage1();
  QWizardPage*          createPage2();
  QWizardPage*          createPage3();

  //First page
  QLineEdit*            myFileName; 
  QGroupBox*            myFileNameGroup;
  QListWidget*          myPolygonsListWidget;  	
  QGroupBox*            myObjectNameGroup;
  QLineEdit*            myObjectName;
  QCheckBox*            myAttrCheckBox;

  //Second page
  QListWidget*          myDBFAttrListWidget; 
  QLabel*               myAvAttrLabel;

  //Third page
  QLabel*               myCorrLabel;
  QTableWidget*         myCorrTableWidget;

  //State of the first page
  bool                  myFirstPageState;
  QVector<QColor>       myStrColors;
  bool                  myDbfState;
  int                   myDbfRecordsNbs;

  bool                  myUpdateViewerState;
};

#endif

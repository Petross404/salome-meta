// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_PrsPolylineDriver.h"

#include "HYDROGUI_PrsPolyline.h"

#include <HYDROData_PolylineXY.h>

HYDROGUI_PrsPolylineDriver::HYDROGUI_PrsPolylineDriver()
:HYDROGUI_PrsDriver()
{
}

HYDROGUI_PrsPolylineDriver::~HYDROGUI_PrsPolylineDriver()
{
}

bool HYDROGUI_PrsPolylineDriver::Update( const Handle(HYDROData_Entity)& theObj,
                                         HYDROGUI_Prs*& thePrs )
{
  HYDROGUI_PrsDriver::Update( theObj, thePrs );

  if( theObj.IsNull() )
    return false;

  Handle(HYDROData_PolylineXY) aPolyline = Handle(HYDROData_PolylineXY)::DownCast( theObj );
  if( aPolyline.IsNull() )
    return false;

  if( !thePrs )
    thePrs = new HYDROGUI_PrsPolyline( theObj );

  HYDROGUI_PrsPolyline* aPrsPolyline = (HYDROGUI_PrsPolyline*)thePrs;

  aPrsPolyline->setName( aPolyline->GetName() );
  aPrsPolyline->setPath( aPolyline->GetPainterPath() );

  aPrsPolyline->compute();

  return true;
}

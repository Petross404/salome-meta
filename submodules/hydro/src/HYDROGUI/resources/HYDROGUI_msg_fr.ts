<!DOCTYPE TS>
<TS version="1.1" >

  <context>
    <name>@default</name>
    <message>
      <source>RENAME_TO</source>
      <translation>Rename to %1</translation>
    </message>
    <message>
      <source>DEFAULT_IMAGE_NAME</source>
      <translation>Image</translation>
    </message>
    <message>
      <source>DEFAULT_POLYLINE_NAME</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>DEFAULT_OBSTACLE_NAME</source>
      <translation>Obstacle</translation>
    </message>
    <message>
      <source>DEFAULT_POLYLINE_3D_NAME</source>
      <translation>Polyline3D</translation>
    </message>
    <message>
      <source>DEFAULT_PROFILE_NAME</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>DEFAULT_VISUAL_STATE_NAME</source>
      <translation>Visual state</translation>
    </message>
    <message>
      <source>DEFAULT_BATHYMETRY_NAME</source>
      <translation>Bathymetry</translation>
    </message>
    <message>
      <source>DEFAULT_CALCULATION_CASE_NAME</source>
      <translation>Case</translation>
    </message>
    <message>
      <source>DEFAULT_IMMERSIBLE_ZONE_NAME</source>
      <translation>Immersible zone</translation>
    </message>
    <message>
      <source>DEFAULT_STRICKLER_TABLE_NAME</source>
      <translation>Strickler table</translation>
    </message>
    <message>
      <source>DEFAULT_LAND_COVER_MAP_NAME</source>
      <translation>Land cover map</translation>
    </message>
    <message>
      <source>IMAGES</source>
      <translation>IMAGES</translation>
    </message>
    <message>
      <source>POLYLINES</source>
      <translation>POLYLINES</translation>
    </message>
    <message>
      <source>POLYLINES_3D</source>
      <translation>POLYLINES 3D</translation>
    </message>
    <message>
      <source>PROFILES</source>
      <translation>PROFILES</translation>
    </message>
    <message>
      <source>VISUAL_STATES</source>
      <translation>VISUAL STATES</translation>
    </message>
    <message>
      <source>BATHYMETRIES</source>
      <translation>BATHYMETRIES</translation>
    </message>
    <message>
      <source>CALCULATION_CASES</source>
      <translation>CALCULATION CASES</translation>
    </message>
    <message>
      <source>OBSTACLES</source>
      <translation>OBSTACLES</translation>
    </message>
    <message>
      <source>STRICKLER_TABLES</source>
      <translation>STRICKLER TABLES</translation>
    </message>
    <message>
      <source>LAND_COVER_MAPS</source>
      <translation>LAND COVER MAPS</translation>
    </message>
    <message>
      <source>ARTIFICIAL_OBJECTS</source>
      <translation>ARTIFICIAL OBJECTS</translation>
    </message>
    <message>
      <source>NATURAL_OBJECTS</source>
      <translation>NATURAL OBJECTS</translation>
    </message>
    <message>
      <source>BOUNDARY_POLYGONS</source>
      <translation>BOUNDARY POLYGONS</translation>
    </message>
    <message>
      <source>BATHYMETRY_FILTER</source>
      <translation>Bathymetry files (*.xyz);;ASC files (*.asc);;All files (*.* *)</translation>
    </message>
    <message>
      <source>CASE_BOUNDARY</source>
      <translation>Boundary</translation>
    </message>
    <message>
      <source>CASE_BOUNDARY_POLYGONS</source>
      <translation>Boundary Polygons</translation>
    </message>
    <message>
      <source>CASE_REFERENCE_ZONES</source>
      <translation>Reference zones</translation>
    </message>
    <message>
      <source>CASE_REGIONS</source>
      <translation>REGIONS</translation>
    </message>
    <message>
      <source>CASE_LAND_COVER_MAP</source>
      <translation>LAND COVER MAP</translation>
    </message>
    <message>
      <source>CASE_SPLIT_GROUPS</source>
      <translation>Split groups</translation>
    </message>
    <message>
      <source>FILE_CAN_NOT_BE_IMPORTED</source>
      <translation>The file '%1' can not be imported: format *.%2 is not supported.</translation>
    </message>
    <message>
      <source>FILE_NOT_EXISTS_OR_CANT_BE_READ</source>
      <translation>The file '%1'
does not exist or you have not enough permissions to open it.</translation>
    </message>
    <message>
      <source>IMAGE_FILTER_IMPORT</source>
      <translation>Image files (*.bmp *.jpg *.jpeg *.png *.tif *.ecw);;All files (*.* *)</translation>
    </message>
    <message>
      <source>IMAGE_FILTER_EXPORT</source>
      <translation>Image files (*.bmp *.jpg *.jpeg *.png *.tif );;All files (*.* *)</translation>
    </message>
    <message>
      <source>INCORRECT_OBJECT_NAME</source>
      <translation>The object name must not be an empty string value.</translation>
    </message>
    <message>
      <source>EMPTY_FILENAMES</source>
      <translation>Files list is empty</translation>
    </message>
    <message>
      <source>BATHYMETRY_IMPORT_WARNING</source>
      <translation>Import of bahemetry - warning</translation>
    </message>

    <message>
      <source>INSUFFICIENT_INPUT_DATA</source>
      <translation>Insufficient input data</translation>
    </message>
    <message>
      <source>INPUT_VALID_DATA</source>
      <translation>Please enter valid data and try again.</translation>
    </message>
    <message>
      <source>LOAD_ERROR</source>
      <translation>Study could not be loaded</translation>
    </message>
    <message>
      <source>SHAPE_IMAGE_ERROR</source>
      <translation>Image shape could not be created</translation>
    </message>
    <message>
      <source>IMAGE_CAN_NOT_BE_CREATED</source>
      <translation>It is not possible to create a presentation for OCC 3D view. </translation>
    </message>
    <message>
      <source>IMAGE_TRANSFORMATION_CAN_NOT_BE_APPLYED</source>
      <translation>The transformated image is out of range.</translation>
    </message>
    <message>
      <source>FILE_CAN_NOT_BE_CREATED</source>
      <translation>The temporary file '%1' can not be created.</translation>
    </message>
    <message>
      <source>OBJECT_EXISTS_IN_DOCUMENT</source>
      <translation>Object with name '%1' already exists in the document.</translation>
    </message>
    <message>
      <source>SAVE_ERROR</source>
      <translation>Study could not be saved</translation>
    </message>
    <message>
      <source>ZONE_POLYLINE</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>ZONE_BATHYMETRY</source>
      <translation>Bathymetry</translation>
    </message>
    <message>
      <source>OBJECT_GROUPS</source>
      <translation>Groups</translation>
    </message>
    <message>
      <source>MERGE_UNKNOWN</source>
      <translation>Unresolved Conflict</translation>
    </message>
    <message>
      <source>MERGE_ZMIN</source>
      <translation>ZMIN</translation>
    </message>
    <message>
      <source>MERGE_ZMAX</source>
      <translation>ZMAX</translation>
    </message>
    <message>
      <source>NEW_REGION</source>
      <translation>&lt;New region&gt;</translation>
    </message>
    <message>
      <source>OBSTACLE_FILTER</source>
      <translation>BREP files (*.brep);;IGES files (*.iges *.igs);;STEP files (*.step *.stp);;
All supported formats (*.brep *.iges *.igs *.step *.stp)</translation>
    </message>
    <message>
      <source>COORDINATES_INFO</source>
      <translation>Local CS: (%1, %2); Global CS: (%3, %4)</translation>
    </message>
    <message>
      <source>POLYLINE3D_POLYLINE</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>POLYLINE3D_PROFILE</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>POLYLINE3D_BATHYMETRY</source>
      <translation>Bathymetry</translation>
    </message>
    <message>
      <source>CHANNEL_GUIDE_LINE</source>
      <translation>Guide line</translation>
    </message>
    <message>
      <source>CHANNEL_PROFILE</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>STREAM_HYDRAULIC_AXIS</source>
      <translation>Hydraulic axis</translation>
    </message>
    <message>
      <source>STREAM_PROFILES</source>
      <translation>Profiles</translation>
    </message>
    <message>
      <source>BC_POLYGON_POLYLINE</source>
      <translation>Boundary Polyline</translation>
    </message>
    <message>
      <source>STREAM_WARNINGS</source>
      <translation>Stream Warnings</translation>
    </message>
     <message>
      <source>STREAM_PROJECTION_FAILED</source>
      <translation>Warning: Projection of banks/profiles are failed</translation>
    </message>
    <message>
      <source>PREF_TAB_GENERAL</source>
      <translation>General</translation>
    </message>
    <message>
      <source>PREF_GROUP_CURSOR</source>
      <translation>Cursor for edition operations</translation>
    </message>
    <message>
      <source>PREF_TYPE_OF_CURSOR</source>
      <translation>Type</translation>
    </message>
    <message>
      <source>PREF_GROUP_VIEWER</source>
      <translation>Viewer</translation>
    </message>
    <message>
      <source>PREF_VIEWER_AUTO_FITALL</source>
      <translation>Make automatic fit all after show object operation</translation>
    </message>
    <message>
      <source>PREF_VIEWER_ZOOM_SHUTOFF</source>
      <translation>Conservation of zoom when top-view/etc is activated</translation>
    </message>
    <message>
      <source>PREF_VIEWER_CHAINED_PANNING</source>
      <translation>Chained panning</translation>
    </message>
    <message>
      <source>PREF_GROUP_STRICKLER_TABLE</source>
      <translation>Strickler table</translation>
    </message>
    <message>
      <source>PREF_DEFAULT_STRICKLER_COEFFICIENT</source>
      <translation>Default Strickler coefficient</translation>
    </message>
    <message>
      <source>STRICKLER_TABLE_FILTER</source>
      <translation>Strickler table files (*.txt);;All files (*.* *)</translation>
    </message>
    <message>
      <source>LAND_COVER_POLYLINES</source>
      <translation>Polylines</translation>
    </message>
    <message>
      <source>OVERVIEW</source>
      <translation>Overview</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_CalculationDlg</name>
    <message>
      <source>CALCULATION_NAME</source>
      <translation>Calculation case name</translation>
    </message>
    <message>
      <source>CALCULATION_REFERENCE_OBJECTS</source>
      <translation>Objects</translation>
    </message>
    <message>
      <source>INCLUDED_OBJECTS</source>
      <translation>Included objects</translation>
    </message>
    <message>
      <source>ALREADY_INCLUDED_OBJECTS</source>
      <translation>Already included objects (can't be excluded)</translation>
    </message>
    <message>
      <source>AVAILABLE_GROUPS</source>
      <translation>Available groups</translation>
    </message>
    <message>
      <source>INCLUDED_GROUPS</source>
      <translation>Included groups</translation>
    </message>
    <message>
      <source>INCLUDED_BOUNDARY_POLYGONS_CUT</source>
      <translation>Included boundary polygons (cut tools)</translation>
    </message>
    <message>
      <source>INCLUDED_BOUNDARY_POLYGONS_INCSEL</source>
      <translation>Included boundary polygons (include/selection tools)</translation>
    </message>
    <message>
      <source>AVAILABLE_BOUNDARY_POLYGONS</source>
      <translation>Available boundary polygons</translation>
    </message>
    <message>
      <source>LIMITS</source>
      <translation>Limits</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>BATHYMETRY</source>
      <translation>Bathymetry</translation>
    </message>
    <message>
      <source>INCLUDE</source>
      <translation>Include &gt;&gt;</translation>
    </message>
    <message>
      <source>EXCLUDE</source>
      <translation>Exclude &lt;&lt;</translation>
    </message>
    <message>
      <source>ADD</source>
      <translation>Add</translation>
    </message>
    <message>
      <source>REMOVE</source>
      <translation>Remove</translation>
    </message>
    <message>
      <source>EMPTY_GEOMETRY_OBJECTS</source>
      <translation>No one geometry object is selected, should be at least one.</translation>
    </message>
    <message>
      <source>MODE</source>
      <translation>Mode</translation>
    </message>
    <message>
      <source>AUTO</source>
      <translation>Auto</translation>
    </message>
    <message>
      <source>MANUAL</source>
      <translation>Manual</translation>
    </message>
    <message>
      <source>PRIORITY</source>
      <translation>Priority</translation>
    </message>
    <message>
      <source>STRICKLER_TABLE</source>
      <translation>Strickler table</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP</source>
      <translation>Land cover map</translation>
    </message>
    <message>
      <source>STRICKLER_TYPE</source>
      <translation>Strickler type from land cover</translation>
    </message>
    <message>
      <source>RESULTS_ON_GEOMETRY_OBJECTS</source>
      <translation>Results on geometry objects</translation>
    </message>
    <message>
      <source>REGENERATE_COLORS</source>
      <translation>Regenerate colors</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_CalculationOp</name>
    <message>
      <source>CREATE_CALCULATION</source>
      <translation>Create calculation case</translation>
    </message>
    <message>
      <source>EDIT_CALCULATION</source>
      <translation>Edit calculation case</translation>
    </message>
    <message>
      <source>COMPLETE_CASE</source>
      <translation>Complete calculation case</translation>
    </message>
    <message>
      <source>COMPLETE_CASE_INTERSECTION_DETECTED</source>
      <translation>There is an intersection(s) between new objects. Result will depend on order of new objects</translation>
    </message>
    <message>
      <source>COMPLETE_CALCULATION</source>
      <translation>Fast update of calculation case</translation>
    </message>
    <message>
      <source>PREVIEW_CASE_ZONES</source>
      <translation>Preview case zones</translation>
    </message>
    <message>
      <source>REGIONS_CHANGED</source>
      <translation>Regions list modification</translation>
    </message>
    <message>
      <source>ORDER_CHANGED</source>
      <translation>Order of objects is changed</translation>
    </message>
    <message>
      <source>RULE_CHANGED</source>
      <translation>Priority rule for objects is changed</translation>
    </message>
    <message>
      <source>CONFIRM_SPLITTING_ZONES_RECALCULATION_REGIONS</source>
      <translation>Case splitting zones already exist and will be recalculated after regions list modification. Do you confirm the recalculation?</translation>
    </message>
    <message>
      <source>MODE_CHANGED</source>
      <translation>Change creation mode</translation>
    </message>
    <message>
      <source>CONFIRM_SPLITTING_ZONES_RECALCULATION_MODE</source>
      <translation>Case splitting zones already exist and will be recalculated after mode change. Do you confirm the recalculation?</translation>
    </message>
    <message>
      <source>EMPTY_REGIONS</source>
      <translation>Empty regions</translation>
    </message>
    <message>
      <source>CONFIRM_CONTINUE_WITH_OBJECTS_NOT_INCLUDED_TO_REGION</source>
      <translation>Region(s): %1 do not contain any objects. Do you want to continue?</translation>
    </message>
    <message>
      <source>CONFIRM_LAND_COVER_PARTITION_RECALCULATION_REGIONS</source>
      <translation>Case land covers partition already exists and will be recalculated after regions list modification. Do you confirm the recalculation?</translation>
    </message>
    <message>
      <source>CONFIRM_LAND_COVER_PARTITION_RECALCULATION_MODE</source>
      <translation>Case land covers partition already exists and will be recalculated after mode change. Do you confirm the recalculation?</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_DataBrowser</name>
    <message>
      <source>REF_OBJECT_COLUMN</source>
      <translation>Ref.Object</translation>
    </message>
    <message>
      <source>ALTITUDE_COLUMN</source>
      <translation>Altitude.Object</translation>
    </message>
    <message>
      <source>LAND_COVER_COLUMN</source>
      <translation>Land cover</translation>
    </message>
    <message>
      <source>ZONE_TO_NEW_REGION</source>
      <translation>Create a new region</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_CopyPasteOp</name>
    <message>
      <source>COPY_PASTE</source>
      <translation>Copy/paste</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_DuplicateOp</name>
    <message>
      <source>DUPLICATE</source>
      <translation>Duplicate</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_CopyPastePositionOp</name>
    <message>
      <source>COPY_PASTE_VIEW_POSITION</source>
      <translation>Copy/paste view position</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_DeleteOp</name>
    <message>
      <source>DELETE</source>
      <translation>Delete</translation>
    </message>
    <message>
      <source>DELETE_OBJECTS</source>
      <translation>Delete objects</translation>
    </message>
    <message>
      <source>DELETE_OBJECTS_IMPOSIBLE</source>
      <translation>One or more selected objects can not be deleted separately from parent object.</translation>
    </message>
    <message>
      <source>DELETED_OBJECTS_HAS_BACKREFERENCES</source>
      <translation>One or more selected objects are used to create another ones.
First remove objects which are created on their basis.

%1</translation>
    </message>
    <message>
      <source>DELETE_OBJECT_NAME</source>
      <translation>"%1"</translation>
    </message>
    <message>
      <source>DELETE_OBJECT_IS_USED_FOR</source>
      <translation>Object "%1" is used for %2</translation>
    </message>
    <message>
      <source>DELETE_LAST_TABLE_WRN</source>
      <translation>You are about to delete all Strickler tables in the study.
After that the study will contain only the default Strickler table.
Do you want to continue?</translation>
    </message>
    <message>
      <source>WARNING</source>
      <translation>Warning</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_DeleteDlg</name>
    <message>
      <source>DELETE_OBJECTS</source>
      <translation>Delete objects</translation>
    </message>
    <message>
      <source>CONFIRM_DELETION</source>
      <translation>Do you really want to delete %1 object(s)?</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ExportImageOp</name>
    <message>
      <source>EXPORT_IMAGE</source>
      <translation>Export image</translation>
    </message>
    <message>
      <source>EXPORT_IMAGE_TO_FILE</source>
      <translation>Export image to file</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_InputPanel</name>
    <message>
      <source>APPLY_AND_CLOSE</source>
      <translation>Apply and Close</translation>
    </message>
    <message>
      <source>APPLY</source>
      <translation>Apply</translation>
    </message>
    <message>
      <source>CANCEL</source>
      <translation>Cancel</translation>
    </message>
    <message>
      <source>CLOSE</source>
      <translation>Close</translation>
    </message>
    <message>
      <source>HELP</source>
      <translation>Help</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_Wizard</name>
    <message>
      <source>NEXT</source>
      <translation>Next &gt;</translation>
    </message>
    <message>
      <source>BACK</source>
      <translation>&lt; Back</translation>
    </message>
    <message>
      <source>FINISH</source>
      <translation>Finish</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportBathymetryDlg</name>
    <message>
      <source>BATHYMETRY_NAME</source>
      <translation>Bathymetry name</translation>
    </message>
    <message>
      <source>FILE_NAMES</source>
      <translation>File names</translation>
    </message>
    <message>
      <source>IMPORT_BATHYMETRY_FROM_FILE</source>
      <translation>Import bathymetry from file</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>INVERT_BATHYMETRY_ALTITUDES</source>
      <translation>Invert altitude values</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportBathymetryOp</name>
    <message>
      <source>EDIT_IMPORTED_BATHYMETRY</source>
      <translation>Edit imported bathymetry</translation>
    </message>
    <message>
      <source>IMPORT_BATHYMETRY</source>
      <translation>Import bathymetry</translation>
    </message>
    <message>
      <source>BAD_IMPORTED_BATHYMETRY_FILE</source>
      <translation>'%1'
file cannot be correctly imported for a Bathymetry definition.</translation>
    </message>
  <message>
      <source>BAD_IMPORTED_BATHYMETRY_FILES</source>
      <translation>'%1'
All files cannot be correctly imported for a Bathymetry definition.</translation>
    </message>

</context>

  <context>
    <name>HYDROGUI_ImportImageDlg</name>
    <message>
      <source>ACTIVATE_POINT_A_SELECTION</source>
      <translation>Activate point A selection</translation>
    </message>
    <message>
      <source>ACTIVATE_POINT_B_SELECTION</source>
      <translation>Activate point B selection</translation>
    </message>
    <message>
      <source>ACTIVATE_POINT_C_SELECTION</source>
      <translation>Activate point C selection</translation>
    </message>
    <message>
      <source>BY_REFERENCE_IMAGE</source>
      <translation>Choose points on the reference image</translation>
    </message>
    <message>
      <source>FILE_NAME</source>
      <translation>File name</translation>
    </message>
    <message>
      <source>IMAGE_NAME</source>
      <translation>Image name</translation>
    </message>
    <message>
      <source>IMPORT_IMAGE_FROM_FILE</source>
      <translation>Import image from file</translation>
    </message>
    <message>
      <source>MANUALLY_GEODESIC</source>
      <translation>Manually input Geographic coordinates</translation>
    </message>
    <message>
      <source>MANUALLY_LAMBERT93</source>
      <translation>Manually input Plane coordinates (Lambert93)</translation>
    </message>
    <message>
      <source>LAMBERT93_FROM_FILE</source>
      <translation>Get Plane coordinates (Lambert93) from file</translation>
    </message>
    <message>
      <source>IMAGE_CS</source>
      <translation>Image CS</translation>
    </message>
    <message>
      <source>GEODESIC</source>
      <translation>Geographic coordinates</translation>
    </message>
    <message>
      <source>LAMBERT93</source>
      <translation>Plane coordinates (Lambert93)</translation>
    </message>
    <message>
      <source>POINT_LATITUDE</source>
      <translation>Lat</translation>
    </message>
    <message>
      <source>POINT_LONGITUDE</source>
      <translation>Long</translation>
    </message>
    <message>
      <source>REFERENCE_IMAGE_CS</source>
      <translation>Reference Image CS</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>TRANSFORM_IMAGE</source>
      <translation>Transform image</translation>
    </message>
    <message>
      <source>IMPORT_GEO_DATA_FROM_FILE</source>
      <translation>Import georeferencement data from file</translation>
    </message>
    <message>
      <source>IMAGE_GEOREFERENCEMENT_FILTER</source>
      <translation>Image georeferencement files (*.grf);;All files (*.* *)</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportImageOp</name>
    <message>
      <source>EDIT_IMPORTED_IMAGE</source>
      <translation>Edit imported image</translation>
    </message>
    <message>
      <source>IMPORT_IMAGE</source>
      <translation>Import image</translation>
    </message>
    <message>
      <source>IMPORTED_IMAGE</source>
      <translation>Imported image</translation>
    </message>
    <message>
      <source>TRANSFORM_IMAGE</source>
      <translation>Transform image</translation>
    </message>
    <message>
      <source>POINTS_A_B_ARE_IDENTICAL</source>
      <translation>Points A, B are identical.</translation>
    </message>
    <message>
      <source>POINTS_A_B_C_BELONG_TO_SINGLE_LINE</source>
      <translation>Points A, B, C belong to a single line.</translation>
    </message>
    <message>
      <source>REFERENCE_IMAGE</source>
      <translation>Reference image</translation>
    </message>
    <message>
      <source>REFERENCE_IMAGE_IS_NOT_SELECTED</source>
      <translation>Reference image is not selected.</translation>
    </message>
    <message>
      <source>REFERENCE_POINTS_A_B_ARE_IDENTICAL</source>
      <translation>Reference points A, B are identical.</translation>
    </message>
    <message>
      <source>REFERENCE_POINTS_A_B_C_BELONG_TO_SINGLE_LINE</source>
      <translation>Reference points A, B, C belong to a single line.</translation>
    </message>
    <message>
      <source>TRANSFORMATION_MATRIX_CANNOT_BE_COMPUTED</source>
      <translation>Transformation matrix cannot be computed.</translation>
    </message>
    <message>
      <source>CORRECT_INPUT_DATA</source>
      <translation>Correct input data</translation>
    </message>
    <message>
      <source>CONFIRM_REMOVE_REFERENCE_FROM_IMAGE</source>
      <translation>The image "%1" has a reference to the current object.
Would you like to remove all references from the image?</translation>
    </message>
    <message>
      <source>CANT_LOAD_GEOREFERENCEMENT_FILE</source>
      <translation>Can't load data from the image georeferencement file.</translation>
    </message>
    <message>
      <source>SELECT_IMAGE_NAME</source>
      <translation>The image name is not input</translation>
    </message>
    <message>
      <source>SELECT_IMAGE_FILE</source>
      <translation>The image file is not chosen</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_Module</name>
    <message>
      <source>DSK_CREATE_CALCULATION</source>
      <translation>Create calculation case</translation>
    </message>
    <message>
      <source>DSK_CREATE_POLYLINE</source>
      <translation>Create polyline</translation>
    </message>
    <message>
      <source>DSK_CREATE_POLYLINE_3D</source>
      <translation>Create polyline 3D</translation>
    </message>
    <message>
      <source>DSK_CREATE_PROFILE</source>
      <translation>Create profile</translation>
    </message>
    <message>
      <source>DSK_IMPORT_PROFILES</source>
      <translation>Import profiles from file(s)</translation>
    </message>
    <message>
      <source>DSK_GEOREFERENCEMENT</source>
      <translation>Profiles georeferencement</translation>
    </message>
    <message>
      <source>DSK_CREATE_IMMERSIBLE_ZONE</source>
      <translation>Create immersible zone</translation>
    </message>
    <message>
      <source>DSK_EDIT_IMMERSIBLE_ZONE</source>
      <translation>Edit immersible zone</translation>
    </message>
    <message>
      <source>DSK_CREATE_STREAM</source>
      <translation>Create stream</translation>
    </message>
    <message>
      <source>DSK_EDIT_STREAM</source>
      <translation>Edit stream</translation>
    </message>
    <message>
      <source>DSK_CREATE_CHANNEL</source>
      <translation>Create channel</translation>
    </message>
    <message>
      <source>DSK_EDIT_CHANNEL</source>
      <translation>Edit channel</translation>
    </message>
    <message>
      <source>DSK_CREATE_DIGUE</source>
      <translation>Create dyke</translation>
    </message>
    <message>
      <source>DSK_EDIT_DIGUE</source>
      <translation>Edit digue</translation>
    </message>
    <message>
      <source>DSK_IMPORT_STRICKLER_TABLE</source>
      <translation>Import Strickler table</translation>
    </message>
    <message>
      <source>DSK_EXPORT_STRICKLER_TABLE</source>
      <translation>Export Strickler table</translation>
    </message>
    <message>
      <source>DSK_EDIT_STRICKLER_TABLE</source>
      <translation>Edit Strickler table</translation>
    </message>
    <message>
      <source>DSK_DUPLICATE_STRICKLER_TABLE</source>
      <translation>Duplicate Strickler table</translation>
    </message>
    <message>
      <source>DSK_CREATE_LAND_COVER_MAP</source>
      <translation>Create land cover map</translation>
    </message>
    <message>
      <source>DSK_ADD_LAND_COVER</source>
      <translation>Add land cover</translation>
    </message>
    <message>
      <source>DSK_REMOVE_LAND_COVER</source>
      <translation>Remove land cover</translation>
    </message>
    <message>
      <source>DSK_SPLIT_LAND_COVER</source>
      <translation>Split land cover(s)</translation>
    </message>
    <message>
      <source>DSK_MERGE_LAND_COVER</source>
      <translation>Merge land covers</translation>
    </message>
    <message>
      <source>DSK_CHANGE_LAND_COVER_TYPE</source>
      <translation>Change land cover(s) type</translation>
    </message>
    <message>
      <source>DSK_COPY</source>
      <translation>Copy</translation>
    </message>
    <message>
      <source>DSK_CUT_IMAGES</source>
      <translation>Cut images</translation>
    </message>
    <message>
      <source>DSK_DELETE</source>
      <translation>Delete</translation>
    </message>
    <message>
      <source>DSK_EDIT_CALCULATION</source>
      <translation>Edit calculation case</translation>
    </message>
    <message>
      <source>DSK_COMPLETE_CALCULATION</source>
      <translation>Complete calculation case</translation>
    </message>
    <message>
      <source>DSK_EXPORT_CALCULATION</source>
      <translation>Export calculation case to GEOM</translation>
    </message>
    <message>
      <source>DSK_EDIT_CUT_IMAGE</source>
      <translation>Edit cut image</translation>
    </message>
    <message>
      <source>DSK_EDIT_FUSED_IMAGE</source>
      <translation>Edit fused image</translation>
    </message>
    <message>
      <source>DSK_EDIT_IMPORTED_IMAGE</source>
      <translation>Edit imported image</translation>
    </message>
    <message>
      <source>DSK_EDIT_POLYLINE</source>
      <translation>Edit polyline</translation>
    </message>
    <message>
      <source>DSK_EDIT_POLYLINE_3D</source>
      <translation>Edit polyline 3D</translation>
    </message>
    <message>
      <source>DSK_EDIT_SPLIT_IMAGE</source>
      <translation>Edit split image</translation>
    </message>
    <message>
      <source>DSK_COPY_VIEWER_POSITION</source>
      <translation>Copy position</translation>
    </message>
    <message>
      <source>DSK_EXPORT_IMAGE</source>
      <translation>Export image</translation>
    </message>
    <message>
      <source>DSK_FUSE_IMAGES</source>
      <translation>Fuse images</translation>
    </message>
    <message>
      <source>DSK_HIDE</source>
      <translation>Hide</translation>
    </message>
    <message>
      <source>DSK_HIDE_ALL</source>
      <translation>Hide all</translation>
    </message>
    <message>
      <source>DSK_IMPORT_BATHYMETRY</source>
      <translation>Import bathymetry</translation>
    </message>
    <message>
      <source>DSK_EDIT_IMPORTED_BATHYMETRY</source>
      <translation>Edit imported bathymetry</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_BOUNDS</source>
      <translation>Create boundary polyline</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_SELECTION</source>
      <translation>Selection on bathymetry</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_TEXT</source>
      <translation>Z-Values on bathymetry</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_RESCALE_SELECTION</source>
      <translation>Rescale bathymetry by selection</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_RESCALE_VISIBLE</source>
      <translation>Rescale bathymetry by visible range</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_RESCALE_USER</source>
      <translation>Custom rescale bathymetry</translation>
    </message>
    <message>
      <source>DSK_BATHYMETRY_RESCALE_DEFAULT</source>
      <translation>Default rescale bathymetry</translation>
    </message>
    <message>
      <source>DSK_SHOW_HIDE_ARROWS</source>
      <translation>Show/hide arrows</translation>
    </message>
    <message>
      <source>DSK_IMPORT_IMAGE</source>
      <translation>Import image</translation>
    </message>
    <message>
      <source>DSK_IMPORT_POLYLINE</source>
      <translation>Import polyline from file(s)</translation>
    </message>
    <message>
      <source>DSK_IMPORT_LANDCOVER_MAP</source>
      <translation>Import land cover map from file(s)</translation>
    </message>
    <message>
      <source>DSK_IMPORT_SINUSX</source>
      <translation>Import from SinusX</translation>
    </message>
    <message>
      <source>DSK_MEASUREMENT_TOOL</source>
      <translation>Measurement tool</translation>
    </message>
    <message>
      <source>DSK_IMPORT_BC_POLYGON</source>
      <translation>Import boundary polygons from SHP file</translation>
    </message>
    <message>
      <source>DSK_EXPORT_SINUSX</source>
      <translation>Export to SinusX</translation>
    </message>
    <message>
      <source>DSK_LOAD_VISUAL_STATE</source>
      <translation>Load visual state</translation>
    </message>
    <message>
      <source>DSK_OBSERVE_IMAGE</source>
      <translation>Observe image</translation>
    </message>
    <message>
      <source>DSK_PASTE</source>
      <translation>Paste</translation>
    </message>
    <message>
      <source>DSK_REDO</source>
      <translation>Redo</translation>
    </message>
    <message>
      <source>DSK_REMOVE_IMAGE_REFERENCE</source>
      <translation>Remove reference</translation>
    </message>
    <message>
      <source>DSK_SAVE_VISUAL_STATE</source>
      <translation>Save visual state</translation>
    </message>
    <message>
      <source>DSK_SHOW</source>
      <translation>Show</translation>
    </message>
    <message>
      <source>DSK_SHOW_ALL</source>
      <translation>Show all</translation>
    </message>
    <message>
      <source>DSK_SHOW_ONLY</source>
      <translation>Show only</translation>
    </message>
    <message>
      <source>DSK_SPLIT_IMAGE</source>
      <translation>Split image</translation>
    </message>
    <message>
      <source>DSK_SPLIT_POLYLINES</source>
      <translation>Split polylines</translation>
    </message>
    <message>
      <source>DSK_MERGE_POLYLINES</source>
      <translation>Merge polylines</translation>
    </message>
    <message>
      <source>DSK_SHOWATTR_POLYLINES</source>
      <translation>Show DBF attributes</translation>
    </message>
    <message>
      <source>DSK_UNDO</source>
      <translation>Undo</translation>
    </message>
    <message>
      <source>DSK_UPDATE_OBJECT</source>
      <translation>Update</translation>
    </message>
    <message>
      <source>DSK_FORCED_UPDATE_OBJECT</source>
      <translation>Forced update</translation>
    </message>
    <message>
      <source>DSK_IMPORT_OBSTACLE_FROM_FILE</source>
      <translation>Import obstacle from file</translation>
    </message>
    <message>
      <source>DSK_CREATE_BOX</source>
      <translation>Create box obstacle</translation>
    </message>
    <message>
      <source>DSK_IMPORT_GEOM_OBJECT_AS_OBSTACLE</source>
      <translation>Import GEOM object(s) as obstacle(s)</translation>
    </message>
    <message>
      <source>DSK_IMPORT_GEOM_OBJECT_AS_POLYLINE</source>
      <translation>Import GEOM object(s) as polyline(s)</translation>
    </message>
    <message>
      <source>DSK_CREATE_CYLINDER</source>
      <translation>Create cylinder obstacle</translation>
    </message>
    <message>
      <source>DSK_TRANSLATE_OBSTACLE</source>
      <translation>Translate obstacle</translation>
    </message>
    <message>
      <source>DSK_COLOR</source>
      <translation>Set object color</translation>
    </message>
    <message>
      <source>DSK_TRANSPARENCY</source>
      <translation>Set object transparency</translation>
    </message>
    <message>
      <source>DSK_ZLEVEL</source>
      <translation>Change layer order</translation>
    </message>
    <message>
      <source>DSK_EDIT_LOCAL_CS</source>
      <translation>Change local CS</translation>
    </message>
    <message>
      <source>DSK_SUBMERSIBLE</source>
      <translation>Submersible</translation>
    </message>
    <message>
      <source>DSK_UNSUBMERSIBLE</source>
      <translation>Unsubmersible</translation>
    </message>
    <message>
      <source>DSK_EXPORT_TO_SHAPE_FILE</source>
      <translation>Export</translation>
    </message>
    <message>
      <source>DSK_POLYLINE_EXTRACTION</source>
      <translation>Extracts the polyline from selected object</translation>
    </message>

    <message>
      <source>MEN_CREATE_CALCULATION</source>
      <translation>Create calculation case</translation>
    </message>
    <message>
      <source>MEN_CREATE_POLYLINE</source>
      <translation>Create polyline</translation>
    </message>
    <message>
      <source>MEN_CREATE_POLYLINE_3D</source>
      <translation>Create polyline 3D</translation>
    </message>
    <message>
      <source>MEN_CREATE_PROFILE</source>
      <translation>Create profile</translation>
    </message>
    <message>
      <source>MEN_IMPORT_PROFILES</source>
      <translation>Import profiles</translation>
    </message>
    <message>
      <source>MEN_GEOREFERENCEMENT</source>
      <translation>Georeferencement</translation>
    </message>
    <message>
      <source>MEN_CREATE_ZONE</source>
      <translation>Create zone</translation>
    </message>
    <message>
      <source>MEN_CREATE_IMMERSIBLE_ZONE</source>
      <translation>Create immersible zone</translation>
    </message>
    <message>
      <source>MEN_EDIT_IMMERSIBLE_ZONE</source>
      <translation>Edit immersible zone</translation>
    </message>
    <message>
      <source>MEN_CREATE_STREAM</source>
      <translation>Create stream</translation>
    </message>
    <message>
      <source>MEN_EDIT_STREAM</source>
      <translation>Edit stream</translation>
    </message>
    <message>
      <source>MEN_CREATE_CHANNEL</source>
      <translation>Create channel</translation>
    </message>
    <message>
      <source>MEN_EDIT_CHANNEL</source>
      <translation>Edit channel</translation>
    </message>
    <message>
      <source>MEN_CREATE_DIGUE</source>
      <translation>Create dyke</translation>
    </message>
    <message>
      <source>MEN_EDIT_DIGUE</source>
      <translation>Edit digue</translation>
    </message>
    <message>
      <source>MEN_IMPORT_STRICKLER_TABLE</source>
      <translation>Import Strickler table</translation>
    </message>
    <message>
      <source>MEN_EXPORT_STRICKLER_TABLE</source>
      <translation>Export Strickler table</translation>
    </message>
    <message>
      <source>MEN_EDIT_STRICKLER_TABLE</source>
      <translation>Edit Strickler table</translation>
    </message>
    <message>
      <source>MEN_DUPLICATE_STRICKLER_TABLE</source>
      <translation>Duplicate Strickler table</translation>
    </message>
    <message>
      <source>MEN_CREATE_LAND_COVER_MAP</source>
      <translation>Create land cover map</translation>
    </message>
    <message>
      <source>MEN_ADD_LAND_COVER</source>
      <translation>Add land cover</translation>
    </message>
    <message>
      <source>MEN_REMOVE_LAND_COVER</source>
      <translation>Remove land cover</translation>
    </message>
    <message>
      <source>MEN_SPLIT_LAND_COVER</source>
      <translation>Split land cover(s)</translation>
    </message>
    <message>
      <source>MEN_MERGE_LAND_COVER</source>
      <translation>Merge land covers</translation>
    </message>
    <message>
      <source>MEN_CHANGE_LAND_COVER_TYPE</source>
      <translation>Change land cover(s) type</translation>
    </message>
    <message>
      <source>MEN_CUT_IMAGES</source>
      <translation>Cut images</translation>
    </message>
    <message>
      <source>MEN_DELETE</source>
      <translation>Delete</translation>
    </message>
    <message>
      <source>MEN_DESK_ARTIFICIAL</source>
      <translation>Artificial objects</translation>
    </message>
    <message>
      <source>MEN_DESK_HYDRO</source>
      <translation>HYDRO</translation>
    </message>
    <message>
      <source>MEN_DESK_STREAM</source>
      <translation>Stream</translation>
    </message>
    <message>
      <source>MEN_DESK_NATURAL</source>
      <translation>Natural objects</translation>
    </message>
    <message>
      <source>MEN_DESK_OBSTACLE</source>
      <translation>Obstacle</translation>
    </message>
    <message>
      <source>MEN_DESK_PROFILE</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>MEN_DESK_IMAGE</source>
      <translation>Image</translation>
    </message>
    <message>
      <source>MEN_DESK_POLYLINE</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>MEN_DESK_LAND_COVER_MAP</source>
      <translation>Land cover map</translation>
    </message>
    <message>
      <source>MEN_EDIT_CALCULATION</source>
      <translation>Edit calculation case</translation>
    </message>
    <message>
      <source>MEN_COMPLETE_CALCULATION</source>
      <translation>Complete calculation case</translation>
    </message>
    <message>
      <source>MEN_EXPORT_CALCULATION</source>
      <translation>Export calculation case</translation>
    </message>
    <message>
      <source>MEN_EDIT_CUT_IMAGE</source>
      <translation>Edit cut image</translation>
    </message>
    <message>
      <source>MEN_EDIT_FUSED_IMAGE</source>
      <translation>Edit fused image</translation>
    </message>
    <message>
      <source>MEN_EDIT_IMPORTED_IMAGE</source>
      <translation>Edit imported image</translation>
    </message>
    <message>
      <source>MEN_EDIT_POLYLINE</source>
      <translation>Edit polyline</translation>
    </message>
    <message>
      <source>MEN_EDIT_POLYLINE_3D</source>
      <translation>Edit polyline 3D</translation>
    </message>
    <message>
      <source>MEN_EDIT_PROFILE</source>
      <translation>Edit profile</translation>
    </message>
    <message>
      <source>MEN_EDIT_SPLIT_IMAGE</source>
      <translation>Edit split image</translation>
    </message>
    <message>
      <source>MEN_COPY_VIEWER_POSITION</source>
      <translation>Copy position</translation>
    </message>
    <message>
      <source>MEN_EXPORT_IMAGE</source>
      <translation>Export image</translation>
    </message>
    <message>
      <source>MEN_FUSE_IMAGES</source>
      <translation>Fuse images</translation>
    </message>
    <message>
      <source>MEN_HIDE</source>
      <translation>Hide</translation>
    </message>
    <message>
      <source>MEN_HIDE_ALL</source>
      <translation>Hide all</translation>
    </message>
    <message>
      <source>MEN_IMPORT_BATHYMETRY</source>
      <translation>Import bathymetry</translation>
    </message>
    <message>
      <source>MEN_IMPORT_BC_POLYGON</source>
      <translation>Import boundary polygons</translation>
    </message>
    <message>
      <source>MEN_EDIT_IMPORTED_BATHYMETRY</source>
      <translation>Edit imported bathymetry</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_BOUNDS</source>
      <translation>Create boundary polyline</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_SELECTION</source>
      <translation>Selection on bathymetry</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_TEXT</source>
      <translation>Z-Values on bathymetry</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_RESCALE_SELECTION</source>
      <translation>Rescale bathymetry by selection</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_RESCALE_VISIBLE</source>
      <translation>Rescale bathymetry by visible range</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_RESCALE_USER</source>
      <translation>Custom rescale bathymetry</translation>
    </message>
    <message>
      <source>MEN_BATHYMETRY_RESCALE_DEFAULT</source>
      <translation>Default rescale bathymetry</translation>
    </message>
    <message>
      <source>MEN_SHOW_HIDE_ARROWS</source>
      <translation>Show/hide arrows</translation>
    </message>
    <message>
      <source>HIDE_ARROWS</source>
      <translation>Hide arrows</translation>
    </message>
    <message>
      <source>SHOW_ARROWS</source>
      <translation>Show arrows</translation>
    </message>
    <message>
      <source>MEN_IMPORT_IMAGE</source>
      <translation>Import image</translation>
    </message>
    <message>
      <source>MEN_IMPORT_POLYLINE</source>
      <translation>Import polyline</translation>
    </message>
    <message>
      <source>MEN_IMPORT_LANDCOVER_MAP</source>
      <translation>Import land cover map from file(s)</translation>
    </message>
    <message>
      <source>MEN_IMPORT_SINUSX</source>
      <translation>Import from SinusX</translation>
    </message>
    <message>
      <source>MEN_MEASUREMENT_TOOL</source>
      <translation>Measurement tool</translation>
    </message>
    <message>
      <source>MEN_EXPORT_SINUSX</source>
      <translation>Export to SinusX</translation>
    </message>
    <message>
      <source>MEN_LOAD_VISUAL_STATE</source>
      <translation>Load visual state</translation>
    </message>
    <message>
      <source>MEN_OBSERVE_IMAGE</source>
      <translation>Observe image</translation>
    </message>
    <message>
      <source>MEN_COPY</source>
      <translation>Copy</translation>
    </message>
    <message>
      <source>MEN_PASTE</source>
      <translation>Paste</translation>
    </message>
    <message>
      <source>MEN_REDO</source>
      <translation>Redo</translation>
    </message>
    <message>
      <source>MEN_REMOVE_IMAGE_REFERENCE</source>
      <translation>Remove reference</translation>
    </message>
    <message>
      <source>MEN_SAVE_VISUAL_STATE</source>
      <translation>Save visual state</translation>
    </message>
    <message>
      <source>MEN_SHOW</source>
      <translation>Show</translation>
    </message>
    <message>
      <source>MEN_SHOW_ALL</source>
      <translation>Show all</translation>
    </message>
    <message>
      <source>MEN_SHOW_ONLY</source>
      <translation>Show only</translation>
    </message>
    <message>
      <source>MEN_SPLIT_IMAGE</source>
      <translation>Split image</translation>
    </message>
    <message>
      <source>MEN_SPLIT_POLYLINES</source>
      <translation>Split polylines</translation>
    </message>
    <message>
      <source>MEN_MERGE_POLYLINES</source>
      <translation>Merge polylines</translation>
    </message>
    <message>
      <source>MEN_SHOWATTR_POLYLINES</source>
      <translation>Show DBF attributes</translation>
    </message>
    <message>
      <source>MEN_UNDO</source>
      <translation>Undo</translation>
    </message>
    <message>
      <source>MEN_UPDATE_OBJECT</source>
      <translation>Update</translation>
    </message>
    <message>
      <source>MEN_FORCED_UPDATE_OBJECT</source>
      <translation>Forced update</translation>
    </message>
    <message>
      <source>MEN_IMPORT_OBSTACLE_FROM_FILE</source>
      <translation>Import obstacle</translation>
    </message>
    <message>
      <source>MEN_IMPORT_GEOM_OBJECT_AS_OBSTACLE</source>
      <translation>Import as obstacle</translation>
    </message>
    <message>
      <source>MEN_IMPORT_GEOM_OBJECT_AS_POLYLINE</source>
      <translation>Import as polyline</translation>
    </message>
    <message>
      <source>MEN_CREATE_BOX</source>
      <translation>Create box</translation>
    </message>
    <message>
      <source>MEN_CREATE_CYLINDER</source>
      <translation>Create cylinder</translation>
    </message>
    <message>
      <source>MEN_TRANSLATE_OBSTACLE</source>
      <translation>Translate</translation>
    </message>
    <message>
      <source>MEN_COLOR</source>
      <translation>Color</translation>
    </message>
    <message>
      <source>MEN_TRANSPARENCY</source>
      <translation>Transparency</translation>
    </message>
    <message>
      <source>MEN_ZLEVEL</source>
      <translation>Change layer order</translation>
    </message>
    <message>
      <source>MEN_EDIT_LOCAL_CS</source>
      <translation>Change local CS</translation>
    </message>
    <message>
      <source>MEN_SUBMERSIBLE</source>
      <translation>Submersible</translation>
    </message>
    <message>
      <source>MEN_UNSUBMERSIBLE</source>
      <translation>Unsubmersible</translation>
    </message>
    <message>
      <source>MEN_EXPORT_TO_SHAPE_FILE</source>
      <translation>Export</translation>
    </message>
    <message>
      <source>MEN_POLYLINE_EXTRACTION</source>
      <translation>Polyline extraction</translation>
    </message>
    <message>
      <source>MEN_REGENERATE_REGION_COLORS</source>
      <translation>Regenerate region colors</translation>
    </message>
    <message>
      <source>MEN_ZONE_SET_COLOR</source>
      <translation>Set color</translation>
    </message>
    <message>
      <source>MEN_SET_BOUNDARY_TYPE_POLYGON</source>
      <translation>Set boundary type</translation>
    </message>
    <message>
      <source>STB_CREATE_CALCULATION</source>
      <translation>Create calculation case</translation>
    </message>
    <message>
      <source>STB_CREATE_POLYLINE</source>
      <translation>Create polyline</translation>
    </message>
    <message>
      <source>STB_CREATE_POLYLINE_3D</source>
      <translation>Create polyline 3D</translation>
    </message>
    <message>
      <source>STB_CREATE_PROFILE</source>
      <translation>Create profile</translation>
    </message>
    <message>
      <source>STB_IMPORT_PROFILES</source>
      <translation>Import profiles from file(s)</translation>
    </message>
    <message>
      <source>STB_GEOREFERENCEMENT</source>
      <translation>Profiles georeferencement</translation>
    </message>
    <message>
      <source>STB_CREATE_IMMERSIBLE_ZONE</source>
      <translation>Create immersible zone</translation>
    </message>
    <message>
      <source>STB_EDIT_IMMERSIBLE_ZONE</source>
      <translation>Edit immersible zone</translation>
    </message>
    <message>
      <source>STB_CREATE_STREAM</source>
      <translation>Create stream</translation>
    </message>
    <message>
      <source>STB_EDIT_STREAM</source>
      <translation>Edit stream</translation>
    </message>
    <message>
      <source>STB_CREATE_CHANNEL</source>
      <translation>Create channel</translation>
    </message>
    <message>
      <source>STB_EDIT_CHANNEL</source>
      <translation>Edit channel</translation>
    </message>
    <message>
      <source>STB_CREATE_DIGUE</source>
      <translation>Create dyke</translation>
    </message>
    <message>
      <source>STB_EDIT_DIGUE</source>
      <translation>Edit digue</translation>
    </message>
    <message>
      <source>STB_IMPORT_STRICKLER_TABLE</source>
      <translation>Import Strickler table</translation>
    </message>
    <message>
      <source>STB_EXPORT_STRICKLER_TABLE</source>
      <translation>Export Strickler table</translation>
    </message>
    <message>
      <source>STB_EDIT_STRICKLER_TABLE</source>
      <translation>Edit Strickler table</translation>
    </message>
    <message>
      <source>STB_DUPLICATE_STRICKLER_TABLE</source>
      <translation>Duplicate Strickler table</translation>
    </message>
    <message>
      <source>STB_IMPORT_BC_POLYGON</source>
      <translation>Import boundary polygons</translation>
    </message>
    <message>
      <source>STB_CREATE_LAND_COVER_MAP</source>
      <translation>Create land cover map</translation>
    </message>
    <message>
      <source>STB_ADD_LAND_COVER</source>
      <translation>Add land cover</translation>
    </message>
    <message>
      <source>STB_REMOVE_LAND_COVER</source>
      <translation>Remove land cover</translation>
    </message>
    <message>
      <source>STB_SPLIT_LAND_COVER</source>
      <translation>Split land cover(s)</translation>
    </message>
    <message>
      <source>STB_MERGE_LAND_COVER</source>
      <translation>Merge land covers</translation>
    </message>
    <message>
      <source>STB_CHANGE_LAND_COVER_TYPE</source>
      <translation>Change land cover(s) type</translation>
    </message>
    <message>
      <source>STB_COPY</source>
      <translation>Copy</translation>
    </message>
    <message>
      <source>STB_CUT_IMAGES</source>
      <translation>Cut images</translation>
    </message>
    <message>
      <source>STB_DELETE</source>
      <translation>Delete</translation>
    </message>
    <message>
      <source>STB_EDIT_CALCULATION</source>
      <translation>Edit calculation case</translation>
    </message>
    <message>
      <source>STB_COMPLETE_CALCULATION</source>
      <translation>Complete calculation case</translation>
    </message>
    <message>
      <source>STB_EXPORT_CALCULATION</source>
      <translation>Export calculation case to GEOM</translation>
    </message>
    <message>
      <source>STB_EDIT_CUT_IMAGE</source>
      <translation>Edit cut image</translation>
    </message>
    <message>
      <source>STB_EDIT_FUSED_IMAGE</source>
      <translation>Edit fused image</translation>
    </message>
    <message>
      <source>STB_EDIT_IMPORTED_IMAGE</source>
      <translation>Edit imported image</translation>
    </message>
    <message>
      <source>STB_EDIT_POLYLINE</source>
      <translation>Edit polyline</translation>
    </message>
    <message>
      <source>STB_EDIT_POLYLINE_3D</source>
      <translation>Edit polyline 3D</translation>
    </message>
    <message>
      <source>STB_EDIT_SPLIT_IMAGE</source>
      <translation>Edit split image</translation>
    </message>
    <message>
      <source>STB_COPY_VIEWER_POSITION</source>
      <translation>Copy position</translation>
    </message>
    <message>
      <source>STB_EXPORT_IMAGE</source>
      <translation>Export image</translation>
    </message>
    <message>
      <source>STB_FUSE_IMAGES</source>
      <translation>Fuse images</translation>
    </message>
    <message>
      <source>STB_HIDE</source>
      <translation>Hide</translation>
    </message>
    <message>
      <source>STB_HIDE_ALL</source>
      <translation>Hide all</translation>
    </message>
    <message>
      <source>STB_IMPORT_BATHYMETRY</source>
      <translation>Import bathymetry</translation>
    </message>
    <message>
      <source>STB_EDIT_IMPORTED_BATHYMETRY</source>
      <translation>Edit imported bathymetry</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_BOUNDS</source>
      <translation>Create boundary polyline</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_SELECTION</source>
      <translation>Selection on bathymetry</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_RESCALE_SELECTION</source>
      <translation>Rescale bathymetry by selection</translation>
    </message>
    <message>
      <source>STB_POLYLINE_STYLE</source>
      <translation>Change polyline style</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_RESCALE_VISIBLE</source>
      <translation>Rescale bathymetry by visible range</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_RESCALE_USER</source>
      <translation>Custom rescale bathymetry</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_RESCALE_DEFAULT</source>
      <translation>Default rescale bathymetry</translation>
    </message>
    <message>
      <source>STB_IMPORT_IMAGE</source>
      <translation>Import image</translation>
    </message>
    <message>
      <source>STB_IMPORT_POLYLINE</source>
      <translation>Import polyline</translation>
    </message>
    <message>
      <source>STB_IMPORT_LANDCOVER_MAP</source>
      <translation>Import land cover map from file(s)</translation>
    </message>
    <message>
      <source>STB_IMPORT_SINUSX</source>
      <translation>Import from SinusX</translation>
    </message>
    <message>
      <source>STB_MEASUREMENT_TOOL</source>
      <translation>Measurement tool</translation>
    </message>
    <message>
      <source>STB_EXPORT_SINUSX</source>
      <translation>Export to SinusX</translation>
    </message>
    <message>
      <source>STB_LOAD_VISUAL_STATE</source>
      <translation>Load visual state</translation>
    </message>
    <message>
      <source>STB_OBSERVE_IMAGE</source>
      <translation>Observe image</translation>
    </message>
    <message>
      <source>STB_PASTE</source>
      <translation>Paste</translation>
    </message>
    <message>
      <source>STB_REDO</source>
      <translation>Redo</translation>
    </message>
    <message>
      <source>STB_REMOVE_IMAGE_REFERENCE</source>
      <translation>Remove reference</translation>
    </message>
    <message>
      <source>STB_SAVE_VISUAL_STATE</source>
      <translation>Save visual state</translation>
    </message>
    <message>
      <source>STB_SHOW</source>
      <translation>Show</translation>
    </message>
    <message>
      <source>STB_SHOW_ALL</source>
      <translation>Show all</translation>
    </message>
    <message>
      <source>STB_SHOW_ONLY</source>
      <translation>Show only</translation>
    </message>
    <message>
      <source>STB_SPLIT_IMAGE</source>
      <translation>Split image</translation>
    </message>
    <message>
      <source>STB_SPLIT_POLYLINES</source>
      <translation>Split polylines</translation>
    </message>
    <message>
      <source>STB_MERGE_POLYLINES</source>
      <translation>Merge polylines</translation>
    </message>
    <message>
      <source>STB_SHOWATTR_POLYLINES</source>
      <translation>Show DBF attributes</translation>
    </message>
    <message>
      <source>STB_UNDO</source>
      <translation>Undo</translation>
    </message>
    <message>
      <source>STB_UPDATE_OBJECT</source>
      <translation>Update</translation>
    </message>
    <message>
      <source>STB_FORCED_UPDATE_OBJECT</source>
      <translation>Forced update</translation>
    </message>
    <message>
      <source>STB_IMPORT_OBSTACLE_FROM_FILE</source>
      <translation>Import obstacle from file</translation>
    </message>
    <message>
      <source>STB_IMPORT_GEOM_OBJECT_AS_OBSTACLE</source>
      <translation>Import GEOM object(s) as obstacle(s)</translation>
    </message>
    <message>
      <source>STB_IMPORT_GEOM_OBJECT_AS_POLYLINE</source>
      <translation>Import GEOM object(s) as polyline(s)</translation>
    </message>
    <message>
      <source>STB_CREATE_BOX</source>
      <translation>Create box obstacle</translation>
    </message>
    <message>
      <source>STB_CREATE_CYLINDER</source>
      <translation>Create cylinder obstacle</translation>
    </message>
    <message>
      <source>STB_TRANSLATE_OBSTACLE</source>
      <translation>Translate obstacle</translation>
    </message>
    <message>
      <source>STB_COLOR</source>
      <translation>Set object color</translation>
    </message>
    <message>
      <source>STB_TRANSPARENCY</source>
      <translation>Set object transparency</translation>
    </message>
    <message>
      <source>STB_ZLEVEL</source>
      <translation>Change layer order</translation>
    </message>
    <message>
      <source>STB_EDIT_LOCAL_CS</source>
      <translation>Change local CS</translation>
    </message>
    <message>
      <source>STB_POLYLINE_EXTRACTION</source>
      <translation>Polyline extractions</translation>
    </message>
    <message>
      <source>STB_BATHYMETRY_TEXT</source>
      <translation>Z-Values on bathymetry</translation>
    </message>
    <message>
      <source>MEN_CREATE_STREAM_BOTTOM</source>
      <translation>Find bottom</translation>
    </message>
    <message>
      <source>DSK_CREATE_STREAM_BOTTOM</source>
      <translation>Create stream bottom polyline</translation>
    </message>
    <message>
      <source>STB_CREATE_STREAM_BOTTOM</source>
      <translation>Creates the stream bottom polyline on selected stream object</translation>
    </message>
    <message>
      <source>STB_SUBMERSIBLE</source>
      <translation>If the object is submersible</translation>
    </message>
    <message>
      <source>STB_UNSUBMERSIBLE</source>
      <translation>If the object is non submersible</translation>
    </message>
    <message>
      <source>STB_EXPORT_TO_SHAPE_FILE</source>
      <translation>Export</translation>
    </message>


    <message>
      <source>MEN_PROFILE_INTERPOLATE</source>
      <translation>Profiles interpolation</translation>
    </message>
    <message>
      <source>DSK_PROFILE_INTERPOLATE</source>
      <translation>Interpolate the stream profiles</translation>
    </message>
    <message>
      <source>STB_PROFILE_INTERPOLATE</source>
      <translation>Creates the new stream profiles using interpolation of selected ones</translation>
    </message>

    <message>
      <source>MEN_RECOGNIZE_CONTOURS</source>
      <translation>Recognize contours</translation>
    </message>
    <message>
      <source>DSK_RECOGNIZE_CONTOURS</source>
      <translation>Recognize contours</translation>
    </message>
    <message>
      <source>STB_RECOGNIZE_CONTOURS</source>
      <translation>Recognize contours</translation>
    </message>

    <message>
      <source>MEN_LC_SCALARMAP_COLORING_ON</source>
      <translation>Use as scalar scale</translation>
    </message>
    <message>
      <source>DSK_LC_SCALARMAP_COLORING_ON</source>
      <translation>Use the table as a scalar scale for Land Covers coloring</translation>
    </message>
    <message>
      <source>STB_LC_SCALARMAP_COLORING_ON</source>
      <translation>Use as scalar scale</translation>
    </message>

    <message>
      <source>MEN_LC_SCALARMAP_COLORING_OFF</source>
      <translation>Scalar map mode off</translation>
    </message>
    <message>
      <source>DSK_LC_SCALARMAP_COLORING_OFF</source>
      <translation>Turn off Land Covers scalar map coloring mode</translation>
    </message>
    <message>
      <source>STB_LC_SCALARMAP_COLORING_OFF</source>
      <translation>Scalar map mode off</translation>
    </message>
    <message>
      <source>PREF_GROUP_POLYLINES</source>
      <translation>Polylines</translation>
    </message>
    <message>
      <source>PREF_POLYLINE_ARROW</source>
      <translation>Polyline arrow</translation>
    </message>
    <message>
      <source>PREF_POLYLINE_ARROW_SIZE</source>
      <translation>Polyline arrow size</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_PolylineOp</name>
    <message>
      <source>CREATE_POLYLINE</source>
      <translation>Create polyline</translation>
    </message>
    <message>
      <source>EDIT_POLYLINE</source>
      <translation>Edit polyline</translation>
    </message>
    <message>
      <source>EMPTY_POLYLINE_DATA</source>
      <translation>No one section is created for polyline, should be at least one.</translation>
    </message>
    <message>
      <source>NUMBER_OF_SECTION_POINTS_INCORRECT</source>
      <translation>Number of points in each polyline section should not be less than 2.</translation>
    </message>
    <message>
      <source>POLYLINE_IS_UNEDITABLE_TLT</source>
      <translation>Polyline can not be edited</translation>
    </message>
    <message>
      <source>POLYLINE_IS_UNEDITABLE_MSG</source>
      <translation>Polyline has an unsupported format and can not be edited.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_Poly3DOp</name>
    <message>
      <source>CREATE_POLYLINE_3D</source>
      <translation>Create polyline 3D</translation>
    </message>
    <message>
      <source>EDIT_POLYLINE_3D</source>
      <translation>Edit polyline 3D</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ProfileOp</name>
    <message>
      <source>CREATE_PROFILE</source>
      <translation>Create profile</translation>
    </message>
    <message>
      <source>EDIT_PROFILE</source>
      <translation>Edit profile</translation>
    </message>
    <message>
      <source>NUMBER_OF_PROFILE_POINTS_INCORRECT</source>
      <translation>Number of profile points should not be less than 3.</translation>
    </message>
    <message>
      <source>PROFILEOP_WARNING</source>
      <translation>Warning</translation>
    </message>
    <message>
      <source>PROFILES_ALREADY_PRESENT</source>
      <translation>All selected profile(s) are already present</translation>
    </message>
    <message>
      <source>PROFILES_ARE_NOT_SELECTED</source>
      <translation>Profile(s) are not selected</translation>
    </message>
    <message>
      <source>PROFILE_RENAMING_NOTIF</source>
      <translation>The next profile(s) have been renamed:</translation>
    </message>
  </context>
  <context>
    <name>HYDROGUI_RemoveImageRefsOp</name>
    <message>
      <source>REMOVE_IMAGE_REFERENCE</source>
      <translation>Remove image reference</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ShowHideOp</name>
    <message>
      <source>SHOW</source>
      <translation>Show</translation>
    </message>
    <message>
      <source>SHOW_ALL</source>
      <translation>Show all</translation>
    </message>
    <message>
      <source>SHOW_ONLY</source>
      <translation>Show only</translation>
    </message>
    <message>
      <source>HIDE</source>
      <translation>Hide</translation>
    </message>
    <message>
      <source>HIDE_ALL</source>
      <translation>Hide all</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ObserveImageOp</name>
    <message>
      <source>OBSERVE_IMAGE</source>
      <translation>Observe image</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_PolylineDlg</name>
    <message>
      <source>ADD_ELEMENT</source>
      <translation>Add element</translation>
    </message>
    <message>
      <source>EDIT_ELEMENT</source>
      <translation>Edit element</translation>
    </message>
    <message>
      <source>POLYLINE_NAME_TLT</source>
      <translation>Name</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_Poly3DDlg</name>
    <message>
      <source>POLYLINE_3D_NAME</source>
      <translation>Polyline 3d name</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
    <message>
      <source>POLYLINE</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>PROFILE</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>BATH</source>
      <translation>Bathymetry</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ViewerDlg</name>
    <message>
      <source>UZ_COORDINATES_INFO</source>
      <translation>U: %1, Z: %2</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ProfileDlg</name>
    <message>
      <source>ADD_ELEMENT</source>
      <translation>Add element</translation>
    </message>
    <message>
      <source>EDIT_ELEMENT</source>
      <translation>Edit element</translation>
    </message>
    <message>
      <source>PROFILE_NAME_TLT</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>U_TITLE</source>
      <translation>U</translation>
    </message>
    <message>
      <source>Z_TITLE</source>
      <translation>Z</translation>
    </message>
    <message>
      <source>ADD_PROFILES</source>
      <translation>Add Profile(s)</translation>
    </message>
    <message>
      <source>REMOVE_PROFILE</source>
      <translation>Remove Profile</translation>
    </message>
    <message>
      <source>SETCOLOR_PROFILE</source>
      <translation>Set Color</translation>
    </message>
    <message>
      <source>PROFILE_ALREADY_EXISTS</source>
      <translation>Profile with this name is already exists</translation>
    </message>
    <message>
      <source>PROFILEDLG_WARNING</source>
      <translation>Warning</translation>
    </message>

  </context>

  <context>
    <name>HYDROGUI_TwoImagesDlg</name>
    <message>
      <source>BACKGROUND</source>
      <translation>Background</translation>
    </message>
    <message>
      <source>COLOR</source>
      <translation>Color</translation>
    </message>
    <message>
      <source>IMAGE</source>
      <translation>Image</translation>
    </message>
    <message>
      <source>IMAGE_1</source>
      <translation>Image 1</translation>
    </message>
    <message>
      <source>IMAGE_2</source>
      <translation>Image 2</translation>
    </message>
    <message>
      <source>IMAGE_NAME</source>
      <translation>Image name</translation>
    </message>
    <message>
      <source>MODIFY_SELECTED_IMAGE</source>
      <translation>Modify selected image</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
    <message>
      <source>POLYLINE</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>TRANSPARENT</source>
      <translation>Transparent</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_TwoImagesOp</name>
    <message>
      <source>CUT</source>
      <translation>Cut</translation>
    </message>
    <message>
      <source>CUT_IMAGES</source>
      <translation>Cut images</translation>
    </message>
    <message>
      <source>EDIT_CUT_IMAGE</source>
      <translation>Edit cut image</translation>
    </message>
    <message>
      <source>EDIT_FUSED_IMAGE</source>
      <translation>Edit fused image</translation>
    </message>
    <message>
      <source>EDIT_SPLIT_IMAGE</source>
      <translation>Edit split image</translation>
    </message>
    <message>
      <source>FUSE</source>
      <translation>Fuse</translation>
    </message>
    <message>
      <source>FUSE_IMAGES</source>
      <translation>Fuse images</translation>
    </message>
    <message>
      <source>SPLIT</source>
      <translation>Split</translation>
    </message>
    <message>
      <source>SPLIT_IMAGE</source>
      <translation>Split image</translation>
    </message>
    <message>
      <source>OBJECT_ALREADY_SELECTED</source>
      <translation>The object "%1" has been already selected. Please select another one to perform the operation.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_UpdateImageOp</name>
    <message>
      <source>UPDATE_IMAGE</source>
      <translation>Update image</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_VisualStateOp</name>
    <message>
      <source>LOAD_VISUAL_STATE</source>
      <translation>Load visual state</translation>
    </message>
    <message>
      <source>SAVE_VISUAL_STATE</source>
      <translation>Save visual state</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImmersibleZoneDlg</name>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>ZONE_BATHYMETRY</source>
      <translation>Bathymetry</translation>
    </message>
    <message>
      <source>ZONE_NAME</source>
      <translation>Zone name</translation>
    </message>
    <message>
      <source>ZONE_PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
    <message>
      <source>ZONE_POLYLINE</source>
      <translation>Polyline</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImmersibleZoneOp</name>
    <message>
      <source>CREATE_IMMERSIBLE_ZONE</source>
      <translation>Create immersible zone</translation>
    </message>
    <message>
      <source>EDIT_IMMERSIBLE_ZONE</source>
      <translation>Edit immersible zone</translation>
    </message>
    <message>
      <source>ZONE_OBJECT_CANNOT_BE_CREATED</source>
      <translation>Zone object can not be created.
Maybe the polyline is not closed,
or there is a small loop somewhere in the polyline ?</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_LocalCSOp</name>
    <message>
      <source>EDIT_LOCAL_CS</source>
      <translation>Local CS transformation</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportGeomObjectOp</name>
    <message>
      <source>IMPORT_GEOM_OBJECT_AS_OBSTACLE</source>
      <translation>Import GEOM object as obstacle</translation>
    </message>
    <message>
      <source>IMPORT_GEOM_OBJECT_AS_POLYLINE</source>
      <translation>Import GEOM object as polyline</translation>
    </message>
    <message>
      <source>NO_GEOM_OBJECT_TO_IMPORT</source>
      <translation>No GEOM object(s) to import.</translation>
    </message>
    <message>
      <source>POLYLINE_IS_UNRECOGNIZED_TLT</source>
      <translation>Imported curve has an unsupported format</translation>
    </message>
    <message>
      <source>POLYLINE_IS_UNRECOGNIZED_MSG</source>
      <translation>Imported curve has an unsupported by HYDRO format,
it will be unavailable for future editing.
Do you still want to continue?</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportObstacleFromFileOp</name>
    <message>
      <source>IMPORT_OBSTACLE_FROM_FILE</source>
      <translation>Import obstacle</translation>
    </message>
    <message>
      <source>BAD_IMPORTED_OBSTACLE_FILE</source>
      <translation>'%1'
file cannot be correctly imported for an Obstacle definition.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ExportCalculationOp</name>
    <message>
      <source>EXPORT_CALCULATION</source>
      <translation>Export calculation case</translation>
    </message>
    <message>
      <source>EXPORT_STATUS</source>
      <translation>Export status</translation>
    </message>
    <message>
      <source>EXPORT_FINISHED</source>
      <translation>Export finished successfully.</translation>
    </message>
    <message>
      <source>EXPORT_FAILED</source>
      <translation>Export of calculation case failed:</translation>
    </message>
    <message>
      <source>EXPORT_DATA_FAILED</source>
      <translation>parameters of calculation case are invalid.</translation>
    </message>
    <message>
      <source>NULL_DATA_OBJECT</source>
      <translation>data model object is null pointer.</translation>
    </message>
    <message>
      <source>RESULT_SHAPE_NULL</source>
      <translation>shape of calculation case regions is empty.</translation>
    </message>
    <message>
      <source>IMPOSSIBLE_TO_CREATE_GEOM_SHAPE</source>
      <translation>something was wrong during publishing of GEOM shape.</translation>
    </message>
    <message>
      <source>OBJ_PREFIX</source>
      <translation>HYDRO_</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_GeomObjectDlg</name>
    <message>
      <source>GET_SHAPE_FROM_FILE</source>
      <translation>Get shape from file</translation>
    </message>
    <message>
      <source>IMPORT_OBSTACLE_FROM_FILE</source>
      <translation>Import obstacle</translation>
    </message>
    <message>
      <source>FILE_NAME</source>
      <translation>File name</translation>
    </message>
    <message>
      <source>OBJECT_NAME</source>
      <translation>%1 name</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>MODE</source>
      <translation>Mode</translation>
    </message>
    <message>
      <source>CREATE_NEW</source>
      <translation>Create new</translation>
    </message>
    <message>
      <source>MODIFY</source>
      <translation>Modify</translation>
    </message>
    <message>
      <source>OBJECT_TO_EDIT</source>
      <translation>%1 to edit</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportProfilesOp</name>
    <message>
      <source>IMPORT_PROFILES</source>
      <translation>Import profiles</translation>
    </message>
    <message>
      <source>PROFILE_FILTER</source>
      <translation>All files (*.* *)</translation>
    </message>
    <message>
      <source>BAD_PROFILE_IDS</source>
      <translation>numbers of bad profiles</translation>
    </message>
    <message>
      <source>BAD_IMPORTED_PROFILES_FILES_TLT</source>
      <translation>Profiles import error</translation>
    </message>
    <message>
      <source>NO_ONE_PROFILE_IMPORTED</source>
      <translation>No one profile has been import from selected file(s).</translation>
    </message>
    <message>
      <source>BAD_IMPORTED_PROFILES_FILES</source>
      <translation>The data from following files cannot be completely imported:
%1
</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportPolylineOp</name>
    <message>
      <source>IMPORT_POLYLINE</source>
      <translation>Import Polyline</translation>
    </message>
    <message>
      <source>IMPORT_POLYLINE_USE_NAME_ATTR</source>
      <translation>DBF file contains 'name' field. Would you like to use the value of this field as a name of polyline?</translation>
    </message>
    <message>
      <source>POLYLINE_FILTER</source>
      <translation>Shape files (*.shp);;XY files(*.xy);;XYZ files(*.xyz)</translation>
    </message>
    <message>
      <source>IMPORT_POLYLINE_XY_PART_ONLY</source>
      <translation>Do you wish to import the XY part only? (Click 'Yes' to import as polylineXY and 'No' to import as Polyline3D)</translation>
    </message>
    <message>
      <source>BAD_IMPORTED_POLYLINE_FILES_TLT</source>
      <translation>Polyline import error</translation>
    </message>
    <message>
      <source>NO_ONE_POLYLINE_IMPORTED</source>
      <translation>Polyline cannot be read from seleted file</translation>
    </message>

    <message>
      <source>POLYLINE_IMPORT_FAILED_AS_POLYGON</source>
      <translation>Cannot import content of this file as polygon</translation>
    </message>
    <message>
      <source>POLYLINE_IMPORT_FAILED_AS_POLYLINE</source>
      <translation>Cannot import content of this file as polyline</translation>
    </message>
    <message>
      <source>POLYGON_IMPORTED_AS_POLYLINE</source>
      <translation>Contour of the polygon(s) have been imported as polyline(s)</translation>
    </message>
    <message>
      <source>CANT_OPEN_SHP</source>
      <translation>Cannot open SHP file</translation>
    </message>
    <message>
      <source>CANT_OPEN_SHX</source>
      <translation>Cannot open SHX file</translation>
    </message>
     <message>
      <source>SHAPE_TYPE_IS</source>
      <translation>The shape type of file is </translation>
    </message>


    <message>
      <source>BAD_IMPORTED_POLYLINE_FILES</source>
      <translation>The data from following files cannot be completely imported:
%1
</translation>
    </message>

  </context>

 <context>
    <name>HYDROGUI_ImportLandCoverMapDlg</name>
    <message>
      <source>LANDCOVERMAP_FILTER</source>
      <translation>Shape files (*.shp)</translation>
    </message>
    <message>
      <source>IMPORT_LANDCOVER_MAP_FROM_FILE</source>
      <translation>Import land cover map from file</translation>
    </message>
    <message>
      <source>FILE_NAME</source>
      <translation>File name</translation>
    </message>
    <message>
      <source>LANDCOVERMAP_NAME</source>
      <translation>Land cover map name</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>FOUND_POLYGONS</source>
      <translation>Found polygons:</translation>
    </message>
    <message>
      <source>USE_DBF_AS_ST</source>
      <translation>Use dBase attributes as a Strickler Types</translation>
    </message>
    <message>
      <source>AV_ATTRS</source>
      <translation>Available attributes</translation>
    </message>
    <message>
      <source>CORR</source>
      <translation>Correspondence</translation>
    </message>

    <message>
      <source>TABLE_H1</source>
      <translation>Attribute</translation>
    </message>
    <message>
      <source>TABLE_H2</source>
      <translation>Strickler Type</translation>
    </message>
    <message>
      <source>TABLE_H3</source>
      <translation>Color</translation>
    </message>

    <message>
      <source>LCM_IMPORT_WARNING</source>
      <translation>Import of Land cover map</translation>
    </message>
    <message>
      <source>DBF_LOAD_ERROR</source>
      <translation>Cannot load DBF file</translation>
    </message>
    <message>
      <source>DBF_NB_RECORDS_IS_NULL</source>
      <translation>DBF Warning</translation>
    </message>

    <message>
      <source>FILE_ISNT_CHOSEN</source>
      <translation>File isn't chosen</translation>
    </message>
    <message>
      <source>POLYGONS_ISNT_SELECTED</source>
      <translation>Polygons isn't selected</translation>
    </message>
   <message>
      <source>ATTRS_ISNT_SELECTED</source>
      <translation>Attribute isn't selected</translation>
    </message>

   <message>
      <source>DBF_LOAD_ERR_MESS</source>
      <translation>Cannot open DBF file or it's corrupted</translation>
    </message>

   <message>
      <source>DBF_NB_RECORDS_IS_NULL_MESS</source>
      <translation>Cannot use DBF data - number of records is null</translation>
    </message>


  </context>

  <context>
    <name>HYDROGUI_ImportLandCoverMapOp</name>
    <message>
      <source>IMPORT_LANDCOVER_MAP</source>
      <translation>Import land cover map</translation>
    </message>
    <message>
      <source>DEFAULT_LANDCOVER_MAP_NAME</source>
      <translation>Landcovermap_0</translation>
    </message>
    <message>
      <source>DEF_POLYGON_NAME</source>
      <translation>polygon</translation>
    </message>
    <message>
      <source>LCM_IMPORT_WARNING</source>
      <translation>Import of Land cover map</translation>
    </message>
    <message>
      <source>CANNT_IMPORT_LCM</source>
      <translation>Cannot import land cover map;</translation>
    </message>
    <message>
      <source>CANNT_OPEN_SHP</source>
      <translation>Cannot open SHP file</translation>
    </message>
    <message>
      <source>CANNT_OPEN_SHX</source>
      <translation>Cannot open SHX file</translation>
    </message>
    <message>
      <source>SHP_TYPEFORMAT_MESS</source>
      <translation>The shape type of file is </translation>
    </message>
    <message>
      <source>CANNT_IMPORT_POLYGONS_FROM_SHP</source>
      <translation>Cannot import choosed polygons</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ExportLandCoverMapDlg</name>
    <message>
      <source>EXPORT_LANDCOVERMAP</source>
      <translation>Export land cover map to SHP file</translation>
    </message>
    <message>
      <source>WRITE_ST_AS_ATTRS_TO_DBF</source>
      <translation>Write Strickler Types as attributes values to DBF file</translation>
    </message>
    <message>
      <source>ATTRS_NOT_WRITTEN_TO_DBF</source>
      <translation>Note that in current settings the attributes will NOT be written</translation>
    </message>
    <message>
      <source>LCM_DISCR_LABEL</source>
      <translation>Current Land Cover Map contains at least one non-linear border. It will be exported in the discretization mode</translation>
    </message>
    <message>
      <source>LCM_DEFL_LABEL</source>
      <translation>Enter a deflection value:</translation>
    </message>

  </context>


  <context>
    <name>HYDROGUI_ImportSinusXOp</name>
    <message>
      <source>IMPORT_SINUSX</source>
      <translation>Import from SinusX</translation>
    </message>
    <message>
      <source>SINUSX_FILTER</source>
      <translation>SinusX Files (*.sx)</translation>
    </message>
    <message>
      <source>NO_ONE_ENTITY_IMPORTED</source>
      <translation>Entities cant be read</translation>
    </message>
    <message>
      <source>ENTITIES_TO_IMPORT_FROM_SX</source>
      <translation>SinusX file entities</translation>
    </message>

  </context>

  <context>
    <name>HYDROGUI_ExportSinusXOp</name>
    <message>
      <source>EXPORT_SINUSX</source>
      <translation>Export to SinusX</translation>
    </message>
    <message>
      <source>SINUSX_FILTER</source>
      <translation>SinusX Files (*.sx)</translation>
    </message>
    <message>
      <source>NO_ENTITIES_TO_EXPORT</source>
      <translation>No entities to export</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ExportSinusXDlg</name>
    <message>
      <source>OBJECTS_TO_EXPORT</source>
      <translation>Objects:</translation>
    </message>
    <message>
      <source>EXPORT</source>
      <translation>Export</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ExportFileOp</name>
    <message>
      <source>EXPORT_TO_SHAPE_FILE</source>
      <translation>Export</translation>
    </message>
    <message>
      <source>SHP_FILTER</source>
      <translation>Shape Files (*.shp)</translation>
    </message>
    <message>
      <source>CANNOT_BE_EXPORTED</source>
      <translation>Following entities cannot be exported:</translation>
    </message>
    <message>
      <source>ERROR_POLYLINES_OF_DIFF_KIND</source>
      <translation>Cannot export polylines of different kind</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_GeoreferencementDlg</name>
    <message>
      <source>PROFILES</source>
      <translation>Profiles</translation>
    </message>
    <message>
      <source>ALL_MODE</source>
      <translation>All</translation>
    </message>
    <message>
      <source>SELECTED_MODE</source>
      <translation>Selected</translation>
    </message>
    <message>
      <source>UPDATE_SELECTION</source>
      <translation>Update selection</translation>
    </message>
    <message>
      <source>PROFILE_HEADER</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>XG_HEADER</source>
      <translation>X1</translation>
    </message>
    <message>
      <source>YG_HEADER</source>
      <translation>Y1</translation>
    </message>
    <message>
      <source>XD_HEADER</source>
      <translation>X2</translation>
    </message>
    <message>
      <source>YD_HEADER</source>
      <translation>Y2</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_GeoreferencementOp</name>
    <message>
      <source>PROFILES_GEOREFERENCEMENT</source>
      <translation>Profiles georeferencement</translation>
    </message>
    <message>
      <source>INCOMPLETE_DATA</source>
      <translation>Incomplete data is set for '%1'.</translation>
    </message>
    <message>
      <source>CONFIRMATION</source>
      <translation>Confirmation</translation>
    </message>
    <message>
      <source>CONFIRM_STORE</source>
      <translation>Do you want to store table data in the data model?</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_SetColorOp</name>
    <message>
      <source>SET_COLOR</source>
      <translation>Set color</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ColorDlg</name>
    <message>
      <source>COLOR</source>
      <translation>Color</translation>
    </message>
    <message>
      <source>FILLING_COLOR</source>
      <translation>Filling color</translation>
    </message>
    <message>
      <source>OBJECT_COLOR</source>
      <translation>Object color</translation>
    </message>
    <message>
      <source>TRANSPARENT</source>
      <translation>Transparent</translation>
    </message>
    <message>
      <source>BORDER_COLOR</source>
      <translation>Border</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_SetTransparencyOp</name>
    <message>
      <source>SET_TRANSPARENCY</source>
      <translation>Set transparency</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_TransparencyDlg</name>
    <message>
      <source>TRANSPARENCY</source>
      <translation>Transparency</translation>
    </message>
    <message>
      <source>APPLY_AND_CLOSE</source>
      <translation>Apply and Close</translation>
    </message>
    <message>
      <source>APPLY</source>
      <translation>Apply</translation>
    </message>
    <message>
      <source>CLOSE</source>
      <translation>Close</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_StreamDlg</name>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>STREAM_NAME</source>
      <translation>Stream name</translation>
    </message>
    <message>
      <source>STREAM_PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
    <message>
      <source>STREAM_HYDRAULIC_AXIS</source>
      <translation>Hydraulic axis</translation>
    </message>
    <message>
      <source>STREAM_DDZ</source>
      <translation>Stream ddz</translation>
    </message>
    <message>
      <source>STREAM_SPATIAL_STEP</source>
      <translation>Stream spatial step</translation>
    </message>
    <message>
      <source>VIA_DTM</source>
      <translation>via DTM</translation>
    </message>
    <message>
      <source>VIA_LISM</source>
      <translation>via Linear Interpolation</translation>
    </message>
    <message>
      <source>STREAM_LEFT_BANK</source>
      <translation>Left Bank</translation>
    </message>
    <message>
      <source>STREAM_RIGHT_BANK</source>
      <translation>Right Bank</translation>
    </message>
    <message>
      <source>STREAM_PROFILE_POINTS</source>
      <translation>Number of points on profiles</translation>
    </message>

    <message>
      <source>ADD</source>
      <translation>Add</translation>
    </message>
    <message>
      <source>REMOVE</source>
      <translation>Remove</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_StreamOp</name>
    <message>
      <source>CREATE_STREAM_ERROR</source>
      <translation>Stream creation error</translation>
    </message>
     <message>
      <source>CREATE_STREAM</source>
      <translation>Create stream</translation>
    </message>
    <message>
      <source>EDIT_STREAM</source>
      <translation>Edit stream</translation>
    </message>
    <message>
      <source>DEFAULT_STREAM_NAME</source>
      <translation>Stream</translation>
    </message>
    <message>
      <source>WARNING</source>
      <translation>Warning</translation>
    </message>
    <message>
      <source>IGNORED_PROFILES</source>
      <translation>The following profile(s) will be ignored:</translation>
    </message>
    <message>
      <source>INVALID_PROFILES</source>
      <translation>Invalid profiles:
%1</translation>
    </message>
    <message>
      <source>EXISTING_PROFILES</source>
      <translation>Existing profiles:
%1</translation>
    </message>
    <message>
      <source>NOT_INTERSECTED_PROFILES</source>
      <translation>Not intersected with the hydraulic axis:
%1</translation>
    </message>
    <message>
      <source>AXIS_NOT_DEFINED</source>
      <translation>Hydraulic axis is not defined.</translation>
    </message>
    <message>
      <source>PROFILES_NOT_DEFINED</source>
      <translation>At least 2 profiles should be defined.</translation>
    </message>
    <message>
      <source>CONFIRMATION</source>
      <translation>Confirmation</translation>
    </message>
    <message>
      <source>CONFIRM_AXIS_CHANGE</source>
      <translation>The following profiles don't intersect the axis:
%1

Do you want to remove these profiles and continue?</translation>
    </message>
    <message>
      <source>BAD_SELECTED_POLYLINE_TLT</source>
      <translation>Polyline is not appropriate for channel creation</translation>
    </message>
    <message>
      <source>BAD_SELECTED_POLYLINE_MSG</source>
      <translation>Polyline '%1' can not be used as hydraulic axis for channel.
Polyline should consist from one not closed curve.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ChannelDlg</name>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>CHANNEL_NAME</source>
      <translation>Channel name</translation>
    </message>
    <message>
      <source>CHANNEL_PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
    <message>
      <source>CHANNEL_GUIDE_LINE</source>
      <translation>Guide line</translation>
    </message>
    <message>
      <source>CHANNEL_PROFILE</source>
      <translation>Profile</translation>
    </message>
    <message>
      <source>EQUI_DISTANCE</source>
      <translation>Equidistance</translation>
    </message>
    <message>
      <source>INVERT_DIRECTION</source>
      <translation>Invert direction</translation>
    </message>
    <message>
      <source>VIA_PROFILE</source>
      <translation>via profile</translation>
    </message>
    <message>
      <source>PREDEF</source>
      <translation>Predefined</translation>
    </message>
    <message>
      <source>LC_VALUE</source>
      <translation>LC value</translation>
    </message>
    <message>
      <source>DELTAZ_VALUE</source>
      <translation>DeltaZ value</translation>
    </message>
       <message>
      <source>COTEZ_VALUE</source>
      <translation>CoteZ value</translation>
    </message>

  </context>

  <context>
    <name>HYDROGUI_ChannelOp</name>
    <message>
      <source>CREATE_CHANNEL</source>
      <translation>Create channel</translation>
    </message>
    <message>
      <source>EDIT_CHANNEL</source>
      <translation>Edit channel</translation>
    </message>
    <message>
      <source>DEFAULT_CHANNEL_NAME</source>
      <translation>Channel</translation>
    </message>
    <message>
      <source>GUIDE_LINE_IS_NOT_SELECTED</source>
      <translation>Guide line is not chosen</translation>
    </message>
    <message>
      <source>PROFILE_IS_NOT_SELECTED</source>
      <translation>Profile is not chosen</translation>
    </message>
  </context>
  <context>
    <name>HYDROGUI_DigueDlg</name>
    <message>
      <source>DIGUE_NAME</source>
      <translation>Digue name</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_DigueOp</name>
    <message>
      <source>CREATE_DIGUE</source>
      <translation>Create dyke</translation>
    </message>
    <message>
      <source>EDIT_DIGUE</source>
      <translation>Edit digue</translation>
    </message>
    <message>
      <source>DEFAULT_DIGUE_NAME</source>
      <translation>Digue</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_StricklerTableDlg</name>
    <message>
      <source>IMPORT_STRICKLER_TABLE_FILE</source>
      <translation>Import Strickler table file</translation>
    </message>
    <message>
      <source>EXPORT_STRICKLER_TABLE_FILE</source>
      <translation>Export Strickler table file</translation>
    </message>
    <message>
      <source>FILE_NAME</source>
      <translation>File name</translation>
    </message>
    <message>
      <source>STRICKLER_TABLE_NAME</source>
      <translation>Strickler table name</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>DEFAULT_STRICKLER_TABLE_NAME</source>
      <translation>Strickler table</translation>
    </message>
    <message>
      <source>STRICKLER_TABLE_TABLE</source>
      <translation>Strickler table definition</translation>
    </message>
    <message>
      <source>ADD</source>
      <translation>Add</translation>
    </message>
    <message>
      <source>REMOVE</source>
      <translation>Remove</translation>
    </message>
    <message>
      <source>CLEAR_ALL</source>
      <translation>Clear all</translation>
    </message>
    <message>
      <source>STRICKLER_TYPE</source>
      <translation>Type</translation>
    </message>
    <message>
      <source>STRICKLER_COEFFICIENT</source>
      <translation>Coefficient</translation>
    </message>
    <message>
      <source>STRICKLER_TABLE_ATTR_NAME</source>
      <translation>QGIS attribute name</translation>
    </message>
    <message>
      <source>ATTR_NAME</source>
      <translation>Attribute name</translation>
    </message>
    <message>
      <source>ATTR_VALUE</source>
      <translation>Attr.value</translation>
    </message>
    <message>
      <source>COLOR</source>
      <translation>Color</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_StricklerTableOp</name>
    <message>
      <source>IMPORT_STRICKLER_TABLE</source>
      <translation>Import Strickler table</translation>
    </message>
    <message>
      <source>EXPORT_STRICKLER_TABLE</source>
      <translation>Export Strickler table</translation>
    </message>
    <message>
      <source>EDIT_STRICKLER_TABLE</source>
      <translation>Edit Strickler table</translation>
    </message>
    <message>
      <source>SELECT_STRICKLER_TABLE_FILE</source>
      <translation>The Strickler table file is not chosen</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_LandCoverMapDlg</name>
    <message>
      <source>DEFAULT_LAND_COVER_MAP_NAME</source>
      <translation>Land cover map</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP_NAME</source>
      <translation>Land cover map name</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP_PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP_POLYLINE_FACE</source>
      <translation>Polyline / Face</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP_STRICKLER_TYPE</source>
      <translation>Strickler type</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP_SELECTED_FACES</source>
      <translation>

  This operation is performed on a set of land covers selected in the 3D Viewer.

  Number of selected land covers: </translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_LandCoverMapOp</name>
    <message>
      <source>CREATE_LAND_COVER_MAP</source>
      <translation>Create land cover map</translation>
    </message>
    <message>
      <source>ADD_LAND_COVER</source>
      <translation>Add land cover to the land cover map</translation>
    </message>
    <message>
      <source>REMOVE_LAND_COVER</source>
      <translation>Remove land cover from the land cover map</translation>
    </message>
    <message>
      <source>SPLIT_LAND_COVER</source>
      <translation>Split land cover(s) inside the land cover map</translation>
    </message>
    <message>
      <source>MERGE_LAND_COVER</source>
      <translation>Merge land covers inside the land cover map</translation>
    </message>
    <message>
      <source>CHANGE_LAND_COVER_TYPE</source>
      <translation>Change Strickler type(s) assigned to the land cover(s) inside the land cover map</translation>
    </message>
    <message>
      <source>POLYLINE_FACE_NOT_DEFINED</source>
      <translation>Polyline / Face object should be defined.</translation>
    </message>
    <message>
      <source>STRICKLER_TYPE_NOT_DEFINED</source>
      <translation>Strickler type should be defined.</translation>
    </message>
    <message>
      <source>LAND_COVER_MAP_UNDEFINED</source>
      <translation>Land cover map is undefined.</translation>
    </message>
    <message>
      <source>LAND_COVER_NOT_ADDED</source>
      <translation>Land cover can not be added.</translation>
    </message>
    <message>
      <source>LAND_COVER_NOT_REMOVED</source>
      <translation>Land cover can not be removed.</translation>
    </message>
    <message>
      <source>LAND_COVER_NOT_SPLIT</source>
      <translation>Land cover can not be split.</translation>
    </message>
    <message>
      <source>LAND_COVER_NOT_MERGED</source>
      <translation>Land cover can not be merged.</translation>
    </message>
    <message>
      <source>LAND_COVER_TYPE_NOT_CHANGED</source>
      <translation>Strickler type can not be changed for all selected land covers.</translation>
    </message>
    <message>
      <source>LAND_COVER_OBJECT_CANNOT_BE_CREATED</source>
      <translation>Land Cover object can not be created</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_TranslateObstacleDlg</name>
    <message>
      <source>OBSTACLE_NAME</source>
      <translation>Obstacle name</translation>
    </message>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>TRANSLATION_ARGUMENTS</source>
      <translation>Arguments</translation>
    </message>
    <message>
      <source>DX</source>
      <translation>Dx</translation>
    </message>
    <message>
      <source>DY</source>
      <translation>Dy</translation>
    </message>
    <message>
      <source>DZ</source>
      <translation>Dz</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_TranslateObstacleOp</name>
    <message>
      <source>TRANSLATE_OBSTACLE</source>
      <translation>Translation of an obstacle</translation>
    </message>
  </context>


  <context>
    <name>HYDROGUI_RunBrowser</name>
    <message>
        <source>EXTERNAL_BROWSER_CANNOT_SHOW_PAGE</source>
        <translation>External browser &quot;%1&quot; can not show help page &quot;%2&quot;. You could change it in preferences.</translation>
    </message>
    <message>
        <source>WRN_DEFINE_EXTERNAL_BROWSER</source>
        <translation>External browser is not found. Do you want to define it in preferences?</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ZLevelsDlg</name>
    <message>
        <source>CHANGE_LAYER_ORDER</source>
        <translation>Change layer order</translation>
    </message>
    <message>
        <source>ALL_OBJECTS</source>
        <translation>All objects</translation>
    </message>
    <message>
        <source>ALL_OBJECTS_UNCHECKED_TLT</source>
        <translation>Include hidden objects to the list</translation>
    </message>
    <message>
        <source>ALL_OBJECTS_CHECKED_TLT</source>
        <translation>Exclude hidden objects from the list</translation>
    </message>
    <message>
      <source>APPLY_AND_CLOSE</source>
      <translation>Apply and Close</translation>
    </message>
    <message>
        <source>APPLY</source>
        <translation>Apply</translation>
    </message>
    <message>
        <source>CLOSE</source>
        <translation>Close</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_OrderedListWidget</name>
    <message>
        <source>TOP_TLT</source>
        <translation>Move the item(s) on top</translation>
    </message>
    <message>
        <source>UP_TLT</source>
        <translation>Move the item(s) up</translation>
    </message>
    <message>
        <source>DOWN_TLT</source>
        <translation>Move the item(s) down</translation>
    </message>
    <message>
        <source>BOTTOM_TLT</source>
        <translation>Move the item(s) on bottom</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_PriorityWidget</name>
    <message>
      <source>ADD</source>
      <translation>Add</translation>
    </message>
    <message>
      <source>REMOVE</source>
      <translation>Remove</translation>
    </message>
    <message>
      <source>CLEAR_ALL</source>
      <translation>Clear all</translation>
    </message>
    <message>
      <source>INCORRECT_INPUT</source>
      <translation>Incorrect input</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_PriorityTableModel</name>
    <message>
      <source>LESS</source>
      <translation>&lt;</translation>
    </message>
    <message>
      <source>GREATER</source>
      <translation>&gt;</translation>
    </message>
    <message>
      <source>PRIORITY</source>
      <translation>Priority</translation>
    </message>
    <message>
      <source>ZMIN</source>
      <translation>Zmin</translation>
    </message>
    <message>
      <source>ZMAX</source>
      <translation>Zmax</translation>
    </message>
    <message>
      <source>OBJECT1</source>
      <translation>Object 1</translation>
    </message>
    <message>
      <source>OBJECT2</source>
      <translation>Object 2</translation>
    </message>
    <message>
      <source>BATHYMETRY</source>
      <translation>Bathymetry</translation>
    </message>
    <message>
      <source>ALREADY_EXISTS</source>
      <translation>The selected objects combination already exists.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_RiverBottomDlg</name>
    <message>
      <source>STREAM_OBJECT</source>
      <translation>Stream object</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_RiverBottomOp</name>
    <message>
      <source>FIND_STREAM_BOTTOM</source>
      <translation>Find stream bottom</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ProfileInterpolateDlg</name>
    <message>
      <source>STREAM_OBJECT</source>
      <translation>Stream object</translation>
    </message>
    <message>
      <source>INTERPOLATOR</source>
      <translation>Interpolator</translation>
    </message>
    <message>
      <source>DESCRIPTION</source>
      <translation>Description</translation>
    </message>
    <message>
      <source>PROFILE_1</source>
      <translation>Profile 1</translation>
    </message>
    <message>
      <source>PROFILE_2</source>
      <translation>Profile 2</translation>
    </message>
    <message>
      <source>NUMBER_OF_PROFILES</source>
      <translation>Number of profiles</translation>
    </message>
    <message>
      <source>PARAMETERS</source>
      <translation>Parameters</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ProfileInterpolateOp</name>
    <message>
      <source>PROFILE_INTERPOLATION</source>
      <translation>Profile interpolation</translation>
    </message>
    <message>
      <source>INTERPOLATION_ERROR</source>
      <translation>Interpolation error</translation>
    </message>
    <message>
      <source>CALCULATE_ERROR</source>
      <translation>Can't perform calculation: %1</translation>
    </message>
    <message>
      <source>CANT_GET_STREAM_OBJECT</source>
      <translation>Can't obtain stream oject \"%1\"</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_RecognizeContoursDlg</name>
    <message>
      <source>NAME</source>
      <translation>Name</translation>
    </message>
    <message>
      <source>ORIGINAL_IMAGE</source>
      <translation>Original image</translation>
    </message>
    <message>
      <source>RECOGNIZED_POLYLINES</source>
      <translation>Polylines</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_RecognizeContoursOp</name>
    <message>
      <source>CONTOURS_RECOGNITION</source>
      <translation>Contours recognition</translation>
    </message>
    <message>
      <source>NO_DETECTED_CONTOURS</source>
      <translation>No contours were detected.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_SplitPolylinesOp</name>
    <message>
      <source>SPLIT_POLYLINES</source>
      <translation>Split polylines</translation>
    </message>
    <message>
      <source>SPLIT_POLYLINE_BY_TOOL_WARNING_TITLE</source>
      <translation>Split polyline by tool</translation>
    </message>
    <message>
      <source>SPLIT_POLYLINE_BY_TOOL_WARNING_MSG</source>
      <translation>The split polyline is not intersected by the tool.</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ShowAttrPolyOp</name>
    <message>
      <source>SHOWATTR_POLYLINE</source>
      <translation>DBF attributes of polyline</translation>
    </message>
    <message>
      <source>SHOWATTR_POLY_NO_ATTR</source>
      <translation>This polyline does not contain DBF information</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_SplitPolylinesDlg</name>
    <message>
      <source>POLYLINES</source>
      <translation>Polylines</translation>
    </message>
    <message>
      <source>POINT</source>
      <translation>Point</translation>
    </message>
    <message>
      <source>POLYLINE</source>
      <translation>Polyline</translation>
    </message>
    <message>
      <source>TOOL_POLYLINE</source>
      <translation>Tool polyline</translation>
    </message>
    <message>
      <source>SPLIT_POLYLINES</source>
      <translation>Split polylines</translation>
    </message>
    <message>
      <source>BY_POINT</source>
      <translation>By point</translation>
    </message>
    <message>
      <source>BY_TOOL</source>
      <translation>By tool</translation>
    </message>
    <message>
      <source>COMPLETE_SPLIT</source>
      <translation>Complete split</translation>
    </message>
    <message>
      <source>RESULT_NAME</source>
      <translation>Result name prefix:</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_MergePolylinesDlg</name>
    <message>
      <source>POLYLINES</source>
      <translation>Polylines</translation>
    </message>
    <message>
      <source>MERGE_POLYLINES</source>
      <translation>Merge polylines</translation>
    </message>
    <message>
      <source>RESULT_NAME</source>
      <translation>Result name:</translation>
    </message>
    <message>
      <source>IS_CONNECT</source>
      <translation>Connect by a new segment</translation>
    </message>
  </context>
  <context>
    <name>HYDROGUI_MergePolylinesOp</name>
    <message>
      <source>MERGE_POLYLINES</source>
      <translation>Merge polylines</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ObjListBox</name>
    <message>
      <source>INCLUDE</source>
      <translation>Include</translation>
    </message>
    <message>
      <source>EXCLUDE</source>
      <translation>Exclude</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_PolylineExtractionOp</name>
    <message>
      <source>POLYLINE_EXTRACTION</source>
      <translation>Polyline extraction</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_PolylineStyleDlg</name>
    <message>
      <source>POLYLINE_STYLE</source>
      <translation>Polyline style</translation>
    </message>
    <message>
      <source>PREF_POLYLINE_ARROW</source>
      <translation>Polyline arrow</translation>
    </message>
    <message>
      <source>PREF_POLYLINE_ARROW_SIZE</source>
      <translation>Polyline arrow size</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportBCPolygonOp</name>
     <message>
      <source>BC_POLYGON_FILTER</source>
      <translation>Shape files (*.shp)</translation>
    </message>
  </context>
  <context>
    <name>HYDROGUI_SetBoundaryTypePolygonOp</name>
     <message>
      <source>SET_BOUNDARY_TYPE_POLYGON</source>
      <translation>Set Boundary Type</translation>
    </message>
  </context>
  <context>
    <name>HYDROGUI_SetBoundaryTypePolygonDlg</name>
     <message>
      <source>BOUNDARY_TYPE</source>
      <translation>Boundary Type: </translation>
    </message>
     <message>
      <source>CUT_TOOL</source>
      <translation>Cut Tool</translation>
    </message>
     <message>
      <source>INCLUDE_TOOL</source>
      <translation>Include Tool</translation>
    </message>
     <message>
      <source>SELECTION_TOOL</source>
      <translation>Selection tool</translation>
    </message>
  </context>
  <context>
    <name>HYDROGUI_MeasurementToolDlg</name>
     <message>
      <source>LINEAR_DISTANCE</source>
      <translation>Linear distance</translation>
    </message>
     <message>
      <source>POLYLINE_DISTANCE</source>
      <translation>Distance on polyline:</translation>
    </message>
     <message>
      <source>TOTAL_DST</source>
      <translation>Distance:</translation>
    </message>
     <message>
      <source>CLEAR</source>
      <translation>Clear</translation>
    </message>
      <message>
      <source>CLOSE</source>
      <translation>Close</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_MeasurementToolOp</name>
     <message>
      <source>MEASUREMENT_TOOL</source>
      <translation>Measurement Tool</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ImportSinusXDlg</name>
     <message>
      <source>ENTITIES_TO_IMPORT</source>
      <translation>The file contains the entities listed below. Check the entities which you wish to import and their importing type</translation>
    </message>
     <message>
      <source>NAME</source>
      <translation>Entity name</translation>
    </message>
     <message>
      <source>TYPE</source>
      <translation>Type</translation>
    </message>
     <message>
      <source>AS_POLYXY</source>
      <translation>As PolylineXY</translation>
    </message>
     <message>
      <source>AS_BATHY</source>
      <translation>As bathymetry</translation>
    </message>
     <message>
      <source>AS_PROFILE</source>
      <translation>As profile</translation>
    </message>
     <message>
      <source>XYZ_CURVE</source>
      <translation>XYZ curve</translation>
    </message>
     <message>
      <source>XYZ_PROFILE</source>
      <translation>XYZ profile</translation>
    </message>
     <message>
      <source>ISOCONTOUR</source>
      <translation>Isocontour</translation>
    </message>
     <message>
      <source>SCATTER</source>
      <translation>Scatter plot</translation>
    </message>
     <message>
      <source>OK</source>
      <translation>Ok</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_SIProgressIndicator</name>
     <message>
      <source>STRICKLER_INTERPOLATION_TLT</source>
      <translation>Strickler coefficient interpolation</translation>
     </message>
     <message>
      <source>CANCELLING</source>
      <translation>Cancelling...</translation>
    </message>
  </context>

  <context>
    <name>HYDROGUI_ZIProgressIndicator</name>
     <message>
      <source>BATHYMETRY_INTERPOLATION_TLT</source>
      <translation>Bathymetry interpolation</translation>
     </message>
     <message>
      <source>OBJECT</source>
      <translation>Object: %1</translation>
     </message>
     <message>
      <source>QUADTREE_STAGE</source>
      <translation>Quadtree calculation</translation>
     </message>
     <message>
      <source>TRIANGULATION_STAGE</source>
      <translation>Triangulation of the point cloud</translation>
     </message>
     <message>
      <source>EXPLORATION_STAGE</source>
      <translation>Exploration of the mesh nodes</translation>
     </message>
     <message>
      <source>PENDING</source>
      <translation>Pending</translation>
     </message>
     <message>
      <source>IN_PROGRESS</source>
      <translation>In progress</translation>
     </message>
     <message>
      <source>COMPLETED</source>
      <translation>Completed</translation>
     </message>
     <message>
      <source>CANCELLING</source>
      <translation>Cancelling...</translation>
     </message>
  </context>


 <context>
    <name>HYDROGUI_ImportPolylineDlg</name>
     <message>
      <source>POLYLINES_3D_IN_FILES</source>
      <translation>The shp file contains polyline3D objects (ARCZ).
They have been imported as polylineXY only,
but can be imported with Z part (additional profile and bathymetry).
If checkbox of corresponding polyline in the table below is checked
then it will be imported with Z part, otherwise as PolylineXY only</translation>
    </message>
     <message>
      <source>POLYLINE_NAME</source>
      <translation>Polyline name</translation>
    </message>
     <message>
      <source>FULL_IMPORT</source>
      <translation>Full import</translation>
    </message>
     <message>
      <source>CHECK_ALL</source>
      <translation>Check all</translation>
    </message>
    <message>
      <source>UNCHECK_ALL</source>
      <translation>Uncheck all</translation>
    </message>
    <message>
      <source>OK</source>
      <translation>Ok</translation>
    </message>

  </context>


</TS>

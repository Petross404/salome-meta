// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ProfileDlg.h"

#include "HYDROGUI_Tool.h"
#include "HYDROGUI_AISTrihedron.h"
#ifndef TEST_MODE
#include <HYDROGUI_Module.h>
#include "HYDROGUI_Tool2.h"
#endif

#include <CurveCreator_Widget.h>
#include <CurveCreator_ICurve.hxx>
#include <CurveCreator_Utils.hxx>
#include <HYDROGUI_CurveCreatorProfile.h>
#include <HYDROData_Tool.h>
#include <CurveCreator_Displayer.hxx>

#include <OCCViewer_ViewPort3d.h>
#include <OCCViewer_Utilities.h>
#include <OCCViewer_ViewManager.h>
#include <OCCViewer_ViewFrame.h>

#ifndef TEST_MODE
#include <LightApp_Application.h>
#endif
#include <SUIT_Desktop.h>
#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMouseEvent>
#include <QSplitter>
#include <QSettings>
#include <QListWidget>
#include <QPushButton>
#include <SUIT_MessageBox.h>
#include <QColorDialog>

const QString splitter_key = "HYDROGUI_ProfileDlg::splitter";

HYDROGUI_ProfileDlg::HYDROGUI_ProfileDlg( HYDROGUI_Module* theModule, const QString& theTitle, 
  bool theIsEdit )
: HYDROGUI_ViewerDlg( theModule, theTitle, true, false ),
  myName( NULL ), myProfileNames (NULL), myProfilesPointer (NULL),
  myIsEdit (theIsEdit)
{
  QFrame* name_frame = new QFrame( mainFrame() );
  QHBoxLayout* name_layout = new QHBoxLayout( name_frame );
  name_layout->setMargin( 0 );
  QLabel* aNameLabel = new QLabel(tr("PROFILE_NAME_TLT"), this);
  name_layout->addWidget(aNameLabel);
  if (!theIsEdit)
  {
    myName = new QLineEdit(this);
    name_layout->addWidget(myName);
  }
  else
  {
    myProfileNames = new QListWidget(this);
    myProfileNames->setSelectionMode(QAbstractItemView::SingleSelection);
    name_layout->addWidget(myProfileNames);

    QVBoxLayout* btn_layout = new QVBoxLayout( name_frame );
    btn_layout->setAlignment(Qt::AlignTop);

    myAddProfBtn = new QPushButton(tr("ADD_PROFILES"), this);    
    myRemProfBtn = new QPushButton(tr("REMOVE_PROFILE"), this);
    mySetColorProfBtn = new QPushButton(tr("SETCOLOR_PROFILE"), this);
    btn_layout->addWidget(myAddProfBtn);
    btn_layout->addWidget(myRemProfBtn);
    btn_layout->addWidget(mySetColorProfBtn);

    name_layout->addLayout(btn_layout);

  }

  insertWidget( name_frame, 0, 0 );

  int anActionFlags = 
    CurveCreator_Widget::DisableNewSection | CurveCreator_Widget::DisableDetectionMode |
    CurveCreator_Widget::DisableClosedSection | CurveCreator_Widget::DisableSetColor;
  QStringList aCoordTitles;
  aCoordTitles << tr( "U_TITLE" ) << tr( "Z_TITLE" );
  myEditorWidget = new CurveCreator_Widget( this, NULL, anActionFlags, aCoordTitles );
  insertWidget( myEditorWidget, 1, 1 );

  myAddElementBox = new QGroupBox( tr( "ADD_ELEMENT" ), this );
  insertWidget( myAddElementBox, 2, 1 );

  QBoxLayout* anAddElementLayout = new QVBoxLayout( myAddElementBox );
  anAddElementLayout->setMargin( 0 );
  anAddElementLayout->setSpacing( 5 );

  myEditorWidget->setOCCViewer( viewer() );

  connect( myEditorWidget, SIGNAL( selectionChanged() ), this, SIGNAL( selectionChanged() ) );
  connect( myEditorWidget, SIGNAL( subOperationStarted(QWidget*, bool) ), this, SLOT( processStartedSubOperation(QWidget*, bool) ) );
  connect( myEditorWidget, SIGNAL( subOperationFinished(QWidget*) ), this, SLOT( processFinishedSubOperation(QWidget*) ) );
  if (myIsEdit)
  {
    connect( myProfileNames, SIGNAL( currentTextChanged(QString)), SLOT(ProfileNameChanged(QString)) );
    connect( myProfileNames, SIGNAL( itemSelectionChanged()), this, SLOT( onProfileIndexChanged()));
    connect( myProfileNames, SIGNAL( itemChanged(QListWidgetItem*)), this, SLOT( onProfileNameChanged(QListWidgetItem*)));
    connect( myAddProfBtn, SIGNAL( clicked(bool)), this, SLOT( onAddBtnPressed(bool)));
    connect( myRemProfBtn, SIGNAL( clicked(bool)), this, SLOT( onRemoveBtnPressed(bool)));
    connect( mySetColorProfBtn, SIGNAL( clicked(bool)), this, SLOT( onSetColorBtnPressed(bool)));
  }
  myAddElementBox->hide();

  QList<int> sizes;
  sizes.append( 25 );
  sizes.append( 100 );
  sizes.append( 100 );
  sizes.append( 200 );
  sizes.append( 25 );
  splitter()->setSizes( sizes );

#ifndef TEST_MODE
  QSettings settings;
  splitter()->restoreState( settings.value( splitter_key ).toByteArray() );
#endif
}

HYDROGUI_ProfileDlg::~HYDROGUI_ProfileDlg()
{
#ifndef TEST_MODE
  QSettings settings;
  settings.setValue( splitter_key, splitter()->saveState() );
#endif
}

void HYDROGUI_ProfileDlg::reset()
{
  myEditorWidget->reset();
  myEditorWidget->setActionMode( CurveCreator_Widget::AdditionMode );
  viewer()->setTrihedronShown( false ); // Issue #548
  viewer()->setStaticTrihedronDisplayed(false);
  if (myProfileNames)
    myProfileNames->clear();
}

void HYDROGUI_ProfileDlg::setProfileName( const QString& theName )
{
  if (myIsEdit)
    return;
  myName->setText(theName);
}

void HYDROGUI_ProfileDlg::eraseProfile( int index )
{
  myProfileNames->takeItem(index);
  if (myProfileNames->count() == 1)
    myRemProfBtn->setEnabled(false);
}

void HYDROGUI_ProfileDlg::addProfileName( const QString& theName, const QColor& theColor )
{
  if (!myIsEdit)
    return;
  myProfileNames->blockSignals(true);
  myProfileNames->addItem(theName);
  int count = myProfileNames->count();
  QListWidgetItem* anItem = myProfileNames->item(count - 1);
  anItem->setFlags (anItem->flags () | Qt::ItemIsEditable);
  QPixmap SPixmap(16, 16);
  SPixmap.fill(theColor);
  QIcon SIcon(SPixmap);
  anItem->setIcon( SIcon );
  if (count == 1)
    anItem->setSelected(true);
  if (count == 1 && myIsEdit)
    myRemProfBtn->setEnabled(false);
  if (count > 1)
    myRemProfBtn->setEnabled(true);

  myProfileNames->blockSignals(false);
}

QStringList HYDROGUI_ProfileDlg::getProfileNames() const
{
  QStringList aProfNames;
  if (!myIsEdit)
    aProfNames << myName->text();
  else
    for (int i = 0; i < myProfileNames->count(); i++)
      aProfNames <<  myProfileNames->item(i)->text();
  return aProfNames;
}

void HYDROGUI_ProfileDlg::setProfile( CurveCreator_ICurve* theProfile )
{
  myEditorWidget->setCurve( theProfile );

  // select the single section by default
  QList<int> aSections;
  aSections << 0;
  myEditorWidget->setSelectedSections( aSections );
}

void HYDROGUI_ProfileDlg::setProfilesPointer(std::vector<HYDROGUI_CurveCreatorProfile*>* theProfilesPointer)
{
  myProfilesPointer = theProfilesPointer;
}

QList<int> HYDROGUI_ProfileDlg::getSelectedSections()
{
  return myEditorWidget->getSelectedSections();
}

void HYDROGUI_ProfileDlg::switchToFirstProfile()
{
  emit onProfileIndexChanged();
}

/**
 * Redirect the delete action to editor widget
 */
void HYDROGUI_ProfileDlg::deleteSelected()
{
  myEditorWidget->removeSelected();
}

/**
 * Checks whether there are some to delete
 */
bool HYDROGUI_ProfileDlg::deleteEnabled()
{
  return myEditorWidget->removeEnabled();
}

void HYDROGUI_ProfileDlg::processStartedSubOperation( QWidget* theWidget, bool theIsEdit )
{
  myEditorWidget->setEnabled( false );

  myAddElementBox->setTitle( theIsEdit ? tr( "EDIT_ELEMENT" ) : tr( "ADD_ELEMENT" ) );
  QBoxLayout* anAddElementLayout = dynamic_cast<QBoxLayout*>( myAddElementBox->layout() );
  anAddElementLayout->addWidget( theWidget );

  theWidget->show();
  myAddElementBox->show();
}

void HYDROGUI_ProfileDlg::processFinishedSubOperation( QWidget* theWidget )
{
  myEditorWidget->setEnabled( true );

  QBoxLayout* anAddElementLayout = dynamic_cast<QBoxLayout*>( myAddElementBox->layout() );
  anAddElementLayout->removeWidget( theWidget );

  theWidget->hide();
  myAddElementBox->hide();
}

void HYDROGUI_ProfileDlg::ProfileNameChanged(QString text)
{
  myCurrentName = text;
}

void HYDROGUI_ProfileDlg::onProfileIndexChanged()
{
  int theIndex = GetProfileSelectionIndex();
  if (theIndex > -1)
    SwitchToProfile(theIndex);
}

int HYDROGUI_ProfileDlg::GetProfileSelectionIndex()
{
  if (!myProfilesPointer)
    return -1;
  QModelIndexList MI = myProfileNames->selectionModel()->selectedIndexes();
  if (MI.size() != 1)
    return -1;
  return MI.first().row();
}

void HYDROGUI_ProfileDlg::BlockProfileNameSignals(bool state)
{
  myProfileNames->blockSignals(state);
}

void HYDROGUI_ProfileDlg::SwitchToProfile(int theIndex)
{
  myEditorWidget->setCurve(NULL);
  myEditorWidget->reset();
  myEditorWidget->setActionMode( CurveCreator_Widget::AdditionMode );
  myEditorWidget->setSelectedSections(QList<int>());
  setProfile( (*myProfilesPointer)[theIndex] );
  for (int i = 0; i < myProfilesPointer->size(); i++)
  {
    HYDROGUI_CurveCreatorProfile* aCurve = (*myProfilesPointer)[i];
    if (i == theIndex)
    {
      aCurve->myLineWidth = 3;
      Handle(AIS_InteractiveObject) anAISObject = aCurve->getAISObject();
      if (anAISObject)
        anAISObject->SetWidth(3);
    }
    else
    {
      aCurve->myLineWidth = 1;
      Handle(AIS_InteractiveObject) anAISObject = aCurve->getAISObject();
      if (anAISObject)
        anAISObject->SetWidth(1);
    }
  }

  if( myProfilesPointer && 
      myProfilesPointer->size()>0 && 
      myProfilesPointer->at(0) && 
      myProfilesPointer->at(0)->getDisplayer() )
    myProfilesPointer->at(0)->getDisplayer()->Update();
}

void HYDROGUI_ProfileDlg::onAddBtnPressed(bool)
{ 
  emit AddProfiles();
}

void HYDROGUI_ProfileDlg::onRemoveBtnPressed(bool)
{
  int theIndex = GetProfileSelectionIndex();
  if (theIndex > -1)
    emit RemoveProfile(theIndex);
}

void HYDROGUI_ProfileDlg::onSetColorBtnPressed(bool)
{
  int theIndex = GetProfileSelectionIndex();
  if (theIndex < 0)
    return;

  HYDROGUI_CurveCreatorProfile* aCurve = (*myProfilesPointer)[theIndex];
  
  QColor aCurrentColor = HYDROData_Tool::toQtColor(/*aCurve->myCurveColor*/aCurve->getColorSection(0));
  QColor newQCurCol = QColorDialog::getColor( aCurrentColor, this );
  if( !newQCurCol.isValid() )
    return;

  Quantity_Color newOCCCol = HYDROData_Tool::toOccColor(newQCurCol);
  aCurve->setColorSectionInternal(0, newOCCCol);
  Handle(AIS_InteractiveObject) anAISObject = aCurve->getAISObject();
  if (anAISObject)
  {
    //color points of shape in the same color as edges
    Handle(Prs3d_PointAspect) anAspect = anAISObject->Attributes()->PointAspect();
    anAspect->SetColor(newOCCCol);
    anAISObject->Attributes()->SetPointAspect( anAspect );
  }

  QListWidgetItem* item = myProfileNames->item(theIndex);
  QPixmap SPixmap(16, 16);
  SPixmap.fill(newQCurCol);
  QIcon SIcon(SPixmap);
  item->setIcon( SIcon );

  if( myProfilesPointer && 
    myProfilesPointer->size()>0 && 
    myProfilesPointer->at(0) && 
    myProfilesPointer->at(0)->getDisplayer() )
    myProfilesPointer->at(0)->getDisplayer()->Update();
}

void HYDROGUI_ProfileDlg::onProfileNameChanged(QListWidgetItem* item)
{
  int ind = GetProfileSelectionIndex();
  if (ind > -1)
  {
    int count = myProfileNames->count();
    QSet<QString> names;
    for (int i = 0; i < count; i++)
    {
      QListWidgetItem* citem = myProfileNames->item(i);
      if (item!=citem)
        names << citem->text();
    }
    QString curText = item->text();
    myProfileNames->blockSignals(true);
    if (names.contains(curText))
    {
      QString mes = tr( "PROFILE_ALREADY_EXISTS" );
      QString title = tr( "PROFILEDLG_WARNING" );
#ifndef TEST_MODE
      SUIT_MessageBox::warning( module()->getApp()->desktop(), title, mes );
#endif
      item->setText(myCurrentName);
    }
    myProfileNames->blockSignals(false);
  }
}
/*
void HYDROGUI_ProfileDlg::SetSingleProfileMode(bool SingleMode)
{
  mySingleProfileMode = SingleMode;
}

bool HYDROGUI_ProfileDlg::GetSingleProfileMode() const
{
  return mySingleProfileMode;
}*/

Handle(AIS_Trihedron) HYDROGUI_ProfileDlg::trihedron()
{
  SUIT_ResourceMgr* aResMgr = SUIT_Session::session()->resourceMgr();
  Handle(AIS_Trihedron) aTrihedron =
    HYDROGUI_AISTrihedron::createTrihedron( aResMgr->doubleValue( "3DViewer", "trihedron_size", viewer()->trihedronSize() ) );
  return aTrihedron;
}

// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_CALCULATIONDLG_H
#define HYDROGUI_CALCULATIONDLG_H

#include "HYDROGUI_Wizard.h"
#include <HYDROData_CalculationCase.h>
#include <HYDROData_LandCoverMap.h>

class HYDROGUI_ObjSelector;
class HYDROGUI_DataBrowser;
class HYDROGUI_NameValidator;
class HYDROGUI_Zone;
class HYDROGUI_OrderedListWidget;
class HYDROGUI_PriorityWidget;

class SUIT_DataObject;

class QButtonGroup;
class QGroupBox;
class QLineEdit;
class QListWidget;
class QComboBox;
class QLabel;
class QSplitter;
class QStringList;


class HYDROGUI_CalculationDlg : public HYDROGUI_Wizard
{
  Q_OBJECT

public:
  HYDROGUI_CalculationDlg( HYDROGUI_Module* theModule, const QString& theTitle, bool IsComplete );
  virtual ~HYDROGUI_CalculationDlg();

  void                       reset();

  int                        getMode() const;
  
  void                       setObjectName( const QString& theName );
  QString                    getObjectName() const;

  void                       setEditedObject( const Handle(HYDROData_CalculationCase) theCase );

  void                       setAllGeomObjects( const QStringList& theObjects, const QStringList& theObjectsEntries );
  QStringList                getAllGeomObjects() const;
  void                       setPolylineNames( const QStringList& theObjects, const QStringList& theObjectsEntries );
  void                       setLandCoverMapsNames( const QStringList& theObjects, const QStringList& theObjectsEntries );
  void                       setStricklerTableNames( const QStringList& theObjects, const QStringList& theObjectsEntries );
  QStringList                getSelectedGeomObjects() const;
  QStringList                getSelectedAvailableGeomObjects() const;
  QStringList                getSelectedGroups() const;
  QStringList                getSelectedAvailableGroups() const;
  QStringList                getSelectedBoundaryPolygons() const;
  QStringList                getSelectedISBoundaryPolygons() const;
  QStringList                getSelectedAvailableBoundaryPolygons() const;
  HYDROGUI_Zone*             getCurrentZone() const;
  
  void                       setAvailableGroups( const QStringList& );

  void                       setAvailableBoundaryPolygons( const QStringList&, const QVector<int>& );

  void                       setEditZonesEnabled( const bool theIsEnabled );

  HYDROData_ListOfRules      getRules() const;
  void                       setRules( const HYDROData_ListOfRules& theRules ) const;

  QList<Handle(HYDROData_Entity)> getGeometryObjects(bool GeomObjOnly = true);

public slots:
  void                       setMode( int theMode );
  void                       setGeomOrderingEnabled( int enabled );

  void                       setBoundary( const QString& theObjName );
  void                       includeGeomObjects( const QStringList& theObjects );
  void                       excludeGeomObjects( const QStringList& theObjects );
  void                       includeGroups( const QStringList& theObjects );
  void                       excludeGroups( const QStringList& theObjects );

  void                       hideAvailableGeomObjects( const QStringList& theObjects );
  void                       setAlreadyAddedGeomObjects( const QStringList& theObjects ); 
  QStringList                getAlreadyAddedGeomObjects();


  void                       includeBoundaryPolygons( const QStringList& theObjects );
  void                       includeISBoundaryPolygons( const QStringList& theObjects );

  void                       excludeBoundaryPolygons( const QStringList& theObjects );
  void                       excludeISBoundaryPolygons( const QStringList& theObjects );


  void                       onEmptyName();
  void                       onAlreadyExists( QString theName );
  void                       refreshZonesBrowser();
  void                       onDataChanged();
  void                       onOrderChanged();
  void                       onRuleChanged();

  void                       setStricklerTable( const QString& theStricklerTableName, bool theBlockSignals = true );
  void                       setLandCoverMap( const QString& theLandCoverMapName, bool theBlockSignals = true );
  
  /**
   * Process items selection: hide/show bathymetry merge type selector.
   */
  void                       onSelected( SUIT_DataObject* theObject );
  /**
   * Process merge type selection: set the selected bathymetry merge type for the currently selected zone.
   */
  void                       onMergeTypeSelected( int theIndex );
  /**
   * Process zones moving. Create a new region with dropped zones or add to existing one.
   */
  void                       onZonesDropped( const QList<SUIT_DataObject*>& theList, 
    SUIT_DataObject* theTargetParent, int theTargetRow, Qt::DropAction theDropAction );

signals:
  void                       changeMode( int theMode );  

  void                       addObjects();
  void                       removeObjects();
  void                       objectsSelected();
  void                       orderChanged( bool& isConfirmed );
  void                       ruleChanged( bool& isConfirmed );

  void                       addGroups();
  void                       removeGroups();
  void                       groupsSelected();

  void                       addBoundaryPolygons();
  void                       removeBoundaryPolygons();

  void                       boundarySelected( const QString & theObjName );
  void                       setMergeType( int theMergeType, QString& theBathymetryName );
  void                       createRegion( const QList<SUIT_DataObject*>& theZonesList );
  void                       moveZones( SUIT_DataObject* theRegion, const QList<SUIT_DataObject*>& theZonesList );
  void                       clickedInZonesBrowser( SUIT_DataObject* );

  void                       landCoverMapSelected( const QString & theObjName );

  void                       StricklerTableSelected( const QString & theObjName );

  void                       regenerateColors();

protected:

  virtual bool               acceptCurrent() const;

protected slots:
  void OnNewRegion();

private:

  
  QWizardPage*               createObjectsPage();
  QWizardPage*               createGroupsPage();
  QWizardPage*               createBoundaryPolygonsPage();

  QWizardPage*               createLandCoverMapPage();
  QWizardPage*               createZonesPage();
  
  QSplitter*                 mySplitter;
  
  QGroupBox*                 myObjectNameGroup;
  QLineEdit*                 myObjectName;
  HYDROGUI_NameValidator*    myValidator;

  QComboBox*                 myPolylineName;
  QComboBox*                 myLandCoverMapName;
  QComboBox*                 myStricklerTableName;

  QButtonGroup*              myModeButtons;

  QListWidget*               myAvailableGeomObjects;
  HYDROGUI_OrderedListWidget* myGeomObjects;

  QListWidget*               myAlreadyAddedGeomObjects;


  HYDROGUI_PriorityWidget*   myPriorityWidget;

  QListWidget*               myAvailableGroups;
  QListWidget*               myGroups;

  QListWidget*               myAvailableBoundaryPolygons;
  QListWidget*               myBoundaryPolygons;
  QListWidget*               myISBoundaryPolygons;


  HYDROGUI_DataBrowser*      myBrowser;
  Handle(HYDROData_CalculationCase) myEditedObject;
  QComboBox*                 myBathymetryChoice;
  QLabel*                    myBathymetryLabel;
  HYDROGUI_Zone*             myCurrentZone;

  bool                       myIsComplete;
};

#endif


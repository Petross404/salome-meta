// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_ZLevelsOp.h"

#include "HYDROGUI_ZLevelsDlg.h"
#include "HYDROGUI_DataModel.h"
#include "HYDROGUI_Module.h"
#include "HYDROGUI_Tool2.h"
#include "HYDROGUI_UpdateFlags.h"
#include "HYDROGUI_ListSelector.h"

#include <HYDROData_Entity.h>

#include <LightApp_Application.h>
#include <LightApp_UpdateFlags.h>

#include <SUIT_Desktop.h>

/**
  Constructor.
  @param theModule the module
*/
HYDROGUI_ZLevelsOp::HYDROGUI_ZLevelsOp( HYDROGUI_Module* theModule )
: HYDROGUI_Operation( theModule ),
  myDlg( NULL )
{
  setName( tr( "SET_Z_LEVELS" ) );
}

/**
  Destructor.
*/
HYDROGUI_ZLevelsOp::~HYDROGUI_ZLevelsOp()
{
}

/**
*/
void HYDROGUI_ZLevelsOp::startOperation()
{
  HYDROGUI_Operation::startOperation();

  // Prepare the list of objects
  HYDROGUI_ListModel::Object2VisibleList anObject2VisibleList;

  // get the document
  Handle(HYDROData_Document) aDoc = doc();
  if( !aDoc.IsNull() ) {
    // get active OCC view id
    size_t anActiveOCCViewId = HYDROGUI_Tool::GetActiveOCCViewId( module() );

    // get objects list
    HYDROData_SequenceOfObjects aSeqOfObjects = aDoc->GetObjectsLayerOrder( Standard_True );
    HYDROData_SequenceOfObjects::Iterator anIter( aSeqOfObjects );
    for ( ; anIter.More(); anIter.Next() ) {
      Handle(HYDROData_Entity) anObject = anIter.Value();
      if ( !anObject.IsNull() ) {
        bool isVisible = module()->isObjectVisible( anActiveOCCViewId, anObject );
        anObject2VisibleList << HYDROGUI_ListModel::Object2Visible( anObject, isVisible );
      }
    }
  }

  // Show the dialog
  if ( !myDlg ) {
    myDlg = new HYDROGUI_ZLevelsDlg( module()->getApp()->desktop(), module() );
	connect( myDlg, SIGNAL( applyOrderAndClose() ), this, SLOT( onApplyAndClose() ) );
    connect( myDlg, SIGNAL( applyOrder() ), this, SLOT( onApply() ) );
    connect( myDlg, SIGNAL( rejected() ), this, SLOT( onCancel() ) );
  }
  myDlg->setObjects( anObject2VisibleList );

  myDlg->exec();
}

/**
*/
bool HYDROGUI_ZLevelsOp::processApply( int& theUpdateFlags,
                                       QString& theErrorMsg,
                                       QStringList& theBrowseObjectsEntries )
{
  bool aRes = false;

  if ( myDlg ) {
    Handle(HYDROData_Document) aDoc = doc();
    if( !aDoc.IsNull() ) {
      HYDROGUI_ListModel::ObjectList anObjects = myDlg->getObjects();
      HYDROData_SequenceOfObjects anOrderedObjects;
      foreach ( const Handle(HYDROData_Entity) anObject, anObjects ) {
        anOrderedObjects.Append( anObject );
      }

      aDoc->SetObjectsLayerOrder( anOrderedObjects );

      theUpdateFlags = UF_Model | UF_OCCViewer;
      aRes = true;
    }
  }

  return aRes;
}

/**
*/
bool HYDROGUI_ZLevelsOp::isGranted() const
{
  return true;
}

/**
*/
void HYDROGUI_ZLevelsOp::processCancel()
{
  // Delete the dialog
  if ( myDlg ) {
    delete myDlg;
    myDlg = 0;
  }
}

void HYDROGUI_ZLevelsOp::onApplyAndClose()
{
  HYDROGUI_Operation::onApplyAndClose();
  if ( myDlg )
    myDlg->reject();
}

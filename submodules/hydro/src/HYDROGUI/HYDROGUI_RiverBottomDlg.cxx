// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "HYDROGUI_RiverBottomDlg.h"

#include "HYDROGUI_Tool.h"
#include "HYDROGUI_ObjComboBox.h"

#include <HYDROData_Stream.h>
#include <HYDROData_Polyline3D.h>

#include <QLabel>
#include <QLayout>
#include <QComboBox>
#include <QGroupBox>

HYDROGUI_RiverBottomDlg::HYDROGUI_RiverBottomDlg( HYDROGUI_Module* theModule, const QString& theTitle )
    : HYDROGUI_InputPanel( theModule, theTitle ),
    HYDROGUI_ObjComboBoxFilter()
{
    // Channel name
    QGroupBox* group = new QGroupBox( mainFrame() );
    QBoxLayout* base = new QVBoxLayout( group );

    base->addWidget( myRivers = new HYDROGUI_ObjComboBox( theModule, tr( "STREAM_OBJECT" ), KIND_STREAM, group ) );
    myRivers->setObjectFilter( this );

    addWidget( group );

    addStretch();

    connect( myRivers, SIGNAL( objectSelected( const QString& ) ), this, SLOT( onRiverChanged( const QString& ) ) );

    updateState();
}

HYDROGUI_RiverBottomDlg::~HYDROGUI_RiverBottomDlg()
{
}

void HYDROGUI_RiverBottomDlg::reset()
{
    bool isBlocked = blockSignals( true );

    myRivers->reset();

    blockSignals( isBlocked );

    updateState();
}

QString HYDROGUI_RiverBottomDlg::getRiverName() const
{
    return myRivers->selectedObject();
}

void HYDROGUI_RiverBottomDlg::setRiverName( const QString& theName )
{
    myRivers->setSelectedObject( theName );
}

bool HYDROGUI_RiverBottomDlg::isOk( const Handle(HYDROData_Entity)& theEntity ) const
{
    Handle(HYDROData_Stream) aStream = Handle(HYDROData_Stream)::DownCast(theEntity);
    return !aStream.IsNull() && aStream->GetBottomPolyline().IsNull();
}

void HYDROGUI_RiverBottomDlg::onRiverChanged( const QString& )
{
    updateState();
}

void HYDROGUI_RiverBottomDlg::updateState()
{
    setApplyEnabled( !getRiverName().isEmpty() );
}

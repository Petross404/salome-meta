// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//


#ifndef HYDROGUI_OVERVIEW_H
#define HYDROGUI_OVERVIEW_H

#include <QFrame>
#include <OCCViewer_ViewWindow.h>
#include <AIS_InteractiveContext.hxx>

class OCCViewer_ViewPort3d;
class OCCViewer_ViewFrame;
class HYDROGUI_OverviewBand;
class QGridLayout;

class HYDROGUI_Overview : public QFrame
{
  Q_OBJECT

public:
  HYDROGUI_Overview( const QString& theTitle, int theMargin = 5, QWidget* theParent = 0 );
  virtual ~HYDROGUI_Overview();

  void setMainView( OCCViewer_ViewFrame* );
  void setTopView();

  QPoint fromMain( int xp, int yp ) const;
  OCCViewer_ViewPort3d* getViewPort( bool isMain ) const;

  QImage dump() const;

  Handle(AIS_InteractiveContext) context() const;

protected:
  virtual bool eventFilter( QObject*, QEvent* );
  void CustomFitSelection() const;
  Bnd_Box BoundingForSelection() const;

private slots:
  void OnTransformationAfterOp( OCCViewer_ViewWindow::OperationType );
  void OnTransformation();
  void OnMouseEvent( QMouseEvent* );
  void OnResizeEvent( QResizeEvent* );
  void onMainViewDestr();
  void onViewPortDestr();

private:
  QGridLayout*           myLayout;
  int                    myMargin;
  OCCViewer_ViewFrame*   myMainView;
  OCCViewer_ViewPort3d*  myViewPort;
  HYDROGUI_OverviewBand* myBand;
};

#endif



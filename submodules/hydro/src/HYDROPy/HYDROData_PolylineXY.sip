// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

%ExportedHeaderCode
#include <HYDROData_PolylineXY.h>
%End

class HYDROData_PolylineXY : public HYDROData_IPolyline
{

%TypeHeaderCode
//#define _DEVDEBUG_
#include "HYDRO_trace.hxx"
#include <HYDROData_PolylineXY.h>
%End

%ConvertToSubClassCode
    switch ( sipCpp->GetKind() )
    {
      case KIND_POLYLINEXY:
        sipClass = sipClass_HYDROData_PolylineXY;
        break;

      default:
        // We don't recognise the type.
        sipClass = NULL;
    }
%End

public:      

  /**
   * Returns default wire color for new polyline.
   */
  static QColor DefaultWireColor() const;

  /**
   * Returns the presentation of polyline section in Qt maner.
   */
  static void BuildPainterPath( QPainterPath&                       thePath,
                                const SectionType&                  theType,
                                const bool&                         theIsClosed,
                                const NCollection_Sequence<gp_XYZ>& thePoints );

  /**
   * Returns flag indicating that polyline can be edited or not.
   */
  virtual bool IsEditable() const;

  
public:

  /**
   * Imports shape from IOR.
   * \param theIOR the IOR of Geom object
   * \return \c true if shape has been successfully imported
   */
  bool ImportFromGeomIOR( const TCollection_AsciiString& theIOR );

  /**
   * Stores the study entry of the imported GEOM object.
   * \param theEntry GEOM object entry
   */
  void SetGeomObjectEntry( const TCollection_AsciiString& theEntry );

  /**
   * Returns the imported GEOM object entry.
   */
  TCollection_AsciiString GetGeomObjectEntry() const;


public:

  /**
   * Returns true if polyline is closed
   * \param theIsSimpleCheck flag indicating the type of checking
   *        - if true then all wires checked on closures
   *        - if false then for positive result polyline should consist of
   *          only one wire and which must be closed
   */
  bool IsClosed( const bool theIsSimpleCheck = true ) const;

   /**
   * Returns the distance beetwen first and point with index thePointIndex
   * at the section with index theSectionIndex. -1 is returned if error is occurred.
   */
  double GetDistance( const int theSectionIndex,
                      const int thePointIndex ) const;

  /**
   * Adds new one section.
   * \param theSectName name of the section
   * \param theSectionType type of section
   * \param theIsClosed flag indicates closures of section
   */
  void GetSections( NCollection_Sequence<TCollection_AsciiString>& theSectNames /Out/,
                    NCollection_Sequence<HYDROData_IPolyline::SectionType>& theSectTypes /Out/,
                    NCollection_Sequence<bool>& theSectClosures /Out/ ) const;

  /**
   * Replaces point for section with index "theSectionIndex".
   * \param theSectionIndex index of section
   * \param thePoints new points
   */
  void SetPoints( const int         theSectionIndex,
                  const HYDROData_IPolyline::PointsList& thePoints );

  /**
   * Returns the painter path.
   * Note: currently only the first section of the polyline data is taken into account.
   * \return polyline painter path.
   */
  virtual QPainterPath GetPainterPath() const;

  /**
   * import polylines from .shp or .xyz files
   */
  static HYDROData_SequenceOfObjects ImportShapesFromFile( const QString& theFileName );
//  [HYDROData_SequenceOfObjects ( const QString& )];
//  %MethodCode
//    QString aString(*a0);
//    Py_BEGIN_ALLOW_THREADS
//    sipRes = HYDROData_PolylineXY::ImportShapesFromFile( aString );
//    Py_END_ALLOW_THREADS
//  %End

  static bool ExportShapeXY( HYDROData_Document theDocument,
                             const QString& aFileName,
                             const NCollection_Sequence<opencascade::handle<HYDROData_PolylineXY> >& aPolyXYSeq)
                    [ void ( opencascade::handle<HYDROData_Document>,
                             const QString&,
                             const NCollection_Sequence<opencascade::handle<HYDROData_PolylineXY> >&,
                             QStringList& ) ];
  %MethodCode
    Handle(HYDROData_Document) aDoc = HYDROData_Document::Document();
    QString aFileName(*a1);
    QStringList aNonExpList;
    DEBTRACE(" --- 0");
    if ( !aDoc.IsNull() )
    {
        DEBTRACE("before HYDROData_PolylineXY::ExportShapeXY");
        Py_BEGIN_ALLOW_THREADS
        HYDROData_PolylineXY::ExportShapeXY( aDoc, aFileName, *a2, aNonExpList );
        Py_END_ALLOW_THREADS
        DEBTRACE("after");
    }
    bool ret = (aNonExpList.size() == 0);
    sipRes = ret;
  %End

protected:

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDROData_PolylineXY();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  ~HYDROData_PolylineXY();
};



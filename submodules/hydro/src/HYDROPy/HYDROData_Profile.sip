// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

%ExportedHeaderCode
#include "HYDROData_Profile.h"
%End

class HYDROData_Profile : public HYDROData_Object
{
%TypeHeaderCode
#include "HYDROData_Profile.h"
%End
public:

  typedef gp_XYZ                       ProfilePoint;
  typedef NCollection_Sequence<gp_XYZ> ProfilePoints;

public:
  /**
   * Returns default filling color for new profile.
   */
  virtual QColor DefaultFillingColor() const;

  /**
   * Returns default border color for new profile.
   */
  virtual QColor DefaultBorderColor() const;


public:

  /**
   * Check profile data and returns true if profile is valid.
   * Validity is determined by:
   *   - First(left) and Last(right) point was set
   *   - Parametric points size is more than 1
   */
  bool IsValid() const;

  /**
   * Invalidate profile first and last points.
   */
  void Invalidate();
  
public:

  // Public methods to work with profile points.

  /**
   * Set first(left) point for profile.
   * \param thePoint the point
   */
  void SetLeftPoint( const gp_XY& thePoint );

  /**
   * Returns first(left) point of profile.
   * \param thePoint[out] profile first point
   * \return true if point has been set
   */
  bool GetLeftPoint( gp_XY& thePoint ) const;
  
  /**
   * Set last(right) point for profile.
   * \param thePoint the point
   */
  void SetRightPoint( const gp_XY& thePoint );

  /**
   * Returns last(right) point of profile.
   * \param thePoint[out] profile last point
   * \return true if point has been set
   */
  bool GetRightPoint( gp_XY& thePoint ) const;


  /**
   * Returns object which store parametric presentation of profile points.
   * \return profile U,Z
   */
  HYDROData_ProfileUZ GetProfileUZ( const bool theIsCreate = true ) const 
    [opencascade::handle<HYDROData_ProfileUZ> ( const bool theIsCreate = true )];
  %MethodCode

    Handle(HYDROData_ProfileUZ) aRefProfileUZ;
    
    Py_BEGIN_ALLOW_THREADS
    aRefProfileUZ = sipSelfWasArg ? sipCpp->HYDROData_Profile::GetProfileUZ() : 
                                   sipCpp->GetProfileUZ();
    Py_END_ALLOW_THREADS
    
    sipRes = (HYDROData_ProfileUZ*)createPointer( aRefProfileUZ );
  
  %End


  /**
   * Return number of profile points.
   * \return number of points
   */
  int NbPoints() const;

  /**
   * Remove all profile points.
   */
  void RemovePoints();


  /**
   * Replace current profile parametric points by new one.
   * \param thePoints the list with new points in parametric form
   */
  void SetParametricPoints( const HYDROData_IPolyline::PointsList& thePoints );

  /**
   * Returns profile points in parametric form.
   * \return points list
   */
  HYDROData_IPolyline::PointsList GetParametricPoints() const;


  /**
   * Replace current profile points by new one.
   * First and last points will be automatically updated.
   * \param thePoints the list with new profile points
   */
  void SetProfilePoints( const HYDROData_Profile::ProfilePoints thePoints );

  /**
   * Returns profile points.
   * Empty sequence is returned if first or last point was not set.
   * \return profile points list
   */
  HYDROData_Profile::ProfilePoints GetProfilePoints() const;


public:
  // Public methods to work with files.

  /**
   * Stores the profile file path
   * \param theFilePath profile file path
   */
  void SetFilePath( const TCollection_AsciiString& theFilePath );

  /**
   * Returns uploaded profile file path
   */
  TCollection_AsciiString GetFilePath() const;

  /**
   * Imports Profile data from file. The supported file types:
   *  - parametric presentation of profile (2 points in line U,Z)
   *  - georeferenced presentation of profile (3 points in line X,Y,Z)
   * Create as many objects as many profiles in the file are defined.
   * \param theFileName the path to file
   * \return \c number of successfully imported profiles
   */
  static int ImportFromFile( HYDROData_Document             theDoc,
                             const TCollection_AsciiString& theFileName,
                             NCollection_Sequence<int>&     theBadProfilesIds,
                             bool isToProject = true )
  [int ( const opencascade::handle<HYDROData_Document>&,
         const TCollection_AsciiString&,
         NCollection_Sequence<int>& )];
  %MethodCode
    Py_BEGIN_ALLOW_THREADS
    sipRes = HYDROData_Profile::ImportFromFile( a0, *a1, *a2, a3 );
    Py_END_ALLOW_THREADS
  %End

  /**
   * Imports Profile data from file.
   * \param theFileName the path to file
   * \param theIsRead set to true if at least one non empty string was read from file
   * \return \c true if file has been successfully read
   */
  virtual bool ImportFromFile( const TCollection_AsciiString& theFileName,
							   bool isToProject = true,
                               bool* isNotEmpty = 0 );

protected:
  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDROData_Profile();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  ~HYDROData_Profile();
};

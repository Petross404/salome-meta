// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

%ExportedHeaderCode
#include <HYDROData_StricklerTable.h>
#include <TColStd_SequenceOfExtendedString.hxx>
%End

class HYDROData_StricklerTable : public HYDROData_Entity
{

%ConvertToSubClassCode
    switch ( sipCpp->GetKind() )
    {
      case KIND_STRICKLER_TABLE:
        sipClass = sipClass_HYDROData_StricklerTable;
        break;

      default:
        // We don't recognise the type.
        sipClass = NULL;
    }
%End

%TypeHeaderCode
#include <HYDROData_StricklerTable.h>
%End

public:      

  bool Import( const QString& theFileName );
  bool Export( const QString& theFileName );

  double Get( const QString& theType, double theDefault ) const;
  void   Set( const QString& theType, double theCoefficient );

  QString GetAttrName() const;
  bool    SetAttrName( const QString& ) const;

  QString GetAttrValue( const QString& theType ) const;
  void    SetAttrValue( const QString& theType, const QString& theAttrValue ) const;

  QColor GetColor( const QString& theType ) const;
  void   SetColor( const QString& theType, const QColor& theColor ) const;

  QStringList GetTypes() const;

  void Clear();

protected:
  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDROData_StricklerTable();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  ~HYDROData_StricklerTable();
};



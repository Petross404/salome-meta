// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

%ExportedHeaderCode
#include <HYDROData_LandCoverMap.h>
%End

class HYDROData_LandCoverMap : public HYDROData_Entity
{
%TypeHeaderCode
#include <HYDROData_LandCoverMap.h>
%End

%ConvertToSubClassCode
    switch ( sipCpp->GetKind() )
    {
      case KIND_LAND_COVER_MAP:
        sipClass = sipClass_HYDROData_LandCoverMap;
        break;

      default:
        // We don't recognise the type.
        sipClass = NULL;
    }
%End

public:
  enum DBFStatus
  {
    DBFStatus_OK,
    DBFStatus_DIFF_SIZE_ERROR,
    DBFStatus_OPEN_FILE_ERROR,
    DBFStatus_NO_SUCH_FIELD_ERROR,
    DBFStatus_NO_DBFVALUES_CORRESPONDENCE_WARNING
  };

  HYDROData_LandCoverMap();
  ~HYDROData_LandCoverMap();

  DBFStatus ImportDBF( const QString& theDBFFileName, 
    const QString& theFieldName, 
    const QStringList& DBFValues,
    const QStringList& StricklerTypes );
 
  void ExportDBF( const QString& theDBFFileName,
    const QString& theFieldName, 
    const QStringList& theDBFValues,
    const QStringList& theStricklerTypes ) const;
 
  bool ImportSHP( const QString& theFileName, const QList<int>& theIndices = QList<int>() );

  bool ExportSHP( const QString& theFileName ) const;

  bool ExportTelemac( const QString& theFileName, double theDeflection,
                      HYDROData_StricklerTable theTable ) const
  [bool( const QString&, double, const opencascade::handle<HYDROData_StricklerTable>& )];
  %MethodCode
    Handle(HYDROData_StricklerTable) aRef =
      Handle(HYDROData_StricklerTable)::DownCast( createHandle( a2 ) );
    if ( !aRef.IsNull() )
    {
      Py_BEGIN_ALLOW_THREADS
	  QString messStat;
      sipRes = sipSelfWasArg ? sipCpp->HYDROData_LandCoverMap::ExportTelemac( *a0, a1, aRef, messStat ):
                               sipCpp->ExportTelemac( *a0, a1, aRef, messStat );
      Py_END_ALLOW_THREADS
    }
  %End

  bool Add( HYDROData_Object theObject, const QString& theType )
  [bool ( const opencascade::handle<HYDROData_Object>&, const QString& )];
  %MethodCode
    Handle(HYDROData_Object) aRef =
      Handle(HYDROData_Object)::DownCast( createHandle( a0 ) );
    if ( !aRef.IsNull() )
    {
      Py_BEGIN_ALLOW_THREADS
      sipRes = sipSelfWasArg ? sipCpp->HYDROData_LandCoverMap::Add( aRef, *a1 ):
                               sipCpp->Add( aRef, *a1 );
      Py_END_ALLOW_THREADS
    }
  %End

  bool Add( HYDROData_PolylineXY thePolyline, const QString& theType )
  [bool ( const opencascade::handle<HYDROData_PolylineXY>&, const QString& )];
  %MethodCode
    Handle(HYDROData_PolylineXY) aRef =
      Handle(HYDROData_PolylineXY)::DownCast( createHandle( a0 ) );
    if ( !aRef.IsNull() )
    {
      Py_BEGIN_ALLOW_THREADS
      sipRes = sipSelfWasArg ? sipCpp->HYDROData_LandCoverMap::Add( aRef, *a1 ):
                               sipCpp->Add( aRef, *a1 );
      Py_END_ALLOW_THREADS
    }
  %End

  bool Split( HYDROData_PolylineXY thePolyline )
  [bool ( const opencascade::handle<HYDROData_PolylineXY>& )];
  %MethodCode
    Handle(HYDROData_PolylineXY) aRef =
      Handle(HYDROData_PolylineXY)::DownCast( createHandle( a0 ) );
    if ( !aRef.IsNull() )
    {
      Py_BEGIN_ALLOW_THREADS
      sipRes = sipSelfWasArg ? sipCpp->HYDROData_LandCoverMap::Split( aRef ):
                               sipCpp->Split( aRef );
      Py_END_ALLOW_THREADS
    }
  %End
};

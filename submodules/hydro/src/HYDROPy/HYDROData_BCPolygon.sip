// Copyright (C) 2014-2015  EDF-R&D
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

%ExportedHeaderCode
#include <HYDROData_BCPolygon.h>
%End

class HYDROData_BCPolygon : public HYDROData_Object
{

%TypeHeaderCode
#include <HYDROData_BCPolygon.h>
%End

%ConvertToSubClassCode
    switch ( sipCpp->GetKind() )
    {
      case KIND_BC_POLYGON:
        sipClass = sipClass_HYDROData_BCPolygon;
        break;

      default:
        // We don't recognise the type.
        sipClass = NULL;
    }
%End

public:      

  /**
   * Sets reference polyline object for boundary polygon.
   */
  void SetPolyline( HYDROData_PolylineXY thePolyline ) [void (const opencascade::handle<HYDROData_PolylineXY>&)];
  %MethodCode

    Handle(HYDROData_PolylineXY) aRefPolyline =
      Handle(HYDROData_PolylineXY)::DownCast( createHandle( a0 ) );
    if ( !aRefPolyline.IsNull() )
    {
      Py_BEGIN_ALLOW_THREADS
      sipSelfWasArg ? sipCpp->HYDROData_BCPolygon::SetPolyline( aRefPolyline ) : 
                      sipCpp->SetPolyline( aRefPolyline );
      Py_END_ALLOW_THREADS
    }

  %End

  /**
   * Returns reference polyline object of boundary polygon.
   */
  HYDROData_PolylineXY GetPolyline() const [opencascade::handle<HYDROData_PolylineXY> ()];
  %MethodCode

    Handle(HYDROData_PolylineXY) aRefPolyline;
    
    Py_BEGIN_ALLOW_THREADS
    aRefPolyline = sipSelfWasArg ? sipCpp->HYDROData_BCPolygon::GetPolyline() : 
                                   sipCpp->GetPolyline();
    Py_END_ALLOW_THREADS
    
    sipRes = (HYDROData_PolylineXY*)createPointer( aRefPolyline );
  
  %End

  /**
   * Remove reference polyline object of boundary polygon.
   */
  void RemovePolyline();

  /**
   * Set boundary type for boundary polygon.
   */
  void SetBoundaryType( int ) const;

  /**
   * Get boundary type for boundary polygon.
   */
   int GetBoundaryType() const;

protected:

  /**
   * Creates new object in the internal data structure. Use higher level objects 
   * to create objects with real content.
   */
  HYDROData_BCPolygon();

  /**
   * Destructs properties of the object and object itself, removes it from the document.
   */
  ~HYDROData_BCPolygon();
};



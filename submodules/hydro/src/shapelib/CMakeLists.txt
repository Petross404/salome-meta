#include(../../CMake/Common.cmake)

set(PROJECT_HEADERS
    shapefil.h
)

set(PROJECT_SOURCES 
    dbfopen.c
    safileio.c
    shpopen.c
    shptree.c
    shputils.c
)

add_definitions(
  -DSHAPELIB_DLLEXPORT -fPIC
)

include_directories(
)

add_library(shapelib STATIC ${PROJECT_SOURCES} ${PROJECT_HEADERS})
INSTALL(TARGETS shapelib EXPORT ${PROJECT_NAME}TargetGroup DESTINATION ${SALOME_INSTALL_LIBS})

set(PROJECT_LIBRARIES shapelib)


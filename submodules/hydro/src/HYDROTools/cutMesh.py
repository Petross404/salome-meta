#!/usr/bin/env python

import os
import sys
import salome

salome.salome_init()

from HYDROPy import *

def cutMesh(meshFileIn, polyFile, meshFileOut = "", offsetX=0, offsetY=0):
    """
    In a given MED mesh, remove faces and edges contained in a polygon (shapefile). Save the result in a new file.
    parameters:
    meshFileIn: full path of input MED file. Coordinates should be without an origin offset of coordinates.
    polyFile: a shapefile giving a single polygon for cut (should be in the same coordinate system as the mesh, without an origin offset of coordinates.
    meshFileout: full path of output MED file (default="" : when "", output file is suffixed with "_cut.med"
    offsetX: local X origin for cut operation and output
    offsetY: local Y origin for cut operation and output
    return:
    meshFileout
    """
    hydro_doc = HYDROData_Document.Document()

    # --- coordonnées locales

    hydro_doc.SetLocalCS( offsetX, offsetY )

    # --- polygone decoupe

    a = os.path.splitext(polyFile)
    polyName = os.path.basename(a[0])
    poly = HYDROData_PolylineXY.ImportShapesFromFile(polyFile)
    #poly = hydro_doc.FindObjectByName(polyName + '_PolyXY_0')

    # --- zone immersible

    zoneImmersible = hydro_doc.CreateObject( KIND_IMMERSIBLE_ZONE )
    zoneImmersible.SetName( "zoneImmersible" )
    zoneImmersible.SetPolyline( poly[0] )
    zoneImmersible.Update()

    # --- cas de calcul = une face simple sur le polygone

    casCalcul = hydro_doc.CreateObject( KIND_CALCULATION )
    casCalcul.SetName( "casCalcul" )
    casCalcul.SetAssignmentMode( HYDROData_CalculationCase.AUTOMATIC )
    casCalcul.AddGeometryObject( zoneImmersible )
    case_geom_group = zoneImmersible.GetGroup( 0 )
    casCalcul.AddGeometryGroup( case_geom_group )
    casCalcul.SetBoundaryPolyline( poly[0] )
    casCalcul.Update()

    # --- export geom

    casCalcul_entry = casCalcul.Export()
    print("casCalcul_entry", casCalcul_entry)

    import GEOM
    from salome.geom import geomBuilder
    geompy = geomBuilder.New()

    geomObj = salome.IDToObject(str(casCalcul_entry))

    # --- decoupe maillage dans SMESH

    import  SMESH, SALOMEDS
    from salome.smesh import smeshBuilder

    # --- chargement maillage, changement de repère

    smesh = smeshBuilder.New()
    ([Mesh], status) = smesh.CreateMeshesFromMED(meshFileIn)
    Mesh.TranslateObject( Mesh, [ -offsetX, -offsetY, 0 ], 0 )

    # --- groupes des faces et des edges contenues dans le polygone de découpe

    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.FACE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined, geomObj)
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(Mesh.GetMesh())
    decoupeFaces = Mesh.GroupOnFilter( SMESH.FACE, 'decoupeFaces', aFilter )

    aCriteria = []
    aCriterion = smesh.GetCriterion(SMESH.EDGE,SMESH.FT_BelongToGeom,SMESH.FT_Undefined, geomObj)
    aCriteria.append(aCriterion)
    aFilter = smesh.GetFilterFromCriteria(aCriteria)
    aFilter.SetMesh(Mesh.GetMesh())
    decoupeEdges = Mesh.GroupOnFilter( SMESH.EDGE, 'decoupeEdges', aFilter )

    # --- suppression des groupes et de leurs mailles

    Mesh.RemoveGroupWithContents( decoupeFaces )
    Mesh.RemoveGroupWithContents( decoupeEdges )

    # --- regeneration des edges de bord manquantes

    nbAdded, Mesh, _NoneGroup = Mesh.MakeBoundaryElements( SMESH.BND_1DFROM2D, '', '', 0, [])

    # --- enregistrement MED du maillage découpé

    if meshFileOut == "":
        a = os.path.splitext(meshFileIn)
        smesh.SetName(Mesh, os.path.basename(a[0]))
        meshFileOut = a[0] + '_cut' + a[1]

    Mesh.ExportMED(meshFileOut,auto_groups=0,minor=40,overwrite=1,meshPart=None,autoDimension=1)

    if salome.sg.hasDesktop():
        salome.sg.updateObjBrowser()

    return meshFileOut

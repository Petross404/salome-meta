# -*- coding: utf-8 -*-

"""
assignStrickler
"""
__revision__ = "V2.02"

import os

import salome

import HYDROPy

import numpy as np
import MEDLoader as ml
import medcoupling as mc

salome.salome_init()

class BadParamsError(ValueError):
  """Bad parameters exception"""
  pass


def assignStrickler(nomCas, input_file_name, output_file_name="", med_field_name='BOTTOM FRICTION', verbose=False):
  """
  assignStrickler creates the scalar field associated with the mesh of the calculation case that
  represents the Strickler coefficient of the land cover.

  In:
    nomCas: Calculation Case Name in module HYDRO
    input_file_name: med file name corresponding to the HYDRO case
    output_file_name: med file name with the Strickler coefficient; default is the same as fichierMaillage
    med_field_name: name of the field of the Strickler coefficient
                     default value is 'BOTTOM FRICTION'.
  Out:
  """
  erreur = 0
  message = ""

  while not erreur:

    if verbose:
      print("nomCas:", nomCas)
      print("input_file_name:", input_file_name)
      print("output_file_name:", output_file_name)
      print("med_field_name:", med_field_name)

# 1. Controls
# 1.1. Check calculation case
    doc = HYDROPy.HYDROData_Document.Document()
    case = doc.FindObjectByName(nomCas)
    if case is None:
      raise BadParamsError("Calculation case '%s' not found" % nomCas)

# 1.2. Check input MED file
    if not os.path.exists(input_file_name):
      raise BadParamsError("Input file '%s' not exists" % input_file_name)

# 2. The output file
# 2.1. If output MED file is not defined, it is equal to the input file
    if not output_file_name:
      output_file_name = input_file_name

# 2.2. Copy input file to the output path, if the output path is different from the input path
    if output_file_name != input_file_name:
      import shutil
      shutil.copyfile(input_file_name, output_file_name)
#
# 3. Reads the mesh
#
    meshMEDFileRead = ml.MEDFileMesh.New(output_file_name)
#
# 4. Gets the information about the nodes
#
    nbnodes = meshMEDFileRead.getNumberOfNodes()
    if verbose:
      print("Number of nodes: %d" % nbnodes)
#
    coords = meshMEDFileRead.getCoords()
    #print "coords =\n", coords
#
# 5. Calculation of the Strickler coefficient
    values = list()
    x_coords = coords[:, 0]
    y_coords = coords[:, 1]
    #print "x_coords =\n", x_coords
    #print "y_coords =\n", y_coords
    values = case.GetStricklerCoefficientForPoints(x_coords, y_coords, 0.0, True)
#
# 6. Field MED file
#
    coeff = mc.DataArrayDouble(np.asfarray(values, dtype='float'))
    coeff.setInfoOnComponents(["Strickler [SI]"])
    #print "coeff =\n", coeff
    if verbose:
      print(".. Ecriture du Strickler sous le nom '"+med_field_name+"'")
    fieldOnNodes = ml.MEDCouplingFieldDouble(ml.ON_NODES)
    fieldOnNodes.setName(med_field_name)
    fieldOnNodes.setMesh(meshMEDFileRead.getMeshAtLevel(0))
    fieldOnNodes.setArray(coeff)
#   Ces valeurs d'instants sont mises pour assurer la lecture par TELEMAC
#   instant = 0.0
#   numero d'itération : 0
#   pas de numero d'ordre (-1)
    fieldOnNodes.setTime(0.0, 0, -1)
#
    fMEDFile_ch_d = ml.MEDFileField1TS()
    fMEDFile_ch_d.setFieldNoProfileSBT(fieldOnNodes)
    fMEDFile_ch_d.write(output_file_name, 0)
#
    break

  if erreur:
    print(message)

  return

# -*- coding: utf-8 -*-
"""
Example of use case of interpolZ with the default values:

# --- case name in HYDRO
nomCas = 'inondation1'

# --- med file 2D(x,y) of the case produced by SMESH
fichierMaillage = '/home/B27118/projets/LNHE/garonne/inondation1.med'

# --- dictionary [med group name] = region name
dicoGroupeRegion= dict(litMineur          = 'inondation1_litMineur',
                       litMajeurDroite    = 'inondation1_riveDroite',
                       litMajeurGauche    = 'inondation1_riveGauche',

# --- Z interpolation on the bathymety/altimetry on the mesh nodes
interpolZ(nomCas, fichierMaillage, dicoGroupeRegion)
"""
__revision__ = "V3.01"
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------

import salome

salome.salome_init()

import numpy as np
import MEDLoader as ml
import medcoupling as mc

# -----------------------------------------------------------------------------

from med import medfile
from med import medmesh
from med import medfield
from med import medenum

# -----------------------------------------------------------------------------

import HYDROPy

class MyInterpolator( HYDROPy.HYDROData_IInterpolator ):
  """
  Class MyInterpolator
  """
  def __init__ (self) :
    """
Constructor
    """
  def GetAltitudeForPoint( self, theCoordX, theCoordY ):
    """
    Function
    """
    alt_obj = HYDROPy.HYDROData_IInterpolator.GetAltitudeObject( self )
    z = alt_obj.GetAltitudeForPoint( theCoordX, theCoordY )    # Custom calculation takes the base value and changes it to test
    #z2 = (z - 74.0)*10
    z2 = z
    return z2

# -----------------------------------------------------------------------------


def interpolZ(nomCas, fichierMaillage, dicoGroupeRegion, zUndef=-100., regions_interp_method=None, m3d=True, xyzFile=False, verbose=False):
  """
  interpolZ takes a 2D (x,y) mesh and calls the active instance of module HYDRO
  to interpolate the bathymetry/altimetry on the mesh nodes, to produce the Z value of each node.

  In:
    nomCas: Calculation Case Name in module HYDRO
    fichierMaillage: med file name produced by SMESH, corresponding to the HYDRO case
    dicoGroupeRegion: python dictionary giving the correspondance of mesh groups to HYDRO regions.
                      Key: face group name
                      Value: region name in the HYDRO Case
    zUndef: Z value to use for nodes outside the regions (there must be none if the case is correct).
                     default value is -100.
    interpolMethod: integer value or dict
                    if integer : 
                    0 = nearest point on bathymetry
                    1 = linear interpolation
                    if dict : key is a region name, value is a type of interpolation (0 or 1)
                    if None : default type used (0)
    m3d: True/False to produce a 3D mesh. Default is False.
    xyzFile: True/False to write an ascii file with xyz for every node. Default is False.
  Out:
    statz: statistique for z
                      Key: face group name
                      Value: (minz, maxz, meanz, stdz, v05z, v95z)
  Out:
    return <fichierMaillage>F.med : med file with Z value in a field "BOTTOM"
                                    Option: Z value also in Z coordinate if m3D is true
    return <fichierMaillage>.xyz : text file with X, Y, Z values
  """
  statz = dict()
  erreur = 0
  message = ""

  while not erreur:

    if verbose:
      ligne = "nomCas: %s" % nomCas
      ligne += "\ninterpolMethods: %s" % regions_interp_method
      if (zUndef != None ):
        ligne += "\nzUndef: %f" % zUndef
      ligne += "\nm3d: %d" % m3d
      print (ligne)

    doc = HYDROPy.HYDROData_Document.Document()
    cas = doc.FindObjectByName(nomCas)
    print ( "cas : ", cas)
    custom_inter = MyInterpolator()

    basename = fichierMaillage[:-4]
    fichierFMaillage = basename + 'F.med'

    print ("dicoGroupeRegion = ", dicoGroupeRegion)
    ligne = "fichierMaillage  = %s" % fichierMaillage
    ligne += "\nfichierFMaillage = %s" % fichierFMaillage
    if xyzFile:
      fichierFonds = basename + '.xyz'
      ligne += "\nfichierFonds     = %s" % fichierFonds
    print (ligne)
#
# 1. Reads the mesh
#
    meshMEDFileRead = ml.MEDFileMesh.New(fichierMaillage)
    if verbose:
      print (meshMEDFileRead)
#
# 2. Checks the names of the groups of faces
#
    t_group_n = meshMEDFileRead.getGroupsNames()
    dicoGroupeRegion_0 = dict()
    nb_pb = 0
    for gr_face_name in dicoGroupeRegion:
      saux = gr_face_name.strip()
      if saux not in t_group_n:
        message += "Group: '" + gr_face_name + "'\n"
        nb_pb += 1
      else :
        dicoGroupeRegion_0[saux] =  dicoGroupeRegion[gr_face_name]
    if verbose:
      ligne = "Number of problems: %d" % nb_pb
      print (ligne)
#
    if nb_pb > 0:
      if nb_pb == 1:
        message += "This group does"
      else:
        message += "These %d groups do" % nb_pb
      message += " not belong to the mesh.\n"
      message += "Please check the names of the group(s) of faces corresponding to each region of the HYDRO case.\n"
      message += "Groups for this file:\n"
      for group_n in t_group_n :
        message += "'%s'\n" % group_n
      erreur = 2
      break
#
# 3. Gets the information about the nodes
#
    nbnodes = meshMEDFileRead.getNumberOfNodes()
    if verbose:
      ligne = "Number of nodes: %d" % nbnodes
      print (ligne)
#
    coords = meshMEDFileRead.getCoords()
    #print (coords)
    if verbose:
      nb_comp = coords.getNumberOfComponents()
      l_info = coords.getInfoOnComponents()
      ligne = ""
      l_info_0=["X", "Y", "Z"]
      for id_node in (0, 1, nbnodes-1):
        ligne += "\nNode #%6d:" % id_node
        for iaux in range(nb_comp):
          if l_info[iaux]:
            saux = l_info[iaux]
          else:
            saux = l_info_0[iaux]
          ligne += " %s" % saux
          ligne += "=%f" % coords[id_node, iaux]
      print (ligne)
#
# 4. Exploration of every group of faces
#
    tb_aux = np.zeros(nbnodes, dtype=np.bool)
#
    bathy = np.zeros(nbnodes, dtype=np.float)
    bathy.fill(zUndef)
#
    for gr_face_name in dicoGroupeRegion_0:
#
#     4.1. Region connected to the group
#
      nomreg = dicoGroupeRegion_0[gr_face_name]
      ligne = "------- Region: '%s'" % nomreg
      ligne += ", connected to group '%s'" % gr_face_name
      print (ligne)
      region = doc.FindObjectByName(nomreg)
#
#     4.2. Mesh of the group
#
      mesh_of_the_group = meshMEDFileRead.getGroup(0, gr_face_name, False)
      nbr_cells = mesh_of_the_group.getNumberOfCells()
      if verbose:
        ligne = "\t. Number of cells: %d" % nbr_cells
        print (ligne)
#
#     4.3. Nodes of the meshes of the group
#          Every node is flagged in tb_aux
#
      tb_aux.fill(False)
      for id_elem in range(nbr_cells):
        l_nodes = mesh_of_the_group.getNodeIdsOfCell(id_elem)
        #print l_nodes
        for id_node in l_nodes:
          tb_aux[id_node] = True
      np_aux = tb_aux.nonzero()
      if len(np_aux[0]):
        if verbose:
          ligne = "\t. Number of nodes for this group: %d" % len(np_aux[0])
          print (ligne)
      #print ("np_aux:", np_aux)
#
#     4.4. Interpolation over the nodes of the meshes of the group
#
      vx = list()
      vy = list()
      for nid in np_aux[0]:
        nodeId = nid.item()
        vx.append(coords[nodeId, 0])
        vy.append(coords[nodeId, 1])
      #print ("vx:\n", vx)
      #print ("vy:\n", vy)
#
      interpolMethod = 0
      if regions_interp_method is not None:
        if isinstance(regions_interp_method, dict) and nomreg in list(regions_interp_method.keys()):
          interpolMethod = int(regions_interp_method[nomreg])
        elif isinstance(regions_interp_method, int):
          interpolMethod = regions_interp_method

      #print ('interp', interpolMethod )
      vz = cas.GetAltitudesForPoints(vx, vy, region, interpolMethod)
#
      #print ("vz:\n", vz)
      minz = np.amin(vz)
      maxz = np.amax(vz)
      meanz = np.mean(vz)
      stdz = np.std(vz)
      v05z = np.percentile(vz, 0o5)
      v95z = np.percentile(vz, 95)
#
      if verbose:
        ligne = ".. Minimum: %f" % minz
        ligne += ", maximum: %f" % maxz
        ligne += ", mean: %f\n" % meanz
        ligne += ".. stdeviation: %f" % stdz
        ligne += ", v05z: %f" % v05z
        ligne += ", v95z: %f" % v95z
        print (ligne)
#
#     4.5. Storage of the z and of the statistics for this region
#
      statz[gr_face_name] = (minz, maxz, meanz, stdz, v05z, v95z)
#
      for iaux, nodeId in enumerate(np_aux[0]):
        bathy[nodeId] = vz[iaux]
#
# 5. Minimum:
#    During the interpolation, if no value is available over a node, a default value
#    is set: -9999. It has no importance for the final computation, but if the field
#    or the mesh is displayed, it makes huge gap. To prevent this artefact, a more
#    convenient "undefined" value is set. This new undefined value is given by the user.
#
#    zUndefThreshold: the default value is zUndef +10. It is tied with the value -100. given
#                     by the interpolation when no value is defined.
#
    zUndefThreshold = zUndef +10.
    if verbose:
      ligne = "zUndefThreshold: %f" % zUndefThreshold
      print (ligne)
#
    #print "bathy :\n", bathy
    np_aux_z = (bathy < zUndefThreshold).nonzero()
    if verbose:
      ligne = ".. Number of nodes below the minimum: %d" % len(np_aux_z[0])
      print (ligne)
#
# 6. Option : xyz file
#
    if xyzFile:
#
      if verbose:
        ligne = ".. Ecriture du champ de bathymétrie sur le fichier :\n%s" % fichierFonds
        print (ligne)
#
      with open(fichierFonds, "w") as fo :
        for nodeId in range(nbnodes):
          ligne = "%11.3f %11.3f %11.3f\n" % (coords[nodeId, 0], coords[nodeId, 1], bathy[nodeId])
          fo.write(ligne)
#
# 7. Final MED file
# 7.1. Transformation of the bathymetry as a double array
#
    bathy_dd = mc.DataArrayDouble(np.asfarray(bathy, dtype='float'))
    bathy_dd.setInfoOnComponents(["Z [m]"])
#
# 7.2. If requested, modification of the z coordinate
#
    if m3d:
      coords3D = ml.DataArrayDouble.Meld([coords[:,0:2], bathy_dd])
      coords3D.setInfoOnComponents(["X [m]", "Y [m]", "Z [m]"])
      #print "coords3D =\n", coords3D
      meshMEDFileRead.setCoords(coords3D)
#
# 7.3. Writes the mesh
#
    if verbose:
      if m3d:
        saux = " 3D"
      else:
        saux = ""
      ligne = ".. Ecriture du maillage" + saux
      ligne += " sur le fichier :\n%s" % fichierFMaillage
      print (ligne)
#
    meshMEDFileRead.write(fichierFMaillage, 2)
#
# 7.4. Writes the field
#
    med_field_name = "BOTTOM"
    if verbose:
      ligne = ".. Ecriture du champ '%s'" % med_field_name
      print (ligne)
#
    #print "bathy_dd =\n", bathy_dd
    fieldOnNodes = ml.MEDCouplingFieldDouble(ml.ON_NODES)
    fieldOnNodes.setName(med_field_name)
    fieldOnNodes.setMesh(meshMEDFileRead.getMeshAtLevel(0))
    fieldOnNodes.setArray(bathy_dd)
#   Ces valeurs d'instants sont mises pour assurer la lecture par TELEMAC
#   instant = 0.0
#   numero d'itération : 0
#   pas de numero d'ordre (-1)
    fieldOnNodes.setTime(0.0, 0, -1)
#
    fMEDFile_ch_d = ml.MEDFileField1TS()
    fMEDFile_ch_d.setFieldNoProfileSBT(fieldOnNodes)
    fMEDFile_ch_d.write(fichierFMaillage, 0)
#
    break
#
  if erreur:
    print(message)
#
  return statz


def interpolZ_B(bathyName, fichierMaillage, gr_face_name, zUndef=-100., interp_method=0, m3d=True, xyzFile=False, verbose=False):
  """
  interpolZ_B takes a 2D (x,y) mesh and calls the active instance of module HYDRO
  to interpolate the bathymetry/altimetry on the mesh nodes, to produce the Z value of each node.

  In:
    bathyName: Bathymetry Name in module HYDRO
    fichierMaillage: med file name produced by SMESH, corresponding to the HYDRO case
    gr_face_name:  face group name
    zUndef: Z value to use for nodes outside the regions (there must be none if the case is correct).
                     default value is -100.
    interp_method: interpolation method
                    0 = nearest point on bathymetry
                    1 = linear interpolation
    m3d: True/False to produce a 3D mesh. Default is True.
    xyzFile: True/False to write an ascii file with xyz for every node. Default is False.
  Out:
    if OK : statz_gr_face_name: statistic for z: (minz, maxz, meanz, stdz, v05z, v95z)
	else FALSE
  Out:
    return <fichierMaillage>F.med : med file with Z value in a field "BOTTOM"
                                    Option: Z value also in Z coordinate if m3D is true
    return <fichierMaillage>.xyz : text file with X, Y, Z values
  """
  statz = dict()
  doc = HYDROPy.HYDROData_Document.Document()
  bathy_obj = doc.FindObjectByName(bathyName)
  print( "bathy : ", bathy_obj)
  if bathy_obj is None:
    print ( "bathy is None")
    return False

  basename = fichierMaillage[:-4]
  fichierFMaillage = basename + 'F.med'

  ligne = "fichierMaillage  = %s" % fichierMaillage
  ligne += "\nfichierFMaillage = %s" % fichierFMaillage
  
  if xyzFile:
    fichierFonds = basename + '.xyz'
    ligne += "\nfichierFonds     = %s" % fichierFonds
  print (ligne)
#
# 1. Reads the mesh
#
  meshMEDFileRead = ml.MEDFileMesh.New(fichierMaillage)
  if verbose:
    print (meshMEDFileRead)
#
# 2. Checks the names of the groups of faces
#
  t_group_n = meshMEDFileRead.getGroupsNames()
  gr_face_name_tr = gr_face_name.strip()
  if gr_face_name_tr not in t_group_n:
    print("Group not found")
    return False	  
#
# 3. Gets the information about the nodes
#
  nbnodes = meshMEDFileRead.getNumberOfNodes()
  if verbose:
    ligne = "Number of nodes: %d" % nbnodes
    print (ligne)
#
  coords = meshMEDFileRead.getCoords()
  #print(coords[:,2])
  if verbose:
    nb_comp = coords.getNumberOfComponents()
    l_info = coords.getInfoOnComponents()
    ligne = ""
    l_info_0=["X", "Y", "Z"]
    for id_node in (0, 1, nbnodes-1):
      ligne += "\nNode #%6d:" % id_node
      for iaux in range(nb_comp):
        if l_info[iaux]:
          saux = l_info[iaux]
        else:
          saux = l_info_0[iaux]
        ligne += " %s" % saux
        ligne += "=%f" % coords[id_node, iaux]
    print (ligne)
#
# 4. Exploration of every group of faces
#
  tb_aux = np.zeros(nbnodes, dtype=np.bool)
#
  bathy = np.zeros(nbnodes, dtype=np.float)
  bathy.fill(zUndef)
  if (coords.getNumberOfComponents() >2):
    bathy = coords[:,2].toNumPyArray()
  else:
    print("=== WARNING! === Mesh has no altitude component z, z will be filled with zUndef = %s outside the group!"%zUndef)  

#
# 4.1. Mesh of the group
#
  mesh_of_the_group = meshMEDFileRead.getGroup(0, gr_face_name_tr, False)
  nbr_cells = mesh_of_the_group.getNumberOfCells()
  if verbose:
    ligne = "\t. Number of cells: %d" % nbr_cells
    print (ligne)
#
# 4.2. Nodes of the meshes of the group
#      Every node is flagged in tb_aux
#
  tb_aux.fill(False)
  for id_elem in range(nbr_cells):
    l_nodes = mesh_of_the_group.getNodeIdsOfCell(id_elem)
    #print l_nodes
    for id_node in l_nodes:
      tb_aux[id_node] = True
  np_aux = tb_aux.nonzero()
  if len(np_aux[0]):
    if verbose:
      ligne = "\t. Number of nodes for this group: %d" % len(np_aux[0])
      print (ligne)
  #print ("np_aux:", np_aux)
#
# 4.3. Interpolation over the nodes of the meshes of the group
#
  vx = list()
  vy = list()
  for nid in np_aux[0]:
    nodeId = nid.item()
    vx.append(coords[nodeId, 0])
    vy.append(coords[nodeId, 1])
  #print ("vx:\n", vx)
  #print ("vy:\n", vy)
#
  vz = bathy_obj.GetAltitudesForPoints(vx, vy, interp_method)
#
  #print ("vz:\n", vz)
  minz = np.amin(vz)
  maxz = np.amax(vz)
  meanz = np.mean(vz)
  stdz = np.std(vz)
  v05z = np.percentile(vz, 0o5)
  v95z = np.percentile(vz, 95)
#
  if verbose:
    ligne = ".. Minimum: %f" % minz
    ligne += ", maximum: %f" % maxz
    ligne += ", mean: %f\n" % meanz
    ligne += ".. stdeviation: %f" % stdz
    ligne += ", v05z: %f" % v05z
    ligne += ", v95z: %f" % v95z
    print (ligne)
#
# 4.4. Storage of the z and of the statistics for this region
#
  statz[gr_face_name] = (minz, maxz, meanz, stdz, v05z, v95z)
#
  for iaux, nodeId in enumerate(np_aux[0]):
    bathy[nodeId] = vz[iaux]
#
# 5. Minimum:
#    During the interpolation, if no value is available over a node, a default value
#    is set: -9999. It has no importance for the final computation, but if the field
#    or the mesh is displayed, it makes huge gap. To prevent this artefact, a more
#    convenient "undefined" value is set. This new undefined value is given by the user.
#
#    zUndefThreshold: the default value is zUndef +10. It is tied with the value -100. given
#                     by the interpolation when no value is defined.
#
  zUndefThreshold = zUndef + 10.
  if verbose:
    ligne = "zUndefThreshold: %f" % zUndefThreshold
    print (ligne)
#
  #print "bathy :\n", bathy
  np_aux_z = (bathy < zUndefThreshold).nonzero()
  if verbose:
    ligne = ".. Number of nodes below the minimum: %d" % len(np_aux_z[0])
    print (ligne)
#
# 6. Option : xyz file
#
  if xyzFile:
    if verbose:
      ligne = ".. Ecriture du champ de bathymétrie sur le fichier :\n%s" % fichierFonds
      print (ligne)
#
    with open(fichierFonds, "w") as fo :
      for nodeId in range(nbnodes):
        ligne = "%11.3f %11.3f %11.3f\n" % (coords[nodeId, 0], coords[nodeId, 1], bathy[nodeId])
        fo.write(ligne)
#
# 7. Final MED file
# 7.1. Transformation of the bathymetry as a double array
#
  bathy_dd = mc.DataArrayDouble(np.asfarray(bathy, dtype='float'))
  bathy_dd.setInfoOnComponents(["Z [m]"])
#
# 7.2. If requested, modification of the z coordinate
#
  if m3d:
    coords3D = ml.DataArrayDouble.Meld([coords[:,0:2], bathy_dd])
    coords3D.setInfoOnComponents(["X [m]", "Y [m]", "Z [m]"])
    #print "coords3D =\n", coords3D
    meshMEDFileRead.setCoords(coords3D)
#
# 7.3. Writes the mesh
#
  if verbose:
    if m3d:
      saux = " 3D"
    else:
      saux = ""
    ligne = ".. Ecriture du maillage" + saux
    ligne += " sur le fichier :\n%s" % fichierFMaillage
    print (ligne)
#
  meshMEDFileRead.write(fichierFMaillage, 2)
#
# 7.4. Writes the field
#
  med_field_name = "BOTTOM"
  if verbose:
    ligne = ".. Ecriture du champ '%s'" % med_field_name
    print (ligne)
#
  #print "bathy_dd =\n", bathy_dd
  fieldOnNodes = ml.MEDCouplingFieldDouble(ml.ON_NODES)
  fieldOnNodes.setName(med_field_name)
  fieldOnNodes.setMesh(meshMEDFileRead.getMeshAtLevel(0))
  fieldOnNodes.setArray(bathy_dd)
# Ces valeurs d'instants sont mises pour assurer la lecture par TELEMAC
# instant = 0.0
# numero d'itération : 0
# pas de numero d'ordre (-1)
  fieldOnNodes.setTime(0.0, 0, -1)
#
  fMEDFile_ch_d = ml.MEDFileField1TS()
  fMEDFile_ch_d.setFieldNoProfileSBT(fieldOnNodes)
  fMEDFile_ch_d.write(fichierFMaillage, 0)
#
  return statz

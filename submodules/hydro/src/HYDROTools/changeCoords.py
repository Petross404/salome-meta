
import shutil
import numpy as np
import MEDLoader as ml
import medcoupling as mc

from pyproj import CRS
from pyproj import Transformer

def changeCoords(fileIn, fileOut, epsgIn=2154, epsgOut=2154, offsetXin=0, offsetYin=0, offsetXout=0, offsetYout=0):
    """
    change the coordinates of a MED mesh file, with an optional offset reference in Input, and an optional change of system of coordinates (for instance Lambert II --> Lambert 93). The system of coordinates are given with EPSG (European Petroleum Survey Group) integer codes. The transformation uses pyproj.

    When there is a change of system of coordinates, input offset should be given in order to get the correct input coordinates before transformation. example, with an input file in LambertII and an origin offset of (500000, 6000000), to obtain the coordinates in Lambert93 without origin offset: changeCoords("medfileLambertII.med", "medfileLambert93.med", 27572, 2154, 500000, 6000000, 0, 0)

    When there is only an origin offset change, use the same code for epsgIn and epsgOut.
    The operation is: Xout = Xin + offsetXin -offsetXout, Yout = Yin + offsetYin -offsetYout.
    If the epsgIn and epsgOut codes are equal, the transformation of system of coordinates (pyproj) is not called.

    parameters:
    fileIn: full path of input MED file
    fileOut: full path of output MED file
    epsgIn: integer code of the EPSG for the input system of coordinates (for instance, Lambert II is 27572)
    epsgOut: integer code of the EPSG for the output system of coordinates (for instance, Lambert 93 is 2154)
    offsetXin: X origin in the input system of coordinates (default 0)
    offsetYin: Y origin in the input system of coordinates (default 0)
    offsetXout: X origin in the output system of coordinates (default 0)
    offsetYout: Y origin in the output system of coordinates (default 0)
    return:
    O if OK
    """
    # TODO: do we need to transform the exception in return codes ?
    if fileOut != fileIn:
        shutil.copyfile(fileIn, fileOut)

    if epsgIn != epsgOut:
        crs_in = CRS.from_epsg(epsgIn)
        crs_out = CRS.from_epsg(epsgOut)
        transformer = Transformer.from_crs(crs_in, crs_out)

    meshMEDFileRead = ml.MEDFileMesh.New(fileIn)
    coords = meshMEDFileRead.getCoords()
    nb_comp = coords.getNumberOfComponents()

    vx = coords[:,0]
    vy = coords[:,1]
    avx = vx.toNumPyArray() + offsetXin
    avy = vy.toNumPyArray() + offsetYin

    if nb_comp == 3:
        vz = coords[:,2]
        avz = vz.toNumPyArray()

    if epsgIn != epsgOut:
        navx, navy = transformer.transform(avx, avy)
    else:
        navx = avx
        navy = avy
    navx = navx - offsetXout
    navy = navy - offsetYout

    if nb_comp == 3:
        navxy = np.stack((navx, navy, avz), axis=-1)
        ncoords = mc.DataArrayDouble(navxy)
        ncoords.setInfoOnComponents(["X [m]", "Y [m]", "Z [m]"])
    else:
        navxy = np.stack((navx, navy), axis=-1)
        ncoords = mc.DataArrayDouble(navxy)
        ncoords.setInfoOnComponents(["X [m]", "Y [m]"])
    meshMEDFileRead.setCoords(ncoords)

    meshMEDFileRead.write(fileOut, 0)
    return 0

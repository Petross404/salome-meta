#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import salome

salome.salome_init()

import SalomePyQt
sgPyQt = SalomePyQt.SalomePyQt()
import libSALOME_Swig
salome_gui = libSALOME_Swig.SALOMEGUI_Swig()

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from HYDROPy import *
import json

from salome.hydrotools.shapesGroups import freeBordersGroup, exploreEdgeGroups
from salome.hydrotools.hydroGeoMeshUtils import importPolylines

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

hydro_root = os.path.join(os.environ['HYDRO_ROOT_DIR'], 'share', 'salome', 'plugins', 'hydro', 'plugins')
hydro_resources = os.path.join(os.environ['HYDRO_ROOT_DIR'], 'share', 'salome', 'resources', 'hydro')

class meshEdgesToShapesDialog(QDialog):
    """
    This dialog is used to extract all groups of edges from a mesh, plus all the border (free) edges,
    and write the groups as shapefiles.
    """

    def __init__(self, parent = None, modal = 0):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(hydro_root, 'meshEdgesToShapes.ui'), self )

        # Connections
        self.pb_medFile.clicked.connect(self.on_med_file_browse)
        self.tb_medFile.setIcon(QIcon(QPixmap(os.path.join(hydro_resources, "icon_select.png"))))
        self.tb_medFile.clicked.connect(self.on_select_med_file)
        self.pb_outDir.clicked.connect(self.on_outputDir_browse)
        self.pb_help.clicked.connect(self.on_help)
        self.pb_ok.accepted.connect(self.on_accept)
        self.pb_ok.rejected.connect(self.on_reject)
        self.cb_keepOutMed.setChecked(True)
        self.medFile = None
        self.outDir = None
        
    def get_selected_mesh(self):
        """
        Select a mesh in the object browser and return associated filename
        """
        nbsel = salome.sg.SelectedCount()
        if nbsel != 1:
            return ""
        sel=salome.sg.getSelected(0)
        so=salome.myStudy.FindObjectID(sel)
        if so is None:
            return ""
        obj=so.GetObject()
        smesh = smeshBuilder.New()
        smesh.SetEnablePublish( False )
        mesh1 = smesh.Mesh()
        smesh.SetEnablePublish( True )
        if not isinstance(obj, type(mesh1.GetMesh())):
            return ""
        filename = obj.GetMesh().GetMEDFileInfo().fileName
        return filename
    
    def on_select_med_file(self):
        """
        set selected mesh filename on dialog
        """
        filename = self.get_selected_mesh()
        print("selected mesh: %s"%filename)
        if filename != "":
            self.medFile = filename
            self.le_medFile.setText(self.medFile)
        
    def on_med_file_browse(self):
        """
        Select MED file in file system
        """
        print("on_med_file_browse")
        self.medFile, filt = QFileDialog.getOpenFileName(self, self.tr("Input MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFile)
        if not self.medFile:
            return
        self.le_medFile.setText(self.medFile)
        
    def on_outputDir_browse(self):
        """
        Select OutputDirectory
        """
        print("on_ouptutDir_browse")
        self.outDir = QFileDialog.getExistingDirectory(self, self.tr("Output Directory"), "")
        print(self.outDir)
        if not self.outDir:
            return
        self.le_outDir.setText(self.outDir)
        
    def on_help(self):
        """
        display a help message
        """
        msg = """
        <h2>Mesh Edges to Shapes Dialog</h2>

        This dialog is used to extract all groups of edges from a mesh, plus all the border (free) edges,
        and write the groups as shapefiles.
        <br><br>
        The free edges regroup the external border of the mesh, and all the internal borders (isles).
        A group containing the free edges is added to the mesh.
        <br>        
        The mesh is saved in a new file, in an ouput directory used also to store the shapefiles.
        <br>        
        A shapefile of edges and a shapefile of points are generated for each group of edges.
        The shapefiles are intended to be loaded in a SIG tool (Qgis) and should preferaby be set in a correct coordinates system.
        <br>
        If the mesh uses a local coordinate system with an origin offset, the coordinates of this origin should be set in the dialog.
        <br><br>         
        Below is the description of the dialog controls.

        <h3>MED file</h3>
        This field allows selection of a med file (via the standard file open dialog).
        The filling of this field is mandatory.
        
        <h3>offsetX, offsetY</h3>
        These fields are used to set the Origin of the local coordinates system of the mesh, if any. 

        <h3>Output directory</h3>
        This field allows selection of a directory to store the shapes and the outputMesh.

        <h3>Keep output MED file</h3>
        If this checkbox is unchecked, the output mesh will be removed after calculation of the shapes.
        """
        QMessageBox.about(self, self.tr("About mesh edges to shapes dialog"), msg);
       
   
    def on_accept(self):
        print("accept")
        medFile = self.le_medFile.text()
        outDir = self.le_outDir.text()
        offsetX = self.dsb_offsetX.value()
        offsetY = self.dsb_offsetY.value()
        isOutMedKept = self.cb_keepOutMed.isChecked()
        isLoadFreeBorders = self.cb_loadFreeBorders.isChecked()
        isLoadOthers = self.cb_loadOthers.isChecked()
        self.accept()
        print(medFile)
        print(outDir)
        print(isOutMedKept)
        print(offsetX, offsetY)
        a = os.path.splitext(medFile)
        medFileOut = os.path.join(outDir, os.path.basename(a[0]) + '_brd' + a[1])
        print(medFileOut)
        smesh = smeshBuilder.New()
        smesh.SetEnablePublish( False )
        medFileOut = freeBordersGroup(medFile, medFileOut)
        exploreEdgeGroups(medFileOut, outDir, offsetX, offsetY)
        smesh.SetEnablePublish( True )
        if not isOutMedKept:
            print("remove", medFileOut)
            os.remove(medFileOut)
        shapesListFile = os.path.join(outDir, "shapesList.json")
        fileShapes = []
        with open(shapesListFile, 'r') as f:
            fileShapes = json.load(f)
        print(fileShapes)
        hydro_doc = HYDROData_Document.Document()
        l = []
        if isLoadFreeBorders:
            l = l + [ a for a in fileShapes if "FreeBorders.shp" in a]
        if isLoadOthers:
            l = l + [ a for a in fileShapes if "FreeBorders.shp" not in a]
        for fileShape in l:
            a = os.path.splitext(os.path.basename(fileShape))            
            shapeName = a[0]
            shapes = importPolylines(hydro_doc, fileShape, 0, 0)
        if salome.sg.hasDesktop():
            salome.sg.updateObjBrowser()
            

    def on_reject(self):
        print("reject")
        self.reject()
        

def execDialog(context):
    print("execDialog")
    desktop = sgPyQt.getDesktop()
    dlg = meshEdgesToShapesDialog(desktop)
    dlg.show()        

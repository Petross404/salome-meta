#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import salome

salome.salome_init()

import SalomePyQt
sgPyQt = SalomePyQt.SalomePyQt()
import libSALOME_Swig
salome_gui = libSALOME_Swig.SALOMEGUI_Swig()

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from HYDROPy import *

from salome.hydrotools.shapesGroups import fitShapePointsToMesh
from salome.hydrotools.hydroGeoMeshUtils import importPolylines

import tempfile

hydro_root = os.path.join(os.environ['HYDRO_ROOT_DIR'], 'share', 'salome', 'plugins', 'hydro', 'plugins')
hydro_resources = os.path.join(os.environ['HYDRO_ROOT_DIR'], 'share', 'salome', 'resources', 'hydro')

class fitShapePointsToMeshEdgesDialog(QDialog):
    """
    This dialog is used to adjust a shapefile crossing another shapefile corresponding to edges of a mesh, for instance the free borders.
    the shapeFile to adjust must be a closed line or polygon crossing the free border shapefile in 2 points.
    """

    def __init__(self, parent = None, modal = 0):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(hydro_root, 'fitShapePointsToMeshEdges.ui'), self )

        # Connections
        self.tb_meshEdges.clicked.connect(self.on_select_meshEdges)
        self.tb_shapeToAdjust.clicked.connect(self.on_select_shapeToAdjust)
        self.tb_meshEdges.setIcon(QIcon(QPixmap(os.path.join(hydro_resources, "icon_select.png"))))
        self.tb_shapeToAdjust.setIcon(QIcon(QPixmap(os.path.join(hydro_resources, "icon_select.png"))))
        self.pb_meshEdges.clicked.connect(self.on_meshEdges_browse)
        self.pb_shapeToAdjust.clicked.connect(self.on_shapeToAdjust_browse)
        self.pb_outDir.clicked.connect(self.on_outputDir_browse)
        self.pb_help.clicked.connect(self.on_help)
        self.pb_ok.accepted.connect(self.on_accept)
        self.pb_ok.rejected.connect(self.on_reject)
        self.cb_splineMeshEdges.setEnabled(False)
        self.cb_splineShapeToAdjust.setEnabled(False)
        self.meshEdges = None
        self.shapeToAdjust = None
        self.tmpdir = tempfile.mkdtemp()
        print("tmpdir=",self.tmpdir)

        
    def get_selected_polyline(self):
        """
        Select a polyline2D in the HYDRO object browser
        """
        ind = SalomePyQt.SalomePyQt.getObjectBrowser().selectionModel().selectedIndexes()
        doc = HYDROData_Document.Document()
        for i in ind:
            if i.column()==0:
                name = str(i.data())
                case = doc.FindObjectByName( name )
                if isinstance(case, HYDROData_PolylineXY):
                    print("selected %s"%name)
                    return name
        return None
    
    def on_select_meshEdges(self):
        """
        Get selected Polyline in the HYDRO object browser
        """
        name = self.get_selected_polyline()
        if name is None:
            return 
        doc = HYDROData_Document.Document()
        polyXY = doc.FindObjectByName(name)
        if polyXY is None:
            return
        self.meshEdges = os.path.join(self.tmpdir, name + ".shp")
        res = HYDROData_PolylineXY.ExportShapeXY(doc, self.meshEdges, [polyXY])
        self.le_meshEdges.setText(self.meshEdges)
        
    def on_select_shapeToAdjust(self):
        """
        Get selected Polyline in the HYDRO object browser
        """
        name = self.get_selected_polyline()
        if name is None:
            return 
        doc = HYDROData_Document.Document()
        polyXY = doc.FindObjectByName(name)
        if polyXY is None:
            return
        self.shapeToAdjust = os.path.join(self.tmpdir, name + ".shp")
        res = HYDROData_PolylineXY.ExportShapeXY(doc, self.shapeToAdjust, [polyXY])
        self.le_shapeToAdjust.setText(self.shapeToAdjust)

    def on_meshEdges_browse(self):
        """
        Select Shapefile of mesh edges
        """
        print("on_meshEdges_browse")
        self.meshEdges, filt = QFileDialog.getOpenFileName(self, self.tr("shapefile of mesh border edges"), "", self.tr("shapefiles (*.shp)"))
        print(self.meshEdges)
        if not self.meshEdges:
            return
        self.le_meshEdges.setText(self.meshEdges)
        
    def on_shapeToAdjust_browse(self):
        """
        Select shapefile to adjust
        """
        print("on_shapeToAdjust_browse")
        self.shapeToAdjust, filt = QFileDialog.getOpenFileName(self, self.tr("shapefile to adjust"), "", self.tr("shapefiles (*.shp)"))
        print(self.shapeToAdjust)
        if not self.shapeToAdjust:
            return
        self.le_shapeToAdjust.setText(self.shapeToAdjust)
        self.le_outDir.setText(os.path.dirname(self.shapeToAdjust))
        
    def on_outputDir_browse(self):
        """
        Select OutputDirectory
        """
        print("on_ouptutDir_browse")
        self.outDir = QFileDialog.getExistingDirectory(self, self.tr("Output Directory"), "")
        print(self.outDir)
        if not self.outDir:
            return
        self.le_outDir.setText(self.outDir)
        
    def on_help(self):
        """
        display a help message
        """
        msg = """
        <h2>Fit shape points to mesh edges dialog</h2>

        This dialog is used to adjust a shapefile crossing another shapefile corresponding to edges of a mesh, for instance the free borders.
        the shapeFile to adjust must be a closed line or polygon crossing the free border shapefile in 2 points.
        <br><br>
        The algorithm find in the shapefile to adjust and in the free border shapefile the two closest pairs of corresponding points 
        and move the points in the shapefile to adjust to correspond to the points found in the free border shapefile.
        <br>
        If requested, it splits the free border shapefile and/or the shapefile adjusted in two parts, at their intersection.
        <br>
        The shapefile adusted is written by default in the directory of the shapefile to adjust, with a suffix '_adj'.
        <br>
        The split free border shapefile is written in the directory of the free border shapefile, with a suffix '_split'.
        <br><br>         
        Below is the description of the dialog controls.

        <h3>Mesh edges shapefile</h3>
        This field allows selection of a shapefile (via the standard file open dialog or by selection in the Object Browser).
        <h3>Shapefile to adjust</h3>
        This field allows selection of a shapefile (via the standard file open dialog or by selection in the Object Browser).
        <h3>Output dir</h3>
        This field allows selection of the directory used to store the results.
        <h3>Split mesh edges shapefile</h3>
        If this checkbox is checked, the mesh edges shapefile is split at the intersection with the other shapefile.
        <h3>Split shapefile to adjust</h3>
        If this checkbox is checked, the shapefile to adjust is split at the intersection with the other shapefile.
        <h3>Load result</h3>
        If this checkbox is checked, the corresponding shapefile is loaded.
        <h3>Load as spline</h3>
        If this checkbox is checked, the corresponding shapefile is loaded as a spline.
        """
        QMessageBox.about(self, self.tr("About fit shape points to mesh edges dialog"), msg);
       
   
    def on_accept(self):
        print("accept")
        meshEdges = self.le_meshEdges.text()
        shapeToAdjust = self.le_shapeToAdjust.text()
        isMeshEdgesSplit = self.cb_splitMeshEdges.isChecked()
        isShapeToAdjustSplit = self.cb_splitShapeToAdjust.isChecked()
        isLoadMeshEdges = self.cb_loadMeshEdges.isChecked()
        isLoadShapeToAdjust = self.cb_loadShapeToAdjust.isChecked()
        isSplineMeshEdges = self.cb_splineMeshEdges.isChecked()
        isSplineShapeToAdjust = self.cb_splineShapeToAdjust.isChecked()
        outdir = self.le_outDir.text()
        if not os.path.isdir(outdir):
            msgBox = QMessageBox()
            msgBox.setText( "Output directory is not set, please select" )
            msgBox.exec_()
            return
        self.accept()
        print(meshEdges)
        print(shapeToAdjust)
        print(isMeshEdgesSplit)
        print(isShapeToAdjustSplit)
        fitShapePointsToMesh(meshEdges, shapeToAdjust, outdir, isMeshEdgesSplit, isShapeToAdjustSplit)
        hydro_doc = HYDROData_Document.Document()
        if isLoadMeshEdges:
            a = os.path.splitext(os.path.basename(meshEdges))
            ext = ""
            if isMeshEdgesSplit:
                ext = "_split"
            meshEdgesShapefile = os.path.join(outdir, a[0] + ext + a[1]) 
            shapeName = a[0] #+ ext + "_PolyXY"
            print("isSplineMeshEdges %s"%isSplineMeshEdges)
            meshEdgesShapes = importPolylines(hydro_doc, meshEdgesShapefile, isSplineMeshEdges, 0)
        if isLoadShapeToAdjust:            
            a = os.path.splitext(os.path.basename(shapeToAdjust))            
            ext = "_adj"
            adjustedShapefile = os.path.join(outdir, a[0] + ext + a[1]) 
            shapeName = a[0] #+ ext + "_PolyXY"
            print("isSplineShapeToAdjust %s"%isSplineShapeToAdjust)
            adjustedShapes = importPolylines(hydro_doc, adjustedShapefile, isSplineShapeToAdjust, 0)
        if salome.sg.hasDesktop():
            salome.sg.updateObjBrowser()


    def on_reject(self):
        print("reject")
        self.reject()
        

def execDialog(context):
    print("execDialog")
    desktop = sgPyQt.getDesktop()
    dlg = fitShapePointsToMeshEdgesDialog(desktop)
    dlg.show()        

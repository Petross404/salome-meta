#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import salome

salome.salome_init()

import SalomePyQt
sgPyQt = SalomePyQt.SalomePyQt()
import libSALOME_Swig
salome_gui = libSALOME_Swig.SALOMEGUI_Swig()

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from HYDROPy import *
import json

import tempfile

from salome.hydrotools.hydroGeoMeshUtils import importPolylines
from salome.hydrotools.changeCoords import changeCoords
from salome.hydrotools.cutMesh import cutMesh

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

hydro_root = os.path.join(os.environ['HYDRO_ROOT_DIR'], 'share', 'salome', 'plugins', 'hydro', 'plugins')
hydro_resources = os.path.join(os.environ['HYDRO_ROOT_DIR'], 'share', 'salome', 'resources', 'hydro')

class cutMeshDialog(QDialog):
    """
    This dialog is used to extract all groups of edges from a mesh, plus all the border (free) edges,
    and write the groups as shapefiles.
    """

    def __init__(self, parent = None, modal = 0):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(hydro_root, 'cutMesh.ui'), self )

        # Connections
        self.pb_medFile.clicked.connect(self.on_med_file_browse)
        self.tb_medFile.setIcon(QIcon(QPixmap(os.path.join(hydro_resources, "icon_select.png"))))
        self.tb_medFile.clicked.connect(self.on_select_med_file)
        self.pb_shapeCut.clicked.connect(self.on_shape_file_browse)
        self.tb_shapeCut.setIcon(QIcon(QPixmap(os.path.join(hydro_resources, "icon_select.png"))))
        self.tb_shapeCut.clicked.connect(self.on_select_shape_file)
        self.pb_outDir.clicked.connect(self.on_outputDir_browse)
        self.pb_help.clicked.connect(self.on_help)
        self.pb_ok.accepted.connect(self.on_accept)
        self.pb_ok.rejected.connect(self.on_reject)
        self.medFile = None
        self.outDir = None
        self.tmpdir = tempfile.mkdtemp()
        print("tmpdir=",self.tmpdir)
        
    def get_selected_mesh(self):
        """
        Select a mesh in the object browser and return associated filename
        """
        nbsel = salome.sg.SelectedCount()
        if nbsel != 1:
            return ""
        sel=salome.sg.getSelected(0)
        so=salome.myStudy.FindObjectID(sel)
        if so is None:
            return ""
        obj=so.GetObject()
        smesh = smeshBuilder.New()
        smesh.SetEnablePublish( False )
        mesh1 = smesh.Mesh()
        smesh.SetEnablePublish( True )
        if not isinstance(obj, type(mesh1.GetMesh())):
            return ""
        filename = obj.GetMesh().GetMEDFileInfo().fileName
        return filename
    
    def on_select_med_file(self):
        """
        set selected mesh filename on dialog
        """
        filename = self.get_selected_mesh()
        print("selected mesh: %s"%filename)
        if filename != "":
            self.medFile = filename
            self.le_medFile.setText(self.medFile)
        
    def on_med_file_browse(self):
        """
        Select MED file in file system
        """
        print("on_med_file_browse")
        self.medFile, filt = QFileDialog.getOpenFileName(self, self.tr("Input MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFile)
        if not self.medFile:
            return
        self.le_medFile.setText(self.medFile)

    def get_selected_polyline(self):
        """
        Select a polyline2D in the HYDRO object browser
        """
        ind = SalomePyQt.SalomePyQt.getObjectBrowser().selectionModel().selectedIndexes()
        doc = HYDROData_Document.Document()
        for i in ind:
            if i.column()==0:
                name = str(i.data())
                case = doc.FindObjectByName( name )
                if isinstance(case, HYDROData_PolylineXY):
                    print("selected %s"%name)
                    return name
        return None
    
    def on_select_shape_file(self):
        """
        Get selected Polyline in the HYDRO object browser
        """
        name = self.get_selected_polyline()
        if name is None:
            return 
        doc = HYDROData_Document.Document()
        polyXY = doc.FindObjectByName(name)
        if polyXY is None:
            return
        self.shapeCut = os.path.join(self.tmpdir, name + ".shp")
        res = HYDROData_PolylineXY.ExportShapeXY(doc, self.shapeCut, [polyXY])
        self.le_shapeCut.setText(self.shapeCut)

    def on_shape_file_browse(self):
        """
        Select shapefile to cut mesh
        """
        print("on_shape_file_browse")
        self.shapeCut, filt = QFileDialog.getOpenFileName(self, self.tr("shapefile of mesh border edges"), "", self.tr("shapefiles (*.shp)"))
        print(self.shapeCut)
        if not self.shapeCut:
            return
        self.le_shapeCut.setText(self.shapeCut)


    def on_outputDir_browse(self):
        """
        Select OutputDirectory
        """
        print("on_ouptutDir_browse")
        self.outDir = QFileDialog.getExistingDirectory(self, self.tr("Output Directory"), "")
        print(self.outDir)
        if not self.outDir:
            return
        self.le_outDir.setText(self.outDir)
        
    def on_help(self):
        """
        display a help message
        """
        msg = """
        <h2>Cut a mesh by a polyline shape Dialog</h2>

        This dialog is used to cut a mesh using a polygonal shape, removing all the nodes and elements inside the shape.
        <br><br>
        The shape should be in the same system of coordinates of the mesh, without origin offset.
        <br>        
        The mesh is saved in a new file.
        <br>
        If the mesh uses a local coordinate system with an origin offset, the coordinates of this origin should be set in the dialog.
        <br><br>         
        Below is the description of the dialog controls.

        <h3>MED file</h3>
        This field allows selection of a med file (via the standard file open dialog).
        The filling of this field is mandatory.
        
        <h3>offsetX, offsetY</h3>
        These fields are used to set the Origin of the local coordinates system of the mesh, if any. 

        <h3>Cutting shapefile</h3>
        Select the shape via the standard file open dialog or by selection in the object browser.
        
        <h3>Output directory</h3>
        This field allows selection of a directory to store the shapes and the outputMesh.

        <h3>offsetX, offsetY</h3>
        These fields are used to set the Origin of the local coordinates system of the output mesh, if any. 
        """
        QMessageBox.about(self, self.tr("About cut mesh by shape dialog"), msg);
       
   
    def on_accept(self):
        print("accept")
        medFile = self.le_medFile.text()
        offsetX = self.dsb_offsetX.value()
        offsetY = self.dsb_offsetY.value()
        shapeCut = self.le_shapeCut.text()
        outDir = self.le_outDir.text()
        outOffsetX = self.dsb_outOffsetX.value()
        outOffsetY = self.dsb_outOffsetY.value()
        self.accept()
        print(medFile)
        print(offsetX, offsetY)
        print(shapeCut)
        print(outDir)
        print(outOffsetX, outOffsetY)
        a = os.path.splitext(medFile)
        
        medFileTrans = os.path.join(self.tmpdir, os.path.basename(a[0])+'_trs.med')
        changeCoords(medFile, medFileTrans, 2154, 2154, offsetX, offsetY, 0, 0)
        
        medFileOut = os.path.join(outDir, os.path.basename(a[0]) + '_cut' + a[1])
        print(medFileOut)
        meshFileOut = cutMesh(medFileTrans,
                              shapeCut,
                              medFileOut, outOffsetX, outOffsetY)
        
        if salome.sg.hasDesktop():
            salome.sg.updateObjBrowser()
            

    def on_reject(self):
        print("reject")
        self.reject()
        

def execDialog(context):
    print("execDialog")
    desktop = sgPyQt.getDesktop()
    dlg = cutMeshDialog(desktop)
    dlg.show()        

import sys
import os
import salome

salome.salome_init()

from HYDROPy import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from random import randint

def getChildrenInStudy(obj):
    """
    Given an object published in SALOME study (for instance a GEOM object), retreive its children.
    parameters:
    obj: a SALOME object published in SALOME study
    return:
    a dictionary [name] --> object
    """
    SO = salome.myStudy.FindObjectIOR(salome.myStudy.ConvertObjectToIOR(obj))
    childIterator = salome.myStudy.NewChildIterator(SO)
    children = {}
    while childIterator.More():
        childItem = childIterator.Value()
        print("item", childItem)
        itemName = childItem.GetName()
        itemID = childItem.GetID()
        itemObj = salome.IDToObject(str(itemID))
        children[itemName] = itemObj
        childIterator.Next()
    return children

def loadImage(document, imageFile, imageName, displayLevel):
    """
    load an image (photo, map...) in order to georeference it and use it as background for drawing shapes
    parameters:
    document: current HYDROData document
    imageFile: full path of the image file to load
    imageName: name to give to the object loaded in the HYDRO document
    displayLevel: integer >=0, the higher levels are displayed on top
    return:
    the object image loaded
    """
    image = document.CreateObject(KIND_IMAGE)
    image.SetName(imageName)
    image.SetZLevel(displayLevel)
    if not(image.LoadImage(imageFile)):
        raise ValueError('problem while loading image %s'%imageFile)
    image.Update()
    return image
    
def GeolocaliseImageCoords(image, a, b, c ,d):
    """
    Geolocalize a loaded image with to sets of points: pixels coordinates <--> absolutes coordinates in the geographic reference system.
    parameters:
    image: loaded image to localize
    a: first pixel coordinates QPoint(x, y) (integers)
    b: second pixel coordinates QPoint(x, y) (integers)
    c: first geographic point QPointF(X, Y) (floats)
    d: second geographic point QPointF(X, Y) (floats)
    """
    image.SetLocalPoints(a, b)
    image.SetGlobalPoints(1, c, d)
    image.Update()

def GeolocaliseImageReference(image, imageRef, a, b, c ,d):
    """
    Geolocalize an image using another one already localized, with two pair of corresponding pixels on the images
    parameters:
    image: loaded image to localize
    imageRef: loaded image used as a reference
    a, b : pixel coordinates on the image to localize QPoint(x, y) (integers) 
    c, d : pixel coordinates on the reference image QPoint(x, y) (integers) 
    """
    image.SetLocalPoints(a, b)
    image.SetGlobalPoints(3, c, d)
    image.SetTrsfReferenceImage(imageRef)
    image.Update()
    
def importPolylines(document, shapeFile, iSpline, displayLevel):
    """
    Import a QGis shape (*.shp) containing one or several 2D polylines or polygons.
    The polyline is either named from its name in the shape database if existing, or using the shapefile basename
    with a suffix '_PolyXY'.
    In case of several polylines, name are suffixed in sequence with '_n'.
    parameters:
    document: current HYDROData document
    shapeFile: full path of the shapefile (*.shp)
    isSpline: boolean to convert the polylines in  splines
    displayLevel: integer >=0, the higher levels are displayed on top
    return:
    list of shape objects
    """
    entities = HYDROData_PolylineXY.ImportShapesFromFile(shapeFile)
    shapeType = 0 # polyline
    if iSpline:
        shapeType = 1 # polyline
    shapes = []
    for shape in entities:
        print(shape.GetName())
        if shape.GetKind() == KIND_POLYLINEXY:
            print("polyline %s"%shape.GetName())
            for i in range(shape.NbSections()):
                shape.SetSectionType(i, shapeType)
                shape.Update()
                shape.SetZLevel(displayLevel)
            shapes.append(shape)
    return shapes

def splitShapeTool(document, aShape, toolShape, tolerance=1.e-2):
    """
    Split the shape (2D polyline or spline) by the shape tool.
    return a list of all the split shapes, named after their original shape name, with a sequence suffix _n
    parameters;
    document: current HYDROData document
    aShape: a loaded shape (2D polyline or spline)
    toolShape: a loaded shape (2D polyline or spline)
    tolerance: used by the algorithm to detect intersections, default value 1.e-2
    return:
    a list of all the split shapes, named after their original shape name, with a sequence suffix _n
    """
    name = aShape.GetName()
    existingNbSplit = {}
    index = 1
    found = True
    while found:
        nameIndex = name + "_%d"%(index)
        shape = document.FindObjectByName(nameIndex)
        if shape is None:
            found = False
        else:
            index = index + 1
    existingNbSplit[name] = index
    print(existingNbSplit)
    
    op = HYDROData_PolylineOperator()
    op.SplitTool(document, aShape, toolShape, tolerance)
    
    listSplit = []
    index = existingNbSplit[name]
    found = True
    while found:
        nameIndex = name + "_%d"%(index)
        shape = document.FindObjectByName(nameIndex)
        if shape is None:
            found = False
        else:
            print("found %s"%nameIndex)
            index = index + 1
            listSplit.append(shape)
    return listSplit

def splitShapesAll(document, shapeslist, precision=1.E-2):
    """
    Split all the shapes (2D polylines or splines) in the list by all the other shapes.
    return a list of all the split shapes, named after their original shape name, with a sequence suffix _n
    parameters;
    document: current HYDROData document
    shapeslist: a list of loaded shapes (2D polylines or splines)
    precision: used by the algorithm to detect intersections, default value 1.e-2
    return:
    a list of all the split shapes, named after their original shape name, with a sequence suffix _n
    """
    names = [s.GetName() for s in shapeslist]
    
    existingNbSplit = {}
    for name in names:
        index = 1
        found = True
        while found:
            nameIndex = name + "_%d"%(index)
            shape = document.FindObjectByName(nameIndex)
            if shape is None:
                found = False
            else:
                index = index + 1
        existingNbSplit[name] = index
    print(existingNbSplit)

    op = HYDROData_PolylineOperator()
    op.SplitAll(document, shapeslist, precision)

    listSplit = []
    for name in names:
        index = existingNbSplit[name]
        found = True
        while found:
            nameIndex = name + "_%d"%(index)
            shape = document.FindObjectByName(nameIndex)
            if shape is None:
                found = False
            else:
                print("found %s"%nameIndex)
                index = index + 1
                listSplit.append(shape)
    return listSplit
    
    
def importBathymetry(document, bathyFile):
    """
    import a bathymetry file (*.xyz or *.asc)
    parameters:
    document: current HYDROData document
    bathyFile: full path of the bathymetry (*.xyz or *.shp)
    return:
    the bathymetry object
    """
    bathy = document.CreateObject(KIND_BATHYMETRY)
    a = os.path.splitext(bathyFile)
    bathyName = os.path.basename(a[0])
    bathy.SetName(bathyName)
    bathy.SetAltitudesInverted(0)
    if not(bathy.ImportFromFile(bathyFile)):
        raise ValueError('problem while loading bathymetry')
    bathy.Update()
    return bathy

def createImmersibleZone(document, imzName, polyLine, bathy, isImmersible, displayLevel):
    """
    Create an immersible or unsubmersible zone with a closed polyline and a bathymetry.
    parameters:
    document: current HYDROData document
    imzName: the name to give to the zone object
    polyline: the closed 2D polyline or spline delimiting the zone
    bathy: the bathymetry to associate to the zone
    isImmersible: boolean
    displayLevel: integer >=0, the higher levels are displayed on top
    return:
    the zone created
    """
    imz = document.CreateObject(KIND_IMMERSIBLE_ZONE)
    imz.SetName(imzName)
    imz.SetZLevel(displayLevel)
    if bathy is not None:
        imz.SetAltitudeObject(bathy)
    imz.SetPolyline(polyLine)
    imz.SetIsSubmersible(isImmersible)
    imz.SetFillingColor(QColor(randint(0,255), randint(0,255), randint(0,255), 255))
    imz.Update()
    return imz

def mergePolylines(document, polyName, polyLines, isConnectedBySegment = False, tolerance = 1.E-3):
    """
    Regroup several 2D polylines or spline in one, with several sections
    parameters:
    document: current HYDROData document
    polyName: name to give to the new object
    polyLines: a list of loaded shapes (2D polylines or splines)
    isConnectedBySegment: boolean, to add a segment connecting the polylines, default False
    tolerance: used by the algorithm to detect coincidences, default 1.E-3
    return:
    the merged polyline
    """
    op=HYDROData_PolylineOperator()
    op.Merge(document, polyName, polyLines, isConnectedBySegment, tolerance)
    shape = document.FindObjectByName(polyName)
    return shape

def createAxis3DDEmbankmentAltiProfile(document, axis3DName, axisXY, altiPts):
    """
    Create a 3D polyline to be used as an axis for an embankement, using a 2D axis and a list of altitudes along the axis.
    parameters:
    document: current HYDROData_Document
    axis3DName: name to give to the axis3D created
    axisXY: 2D polyline (spline), Embankment line
    altiPts: list of tuples(x, h) : parametric coordinate, altitude, along the axisXY
    return:
    altiProfile, axis3D
    """
    altiProfile = document.CreateObject(KIND_PROFILE)
    altiProfile.SetName("%s_altiProfile"%axis3DName)
    altiProfilePoints = []
    for p in altiPts:
        altiProfilePoints.append(gp_XY(p[0], p[1]))
    altiProfile.SetParametricPoints(altiProfilePoints)
    altiProfile.Update()
    axis3D = document.CreateObject(KIND_POLYLINE)
    axis3D.SetName(axis3DName)
    axis3D.SetPolylineXY(axisXY)
    axis3D.SetProfileUZ(altiProfile.GetProfileUZ())
    axis3D.Update()
    return (altiProfile, axis3D)
    
def createAxis3DDEmbankmentBathy(document, axis3DName, axisXY, aCloud):
    """
    Create a 3D polyline to be used as an axis for an embankement, using a 2D axis and a bathymetry to project the axis.
    parameters:
    document: current HYDROData_Document
    axis3DName: name to give to the axis3D created
    axisXY: imported polyline, Embankment line
    aCloud:  altimetry, for projection of axisXY
    return:
    altiProfile, axis3D
    """
    altiProfile = document.CreateObject(KIND_PROFILE)
    altiProfile.SetName("%s_altiProfile"%axis3DName)
    axis3D = document.CreateObject(KIND_POLYLINE)
    axis3D.SetName(axis3DName)
    axis3D.SetPolylineXY(axisXY)
    axis3D.SetAltitudeObject(aCloud)
    axis3D.SetChildProfileUZ(altiProfile.GetProfileUZ())
    altiProfile.Update()
    axis3D.Update()
    return (altiProfile, axis3D)

def createEmbankmentSectionA(document, embankmentName, axis3D, sectionPoints, d, displayLevel):
    """
    Define the section of an embankement and extrude it along a 3D axis. Section is defined with a list of points (axis distance, altitude).
    parameters:
    document: current HYDROData_Document
    embankmentName: name to give to the embankment
    axis3D: the 3D axis of the embankment
    sectionPoints: a list of tuple (x, h) to define a half section, beginning to x=0 (center)
    d calculation parameter, take 2 or 3 times the section width
    displayLevel : z level for 2D representation: high values are drawn above low values
    return:
    section, embankment
    """
    section = document.CreateObject(KIND_PROFILE)
    section.SetName("%s_section"%embankmentName)
    sectionPts = []
    for i,p in enumerate(sectionPoints):
        sectionPts.append(gp_XY(p[0], p[1]))
        if i>0:
            sectionPts.insert(0, gp_XY(-p[0], p[1]))
    section.SetParametricPoints(sectionPts)
    section.Update()
    embankment = document.CreateObject(KIND_DIGUE)
    embankment.SetName(embankmentName)
    embankment.SetGuideLine(axis3D)
    embankment.SetProfileMode(True)
    embankment.SetProfile(section)
    embankment.SetEquiDistance(d)
    embankment.SetZLevel(displayLevel)
    embankment.SetFillingColor(QColor(randint(0,255), randint(0,255), randint(0,255), 255))
    embankment.Update()
    return (section, embankment)

def createEmbankmentSectionB(document, embankmentName, axis3D, LC,DZ,CZ, d, displayLevel):
    """
    Define the section of an embankement and extrude it along a 3D axis. Section is defined with a width, an height, the position of the axis.
    parameters:
    document: current HYDROData_Document
    embankmentName: name to give to the embankment
    axis3D: the 3D axis of the embankment
    LC, DZ, CZ: width, height, height offset above axis3D
    d calculation parameter, take 2 or 3 times the section width
    displayLevel : z level for 2D representation: high values are drawn above low values
    return:
    embankment
    """
    embankment = document.CreateObject(KIND_DIGUE)
    embankment.SetName(embankmentName)
    embankment.SetGuideLine(axis3D)
    embankment.SetProfileMode(False)
    embankment.SetLCValue(LC)
    embankment.SetDeltaZValue(DZ)
    embankment.SetCoteZValue(CZ)
    embankment.SetEquiDistance(d)
    embankment.SetZLevel(displayLevel)
    embankment.SetFillingColor(QColor(randint(0,255), randint(0,255), randint(0,255), 255))
    embankment.Update()
    return embankment

def loadStricklerTable(document, stricklerFile):    
    """
    Load a table of Corine Land Cover types  and codes, used for Strickler coefficients (editable text format)
    parameters:
    document: current HYDROData document
    stricklerFile: full path of the table (*.txt)
    return:
    table loaded, code corresponding to Corine Land Cover used for the table ('CODE_06', 'CODE_12'...)
    """
    code = ""
    with open(stricklerFile) as f:
        line = f.readline()
        code = line.split()[0]
    a = os.path.splitext(stricklerFile)
    stricklerName = os.path.basename(a[0])
    strickler_table = document.CreateObject( KIND_STRICKLER_TABLE )
    strickler_table.Import(stricklerFile)
    strickler_table.SetName(stricklerName)
    strickler_table.SetAttrName(code)
    strickler_table.Update()
    return strickler_table, code
    
def loadLandCoverMap(document, landCoverFile, stricklerFile, displayLevel):
    """
    Load a Corine Land Cover Map
    parameters:
    document: current HYDROData document
    landCoverFile: shapeFile defining the map (*.shp)
    stricklerFile: full path of the table (*.txt)
    displayLevel : z level for 2D representation: high values are drawn above low values
    return:
    land cover map loaded
    """
    alltypes = []
    allvalues = []
    code = ""
    l = 0
    with open(stricklerFile) as f:
        for line in f:
            items = line.split('"')
            if l ==0:
                code = line.split()[0]
            else:
                alltypes.append(items[1])
                itm2 = items[-1].split()
                allvalues.append(itm2[-1])
            l = l + 1
    print(alltypes)
    print(allvalues)
    while code[0] != 'C':    # the code begins with 'C'
        code = code[1:]      # get rid of encoding
    print (code)
    
    a = os.path.splitext(landCoverFile)
    landCoverName = os.path.basename(a[0])
    landCoverDB = a[0] + '.dbf'
    landCover = document.CreateObject(KIND_LAND_COVER_MAP)
    print(landCoverFile)
    print(landCoverDB)
    print(landCoverName)
    landCover.SetName(landCoverName)
    landCover.SetZLevel(displayLevel)
    if not landCover.ImportSHP(landCoverFile):
        raise ValueError('problem while loading LandCoverMap shape')
    if landCover.ImportDBF(landCoverDB, code, allvalues, alltypes ) != landCover.DBFStatus_OK:
        raise ValueError('problem while loading LandCoverMap data base')
    landCover.Update()
    return landCover
    






# -*- coding: utf-8 -*-

# -------------------------------------

def controlGeomProps(geompy, geomShape, refLength, refArea):
  """
  compare area and length of a geometric face with a reference,
  with relative precision of 1.E-3
  """
  props = geompy.BasicProperties(geomShape)
  print(" Wires length: ", props[0])
  print(" Surface area: ", props[1])
  print(" Volume      : ", props[2])
  deltaLength = 2.0*abs((props[0] - refLength)/(props[0] + refLength))
  deltaArea   = 2.0*abs((props[1] - refArea)/(props[1] + refArea))
  if deltaLength > 1e-3 or deltaArea > 1e-3 or props[2] != 0:
    print("While must be:")
    print(" Wires length: ", refLength)
    print(" Surface area: ", refArea)
    print(" Volume      : ", 0.)
    raise ValueError("Bad length or area")

# -------------------------------------

def controlMeshStats(aMesh, nbNodes, nbEdges, nbTriangles):
  """
  Compare number of nodes and elements with a reference,
  with a precision of 5%.
  Only for a mesh, not for a subMesh.
  """
  tolerance = 0.05
  references = {}
  references['Entity_Node'] = nbNodes
  references['Entity_Edge'] = nbEdges
  references['Entity_Triangle'] = nbTriangles

  mesures = aMesh.GetMeshInfo()
  #print mesures
  d= {}
  for key, value in mesures.items():
    d[str(key)] = value
  for key in ('Entity_Triangle', 'Entity_Edge', 'Entity_Node'):
    if (d[key] < (1.0 - tolerance)*references[key]) \
    or (d[key] > (1.0 + tolerance)*references[key]):
      print(aMesh.GetName())
      print(key, ": value: ", d[key], " reference: ", references[key])
      raise ValueError("Bad number of nodes or elements")

# -------------------------------------

def controlSubMeshStats(aSubMesh, nbItems):
  """
  Compare number of nodes and elements with a reference,
  with a precision of 5%.
  Only for a subMesh, not for a mesh.
  """
  tolerance = 0.05
  mesures = aSubMesh.GetMeshInfo()
  nbRef = sum(mesures)
  if (nbItems < (1.0 - tolerance)*nbRef) \
  or (nbItems > (1.0 + tolerance)*nbRef):
    print(aSubMesh.GetName())
    print("value: ", nbRef, " reference: ", nbItems)
    raise ValueError("Bad number of nodes or elements")

# -------------------------------------

def controlStatZ(statz,refstatz):
  """
  Compare min, max, mean, standard deviation, percentile 5 and 95 of z of regions with a reference,
  with a precision of 0.05m by default and 1.0m for min and max.
  """
  for nomreg, valsref in refstatz.items():
    vals = statz[nomreg]
    tolerance =0.2
    if len(valsref) > 6:
      tolerance = valsref[6]
    if abs(vals[0] - valsref[0]) > 10*tolerance \
    or abs(vals[1] - valsref[1]) > 10*tolerance \
    or abs(vals[2] - valsref[2]) > tolerance \
    or abs(vals[3] - valsref[3]) > tolerance \
    or abs(vals[4] - valsref[4]) > 10*tolerance \
    or abs(vals[5] - valsref[5]) > 10*tolerance :
      print(nomreg)
      print("value: ", vals)
      print("reference: ", valsref, "tolerance for mean, std dev: ", tolerance, "and ", 10*tolerance, "for other values")
      raise ValueError("z interpolation error")
    else:
      print(nomreg)
      print("value: ", vals)
      print("reference: ", valsref, "tolerance for mean, std dev: ", tolerance, "and ", 10*tolerance, "for other values (0, 1, 4, 5): OK")

def controlValues(vals, refs, tol = 0.1):
    """
    control a list of values against a list of references, with an absolute tolerance
    """
    print("values: ", vals)
    print("references: ", refs)
    print("tolerance: ", tol)
    for v,r in zip(vals, refs):
      if abs(v-r) >tol:
        print("value: ", v)
        print("reference: ", r)
        print("tolerance: ", tol)
        raise ValueError("value error")

import MEDLoader
from MEDLoader import MEDCouplingFieldDouble, ON_NODES, DataArrayDouble, MEDFileMesh
import os, time

def controlTelemacResult(aMedFile, refs):
  """
  Check if the result med file exist and contains result fields
  """
  print(aMedFile)
  for i in range(10):
    time.sleep(3)
    print('waiting result...')
    if os.path.exists(aMedFile):
      break
  time.sleep(3)
  try:
    MEDLoader.CheckFileForRead(aMedFile)
  except:
    raise ValueError("problem while reading Telemac result med file")
  names = MEDLoader.GetMeshNames(aMedFile)
  infos = MEDLoader.GetUMeshGlobalInfo(aMedFile, names[0])
  stats = {}
  stats['nbTriangles'] = infos[0][0][0][1]
  stats['nbNodes'] = infos[3]
  stats['fieldNames'] = MEDLoader.GetNodeFieldNamesOnMesh(aMedFile, names[0])
  stats['iterations'] = MEDLoader.GetNodeFieldIterations(aMedFile, names[0], 'FREE SURFACE')
  for nomreg, valsref in refs.items():
    vals = stats[nomreg]
    if vals != valsref:
      print(nomreg)
      print("value: ", vals, " reference: ", valsref)
      raise ValueError("error in Telemac result")


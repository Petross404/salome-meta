#! /usr/bin/env python
#  -*- coding: iso-8859-1 -*-

import os

def set_env( args ):
    os.environ["SALOME_MODULES_ORDER"] = "HYDRO:SHAPER:GEOM:SMESH"
    os.environ["GEOM_IGNORE_RESTORE_SHAPE"] = "2"
    pass

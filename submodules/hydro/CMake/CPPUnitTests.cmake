include_directories(
  ${CPPUNIT_INCLUDE_DIRS}
)
add_definitions(${CPPUNIT_DEFINITIONS})

add_executable(${TEST_EXE} ${TEST_HEADERS} ${TEST_SOURCES})

get_target_property(MyUnitTestTarget ${TEST_EXE} LOCATION)
# replace $(OutDir) in this path by "Debug" or "Release" because it is not recognized by CTest later
STRING(REGEX REPLACE "\\$\\(OutDir\\)" "${CMAKE_BUILD_TYPE}" MyUnitTestTarget_upd ${MyUnitTestTarget})
enable_testing()

add_test(${TEST_EXE} ${MyUnitTestTarget_upd})
                        
# Copyright (C) 2012-2013  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Author: Vadim SANDLER, Open CASCADE S.A.S. (vadim.sandler@opencascade.com)

####################################################################
#
# PYQT4_WRAP_SIP_EXT macro
#
# Generate C++ wrappings for *.sip files by processing them with sip.
#
# USAGE: PYQT4_WRAP_SIP_EXT(output_files sip_files)
#
# ARGUMENTS:
#   output_files [out] variable where output file names are listed to
#   sip_files    [in]  list of *.sip files
# 
# NOTES:
#   - Input files are considered relative to the current source directory.
#   - Output files are generated in the current build directory.
#   - This version of macro requires class(es) definition in the 
#     *.sip file to be started on a new line without any preceeding characters.
#
# TODO:
#   - Check if dependency of static sources on generated headers works properly:
#     if header is changed, dependant sources should be recompiled.
# 
####################################################################
MACRO(PYQT4_WRAP_SIP_EXT outfiles)
  FOREACH(_input ${ARGN})

    SET(_output)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPycmodule.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPycmodule.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Application.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Application.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ArtificialObject.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ArtificialObject.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Bathymetry.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Bathymetry.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_CalculationCase.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_CalculationCase.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Document.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Document.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Entity.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Entity.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IAltitudeObject.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IAltitudeObject.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_AltitudeObject.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_AltitudeObject.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ObstacleAltitude.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ObstacleAltitude.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_StreamAltitude.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_StreamAltitude.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Image.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Image.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ImmersibleZone.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ImmersibleZone.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IPolyline.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IPolyline.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_NaturalObject.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_NaturalObject.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Confluence.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Confluence.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Object.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Object.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Profile.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Profile.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Polyline3D.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Polyline3D.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Obstacle.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Obstacle.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Channel.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Channel.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Digue.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Digue.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_PolylineXY.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_PolylineXY.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ProfileUZ.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ProfileUZ.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Region.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Region.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_River.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_River.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Stream.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Stream.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Zone.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_Zone.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IInterpolator.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IInterpolator.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IProfilesInterpolator.cc)   
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_IProfilesInterpolator.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPygp_XYZ.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPygp_XYZ.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPygp_XY.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPygp_XY.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ShapesGroup.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_ShapesGroup.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_SplitShapesGroup.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_SplitShapesGroup.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyTCollection_AsciiString.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyTCollection_AsciiString.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence1800.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence1800.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence2600.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence2600.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence2400.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence2400.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100Handle_HYDROData_Entity.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100Handle_HYDROData_Entity.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100gp_XY.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100gp_XY.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100gp_XYZ.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100gp_XYZ.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100HYDROData_IPolylineSectionType.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100HYDROData_IPolylineSectionType.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100TCollection_AsciiString.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyNCollection_Sequence0100TCollection_AsciiString.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_StricklerTable.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_StricklerTable.cc)

    LIST(APPEND _output ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_LandCoverMap.cc)
    SET(${outfiles} ${${outfiles}} ${CMAKE_CURRENT_BINARY_DIR}/sipHYDROPyHYDROData_LandCoverMap.cc)

    ADD_CUSTOM_COMMAND(
      OUTPUT ${_output}
      COMMAND ${SIP_EXECUTABLE} ${PYQT_SIPFLAGS} ${CMAKE_CURRENT_SOURCE_DIR}/${_input}
      MAIN_DEPENDENCY ${_input}
      )

  ENDFOREACH()
ENDMACRO(PYQT4_WRAP_SIP_EXT)



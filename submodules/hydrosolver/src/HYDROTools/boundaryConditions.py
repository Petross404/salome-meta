import os

PRS_SEP = ","
BND_SEP = " "

"""Preset file reader"""
class PresetReader():
    def __init__(self, file_name):
        self.file_name = file_name
        self._errors = []
    
    @property
    def errors(self):
        return self._errors
    
    def __get_value(self, str):
        value = ''
    
        if str.isdigit():
            value = int(str)
            
        return value
    
    def read(self):
        del self._errors[:]
        
        presets = {}
        
        try:
            with open(self.file_name) as f:
                # Read presets line by line
                line_number = 1
                groups = []
                for line in f:
                    cstr = " ".join(line.split())
                    values = cstr.strip().split(PRS_SEP)
                    #print values
                    if len(values) != 5:
                        self._errors.append("Line #%s: wrong number of values in the preset." % line_number)
                    elif values[0].strip() == "":
                        self._errors.append("Line #%s: empty name of the preset." % line_number)
                    elif values[0] in presets:
                        self._errors.append("Line #%s: preset name %s is already used." % (line_number, values[0]))
                    else:
                        name = values[0]
                        lihbor = self.__get_value(values[1])
                        liubor = self.__get_value(values[2])
                        livbor = self.__get_value(values[3])
                        litbor = self.__get_value(values[4])
                        
                        presets[name] = (lihbor, liubor, livbor, litbor)
                        #print name, presets[name]
            #print presets.keys()
        except IOError as err:
            self._errors.append(err.strerror)
        return presets    

"""Boundary condition object"""
class BoundaryCondition():
    def __init__(self, lihbor, liubor, livbor, litbor, group):
        self.lihbor = lihbor
        self.liubor = liubor
        self.livbor = livbor
        self.litbor = litbor
        self.group = group
        
    def get_min(self):
        return min( self.lihbor, self.liubor, self.livbor,  self.litbor )
        
    def get_max(self):
        return max( self.lihbor, self.liubor, self.livbor,  self.litbor )
        
    def set_range(self, min_val, max_val):
        self.lihbor = min(self.lihbor, max_val)
        self.lihbor = max(self.lihbor, min_val)
        self.liubor = min(self.liubor, max_val)
        self.liubor = max(self.liubor, min_val)
        self.livbor = min(self.livbor, max_val)
        self.livbor = max(self.livbor, min_val)
        self.litbor = min(self.litbor, max_val)
        self.litbor = max(self.litbor, min_val)
        
"""Boundary conditions file reader"""
class BoundaryConditionReader():
    def __init__(self, file_name):
        self.file_name = file_name
        self._errors = []
    
    def __get_condition(self, str):
        condition = None
                
        try:
            cstr = " ".join(str.split())
            values = cstr.strip().split(BND_SEP)
            
            if len(values) == 5:
               lihbor = int(values[0])
               liubor = int(values[1])
               livbor = int(values[2])
               litbor = int(values[3])
               group = values[4]
               
               condition = BoundaryCondition(lihbor, liubor, livbor, litbor, group)
        except ValueError:
            pass
                
        return condition    
    
    @property
    def errors(self):
        return self._errors
       
    def read(self):
        del self._errors[:]
        
        conditions = []
        
        try:
            with open(self.file_name) as f:
                # Read the number of conditions
                nb_conditions = 0
                first_line = f.readline().strip()
                if not first_line.isdigit():
                    self._errors.append("First line is not a number.")
                else:
                    nb_conditions = int(first_line)
                        
                # Read conditions line by line
                line_number = 2
                cnd_count = 0
                groups = []
                for line in f:
                    cnd = self.__get_condition(line)
                
                    if cnd:
                        if groups.count(cnd.group) == 0:
                            if cnd.get_min() < 0 or cnd.get_max() > 6:
                                cnd.set_range(0, 6)
                                self._errors.append("Line #%s: values out of range [0, 6] have been adjusted." % line_number)
                        
                            conditions.append(cnd)
                            groups.append(cnd.group)
                            cnd_count += 1
                        else:
                            self._errors.append("Line #%s: group name '%s' is already used." % (line_number, cnd.group))
                    else:
                        self._errors.append("Line #%s: wrong format." % line_number)
                
                    line_number += 1
                
                if cnd_count != nb_conditions:
                    self._errors.append("Number of conditions does not match (%s instead of %s)" %  (cnd_count, nb_conditions))
        except IOError as err:
            self._errors.append(err.strerror)
        return conditions
        
"""Boundary conditions file writer"""
class BoundaryConditionWriter():
    def __init__(self, file_name):
        self.file_name = file_name
        self._errors = []
    
    def __get_string(self, condition):
        lihbor = str(condition.lihbor)
        liubor = str(condition.liubor)
        livbor = str(condition.livbor)
        litbor = str(condition.litbor)
        values = (lihbor, liubor, livbor, litbor, condition.group)
        line = BND_SEP.join(values)
                
        return line
    
    @property
    def errors(self):
        return self._errors
       
    def write(self, conditions):
        del self._errors[:]
    
        try:
            with open(self.file_name, 'w') as f:
                lines = []
            
                # The number of conditions
                lines.append(str(len(conditions)) + '\n')
                        
                # Conditions
                for cnd in conditions:
                    lines.append(self.__get_string(cnd) + '\n')
                    
                # Write
                if len(lines) > 1:
                    f.writelines(lines)
        except IOError as err:
            self._errors.append(err.strerror)           
    

#!/usr/bin/env python

import MEDLoader as ml
import numpy as np

# Open the file

def set_var_on_group(filename, meshname, groups_val, varname):
    """
    Set values on groups_val

    @param filename Name of the MED file
    @param meshname Name of the mesh in the MED file
    @param groups_val List of tuples(Name, value)
    @param varname Name of the field
    """
    # Opening mesh
    meshFile2D = ml.MEDFileUMesh(filename, meshname)

    m2D = meshFile2D.getMeshAtLevel(0)

    npoin = m2D.getNumberOfNodes()

    # Values for the field
    values = ml.DataArrayDouble(npoin)

    for group , val in groups_val:
        # Getting element in the group
        cellsInGroup = meshFile2D.getGroupArr(0, group)
        for cell in cellsInGroup:
            # Get list of nodes in the element
            nodes = m2D.getNodeIdsOfCell(cell[0])
            for node in nodes:
                values[node] = val

    # Building the new field
    field = ml.MEDCouplingFieldDouble.New(ml.ON_NODES)
    field.setName(varname)
    field.setMesh(m2D)
    field.setArray(values)
    field.setTime(0.0, 0, -1)

    # Writing field on file
    ml.WriteField(filename, field, False)


if __name__ == "__main__":
    filename = "garonne_1F.med"
    meshname = "HYDRO_garonne_1"
    groups = [("riveDroite",0.010),
              ("riveGauche",0.010),
              ("litMineur",10.0),
              ]
    varname = "WATER DEPTH"

    set_var_on_group(filename, meshname, groups, varname)




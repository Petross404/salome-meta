#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import traceback
import logging

from PyQt5.QtWidgets import QMessageBox, QApplication, QDialog

import salome
import SALOME
import SalomePyQt
sgPyQt = SalomePyQt.SalomePyQt()
from salome.kernel.studyedit import getStudyEditor
from salome.kernel.logger import Logger
from salome.kernel import termcolor
logger = Logger("HYDROGUI", color = termcolor.BLUE)
#logger.setLevel(logging.ERROR)

import HYDROSOLVER_ORB

from salome.hydro.interpolz_gui import InterpolzDlg
from salome.hydro.assignStrickler_gui import assignStricklerDlg
from salome.hydro.changeCoordsDialog import changeCoordsDialog
from salome.hydro.gui_utils import HSGUIException, wait_cursor, \
                                   get_and_check_selected_file_path
import salome.hydro.study as hydro_study
from salome.hydro.run_study.eficas.appli import EficasForRunStudyAppli
from salome.hydro.param_study.eficas.appli import EficasForParamStudyAppli
from salome.hydro.telma.eficas.appli import EficasForTelmaAppli
from salome.hydro.run_study.gui import create_case_study, \
                                   run_selected_case_study, \
                                   edit_selected_case_study, \
                                   generate_job_for_selected_case_study
from eficasSalome import runEficas

from BndConditionsDialog import BoundaryConditionsDialog
from salome.hydro.initialFieldDialog import initialFieldDialog
from salome.hydro.checkBoundariesDialog import checkBoundariesDialog


################################################
# GUI context class
# Used to store actions, menus, toolbars, etc...
################################################

class GUIcontext:

    # --- menus/toolbars/actions IDss

    HYDRO_MENU_ID = 90
    CREATE_STUDY_ID = 951
    EDIT_STUDY_ID = 952
    RUN_STUDY_ID = 953
    GEN_STUDY_BATCH_ID = 954

    CREATE_TELMA_CAS_ID = 955
    EDIT_TELMA_CAS_ID = 956

    GENERATE_INTERPOLZ_PY_ID = 957
    GENERATE_ASSIGNSTRICKLER_PY_ID = 958

    # TODO Add create and edit ?
    EDIT_BOUNDARY_CONDITIONS_FILE_ID = 959
    EDIT_INITIAL_FIELD_FILE_ID = 962

    CREATE_PARAM_STUDY_ID = 963
    EDIT_PARAM_STUDY_ID = 964
    GEN_PARAM_STUDY_PYTHON_ID = 965
    GEN_PARAM_STUDY_YACS_ID = 966

    CHANGECOORDS_PY_ID = 967
    CHECK_BOUNDARY_CONDITIONS_ID = 968

    def __init__(self):
        # create top-level menu
        mid = sgPyQt.createMenu("Hydro", -1, GUIcontext.HYDRO_MENU_ID,
                                sgPyQt.defaultMenuGroup())
        # create toolbar
        tid = sgPyQt.createTool("Hydro")
        # create actions and fill menu and toolbar with actions
        act = sgPyQt.createAction(\
                GUIcontext.CHANGECOORDS_PY_ID,
                "Change coordinates",
                "Change coordinates",
                "Change mesh coordinates",
                "changes_coords.png")
        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createAction(\
                GUIcontext.GENERATE_INTERPOLZ_PY_ID,
                "Generate interpolz.py",
                "Generate interpolz.py",
                "Generate interpolation script for altitudes",
                "generate_interpolz_py.png")
        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createAction(\
                GUIcontext.GENERATE_ASSIGNSTRICKLER_PY_ID,
                "Generate assignStrickler.py",
                "Generate assignStrickler.py",
                "Generate assignation script for bottom friction coefficients",
                 "assign_Strickler_py.png" )
        sgPyQt.createMenu( act, mid )
        sgPyQt.createTool( act, tid )

        act = sgPyQt.createSeparator()

        act = sgPyQt.createAction(\
                GUIcontext.EDIT_BOUNDARY_CONDITIONS_FILE_ID,
                "Edit boundary conditions file",
                "Edit boundary conditions file",
                "Create/edit the boundary conditions file for Telemac",
                "edit_boundary_conditions_file.png")
        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createAction(\
                GUIcontext.CHECK_BOUNDARY_CONDITIONS_ID,
                "Check boundary conditions",
                "Check boundary conditions",
                "display a graph with boundary conditions on the mesh",
                "check_boundary_conditions.png")
        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createAction(\
                GUIcontext.EDIT_INITIAL_FIELD_FILE_ID,
                "Edit initial field file",
                "Edit initial field file",
                "Create/edit the initial field file for Telemac",
                "edit_initial_field_file.png" )
        sgPyQt.createMenu( act, mid )
        sgPyQt.createTool( act, tid )

        act = sgPyQt.createAction(\
                GUIcontext.CREATE_TELMA_CAS_ID,
                "Edit cas file (English)",
                "Edit cas file (English)",
                "Create/edit act .cas file for Telemac (English)",
                "create_telma_cas.png")
        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createAction(\
                GUIcontext.CREATE_STUDY_ID,
                "Execute a steering file",
                "Execute a steering file",
                "Fill formular for a normal execution",
                "create_study.png")

        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createAction(\
                GUIcontext.CREATE_PARAM_STUDY_ID,
                "Create Parameter Study",
                "Create Parameter Study",
                "Create act new Parameter Study",
                "create_param_study.png")
        sgPyQt.createMenu(act, mid)
        sgPyQt.createTool(act, tid)

        act = sgPyQt.createSeparator()

        # the following action are used in context popup

        sgPyQt.createAction(\
                GUIcontext.EDIT_PARAM_STUDY_ID,
                "Edit param study",
                "Edit param study",
                "Edit study using python launcher")
        sgPyQt.createAction(\
                GUIcontext.GEN_PARAM_STUDY_PYTHON_ID,
                "Generate Python script",
                "Generate Python script",
                "Generate act Python script from the eficas date")
        sgPyQt.createAction(\
                GUIcontext.GEN_PARAM_STUDY_YACS_ID,
                "Generate YACS script",
                "Generate YACS script",
                "Generate act YACS script from the eficas date")

        sgPyQt.createAction(\
                GUIcontext.RUN_STUDY_ID,
                "Compute study",
                "Compute study",
                "Compute study using python launcher")
        sgPyQt.createAction(\
                GUIcontext.EDIT_STUDY_ID,
                "Edit study",
                "Edit study",
                "Edit the selected study")
        sgPyQt.createAction(\
                GUIcontext.GEN_STUDY_BATCH_ID,
                "Generate batch job",
                "Generate batch job",
                "Generate a batch job to run the selected study")

        sgPyQt.createAction(\
                GUIcontext.EDIT_TELMA_CAS_ID,
                "Edit Steering file",
                "Edit Steering file",
                "Edit a Telemac-Mascaret steering file")


################################################
# Global variables
################################################

# study-to-context map
__study2context__ = {}
# current context
__current_context__ = None
# object counter
__objectid__ = 0

################################################
# Internal methods
################################################

###
# get active study ID
###
#def _getStudyId():
    #return sgPyQt.getStudyId()

###
# get active study
###
#def _getStudy():
    #studyId = _getStudyId()
    #study = getStudyManager().GetStudyByID(studyId)
    #return study

###
# returns True if object has children
###
def _hasChildren(sobj):
    if sobj:
        study = salome.myStudy
        iter  = study.NewChildIterator(sobj)
        while iter.More():
            name = iter.Value().GetName()
            if name:
                return True
            iter.Next()
            pass
        pass
    return False

###
# get current GUI context
###
def _getContext():
    global __current_context__
    return __current_context__

###
# set and return current GUI context
# study ID is passed as parameter
###
def _setContext():
    global __study2context__, __current_context__
    #if studyID not in __study2context__:
        #__study2context__[studyID] = GUIcontext()
        #pass
    #__current_context__ = __study2context__[studyID]
    __current_context__ = GUIcontext()
    return __current_context__

###
# increment object counter in the map
###
def _incObjToMap(m, id):
    if id not in m: m[id] = 0
    m[id] += 1
    pass

################################################
# Callback functions
################################################

# called when module is activated
# returns True if activating is successfull and False otherwise
def activate():
    ctx = _setContext()
    return True

# called when module is deactivated
def deactivate():
    pass

# called when active study is changed
# active study ID is passed as parameter
def activeStudyChanged():
    ctx = _setContext()
    pass

# called when popup menu is invoked
# popup menu and menu context are passed as parameters
def createPopupMenu(popup, context):
    ed = getStudyEditor()
    _setContext()
    if salome.sg.SelectedCount() == 1:
        # one object is selected
        sobj = ed.study.FindObjectID(salome.sg.getSelected(0))
        print("sobj: %s"%sobj) # strange bug with sobj is None when right clic on 3Dview in HYDROSOLVER used after HYDRO (generate AssignStrickler)
        if sobj:
            selectedType = ed.getTypeId(sobj)
            if selectedType == hydro_study.TELMA_TYPE_ID:
                popup.addAction(sgPyQt.action(GUIcontext.EDIT_TELMA_CAS_ID))
            elif selectedType == hydro_study.PARAM_STUDY_TYPE_ID:
                popup.addAction(sgPyQt.action(GUIcontext.EDIT_PARAM_STUDY_ID))
                popup.addAction(sgPyQt.action(GUIcontext.GEN_PARAM_STUDY_PYTHON_ID))
                popup.addAction(sgPyQt.action(GUIcontext.GEN_PARAM_STUDY_YACS_ID))
            elif selectedType == hydro_study.STUDY_TYPE_ID:
                popup.addAction(sgPyQt.action(GUIcontext.EDIT_STUDY_ID))
                popup.addAction(sgPyQt.action(GUIcontext.RUN_STUDY_ID))
                popup.addAction(sgPyQt.action(GUIcontext.GEN_STUDY_BATCH_ID))

# called when GUI action is activated
# action ID is passed as parameter
def OnGUIEvent(commandID):
    try:
        dict_command[commandID]()
    except HSGUIException as exc:
        QMessageBox.critical(sgPyQt.getDesktop(),
                                   QApplication.translate("OnGUIEvent", "Error"),
                                   str(exc))
    except:
        logger.exception("Unhandled exception caught in HYDROSOLVER GUI")
        msg = QApplication.translate("OnGUIEvent",
            "Unhandled error happened in HYDROSOLVER module. Please report a bug on "
            '<a href="https://forge-pleiade.der.edf.fr/projects/salome-rex/issues">SALOME bugtracker</a>, '
            'category "HYDROSOLVER", describing how you got there and including the following traceback:')
        msgBox = QMessageBox(QMessageBox.Critical,
                                   QApplication.translate("OnGUIEvent", "Error"),
                                   msg,
                                   parent=sgPyQt.getDesktop())
        msgBox.setDetailedText(traceback.format_exc())
        msgBox.exec_()

################################################
# GUI actions implementation
################################################

###
# Open Eficas for a new parametric study
###
def create_param_study():
    EficasForParamStudyAppli()
###
# Open Eficas to edit a new parametric study
###
def edit_param_study():
    EficasForParamStudyAppli(get_and_check_selected_file_path())

###
# Generate a python script from the eficas file
###
def generate_param_study_python():
    try:
        with wait_cursor:
            ed = hydro_study.HydroStudyEditor()
            sobj = get_and_check_selected_file_path()
            python_file = ed.generate_study_script(sobj)
    except SALOME.SALOME_Exception as exc:
        salome.sg.updateObjBrowser()
        msg = str(QApplication.translate("generate_telemac2d_python",
                      "An error happened while trying to generate telemac2d Python script:"))
        msg += "\n" + exc.details.text
        raise HSGUIException(msg)
    QMessageBox.information(sgPyQt.getDesktop(),
                            "Dumping done",
                            "Generated Python script in:\n{}".format(python_file))

###
# Generate a python script from the eficas file
###
def generate_param_study_yacs():
    try:
        with wait_cursor:
            ed = hydro_study.HydroStudyEditor()
            sobj = get_and_check_selected_file_path()
            yacs_file = ed.generate_study_yacs(sobj)
    except SALOME.SALOME_Exception as exc:
        salome.sg.updateObjBrowser()
        msg = str(QApplication.translate("generate_telemac2d_yacs",
                      "An error happened while trying to generate telemac2d Python script:"))
        msg += "\n" + exc.details.text
        raise HSGUIException(msg)
    QMessageBox.information(sgPyQt.getDesktop(),
                            "Dumping done",
                            "Generated YACS scheme in:\n{}".format(yacs_file))

def open_schema_in_yacs():
    ed = getStudyEditor()
    sobj = ed.study.FindObjectID(salome.sg.getSelected(0))
    filename = sobj.GetComment()
    if filename.endswith(".comm"):
        filename = filename[:-5] + ".xml"
    if not os.path.isfile(filename):
        raise HSGUIException(QApplication.translate("open_schema_in_yacs",
                                  "Schema file %1 does not exist").arg(filename))
    yg = salome.ImportComponentGUI("YACS")
    yg.loadSchema(filename)

###
# Open dialog for boundary conditions edition
###
def edit_boundary_conditions_file():
    desktop = sgPyQt.getDesktop()
    dlg = BoundaryConditionsDialog(desktop)
    dlg.exec_()

###
# Open dialog for initial conditions edition
###
def edit_initial_field_file():
    desktop = sgPyQt.getDesktop()
    dlg = initialFieldDialog(desktop)
    dlg.exec_()

###
# Open dialog for initial conditions edition
###
def check_boundaries():
    desktop = sgPyQt.getDesktop()
    dlg = checkBoundariesDialog(desktop)
    dlg.exec_()

###
# Open dialog for interpolz.py script generation
###
def generate_interpolz_py():
    desktop = sgPyQt.getDesktop()
    dlg = InterpolzDlg(desktop)
    dlg.show()

###
# Open dialog for assignStrickler.py script generation
###
def generate_assignStrickler_py():
    desktop = sgPyQt.getDesktop()
    dlg = assignStricklerDlg(desktop)
    dlg.show()

def changeCoords_py():
    desktop = sgPyQt.getDesktop()
    dlg = changeCoordsDialog(desktop)
    dlg.exec_()

###
# Open dialog for boundary conditions edition
###
def generate_interpolks_py():
    QMessageBox.warning(sgPyQt.getDesktop(),
                        "",
                        "Generation of interpolks.py not implemented yet")
    return

###
# Open dialog for creation of steering file
###
def create_telma_cas():
    EficasForTelmaAppli(code='TELEMAC', lang='en')

###
# Open dialog for edition of steering file
###
def edit_telma_cas():
    # TODO: See how to detect module
    EficasForTelmaAppli(fichier=get_and_check_selected_file_path(),
                        code='TELEMAC', lang='en')

###
# Commands dictionary
###
dict_command = {
    GUIcontext.CREATE_STUDY_ID: create_case_study,
    GUIcontext.EDIT_STUDY_ID: edit_selected_case_study,
    GUIcontext.RUN_STUDY_ID: run_selected_case_study,
    GUIcontext.GEN_STUDY_BATCH_ID: generate_job_for_selected_case_study,
    GUIcontext.CREATE_TELMA_CAS_ID: create_telma_cas,
    GUIcontext.EDIT_TELMA_CAS_ID: edit_telma_cas,
    GUIcontext.GENERATE_INTERPOLZ_PY_ID: generate_interpolz_py,
    GUIcontext.GENERATE_ASSIGNSTRICKLER_PY_ID: generate_assignStrickler_py,
    GUIcontext.CHANGECOORDS_PY_ID: changeCoords_py,
    GUIcontext.EDIT_BOUNDARY_CONDITIONS_FILE_ID: edit_boundary_conditions_file,
    GUIcontext.CHECK_BOUNDARY_CONDITIONS_ID: check_boundaries,
    GUIcontext.EDIT_INITIAL_FIELD_FILE_ID: edit_initial_field_file,
    GUIcontext.CREATE_PARAM_STUDY_ID: create_param_study,
    GUIcontext.EDIT_PARAM_STUDY_ID: edit_param_study,
    GUIcontext.GEN_PARAM_STUDY_PYTHON_ID: generate_param_study_python,
    GUIcontext.GEN_PARAM_STUDY_YACS_ID: generate_param_study_yacs,
    }

# -*- coding: utf-8 -*-

import sys, os
import string

import sysconfig
pythonVersion = 'python' + sysconfig.get_python_version()
hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')

import salome
salome.salome_init()

import HYDROPy
from salome.hydrotools.interpolS import assignStrickler

from .generate_interpolz import replace

from PyQt5.QtWidgets import QDialog, QFileDialog, QTableWidgetItem, QComboBox, QMessageBox
from PyQt5 import uic

import SalomePyQt
import libSALOME_Swig
salome_gui = libSALOME_Swig.SALOMEGUI_Swig()


def get_selected_calc_case():
    ind = SalomePyQt.SalomePyQt.getObjectBrowser().selectionModel().selectedIndexes()
    #aStudyId = salome.myStudyId
    doc = HYDROPy.HYDROData_Document.Document()
    for i in ind:
        if i.column()==0:
            name = str(i.data())
            case = doc.FindObjectByName( name )
            if isinstance(case, HYDROPy.HYDROData_CalculationCase):
                return name
    return None

class assignStricklerDlg( QDialog ):
    def __init__(self, parent = None):
        QDialog.__init__( self, parent )
        p = hydro_solver_root
        uic.loadUi( p+'/assignStrickler.ui', self )
        self.setWindowTitle( 'Generate assignStrickler script' )
        SalomePyQt.SalomePyQt.getObjectBrowser().selectionChanged.connect(self.onSelectionChanged)
        self.btnOutputPath.clicked.connect(self.onOutputFile)
        self.btnMEDFile.clicked.connect(self.onMEDFile)
        self.CalcCase.textChanged.connect(self.onCalcCaseChanged)
        self.MEDFile.textChanged.connect(self.onMEDChanged)
        self.ApplyClose.clicked.connect(self.onApplyClose)
        self.Apply.clicked.connect(self.onApply)
        self.Close.clicked.connect(self.onClose)
        self.Help.clicked.connect(self.onHelp)
        self.onSelectionChanged()

    def onSelectionChanged( self ):
        calc_case_name = get_selected_calc_case()
        if calc_case_name is not None:
          self.CalcCase.setText( calc_case_name )

    def onOutputFile( self ):
      caption = "Python file"
      mask = "*.py"
      theDir="."
      fname, filt = QFileDialog.getSaveFileName( self, caption, theDir, mask )
      #print("fname: %s"%fname)
      if fname!=None and fname!="":
        if fname.split('.')[-1] != 'py':
          fname += '.py'
        self.OutputPath.setText( fname )

    def onMEDFile( self ):
      caption = "MED file"
      mask = "*.med"
      fname, filt = QFileDialog.getOpenFileName( self, caption, ".", mask )
      if fname!=None and fname!="":
        self.MEDFile.setText( fname )

    def onCalcCaseChanged( self ):
      self.onMEDChanged()

    def onMEDChanged( self ):
      pass
                 
    def onApplyClose( self ):
        if self.onApply():
            self.onClose()

    def generateScript( self, path, calc_case, med_file ):
      f = open( path, "w" )
      tf = open( hydro_solver_root+"/assignStrickler.template", "r" )
      templ = tf.readlines()
      replace( templ, "<case_name_from_dlg>", calc_case )
      replace( templ, "<MED_file_path_from_dlg>", med_file )
      for line in templ:
        f.write( line )
      f.close()

    def onApply( self ):
        path = str(self.OutputPath.text())
        calc_case = str(self.CalcCase.text())
        med_file = str(self.MEDFile.text())
        isScriptExec = self.cb_scriptExec.isChecked()
        msg = ""
        if len(path)==0:
            msg = "Please input the output path"
        elif len(calc_case)==0:
            msg = "Please choose the calculation case"
        elif len(med_file)==0:
            msg = "Please choose the MED file"

        result = False
        
        if len(msg)==0:
            self.generateScript( path, calc_case, med_file )
            msg = "Strickler script is successfully generated"
            if isScriptExec:
              assignStrickler(calc_case, med_file) #, output_file_name=fichierMaillage_out, verbose=True)
              msg = "Strickler script is successfully generated and executed"
            result = True

        QMessageBox.information( self, "", msg )
        return result

    def onClose( self ):
        self.close()

    """Shows help page"""
    def onHelp( self ):
        msg = """
        <h2>assignStrickler dialog</h2>

        This dialog is a tool for generation and execution of the script <b>assignStrickler.py</b>.

        <h3>Calculation case</h3>
        The name of the calculation case. It can be filled automatically on the base of selection or can be set by user.

        <h3>Script output path</h3>
        The path for the output, i.e. the path of the target script assignStrickler.

        <h3>Input MED file</h3>
        The path to MED file where the MED field is written.

        <h3>Execute the script</h3>
        Execute the script on Apply if checked, or just keep it for later use.
        """
        QMessageBox.about(self, self.tr("About assign Strckler coefficients dialog"), msg);


if __name__=='__main__':
  dlg = assignStricklerDlg()
  dlg.show()

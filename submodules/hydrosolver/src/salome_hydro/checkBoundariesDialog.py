#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

import subprocess
import time

import sysconfig
pythonVersion = 'python' + sysconfig.get_python_version()
hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')

class checkBoundariesDialog(QDialog):
    """
    """

    def __init__(self, parent = None, modal = 0):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(hydro_solver_root, 'checkBoundaries.ui'), self )

        # Connections
        self.pb_medFile.clicked.connect(self.on_med_file_browse)
        self.pb_bcdFile.clicked.connect(self.on_bcd_file_browse)
        self.rb_liqbcd.toggled.connect(self.on_liqbcd)
        self.pb_help.clicked.connect(self.on_help)
        self.pb_ok.accepted.connect(self.on_accept)
        self.pb_ok.rejected.connect(self.on_reject)
        self.medFile = None
        self.bcdFile = None
        self.rb_liqbcd.setChecked(True)
        self.rb_allbcd.setChecked(False)
        self.liqbcd = True

    def on_med_file_browse(self):
        """
        Select MED file
        """
        print("on_med_file_browse")
        self.medFile, _filt = QFileDialog.getOpenFileName(self, self.tr("MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFile)
        if not self.medFile:
            return
        self.le_medFile.setText(self.medFile)

    def on_bcd_file_browse(self):
        """
        Select bcd file
        """
        print("on_bcd_file_browse")
        self.bcdFile, _filt = QFileDialog.getOpenFileName(self, self.tr("Boundary Condition file"), "", self.tr("Boundary Condition files (*)"))
        print(self.bcdFile)
        if not self.bcdFile:
            return
        self.le_bcdFile.setText(self.bcdFile)

    def on_liqbcd(self):
        """
        switch type of display
        """
        print("on_liqbcd")
        if self.rb_liqbcd.isChecked():
            self.liqbcd = True
        else:
            self.liqbcd = False

    def on_help(self):
        """
        display a help message
        """
        msg = """
        <h2>Display boundarycondition dialog</h2>

        This dialog is used to draw the mesh with colored boundary conditions on nodes.
        <br><br>
        The boundary condition file (bcd) associates a type of boundary condition to each concerned group in the mesh.
        <br>
        The option "Liquid /solid boundaries" allows to distinguish all the differents boundary condition groups
        <br>
        The option "All boundaries by Telemac type" allows to distinguish all the types of boundary conditions.
        <br><br>
        The drawing computation Is launch in a separate process, without waiting.
        It may be long, for a big mesh...
        """
        QMessageBox.about(self, self.tr("About change coordinates dialog"), msg);


    def on_accept(self):
        print("accept")
        #TODO check medfile in and out not empty
        #TODO preset for bcdFile
        medFile = self.le_medFile.text()
        bcdFile = self.le_bcdFile.text()
        if not os.path.isfile(medFile):
            msgBox = QMessageBox()
            msgBox.setText( "MED file does not exist" )
            msgBox.exec_()
            return
        if not os.path.isfile(bcdFile):
            msgBox = QMessageBox()
            msgBox.setText( "Boundary condition file does not exist" )
            msgBox.exec_()
            return
        option = "--bnd"
        if self.liqbcd:
            option = "--liq-bnd"
        cmd = ["plot.py", "mesh2d", medFile, "-b", bcdFile, option]
        try:
            proc = subprocess.Popen(cmd)
        except OSError:
            print("invalid command")
            QMessageBox.critical(self, "command error", "%s is not found" % cmd[0])
        self.accept()

    def on_reject(self):
        print("reject")
        self.reject()


def execDialog(context):
    print("execDialog")
    # get context study, salomeGui
    study = context.study
    sg = context.sg
    dlg = checkBoundariesDialog()
    dlg.exec_()

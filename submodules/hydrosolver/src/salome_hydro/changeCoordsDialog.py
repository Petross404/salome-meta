#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

import pyproj
from pyproj import CRS

from salome.hydrotools.changeCoords import changeCoords

import sysconfig
pythonVersion = 'python' + sysconfig.get_python_version()
hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')

class changeCoordsDialog(QDialog):
    """
    """

    def __init__(self, parent = None, modal = 0):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(hydro_solver_root, 'changeCoords.ui'), self )

        # Connections
        self.pb_medFileIn.clicked.connect(self.on_med_file_in_browse)
        self.pb_medFileOut.clicked.connect(self.on_med_file_out_browse)
        self.pb_help.clicked.connect(self.on_help)
        self.pb_ok.accepted.connect(self.on_accept)
        self.pb_ok.rejected.connect(self.on_reject)
        self.medFileIn = None
        self.medFileOut = None
        EPSG_list = (2154, 27561, 27562, 27563, 27564, 27571, 27572, 27573, 27574 )
        for epsg in EPSG_list:
            crs_code = CRS.from_epsg(epsg)
            codeName = str(epsg) + ' ' + crs_code.name
            self.cmb_systemIn.addItem(codeName)
            self.cmb_systemOut.addItem(codeName)
        self.cb_changeCoordSystem.setChecked(True)
        self.cb_changeCoordSystem.setChecked(False)
        
    def on_med_file_in_browse(self):
        """
        Select input MED file
        """
        print("on_med_file_in_browse")
        self.medFileIn, filt = QFileDialog.getOpenFileName(self, self.tr("Input MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFileIn)
        if not self.medFileIn:
            return
        self.le_medFileIn.setText(self.medFileIn)
        a = os.path.splitext(self.medFileIn)
        self.medFileOut = a[0] + '_coo' + a[1]
        self.le_medFileOut.setText(self.medFileOut)
        
    def on_med_file_out_browse(self):
        """
        Select output MED file
        """
        print("on_med_file_out_browse")
        self.medFileOut, filt = QFileDialog.getSaveFileName(self, self.tr("Output MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFileOut)
        if not self.medFileOut:
            return
        self.le_medFileOut.setText(self.medFileOut)
        
    def on_help(self):
        """
        display a help message
        """
        msg = """
        <h2>Change mesh coordinates dialog</h2>

        This dialog is used to change the nodes coordinates in a mesh. It is used either for a simple translation, a change of coordinates system
        (for instance from Lambert II to Lambert-93), or a combination.
        <br><br>        
        The modified mesh is saved in a new file.
        <br> 
        If there is a change of coordinates system, the input mesh is first translated, when it uses a local origin.
        After the change of coordinates system, the output mesh can be translated (new local origin).
        <br>     
        When there is only a simple translation, the operation is: Xout = Xin + offsetXin -offsetXout, Yout = Yin + offsetYin -offsetYout.
        <br><br>         
        Below is the description of the dialog controls.

        <h3>Change coordinates system</h3>
        If this checkbox is checked, the output mesh will be removed after calculation of the shapes.

        <h3>Input MED file</h3>
        This field allows the selection of a med file (via the standard file open dialog).
        The filling of this field is mandatory.
        
        <h3>Input offsetX, offsetY</h3>
        These fields are used to set the Origin of the local coordinates system of the input mesh, if any. 

        <h3>Input Coord System</h3>
        This combo box is use to select the coordinate system of the input mesh. 

        <h3>Output MED file</h3>
        This field allows the definition of a med file (via the standard file save dialog).
        The filling of this field is mandatory.
        
        <h3>Output offsetX, offsetY</h3>
        These fields are used to set the Origin of the local coordinates system of the output mesh, if any. 

        <h3>Output Coord System</h3>
        This combo box is use to select the coordinate system of the output mesh. 
        """
        QMessageBox.about(self, self.tr("About change coordinates dialog"), msg);
       
   
    def on_accept(self):
        print("accept")
        #TODO check medfile in and out not empty
        #TODO preset for medFileOut
        medFileIn = self.le_medFileIn.text()
        medFileOut = self.le_medFileOut.text()
        offsetXin = self.dsb_offsetXin.value()
        offsetYin = self.dsb_offsetYin.value()
        offsetXout = self.dsb_offsetXout.value()
        offsetYout = self.dsb_offsetYout.value()
        isChangeCoordSystem = self.cb_changeCoordSystem.isChecked()
        epsgIn = 2154
        epsgOut = 2154
        if isChangeCoordSystem:
            epsgIn = int(self.cmb_systemIn.currentText().split()[0])
            epsgOut = int(self.cmb_systemOut.currentText().split()[0])
        print(medFileIn)
        print(medFileOut)
        print(isChangeCoordSystem)
        print(offsetXin, offsetYin)
        print(offsetXout, offsetYout)
        print(epsgIn, epsgOut)
        if not os.path.isfile(medFileIn):
            msgBox = QMessageBox()
            msgBox.setText( "Input MED file does not exist" )
            msgBox.exec_()
            return
        dir = os.path.dirname(medFileOut)
        base = os.path.basename(medFileOut)
        ext = os.path.splitext(medFileOut)[1]
        if not os.path.isdir(dir) or (base == "") or (ext.lower() != ".med"):
            msgBox = QMessageBox()
            msgBox.setText( "Output MED file is not a valid path" )
            msgBox.exec_()
            return
        self.accept()
        changeCoords(medFileIn, medFileOut, epsgIn, epsgOut, offsetXin, offsetYin, offsetXout, offsetYout)

    def on_reject(self):
        print("reject")
        self.reject()
        

def execDialog(context):
    print("execDialog")
  # get context study, salomeGui
    study = context.study
    sg = context.sg
    dlg = changeCoordsDialog()
    dlg.exec_()        

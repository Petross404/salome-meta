#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
#from PyQt4 import QtCore, QtGui
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui  import QCursor
from PyQt5.QtCore import Qt

import salome
from salome.kernel.studyedit import getStudyEditor


class HSGUIException(Exception):
  """
  Exception class used to display user-friendly messages in a dialog box for
  standard errors (missing files, etc.).
  """
  pass


class OverrideCursorCM:
  """
  This class is a context manager that can be used to set an override cursor
  in a QT application and automatically restore the base cursor
  """
  def __init__(self, cursor):
    self.cursor = cursor

  def __enter__(self):
    #QtGui.QApplication.setOverrideCursor(self.cursor)
    QApplication.setOverrideCursor(self.cursor)

  def __exit__(self, exc_type, exc_value, traceback):
    #QtGui.QApplication.restoreOverrideCursor()
    QApplication.restoreOverrideCursor()

#wait_cursor = OverrideCursorCM(QCursor(QtCore.Qt.WaitCursor))
wait_cursor = OverrideCursorCM(QCursor(Qt.WaitCursor))


def get_and_check_selected_file_path():
  """
  Get the first selected item in the object browser, check that its "Comment"
  attribute contains a valid file path, and return this file path.
  """
  ed = getStudyEditor()
  if len(salome.sg.getAllSelected()) == 0 : return None
  sobj = ed.study.FindObjectID(salome.sg.getSelected(0))
  filepath = sobj.GetComment()
  if not os.path.isfile(filepath):
    #raise HSGUIException(QtGui.QApplication.translate("get_and_check_selected_file_path",
    raise HSGUIException(QApplication.translate("get_and_check_selected_file_path",
                                                 "File %1 does not exist").arg(filepath))
  return filepath

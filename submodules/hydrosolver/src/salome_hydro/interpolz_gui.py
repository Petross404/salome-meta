
import sys, os
import string
import sysconfig
pythonVersion = 'python' + sysconfig.get_python_version()
hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')

import salome
salome.salome_init()

import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()

#import MEDLoader
import HYDROPy

from PyQt5.QtWidgets import QDialog, QFileDialog, QTableWidgetItem, QComboBox, QMessageBox
from PyQt5 import uic

import SalomePyQt
import libSALOME_Swig
salome_gui = libSALOME_Swig.SALOMEGUI_Swig()

from .generate_interpolz import generate, generate_B

def get_med_groups( file_path ):
    #print "get_med_groups", file_path
    try:
        #meshes = MEDLoader.GetMeshNames(file_path)
        (meshes, status) = smesh.CreateMeshesFromMED(file_path)
    except:
        print('No meshes found')
        return []
    if len(meshes)==0:
        print('No mesh found')
        return []
    mesh1 = meshes[0]
    print('Found mesh:', mesh1)
    try:
        #groups = list(MEDLoader.GetMeshGroupsNames(file_path, mesh1))
        grps = mesh1.GetGroups()
        groups = [grp.GetName() for grp in grps if grp.GetType() == SMESH.FACE]
        if len(groups) == 0:
          print("Problem! There are no groups of faces in the mesh!")
          print("Please create at least the groups of faces corresponding to each region of the HYDRO case")
          return []
        print('Found groups:', groups)
    except:
        print('No groups found')
        return []
    return groups

def get_hydro_regions( calc_case_name ):
    #aStudyId = salome.myStudyId
    doc = HYDROPy.HYDROData_Document.Document()
    case = doc.FindObjectByName( calc_case_name )
    if isinstance(case, HYDROPy.HYDROData_CalculationCase):
      regions = case.GetRegions()
      regions_names = []
      for r in regions:
        rname = r.GetName()
        regions_names.append( str(rname) )

#       shape_groups = case.GetGeometryGroups()
#       for sg in shape_groups:
#           sgname = sg.GetName()
#           regions_names.append( sgname )

      return regions_names
    else:
      return []

def get_selected_calc_case():
    ind = SalomePyQt.SalomePyQt.getObjectBrowser().selectionModel().selectedIndexes()
    #aStudyId = salome.myStudyId
    doc = HYDROPy.HYDROData_Document.Document()
    for i in ind:
        if i.column()==0:
            name = str(i.data())
            case = doc.FindObjectByName( name )
            if isinstance(case, HYDROPy.HYDROData_CalculationCase):
                return name
    return None

def get_selected_bathy():
    ind = SalomePyQt.SalomePyQt.getObjectBrowser().selectionModel().selectedIndexes()
    #aStudyId = salome.myStudyId
    doc = HYDROPy.HYDROData_Document.Document()
    for i in ind:
        if i.column()==0:
            name = str(i.data())
            case = doc.FindObjectByName( name )
            if isinstance(case, HYDROPy.HYDROData_Bathymetry):
                return name
    return None

class InterpolzDlg( QDialog ):
    def __init__(self, parent = None):
        QDialog.__init__( self, parent )
        p = hydro_solver_root
        uic.loadUi( p+'/interpolz.ui', self )
        self.setWindowTitle( 'Generate interpolz script' )
        SalomePyQt.SalomePyQt.getObjectBrowser().selectionChanged.connect(self.onSelectionChanged)
        self.btnOutputPath.clicked.connect(self.onOutputFile)
        self.btnMEDFile.clicked.connect(self.onMEDFile)
        self.CalcCase.textChanged.connect(self.onCalcCaseChanged)
        #self.BathyName.textChanged.connect(self.onBathyNameChanged)
        self.MEDFile.textChanged.connect(self.onMEDChanged)
        self.UndefZ.setRange( -100000, 100000 )
        self.UndefZ.setValue( -9999 )
        self.UndefZ_B.setRange( -100000, 100000 )
        self.UndefZ_B.setValue( -9999 )
        self.InterpMethod_B.addItem( "Interpolation at the nearest point" )
        self.InterpMethod_B.addItem( "Linear interpolation on a cloud triangulation" )
        self.InterpMethod.addItem( "Interpolation at the nearest point" )
        self.InterpMethod.addItem( "Linear interpolation on a cloud triangulation" )
        self.InterpMethod.currentIndexChanged.connect(self.onInterpChanged)
        self.ApplyClose.clicked.connect(self.onApplyClose)
        self.Apply.clicked.connect(self.onApply)
        self.Close.clicked.connect(self.onClose)
        self.Help.clicked.connect(self.onHelp)
        self.onSelectionChanged()
        self.tabWidget.setCurrentIndex(0)
        self.regions = [] #to avoid attrib error

    def onSelectionChanged( self ):
        calc_case_name = get_selected_calc_case()
        if calc_case_name is not None:
          self.CalcCase.setText( calc_case_name )
        bathy_name = get_selected_bathy()
        if bathy_name is not None:
          self.BathyName.setText( bathy_name )

    def onOutputFile( self ):
      caption = "Python file"
      mask = "*.py"
      fname, filt = QFileDialog.getSaveFileName( self, caption, ".", mask )
      if fname!=None and fname!="":
        if fname.split('.')[-1] != 'py':
          fname += '.py'
        self.OutputPath.setText( fname )

    def onMEDFile( self ):
      caption = "MED file"
      mask = "*.med"
      fname, filt = QFileDialog.getOpenFileName( self, caption, ".", mask )
      if fname!=None and fname!="":
        self.MEDFile.setText( fname )

    def onCalcCaseChanged( self ):
      self.regions = get_hydro_regions( str(self.CalcCase.text()) )
      self.onMEDChanged()
	  
	#def onBathyNameChanged( self ):
    #  #self.regions = get_hydro_regions( str(self.CalcCase.text()) )
    #  self.onMEDChanged()

    def onMEDChanged( self ):
      self.med_groups = get_med_groups( str(self.MEDFile.text()) )
      print(self.med_groups)
      n = len( self.med_groups )
      self.Groups.setRowCount( n )
      self.medGroupNames.clear()
      for i in range( 0, n ):
        self.medGroupNames.addItem(self.med_groups[i])

      for i in range( 0, n ):
        if self.Groups.item( i, 0 ) is None:
          self.Groups.setItem( i, 0, QTableWidgetItem() )
          self.Groups.setItem( i, 1, QTableWidgetItem() )
          self.Groups.setItem( i, 2, QTableWidgetItem() )
        self.Groups.item( i, 0 ).setText( self.med_groups[i] )

        cb = QComboBox( self.Groups )
        cb.addItem( 'None' )
        for r in self.regions:
          cb.addItem( r )
        self.Groups.setCellWidget( i, 1, cb )

        icb = QComboBox( self.Groups )
        icb.addItem( 'Interpolation at the nearest point' )
        icb.addItem( 'Linear interpolation on a cloud triangulation' )
        self.Groups.setCellWidget( i, 2, icb )
        icb.currentIndexChanged.connect(self.onCBInterpChanged)
        

    def onCBInterpChanged( self ):
      ind_set = set()
      for i in range( 0, self.Groups.rowCount() ):
        ind_set.add( self.Groups.cellWidget( i, 2 ).currentIndex() )
      if len(ind_set) == 2:
        self.InterpMethod.setStyleSheet("QComboBox { background-color: grey; }")
      elif len(ind_set) == 1:
        self.InterpMethod.setStyleSheet("")

    def onInterpChanged( self ):
      n = self.Groups.rowCount()
      for i in range( 0, n ):
        icb = self.Groups.cellWidget(i, 2)
        icb.setCurrentIndex(self.InterpMethod.currentIndex())
 
    def onApplyClose( self ):
        if self.onApply():
            self.onClose()

    def onApply( self ):
        path = str(self.OutputPath.text())
        med_file = str(self.MEDFile.text())
        print('current  TAB = ', self.tabWidget.currentIndex())
        isScriptExec = self.cb_scriptExec.isChecked()

        if self.tabWidget.currentIndex() == 0: #calc case tab
            calc_case = str(self.CalcCase.text())
            med_groups_regions = {}
            regions_interp_method = {}
            for i in range( 0, self.Groups.rowCount() ):
                med_group = str( self.Groups.item( i, 0 ).text() )
                hydro_reg = str( self.Groups.cellWidget( i, 1 ).currentText() )
                if len(med_group)>0 and len(hydro_reg)>0 and hydro_reg != 'None' :
                    med_groups_regions[med_group] = hydro_reg
                interp_ind = str( self.Groups.cellWidget( i, 2 ).currentIndex() )
                regions_interp_method[hydro_reg] = interp_ind
            z_undef = self.UndefZ.value()
            #interp = str(self.InterpMethod.currentText())
        
            msg = ""
            if len(path)==0:
                msg = "Please input the output path"
            elif len(calc_case)==0:
                msg = "Please choose the calculation case"
            elif len(med_file)==0:
                msg = "Please choose the MED file"
            elif len(med_groups_regions)==0:
                msg = "Please fill groups table"
            #elif len(interp)==0:
            #    msg = "Please choose interpolation method"
        
            result = False
            if len(msg)==0:
                #iinterp = 0
                #if 'Linear' in interp:
                #  iinterp = 1
                #generate( path, calc_case, med_file, med_groups_regions, z_undef, iinterp )
                generate( path, calc_case, med_file, med_groups_regions, z_undef, regions_interp_method )
                msg = "InterpolZ script is successfully generated"                
                result = True
                
                if isScriptExec:
                    msg = msg + " and executed"
                    exec(open(path).read())
        
            QMessageBox.information( self, "", msg )
            return result
            
        elif self.tabWidget.currentIndex() == 1: #bathymetry tab
            bathy_name = str(self.BathyName.text())
            group_name = str(self.medGroupNames.currentText())
            interp = str(self.InterpMethod_B.currentText())
            z_undef = self.UndefZ_B.value()

            msg = ""
            if len(path)==0:
                msg = "Please input the output path"
            elif len(bathy_name)==0:
                msg = "Please choose the bathymetry"
            elif len(med_file)==0:
                msg = "Please choose the MED file"
            elif len(group_name)==0:
                msg = "Please choose MED group"
            elif len(interp)==0:
                msg = "Please choose interpolation method"
        
            result = False
            
            if len(msg)==0:
                iinterp = 0
                if 'Linear' in interp:
                    iinterp = 1
                generate_B( path, bathy_name, med_file, group_name, z_undef, iinterp )
                msg = "InterpolZ_B script is successfully generated"
                result = True
                
                if isScriptExec:
                    msg = msg + " and executed"
                    exec(open(path).read())
        
            QMessageBox.information( self, "", msg )
            return result

    def onClose( self ):
        self.close()

    """Shows help page"""
    def onHelp( self ):
        msg = """
        <h1>Interpolz dialog</h1>

        This dialog is a tool for generation and execution of the script <b>interpolz.py</b>.

        <h3>Input MED file</h3>
        The path to MED file where MED groups are extracted.

        <h2>TAB: Calculation Case</h2>

        <ol><h3>Calculation case</h3>
        The name of the calculation case. It can be filled automatically on the base of selection or can be set by user.

        <h3>Method</h3>
        The interpolation method (interpolation at the nearest point and linear interpolation from a triangulation of the cloud of points).

        <h3>Undefined Z</h3>
        The Z value for nodes outside the regions.

        <h3>Table</h3>
        The table with MED groups and regions names.</ol>

        <h2>TAB: MED group bathymetry</h2>
        
        <ol><h3>Bathymetry</h3>
        The name of the bathymetry. It can be filled automatically on the base of selection or can be set by user.

        <h3>MED Group</h3>
        Select the face group on which to apply the new bathymetry.
        
        <h3>Method</h3>
        The interpolation method (interpolation at the nearest point and linear interpolation from a triangulation of the cloud of points).

        <h3>Undefined Z</h3>
        The Z value for nodes outside the regions.</ol>

        <h3>Script output path</h3>
        The path for the output, i.e. the path of the target script interpolz.

        <h3>Execute the script</h3>
        Execute the script on Apply if checked, or just keep it for later use.

        When the data is set, the user clicks on "Apply" or "Apply and Close" button to perform the script generation.
        """
        QMessageBox.about(self, self.tr("About bathymetry interpolation dialog"), msg);


if __name__=='__main__':
  dlg = InterpolzDlg()
  dlg.show()

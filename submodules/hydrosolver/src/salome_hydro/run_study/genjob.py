#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.
"""
Script for launch a steering file via the jobmanager
"""

import os
import tempfile
from datetime import datetime
import glob

import salome

JOB_SCRIPT_TEMPLATE = """#!/bin/sh

. %(env_file)s
runcode.py %(code)s %(cas)s --ncsize %(nbcore)d
cd -
"""

def generate_job(study_params, resource, telemac_root_dir, telemac_env_file,
                 nbcore, input_data_dir, result_dir):
    """
    Create a Launcher job using the parameters specified by the user.
    """
    # Generate job script
    basename = os.path.basename(study_params["FICHIER_CAS"])
    job_script = JOB_SCRIPT_TEMPLATE % {"env_file": telemac_env_file,
                                        "code": study_params["CODE"],
                                        "cas": basename,
                                        "nbcore": nbcore}


    (fd, job_script_file) = \
            tempfile.mkstemp(prefix="job_" + basename + "_", suffix=".sh")
    os.close(fd)
    f = open(job_script_file, "w")
    f.write(job_script)
    f.close()

    # Define job parameters
    job_params = salome.JobParameters()
    job_params.job_name = basename
    job_params.job_type = "command"
    job_params.job_file = job_script_file
    input_files = glob.glob(os.path.join(input_data_dir, "*")) \
                  + [study_params["FICHIER_CAS"]]
    input_files = list(set(input_files)) # Remove duplicates
    job_params.in_files = input_files
    job_params.out_files = []
    job_params.result_directory = result_dir

    # Define resource parameters
    job_params.resource_required = salome.ResourceParameters()
    job_params.resource_required.name = resource
    job_params.resource_required.nb_proc = nbcore

    # Generate name for the working directory
    res_manager = salome.naming_service.Resolve("/ResourcesManager")
    res_definition = res_manager.GetResourceDefinition(resource)
    res_work_dir = res_definition.working_directory
    if res_work_dir != "":
        timestr = datetime.now().ctime()
        timestr = timestr.replace('/', '_')
        timestr = timestr.replace('-', '_')
        timestr = timestr.replace(':', '_')
        timestr = timestr.replace(' ', '_')
        work_dir = res_work_dir + "/" + job_params.job_name + "_" + timestr
        job_params.work_directory = work_dir

    # Create Launcher job
    launcher = salome.naming_service.Resolve('/SalomeLauncher')
    launcher.createJob(job_params)

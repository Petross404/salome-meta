# -*- coding: utf-8 -*-
#
#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

from Accas import *

codelist = ("artemis", "postel3d", "sisyphe", "stbtel", "telemac2d", "telemac3d", "tomawac", "mascaret")

JdC = JDC_CATA(regles = (UN_PARMI('RUN_STUDY',)),
                        )
RUN_STUDY = PROC(
    nom = "RUN_STUDY", op = None,
    fr = "Définition d'un cas pour le lanceur Python",
    ang = "Definition of a case for the Python launcher",
    CODE = SIMP(statut = "o", typ = "TXM", into = codelist, defaut = "telemac2d",
                fr = "Code à exécuter",
                ang = "Code to run"),
    FICHIER_CAS = SIMP(statut = "o", typ = 'Fichier',
                       fr = "Fichier de description du cas",
                       ang = "Case description file"),
    REPERTOIRE_TRAVAIL = SIMP(statut = "f", typ = 'Repertoire',
                              fr = "Répertoire de travail",
                              ang = "Working directory"),
)

TEXTE_NEW_JDC="RUN_STUDY()"

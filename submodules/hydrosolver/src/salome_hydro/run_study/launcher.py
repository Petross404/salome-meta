#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.
"""
Script for launch in terminal of a steering file
"""

import os
import subprocess
import tempfile

from salome.kernel.logger import Logger
from salome.kernel import termcolor
logger = Logger("salome.hydro.run_study.launcher", color = termcolor.BLUE)
#logger.setLevel(logging.ERROR)

def run_study(param_dict):
  """
  Run the Python Telemac launching script, eventually preceded and
  followed by data conversion scripts.
  """
  interm_files = [] # Intermediate files that can eventually be deleted in the end

  # Get and eventually create working directory
  if "REPERTOIRE_TRAVAIL" in param_dict:
    wrkdir = param_dict["REPERTOIRE_TRAVAIL"]
    if not os.path.exists(wrkdir):
      os.makedirs(wrkdir)
  else:
    wrkdir = tempfile.mkdtemp(prefix = "tel-")
    interm_files += [wrkdir]

  # Read original steering file
  steering_filepath = param_dict["FICHIER_CAS"]
  steering_file_dir = os.path.dirname(steering_filepath)
  with open(steering_filepath) as f:
    orig_steering = f.read()
  orig_steering_lines = orig_steering.split("\n")


  # Run the code itself
  code = param_dict["CODE"]
  cmd = "runcode.py {code} -w {wrkdir} {steering_filepath} -s " \
         .format(code=code,
                 wrkdir=wrkdir,
                 steering_filepath=os.path.basename(steering_filepath))

  # Launch the command
  logger.debug("Running the following command in salome shell in %s: %s", steering_file_dir, cmd)
  try:
      log = subprocess.check_output(cmd, cwd=steering_file_dir, stderr=subprocess.STDOUT, shell=True)
      passed = True
      log = log.decode('utf-8')
  except subprocess.CalledProcessError as e:
      passed = False
      log = str(e)+"\n"+e.output.decode('utf-8')

  return passed, log


def check_file_or_create_link(filepath, dirpath, interm_file_list = None):
  """
  This utility function checks if the file *filepath* is in directory *dirpath*.
  If not, it creates a link with a unique name in *dirpath* to the file *filepath*
  and it prepends the link path to the list *interm_file_list* if it is not None.
  It returns the name of the link if a link was created, or the name of the file
  *filepath* otherwise.
  """
  filename = os.path.basename(filepath)
  if not os.path.samefile(dirpath, os.path.dirname(filepath)):
    name_wo_ext, ext = os.path.splitext(filename)
    linkpath = tempfile.mktemp(dir = dirpath, prefix = name_wo_ext + "_", suffix = ext)
    os.symlink(filepath, linkpath)
    filename = os.path.basename(linkpath)
    if interm_file_list is not None:
      interm_file_list[:0] = [linkpath]
  return filename

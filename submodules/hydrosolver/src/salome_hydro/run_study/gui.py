#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import SalomePyQt
sgPyQt = SalomePyQt.SalomePyQt()

from salome.hydro.gui_utils import get_and_check_selected_file_path
from salome.hydro.study import jdc_to_dict

from salome.hydro.run_study.eficas.appli import EficasForRunStudyAppli
from .launcher import run_study
from .genjobwindow import GenJobDialog

from PyQt5.QtWidgets import  QFileDialog, QMessageBox, QTextEdit

def create_case_study():
    EficasForRunStudyAppli()

def edit_selected_case_study():
    file=get_and_check_selected_file_path()
    if file == None:
        file, filt = QFileDialog.getOpenFileName(sgPyQt.getDesktop(), "Open study file", "", )
    EficasForRunStudyAppli(fichier=file)

def get_params_from_selected_case():
  """
  Get the parameters dict from the selected case in Salome study
  """
  jdcpath = get_and_check_selected_file_path()
  with open(jdcpath) as jdcfile:
    jdc = jdcfile.read()
  param_dict = jdc_to_dict(jdc, ["RUN_STUDY", "_F"])
  return param_dict

def run_selected_case_study():
  param_dict = get_params_from_selected_case()

  view = sgPyQt.getDesktop()

  QMessageBox.information(view, "Information", "The job is running.")
  passed, tmp = run_study(param_dict)
  qmsg = QMessageBox(view)
  qmsg.setIcon(QMessageBox.Information)
  if passed:
      qmsg.setText("Job successfull"+" "*80)
  else:
      qmsg.setText("Job failed"+" "*85)
  qmsg.setDetailedText(tmp)
  qmsg.exec_()

def generate_job_for_selected_case_study():
  param_dict = get_params_from_selected_case()
  dialog = GenJobDialog(sgPyQt.getDesktop(), param_dict)
  dialog.exec_()

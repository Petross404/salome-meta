#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

ADD_SUBDIRECTORY(eficas)

INCLUDE(UsePyQt)

# --- Python files ---

SET(PYFILES
  __init__.py
  genjob.py
  genjobwindow.py
  launcher.py
  gui.py
)

# uic files / to be processed by pyuic
SET(UIFILES
  genjobwindow.ui
)

# scripts / pyuic wrappings
PYQT_WRAP_UIC(_pyuic_SCRIPTS ${UIFILES})

# --- rules ---

# scripts / pyuic wrappings

SALOME_INSTALL_SCRIPTS("${PYFILES}" ${SALOME_INSTALL_PYTHON}/salome/hydro/run_study)
INSTALL( FILES ${_pyuic_SCRIPTS} DESTINATION ${SALOME_INSTALL_PYTHON}/salome/hydro/run_study)



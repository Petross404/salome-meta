#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
from PyQt5.QtWidgets import QDialog , QFileDialog

import salome
from salome.hydro.run_study.genjob import generate_job
from .genjobwindow_ui import Ui_GenJobDialog


class GenJobDialog(QDialog, Ui_GenJobDialog):

  def __init__(self, parent, study_params):
    QDialog.__init__(self, parent)
    self.setupUi(self)
    self.dialogButtonBox.accepted.connect(self.validate)
    self.dialogButtonBox.rejected.connect( self.close)
    self.chooseInputDataDirectoryButton.clicked.connect( self.choose_input_dir)
    self.chooseResultDirectoryButton.clicked.connect(self.choose_result_dir)

    self.telemacRootDirLE.setText("/projets/systel/V8P0")
    self.telemacEnvFileLE.setText("/projets/systel/V8P0/config/pysource.eole.intel.sh")
    casedir = os.path.dirname(study_params["FICHIER_CAS"])
    self.inputDataDirectoryLE.setText(casedir)
    self.resultDirectoryLE.setText(casedir)

    # Populate resource combo box
    res_manager = salome.naming_service.Resolve("/ResourcesManager")
    res_params = salome.ResourceParameters()
    res_list = res_manager.GetFittingResources(res_params)
    self.resourceCB.addItems(res_list)

    self.study_params = study_params

  def choose_input_dir(self):
    directory = QFileDialog.getExistingDirectory(self,
            directory = self.inputDataDirectoryLE.text(),
            options = QFileDialog.ShowDirsOnly)
    if not directory.isNull():
      self.inputDataDirectoryLE.setText(directory)

  def choose_result_dir(self):
    directory = QFileDialog.getExistingDirectory(self,
            directory = self.resultDirectoryLE.text(),
            options = QFileDialog.ShowDirsOnly)
    if not directory.isNull():
      self.resultDirectoryLE.setText(directory)

  def validate(self):
    generate_job(self.study_params,
                 str(self.resourceCB.currentText()),
                 str(self.telemacRootDirLE.text()),
                 str(self.telemacEnvFileLE.text()),
                 self.nbCoreSB.value(),
                 str(self.inputDataDirectoryLE.text()),
                 str(self.resultDirectoryLE.text()))
    self.close()

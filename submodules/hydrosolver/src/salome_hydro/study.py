#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import types

from salome.kernel.studyedit import getStudyEditor
from salome.kernel.parametric import study_exchange_vars

from .param_study.generate_study import generate_study_script, generate_study_yacs

import HYDROSOLVER_ORB

# module constants
MODULE_NAME = "HYDROSOLVER"

COMPONENT_NAME = "HydroSolver"
COMPONENT_ICON = "HYDRO_small.png"

VARS_ICON = "icon_variables.png"

TELMA_FILE_TYPE = "TELMA_EFICAS_FILE"
TELMA_ICON = "telma_file.png"
TELMA_TYPE_ID = 1

STUDY_FILE_TYPE = "STUDY_EFICAS_FILE"
STUDY_ICON = "study_file.png"
STUDY_TYPE_ID = 2

PARAM_STUDY_FILE_TYPE = "PARAM_STUDY_EFICAS_FILE"
PARAM_STUDY_ICON = "param_study_file.png"
PARAM_STUDY_TYPE_ID = 3

def jdc_to_dict(jdc, command_list):
    """
    This tricky function transforms a JdC with a single command into a
    dictionary that can be used more easily from a Python context (thanks to
    M. Courtois and G. Boulant).
    """
    context = {}
    for command in command_list:
        context[command] = _args_to_dict
    exec("parameters = " + jdc.strip(), context)
    return context['parameters']

def _args_to_dict(**kwargs):
    return kwargs

def get_jdc_dict_var_as_tuple(jdc_dict, varname):
    if varname not in jdc_dict:
        return tuple()
    elif not isinstance(jdc_dict[varname], tuple):
        return (jdc_dict[varname],)
    else:
        return jdc_dict[varname]

class HydroStudyEditor:
    """
    This class provides utility methods to edit the component "Hydro" in
    the study. The parameter `studyId` defines the ID of the study to edit. If
    it is :const:`None`, the edited study will be the current study.
    """
    def __init__(self, studyId = None):
        self.editor = getStudyEditor()
        self.hydroComp = None

    def find_or_create_hydro_component(self):
        """
        Find the component "Hydro" or create it if none is found
        :return: the SComponent found or created.
        """
        if self.hydroComp is None:
            self.hydroComp = self.editor.findOrCreateComponent(MODULE_NAME,
                                                               COMPONENT_NAME,
                                                               COMPONENT_ICON)
        return self.hydroComp

    def find_or_create_telma(self, filePath):
        self.find_or_create_hydro_component()
        itemName = os.path.splitext(os.path.basename(filePath))[0]
        sobj = self.editor.findOrCreateItem(self.hydroComp,
                                            name=itemName,
                                            fileType=TELMA_FILE_TYPE,
                                            fileName=filePath,
                                            icon=TELMA_ICON,
                                            comment=str(filePath),
                                            typeId=TELMA_TYPE_ID)

    def find_or_create_param_study(self, filePath):
        self.find_or_create_hydro_component()
        itemName = os.path.splitext(os.path.basename(filePath))[0]
        sobj = self.editor.findOrCreateItem(self.hydroComp,
                                            name=itemName,
                                            fileType=PARAM_STUDY_FILE_TYPE,
                                            fileName=filePath,
                                            icon=PARAM_STUDY_ICON,
                                            comment=str(filePath),
                                            typeId=PARAM_STUDY_TYPE_ID)

    def find_or_create_run_study(self, filePath):
        self.find_or_create_hydro_component()
        itemName = os.path.splitext(os.path.basename(filePath))[0]
        sobj = self.editor.findOrCreateItem(self.hydroComp,
                                            name=itemName,
                                            fileType=STUDY_FILE_TYPE,
                                            fileName=filePath,
                                            icon=STUDY_ICON,
                                            comment=str(filePath),
                                            typeId=STUDY_TYPE_ID)

    def generate_study_script(self, filePath):
        """
        Generating a python script from the eficas info
        """
        # Create "Python script" item
        with open(filePath) as jdcfile:
            jdc = jdcfile.read()
        params = jdc_to_dict(jdc, ["TELEMAC2D", "_F"])

        # Generation script string
        python_script = generate_study_script(params)

        # Computing name of the file (same as filePath)
        file_dir, filename = os.path.split(filePath)
        root, _ = os.path.splitext(filename)
        python_file = os.path.join(file_dir, root+".py")

        # Writting to file
        with open(python_file, 'w') as pfile:
            pfile.write(python_script)

        return python_file

    def generate_study_yacs(self, filePath):
        """
        Generating a yacs file from the eficas info
        """
        # Create "Python script" item
        with open(filePath) as jdcfile:
            jdc = jdcfile.read()
        params = jdc_to_dict(jdc, ["TELEMAC2D", "_F"])

        # Generation YACS scheme
        yacs_scheme = generate_study_yacs(params)

        # Computing name of the file (same as filePath)
        file_dir, filename = os.path.split(filePath)
        root, _ = os.path.splitext(filename)
        yacs_file = os.path.join(file_dir, root+".xml")

        # Writting to file
        yacs_scheme.saveSchema(yacs_file)

        return yacs_file

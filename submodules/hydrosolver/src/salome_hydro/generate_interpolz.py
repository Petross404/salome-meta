
import os
import sysconfig
pythonVersion = 'python' + sysconfig.get_python_version()
hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')

def replace( lines, pattern, subst ):
  for i in range( 0, len( lines ) ):
    line = lines[i]
    if pattern in lines[i]:
      if isinstance( subst, list ):
        del lines[i]
        for s in subst:
          new_line = line.replace( pattern, repr(s) )
          lines.insert( i, new_line )
      elif isinstance( subst, dict ):
        del lines[i]
        name = pattern[1:-1]
        lines.insert( i, "%s = {}\n" % name )
        keys = list(subst.keys())
        keys.sort()
        i = i+1
        for k in keys:
          v = subst[k]
          lines.insert( i, "%s[%s] = %s\n" % (name, repr(k), repr(v) ) )
          i = i+1
      else:
        new_line = line.replace( pattern, repr(subst) )
        lines[i] = new_line


def generate( path, calc_case, med_file, med_groups_regions, z_undef, regions_interp_method ):
  f = open( path, "w" )
  tf = open( hydro_solver_root+"/interpolz.template", "r" )
  templ = tf.readlines()

  replace( templ, "<case_name_from_dlg>", calc_case )
  replace( templ, "<MED_file_path_from_dlg>", med_file )
  replace( templ, "<dictGroupRegion>", med_groups_regions )
  replace( templ, "<z_undef_value_from_dlg>", z_undef )
  replace( templ, "<dict_region_interpolation_method>", regions_interp_method )

  for line in templ:
    f.write( line )
  f.close()

def generate_B( path, bathy_name, med_file, group_name, z_undef, iinterp ):
  f = open( path, "w" )
  tf = open( hydro_solver_root+"/interpolz_b.template", "r" )
  templ = tf.readlines()

  replace( templ, "<bathy_name_from_dlg>", bathy_name )
  replace( templ, "<MED_file_path_from_dlg>", med_file )
  replace( templ, "<MED_Group>", group_name )
  replace( templ, "<z_undef_value_from_dlg>", z_undef )
  replace( templ, "<interpolation_method>", iinterp )

  for line in templ:
    f.write( line )
  f.close()

if __name__=='__main__':
    generate( "test_interpolz.py", "case_1", "mesh.med", {"group_1":"reg_1", "group_2":"reg_2"}, -9999, {"reg_1" : 0, "reg_2": 1} )

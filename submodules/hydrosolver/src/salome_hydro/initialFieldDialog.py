#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import shutil

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

import salome
salome.salome_init()

import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()

from salome.hydro.initialField import set_var_on_group

import sysconfig
pythonVersion = 'python' + sysconfig.get_python_version()
hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')

def get_med_groups( file_path ):
    """
    Get all the groups of faces in the first mesh
    """
    try:
        (meshes, status) = smesh.CreateMeshesFromMED(file_path)
    except:
        print('No meshes found')
        return (None, [])
    if len(meshes)==0:
        print('No mesh found')
        return (None, [])
    mesh1 = meshes[0]
    print('Found mesh:', mesh1)
    try:
        grps = mesh1.GetGroups()
        groups = [grp.GetName().strip() for grp in grps if grp.GetType() == SMESH.FACE]
        if len(groups) == 0:
            print("Problem! There are no groups of faces in the mesh!")
            print("Please create at least the groups of faces corresponding to each region of the HYDRO case")
            return (mesh1, [])
        print('Found groups:', groups)
    except:
        print('No groups found')
        return (mesh1, [])
    return (mesh1, groups)

class initialFieldDialog(QDialog):
    """
    """

    def __init__(self, parent = None, modal = 0):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(hydro_solver_root, 'initialField.ui'), self )

        # Connections
        self.pb_medFileIn.clicked.connect(self.on_med_file_in_browse)
        self.pb_medFileOut.clicked.connect(self.on_med_file_out_browse)
        self.pb_reset.clicked.connect(self.on_fieldReset)
        self.pb_help.clicked.connect(self.on_help)
        self.pb_ok.accepted.connect(self.on_accept)
        self.pb_ok.rejected.connect(self.on_reject)
        self.dsb_defaultValue.valueChanged.connect(self.on_defaultValueChanged)
        self.medFileIn = None
        self.medFileOut = None
        self.le_field.setText("WATER DEPTH")
        self.dsb_defaultValue.setValue(0.01)
        self.previousDefaultValue = self.dsb_defaultValue.value()
        self.med_groups = []
        
    def on_med_file_in_browse(self):
        """
        Select input MED file
        """
        print("on_med_file_in_browse")
        self.medFileIn, filt = QFileDialog.getOpenFileName(self, self.tr("Input MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFileIn)
        if not self.medFileIn:
            return
        self.le_medFileIn.setText(self.medFileIn)
        if not os.path.isfile(self.medFileIn):
            msgBox = QMessageBox()
            msgBox.setText( "Input MED file does not exist" )
            msgBox.exec_()
            return        
        a = os.path.splitext(self.medFileIn)
        self.medFileOut = a[0] + '_ic' + a[1]
        self.le_medFileOut.setText(self.medFileOut)
        self.onMEDChanged()
        
    def onMEDChanged(self):
        mesh, self.med_groups = get_med_groups(self.medFileIn)
        self.meshName = mesh.GetName()
        print(self.meshName)
        print(self.med_groups)
        
        self.dsbItems = []
        n = len(self.med_groups)
        self.tw_fieldValues.setRowCount(n)
        for i in range(n):
            if self.tw_fieldValues.item( i, 0 ) is None:
                self.tw_fieldValues.setItem( i, 0, QTableWidgetItem() )
                self.tw_fieldValues.setItem( i, 1, QTableWidgetItem() )
                self.tw_fieldValues.item( i, 0 ).setText( self.med_groups[i] )
            dsb = QDoubleSpinBox()
            dsb.setDecimals(3)
            dsb.setRange(-9999.0, 9999.0)
            dsb.setSingleStep(1.0)
            dsb.setValue(self.dsb_defaultValue.value())
            self.tw_fieldValues.setCellWidget( i, 1, dsb )
            self.dsbItems.append(dsb)
        
    def on_med_file_out_browse(self):
        """
        Select output MED file
        """
        print("on_med_file_out_browse")
        self.medFileOut, filt = QFileDialog.getSaveFileName(self, self.tr("Output MED file"), "", self.tr("MED files (*.med)"))
        print(self.medFileOut)
        if not self.medFileOut:
            return
        self.le_medFileOut.setText(self.medFileOut)
        
    def on_fieldReset(self):
        """
        Reset the values on each group to default value, erasing previous settings
        """
        print("onFieldReset")
        for dsb in self.dsbItems:
            dsb.setValue(self.dsb_defaultValue.value())
            
    def on_defaultValueChanged(self):
        """
        change the values set to previous default to new default, leaving other values unchanged
        """
        print("on_defaultValueChanged")
        for dsb in self.dsbItems:
            if dsb.value() == self.previousDefaultValue:
                dsb.setValue(self.dsb_defaultValue.value())
        self.previousDefaultValue = self.dsb_defaultValue.value()
       
    def on_help(self):
        """
        display a help message
        """
        msg = """
        <h2>Initial field dialog</h2>

        This dialog is used set an initial field with constant values per groups of faces in a mesh.
        Typical use is to set an initial water depth on the minor bed of a river, for instance.
        <br><br>        
        The modified mesh is saved in a new file by default.
        <br>
        All the groups of faces are listed with their name, and a constant default value is set by default for each group.
        It is possible to modify the value associated with some groups. It is also possible to modify the default value,
        or to reset all the values to the default.
        <br>     
         <br><br>         
        Below is the description of the dialog controls.

        <h3>Input MED file</h3>
        This field allows the selection of a med file (via the standard file open dialog).
        The filling of this field is mandatory.

        <h3>Output MED file</h3>
        This field allows the definition of a med file (via the standard file save dialog).
        The filling of this field is mandatory. It is set by default, with a suffix on input MED file.
        It can be set equal to input MED file.
        
        <h3>Field name</h3>
        The name of the field to create. 

        <h3>default value</h3>
        The default value for the field. When modified, groups set to the default value are modified.
        
        <h3>reset</h3>
        This button forces a general reset of all the groups to the default value.
        
        <h3>table group value</h3>
        This table is filled with one line per group of faces found in the mesh
        """
        QMessageBox.about(self, self.tr("About mesh edges to shapes dialog"), msg);
       
   
    def on_accept(self):
        print("accept")
        #TODO check medfile in and out not empty
        #TODO preset for medFileOut
        medFileIn = self.le_medFileIn.text()
        medFileOut = self.le_medFileOut.text()
        meshName = self.meshName
        fieldName = self.le_field.text()
        groups = []
        n = len(self.med_groups)
        for i in range(n):
            groups.append((self.med_groups[i], self.dsbItems[i].value()))
        print(medFileIn)
        print(medFileOut)
        print(meshName)
        print(fieldName)
        print(groups)

        if not os.path.isfile(medFileIn):
            msgBox = QMessageBox()
            msgBox.setText( "Input MED file does not exist" )
            msgBox.exec_()
            return
        dir = os.path.dirname(medFileOut)
        base = os.path.basename(medFileOut)
        ext = os.path.splitext(medFileOut)[1]
        if not os.path.isdir(dir) or (base == "") or (ext.lower() != ".med"):
            msgBox = QMessageBox()
            msgBox.setText( "Output MED file is not a valid path" )
            msgBox.exec_()
            return
        if medFileOut != medFileIn:
            shutil.copyfile(medFileIn, medFileOut)
        set_var_on_group(medFileOut, meshName, groups, fieldName)
        self.accept()
        
        #initialField(medFileIn, medFileOut, epsgIn, epsgOut, offsetXin, offsetYin, offsetXout, offsetYout)

    def on_reject(self):
        print("reject")
        self.reject()
        

def execDialog(context):
    print("execDialog")
  # get context study, salomeGui
    study = context.study
    sg = context.sg
    dlg = initialFieldDialog()
    dlg.exec_()        

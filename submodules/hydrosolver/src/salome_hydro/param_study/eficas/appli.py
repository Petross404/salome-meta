# -*- coding: utf-8 -*-
#
#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import re

from PyQt5.QtWidgets import QMessageBox , QScrollArea, QGridLayout


import salome
import SalomePyQt
sgPyQt = SalomePyQt.SalomePyQt()

from salome.kernel.logger import Logger
from salome.kernel import termcolor
logger = Logger("salome.hydro.param_study.eficas.appli",
                color = termcolor.GREEN_FG)

import eficasSalome

from salome.hydro.study import HydroStudyEditor

def importFirst():
    """
    Horrible hack to allow a correct load of a Telemac case file when done after
    some other edition, such as create case for Pytel execution.
    """
    eficasTelemacPath = os.path.join(eficasSalome.eficasConfig.eficasPath,'Telemac')
    sys.path.insert(0, eficasTelemacPath)
    from telemac2d_enum_auto import TelemacdicoEn
    sys.path.pop(0)

importFirst()

class EficasForParamStudyAppli(eficasSalome.MyEficas):
    """
    This class launches Eficas and adds entries for the created files in
    HYDRO component in the study tree. The messages in this class are in
    french because they are displayed in Eficas interface.

    :type  fichier: string
    :param fichier: path of an Eficas file to open

    """
    def __init__(self, code="telemac2d", fichier=None, version=None, lang=None):
        self.ed = HydroStudyEditor()
        self.codedir = os.path.dirname(__file__)
        sys.path[:0] = [self.codedir]
        area = QScrollArea(SalomePyQt.SalomePyQt().getDesktop());
        eficasSalome.MyEficas.__init__(self, area,
                                       code=code, fichier=fichier,
                                       version=version, lang=lang)
        gridLayout = QGridLayout(area)
        gridLayout.addWidget(self)
        area.setWidgetResizable(1)
        sgPyQt.createView("Eficas Telemac2D", self)
        if fichier == None:
            self.fileNew()
        else:
            self.addJdcInSalome(fichier)

    def addJdcInSalome(self, jdcPath):
        """
        Add the newly created file in Salome study
        """
        try:
            self.ed.find_or_create_param_study(jdcPath)
        except Exception as exc:
            msgError = "Can't add file to Salome study tree"
            logger.exception(msgError)
            QMessageBox.warning(self, self.tr("Warning"),
                                self.tr("%s. Reason:\n%s\n\nSee logs for "
                                        "more details." % (msgError, exc)))
        salome.sg.updateObjBrowser()

    def closeEvent(self, event):
        while self.codedir in sys.path:
            sys.path.remove(self.codedir)
        eficasSalome.MyEficas.closeEvent(self, event)

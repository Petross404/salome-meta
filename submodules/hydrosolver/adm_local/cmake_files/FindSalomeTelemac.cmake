#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

# Telemac detection for Salome
#
#  !! Please read the generic detection procedure in SalomeMacros.cmake !!
#
SALOME_FIND_PACKAGE_AND_DETECT_CONFLICTS(Telemac TELEMAC_ROOT_DIR 0)
MARK_AS_ADVANCED(TELEMAC_LIBRARY_api
                 TELEMAC_LIBRARY_bief
                 TELEMAC_LIBRARY_damocles
                 TELEMAC_LIBRARY_parallel
                 TELEMAC_LIBRARY_sisyphe
                 TELEMAC_LIBRARY_special
                 TELEMAC_LIBRARY_telemac2d
                 TELEMAC_LIBRARY_mascaret
                 TELEMAC_LIBRARY_tomawac
                 TELEMAC_LIBRARY_gretel
                 TELEMAC_LIBRARY_partel
                 TELEMAC_LIBRARY_hermes)

IF(Telemac_FOUND OR TELEMAC_FOUND)
  SALOME_ACCUMULATE_HEADERS(TELEMAC_INCLUDE_DIR)
ENDIF()

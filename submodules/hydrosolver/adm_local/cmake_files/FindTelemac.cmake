#  Copyright (C) 2012-2013 EDF
#
#  This file is part of SALOME HYDRO module.
#
#  SALOME HYDRO module is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SALOME HYDRO module is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.

FIND_LIBRARY(TELEMAC_LIBRARY_mascaret mascaret
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_api api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_bief bief4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_damocles damocles4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_parallel parallel4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_sisyphe sisyphe4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_special special4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_telemac2d telemac2d4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_tomawac tomawac4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_gretel gretel4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_partel partel4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_LIBRARY(TELEMAC_LIBRARY_hermes hermes4api
             PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/lib)
FIND_PATH(TELEMAC_INCLUDE_DIR interface_telemac2d.mod
          PATHS ${TELEMAC_ROOT_DIR}/builds/salomeHPC/wrap_api/include)

# Handle the standard arguments of the find_package() command:
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Telemac REQUIRED_VARS TELEMAC_LIBRARY_api
                                                        TELEMAC_LIBRARY_bief
                                                        TELEMAC_LIBRARY_damocles
                                                        TELEMAC_LIBRARY_parallel
                                                        TELEMAC_LIBRARY_sisyphe
                                                        TELEMAC_LIBRARY_special
                                                        TELEMAC_LIBRARY_telemac2d
                                                        TELEMAC_LIBRARY_mascaret
                                                        TELEMAC_LIBRARY_tomawac
                                                        TELEMAC_LIBRARY_gretel
                                                        TELEMAC_LIBRARY_partel
                                                        TELEMAC_LIBRARY_hermes)

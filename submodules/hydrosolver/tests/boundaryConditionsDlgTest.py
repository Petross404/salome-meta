import os
import sys

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

cur_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(cur_dir, "data")

#import sysconfig
#pythonVersion = 'python' + sysconfig.get_python_version()
#hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')
from BndConditionsDialog import BoundaryConditionsDialog

# Show the dialog
app = QApplication(sys.argv)
window = BoundaryConditionsDialog()

window.show()
sys.exit(app.exec_())



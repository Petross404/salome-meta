import os
import sys
import tempfile
import filecmp

import unittest

cur_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(cur_dir, "data")

#import sysconfig
#pythonVersion = 'python' + sysconfig.get_python_version()
#hydro_solver_root = os.path.join(os.environ['HYDROSOLVER_ROOT_DIR'], 'lib', pythonVersion, 'site-packages', 'salome', 'salome', 'hydro')
import boundaryConditions


class TestBoundaryConditions(unittest.TestCase):
    def testReadPresets(self):
        file_path = os.path.join(data_dir, "bnd_conditions_presets.txt")
        reader = boundaryConditions.PresetReader(file_path)
        presets = reader.read()
        self.assertEqual(3, len(presets))

        self.assertEqual(True, "Closed boundaries/walls" in presets)
        self.assertEqual((2,2,2,2), presets["Closed boundaries/walls"])

        self.assertEqual(True, "Incident waves" in presets)
        self.assertEqual((1,1,1,None), presets["Incident waves"])

        self.assertEqual(True, "Free T" in presets)
        self.assertEqual((None,None,None,4), presets["Free T"])

    def testRead(self):
        file_path = os.path.join(data_dir, "bnd_conditions1.cli")
        reader = boundaryConditions.BoundaryConditionReader(file_path)
        conditions = reader.read()
        self.assertEqual(7, len(conditions))
        self.assertEqual(0, len(reader.errors))

    def testWrite(self):
        file_path = os.path.join(tempfile.gettempdir(), "bnd_conditions_w.cli")
        writer = boundaryConditions.BoundaryConditionWriter(file_path)

        cnd1 = boundaryConditions.BoundaryCondition(5, 4, 4, 4, "group1")
        cnd2 = boundaryConditions.BoundaryCondition(4, 5, 5, 5, "group2")

        writer.write([cnd1, cnd2])
        self.assertEqual(True, os.path.exists(file_path))
        self.assertEqual(0, len(writer.errors))

        os.remove(file_path)

    def testReadWrite(self):
        input_file_path = os.path.join(data_dir, "bnd_conditions1.cli")
        reader = boundaryConditions.BoundaryConditionReader(input_file_path)
        conditions = reader.read()

        output_file_path = os.path.join(tempfile.gettempdir(), "bnd_conditions1_w.cli")
        writer = boundaryConditions.BoundaryConditionWriter(output_file_path)
        writer.write(conditions)

        try:
            self.assertEqual(True, filecmp.cmp(input_file_path, output_file_path))
        finally:
            os.remove(output_file_path)

unittest.main()
..
   Copyright (C) 2012-2013 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.


############
Simple Usage
############

Running a Telemac case
======================

.. |run_pytel_button| image:: /_static/create_case_pytel.png
   :align: middle

To run a Telemac case from Salome-Hydro, activate HydroSolver module and click
on the button |run_pytel_button|. A new dialog appears (the *Eficas* interface)
that let you choose the case file, dictionary, geometry file, etc. When all the
desired files are selected, save this case description file. It will appear in
Salome object browser. Right-click on the item in the object browser and select
"Compute case" in the popup menu. Telemac will start the computation and the
execution log will be shown in a terminal.

**Note**: In case of a crash or errors, check your */tmp* directory to find log
files.

**Warning**: If you use the *Universal* distribution of Salome-Hydro on a
platform that does not include gfortran 4.4.5 or a compatible version, you will
not be able to use a user fortran file with your Telemac case.

Running a Telemac 2D case with the API
======================================

.. |create_t2d_api_button| image:: /_static/create_case2d.png
   :align: middle

To run a Telemac 2D case through the TELEMAC2D API from Salome-Hydro, activate
HydroSolver module and click on the button |create_t2d_api_button|. A new
dialog appears that let you choose the case file, dictionary, geometry file,
etc. When all the desired files are selected, save this case description file.
It will appear in Salome object browser. Right-click on the item in the object
browser and select "Compute case" in the popup menu. Telemac will start the
computation.

**Note**: Nothing is shown in the interface for now with this service. Check
your */tmp* directory to find log files.

**Warning**: If you use the *Universal* distribution of Salome-Hydro on a
platform that does not include gfortran 4.4.5 or a compatible version, you will
not be able to use a user fortran file with your Telemac 2D case.

**Warning**: Due to a remaining bug, it is mandatory with this service to have a
*CONFIG* file in the same directory as the case file, containing the
declarations for *LU* and *LNG* variables.

Running a Mascaret case
=======================

.. |create_mascaret_button| image:: /_static/create_case1d.png
   :align: middle

To run a Mascaret case from Salome-Hydro, activate HydroSolver module and click
on the button |create_mascaret_button|. A new dialog appears that let you
choose the case file, dictionary, geometry file, etc. You can also add several
output variables with the parameter "VARIABLE_SORTIE". The name ("NOM") is
free, but the Mascaret variable ("VARIABLE_MASCARET") must use a specific
syntax inspired by Mascaret API. This syntax is
*Type.NomVar.SubVar(idx1, idx2, idx3)*, where *Type* is "Etat" or "Modele",
*NomVar* and *SubVar* are variables names (*SubVar* is not necessary for all
variables), and *idxX* are indexes if the variable is an array. For instance,
*Etat.Z(1,0,0)* is the water heigth on the first section of the model.

When all the desired files are selected, save this case description file. It
will appear in Salome object browser. Right-click on the item in the object
browser and select "Compute case" in the popup menu. Mascaret will start the
computation of the case. Unfortunately, Mascaret logs are not available yet
through Mascaret API, so the only output available is the value of the
specified output variables.

**Note**: In case of a crash or errors, check your */tmp* directory to find log
files.

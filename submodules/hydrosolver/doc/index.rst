..
   Copyright (C) 2012-2013 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
HydroSolver module documentation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

.. |hydrosolver_module_button| image:: /_static/HYDRO.png
   :align: middle
   :width: 16pt
   :height: 16pt

This documentation covers the usage of HydroSolver module in Salome. This module
aims at providing services for the usage of Telemac and Mascaret codes. To
activate this module, click on the |hydrosolver_module_button| button in the
toolbar or select "HydroSolver" item in the "SALOME" menu. The first part of
this guide addresses simple use cases (how to run the codes from Salome-Hydro
platform) while the second part presents more advanced topics (code coupling).

.. toctree::
   :maxdepth: 2

   basic.rst
   advanced.rst

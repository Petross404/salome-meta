..
   Copyright (C) 2012-2013 EDF

   This file is part of SALOME HYDRO module.

   SALOME HYDRO module is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SALOME HYDRO module is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SALOME HYDRO module.  If not, see <http://www.gnu.org/licenses/>.


####################
Advanced usage guide
####################

Code coupling
=============

.. |run_coupling_button| image:: /_static/create_case_couplage.png
   :align: middle

Salome-Hydro offers a service to run coupled Mascaret - Telemac2D cases. First,
you need to create a Mascaret case and a Telemac2D API case. Then, to set up
the coupling, click on the button |run_coupling_button|. A new dialog appears
(the *Eficas* interface) that let you choose the Mascaret and Telemac2D cases,
an output file for the coupling log and another one in CSV format for graph
generation. You must also specify a file to describe the coupling itself. This
file must be a Python file declaring several variables. Here is an example of
such a file::

   # Time data
   # Note that Telemac2D API can't use those values for now, so T2D case must be adapted manually
   starttime = 0.0
   endtime = 270300.0
   timestep = 10.0
   
   # Tolerances for convergence iterations
   eps_Q = 1.0
   eps_Z = 0.01
   
   # Maximum number of convergence iterations
   maxiter = 5
   
   # Border descriptions
   upstream_border_mascaret = {"code": "MASCARET",
                               "position": "upstream",
                               "section_id": 40,
                               "loi_id": 2}
   
   upstream_border_telemac = {"code": "TELEMAC2D",
                              "position": "downstream",
                              "points_id": (1, 30), # First point included, last point excluded
                              "border_id": 1}
   
   downstream_border_telemac = {"code": "TELEMAC2D",
                                "position": "upstream",
                                "points_id": (139, 154), # First point included, last point excluded
                                "border_id": 3}
   
   downstream_border_mascaret = {"code": "MASCARET",
                                 "position": "downstream",
                                 "section_id": 41,
                                 "loi_id": 3}
   
   barrage_border_telemac = {"code": "TELEMAC2D",
                             "position": "upstream",
                             "points_id": (82, 101), # First point included, last point excluded
                             "border_id": 2}
   
   source_mascaret = {"code": "MASCARET",
                      "position": "downstream",
                      "section_id": 1,
                      "loi_id": 1}
   
   aval_mascaret = {"code": "MASCARET",
                    "position": "upstream",
                    "section_id": 79,
                    "loi_id": 4}
   
   borders = [{"name": "amont",
               "model1": upstream_border_mascaret,
               "model2": upstream_border_telemac},
              {"name": "ecluse",
               "model1": downstream_border_telemac,
               "model2": downstream_border_mascaret},
              {"name": "barrage",
               "model1": barrage_border_telemac,
               "model2": None},
              {"name": "source",
               "model1": source_mascaret,
               "model2": None},
              {"name": "aval",
               "model1": aval_mascaret,
               "model2": None}]

In this file, the coupling points (borders) are described by their two adjacent
models, represented by a Python dictionary. In this description, the parameter
"code" indicates which code is used on this side of the border (MASCARET or
TELEMAC2D). The parameter "position" indicates if it is on the upstream or
downstream side of the border.

The parameter "section_id" for MASCARET indicates the section identifiant of the
coupled point. The parameter "loi_id" for MASCARET indicates the identifiant of
the law used to set values on that point.

The parameter "points_id" for TELEMAC2D indicates the first and last point of
the border. The parameter "border_id" identifies the liquid boundary.

When all the desired files are selected, save this coupling description file. It
will appear in Salome object browser. Right-click on the item in the object
browser and select "Open schema in YACS" in the popup menu. YACS interface will
open and show the coupling schema. You can then launch the schema execution
directly in YACS (see YACS documentation for more information about launching
or editing the schema directly).

**Note**: In case of a crash or errors, check your */tmp* directory to find log
files.

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Modules Python
import sys
from os import path, listdir, system, chdir, environ, remove
from argparse import ArgumentParser
import shutil
import re

import salome
from execution.telemac_cas import TelemacCas, get_dico

# Adding Eficas to PYTHONPATH
print(environ["EFICAS_NOUVEAU_ROOT"])
sys.path.append(environ["EFICAS_NOUVEAU_ROOT"])
try:
    import Telemac.prefs
except ImportError as excp:
    print("Add the path to eficas to PYTHONPATH")
if hasattr(Telemac.prefs, 'encoding'):
    # Hack pour changer le codage par defaut des strings
    import sys
    reload(sys)
    sys.setdefaultencoding(prefs.encoding)
    del sys.setdefaultencoding
    # Fin hack

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

PREFIX = {
          't2d':'telemac2d',
          't3d':'telemac3d',
          'tom':'tomawac',
          'art':'artemis',
          'sis':'sisyphe',
          'waq':'waqtel',
          'gai':'gaia',
#         'ice':'khione',
#         'stb':'stbtel',
#         'p3d':'postel3d',
         }

for module in PREFIX.values():
    import importlib
    module_name = "{}_cata_auto".format(module)
    print(" ~> Trying to import :", module_name)
    _ = importlib.import_module(module_name)

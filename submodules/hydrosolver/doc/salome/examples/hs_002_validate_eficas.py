#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Modules Python
import sys
from os import path, listdir, system, chdir, environ, remove
from argparse import ArgumentParser
import shutil
import re

import salome
from execution.telemac_cas import TelemacCas, get_dico

# Adding Eficas to PYTHONPATH
print(environ["EFICAS_NOUVEAU_ROOT"])
sys.path.append(environ["EFICAS_NOUVEAU_ROOT"])
try:
    import Telemac.prefs
except ImportError as excp:
    print("Add the path to eficas to PYTHONPATH")
if hasattr(Telemac.prefs, 'encoding'):
    # Hack pour changer le codage par defaut des strings
    import sys
    reload(sys)
    sys.setdefaultencoding(prefs.encoding)
    del sys.setdefaultencoding
    # Fin hack

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

PREFIX = {
         't2d':'telemac2d',
         't3d':'telemac3d',
         'tom':'tomawac',
         'art':'artemis',
         'sis':'sisyphe',
         'waq':'waqtel',
         'gai':'gaia',
         'ice':'khione',
         'stb':'stbtel',
         'p3d':'postel3d',
         }

def diff_cas(cas1, cas2):
    """
    Diff between two cases will return True if cas.get for each keyword (in
    both files) returns the same thing

    @param cas1 (TelemacCas) cas1
    @param cas2 (TelemacCas) cas2

    @returns True if identical
    """

    # Get the list of keys from cas1 and cas2
    keys = list(cas1.values.keys())
    keys.extend(key for key in cas2.values.keys() if key not in keys)

    diff = True
    for key in keys:
        val1 = cas1.get(key)
        val2 = cas2.get(key)
        # This is to handles graphical outputs and so on
        if isinstance(val1, str) and isinstance(val2, str):
            val1 = val1.replace(";", ",").strip()
            val2 = val2.replace(";", ",").strip()

        # Specific replace for some variables
        if key in ['VARIABLES FOR 2D GRAPHIC PRINTOUTS']:
            val1 = val1.replace(' ', '')
            val2 = val2.replace(' ', '')

        if val1 != val2:
            diff = False
            print("Differences for {}\n{}: {}\n{}:{}"\
                  .format(key,
                          path.basename(cas1.file_name),
                          val1,
                          path.basename(cas2.file_name),
                          val2,
                          ))

    return diff


def read_write_eficas(module, steering_file, eficas_steering):
    """
    Import a steergin file in Eficas and save it back

    @param module Name of the telemac-mascaret module
    @param steering_file Name of the steering file
    @param eficas_file Name of the steering file written by Eficas
    """
    from InterfaceQT4.eficas_go import getEficasSsIhm
    my_eficas = getEficasSsIhm(code='TELEMAC', labelCode='TELEMAC'+module,
                               ssCode=module)

    handler = my_eficas.fileOpen(steering_file)
    if not handler:
        raise Exception(steering_file, "Eficas crashed")
    if not handler.isJdcValid():
        report = handler.getJdcRapport()
        raise Exception(steering_file, report)

    handler.fileSaveAs(eficas_steering)

def validate_catalog(root_dir):
    """
    Validate a given Catalog for a given module

    @param module Name of the module
    @param root_dir Telemac root path
    """

    # First check that we can import all catalogues
    # Other Python files are checked during damocles.py --eficas
    for module in PREFIX.values():
        import importlib
        module_name = "{}_cata_auto".format(module)
        _ = importlib.import_module(module_name)

    examples_dir = path.join(root_dir, 'examples')
    output_dir = path.join(root_dir, 'examples', 'eficas')

    crashed = []
    different = []
    for folder in sorted(listdir(examples_dir)):
        # Nothing to check in there
        if folder in ['python3', 'mascaret', 'courlis']:
            continue
        for example in sorted(listdir(path.join(examples_dir, folder))):
            example_dir = path.join(examples_dir, folder, example)
            # Skipping files
            if not path.isdir(example_dir):
                continue
            chdir(example_dir)
            print("  "*3, "~> In example folder ", example_dir)
            for case in sorted(listdir(example_dir)):
                if case.endswith('.cas') and \
                   "_reecrit" not in case and \
                   "_reecrit" not in case:

                    module = PREFIX.get(case[:3], None)
                    if module is None:
                        print("Skipping (prefix not handled): ", case)
                        continue

                    # Adding lang extension (.fr for translated french case)
                    print("  "*4, "~> For test case ", case)
                    root, _ = path.splitext(case)

                    lang = ''
                    eficas_case = root + "_reecrit.cas"


                    # Import and export in eficas
                    try:
                        read_write_eficas(module, case, eficas_case)
                    except Exception as e:
                        print(e)
                        crashed.append(folder+'/'+example_dir+'/'+case)
                        print(" "*8+FAIL+"FAILED"+ENDC)
                        print(" "*8+"Crashed in eficas")
                        continue

                    telemac_dico = path.join(root_dir, 'sources', module, module+'.dico')
                    ori_cas = TelemacCas(case, telemac_dico)
                    eficas_cas = TelemacCas(eficas_case, telemac_dico)

                    isdiff = diff_cas(ori_cas, eficas_cas)

                    if not isdiff:
                        different.append(folder+'/'+example_dir+'/'+case)
                        print(" "*8+FAIL+"FAILED"+ENDC)
                        print(" "*8+"Diff in steering case")
                        continue

                    # Clean up of files
                    remove(eficas_case)

                    # Passed the test case
                    print(" "*8+OKGREEN+"PASSED"+ENDC)

    return crashed, different


#-----------------
#-- Eficas
#-----------------
crasheds = []
differents = []

# Testing loading of catalog
crasheds = []
differents = []
crashed, different = validate_catalog(environ["HOMETEL"])
crasheds.extend(crashed)
differents.extend(different)

if crasheds != []:
    print("The following tests "
          " crashed in eficas:", crashed)
if differents != []:
    print("The following tests",
          " have a difference with normal run:", different)

if crasheds != [] or differents != []:
    sys.exit(1)

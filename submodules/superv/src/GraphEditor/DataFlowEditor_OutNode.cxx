//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowEditor_OutNode.cxx
//  Module : SUPERV

using namespace std;
#include <sstream>
#include <iostream>
#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowEditor_OutNode.hxx"
#include "DataFlowBase_EndOfLoopNode.hxx"
#include "DataFlowBase_EndOfSwitchNode.hxx"

//map< string , GraphBase::Graph * > GraphEditor::OutNode::_MapOfGraphs;
map< string , int > _MapOfGraphNames;

string GraphInstanceName( const char * aGraphName ) {
  // mkr : PAL8004 -->
  // For Macro nodes : names of subgraphs must be unique and differ from the main dataflow
  // name (i.e. the dataflow with MacroLevel = 0).
  // It is neccessary for reqursive call of LoadDataFlows(...) method for MacroNodes execution.
  int GraphInstanceNumber = _MapOfGraphNames[ aGraphName ] ;
  string theGraphInstanceName = string( aGraphName ) ;
  if ( GraphInstanceNumber ) {
    theGraphInstanceName += "_" ;
    ostringstream astr ;
    astr << GraphInstanceNumber ;
    theGraphInstanceName += astr.str() ;
  }
  if ( theGraphInstanceName != string( aGraphName ) )
    _MapOfGraphNames[ theGraphInstanceName ] = GraphInstanceNumber ;
  _MapOfGraphNames[ aGraphName ] = GraphInstanceNumber + 1 ;
  // mkr : PAL8004 <--
  return theGraphInstanceName ;
}


// Implementation de la classe GraphEditor::Graph

GraphEditor::OutNode::OutNode() {
//             Graph() {
  cdebug_in << "GraphEditor::OutNode::OutNode()" << endl;

  _Graph = NULL ;
  _Imported = false ;
  _Valid = false ;
  _Executable = false ;

  cdebug_out << "GraphEditor::OutNode::OutNode()" << endl;
}

GraphEditor::OutNode::OutNode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService * ptrNamingService ,
                               const char * DataFlowName ,
                               const char * DebugFileName ,
                               const SUPERV::KindOfNode aKindOfNode ) {
//             Graph( ORB , ptrNamingService , DataFlowName , DebugFileName ) {
  _Graph = NULL ;
  Set_prof_debug( ORB , DebugFileName ) ;
  cdebug_in << "GraphEditor::OutNode::OutNode(" ;
  if ( DataFlowName ) {
    cdebug << DataFlowName ;
  }
  cdebug << ")" << endl;

  if ( aKindOfNode == SUPERV::DataFlowGraph ) {
    _StreamGraph = NULL ;
    _Graph = new GraphBase::Graph( ORB , ptrNamingService , DataFlowName , aKindOfNode ,
                                   _prof_debug , _fdebug ) ; 
//    MapGraph( _Graph , _Graph->Name() ) ;
  }
  else if ( aKindOfNode == SUPERV::DataStreamGraph ) {
    _StreamGraph = new GraphBase::StreamGraph( ORB , ptrNamingService , DataFlowName , aKindOfNode ,
                                               _prof_debug , _fdebug ) ;
    _Graph = _StreamGraph ;
//    MapGraph( _Graph , _Graph->Name() ) ;
  }
  _Orb = CORBA::ORB::_duplicate( ORB ) ;
  _Imported = false ;
  _Valid = false ;
  _Executable = false ;

  cdebug_out << "GraphEditor::OutNode::OutNode" << endl;
}

GraphEditor::OutNode::OutNode(
                     CORBA::ORB_ptr ORB ,
                     SALOME_NamingService * ptrNamingService ,
                     const SALOME_ModuleCatalog::Service& DataFlowService ,
                     const char * DataFlowComponentName ,
                     const char * DataFlowInterfaceName ,
                     const char * DataFlowName ,
                     const SUPERV::KindOfNode DataFlowkind ,
                     const SUPERV::SDate DataFlowFirstCreation ,
                     const SUPERV::SDate DataFlowLastModification ,
                     const char * DataFlowEditorRelease ,
                     const char * DataFlowAuthor ,
                     const char * DataFlowComputer ,
                     const char * DataFlowComment ,
                     const char * DebugFileName ) {
  _Graph = NULL ;
  Set_prof_debug( ORB , DebugFileName ) ;

  if ( DataFlowkind == SUPERV::DataFlowGraph ) {
    _StreamGraph = NULL ;
    _Graph = new GraphBase::Graph( ORB , ptrNamingService , DataFlowService , DataFlowComponentName ,
                                   DataFlowInterfaceName , DataFlowName , DataFlowkind ,
                                   DataFlowFirstCreation , DataFlowLastModification  ,
                                   DataFlowEditorRelease , DataFlowAuthor ,
                                   DataFlowComputer , DataFlowComment ,
                                   _prof_debug , _fdebug ) ;
//    MapGraph( _Graph , _Graph->Name() ) ;
  }
  else if ( DataFlowkind == SUPERV::DataStreamGraph ) {
    _StreamGraph = new GraphBase::StreamGraph( ORB , ptrNamingService , DataFlowService , DataFlowComponentName ,
                                               DataFlowInterfaceName , DataFlowName , DataFlowkind ,
                                               DataFlowFirstCreation , DataFlowLastModification  ,
                                               DataFlowEditorRelease , DataFlowAuthor ,
                                               DataFlowComputer , DataFlowComment ,
                                               _prof_debug , _fdebug ) ;
    _Graph = _StreamGraph ;
//    MapGraph( _Graph , _Graph->Name() ) ;
  }
  _Orb = CORBA::ORB::_duplicate( ORB ) ;
  _Imported = false ;
  _Valid = false ;
  _Executable = false ;

} ;

GraphEditor::OutNode::~OutNode() {
//  EraseGraph( Graph->Name() ) ;
//  delete _DataFlowNode ;
//  delete _DataFlowDatas ;
//  delete _GT ;
}

bool GraphEditor::OutNode::Name( const char * aName ) {
  bool RetVal = false ;
//  if ( !GraphName( aName ) ) {
//    char * aGraphName = Graph()->Name() ;
    RetVal = Graph()->Name( aName ) ;
//    if ( RetVal ) {
//      EraseGraph( aGraphName ) ;
//      MapGraph( Graph() , aName ) ;
//    }
//  }
  return RetVal ;
}

void GraphEditor::OutNode::Set_prof_debug( CORBA::ORB_ptr ORB ,
                                           const char * DebugFileName ) {
  _Graph_prof_debug = 0 ;
  _prof_debug = 0 ;
  if ( DebugFileName ) {
    _fdebug = new ofstream( DebugFileName );
    SetDebug( ORB , &_Graph_prof_debug , _fdebug ) ;
    MESSAGE( endl << "Trace redirected to file " << DebugFileName << endl)
  }
}

bool GraphEditor::OutNode::LoadDataFlow( const GraphBase::SGraph * aDataFlow ) {
  bool RetVal = false ;
  cdebug_in << "GraphEditor::OutNode::LoadDataFlow() " << (*aDataFlow).Info.theName.c_str()
            << " GraphNodesSize " << Graph()->GraphNodesSize() << endl;
  if ( !_Imported ) {
    RetVal = LoadInfo( (*aDataFlow).Info ) ;
    _Imported = true ;
  }
  else if ( Graph()->IsDataStreamNode() || (*aDataFlow).Info.theKind == SUPERV::DataFlowGraph ) {
    RetVal = true ;
  }
  cdebug << "GraphEditor::OutNode::LoadDataFlow() _Imported " << _Imported << " RetVal " << RetVal << endl;

  map< string , int > aMapOfNodes ;
  if ( RetVal ) {
    cdebug << "GraphEditor::OutNode::LoadDataFlow() LoadNodes GraphNodesSize " << Graph()->GraphNodesSize() << endl;
    RetVal = LoadNodes( aMapOfNodes , (*aDataFlow).Nodes ) ;
  }
  if ( RetVal ) {
    cdebug << "GraphEditor::OutNode::LoadDataFlow() LoadLinks GraphNodesSize " << Graph()->GraphNodesSize() << endl;
    RetVal = LoadLinks( aMapOfNodes , (*aDataFlow).Links ) ;
  }
  if ( RetVal ) {
    Valid() ;
    cdebug << "GraphEditor::OutNode::LoadDataFlow() LoadDatas GraphNodesSize " << Graph()->GraphNodesSize() << endl;
    RetVal = LoadDatas( aMapOfNodes , (*aDataFlow).Datas ) ;
  }
  cdebug_out << "GraphEditor::OutNode::LoadDataFlow done GraphNodesSize " << Graph()->GraphNodesSize()
             << " _Valid " << _Valid << " _Executable " << _Executable << " RetVal " << RetVal << endl;
  return RetVal ;
}

bool GraphEditor::OutNode::LoadXml( const char* myFileName , GraphBase::ListOfSGraphs & aListOfDataFlows ) {
  bool RetVal = false ;
//  GraphBase::ListOfSGraphs aListOfDataFlows ;
  if ( myFileName == NULL ) {
    cdebug << "GraphEditor::OutNode::LoadXml() No file" << endl;
    _Imported = true ;
    char * aDataFlowName = Graph()->Name() ;
//  Name( Graph()->Name() ) ;
    Name( GraphInstanceName( Graph()->Name() ).c_str() ) ;
//    MapGraph( Graph() , Graph()->Name() ) ;
    cdebug << "GraphEditor::OutNode::LoadXml() " << aDataFlowName << " --> " << Graph()->Name() << endl;
    RetVal = true ;
  }
  else {
    cdebug_in << "GraphEditor::OutNode::LoadXml() " << myFileName << endl;
    RetVal = Graph()->LoadXml( _Orb , myFileName , aListOfDataFlows ) ;
//    RetVal = LoadDataFlows( &aListOfDataFlows ) ;
    cdebug_out << "GraphEditor::OutNode::LoadXml " << RetVal << " " << aListOfDataFlows.size()
               << " Graphs" << endl;
  }
  return RetVal ;
} 

#if 0
bool GraphEditor::OutNode::LoadXml( const char* myFileName ) {
  bool RetVal = false ;
  GraphBase::ListOfSGraphs aListOfDataFlows ;
  if ( myFileName == NULL ) {
    cdebug << "GraphEditor::OutNode::LoadXml() No file" << endl;
    _Imported = true ;
    RetVal = true ;
  }
  else if ( Graph()->LoadXml( _Orb , myFileName , aListOfDataFlows ) ) {
    cdebug_in << "GraphEditor::OutNode::LoadXml() " << myFileName << endl;
    RetVal = LoadDataFlows( &aListOfDataFlows ) ;
    cdebug_out << "GraphEditor::OutNode::LoadXml " << RetVal << endl;
  }
  return RetVal ;
}
#endif

bool GraphEditor::OutNode::LoadInfo(const GraphBase::SNode &aDataFlowInfo ) {
  bool RetVal = false ;
  cdebug_in << "GraphEditor::OutNode::LoadInfo " << aDataFlowInfo.theName.c_str()
            << endl ;
//  MESSAGE( "GraphEditor::OutNode::LoadInfo" );
//  ComponentName( aDataFlowInfo.theComponentName.c_str() ) ;
//  InterfaceName( aDataFlowInfo.theInterfaceName.c_str() ) ;
  if ( Graph()->IsDataStreamNode() || aDataFlowInfo.theKind == SUPERV::DataFlowGraph ) {
    char * aDataFlowName = Graph()->Name() ;
//    Graph()->Name( aDataFlowInfo.theName.c_str() ) ;
    Graph()->Name( GraphInstanceName( aDataFlowInfo.theName.c_str() ).c_str() ) ;
//    MapGraph( Graph() , Graph()->Name() ) ;
    cdebug << "GraphEditor::OutNode::LoadInfo " << aDataFlowName << " --> " << Graph()->Name()
           << " aDataFlowInfo.Kind " << aDataFlowInfo.theKind << " Kind() " << Graph()->Kind() << endl ;
    if ( Graph()->IsDataStreamNode() ) {
      Graph()->Kind( SUPERV::DataStreamGraph ) ;
      StreamGraph()->SetStreamParams( aDataFlowInfo.theTimeout , aDataFlowInfo.theDataStreamTrace , aDataFlowInfo.theDeltaTime ) ;
    }
    else {
      Graph()->Kind( SUPERV::DataFlowGraph ) ;
    }
    Graph()->SetService( aDataFlowInfo.theService ) ;
    Graph()->FirstCreation( aDataFlowInfo.theFirstCreation ) ;
    Graph()->LastModification( aDataFlowInfo.theLastModification ) ;
    Graph()->EditorRelease( aDataFlowInfo.theEditorRelease.c_str() ) ;
    Graph()->Author( aDataFlowInfo.theAuthor.c_str()  ) ;
//    Graph()->Computer( aDataFlowInfo.theContainer.c_str() ) ;
    Graph()->Comment( aDataFlowInfo.theComment.c_str() ) ;
// Not in OutNode/DataFlow but in InNode/DataFlow_in_an_other_DataFlow
//    Graph()->Coordinates( aDataFlowInfo.theX , aDataFlowInfo.theY ) ;
    RetVal = true ;
  }
  else {
    Graph()->Kind( aDataFlowInfo.theKind ) ;
    cdebug << "GraphEditor::OutNode::LoadInfo aDataFlowInfo.Kind " << aDataFlowInfo.theKind
           << " != IsDataStreamNode() " << Graph()->IsDataStreamNode() << endl ;
  }
  cdebug_out << "GraphEditor::OutNode::LoadInfo " << RetVal << endl ;
  return RetVal ;
}

bool GraphEditor::OutNode::LoadNodes(map< string , int > & aMapOfNodes ,
                                     const GraphBase::ListOfSNodes &aListOfNodes ) {
  GraphEditor::InNode * anInNode ;
  cdebug_in << "GraphEditor::OutNode::LoadNodes " << endl ;
  int i ;
  for ( i = 0 ; i < (int ) aListOfNodes.size() ; i++ ) {
    GraphBase::SNode aNode = aListOfNodes[ i ] ;
    const char * aNodeName = aNode.theName.c_str() ;
//    cout << "GraphEditor::OutNode::LoadNodes " << aNodeName << " "
//         << aNode.theService.ServiceinParameter.length() << " InParameters "
//         << aNode.theService.ServiceoutParameter.length() << " OutParameters "
//         << aNode.theListOfInDataStreams.size() << " InDataStreams "
//         << aNode.theListOfOutDataStreams.size() << " OutDataStreams "
//         << " _prof_debug " << _prof_debug << endl ;
    cdebug << "GraphEditor::OutNode::LoadNodes " << aNodeName << " "
           << aNode.theService.ServiceinParameter.length() << " InParameters "
           << aNode.theService.ServiceoutParameter.length() << " OutParameters "
           << aNode.theListOfInDataStreams.size() << " InDataStreams "
           << aNode.theListOfOutDataStreams.size() << " OutDataStreams "
           << endl ;
    if ( aNode.theListOfFuncName.size() == 0 ) {
      aNode.theListOfFuncName.resize( 1 ) ;
      aNode.theListOfFuncName[ 0 ] = "" ;
      aNode.theListOfPythonFunctions.resize( 1 ) ;
      aNode.theListOfPythonFunctions[ 0 ] = new SUPERV::ListOfStrings() ;
    }
    if ( Graph()->GetGraphNode( aNode.theName.c_str() ) ) {
      aNodeName = NULLSTRING ;
    }

    aNode.theService.ServiceinDataStreamParameter.length( aNode.theListOfInDataStreams.size() ) ;
    aNode.theService.ServiceoutDataStreamParameter.length( aNode.theListOfOutDataStreams.size() ) ;
    unsigned int j ;
    for ( j = 0 ; j < aNode.theListOfInDataStreams.size() ; j++ ) {
      aNode.theService.ServiceinDataStreamParameter[ j ].Parametername = aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametername ,
      aNode.theService.ServiceinDataStreamParameter[ j ].Parametertype = aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametertype ,
      aNode.theService.ServiceinDataStreamParameter[ j ].Parameterdependency = aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parameterdependency ;
    }
    for ( j = 0 ; j < aNode.theListOfOutDataStreams.size() ; j++ ) {
      aNode.theService.ServiceoutDataStreamParameter[ j ].Parametername = aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametername ,
      aNode.theService.ServiceoutDataStreamParameter[ j ].Parametertype = aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametertype ,
      aNode.theService.ServiceoutDataStreamParameter[ j ].Parameterdependency = aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parameterdependency ;
    }

    anInNode = AddNode( aNode.theService ,
                        aNode.theListOfFuncName ,
                        aNode.theListOfPythonFunctions ,
                        aNode.theComponentName.c_str() ,
                        aNode.theInterfaceName.c_str() , aNodeName ,
                        aNode.theKind ,
                        aNode.theFirstCreation , aNode.theLastModification ,
                        aNode.theEditorRelease.c_str() ,
                        aNode.theAuthor.c_str() , aNode.theContainer.c_str() ,
                        aNode.theComment.c_str() ,
                        aNode.theCoords.theX , aNode.theCoords.theY ) ;

    // insert container into < ComponentName, Container > map for corresponding component
    Graph()->InsertToMapOfComponentNameContainer( aNode.theComponentName.c_str(), aNode.theContainer.c_str() ) ; // mkr : PAL13947

    string * aNodetheName = new string( aNode.theName ) ;
    aMapOfNodes[ *aNodetheName ] = Graph()->GetGraphNodeIndex( anInNode->Name() ) ;
    if ( anInNode->IsOneOfInLineNodes() || anInNode->IsMacroNode() ) {
      anInNode->GraphEditor::InNode::InLineNode()->DefPortsOfNode(
                _Orb , aNode.theService , anInNode->NamePtr() ,
                anInNode->Kind() ,
                _prof_debug , _fdebug ) ;
      GraphBase::InLineNode * aINode = anInNode->InLineNode() ;
      GraphBase::LoopNode * aLNode = NULL ;
      if ( aINode->IsLoopNode() ) {
        aLNode = anInNode->LoopNode() ;
        aLNode->SetPythonFunction( aNode.theListOfFuncName[ 0 ].c_str() ,
                                   *aNode.theListOfPythonFunctions[ 0 ] ) ;
        aLNode->SetMorePythonFunction( aNode.theListOfFuncName[ 1 ].c_str() ,
                                       *aNode.theListOfPythonFunctions[ 1 ] ) ;
        aLNode->SetNextPythonFunction( aNode.theListOfFuncName[ 2 ].c_str() ,
                                       *aNode.theListOfPythonFunctions[ 2 ] ) ;
      }
      else if ( aINode->IsMacroNode() || aINode->IsInLineNode() || aINode->IsGOTONode() ||
                aINode->IsSwitchNode() || aINode->IsEndSwitchNode() ) {
        aINode->SetPythonFunction( aNode.theListOfFuncName[ 0 ].c_str() ,
                                   *aNode.theListOfPythonFunctions[ 0 ] ) ;
      }
    }

    for ( j = 0 ; j < aNode.theListOfInDataStreams.size() ; j++ ) {
      GraphBase::InPort * anInPort ;
      if ( anInNode->IsOneOfInLineNodes() ) {
        anInPort = anInNode->ComputingNode()->AddInDataStreamPort( aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametername ,
                                                                   aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametertype ,
                                                                   aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parameterdependency ,
                                                                   SUPERV::DataStreamParameter ) ;
      }
      else {
        anInPort = anInNode->ComputingNode()->GetChangeInPort( aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametername ) ;
      }
      ((GraphBase::InDataStreamPort * ) anInPort)->SetParams( aNode.theListOfInDataStreams[ j ].theKindOfSchema ,
                                                              aNode.theListOfInDataStreams[ j ].theKindOfInterpolation ,
                                                              aNode.theListOfInDataStreams[ j ].theKindOfExtrapolation ) ;
    }
    for ( j = 0 ; j < aNode.theListOfOutDataStreams.size() ; j++ ) {
      GraphBase::OutPort * anOutPort ;
      if ( anInNode->IsOneOfInLineNodes() ) {
        anOutPort = anInNode->ComputingNode()->AddOutDataStreamPort( aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametername ,
                                                                     aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametertype ,
                                                                     aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parameterdependency ,
                                                                     SUPERV::DataStreamParameter ) ;
      }
      else {
        anOutPort = anInNode->ComputingNode()->GetChangeOutPort( aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametername ) ;
      }
      ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues( aNode.theListOfOutDataStreams[ j ].theNumberOfValues ) ;
    }
    delete aNodetheName ;
    if ( !anInNode ) {
      return false ;
    }
  }

  // setting coupled pairs of nodes: Loop-EndLoop, Switch-EndSwitch, InLine-GOTO, MacroNode-Graph
  for ( i = 0 ; i < (int ) aListOfNodes.size() ; i++ ) {
    GraphBase::SNode aNode = aListOfNodes[ i ] ;
    cdebug << "GraphEditor::OutNode::LoadNodes " << aNode.theName.c_str() << " Coupled to "
           << aNode.theCoupledNode.c_str() << endl ;
    anInNode = (GraphEditor::InNode * ) Graph()->GetChangeGraphNode( aMapOfNodes[ aNode.theName.c_str() ] )->GetInNode() ;

    if ( anInNode->IsOneOfGOTONodes() && strlen( aNode.theCoupledNode.c_str() ) ) {
      GraphBase::GOTONode * aGOTONode;
      aGOTONode = (GraphBase::GOTONode * ) anInNode->ComputingNode() ;

      // asv : 25.10.04 : if aNode is a MacroNode, then its coupled node (another Graph) is NOT in aMapOfNodes 
      //                  and we must couple MacroNode only with name to its subgraph
      if ( aGOTONode->IsMacroNode() ) {
        cdebug << "GraphEditor::OutNode::LoadNodes MacroNode " << aNode.theName.c_str()
	       << " is Coupled ONLY WITH NAME to its subgraph " << aNode.theCoupledNode.c_str() << endl;
        aGOTONode->CoupledNodeName( aNode.theCoupledNode.c_str() ) ;
      }
      else { // coupling Loop-EndLoop, Switch-EndSwitch, InLine-GOTO
	// asv : fix for 6822 : using map because if aNode's name is the same as some existing node's name
	// aMap will give the correct index any way (aMap has already a different name for aNode, SNode still has old name)
	int aCoupledNodeIndex = aMapOfNodes[ aNode.theCoupledNode.c_str() ] ;
	cdebug << "GraphEditor::OutNode::LoadNodes " << aNode.theCoupledNode.c_str()
	       << " index " << aCoupledNodeIndex << endl ;
        GraphBase::GOTONode * aCoupledNode ;
        aCoupledNode = (GraphBase::GOTONode * ) Graph()->GetChangeGraphNode( aCoupledNodeIndex ) ;
        cdebug << "GraphEditor::OutNode::LoadNodes " << aNode.theName.c_str()
               << " is now Coupled to " << aNode.theCoupledNode.c_str() << endl ;
        aGOTONode->CoupledNode( aCoupledNode ) ;
      }
    }
  }
  cdebug_out << "GraphEditor::OutNode::LoadNodes" << endl ;
  return true ;
}

bool GraphEditor::OutNode::LoadLinks(map< string , int > & aMapOfNodes ,
                                     const GraphBase::ListOfSLinks &aListOfLinks ) {
  bool RetVal = true ;
  bool RetAddLink ;
  cdebug_in << "GraphEditor::OutNode::LoadLinks" << endl ;
//  MESSAGE( "GraphEditor::OutNode::LoadLinks" );
  int i , j ;
  for ( i = 0 ; i < (int ) aListOfLinks.size() ; i++ ) {
    GraphBase::SLink aLink = aListOfLinks[ i ] ;
    string * aLinkFromNodeName = new string( aLink.FromNodeName.c_str() ) ;
    string * aLinkToNodeName = new string( aLink.ToNodeName.c_str() ) ;
    cdebug << "LoadLinks " << aLinkFromNodeName->c_str() << "( "
           << aLink.FromServiceParameterName.c_str() << " ) --> "
           << aLinkToNodeName->c_str() << "( "
           << aLink.ToServiceParameterName.c_str() << " )" << endl ;
    if ( Graph()->GetGraphNode( aMapOfNodes[ aLinkFromNodeName->c_str() ] ) &&
         Graph()->GetGraphNode( aMapOfNodes[ aLinkToNodeName->c_str() ] ) ) {
//JR 08.02.2005 : Rule of CEA : a bad graph may be stored in a xml
      RetAddLink = AddLink( Graph()->GetGraphNode( aMapOfNodes[ aLinkFromNodeName->c_str() ] )->Name() ,
                            aLink.FromServiceParameterName.c_str() ,
                            Graph()->GetGraphNode( aMapOfNodes[ aLinkToNodeName->c_str() ] )->Name() ,
                            aLink.ToServiceParameterName.c_str() ) ;
    }
    else {
      RetVal = false ;
    }
    if ( RetVal && RetAddLink ) {
      for ( j = 0 ; j < (int ) aLink.aListOfCoords.size() ; j++ ) {
        RetVal = AddLinkCoord( Graph()->GetGraphNode( aMapOfNodes[ aLinkFromNodeName->c_str() ] )->Name() ,
                                        aLink.FromServiceParameterName.c_str() ,
                                        Graph()->GetGraphNode( aMapOfNodes[ aLink.ToNodeName.c_str() ] )->Name() ,
                                        aLink.ToServiceParameterName.c_str() ,
                                       j + 1 ,
                                       aLink.aListOfCoords[j].theX ,
                                       aLink.aListOfCoords[j].theY ) ;
        if ( !RetVal )
          break ;
      }
    }
    delete aLinkFromNodeName ;
    delete aLinkToNodeName ;
  }
  cdebug_out << "GraphEditor::OutNode::LoadLinks " << RetVal << endl ;
  return RetVal ;
}

bool GraphEditor::OutNode::LoadDatas(map< string , int > & aMapOfNodes ,
                                     const GraphBase::ListOfSLinks &aListOfDatas ) {
  bool RetVal = true ;
  bool RetAddLink ;
  cdebug_in << "GraphEditor::OutNode::LoadDatas" << endl ;
//  MESSAGE( "GraphEditor::OutNode::LoadDatas" );
  int i ;
  for ( i = 0 ; i < (int ) aListOfDatas.size() ; i++ ) {
    GraphBase::SLink aLink = aListOfDatas[ i ] ;
    cdebug << "OutNode::LoadDatas " << i << aLink.FromNodeName.c_str() << "(" << aLink.FromServiceParameterName
           << ") --> " << aLink.ToNodeName.c_str() << "(" << aLink.ToServiceParameterName << ") CORBA::tk_xxx "
           << aLink.aLinkValue.type()->kind() << endl ;
    string * aLinkFromNodeName = new string( aLink.FromNodeName.c_str() ) ;
    string * aLinkToNodeName = new string( aLink.ToNodeName.c_str() ) ;
//      cout << "LoadDatas " << aLink.FromNodeName.c_str() << " "
//           << aMapOfNodes[ aLinkFromNodeName->c_str() ] << endl ;
//      cout << "          " << aLink.ToNodeName.c_str() << " "
//           << aMapOfNodes[ aLinkToNodeName->c_str() ] << endl ;
    RetAddLink = Graph()->AddInputData( Graph()->GetGraphNode( aMapOfNodes[ aLinkToNodeName->c_str() ] )->Name() ,
                                        aLink.ToServiceParameterName.c_str() ,
                                        aLink.aLinkValue ) ;
    delete aLinkFromNodeName ;
    delete aLinkToNodeName ;
    if ( !RetVal )
      break ;
  }
  cdebug_out << "GraphEditor::OutNode::LoadDatas " << RetVal << endl ;
  return RetVal ;
}

bool GraphEditor::OutNode::SaveXml(const char* filename) {
  bool test;
  cdebug_in << "GraphEditor::OutNode::SaveXml(" << filename << ")" << endl;
  ofstream f(filename);
  IsValid() ;
  QDomDocument DomGraph ;
  QDomElement Domsupergraph ;
  cdebug << "OutNode::SaveXML("<< filename << ") ---> OutNode::SaveXML( ostream & f , QDomDocument & , true "
         << " , QDomElement & ) " << Graph()->Name() << endl ;
  test = SaveXML( f , DomGraph , true , Domsupergraph );
  QString xml = DomGraph.toString() ;
  f << xml << endl ;

  cdebug << "OutNode::SaveXML("<< filename << ") done" << endl ;
//  if ( test ) {
//    QString xml = Graph.toString() ;
//    cout << "GraphEditor::OutNode::SaveXML " << xml << endl ;
//    f << xml << endl ;
//  }
  cdebug_out << "GraphEditor::OutNode::SaveXml " << test << endl;
  return test;
}


bool GraphEditor::OutNode::SavePy( const char* filename ) {
  bool test;
  cdebug_in << "GraphEditor::OutNode::SavePy(" << filename << ")" << endl;
  ofstream f( filename ) ;
  IsValid() ;
  test = SavePY( f , true );
  f << endl << Graph()->Name() << " = Def" << Graph()->Name() << "()" << endl ;
  cdebug_out << "GraphEditor::OutNode::SavePy " << test << endl;
  return test;
}

GraphBase::ListOfSGraphs * GraphEditor::OutNode::GetDataFlows( GraphBase::ListOfSGraphs * aListOfDataFlows ) {
//  GraphBase::ListOfSGraphs * aListOfDataFlows = new GraphBase::ListOfSGraphs;
  int index = aListOfDataFlows->size() ;
  aListOfDataFlows->resize( index + 1 ) ;
  if ( Graph()->IsDataFlowNode() ) {
    (*aListOfDataFlows)[ index ].Info = *Graph()->GetInfo() ;
    (*aListOfDataFlows)[ index ].Nodes = *Graph()->GetNodes() ;
    (*aListOfDataFlows)[ index ].Links = *Graph()->GetLinks( true ) ;
    (*aListOfDataFlows)[ index ].Datas = *Graph()->GetDatas() ;
  }
  else {
    (*aListOfDataFlows)[ index ].Info = *StreamGraph()->GetInfo() ;
    (*aListOfDataFlows)[ index ].Nodes = *StreamGraph()->GetNodes() ;
    (*aListOfDataFlows)[ index ].Links = *StreamGraph()->GetLinks( true ) ;
    (*aListOfDataFlows)[ index ].Datas = *StreamGraph()->GetDatas() ;
  }
  int i ;
  for ( i = 0 ; i < (int ) (*aListOfDataFlows)[ index ].Nodes.size() ; i++ ) {
    const GraphBase::ComputingNode * aNode = Graph()->GetGraphNode( (*aListOfDataFlows)[ index ].Nodes[i].theName.c_str() ) ;
    if ( aNode->IsMacroNode() ) {
//      string aCoupledNodeName = (*aListOfDataFlows)[ index ].Nodes[i].theCoupledNode ;
      GraphBase::Graph * aGraph = (GraphBase::Graph * ) ((GraphBase::GOTONode * ) aNode )->CoupledNode() ;
//      GraphBase::Graph * aGraph = MapGraph( aCoupledNodeName.c_str() ) ;
      aGraph->GraphEditor()->GraphEditor::OutNode::GetDataFlows( aListOfDataFlows ) ;
    }
  }
  return aListOfDataFlows ;
}

void GraphEditor::OutNode::DateModification() {
  time_t T = time(NULL);
  struct tm * Tm = localtime(&T);
  SUPERV::SDate aLastModificationDate ;

  aLastModificationDate.Second = Tm->tm_sec;
  aLastModificationDate.Minute = Tm->tm_min;
  aLastModificationDate.Hour   = Tm->tm_hour;
  aLastModificationDate.Day    = Tm->tm_mday;
  aLastModificationDate.Month  = Tm->tm_mon + 1;
  aLastModificationDate.Year   = Tm->tm_year + 1900;
  Graph()->LastModification( aLastModificationDate ) ;
}

void GraphEditor::OutNode::Coordinates( const char* NodeName ,
                                        const int X ,
                                        const int Y ) {
  ((GraphEditor::InNode * ) Graph()->GetChangeGraphNode( NodeName ))->Coordinates( X , Y ) ;
}

const int GraphEditor::OutNode::XCoordinate( const char* NodeName ) {
  return ((GraphEditor::InNode * ) Graph()->GetChangeGraphNode( NodeName ))->XCoordinate() ;
}

const int GraphEditor::OutNode::YCoordinate( const char* NodeName ) {
  return ((GraphEditor::InNode * ) Graph()->GetChangeGraphNode( NodeName ))->YCoordinate() ;
}

GraphEditor::InNode * GraphEditor::OutNode::AddNode(
                      const SALOME_ModuleCatalog::Service& NodeService ,
                      GraphBase::ListOfFuncName aFuncName ,
                      GraphBase::ListOfPythonFunctions aPythonFunction ,
                      const char * NodeComponentName ,
                      const char * NodeInterfaceName ,
                      const char * theNodeName ,
                      const SUPERV::KindOfNode NodeKindOfNode ,
                      const SUPERV::SDate NodeFirstCreation ,
                      const SUPERV::SDate NodeLastModification  ,
                      const char * NodeEditorRelease ,
                      const char * NodeAuthor ,
                      const char * NodeComputer ,
                      const char * NodeComment ,
                      const int NodeX ,
                      const int NodeY ) {
  cdebug_in << "GraphEditor::OutNode::AddNode( " ;
  if ( NodeComponentName != NULLSTRING && strlen( NodeComponentName ) ) {
    cdebug << "Component('" << NodeComponentName << "') , Node('" ;
  }
  else {
    cdebug << "NodeComponentName[NULL] )" << endl;
  }
  if ( theNodeName == NULL ) {
    theNodeName = NULLSTRING ;
  }
  if ( theNodeName != NULLSTRING && strlen( theNodeName ) ) {
    cdebug << theNodeName << "' )" ;
  }
  else {
    cdebug << "NodeName[NULLSTRING]' )" ;
  }
  cdebug << " " << NodeKindOfNode << endl ;
  char * RetVal = NULLSTRING ;
  GraphEditor::InNode *Nd = NULL ;
  char * aNodeName = NULLSTRING ;
  bool   GeneratedName = false ;
//PAL9048 JR Debug : a node may not have the same name as the graph
  bool GraphNodeSameName = false ;
  if ( theNodeName && !strcmp( Graph()->Name() , theNodeName ) ) {
    GraphNodeSameName = true ;
  }
  if ( NodeKindOfNode == SUPERV::InLineNode ||
       NodeKindOfNode == SUPERV::LoopNode ||
       NodeKindOfNode == SUPERV::EndLoopNode ||
       NodeKindOfNode == SUPERV::SwitchNode ||
       NodeKindOfNode == SUPERV::EndSwitchNode ||
       NodeKindOfNode == SUPERV::GOTONode ) {
    if ( theNodeName == NULLSTRING || strlen( theNodeName ) == 0 ) {
      if ( NodeKindOfNode == SUPERV::InLineNode ) {
        ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = my_strdup( "InLine" ) ;
      }
      else if ( NodeKindOfNode == SUPERV::LoopNode ) {
        ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = my_strdup( "Loop" ) ;
      }
      else if ( NodeKindOfNode == SUPERV::EndLoopNode ) {
        ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = my_strdup( "EndLoop" ) ;
      }
      else if ( NodeKindOfNode == SUPERV::SwitchNode ) {
        ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = my_strdup( "Switch" ) ;
      }
      else if ( NodeKindOfNode == SUPERV::EndSwitchNode ) {
        ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = my_strdup( "EndSwitch" ) ;
      }
      else if ( NodeKindOfNode == SUPERV::GOTONode ) {
        ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = my_strdup( "GOTO" ) ;
      }
    }
    else {
      ((SALOME_ModuleCatalog::Service& ) NodeService).ServiceName = CORBA::string_dup( theNodeName ) ;
    }
    theNodeName = NULLSTRING ;
  }
//PAL9048 JR Debug : a node may not have the same name as the graph
//  if ( theNodeName == NULLSTRING || strlen( theNodeName ) == 0 || Graph()->GetGraphNode( theNodeName ) ) {
  if ( theNodeName == NULLSTRING || strlen( theNodeName ) == 0 ||
       Graph()->GetGraphNode( theNodeName ) || GraphNodeSameName ) {
    cdebug << "OutNode::AddNode : '" << theNodeName << "' GraphNodeSameName "
           << GraphNodeSameName << endl;
    aNodeName = new char[ strlen( NodeService.ServiceName )+1 ] ;
    strcpy( aNodeName , NodeService.ServiceName ) ;
//    if ( Graph()->GetGraphNode( NodeService.ServiceName ) ) {
    if ( Graph()->GetGraphNode( NodeService.ServiceName ) || GraphNodeSameName ) {
      GeneratedName = true ;
      while ( Graph()->GetGraphNode( aNodeName ) || GraphNodeSameName ) {
        cdebug << "OutNode::AddNode : '" << aNodeName << "' exists or GraphNodeSameName "
               << GraphNodeSameName << endl;
        if ( aNodeName ) {
          delete [] aNodeName ;
	}
//JR 09.08.2005 Debug : folowing line does not run with OMNIORB4
//        char * aServiceName = (CORBA::String_member ) NodeService.ServiceName ;
        int num = Graph()->GetNewServiceInstanceNumber( (CORBA::String_member ) NodeService.ServiceName ) ;
        ostringstream astr ;
        astr << num << ends ;
//        const char * n_instance = astr.str().c_str() ;
        int lname = strlen( NodeService.ServiceName ) + 1 +
                    strlen( astr.str().c_str() ) + 1 ;
        aNodeName = new char[lname] ;
        strcpy( aNodeName , NodeService.ServiceName ) ;
        strcat( aNodeName , "_" ) ;
        strcat( aNodeName , astr.str().c_str() ) ;
        GraphNodeSameName = !strcmp( Graph()->Name() , aNodeName ) ;
      }
    }
  }
  else {
    if ( Graph()->GetGraphNode( theNodeName ) == NULL ) {
      aNodeName = new char[ strlen( theNodeName )+1 ] ;
      strcpy( aNodeName , theNodeName ) ;
    }
    else {
      aNodeName = NULLSTRING ;
    }
  }
  if ( aNodeName != NULLSTRING ) {
    Nd = new GraphEditor::InNode( _Orb , Graph()->NamingService() ,
                                  aFuncName , aPythonFunction , NodeService ,
                                  NodeComponentName , NodeInterfaceName ,
                                  aNodeName , NodeKindOfNode ,
                                  NodeFirstCreation , NodeLastModification ,
                                  NodeEditorRelease , NodeAuthor ,
                                  NodeComputer , NodeComment , GeneratedName ,
                                  NodeX , NodeY ,
                                  _prof_debug , _fdebug ) ;
    
    // asv: 28.09.04 fix for 6621
    //if ( Nd->IsMacroNode() )
    //  MapGraph( Nd->GraphMacroNode(), aNodeName );

    if ( Graph()->IsDataStreamNode() && ( Nd->IsComputingNode() || Nd->IsFactoryNode() ) ) {
      unsigned int i ;
      for ( i = 0 ; i < NodeService.ServiceinDataStreamParameter.length() ; i++ ) {
        GraphBase::InDataStreamPort * aDataStreamPort ;
        aDataStreamPort = Nd->ComputingNode()->AddInDataStreamPort(
//JR 17.02.2005 Memory Leak                                         my_strdup( NodeService.ServiceinDataStreamParameter[i].Parametername ) ,
                                         NodeService.ServiceinDataStreamParameter[i].Parametername ,
                                         NodeService.ServiceinDataStreamParameter[i].Parametertype ,
                                         NodeService.ServiceinDataStreamParameter[i].Parameterdependency ,
					 SUPERV::DataStreamParameter ) ;
      }
      for ( i = 0 ; i < NodeService.ServiceoutDataStreamParameter.length() ; i++ ) {
        GraphBase::OutDataStreamPort * aDataStreamPort ;
        aDataStreamPort = Nd->ComputingNode()->AddOutDataStreamPort(
//JR 17.02.2005 Memory Leak                                         my_strdup( NodeService.ServiceoutDataStreamParameter[i].Parametername ) ,
                                         NodeService.ServiceoutDataStreamParameter[i].Parametername ,
                                         NodeService.ServiceoutDataStreamParameter[i].Parametertype ,
                                         NodeService.ServiceoutDataStreamParameter[i].Parameterdependency ,
					 SUPERV::DataStreamParameter ) ;
      }
    }

    if ( Graph()->AddNode( Nd->ComputingNode() ) ) {
      DateModification() ;
      RetVal = Nd->Name() ;
    }
    else {
      cdebug << "NodeName already exists." << endl ;
    }
  }
  else {
    cdebug << "ERROR NodeName is NULL or already exists." << endl ;
  }
//  delete [] aNodeName ;
  _Valid = false ;
  if ( Nd == NULL ) {
    cdebug_out << "GraphEditor::OutNode::AddNode : NULL" << endl;
  }
  else {
    cdebug_out << "GraphEditor::OutNode::AddNode : " << Nd << " " << Nd->Name() << endl;
  }
  return Nd ;
}

bool GraphEditor::OutNode::AddLinkCoord( const char* FromNodeName ,
                                         const char* FromServiceParameterName ,
                                         const char* ToNodeName ,
                                         const char* ToServiceParameterName ,
                                         const int nXY ,
                                         const int* X ,
                                         const int* Y ) {
  GraphBase::InPort * anInPort = Graph()->GetChangeInPort( ToNodeName ,
                                                           ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::AddLinkCoord " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::AddLinkCoord " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->AddCoord( nXY , X , Y ) ;
    }
    else {
      return anInPort->AddCoord( nXY , X , Y ) ;
    }
  }
  return false ;
}

bool GraphEditor::OutNode::AddLinkCoord( const char* FromNodeName ,
                                         const char* FromServiceParameterName ,
                                         const char* ToNodeName ,
                                         const char* ToServiceParameterName ,
                                         const int index ,
                                         const int X ,
                                         const int Y ) {
  GraphBase::InPort * anInPort = Graph()->GetChangeInPort( ToNodeName ,
                                                           ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::AddLinkCoord " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::AddLinkCoord " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->AddCoord( index , X , Y ) ;
    }
    else {
      return anInPort->AddCoord( index , X , Y ) ;
    }
  }
  return false ;
}

bool GraphEditor::OutNode::ChangeLinkCoord( const char* FromNodeName ,
                                            const char* FromServiceParameterName ,
                                            const char* ToNodeName ,
                                            const char* ToServiceParameterName ,
                                            const int index ,
                                            const int X ,
                                            const int Y ) {
  GraphBase::InPort * anInPort = Graph()->GetChangeInPort( ToNodeName ,
                                                           ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::ChangeLinkCoord " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::ChangeLinkCoord " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->ChangeCoord( index , X , Y ) ;
    }
    else {
      return anInPort->ChangeCoord( index , X , Y ) ;
    }
  }
  return false ;
}

bool GraphEditor::OutNode::RemoveLinkCoord( const char* FromNodeName ,
                                            const char* FromServiceParameterName ,
                                            const char* ToNodeName ,
                                            const char* ToServiceParameterName ,
                                            const int index ) {
  GraphBase::InPort * anInPort = Graph()->GetChangeInPort( ToNodeName ,
                                                           ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::RemoveLinkCoord " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::RemoveLinkCoord " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->RemoveCoord( index ) ;
    }
    else {
      return anInPort->RemoveCoord( index ) ;
    }
  }
  return false ;
}

int GraphEditor::OutNode::GetLinkCoordSize( const char* FromNodeName ,
                                            const char* FromServiceParameterName ,
                                            const char* ToNodeName ,
                                            const char* ToServiceParameterName ) {
  const GraphBase::InPort * anInPort = Graph()->GetInPort( ToNodeName , ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::GetLinkCoordSize " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::GetLinkCoordSize " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->GetCoord() ;
    }
    else {
      return anInPort->GetCoord() ;
    }
  }
  return 0 ;
}

bool GraphEditor::OutNode::GetLinkCoord( const char* FromNodeName ,
                                         const char* FromServiceParameterName ,
                                         const char* ToNodeName ,
                                         const char* ToServiceParameterName ,
                                         int *X , int *Y ) {
  const GraphBase::InPort * anInPort = Graph()->GetInPort( ToNodeName , ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::GetLinkCoord " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::GetLinkCoord " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->GetCoord( X , Y ) ;
    }
    else {
      return anInPort->GetCoord( X , Y ) ;
    }
  }
  return false ;
}

bool GraphEditor::OutNode::GetLinkCoord( const char* FromNodeName ,
                                         const char* FromServiceParameterName ,
                                         const char* ToNodeName ,
                                         const char* ToServiceParameterName ,
                                         const int index , CORBA::Long &X , CORBA::Long &Y ) {
  GraphBase::InPort * anInPort = Graph()->GetChangeInPort( ToNodeName ,
                                                           ToServiceParameterName ) ;
//  cdebug << "GraphEditor::OutNode::GetLinkCoord " << ToNodeName << "( " << ToServiceParameterName
//         << " ) " << anInPort << " IsEndSwitch " << anInPort->IsEndSwitch() << endl ;
  if ( anInPort ) {
    if ( anInPort->IsEndSwitch() ) {
//      cdebug << "GraphEditor::OutNode::GetLinkCoord " << FromNodeName << "( " << FromServiceParameterName
//             << " )" << endl ;
      return Graph()->GetChangeOutPort( FromNodeName , FromServiceParameterName )->GetCoord( index , X , Y ) ;
    }
    else {
      return anInPort->GetCoord( index , X , Y ) ;
    }
  }
  return false ;
}

bool GraphEditor::OutNode::UnValid() {
  bool RetVal = _Valid ;
  _Valid = false ;
  if ( Graph()->GraphMacroLevel() != 0 ) {
    cdebug << "GraphEditor::OutNode::UnValid() GraphMacroLevel " << Graph()->GraphMacroLevel() << endl ;
    RetVal = Valid() ;
  }
  return RetVal ;
}

//JR Optional parameter kLoopSwitch (default = true) :
//In some cases we do not need to check the validity of loops and switchs
//JR 07.07.2005 PAL9342 : that code is now in Executable() method instead of Valid() method
//bool GraphEditor::OutNode::Valid(bool kLoopSwitch ) {
bool GraphEditor::OutNode::Valid() {
  bool RetVal = true ;
  cdebug_in << "GraphEditor::OutNode::Valid" << endl;
//  if ( _Valid )
//    return true ;

  _Executable = false ;

  if ( !Graph()->CreateService() ) {
    cdebug << "GraphEditor::OutNode::Valid ERROR _Valid " << _Valid << endl;
    RetVal = false ;
  }
  
  if ( Graph()->GraphMacroLevel() != 0 ) {
    cdebug << "CoupledNode " << Graph()->CoupledNode() << endl ;
    cdebug << "GraphEditor " << Graph()->CoupledNode()->GraphEditor() << endl ;
    cdebug << "Graph " << Graph()->CoupledNode()->GraphEditor()->Graph() << endl ;
    cdebug << "Name " << Graph()->CoupledNode()->GraphEditor()->Graph()->Name() << endl ;
    cdebug << "Valid --> UpdateMacroPorts of " << Graph()->CoupledNodeName() << " of "
           << Graph()->CoupledNode()->GraphEditor()->Graph()->Name() << endl ;
    cdebug << Graph()->CoupledNode() << endl ;
    Graph()->CoupledNode()->UpdateMacroPorts( Graph() ) ;
    cdebug << Graph()->CoupledNode()->Name() << " Valid --> UnValid of graph "
           << Graph()->CoupledNode()->GraphEditor()->Graph()->Name()
           << " GraphMacroLevel " << Graph()->CoupledNode()->GraphEditor()->Graph()->GraphMacroLevel()  << endl ;
    Graph()->CoupledNode()->GraphEditor()->UnValid() ;
  }

  Graph()->InLineServices() ;

//JR 07.07.2005 PAL9342 : that code is now in Executable() method instead of Valid() method
#if 0
  int SubStreamGraphsNumber = 0 ;
  if ( !Graph()->Sort( SubStreamGraphsNumber ) ) {
    cdebug_out << "This DataFlow is not valid." << endl ;
    RetVal = false ;
  }
  if ( Graph()->IsDataStreamNode() ) {
    StreamGraph()->SubStreamGraphsNumber( SubStreamGraphsNumber ) ;
  }

//JR Debug 24.08.2005 : InLineServices is needed for Export ==> it is executed above
//  Graph()->InLineServices() ;

  if ( kLoopSwitch ) {
    if ( !Graph()->ValidLoops() ) {
      cdebug_out << "This DataFlow have not valid Loops." << endl ;
      RetVal = false ;
    }
    if ( !Graph()->ValidSwitchs() ) {
      cdebug_out << "This DataFlow have not valid Switchs." << endl ;
      RetVal = false ;
    }
  }
  
  Graph()->ComputingNodes() ;
#endif

  if ( !Graph()->ValidGOTO() ) { // mkr : PAL12575
    cdebug << "Editor::OutNode::Valid This DataFlow have not valid GOTO(ValidGOTO)." << endl ;
    RetVal = false ;
  }

  if ( RetVal ) {
    _Valid = true ;
  }

  cdebug_out << "GraphEditor::OutNode::Valid " << _Valid << " RetVal " << RetVal << endl;
  return RetVal ;
}

bool GraphEditor::OutNode::Executable() {
  cdebug_in << "GraphEditor::OutNode::Executable" << endl;
  bool RetVal = true ;
  bool NewLink ;
// LinkLoopNodes manage input values of LoopNodes and EndLoopNodes
  if ( Graph()->LinkLoopNodes( NewLink ) ) {
    if ( NewLink ) {
      _Valid = false ;
      RetVal = false ;
    }
  }
  else {
    cdebug << "Editor::OutNode::Executable This DataFlow is not executable(LinkLoopNodes)." << endl ;
    _Executable = false ;
    RetVal = false ;
  }
  if ( !IsValid() ) {
    Valid() ;
  }
  if ( !IsValid() ) {
    RetVal = false ;
  }

//JR 07.07.2005 PAL9342 : that code is now in Executable() method instead of Valid() method
  bool IsValid = true;
  int SubStreamGraphsNumber = 0 ;
  if ( !Graph()->Sort( SubStreamGraphsNumber ) ) {
    cdebug << "Editor::OutNode::Executable This DataFlow is not valid(Sort)." << endl ;
    RetVal = false ;
    //mkr : 28.09.2005 : if dataflow is not valid => it is not executable
    IsValid = false ;
  }
  if ( Graph()->IsDataStreamNode() )
    StreamGraph()->SubStreamGraphsNumber( SubStreamGraphsNumber ) ;

  Graph()->InLineServices() ;
  
//  if ( kLoopSwitch ) {
    if ( !Graph()->ValidLoops() ) {
      cdebug << "Editor::OutNode::Executable This DataFlow have not valid Loops(ValidLoops)." << endl ;
      RetVal = false ;
      //mkr : 28.09.2005 : if dataflow is not valid => it is not executable
      IsValid = false ;
    }
    if ( !Graph()->ValidSwitchs() ) {
      cdebug << "Editor::OutNode::Executable This DataFlow have not valid Switchs(ValidSwitchs)." << endl ;
      RetVal = false ;
      //mkr : 28.09.2005 : if dataflow is not valid => it is not executable
      IsValid = false ;
    }
//  }
  
  Graph()->ComputingNodes() ;

  if ( Graph()->DataServerNodes() )
    _Executable = true ;
  else {
    cdebug << "Editor::OutNode::Executable This DataFlow is not executable(DataServerNodes)." << endl ;
    _Executable = false ;
    RetVal = false ;
  }

  if ( _Executable && Graph()->IsDataStreamNode() ) {
    StreamGraph()->CreateStreamTopology( "/tmp/" ) ;
  }

  // asv : 13.12.04 : introducing check for compatibility of linked ports' types.
  if ( !IsLinksCompatible() ) {
    _Executable = false;
    RetVal = false ;
  }    

  //mkr : 28.09.2005 : if dataflow is not valid => it is not executable
  if ( !IsValid ) _Executable = false ;

  cdebug_out << "GraphEditor::OutNode::Executable _Executable " << _Executable << " RetVal " << RetVal
             << endl;
  return RetVal ;
}

//JR 30.03.2005const CORBA::Any *GraphEditor::OutNode::GetInData(
const CORBA::Any GraphEditor::OutNode::GetInData(
                              const char * ToNodeName ,
                              const char * ToParameterName ) {
//  cdebug_in << "GraphEditor::OutNode::GetInData " << ToNodeName
//            << " " << ToParameterName << endl ;
//JR 30.03.2005  const CORBA::Any * retdata = Graph()->PortInData( ToNodeName , ToParameterName ) ;
  const CORBA::Any retdata = Graph()->PortInData( ToNodeName , ToParameterName ) ;
//  cdebug_out << "GraphEditor::OutNode::GetInData" << endl ;
  return retdata ;
}

//JR 30.03.2005const CORBA::Any *GraphEditor::OutNode::GetOutData(
const CORBA::Any GraphEditor::OutNode::GetOutData(
                              const char * FromNodeName ,
                              const char * FromParameterName ) {
//  cdebug_in << "GraphEditor::OutNode::GetOutData " << FromNodeName
//            << " " << FromParameterName << endl ;
//JR 30.03.2005  const CORBA::Any * retdata = Graph()->PortOutData( FromNodeName , FromParameterName ) ;
  const CORBA::Any retdata = Graph()->PortOutData( FromNodeName , FromParameterName ) ;
//  cdebug_out << "GraphEditor::OutNode::GetOutData" << endl ;
  return retdata ;
}

//bool GraphEditor::OutNode::LinkSaveXML( ostream &f , char *Tabs ,
bool GraphEditor::OutNode::LinkSaveXML( QDomDocument & Graph , QDomElement & link ,
                                        GraphBase::SLink aLink ,
                                        bool wdata ) const {
  cdebug_in << "GraphEditor::OutNode::LinkSaveXML " << aLink.FromNodeName
            << "(" << aLink.FromServiceParameterName << ") --> "
            << aLink.ToNodeName << "(" << aLink.ToServiceParameterName << ")" << endl ;
  QDomElement fromnodename = Graph.createElement( "fromnode-name" ) ;
  QDomText aField ;
  if ( strlen( aLink.FromNodeName.c_str() ) ) {
//    f << Tabs << "<fromnode-name>" << aLink.FromNodeName.c_str()
//      << "</fromnode-name>" << endl ;
    aField = Graph.createTextNode( aLink.FromNodeName.c_str() ) ;
  }
  else {
//    f << Tabs << "<fromnode-name>?</fromnode-name>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  link.appendChild( fromnodename ) ;
  fromnodename.appendChild( aField ) ;

//  f << Tabs << "<fromserviceparameter-name>"
//    << aLink.FromServiceParameterName.c_str() << "</fromserviceparameter-name>"
//    << endl ;
  QDomElement fromserviceparametername = Graph.createElement( "fromserviceparameter-name" ) ;
  aField = Graph.createTextNode( aLink.FromServiceParameterName.c_str() ) ;
  link.appendChild( fromserviceparametername ) ;
  fromserviceparametername.appendChild( aField ) ;

  QDomElement tonodename = Graph.createElement( "tonode-name" ) ;
  if ( strlen( aLink.ToNodeName.c_str() ) ) {
//    f << Tabs << "<tonode-name>" << aLink.ToNodeName.c_str()
//      << "</tonode-name>" << endl ;
    aField = Graph.createTextNode( aLink.ToNodeName.c_str() ) ;
  }
  else {
//    f << Tabs << "<tonode-name>?</tonode-name>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  link.appendChild( tonodename ) ;
  tonodename.appendChild( aField ) ;

//  f << Tabs << "<toserviceparameter-name>"
//    << aLink.ToServiceParameterName.c_str() << "</toserviceparameter-name>"
//    << endl ;
  QDomElement toserviceparametername = Graph.createElement( "toserviceparameter-name" ) ;
  aField = Graph.createTextNode( aLink.ToServiceParameterName.c_str() ) ;
  link.appendChild( toserviceparametername ) ;
  toserviceparametername.appendChild( aField ) ;

  if ( wdata ) {
//    f << Tabs << "<data-value>" << endl ;
    QDomElement datavalue = Graph.createElement( "data-value" ) ;
    link.appendChild( datavalue ) ;
//    f << Tabs << "	<value-type>" << aLink.aLinkValue.type()->kind()
//      << "</value-type>" << endl ;
    QDomElement valuetype = Graph.createElement( "value-type" ) ;
    QString aKind ;
    aKind = aKind.setNum( aLink.aLinkValue.type()->kind() ) ;
    aField = Graph.createTextNode( aKind ) ;
    datavalue.appendChild( valuetype ) ;
    valuetype.appendChild( aField ) ;
    switch (aLink.aLinkValue.type()->kind()) {
      case CORBA::tk_string: {
        const char* retstr ;
        aLink.aLinkValue >>= retstr;
//        f << Tabs << "	<value>" << retstr << "</value>" << endl ;
        QDomElement value = Graph.createElement( "value" ) ;
//PAL9133 Debug JR : accept void strings
        QDomCDATASection aCDATA ;
        int i ;
        for ( i = 0 ; i < (int ) strlen( retstr ) ; i++ ) {
          if ( retstr[ i ] != ' ' ) {
            break ;
    	  }
        }
        if ( i == (int ) strlen( retstr ) ) {
          aCDATA = Graph.createCDATASection( "?" ) ;
        }
        else {
          aCDATA = Graph.createCDATASection( retstr ) ;
	}
//        aField = Graph.createTextNode( retstr ) ;
        datavalue.appendChild( value ) ;
//        datavalue.appendChild( value ) ;
        value.appendChild( aCDATA ) ;
//        MESSAGE( "ToString( string ) " << retstr );
        break ;
      }
      case CORBA::tk_double: {
        CORBA::Double d;
        aLink.aLinkValue >>= d;
//        f << Tabs << "	<value>" << d << "</value>" << endl ;
        QDomElement value = Graph.createElement( "value" ) ;
        QString aKind ;
        aKind = aKind.setNum( d ) ;
        aField = Graph.createTextNode( aKind ) ;
        datavalue.appendChild( value ) ;
        value.appendChild( aField ) ;
//        MESSAGE( "ToString( double ) " << d );
        break ;
      }
      case CORBA::tk_long: {
        CORBA::Long l;
        aLink.aLinkValue >>= l;
//        f << Tabs << "	<value>" << l << "</value>" << endl ;
        QDomElement value = Graph.createElement( "value" ) ;
        QString aKind ;
        aKind = aKind.setNum( l ) ;
        aField = Graph.createTextNode( aKind ) ;
        datavalue.appendChild( value ) ;
        value.appendChild( aField ) ;
//        MESSAGE( "ToString( CORBA::Long ) " << l );
        break ;
      }
      case CORBA::tk_objref: {
        char* retstr ;
        CORBA::Object_ptr obj ;
#if OMNIORB_VERSION >= 4
        aLink.aLinkValue >>= (CORBA::Any::to_object)obj;
#else
	aLink.aLinkValue >>= obj;
#endif 
        retstr = _Orb->object_to_string(obj );
//        f << Tabs << "	<value>" << retstr << "</value>" << endl ;
        QDomElement value = Graph.createElement( "value" ) ;
        aField = Graph.createTextNode( retstr ) ;
        datavalue.appendChild( value ) ;
        value.appendChild( aField ) ;
//        MESSAGE( "ToString( object ) " << retstr );
        break ;
      }
      default: {
//        f << Tabs << "	<value>?</value>" << endl ;
        QDomElement value = Graph.createElement( "value" ) ;
        aField = Graph.createTextNode( "?" ) ;
        datavalue.appendChild( value ) ;
        value.appendChild( aField ) ;
//        MESSAGE( "Unknown CORBA::Any Type" );
        break ;
      }
    }
//    f << Tabs << "</data-value>" << endl ;
  }
//  f << Tabs << "<coord-list>" << endl ;
  QDomElement coordlist = Graph.createElement( "coord-list" ) ;
  link.appendChild( coordlist ) ;
  
  int i ;
  for ( i = 0 ; i < (int ) aLink.aListOfCoords.size() ; i++ ) {
//    f << Tabs << "	<coord>" << endl ;
    QDomElement coord = Graph.createElement( "coord" ) ;
    coordlist.appendChild( coord ) ;
//    f << Tabs << "		<x>" << aLink.aListOfCoords[ i ].theX << "</x>" << endl ;
    QDomElement x = Graph.createElement( "x" ) ;
    QString ax ;
    ax = ax.setNum( aLink.aListOfCoords[ i ].theX ) ;
    aField = Graph.createTextNode( ax ) ;
    coord.appendChild( x ) ;
    x.appendChild( aField ) ;    
//    f << Tabs << "		<y>" << aLink.aListOfCoords[ i ].theY << "</y>" << endl ;
    QDomElement y = Graph.createElement( "y" ) ;
    QString ay ;
    ay = ay.setNum( aLink.aListOfCoords[ i ].theY ) ;
    aField = Graph.createTextNode( ay ) ;
    coord.appendChild( y ) ;
    y.appendChild( aField ) ;    
//    f << Tabs << "	</coord>" << endl ;
  }
//  f << Tabs << "</coord-list>" << endl ;
  cdebug_out << "GraphEditor::OutNode::LinkSaveXML " << aLink.FromNodeName
             << "(" << aLink.FromServiceParameterName << ") --> "
             << aLink.ToNodeName << "(" << aLink.ToServiceParameterName << ")"
             << endl ;
  return true ;
}

bool GraphEditor::OutNode::LinkSavePY( ostream &f , const char * aGraphName ,
                                       GraphBase::SLink aLink ,
                                       bool fromparam , bool toparam ,
                                       bool wdata ) const {
  if ( !wdata ) {
//    if ( intervar ) {
//      f << "O" << aLink.FromNodeName.c_str() << aLink.FromServiceParameterName.c_str()
//        << " = "
//        << aLink.FromNodeName.c_str() << ".GetOutPort( '"
//        << aLink.FromServiceParameterName.c_str()
//        << "' )" << endl ;
//    }
    f << "    " << "L" << aLink.FromNodeName.c_str() << aLink.FromServiceParameterName.c_str()
      << aLink.ToNodeName.c_str() << aLink.ToServiceParameterName.c_str() ;
    if ( ((GraphBase::Graph *) Graph())->GetChangeGraphNode( aLink.FromNodeName.c_str() )->GetChangeOutPort( aLink.FromServiceParameterName.c_str() )->IsDataStream() ) {
      f << " = " << aGraphName << ".StreamLink( " ;
    }
    else {
      f << " = " << aGraphName << ".Link( " ;
    }
//    if ( !fromparam ) {
      f << "O" ;
//    }
    f << aLink.FromNodeName.c_str() << aLink.FromServiceParameterName.c_str() << " , " ;
//    if ( !toparam ) {
      f << "I" ;
//    }
    f << aLink.ToNodeName.c_str() << aLink.ToServiceParameterName.c_str() << " )" << endl ;
  }
  else {
    f << "    " << "I"<< aLink.ToNodeName.c_str() << aLink.ToServiceParameterName.c_str()
//      << " = " << aLink.ToNodeName.c_str() << ".Input( '"
//      << aLink.ToServiceParameterName.c_str() << "' , " ;
      << ".Input( " ;
    switch (aLink.aLinkValue.type()->kind()) {
      case CORBA::tk_string: {
        const char* retstr ;
        aLink.aLinkValue >>= retstr;
        f << "'" << retstr << "'" ;
        break ;
      }
      case CORBA::tk_double: {
        CORBA::Double d;
        aLink.aLinkValue >>= d;
        f << d ;
        break ;
      }
      case CORBA::tk_long: {
        CORBA::Long l;
        aLink.aLinkValue >>= l;
        f << l ;
        break ;
      }
      case CORBA::tk_objref: {
        char* retstr ;
        CORBA::Object_ptr obj ;
#if OMNIORB_VERSION >= 4
        aLink.aLinkValue >>= (CORBA::Any::to_object)obj;
#else
	aLink.aLinkValue >>= obj;
#endif 
        retstr = _Orb->object_to_string(obj );
        f << "'" << retstr << "'" ;
        break ;
      }
      default: {
        f << "?" ;
//        MESSAGE( "Unknown CORBA::Any Type" );
        break ;
      }
    }
    f << " )" << endl ;
  }
  int i ;
  for ( i = 0 ; i < (int ) aLink.aListOfCoords.size() ; i++ ) {
    f << "    " << "L" << aLink.FromNodeName.c_str() << aLink.FromServiceParameterName.c_str()
      << aLink.ToNodeName.c_str() << aLink.ToServiceParameterName.c_str() << ".AddCoord( " << i+1 << " , "
      << aLink.aListOfCoords[ i ].theX << " , "
      << aLink.aListOfCoords[ i ].theY << " )" << endl ;
  }
  return true ;
}

//bool GraphEditor::OutNode::SaveXML(ostream & f ) {
bool GraphEditor::OutNode::SaveXML( ostream & f , QDomDocument & GraphQDom ,
                                    bool aSuperGraph , QDomElement & supergraph ) {
  cdebug_in << "OutNode::SaveXML( ostream & f , QDomDocument & , " << aSuperGraph << " , QDomElement & ) "
            << Graph()->Name() << endl ;
  int i ;

  QDomElement dataflow ;
  if ( aSuperGraph ) {
    QString SuperGraph("SuperGraph") ;
    GraphQDom = QDomDocument(SuperGraph) ;

    supergraph = GraphQDom.createElement( "supergraph" ) ;
    GraphQDom.appendChild( supergraph ) ;

    dataflow = GraphQDom.createElement( "dataflow" ) ;
    supergraph.appendChild( dataflow ) ;
  }
  else {
//    QString Dataflow("Dataflow") ;
//    GraphQDom = QDomDocument(Dataflow) ;

    dataflow = GraphQDom.createElement( "dataflow" ) ;
    supergraph.appendChild( dataflow ) ;
  }

  QDomElement info = GraphQDom.createElement( "info-list" ) ;
  dataflow.appendChild( info ) ;

  Graph()->SaveXML( GraphQDom , info , 0 , 0 ) ;

  QDomElement nodelist = GraphQDom.createElement( "node-list" ) ;
  dataflow.appendChild( nodelist ) ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
//      f << "		<node>" << endl ;
      if ( Graph()->GraphNodes( i )->IsComputingNode() ) {
//        ((GraphBase::ComputingNode *)GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::ComputingNode *) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsFactoryNode() ) {
//        ((GraphBase::FactoryNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::FactoryNode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsInLineNode()  ) {
//        ((GraphBase::InLineNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::InLineNode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsMacroNode() ) {
//        ((GraphBase::InLineNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::GOTONode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsGOTONode() ) {
//        ((GraphBase::GOTONode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::GOTONode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsLoopNode() ) {
//        ((GraphBase::LoopNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::LoopNode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsEndLoopNode() ) {
//        ((GraphBase::EndOfLoopNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::EndOfLoopNode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsSwitchNode() ) {
//        ((GraphBase::SwitchNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::SwitchNode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
      else if ( Graph()->GraphNodes( i )->IsEndSwitchNode() ) {
//        ((GraphBase::EndOfSwitchNode * ) GraphNodes( i ))->SaveXML( f ,
//                    "			" ,
        ((GraphBase::EndOfSwitchNode * ) Graph()->GraphNodes( i ))->SaveXML( GraphQDom , nodelist ,
                    Graph()->GraphNodes( i )->XCoordinate() ,
                    Graph()->GraphNodes( i )->YCoordinate() ) ;
      }
//      f << "		</node>" << endl ;
//    }
  }
//  f << "	</node-list>" << endl << endl ;

//  f << "	<link-list>" << endl ;
  QDomElement linklist = GraphQDom.createElement( "link-list" ) ;
  dataflow.appendChild( linklist ) ;
  const GraphBase::ListOfSLinks * Links = Graph()->GetLinks( true ) ;
  for ( i = 0 ; i < (int ) Links->size() ; i++ ) {
//    f << "		<link>" << endl ;
    QDomElement link = GraphQDom.createElement( "link" ) ;
    linklist.appendChild( link ) ;
//    LinkSaveXML( f , "			" , (*Links)[ i ] , false ) ;
    LinkSaveXML( GraphQDom , link , (*Links)[ i ] , false ) ;
//    f << "		</link>" << endl ;
  }
//  f << "	</link-list>" << endl << endl ;

//  f << "	<data-list>" << endl ;
  QDomElement datalist = GraphQDom.createElement( "data-list" ) ;
  dataflow.appendChild( datalist ) ;
  if ( Graph()->GraphMacroLevel() == 0 ) {
    const GraphBase::ListOfSLinks * Datas = Graph()->GetDatas() ;
    for ( i = 0 ; i < (int ) Datas->size() ; i++ ) {
//    f << "		<data>" << endl ;
      QDomElement data = GraphQDom.createElement( "data" ) ;
      datalist.appendChild( data ) ;
//    LinkSaveXML( f , "			" , (*Datas)[ i ] , true ) ;
      LinkSaveXML( GraphQDom , data , (*Datas)[ i ] , true ) ;
//    f << "		</data>" << endl ;
    }
  }

  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsMacroNode() ) {
      GraphBase::GOTONode * aMacroNode = (GraphBase::GOTONode * ) Graph()->GraphNodes( i ) ;
      GraphBase::Graph * aMacroGraph = (GraphBase::Graph * ) aMacroNode->CoupledNode() ;
      cdebug << "OutNode::SaveXML ---> OutNode::SaveXML( ostream & f , QDomDocument & , false "
             << " , QDomElement & ) MacroGraph " << aMacroGraph->Name() << endl ;
      if ( !aMacroGraph->GraphEditor()->SaveXML( f , GraphQDom , false , supergraph ) ) {
        return false ;
      cdebug << "OutNode::SaveXML MacroGraph "<< aMacroGraph->Name() << " done" << endl ;
      }
    }
  }

  cdebug_out << "OutNode::SaveXML( ostream & f , QDomDocument & , " << aSuperGraph << " , QDomElement & ) "
             << Graph()->Name() << endl ;

  return true ;
}

bool GraphEditor::OutNode::SavePY( ostream & f , bool importSuperV ) {
  int i ;
  int j ;
  const GraphBase::ListOfSLinks * Links ;
  if ( importSuperV ) {
    f << endl << "# Generated python file of Graph " << Graph()->Name() << endl << endl ;

    f << "from SuperV import *" << endl << endl ;
  }

  f << "# Graph creation of " << Graph()->Name() << endl ;
  f << "def Def" << Graph()->Name() << "() :" << endl ;
  Graph()->SavePY( f , Graph()->Name() , 0 , 0 ) ;

  f << "    " << endl << "    " << "# Creation of Factory Nodes" << endl ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsFactoryNode() ) {
      f << "    " << endl ;
      ((GraphBase::FactoryNode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                      Graph()->GraphNodes( i )->XCoordinate() ,
                                                                      Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  bool first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsComputingNode() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Creation of Computing Nodes" << endl ;
        first = false ;
      }
      else {
        f << "    " << endl ;
      }
      ((GraphBase::ComputingNode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                        Graph()->GraphNodes( i )->XCoordinate() ,
                                                                        Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsInLineNode() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Creation of InLine Nodes" << endl ;
        first = false ;
      }
      else {
        f << "    " << endl ;
      }
      ((GraphBase::InLineNode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                     Graph()->GraphNodes( i )->XCoordinate() ,
                                                                     Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsLoopNode() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Creation of Loop Nodes" << endl ;
        first = false ;
      }
      else {
        f << "    " << endl ;
      }
      ((GraphBase::LoopNode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                   Graph()->GraphNodes( i )->XCoordinate() ,
                                                                   Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsSwitchNode() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Creation of Switch Nodes" << endl ;
        first = false ;
      }
      else {
        f << "    " << endl ;
      }
      ((GraphBase::SwitchNode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                     Graph()->GraphNodes( i )->XCoordinate() ,
                                                                     Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsGOTONode() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Creation of GOTO Nodes" << endl ;
        first = false ;
      }
      else {
        f << "    " << endl ;
      }
      ((GraphBase::GOTONode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                   Graph()->GraphNodes( i )->XCoordinate() ,
                                                                   Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsMacroNode() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Creation of Macro Nodes" << endl ;
        first = false ;
      }
      else {
        f << "    " << endl ;
      }
      ((GraphBase::GOTONode * ) Graph()->GraphNodes( i ))->SavePY( f , Graph()->Name() ,
                                                                   Graph()->GraphNodes( i )->XCoordinate() ,
                                                                   Graph()->GraphNodes( i )->YCoordinate() ) ;
    }
  }

  Links = Graph()->GetLinks() ;
//  bool intervar ;
//  map< string , int > aMapOfOutPorts ;
  first = true ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    for ( j = 0 ; j < (int ) Links->size() ; j++ ) {
      if ( !strcmp( Graph()->GraphNodes( i )->Name() , (*Links)[ j ].FromNodeName.c_str() ) ) {
        if ( first ) {
          f << "    " << endl
            << "    " << "# Creation of Links"
            << endl ;
          first = false ;
        }
        else {
          f << "    " << endl ;
        }
//        char * NodePort = new char [ strlen( (*Links)[ j ].FromNodeName.c_str() ) +
//                                     strlen( (*Links)[ j ].FromServiceParameterName.c_str() ) + 1 ] ;
//        strcpy( NodePort , (*Links)[ j ].FromNodeName.c_str() ) ;
//        strcat( NodePort , (*Links)[ j ].FromServiceParameterName.c_str() ) ;
//        if ( aMapOfOutPorts[ NodePort ] == 0 ) {
//          aMapOfOutPorts[ NodePort ] = j + 1 ;
//          intervar = true ;
//        }
//        else {
//          intervar = false ;
//        }
        bool fromparam = false ;
        if ( Graph()->GraphNodes( i )->GetOutPort( (*Links)[ j ].FromServiceParameterName.c_str() )->IsParam() ) {
          fromparam = true ;
	}
        bool toparam = false ;
        if ( Graph()->GetChangeGraphNode( (*Links)[ j ].ToNodeName.c_str() )->GetInPort( (*Links)[ j ].ToServiceParameterName.c_str() )->IsParam() ) {
          toparam = true ;
	}
        LinkSavePY( f , Graph()->Name() , (*Links)[ j ] , fromparam , toparam , false ) ;
//        delete [] NodePort ;
      }
    }
  }

  if ( Graph()->GraphMacroLevel() == 0 ) {
    const GraphBase::ListOfSLinks * Datas = Graph()->GetDatas() ;
    first = true ;
    for ( i = 0 ; i < (int ) Datas->size() ; i++ ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Input datas" << endl ;
        first = false ;
      }
      bool fromparam = true ;
      bool toparam = true ;
      LinkSavePY( f , Graph()->Name() , (*Datas)[ i ] , fromparam , toparam , true ) ;
    }
  }

  first = true ;
  const SALOME_ModuleCatalog::ListOfServicesParameter ListOfInParam = Graph()->ServiceInParameter() ;
  for ( i = 0 ; i < (int ) ListOfInParam.length() ; i++ ) {
    string _aParam = CORBA::string_dup(ListOfInParam[ i ].Parametername) ;
    const char * aParam = _aParam.c_str() ;
    char * aNodeName ;
    char * aPortName ;
    int j ;
    for ( j = 0 ; j < (int ) strlen( aParam ) ; j++ ) {
//      if ( aParam[ j ] == '\\' ) {
      if ( aParam[ j ] == '_' && aParam[ j+1 ] == '_' ) {
        aNodeName = new char[ j+1 ] ;
        strncpy( aNodeName , aParam , j ) ;
        aNodeName[ j ] = '\0' ;
        aPortName = new char[ strlen( aParam ) - j-1 ] ;
        strncpy( aPortName , &aParam[ j+2 ] , strlen( aParam ) - j-1 ) ;
        break ;
      }
    }
    const GraphBase::InPort * anInPort = Graph()->GetChangeGraphNode( aNodeName )->GetInPort( aPortName ) ;
    if ( !anInPort->IsDataConnected() ) {
      if ( first ) {
        f << "    " << endl << "    " << "# Input Ports of the graph" << endl ;
        first = false ;
      }
      f << "    " << "#I" << aNodeName << aPortName << " = " << aNodeName << ".GetInPort( '"
        << aPortName << "' )" << endl ;
    }
    delete [] aNodeName ;
    delete [] aPortName ;
  }

  f << "    " << endl << "    # Output Ports of the graph" << endl ;
  const SALOME_ModuleCatalog::ListOfServicesParameter ListOfOutParam = Graph()->ServiceOutParameter() ;
  for ( i = 0 ; i < (int ) ListOfOutParam.length() ; i++ ) {
    string _aParam = CORBA::string_dup(ListOfOutParam[ i ].Parametername) ;
    const char * aParam = _aParam.c_str() ;
    char * aNodeName ;
    char * aPortName ;
    int j ;
    for ( j = 0 ; j < (int ) strlen( aParam ) ; j++ ) {
//      if ( aParam[ j ] == '\\' ) {
      if ( aParam[ j ] == '_' && aParam[ j+1 ] == '_' ) {
        aNodeName = new char[ j+1 ] ;
        strncpy( aNodeName , aParam , j ) ;
        aNodeName[ j ] = '\0' ;
        aPortName = new char[ strlen( aParam ) - j-1 ] ;
        strncpy( aPortName , &aParam[ j+2 ] , strlen( aParam ) - j-1 ) ;
        break ;
      }
    }
    f << "    " << "#O" << aNodeName << aPortName << " = " << aNodeName << ".GetOutPort( '"
      << aPortName << "' )" << endl ;
    delete [] aNodeName ;
    delete [] aPortName ;
  }

  f << "    " << "return " << Graph()->Name() << endl << endl ;

// RECURSIVE CREATION OF GRAPHS OF MACRONODES
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    if ( Graph()->GraphNodes( i )->IsMacroNode() ) {
      GraphBase::GOTONode * aMacroNode = (GraphBase::GOTONode * ) Graph()->GraphNodes( i ) ;
      GraphBase::Graph * aMacroGraph = (GraphBase::Graph * ) aMacroNode->CoupledNode() ;
      cdebug << "OutNode::SavePY ---> OutNode::SavePY( ostream & f ) MacroGraph " << aMacroGraph->Name() << endl ;
      GraphEditor::DataFlow * aDataFlow = aMacroGraph->GraphEditor() ;
      cdebug << "SavePY of the Graph " << aDataFlow->Graph() << " of the MacroNode "
             << aMacroGraph->Name() << endl ;
      if ( !aDataFlow->SavePY( f , false ) ) {
        return false ;
      }
    }
  }

//  f << Graph()->Name() << " = " << Graph()->Name() << "()" << endl ;

  return true ;
}

/** Iterate through ALL links (OutPort-InPort pairs) and check if their types are 
 *  compatible - call GraphEditor::DataFlow::IsCompatible(type1, type2).
 *  Returns true if all are compatible.
 */
bool GraphEditor::OutNode::IsLinksCompatible() {
  cdebug_in << "Editor::OutNode::IsLinksCompatible()" << endl ;
  bool RetVal = true;
  bool b ;
  const GraphBase::ListOfSLinks * Links = Graph()->GetLinks( true ) ;
  cdebug_in << "Editor::OutNode::IsLinksCompatible() " << Links->size() << " Links" << endl ;
//  for ( int i = 0 ; i < (int ) Links->size() && b ; i++ ) {
  for ( int i = 0 ; i < (int ) Links->size() ; i++ ) {
    GraphBase::SLink aLink = (*Links)[i];
    GraphBase::ComputingNode* anOutNode = Graph()->GetChangeGraphNode( aLink.FromNodeName.c_str() );
    GraphBase::ComputingNode* anInNode = Graph()->GetChangeGraphNode( aLink.ToNodeName.c_str() );
    const GraphBase::OutPort* anOutPort = anOutNode->GetOutPort( aLink.FromServiceParameterName.c_str() );
    const GraphBase::InPort* anInPort = anInNode->GetInPort( aLink.ToServiceParameterName.c_str() );    
    b = IsCompatible( anOutPort->PortType(), anInPort->PortType() );
    cdebug << "GraphEditor::OutNode::IsLinksCompatible:  " << aLink.FromNodeName << "( "
           << aLink.FromServiceParameterName << " " << anOutPort->PortType()
           << " )  -->  " << aLink.ToNodeName <<"( " << aLink.ToServiceParameterName << " "
           << anInPort->PortType() << " ) = " << (b ? "OK" : "Not compatible (ERROR)") << endl;
    if ( !b ) {
      RetVal = false ;
      MESSAGE( "Graph structure ERROR: type of port \"" << aLink.FromServiceParameterName
               << "\" of node \"" << aLink.FromNodeName
               << "\" is not compatible with type of linked port \""
	       << aLink.ToServiceParameterName << "\" of node \"" << aLink.ToNodeName<<"\"" ) ; 
      ostringstream aTypeOutPortstr ;
      aTypeOutPortstr << anOutPort->PortType() ;
      ostringstream aTypeInPortstr ;
      aTypeInPortstr << anInPort->PortType() ;
      string anErrorMessage = string( "PortTypes of " ) + string( aLink.FromNodeName ) +
                              string( "( " ) + aLink.FromServiceParameterName +
                              string( " ) " ) + aTypeOutPortstr.str() + string( " and " ) +
                              string( aLink.ToNodeName ) + string( "( " ) +
                              string( aLink.ToServiceParameterName ) + 
                              string( " ) " ) + aTypeInPortstr.str() +
                              string( " are not compatibles.\n" ) ;
      Graph()->SetMessages( anErrorMessage ) ;
    }
  }
  cdebug_out << "Editor::OutNode::IsLinksCompatible() RetVal " << RetVal << endl ;
  return RetVal ;
}

static const char* gSimpleTypes[] = 
  {"boolean", "char", "short", "int", "long", "float", "double"};
bool isSimpleType( string type ) {
  for ( int i = 0; i < 7; i++ )
    if ( type == gSimpleTypes[i] )
      return true;
  return false;
}

/**Returns true if an out-port of type "OutPortType" can be bound with in-port of type "InPortType". 
 * Types: {"string", "boolean", "char", "short", "int", "long", "float", "double", "objref"};
 * Currently considered compatible ALL types except for objref - they must match exactly
 */
bool GraphEditor::OutNode::IsCompatible( const char* OutPortType, const char* InPortType ) const {
  bool ret = true;
  string t1 = OutPortType;
  string t2 = InPortType;
  // if ANY is a string - the link is OK
  if ( t1 == "string" || t2 == "string" )
    ret = true;

  // the next check prohibits linkage of "objref" to any simple type (int, char, etc.)
  // it is still possible to link "objref" to some UNKNOWN type (probably objref, too,
  // which interface name came from Cataloge
  else if ( ( t1 == "objref" && isSimpleType( t2 ) ) ||  
	    ( t2 == "objref" && isSimpleType( t1 ) ) )
    ret = false; 
  return ret;
}

ostream & operator<< (ostream & f,const GraphEditor::OutNode & G) {
  f << (GraphBase::ComputingNode ) *(G.Graph()) ;
  f << endl ;

  f << "  Nodes : " << (G.Graph())->GraphNodesSize() << " node" 
    << ((G.Graph())->GraphNodesSize() > 1 ? "s" : "") << endl;
  
  int i ;
  for ( i = 0 ; i < (G.Graph())->GraphNodesSize() ; i++ ) {
    f
//      << hex << (void *) G.Graph().GraphNodes( i ) << dec << " "
      << (G.Graph())->GraphNodes( i ) << endl;
  }

  f << "  Links : " << endl ;
  for ( i = 0 ; i < (G.Graph())->GraphNodesSize() ; i++ ) {
    (G.Graph())->GraphNodes( i )->ListLinks( f ) ;
  }

  f << "  Datas : " << endl ;
  (G.Graph())->ListDatas( f ) ;

  f << "DataFlow " << (G.Graph())->Name() << " is " ;
  if ( G.IsNotValid() )
    f << "not " ;
  f << "valid and is " ;
  if ( G.IsNotExecutable() )
    f << "not " ;
  f << "executable." << endl ;

  f << endl ;
  
  return f;
}

ostream & operator<< (ostream &fOut,const SUPERV::SDate &D)
{
//  cdebug_in << "operator<< GraphEditor::Date" << endl;

  fOut  << D.Day << "/" 
	<< D.Month << "/" 
	<< D.Year << " - " 
	<< D.Hour << ":" 
	<< D.Minute <<  ":"  
	<< D.Second;

//  cdebug_out << "operator<< GraphEditor::Date" << endl;
  return fOut;
}

/*
GraphBase::Graph * GraphEditor::OutNode::MapGraph( const char * aGraphName ) {
  GraphBase::Graph * aGraph = _MapOfGraphs[ aGraphName ] ;
  return aGraph ;
}

bool GraphEditor::OutNode::MapGraph( GraphBase::Graph * aGraph , const char * aGraphName ) {
  if ( MapGraph( aGraphName ) ) {
    return false ;
  }
  _MapOfGraphs[ aGraphName ] = aGraph ;
  return true ;
}

void GraphEditor::OutNode::EraseGraph( const char * aGraphName ) {
  _MapOfGraphs.erase( aGraphName ) ;
}

bool GraphEditor::OutNode::GraphName( const char * aGraphName ) {
  return  _MapOfGraphNames[ aGraphName ] ;
}
*/



//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowEditor_InNode.cxx
//  Module : SUPERV

using namespace std;
//#include <string.h>
//#include <sstream>
//#include <iostream>

#include "DataFlowEditor_InNode.hxx"

GraphEditor::InNode::InNode() {
}

#if 0
GraphEditor::InNode::InNode(GraphBase::GOTONode * aGOTONode ) {
  _ComputingNode = aGOTONode ;
  _GOTONode = aGOTONode ;
}
#endif

//ostream * fdebug = NULL ;
GraphEditor::InNode::InNode( CORBA::ORB_ptr ORB, 
	                     SALOME_NamingService * ptrNamingService ,
                             GraphBase::ListOfFuncName aFuncName ,
                             GraphBase::ListOfPythonFunctions aPythonFunction ,
                             const SALOME_ModuleCatalog::Service& aService ,
                             const char * ComponentName ,
                             const char * NodeInterfaceName ,
                             const char * NodeName ,
                             const SUPERV::KindOfNode akind ,
                             const SUPERV::SDate NodeFirstCreation ,
                             const SUPERV::SDate NodeLastModification  ,
                             const char * NodeEditorRelease ,
                             const char * NodeAuthor ,
                             const char * NodeComputer ,
                             const char * NodeComment ,
                             const bool   GeneratedName ,
                             const int NodeX ,
                             const int NodeY ,
                             int * Graph_prof_debug,
                             ofstream * Graph_fdebug) {
  SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
//  cout << "GraphEditor::InNode::InNode GraphBase::Base::_fdebug " << GraphBase::Base::_fdebug << endl ;
  cdebug_in << "GraphEditor::InNode::InNode" << endl ;
//  if ( GraphBase::Base::_fdebug )
//    *GraphBase::Base::_fdebug << endl << "xxx-->" << " " << "GraphEditor::InNode::InNode" << endl ;
//  if ( fdebug )
//    (*fdebug) << endl << "xxxxxx-->" << " " << "GraphEditor::InNode::InNode" << endl ;
//  cout << "GraphEditor::InNode::InNode GraphBase::Base::_fdebug " << GraphBase::Base::_fdebug << endl ;
  _ComputingNode = NULL ;
  _FactoryNode = NULL ;
  _InLineNode = NULL ;
  _GOTONode = NULL ;
  _LoopNode = NULL ;
  _EndOfLoopNode = NULL ;
  _SwitchNode = NULL ;
  _EndOfSwitchNode = NULL ;
  switch ( akind ) {
  case SUPERV::ComputingNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::ComputingNode : " << NodeName << endl ;
    _ComputingNode = new GraphBase::ComputingNode( ORB , ptrNamingService , aService ,
                                                   NodeName , akind ,
                                                   NodeFirstCreation ,
                                                   NodeLastModification  ,
                                                   NodeEditorRelease , NodeAuthor ,
                                                   NodeComment , GeneratedName ,
                                                   NodeX , NodeY ,
                                                   Graph_prof_debug , Graph_fdebug ) ;
    break ;
  }
  case SUPERV::FactoryNode : {
//    cout << "GraphEditor::InNode::InNode SUPERV::FactoryNode : " << NodeName << endl ;
    cdebug << "GraphEditor::InNode::InNode SUPERV::FactoryNode : " << NodeName << endl ;
    _FactoryNode = new GraphBase::FactoryNode( ORB , ptrNamingService , aService ,
                                               ComponentName , NodeInterfaceName ,
                                               NodeName , akind ,
                                               NodeFirstCreation ,
                                               NodeLastModification  ,
                                               NodeEditorRelease , NodeAuthor ,
                                               NodeComputer , NodeComment ,
                                               GeneratedName , NodeX , NodeY ,
                                               Graph_prof_debug , Graph_fdebug ) ;
//    cout << "GraphEditor::InNode::InNode SUPERV::FactoryNode : " << NodeName << endl ;
    _ComputingNode = (GraphBase::ComputingNode *) _FactoryNode ;
    break ;
  }
  case SUPERV::InLineNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::InLineNode : " << NodeName << endl ;
    _InLineNode = new GraphBase::InLineNode( ORB , ptrNamingService ,
                                             aFuncName[0].c_str() , *aPythonFunction[0] ,
                                             NodeName , akind ,
                                             NodeFirstCreation , NodeLastModification  ,
                                             NodeEditorRelease , NodeAuthor ,
                                             NodeComment , GeneratedName ,
                                             NodeX , NodeY ,
                                             Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _InLineNode ;
    break ;
  }
  case SUPERV::MacroNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::MacroNode : " << NodeName << endl ;
    _GraphMacroNode = new GraphBase::Graph( ORB , ptrNamingService ,
//                                            aFuncName[0].c_str() , *aPythonFunction[0] ,
                                            NodeName ,
                                            akind ,
//                                            NodeFirstCreation , NodeLastModification  ,
//                                            NodeEditorRelease , NodeAuthor ,
//                                            NodeComment , GeneratedName ,
//                                            NodeX , NodeY ,
                                            Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _GraphMacroNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    _GOTONode = (GraphBase::GOTONode *) _InLineNode ;
    _GraphMacroNode->Coordinates( NodeX , NodeY ) ;
    break ;
  }
  case SUPERV::GOTONode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::GOTONode : " << NodeName << endl ;
    _GOTONode = new GraphBase::GOTONode( ORB , ptrNamingService ,
                                         aFuncName[0].c_str() , *aPythonFunction[0] ,
                                         NodeName , akind ,
                                         NodeFirstCreation , NodeLastModification  ,
                                         NodeEditorRelease , NodeAuthor ,
                                         NodeComment , GeneratedName ,
                                         NodeX , NodeY ,
                                         Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _GOTONode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::LoopNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::LoopNode : " << NodeName << endl ;
    _LoopNode = new GraphBase::LoopNode( ORB , ptrNamingService ,
                                         aFuncName[0].c_str() , *aPythonFunction[0] ,
                                         aFuncName[1].c_str() , *aPythonFunction[1] ,
                                         aFuncName[2].c_str() , *aPythonFunction[2] ,
                                         NodeName , akind ,
                                         NodeFirstCreation , NodeLastModification  ,
                                         NodeEditorRelease , NodeAuthor ,
                                         NodeComment , GeneratedName ,
                                         NodeX , NodeY ,
                                         Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _LoopNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::EndLoopNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::EndOfLoopNode : " << NodeName << endl ;
    _EndOfLoopNode = new GraphBase::EndOfLoopNode(
                                         ORB , ptrNamingService ,
                                         aFuncName[0].c_str() , *aPythonFunction[0] ,
                                         NodeName , akind ,
                                         NodeFirstCreation , NodeLastModification  ,
                                         NodeEditorRelease , NodeAuthor ,
                                         NodeComment , GeneratedName ,
                                         NodeX , NodeY ,
                                         Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _EndOfLoopNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::SwitchNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::SwitchNode : " << NodeName << endl ;
    _SwitchNode = new GraphBase::SwitchNode( ORB , ptrNamingService ,
                                             aFuncName[0].c_str() , *aPythonFunction[0] ,
                                             NodeName , akind ,
                                             NodeFirstCreation , NodeLastModification  ,
                                             NodeEditorRelease , NodeAuthor ,
                                             NodeComment , GeneratedName ,
                                             NodeX , NodeY ,
                                             Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _SwitchNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::EndSwitchNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::EndOfSwitchNode : " << NodeName << endl ;
    _EndOfSwitchNode = new GraphBase::EndOfSwitchNode(
                                             ORB , ptrNamingService ,
                                             aFuncName[0].c_str() , *aPythonFunction[0] ,
                                             NodeName , akind ,
                                             NodeFirstCreation , NodeLastModification  ,
                                             NodeEditorRelease , NodeAuthor ,
                                             NodeComment , GeneratedName ,
                                             NodeX , NodeY ,
                                             Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _EndOfSwitchNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::DataFlowGraph : {
    cdebug << "GraphEditor::InNode::InNode ERROR SUPERV::DataFlowGraph : " << NodeName << endl ;
  }
  case SUPERV::DataStreamGraph : {
    cdebug << "GraphEditor::InNode::InNode ERROR SUPERV::DataStreamGraph : " << NodeName << endl ;
  }
  case SUPERV::UnknownNode : {
    cdebug << "GraphEditor::InNode::InNode ERROR SUPERV::UnknownNode : " << NodeName << endl ;
  }
  }
  _ComputingNode->InNode( this ) ;
  cdebug_out << "GraphEditor::InNode::InNode " << (void *) this
             << " _ComputingNode " << (void *) _ComputingNode  << endl ;
}

GraphEditor::InNode::~InNode() {
}


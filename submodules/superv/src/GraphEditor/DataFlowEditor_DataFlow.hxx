//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowEditor_DataFlow.hxx
//  Module : SUPERV

#ifndef _DATAFLOWEDITOR_DATAFLOW_HXX
#define _DATAFLOWEDITOR_DATAFLOW_HXX

#include "DataFlowEditor_OutNode.hxx"

namespace GraphExecutor {
  
  class DataFlow;

} 

namespace GraphEditor {

  class DataFlow : public GraphEditor::OutNode {
    
    private :

      SALOME_NamingService*     _theNamingService ;
      GraphExecutor::DataFlow * _DataFlowExecutor ;
      bool                      _ReadOnly ;
      bool                      _Executing ;
      bool                      _EditedAfterExecution ;

    public:

      DataFlow();
      DataFlow( CORBA::ORB_ptr ORB, SALOME_NamingService * ptrNamingService ,
                const char * DataFlowName ,
                const char * DebugFileName ,
                const SUPERV::KindOfNode aKindOfNode );
      DataFlow( CORBA::ORB_ptr ORB, SALOME_NamingService * ptrNamingService ,
                const SALOME_ModuleCatalog::Service& DataFlowService ,
                const char * DataFlowComponentName ,
                const char * DataFlowInterfaceName ,
                const char * DataFlowName ,
                const SUPERV::KindOfNode DataFlowkind = SUPERV::ComputingNode ,
                const SUPERV::SDate DataFlowFirstCreation = SUPERV::SDate() ,
                const SUPERV::SDate DataFlowLastModification = SUPERV::SDate() ,
                const char * DataFlowEditorRelease = NULLSTRING ,
                const char * DataFlowAuthor = NULLSTRING ,
                const char * DataFlowComputer = NULLSTRING ,
                const char * DataFlowComment = NULLSTRING ,
                const char * DebugFileName = NULLSTRING ) ;
      virtual ~DataFlow();

      void ReadOnly() ;
      char * DataFlowInfo() ;
      char * DataNodeInfo() ;
      char * NodeInfo( const char * aNodeName ) ;

      bool LoadDataFlow( const GraphBase::SGraph * aDataFlow ) ;
//      bool LoadXml( const char * myFileName ) ;
      bool LoadXml( const char * myFileName , GraphBase::ListOfSGraphs & aListOfDataFlows ) ;
      bool LoadInfo( const GraphBase::SNode &aDataFlowInfo ) ;

      bool SaveXml(const char * myFileName ) ;
      bool SavePy(const char * myFileName ) ;


//      void DateModification() ;

      GraphBase::ListOfSGraphs * GetDataFlows() ;
      GraphBase::SNode * GetInfo() const ;
      GraphBase::ListOfSNodes * GetNodes() const ;
      GraphBase::ListOfSLinks * GetLinks() const ;
      GraphBase::ListOfSGraphs * GetGraphs() const ;
      GraphBase::ListOfSLinks * GetDatas() const ;
      const SALOME_ModuleCatalog::Service * GetService() ;

      GraphEditor::InNode * AddService (
                            SALOME_ModuleCatalog::Service& aService ,
                            const char * NodeComponentName ,
                            const char * NodeInterfaceName ) {
             return AddNode( aService , NodeComponentName ,
                             NodeInterfaceName ) ; } ;
      GraphEditor::InNode * AddNode(
                        const SALOME_ModuleCatalog::Service& NodeService ,
                        const char * NodeComponentName ,
                        const char * NodeInterfaceName ,
                        const char * NodeName = NULLSTRING ,
                        const SUPERV::KindOfNode NodeKindOfNode = SUPERV::ComputingNode ,
                        GraphBase::ListOfFuncName aFuncName = GraphBase::ListOfFuncName() ,
                        GraphBase::ListOfPythonFunctions aPythonFunction = GraphBase::ListOfPythonFunctions() ,
                        const SUPERV::SDate NodeFirstCreation = SUPERV::SDate() ,
                        const SUPERV::SDate NodeLastModification = SUPERV::SDate() ,
                        const char * NodeEditorRelease = NULLSTRING ,
                        const char * NodeAuthor = NULLSTRING ,
                        const char * NodeComputer = NULLSTRING ,
                        const char * NodeComment = NULLSTRING ,
                        const int NodeX = 0 ,
                        const int NodeY = 0 ) ;
      GraphEditor::InNode * GetNode( const char* NodeName ) ;
      bool RemoveNode( const char * NodeName ) ;
      bool ReNameNode( const char * OldNodeName ,
                       const char * NewNodeName ) ;

      const SALOME_ModuleCatalog::Service * NodeService( const char * NodeName ) ;

      void Coordinates( const int X , const int Y ) ;
      const int XCoordinate() ;
      const int YCoordinate() ;
      void Coordinates( const char * NodeName , const int X , const int Y ) ;
      const int XCoordinate( const char * NodeName ) ;
      const int YCoordinate( const char * NodeName ) ;

      const GraphBase::InPort * GetInPort( const char * name ) ;
      const GraphBase::OutPort * GetOutPort( const char * name ) ;
      GraphBase::InPort * GetChangeInPort( const char * name ) ;
      GraphBase::OutPort * GetChangeOutPort( const char * name ) ;

      bool HasInput(const char * ToServiceParameterName ) ;

//      bool AddLinkValue( const char * FromNodeName ,
//                         const char * FromServiceParameterName ,
//                         const char * ToNodeName ,
//                         const char * ToServiceParameterName ,
//                         const CORBA::Any aValue ) {
//           return AddLinkValue( FromNodeName, FromServiceParameterName , ToNodeName ,
//                                ToServiceParameterName , aValue ) ; } ;
      bool AddLink( const char * FromNodeName ,
                    const char * FromServiceParameterName ,
	            const char * ToNodeName ,
                    const char * ToServiceParameterName ) ;

      bool RemoveLink( const char * FromNodeName ,
                       const char * FromServiceParameterName ,
                       const char * ToNodeName ,
                       const char * ToServiceParameterName ) ;

      bool GetLink(const char * ToNodeName ,
                   const char * ToServiceParameterName ,
                   char ** FromNodeName ,
                   char ** FromServiceParameterName ) ;
      GraphBase::SLink * GetLink( GraphBase::ComputingNode * aNode ,
                                  const char* ToServiceParameterName ) ;

      bool AddLinkCoord( const char * FromNodeName ,
                         const char * FromServiceParameterName ,
                         const char * ToNodeName ,
                         const char * ToServiceParameterName ,
                         const int nXY ,
                         const int * X ,
                         const int * Y ) ;
      bool AddLinkCoord( const char * FromNodeName ,
                         const char * FromServiceParameterName ,
                         const char * ToNodeName ,
                         const char * ToServiceParameterName ,
                         const int index ,
                         const int X ,
                         const int Y ) ;
      bool ChangeLinkCoord( const char * FromNodeName ,
                            const char * FromServiceParameterName ,
                            const char * ToNodeName ,
                            const char * ToServiceParameterName ,
                            const int index ,
                            const int X ,
                            const int Y ) ;
      bool RemoveLinkCoord( const char * FromNodeName ,
                            const char * FromServiceParameterName ,
                            const char * ToNodeName ,
                            const char * ToServiceParameterName ,
                            const int index ) ;
      int GetLinkCoordSize( const char * FromNodeName ,
                            const char * FromServiceParameterName ,
                            const char * ToNodeName ,
                            const char * ToServiceParameterName ) ;
      bool GetLinkCoord( const char * FromNodeName ,
                         const char * FromServiceParameterName ,
                         const char * ToNodeName ,
                         const char * ToServiceParameterName ,
                         int * X , int * Y ) ;
      bool GetLinkCoord( const char * FromNodeName ,
                         const char * FromServiceParameterName ,
                         const char * ToNodeName ,
                         const char * ToServiceParameterName ,
                         const int index , CORBA::Long &X , CORBA::Long &Y ) ;

      bool AddInputData( const char * ToNodeName ,
                         const char * ToParameterName ,
                         const CORBA::Any aValue = CORBA::Any() ) ;

//JR 30.03.2005      const CORBA::Any * GetInData( const char * ToNodeName ,
      const CORBA::Any GetInData( const char * ToNodeName ,
                                  const char * ToParameterName ) ;
//JR 30.03.2005      const CORBA::Any * GetOutData( const char * FromNodeName ,
      const CORBA::Any GetOutData( const char * FromNodeName ,
                                   const char * FromParameterName ) ;

//      bool IsValid(bool kLoopSwitch = true ) ;
      bool IsValid() ;
      bool IsExecutable() ;

      void Executing() ; // asv : removed a bool parameter, use Editing() to set _Executing = false
      bool IsExecuting() ;
      void Editing() ;
      bool IsEditing() ;
      bool UnValid() ;
      void EditedAfterExecution(bool EditedAfterExecution ) ;
      bool EditedAfterExecution() ;

      bool IsReadOnly() ;

      long LevelMax() ;
      SUPERV::ListOfStrings * LevelNodes(long aLevel ) ;
      long ThreadsMax() ;
      long SubGraphsNumber() ;
      long SubStreamGraphsNumber() ;

      void Executor(GraphExecutor::DataFlow * DataFlowExecutor ) ;
      GraphExecutor::DataFlow * Executor() const ;

  };

};

#include "DataFlowEditor_DataFlow.lxx"

ostream & operator << (ostream &,const GraphEditor::DataFlow & G);
ostream & operator << (ostream &,const SUPERV::SDate &);

#endif




//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowEditor_InNode.hxx
//  Module : SUPERV

#ifndef _DATAFLOWEDITOR_INNODE_HXX
#define _DATAFLOWEDITOR_INNODE_HXX

#include "DataFlowBase_Graph.hxx"
#include "DataFlowBase_FactoryNode.hxx"
#include "DataFlowBase_GOTONode.hxx"
#include "DataFlowBase_LoopNode.hxx"
#include "DataFlowBase_EndOfLoopNode.hxx"
#include "DataFlowBase_SwitchNode.hxx"
#include "DataFlowBase_EndOfSwitchNode.hxx"

namespace GraphEditor {

  class InNode : public GraphBase::Base {

    private:

      GraphBase::ComputingNode       * _ComputingNode ;
      GraphBase::FactoryNode         * _FactoryNode ;
      GraphBase::InLineNode          * _InLineNode ;
      GraphBase::GOTONode            * _GOTONode ;
      GraphBase::LoopNode            * _LoopNode ;
      GraphBase::EndOfLoopNode       * _EndOfLoopNode ;
      GraphBase::SwitchNode          * _SwitchNode ;
      GraphBase::EndOfSwitchNode     * _EndOfSwitchNode ;
      GraphBase::Graph               * _GraphMacroNode ;
    
    public:

      InNode() ;
//      InNode(GraphBase::GOTONode * aGOTONode ) ;
      InNode( CORBA::ORB_ptr ORB, 
	      SALOME_NamingService* ptrNamingService ,
              GraphBase::ListOfFuncName aFuncName ,
              GraphBase::ListOfPythonFunctions aPythonFunction ,
              const SALOME_ModuleCatalog::Service& NodeService ,
              const char *NodeComponentName ,
              const char* NodeInterfaceName ,
              const char *NodeName ,
              const SUPERV::KindOfNode akind ,
              const SUPERV::SDate NodeFirstCreation ,
              const SUPERV::SDate NodeLastModification ,
              const char * NodeEditorRelease ,
              const char * NodeAuthor ,
              const char * NodeComputer ,
              const char * NodeComment ,
              const bool   GeneratedName ,
              const int NodeX ,
              const int NodeY ,
              int * Graph_prof_debug = NULL ,
              ofstream * Graph_fdebug = NULL ) ;
      virtual ~InNode() ;

      char * Name() const { return _ComputingNode->Name() ; } ;
      const char *const * NamePtr() const {
                          return _ComputingNode->NamePtr() ; } ;

      SUPERV::KindOfNode Kind() const {
                         return _ComputingNode->Kind() ; } ;
      const bool IsComputingNode() const {
                 return _ComputingNode->IsComputingNode() ; } ;
      const bool IsFactoryNode() const {
                 return _ComputingNode->IsFactoryNode() ; } ;
      const bool IsInLineNode() const {
                 return _ComputingNode->IsInLineNode() ; } ;
      const bool IsOneOfInLineNodes() const {
                 return _ComputingNode->IsOneOfInLineNodes() ; } ;
      const bool IsOneOfGOTONodes() const {
                 return _ComputingNode->IsOneOfGOTONodes() ; } ;
      const bool IsMacroNode() const {
                 return _ComputingNode->IsMacroNode() ; } ;
      const bool IsDataFlowNode() const {
                 return _ComputingNode->IsDataFlowNode() ; } ;
      const bool IsDataStreamNode() const {
                 return _ComputingNode->IsDataStreamNode() ; } ;
      const bool IsGOTONode() const {
                 return _ComputingNode->IsGOTONode() ; } ;
      const bool IsLoopNode() const {
                 return _ComputingNode->IsLoopNode() ; } ;
      const bool IsEndLoopNode() const {
                 return _ComputingNode->IsEndLoopNode() ; } ;
      const bool IsSwitchNode() const {
                 return _ComputingNode->IsSwitchNode() ; } ;
      const bool IsEndSwitchNode() const {
                 return _ComputingNode->IsEndSwitchNode() ; } ;
      const bool IsHeadNode() const {
                 return _ComputingNode->IsHeadNode() ; } ;
      GraphBase::ComputingNode * ComputingNode() {
                                 return _ComputingNode ; } ;
      GraphBase::FactoryNode * FactoryNode() {
                               return _FactoryNode ; } ;
      GraphBase::GOTONode * GOTONode() {
                            if ( _GOTONode )
                              return _GOTONode ;
                            if ( _LoopNode )
                              return _LoopNode ;
                            if ( _EndOfLoopNode )
                              return _EndOfLoopNode ;
                            if ( _SwitchNode )
                              return _SwitchNode ;
                            if ( _EndOfSwitchNode )
                              return _EndOfSwitchNode ;
                            return NULL ;
                            } ;
      GraphBase::GOTONode * GOTONode() const {
                            if ( _GOTONode )
                              return _GOTONode ;
                            if ( _LoopNode )
                              return _LoopNode ;
                            if ( _EndOfLoopNode )
                              return _EndOfLoopNode ;
                            if ( _SwitchNode )
                              return _SwitchNode ;
                            if ( _EndOfSwitchNode )
                              return _EndOfSwitchNode ;
                            return NULL ;
                            } ;
      GraphBase::InLineNode * InLineNode() {
                              GraphBase::InLineNode * aNode = GOTONode() ;
                              if ( aNode == NULL )
                                return _InLineNode ;
                              return aNode ;
                              } ;
      GraphBase::InLineNode * InLineNode() const {
                              GraphBase::InLineNode * aNode = GOTONode() ;
                              if ( aNode == NULL )
                                return _InLineNode ;
                              return aNode ;
                              } ;
      GraphBase::LoopNode * LoopNode() {
                            return _LoopNode ; } ;
      GraphBase::EndOfLoopNode * EndOfLoopNode() {
                                 return _EndOfLoopNode ; } ;
      GraphBase::SwitchNode * SwitchNode() {
                              return _SwitchNode ; } ;
      GraphBase::EndOfSwitchNode * EndOfSwitchNode() {
                                   return _EndOfSwitchNode ; } ;
      GraphBase::Graph * GraphMacroNode() {
                         return _GraphMacroNode ; } ;

      SUPERV::CNode_var ObjRef() const { return _ComputingNode->ObjRef() ; } ;
      void SetObjRef(SUPERV::CNode_var aNode) {
                     _ComputingNode->SetObjRef( aNode ) ; } ;

      CNode_Impl * ObjImpl() const { return _ComputingNode->ObjImpl() ; } ;
      void SetObjImpl( CNode_Impl * aGraph ) {
                       _ComputingNode->SetObjImpl( aGraph ) ; } ;

      const SALOME_ModuleCatalog::Service * GetService() const {
            return _ComputingNode->GetService() ; } ;

      SUPERV::SDate FirstCreation() const {
                    return _ComputingNode->FirstCreation() ; } ;
      SUPERV::SDate LastModification() const {
                    return _ComputingNode->LastModification() ; } ;
      char* Author() const {
//JR 17.02.2005 Memory Leak            return my_strdup( _ComputingNode->Author() ) ; }
            return _ComputingNode->Author() ; }
      char* EditorRelease() const {
//JR 17.02.2005 Memory Leak            return my_strdup( _ComputingNode->EditorRelease() ) ; } ;
            return _ComputingNode->EditorRelease() ; } ;
      char * Comment() const {
//JR 17.02.2005 Memory Leak             return my_strdup( _ComputingNode->Comment() ) ; } ;
             return _ComputingNode->Comment() ; } ;
      bool Author( const char * anAuthor ) {
           return _ComputingNode->Author( anAuthor ) ; } ;
      bool EditorRelease( const char* anEditorRelease ) {
           return _ComputingNode->EditorRelease( anEditorRelease ) ; } ;
      bool Comment( const char *c ) {
           return _ComputingNode->Comment( c ) ; };

      GraphBase::SNode * GetInfo() {
                         return _ComputingNode->GetInfo() ; } ;

      GraphBase::InPort * AddInPort( const char * InputParameterName ,
                                     const char * InputParameterType ,
                                     const SUPERV::KindOfPort aKindOfPort ) {
                          return _ComputingNode->AddInPort( InputParameterName ,
                                                            InputParameterType ,
                                                            aKindOfPort ) ; } ;
      GraphBase::OutPort * AddOutPort( const char * OutputParameterName ,
                                       const char * OutputParameterType ,
                                       const SUPERV::KindOfPort aKindOfPort ) {
                           return _ComputingNode->AddOutPort( OutputParameterName ,
                                                              OutputParameterType ,
                                                              aKindOfPort ) ; } ;
      int IncrDataStreamInPorts() {
        return _ComputingNode->IncrDataStreamInPorts() ;
      } ;
      int IncrDataStreamOutPorts() {
        return _ComputingNode->IncrDataStreamOutPorts() ;
      } ;

      void DelInPort( const char * InputParameterName ) {
           _ComputingNode->DelInPort( InputParameterName ) ; } ;
      void DelOutPort( const char * OutputParameterName ) {
           _ComputingNode->DelOutPort( OutputParameterName ) ; } ;

      bool IsLinked(const char * ToServiceParameterName, bool isInput ) {
           return _ComputingNode->IsLinked( ToServiceParameterName, isInput ) ; } ; // mkr : PAL8060
      bool HasInput(const char * ToServiceParameterName ) {
           return _ComputingNode->HasInput( ToServiceParameterName ) ; } ;

//      void InOutPort( GraphBase::InPort * InputPort ,
//                      GraphBase::OutPort * OutputPort ) {
//           return _ComputingNode->InOutPort( InputPort , OutputPort ) ; } ;

      const int GetNodeInPortsSize() const {
                return _ComputingNode->GetNodeInPortsSize() ; } ;
      const GraphBase::InPort *GetNodeInPort(int i) const {
                              return _ComputingNode->GetNodeInPort( i ) ; } ;
      GraphBase::InPort *GetChangeNodeInPort(int i) const {
                        return _ComputingNode->GetChangeNodeInPort( i ) ; } ;
      const int GetNodeOutPortsSize() const {
                return _ComputingNode->GetNodeOutPortsSize() ; } ;
      const GraphBase::OutPort *GetNodeOutPort(int i) const {
                               return _ComputingNode->GetNodeOutPort( i ) ; } ;
      GraphBase::OutPort *GetChangeNodeOutPort(int i) const {
                         return _ComputingNode->GetChangeNodeOutPort( i ) ; } ;

      const GraphBase::InPort *GetInPort( const char *name ) {
            return _ComputingNode->GetInPort( name ) ; } ;
      const GraphBase::OutPort *GetOutPort( const char *name ) {
            return _ComputingNode->GetOutPort( name ) ; } ;
      GraphBase::InPort *GetChangeInPort( const char *name ) {
                        return _ComputingNode->GetChangeInPort( name ) ; } ;
      GraphBase::OutPort *GetChangeOutPort( const char *name ) {
                         return _ComputingNode->GetChangeOutPort( name ) ; } ;

      void SetPythonFunction( const char * aFuncName ,
                              const SUPERV::ListOfStrings & aPythonFunction ) {
           InLineNode()->SetPythonFunction( aFuncName , aPythonFunction ) ; }
      const SUPERV::ListOfStrings * PythonFunction() const {
            return InLineNode()->PythonFunction() ; } ;
      char * PyFuncName() const {
             return InLineNode()->PyFuncName() ; } ;

      void SetPyMorePythonFunction( const char * aMoreName ,
                                    const SUPERV::ListOfStrings & aMorePythonFunction ) {
           _LoopNode->SetMorePythonFunction( aMoreName , aMorePythonFunction ) ; }
      SUPERV::ListOfStrings * PyMorePythonFunction() const {
                    return _LoopNode->MorePythonFunction() ; } ;
      char * PyMoreName() {
             return _LoopNode->PyMoreName() ; };

      void SetPyNextPythonFunction( const char * aNextName ,
                               const SUPERV::ListOfStrings & aNextPythonFunction ) {
           _LoopNode->SetNextPythonFunction( aNextName , aNextPythonFunction ) ; }
      SUPERV::ListOfStrings * PyNextPythonFunction() const {
                    return _LoopNode->NextPythonFunction() ; } ;
      char * PyNextName() {
             return _LoopNode->PyNextName() ; } ;

      void CoupledNode( GraphBase::InLineNode * aCoupledNode ) {
           _GOTONode->CoupledNode( aCoupledNode ) ; } ;
      GraphBase::InLineNode * CoupledNode() {
                              return _GOTONode->CoupledNode() ; } ;

      int SubGraph() {
          return _ComputingNode->SubGraph() ; } ;
      int SubStreamGraph() {
          return _ComputingNode->SubStreamGraph() ; } ;

      char * ComponentName() const {
             return _FactoryNode->ComponentName() ; } ;
      char * InterfaceName() const {
             return _FactoryNode->InterfaceName() ; } ;
      char * Computer() const {
             return _FactoryNode->Computer() ; } ;

      bool ComponentName( const char * aComponentName ) {
           return _FactoryNode->ComponentName( aComponentName ) ; } ;
      bool InterfaceName( const char * anInterfaceName ) {
           return _FactoryNode->InterfaceName( anInterfaceName ) ; } ;
      bool Computer( const char *c ) {
           return _FactoryNode->Computer( c ) ; } ;

      void Coordinates( const int X , const int Y ) {
           _ComputingNode->Coordinates( X , Y ) ; } ;
      int XCoordinate () {
          return _ComputingNode->XCoordinate () ; } ;
      int YCoordinate () {
          return _ComputingNode->YCoordinate () ; } ;
  };
  

};

#endif

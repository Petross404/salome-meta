//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowEditor_OutNode.hxx
//  Module : SUPERV

#ifndef _DATAFLOWEDITOR_OUTNODE_HXX
#define _DATAFLOWEDITOR_OUTNODE_HXX

#include "DataFlowBase_StreamGraph.hxx"

#include "DataFlowEditor_InNode.hxx"

namespace GraphEditor {

  class OutNode : public GraphBase::Base {
    
    private :

      GraphBase::StreamGraph * _StreamGraph ;
      GraphBase::Graph       * _Graph ;

      int  _Graph_prof_debug ;

      bool _Imported ;

      bool _Valid ;

      bool _Executable ;

      CORBA::ORB_ptr _Orb;

//      bool Valid(bool kLoopSwitch = true ) ;
      bool Valid() ;
      bool Executable() ;

      bool LoadNodes( map< string , int > & aMapOfNodes ,
                      const GraphBase::ListOfSNodes &aNodes ) ;
      bool LoadLinks( map< string , int > & aMapOfNodes ,
                      const GraphBase::ListOfSLinks &aLinks ) ;
      bool LoadDatas( map< string , int > & aMapOfNodes ,
                      const GraphBase::ListOfSLinks &aDatas ) ;

//      bool GraphEditor::OutNode::LinkSaveXML( ostream &f , char *Tabs ,
      bool LinkSaveXML( QDomDocument & Graph , QDomElement & link ,
                                              GraphBase::SLink aLink ,
                                              bool wdata ) const ;
//      bool SaveXML(ostream &f ) ;QDomDocument & Graph 
      bool SaveXML( ostream & f , QDomDocument & Graph ,
                    bool aSuperGraph , QDomElement & supergraph ) ;

      bool LinkSavePY( ostream &f ,
                                             const char *aGraphName ,
                                             GraphBase::SLink aLink ,
                                             bool fromparam ,
                                             bool toparam ,
                                             bool wdata ) const;
      void DateModification() ;

      // asv 20.09.04 added from GraphExecutor::FiniteStateMachine class (removed from there)
//      static map< string , GraphBase::Graph * > _MapOfGraphs ;
//      static map< string , int >                _MapOfGraphNames ;
    
//      GraphBase::Graph * MapGraph( const char * aGraphName ) ;
//      bool MapGraph( GraphBase::Graph * aGraph , const char * aGraphName ) ;
//      void EraseGraph( const char * aGraphName ) ;
//      bool GraphName( const char * aGraphName ) ;
//      string GraphInstanceName( const char * aGraphName ) ;
      // end added from FiniteStateMachine


    public:

      OutNode();
      OutNode( CORBA::ORB_ptr ORB,
               SALOME_NamingService* ptrNamingService ,
               const char *DataFlowName ,
               const char * DebugFileName ,
               const SUPERV::KindOfNode aKindOfNode );
      OutNode( CORBA::ORB_ptr ORB,
               SALOME_NamingService* ptrNamingService ,
               const SALOME_ModuleCatalog::Service& DataFlowService ,
               const char *DataFlowComponentName ,
               const char *DataFlowInterfaceName ,
               const char *DataFlowName ,
               const SUPERV::KindOfNode DataFlowkind ,
               const SUPERV::SDate DataFlowFirstCreation ,
               const SUPERV::SDate DataFlowLastModification ,
               const char * DataFlowEditorRelease ,
               const char * DataFlowAuthor ,
               const char * DataFlowComputer ,
               const char * DataFlowComment ,
               const char * DebugFileName ) ;
      virtual ~OutNode();

      bool Name( const char * aName ) ;

      void Set_prof_debug( CORBA::ORB_ptr ORB , const char * DebugFileName ) ;
      GraphBase::StreamGraph * StreamGraph() {
                               return _StreamGraph ; } ;
      GraphBase::StreamGraph * StreamGraph() const {
                               return _StreamGraph ; } ;
      GraphBase::Graph * Graph() {
                         return _Graph ; } ;
      const GraphBase::Graph * Graph() const {
                               return _Graph ; } ;

      bool LoadDataFlow( const GraphBase::SGraph *aDataFlow ) ;
      bool LoadXml( const char* myFileName , GraphBase::ListOfSGraphs & aListOfDataFlows ) ;
//      bool LoadXml( const char* myFileName ) ;
      bool LoadInfo( const GraphBase::SNode &aDataFlowInfo ) ;

      bool SaveXml(const char* myFileName ) ;

      bool SavePy(const char* myFileName ) ;
      bool SavePY(ostream &f , bool importSuperV ) ;


// get all DataFlow informations (for a .XML file) :
      GraphBase::ListOfSGraphs * GetDataFlows( GraphBase::ListOfSGraphs * aListOfDataFlows ) ;

//    void DateModification() ;

      GraphEditor::InNode * AddNode(
                        const SALOME_ModuleCatalog::Service& NodeService ,
                        GraphBase::ListOfFuncName aFuncName ,
                        GraphBase::ListOfPythonFunctions aPythonFunction ,
                        const char* NodeComponentName ,
                        const char* NodeInterfaceName ,
                        const char* NodeName ,
                        const SUPERV::KindOfNode NodeKindOfNode ,
                        const SUPERV::SDate NodeFirstCreation ,
                        const SUPERV::SDate NodeLastModification ,
                        const char * NodeEditorRelease ,
                        const char * NodeAuthor ,
                        const char * NodeComputer ,
                        const char * NodeComment ,
                        const int NodeX ,
                        const int NodeY ) ;
      GraphEditor::InNode * GetNode( const char* NodeName ) {
               const GraphBase::Graph::ComputingNode * aNode = _Graph->GetGraphNode( NodeName ) ;
               if ( aNode ) {
                 return (GraphEditor::InNode * ) (aNode->GetInNode()) ;
               }
               else {
                 return (GraphEditor::InNode * ) NULL ;
               } } ;
      bool RemoveNode( const char* NodeName ) {
           DateModification() ;
           _Valid = false ;
           return _Graph->RemoveNode( NodeName ) ; } ;
      bool ReNameNode( const char* OldNodeName ,
                       const char* NewNodeName ) {
//PAL9048 JR Debug : a node may not have the same name as the graph
           if ( strcmp( Graph()->Name() , NewNodeName ) ) {
             DateModification() ;
             _Valid = false ;
             return _Graph->ReNameNode( OldNodeName , NewNodeName ) ;
	   }
           cdebug << "Editor::OutNode::ReNameNode ERROR "  << NewNodeName << " already exists"
                  << endl ;
           return false ; } ;

      void Coordinates( const int X , const int Y ) {
           return _Graph->Coordinates( X , Y ) ; } ;
      const int XCoordinate() {
           return _Graph->XCoordinate() ; } ;
      const int YCoordinate() {
           return _Graph->YCoordinate() ; } ;
      void Coordinates( const char* NodeName , const int X , const int Y ) ;
      const int XCoordinate( const char* NodeName ) ;
      const int YCoordinate( const char* NodeName ) ;

      const GraphBase::InPort *GetInPort( const char * InPortName ) {
            return _Graph->GetInPort( InPortName ) ; } ;
      const GraphBase::OutPort *GetOutPort( const char * OutPortName ) {
            return _Graph->GetOutPort( OutPortName ) ; } ;
      GraphBase::InPort *GetChangeInPort( const char * InPortName ) {
            return _Graph->GetChangeInPort( InPortName ) ; } ;
      GraphBase::OutPort *GetChangeOutPort( const char * OutPortName ) {
            return _Graph->GetChangeOutPort( OutPortName ) ; } ;

      bool HasInput(const char * ToServiceParameterName ) {
           return _Graph->HasInput( ToServiceParameterName ) ;
	 }

      bool AddLink( const char* FromNodeName ,
                    const char* FromServiceParameterName ,
	            const char* ToNodeName ,
                    const char* ToServiceParameterName ) {
//                    , const CORBA::Any aValue ) {
           DateModification() ;
           _Valid = false ;
           return _Graph->AddLink( FromNodeName , FromServiceParameterName ,
                                   ToNodeName , ToServiceParameterName ) ; } ;
//                                   , aValue ) ; } ;

      bool RemoveLink( const char* FromNodeName ,
                       const char* FromServiceParameterName ,
                       const char* ToNodeName ,
                       const char* ToServiceParameterName ) {
           bool RetVal = _Graph->RemoveLink( FromNodeName ,
                                             FromServiceParameterName , 
                                             ToNodeName ,
                                             ToServiceParameterName ) ;
           if ( RetVal )
             DateModification() ;
           _Valid = false ;
           return RetVal ; } ;

      bool GetLink(const char* ToNodeName ,
                   const char* ToServiceParameterName ,
                   char** FromNodeName ,
                   char** FromServiceParameterName ) {
           return _Graph->GetLink( ToNodeName ,
                                   ToServiceParameterName ,
                                   FromNodeName ,
			           FromServiceParameterName ) ; } ;

      bool AddLinkCoord( const char* FromNodeName ,
                         const char* FromServiceParameterName ,
                         const char* ToNodeName ,
                         const char* ToServiceParameterName ,
                         const int nXY ,
                         const int* X ,
                         const int* Y ) ;
      bool AddLinkCoord( const char* FromNodeName ,
                         const char* FromServiceParameterName ,
                         const char* ToNodeName ,
                         const char* ToServiceParameterName ,
                         const int index ,
                         const int X ,
                         const int Y ) ;
      bool ChangeLinkCoord( const char* FromNodeName ,
                            const char* FromServiceParameterName ,
                            const char* ToNodeName ,
                            const char* ToServiceParameterName ,
                            const int index ,
                            const int X ,
                            const int Y ) ;
      bool RemoveLinkCoord( const char* FromNodeName ,
                            const char* FromServiceParameterName ,
                            const char* ToNodeName ,
                            const char* ToServiceParameterName ,
                            const int index ) ;
      int GetLinkCoordSize( const char* FromNodeName ,
                            const char* FromServiceParameterName ,
                            const char* ToNodeName ,
                            const char* ToServiceParameterName ) ;
      bool GetLinkCoord( const char* FromNodeName ,
                         const char* FromServiceParameterName ,
                         const char* ToNodeName ,
                         const char* ToServiceParameterName ,
                         int *X , int *Y ) ;
      bool GetLinkCoord( const char* FromNodeName ,
                         const char* FromServiceParameterName ,
                         const char* ToNodeName ,
                         const char* ToServiceParameterName ,
                         const int index , CORBA::Long &X , CORBA::Long &Y ) ;


//      bool IsValid(bool kLoopSwitch = true ) {
      bool IsValid( ) {
           if ( !_Valid )
//             Valid( kLoopSwitch ) ;
             Valid() ;
           return _Valid ; } ;
      bool IsNotValid() const {
           return !_Valid ; } ;
      bool UnValid() ;

      bool IsExecutable() {
           cdebug_in << "Editor::OutNode::IsExecutable() " << " _Valid " << _Valid
                     << " _Executable " << _Executable << endl;
           if ( !_Valid )
             Valid() ;
           if ( _Valid ) {
             if ( !_Executable )
               Executable() ;
	   }
           else
             _Executable = false ;
           cdebug_out << "Editor::OutNode::IsExecutable() " << " _Valid " << _Valid
                      << " _Executable " << _Executable << endl ;
           return _Executable ; } ;
      bool IsNotExecutable() const {
           return !_Executable ; } ;

      // iterate through ALL links (OutPort-InPort pairs) and check if their types are 
      // compatible - IsCompatible(type1, type2).  
      // Returns true if all are compatible.   
      bool IsLinksCompatible();

      // Returns true if an out-port of type "OutPortType" can be bound with 
      // in-port of type "InPortType".  Called from IsLinksCompatible() and Link_Impl::IsValid().
      bool IsCompatible( const char* OutPortType, const char* InPortType ) const;

//JR 30.03.2005      const CORBA::Any *GetInData( const char *ToNodeName ,
      const CORBA::Any GetInData( const char *ToNodeName ,
                                  const char *ToParameterName ) ;
//JR 30.03.2005      const CORBA::Any *GetOutData( const char *FromNodeName ,
      const CORBA::Any GetOutData( const char *FromNodeName ,
                                   const char *FromParameterName ) ;
  } ;

};

ostream & operator << (ostream &,const GraphEditor::OutNode & G);
ostream & operator << (ostream &,const SUPERV::SDate &);

#endif




//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowEditor_DataFlow.cxx
//  Module : SUPERV

using namespace std;
#include "DataFlowEditor_DataFlow.hxx"

// Implementation de la classe GraphEditor::Graph

GraphEditor::DataFlow::DataFlow() :
  OutNode() {
  cdebug_in << "GraphEditor::DataFlow::DataFlowEditor()" << endl;

  _theNamingService = NULL ;
  _DataFlowExecutor = NULL ;
  _ReadOnly  = false ;
  _Executing = false ;

  cdebug_out << "GraphEditor::DataFlow::DataFlowEditor()" << endl;
}

//extern ostream * fdebug ;

GraphEditor::DataFlow::DataFlow( CORBA::ORB_ptr ORB,
                                 SALOME_NamingService* ptrNamingService ,
                                 const char *DataFlowName ,
                                 const char * DebugFileName ,
                                 const SUPERV::KindOfNode aKindOfNode ) :
  OutNode( ORB, ptrNamingService , DataFlowName , DebugFileName , aKindOfNode ) {
//  cout << "GraphEditor::DataFlow::DataFlow(" ;
  cdebug_in << "GraphEditor::DataFlow::DataFlow(" ;
  if ( DataFlowName ) {
//    cout << DataFlowName << " , " << DebugFileName ;
    cdebug << DataFlowName << " , " << DebugFileName;
  }
//  cout << ")" << endl;
  cdebug << ")" << endl;

  _theNamingService = ptrNamingService ;
  _DataFlowExecutor = NULL ;
  _ReadOnly  = false ;
  _Executing = false ;

  cdebug_out << "GraphEditor::DataFlow::DataFlow" << endl;
//  fdebug = new ofstream( DebugFileName ); // GraphBase::Base::_fdebug ;
}

GraphEditor::DataFlow::DataFlow(
                     CORBA::ORB_ptr ORB,
                     SALOME_NamingService* ptrNamingService ,
                     const SALOME_ModuleCatalog::Service& DataFlowService ,
                     const char *DataFlowComponentName ,
                     const char *DataFlowInterfaceName ,
                     const char *DataFlowName ,
                     const SUPERV::KindOfNode DataFlowkind ,
                     const SUPERV::SDate DataFlowFirstCreation ,
                     const SUPERV::SDate DataFlowLastModification ,
                     const char * DataFlowEditorRelease ,
                     const char * DataFlowAuthor ,
                     const char * DataFlowComputer ,
                     const char * DataFlowComment ,
                     const char * DebugFileName ) :
     OutNode( ORB, ptrNamingService , DataFlowService , DataFlowComponentName ,
              DataFlowInterfaceName , DataFlowName , DataFlowkind ,
              DataFlowFirstCreation , DataFlowLastModification  ,
              DataFlowEditorRelease , DataFlowAuthor ,
              DataFlowComputer , DataFlowComment , DebugFileName ) {
  cdebug_in << "GraphEditor::DataFlow::DataFlow(" << DataFlowName << ")" << endl;

  _theNamingService = ptrNamingService ;
  _DataFlowExecutor = NULL ;
  _ReadOnly  = false ;
  _Executing = false ;

  cdebug_out << "GraphEditor::DataFlow::DataFlow" << endl;
} ;

GraphEditor::DataFlow::~DataFlow() {
//  delete _DataFlowNode ;
//  delete _DataFlowDatas ;
//  delete _GT ;
}



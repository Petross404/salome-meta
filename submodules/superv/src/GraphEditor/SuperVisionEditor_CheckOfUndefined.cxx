//  SUPERV GraphEditor : contains classes that permit edition of graphs
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SuperVisionEditor_CheckOfUndefined.cxx
//  Module : SUPERV

using namespace std;
#include <iostream>
#include <fstream>
#include <unistd.h>

#include "DataFlowBase_Base.hxx"
#include "DataFlowBase_Graph.hxx"
#include "DataFlowEditor_InNode.hxx"
#include "DataFlowEditor_OutNode.hxx"
#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowEditor_DataFlow.lxx"

int _ArgC ;
char ** _ArgV ;

int main(int argc, char **argv) {

  return 1;
}


//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowExecutor_DataFlow.hxx
//  Module : SUPERV

#ifndef _DATAFLOWEXECUTOR_DATAFLOW_HXX
#define _DATAFLOWEXECUTOR_DATAFLOW_HXX

#include "DataFlowExecutor_OutNode.hxx"

namespace GraphExecutor {

  class DataFlow : public GraphExecutor::OutNode {
    
    private :

      SALOME_NamingService* _theNamingService ;

    public:

      DataFlow();
      DataFlow( CORBA::ORB_ptr ORB, SALOME_NamingService* ptrNamingService ,
                const char * DataFlowName ,
                const char * DebugFileName ,
                const SUPERV::KindOfNode aKindOfNode );
      DataFlow( CORBA::ORB_ptr ORB, SALOME_NamingService* ptrNamingService ,
                const SALOME_ModuleCatalog::Service& DataFlowService ,
                const char *DataFlowComponentName ,
                const char *DataFlowInterfaceName ,
                const char *DataFlowName ,
                const SUPERV::KindOfNode DataFlowkind = SUPERV::ComputingNode ,
                const SUPERV::SDate DataFlowFirstCreation = SUPERV::SDate() ,
                const SUPERV::SDate DataFlowLastModification = SUPERV::SDate() ,
                const char * DataFlowEditorRelease = NULLSTRING ,
                const char * DataFlowAuthor = NULLSTRING ,
                const char * DataFlowComputer = NULLSTRING ,
                const char * DataFlowComment = NULLSTRING ,
                const char * DebugFileName = NULLSTRING ) ;
      virtual ~DataFlow();

      bool LoadDataFlow( const GraphBase::SGraph * aDataFlow ) ;
//      bool LoadXml( const char* myFileName ) ;

      const SALOME_ModuleCatalog::Service * NodeService( const char * NodeName ) ;

      bool AddInputData( const char * ToNodeName ,
                         const char * ToParameterName ,
                         const CORBA::Any aValue = CORBA::Any() ) ;
      bool ChangeInputData( const char * ToNodeName ,
                            const char * ToParameterName ,
                            const CORBA::Any aValue = CORBA::Any() ) ;

      bool InputOfAny( const char * ToServiceParameterName ,
                       const CORBA::Any & aValue ,
                       const bool SomeDataReady = true ) ;
      bool OutputOfAny( const char * aNodeName ,
                        const char * ToServiceParameterName ,
                        const CORBA::Any & aValue ) ;
      bool SetWaitingStates( const char * ToServiceParameterName ) ;

      bool IsValid() ;
      bool IsExecutable() ;

      bool Run( const bool AndSuspend ) ;

      long LastLevelDone() ;

      void State(GraphExecutor::AutomatonState aState ) ;
      SUPERV::GraphState State() ;
      SUPERV::GraphState State(const char * aNodeName ) ;
      SUPERV::GraphState State( const char * aNodeName ,
                                 const char * anOutServiceParameterName ) ;

      long Thread() ;
      long Thread(const char * aNodeName ) ;

      GraphExecutor::AutomatonState AutomatonState() ;
      GraphExecutor::AutomatonState AutomatonState(const char * aNodeName ) ;

      SUPERV::ControlState ControlState() ;
      SUPERV::ControlState ControlState(const char * aNodeName ) ;
      void ControlClear() ;
      void ControlClear(const char * aNodeName ) ;

      bool Event( char ** aNodeName ,
                  SUPERV::GraphEvent & anEvent ,
                  SUPERV::GraphState & aState ,
                  bool WithWait = true ) ;
      bool EventW( char ** aNodeName ,
                   SUPERV::GraphEvent & anEvent ,
                   SUPERV::GraphState & aState ) ;
      long EventQSize() ;

      bool IsWaiting() ;
      bool IsReady() ;
      bool IsRunning() ;
      bool IsDone() ;
      bool IsSuspended() ;
      bool IsWaiting(const char * aNodeName ) ;
      bool IsReady(const char * aNodeName ) ;
      bool IsRunning(const char * aNodeName ) ;
      bool IsDone(const char * aNodeName ) ;
      bool IsSuspended(const char * aNodeName ) ;
      bool PortDone( const char * aNodeName ,
                     const char * anOutServiceParameterName ) ;

//JR 30.03.2005      const CORBA::Any *GetInData( const char * ToNodeName ,
      const CORBA::Any GetInData( const char * ToNodeName ,
                                  const char * ToParameterName ) ;
//JR 30.03.2005      const CORBA::Any *GetOutData( const char * FromNodeName ,
      const CORBA::Any GetOutData( const char * FromNodeName ,
                                   const char * FromParameterName ) ;

      long Threads() ;

      bool ReadyWait() ;
      bool RunningWait() ;
      bool DoneWait() ;
      bool SuspendedWait() ;
      bool ReadyWait(const char * aNodeName ) ;
      bool RunningWait(const char * aNodeName ) ;
      bool DoneWait(const char * aNodeName ) ;
      bool SuspendedWait(const char * aNodeName ) ;

      bool Ping(const char * aNodeName ) ;
      bool ContainerKill() ;
      bool ContainerKill(const char * aNodeName ) ;

      bool Kill() ;
      bool Kill(const char * aNodeName ) ;
      bool KillDone(const char * aNodeName ) ;
      bool Suspend() ;
      bool Suspend(const char * aNodeName ) ;
      bool SuspendDone() ;
      bool SuspendDone(const char * aNodeName ) ;
      bool Resume() ;
      bool Resume(const char * aNodeName ) ;
      bool Stop() ;
      bool Stop(const char * aNodeName ) ;

  };

};

#include "DataFlowExecutor_DataFlow.lxx"

ostream & operator << (ostream &,const GraphExecutor::DataFlow & G);
ostream & operator << (ostream &,const SUPERV::SDate &);

#endif




//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowExecutor_DataFlow.cxx
//  Module : SUPERV

using namespace std;
#include "DataFlowExecutor_DataFlow.hxx"
#include "DataFlowEditor_DataFlow.hxx"

// Implementation de la classe GraphExecutor::Graph

GraphExecutor::DataFlow::DataFlow() :
  OutNode() {
  cdebug_in << "GraphExecutor::DataFlow::DataFlowExecutor()" << endl;

  _theNamingService = NULL ;
  
  cdebug_out << "GraphExecutor::DataFlow::DataFlowExecutor()" << endl;
}

GraphExecutor::DataFlow::DataFlow( CORBA::ORB_ptr ORB,
				   SALOME_NamingService* ptrNamingService ,
                                   const char *DataFlowName ,
                                   const char * DebugFileName ,
                                   const SUPERV::KindOfNode aKindOfNode ) :
  OutNode( ORB, ptrNamingService , DataFlowName , DebugFileName , aKindOfNode ) {
  cdebug_in << "GraphExecutor::DataFlow::DataFlow(" ;
  if ( DataFlowName ) {
    cdebug << DataFlowName ;
  }
  cdebug << ")" << endl;

  _theNamingService = ptrNamingService ;

  cdebug_out << "GraphExecutor::DataFlow::DataFlow" << endl;
}

GraphExecutor::DataFlow::DataFlow( CORBA::ORB_ptr ORB,
		                   SALOME_NamingService* ptrNamingService ,
                                   const SALOME_ModuleCatalog::Service& DataFlowService ,
                                   const char *DataFlowComponentName ,
                                   const char *DataFlowInterfaceName ,
                                   const char *DataFlowName ,
                                   const SUPERV::KindOfNode DataFlowkind ,
                                   const SUPERV::SDate DataFlowFirstCreation ,
                                   const SUPERV::SDate DataFlowLastModification ,
                                   const char * DataFlowExecutorRelease ,
                                   const char * DataFlowAuthor ,
                                   const char * DataFlowComputer ,
                                   const char * DataFlowComment ,
                                   const char * DebugFileName ) :
     OutNode( ORB, ptrNamingService , DataFlowService , DataFlowComponentName ,
              DataFlowInterfaceName , DataFlowName , DataFlowkind ,
              DataFlowFirstCreation , DataFlowLastModification  ,
              DataFlowExecutorRelease , DataFlowAuthor ,
              DataFlowComputer , DataFlowComment , DebugFileName ) {
  cdebug_in << "GraphExecutor::DataFlow::DataFlow(" << DataFlowName << ")" << endl;

  _theNamingService = ptrNamingService ;

  cdebug_out << "GraphExecutor::DataFlow::DataFlow" << endl;
} ;

GraphExecutor::DataFlow::~DataFlow() {
//  delete _DataFlowNode ;
//  delete _DataFlowDatas ;
//  delete _GT ;
}

bool GraphExecutor::DataFlow::Ping( const char *aNodeName ) {
  cdebug_in << "GraphExecutor::DataFlow::Ping" << aNodeName << " )" << endl;
  bool RetVal = false ;
  if ( Graph()->GetGraphNode( aNodeName ) )
    RetVal = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode())->Ping() ;
  cdebug_out << "GraphExecutor::DataFlow::Ping" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::ContainerKill() {
  cdebug_in << "GraphExecutor::DataFlow::ContainerKill()" << endl;
  bool RetVal = GraphExecutor::OutNode::ContainerKill() ;
  cdebug_out << "GraphExecutor::DataFlow::ContainerKill()" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::ContainerKill( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::ContainerKill( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->ContainerKill() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::ContainerKill" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::InputOfAny( const char * ToServiceParameterName ,
                                          const CORBA::Any & aValue ,
                                          bool SomeDataReady ) {
  cdebug_in <<"GraphExecutor::DataFlow::InputOfAny( " << ToServiceParameterName
            << " SomeDataReady " << SomeDataReady << " )" << endl ;
  bool RetVal = false ;
  cdebug << pthread_self() << "GraphExecutor::DataFlow::InputOfAny " << Name() << "( "
         << ToServiceParameterName << " , aValue , SomeDataReady " << SomeDataReady << " ) "
         << endl ;
  if ( Graph()->GraphMacroLevel() ) {
    GraphBase::OutPort * anOutPort ;
    anOutPort = Graph()->GetChangeInDataNodePort( ToServiceParameterName ) ;
    cdebug << pthread_self() << "GraphExecutor::DataFlow::InputOfAny " << Name()
           << " " << State() << " " << ToServiceParameterName << " " << anOutPort->PortState()
           << " Done : " << anOutPort->PortDone() << endl ;
    RetVal = AddInputData( Name() , ToServiceParameterName , aValue ) ;
    anOutPort->PortState(  SUPERV::ReadyState ) ;
// There is only one port :
    anOutPort->ChangeInPorts( 0 )->PortState( SUPERV::ReadyState ) ;
    anOutPort->PortDone( true ) ;
// There is only one inport of a Node in an ReversedOutport of a graph :
    GraphExecutor::InNode * anInNode = (GraphExecutor::InNode * ) Graph()->GetChangeGraphNode( anOutPort->InPorts( 0 )->NodeName() )->GetInNode() ;
    cdebug << pthread_self() << "GraphExecutor::DataFlow::InputOfAny : " << anInNode->Name()
           << "->SendSomeDataReady( " << Name() << " ) for Port "
           << anOutPort->InPorts( 0 )->PortName() << " SomeDataReady " << SomeDataReady << endl ;
    if ( SomeDataReady ) {
      MESSAGE( "GraphExecutor::InputOfAny " << Name() << " SendSomeDataReady --> " << anInNode->Name()
               << " " << anInNode->State() << " " << anOutPort->InPorts( 0 )->PortName() ) ;
      int sts = anInNode->SendSomeDataReady( Name() ) ;
      cdebug << "GraphExecutor::DataFlow::InputOfAny " << anInNode->Name()
             << "->SendSomeDataReady( " << Name() << " ) sts " << sts << " State "
             << anInNode->State() << " IsReady " << anInNode->IsReady()
             << " SomeDataReady " << SomeDataReady << endl ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
      if ( sts && anInNode->HasAllDataReady() ) {
        cdebug << pthread_self() << "/" << anInNode->ThreadNo()
               << "GraphExecutor::DataFlow::InputOfAny : "
               << anInNode->Name() << "->SendEvent( GraphExecutor::ExecuteEvent ) "
               << " " << anInNode->Name() << "->HasAllDataReady() " << anInNode->HasAllDataReady()
               << " State " << anInNode->State() << endl ;
        anInNode->ThreadNo( 0 ) ;
        anInNode->CreateNewThread( true ) ;
        if ( !anInNode->SendEvent( GraphExecutor::AllDataReadyEvent ) ) { // ==> Ready to execute
//JR 06.05.2005 Debug PAL8624 RetroConception :
#if 0
        if ( res && anInNode->IsReady() ) {
          cdebug << pthread_self() << "/" << anInNode->ThreadNo() << "GraphExecutor::DataFlow::InputOfAny : "
                 << anInNode->Name() << "->SendEvent( GraphExecutor::ExecuteEvent ) "
                 << " " << anInNode->Name() << "->IsReady() " << anInNode->IsReady() << " State "
                 << anInNode->State() << endl ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//          if ( anInNode->IsLockedDataWait() ) {
//            cdebug << pthread_self() << "/" << anInNode->ThreadNo() << "GraphExecutor::DataFlow::InputOfAny : "
//                   << anInNode->Name() << " IsLockedDataWait() ==> UnLockDataWait" << endl ;
//            anInNode->UnLockDataWait() ;
//          }
          anInNode->ThreadNo( 0 ) ;
          anInNode->CreateNewThread( true ) ;
          anInNode->SendEvent( GraphExecutor::ExecuteEvent ) ;
//          State( GraphExecutor::ExecutingState ) ;
        }
        else {
          RetVal = false ;
#endif
          cdebug << pthread_self() << "/" << anInNode->ThreadNo()
                 << "GraphExecutor::DataFlow::InputOfAny : NotAllDataReady ERROR : "
                 << anInNode->Name() << "->SendEvent( GraphExecutor::ExecuteEvent ) "
                 << " " << anInNode->Name() << "->IsReady() " << anInNode->IsReady() << " State "
                 << anInNode->State() << endl ;
	}
      }
      else {
        cdebug << pthread_self() << "/" << anInNode->ThreadNo()
               << "GraphExecutor::DataFlow::InputOfAny : NotAllDataReady : "
               << anInNode->Name() << "->SendEvent( GraphExecutor::ExecuteEvent ) "
               << " " << anInNode->Name() << "->IsReady() " << anInNode->IsReady() << " State "
               << anInNode->State() << endl ;
      }
    }
    else {
      cdebug << "GraphExecutor::DataFlow::InputOfAny " << anInNode->Name()
             << "->SendSomeDataReady( " << Name() << " ) State "
             << anInNode->State() << " IsReady " << anInNode->IsReady()
             << " SomeDataReady " << SomeDataReady << endl ;
    }
  }
  else {
    cdebug << pthread_self() << "GraphExecutor::DataFlow::InputOfAny GraphMacroLevel "
           << Graph()->GraphMacroLevel() << " ERROR" << endl ;
    MESSAGE( "GraphExecutor::DataFlow::InputOfAny GraphMacroLevel " << Graph()->GraphMacroLevel()
             << " ERROR" ) ;
  }
  cdebug_out << pthread_self() << " GraphExecutor::DataFlow::InputOfAny " << RetVal << endl ;
  return RetVal ;
}

bool GraphExecutor::DataFlow::OutputOfAny( const char * aNodeName ,
                                           const char * ToServiceParameterName ,
                                           const CORBA::Any & aValue ) {
  cdebug_in << pthread_self() << "/" << ThreadNo() << "GraphExecutor::DataFlow::OutputOfAny( " << aNodeName
            << " , " << ToServiceParameterName
            << " , aValue ) from " << Name() << endl ;
  bool RetVal = false ;
  GraphBase::Graph * aMacroNode = (GraphBase::Graph * ) Graph()->GetChangeGraphNode( aNodeName ) ;
//  GraphExecutor::InNode * anInNode = (GraphExecutor::InNode * ) aMacroGraph->GetInNode() ;
  RetVal = Graph()->AddOutputData( aNodeName , ToServiceParameterName , aValue ) ;
  GraphBase::OutPort * anOutPort = aMacroNode->GetChangeOutPort( ToServiceParameterName ) ;
  int i ;
  for ( i = 0 ; i < aMacroNode->GetNodeOutPortsSize() ; i++ ) {
    cdebug << "Out" << i << " " << aMacroNode->GetNodeOutPort( i )->PortName() << " "
           << aMacroNode->GetChangeNodeOutPort( i )->PortState() << " Done="
           << aMacroNode->GetChangeNodeOutPort( i )->PortDone() << " " ;
    if ( GraphBase::Base::_prof_debug ) {
      aMacroNode->GetNodeOutPort( i )->StringValue( *GraphBase::Base::_fdebug ) ;
    }
    if ( aMacroNode->GetChangeNodeOutPort( i )->IsGate() ) {
      cdebug << " BoolValue " << aMacroNode->GetChangeNodeOutPort( i )->BoolValue() ;
    }
    cdebug << endl ;
  }
// Loop over Inports linked to that OutPort of the MacroNode
  for ( i = 0 ; i < anOutPort->InPortsSize() ; i++ ) {
    const char * ToNodeName = anOutPort->ChangeInPorts( i )->NodeName() ;
    const char * ToParameterName = anOutPort->ChangeInPorts( i )->PortName() ;
    if ( strcmp( ToNodeName , Name() ) ) {
      GraphBase::ComputingNode * aComputingNode = Graph()->GetChangeGraphNode( ToNodeName ) ;
      GraphExecutor::InNode * aLinkedNode = (GraphExecutor::InNode * ) aComputingNode->GetInNode() ;
      cdebug << pthread_self() << "/" << aLinkedNode->ThreadNo()
             << "GraphExecutor::DataFlow::OutputOfAny to Node "
             << ToNodeName << "(" << ToParameterName << ") from MacroNode " << aNodeName << endl ;
      int sts ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//      if ( aLinkedNode->IsLockedDataWait() ) {
//        cdebug << "GraphExecutor::DataFlow::OutputOfAny " << aLinkedNode->Name()
//               << " IsLockedDataWait --> UnLockDataWait" << endl ;
//      }
      aLinkedNode->LockDataReady() ;
      sts = aLinkedNode->SendSomeDataReady( (char * ) aNodeName ) ;
      cdebug << pthread_self() << "/" << aLinkedNode->ThreadNo()
             << "GraphExecutor::DataFlow::OutputOfAny " << aLinkedNode->Name()
             << "->SendSomeDataReady( " << aNodeName << " ) sts " << sts << " " << aLinkedNode->State() << endl ;
      if ( sts ) {
//        if ( aLinkedNode->State() == GraphExecutor::DataReadyState ) {
        if ( aLinkedNode->HasAllDataReady() ) {
          cdebug << pthread_self() << "/" << aLinkedNode->ThreadNo()
                 << "GraphExecutor::DataFlow::OutputOfAny SendEvent(ExecuteEvent) to "
                 << aLinkedNode->Name() << endl ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//          aLinkedNode->CreateNewThreadIf( true ) ;
//          aLinkedNode->UnLockDataWait() ;
          aLinkedNode->CreateNewThread( true ) ;
          aLinkedNode->UnLockDataReady() ;
          aLinkedNode->HasAllDataReady( false ) ;
//          aLinkedNode->SendEvent( GraphExecutor::ExecuteEvent ) ;
          aLinkedNode->SendEvent( GraphExecutor::AllDataReadyEvent ) ;
        }
        else {
          aLinkedNode->UnLockDataReady() ;
          cdebug << pthread_self() << "/" << aLinkedNode->ThreadNo()
                 << "GraphExecutor::DataFlow::OutputOfAny NO SendEvent(ExecuteEvent) to "
                 << aLinkedNode->Name() << endl ;
        }
      }
      else {
        aLinkedNode->UnLockDataReady() ;
      }
    }
    else if ( Graph()->GraphMacroLevel() != 0 ) {
      cdebug << pthread_self() << "/" << Graph()->CoupledNode()->ThreadNo()
             << "GraphExecutor::DataFlow::OutputOfAny to that graph "
             << ToNodeName << "(" << ToParameterName << ") " << State() << " " << Graph()->CoupledNodeName ()
             << " sended recursively to the MacroNode coupled to that graph" << endl ;
      Graph()->CoupledNode()->GraphEditor()->Executor()->OutputOfAny( Graph()->CoupledNodeName() ,
                                                            ToParameterName ,
//JR 30.03.2005                                                            *anOutPort->Value() ) ;
                                                            anOutPort->Value() ) ;
    }
    else {
      cdebug << pthread_self() << "GraphExecutor::DataFlow::OutputOfAny to Node "
             << ToNodeName << "(" << ToParameterName << ") from MacroNode " << aNodeName
             << ") ignored" << endl ;
    }
  }
  cdebug_out << pthread_self() << "/" << ThreadNo() << " GraphExecutor::DataFlow::OutputOfAny " << RetVal
             << endl ;
  return RetVal ;
}

//JR NPAL14110 09.02.2007 : SetWaitingStates was missing in SubGraph of MacroNode !...
bool GraphExecutor::DataFlow::SetWaitingStates( const char * ToServiceParameterName ) {
  cdebug_in << pthread_self() << "/" << ThreadNo() << "GraphExecutor::DataFlow::SetWaitingStates( "
            << ToServiceParameterName << " ) MacroGraph " << Name() << endl ;
  bool RetVal = true ;
  State( GraphExecutor::DataWaitingState ) ;
  Done( false ) ;
  GraphBase::OutPort * anOutPort ;
  anOutPort = Graph()->GetChangeInDataNodePort( ToServiceParameterName ) ;
  anOutPort->PortState(  SUPERV::WaitingState ) ;
  anOutPort->PortDone( false ) ;
  cdebug << pthread_self() << "GraphExecutor::DataFlow::SetWaitingStates " << Name()
         << " " << State() << " " << ToServiceParameterName << " " << anOutPort->PortState()
         << " Done : " << anOutPort->PortDone() << endl ;
  if ( ! anOutPort->IsGate() ) {
    GraphExecutor::InNode * anInNode = (GraphExecutor::InNode * ) Graph()->GetChangeGraphNode( anOutPort->InPorts( 0 )->NodeName() )->GetInNode() ;
    cdebug << pthread_self() << "GraphExecutor::DataFlow::SetWaitingStates : "
           << anInNode->Name() << "->SetWaitingStates( NULL ) for Port "
           << anOutPort->InPorts( 0 )->PortName() << endl ;
    anInNode->SetWaitingStates( NULL ) ;
  }
  cdebug_out << "GraphExecutor::DataFlow::SetWaitingStates() " << RetVal << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Kill() {
  cdebug_in << "GraphExecutor::DataFlow::Kill()" << endl;
  bool RetVal = GraphExecutor::OutNode::Kill() ;
  cdebug_out << "GraphExecutor::DataFlow::Kill() " << RetVal << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Kill( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::Kill( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->Kill() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::Kill" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::KillDone( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::KillDone( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->KillDone() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::KillDone" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Suspend() {
  cdebug_in << "GraphExecutor::DataFlow::Suspend()" << endl;
  bool RetVal = GraphExecutor::OutNode::Suspend() ;
  cdebug_out << "GraphExecutor::DataFlow::Suspend()" << RetVal << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Suspend( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::Suspend( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->Suspend() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::Suspend" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::SuspendDone() {
  cdebug_in << "GraphExecutor::DataFlow::SuspendDone()" << endl;
  bool RetVal = false ;
  cdebug << "Kill not yet implemented." << endl;
  cdebug_out << "GraphExecutor::DataFlow::SuspendDone()" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::SuspendDone( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::SuspendDone( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->SuspendDone() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::SuspendDone" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Resume() {
  cdebug_in << "GraphExecutor::DataFlow::Resume()" << endl;
  bool RetVal = GraphExecutor::OutNode::Resume() ;
  cdebug_out << "GraphExecutor::DataFlow::Resume()" << RetVal << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Resume( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::Resume( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->Resume() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::Resume" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Stop() {
  cdebug_in << "GraphExecutor::DataFlow::Stop()" << endl;
  bool RetVal = GraphExecutor::OutNode::Stop() ;
  cdebug_out << "GraphExecutor::DataFlow::Stop()" << endl;
  return RetVal ;
}

bool GraphExecutor::DataFlow::Stop( const char *aNodeName ) {
//  cdebug_in << "GraphExecutor::DataFlow::Stop( " << aNodeName << " )"<< endl;
  bool RetVal = false ;
  GraphExecutor::InNode * aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode()) ;
  if ( aNode ) {
    RetVal = aNode->Stop() ;
  }
//  cdebug_out << "GraphExecutor::DataFlow::Stop" << endl;
  return RetVal ;
}


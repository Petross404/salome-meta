//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_DynInvoke.cxx
//  Author : Marc Tajchman, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include <stdarg.h>
#include <map>

#include "DataFlowExecutor_InNode.hxx"

using namespace CORBA ;


#define DynInvokeTrace 0
void GraphExecutor::InNode::DynInvoke(Engines::Component_ptr objComponent ,
		                      const char *method , 
		                      ServicesAnyData * inParams , int nInParams ,
		                      ServicesAnyData * outParams , int nOutParams ) {
  Request_var req = objComponent->_request( method ) ;
  const char *ArgName ;
  int Type ;

  NVList_ptr arguments = req->arguments() ;

  int i ;

  int n_in  = nInParams ;
  int n_out = nOutParams ;

#if DynInvokeTrace
  cdebug << ThreadNo() << "InNode::DynInvoke " << Name() << " method " << method
         << " objComponent " << objComponent << " " << ObjectToString( objComponent )
         << endl ;
#endif
//  MESSAGE( aComponent << "->" << method ) ;
  for ( i = 0 ; i < n_in ; i++ ) {
    CORBA::Any & data = inParams[i].Value ;
    ArgName           = inParams[i].Name.c_str() ;
    Type              = data.type()->kind() ;
    arguments->add_value( ArgName , data , CORBA::ARG_IN ) ;
#if DynInvokeTrace
    switch ( Type ) {
    case CORBA::tk_string : {
      const char * t ;
      data >>= t ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << t << " (string)" 
             << endl ;
      break ;
    }
    case CORBA::tk_boolean : {
      bool b ;
      data >>= (CORBA::Any::to_boolean ) b ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << b << " (boolean)" << endl ;
      break ;
    }
    case CORBA::tk_char : {
      unsigned char c ;
      data >>= (CORBA::Any::to_char ) c ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << (int ) c << " (char)" << endl ;
      break ;
    }
    case CORBA::tk_short : {
      CORBA::Short s ;
      data >>= s ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << s << " (short)" << endl ;
      break ;
    }
    case CORBA::tk_long : {
      CORBA::Long l ;
      data >>= l ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << l << " (CORBA::Long)" << endl ;
      break ;
    }
    case CORBA::tk_float : {
      CORBA::Float f ;
      data >>= f ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << f << " (float)" << endl ;
      break ;
    }
    case CORBA::tk_double : {
      CORBA::Double d ;
      data >>= d ;
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << d << " (double)" << endl ;
      break ;
    }
    case CORBA::tk_objref : {
      CORBA::Object_ptr obj ;
      char * retstr = "Catched ERROR";
      try {
        data >>= obj ;
        retstr = ObjectToString( obj ) ;
      }
      catch( ... ) {
      }
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << retstr << "(object reference)" << endl ;
      break ;
    }
    default : {
      cdebug << "ArgIn" << i << " : " << ArgName << " Value " << "(other ERROR) kind " << Type << endl ;
    }
    }
    cdebug << "" << endl ;
#endif
  }

  for ( i = 0 ; i < n_out ; i++ ) {
    CORBA::Any & data = outParams[i].Value ;
    ArgName           = outParams[i].Name.c_str() ;
    Type              = data.type()->kind() ;
    arguments->add_value( ArgName , data , CORBA::ARG_OUT ) ;
#if DynInvokeTrace
    switch ( Type ) {
    case CORBA::tk_string : {
      cont char * t ;
      data >>= t ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << t << " (string)" << endl ;
      break ;
    }
    case CORBA::tk_boolean : {
      bool b ;
      data >>= (CORBA::Any::to_boolean ) b ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << b << " (boolean)" << endl ;
      break ;
    }
    case CORBA::tk_char : {
      unsigned char c ;
      data >>= (CORBA::Any::to_char ) c ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << (int ) c << " (char)" << endl ;
      break ;
    }
    case CORBA::tk_short : {
      CORBA::Short s ;
      data >>= s ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << s << " (short)" << endl ;
      break ;
    }
    case CORBA::tk_long : {
      CORBA::Long l ;
      data >>= l ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << l << " (CORBA::Long)" << endl ;
      break ;
    }
    case CORBA::tk_float : {
      CORBA::Float f ;
      data >>= f ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << f << " (float)" << endl ;
      break ;
    }
    case CORBA::tk_double : {
      CORBA::Double d ;
      data >>= d ;
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << d << " (double)" << endl ;
      break ;
    }
    case CORBA::tk_objref : {
      CORBA::Object_ptr obj ;
      char * retstr = "Catched ERROR";
      try {
        data >>= obj ;
        retstr = ObjectToString( obj ) ;
      }
      catch( ... ) {
      }
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << retstr << "(object reference)" << endl ;
      break ;
    }
    default : {
      cdebug << "ArgOut" << i << " : " << ArgName << " Value " << "(other ERROR) kind " << Type << endl ;
    }
    }
    cdebug << "" << endl ;
#endif
  }

  // IPAL9273, 9369, 9731 : replace blocking function invoke() with non blocking send_deferred()
  // to provide the correct process of killing dataflow execution
  req->send_deferred() ;
  while( !req->poll_response() ) {}
  req->get_response();
  
  if( req->env()->exception() ) {
    req->env()->exception()->_raise() ;
    return ; // pas utile ?
  }

  for ( i = 0 ; i < n_out ; i++ ) {

      outParams[i].Value = *( arguments->item( i + n_in )->value() ) ;
  }

  return;

}

void GraphExecutor::InNode::DynInvoke( Engines::Component_ptr obj ,
		                       const char *method , 
		                       const char * aGraphName ,
	                               const char * aNodeName ) {
  Request_var req = obj->_request( method ) ;

  NVList_ptr arguments =req->arguments() ;

  CORBA::Any graph ;
  graph <<= aGraphName ;
  arguments->add_value( "aGraphName" , graph , CORBA::ARG_IN ) ;
  CORBA::Any node ;
  node <<= aNodeName ;
  arguments->add_value( "aNodeName" , node , CORBA::ARG_IN ) ;

  req->invoke() ;

  if( req->env()->exception() ) {
    req->env()->exception()->_raise();
  }
  return;

}

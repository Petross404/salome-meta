//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_FiniteStateMachine.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWEXECUTOR_FINITESTATEMACHINE
#define _DATAFLOWEXECUTOR_FINITESTATEMACHINE

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>

#include <Python.h>

#include <string>
#include <map>

#include "DataFlowBase_Base.hxx"

#include <SALOMEconfig.h>
//#include CORBA_SERVER_HEADER(SUPERV)
#include <SALOME_LifeCycleCORBA.hxx>

class Graph_Impl ;

namespace GraphBase {
  class Graph ;
}

namespace GraphExecutor {

//The AutomatonState is an internal state of the automaton of the Executor
  enum AutomatonState { UnKnownState ,
                        DataUndefState ,
                        DataWaitingState ,
                        DataReadyState ,
                        SuspendedReadyState,
                        SuspendedReadyToResumeState ,
                        ResumedReadyState ,
                        KilledReadyState ,
                        StoppedReadyState ,
                        ExecutingState ,
                        SuspendedExecutingState ,
                        ResumedExecutingState ,
                        KilledExecutingState ,
                        StoppedExecutingState ,
                        SuccessedExecutingState ,
                        ErroredExecutingState ,
                        SuspendedSuccessedState ,
                        SuspendedErroredState,
                        ResumedSuccessedState ,
                        ResumedErroredState ,
		        KilledSuccessedState ,
                        KilledErroredState ,
		        StoppedSuccessedState ,
                        StoppedErroredState ,
                        SuccessedState ,
                        ErroredState ,
                        SuspendedState ,
                        KilledState ,
                        StoppedState ,
                        SuspendedSuccessedToReStartState ,
                        SuspendedErroredToReStartState ,
                        ReStartedState ,
			LoadingState,
                        NumberOfAutomatonStates } ;

  enum NodeEvent { UndefinedEvent , 
                   NewThreadEvent ,        
                   SuspendEvent , 
                   ToResumeEvent  , 
                   ResumeEvent ,
                   KillEvent , 
                   StopEvent ,
                   ExecuteEvent , 
                   SuccessEvent , 
                   ErrorEvent ,
                   ToReStartEvent , 
                   ReStartEvent , 
                   ReStartAndSuspendEvent ,
                   EndExecuteEvent ,
                   NoDataReadyEvent , 
                   SomeDataReadyEvent ,
                   NotAllDataReadyEvent , 
                   AllDataReadyEvent ,
                   ReadyEvent , 
                   SuspendedReadyEvent ,
                   ResumedReadyEvent ,
                   KilledReadyEvent , 
                   StoppedReadyEvent ,
                   ExecutingEvent , 
                   SuspendedExecutingEvent ,
                   ResumedExecutingEvent , 
                   KilledExecutingEvent ,
                   StoppedExecutingEvent ,
                   SuccessedExecutingEvent , 
                   ErroredExecutingEvent ,
                   SuspendedSuccessedEvent , 
                   SuspendedErroredEvent ,
                   ResumedSuccessedEvent , 
                   ResumedErroredEvent ,
                   KilledEvent , 
                   StoppedEvent ,
                   ReStartedEvent , 
                   ReStartedAndSuspendEvent ,
                   NumberOfEvents } ;

  enum StateEventAction { ErrorAction ,
                          VoidAction ,
                          executeAction , // +- pthread_create
                          ExecuteAction , // +- pthread_create
                          DataWaiting_SomeDataReadyAction ,
                          DataUndef_NotAllDataReadyAction ,
                          DataUndef_AllDataReadyAction ,
                          DataReady_SuspendAction ,
                          SuspendedReady_ResumeAction ,
                          DataReady_KillAction ,
                          DataReady_StopAction ,
                          DataReady_ExecuteAction ,
                          Executing_SuspendAction ,
                          SuspendedExecuting_ResumeAction ,
                          Executing_KillAction ,
                          Executing_StopAction ,
                          Executing_SuccessAction ,
//                          Executing_ErrorAction ,
                          Errored_ExecutingAction ,
                          Successed_SuccessAction ,
                          Errored_ErrorAction ,
                          Successed_SuspendAction ,
                          Errored_SuspendAction ,
                          SuspendedSuccessed_ResumeAction ,
                          SuspendedErrored_ResumeAction ,
                          Successed_KillAction ,
                          Errored_KillAction ,
                          Successed_StopAction ,
                          Errored_StopAction ,
                          SuspendedSuccessed_ReStartAction ,
                          SuspendedErrored_ReStartAction ,
                          SuspendedSuccessed_ReStartAndSuspendAction ,
                          SuspendedErrored_ReStartAndSuspendAction ,
                          NumberOfActions } ;

  class FiniteStateMachine {

    private :

      char *              _ControlStateName[ SUPERV::NumberOfControlStates ] ;
      char *              _StateName[ GraphExecutor::NumberOfAutomatonStates ] ;
      char *              _GraphStateName[ SUPERV::NumberOfGraphStates ] ;
      char *              _EventName[ GraphExecutor::NumberOfEvents ] ;
      char *              _ActionName[ NumberOfActions ] ;
      GraphExecutor::AutomatonState _TransitionTable[ GraphExecutor::NumberOfAutomatonStates ]
                                                    [ GraphExecutor::NumberOfEvents ] ;
      GraphExecutor::StateEventAction _ActionTable[ GraphExecutor::NumberOfAutomatonStates ]
                                                  [ GraphExecutor::NumberOfEvents ] ;

      pthread_mutex_t            _MutexPythonWait ;
      bool                       _MutexPythonLocked ;
// Map between FuncName and corresponding Python Object Function
      map< string , PyObject * > _MapOfPyFunctions ;
      bool                       _PyInitModule ;
      int                        _ExecNumber ;
      int                        _GraphExecutingNumber ;

      int                                _DbgFileNumber ;

// fields and methods that use them are moved to GraphEditor::OutNode class (asv 20.09.04)  
//map< string , GraphBase::Graph * > _MapOfGraphs ;
//map< string , int >                _MapOfGraphNames ;

      bool              _JoinThread ;
      pthread_t         _JoinThreadNo ;
      pthread_mutex_t   _MutexJoinWait ;
      pthread_cond_t    _JoinWait ;
      list< pthread_t > _ThreadList ;

      Graph_Impl * _GraphImpl ;

    public :

      FiniteStateMachine() ;
      virtual ~FiniteStateMachine() {} ;

      int ExecNumber() ;
      void Executed() ;
      void PyInitModule(bool) ;
      bool PyInitModule() ;
      void PyLock() ;
      void PyUnLock() ;
      void PyLockReset() ;
      bool PyFunction( const char * aPyFuncName , PyObject * aPyFunction ) ;
      PyObject * PyFunction( const char * aPyFuncName ) ;
      bool ErasePyFunction( const char * aPyFuncName ) ;


      string DbgFileNumber() {
             _DbgFileNumber += 1 ;
             ostringstream astr ;
             astr << _DbgFileNumber ;
             return astr.str() ; } ;
/*
      GraphBase::Graph * MapGraph( const char * aGraphName ) ;
      bool MapGraph( GraphBase::Graph * aGraph , const char * aGraphName ) ;
      void EraseGraph( const char * aGraphName ) ;
      bool GraphName( const char * aGraphName ) ;
      string GraphInstanceName( const char * aGraphName ) ;
*/

      void JoinThread() ;
      void JoinThread( pthread_t aThread ) ;

      int ThreadsNumber() ; // mkr : IPAL14711

      const char * ControlStateName( const SUPERV::ControlState & aState ) const {
            return _ControlStateName[ aState ] ; } ;
      const char * StateName( const GraphExecutor::AutomatonState & aState ) const {
            return _StateName[ aState ] ; } ;
      const char * StateName( const SUPERV::GraphState & aState ) const {
            return _GraphStateName[ aState ] ; } ;
      const char * EventName( const GraphExecutor::NodeEvent & anEvent ) const {
            return _EventName[ anEvent ] ; } ;
      const char * ActionName( const GraphExecutor::StateEventAction & anAction ) const {
            return _ActionName[ anAction ] ; } ;

      const GraphExecutor::AutomatonState NextState( const GraphExecutor::AutomatonState & aState ,
                                                     const GraphExecutor::NodeEvent & anEvent ) const {
            return _TransitionTable[ aState ][ anEvent ] ; } ;

      const GraphExecutor::StateEventAction NextAction( const GraphExecutor::AutomatonState & aState ,
                                                        const GraphExecutor::NodeEvent & anEvent ) const {
            return _ActionTable[ aState ][ anEvent ] ; } ;

    void GraphImpl( Graph_Impl * aGraphImpl ) {// For GraphMNode ...
         _GraphImpl = aGraphImpl ; } ;
    Graph_Impl * GraphImpl() {
                 return _GraphImpl ; } ;
  };

};

ostream & operator<< (ostream &f ,const GraphExecutor::AutomatonState & aState ) ;

#endif

//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowExecutor_DynInvoke.hxx
//  Module : SUPERV

#ifndef _DATAFLOWEXECUTOR_DYNINVOKE_
#define _DATAFLOWEXECUTOR_DYNINVOKE_

#include <SALOMEconfig.h>
//#include CORBA_CLIENT_HEADER(SALOME_Component)
//#include CORBA_CLIENT_HEADER(SUPERV)
#include  <string>

struct ServicesAnyData {

  string Name;
  CORBA::Any Value;

};


// dynamic call for function with list of in parameters, followed 
// by out parameters with no return value

void DynInvoke(Engines::Component_ptr obj,
	       const char *method, 
	       ServicesAnyData * inParams, int nInParams,
	       ServicesAnyData * inParams, int nOutParams);
void DynInvoke(Engines::Component_ptr obj,
	       const char *method, 
	       const char * aGraphName ,
	       const char * aNodeName );

#endif

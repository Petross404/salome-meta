//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_InNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWEXECUTOR_INNODE_HXX
#define _DATAFLOWEXECUTOR_INNODE_HXX

#include <stdio.h>

#include <Python.h>

#include "SALOME_Container_i.hxx"

#include "DataFlowBase_Graph.hxx"
#include "DataFlowBase_FactoryNode.hxx"
#include "DataFlowBase_GOTONode.hxx"
#include "DataFlowBase_LoopNode.hxx"
#include "DataFlowBase_EndOfLoopNode.hxx"
#include "DataFlowBase_SwitchNode.hxx"
#include "DataFlowBase_EndOfSwitchNode.hxx"

#include "DataFlowExecutor_FiniteStateMachine.hxx"

#define MAXSTACKTHREADSIZE 127

void * run_function(void *p);

extern GraphExecutor::FiniteStateMachine * theAutomaton ;

extern "C" PyObject * PyRunMethod( PyObject * dummy , PyObject * args ) ;

namespace GraphExecutor {

  struct ServicesAnyData {
    string     Name;
    CORBA::Any Value;
  };

  class OutNode ;

//  class InNode : public GraphBase::Node {
  class InNode : public GraphBase::Base {

    private:

      GraphBase::ComputingNode       * _ComputingNode ;
      GraphBase::FactoryNode         * _FactoryNode ;
      GraphBase::InLineNode          * _InLineNode ;
      GraphBase::GOTONode            * _GOTONode ;
      GraphBase::LoopNode            * _LoopNode ;
      GraphBase::EndOfLoopNode       * _EndOfLoopNode ;
      GraphBase::SwitchNode          * _SwitchNode ;
      GraphBase::EndOfSwitchNode     * _EndOfSwitchNode ;
      GraphBase::Graph               * _GraphMacroNode ;

      PyObject                       * _MyPyRunMethod ;

      bool                             _createNewThread ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//      bool                             _createNewThreadIf ;
      int                              _RewindStack ;
      GraphExecutor::AutomatonState    _OldState ;
      GraphExecutor::AutomatonState    _currentState ;
      GraphExecutor::NodeEvent         _CurrentEvent ;
      SUPERV::ControlState             _ControlState ;
      GraphExecutor::AutomatonState    _NextState ;
      GraphExecutor::StateEventAction  _NextAction ;

      pthread_mutex_t                  _MutexDataReady ;
      bool                             _MutexDataReadyLocked ;
      bool                             _HasAllDataReady ;

      bool                             _PyFuncRunned ;
      bool                             _Loading ;
    
//JR 15.04.2005 Debug PAL8624 RetroConception :
//      pthread_mutex_t                  _MutexDataWait ;
//      bool                             _DataWait ;

      pthread_mutex_t                  _MutexWait ;

      pthread_cond_t                   _ReadyWait ;
      pthread_cond_t                   _RunningWait ;
      pthread_cond_t                   _DoneWait ;
      pthread_cond_t                   _SuspendedWait ;

      pthread_cond_t                   _SuspendWait ;
      bool                             _SuspendSync ;
      pthread_cond_t                   _ResumeWait ;
      bool                             _ResumeSync ;
      GraphExecutor::NodeEvent         _aResumeEvent ;
      GraphExecutor::InNode *          _aReStartNode ;
      GraphExecutor::NodeEvent         _aReStartEvent ;

      pthread_cond_t                   _KillWait ;
      bool                             _KillSync ;
      pthread_cond_t                   _StopWait ;

      pthread_cond_t                   _ThreadStartedWait ;
      bool                             _ThreadStartedSync ;

      pthread_t                        _Pythread ;
      long                             _PyCpuUsed ;
      long                             _PyTotCpuUsed ;

      GraphExecutor::FiniteStateMachine * _Automaton ;
    
      CORBA::ORB_ptr                    _Orb;

      GraphExecutor::OutNode          * _OutNode ;

      char                            * _DataFromNode ;
      bool                              _InitLoop ;

    public:

      InNode() ;
      InNode( CORBA::ORB_ptr ORB, SALOME_NamingService* ptrNamingService,
              const SALOME_ModuleCatalog::Service& NodeService ,
              const char *NodeComponentName ,
              const char* NodeInterfaceName ,
              const char *NodeName ,
              const SUPERV::KindOfNode akind = SUPERV::ComputingNode ,
              GraphBase::ListOfFuncName aFuncName = GraphBase::ListOfFuncName() ,
              GraphBase::ListOfPythonFunctions aPythonFunction = GraphBase::ListOfPythonFunctions() ,
              const SUPERV::SDate NodeFirstCreation = SUPERV::SDate() ,
              const SUPERV::SDate NodeLastModification = SUPERV::SDate() ,
              const char * NodeEditorRelease = NULLSTRING ,
              const char * NodeAuthor = NULLSTRING ,
              const char * NodeComputer = NULLSTRING ,
              const char * NodeComment = NULLSTRING ,
              const bool   GeneratedName = false ,
              const int NodeX = 0 ,
              const int NodeY = 0 ,
              int * Graph_prof_debug = NULL ,
              ofstream * Graph_fdebug = NULL ) ;
      virtual ~InNode() ;

      pthread_t ThreadNo() {
                return _ComputingNode->ThreadNo() ; } ;
      void ThreadNo( pthread_t aThread ) {
           _ComputingNode->ThreadNo ( aThread ) ; } ;

      char * Name() const {
             return _ComputingNode->Name() ; } ;
      const char *const * NamePtr() const {
                          return _ComputingNode->NamePtr() ; } ;
      SUPERV::KindOfNode Kind() const {
                         return _ComputingNode->Kind() ; } ;
      const bool IsComputingNode() const {
                 return _ComputingNode->IsComputingNode() ; } ;
      const bool IsFactoryNode() const {
                 return _ComputingNode->IsFactoryNode() ; } ;
      const bool IsInLineNode() const {
                 return _ComputingNode->IsInLineNode() ; } ;
      const bool IsOneOfInLineNodes() const {
                 return _ComputingNode->IsOneOfInLineNodes() ; } ;
      const bool IsOneOfGOTONodes() const {
                 return _ComputingNode->IsOneOfGOTONodes() ; } ;
      const bool IsMacroNode() const {
                 return _ComputingNode->IsMacroNode() ; } ;
      const bool IsDataFlowNode() const {
                 return _ComputingNode->IsDataFlowNode() ; } ;
      const bool IsDataStreamNode() const {
                 return _ComputingNode->IsDataStreamNode() ; } ;
      const bool IsLoopNode() const {
                 return _ComputingNode->IsLoopNode() ; } ;
      const bool IsEndLoopNode() const {
                 return _ComputingNode->IsEndLoopNode() ; } ;
      const bool IsSwitchNode() const {
                 return _ComputingNode->IsSwitchNode() ; } ;
      const bool IsEndSwitchNode() const {
                 return _ComputingNode->IsEndSwitchNode() ; } ;
      const bool IsGOTONode() const {
                 return _ComputingNode->IsGOTONode() ; } ;
      const bool IsHeadNode() const {
                 return _ComputingNode->IsHeadNode() ; } ;
      GraphBase::ComputingNode * ComputingNode() {
                                 return _ComputingNode ; } ;
      GraphBase::FactoryNode * FactoryNode() {
                                 return _FactoryNode ; } ;
      GraphBase::GOTONode * GOTONode() {
                            if ( _GOTONode )
                              return _GOTONode ;
                            if ( _LoopNode )
                              return _LoopNode ;
                            if ( _EndOfLoopNode )
                              return _EndOfLoopNode ;
                            if ( _SwitchNode )
                              return _SwitchNode ;
                            if ( _EndOfSwitchNode )
                              return _EndOfSwitchNode ;
                            return NULL ;
                            } ;
      GraphBase::InLineNode * InLineNode() {
                              GraphBase::InLineNode * aNode = GOTONode() ;
                              if ( aNode == NULL )
                                return _InLineNode ;
                              return aNode ;
                              } ;
      GraphBase::LoopNode * LoopNode() {
                            return _LoopNode ; } ;
      GraphBase::Graph * GraphMacroNode() {
                         return _GraphMacroNode ; } ;

      SUPERV::CNode_var ObjRef() const { return _ComputingNode->ObjRef() ; } ;
      void SetObjRef( SUPERV::CNode_var aNode ) {
                     _ComputingNode->SetObjRef( aNode ) ; } ;

      CNode_Impl * ObjImpl() const { return _ComputingNode->ObjImpl() ; } ;
      void SetObjImpl( CNode_Impl * aGraph ) {
                       _ComputingNode->SetObjImpl( aGraph ) ; } ;

      Engines::Component_var Component() const ;
      Engines::Container_var Container() const ;
      void SetContainer(Engines::Container_var aContainer) {
                        _FactoryNode->SetContainer( aContainer ) ; } ;
      void SetComponent(Engines::Component_var anObjComponent) {
                        _FactoryNode->SetComponent( anObjComponent ) ; } ;
      void ObjInterface( bool k_interface ) {
                        _ComputingNode->ObjInterface( k_interface ) ; } ;
      bool ObjInterface() {
                        return _FactoryNode->ObjInterface() ; } ;
      char * ComponentName() const { return _FactoryNode->ComponentName() ; } ;
      char * InterfaceName() const { return _FactoryNode->InterfaceName() ; } ;
      char * Computer() const { return _FactoryNode->Computer() ; } ;
      const char * ServiceName() const {
                   return _ComputingNode->ServiceName() ; } ;
      const SALOME_ModuleCatalog::ListOfServicesParameter ServiceInParameter() const {
            return _ComputingNode->ServiceInParameter() ; } ;
      const SALOME_ModuleCatalog::ListOfServicesParameter ServiceOutParameter() const {
            return _ComputingNode->ServiceOutParameter() ; } ;

      void CoupledNode( GraphBase::InLineNode * aCoupledNode ) {
           GOTONode()->CoupledNode( aCoupledNode ) ; } ;
      GraphBase::InLineNode * CoupledNode() {
                              return GOTONode()->CoupledNode() ; } ;

      GraphBase::InPort * AddInPort( const char * InputParameterName ,
                                     const char * InputParameterType ,
                                     const SUPERV::KindOfPort aKindOfPort ) {
                          return _ComputingNode->AddInPort( InputParameterName ,
                                                            InputParameterType ,
                                                            aKindOfPort ) ; } ;
      GraphBase::OutPort * AddOutPort( const char * OutputParameterName ,
                                       const char * OutputParameterType ,
                                       const SUPERV::KindOfPort aKindOfPort ) {
                           return _ComputingNode->AddOutPort( OutputParameterName ,
                                                              OutputParameterType ,
                                                              aKindOfPort ) ; } ;
//      void InOutPort( GraphBase::InPort * InputPort ,
//                      GraphBase::OutPort * OutputPort ) {
//           return _ComputingNode->InOutPort( InputPort , OutputPort ) ; } ;
      int LinkedNodesSize() const {
          return _ComputingNode->LinkedNodesSize() ; } ;
//      GraphBase::ComputingNode * LinkedNodes( int i ) const {
      GraphBase::StreamNode * LinkedNodes( int i ) const {
                              return _ComputingNode->LinkedNodes( i ) ; } ;
      const int LinkedInPortsNumber( int i ) const {
                return _ComputingNode->LinkedInPortsNumber( i ) ; } ;

      const int GetNodeInPortsSize() const {
                return _ComputingNode->GetNodeInPortsSize() ; } ;
      const GraphBase::InPort *GetNodeInLoop() const {
                              return _ComputingNode->GetNodeInLoop() ; } ;
      const GraphBase::InPort *GetNodeInGate() const {
                              return _ComputingNode->GetNodeInGate() ; } ;
      const GraphBase::InPort *GetNodeInPort(int i) const {
                              return _ComputingNode->GetNodeInPort( i ) ; } ;
      GraphBase::InPort *GetChangeNodeInLoop() const {
                        return _ComputingNode->GetChangeNodeInLoop() ; } ;
      GraphBase::InPort *GetChangeNodeInGate() const {
                        return _ComputingNode->GetChangeNodeInGate() ; } ;
      GraphBase::InPort *GetChangeNodeInPort(int i) const {
                        return _ComputingNode->GetChangeNodeInPort( i ) ; } ;
      const int GetNodeOutPortsSize() const {
                return _ComputingNode->GetNodeOutPortsSize() ; } ;
      const GraphBase::OutPort *GetNodeOutLoop() const {
                               return _ComputingNode->GetNodeOutLoop() ; } ;
      const GraphBase::OutPort *GetNodeOutGate() const {
                               return _ComputingNode->GetNodeOutGate() ; } ;
      const GraphBase::OutPort *GetNodeOutPort(int i) const {
                               return _ComputingNode->GetNodeOutPort( i ) ; } ;
      GraphBase::OutPort *GetChangeNodeOutLoop() const {
                         return _ComputingNode->GetChangeNodeOutLoop() ; } ;
      GraphBase::OutPort *GetChangeNodeOutGate() const {
                         return _ComputingNode->GetChangeNodeOutGate() ; } ;
      GraphBase::OutPort *GetChangeNodeOutPort(int i) const {
                         return _ComputingNode->GetChangeNodeOutPort( i ) ; } ;

      const GraphBase::InPort *GetInPort( const char *name ) {
            return _ComputingNode->GetInPort( name ) ; } ;
      const GraphBase::OutPort *GetOutPort( const char *name ) {
            return _ComputingNode->GetOutPort( name ) ; } ;
      GraphBase::InPort *GetChangeInPort( const char *name ) {
                        return _ComputingNode->GetChangeInPort( name ) ; } ;
      GraphBase::OutPort *GetChangeOutPort( const char *name ) {
                         return _ComputingNode->GetChangeOutPort( name ) ; } ;

      void PyFuncRunned( bool arunned ) {
           _PyFuncRunned = arunned ; } ;
      bool PyFuncRunned() {
           return _PyFuncRunned ; } ;

      void OutNode( GraphExecutor::OutNode * theOutNode ) {
           _OutNode = theOutNode ; } ;

      bool InitPython() ;
      PyObject * InitPyDynInvoke( char * PyFuncName ,
                                  const SUPERV::ListOfStrings * aPythonFunction ,
                                  bool & Err ) ;
      void RemovePyDynInvoke( char * PyFuncName ) ;

//JR 15.04.2005 Debug PAL8624 RetroConception :
//      void LockDataWait() ;
//      void UnLockDataWait() ;
//      bool IsLockedDataWait() { return _DataWait ; } ;
      void LockDataReady() ;
      void UnLockDataReady() ;
      void HasAllDataReady( bool hasAllDataReady ) {
//           cdebug << "Executor::InNode::HasAllDataReady( " << hasAllDataReady
//                  << " ) " << Name() << " previous _HasAllDataReady " << _HasAllDataReady
//                  << endl ;
           _HasAllDataReady = hasAllDataReady ; } ;
      bool HasAllDataReady() const {
           return _HasAllDataReady ; }

      bool Ping() ;
      bool ContainerKill() ;

      bool Kill() ;
      bool KillDone() ;
      bool Suspend() ;
      bool SuspendDone() ;
      bool Resume() ;
      bool Stop() ;

      void CreateNewThread( bool k_create ) {
//           cdebug << Name() << " CreateNewThread " << k_create << endl ;
           _createNewThread = k_create ; } ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//      void CreateNewThreadIf( bool k_create ) {
//           cdebug << Name() << " CreateNewThreadIf( " << k_create << " )" << endl ;
//           _createNewThreadIf = k_create ; } ;
      bool CreateNewThread() { return _createNewThread ; } ;
//      bool CreateNewThreadIf() { return _createNewThreadIf ; } ;
      void NewThread( pthread_t aThread ) ;
      void ExitThread() ;
      void RewindStack( int aRewindStack ) { _RewindStack = aRewindStack ; } ;
      int RewindStack() const { return _RewindStack ; } ;

      GraphExecutor::AutomatonState State() const {
             return _currentState; };
      void State(GraphExecutor::AutomatonState aState ) {
//           cdebug << "GraphExecutor::InNode::State( "
//                  << Automaton()->StateName( _currentState ) << " --> "
//                  << Automaton()->StateName( aState ) << " )"  << endl ;
           _currentState = aState ; } ;
      SUPERV::ControlState ControlState() const {
             return _ControlState; };
      void ControlState(SUPERV::ControlState aControlState ) {
           _ControlState = aControlState ; } ;
      void ControlClear() {
           _ControlState = SUPERV::VoidState ; } ;

      void SetAutomaton() {
            _Automaton = theAutomaton ; } ;
      GraphExecutor::FiniteStateMachine * Automaton() const {
            return _Automaton ; } ;

      bool IsWaiting() ;
      bool IsReady() ;
      bool IsRunning() ;
      bool IsDone() ;
      bool IsSuspended() ;
      bool IsKilled() ;
      bool IsStopped() ;
      void IsLoading( bool Loading );
      bool IsLoading() { return _Loading ; } ;

      bool StateWait( SUPERV::GraphState aState ) ;
      bool ReadyWait() ;
      bool RunningWait() ;
      bool DoneWait() ;
      bool SuspendedWait() ;

      void InitialState() ;
      bool InitPythonFunctions(bool WithErr ) ;
      void SetWaitingStates(GraphExecutor::InNode * EndNode ) ;

      int SendEvent(const GraphExecutor::NodeEvent anEvent ) ;
      void DataFromNode( char * FromNodeName ) {
           _DataFromNode = FromNodeName ; } ;
      const char * DataFromNode() const { return _DataFromNode ; } ;

      int ErrorAction();
      int VoidAction();
      void ReadyAction() ;
      void RunningAction() ;
      void DoneAction() ;
      void SuspendedAction() ;
      GraphExecutor::InNode * SuspendAction() ;
      bool ResumeAction(GraphExecutor::NodeEvent aResumeEvent ) ;
      bool ReStartAction( GraphExecutor::InNode * aRestartNode ,
                          GraphExecutor::NodeEvent anEvent ) ;
      void KillAction() ;
      void KilledAction() ;
      void ThreadStartAction() ;
      void ThreadStartedAction() ;
      void StopAction() ;
      void StoppedAction() ;
      int executeAction() ; // New Thread or Same Thread
      int ExecuteAction() ;

      int DataWaiting_SomeDataReadyAction() ;
      int DataUndef_NotAllDataReadyAction() ;
      int DataUndef_AllDataReadyAction() ;
      int DataReady_SuspendAction() ;
      int SuspendedReady_ResumeAction() ;
      int DataReady_KillAction() ;
      int DataReady_StopAction() ;
      int DataReady_ExecuteAction() ;
      int DataReady_ExecuteActionInLineNodes( ServicesAnyData * InParametersList ,
                                              ServicesAnyData * OutParametersList ) ;      int DataReady_ExecuteActionLoopNodes( ServicesAnyData * InParametersList ,
                                            ServicesAnyData * OutParametersList ,
                                            bool & CopyInOut ) ;
      void DynInvoke( Engines::Component_ptr obj,
		      const char *method, 
		      ServicesAnyData * inParams, int nInParams,
		      ServicesAnyData * outParams, int nOutParams) ;
      void DynInvoke( Engines::Component_ptr obj,
	              const char *method, 
	              const char * aGraphName ,
	              const char * aNodeName );
      bool PyDynInvoke( PyObject * MyPyRunMethod ,
	                const char *method , 
	                ServicesAnyData * inParams , int nInParams ,
	                ServicesAnyData * outParams, int nOutParams ) ;

      int Executing_SuspendAction() ;
      int SuspendedExecuting_ResumeAction() ;
      int Executing_KillAction() ;
      int Executing_StopAction() ;
      int Executing_SuccessAction() ;
//      int Executing_ErrorAction() ;
      int Errored_ExecutingAction() ;
      int Successed_SuccessAction() ;
      bool SendSomeDataReady( char * FromNodeName ) ;
      int Errored_ErrorAction() ;
      int Successed_SuspendAction() ;
      int Errored_SuspendAction() ;
      int SuspendedSuccessed_ResumeAction() ;
      int SuspendedErrored_ResumeAction() ;
      int Successed_KillAction() ;
      int Errored_KillAction() ;
      int Successed_StopAction() ;
      int Errored_StopAction() ;
      int SuspendedSuccessed_ReStartAction() ;
      int SuspendedErrored_ReStartAction() ;
      int SuspendedSuccessed_ReStartAndSuspendAction() ;
      int SuspendedErrored_ReStartAndSuspendAction() ;

      void InParametersSet( bool & Err ,
                            int  nInParams ,
                            ServicesAnyData * InParametersList ) ;
      void InOutParametersSet( int nOutParams ,
                               ServicesAnyData * OutParametersList ) ;
      bool OutParametersSet( bool Err , SUPERV::GraphState PortState ,
                             int nOutParams ,
                             ServicesAnyData * OutParametersList ) ;
      void SetOutPortsOfInportsOfEndSwitch( GraphBase::OutPort * anOutPort ,
                                            const char * anEndSwitchNodeName ) ;
      void coutbegin() ;
      void coutexit() ;

      const long CpuUsed( bool tot = false ) ;

      long PyCpuUsed( bool tot = false ) ;
      void SetPyCpuUsed() ;
      long PyCpu() ;

      bool PyRunSimpleString( char* thePyString );
      PyObject * PyEvalCallObject( const char *method ,
                                   PyObject * MyPyRunMethod ,
                                   PyObject * ArgsList ) ;
    } ;

} ;


#endif


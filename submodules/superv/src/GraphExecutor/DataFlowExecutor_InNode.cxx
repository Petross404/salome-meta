//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowExecutor_InNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include "OpUtil.hxx"

#include <SALOMEconfig.h>
#include CORBA_CLIENT_HEADER(SALOME_Component)
//#include "SALOME_NamingService.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "DataFlowBase_FactoryNode.hxx"
#include "DataFlowBase_GOTONode.hxx"
#include "DataFlowBase_LoopNode.hxx"
#include "DataFlowBase_EndOfLoopNode.hxx"
#include "DataFlowBase_SwitchNode.hxx"
#include "DataFlowBase_EndOfSwitchNode.hxx"

#include "DataFlowExecutor_DataFlow.hxx"
#include "DataFlowEditor_DataFlow.hxx"   // GraphEditor package must be built BEFORE

static void InitInNode( int &_RewindStack ,
                        SUPERV::ControlState &_ControlState ,
                        GraphExecutor::AutomatonState &_currentState ,
                        GraphExecutor::InNode ** _aReStartNode ,
                        bool & _PyFuncRunned ,
                        PyObject ** _MyPyRunMethod ,
//                        pthread_mutex_t &_MutexDataWait ,
//                        bool &_DataWait ,
                        pthread_mutex_t &_MutexDataReady ,
                        pthread_mutex_t &_MutexWait ,
                        pthread_cond_t &_ReadyWait ,
                        pthread_cond_t &_RunningWait ,
                        pthread_cond_t &_DoneWait ,
                        pthread_cond_t &_SuspendedWait ,
                        pthread_cond_t &_SuspendWait ,
                        bool &_SuspendSync ,
                        pthread_cond_t &_ResumeWait ,
                        bool &_ResumeSync ,
                        pthread_cond_t &_KillWait ,
                        bool &_KillSync ,
                        pthread_cond_t &_ThreadStartedWait ,
                        bool &_ThreadStartedSync ,
                        pthread_cond_t &_StopWait ,
                        GraphExecutor::FiniteStateMachine ** _Automaton ,
                        GraphExecutor::FiniteStateMachine * theAutomaton ,
                        CORBA::ORB_ptr * _Orb ,
                        CORBA::ORB_ptr ORB,
		        bool &_Loading ) {
  _RewindStack = 0 ;
  _ControlState = SUPERV::VoidState ;
  _currentState = GraphExecutor::UnKnownState ;
  *_aReStartNode = NULL ;
  _PyFuncRunned = false ;
  *_MyPyRunMethod = NULL ;
//  pthread_mutex_init( &_MutexDataWait , NULL ) ;
//  _DataWait = false ;
  pthread_mutex_init( &_MutexDataReady , NULL ) ;
  pthread_mutex_init( &_MutexWait , NULL ) ;
  if ( pthread_cond_init( &_ReadyWait , NULL ) ) {
    perror("pthread_cond_init( &_ReadyWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_RunningWait , NULL ) ) {
    perror("pthread_cond_init( &_RunningWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_DoneWait , NULL ) ) {
    perror("pthread_cond_init( &_DoneWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_SuspendedWait , NULL ) ) {
    perror("pthread_cond_init( &_SuspendedWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_SuspendWait , NULL ) ) {
    perror("pthread_cond_init( &_SuspendWait , NULL )") ;
    exit( 0 ) ;
  }
  _SuspendSync = false ;
  if ( pthread_cond_init( &_ResumeWait , NULL ) ) {
    perror("pthread_cond_init( &_ResumeWait , NULL )") ;
    exit( 0 ) ;
  }
  _ResumeSync = false ;
  if ( pthread_cond_init( &_KillWait , NULL ) ) {
    perror("pthread_cond_init( &_KillWait , NULL )") ;
    exit( 0 ) ;
  }
  _KillSync = false ;
  if ( pthread_cond_init( &_ThreadStartedWait , NULL ) ) {
    perror("pthread_cond_init( &_ThreadStartedWait , NULL )") ;
    exit( 0 ) ;
  }
  _ThreadStartedSync = false ;
  if ( pthread_cond_init( &_StopWait , NULL ) ) {
    perror("pthread_cond_init( &_StopWait , NULL )") ;
    exit( 0 ) ;
  }
  *_Automaton = theAutomaton ;
  *_Orb = CORBA::ORB::_nil();
  _Loading = false;
}

GraphExecutor::FiniteStateMachine * theAutomaton = new GraphExecutor::FiniteStateMachine() ;

//GraphExecutor::InNode::InNode() :
//     GraphBase::FactoryNode() {
GraphExecutor::InNode::InNode() {
  InitInNode( _RewindStack ,
              _ControlState ,
              _currentState ,
              &_aReStartNode ,
              _PyFuncRunned ,
              &_MyPyRunMethod ,
//              _MutexDataWait ,
//              _DataWait ,
              _MutexDataReady ,
              _MutexWait ,
              _ReadyWait ,
              _RunningWait ,
              _DoneWait ,
              _SuspendedWait ,
              _SuspendWait ,
              _SuspendSync ,
              _ResumeWait ,
              _ResumeSync ,
              _KillWait ,
              _KillSync ,
              _ThreadStartedWait ,
              _ThreadStartedSync ,
              _StopWait ,
              &_Automaton ,
              theAutomaton ,
              &_Orb ,
              CORBA::ORB::_nil(),
	      _Loading ) ;
}

GraphExecutor::InNode::InNode( CORBA::ORB_ptr ORB,
                               SALOME_NamingService* ptrNamingService ,
                               const SALOME_ModuleCatalog::Service& aService ,
                               const char * ComponentName ,
                               const char * NodeInterfaceName ,
                               const char * NodeName ,
                               const SUPERV::KindOfNode akind ,
                               GraphBase::ListOfFuncName aFuncName ,
                               GraphBase::ListOfPythonFunctions aPythonFunction ,
                               const SUPERV::SDate NodeFirstCreation ,
                               const SUPERV::SDate NodeLastModification  ,
                               const char * NodeEditorRelease ,
                               const char * NodeAuthor ,
                               const char * NodeComputer ,
                               const char * NodeComment ,
                               const bool   GeneratedName ,
                               const int NodeX ,
                               const int NodeY ,
                               int * Graph_prof_debug,
                               ofstream * Graph_fdebug) {
//               ostream * Graph_fdebug = NULL ) :
//             GraphBase::FactoryNode( ORB , ptrNamingService , aService ,
//                                     ComponentName , NodeInterfaceName ,
//                                     NodeName , akind ,
//                                     NodeFirstCreation , NodeLastModification  ,
//                                     NodeEditorRelease , NodeAuthor ,
//                                     NodeComputer , NodeComment , GeneratedName ,
//                                     0 , 0 ,
//                                     Graph_prof_debug , Graph_fdebug ) {
  InitInNode( _RewindStack ,
              _ControlState ,
              _currentState ,
              &_aReStartNode ,
              _PyFuncRunned ,
              &_MyPyRunMethod ,
//              _MutexDataWait ,
//              _DataWait ,
              _MutexDataReady ,
              _MutexWait ,
              _ReadyWait ,
              _RunningWait ,
              _DoneWait ,
              _SuspendedWait ,
              _SuspendWait ,
              _SuspendSync ,
              _ResumeWait ,
              _ResumeSync ,
              _KillWait ,
              _KillSync ,
              _ThreadStartedWait ,
              _ThreadStartedSync ,
              _StopWait ,
              &_Automaton ,
              theAutomaton ,
              &_Orb ,
              ORB,
	      _Loading ) ;
  SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;

  _ComputingNode = NULL ;
  _FactoryNode = NULL ;
  _InLineNode = NULL ;
  _GOTONode = NULL ;
  _LoopNode = NULL ;
  _EndOfLoopNode = NULL ;
  _SwitchNode = NULL ;
  _EndOfSwitchNode = NULL ;
  switch ( akind ) {
  case SUPERV::ComputingNode : {
    cdebug << "GraphExecutor::InNode::InNode SUPERV::ComputingNode : " << NodeName ;
    _ComputingNode = new GraphBase::ComputingNode( ORB , ptrNamingService ,
                                                   aService ,
                                                   NodeName , akind ,
                                                   NodeFirstCreation ,
                                                   NodeLastModification  ,
                                                   NodeEditorRelease , NodeAuthor ,
                                                   NodeComment , GeneratedName ,
                                                   NodeX , NodeY ,
                                                   Graph_prof_debug , Graph_fdebug ) ;
    break ;
  }
  case SUPERV::FactoryNode : {
    cdebug << "GraphExecutor::InNode::InNode SUPERV::FactoryNode : " << NodeName ;
    _FactoryNode = new GraphBase::FactoryNode( ORB , ptrNamingService , aService ,
                                               ComponentName , NodeInterfaceName ,
                                               NodeName , akind ,
                                               NodeFirstCreation ,
                                               NodeLastModification  ,
                                               NodeEditorRelease , NodeAuthor ,
                                               NodeComputer , NodeComment ,
                                               GeneratedName , NodeX , NodeY ,
                                               Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _FactoryNode ;
    break ;
  }
  case SUPERV::InLineNode : {
    cdebug << "GraphExecutor::InNode::InNode SUPERV::InLineNode : " << NodeName ;
    _InLineNode = new GraphBase::InLineNode( ORB , ptrNamingService ,
                                             aFuncName[0].c_str() , *aPythonFunction[0] ,
                                             NodeName , akind ,
                                             NodeFirstCreation , NodeLastModification  ,
                                             NodeEditorRelease , NodeAuthor ,
                                             NodeComment , GeneratedName ,
                                             NodeX , NodeY ,
                                             Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _InLineNode ;
    break ;
  }
  case SUPERV::MacroNode : {
    cdebug << "GraphExecutor::InNode::InNode SUPERV::MacroNode : " << NodeName << endl ;
    _GraphMacroNode = new GraphBase::Graph( ORB , ptrNamingService ,
//                                            aFuncName[0].c_str() , *aPythonFunction[0] ,
                                            NodeName , akind ,
//                                            NodeFirstCreation , NodeLastModification  ,
//                                            NodeEditorRelease , NodeAuthor ,
//                                            NodeComment , GeneratedName ,
//                                            NodeX , NodeY ,
                                            Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _GraphMacroNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    _GOTONode = (GraphBase::GOTONode *) _InLineNode ;
    _GraphMacroNode->Coordinates( NodeX , NodeY ) ;
    break ;
  }
  case SUPERV::GOTONode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::GOTONode : " << NodeName ;
    _GOTONode = new GraphBase::GOTONode( ORB , ptrNamingService ,
                                         aFuncName[0].c_str() , *aPythonFunction[0] ,
                                         NodeName , akind ,
                                         NodeFirstCreation , NodeLastModification  ,
                                         NodeEditorRelease , NodeAuthor ,
                                         NodeComment , GeneratedName ,
                                         NodeX , NodeY ,
                                         Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _GOTONode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::LoopNode : {
    cdebug << "GraphExecutor::InNode::InNode SUPERV::LoopNode : " << NodeName ;
    _LoopNode = new GraphBase::LoopNode( ORB , ptrNamingService ,
                                         aFuncName[0].c_str() , *aPythonFunction[0] ,
                                         aFuncName[1].c_str() , *aPythonFunction[1] ,
                                         aFuncName[2].c_str() , *aPythonFunction[2] ,
                                         NodeName , akind ,
                                         NodeFirstCreation , NodeLastModification  ,
                                         NodeEditorRelease , NodeAuthor ,
                                         NodeComment , GeneratedName ,
                                         NodeX , NodeY ,
                                         Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _LoopNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::EndLoopNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::EndOfLoopNode : " << NodeName ;
    _EndOfLoopNode = new GraphBase::EndOfLoopNode(
                                         ORB , ptrNamingService ,
                                         aFuncName[0].c_str() , *aPythonFunction[0] ,
                                         NodeName , akind ,
                                         NodeFirstCreation , NodeLastModification  ,
                                         NodeEditorRelease , NodeAuthor ,
                                         NodeComment , GeneratedName ,
                                         NodeX , NodeY ,
                                         Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _EndOfLoopNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::SwitchNode : {
    cdebug << "GraphExecutor::InNode::InNode SUPERV::SwitchNode : " << NodeName ;
    _SwitchNode = new GraphBase::SwitchNode( ORB , ptrNamingService ,
                                             aFuncName[0].c_str() , *aPythonFunction[0] ,
                                             NodeName , akind ,
                                             NodeFirstCreation , NodeLastModification  ,
                                             NodeEditorRelease , NodeAuthor ,
                                             NodeComment , GeneratedName ,
                                             NodeX , NodeY ,
                                             Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _SwitchNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::EndSwitchNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::EndOfSwitchNode : " << NodeName ;
    _EndOfSwitchNode = new GraphBase::EndOfSwitchNode(
                                             ORB , ptrNamingService ,
                                             aFuncName[0].c_str() , *aPythonFunction[0] ,
                                             NodeName , akind ,
                                             NodeFirstCreation , NodeLastModification  ,
                                             NodeEditorRelease , NodeAuthor ,
                                             NodeComment , GeneratedName ,
                                             NodeX , NodeY ,
                                             Graph_prof_debug , Graph_fdebug ) ;
    _ComputingNode = (GraphBase::ComputingNode *) _EndOfSwitchNode ;
    _GOTONode = (GraphBase::GOTONode *) _ComputingNode ;
    _InLineNode = (GraphBase::InLineNode *) _ComputingNode ;
    break ;
  }
  case SUPERV::DataFlowGraph : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::DataFlowGraph ERROR : " << NodeName ;
  }
  case SUPERV::DataStreamGraph : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::DataStreamGraph ERROR : " << NodeName ;
  }
  case SUPERV::UnknownNode : {
    cdebug << "GraphEditor::InNode::InNode SUPERV::UnknownNode ERROR : " << NodeName ;
  }
  }
  cdebug << "GraphExecutor::InNode::InNode "  << (void *) this
         << " _ComputingNode " << (void *) _ComputingNode  ;
  _ComputingNode->InNode( this ) ;
}

GraphExecutor::InNode::~InNode() {
}

//JR 15.04.2005 Debug PAL8624 RetroConception :
#if 0
void GraphExecutor::InNode::LockDataWait() {
//  cdebug_in << "GraphExecutor::InNode::LockDataWait " << endl ;
  if ( pthread_mutex_lock( &_MutexDataWait ) ) {
    perror("Ready pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  _DataWait = true ;
//  cdebug_out << "GraphExecutor::InNode::LockDataWait " << endl ;
}
void GraphExecutor::InNode::UnLockDataWait() {
//  cdebug_in << "GraphExecutor::InNode::UnLockDataWait " << endl ;
  _DataWait = false ;
  if ( pthread_mutex_unlock( &_MutexDataWait ) ) {
    perror("Ready pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
//  cdebug_out << "GraphExecutor::InNode::UnLockDataWait " << endl ;
}
#endif

//JR 15.04.2005 Debug PAL8624 RetroConception :
void GraphExecutor::InNode::LockDataReady() {
//  cdebug_in << pthread_self() << "/" << ThreadNo()
//            << "GraphExecutor::InNode::LockDataReady : " << Name() << " _MutexDataReadyLocked "
//            << _MutexDataReadyLocked << " HasAllDataReady " << HasAllDataReady() << endl ;
  if ( pthread_mutex_lock( &_MutexDataReady ) ) {
    perror("MutexDataReady pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  _MutexDataReadyLocked = true ;
//  cdebug_out << pthread_self() << "/" << ThreadNo()
//             << "GraphExecutor::InNode::LockDataReady : " << Name() << endl ;
}
void GraphExecutor::InNode::UnLockDataReady() {
//  cdebug_in << pthread_self() << "/" << ThreadNo()
//            << "GraphExecutor::InNode::UnLockDataReady : " << Name() << " _MutexDataReadyLocked "
//            << _MutexDataReadyLocked << " HasAllDataReady " << HasAllDataReady() << endl ;
  if ( pthread_mutex_unlock( &_MutexDataReady ) ) {
    perror("MutexDataReady pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
  _MutexDataReadyLocked = false ;
//  cdebug_out << pthread_self() << "/" << ThreadNo()
//             << "GraphExecutor::InNode::UnLockDataReady : " << Name() << endl ;
}

Engines::Component_var GraphExecutor::InNode::Component() const {
  if ( IsFactoryNode() ) {
    return _FactoryNode->Component() ;
  }
  else {
//JR 30.03.2005    CORBA::Any const * anAnyComponent = GetChangeNodeInPort( 0 )->GetOutPort()->Value() ; // this
    const CORBA::Any anAnyComponent = GetChangeNodeInPort( 0 )->GetOutPort()->Value() ; // this
    CORBA::Object_ptr obj ;
    try {
//JR 30.03.2005      *anAnyComponent >>= obj ;
      anAnyComponent >>= obj ;
      return Engines::Component::_narrow( obj ) ;
    }
    catch( ... ) {
      cdebug << "GraphExecutor::InNode::Component Component catch" << endl ;
    }
  }
  return Engines::Component::_nil() ;
}

Engines::Container_var GraphExecutor::InNode::Container() const {
  if ( IsFactoryNode() ) {
    return _FactoryNode->Container() ;
  }
  return Engines::Container::_nil() ;
}


bool GraphExecutor::InNode::Ping() {
  cdebug_in << "GraphExecutor::InNode::Ping" << endl;
  bool RetVal = true ;
  if ( IsFactoryNode() ) {
    RetVal = !CORBA::is_nil( _FactoryNode->Component() ) ;
    if ( RetVal ) {
      if ( State() != GraphExecutor::SuspendedExecutingState ) {
        try {
          _FactoryNode->Component()->ping() ;
	}
        catch( ... ) {
          cdebug << "InNode::Ping() ERROR catched" << endl ;
          State( GraphExecutor::ErroredState ) ;
          _OutNode->State( GraphExecutor::ErroredState ) ;
          RetVal = false ;
	}
      }
      else {
        RetVal = false ;
      }
    }
    else {
      cdebug << "GraphExecutor::InNode::Ping Component ObjRef is NIL" << endl;
    }
  }
  cdebug_out << "GraphExecutor::InNode::Ping" << endl ;
  return RetVal ;
}

void GraphExecutor::InNode::NewThread( pthread_t aThread ) {
  ThreadNo ( aThread ) ; 
  if ( aThread )
    _OutNode->NewThread() ;
}
void GraphExecutor::InNode::ExitThread() {
  ThreadNo( 0 ) ;
  _OutNode->ExitThread() ;
} 

bool GraphExecutor::InNode::Suspend() {
  cdebug_in << "GraphExecutor::InNode::Suspend " << Name() << " " << ThreadNo()
            << " " << Automaton()->StateName( State() ) << endl;
  bool RetVal = false ;
  if ( IsDone() ) {
//If loop we need to suspend also    ControlState( SUPERV::VoidState ) ;
    ControlState( SUPERV::ToSuspendState ) ;
    RetVal = true ;
    if ( _OutNode->IsDone() ) {
      ControlState( SUPERV::VoidState ) ;
      RetVal = false ;
    }
  }
  else if ( IsWaiting() || IsReady() ) {
    ControlState( SUPERV::ToSuspendState ) ;
    RetVal = true ;
  }
  else  if ( IsRunning() ) {
    ControlState( SUPERV::ToSuspendState ) ;
    if ( IsFactoryNode() || IsComputingNode() ) {
// We have to suspend in the container of that node
      int TrySuspend = 10 ;
      while ( TrySuspend ) {
        if ( !CORBA::is_nil( Component() ) ) {
// We can call that component
          try {
            RetVal = Component()->Suspend_impl() ;
	  }
          catch( ... ) {
            cdebug << "InNode::Suspend() ERROR catched" << endl ;
            State( GraphExecutor::ErroredState ) ;
            _OutNode->State( GraphExecutor::ErroredState ) ;
            RetVal = false ;
            TrySuspend = 1 ;
	  }
          cdebug << "Component()->Suspend_impl() returns status " << RetVal << endl ;
          if ( RetVal ) {
            if ( IsRunning() ) {
              cdebug << pthread_self() << "GraphExecutor::InNode::Suspend_impl " << Name()
                     << " --> thread" << ThreadNo() << " SuspendEvent " << endl;
              SendEvent( GraphExecutor::SuspendEvent ) ;
              cdebug << pthread_self() << "GraphExecutor::InNode::Suspended_impl in Container"
                     << Name() << " --> thread" << ThreadNo() << endl;
              TrySuspend = 1 ;
            }
            else if ( IsDone() ) {
              ControlState( SUPERV::VoidState ) ;
              RetVal = false ; // Too late ...
              TrySuspend = 1 ;
            }
            else {
              cdebug << "InNode::Suspend component Suspended and !IsDone and !IsRunning !"
                     << endl ;
              MESSAGE("InNode::Suspend component Suspended and !IsDone and !IsRunning !") ;
              TrySuspend = 1 ;
	    }
          }
          else {
// Suspend in the Container failed : it is always false if it is a Python Container
            cdebug << "InNode::Suspend cannot Suspend component ! Python Component ?"
                   << endl ;
            if ( TrySuspend == 1 ) {
              if ( IsSuspended() ) {
                RetVal = true ;
	      }
              else {
                RetVal = false ;
	      }
	    }
          }
        }
        else {
          cdebug << "InNode::Suspend with nilComponent while RunningState !. Loading Component ?"
                 << endl ;
// Wait for the end of loading of the component
          while ( IsLoading() ) {
            sleep( 1 ) ;
	  }
          if ( TrySuspend == 1 ) {
            if ( IsSuspended() ) {
              RetVal = true ;
	    }
            else {
              RetVal = false ;
	    }
	  }
        }
        TrySuspend -= 1 ;
        if ( TrySuspend ) {
          sleep( 1 ) ;
	}
      }
    }
    else if ( IsMacroNode() ) {
// It should be like that but it is not completely implemented
      GraphBase::Graph * aGraph = (GraphBase::Graph * ) GraphMacroNode()->CoupledNode() ;
      RetVal = aGraph->GraphEditor()->Executor()->Suspend() ;
      if ( RetVal ) {
        State( GraphExecutor::SuspendedState ) ;
      }
    }
    else {
// Now we can suspend an InLineNode with the handler of the SuperVision Container
      // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL 
      State( GraphExecutor::ErroredState ) ;
      _OutNode->State( GraphExecutor::ErroredState ) ;      RetVal = false;
      cdebug << "Suspend of InLine nodes is NOT implemented." << endl;
      MESSAGE( "Suspend of InLine nodes is NOT implemented." );
      /*   
      if ( pthread_kill( _OutNode->MainThreadId() , SIGUSR2 ) == -1 ) {
        perror("Suspend pthread_kill error") ;
        State( GraphExecutor::ErroredState ) ;
        _OutNode->State( GraphExecutor::ErroredState ) ;
        RetVal = false ;
      }
      else {
        RetVal = true ;
      }
      if ( RetVal ) {
        if ( IsRunning() ) {
          cdebug << pthread_self() << "GraphExecutor::InNode::Suspend " << Name()
                 << " --> thread" << ThreadNo() << " SuspendEvent " << endl;
          SendEvent( GraphExecutor::SuspendEvent ) ;
          cdebug << pthread_self() << "GraphExecutor::InNode::Suspended in SuperVision Container"
                 << Name() << " --> thread" << ThreadNo() << endl;
        }
        else if ( IsDone() ) {
          ControlState( SUPERV::VoidState ) ;
          RetVal = false ; // Too late ...
        }
        else {
          cdebug << "component Suspended and !IsDone and !IsRunning !"
                 << endl ;
	}
      }
      */
    }
  }
  else {
    cdebug << "Suspend and IsDone " << IsDone() << " and IsRunning " << IsRunning()
           << " and IsWaiting " << IsWaiting() << " and IsReady " << IsReady()
           << " ?" << endl ;
    RetVal = false ;
  }
  cdebug_out << "GraphExecutor::InNode::Suspend " << RetVal << " "
             << Automaton()->StateName( State() ) << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::ContainerKill() {
  cdebug_in << "GraphExecutor::InNode::ContainerKill " << Name() << " "
            << ThreadNo() << endl;
  bool RetVal ;
  if ( IsFactoryNode() ) {
    Kill() ;
    RetVal = Container()->Kill_impl() ;
  }
  cdebug_out << "GraphExecutor::InNode::ContainerKill" << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::Kill() {
  cdebug_in << "GraphExecutor::InNode::Kill " << Name() << " " << ThreadNo() << " " 
            << Automaton()->StateName( State() ) << " Threads " << _OutNode->Threads()
            << " SuspendedThreads " << _OutNode->SuspendedThreads()
            << " EventQSize " << _OutNode->EventQSize() << endl;
  bool RetVal ;
  if ( IsDone() ) {
    ControlState( SUPERV::ToKillState ) ; // if loop
    if ( _OutNode->IsDone() ) {
      ControlState( SUPERV::VoidState ) ;
    }
    RetVal = false ;
  }
  else {
    ControlState( SUPERV::ToKillState ) ;
    if ( IsDone() ) {
      if ( _OutNode->IsDone() ) {
        ControlState( SUPERV::VoidState ) ;
      }
      RetVal = false ;
    }
    else {
      if ( IsRunning() ) {
        if ( IsFactoryNode() || IsComputingNode() ) {
// We have to suspend in the container of that node
          int TryKill = 10 ;
          while ( TryKill ) {
            if ( !CORBA::is_nil( Component() ) ) {
// We can call that component
              try {
                RetVal = Component()->Kill_impl() ;
	      }
              catch( ... ) {
                cdebug << "InNode::Kill_impl ERROR catched" << endl ;
                State( GraphExecutor::ErroredState ) ;
                _OutNode->State( GraphExecutor::ErroredState ) ;
                RetVal = false ;
                TryKill = 1 ;
	      }
              cdebug << "Component()->Kill_impl() returns status " << RetVal << endl ;
              if ( RetVal ) {
                if ( IsRunning() ) {
                  cdebug << pthread_self() << "GraphExecutor::InNode::Kill_impl " << Name()
                         << " --> thread" << ThreadNo() << " KillEvent " << endl;
                  SendEvent( GraphExecutor::KillEvent ) ;
                  cdebug << pthread_self() << "GraphExecutor::InNode::Killed_impl in Container"
                         << Name() << " --> thread" << ThreadNo() << endl;
                  TryKill = 1 ;
	        }
                else if ( IsDone() ) {
                  ControlState( SUPERV::VoidState ) ;
                  RetVal = false ; // Too late ...
                  TryKill = 1 ;
	        }
                else {
                  cdebug << "Kill component Killed and !IsDone and !IsRunning !"
                         << endl ;
                  TryKill = 1 ;
	        }
	      }
              else {
//  Kill in the Container failed : it is always false if it is a Python Container
                cdebug << "InNode::Suspend cannot  Kill component ! Python Component ?"
                       << endl ;
                if ( TryKill == 1 ) {
                  if ( IsKilled() ) {
                    RetVal = true ;
		  }
                  else {
                    RetVal = false ;
		  }
	        }
	      }
	    }
            else {
              cdebug << "InNode::Kill with nilComponent while RunningState !. Loading Component ?"
                     << endl ;
// Wait for the end of loading of the component
              while ( IsLoading() ) {
                sleep( 1 ) ;
	      }
              if ( TryKill == 1 ) {
                if ( IsKilled() ) {
                  RetVal = true ;
		}
                else {
                  RetVal = false ;
		}
	      }
	    }
            TryKill -= 1 ;
            if ( TryKill ) {
              sleep( 1 ) ;
            }
	  }
	}
        else if ( IsMacroNode() ) {
// It should be like that but it is not completely implemented
          GraphBase::Graph * aGraph = (GraphBase::Graph * ) GraphMacroNode()->CoupledNode() ;
          RetVal = aGraph->GraphEditor()->Executor()->Kill() ;
          if ( RetVal ) {
            State( GraphExecutor::KilledState ) ;
	  }
	}
        else {
//PAL6886
// Now we can kill an InLineNode with the handler of the SuperVision Container
	  // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
	  State( GraphExecutor::ErroredState ) ;
	  _OutNode->State( GraphExecutor::ErroredState ) ;
	  RetVal = false ;
	  cdebug << "Kill of InLine nodes is NOT implemented." << endl;
	  MESSAGE( "Kill of InLine nodes is NOT implemented." );
	  /*
          cdebug << pthread_self() << "Kill of InLineNode " << Name() << " MainThreadId "
                 << _OutNode->MainThreadId() << " :" << endl ;
          MESSAGE( pthread_self() << "Kill of InLineNode " << Name() << " MainThreadId "
                 << _OutNode->MainThreadId() << " :" ) ;
          if ( pthread_kill( _OutNode->MainThreadId() , SIGINT ) == -1 ) {
// python signals run only in main thread ...
            perror("Kill pthread_kill error") ;
            State( GraphExecutor::ErroredState ) ;
            _OutNode->State( GraphExecutor::ErroredState ) ;
            RetVal = false ;
	  }
          else {
            cdebug << pthread_self() << "pthread_kill of InLineNode " << Name()
                   << " done. MainThreadId " << _OutNode->MainThreadId() << endl ;
            MESSAGE( pthread_self() << "pthread_kill of InLineNode " << Name()
                   << " done. MainThreadId " << _OutNode->MainThreadId() ) ;
            RetVal = true ;
	  }
	  */
	}
      }
      else if ( IsSuspended() ) {
        cdebug << pthread_self() << "GraphExecutor::InNode::Kill " << Name()
               << " --> thread" << ThreadNo() << " Resume()" << endl;
        if ( Resume() ) {
          RetVal = Kill() ;
	}
        else {
          RetVal = false ;
	}
      }
      else if ( IsWaiting() ) {
        RetVal = false ;
      }
      else if ( IsReady() ) {
        RetVal = true ;
      }
      else {
       cdebug << "Kill and IsDone " << IsDone() << " and IsRunning " << IsRunning()
              << " and IsWaiting " << IsWaiting() << " and IsReady " << IsReady()
              << " ?" << endl ;
        RetVal = false ;
      }
    }
  }
// PAL8003
// JR 24.03.2005 Debug it may have be killed if we have Suspend-Resume-Kill
  if ( !RetVal && IsKilled() ) {
    RetVal = true ;
  }
  cdebug_out << "GraphExecutor::InNode::Kill " << Name() << " " << ThreadNo() << " " 
             << Automaton()->StateName( State() ) << " Threads " << _OutNode->Threads()
             << " SuspendedThreads " << _OutNode->SuspendedThreads()
             << " EventQSize " << _OutNode->EventQSize() << " returns " << RetVal << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::KillDone() {
  cdebug_in << "GraphExecutor::InNode::KillDone " << Name() << " " << ThreadNo()
            << endl;
  bool RetVal ;
  if ( ControlState() == SUPERV::ToKillDoneState || IsDone() ) {
    RetVal = false ;
  }
  else {
    ControlState( SUPERV::ToKillDoneState ) ;
    if ( IsDone() ) {
      if ( _OutNode->IsDone() ) {
        ControlState( SUPERV::VoidState ) ;
      }
      RetVal = false ;
    }
    else {
      if ( IsRunning() ) {
        RetVal = true ;
      }
      else if ( IsWaiting() ) {
        RetVal = true ;
      }
      else {
        cdebug << "KillDone and !IsDone and !IsRunning and !IsWaiting ?"
               << endl ;
        RetVal = false ;
      }
    }
  }
  cdebug_out << "GraphExecutor::InNode::KillDone" << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::Stop() {
  cdebug_in << "GraphExecutor::InNode::Stop " << Name() << " " << ThreadNo()
            << endl;
  bool RetVal ;
  if ( ControlState() == SUPERV::ToStopState || IsDone() ) {
    RetVal = false ;
  }
  else {
    ControlState( SUPERV::ToStopState ) ;
    if ( IsDone() ) {
      if ( _OutNode->IsDone() ) {
        ControlState( SUPERV::VoidState ) ;
      }
      RetVal = false ;
    }
    else {
      if ( IsRunning() ) {
        if ( IsFactoryNode() || IsComputingNode() ) {
          if ( !CORBA::is_nil( Component() ) ) {
            try {
              RetVal = Component()->Stop_impl() ;
	    }
            catch( ... ) {
              cdebug << "InNode::Stop() ERROR catched" << endl ;
              State( GraphExecutor::ErroredState ) ;
              _OutNode->State( GraphExecutor::ErroredState ) ;
              RetVal = false ;
	    }
            if ( RetVal ) {
              if ( IsRunning() ) {
                SendEvent( GraphExecutor::StopEvent ) ;
	      }
              else if ( IsDone() ) {
                ControlState( SUPERV::VoidState ) ;
                RetVal = false ; // Too late ...
	      }
              else {
                cdebug << "component Suspended and !IsDone and !IsRunning !"
                       << endl ;
	      }
	    }
	  }
          else {
            cdebug << "Suspend cannot Stop component ! Python Component ?" << endl ;
            RetVal = false ;
	  }
	}
        else {
          cdebug << "Suspend with nilComponent while RunningState !" << endl ;
          RetVal = false ;
        }
      }
      else if ( IsWaiting() ) {
        RetVal = true ;
      }
      else {
        cdebug << "Suspend and !IsDone and !IsRunning and !IsWaiting ?"
               << endl ;
        RetVal = false ;
      }
    }
  }
  cdebug_out << "GraphExecutor::InNode::Stop" << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::SuspendDone() {
  cdebug_in << "GraphExecutor::InNode::SuspendDone " << Name() << " "
            << ThreadNo() << endl;
  bool RetVal ;
  if ( ControlState() == SUPERV::ToSuspendDoneState || IsDone() ) {
    RetVal = false ;
  }
  else {
    ControlState( SUPERV::ToSuspendDoneState ) ;
    if ( IsDone() ) {
      if ( _OutNode->IsDone() ) {
        ControlState( SUPERV::VoidState ) ;
      }
      RetVal = false ;
    }
    else {
      RetVal = true ;
    }
  }
  cdebug_out << "GraphExecutor::InNode::SuspendDone" << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::Resume() {
  cdebug_in << pthread_self() << "/" << ThreadNo()
            << " GraphExecutor::InNode::Resume " << Name() << " "
            << Automaton()->StateName( State() ) << endl;
  bool RetVal = false ;
  if ( IsSuspended() ) {
    if ( State() == GraphExecutor::SuspendedReadyState ) {
      ResumeAction( GraphExecutor::ToResumeEvent ) ;
      RetVal = true ;
    }
    else if ( State() == GraphExecutor::SuspendedExecutingState ) {
      if ( IsFactoryNode() || IsComputingNode() ) {
        if ( pthread_mutex_lock( &_MutexWait ) ) {
          perror("ResumeAction pthread_mutex_lock ") ;
          exit( 0 ) ;
        }
        try {
          RetVal = Component()->Resume_impl() ;
          if ( RetVal ) {
            State( GraphExecutor::ExecutingState ) ;
	  }
	}
        catch( ... ) {
          cdebug << "InNode::Resume() ERROR catched" << endl ;
          State( GraphExecutor::ErroredState ) ;
          _OutNode->State( GraphExecutor::ErroredState ) ;
          RetVal = false ;
	}
        if ( pthread_mutex_unlock( &_MutexWait ) ) {
          perror("ResumeAction pthread_mutex_unlock ") ;
          exit( 0 ) ;
        }
      }
      else if ( IsMacroNode() ) {
        cdebug << "Suspend of MacroNode not yet implemented ? Trying" << endl ;
        GraphBase::Graph * aGraph = (GraphBase::Graph * ) GraphMacroNode()->CoupledNode() ;
        RetVal = aGraph->GraphEditor()->Executor()->Resume() ;
        if ( RetVal ) {
          State( GraphExecutor::ExecutingState ) ;
	}
      }
      else {
// Resume of InLinePythonNode in the Node of the SuperVisionContainer ...
	// asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
	State( GraphExecutor::ErroredState ) ;
	_OutNode->State( GraphExecutor::ErroredState ) ;
	RetVal = false ;
	cdebug << "Resume of InLine nodes is NOT implemented." << endl;
	MESSAGE( "Resume of InLine nodes is NOT implemented." );
	/*
        cdebug << ThreadNo() << "/" << pthread_self()
               << "Resume of InLineNode pthread_kill" << Name() << endl ;
        if ( pthread_kill( _OutNode->MainThreadId() , SIGCONT ) == -1 ) {
          perror("Resume pthread_kill error") ;
          State( GraphExecutor::ErroredState ) ;
          _OutNode->State( GraphExecutor::ErroredState ) ;
          RetVal = false ;
        }
        else {
          State( GraphExecutor::ExecutingState ) ;
          RetVal = true ;
        }
	*/
      }
    }
    else if ( State() == GraphExecutor::SuspendedSuccessedState ) {
      ResumeAction( GraphExecutor::ResumeEvent ) ;
      RetVal = true ;
    }
    else if ( State() == GraphExecutor::SuspendedErroredState ) {
      ResumeAction( GraphExecutor::ResumeEvent ) ;
      RetVal = true ;
    }
    else {
      cdebug << "GraphExecutor::InNode::Resume Not SuspendedReady/Executing/Successed/ErroredState "
             << Automaton()->StateName( State() ) << endl ;
      RetVal = false ;
    }
  }
  else {
    cdebug << "GraphExecutor::InNode::Resume Not Suspended State "
           << Automaton()->StateName( State() ) << endl ;
    RetVal = false ;
  }
  if ( ControlState() == SUPERV::ToSuspendStartState ) {
    ControlState( SUPERV::VoidState ) ;
  }

#if 0
  if ( ControlState() == SUPERV::ToSuspendRunState ||
       ( ControlState() == SUPERV::ToSuspendState &&
         State() == GraphExecutor::SuspendedReadyState) ) {
    if ( IsSuspended() ) {
      if ( State() == GraphExecutor::SuspendedReadyState ) {
        ResumeAction() ;
        RetVal = true ;
      }
      else if ( State() == GraphExecutor::SuspendedExecutingState ) {
        ResumeAction() ;
        RetVal = Component()->Resume_impl() ;
      }
      else {
        cdebug << "GraphExecutor::InNode::Resume State "
               << Automaton()->StateName( State() ) << endl ;
        RetVal = false ;
      }
      if ( ControlState() != SUPERV::ToSuspendState ) {
        ControlState( SUPERV::VoidState ) ;
      }
    }
    else if ( IsRunning() ) {
      RetVal = true ;
    }
    else if ( IsWaiting() ) {
      ControlState( SUPERV::VoidState ) ;
      RetVal = true ;
    }
    else if ( IsDone() ) {
      RetVal = true ;
    }
  }
  else if ( ControlState() == SUPERV::ToSuspendDoneState ||
            ( ControlState() == SUPERV::ToSuspendState &&
              State() == GraphExecutor::SuspendedSuccessedState) ) {
    if ( IsSuspended() ) {
      if ( State() == GraphExecutor::SuspendedSuccessedState ) {
        ResumeAction() ;
        RetVal = true ;
      }
      else if ( State() == GraphExecutor::SuspendedErroredState ) {
        ResumeAction() ;
        RetVal = true ;
      }
      else {
        cdebug << "GraphExecutor::InNode::Resume State " << State() << endl ;
        RetVal = false ;
      }
      if ( ControlState() != SUPERV::ToSuspendState ) {
        ControlState( SUPERV::VoidState ) ;
      }
    }
    else if ( IsRunning() ) {
      ControlState( SUPERV::VoidState ) ;
      RetVal = true ;
    }
    else if ( IsWaiting() ) {
      ControlState( SUPERV::VoidState ) ;
      RetVal = true ;
    }
    else if ( IsDone() ) {
      ControlState( SUPERV::VoidState ) ;
      RetVal = true ;
    }
  }
#endif
  cdebug_out << "GraphExecutor::InNode::Resume " << Name() << " " << RetVal << " "
             << Automaton()->StateName( State() ) << endl ;
  return RetVal ;
}

bool GraphExecutor::InNode::IsWaiting() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsWaiting " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
  if ( aState == GraphExecutor::DataUndefState ||
       aState == GraphExecutor::DataWaitingState ||
       aState == GraphExecutor::SuspendedReadyState )
//       aState == GraphExecutor::SuspendedExecutingState ||
//       aState == GraphExecutor::SuspendedSuccessedState ||
//       aState == GraphExecutor::SuspendedErroredState ||
//       aState == GraphExecutor::SuspendedState
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsWaiting" << endl ;
  return aret ;
}

bool GraphExecutor::InNode::IsReady() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsReady " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
//  if ( aState == GraphExecutor::DataUndefState ||
//       aState == GraphExecutor::DataWaitingState ||
  if ( aState == GraphExecutor::DataReadyState ||
       aState == GraphExecutor::ResumedReadyState )
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsReady" << endl ;
  return aret ;
}

bool GraphExecutor::InNode::IsRunning() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsRunning " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
  if ( aState == GraphExecutor::ExecutingState ||
       aState == GraphExecutor::ResumedExecutingState )
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsRunning" << endl ;
  return aret ;
}

bool GraphExecutor::InNode::IsDone() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsDone " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
  if ( aState == GraphExecutor::KilledReadyState ||
       aState == GraphExecutor::StoppedReadyState ||
       aState == GraphExecutor::KilledExecutingState ||
       aState == GraphExecutor::StoppedExecutingState ||
       aState == GraphExecutor::SuspendedSuccessedState ||
       aState == GraphExecutor::SuspendedErroredState ||
//       aState == GraphExecutor::SuccessedExecutingState ||
//       aState == GraphExecutor::ErroredExecutingState ||
       aState == GraphExecutor::SuccessedState ||
       aState == GraphExecutor::ErroredState ||
       aState == GraphExecutor::ResumedSuccessedState ||
       aState == GraphExecutor::ResumedErroredState ||
       aState == GraphExecutor::KilledSuccessedState ||
       aState == GraphExecutor::StoppedSuccessedState )
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsDone" << endl ;
  return aret ;
}

bool GraphExecutor::InNode::IsSuspended() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsSuspended " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
  if ( aState == GraphExecutor::SuspendedReadyState ||
       aState == GraphExecutor::SuspendedExecutingState ||
       aState == GraphExecutor::SuspendedSuccessedState ||
       aState == GraphExecutor::SuspendedErroredState ||
       aState == GraphExecutor::SuspendedState )
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsSuspended" << endl ;
  return aret ;
}
bool GraphExecutor::InNode::IsKilled() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsKilled " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
  if ( aState == GraphExecutor::KilledReadyState ||
       aState == GraphExecutor::KilledExecutingState ||
       aState == GraphExecutor::KilledSuccessedState ||
       aState == GraphExecutor::KilledErroredState ||
       aState == GraphExecutor::KilledState )
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsKilled" << endl ;
  return aret ;
}
bool GraphExecutor::InNode::IsStopped() {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::InNode::IsStopped " << Name() << endl;
  GraphExecutor::AutomatonState aState = State() ;
  if ( aState == GraphExecutor::StoppedReadyState ||
       aState == GraphExecutor::StoppedExecutingState ||
       aState == GraphExecutor::StoppedSuccessedState ||
       aState == GraphExecutor::StoppedErroredState ||
       aState == GraphExecutor::StoppedState )
    aret = true ;
//  cdebug_out << "GraphExecutor::InNode::IsStopped" << endl ;
  return aret ;
}

bool GraphExecutor::InNode::StateWait( SUPERV::GraphState aState ) {
  bool RetVal = false ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _Wait") ;
    exit( 0 ) ;
  }
  switch ( aState ) {
  case SUPERV::ReadyState : {
    RetVal = IsReady() ;
    cdebug_in << pthread_self() << " StateWait( Ready ) " << RetVal
              << " " << Automaton()->StateName( _currentState )
              << " pthread_cond_wait _ReadyWait " << Name() << endl ;
    while ( !RetVal && !IsDone() ) {
      cdebug << pthread_self() << " pthread_cond_wait ReadyWait" << endl ;
      pthread_cond_wait( &_ReadyWait , &_MutexWait );
      RetVal = IsReady() ;
      cdebug << pthread_self() << " pthread_cond_waited ReadyWait "
             << Automaton()->StateName( _currentState ) << " " << RetVal
             << endl ;
    }
    cdebug_out << pthread_self() << " StateWait( Ready ) " << RetVal
               << " " << Automaton()->StateName( _currentState )
               << " pthread_cond_wait _ReadyWait " << Name() << endl ;
    break ;
  }
  case SUPERV::RunningState : {
    RetVal = IsRunning() ;
    cdebug_in << pthread_self() << " StateWait( Running ) " << RetVal
              << " " << Automaton()->StateName( _currentState )
              << " pthread_cond_wait _RunningWait " << Name() << endl ;
    // mkr : IPAL10056 : additional checking for node aborted
    while ( !RetVal && !IsDone() && !_OutNode->IsNodeAborted() ) {
      cdebug << pthread_self() << " pthread_cond_wait RunningWait " << Name() << endl ;
      pthread_cond_wait( &_RunningWait , &_MutexWait );
//We may have pthread_cond_waited but !IsRunning and !IsDone :
      RetVal = IsRunning() || State() == GraphExecutor::SuccessedExecutingState ||
	       State() == GraphExecutor::ErroredExecutingState ;
      cdebug << pthread_self() << " pthread_cond_waited RunningWait "
	     << Automaton()->StateName( _currentState ) << " " << RetVal
	     << " " << Name() << endl ;
    }
    cdebug_out << pthread_self() << " StateWait( Running ) " << RetVal
               << " " << Automaton()->StateName( _currentState )
               << " pthread_cond_wait _RunningWait " << Name() << endl ;
    break ;
  }
  case SUPERV::DoneState : {
    RetVal = IsDone() ;
    cdebug_in << pthread_self() << " StateWait( Done ) " << RetVal
              << " " << Automaton()->StateName( _currentState )
              << " pthread_cond_wait _DoneWait " << Name() << endl ;
    while ( !RetVal ) {
      cdebug << pthread_self() << " pthread_cond_wait DoneWait" << endl ;
      pthread_cond_wait( &_DoneWait , &_MutexWait );
      RetVal = IsDone() ;
      cdebug << pthread_self() << " pthread_cond_waited DoneWait "
             << Automaton()->StateName( _currentState ) << " " << RetVal
             << endl ;
    }
    cdebug_out << pthread_self() << " StateWait( Done ) " << RetVal
               << " " << Automaton()->StateName( _currentState )
               << " pthread_cond_wait _DoneWait " << Name() << endl ;
    break ;
  }
  case SUPERV::SuspendState : {
    RetVal = IsSuspended() ;
    cdebug_in << pthread_self() << " StateWait( Suspend ) " << RetVal
              << " " << Automaton()->StateName( _currentState )
              << " pthread_cond_wait _SuspendedWait " << Name() << endl ;
    while ( !RetVal && !IsDone() ) {
      cdebug << pthread_self() << " pthread_cond_wait SuspendedWait" << endl ;
      pthread_cond_wait( &_SuspendedWait , &_MutexWait );
      RetVal = IsSuspended() ;
      cdebug << pthread_self() << " pthread_cond_waited SuspendedWait "
             << Automaton()->StateName( _currentState ) << " " << RetVal
             << endl ;
    }
    cdebug_out << pthread_self() << " StateWait( Suspend ) " << RetVal
               << " " << Automaton()->StateName( _currentState )
               << " pthread_cond_wait _SuspendedWait " << Name() << endl ;
    break ;
  }
  default : {
    cdebug << " SUPERV::OutNode::StateWait Error Undefined State : "
           << aState << endl ;
  }
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _Wait") ;
    exit( 0 ) ;
  }
  return RetVal ;
}

bool GraphExecutor::InNode::ReadyWait() {
//  cdebug_in << "GraphExecutor::InNode::ReadyWait " << Name() << endl;
  bool aret ;
  aret = StateWait( SUPERV::ReadyState ) ;
//  cdebug_out << "GraphExecutor::InNode::ReadyWait" << endl ;
  return aret ;
}

bool GraphExecutor::InNode::RunningWait() {
  cdebug_in << pthread_self() << "GraphExecutor::InNode::RunningWait " << Name()
            << " " << Automaton()->StateName( State() ) << endl;
  bool aret ;
  aret = StateWait( SUPERV::RunningState ) ;
  cdebug_out << pthread_self() << "GraphExecutor::InNode::RunningWait " << Name()
             << " " << Automaton()->StateName( State() ) << endl;
  return aret ;
}

bool GraphExecutor::InNode::DoneWait() {
//  cdebug_in << "GraphExecutor::InNode::DoneWait " << Name() << endl;
  bool aret ;
  aret = StateWait( SUPERV::DoneState ) ;
  return aret ;
}

bool GraphExecutor::InNode::SuspendedWait() {
//  cdebug_in << "GraphExecutor::InNode::SuspendedWait " << Name() << endl;
  bool aret ;
  aret = StateWait( SUPERV::SuspendState ) ;
  return aret ;
}

void GraphExecutor::InNode::InitialState()
{
  cdebug_in << "GraphExecutor::InNode::InitialState Node " << Name() << endl;

  int i;
  _ControlState = SUPERV::VoidState ;
  CreateNewThread( false ) ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//  CreateNewThreadIf( false ) ;
  HasAllDataReady( false ) ;
  _SuspendSync = false ;
  _ResumeSync = false ;
  _InitLoop = false ;

  // asv : 13.12.04 : Decided to set "Loading" state for factory and computing nodes ONLY.
  //                  See extended comment in p.2.19 of "Bugs and Improvements" about IsLoading for InLine.
  if ( IsComputingNode() || IsFactoryNode() ) {
    IsLoading( true ) ;
  }

//  ThreadNo( pthread_self() ) ;
  ThreadNo( 0 ) ;

  for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
    if ( GetNodeOutPort(i)->IsDataStream() ) {
      GetChangeNodeOutPort(i)->PortState(  SUPERV::ReadyState ) ;
      GetChangeNodeOutPort(i)->PortDone( true ) ;
    }
//JR Debug 01.07.2005 :
//    else if ( i != 0 || !IsGOTONode() ) {
    else {
      GetChangeNodeOutPort(i)->PortState(  SUPERV::WaitingState ) ;
      GetChangeNodeOutPort(i)->PortDone( false ) ;
    }
  }

  int InPortsCount = GetNodeInPortsSize() ;
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GraphBase::InPort * anInPort = GetChangeNodeInPort(i) ;
    anInPort->PortState( SUPERV::WaitingState ) ;
    GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
    if ( IsHeadNode() && IsLoopNode() && anInPort->IsLoop() ) {
      anOutPort->PortStatus( DataConnected );
      anOutPort->PortState( SUPERV::ReadyState ) ;
      anOutPort->PortDone( true ) ;
//JR 21.02.2005 Debug Memory leak :      CORBA::Any * anAny = new CORBA::Any() ;
      CORBA::Any anAny = CORBA::Any() ;
//JR 21.02.2005 Debug Memory leak :      *anAny <<= (CORBA::Long ) 1 ;
      anAny <<= (CORBA::Long ) 1 ;
      anOutPort->SetValue( anAny ) ;
      _InitLoop = true ;
      cdebug << "InPort" << i << " " << anInPort->PortName() << " " << anInPort->PortStatus()
             << " OutPort " << anOutPort->PortStatus() << theAutomaton->StateName( anOutPort->PortState() )
             << " InitLoop HeadNode" << endl ;
    }
// JR 15_09_2004 if backward link from GOTONode or EndLoopNode ==> DataConnected
    else if ( anInPort->IsGate() && anOutPort ) {
      anOutPort->PortState( SUPERV::WaitingState ) ;
      anOutPort->PortDone( false ) ;
      const GraphBase::ComputingNode * aFromNode =  _OutNode->Graph()->GetGraphNode( anOutPort->NodeName() ) ; 
//JR      if ( aFromNode->IsGOTONode() || aFromNode->IsEndLoopNode() ) {
      if ( aFromNode->IsGOTONode() || ( IsLoopNode() && CoupledNode() == aFromNode ) ) {
// ASV: bug with synchronization of Inline nodes (via Gate ports) fixed.  
// before was "else if ( IsOneOfInlineNodes() )"
// IsOneOfInline() == ( Inline || IsOneOfGOTO() ), so Inline are removed..
        anOutPort->PortStatus( DataConnected );
        anOutPort->PortState( SUPERV::ReadyState ) ;
        anOutPort->PortDone( true ) ;
      }
      cdebug << "InPort" << i << " " << anInPort->PortName() << " " << anInPort->PortStatus()
             << " OutPort " << anOutPort->PortStatus() << theAutomaton->StateName( anOutPort->PortState() )
             << " Gate HeadNode" << endl ;
    }
    else {
      cdebug << Name() << " IsHeadNode " << IsHeadNode() << " InPort" << i << " "
             << anInPort->PortName() << " " << anInPort->PortStatus() << endl ;
    }
    if ( anInPort->IsGate() && anOutPort == NULL ) {
      InPortsCount-- ;
      cdebug << "InPort" << i << " " << anInPort->PortName() << " Not connected InPortsCount "
             << InPortsCount << endl ;
    }
    else if ( anOutPort ) {
      if ( anOutPort->IsDataConnected() || anOutPort->IsDataStream() ) {
        InPortsCount-- ;
        anOutPort->PortState( SUPERV::ReadyState ) ;
        anOutPort->PortDone( true ) ;
        cdebug << "InPort" << i << " " << anInPort->PortName() << " " << anInPort->PortStatus()
               << " " << theAutomaton->StateName( anOutPort->PortState() ) << " InPortsCount "
               << InPortsCount << endl ;
      }
      else if ( anOutPort->IsPortConnected() ) {
        anOutPort->PortState( SUPERV::WaitingState ) ;
        anOutPort->PortDone( false ) ;
        cdebug << "InPort" << i << " " << anInPort->PortName() << " " << " "
               << anInPort->PortStatus()
               << " " << theAutomaton->StateName( anOutPort->PortState() ) << " InPortsCount "
               << InPortsCount << endl ;
      }
      else {
        cdebug << "InPort" << i << " " << anInPort->PortName() << " " << anInPort->PortStatus()
               << " OutPort " << anOutPort->NodeName() << " " << anOutPort->PortName() << " "
               << theAutomaton->StateName( anOutPort->PortState() ) << " InPortsCount "
               << InPortsCount << endl ;
      }
    }
    else {
      cdebug << "InPort" << i << " " << anInPort->PortName() << " " << " " << anInPort->PortStatus()
             << " no corresponding OutPort InPortsCount " << InPortsCount << endl ;
    }
    if ( anOutPort ) {
      if ( !anOutPort->IsDataStream() || anInPort->IsDataStream() ) {
        cdebug << "InPort" << i << " state change : " << anInPort->PortName() << " from OutPort "
               << anOutPort->PortName() << " from Node " << anOutPort->NodeName()
               << " with state of OutPort : " << theAutomaton->StateName( anOutPort->PortState() ) << endl ;
        GetChangeNodeInPort(i)->PortState( anOutPort->PortState() ) ;
      }
      else if ( anOutPort->IsDataConnected() ) {
        cdebug << "InPort" << i << " state change : " << anInPort->PortName() << " from OutPort "
               << anOutPort->PortName() << " from Node " << anOutPort->NodeName()
               << " with state ReadyState" << endl ;
        GetChangeNodeInPort(i)->PortState( SUPERV::ReadyState ) ;
      }
      else {
        cdebug << "InPort" << i << " state NOT changed : " << anInPort->PortName() << " from OutPort "
               << anOutPort->PortName() << " " << anOutPort->PortStatus() << " from Node " << anOutPort->NodeName()
               << " with state " << theAutomaton->StateName( anOutPort->PortState() ) << endl ;
      }
    }
    if ( anOutPort ) {
      cdebug << "InPort" << i << " : " << anInPort->PortName() << " from OutPort "
             << anOutPort->PortName() << " from Node " << anOutPort->NodeName()
             << " with state " ;
      if ( anOutPort->PortState() == SUPERV::WaitingState ) {
        cdebug << "WaitingState" ;
      }
      else if ( anOutPort->PortState() == SUPERV::ReadyState ) {
        cdebug << "ReadyState" ;
      }
      else {
        cdebug << "???" ;
      }
      cdebug << " OutPortStatus " << anOutPort->PortStatus() << " State "
             << theAutomaton->StateName( anOutPort->PortState() ) << endl ;
    }
  }

  _currentState = InPortsCount > 0 ? GraphExecutor::DataWaitingState 
                         : GraphExecutor::DataReadyState ;
  if ( InPortsCount == GetNodeInPortsSize() ) {
    _OutNode->PushEvent( this , GraphExecutor::NoDataReadyEvent ,
                         _currentState ) ; 
  }
  else if ( InPortsCount != 0 ) {
    _OutNode->PushEvent( this , GraphExecutor::SomeDataReadyEvent ,
                         _currentState ) ; 
  }
  else {
    _OutNode->PushEvent( this , GraphExecutor::AllDataReadyEvent ,
                         _currentState ) ; 
  }

  for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
    cdebug << "OutPort" << i << " : " << GetNodeOutPort(i)->PortName() << " "
           << theAutomaton->StateName( GetChangeNodeOutPort(i)->PortState() )
           << " " << GetNodeOutPort(i)->Kind() << endl ;
  }

  cdebug_out << "GraphExecutor::InNode::InitialState Node " << Name() << " CurrentState = "
             << theAutomaton->StateName( _currentState ) << " HasAllDataReady "
             << HasAllDataReady() << endl;
}

bool GraphExecutor::InNode::InitPythonFunctions(bool WithErr ) {
  //JR Look at DataFlowExecutor for the meaning of WithErr
  cdebug_in << "GraphExecutor::InNode::InitPythonFunctions " << Name() << " WithErr " << WithErr
            << " PyFuncRunned() " << PyFuncRunned() << endl;
  bool Err = false ;
  bool OneErr ;
  if ( !PyFuncRunned() && IsOneOfInLineNodes() ) {
    if ( IsLoopNode() ) {
      PyObject * PyRunMethod = InLineNode()->PyRunMethod() ;
      PyObject * PyMoreMethod = NULL ;
      PyObject * PyNextMethod = NULL ;
      if ( PyRunMethod ) {
        OneErr = false ;
      }
      else {
        PyRunMethod = InitPyDynInvoke( InLineNode()->PyFuncName() ,
                                       InLineNode()->PythonFunction() ,
                                       OneErr ) ;
	// JR Debug : if we must have a python function and if we have not : error
        if ( PyRunMethod ) {
          InLineNode()->PyRunMethod( PyRunMethod ) ;
	}
        else if ( WithErr && strlen( InLineNode()->PyFuncName() ) ) {
          OneErr = true ;
	}
      }
      Err = Err || OneErr ;
      if ( OneErr && WithErr ) {
        string anErrorMessage = string( "Error while declaring the Python function " ) +
                                string( LoopNode()->PyFuncName() ) + string( " in Node " ) +
                                string( Name() ) ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      }
      PyMoreMethod = LoopNode()->PyMoreMethod() ;
      if ( PyMoreMethod ) {
        OneErr = false ;
      }
      else {
        PyMoreMethod = InitPyDynInvoke( LoopNode()->PyMoreName() ,
                                        LoopNode()->MorePythonFunction() ,
                                        OneErr ) ;
	// JR Debug : if we must have a python function and if we have not : error
        if ( PyMoreMethod ) {
          LoopNode()->PyMoreMethod( PyMoreMethod ) ;
	}
        else if ( WithErr && strlen( LoopNode()->PyMoreName() ) ) {
          OneErr = true ;
	}
      }
      Err = Err || OneErr ;
      if ( OneErr && WithErr ) {
        string anErrorMessage = string( "Error while declaring the Python function " ) +
                                string( LoopNode()->PyMoreName() ) + string( " in Node " ) +
                                string( Name() ) ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      }
      PyNextMethod = LoopNode()->PyNextMethod() ;
      if ( PyNextMethod ) {
        OneErr = false ;
      }
      else {
        PyNextMethod = InitPyDynInvoke( LoopNode()->PyNextName() ,
                                        LoopNode()->NextPythonFunction() ,
                                        OneErr ) ;
	// JR Debug : if we must have a python function and if we have not : error
        if ( PyNextMethod ) {
          LoopNode()->PyNextMethod( PyNextMethod ) ;
	}
        else if ( WithErr && strlen( LoopNode()->PyNextName() ) ) {
          OneErr = true ;
	}
      }
      Err = Err || OneErr ;
      if ( OneErr && WithErr ) {
        string anErrorMessage = string( "Error while declaring the Python function " ) +
                                string( LoopNode()->PyNextName() ) + string( " in Node " ) +
                                string( Name() ) ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      }
      //JR Debug 12854
      if ( Err ) {
        PyFuncRunned( false ) ;
      }
      cdebug << "GraphExecutor::InNode::InitPythonFunctions " << Name() << " PyRunMethod(Init) "
             << PyRunMethod << " PyMoreMethod " << PyMoreMethod << " PyNextMethod " << PyNextMethod
             << endl;
    }
    else if ( IsInLineNode() || IsSwitchNode() ) {
      PyObject * PyRunMethod = InLineNode()->PyRunMethod() ;
      if ( PyRunMethod ) {
        OneErr = false ;
      }
      else {
	PyRunMethod = InitPyDynInvoke( InLineNode()->PyFuncName() ,
                                       InLineNode()->PythonFunction() ,
                                       OneErr ) ;
	// JR Debug : if we must have a python function and if we have not : error
        if ( PyRunMethod != NULL ) {
          InLineNode()->PyRunMethod( PyRunMethod ) ;
	}
        else if ( WithErr && strlen( InLineNode()->PyFuncName() ) ) {
          OneErr = true ;
	}
      }
      Err = Err || OneErr ;
      if ( OneErr && WithErr ) {
        string anErrorMessage = string( "Error while declaring the Python function " ) +
                                string( InLineNode()->PyFuncName() ) + string( " in Node " ) +
                                string( Name() ) ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      }
      cdebug << "GraphExecutor::InNode::InitPythonFunctions " << Name() << " PyRunMethod " << PyRunMethod << endl;
    }
    else if ( ( IsEndLoopNode() || IsEndSwitchNode() || IsGOTONode() ) &&
              (*InLineNode()->PythonFunction()).length() ) {
      PyObject * PyRunMethod = InLineNode()->PyRunMethod() ;
      if ( PyRunMethod ) {
        OneErr = false ;
      }
      else {
        PyRunMethod = InitPyDynInvoke( InLineNode()->PyFuncName() ,
                                       InLineNode()->PythonFunction() ,
                                       OneErr ) ;
	// JR Debug : if we must have a python function and if we have not : error
        if ( PyRunMethod != NULL ) {
          InLineNode()->PyRunMethod( PyRunMethod ) ;
	}
        else if ( WithErr && strlen( InLineNode()->PyFuncName() ) ) {
          OneErr = true ;
	}
      }
      Err = Err || OneErr ;
      if ( OneErr && WithErr ) {
        string anErrorMessage = string( "Error while declaring the Python function " ) +
                                string( InLineNode()->PyFuncName() ) + string( " in Node " ) +
                                string( Name() )  ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      }
      cdebug << "GraphExecutor::InNode::InitPythonFunctions " << Name() << " PyRunMethod " << PyRunMethod << endl;
    }
  }
  //JR Look at DataFlowExecutor :
  //  Err = WithErr && Err ;
  cdebug_out << "GraphExecutor::InNode::InitPythonFunctions " << Name() ;
  if ( WithErr && Err ) {
    cdebug << " Error " << WithErr && Err ;
  }
  cdebug << endl;
  return !Err ;
}

const long GraphExecutor::InNode::CpuUsed( bool tot ) {
  CORBA::Long cpu = 0 ;
//  cdebug_in << "GraphExecutor::InNode::CpuUsed( " << tot << " ) " << Name() << endl ;
//  cout << "Begin CpuUsed " << Name() << " CpuUsed : " << cpu << " State "
//       << theAutomaton->StateName( _currentState ) << endl ;
  if ( IsOneOfInLineNodes() ) {
//    cdebug << "CpuUsed " << Name() << " --> PyCpuUsed()" << endl ;
//    cout << "CpuUsed " << Name() << " --> PyCpuUsed()" << endl ;
    cpu = PyCpuUsed( tot ) ;
  }
  else {
    if ( !CORBA::is_nil( Component() ) ) {
//      cdebug << "CpuUsed " << Name() << " --> Component()->CpuUsed_impl()" << endl ;
//      cout << "CpuUsed " << Name() << " --> Component()->CpuUsed_impl()" << endl ;
      try {
        cpu = Component()->CpuUsed_impl() ;
      }
      catch ( ... ) {
        if ( _OutNode->IsDone() ) {
          cdebug << "CpuUsed " << Name() << " --> Component()->CpuUsed_impl() WARNING catched Graph is done "
                 << Automaton()->StateName( _OutNode->State() ) << endl ;
        }
        else {
          cdebug << "CpuUsed " << Name() << " --> Component()->CpuUsed_impl() WARNING catched "
                 << Automaton()->StateName( _OutNode->State() ) << endl ;
//JR NPAL14110 09.02.2007 : If there is a shutdown of components ===> NodeState must not
//                          be aborted ... ==> Commented :
          //JRState( GraphExecutor::ErroredState ) ;
          //JR_OutNode->State( GraphExecutor::ErroredState ) ;
        }
        cpu = -1 ;
      }
    }
    else {
      cdebug << "CpuUsed " << Name() << " Component() is NIL" << endl ;
      cpu = -1 ;
    }
  }
//  cdebug_out << "GraphExecutor::InNode::CpuUsed " << Name() << " CpuUsed : " << cpu << endl ;
//  cout << "End CpuUsed " << Name() << " CpuUsed : " << cpu << " State "
//       << theAutomaton->StateName( _currentState ) << endl ;
  return cpu ;
}

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

long GraphExecutor::InNode::PyCpu() {
  struct rusage usage ;
  long cpu ;
  if ( getrusage( RUSAGE_SELF , &usage ) == -1 ) {
    perror("GraphExecutor::InNode::PyCpu") ;
    return 0 ;
  }
//  return usage.ru_utime.__time_t tv_sec ;
//  cdebug << pthread_self() << "PyCpu " << Name() << " " << usage.ru_utime.tv_sec << " "
//         << usage.ru_utime.tv_usec << " " << usage.ru_stime.tv_sec << " " << usage.ru_stime.tv_usec
//         << endl ;
  cpu = usage.ru_utime.tv_sec ;
  return cpu ;
}

long GraphExecutor::InNode::PyCpuUsed( bool tot ) {
  long cpu ;
  if ( _PyTotCpuUsed == -1 ) {
    if ( _Pythread == pthread_self() ) {
//      cdebug << pthread_self() << "GraphExecutor::InNode::PyCpuUsed(" << tot << ") " << Name()
//             << " _PyTotCpuUsed " <<  _PyTotCpuUsed << " PyCpu() " << PyCpu() << " - " << " _PyCpuUsed "
//             << _PyCpuUsed << endl ;
      cpu = PyCpu() - _PyCpuUsed ;
      if ( tot ) {
        _PyTotCpuUsed = cpu ;
      }
    }
    else {
      cpu = 0 ;
    }
  }
  else {
    cpu = _PyTotCpuUsed ;
  }
//  cdebug << pthread_self() << "GraphExecutor::InNode::PyCpuUsed(" << tot << ") " << Name() << "_PyTotCpuUsed"
//         <<  _PyTotCpuUsed << " CpuUsed : " << cpu << endl ;
  return cpu ;
}

void GraphExecutor::InNode::SetPyCpuUsed() {
  _PyTotCpuUsed = -1 ;
  _PyCpuUsed = 0 ;
  _Pythread = pthread_self() ;
  _PyCpuUsed = PyCpu() ;
//  cdebug << pthread_self() << "GraphExecutor::InNode::SetPyCpuUsed " << Name() << " _PyCpuUsed : "
//         << _PyCpuUsed << endl ;
}

void GraphExecutor::InNode::IsLoading( bool Loading ) {
  _Loading = Loading ;
  
  // asv : 09.12.04 : "Bugs and Improvents" 2.19 : how it works: 
  // LoadingState is returned by OutNode::State( NodeName ) if InNode->IsLoading()
  // after Loading is finished (here below), ExecutingState must be pushed for GUI.  
  if ( !Loading )
    _OutNode->PushEvent( this, GraphExecutor::ExecuteEvent, GraphExecutor::ExecutingState );
}
     

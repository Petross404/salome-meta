//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowExecutor_DataFlow.lxx
//  Module : SUPERV

#ifndef _DATAFLOWEXECUTOR_DATAFLOW_LXX
#define _DATAFLOWEXECUTOR_DATAFLOW_LXX

#include "DataFlowExecutor_OutNode.hxx"

inline bool GraphExecutor::DataFlow::LoadDataFlow( const GraphBase::SGraph * aDataFlow ) {
  return GraphExecutor::OutNode::LoadDataFlow( aDataFlow ) ; 
}

//inline bool GraphExecutor::DataFlow::LoadXml( const char* myFileName ) {
//  return GraphExecutor::OutNode::LoadXml( myFileName ) ; 
//}

inline const SALOME_ModuleCatalog::Service * GraphExecutor::DataFlow::NodeService( const char * aNodeName ) {
  if ( Graph()->GetGraphNode( aNodeName ) )
    return Graph()->GetGraphNode( aNodeName )->GetService() ;
  return NULL ;
}

inline bool GraphExecutor::DataFlow::AddInputData( const char* ToNodeName ,
                                                   const char* ToParameterName ,
                                                   const CORBA::Any aValue ) {
  if ( !IsValid() )
    return false ;
  return Graph()->AddInputData( ToNodeName , ToParameterName , aValue ) ; 
} ;

inline bool GraphExecutor::DataFlow::ChangeInputData( const char* ToNodeName ,
                                                      const char* ToParameterName ,
                                                      const CORBA::Any aValue ) {
  if ( !IsValid() )
    return false ;
  return Graph()->ChangeInputData( ToNodeName , ToParameterName , aValue ) ; 
} ;

inline bool GraphExecutor::DataFlow::IsValid() {
  return GraphExecutor::OutNode::IsValid() ;
}

inline bool GraphExecutor::DataFlow::IsExecutable() {
  return GraphExecutor::OutNode::IsExecutable() ;
}

inline bool GraphExecutor::DataFlow::Run( const bool AndSuspend ) {
  return GraphExecutor::OutNode::Run( AndSuspend ) ;
}

inline long GraphExecutor::DataFlow::LastLevelDone() {
  return GraphExecutor::OutNode::LastLevelDone() ;
}

inline long GraphExecutor::DataFlow::Threads() {
  return GraphExecutor::OutNode::Threads() ;
}

inline bool GraphExecutor::DataFlow::Event( char ** aNodeName ,
                                            SUPERV::GraphEvent & anEvent ,
                                            SUPERV::GraphState & aState ,
                                            bool WithWait ) {
  return GraphExecutor::OutNode::Event( aNodeName , anEvent , aState , WithWait ) ;
}
inline bool GraphExecutor::DataFlow::EventW( char ** aNodeName ,
                                             SUPERV::GraphEvent & anEvent ,
                                             SUPERV::GraphState & aState ) {
  return GraphExecutor::OutNode::EventW( aNodeName , anEvent , aState ) ;
}
inline long GraphExecutor::DataFlow::EventQSize() {
  return GraphExecutor::OutNode::EventQSize() ;
}

inline void GraphExecutor::DataFlow::State(GraphExecutor::AutomatonState aState ) {
  GraphExecutor::OutNode::State( aState ) ;
}

inline SUPERV::GraphState GraphExecutor::DataFlow::State() {
  return GraphExecutor::OutNode::State() ;
}

inline SUPERV::GraphState GraphExecutor::DataFlow::State(
                               const char * aNodeName ) {
  return GraphExecutor::OutNode::State( aNodeName ) ;
}

inline SUPERV::GraphState GraphExecutor::DataFlow::State(
                               const char * aNodeName ,
                               const char * anOutServiceParameterName ) {
  return GraphExecutor::OutNode::State( aNodeName ,
                                        anOutServiceParameterName ) ;
}

inline long GraphExecutor::DataFlow::Thread() {
  return GraphExecutor::OutNode::ThreadNo() ;
}
inline long GraphExecutor::DataFlow::Thread( const char * aNodeName ) {
  return GraphExecutor::OutNode::Thread( aNodeName ) ;
}

inline GraphExecutor::AutomatonState GraphExecutor::DataFlow::AutomatonState() {
  return GraphExecutor::OutNode::AutomatonState() ;
}

inline GraphExecutor::AutomatonState GraphExecutor::DataFlow::AutomatonState(
                               const char * aNodeName ) {
  return GraphExecutor::OutNode::AutomatonState( aNodeName ) ;
}

inline SUPERV::ControlState GraphExecutor::DataFlow::ControlState() {
  return GraphExecutor::OutNode::ControlState() ;
}

inline SUPERV::ControlState GraphExecutor::DataFlow::ControlState(
                               const char * aNodeName ) {
  return GraphExecutor::OutNode::ControlState( aNodeName ) ;
}

inline void GraphExecutor::DataFlow::ControlClear() {
  return GraphExecutor::OutNode::ControlClear() ;
}

inline void GraphExecutor::DataFlow::ControlClear(
                               const char * aNodeName ) {
  return GraphExecutor::OutNode::ControlClear( aNodeName ) ;
}

inline bool GraphExecutor::DataFlow::IsWaiting() {
  return GraphExecutor::OutNode::IsWaiting() ;
}
inline bool GraphExecutor::DataFlow::IsReady() {
  return GraphExecutor::OutNode::IsReady() ;
}
inline bool GraphExecutor::DataFlow::IsRunning() {
  return GraphExecutor::OutNode::IsRunning() ;
}
inline bool GraphExecutor::DataFlow::IsDone() {
  return GraphExecutor::OutNode::IsDone() ;
}
inline bool GraphExecutor::DataFlow::IsSuspended() {
  return GraphExecutor::OutNode::IsSuspended() ;
}

inline bool GraphExecutor::DataFlow::IsWaiting(const char * aNodeName ) {
  return GraphExecutor::OutNode::IsWaiting( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::IsReady(const char * aNodeName ) {
  return GraphExecutor::OutNode::IsReady( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::IsRunning(const char * aNodeName ) {
  return GraphExecutor::OutNode::IsRunning( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::IsDone(const char * aNodeName ) {
  return GraphExecutor::OutNode::IsDone( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::IsSuspended(const char * aNodeName ) {
  return GraphExecutor::OutNode::IsSuspended( aNodeName ) ;
}

inline bool GraphExecutor::DataFlow::PortDone( const char * aNodeName ,
                                               const char * anOutServiceParameterName ) {
  return GraphExecutor::OutNode::PortDone( aNodeName ,
                                           anOutServiceParameterName ) ;
}

inline bool GraphExecutor::DataFlow::ReadyWait() {
  return GraphExecutor::OutNode::ReadyWait() ;
}
inline bool GraphExecutor::DataFlow::RunningWait() {
  return GraphExecutor::OutNode::RunningWait() ;
}
inline bool GraphExecutor::DataFlow::DoneWait() {
  return GraphExecutor::OutNode::DoneWait() ;
}
inline bool GraphExecutor::DataFlow::SuspendedWait() {
  return GraphExecutor::OutNode::SuspendedWait() ;
}

inline bool GraphExecutor::DataFlow::ReadyWait(const char * aNodeName ) {
  return GraphExecutor::OutNode::ReadyWait( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::RunningWait(const char * aNodeName ) {
  return GraphExecutor::OutNode::RunningWait( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::DoneWait(const char * aNodeName ) {
  return GraphExecutor::OutNode::DoneWait( aNodeName ) ;
}
inline bool GraphExecutor::DataFlow::SuspendedWait(const char * aNodeName ) {
  return GraphExecutor::OutNode::SuspendedWait( aNodeName ) ;
}

//JR 30.03.2005inline const CORBA::Any *GraphExecutor::DataFlow::GetInData(
inline const CORBA::Any GraphExecutor::DataFlow::GetInData(
                                   const char * ToNodeName ,
                                   const char * ToParameterName ) {
  return GraphExecutor::OutNode::GetInData( ToNodeName ,
                                            ToParameterName ) ;
}
//JR 30.03.2005inline const CORBA::Any *GraphExecutor::DataFlow::GetOutData(
inline const CORBA::Any GraphExecutor::DataFlow::GetOutData(
                                   const char * FromNodeName ,
                                   const char * FromParameterName ) {
  return GraphExecutor::OutNode::GetOutData( FromNodeName ,
                                             FromParameterName ) ;
}
#endif




//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_OutNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;

#include "DataFlowExecutor_OutNode.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

// asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
//extern Engines_Container_i * _TheContainer ;

extern GraphExecutor::FiniteStateMachine * theAutomaton ;

// static const char *ComponentName = "SalomeSuperVisionComponent" ;

extern int _ArgC ;
extern char ** _ArgV ;

GraphExecutor::OutNode::OutNode() {
//               Graph() {
  // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
  //_SuperVisionContainer = _TheContainer ;
  //_MainThreadId = SuperVisionContainer()->MainThreadId() ;
  _Valid = false ;
  _Executable = false ;
  _Done = false ;
  _NodeAborted = false ;
  _Threads = 0 ;
  _ControlState = SUPERV::VoidState ;
  _State = GraphExecutor::UnKnownState ;
  _PyInitialized = false ;
  _run_func_thread = 0;
  pthread_mutex_init( &_MutexWait , NULL ) ;
  pthread_mutex_init( &_PyMutexWait , NULL ) ;
  if ( pthread_cond_init( &_EventWait , NULL ) ) {
    perror("pthread_cond_init( &_EventWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_JoinWait , NULL ) ) {
    perror("pthread_cond_init( &_JoinWait , NULL )") ;
    exit( 0 ) ;
  }
}

GraphExecutor::OutNode::OutNode( CORBA::ORB_ptr ORB, 
				 SALOME_NamingService* ptrNamingService ,
                                 const char *DataFlowName ,
                                 const char * DebugFileName ,
                                 const SUPERV::KindOfNode aKindOfNode ) {
  Set_prof_debug( ORB , DebugFileName ) ;
  cdebug_in << "GraphExecutor::OutNode::OutNode(" << DataFlowName << " , " << DebugFileName
            << " , " << aKindOfNode << ")" << endl;
  if ( aKindOfNode == SUPERV::DataFlowGraph ) {
    _StreamGraph = NULL ;
    _Graph = new GraphBase::Graph( ORB , ptrNamingService , DataFlowName , aKindOfNode ,
                                   _prof_debug , _fdebug ) ;
  }
  else if ( aKindOfNode == SUPERV::DataStreamGraph ) {
    _StreamGraph = new GraphBase::StreamGraph( ORB , ptrNamingService , DataFlowName , aKindOfNode ,
                                               _prof_debug , _fdebug ) ;
    _Graph = _StreamGraph ;
  }
  // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
  //_SuperVisionContainer = _TheContainer ;
  //_MainThreadId = SuperVisionContainer()->MainThreadId() ;
  _Valid = false ;
  _Executable = false ;
  _Done = false ;
  _NodeAborted = false ;
  _Threads = 0 ;
  _ControlState = SUPERV::VoidState ;
  _State = GraphExecutor::UnKnownState ;
  _PyInitialized = false ;
  _run_func_thread = 0;
  _Orb = CORBA::ORB::_duplicate( ORB ) ;
  pthread_mutex_init( &_MutexWait , NULL ) ;
  pthread_mutex_init( &_PyMutexWait , NULL ) ;
  if ( pthread_cond_init( &_EventWait , NULL ) ) {
    perror("pthread_cond_init( &_EventWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_JoinWait , NULL ) ) {
    perror("pthread_cond_init( &_JoinWait , NULL )") ;
    exit( 0 ) ;
  }
  cdebug_out << "GraphExecutor::OutNode::OutNode" << endl;
}

GraphExecutor::OutNode::OutNode( CORBA::ORB_ptr ORB, 
	                         SALOME_NamingService* ptrNamingService ,
                                 const SALOME_ModuleCatalog::Service& DataFlowService ,
                                 const char *DataFlowComponentName ,
                                 const char *DataFlowInterfaceName ,
                                 const char *DataFlowName ,
                                 const SUPERV::KindOfNode DataFlowkind ,
                                 const SUPERV::SDate DataFlowFirstCreation ,
                                 const SUPERV::SDate DataFlowLastModification ,
                                 const char * DataFlowEditorRelease ,
                                 const char * DataFlowAuthor ,
                                 const char * DataFlowComputer ,
                                 const char * DataFlowComment ,
                                 const char * DebugFileName ) {
  Set_prof_debug( ORB , DebugFileName ) ;

  cdebug_in << "GraphExecutor::OutNode::OutNode(" << DataFlowComponentName << " , "
            << DataFlowInterfaceName << " , " << DataFlowName << " , " << DataFlowkind << ")"
            << " , ... , " << DebugFileName << ")" << endl;
  if ( DataFlowkind == SUPERV::DataFlowGraph ) {
    _StreamGraph = NULL ;
    _Graph = new GraphBase::Graph( ORB , ptrNamingService , DataFlowService , DataFlowComponentName ,
                                   DataFlowInterfaceName , DataFlowName , DataFlowkind ,
                                   DataFlowFirstCreation , DataFlowLastModification  ,
                                   DataFlowEditorRelease , DataFlowAuthor ,
                                   DataFlowComputer , DataFlowComment ,
                                   _prof_debug , _fdebug ) ;
//                                   DataFlowComputer , DataFlowComment , DebugFileName ) ;
  }
  else if ( DataFlowkind == SUPERV::DataStreamGraph ) {
    _StreamGraph = new GraphBase::StreamGraph( ORB , ptrNamingService , DataFlowService , DataFlowComponentName ,
                                               DataFlowInterfaceName , DataFlowName , DataFlowkind ,
                                               DataFlowFirstCreation , DataFlowLastModification  ,
                                               DataFlowEditorRelease , DataFlowAuthor ,
                                               DataFlowComputer , DataFlowComment ,
                                               _prof_debug , _fdebug ) ;
    _Graph = _StreamGraph ;
  }
  // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
  //_SuperVisionContainer = _TheContainer ;
  //_MainThreadId = SuperVisionContainer()->MainThreadId() ;
  _Valid = false ;
  _Executable = false ;
  _Done = false ;
  _NodeAborted = false ;
  _Threads = 0 ;
  _ControlState = SUPERV::VoidState ;
  _State = GraphExecutor::UnKnownState ;
  _PyInitialized = false ;
  _run_func_thread = 0;
  _Orb = CORBA::ORB::_duplicate( ORB ) ;
  pthread_mutex_init( &_MutexWait , NULL ) ;
  pthread_mutex_init( &_PyMutexWait , NULL ) ;
  if ( pthread_cond_init( &_EventWait , NULL ) ) {
    perror("pthread_cond_init( &_EventWait , NULL )") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_init( &_JoinWait , NULL ) ) {
    perror("pthread_cond_init( &_JoinWait , NULL )") ;
    exit( 0 ) ;
  }
}

GraphExecutor::OutNode::~OutNode() {
}

void GraphExecutor::OutNode::Set_prof_debug( CORBA::ORB_ptr ORB ,
                                             const char * DebugFileName ) {
  _Graph_prof_debug = 0 ;
  if ( DebugFileName ) {
    _fdebug = new ofstream( DebugFileName );
    SetDebug( ORB , &_Graph_prof_debug , _fdebug ) ;
    MESSAGE( endl << "Trace redirected to file " << DebugFileName << endl)
  }
}

bool GraphExecutor::OutNode::LoadDataFlow(const GraphBase::SGraph * aDataFlow ) {
  bool RetVal = false ;
  cdebug_in << "GraphExecutor::OutNode::LoadDataFlow " << (*aDataFlow).Info.theName << endl ;
  RetVal = LoadInfo( (*aDataFlow).Info ) ;
  if ( Graph()->ServiceName() != NULL ) {
//    MESSAGE( "GraphExecutor::OutNode::LoadDataFlow" );
    if ( RetVal ) {
      RetVal = LoadNodes( (*aDataFlow).Nodes ) ;
      if ( RetVal ) {
        RetVal = LoadLinks( (*aDataFlow).Links ) ;
        if ( RetVal ) {
          IsValid() ;
          RetVal = LoadDatas( (*aDataFlow).Datas ) ;
//          IsExecutable() ;
          if ( !RetVal) {
            cdebug << "GraphExecutor::OutNode::LoadDataFlow LoadDatas Error."
                   << endl ;
	  }
	}
        else {
          cdebug << "GraphExecutor::OutNode::LoadDataFlow LoadLinks Error."
               << endl ;
	}
      }
      else {
        cdebug << "GraphExecutor::OutNode::LoadDataFlow LoadNodes Error."
             << endl ;
      }
    }
    else {
      cdebug << "GraphExecutor::OutNode::LoadDataFlow LoadInfo Error."
           << endl ;
    }
  }
  else {
    cdebug << "GraphExecutor::OutNode::LoadDataFlow ServiceName Error." << endl ;
  }
  cdebug_out << "GraphExecutor::OutNode::LoadDataFlow " << Name() << " RetVal "
             << RetVal << endl ;
  return RetVal ;
}

#if 0
bool GraphExecutor::OutNode::LoadXml( const char* myFileName ) {
  bool RetVal = false ;
  GraphBase::ListOfSGraphs aListOfDataFlows ;
  if ( Graph()->LoadXml( _Orb , myFileName , aListOfDataFlows ) ) {
    RetVal = LoadDataFlow( aListOfDataFlows ) ;
//    if ( aConstructor && RetVal )
//      RetVal = Name( aDataFlow.Info.theName.c_str() ) ;
  }
  return RetVal ;
}
#endif

bool GraphExecutor::OutNode::LoadInfo(const GraphBase::SNode &aDataFlowInfo ) {
  cdebug << "GraphExecutor::OutNode::LoadInfo _StreamGraph " << _StreamGraph << endl ;
//  ComponentName( aDataFlowInfo.theComponentName.c_str()  ) ;
  Graph()->Name( aDataFlowInfo.theName.c_str()  ) ;
  Graph()->Kind( aDataFlowInfo.theKind ) ;
  if ( Graph()->IsDataStreamNode() && aDataFlowInfo.theKind == SUPERV::DataStreamGraph ) {
    StreamGraph()->SetStreamParams( aDataFlowInfo.theTimeout , aDataFlowInfo.theDataStreamTrace , aDataFlowInfo.theDeltaTime ) ;
  }
  Graph()->DataService( _Orb , aDataFlowInfo.theService , _prof_debug , _fdebug ) ;
  Graph()->FirstCreation( aDataFlowInfo.theFirstCreation ) ;
  Graph()->LastModification( aDataFlowInfo.theLastModification ) ;
  Graph()->EditorRelease( aDataFlowInfo.theEditorRelease.c_str()  ) ;
  Graph()->Author( aDataFlowInfo.theAuthor.c_str()   ) ;
//  Graph()->Computer( aDataFlowInfo.theContainer.c_str()  ) ;
  Graph()->Comment( aDataFlowInfo.theComment.c_str()  ) ;
// Not in OutNode/DataFlow but in InNode/DataFlow_in_an_other_DataFlow
//  Graph()->Coordinates( aDataFlowInfo.theX , aDataFlowInfo.theY ) ;
  return true ;
}

bool GraphExecutor::OutNode::LoadNodes(const GraphBase::ListOfSNodes &aListOfNodes ) {
  bool RetVal = true ;
  GraphExecutor::InNode * anInNode ;
  cdebug << "GraphExecutor::OutNode::LoadNodes" << endl ;
  int i ;
  for ( i = 0 ; i < (int ) aListOfNodes.size() ; i++ ) {
    GraphBase::SNode aNode = aListOfNodes[ i ] ;
    anInNode = AddNode( aNode.theService ,
                        aNode.theListOfFuncName ,
                        aNode.theListOfPythonFunctions ,
                        aNode.theComponentName.c_str() ,
                        aNode.theInterfaceName.c_str()  , aNode.theName.c_str() ,
                        aNode.theKind ,
                        aNode.theFirstCreation , aNode.theLastModification ,
                        aNode.theEditorRelease.c_str() ,
                        aNode.theAuthor.c_str()  , aNode.theContainer.c_str() ,
                        aNode.theComment.c_str() ,
                        aNode.theCoords.theX , aNode.theCoords.theY ) ;
    if ( !anInNode ) {
      RetVal = false ;
      break ;
    }
    if ( anInNode->IsOneOfInLineNodes() || anInNode->IsMacroNode() ) {
      anInNode->GraphExecutor::InNode::InLineNode()->DefPortsOfNode(
                                _Orb , aNode.theService , anInNode->NamePtr() ,
                                anInNode->Kind() ,
                                _prof_debug , _fdebug ) ;
    }
//    GraphBase::ComputingNode * theNode = Graph()->GetChangeGraphNode( aNode.theName.c_str() ) ;

    unsigned int j ;
    for ( j = 0 ; j < aNode.theListOfInDataStreams.size() ; j++ ) {
      GraphBase::InPort * anInPort ;
      anInPort = anInNode->ComputingNode()->AddInDataStreamPort( aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametername ,
                                                                 aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parametertype ,
                                                                 aNode.theListOfInDataStreams[ j ].theDataStreamParameter.Parameterdependency ,
                                                                 SUPERV::DataStreamParameter ) ;
      ((GraphBase::InDataStreamPort * ) anInPort)->SetParams( aNode.theListOfInDataStreams[ j ].theKindOfSchema ,
                                                              aNode.theListOfInDataStreams[ j ].theKindOfInterpolation ,
                                                              aNode.theListOfInDataStreams[ j ].theKindOfExtrapolation ) ;
    }
    for ( j = 0 ; j < aNode.theListOfOutDataStreams.size() ; j++ ) {
      GraphBase::OutPort * anOutPort ;
      anOutPort = anInNode->ComputingNode()->AddOutDataStreamPort( aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametername ,
                                                                   aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parametertype ,
                                                                   aNode.theListOfOutDataStreams[ j ].theDataStreamParameter.Parameterdependency ,
                                                                   SUPERV::DataStreamParameter ) ;
      ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues( aNode.theListOfOutDataStreams[ j ].theNumberOfValues ) ;
    }


//    theNode->DataStreamInPortsNumber( aNode.theDataStreamInArgsNumber ) ;
//    theNode->DataStreamOutPortsNumber( aNode.theDataStreamOutArgsNumber ) ;
//    cdebug << "GraphExecutor::OutNode::LoadNodes " << anInNode->Name()
//           << " InPortsSize " << theNode->GetNodeInPortsSize()
//           << " OutPortsSize " << theNode->GetNodeOutPortsSize()
//           << " DataStreamInPortsNumber " << aNode.theDataStreamInArgsNumber
//           << " DataStreamOutPortsNumber " << aNode.theDataStreamOutArgsNumber
//           << endl ;
//    int j ;
//    if ( aNode.theDataStreamInArgsNumber ) { // -1 because of Gates
//      for ( j = theNode->GetNodeInPortsSize() - aNode.theDataStreamInArgsNumber - 1 ; j < theNode->GetNodeInPortsSize() - 1 ; j++ ) {
//        cdebug << "GraphExecutor::OutNode::LoadNodes " << anInNode->Name()
//               << " InPort " << theNode->GetChangeNodeInPort( j )->PortName()
//               << " Kind( DataStreamParameter)" << endl ;
//        theNode->GetChangeNodeInPort( j )->Kind( SUPERV::DataStreamParameter ) ;
//      }
//    }
//    if ( aNode.theDataStreamOutArgsNumber ) {
//      int withgate = 1 ;
//      if ( theNode->IsLoopNode() || theNode->IsEndLoopNode() ) {
//        withgate = 0 ;
//      }
//      for ( j = theNode->GetNodeOutPortsSize() - aNode.theDataStreamOutArgsNumber - withgate ; j < theNode->GetNodeOutPortsSize() - withgate ; j++ ) {
//        cdebug << "GraphExecutor::OutNode::LoadNodes " << anInNode->Name()
//               << " OutPort " << theNode->GetChangeNodeOutPort( j )->PortName()
//               << " Kind( DataStreamParameter)" << endl ;
//        theNode->GetChangeNodeOutPort( j )->Kind( SUPERV::DataStreamParameter ) ;
//      }
//    }
  }
  for ( i = 0 ; i < (int ) aListOfNodes.size() ; i++ ) {
    GraphBase::SNode aNode = aListOfNodes[ i ] ;
    anInNode = (GraphExecutor::InNode * ) Graph()->GetChangeGraphNode( aNode.theName.c_str() )->GetInNode() ;
    cdebug << "GraphExecutor::OutNode::LoadNodes " << anInNode->Name() << " IsOneOfGOTONodes "
           << anInNode->IsOneOfGOTONodes() << " " << aNode.theCoupledNode.c_str() << endl ;
    if ( anInNode->IsOneOfGOTONodes() && strlen( aNode.theCoupledNode.c_str() ) ) {
      GraphBase::GOTONode * aCoupledNode ;
      aCoupledNode = (GraphBase::GOTONode * ) Graph()->GetGraphNode( aNode.theName.c_str() ) ;
      GraphBase::GOTONode * aCoupledCoupledNode ;
      aCoupledCoupledNode = (GraphBase::GOTONode * ) Graph()->GetChangeGraphNode( aNode.theCoupledNode.c_str() ) ;
      if ( aCoupledCoupledNode != NULL ) {
        aCoupledNode->CoupledNode( aCoupledCoupledNode ) ;
      }
      else {
        aCoupledNode->CoupledNodeName( aNode.theCoupledNode.c_str() ) ;
      }
    }
  }
  return RetVal ;
}

bool GraphExecutor::OutNode::LoadLinks(const GraphBase::ListOfSLinks &aListOfLinks ) {
  bool RetVal = true ;
  cdebug << "GraphExecutor::OutNode::LoadLinks " << aListOfLinks.size()
         << endl ;
  int i ;
  for ( i = 0 ; i < (int ) aListOfLinks.size() ; i++ ) {
    GraphBase::SLink aLink = aListOfLinks[ i ] ;
    RetVal = Graph()->AddLink( aLink.FromNodeName.c_str() ,
                               aLink.FromServiceParameterName.c_str() ,
                               aLink.ToNodeName.c_str() ,
                               aLink.ToServiceParameterName.c_str() ) ;
//                               , aLink.aLinkValue ) ;
    if ( !RetVal )
      break ;
  }
  return RetVal ;
}

bool GraphExecutor::OutNode::LoadDatas(const GraphBase::ListOfSLinks &aListOfDatas ) {
  bool RetVal = true ;
  cdebug << "GraphExecutor::OutNode::LoadDatas " << aListOfDatas.size()
         << endl ;
  int i ;
  for ( i = 0 ; i < (int ) aListOfDatas.size() ; i++ ) {
    GraphBase::SLink aLink = aListOfDatas[ i ] ;
    if ( !strcmp( aLink.FromNodeName.c_str() , Name() ) )
      RetVal = Graph()->AddInputData( aLink.ToNodeName.c_str() ,
                                      aLink.ToServiceParameterName.c_str() ,
                                      aLink.aLinkValue ) ;
//    else if ( !strcmp( aLink.ToNodeName.c_str() , Name() ) ) {
//      RetVal = Graph()->AddOutputData( aLink.FromNodeName.c_str() ,
//                                       aLink.FromServiceParameterName.c_str() ) ;
//      Graph()->AddLink( aLink.FromNodeName.c_str() , (GraphBase::ComputingNode *) this ) ;
//    }
    else {
      cdebug << "GraphExecutor::OutNode::LoadDatas Error " << aLink.FromNodeName
           << " and " << aLink.ToNodeName << " differents from " << Name()
           << endl ;
      RetVal = false ;
    }
    if ( !RetVal )
      break ;
  }
  return RetVal ;
}

GraphExecutor::InNode *GraphExecutor::OutNode::AddNode(
                      const SALOME_ModuleCatalog::Service& NodeService ,
                      GraphBase::ListOfFuncName aFuncName ,
                      GraphBase::ListOfPythonFunctions aPythonFunction ,
                      const char * NodeComponentName ,
                      const char * NodeInterfaceName ,
                      const char * NodeName ,
                      const SUPERV::KindOfNode NodeKindOfNode ,
                      const SUPERV::SDate NodeFirstCreation ,
                      const SUPERV::SDate NodeLastModification  ,
                      const char * NodeEditorRelease ,
                      const char * NodeAuthor ,
                      const char * NodeComputer ,
                      const char * NodeComment ,
                      const int NodeX ,
                      const int NodeY ) {
  cdebug_in << "GraphExecutor::OutNode::AddNode(" << NodeComponentName << " , "
            << NodeName << ")" << endl;
  GraphExecutor::InNode *Nd = NULL ;
  Nd = new GraphExecutor::InNode( _Orb, Graph()->NamingService() , NodeService ,
                                  NodeComponentName , NodeInterfaceName ,
                                  NodeName , NodeKindOfNode ,
                                  aFuncName , aPythonFunction ,
                                  NodeFirstCreation , NodeLastModification ,
                                  NodeEditorRelease , NodeAuthor ,
                                  NodeComputer , NodeComment , false , NodeX , NodeY ,
                                  _prof_debug , _fdebug ) ;
  Graph()->AddNode( Nd->ComputingNode() ) ;
  cdebug_out << "GraphExecutor::OutNode::AddNode" << endl;
  return Nd ;
}


bool GraphExecutor::OutNode::Valid() {
  cdebug_in << "GraphExecutor::OutNode::Valid" << endl;
  bool RetVal = true ;
  Graph()->ReSetMessages() ; // ==> Only one set of errors messages ...
  if ( _Valid )
    return true ;

  _Executable = false ;

  if ( !Graph()->CreateService() ) {
    cdebug_out << "This DataFlow has invalid type(s)." << endl ;
    RetVal = false ;
  }
  int SubStreamGraphsNumber = 0 ;
  if ( !Graph()->Sort( SubStreamGraphsNumber ) ) {
    cdebug_out << "This DataFlow is not valid." << endl ;
    RetVal = false ;
  }
  if ( Graph()->IsDataStreamNode() ) {
    StreamGraph()->SubStreamGraphsNumber( SubStreamGraphsNumber ) ;
  }
  Graph()->InLineServices() ;

  if ( !Graph()->ValidLoops() ) {
    cdebug_out << "This DataFlow have not valid Loops." << endl ;
    RetVal = false ;
  }
  if ( !Graph()->ValidSwitchs() ) {
    cdebug_out << "This DataFlow have not valid Switchs." << endl ;
    RetVal = false ;
  }

//  CreateService() ;

  Graph()->ComputingNodes() ;
  if ( RetVal ) {
    _Valid = true ;
  }

  cdebug_out << "GraphExecutor::OutNode::Valid " << _Valid << " RetVal " << RetVal << endl;
  return RetVal ;
}

bool GraphExecutor::OutNode::Executable() {
  cdebug_in << "GraphExecutor::OutNode::Executable" << endl;
  bool RetVal = true ;
  if ( !IsValid() ) {
    RetVal = false ;
  }

  if ( Graph()->DataServerNodes() ) {
    if ( RetVal ) {
      _Executable = true ;
    }
  }
  else {
    cdebug << "This DataFlow is not executable." << endl ;
    _Executable = false ;
    RetVal = false ;
  }

  cdebug_out << "GraphExecutor::OutNode::Executable " << _Executable << endl;
  return _Executable ;
}

bool GraphExecutor::OutNode::Run( const bool AndSuspend ) {
  bool RetVal = false ;
  cdebug_in << pthread_self() << "GraphExecutor::OutNode::Run( AndSuspend " << AndSuspend
            << " ) State " << theAutomaton->StateName( State() ) << endl;

  if ( Executable() ) {
    _ControlState = SUPERV::VoidState ;
    _SuspendedThreads = 0 ;
    Graph()->ThreadNo( pthread_self() ) ;
    Done( false ) ;
    _JustStarted = true ;
    RetVal = true ;
    int i ;
    bool again = true ;
    for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
      GraphExecutor::InNode * anInNode = (GraphExecutor::InNode *) Graph()->GraphNodes( i )->GetInNode() ;
      anInNode->OutNode( this ) ;
// Initialize in python interpretor the python functions
      if ( !PyInitialized() && anInNode->IsOneOfInLineNodes() ) {
        if ( !Py_IsInitialized() ) {
//          PyEval_InitThreads() ;
//          Py_Initialize() ;
	  // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL  
	  //                  1 line below uncommented
          PySys_SetArgv( _ArgC , _ArgV ) ;
	}
        anInNode->InitPython() ;
        PyInitialized( true ) ;
      }
      anInNode->InitialState() ;
      if ( anInNode->IsOneOfInLineNodes() ) {
	//JR with Python2.3.4, we may get an error because of a normal previous error
        bool sts = anInNode->InitPythonFunctions( false ) ;
        if ( again && !sts ) {
          sts = anInNode->InitPythonFunctions( false ) ;
          again = false ;
	}
      }
    }
// One more time because inline nodes may share one definition of the same function
    for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
      GraphExecutor::InNode * anInNode = (GraphExecutor::InNode *) Graph()->GraphNodes( i )->GetInNode() ;
      if ( anInNode->IsOneOfInLineNodes() ) {
        if ( !anInNode->InitPythonFunctions( true ) ) {
          anInNode->State( GraphExecutor::ErroredState ) ;
// PAL 8515
//JR 24.03.2005 debug : State and done flags were missing
          State( GraphExecutor::ErroredState ) ;
          Done( true ) ;
          cdebug_out << "GraphExecutor::OutNode::Run InitPythonFunctions ERROR "
                     << anInNode->Name() << endl ;
          RetVal = false ;
	}
      }
    }
    /* asv : 29.09.04 : commented out because it does not do anything useful 
    for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
      GraphExecutor::InNode * anInNode = (GraphExecutor::InNode *) Graph()->GraphNodes( i )->GetInNode() ;
      if ( RetVal && anInNode->IsMacroNode() ) {
        GraphBase::Graph * aMacroGraph = anInNode->GraphMacroNode() ;
        cdebug << "GraphExecutor::OutNode::Run " << anInNode << " MacroNode " << aMacroGraph
               << aMacroGraph->Name() << endl ;
        SUPERV::Graph_var iobject = anInNode->GraphMacroNode()->MacroObject() ;
        if ( CORBA::is_nil( iobject ) ) {
          cdebug << "GraphExecutor::OutNode::Run MacroObject() is a nil object. Error" << endl ;
          RetVal = false ;
	}
        else {
          cdebug << "GraphExecutor::OutNode::Run MacroObject() is NOT a nil object." << endl ;
	}
      }
    }
    */
    //JR Debug : if there was an error in initialisation of python functions,
    //           we must not try to execute the graph.
    if ( RetVal ) {
    if ( Graph()->GraphMacroLevel() != 0 ) {
      cdebug << "GraphExecutor::OutNode::Run Execution starting GraphExecutor::Action_DataOk_RunService Node "
             << Name() << " GraphMacroLevel " << Graph()->GraphMacroLevel() << endl ;
      PushEvent( NULL , GraphExecutor::NoDataReadyEvent ,
                 GraphExecutor::DataUndefState ) ; 
    }
    else {
      cdebug << "Execution starting GraphExecutor::Action_DataOk_RunService Node "
             << Name() << " GraphMacroLevel " << Graph()->GraphMacroLevel() << endl ;
      PushEvent( NULL , GraphExecutor::ReadyEvent ,
                 GraphExecutor::DataReadyState ) ; 
      State( GraphExecutor::DataReadyState ) ;

      cdebug << pthread_self() << " GraphExecutor::OutNode::Run Start of the " << Graph()->HeadNodesSize()
             << " HeadNodes" << endl ;
      for ( i = 0 ; i < Graph()->HeadNodesSize() ; i++ ) {
        GraphExecutor::InNode * anInNode = (GraphExecutor::InNode *) Graph()->HeadNodes( i )->GetInNode() ;
        cdebug << pthread_self() <<  "GraphExecutor::OutNode::Run Start of HeadNode " << i << " "
               << anInNode->Name() << endl ;
        if ( anInNode->State() != GraphExecutor::DataReadyState ) {
// PAL 8515
//JR 24.03.2005 debug : State and done flags were missing
          State( GraphExecutor::ErroredState ) ;
          Done( true ) ;
          cdebug << "GraphExecutor::OutNode::Run inconsistency State of Node "
                 << anInNode->Name() << " : " << anInNode->State() << endl ;
          cdebug_out << "GraphExecutor::OutNode::Run State ERROR" << endl ;
          return false ;
        }
//        PushEvent( anInNode , GraphExecutor::ReadyEvent ,
//                   GraphExecutor::DataReadyState ) ; 
// We say that we have to create a thread for that HeadNode 'anInNode'
        if ( !anInNode->IsMacroNode() ) {
          anInNode->CreateNewThread( true ) ;
          IncrCreatedThreads() ;
	}
        anInNode->DataFromNode( Name() ) ;
// AndSuspend == true <==> Start()
        if ( AndSuspend ) {
          anInNode->State( GraphExecutor::DataWaitingState ) ;
          anInNode->ControlState( SUPERV::ToSuspendStartState ) ;
// We send SomeDataReadyEvent
// It is a HeadNode ==> AllDataReadyEvent ==> InNode::executeAction() ==> pthread_create
          if ( !anInNode->SendEvent( GraphExecutor::SomeDataReadyEvent ) ) {
// PAL 8515
//JR 24.03.2005 debug : State and done flags were missing
            State( GraphExecutor::ErroredState ) ;
            Done( true ) ;
            cdebug << "InNode::SendEvent( SomeDataReadyEvent ) ERROR Node "
                   << anInNode->Name() << endl ;
            cdebug_out << "GraphExecutor::OutNode::Run SendEvent ERROR" << endl ;
            return false ;
          }
// ==> We wait for SuspendedState
          anInNode->SuspendedWait() ;
        }
// AndSuspend == false <==> Run()
        else if ( anInNode->IsMacroNode() ) {
          if ( !anInNode->SendEvent( GraphExecutor::SomeDataReadyEvent ) ) {
// PAL 8515
//JR 24.03.2005 debug : State and done flags were missing
            State( GraphExecutor::ErroredState ) ;
            Done( true ) ;
            cdebug << "InNode::SendEvent( SomeDataReadyEvent ) ERROR Node "
                   << anInNode->Name() << endl ;
            cdebug_out << "GraphExecutor::OutNode::Run SendEvent ERROR" << endl ;
            return false ;
	  }
//JR 15.04.2005 Debug PAL8624 RetroConception :
          anInNode->CreateNewThread( true ) ;
          IncrCreatedThreads() ;
          if ( anInNode->HasAllDataReady() ) {
            if ( !anInNode->SendEvent( GraphExecutor::AllDataReadyEvent ) ) { // ==> Ready to execute
              cdebug <<  "GraphExecutor::OutNode::Run SendEvent(AllDataReadyEvent) ERROR "
                     << " Node " << anInNode->Name() << endl ;
              cdebug_out << "GraphExecutor::OutNode::Run SendEvent ERROR" << endl ;
              return false ;
	    }
	  }
          else {
            State( GraphExecutor::ErroredState ) ;
            Done( true ) ;
            cdebug <<  "GraphExecutor::OutNode::Run HasAllDataReady " << anInNode->HasAllDataReady()
                   << " Node " << anInNode->Name() << endl ;
            cdebug_out << "GraphExecutor::OutNode::Run HasAllDataReady ERROR" << endl ;
            return false ;
	  }
          cdebug << pthread_self() << " GraphExecutor::OutNode::Run " << anInNode->Name() << "->RunningWait"
                 << endl ;
// ==> We wait for RunningState
          anInNode->RunningWait() ;
          cdebug << pthread_self() << " GraphExecutor::OutNode::Run " << anInNode->Name() << "->RunningWaited"
                 << endl ;
	}
        else if ( !anInNode->IsMacroNode() ) {
// It is a HeadNode ==> AllDataReadyEvent ==> InNode::executeAction() ==> pthread_create
          if ( !anInNode->SendEvent( GraphExecutor::ExecuteEvent ) ) {
// PAL 8515
//JR 24.03.2005 debug : State and done flags were missing
            State( GraphExecutor::ErroredState ) ;
            Done( true ) ;
            cdebug << "InNode::SendEvent( ExecuteEvent ) ERROR Node "
                   << anInNode->Name() << endl ;
            cdebug_out << "GraphExecutor::OutNode::Run SendEvent ERROR" << endl ;
            return false ;
	  }
// ==> We wait for RunningState
          anInNode->RunningWait() ;
        }
      }

      if ( AndSuspend ) {
        PushEvent( NULL , GraphExecutor::SuspendedReadyEvent ,
                   GraphExecutor::SuspendedReadyState ) ; 
      }
      else {
        PushEvent( NULL , GraphExecutor::ExecutingEvent ,
                   GraphExecutor::ExecutingState ) ; 
        if (AutomatonState() == GraphExecutor::DataReadyState) {
            State( GraphExecutor::ExecutingState ) ;
        };
      }
//      ostringstream astr ;
//      astr << "Graph " << Name() << " is running" ;
//      Graph()->ObjImpl()->sendMessage( NOTIF_STEP, astr.str().c_str() ) ;
      RetVal = true ;
    }
    //JR Debug
    }
    else {
      State( GraphExecutor::ErroredState ) ;
      Done( true ) ;
      if ( IsDone() && Graph()->GraphMacroLevel() == 0 ) {
        cdebug << " ==> theAutomaton->Executed() " << endl ;
        theAutomaton->Executed() ;
      }
      else {
        cdebug << " NO theAutomaton->Executed() " << endl ;
      }
    }
  }
  else {
// PAL 8515
//JR 24.03.2005 debug : State and done flags were missing
    State( GraphExecutor::ErroredState ) ;
    Done( true ) ;
//JR 17.06.2005 debug call to executed was missing ===>
//              _GraphExecutingNumber field of FiniteStateMachine was not decremented ===>
//              register of python functions was de-activated ==>
//              two graphs execution with two different python functions with the same name
//              give aborted state for the second one
//Example : GraphLoopSwitchOfSwitchNOTValid, GraphLoopSwitchs and GraphLoopSwitchsBranches
    if ( IsDone() && Graph()->GraphMacroLevel() == 0 ) {
      cdebug << " ==> theAutomaton->Executed() " << endl ;
      theAutomaton->Executed() ;
    }
    else {
      cdebug << " NO theAutomaton->Executed() " << endl ;
    }
    cdebug_out << "GraphExecutor::OutNode::Run Not Executable ERROR " << endl ;
    return false ;
  }

  cdebug_out << pthread_self() << "GraphExecutor::OutNode::Run " << Name()
             << " State " << theAutomaton->StateName( State() ) << " GraphMacroLevel "
             << Graph()->GraphMacroLevel() << " RetVal " << RetVal << endl ;
  return RetVal ;
}

void GraphExecutor::OutNode::CheckAllDone() {
  int j ;
  cdebug_in << "GraphExecutor::OutNode::CheckAllDone " << endl;
  GraphExecutor::AutomatonState OutNodeState = GraphExecutor::SuccessedState ;
  GraphExecutor::AutomatonState InNodeState ;
  bool AllDone = true ;
  if ( !Done() ) {
    for ( j = 0 ; j < Graph()->QueueNodesSize() ; j++ ) {
      InNodeState = ( (GraphExecutor::InNode * ) Graph()->QueueNodes( j )->GetInNode() )->State() ;
      cdebug << j << ". "
             << ( (GraphExecutor::InNode * ) Graph()->QueueNodes( j )->GetInNode() )->Name()
             << " " << theAutomaton->StateName( InNodeState ) << endl ;
      if ( InNodeState != GraphExecutor::SuccessedState &&
           InNodeState != GraphExecutor::ErroredState &&
           InNodeState != GraphExecutor::DataWaitingState ) {
        if ( !IsNodeAborted() ) {
          AllDone = false ;
	}
      }
      if ( InNodeState != GraphExecutor::SuccessedState &&
           InNodeState != GraphExecutor::DataWaitingState &&
           InNodeState != GraphExecutor::DataReadyState ) {
        OutNodeState = InNodeState ;
      }
    }
    if ( AllDone ) {
      if( _Threads == 0 && _SuspendedThreads == 0 ) {
        if ( OutNodeState != GraphExecutor::ErroredState ) {
          OutNodeState = GraphExecutor::SuccessedState ;
        }
      }
      else {
        AllDone = false ;
      }
    }
    if ( AllDone ) {
      int alivenodes = 0 ;
      for ( j = 0 ; j < Graph()->GraphNodesSize()  ; j++ ) {
        GraphExecutor::InNode * aNode ;
        aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( j )->GetInNode() ;
        SUPERV::GraphState aState = AutomatonGraphState( aNode->State() ) ;
        cdebug << "GraphExecutor::OutNode::CheckAllDone " << aNode->Name() << " "
               << theAutomaton->StateName( aNode->State() ) << " CreateNewThread " << aNode->CreateNewThread()
               << endl ;
        if ( aState == SUPERV::ErrorState ||
             aState == SUPERV::SuspendErroredState ||
             aState == SUPERV::KillState ||
             aState == SUPERV::StopState ) {
          OutNodeState = aNode->State() ;
          State( OutNodeState ) ;
	}
        else if ( aState == SUPERV::ReadyState ||
                  aState == SUPERV::SuspendReadyState ||
                  aState == SUPERV::RunningState ||
                  aState == SUPERV::SuspendDoneState ||
                  aState == SUPERV::SuspendErroredState ||
                  aState == SUPERV::SuspendState ) {
          if ( !IsNodeAborted() ) {
            alivenodes += 1 ;
	  }
	}
        aNode->SuspendedAction() ;
        aNode->DoneAction() ;
      }
// PushEvent AFTER State and _Done ! ...
      if ( alivenodes == 0 ) {
        State( OutNodeState ) ;
        Done( true ) ;
        _JustStarted = false ;
      }
      cdebug << Name() << " alivenodes " << alivenodes << " OutNodeState " << OutNodeState
             << endl ;
      PushEvent( NULL , GraphExecutor::EndExecuteEvent ,
                 OutNodeState ) ;
//      Py_Finalize() ;
//      PyInitialized( false ) ;
    }
  }
  if ( IsDone() ) {
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS DONE : " <<  theAutomaton->StateName( AutomatonState() ) << " EventQSize "
             << EventQSize() ) ;
    MESSAGE("================================================================================") ;
    cdebug << "================================================================================" << endl ;
    cdebug << Name() << " IS DONE : " <<  theAutomaton->StateName( AutomatonState() ) << " EventQSize "
	     << EventQSize() << endl  ;
    cdebug << "================================================================================" << endl ;

    if ( Graph()->GraphMacroLevel() != 0 ) {
//JR NPAL14110 09.02.2007 : That must be done only at the end of SubGraph because we may
//                          have an output of the SubGraph in a Loop !...
//                          So that code must be done in CheckAllDone instead of OutParametersSet
      cdebug << "CheckAllDone ToNodeName " << Name() << " CoupledNode "
             << Graph()->CoupledNodeName() << Graph()->CoupledNode()
             << endl ;
      cdebug << "CheckAllDone GraphExecutor " << Graph()->CoupledNode()->GraphEditor()->Executor() << endl ;
      const GraphBase::InPort * anInPort ;
      GraphBase::OutPort * anOutPort ;
      int i ;
      for ( i = 0 ; i <  Graph()->GetNodeOutDataNodePortsSize() ; i++ ) {
         anInPort = Graph()->GetNodeOutDataNodePort( i ) ;
         if ( !anInPort->IsGate() ) {
           anOutPort = anInPort->GetOutPort() ;
           Graph()->CoupledNode()->GraphEditor()->Executor()->OutputOfAny( Graph()->CoupledNodeName() ,
                                                                           anInPort->PortName() ,
                                                                           anOutPort->Value() ) ;
           cdebug << "CheckAllDone OutputOfAny( " << Graph()->CoupledNodeName() << " , "
                  << anInPort->PortName() << " , value ) done" << endl ;
         }
      }
    }

//PAL8520
//JR 23.02.2005, Debug : siftEvents() must be inside the pthread_mutex_lock
    if ( pthread_mutex_lock( &_MutexWait ) ) {
      perror("EventLoop pthread_mutex_lock ") ;
      exit( 0 ) ;
    }
    // asv : fix for 6798 and other bugs: "sift" the event queue after execution
    siftEvents();
    if ( pthread_mutex_unlock( &_MutexWait ) ) {
      perror("PushEvent pthread_mutex_unlock ") ;
      exit( 0 ) ;
    }

    // asv : 25.10.04 : calling Editing() to indicate stop of execution
    Graph()->GraphEditor()->Editing();
  }
  cdebug_out << "GraphExecutor::OutNode::CheckAllDone " << IsDone() << " this " << this << " "
             << Name() << " GraphAutomatonState "
             << theAutomaton->StateName( AutomatonState() )
             << " State " << State() << " Threads " << _Threads << " SuspendedThreads "
             << _SuspendedThreads << " EventQSize " << EventQSize() << endl ;
}

// asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
//Engines_Container_i * GraphExecutor::OutNode::SuperVisionContainer() {
//  return _SuperVisionContainer ;
//}

//const pthread_t GraphExecutor::OutNode::MainThreadId() const {
//  return _MainThreadId ;
//}

void GraphExecutor::OutNode::PThreadLock( pthread_mutex_t * aMutex , char * errmsg ) {
//  if ( strcmp( errmsg , "EventLoop" ) && strcmp( errmsg , "EventW" ) ) {
//    cdebug << "GraphExecutor::OutNode::PThreadLock " << pthread_self() << " " << aMutex << " "
//           << errmsg << endl ;
//  }
  if ( pthread_mutex_lock( aMutex ) ) {
    perror( errmsg ) ;
    exit( 0 ) ;
  }
//  if ( strcmp( errmsg , "EventLoop" ) && strcmp( errmsg , "EventW" ) ) {
//    cdebug << "GraphExecutor::OutNode::PThreadLocked " << pthread_self() << " " << aMutex << " "
//           << errmsg << endl ;
//  }
}

void GraphExecutor::OutNode::PThreadUnLock( pthread_mutex_t * aMutex , char * errmsg ) {
//  if ( strcmp( errmsg , "EventLoop" ) && strcmp( errmsg , "EventW" ) ) {
//    cdebug << " GraphExecutor::OutNode::PThreadUnLock " << pthread_self() << " " << aMutex << " "
//           << errmsg << endl ;
//  }
  if ( pthread_mutex_unlock( aMutex ) ) {
    perror( errmsg ) ;
    exit( 0 ) ;
  }
}

void GraphExecutor::OutNode::PyThreadLock() {
//  cout << " GraphExecutor::OutNode::PyThreadLock " << pthread_self() << endl ;
  if ( pthread_mutex_lock( &_PyMutexWait ) ) {
    perror( "GraphExecutor::OutNode::PyThreadLock" ) ;
    exit( 0 ) ;
  }
  theAutomaton->PyLock() ;
//  cout << " GraphExecutor::OutNode::PyThreadLocked " << pthread_self() << endl ;
}

void GraphExecutor::OutNode::PyThreadUnLock() {
//  cout << " GraphExecutor::OutNode::PyThreadUnLock " << pthread_self() << endl ;
  if ( pthread_mutex_unlock( &_PyMutexWait ) ) {
    perror( "GraphExecutor::OutNode::PyThreadUnLock" ) ;
    exit( 0 ) ;
  }
  theAutomaton->PyUnLock() ;
//  cout << " GraphExecutor::OutNode::PyThreadUnLocked " << pthread_self() << endl ;
}

void GraphExecutor::OutNode::NewThread() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _NewThread") ;
    exit( 0 ) ;
  }
  _Threads += 1 ;
//  cdebug << "NewThread : " << _Threads << " running threads "
//         << _SuspendedThreads << " suspended threads"
//         << endl ;
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_unlock _NewThread") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::OutNode::ExitThread( pthread_t ThreadNumber ) {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _ExitThread") ;
    exit( 0 ) ;
  }
  if ( ThreadNumber == 0 ) {
    ThreadNumber = pthread_self() ;
  }
  _Threads -= 1 ;
//  cdebug << pthread_self() << " ExitThread( " << ThreadNumber << " ) " << _Threads << " running threads "
//         << _SuspendedThreads << " suspended threads"
//         << endl ;
  theAutomaton->JoinThread( ThreadNumber ) ;
  if ( pthread_cond_signal( &_JoinWait ) ) {
    perror("ExitThread pthread_cond_signal ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_unlock _ExitThread") ;
    exit( 0 ) ;
  }
  if ( _Threads == 0 && _SuspendedThreads == 0 ) {
    CheckAllDone() ;
    cdebug << pthread_self() << " ExitThread( " << ThreadNumber << " ) " << _Threads
           << " running threads " << _SuspendedThreads << " suspended threads IsDone() "
           << IsDone() ;
    _run_func_thread = 0 ; // mkr : IPAL14711
    if ( IsDone() && Graph()->GraphMacroLevel() == 0 ) {
      cdebug << " ==> theAutomaton->Executed() " << endl ;
      theAutomaton->Executed() ;
    }
    else {
      cdebug << " NO theAutomaton->Executed() " << endl ;
    }
  }
}
void GraphExecutor::OutNode::JoinedWait() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock JoinedWait") ;
    exit( 0 ) ;
  }
  while ( _Threads ) {
    if ( pthread_cond_wait( &_JoinWait , &_MutexWait ) ) {
      perror("JoinedWait pthread_cond_wait ") ;
    }
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_unlock JoinedWait") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::OutNode::SuspendThread() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _SuspendThread") ;
    exit( 0 ) ;
  }
  _SuspendedThreads += 1 ;
  cdebug << "SuspendThread : " << _Threads << " running threads "
         << _SuspendedThreads << " suspended threads"
         << endl ;
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_unlock _SuspendThread") ;
    exit( 0 ) ;
  }
  if ( IsSuspended() ) {
    PushEvent( NULL , GraphExecutor::SuspendEvent , GraphExecutor::SuspendedState ) ;
  }
}
void GraphExecutor::OutNode::ResumeThread() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _ResumeThread") ;
    exit( 0 ) ;
  }
  _SuspendedThreads -= 1 ;
  cdebug << "ResumeThread : " << _Threads << " running threads "
         << _SuspendedThreads << " suspended threads"
         << endl ;
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_unlock _ResumeThread") ;
    exit( 0 ) ;
  }
}

long GraphExecutor::OutNode::Thread( const char * aNodeName ) {
  long RetVal = 0 ;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( aNodeName )->GetInNode() ;
  if ( anInNode ) {
    RetVal = anInNode->ThreadNo() ;
  }
  return RetVal ;
}

SUPERV::GraphEvent GraphExecutor::OutNode::AutomatonGraphEvent(GraphExecutor::NodeEvent anEvent ) {
  SUPERV::GraphEvent aGraphEvent ;
  switch ( anEvent ) {
  case GraphExecutor::UndefinedEvent : {
    aGraphEvent = SUPERV::UndefinedEvent ;
    break ;
  }
  case GraphExecutor::NewThreadEvent : {
    aGraphEvent = SUPERV::NewThreadEvent ;
    break ;
  }
  case GraphExecutor::SuspendEvent : {
    aGraphEvent = SUPERV::SuspendEvent ;
    break ;
  }
  case GraphExecutor::ResumeEvent : {
    aGraphEvent = SUPERV::ResumeEvent ;
    break ;
  }
  case GraphExecutor::KillEvent : {
    aGraphEvent = SUPERV::KillEvent ;
    break ;
  }
  case GraphExecutor::StopEvent : {
    aGraphEvent = SUPERV::StopEvent ;
    break ;
  }
  case GraphExecutor::ExecuteEvent : {
    aGraphEvent = SUPERV::RunningEvent ;
    break ;
  }
  case GraphExecutor::SuccessEvent : {
    aGraphEvent = SUPERV::DoneEvent ;
    break ;
  }
  case GraphExecutor::ErrorEvent : {
    aGraphEvent = SUPERV::ErroredEvent ;
    break ;
  }
  case GraphExecutor::NoDataReadyEvent : {
    aGraphEvent = SUPERV::WaitingEvent ;
    break ;
  }
  case GraphExecutor::SomeDataReadyEvent : {
    aGraphEvent = SUPERV::WaitingEvent ;
    break ;
  }
  case GraphExecutor::NotAllDataReadyEvent : {
    aGraphEvent = SUPERV::WaitingEvent ;
    break ;
  }
  case GraphExecutor::AllDataReadyEvent : {
    aGraphEvent = SUPERV::ReadyEvent ;
    break ;
  }
  case GraphExecutor::ReadyEvent : {
    aGraphEvent = SUPERV::ReadyEvent ;
    break ;
  }
  case GraphExecutor::SuspendedReadyEvent : {
    aGraphEvent = SUPERV::SuspendEvent ;
    break ;
  }
  case GraphExecutor::ResumedReadyEvent : {
    aGraphEvent = SUPERV::ResumeEvent ;
    break ;
  }
  case GraphExecutor::KilledReadyEvent : {
    aGraphEvent = SUPERV::KillEvent ;
    break ;
  }
  case GraphExecutor::StoppedReadyEvent : {
    aGraphEvent = SUPERV::StopEvent ;
    break ;
  }
  case GraphExecutor::ExecutingEvent : {
    aGraphEvent = SUPERV::RunningEvent ;
    break ;
  }
  case GraphExecutor::SuspendedExecutingEvent : {
    aGraphEvent = SUPERV::SuspendEvent ;
    break ;
  }
  case GraphExecutor::ResumedExecutingEvent : {
    aGraphEvent = SUPERV::ResumeEvent ;
    break ;
  }
  case GraphExecutor::KilledExecutingEvent : {
    aGraphEvent = SUPERV::KillEvent ;
    break ;
  }
  case GraphExecutor::StoppedExecutingEvent : {
    aGraphEvent = SUPERV::StopEvent ;
    break ;
  }
  case GraphExecutor::SuccessedExecutingEvent : {
    aGraphEvent = SUPERV::DoneEvent ;
    break ;
  }
  case GraphExecutor::ErroredExecutingEvent : {
    aGraphEvent = SUPERV:: ErroredEvent;
    break ;
  }
  case GraphExecutor::SuspendedSuccessedEvent : {
    aGraphEvent = SUPERV::SuspendEvent ;
    break ;
  }
  case GraphExecutor::SuspendedErroredEvent : {
    aGraphEvent = SUPERV::SuspendEvent ;
    break ;
  }
  case GraphExecutor::ResumedSuccessedEvent : {
    aGraphEvent = SUPERV::ResumeEvent ;
    break ;
  }
  case GraphExecutor::ResumedErroredEvent : {
    aGraphEvent = SUPERV::ResumeEvent ;
    break ;
  }
  case GraphExecutor::KilledEvent : {
    aGraphEvent = SUPERV::KillEvent ;
    break ;
  }
  case GraphExecutor::StoppedEvent : {
    aGraphEvent = SUPERV::StopEvent ;
    break ;
  }
  case GraphExecutor::EndExecuteEvent : {
    aGraphEvent = SUPERV::DoneEvent ;
    break ;
  }
  default : {
    cdebug << " GraphExecutor::OutNode::AutomatonGraphEvent Error Undefined Event : "
           << anEvent << endl ;
    aGraphEvent = SUPERV::UndefinedEvent ;
  }
  }
  return aGraphEvent ;
}

SUPERV::GraphState GraphExecutor::OutNode::AutomatonGraphState(GraphExecutor::AutomatonState aState ) {
  SUPERV::GraphState aGraphState ;
  switch ( aState ) {
  case GraphExecutor::UnKnownState : {
    aGraphState = SUPERV::UndefinedState ;
    break ;
  }
  case GraphExecutor::DataUndefState : {
    aGraphState = SUPERV::UndefinedState ;
    break ;
  }
  case GraphExecutor::DataWaitingState : {
    aGraphState = SUPERV::WaitingState ;
    break ;
  }
  case GraphExecutor::DataReadyState : {
    aGraphState = SUPERV::ReadyState ;
    break ;
  }
  case GraphExecutor::SuspendedReadyState : {
    aGraphState = SUPERV::SuspendReadyState ;
    break ;
  }
  case GraphExecutor::ResumedReadyState : {
    aGraphState = SUPERV::ReadyState ;
    break ;
  }
  case GraphExecutor::KilledReadyState : {
    aGraphState = SUPERV::KillState ;
    break ;
  }
  case GraphExecutor::StoppedReadyState : {
    aGraphState = SUPERV::StopState ;
    break ;
  }
  case GraphExecutor::ExecutingState : {
    aGraphState = SUPERV::RunningState ;
    break ;
  }
  case GraphExecutor::SuspendedExecutingState : {
    aGraphState = SUPERV::SuspendState ;
    break ;
  }
  case GraphExecutor::ResumedExecutingState : {
    aGraphState = SUPERV::RunningState ;
    break ;
  }
  case GraphExecutor::KilledExecutingState : {
    aGraphState = SUPERV::KillState ;
    break ;
  }
  case GraphExecutor::StoppedExecutingState : {
    aGraphState = SUPERV::StopState ;
    break ;
  }
  case GraphExecutor::SuccessedExecutingState : {
    aGraphState = SUPERV::DoneState ;
    break ;
  }
  case GraphExecutor::ErroredExecutingState : {
    aGraphState = SUPERV::ErrorState ;
    break ;
  }
  case GraphExecutor::SuspendedSuccessedState : {
    aGraphState = SUPERV::SuspendDoneState ;
    break ;
  }
  case GraphExecutor::SuspendedErroredState : {
    aGraphState = SUPERV::SuspendErroredState ;
    break ;
  }
  case GraphExecutor::ResumedSuccessedState : {
    aGraphState = SUPERV::DoneState ;
    break ;
  }
  case GraphExecutor::ResumedErroredState : {
    aGraphState = SUPERV::ErrorState ;
    break ;
  }
  case GraphExecutor::KilledSuccessedState : {
    aGraphState = SUPERV::KillState ;
    break ;
  }
  case GraphExecutor::KilledErroredState : {
    aGraphState = SUPERV::KillState ;
    break ;
  }
  case GraphExecutor::StoppedSuccessedState : {
    aGraphState = SUPERV::StopState ;
    break ;
  }
  case GraphExecutor::StoppedErroredState : {
    aGraphState = SUPERV::StopState ;
    break ;
  }
  case GraphExecutor::SuccessedState : {
    aGraphState = SUPERV::DoneState ;
    break ;
  }
  case GraphExecutor::ErroredState : {
    aGraphState = SUPERV::ErrorState ;
    break ;
  }
  case GraphExecutor::SuspendedState : {
    aGraphState = SUPERV::SuspendState ;
    break ;
  }
  case GraphExecutor::KilledState : {
    aGraphState = SUPERV::KillState ;
    break ;
  }
  case GraphExecutor::StoppedState : {
    aGraphState = SUPERV::StopState ;
    break ;
  }
  case GraphExecutor::LoadingState : {
    aGraphState = SUPERV::LoadingState ;
    break;
  }
  default : {
    cdebug << " GraphExecutor::OutNode::AutomatonGraphState Error Undefined State : "
           << aGraphState << endl ;
    aGraphState = SUPERV::UndefinedState ;
  }
  }
  return aGraphState ;
}

//JR 07.04.2005, Debug :  access to _EventNodes must be inside the pthread_mutex_lock
int GraphExecutor::OutNode::GetListSize() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("PushEvent pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  int eventnbr = _EventNodes.size();
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("PushEvent pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
  return eventnbr ;
}

bool GraphExecutor::OutNode::PushEvent( GraphExecutor::InNode * aNode ,
                                        GraphExecutor::NodeEvent anEvent ,
                                        GraphExecutor::AutomatonState aState ) {
//  cdebug_in << "PushEvent Threads " << Threads() << " SuspendedThreads "
//            << SuspendedThreads() << endl ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("PushEvent pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  char* aNodeName = aNode ? aNode->Name() : Name() ;
  _EventNodes.push_back( aNodeName ) ;
  _Events.push_back( anEvent ) ;
  _States.push_back( aState ) ;

  pthread_cond_broadcast( &_EventWait );
//  cdebug << pthread_self() << " PushEvent Threads " << Threads() << " SuspendedThreads "
//         << SuspendedThreads() << " pthread_cond_broadcast _EventWait " << &_EventWait << endl ;
  if ( aNode ) {
//    cdebug << aNode->ThreadNo() << " PushEvent " << aNode->Name() ;
//    cdebug << " " << aNode->Automaton()->EventName( anEvent )
//           << " " << aNode->Automaton()->StateName( aState )
//           << " ControleState "
//           << aNode->Automaton()->ControlStateName( aNode->ControlState() ) ;
  }
  else {
//    cdebug << "PushEvent " << Name() ;
//    cdebug << " " << theAutomaton->EventName( anEvent ) << " "
//           << theAutomaton->StateName( aState ) ;
  }
//  cdebug_out << "PushEvent Threads " << Threads() << " SuspendedThreads "
//             << SuspendedThreads() << endl ;

//PAL8520
//JR 21.02.2005 Debug : old events should be removed at creation time for python-clients
  int EventNodesSize = _EventNodes.size() ;
  if ( EventNodesSize > Graph()->GraphNodesSize()*70 ) {
    siftEvents();
//    cdebug << "OutNode::PushEvent " << _EventNodes.size() << " in queue instead of "
//           << EventNodesSize << endl ;
  }

  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("PushEvent pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
  return true ;
}

bool GraphExecutor::OutNode::StateWait( SUPERV::GraphState aState ) {
  return false ;
}

bool GraphExecutor::OutNode::Event( char ** aNodeName ,
                                    SUPERV::GraphEvent & anEvent ,
                                    SUPERV::GraphState & aState ,
                                    bool WithWait ) {
//PAL8520
//JR 23.02.2005, Debug : siftEvents() must be inside the pthread_mutex_lock
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("EventLoop pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  // asv : fixing problem of loops with large number of iterations (>5000)
  // experimentally was found that number of non-handled by GUI events = (number_of_nodes x 7)
  // or 7 events for each node - so what we want to do here is remove "old" events for nodes
  // "on the fly".  Once again it is done in Stop(), Kill(), Done() functions.
  int EventNodessize = _EventNodes.size() ;
  if ( EventNodessize > Graph()->GraphNodesSize()*7 )
    siftEvents();

  int ThreadsNumber ;
  int SuspendedThreadsNumber ;
  bool cdebuginout = false ;
  if ( _EventNodes.size() > 0 ) {
//    cdebug_in << "GraphExecutor::OutNode::Event " << _EventNodes.size() << " in queue instead of "
//              << EventNodessize << endl ;
    cdebuginout = true ;
  }
  _JustStarted = false ;
  ThreadsNumber = Threads() ;
  SuspendedThreadsNumber = SuspendedThreads() ;
  bool RetVal = ( ThreadsNumber - SuspendedThreadsNumber) != 0 ||
                _EventNodes.size() > 0 ;
  char * NodeName = "" ;
  GraphExecutor::NodeEvent theEvent = GraphExecutor::UndefinedEvent ;
  GraphExecutor::AutomatonState theState = GraphExecutor::UnKnownState ;
  anEvent = SUPERV::UndefinedEvent ;
  aState = SUPERV::UndefinedState ;
  if ( ( Done() || IsKilled() || IsStopped() ) && _EventNodes.size() == 0 ) {
//    cdebug << "EventLoop Done()/IsKilled()/IsStopped() && _EventNodes.size() == 0" << endl ;
    RetVal = false ;
  }
  else if ( !WithWait && _EventNodes.size() == 0 ) {
    anEvent = SUPERV::NoEvent ;
    aState = SUPERV::NoState ;
    RetVal = true ;
  }
  else if ( RetVal ) {
    while ( !IsSuspended() && _EventNodes.size() == 0 ) {
//      cdebug << "EventLoop pthread_cond_wait _EventWait" << endl ;
      pthread_cond_wait( &_EventWait , &_MutexWait );
//      cdebug << "EventLoop pthread_cond_waited _EventWait"
//             << " _EventNodes.size() " << _EventNodes.size() << endl ;
    }
    if ( _EventNodes.size() ) {
      ThreadsNumber = Threads() ;
      NodeName = _EventNodes.front() ;
      _EventNodes.pop_front() ;
      theEvent = _Events.front() ;
      anEvent = AutomatonGraphEvent( theEvent ) ;
      _Events.pop_front() ;
      theState = _States.front() ;
      aState = AutomatonGraphState( theState ) ;
      _States.pop_front() ;
    }
  }
  *aNodeName = NodeName ;
  if ( IsSuspended() && _EventNodes.size() == 0 ) {
    RetVal = false ;
  }
  if ( anEvent != SUPERV::NoEvent ) {
//    cdebug << pthread_self() << "EventLoop "
//           << NodeName << " " << theAutomaton->StateName( theState )
//           << " _EventNodes.size() " << _EventNodes.size()
//           << " Threads " << Threads() << " SuspendedThreads "
//           << SuspendedThreads() << " RetVal " << RetVal << endl ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("EventLoop pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( cdebuginout ) {
//    cdebug_out << "GraphExecutor::OutNode::Event " << _EventNodes.size() << " in queue"
//               << *aNodeName << " " << anEvent << " " << aState << endl ;
  }
  return RetVal ;
}

bool GraphExecutor::OutNode::EventW( char ** aNodeName ,
                                     SUPERV::GraphEvent & anEvent ,
                                     SUPERV::GraphState & aState ) {
  bool sts = true ;
  char * NodeName ;
  aState = SUPERV::UndefinedState ;
  while ( sts &&
          aState != SUPERV::SuspendReadyState &&
          aState != SUPERV::RunningState &&
          aState != SUPERV::SuspendDoneState &&
          aState != SUPERV::SuspendErroredState ) {
    NodeName = Name() ;
    while ( sts && !strcmp( NodeName , Name() ) ) {
      sts = EventWait( aNodeName , anEvent , aState ) ;
      NodeName = *aNodeName ;
    }
  }
  return sts ;
}

bool GraphExecutor::OutNode::EventWait( char ** aNodeName ,
                                        SUPERV::GraphEvent & anEvent ,
                                        SUPERV::GraphState & aState ) {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("EventW pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  int ThreadsNumber ;
  int SuspendedThreadsNumber ;
  ThreadsNumber = Threads() ;
  SuspendedThreadsNumber = SuspendedThreads() ;
  bool RetVal = ( ThreadsNumber - SuspendedThreadsNumber) != 0 ||
                _EventNodes.size() > 0 ;
  cdebug << "--> EventW RetVal " << RetVal << endl ;
  char * NodeName = "" ;
  GraphExecutor::NodeEvent theEvent = GraphExecutor::UndefinedEvent ;
  GraphExecutor::AutomatonState theState = GraphExecutor::UnKnownState ;
  anEvent = SUPERV::UndefinedEvent ;
  aState = SUPERV::UndefinedState ;
  if ( IsDone() && _EventNodes.size() == 0 ) {
    cdebug << "EventW IsDone() && _EventNodes.size() == 0" << endl ;
    RetVal = 0 ;
  }
  else if ( RetVal ) {
    GraphExecutor::InNode * aNode = NULL ;
    while ( aNode == NULL && RetVal ) {
      NodeName = _EventNodes.front() ;
      theEvent = _Events.front() ;
      anEvent = AutomatonGraphEvent( theEvent ) ;
      theState = _States.front() ;
      aState = AutomatonGraphState( theState ) ;

      if ( _JustStarted ) {
        _JustStarted = false ;
      }
      else {
        _EventNodes.pop_front() ;
        _Events.pop_front() ;
        _States.pop_front() ;
      }

      aNode = ((GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode()) ;
      cdebug << "EventW Previous Node " << NodeName << " ThreadsNumber "
             << ThreadsNumber
             << " _EventNodes.size() " << _EventNodes.size() << " "
             << theAutomaton->StateName( theState )
             << " Threads " << Threads() << " SuspendedThreads "
             << SuspendedThreads() << endl ;
      if ( aNode ) {
      }
      else if ( IsDone() && _EventNodes.size() == 0 ) {
        cdebug << "EventW IsDone() && _EventNodes.size() == 0" << endl ;
        RetVal = 0 ;
      }
      else {
        cdebug << "EventW Not InNode " << NodeName
               << " _EventNodes.size() " << _EventNodes.size() << endl ;
        while ( _EventNodes.size() == 0 ) {
          pthread_cond_wait( &_EventWait , &_MutexWait );
        }
        cdebug << "EventW pthread_cond_waited Not InNode " << NodeName
               << " _EventNodes.size() " << _EventNodes.size() << endl ;
      }
    }

    if ( aNode ) {
      if ( aState == SUPERV::SuspendState ||
           aState == SUPERV::SuspendReadyState ||
           aState == SUPERV::SuspendDoneState ||
           aState == SUPERV::SuspendErroredState ) {
        aNode->ControlState( SUPERV::ToSuspendState ) ;
        if ( aNode->IsSuspended() ) {
          if ( pthread_mutex_unlock( &_MutexWait ) ) {
            perror("EventW pthread_mutex_lock ") ;
            exit( 0 ) ;
          }
          cdebug << "EventW " << aNode->Name() << " ResumeAction" << endl ;
          aNode->ResumeAction( GraphExecutor::ToResumeEvent ) ;
          cdebug << "EventW " << aNode->Name() << " ResumedAction" << endl ;
          if ( pthread_mutex_lock( &_MutexWait ) ) {
            perror("EventW pthread_mutex_lock ") ;
            exit( 0 ) ;
          }
	}
        else {
          cdebug << "EventW inconsistent SuspendState" << endl ;
          RetVal = false ; 
	}
      }
      else {
        if ( aNode->IsDone() ) {
          RetVal = true ;
	}
        else {
          cdebug << "EventW NOT SuspendedState _EventNodes.size() "
                 << _EventNodes.size() << endl ;
          RetVal = true ;
	}
      }
      if ( RetVal ) {
        cdebug << "EventW " << aNode->Name() << " pthread_cond_wait" << endl ;
        while ( _EventNodes.size() == 0 ) {
          pthread_cond_wait( &_EventWait , &_MutexWait );
        }
        ThreadsNumber = Threads() ;
        NodeName = _EventNodes.front() ;
        theEvent = _Events.front() ;
        anEvent = AutomatonGraphEvent( theEvent ) ;
        theState = _States.front() ;
        aState = AutomatonGraphState( theState ) ;
      }
    }
  }
  *aNodeName = NodeName ;
  cdebug << "<-- EventW RetVal " << RetVal << " " << NodeName
         << " Threads " << Threads() << " SuspendedThreads "
         << SuspendedThreads()
         << " _EventNodes.size() " << _EventNodes.size()
         << " " << theAutomaton->EventName( theEvent ) << " "
         << theAutomaton->StateName( theState ) << endl ;
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("EventW pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  return RetVal ;
}
long GraphExecutor::OutNode::EventQSize() {
  return _EventNodes.size() ;
}

void GraphExecutor::OutNode::EventList() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("EventList pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  list< char * >::iterator itEventNodes = _EventNodes.begin() ;
  list< GraphExecutor::NodeEvent >::iterator itEvents = _Events.begin() ;
  list< GraphExecutor::AutomatonState >::iterator itStates = _States.begin() ;
  while ( itEventNodes != _EventNodes.end() ) {
    cdebug << pthread_self() << "EVENTSTACK "
           << *itEventNodes << " " << *itEvents << " "
           << theAutomaton->StateName( *itStates )
           << " Threads " << Threads() << " SuspendedThreads " << SuspendedThreads() << endl ;
    itEventNodes++ ;
    itEvents++ ;
    itStates++ ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("EventList pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::OutNode::SendSomeDataReady( const char * aNodeName ) {
  cdebug_in << "GraphExecutor::OutNode::SendSomeDataReady " << aNodeName << "->SendSomeDataReady( "
            << Name() << " )" << endl;
  int sts ;
  sts = ((GraphExecutor::InNode * ) Graph()->GetChangeGraphNode( aNodeName )->GetInNode())->SendSomeDataReady( Name() ) ;
  cdebug_out << "GraphExecutor::OutNode::SendSomeDataReady sts " << sts << endl ;
}

void GraphExecutor::OutNode::State(GraphExecutor::AutomatonState aState ) {
//  cdebug << "GraphExecutor::OutNode::State " << Name() << " "
//         << theAutomaton->StateName( AutomatonGraphState( _State ) ) << " ---> "
//         << theAutomaton->StateName( AutomatonGraphState( aState ) ) << endl ;
  _State = aState ;
}

SUPERV::GraphState GraphExecutor::OutNode::State() {
//  cdebug_in << "GraphExecutor::OutNode::State" << endl;
//  cdebug_out << "GraphExecutor::OutNode::State" << endl ;
//  cdebug << "GraphExecutor::OutNode::State GraphState "
//         << theAutomaton->StateName( AutomatonGraphState( _State ) ) << endl ;
  return AutomatonGraphState( _State ) ;
}

SUPERV::GraphState GraphExecutor::OutNode::State( const char * NodeName ) {
//  cdebug_in << "GraphExecutor::OutNode::State " << NodeName << endl;
  GraphExecutor::AutomatonState aret = GraphExecutor::UnKnownState ;
  const GraphBase::ComputingNode * aCNode =  Graph()->GetGraphNode( NodeName ) ;
  if ( aCNode ) {
    GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *)aCNode->GetInNode() ;
    if ( anInNode ) {
      aret = anInNode->State() ;
      // asv 27.01.05 - fix for PAL7849, return "Loading" state only if internal state is SUPERV::RunningState
      if ( ( aret == GraphExecutor::ExecutingState || 
	     aret == GraphExecutor::ResumedExecutingState ) && 
	  anInNode->IsLoading() )
	aret = GraphExecutor::LoadingState;
//      cdebug << "GraphExecutor::OutNode::State( " << NodeName << " ) "
//             << theAutomaton->StateName( AutomatonGraphState( aret ) ) << endl ;
    }
  }
//  cdebug_out << "GraphExecutor::OutNode::State" << endl ;
  return AutomatonGraphState( aret ) ;
}

SUPERV::GraphState GraphExecutor::OutNode::State( const char * NodeName ,
                                                  const char * ServiceParameterName )  {
//  cdebug_in << "GraphExecutor::OutNode::State " << NodeName << " "
//            << ServiceParameterName<< endl;
  SUPERV::GraphState aret = Graph()->PortState( NodeName , ServiceParameterName ) ;
//  cdebug_out << "GraphExecutor::OutNode::State" << endl ;
  return aret ;
}

GraphExecutor::AutomatonState GraphExecutor::OutNode::AutomatonState() {
//  cdebug_in << "GraphExecutor::OutNode::AutomatonState" << endl;
//  cdebug_out << "GraphExecutor::OutNode::AutomatonState" << endl ;
  return _State ;
}

GraphExecutor::AutomatonState GraphExecutor::OutNode::AutomatonState( const char * NodeName ) {
//  cdebug_in << "GraphExecutor::OutNode::AutomatonState " << NodeName << endl;
  GraphExecutor::AutomatonState aret = GraphExecutor::UnKnownState ;
  const GraphBase::ComputingNode * aNode = Graph()->GetGraphNode( NodeName ) ;
  if ( aNode ) {
    GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) aNode->GetInNode() ;
    if ( anInNode ) {
      aret = anInNode->State() ;
    }
  }
//  cdebug_out << "GraphExecutor::OutNode::AutomatonState" << endl ;
  return aret ;
}

SUPERV::ControlState GraphExecutor::OutNode::ControlState() {
//  cdebug_in << "GraphExecutor::OutNode::ControlState" << endl;
//  cdebug_out << "GraphExecutor::OutNode::ControlState" << endl ;
  return _ControlState ;
}

SUPERV::ControlState GraphExecutor::OutNode::ControlState( const char * NodeName ) {
//  cdebug_in << "GraphExecutor::OutNode::ControlState " << NodeName << endl;
  SUPERV::ControlState aret = SUPERV::VoidState ;
  const GraphBase::ComputingNode * aNode = Graph()->GetGraphNode( NodeName ) ;
  if ( aNode ) {
    GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) aNode->GetInNode() ;
    if ( anInNode ) {
      aret = anInNode->ControlState() ;
    }
  }
//  cdebug_out << "GraphExecutor::OutNode::ControlState" << endl ;
  return aret ;
}

void GraphExecutor::OutNode::ControlClear() {
//  cdebug_in << "GraphExecutor::OutNode::ControlClear" << endl;
//  cdebug_out << "GraphExecutor::OutNode::ControlClear" << endl ;
  _ControlState = SUPERV::VoidState;
}

void GraphExecutor::OutNode::ControlClear( const char * NodeName ) {
//  cdebug_in << "GraphExecutor::OutNode::ControlClear " << NodeName << endl;
  const GraphBase::ComputingNode * aNode = Graph()->GetGraphNode( NodeName ) ;
  if ( aNode ) {
    GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) aNode->GetInNode() ;
    if ( anInNode ) {
      anInNode->ControlClear() ;
    }
  }
//  cdebug_out << "GraphExecutor::OutNode::ControlClear" << endl ;
}

bool GraphExecutor::OutNode::IsWaiting() {
//  cdebug_in << "GraphExecutor::OutNode::IsWaiting" << endl;
//  cdebug_out << "GraphExecutor::OutNode::IsWaiting" << endl ;
  return !IsDone() ;
}

bool GraphExecutor::OutNode::IsReady() {
//  cdebug_in << "GraphExecutor::OutNode::IsReady" << endl;
//  cdebug_out << "GraphExecutor::OutNode::IsReady" << endl ;
  return !IsDone() ;
}

bool GraphExecutor::OutNode::IsRunning() {
//  cdebug_in << "GraphExecutor::OutNode::IsRunning" << endl;
//  cdebug_out << "GraphExecutor::OutNode::IsRunning" << endl ;
  return !IsDone() ;
}

bool GraphExecutor::OutNode::IsDone() {
//  cdebug_in << "GraphExecutor::OutNode::IsDone" << endl;
//  cdebug_out << "GraphExecutor::OutNode::IsDone" << endl ;
  return ( Done() || IsKilled() || IsStopped() ) ;
}

bool GraphExecutor::OutNode::IsSuspended() {
//  cdebug_in << "GraphExecutor::OutNode::IsSuspended" << endl;
  bool aret = false ;
//  if ( _SuspendedThreads == _Threads && _Threads != 0 ) {
  if ( AutomatonGraphState( _State ) ==  SUPERV::SuspendState ) {
    aret = true ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsSuspended" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsKilled() {
//  cdebug_in << "GraphExecutor::OutNode::IsKilled" << endl;
  bool aret = false ;
  if ( AutomatonGraphState( _State ) == SUPERV::KillState ) {
    aret = true ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsKilled" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsStopped() {
//  cdebug_in << "GraphExecutor::OutNode::IsStopped" << endl;
  bool aret = false ;
  if ( AutomatonGraphState( _State ) == SUPERV::StopState ) {
    aret = true ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsStopped" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsWaiting( const char * NodeName ) {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::OutNode::IsWaiting " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->IsWaiting() ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsWaiting" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsReady( const char * NodeName ) {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::OutNode::IsReady " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->IsReady() ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsReady" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsRunning( const char * NodeName ) {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::OutNode::IsRunning " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->IsRunning() ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsRunning" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsDone( const char * NodeName ) {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::OutNode::IsDone " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->IsDone() ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsDone" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::IsSuspended( const char * NodeName ) {
  bool aret = false ;
//  cdebug_in << "GraphExecutor::OutNode::IsSuspended " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->IsSuspended() ;
  }
//  cdebug_out << "GraphExecutor::OutNode::IsSuspended" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::PortDone( const char * NodeName ,
                                       const char * ServiceParameterName )  {
//  cdebug_in << "GraphExecutor::OutNode::IsDone " << NodeName << " "
//            << ServiceParameterName<< endl;
  bool aret = Graph()->PortDone( NodeName , ServiceParameterName ) ;
//  cdebug_out << "GraphExecutor::OutNode::IsDone" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::ContainerKill() {
  bool RetVal = true ;
  cdebug_in << "GraphExecutor::OutNode::ContainerKill" << endl;
  _ControlState = SUPERV::ToSuspendState ;
  int i ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
    bool sts = aNode->ContainerKill() ;
    if ( sts && aNode->IsKilled() ) {
      cdebug << aNode->Name() << " killed" << endl ;
    }
    else if ( aNode->IsWaiting() || aNode->IsDone() ) {
      cdebug << aNode->Name() << " not killed : "
             << theAutomaton->StateName( aNode->State() ) << endl ;
    }
    else {
      cdebug << aNode->Name() << " cannot be killed : "
             << theAutomaton->StateName( aNode->State() ) << endl ;
      RetVal = false ;
    }
  }
  if ( !RetVal || Threads() != 0 ) {
    for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
      GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
      if ( !aNode->IsKilled() && !aNode->IsWaiting() && !aNode->IsDone() ) {
        aNode->KilledAction() ;
      }
    }
    RetVal = true ;
  }
  State( GraphExecutor::KilledState ) ;
  cdebug_out << "GraphExecutor::OutNode::ContainerKill" << endl ;
  return RetVal ;
}

bool GraphExecutor::OutNode::Suspend() {
  int RetVal = 0 ;
  cdebug_in << "GraphExecutor::OutNode::Suspend" << endl;
//  _ControlState = SUPERV::ToSuspendState ;
  int i ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
    bool sts = aNode->Suspend() ;
    if ( sts && aNode->IsSuspended() ) {
      RetVal += 1 ;
      cdebug << aNode->Name() << " Suspended" << endl ;
    }
    else if ( aNode->IsWaiting() || aNode->IsReady() ) {
      RetVal += 1 ;
      cdebug << aNode->Name() << " will be Suspended" << endl ;
    }
    else if ( aNode->IsDone() ) {
      cdebug << aNode->Name() << " not Suspended : "
             << theAutomaton->StateName( aNode->State() ) << endl ;
    }
    else {
//      RetVal += 1 ;
      cdebug << aNode->Name() << " cannot be Suspended : "
             << theAutomaton->StateName( aNode->State() ) << endl ;
    }
  }
  if ( RetVal ) {
    State( GraphExecutor::SuspendedState ) ;
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS SUSPENDED" ) ;
    MESSAGE("================================================================================") ;
  }
  else {
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS NOT SUSPENDED" ) ;
    MESSAGE("================================================================================") ;
  }
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
    MESSAGE(aNode->Name() << " " << theAutomaton->StateName( aNode->State() ) ) ;
    cdebug << aNode->Name() << " " << theAutomaton->StateName( aNode->State() ) << endl ;
  }
  cdebug_out << "GraphExecutor::OutNode::Suspend " << theAutomaton->StateName( State() )
             << " EventQSize " << EventQSize() << endl ;
  return RetVal ;
}

bool GraphExecutor::OutNode::Resume() {
  int RetVal = 0 ;
  cdebug_in << "GraphExecutor::OutNode::Resume "
            << theAutomaton->StateName( State() ) << endl;
  if ( IsSuspended() ) {
    State( GraphExecutor::ExecutingState ) ;
    int i ;
    for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
      GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
      aNode->ControlState( SUPERV::VoidState ) ;
      if ( aNode->IsSuspended() ) {
        cdebug << aNode->Name() << "->Resume " << theAutomaton->StateName( aNode->State() )
               << endl ;
        if ( aNode->Resume() ) {
          cdebug << aNode->Name() << " Resumed " << theAutomaton->StateName( aNode->State() )
                 << endl ;
          RetVal += 1 ;
	}
        else {
          cdebug << aNode->Name() << " Resume failed"
                 << theAutomaton->StateName( aNode->State() ) << endl ;
        }
      }
      else {
        cdebug << aNode->Name() << " " << theAutomaton->StateName( aNode->State() )
               << endl ;
      }
    }
  }
  else {
    cdebug << Name() << " not suspended " << theAutomaton->StateName( State() ) << endl ;
  }
  if ( RetVal ) {
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS RESUMED" ) ;
    MESSAGE("================================================================================") ;
  }
  else {
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS NOT RESUMED" ) ;
    MESSAGE("================================================================================") ;
  }
  int i ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
    MESSAGE(aNode->Name() << " " << theAutomaton->StateName( aNode->State() ) ) ;
    cdebug << aNode->Name() << " " << theAutomaton->StateName( aNode->State() ) << endl ;
  }
  cdebug_out << "GraphExecutor::OutNode::Resume" << theAutomaton->StateName( State() ) << " " << RetVal
             << " EventQSize " << EventQSize() << endl ;
  return RetVal ;
}

bool GraphExecutor::OutNode::Kill() {
  bool RetVal = false ;
  cdebug_in << "GraphExecutor::OutNode::Kill"
            << " EventQSize " << EventQSize() << " State "
            << theAutomaton->StateName( State() ) << endl;
  if ( IsDone() ) {
    cdebug_out << "GraphExecutor::OutNode::Kill " << RetVal
               << " Threads " << _Threads << " SuspendedThreads " << _SuspendedThreads
               << " EventQSize " << EventQSize() << " State "
               << theAutomaton->StateName( State() ) << endl ;
    return RetVal ;
  }
  _ControlState = SUPERV::ToSuspendState ;
  int i ;
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
    bool sts = aNode->Kill() ;
    if ( sts && Threads() != 0 ) {
      if ( !aNode->IsKilled() ) {
        cdebug << aNode->Name() << " not killed : "
               << theAutomaton->StateName( aNode->State() ) << " " << aNode->Name() << "->"
               << "KilledAction()" << endl ;
        MESSAGE( aNode->Name() << " not killed : KilledAction()" ) ;
        aNode->KilledAction() ;
      }
      if ( aNode->IsKilled() ) {
        cdebug << aNode->Name() << " killed" << endl ;
        RetVal = true ;
      }
    }
//PAL8003
// JR 24.03.2005 Debug : the node may have been killed
    else if ( aNode->IsKilled() ) {
      cdebug << aNode->Name() << " killed" << endl ;
      RetVal = true ;
    }
    else if ( aNode->IsWaiting() || aNode->IsDone() ) {
      cdebug << aNode->Name() << " not killed : "
             << theAutomaton->StateName( aNode->State() ) << endl ;
    }
    else {
      cdebug << aNode->Name() << " cannot be killed : "
             << theAutomaton->StateName( aNode->State() ) << endl ;
    }
  }
  if ( RetVal ) {
    State( GraphExecutor::KilledState ) ;
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS KILLED" <<  theAutomaton->StateName( AutomatonState() ) << " EventQSize "
             << EventQSize() ) ;
    MESSAGE("================================================================================") ;
  }
  else {
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS NOT KILLED" ) ;
    MESSAGE("================================================================================") ;
  }
  for ( i = 0 ; i < Graph()->GraphNodesSize() ; i++ ) {
    GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) Graph()->GraphNodes( i )->GetInNode() ;
    MESSAGE(aNode->Name() << " " << theAutomaton->StateName( aNode->State() ) ) ;
    cdebug << aNode->Name() << " " << theAutomaton->StateName( aNode->State() ) << endl ;
  }

  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("EventLoop pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
//PAL8520
//JR 23.02.2005, Debug : siftEvents() must be inside the pthread_mutex_lock
  // remove "extra" events from the event queue
  siftEvents();
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("PushEvent pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }

  // asv : 25.10.04 : calling Editing() to indicate stop of execution
  if ( RetVal ) // mkr : IPAL10722 : we have to indicate stop of execution if only execution was successfully killed
    Graph()->GraphEditor()->Editing();

  cdebug_out << "GraphExecutor::OutNode::Kill " << RetVal
             << " Threads " << _Threads << " SuspendedThreads " << _SuspendedThreads
             << " EventQSize " << EventQSize() << endl ;

  return RetVal ;
}

bool GraphExecutor::OutNode::Stop() {
  bool RetVal = false ;
  cdebug_in << "GraphExecutor::OutNode::Stop" << endl;
  RetVal = Kill() ;
  cdebug_out << "GraphExecutor::OutNode::Stop" << endl ;
  if ( RetVal ) {
    MESSAGE("================================================================================") ;
    MESSAGE( Name() << " IS STOPPED" ) ;
    MESSAGE("================================================================================") ;
  }
  return RetVal ;
}

bool GraphExecutor::OutNode::ReadyWait() {
  cdebug_in << "GraphExecutor::OutNode::ReadyWait" << endl;
  bool aret ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _ReadyWait") ;
    exit( 0 ) ;
  }
  aret = IsReady() ;
  if ( !aret ) {
    Suspend() ;
    pthread_cond_wait( &_EventWait , &_MutexWait );
    aret = IsReady() ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _ReadyWait") ;
    exit( 0 ) ;
  }
  cdebug_out << "GraphExecutor::OutNode::ReadyWait" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::RunningWait() {
  cdebug_in << "GraphExecutor::OutNode::RunningWait" << endl;
  bool aret ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _RunningWait") ;
    exit( 0 ) ;
  }
  aret = IsRunning() ;
  if ( !aret ) {
    cdebug << "RunningWait pthread_cond_wait _EventWait" << endl;
    pthread_cond_wait( &_EventWait , &_MutexWait );
    aret = IsRunning() ;
    cdebug << "RunningWait pthread_cond_waited _EventWait " << aret << endl;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _RunningWait") ;
    exit( 0 ) ;
  }
  cdebug_out << "GraphExecutor::OutNode::RunningWait " << aret << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::DoneWait() {
  cdebug_in << pthread_self() << " GraphExecutor::DoneWait " << this << " " << Name() << endl;
  bool aret ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _DoneWait") ;
    exit( 0 ) ;
  }
  aret = IsDone() ;
  while ( !aret && !IsSuspended() && IsRunning() ) {
    cdebug << pthread_self() << " GraphExecutor::DoneWait " << this << " " << Name()
           << " DoneWait pthread_cond_wait _EventWait " << &_EventWait << endl;
    pthread_cond_wait( &_EventWait , &_MutexWait );
    aret = IsDone() ;
    cdebug << pthread_self() << " GraphExecutor::DoneWait " << this << " " << Name()
           << " DoneWait pthread_cond_waited _EventWaited " << &_EventWait << " : "<< aret << endl;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _DoneWait") ;
    exit( 0 ) ;
  }
  while ( _run_func_thread || theAutomaton->ThreadsNumber() ) {
    // mkr : IPAL14711
    // waiting for pthread_join in FiniteStateMachine
  }
  if ( Done() || IsKilled() ) Graph()->GraphEditor()->Editing(); // mkr : IPAL14572 : calling Editing() to indicate stop of execution
  cdebug_out << pthread_self() << " GraphExecutor::DoneWait " << this << " " << Name()
             << " " << State() << " : " << aret << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::SuspendedWait() {
  cdebug_in << "GraphExecutor::OutNode::SuspendedWait" << endl;
  bool aret ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _SuspendedWait") ;
    exit( 0 ) ;
  }
  aret = IsSuspended() ;
  while ( !aret && !IsDone() ) {
    pthread_cond_wait( &_EventWait , &_MutexWait );
    aret = IsSuspended() ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("pthread_mutex_lock _SuspendedWait") ;
    exit( 0 ) ;
  }
  cdebug_out << "GraphExecutor::OutNode::SuspendedWait" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::ReadyWait( const char * NodeName ) {
  bool aret = false ;
  cdebug_in << "GraphExecutor::OutNode::ReadyWait " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->ReadyWait() ;
  }
  cdebug_out << "GraphExecutor::OutNode::ReadyWait" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::RunningWait( const char * NodeName ) {
  bool aret = false ;
  cdebug_in << "GraphExecutor::OutNode::RunningWait " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->RunningWait() ;
  }
  cdebug_out << "GraphExecutor::OutNode::RunningWait" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::DoneWait( const char * NodeName ) {
  bool aret = false ;
  cdebug_in << "GraphExecutor::OutNode::DoneWait " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->DoneWait() ;
  }
  cdebug_out << "GraphExecutor::OutNode::DoneWait" << endl ;
  return aret ;
}

bool GraphExecutor::OutNode::SuspendedWait( const char * NodeName ) {
  bool aret = false ;
  cdebug_in << "GraphExecutor::OutNode::SuspendedWait " << NodeName << endl;
  GraphExecutor::InNode *anInNode = (GraphExecutor::InNode *) Graph()->GetGraphNode( NodeName )->GetInNode() ;
  if ( anInNode ) {
    aret = anInNode->SuspendedWait() ;
  }
  cdebug_out << "GraphExecutor::OutNode::SuspendedWait" << endl ;
  return aret ;
}

long GraphExecutor::OutNode::LastLevelDone() {
  int RetVal = -1 ;
  int i , j ;
  for ( i = 0 ; i <= Graph()->LevelMax() ; i++ ) {
    for ( j = 0 ; j <= Graph()->NodesNumber( i ) ; j++ ) {
      GraphBase::ComputingNode * aNode = Graph()->SortedNodes( i , j ) ;
      if ( !IsDone( aNode->Name() ) ) {
        break ;
      }
    }
    if ( j != Graph()->NodesNumber( i ) + 1 )
      break ;
    RetVal = i ;
  }
  return RetVal ;
}


//JR 30.03.2005const CORBA::Any *GraphExecutor::OutNode::GetInData( const char * NodeName ,
const CORBA::Any GraphExecutor::OutNode::GetInData( const char * NodeName ,
                                                    const char * ServiceParameterName ) {
//  cdebug_in << "GraphExecutor::OutNode::GetInData " << NodeName << " "
//            << ServiceParameterName << endl ;
//JR 30.03.2005  const CORBA::Any * retdata = Graph()->PortInData( NodeName , ServiceParameterName ) ;
  const CORBA::Any retdata = Graph()->PortInData( NodeName , ServiceParameterName ) ;
//  cdebug_out << "GraphExecutor::OutNode::GetInData" << endl ;
  return retdata ;
}

//JR 30.03.2005const CORBA::Any *GraphExecutor::OutNode::GetOutData( const char * NodeName ,
const CORBA::Any GraphExecutor::OutNode::GetOutData( const char * NodeName ,
                                                      const char * ServiceParameterName ) {
//  cdebug_in << "GraphExecutor::OutNode::GetOutData " << NodeName << " "
//            << ServiceParameterName << endl ;
//JR 30.03.2005  const CORBA::Any * retdata = Graph()->PortOutData( NodeName , ServiceParameterName ) ;
  const CORBA::Any retdata = Graph()->PortOutData( NodeName , ServiceParameterName ) ;
//  cdebug_out << "GraphExecutor::OutNode::GetOutData" << endl ;
  return retdata ;
}

const long GraphExecutor::OutNode::CpuUsed() {
  return Graph()->CpuUsed() ;
}

const long GraphExecutor::OutNode::CpuUsed( const char * aNodeName ) {
  GraphBase::ComputingNode * aNode = Graph()->GetChangeGraphNode( aNodeName ) ;
  if ( aNode ) {
    GraphExecutor::InNode * anInNode = (GraphExecutor::InNode * ) aNode->GetInNode() ;
    if ( anInNode ) {
      return anInNode->CpuUsed() ;
    }
  }
  return 0 ;
}

// remove equal sets "Node-Event-State" from _EventNodes/_Events/_States queues
void GraphExecutor::OutNode::siftEvents() {
  list<char*>::reverse_iterator itNodes = _EventNodes.rbegin();
  list<GraphExecutor::NodeEvent>::reverse_iterator itEvents = _Events.rbegin();
  list<GraphExecutor::AutomatonState>::reverse_iterator itStates = _States.rbegin();
  list<char*> _NewEventNodes;
  list<GraphExecutor::NodeEvent> _NewEvents;
  list<GraphExecutor::AutomatonState> _NewStates;
  for ( ; itNodes != _EventNodes.rend() ; ++itNodes, ++itEvents, ++itStates) {
    //cout << "----- aNodeName ==> " << *itNodes;
    list<char*>::reverse_iterator itNewNodes = _NewEventNodes.rbegin();
    bool found = false;
    for ( ; itNewNodes != _NewEventNodes.rend() ; ++itNewNodes ) {
      if ( !strcmp( *itNewNodes, *itNodes ) ) {
	found = true;
	break;
      }
    }

    if ( found ) {
      //cout << "   FOUND";
    }
    else {
      _NewEventNodes.push_back( *itNodes );
      _NewEvents.push_back( *itEvents );
      _NewStates.push_back( *itStates );
    }
    //cout << endl;
  }
  _EventNodes = _NewEventNodes;
  _Events = _NewEvents;
  _States = _NewStates;
}

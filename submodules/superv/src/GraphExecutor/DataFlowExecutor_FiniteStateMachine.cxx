//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_FiniteStateMachine.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>

#include "DataFlowExecutor_FiniteStateMachine.hxx"

void * start_function( void *p ) ;

// ControlStates :
#define VoidState           SUPERV::VoidState
#define ToSuspendStartState SUPERV::ToSuspendStartState
#define ToSuspendState      SUPERV::ToSuspendState
#define ToSuspendDoneState  SUPERV::ToSuspendDoneState
#define ToKillState         SUPERV::ToKillState
#define ToKillDoneState     SUPERV::ToKillDoneState
#define ToStopState         SUPERV::ToStopState

#if 0
// States :
#define UnKnownState            GraphExecutor::UnKnownState
#define DataUndefState            GraphExecutor::DataUndefState
#define DataWaitingState          GraphExecutor::DataWaitingState
#define DataReadyState          GraphExecutor::DataReadyState
#define SuspendedReadyState     GraphExecutor::SuspendedReadyState
#define SuspendedReadyToResumeState GraphExecutor::SuspendedReadyToResumeState
#define ResumedReadyState       GraphExecutor::ResumedReadyState
#define KilledReadyState        GraphExecutor::KilledReadyState
#define StoppedReadyState       GraphExecutor::StoppedReadyState
#define ExecutingState            GraphExecutor::ExecutingState
#define SuspendedExecutingState   GraphExecutor::SuspendedExecutingState
#define ResumedExecutingState     GraphExecutor::ResumedExecutingState
#define KilledExecutingState      GraphExecutor::KilledExecutingState
#define StoppedExecutingState     GraphExecutor::StoppedExecutingState
#define SuccessedExecutingState   GraphExecutor::SuccessedExecutingState
#define ErroredExecutingState     GraphExecutor::ErroredExecutingState
#define SuspendedSuccessedState GraphExecutor::SuspendedSuccessedState
#define SuspendedErroredState   GraphExecutor::SuspendedErroredState
#define SuspendedSuccessedToReStartState GraphExecutor::SuspendedSuccessedToReStartState
#define SuspendedErroredToReStartState   GraphExecutor::SuspendedErroredToReStartState
#define ResumedSuccessedState   GraphExecutor::ResumedSuccessedState
#define ResumedErroredState     GraphExecutor::ResumedErroredState
#define KilledSuccessedState    GraphExecutor::KilledSuccessedState
#define KilledErroredState      GraphExecutor::KilledErroredState
#define StoppedSuccessedState   GraphExecutor::StoppedSuccessedState
#define StoppedErroredState     GraphExecutor::StoppedErroredState
#define SuccessedState          GraphExecutor::SuccessedState
#define ErroredState            GraphExecutor::ErroredState
#define SuspendedState          GraphExecutor::SuspendedState
#define KilledState             GraphExecutor::KilledState
#define StoppedState            GraphExecutor::StoppedState
#define ReStartedState            GraphExecutor::ReStartedState
#define LoadingState              GraphExecutor::LoadingState
#define NumberOfAutomatonStates        GraphExecutor::NumberOfAutomatonStates
#endif

// Events :
#define UndefinedEvent        GraphExecutor::UndefinedEvent
#define NewThreadEvent        GraphExecutor::NewThreadEvent
#define SuspendEvent            GraphExecutor::SuspendEvent
#define ToResumeEvent           GraphExecutor::ToResumeEvent
#define ResumeEvent             GraphExecutor::ResumeEvent
#define KillEvent               GraphExecutor::KillEvent
#define StopEvent               GraphExecutor::StopEvent
#define ExecuteEvent            GraphExecutor::ExecuteEvent
#define SuccessEvent            GraphExecutor::SuccessEvent
#define ErrorEvent              GraphExecutor::ErrorEvent
#define ReStartEvent            GraphExecutor::ReStartEvent
#define ReStartAndSuspendEvent  GraphExecutor::ReStartAndSuspendEvent
#define NoDataReadyEvent      GraphExecutor::NoDataReadyEvent
#define SomeDataReadyEvent    GraphExecutor::SomeDataReadyEvent
#define NotAllDataReadyEvent  GraphExecutor::NotAllDataReadyEvent
#define AllDataReadyEvent     GraphExecutor::AllDataReadyEvent
#define ReadyEvent              GraphExecutor::ReadyEvent
#define SuspendedReadyEvent     GraphExecutor::SuspendedReadyEvent
#define ResumedReadyEvent       GraphExecutor::ResumedReadyEvent
#define KilledReadyEvent        GraphExecutor::KilledReadyEvent
#define StoppedReadyEvent       GraphExecutor::StoppedReadyEvent
#define ExecutingEvent            GraphExecutor::ExecutingEvent
#define SuspendedExecutingEvent   GraphExecutor::SuspendedExecutingEvent
#define ResumedExecutingEvent     GraphExecutor::ResumedExecutingEvent
#define KilledExecutingEvent      GraphExecutor::KilledExecutingEvent
#define StoppedExecutingEvent     GraphExecutor::StoppedExecutingEvent
#define SuccessedExecutingEvent GraphExecutor::SuccessedExecutingEvent
#define ErroredExecutingEvent   GraphExecutor::ErroredExecutingEvent
#define SuspendedSuccessedEvent   GraphExecutor::SuspendedSuccessedEvent
#define SuspendedErroredEvent     GraphExecutor::SuspendedErroredEvent
#define ResumedSuccessedEvent     GraphExecutor::ResumedSuccessedEvent
#define ResumedErroredEvent       GraphExecutor::ResumedErroredEvent
#define KilledEvent               GraphExecutor::KilledEvent
#define StoppedEvent              GraphExecutor::StoppedEvent
#define ToReStartEvent              GraphExecutor::ToReStartEvent
#define ReStartedEvent              GraphExecutor::ReStartedEvent
#define ReStartedAndSuspendEvent    GraphExecutor::ReStartedAndSuspendEvent
#define EndExecuteEvent           GraphExecutor::EndExecuteEvent
#define NumberOfEvents            GraphExecutor::NumberOfEvents

GraphExecutor::FiniteStateMachine::FiniteStateMachine() {
  MESSAGE( "FiniteStateMachine::FiniteStateMachine" ) ;

  _ControlStateName[ VoidState ]          = "VoidState" ;
  _ControlStateName[ ToSuspendStartState ]= "ToSuspendStartState" ;
  _ControlStateName[ ToSuspendState ]     = "ToSuspendState" ;
  _ControlStateName[ ToSuspendDoneState ] = "ToSuspendDoneState" ;
  _ControlStateName[ ToKillState ]        = "ToKillState" ;
  _ControlStateName[ ToKillDoneState ]    = "ToKillDoneState" ;
  _ControlStateName[ ToStopState ]        = "ToStopState" ;

  _StateName[ UnKnownState ]          = "UnKnownState" ;
  _StateName[ DataUndefState ]            = "DataUndefState" ;
  _StateName[ DataWaitingState ]          = "DataWaitingState" ;
  _StateName[ DataReadyState ]            = "DataReadyState" ;
  _StateName[ SuspendedReadyState ]     = "SuspendedReadyState" ;
  _StateName[ SuspendedReadyToResumeState ] = "SuspendedReadyToResumeState" ;
  _StateName[ ResumedReadyState ]       = "ResumedReadyState" ;
  _StateName[ KilledReadyState ]        = "KilledReadyState" ;
  _StateName[ StoppedReadyState ]       = "StoppedReadyState" ;
  _StateName[ ExecutingState ]            = "ExecutingState" ;
  _StateName[ SuspendedExecutingState ]   = "SuspendedExecutingState" ;
  _StateName[ ResumedExecutingState ]     = "ResumedExecutingState" ;
  _StateName[ KilledExecutingState ]      = "KilledExecutingState" ;
  _StateName[ StoppedExecutingState ]     = "StoppedExecutingState" ;
  _StateName[ SuccessedExecutingState ]   = "SuccessedExecutingState" ;
  _StateName[ ErroredExecutingState ]     = "ErroredExecutingState" ;
  _StateName[ SuspendedSuccessedState ] = "SuspendedSuccessedState" ;
  _StateName[ SuspendedErroredState ]   = "SuspendedErroredState" ;
  _StateName[ SuspendedSuccessedToReStartState ] = "SuspendedSuccessedToReStartState" ;
  _StateName[ SuspendedErroredToReStartState ]   = "SuspendedErroredToReStartState" ;
  _StateName[ ResumedSuccessedState ]   = "ResumedSuccessedState" ;
  _StateName[ ResumedErroredState ]     = "ResumedErroredState" ;
  _StateName[ KilledSuccessedState ]    = "KilledSuccessedState" ;
  _StateName[ KilledErroredState ]      = "KilledErroredState" ;
  _StateName[ StoppedSuccessedState ]   = "StoppedSuccessedState" ;
  _StateName[ StoppedErroredState ]     = "StoppedErroredState" ;
  _StateName[ SuccessedState ]          = "SuccessedState" ;
  _StateName[ ErroredState ]            = "ErroredState" ;
  _StateName[ SuspendedState ]          = "SuspendedState" ;
  _StateName[ KilledState ]             = "KilledState" ;
  _StateName[ StoppedState ]            = "StoppedState" ;
  _StateName[ ReStartedState ]            = "ReStartedState" ;
  _StateName[ LoadingState ]              = "LoadingState" ;

  _EventName[ UndefinedEvent ]          = "UndefinedEvent" ;
  _EventName[ NewThreadEvent ]          = "NewThreadEvent" ;
  _EventName[ SuspendEvent ]              = "SuspendEvent" ;
  _EventName[ ToResumeEvent ]             = "ToResumeEvent" ;
  _EventName[ ResumeEvent ]               = "ResumeEvent" ;
  _EventName[ KillEvent ]                 = "KillEvent" ;
  _EventName[ StopEvent ]                 = "StopEvent" ;
  _EventName[ ExecuteEvent ]              = "ExecuteEvent" ;
  _EventName[ SuccessEvent ]              = "SuccessEvent" ;
  _EventName[ ErrorEvent ]                = "ErrorEvent" ;
  _EventName[ ToReStartEvent ]            = "ToReStartEvent" ;
  _EventName[ ReStartEvent ]              = "ReStartEvent" ;
  _EventName[ ReStartAndSuspendEvent ]    = "ReStartAndSuspendEvent" ;
  _EventName[ NoDataReadyEvent ]        = "NoDataReadyEvent" ;
  _EventName[ SomeDataReadyEvent ]      = "SomeDataReadyEvent" ;
  _EventName[ NotAllDataReadyEvent ]    = "NotAllDataReadyEvent" ;
  _EventName[ AllDataReadyEvent ]       = "AllDataReadyEvent" ;
  _EventName[ ReadyEvent ]                = "ReadyEvent" ;
  _EventName[ SuspendedReadyEvent ]       = "SuspendedReadyEvent" ;
  _EventName[ ResumedReadyEvent ]         = "ResumedReadyEvent" ;
  _EventName[ KilledReadyEvent ]          = "KilledReadyEvent" ;
  _EventName[ StoppedReadyEvent ]         = "StoppedReadyEvent" ;
  _EventName[ ExecutingEvent ]          = "ExecutingEvent" ;
  _EventName[ SuspendedExecutingEvent ] = "SuspendedExecutingEvent" ;
  _EventName[ ResumedExecutingEvent ]   = "ResumedExecutingEvent" ;
  _EventName[ KilledExecutingEvent ]    = "KilledExecutingEvent" ;
  _EventName[ StoppedExecutingEvent ]   = "StoppedExecutingEvent" ;
  _EventName[ SuccessedExecutingEvent ]   = "SuccessedExecutingEvent" ;
  _EventName[ ErroredExecutingEvent ]     = "ErroredExecutingEvent" ;
  _EventName[ SuspendedSuccessedEvent ]   = "SuspendedSuccessedEvent" ;
  _EventName[ SuspendedErroredEvent ]     = "SuspendedErroredEvent" ;
  _EventName[ ResumedSuccessedEvent ]     = "ResumedSuccessedEvent" ;
  _EventName[ ResumedErroredEvent ]       = "ResumedErroredEvent" ;
  _EventName[ KilledEvent ]               = "KilledEvent" ;
  _EventName[ StoppedEvent ]              = "StoppedEvent" ;
  _EventName[ ReStartedEvent ]          = "ReStartedEvent" ;
  _EventName[ ReStartedAndSuspendEvent ]= "ReStartedAndSuspendEvent" ;
  _EventName[ EndExecuteEvent ]         = "EndExecuteEvent" ;

  _ActionName[ ErrorAction ] = "ErrorAction" ;
  _ActionName[ VoidAction ] = "VoidAction" ;
  _ActionName[ executeAction ] = "executeAction" ;
  _ActionName[ ExecuteAction ] = "ExecuteAction" ;
  _ActionName[ DataWaiting_SomeDataReadyAction ] = "DataWaiting_SomeDataReadyAction" ;
  _ActionName[ DataUndef_NotAllDataReadyAction ] = "DataUndef_NotAllDataReadyAction";
  _ActionName[ DataUndef_AllDataReadyAction ] = "DataUndef_AllDataReadyAction" ;

  _ActionName[ DataReady_SuspendAction ] = "DataReady_SuspendAction" ;
  _ActionName[ SuspendedReady_ResumeAction ] = "SuspendedReady_ResumeAction" ;
  _ActionName[ DataReady_KillAction ] = "DataReady_KillAction" ;
  _ActionName[ DataReady_StopAction ] = "DataReady_StopAction" ;

  _ActionName[ DataReady_ExecuteAction ] = "DataReady_ExecuteAction" ;

  _ActionName[ Executing_SuspendAction ] = "Executing_SuspendAction" ;
  _ActionName[ SuspendedExecuting_ResumeAction ] = "SuspendedExecuting_ResumeAction" ;
  _ActionName[ Executing_KillAction ] = "Executing_KillAction" ;
  _ActionName[ Executing_StopAction ] = "Executing_StopAction" ;

  _ActionName[ Executing_SuccessAction ] = "Executing_SuccessAction" ;
//  _ActionName[ Executing_ErrorAction ] = "Executing_ErrorAction" ;
  _ActionName[ Errored_ExecutingAction ] = "Errored_ExecutingAction" ;
  _ActionName[ Successed_SuccessAction ] = "Successed_SuccessAction" ;
  _ActionName[ Errored_ErrorAction ] = "Errored_ErrorAction" ;

  _ActionName[ Successed_SuspendAction ] = "Successed_SuspendAction" ;
  _ActionName[ Errored_SuspendAction ] = "Errored_SuspendAction" ;
  _ActionName[ SuspendedSuccessed_ResumeAction ] = "SuspendedSuccessed_ResumeAction" ;
  _ActionName[ SuspendedErrored_ResumeAction ] = "SuspendedErrored_ResumeAction" ;
  _ActionName[ Successed_KillAction ] = "Successed_KillAction" ;
  _ActionName[ Errored_KillAction ] = "Errored_KillAction" ;
  _ActionName[ Successed_StopAction ] = "Successed_StopAction" ;
  _ActionName[ Errored_StopAction ] = "Errored_StopAction" ;

  _ActionName[ SuspendedSuccessed_ReStartAction ] = "SuspendedSuccessed_ReStartAction" ;
  _ActionName[ SuspendedErrored_ReStartAction ] = "SuspendedErrored_ReStartAction" ;
  _ActionName[ SuspendedSuccessed_ReStartAndSuspendAction ] = "SuspendedSuccessed_ReStartAndSuspendAction" ;
  _ActionName[ SuspendedErrored_ReStartAndSuspendAction ] = "SuspendedErrored_ReStartAndSuspendAction" ;

  _GraphStateName[ SUPERV::UndefinedState ] = "UndefinedState" ;
  _GraphStateName[ SUPERV::NoState ] = "NoState" ;
  _GraphStateName[ SUPERV::EditingState ] = "EditingState" ;
  _GraphStateName[ SUPERV::SuspendState ] = "SuspendState" ;
  _GraphStateName[ SUPERV::WaitingState ] = "WaitingState" ;
  _GraphStateName[ SUPERV::ReadyState ] = "ReadyState" ;
  _GraphStateName[ SUPERV::SuspendReadyState ] = "SuspendReadyState" ;
  _GraphStateName[ SUPERV::RunningState ] = "RunningState" ;
  _GraphStateName[ SUPERV::DoneState ] = "DoneState" ;
  _GraphStateName[ SUPERV::ErrorState ] = "ErrorState" ;
  _GraphStateName[ SUPERV::SuspendDoneState ] = "SuspendDoneState" ;
  _GraphStateName[ SUPERV::SuspendErroredState ] = "SuspendErroredState" ;
  _GraphStateName[ SUPERV::KillState ] = "KillState" ;
  _GraphStateName[ SUPERV::StopState ] = "StopState" ;

  int i , j ;
// NewStates = _TransitionTable[ States ] [ Events ]
// associated action = _ActionTable[ NewStates ] [ Events ]
  for ( i = 0 ; i < NumberOfAutomatonStates ; i++ ) {
    for ( j = 0 ; j < NumberOfEvents ; j++ ) {
// id est = SameState and VoidAction
      _TransitionTable[ i ][ j ] = (GraphExecutor::AutomatonState ) i ;
      _ActionTable[ i ][ j ] = GraphExecutor::ErrorAction ;
    }
  }
// OneEvent ===> Change of State and associated Action :
//               _TransitionTable[ OldState ][ OneEvent ] gives a NewState
//               _Action[ NewState ][ OneEvent ] gives what to do

// INPUT-DATAS :
// NoDataReadyEvent :
  _TransitionTable[ DataUndefState ][ NoDataReadyEvent ] = DataWaitingState ;
  _ActionTable[ DataWaitingState ][ NoDataReadyEvent ] = VoidAction ;
// SomeDataReadyEvent :
  _TransitionTable[ DataWaitingState ][ SomeDataReadyEvent ] = DataUndefState ;
  _ActionTable[ DataUndefState ][ SomeDataReadyEvent ] = DataWaiting_SomeDataReadyAction;
  _TransitionTable[ DataReadyState ][ SomeDataReadyEvent ] = DataUndefState ;
  _ActionTable[ DataUndefState ][ SomeDataReadyEvent ] = DataWaiting_SomeDataReadyAction;
// NotAllDataReadyEvent :
  _TransitionTable[ DataUndefState ][ NotAllDataReadyEvent ] = DataWaitingState ;
  _ActionTable[ DataWaitingState ][ NotAllDataReadyEvent ] = DataUndef_NotAllDataReadyAction;
// AllDataReadyEvent
  _TransitionTable[ DataUndefState ][ AllDataReadyEvent ] = DataReadyState ;
  _ActionTable[ DataReadyState ][ AllDataReadyEvent ] = DataUndef_AllDataReadyAction ;

// DATAS-READY-CONTROL :
// SuspendEvent
  _TransitionTable[ DataReadyState ][ SuspendEvent ] = SuspendedReadyState ;
  _ActionTable[ SuspendedReadyState ][ SuspendEvent ] = DataReady_SuspendAction;
// ResumeEvent
  _TransitionTable[ SuspendedReadyState ][ ToResumeEvent ] = SuspendedReadyToResumeState ;
  _ActionTable[ SuspendedReadyToResumeState ][ ToResumeEvent ] = VoidAction ;

  _TransitionTable[ SuspendedReadyToResumeState ][ ResumeEvent ] = ResumedReadyState ;
  _ActionTable[ ResumedReadyState ][ ResumeEvent] = SuspendedReady_ResumeAction ;
// KillEvent
  _TransitionTable[ SuspendedReadyState ][ KillEvent ] = KilledReadyState ;
// StopEvent
  _TransitionTable[ SuspendedReadyState ][ StopEvent ] = StoppedReadyState ;
// KillEvent
  _TransitionTable[ DataReadyState ][ KillEvent ] = KilledReadyState ;
  _ActionTable[ KilledReadyState ][ KillEvent] = DataReady_KillAction ;
// StopEvent
  _TransitionTable[ DataReadyState ][ StopEvent ] = StoppedReadyState ;
  _ActionTable[ StoppedReadyState ][ StopEvent] = DataReady_StopAction ;

// RUN :
// ExecuteEvent
  _TransitionTable[ SuspendedReadyToResumeState ][ ExecuteEvent ] = ExecutingState ;
// ExecuteEvent
  _TransitionTable[ ResumedReadyState ][ ExecuteEvent ] = ExecutingState ;
// ExecuteEvent
  _TransitionTable[ DataReadyState ][ ExecuteEvent ] = ExecutingState ;
// ExecuteEvent
  _TransitionTable[ ReStartedState ][ ExecuteEvent ] = ExecutingState ;
  _ActionTable[ ExecutingState ][ ExecuteEvent ] = DataReady_ExecuteAction;

// RUN-CONTROL :
// SuspendEvent
  _TransitionTable[ ExecutingState ][ SuspendEvent ] = SuspendedExecutingState ;
  _ActionTable[ SuspendedExecutingState ][ SuspendEvent ] = Executing_SuspendAction;
// ResumeEvent
  _TransitionTable[ SuspendedExecutingState ][ ResumeEvent ] = ResumedExecutingState ;
  _ActionTable[ ResumedExecutingState ][ ResumeEvent] = SuspendedExecuting_ResumeAction;
// ExecuteEvent
  _TransitionTable[ ResumedExecutingState ][ ExecuteEvent ] = ExecutingState ;
// KillEvent
  _TransitionTable[ SuspendedExecutingState ][ KillEvent ] = KilledExecutingState ;
// StopEvent
  _TransitionTable[ SuspendedExecutingState ][ StopEvent ] = StoppedExecutingState ;
// KillEvent
  _TransitionTable[ ExecutingState ][ KillEvent ] = KilledExecutingState ;
  _ActionTable[ KilledExecutingState ][ KillEvent] = Executing_KillAction;
  _TransitionTable[ KilledExecutingState ][ KillEvent ] = KilledState ;
  _ActionTable[ KilledState ][ KillEvent] = VoidAction;
// StopEvent
  _TransitionTable[ ExecutingState ][ StopEvent ] = StoppedExecutingState ;
  _ActionTable[ StoppedExecutingState ][ StopEvent] = Executing_StopAction;
  _TransitionTable[ StoppedExecutingState ][ StopEvent ] = StoppedState ;
  _ActionTable[ StoppedState ][ StopEvent] = VoidAction;

// DONE :
// SuccessEvent
  _TransitionTable[ ExecutingState ][ SuccessEvent ] = SuccessedExecutingState ;
  _ActionTable[ SuccessedExecutingState ][ SuccessEvent ] = Executing_SuccessAction;
// SuspendedExecutingState : NO ResumeEvent 13-03-2003
  _TransitionTable[ SuspendedExecutingState ][ SuccessEvent ] = SuccessedExecutingState ;
  _ActionTable[ SuccessedExecutingState ][ SuccessEvent ] = Executing_SuccessAction;
//JR 24.03.2005 : Debug for PAL8176 : abort of GOTONode
// ErrorEvent
  _TransitionTable[ SuccessedState ][ ErrorEvent ] = ErroredExecutingState ;
//  _ActionTable[ ErroredExecutingState ][ ErrorEvent ] = Executing_ErrorAction;
  _ActionTable[ ErroredExecutingState ][ ErrorEvent ] = Errored_ExecutingAction;
// ErrorEvent
  _TransitionTable[ ExecutingState ][ ErrorEvent ] = ErroredExecutingState ;
//  _ActionTable[ ErroredExecutingState ][ ErrorEvent ] = Executing_ErrorAction;
  _ActionTable[ ErroredExecutingState ][ ErrorEvent ] = Errored_ExecutingAction;
// SuspendedExecutingState : NO ResumeEvent 13-03-2003
  _TransitionTable[ SuspendedExecutingState ][ ErrorEvent ] = ErroredExecutingState ;
//  _ActionTable[ ErroredExecutingState ][ ErrorEvent ] = Executing_ErrorAction;
  _ActionTable[ ErroredExecutingState ][ ErrorEvent ] = Errored_ExecutingAction;

// DONE-CONTROL :
// SuccessedExecutingState - SuccessEvent
  _TransitionTable[ SuccessedExecutingState ][ SuccessEvent ] = SuccessedState ;
  _ActionTable[ SuccessedState ][ SuccessEvent ] = Successed_SuccessAction ;
// ErroredExecutingState - ErrorEvent
  _TransitionTable[ ErroredExecutingState ][ ErrorEvent ] = ErroredState ;
  _ActionTable[ ErroredState ][ ErrorEvent ] = Errored_ErrorAction ;

// SuccessedState - SuspendEvent
  _TransitionTable[ SuccessedExecutingState ][ SuspendEvent ] = SuspendedSuccessedState ;
  _ActionTable[ SuspendedSuccessedState ][ SuspendEvent ] = Successed_SuspendAction;
// ErroredState - SuspendEvent
  _TransitionTable[ ErroredExecutingState ][ SuspendEvent ] = SuspendedErroredState ;
  _ActionTable[ SuspendedErroredState ][ SuspendEvent ] = Errored_SuspendAction;

// SuccessedState - KillEvent
  _TransitionTable[ SuccessedExecutingState ][ KillEvent ] = KilledSuccessedState ;
  _ActionTable[ KilledSuccessedState ][ KillEvent ] = Successed_KillAction;
// ErroredState - KillEvent
  _TransitionTable[ ErroredExecutingState ][ KillEvent ] = KilledErroredState ;
  _ActionTable[ KilledErroredState ][ KillEvent ] = Errored_KillAction;

// SuccessedState - StopEvent
  _TransitionTable[ SuccessedExecutingState ][ StopEvent ] = StoppedSuccessedState ;
  _ActionTable[ StoppedSuccessedState ][ StopEvent ] = Successed_StopAction;
// ErroredState - StopEvent
  _TransitionTable[ ErroredExecutingState ][ StopEvent ] = StoppedErroredState ;
  _ActionTable[ StoppedErroredState ][ StopEvent ] = Errored_StopAction;

// SuspendedSuccessedState - ResumeEvent
  _TransitionTable[ SuspendedSuccessedState ][ ResumeEvent ] = ResumedSuccessedState ;
  _ActionTable[ ResumedSuccessedState ][ ResumeEvent ] = SuspendedSuccessed_ResumeAction;

  _TransitionTable[ ResumedSuccessedState ][ ResumedSuccessedEvent ] = SuccessedState ;
  _ActionTable[ SuccessedState ][ ResumedSuccessedEvent] = Successed_SuccessAction;

// SuspendedErroredState - ResumeEvent
  _TransitionTable[ SuspendedErroredState ][ ResumeEvent ] = ResumedErroredState ;
  _ActionTable[ ResumedErroredState ][ ResumeEvent ] = SuspendedErrored_ResumeAction;
  _TransitionTable[ ResumedErroredState ][ ResumedErroredEvent ] = ErroredState ;
  _ActionTable[ ErroredState ][ ResumedErroredEvent ] = Errored_ErrorAction;

// SuccessedState - KillEvent
  _TransitionTable[ SuccessedState ][ KillEvent ] = KilledSuccessedState ;
  _ActionTable[ KilledSuccessedState ][ KillEvent] = Successed_KillAction;
  _TransitionTable[ KilledSuccessedState ][ KillEvent ] = KilledState ;
  _ActionTable[ KilledState ][ KillEvent ] = VoidAction;
// ErroredState - KillEvent
  _TransitionTable[ ErroredState ][ KillEvent ] = KilledErroredState ;
  _ActionTable[ KilledErroredState ][ KillEvent] = Errored_KillAction;
  _TransitionTable[ KilledErroredState ][ KillEvent ] = KilledState ;
  _ActionTable[ KilledState ][ KillEvent ] = VoidAction;

// SuccessedState - StopEvent
  _TransitionTable[ SuccessedState ][ StopEvent ] = StoppedSuccessedState ;
  _ActionTable[ StoppedSuccessedState ][ StopEvent] = Successed_StopAction;
  _TransitionTable[ StoppedSuccessedState ][ StopEvent ] = StoppedState ;
  _ActionTable[ StoppedState ][ StopEvent ] = VoidAction;
// ErroredState - StopEvent
  _TransitionTable[ ErroredState ][ StopEvent ] = StoppedErroredState ;
  _ActionTable[ StoppedErroredState ][ StopEvent] = Errored_StopAction;
  _TransitionTable[ StoppedErroredState ][ StopEvent ] = StoppedState ;
  _ActionTable[ StoppedState ][ StopEvent ] = VoidAction;

// ReStartEvent
  _TransitionTable[ SuspendedSuccessedState ][ ToReStartEvent ] = SuspendedSuccessedToReStartState ;
  _ActionTable[ SuspendedSuccessedToReStartState ][ ToReStartEvent] = VoidAction ;

  _TransitionTable[ SuspendedSuccessedToReStartState ][ ReStartEvent ] = ReStartedState ;
  _TransitionTable[ SuspendedSuccessedToReStartState ][ ReStartAndSuspendEvent ] = ReStartedState ;
// ReStartEvent
  _TransitionTable[ SuspendedErroredState ][ ToReStartEvent ] = SuspendedErroredToReStartState ;
  _ActionTable[ SuspendedErroredToReStartState ][ ToReStartEvent] = VoidAction ;

  _TransitionTable[ SuspendedErroredToReStartState ][ ReStartEvent ] = ReStartedState ;
  _TransitionTable[ SuspendedErroredToReStartState ][ ReStartAndSuspendEvent ] = ReStartedState ;

  _ActionTable[ ReStartedState ][ ReStartEvent] = SuspendedSuccessed_ReStartAction;
  _ActionTable[ ReStartedState ][ ReStartAndSuspendEvent] = SuspendedSuccessed_ReStartAndSuspendAction;

  pthread_mutex_init( &_MutexJoinWait , NULL ) ;
  if ( pthread_cond_init( &_JoinWait , NULL ) ) {
    perror("pthread_cond_init( &_JoinWait , NULL )") ;
    exit( 0 ) ;
  }
  _JoinThread = true ;
  if ( pthread_create( &_JoinThreadNo , NULL , start_function , this ) ) {
    char * msg = "Cannot pthread_create " ;
    perror( msg ) ;
  }

#if omniORB_4_0_5
  _SuperVisionComponent = SUPERV::SuperG::_nil() ;
#endif

  pthread_mutex_init( &_MutexPythonWait , NULL ) ;
  _MutexPythonLocked = false ;
  _ExecNumber = 0 ;
  _GraphExecutingNumber = 0 ;
  _PyInitModule = false ;
  _DbgFileNumber = 0 ;
  MESSAGE( "FiniteStateMachine::FiniteStateMachine _TransitionTable " ) ;
}

void * start_function( void *p ) {
  GraphExecutor::FiniteStateMachine *anAutomaton = (GraphExecutor::FiniteStateMachine *) p;
  if ( pthread_setcanceltype( PTHREAD_CANCEL_ASYNCHRONOUS , NULL ) ) {
    perror("pthread_setcanceltype ") ;
    exit(0) ;
  }
  if ( pthread_setcancelstate( PTHREAD_CANCEL_ENABLE , NULL ) ) {
    perror("pthread_setcancelstate ") ;
    exit(0) ;
  }
  anAutomaton->JoinThread() ;
  pthread_exit( PTHREAD_CANCELED ) ;
}

int GraphExecutor::FiniteStateMachine::ExecNumber() {
  PyLock() ;
  _ExecNumber += 1 ;
  int RetVal = _ExecNumber ;
  _GraphExecutingNumber += 1 ;
  PyUnLock() ;
  return RetVal ;
}

void GraphExecutor::FiniteStateMachine::Executed() {
  PyLock() ;
  _GraphExecutingNumber -= 1 ;
  if ( _GraphExecutingNumber == 0 ) {
    map< string , PyObject * >::iterator aPyFunction ;
    for ( aPyFunction = _MapOfPyFunctions.begin() ; aPyFunction != _MapOfPyFunctions.end() ; aPyFunction++ ) {
      if ( !strcmp( aPyFunction->first.c_str() , "PyObjRef" ) ||
           !strcmp( aPyFunction->first.c_str() , "PyObjIor" ) ) {
	MESSAGE( "GraphExecutor::FiniteStateMachine::Executed " << aPyFunction->first << " keeped ..."
                 ) ;
      }
      else {
	MESSAGE( "GraphExecutor::FiniteStateMachine::Executed " << aPyFunction->first << " erased ..."
               ) ;
	_MapOfPyFunctions.erase( aPyFunction->first ) ;
      }
    }
  }
  else {
    MESSAGE( "GraphExecutor::FiniteStateMachine::Executed _GraphExecutingNumber "
              << _GraphExecutingNumber << " != 0 ==> no erase" ) ;
  }
  PyUnLock() ;
  return ;
}

void GraphExecutor::FiniteStateMachine::PyInitModule( bool aPyInitModule ) {
  _PyInitModule = aPyInitModule ;
}

bool GraphExecutor::FiniteStateMachine::PyInitModule() {
  bool InitedModule = _PyInitModule ;
  _PyInitModule = true ;
  return InitedModule ;
}

void GraphExecutor::FiniteStateMachine::PyLock() {
//  cout << pthread_self() << " GraphExecutor::FiniteStateMachine::PyLock " << &_MutexPythonWait << endl ;
  if ( pthread_mutex_lock( &_MutexPythonWait ) ) {
    perror( "GraphExecutor::FiniteStateMachine::PyLock" ) ;
    exit( 0 ) ;
  }
  _MutexPythonLocked = true ;
//  cout << pthread_self() << " GraphExecutor::FiniteStateMachine::PyLocked " << &_MutexPythonWait << endl ;
}

void GraphExecutor::FiniteStateMachine::PyUnLock() {
//  cout << pthread_self() << " GraphExecutor::FiniteStateMachine::PyUnLock " << &_MutexPythonWait << endl ;
  if ( pthread_mutex_unlock( &_MutexPythonWait ) ) {
    perror( "GraphExecutor::FiniteStateMachine::PyUnLock" ) ;
    exit( 0 ) ;
  }
  _MutexPythonLocked = false ;
//  cout << pthread_self() << " GraphExecutor::FiniteStateMachine::PyUnLocked " << &_MutexPythonWait << endl ;
}

void GraphExecutor::FiniteStateMachine::PyLockReset() {
  if ( _MutexPythonLocked ) {
    PyUnLock() ;
  }
}

#define PyFunctionTrace 1
PyObject * GraphExecutor::FiniteStateMachine::PyFunction( const char * aPyFuncName ) {
  
  PyObject * RetVal = NULL ;
  bool PyObjRefIor = !strcmp( aPyFuncName , "PyObjRef" ) || !strcmp( aPyFuncName , "PyObjIor" ) ;
  PyObject * PyFunctionMapped = _MapOfPyFunctions[ aPyFuncName ] ;
  if ( _GraphExecutingNumber > 1 && !PyObjRefIor ) {
    RetVal = PyFunctionMapped ;
#if PyFunctionTrace
    MESSAGE( "GraphExecutor::FiniteStateMachine::PyFunction( '" << aPyFuncName << "' ) --> " ) ;
    if ( RetVal ) {
      MESSAGE( RetVal << " ob_refcnt " << RetVal->ob_refcnt ) ;
    }
    else {
      MESSAGE( " NULL" ) ;
    }
#endif
  }
  else {
    RetVal = PyFunctionMapped ;
#if PyFunctionTrace
    MESSAGE( "GraphExecutor::FiniteStateMachine::PyFunction( '" << aPyFuncName << "' ) --> " ) ;
    if ( RetVal && PyObjRefIor ) {
      MESSAGE( RetVal << " " << RetVal->ob_refcnt ) ;
    }
    else if ( RetVal ) {
      MESSAGE( RetVal << " " << RetVal->ob_refcnt ) ;
    }
    else {
      MESSAGE( " NULL" ) ;
    }
#endif
  }
  
  return RetVal ;
}

bool GraphExecutor::FiniteStateMachine::PyFunction( const char * aPyFuncName , PyObject * aPyFunction ) {
  
  bool RetVal = false ;
  if ( _MapOfPyFunctions[ aPyFuncName ] != NULL ) {
#if PyFunctionTrace
    PyObject * aPyFunc = _MapOfPyFunctions[ aPyFuncName ] ;
    MESSAGE( "GraphExecutor::FiniteStateMachine::PyFunction( '" << aPyFuncName << "' , aPyFunction "
             << aPyFunction << " ) ob_refcnt " << aPyFunction->ob_refcnt << " already mapped : "
             << aPyFunc << " ob_refcnt " << aPyFunc->ob_refcnt ) ;
#endif
  }
  else {
    _MapOfPyFunctions[ aPyFuncName ] = aPyFunction ;
#if PyFunctionTrace
    MESSAGE( "GraphExecutor::FiniteStateMachine::PyFunction( '" << aPyFuncName << "' ) " << aPyFunction
             << " ) ob_refcnt " << aPyFunction->ob_refcnt << " mapped" ) ;
#endif
    RetVal = true ;
  }
  
  return RetVal ;
}

bool GraphExecutor::FiniteStateMachine::ErasePyFunction( const char * aPyFuncName ) {
  
#if PyFunctionTrace
  MESSAGE( "GraphExecutor::FiniteStateMachine::ErasePyFunction( '" << aPyFuncName << "' )" ) ;
#endif
  if ( _MapOfPyFunctions[ aPyFuncName ] != NULL ) {
    _MapOfPyFunctions.erase( aPyFuncName ) ;
    return true ;
  }  
  return false ;
}

/*
GraphBase::Graph * GraphExecutor::FiniteStateMachine::MapGraph( const char * aGraphName ) {
  GraphBase::Graph * aGraph = _MapOfGraphs[ aGraphName ] ;
  //cout << "MapGraph " << aGraphName << " --> " << aGraph << endl ;
  return aGraph ;
}

bool GraphExecutor::FiniteStateMachine::MapGraph( GraphBase::Graph * aGraph , const char * aGraphName ) {
  if ( MapGraph( aGraphName ) ) {
    return false ;
  }
  _MapOfGraphs[ aGraphName ] = aGraph ;
  return true ;
}

void GraphExecutor::FiniteStateMachine::EraseGraph( const char * aGraphName ) {
  _MapOfGraphs.erase( aGraphName ) ;
}

bool GraphExecutor::FiniteStateMachine::GraphName( const char * aGraphName ) {
  return  _MapOfGraphNames[ aGraphName ] ;
}

string GraphExecutor::FiniteStateMachine::GraphInstanceName( const char * aGraphName ) {
  int GraphInstanceNumber = _MapOfGraphNames[ aGraphName ] ;
  if ( GraphInstanceNumber ) {
    _MapOfGraphNames[ aGraphName ] = GraphInstanceNumber + 1 ;
  }
  else {
    GraphInstanceNumber = 0 ;
    _MapOfGraphNames[ aGraphName ] = GraphInstanceNumber + 1 ;
  }
  string theGraphInstanceName = string( aGraphName ) ;
  if ( GraphInstanceNumber ) {
    theGraphInstanceName += "_" ;
    ostringstream astr ;
//    astr << GraphInstanceNumber << ends ;
    astr << GraphInstanceNumber ;
    theGraphInstanceName += astr.str() ;
  }
  //cout << "GraphExecutor::FiniteStateMachine::GraphInstanceName( " << aGraphName << " ) --> "
    //   << theGraphInstanceName << endl ;
  return theGraphInstanceName ;
}
*/

void GraphExecutor::FiniteStateMachine::JoinThread() {
  if ( pthread_mutex_lock( &_MutexJoinWait ) ) {
    perror("FiniteStateMachine:JoinThread pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  while ( _JoinThread ) {
    while ( _ThreadList.size() == 0) {
//      MESSAGE( pthread_self() << " FiniteStateMachine:Join pthread_cond_wait" );
      if ( pthread_cond_wait( &_JoinWait , &_MutexJoinWait ) ) {
        perror("FiniteStateMachine:JoinThread pthread_cond_wait ") ;
      }
    }
    pthread_t _Thread = _ThreadList.front() ;
//    MESSAGE( pthread_self() << " FiniteStateMachine:JoinThread pthread_cond_waited : " << _Thread )
//    MESSAGE( pthread_self() << " FiniteStateMachine:JoinThread pthread_join : " << _Thread );
    int joinsts = pthread_join( _Thread , NULL ) ;
    if ( joinsts ) {
      perror("FiniteStateMachine:JoinThread pthread_join ") ;
      MESSAGE( pthread_self() << " FiniteStateMachine:JoinThread pthread_join : " << _Thread
               << " Error" );
      exit( 0 ) ;
    }
    else {
//      MESSAGE( pthread_self() << " FiniteStateMachine:JoinThread pthread_joined : " << _Thread );
      _ThreadList.pop_front() ;
    }
  }
  if ( pthread_mutex_unlock( &_MutexJoinWait ) ) {
    perror("FiniteStateMachine:JoinThread pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::FiniteStateMachine::JoinThread( pthread_t aThread ) {
  if ( pthread_mutex_lock( &_MutexJoinWait ) ) {
    perror("Join pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
//  MESSAGE(pthread_self() << " JoinThread pthread_cond_signal for " << aThread)
  _ThreadList.push_back( aThread ) ;
  if ( pthread_cond_signal( &_JoinWait ) ) {
    perror("Join pthread_cond_signal ") ;
  }
//  MESSAGE( pthread_self() << " JoinThread pthread_cond_signaled for " << aThread)

  if ( pthread_mutex_unlock( &_MutexJoinWait ) ) {
    perror("Join pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

int GraphExecutor::FiniteStateMachine::ThreadsNumber() {
  if( _ThreadList.begin() == _ThreadList.end() )
    return 0;
  else
    return _ThreadList.size();
}

ostream & operator<< (ostream &f ,const GraphExecutor::AutomatonState & aState ) {
  switch (aState) {
  case GraphExecutor::UnKnownState :
    f << "UnKnownState";
    break;
  case GraphExecutor::DataUndefState :
    f << "DataUndefState";
    break;
  case GraphExecutor::DataWaitingState :
    f << "DataWaitingState";
    break;
  case GraphExecutor::DataReadyState :
    f << "DataReadyState";
    break;
  case GraphExecutor::SuspendedReadyState :
    f << "SuspendedReadyState";
    break;
  case GraphExecutor::SuspendedReadyToResumeState :
    f << "SuspendedReadyToResumeState";
    break;
  case GraphExecutor::ResumedReadyState :
    f << "ResumedReadyState";
    break;
  case GraphExecutor::KilledReadyState :
    f << "KilledReadyState";
    break;
  case GraphExecutor::StoppedReadyState :
    f << "StoppedReadyState";
    break;
  case GraphExecutor::ExecutingState :
    f << "ExecutingState";
    break;
  case GraphExecutor::SuspendedExecutingState :
    f << "SuspendedExecutingState";
    break;
  case GraphExecutor::ResumedExecutingState :
    f << "ResumedExecutingState";
    break;
  case GraphExecutor::KilledExecutingState :
    f << "KilledExecutingState";
    break;
  case GraphExecutor::StoppedExecutingState :
    f << "StoppedExecutingState";
    break;
  case GraphExecutor::SuccessedExecutingState :
    f << "SuccessedExecutingState";
    break;
  case GraphExecutor::ErroredExecutingState :
    f << "ErroredExecutingState";
    break;
  case GraphExecutor::SuspendedSuccessedState :
    f << "SuspendedSuccessedState";
    break;
  case GraphExecutor::SuspendedErroredState :
    f << "SuspendedErroredState";
    break;
  case GraphExecutor::ResumedSuccessedState :
    f << "ResumedSuccessedState";
    break;
  case GraphExecutor::ResumedErroredState :
    f << "ResumedErroredState";
    break;
  case GraphExecutor::KilledSuccessedState :
    f << "KilledSuccessedState";
    break;
  case GraphExecutor::KilledErroredState :
    f << "KilledErroredState";
    break;
  case GraphExecutor::StoppedSuccessedState :
    f << "StoppedSuccessedState";
    break;
  case GraphExecutor::StoppedErroredState :
    f << "StoppedErroredState";
    break;
  case GraphExecutor::SuccessedState :
    f << "SuccessedState";
    break;
  case GraphExecutor::ErroredState :
    f << "ErroredState";
    break;
  case GraphExecutor::SuspendedState :
    f << "SuspendedState";
    break;
  case GraphExecutor::KilledState :
    f << "KilledState";
    break;
  case GraphExecutor::StoppedState :
    f << "StoppedState";
    break;
  case GraphExecutor::SuspendedSuccessedToReStartState :
    f << "SuspendedSuccessedToReStartState";
    break;
  case GraphExecutor::SuspendedErroredToReStartState :
    f << "SuspendedErroredToReStartState";
    break;
  case GraphExecutor::ReStartedState :
    f << "ReStartedState";
    break;
  case GraphExecutor::LoadingState :
    f << "LoadingState";
    break;
  default :
    f << "GraphExecutor::AutomatonState_?";
    break;
  }

  return f;
}


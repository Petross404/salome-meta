//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_InNodeThreads.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>

#if defined __GNUC__
  #if __GNUC__ == 2
    #define __GNUC_2__
  #endif
#endif

#if defined __GNUC_2__
// _CS_gbo_040604 include explicite pour l'utilisation de
// std::transform dans UpperCase
#include <cctype> // for toupper
#include <algorithm> // for transform
#endif

#include "Python.h"

#include "OpUtil.hxx"

#include <SALOMEconfig.h>
#include CORBA_CLIENT_HEADER(SALOME_Component)
#include "SALOME_LifeCycleCORBA.hxx"

#include "DataFlowExecutor_DataFlow.hxx"
#include "DataFlowEditor_DataFlow.hxx"   // GraphEditor package must be built BEFORE


static void UpperCase(std::string& rstr)
{
#if defined __GNUC_2__
  // _CS_gbo_040604 towupper n'existe pas. Utilisation de toupper. Par
  // ailleurs, include explicite de cctype et algorithm pour toupper
  // et transform respectivement. 
  std::transform(rstr.begin(), rstr.end(), rstr.begin(),toupper);
#else
  std::transform(rstr.begin(), rstr.end(), rstr.begin(),towupper);
#endif
}

#define SendEventTrace 1
int GraphExecutor::InNode::SendEvent( const GraphExecutor::NodeEvent anEvent ) {  

  _CurrentEvent = (GraphExecutor::NodeEvent ) anEvent ;
#if SendEventTrace
  cdebug_in << pthread_self() << "/" << ThreadNo() << " SendEvent Graph " << _OutNode->Name()
            << " Node " << Name() << " Event : " << Automaton()->EventName( anEvent )
	    << " State : " << Automaton()->StateName( State() ) << " _RewindStack " << _RewindStack
            << " ControlState : " << Automaton()->ControlStateName( ControlState() )
            << endl;
#endif

  _OldState = State() ;
  _NextState = Automaton()->NextState( _OldState , anEvent ) ;
  if ( _NextState == _OldState ) {
    string anErrorMessage = string( "Automaton error for node " ) +
                            string( Name() ) + " (SuperVision executor error)." ;
    _OutNode->Graph()->SetMessages( anErrorMessage ) ;
    cdebug << pthread_self() << "/" << ThreadNo() << " " << Name()
           << " GraphExecutor::InNodeThreads::SendEvent SameStates ERROR _OldState/_NextState "
           << _OldState << " Event " << Automaton()->EventName( anEvent ) << endl ;
    _NextAction = GraphExecutor::VoidAction ;
    return 0 ;
  }
  else {
    _NextAction = Automaton()->NextAction( _NextState , anEvent ) ;
  }
#if SendEventTrace
  cdebug << pthread_self() << "/" << ThreadNo() << "NextState( " << _OldState << " , "
         << Automaton()->EventName( anEvent ) << " ) --> _NextState = " << _NextState
         << " NextAction( " << _NextState << " , " << Automaton()->EventName( anEvent )
         << " ) --> _NextAction = "
         << Automaton()->ActionName( _NextAction ) << endl ;
#endif

//  State( _NextState ) ;
//  if ( _OldState == GraphExecutor::SuccessedExecutingState ||
//       _OldState == GraphExecutor::ErroredExecutingState ) {
//    DoneAction() ;
//  }

#if SendEventTrace
  cdebug << pthread_self() << "/" << ThreadNo() << " SendedEvent Node "
         << Name() << endl << " ControlState : "
         << Automaton()->ControlStateName( ControlState() ) << endl
         << " OldState : " << Automaton()->StateName( _OldState ) << endl
         << " Event : " << Automaton()->EventName( anEvent ) << endl
         << " NextState : " << Automaton()->StateName( _NextState ) << endl
         << " Action : " << Automaton()->ActionName( _NextAction ) << endl
         << " CreateNewThread " << CreateNewThread() << endl
         << " _RewindStack " << _RewindStack  << endl ;
#endif

  int sts = true ;
#if ExitWhenNodeAborted
  if ( _OutNode->IsNodeAborted() ) {
#if SendEventTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " SendedEvent Node " << Name()
           << " will exit : a node was aborted ..." << endl ;
#endif
    State( _NextState ) ;
    sts = false;
  }
  else {
    sts = executeAction() ;
  }
#else
  sts = executeAction() ;
#endif
  
#if SendEventTrace
  cdebug_out << pthread_self() << "/" << ThreadNo() << " <--- SendEvent Node " << Name() 
	     << " Event : " << Automaton()->EventName( anEvent )
	     << " State : " << Automaton()->StateName( State() )
             << endl;
#endif

  return sts ;

}

#define ActionsTrace 1
// ReadyAction - RunningAction - DoneAction - SuspendedAction :
// for StateWait( ReadyW - RunningW - DoneW - SuspendedW )
void GraphExecutor::InNode::ReadyAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Ready pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << "ReadyAction pthread_cond_broadcast _ReadyWait "
         << Name() << endl ;
#endif
  if ( pthread_cond_broadcast( &_ReadyWait ) ) {
    perror("Ready pthread_cond_broadcast ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Ready pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::RunningAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Running pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << "RunningAction pthread_cond_broadcast _RunningWait "
         << Name() << endl ;
#endif
// That activate the pthread_cond_wait for RunninWait
  if ( pthread_cond_broadcast( &_RunningWait ) ) {
    perror("Running pthread_cond_broadcast ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Running pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::DoneAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Done pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << "DoneAction pthread_cond_broadcast _DoneWait "
         << Name() << endl ;
#endif
  if ( pthread_cond_broadcast( &_DoneWait ) ) {
    perror("Done pthread_cond_broadcast ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Done pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::SuspendedAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Suspended pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << "SuspendedAction pthread_cond_broadcast _SuspendedWait "
         << Name() << endl ;
#endif
  if ( pthread_cond_broadcast( &_SuspendedWait ) ) {
    perror("Suspended pthread_cond_broadcast ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Suspended pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

// SuspendAction <--> { ResumeAction - ReStartAction }
GraphExecutor::InNode * GraphExecutor::InNode::SuspendAction() {
  SuspendedAction() ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Suspend pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( !_SuspendSync ) {
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " SuspendAction pthread_cond_wait _SuspendWait "
           << Automaton()->StateName( State() ) << endl ;
    _SuspendSync = true ;
    _OutNode->SuspendThread() ;
    if ( pthread_cond_wait( &_SuspendWait , &_MutexWait ) ) {
      perror("SuspendAction pthread_cond_wait ") ;
    }
    _OutNode->ResumeThread() ;
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " SuspendAction pthread_cond_waited"  
           << Automaton()->StateName( State() ) << endl ;
#endif
  }
  else {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " NO SuspendAction pthread_cond_wait"  
           << Automaton()->StateName( State() ) << endl ;
#endif
  }
//  SendEvent( _aResumeEvent ) ; ===> Mutex with myself !
  _SuspendSync = false ;  
  if ( ControlState() == SUPERV::ToSuspendStartState ||
       ControlState() == SUPERV::ToSuspendState ) {
    ControlState( SUPERV::VoidState ) ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("SuspendAction pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }

  SendEvent( _aResumeEvent ) ;
//  if ( ControlState() == SUPERV::ToSuspendStartState ) {
//    ControlState( SUPERV::VoidState ) ;
//  }

  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("SuspendAction pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( _ResumeSync ) {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " SuspendAction pthread_cond_signal _ResumeWait" << endl ;
#endif
    if ( pthread_cond_signal( &_ResumeWait ) ) {
      perror("SuspendAction pthread_cond_signal _ResumeWait ") ;
    }
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " SuspendAction pthread_cond_signaled _ResumeWait " << endl ;
#endif
  }
  else {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " NO SuspendAction pthread_cond_signal _ResumeWait" << endl ;
#endif
    _ResumeSync = true ;  
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("SuspendAction pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
  if ( _aReStartNode ) {
    cdebug << Name() << " " << Automaton()->StateName( State() )
           << "aReStartNode : " << _aReStartNode->Name() << " "
           << Automaton()->StateName( _aReStartNode->State() ) << endl ;
    _aReStartNode->SendEvent( _aResumeEvent ) ;
  }
  else {
    cdebug << "NO aReStartNode" 
           << Automaton()->StateName( State() ) << endl ;
  }
  return _aReStartNode ;
}

bool GraphExecutor::InNode::ResumeAction( GraphExecutor::NodeEvent aResumeEvent ) {
  bool RetVal ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("ResumeAction pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  _aResumeEvent = aResumeEvent ;
  if ( _SuspendSync ) {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " ResumeAction pthread_cond_signal" << endl ;
#endif
    if ( pthread_cond_signal( &_SuspendWait ) ) {
      perror("ResumeAction pthread_cond_signal ") ;
    }
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " ResumeAction pthread_cond_signaled _SuspendWait " << endl ;
#endif
    RetVal = true ;
  }
  else {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond " << Name()
           << " NO ResumeAction pthread_cond_signal" << endl ;
#endif
    if ( pthread_self() == ThreadNo() ) {
      RetVal = false ; /*/ Ne pas s'attendre soi-meme !...*/
    }
    else {
      _SuspendSync = true ;
      RetVal = true ; // Il faut tout de meme attendre ci-apres ...
    }
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("ResumeAction pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }

  if ( RetVal ) {
    if ( pthread_mutex_lock( &_MutexWait ) ) {
      perror("ResumeAction pthread_mutex_lock ") ;
      exit( 0 ) ;
    }
    if ( !_ResumeSync ) {
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond "
             << Name() << " ResumeAction pthread_cond_wait _ResumeWait " 
             << Automaton()->StateName( State() ) << endl ;
#endif
      _ResumeSync = true ;
      if ( pthread_cond_wait( &_ResumeWait , &_MutexWait ) ) {
        perror("ResumeAction pthread_cond_wait ") ;
      }
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond "
             << Name() << " ResumeAction pthread_cond_waited _ResumeWait"  
             << Automaton()->StateName( State() ) << endl ;
#endif
      RetVal = true ;
    }
    else {
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " pthread_cond "
             << Name() << " NO ResumeAction pthread_cond_wait _ResumeWait" 
             << Automaton()->StateName( State() ) << endl ;
#endif
      RetVal = false ;
    }
    _ResumeSync = false ;  
    if ( pthread_mutex_unlock( &_MutexWait ) ) {
      perror("ResumeAction pthread_mutex_unlock ") ;
      exit( 0 ) ;
    }
  }
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << "GraphExecutor::InNodeThreads::ResumeAction RetVal " << RetVal << endl ;
#endif
  return RetVal ;
}

bool GraphExecutor::InNode::ReStartAction( GraphExecutor::InNode * aReStartNode ,
                                           GraphExecutor::NodeEvent anEvent ) {
  GraphExecutor::InNode * oldReStartNode = _aReStartNode ;
  _aReStartNode = aReStartNode ;
  _aReStartEvent = anEvent ;
  cdebug << pthread_self() << "/" << ThreadNo()
          << " GraphExecutor::InNodeThreads::ReStartAction from "
         << Name() << " " << Automaton()->StateName( State() ) << " to "
         << aReStartNode->ThreadNo() << " " << aReStartNode->Name() << " "
         << Automaton()->StateName( aReStartNode->State() ) ;
  if ( oldReStartNode ) {
    cdebug << " oldReStartNode " << oldReStartNode->Name() << endl ;
  }
  else {
    cdebug << endl ;
  }
  return ResumeAction( GraphExecutor::ToReStartEvent ) ;
}

void GraphExecutor::InNode::KilledAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Killed pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( !_KillSync ) {
    cdebug << "pthread_cond " << Name() << " Killed pthread_cond_wait"
           << endl ;
    _KillSync = true ;
    if ( pthread_cond_wait( &_KillWait , &_MutexWait ) ) {
      perror("Killed pthread_cond_wait ") ;
    }
    cdebug << "pthread_cond " << Name() << " Killed pthread_cond_waited"
           << endl ;
  }
  else {
    cdebug << "pthread_cond " << Name() << " NO Killed pthread_cond_wait"
           << endl ;
  }
  _KillSync = false ;  
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Killed pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::KillAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Kill pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( _KillSync ) {
    cdebug << "pthread_cond " << Name() << " Kill pthread_cond_signal"
           << endl ;
//    if ( pthread_cond_broadcast( &_KillWait ) ) {
    if ( pthread_cond_signal( &_KillWait ) ) {
      perror("Kill pthread_cond_broadcast ") ;
    }
    cdebug << "pthread_cond " << Name() << " Kill pthread_cond_signaled"
           << endl ;
  }
  else {
    cdebug << "pthread_cond " << Name() << " NO Kill pthread_cond_signal"
           << endl ;
    _KillSync = true ;  
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Kill pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::StoppedAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Stopped pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_wait( &_StopWait , &_MutexWait ) ) {
    perror("Stopped pthread_cond_wait ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Stopped pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::StopAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("Stop pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( pthread_cond_broadcast( &_StopWait ) ) {
    perror("Stop pthread_cond_broadcast ") ;
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("Stop pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::ThreadStartedAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("ThreadStarted pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( !_ThreadStartedSync ) {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " ThreadStarted pthread_cond_wait"
           << endl ;
#endif
    _ThreadStartedSync = true ;
    if ( pthread_cond_wait( &_ThreadStartedWait , &_MutexWait ) ) {
      perror("ThreadStarted pthread_cond_wait ") ;
    }
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " ThreadStarted pthread_cond_waited"
           << endl ;
#endif
  }
  else {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " NO ThreadStarted pthread_cond_wait"
           << endl ;
#endif
//Debug :
    _ThreadStartedSync = false ;  
    if ( pthread_cond_signal( &_ThreadStartedWait ) ) {
      perror("ThreadStart pthread_cond_signal ") ;
    }
//Debug
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " NO ThreadStarted pthread_cond_signaled"
           << endl ;
#endif
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("ThreadStarted pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

void GraphExecutor::InNode::ThreadStartAction() {
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror("ThreadStart pthread_mutex_lock ") ;
    exit( 0 ) ;
  }
  if ( _ThreadStartedSync ) {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " ThreadStart pthread_cond_signal"
           << endl ;
#endif
    _ThreadStartedSync = false ;  
    if ( pthread_cond_signal( &_ThreadStartedWait ) ) {
      perror("ThreadStart pthread_cond_broadcast ") ;
    }
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " ThreadStart pthread_cond_signaled"
           << endl ;
#endif
  }
  else {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " NO ThreadStart pthread_cond_signal"
           << endl ;
#endif
    _ThreadStartedSync = true ;
//Debug :
    if ( pthread_cond_wait( &_ThreadStartedWait , &_MutexWait ) ) {
      perror("ThreadStarted pthread_cond_wait ") ;
    }
//Debug
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << "pthread_cond " << Name() << " NO ThreadStart pthread_cond_waited"
           << endl ;
#endif
  }
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror("ThreadStart pthread_mutex_unlock ") ;
    exit( 0 ) ;
  }
}

int GraphExecutor::InNode::executeAction() {
  
  int oldRewindStack = ( _RewindStack > MAXSTACKTHREADSIZE ) ;
  if ( !CreateNewThread() && oldRewindStack ) {
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo()
           << " executeAction start Thread _RewindStack " << _RewindStack << " > "
           << MAXSTACKTHREADSIZE << " CreateNewThread "
           << CreateNewThread() << " " << Automaton()->ActionName( _NextAction ) << "("
           << Name() << ")" << endl;
#endif
    CreateNewThread( true ) ;
    _OutNode->IncrCreatedThreads() ;
    ThreadNo( 0 ) ;
  }
  if ( CreateNewThread() ) {
    CreateNewThread( false ) ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
//    if ( ThreadNo() == 0 ) {
      _RewindStack = 1 ;
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo()
             << " executeAction start Thread _RewindStack " << _RewindStack << " "
             << Automaton()->ActionName( _NextAction ) << "(" << Name() << ")"
             << endl;
#endif
      pthread_t T;
      int pthread_sts = 1 ;
//      _OutNode->PushEvent( NULL , GraphExecutor::NewThreadEvent ,
//                           GraphExecutor::ExecutingState ) ; 
      while ( (pthread_sts = pthread_create(&T, NULL, run_function, this )) ) {
        char * msg = "Cannot pthread_create " ;
        perror( msg ) ;
        cdebug << ThreadNo() << " " << msg << " --> sleep(5)" << endl ;
	//        cdebug << ThreadNo() << " PTHREAD_THREADS_MAX : "
        //       << PTHREAD_THREADS_MAX << " pthread_create status : " ;
        if ( pthread_sts == EAGAIN ) {
          cdebug << "EAGAIN(" << pthread_sts << ")" << endl ;
          cdebug << _OutNode->CreatedThreads() << " was created (and exited)" << endl ;
          cdebug << "It seems to me that with gdb we are limited to 256 threads" << endl ;
	}
        else {
          cdebug << pthread_sts << endl ;
	}
        string smsg = msg ;
        delete [] msg ;
        pthread_exit( msg ) ;
      }
      _OutNode->setRunFuncThread( T ) ; // mkr : IPAL14711
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " " << Name()
             << " executeAction has created thread " << T << endl ;
#endif
      ThreadStartedAction() ;
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " " << Name()
             << "executeAction the thread " << T << " has called NewThread and will call ExecuteAction for node "
             << Name() << endl ;
#endif
    }
//JR 15.04.2005 Debug PAL8624 RetroConception :
#if 0
    else {
#if ActionsTrace
      cdebug << pthread_self() << "/" << ThreadNo()
             << " executeAction restart Thread _RewindStack " << _RewindStack << " "
             << Automaton()->StateName( State() ) << " "
             << Automaton()->ActionName( _NextAction ) << "(" << Name()
             << ") ReStartAction ==>" << endl;
#endif
      State( GraphExecutor::SuspendedSuccessedState ) ;
      if ( !ReStartAction( this , GraphExecutor::ReStartEvent ) ) {
        cdebug << pthread_self() << "/" << ThreadNo()
               << " executeAction STATE & CALLED "
               << Automaton()->ActionName( _NextAction ) << "(" << Name()
               << ") ERROR-DEBUG " << endl;
      }
      else {
#if ActionsTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " executeAction NO CALL "
               << Automaton()->ActionName( _NextAction ) << "(" << Name()
               << ")" << endl;
#endif
      }
    }
  }
#endif
  else {
    if ( _CurrentEvent == ExecuteEvent ) {
      _RewindStack += 1 ;
    }
#if ActionsTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " executeAction call "
           << Automaton()->ActionName( _NextAction ) << "(" << Name() << ") _RewindStack " << _RewindStack
           << endl;
#endif
    return ExecuteAction() ;
  }
  return 1 ;
}

void GraphExecutor::InNode::coutbegin() {
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo() << " run_function begin"
         << " " << Name() << " " << Automaton()->StateName( State() ) << endl ;
#endif
}
void GraphExecutor::InNode::coutexit() {
#if ActionsTrace
  cdebug << pthread_self() << "/" << ThreadNo() << " run_function pthread_exit _RewindStack " << _RewindStack
         << " " << Name() << " " << Automaton()->StateName( State() ) << endl ;
#endif
}
void * run_function(void *p) {
  GraphExecutor::InNode *aNode = (GraphExecutor::InNode *) p;
  aNode->coutbegin() ;
  aNode->NewThread( pthread_self() ) ;
  if ( pthread_setcanceltype( PTHREAD_CANCEL_ASYNCHRONOUS , NULL ) ) {
    perror("pthread_setcanceltype ") ;
    exit(0) ;
  }
  if ( pthread_setcancelstate( PTHREAD_CANCEL_ENABLE , NULL ) ) {
    perror("pthread_setcancelstate ") ;
    exit(0) ;
  }
  aNode->ThreadStartAction() ;
//  cout << "run_function " << aNode->Name() << "->ExecuteAction() Coupled : " << aNode->CoupledNode()
//       << endl ;
  aNode->ExecuteAction() ;
  char * msg = new char[40] ;
  sprintf( msg , "%d" , (int ) aNode->ThreadNo() ) ;
  strcat( msg , " thread exit" ) ;
  aNode->coutexit() ;
  aNode->ExitThread() ;
  string smsg = msg ;
  delete [] msg ;
  pthread_exit( (void * ) smsg.c_str() ) ;
  return msg ;
}

int GraphExecutor::InNode::ExecuteAction() {
  int sts ;

#if ActionsTrace
  const char * nextactionname = Automaton()->ActionName( _NextAction ) ;
  const char * statename = Automaton()->StateName( State() ) ;
  const char * nextstatename = Automaton()->StateName( _NextState ) ;
  cdebug_in << pthread_self() << "/" << ThreadNo() << " " << Name() << " --> ExecuteAction "
            << nextactionname << " "  << statename << " NextState "
            << nextstatename << endl ;
#endif

  State( _NextState ) ;
  switch ( _NextAction ) {
  case GraphExecutor::ErrorAction : {
    sts = ErrorAction() ;
    break ;
  }
  case GraphExecutor::VoidAction : {
    sts = VoidAction() ;
    break ;
  }
  case GraphExecutor::DataWaiting_SomeDataReadyAction : {
    sts = DataWaiting_SomeDataReadyAction() ;
    break ;
  }
  case GraphExecutor::DataUndef_NotAllDataReadyAction : {
    sts = DataUndef_NotAllDataReadyAction() ;
    break ;
  }
  case GraphExecutor::DataUndef_AllDataReadyAction : {
    sts = DataUndef_AllDataReadyAction() ;
    break ;
  }
  case GraphExecutor::DataReady_SuspendAction : {
    sts = DataReady_SuspendAction() ;
    break ;
  }
  case GraphExecutor::SuspendedReady_ResumeAction : {
    sts = SuspendedReady_ResumeAction() ;
    break ;
  }
  case GraphExecutor::DataReady_KillAction : {
    sts = DataReady_KillAction() ;
    break ;
  }
  case GraphExecutor::DataReady_StopAction : {
    sts = DataReady_StopAction() ;
    break ;
  }
  case GraphExecutor::DataReady_ExecuteAction : {
    sts = DataReady_ExecuteAction() ;
    break ;
  }
  case GraphExecutor::Executing_SuspendAction : {
    sts = Executing_SuspendAction() ;
    break ;
  }
  case GraphExecutor::SuspendedExecuting_ResumeAction : {
    sts = SuspendedExecuting_ResumeAction() ;
    break ;
  }
  case GraphExecutor::Executing_KillAction : {
    sts = Executing_KillAction() ;
    break ;
  }
  case GraphExecutor::Executing_StopAction : {
    sts = Executing_StopAction() ;
    break ;
  }
  case GraphExecutor::Executing_SuccessAction : {
    sts = Executing_SuccessAction() ;
    break ;
  }
  case GraphExecutor::Errored_ExecutingAction : {
    sts = Errored_ExecutingAction() ;
    break ;
  }
  case GraphExecutor::Successed_SuccessAction : {
    sts = Successed_SuccessAction() ;
    break ;
  }
  case GraphExecutor::Errored_ErrorAction : {
    sts = Errored_ErrorAction() ;
    break ;
  }
  case GraphExecutor::Successed_SuspendAction : {
    sts = Successed_SuspendAction() ;
    break ;
  }
  case GraphExecutor::Errored_SuspendAction : {
    sts = Errored_SuspendAction() ;
    break ;
  }
  case GraphExecutor::SuspendedSuccessed_ResumeAction : {
    sts = SuspendedSuccessed_ResumeAction() ;
    break ;
  }
  case GraphExecutor::SuspendedErrored_ResumeAction : {
    sts = SuspendedErrored_ResumeAction() ;
    break ;
  }
  case GraphExecutor::Successed_KillAction : {
    sts = Successed_KillAction() ;
    break ;
  }
  case GraphExecutor::Errored_KillAction : {
    sts = Errored_KillAction() ;
    break ;
  }
  case GraphExecutor::Successed_StopAction : {
    sts = Successed_StopAction() ;
    break ;
  }
  case GraphExecutor::Errored_StopAction : {
    sts = Errored_StopAction() ;
    break ;
  }
  case GraphExecutor::SuspendedSuccessed_ReStartAction : {
    sts = SuspendedSuccessed_ReStartAction() ;
    break ;
  }
  case GraphExecutor::SuspendedErrored_ReStartAction : {
    sts = SuspendedErrored_ReStartAction() ;
    break ;
  }
  case GraphExecutor::SuspendedSuccessed_ReStartAndSuspendAction : {
    sts = SuspendedSuccessed_ReStartAndSuspendAction() ;
    break ;
  }
  case GraphExecutor::SuspendedErrored_ReStartAndSuspendAction : {
    sts = SuspendedErrored_ReStartAndSuspendAction() ;
    break ;
  }
  default : {
    cdebug << pthread_self() << "/" << ThreadNo()
           << " GraphExecutor::InNodeThreads::SendEvent Error Undefined Action : "
           << _NextAction << endl ;
    return 0 ;
  }
  }
#if ActionsTrace
  cdebug_out << pthread_self() << "/" << ThreadNo() << "<-- ExecuteAction "
             << nextactionname << endl ;
#endif
  return sts ;
}

int GraphExecutor::InNode::ErrorAction() {
  cdebug << pthread_self() << "/" << ThreadNo() << " Automaton ErrorAction Node "
         << Name() << endl;
  return 0;
}

int GraphExecutor::InNode::VoidAction() {
  cdebug << pthread_self() << "/" << ThreadNo() << " VoidAction "  << Name() << endl;
  return 1;
}

#define SomeDataReadyActionTrace 1
int GraphExecutor::InNode::DataWaiting_SomeDataReadyAction() {
#if SomeDataReadyActionTrace
  cdebug_in << pthread_self() << "/" << ThreadNo() << " " << Name()
            << " DataWaiting_SomeDataReadyAction from " << DataFromNode()
            << " " << GetNodeInPortsSize() << " Inport(s)" << endl;
#endif
  unsigned int k;
  int InReady = 0 ;
  int res = 1;
  bool LoopBeginning = false ;
  bool LoopFinished = false ;
  bool SwitchFinished = false ;
  bool SwitchDefault = false ;
  bool DoAllDataReadyIf = true ;

  if ( IsLoopNode() ) {
    GraphBase::OutPort * anOutLoopPort = GetChangeNodeInLoop()->GetOutPort() ; // DoLoop Port
    if ( anOutLoopPort && anOutLoopPort->BoolValue() ) {
      LoopBeginning = true ; // Beginning of Loop
    }
  }
  else if ( IsEndLoopNode() ) {
    GraphBase::OutPort * anOutLoopPort = GetChangeNodeInLoop()->GetOutPort() ; // DoLoop Port
    if ( anOutLoopPort && !anOutLoopPort->BoolValue() ) {
      LoopFinished = true ; // End of Loop
    }
  }
  else if ( IsEndSwitchNode() ) {
    if ( strcmp( GOTONode()->CoupledNode()->Name() , DataFromNode() ) ) {
      GraphBase::OutPort * anOutGateSwitchPort = GetChangeNodeInGate()->GetOutPort() ; // Default Port
//JR 09.02.2005 : SomeDataReady is NOT from the SwitchNode
      if ( anOutGateSwitchPort && !anOutGateSwitchPort->BoolValue() ) {
//JR 09.02.2005 : the OutPort of the SwitchNode connected to the default port is closed ===>
// Here after we consider that that DefaultPort is Ready (even if it's value is false) in
// order to have the good count of InPorts Ready in the EndSwitchNode
//        SwitchFinished = true ; // End of Switch
//JR 25.08.2005 :
// But we do that only if the InDefaultPort of the EndSwitchNode is not connected or
// is connected from the OutDefaultPort of the corresponding SwitchNode
        if ( !strcmp( GOTONode()->CoupledNode()->Name() , anOutGateSwitchPort->NodeName() ) ) {
          SwitchFinished = true ; // End of Switch
        }
      }
    }
    else {
//JR 20.04.2005 : SomeDataReady is FROM the SwitchNode to that EndSwitchNode
//PAL8518
//JR 16.02.2005 Debug : Change InPorts of EndSwitchNode that have the same name as an OutPort of
// the SwitchNode even if it is the DefaultPort : GraphSwitchCheckDefault1.xml
      GraphBase::OutPort * anOutGateSwitchPort = GetChangeNodeInGate()->GetOutPort() ; // Default P
//JR 20.04.2005 : SomeDataReady is from the SwitchNode and Default is activated :
      if ( anOutGateSwitchPort ) {
        if ( anOutGateSwitchPort->BoolValue() ) {
          SwitchDefault = true ;
	}
//JR 20.04.2005 : SomeDataReady is from the SwitchNode and Default is NOT activated :
//                a SwitchBranch should be activated
        else {
          DoAllDataReadyIf = false ;
	}
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name()
               << " activated from CoupledNode " << GOTONode()->CoupledNode()->Name() << " "
               << anOutGateSwitchPort->NodeName() << "( " << anOutGateSwitchPort->PortName()
               << " ) to InDefault " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutGateSwitchPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
//JR 28.06.2005 : SomeDataReady is from the SwitchNode and the InDefault is not connected :
//                a SwitchBranch should be activated
      else {
        DoAllDataReadyIf = false ;
      }
    }
  }

#if SomeDataReadyActionTrace
  cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " LoopFinished " << LoopFinished
         << " LoopBeginning " << LoopBeginning << " SwitchFinished " << SwitchFinished
         << " SwitchDefault " << SwitchDefault << " DoAllDataReadyIf " << DoAllDataReadyIf << endl ;
#endif
  for ( k = 0 ; k < (unsigned int ) GetNodeInPortsSize() ; k++ ) {
    GraphBase::InPort * anInPort = GetChangeNodeInPort(k) ;
    GraphBase::OutPort * anOutPort ;
    if ( SwitchDefault && !anInPort->IsDataStream() ) {
//Get or Set the field OutPort of that InPort of the EndSwitchNode to the corresponding OutPort
// of the SwitchNode
      anOutPort = anInPort->GetOutPort() ;
      if ( anOutPort ) {
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " InPort"
               << k << " "
               << anInPort->PortName() << " already setted to OutPort " << anOutPort->NodeName()
               << "( " << anOutPort->PortName() << " )" << endl ;
#endif
      }
      else {
        anOutPort = CoupledNode()->GetChangeOutPort( anInPort->PortName() ) ;
        if ( anOutPort ) {
#if SomeDataReadyActionTrace
          cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " InPort"
                 << k << " "
                 << anInPort->PortName() << " change of OutPort from "
                 << anInPort->GetOutPort()->NodeName() << "( " << anInPort->GetOutPort()->PortName()
                 << " ) to " << anOutPort->NodeName() << "( " << anOutPort->PortName() << " )"
                 << endl ;
#endif
	}
        else {
          cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " InPort"
                 << k << " "
                 << anInPort->PortName() << " have NO OutPort ERROR " << endl ;
          return 0 ;
	}
        anInPort->ChangeOutPort( anOutPort ) ;
      }
    }
    else {
      anOutPort = anInPort->GetOutPort() ;
    }
#if SomeDataReadyActionTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " InPort"
           << k << " "
           << anInPort->PortName() << " " << anInPort->PortState() << " "
           << anInPort->PortStatus() << " " << anInPort->Kind()  ;
    if ( anOutPort ) {
      cdebug << " from OutPort " << anOutPort->NodeName() << "( " << anOutPort->PortName()
             << " )" ;
    }
    else {
      cdebug << " without OutPort " ;
    }
    cdebug<< endl ;
#endif
    if ( anInPort->IsGate() && anOutPort == NULL ) {
      InReady += 1 ;
      anInPort->PortState( SUPERV::ReadyState ) ;
#if SomeDataReadyActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
             << anInPort->PortName() << " ControlPort inactive." << endl ;
#endif
    }

// That InPort get its value from an other node : the node of anOutPort linked to that anInPort is
// different from the sender of SomeDataReady (DataFromNode) :
    else if ( strcmp( DataFromNode() , anOutPort->NodeName() ) ) {
      if ( anInPort->PortState() == SUPERV::ReadyState ) {
        InReady += 1 ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " " << anInPort->PortState() << " Was Done from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName() << " , "
               << anOutPort->PortState() << " ) PortDone " << anOutPort->PortDone() << " " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
//JR 30.01.2007 : wrong debug for PAL12455
#if 0
        // mkr : PAL12455 -->
        // MacroNode : give immediately the value to the corresponding graph
        if ( IsMacroNode() ) {
          GraphExecutor::DataFlow * aMacroGraph = GraphMacroNode()->CoupledNode()->GraphEditor()->Executor() ;
          cdebug << "SomeDataReadyAction MacroNode " << aMacroGraph->Name() << " --> InputOfAny "
                 << InReady << "/" << GetNodeInPortsSize() << " InPorts are Ready ( "
                 << anInPort->PortName() << " ) ===> InputOfAny" << endl ;
//        GraphMacroNode()->MacroObject()->InputOfAny( anInPort->PortName() , *anOutPort->Value() ) ;
//JR 30.03.2005        aMacroGraph->InputOfAny( anInPort->PortName() , *anOutPort->Value() ) ;
          aMacroGraph->InputOfAny( anInPort->PortName() , anOutPort->Value() ) ;
        }
        // mkr : PAL12455 <--
#endif
      }
      else if ( IsLoopNode() && anInPort->IsDataConnected() ) {
        anInPort->PortState( SUPERV::ReadyState ) ;
        InReady += 1 ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " Was Done from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") LoopBeginning " << LoopBeginning << " " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
      else if ( LoopFinished ) {
        anInPort->PortState( SUPERV::ReadyState ) ;
        InReady += 1 ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " Was Done from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") LoopFinished " << LoopFinished << " " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
      else if ( anInPort->IsGate() && SwitchFinished ) {
        anInPort->PortState( SUPERV::ReadyState ) ;
        InReady += 1 ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " Was Done from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") SwitchFinished " << SwitchFinished << " " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
      else if ( anInPort->IsGate() &&
                _OutNode->Graph()->GetGraphNode( anOutPort->NodeName() )->IsGOTONode() ) {
// GateOutPort of GOTONodes are always opened
        anInPort->PortState( SUPERV::ReadyState ) ;
        InReady += 1 ;
//JR 21.02.2005 Debug Memory leak :        CORBA::Any * anAny = new CORBA::Any() ;
        CORBA::Any anAny = CORBA::Any() ;
//JR 21.02.2005 Debug Memory leak :        *anAny <<= (CORBA::Long ) 1 ;
        anAny <<= (CORBA::Long ) 1 ;
        _OutNode->Graph()->GetGraphNode( anOutPort->NodeName() )->GetChangeNodeOutGate()->SetValue( anAny ) ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " Was Done from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") GOTONode " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
      else {
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << " " << Automaton()->StateName( State() ) << " LoopBeginning "
               << LoopBeginning << " " << anInPort->PortName() << " DataConnected "
               << anInPort->IsDataConnected() << " Was NOT Done from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName() << " , "
               << anOutPort->PortState() << " , PortDone " << anOutPort->PortDone() << ") "
               << endl ;
#endif
      }
    }

// That InPort get its value from the sending node (DataFromNode)
    else if ( anInPort->IsGate() ) {
//JR 30.03.2005      const CORBA::Any * theValue = anOutPort->Value() ;
      const CORBA::Any theValue = anOutPort->Value() ;
      CORBA::Long GateOpened ;
//JR 30.03.2005      (*theValue) >>= GateOpened ;
      theValue >>= GateOpened ;
      if ( GateOpened != 0 ) {
        InReady += 1 ;
        anInPort->PortState( SUPERV::ReadyState ) ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " Gate is Opened from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
      else if ( LoopFinished ) {
        anInPort->PortState( SUPERV::ReadyState ) ;
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " GATE IS CLOSED from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") LoopFinished" ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
      else {
#if SomeDataReadyActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " "
               << anInPort->PortName() << " GATE IS CLOSED from Node "
               << anOutPort->NodeName() << "( " << anOutPort->PortName()
               << ") " ;
#ifdef _DEBUG_
        if ( GraphBase::Base::_prof_debug ) {
          anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
	}
#endif
        cdebug << endl ;
#endif
      }
    }
    else if ( anOutPort->PortDone() ) {
#if SomeDataReadyActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " " << Name() << " InPort "
             << anInPort->PortName() << " " << anInPort->PortStatus() << " "
             << Automaton()->StateName( anInPort->PortState() ) << " is Done from Node "
             << anOutPort->NodeName() << "( " << anOutPort->PortName() << ") "
             << anOutPort->PortStatus() << " --> ReadyState " ;
#ifdef _DEBUG_
      if ( GraphBase::Base::_prof_debug ) {
        anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
      }
#endif
      cdebug << endl ;
#endif
      InReady += 1 ;
      anInPort->PortState( SUPERV::ReadyState ) ;
// MacroNode : give immediately the value to the corresponding graph
      if ( IsMacroNode() ) {
        GraphExecutor::DataFlow * aMacroGraph = GraphMacroNode()->CoupledNode()->GraphEditor()->Executor() ;
        cdebug << "SomeDataReadyAction MacroNode " << aMacroGraph->Name() << " --> InputOfAny "
               << InReady << "/" << GetNodeInPortsSize() << " InPorts are Ready ( "
               << anInPort->PortName() << " ) ===> InputOfAny" << endl ;
//        GraphMacroNode()->MacroObject()->InputOfAny( anInPort->PortName() , *anOutPort->Value() ) ;
//JR 30.03.2005        aMacroGraph->InputOfAny( anInPort->PortName() , *anOutPort->Value() ) ;
        aMacroGraph->InputOfAny( anInPort->PortName() , anOutPort->Value() ) ;
      }
    }
    else {
#if SomeDataReadyActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " Node " << Name() << "( "
             << anInPort->PortName() << ") " << anInPort->PortStatus()
             << " is NOT Done from Node "
             << anOutPort->NodeName() << "( " << anOutPort->PortName() << ") "
             << anOutPort->PortStatus() << " " << anOutPort->PortDone() << endl ;
#endif
    }
  }

  if ( InReady == GetNodeInPortsSize() && DoAllDataReadyIf ) { // All Flags != 0 :
//JR 15.04.2005 Debug PAL8624 RetroConception :
//    res = SendEvent( GraphExecutor::AllDataReadyEvent ); // ==> Ready to execute
#if SomeDataReadyActionTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " Node " << Name() << " HasAllDataReady"
           << endl ;
#endif
//JR 30.01.2007 Right Debug PAL12455 --->
//That must be done only for DataConnected ports and only once
    if ( IsMacroNode() ) {
      GraphExecutor::DataFlow * aMacroGraph = GraphMacroNode()->CoupledNode()->GraphEditor()->Executor() ;
      for ( k = 0 ; k < (unsigned int ) GetNodeInPortsSize() ; k++ ) {
        GraphBase::InPort * anInPort = GetChangeNodeInPort(k) ;
        GraphBase::OutPort * anOutPort ;
        anOutPort = anInPort->GetOutPort() ;
        if ( anInPort->IsDataConnected() ) {
          cdebug << "SomeDataReadyAction MacroNode " << aMacroGraph->Name()
                 << " --> InputOfAny " << InReady << "/" << GetNodeInPortsSize()
                 << " InPorts are Ready ( " << anInPort->PortName()
                 << " ) ===> InputOfAny" << endl ;
          aMacroGraph->InputOfAny( anInPort->PortName() , anOutPort->Value() ) ;
        }
      }
    }
// <--- JR 30.01.2007 Right Debug PAL12455
    HasAllDataReady( true ) ; // ==> Ready to execute
    res = 1 ;
  }
  else { // At least one Flag == 0 :
#if SomeDataReadyActionTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " Node " << Name() << " HasNOTAllDataReady "
           << InReady << "/" << GetNodeInPortsSize() << " DoAllDataReadyIf " << DoAllDataReadyIf << endl ;
#endif
    HasAllDataReady( false ) ;
    res = SendEvent( GraphExecutor::NotAllDataReadyEvent );
  }

#if SomeDataReadyActionTrace
  cdebug_out << pthread_self() << "/" << ThreadNo() << Name()
             << " DataWaiting_SomeDataReadyAction " << endl;
#endif
  return res ;

}

#define NotAllDataReadyActionTrace 1
int GraphExecutor::InNode::DataUndef_NotAllDataReadyAction() {
//JR 15.04.2005 Debug PAL8624 RetroConception :
//  CreateNewThreadIf( false ) ;
#if NotAllDataReadyActionTrace
  cdebug << pthread_self() << " for " << ThreadNo()
         << " DataUndef_NotAllDataReadyAction " << Name() << endl;
#endif
  return 1;
}

#define AllDataReadyActionTrace 1
int GraphExecutor::InNode::DataUndef_AllDataReadyAction() {
#if AllDataReadyActionTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << " --> DataUndef_AllDataReadyAction " << Name() << endl ;
//         << " CreateNewThreadIf " << CreateNewThreadIf() << " IsLockedDataWait "
//         << IsLockedDataWait() ;
#endif
//JR 15.04.2005 Debug PAL8624 RetroConception :
  if ( !CreateNewThread() ) {
#if AllDataReadyActionTrace
    cdebug << "Thread " << ThreadNo() << " --> " << pthread_self() << endl ;
#endif
    ThreadNo( pthread_self() ) ;
  }
  else {
    _OutNode->IncrCreatedThreads() ;
  }
  _OutNode->PushEvent( this , GraphExecutor::AllDataReadyEvent ,
                       GraphExecutor::DataReadyState ) ; 
  ReadyAction() ;
  SUPERV::ControlState aControl = ControlState() ;
  switch ( aControl ) {
  case SUPERV::VoidState : {
    SendEvent( GraphExecutor::ExecuteEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendState : {
    SendEvent( GraphExecutor::SuspendEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendStartState : {
    SendEvent( GraphExecutor::SuspendEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendDoneState : {
    SendEvent( GraphExecutor::ExecuteEvent ) ;
    break ;
  }
  case SUPERV::ToKillState : {
    SendEvent( GraphExecutor::KillEvent ) ;
    break ;
  }
  case SUPERV::ToKillDoneState : {
    SendEvent( GraphExecutor::ExecuteEvent ) ;
    break ;
  }
  case SUPERV::ToStopState : {
    SendEvent( GraphExecutor::StopEvent ) ;
    break ;
  }
  default : {
    cdebug << ThreadNo()
           << " GraphExecutor::InNodeThreads::DataUndef_AllDataReadyAction Error Undefined Control : "
           << aControl << endl ;
    return 0;
  }
  }
#if AllDataReadyActionTrace
  cdebug << pthread_self() << "/" << ThreadNo()
         << " <-- DataUndef_AllDataReadyAction " << Name() << endl;
#endif
  return 1;
}

int GraphExecutor::InNode::DataReady_SuspendAction() {
  cdebug << pthread_self() << "/" << ThreadNo()
         << "DataReady_SuspendAction --> Suspend " << Name()
         << " Threads " << _OutNode->Threads() << " SuspendedThreads "
         << _OutNode->SuspendedThreads() << endl;
  _OutNode->PushEvent( this , GraphExecutor::SuspendedReadyEvent ,
                       GraphExecutor::SuspendedReadyState ) ;
  GraphExecutor::InNode * aReStartNode = SuspendAction() ;
  cdebug << pthread_self() << "/" << ThreadNo()
         << "DataReady_SuspendAction Resumed " << Name() << endl;
  if ( aReStartNode ) {
    _aReStartNode = NULL ;
    aReStartNode->SendEvent( _aReStartEvent ) ;
  }
  else {
    SendEvent( GraphExecutor::ExecuteEvent ) ;
  }
  return 1 ;
}

int GraphExecutor::InNode::SuspendedReady_ResumeAction() {
  cdebug << pthread_self() << "/" << ThreadNo() << "SuspendedReady_ResumeAction "
         << Name() << endl;
//  ResumeAction() ;
  _OutNode->PushEvent( this , GraphExecutor::ResumedReadyEvent ,
                       GraphExecutor::ResumedReadyState ) ; 
  return 1 ;
}

int GraphExecutor::InNode::DataReady_KillAction() {
  _OutNode->PushEvent( this , GraphExecutor::KilledReadyEvent ,
                       GraphExecutor::KilledReadyState ) ;
  KillAction() ;
  cdebug << pthread_self() << "/" << ThreadNo() << "DataReady_KillAction " << Name()
         << " will pthread_exit()" << endl;
  return 1 ;
}

int GraphExecutor::InNode::DataReady_StopAction() {
  _OutNode->PushEvent( this , GraphExecutor::StoppedReadyEvent ,
                       GraphExecutor::StoppedReadyState ) ; 
  StopAction() ;
  cdebug << pthread_self() << "/" << ThreadNo() << "DataReady_StopAction " << Name()
         << " will pthread_exit()" << endl;
  return 1 ;
}

#include <CORBA.h>

#define TraceDataReady_ExecuteAction 1
int GraphExecutor::InNode::DataReady_ExecuteAction() {

#if TraceDataReady_ExecuteAction
  cdebug_in << pthread_self() << "/" << ThreadNo() << " DataReady_ExecuteAction "
            << Name() << endl;
#endif
  _OutNode->PushEvent( this , GraphExecutor::ExecuteEvent ,
                       GraphExecutor::ExecutingState ) ; 

  RunningAction() ;

  bool Err = false ;

  Engines::Container_var myContainer ;
  Engines::Component_var myObjComponent ;

  SUPERV::GraphState PortState = SUPERV::ReadyState ;
  GraphExecutor::AutomatonState NewState = GraphExecutor::DataUndefState ;
  GraphExecutor::NodeEvent NewEvent = GraphExecutor::UndefinedEvent ;

  int nInParams ;
  ServicesAnyData * InParametersList = NULL ;
  int nOutParams ;
  ServicesAnyData * OutParametersList = NULL ;

  nInParams = GetNodeInPortsSize()  ;
#if TraceDataReady_ExecuteAction
  char * aName = Name() ;
  cdebug << pthread_self() << "/" << ThreadNo() << " " << aName
         << " nInParams " << nInParams << " InParametersList "
         << (void * ) InParametersList << endl ;
#endif
  InParametersList = new ServicesAnyData[nInParams];
  InParametersSet( Err , nInParams , InParametersList ) ;

  nOutParams = GetNodeOutPortsSize() ;
  OutParametersList = new ServicesAnyData[nOutParams];
  InOutParametersSet( nOutParams , OutParametersList ) ;

#if 0
  if ( !Err && IsComputingNode() ) {
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " "
           << " after creation of InParametersList : nInParams " << nInParams
           << " :" << endl;
    int i ;
    for ( i = 0 ; i < nInParams-1 ; i++ ) { // Without Gates
      cdebug << "InParametersList[" << i << "] : "
             << InParametersList[i].Name << " "
             << AnyValue( InParametersList[i].Value ) << endl ;
    }
    CORBA::Object * obj ;
    InParametersList[0].Value >>= obj ;
    Engines::Component_var theObjComponent ;
    theObjComponent = Engines::Component::_narrow( obj ) ;
    DynInvoke( theObjComponent , "ping" ,
               NULL , 0 , NULL , 0 ) ;
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " ping done "
           << endl ;
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " "
           << " after creation of OutParametersList :" << endl;
    for ( i = 0 ; i < nOutParams-1 ; i++ ) { // Without Gates
      cdebug << "OutParametersList[" << i << "] : "
             << OutParametersList[i].Name << " "
             << AnyValue( OutParametersList[i].Value ) << endl ;
    }
  }
#endif

  if ( !IsMacroNode() ) {

    if ( !IsFactoryNode() ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << "No Component : NO StartComponent & No Ping" << endl ;
#endif
      if ( IsComputingNode() ) {
        ObjInterface( true ) ;
//JR 05.08.2005 DEBUG : that code runs with SALOME_3 (OMNIOrb4) ...
#if OMNIORB_VERSION >= 4
        CORBA::Object * obj ;
        InParametersList[0].Value >>= obj ;
        myObjComponent = Engines::Component::_narrow( obj ) ;
//JR 05.08.2005 DEBUG : the folowing code runs with OMNIOrb3 but gives
//                      unpredictable results with SALOME_3 (OMNIOrb4) ...
#else
        CORBA::Object_ptr obj ;
        InParametersList[0].Value >>= obj ;
        CORBA::Object_var objvar = CORBA::Object_var( obj ) ;
        myObjComponent = Engines::Component::_duplicate( Engines::Component::_narrow( objvar ) ) ;
#endif
      }
      else {
      }
    }
    else if ( CORBA::is_nil( Component() ) ) {
      Err = !_OutNode->Graph()->StartComponent( ThreadNo() , Computer() ,
//JR 17.02.2005 Memory Leak                                                my_strdup( ComponentName() ) ,
                                                ComponentName() ,
                                                myContainer , myObjComponent ) ;
      ObjInterface( false ) ;
      SetContainer( myContainer ) ;
      SetComponent( myObjComponent ) ;
    }
    else {
      myContainer = Container() ;
      myObjComponent = Component() ;
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << "Component known : NO StartComponent & Ping"
             << endl ;
      try {
        myObjComponent->ping() ;
      }
      catch( ... ) {
        cdebug << "ping() ERROR catched" << endl ;
        Err = true ;
      }
#endif
    }

    if ( Err || ControlState() == SUPERV::ToKillState ||
                ControlState() == SUPERV::ToKillDoneState ||
                ControlState() == SUPERV::ToStopState ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << "StartComponent Error or ToKillState" << endl ;
#endif
      Err = true ;
    }
    else {
      if ( ControlState() == SUPERV::ToSuspendState ) {
#if TraceDataReady_ExecuteAction
        cdebug << ThreadNo() << "ToSuspendState before running." << endl ;
        MESSAGE(ThreadNo() << "ToSuspendState before running.") ;
#endif
      }
#if TraceDataReady_ExecuteAction
      int i;
      cdebug << ThreadNo() << " Run( '" << ServiceName() << "'" ;
      for ( i = 0 ; i < (int ) ServiceInParameter().length() ; i++ ) {
        cdebug << " , " << InParametersList[ i ].Name << "[kind"
               << InParametersList[ i ].Value.type()->kind() << "]" ;
      }
      for ( i = 0 ; i < (int ) ServiceOutParameter().length() ; i++ ) {
        cdebug << " , " << OutParametersList[ i ].Name << "[kind"
               << OutParametersList[ i ].Value.type()->kind() << "]" ;
      }
      if ( IsOneOfInLineNodes() ) {
        cdebug << " , PyFuncName '" << InLineNode()->PyFuncName() << "' PyRunMethod "
               << InLineNode()->PyRunMethod() << " length "
               << (*InLineNode()->PythonFunction()).length()
               << " InParametersList " << InParametersList
               << " OutParametersList " << OutParametersList ;
      }
      cdebug << ")" << endl ;
#endif

      if ( IsOneOfInLineNodes() ) {
        Err = DataReady_ExecuteActionInLineNodes( InParametersList ,
                                                  OutParametersList ) ;
      }

      else {
#if 0
        if ( IsComputingNode() ) {
          cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name()
                 << " myObjComponent " << myObjComponent << " "
                 << ObjectToString( myObjComponent ) << endl ;
          cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " "
                 << " before DynInvoke nInParams " << nInParams
                 << " :" << endl;
          int i ;
          CORBA::Object * obj ;
          InParametersList[0].Value >>= obj ;
          Engines::Component_var theObjComponent ;
          theObjComponent = Engines::Component::_narrow( obj ) ;
          DynInvoke( theObjComponent , "ping" ,
                     NULL , 0 , NULL , 0 ) ;
          for ( i = 0 ; i < nInParams-1 ; i++ ) { // Without Gates
            cdebug << "InParametersList[" << i << "] : "
                   << InParametersList[i].Name << " "
                   << AnyValue( InParametersList[i].Value ) << endl ;
          }
        }
#endif
        try {
#if TraceDataReady_ExecuteAction
          cdebug << "DynInvoke -> Names " << _OutNode->Name() << " " << Name() << endl ;
#endif
          DynInvoke( myObjComponent, "Names" ,
                     _OutNode->Name() , Name() ) ;
        }
        catch( ... ) {
          string anErrorMessage = string( "Dynamic CORBA call to Names for node " ) +
                                  string( Name() ) + " catched." ;
          _OutNode->Graph()->SetMessages( anErrorMessage ) ;
          cdebug << "DynInvoke Names catched ERROR" << endl ;
        }
// for DataStreamNodes : call of SetProperties ===> environment variables in the component/container
        if ( ComputingNode()->HasDataStream() ) {
          try {
#if TraceDataReady_ExecuteAction
            cdebug << "DynInvoke -> SetProperties " << _OutNode->Name() << " " << Name() << endl ;
#endif
            Engines::FieldsDict_var dict = new Engines::FieldsDict;
            dict->length( 4 );
            dict[ 0 ].key = CORBA::string_dup( "CAL_MACHINE");
		// myContainer->getHostName() ne renvoit pas le nom complet (avec domaine).
		//		dict[ 0 ].value <<= myContainer->getHostName() ;
            char FullyQualifiedDomainName[256]="";
            gethostname(FullyQualifiedDomainName,255);
            dict[ 0 ].value <<=  FullyQualifiedDomainName ;
            dict[ 1 ].key = CORBA::string_dup( "CAL_REPERTOIRE");
            dict[ 1 ].value <<= "/tmp" ;
            dict[ 2 ].key = CORBA::string_dup( "CAL_COUPLAGE");
            stringstream ofst1 ;
            ofst1 << ComputingNode()->SubStreamGraph() ;
            string cpl = string( "/tmp/" ) + string( _OutNode->Name() ) + string( "_" ) + 
          	         ofst1.str() + string( ".cpl" );
            dict[ 2 ].value <<= cpl.c_str() ;
            dict[ 3 ].key = CORBA::string_dup( "SALOME_INSTANCE_NAME");
            string uname = Name();
            UpperCase( uname);
            dict[ 3 ].value <<= uname.c_str() ;

            myObjComponent->setProperties( dict ) ;
          }
          catch( ... ) {
            string anErrorMessage = string( "Dynamic CORBA call to setProperties for node " ) +
                                    string( Name() ) + " catched." ;
            _OutNode->Graph()->SetMessages( anErrorMessage ) ;
            cdebug << "DynInvoke setProperties catched ERROR" << endl ;
            Err = true;
          }
        }

        try {
          if ( !Err && IsComputingNode() ) {
#if TraceDataReady_ExecuteAction
            cdebug << ThreadNo() << " !ObjInterface " << Name()
                   << " IsComputingNode DynInvoke"  << endl ;
            cdebug << ServiceInParameter().length()-1 << " input parameters and "
                   << ServiceOutParameter().length() << " output parameters" << endl ;
#endif
            IsLoading( false ) ;
            DynInvoke( myObjComponent,
                       ServiceName() ,
                       &InParametersList[1] , ServiceInParameter().length()-1 ,
                       &OutParametersList[0] , ServiceOutParameter().length() ) ;
#if 0
            { cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name()
                     << " myObjComponent " << myObjComponent << " "
                     << ObjectToString( myObjComponent ) << endl ;
              cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " "
                     << " after DynInvoke nInParams " << nInParams
                     << " :" << endl;
              CORBA::Object * obj ;
              InParametersList[0].Value >>= obj ;
              Engines::Component_var theObjComponent ;
              theObjComponent = Engines::Component::_narrow( obj ) ;
              DynInvoke( theObjComponent , "ping" ,
                         NULL , 0 , NULL , 0 ) ;
              for ( i = 0 ; i < nInParams-1 ; i++ ) { // Without Gates
                cdebug << "InParametersList[" << i << "] : "
                       << InParametersList[i].Name << " "
                       << AnyValue( InParametersList[i].Value ) << endl ;
              }
            }
#endif
          }
          else if ( !Err && IsFactoryNode() ) {
#if TraceDataReady_ExecuteAction
            cdebug << pthread_self() << "/" << ThreadNo() << " !ObjInterface " << Name()
                   << " IsFactoryNode DynInvoke"  << endl ;
            cdebug << ServiceInParameter().length() << " input parameters and "
                   << ServiceOutParameter().length() << " output parameters" << endl ;
#endif
            IsLoading( false ) ;
            DynInvoke( myObjComponent,
                       ServiceName() ,
                       &InParametersList[0] , ServiceInParameter().length() ,
                       &OutParametersList[0] , ServiceOutParameter().length() ) ;
          }
//            cdebug << ThreadNo() << " Component::CpuUsed " << Name() << " "
//                   << myObjComponent->CpuUsed_impl() << endl ;
        }
        catch( ... ) {
          Err = true ;
          string anErrorMessage = string( "Dynamic CORBA call for node " ) +
                                  string( Name() ) + " catched." ;
          _OutNode->Graph()->SetMessages( anErrorMessage ) ;
          cdebug << pthread_self() << "/" << ThreadNo() << " !ObjInterface " << Name()
                 << " Node(Component) Dynamic Call Exception catched ERROR"
                 << endl ;
//Reset of _ThreadId in the Container ...
//          try {
//            myObjComponent->Kill_impl() ;
//          }
//          catch( ... ) {
//          }
        }
      }
    }
  }

  if ( Err ) {
    
    // if exception or something else - IsLoading( false ) may not NOT has been called
    if ( IsLoading() )
      IsLoading( false );

    if ( ControlState() == SUPERV::ToKillState ||
         ControlState() == SUPERV::ToKillDoneState ||
         ControlState() == SUPERV::ToStopState ) {
      PortState = SUPERV::ErrorState ;
      NewState = GraphExecutor::KilledState ;
      NewEvent = GraphExecutor::KillEvent ;
    }
    else {
      PortState = SUPERV::ErrorState ;
      NewState = GraphExecutor::ErroredState ;
      NewEvent = GraphExecutor::ErrorEvent ;
    }
  }
  else {
    PortState = SUPERV::ReadyState ;
    NewState = GraphExecutor::DataReadyState ;
    NewEvent = GraphExecutor::SuccessEvent ;
  }

  if ( !IsMacroNode() ) {
//JRStep A
    bool ErrOut = OutParametersSet( Err , PortState , nOutParams , OutParametersList ) ;
    if ( !ErrOut ) {
      NewEvent = GraphExecutor::ErrorEvent ;
    }
#if 0
    if ( !Err && IsComputingNode() ) {
      cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name()
             << " myObjComponent " << myObjComponent << " "
             << ObjectToString( myObjComponent ) << endl ;
      cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " "
             << " delete [] InParametersList nInParams " << nInParams
             << " :" << endl;
      int i ;
      CORBA::Object * obj ;
      InParametersList[0].Value >>= obj ;
      Engines::Component_var theObjComponent ;
      theObjComponent = Engines::Component::_narrow( obj ) ;
      DynInvoke( theObjComponent , "ping" ,
                 NULL , 0 , NULL , 0 ) ;
      for ( i = 0 ; i < nInParams-1 ; i++ ) { // Without Gates
        cdebug << "InParametersList[" << i << "] : "
               << InParametersList[i].Name << " "
               << AnyValue( InParametersList[i].Value ) << endl ;
      }
    }
#endif
    try {
      delete [] InParametersList ;
    }
    catch(...) {
      cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name()
             << " catch ERROR of delete [] InParametersList" << endl ;
    }
#if TraceDataReady_ExecuteAction
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " "
           << " delete [] OutParametersList :" << endl;
    int i ;
    for ( i = 0 ; i < nOutParams-1 ; i++ ) { // Without Gates
      cdebug << "OutParametersList[" << i << "] : "
             << OutParametersList[i].Name << " "
             << AnyValue( OutParametersList[i].Value ) << endl ;
    }
#endif
    try {
      delete [] OutParametersList ;
    }
    catch(...) {
      cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name()
             << " catch ERROR of delete [] OutParametersList" << endl ;
    }
    SendEvent( NewEvent ) ;
  }
  else {
    GraphExecutor::DataFlow * aMacroGraph = GraphMacroNode()->CoupledNode()->GraphEditor()->Executor() ;
#if TraceDataReady_ExecuteAction
    int i ;
    for ( i = 0 ; i < GraphMacroNode()->GetNodeOutPortsSize() ; i++ ) {
      cdebug << "Out" << i << " " << GraphMacroNode()->GetNodeOutPort( i )->PortName() << " "
             << GraphMacroNode()->GetChangeNodeOutPort( i )->PortState() << " Done="
             << GraphMacroNode()->GetChangeNodeOutPort( i )->PortDone() << " " ;
      if ( GraphBase::Base::_prof_debug ) {
        GraphMacroNode()->GetNodeOutPort( i )->StringValue( *GraphBase::Base::_fdebug ) ;
      }
      if ( GraphMacroNode()->GetChangeNodeOutPort( i )->IsGate() ) {
        cdebug << " BoolValue " << GraphMacroNode()->GetChangeNodeOutPort( i )->BoolValue() ;
      }
      cdebug << endl ;
    }
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << aMacroGraph << " "
           << aMacroGraph->Name() << " ->DoneWait()"
           << " State " << aMacroGraph->State() << endl;
#endif
    aMacroGraph->DoneWait() ;
#if TraceDataReady_ExecuteAction
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " State "
           << aMacroGraph->State() << endl;
#endif

//JR 29.09.2005 Debug for CEA (examples/GraphMacroNodes1) :
// Set of value of the OutGate of the corresponding MacroNode was missing
    CORBA::Any anAny = CORBA::Any() ;
    anAny <<= (CORBA::Long ) 1 ;
    GraphMacroNode()->GraphEditor()->Executor()->OutputOfAny( Name() , "Gate" , anAny ) ;
#if TraceDataReady_ExecuteAction
    cdebug << "DataReady_ExecuteAction OutputOfAny( " << Name() << " , Gate , 1 )" << endl ;

    for ( i = 0 ; i < GraphMacroNode()->GetNodeOutPortsSize() ; i++ ) {
      cdebug << "Out" << i << " " << GraphMacroNode()->GetNodeOutPort( i )->PortName() << " "
             << GraphMacroNode()->GetChangeNodeOutPort( i )->PortState() << " Done="
             << GraphMacroNode()->GetChangeNodeOutPort( i )->PortDone() << " " ;
      if ( GraphBase::Base::_prof_debug ) {
        GraphMacroNode()->GetNodeOutPort( i )->StringValue( *GraphBase::Base::_fdebug ) ;
      }
      if ( GraphMacroNode()->GetChangeNodeOutPort( i )->IsGate() ) {
        cdebug << " BoolValue " << GraphMacroNode()->GetChangeNodeOutPort( i )->BoolValue() ;
      }
      cdebug << endl ;
    }
    cdebug << ThreadNo() << " DataReady_ExecuteAction " << Name() << " State " << aMacroGraph->State() << endl;
#endif
    if ( aMacroGraph->State() == SUPERV::DoneState ) {
      PortState = SUPERV::ReadyState ;
      NewState = GraphExecutor::DataReadyState ;
      NewEvent = GraphExecutor::SuccessEvent ;
    }
    else {
      Err = true ;
      if ( ControlState() == SUPERV::ToKillState ||
           ControlState() == SUPERV::ToKillDoneState ||
           ControlState() == SUPERV::ToStopState ) {
        PortState = SUPERV::ErrorState ;
        NewState = GraphExecutor::KilledState ;
        NewEvent = GraphExecutor::KillEvent ;
      }
      else {
        PortState = SUPERV::ErrorState ;
        NewState = GraphExecutor::ErroredState ;
        NewEvent = GraphExecutor::ErrorEvent ;
      }
    }
    bool ErrOut = OutParametersSet( Err , PortState , nOutParams , OutParametersList ) ;
    if ( !ErrOut ) {
      NewEvent = GraphExecutor::ErrorEvent ;
    }
    delete [] InParametersList ;
    delete [] OutParametersList ;
    SendEvent( NewEvent ) ;
  }

#if TraceDataReady_ExecuteAction
  cdebug_out << ThreadNo() << " DataReady_ExecuteAction " << Name() << endl;
#endif
  return 1 ;
}

int GraphExecutor::InNode::DataReady_ExecuteActionInLineNodes( ServicesAnyData * InParametersList ,
                                                               ServicesAnyData * OutParametersList ) {

#if TraceDataReady_ExecuteAction
  cdebug_in << pthread_self() << "/" << ThreadNo()
            << " DataReady_ExecuteActionInLineNodes " << Name()
            << " InParametersList " << InParametersList
            << " OutParametersList " << OutParametersList  << endl;
#endif
  bool Err = false ;
  bool StsPyDynInvoke = true ;
  _OutNode->PyThreadLock() ;
  SetPyCpuUsed() ;
  try {
    bool ItIsaLoop = false ;
    bool CopyInOut = false ;
    if ( IsInLineNode() &&
         strlen( InLineNode()->PyFuncName() ) ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name() << " PyFuncName '"
             << InLineNode()->PyFuncName()
             << "' IsInLineNode PyDynInvoke"  << endl ;
#endif
      StsPyDynInvoke = PyDynInvoke( InLineNode()->PyRunMethod() ,
                                    InLineNode()->PyFuncName() ,
                                    InParametersList , ServiceInParameter().length() ,
                                    OutParametersList , ServiceOutParameter().length() ) ;
      if ( !StsPyDynInvoke ) {
        RemovePyDynInvoke( InLineNode()->PyFuncName() ) ;
      }
    }
    else if ( IsLoopNode() ) {
      ItIsaLoop = true ;
      Err = DataReady_ExecuteActionLoopNodes( InParametersList ,
                                              OutParametersList ,
                                              CopyInOut ) ;
    }
    else if ( IsSwitchNode() && /*InLineNode()->PyRunMethod() &&*/
              strlen( InLineNode()->PyFuncName() ) ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name() << " PyFuncName '"
             << InLineNode()->PyFuncName()
             << "' IsSwitchNode PyDynInvoke"  << endl ;
#endif
      StsPyDynInvoke = PyDynInvoke( InLineNode()->PyRunMethod() ,
                                    InLineNode()->PyFuncName() ,
                                    InParametersList , ServiceInParameter().length() ,
                                    OutParametersList , ServiceOutParameter().length() ) ;

      if ( !StsPyDynInvoke ) {
        string anErrorMessage = string( "Dynamic Python call for node " ) +
                                string( Name() ) + " function " +
                                InLineNode()->PyFuncName() + " error." ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
        RemovePyDynInvoke( InLineNode()->PyFuncName() ) ;
      }
    }
    else if ( IsGOTONode() && /*InLineNode()->PyRunMethod() &&*/
              strlen( InLineNode()->PyFuncName() ) ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name() << " PyFuncName '"
             << InLineNode()->PyFuncName()
             << "' IsGOTONode PyDynInvoke"  << endl ;
#endif

      StsPyDynInvoke = PyDynInvoke( InLineNode()->PyRunMethod() ,
                                    InLineNode()->PyFuncName() ,
                                    InParametersList , ServiceInParameter().length() ,
                                    OutParametersList , ServiceOutParameter().length() ) ;

      if ( !StsPyDynInvoke ) {
        string anErrorMessage = string( "Dynamic Python call for node " ) +
                                string( Name() ) + " function " +
                                InLineNode()->PyFuncName() + " error." ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
        RemovePyDynInvoke( GOTONode()->PyFuncName() ) ;
      }
    }
    else if ( ( IsEndSwitchNode() ) &&
              InLineNode()->PyRunMethod() && strlen( InLineNode()->PyFuncName() ) ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name() << " PyFuncName '"
             << InLineNode()->PyFuncName()
             << "' IsSwitchNode PyDynInvoke"  << endl ;
#endif

      StsPyDynInvoke = PyDynInvoke( InLineNode()->PyRunMethod() ,
                                    InLineNode()->PyFuncName() ,
                                    InParametersList , ServiceInParameter().length() ,
                                    OutParametersList , ServiceOutParameter().length() ) ;

      if ( !StsPyDynInvoke ) {
        string anErrorMessage = string( "Dynamic Python call for node " ) +
                                string( Name() ) + " function " +
                                InLineNode()->PyFuncName() + " error." ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
        RemovePyDynInvoke( InLineNode()->PyFuncName() ) ;
      }
    }
    else if ( IsEndLoopNode() &&
              InLineNode()->PyRunMethod() && strlen( InLineNode()->PyFuncName() ) ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name() << " PyFuncName '"
             << InLineNode()->PyFuncName()
             << "' IsSwitchNode PyDynInvoke"  << endl ;
#endif

      StsPyDynInvoke = PyDynInvoke( InLineNode()->PyRunMethod() ,
                                    InLineNode()->PyFuncName() ,
                                    InParametersList , ServiceInParameter().length() + 1 ,
                                    OutParametersList , ServiceOutParameter().length() + 1 ) ;

      if ( !StsPyDynInvoke ) {
        string anErrorMessage = string( "Dynamic Python call for node " ) +
                                string( Name() ) + " function " +
                                InLineNode()->PyFuncName() + " error." ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
        RemovePyDynInvoke( InLineNode()->PyFuncName() ) ;
      }
    }

    if ( (!ItIsaLoop && ( InLineNode()->PyRunMethod() == NULL ||
                          strlen( InLineNode()->PyFuncName() ) == 0 ) ) || CopyInOut ) {
// This is a void Python Function : without code (No PyFuncName)
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name()
             << " Copy of " << ServiceInParameter().length()
             << " OutParameters" << endl ;
#endif
      int i ;
      int argout0 = 0 ;
      int argin0 = 0 ;
      if ( IsLoopNode() || IsEndLoopNode() ) {
        argout0 = 1 ;
        argin0 = 1 ; // after DoLoop
        if ( IsLoopNode() ) { // More() is void
#if TraceDataReady_ExecuteAction
          cdebug << Name() << " Not Beginning of loop and non void EndLoop : DoLoop = EndLoop(DoLoop)"
                 << endl ;
#endif
          GraphExecutor::InNode * anEndLoopNode = (GraphExecutor::InNode * ) CoupledNode()->GetInNode() ;
          OutParametersList[0].Value = anEndLoopNode->GetNodeOutLoop()->Value() ; // DoLoop = EndLoop(DoLoop)
        }
      }
//PAL8072 ==> PAL8512
//JR 24.03.2005 : Debug : void InLine Python function : check of the number of Input Ports
//                        equals the number of Output Ports was missing
      if ( ServiceInParameter().length() != ServiceOutParameter().length() ) {
         string anErrorMessage = string( "Inconsistent number of In<->Out parameters for the vois Python function of the node " ) +
                                 string( Name() ) ;
         _OutNode->Graph()->SetMessages( anErrorMessage ) ;
         StsPyDynInvoke = false ;
      }
      else {
        for ( i = 0 ; i < (int ) ServiceInParameter().length() ; i++ ) {
          OutParametersList[argout0 + i].Value = InParametersList[argin0 + i].Value ;

#if TraceDataReady_ExecuteAction
          cdebug << "ArgOut->In" << InParametersList[argin0 + i].Name.c_str()
                 << " " << AnyValue( InParametersList[argin0 + i].Value )
                 << endl ;
#endif
        }
      }
    }
    if ( !StsPyDynInvoke ) {
      Err = true ;
      string anErrorMessage = string( "Dynamic Python call for node " ) +
                              string( Name() ) + " error." ;
      _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      cdebug << ThreadNo() << " InLineNode " << Name()
             << " Python Dynamic Call Error"
             << endl ;
    }
  }
  catch( ... ) {
    Err = true ;
    string anErrorMessage = string( "Dynamic Python call for node " ) +
                            string( Name() ) + " catched." ;
    _OutNode->Graph()->SetMessages( anErrorMessage ) ;
    cdebug << ThreadNo() << " InLineNode " << Name()
           << " Python Dynamic Call Exception catched ERROR"
           << endl ;
  }
  CpuUsed( true ) ;
  _OutNode->PyThreadUnLock() ;
#if TraceDataReady_ExecuteAction
  cdebug_out << pthread_self() << "/" << ThreadNo()
             << " DataReady_ExecuteActionInLineNodes " << Name() << endl;
#endif
  return Err ;
}

int GraphExecutor::InNode::DataReady_ExecuteActionLoopNodes( ServicesAnyData * InParametersList ,
                                                             ServicesAnyData * OutParametersList ,
                                                             bool & CopyInOut ) {

#if TraceDataReady_ExecuteAction
  cdebug_in << pthread_self() << "/" << ThreadNo()
            << " DataReady_ExecuteActionLoopNodes " << Name()
            << " InParametersList " << InParametersList
            << " OutParametersList " << OutParametersList  << endl;
#endif
  bool Err = false ;
  bool StsPyDynInvoke = true ;
      bool CopyOutIn = false ;
// Switch between Init() and Next()
// if InLoop port is true and does not come from EndLoop ==> execute Init
// if InLoop port is false or come from EndLoop ==> execute Next
  if ( _InitLoop ) {
    if ( strlen( InLineNode()->PyFuncName() ) ) { // InLoop Port = true ==> Init()
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name()
             << " IsLoopNode PyDynInvoke '" << InLineNode()->PyFuncName()
             << "' InitLoop " << LoopNode()->PyRunMethod() << endl ;
#endif
      StsPyDynInvoke = PyDynInvoke( InLineNode()->PyRunMethod() ,
                                    InLineNode()->PyFuncName() ,
                                    &InParametersList[1] , ServiceInParameter().length() ,
                                    &OutParametersList[1] , ServiceOutParameter().length() ) ;
      if ( !StsPyDynInvoke ) {
        string anErrorMessage = string( "Dynamic Python call for node " ) +
                                string( Name() ) + " function " +
                                InLineNode()->PyFuncName() + " error." ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
        RemovePyDynInvoke( InLineNode()->PyFuncName() ) ;
      }
      CopyOutIn = true ;
    }
    else {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name()
             << " IsLoopNode NO PyDynInvoke Void PyFuncName InitLoop" << endl ;
#endif
    }
#if TraceDataReady_ExecuteAction
    cdebug << ThreadNo() << " !ObjInterface " << Name()
           << " IsLoopNode _InitLoop Reset after Init() Python Function" << endl ;
#endif
    _InitLoop = false ;
  }
  else if ( LoopNode()->PyNextMethod() &&
            strlen( LoopNode()->PyNextName() ) ){ // InLoop Port = false ==> Next()
#if TraceDataReady_ExecuteAction
    cdebug << ThreadNo() << " !ObjInterface " << Name()
           << " IsLoopNode PyDynInvoke '" << LoopNode()->PyNextName()
           << "' " << LoopNode()->PyNextMethod() << endl ;
#endif
    StsPyDynInvoke = PyDynInvoke( LoopNode()->PyNextMethod() ,
                                  LoopNode()->PyNextName() ,
                                  &InParametersList[1] , ServiceInParameter().length() ,
                                  &OutParametersList[1] , ServiceOutParameter().length() ) ;
    if ( !StsPyDynInvoke ) {
      string anErrorMessage = string( "Dynamic Python call for node " ) +
                              string( Name() ) + " function " +
                              LoopNode()->PyNextName() + " error." ;
      _OutNode->Graph()->SetMessages( anErrorMessage ) ;
      RemovePyDynInvoke( LoopNode()->PyNextName() ) ;
    }
    CopyOutIn = true ;
  }
  else {
#if TraceDataReady_ExecuteAction
    cdebug << ThreadNo() << " !ObjInterface " << Name()
           << " IsLoopNode NO PyDynInvoke Void PyFuncName NextLoop" << endl ;
#endif
  }
  if ( StsPyDynInvoke ) {
    if ( CopyOutIn ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name()
             << " IsLoopNode PyDynInvoke '" << LoopNode()->PyMoreName()
             << "' Copy of " << ServiceInParameter().length()
             << " OutParameters" << endl ;
#endif
      int i ;
// Start at 1 : Do not copy InLoop ( InLoop == true <==> Init ; InLoop == false <==> Next )
      for ( i = 1 ; i <= (int ) ServiceInParameter().length() ; i++ ) {
        InParametersList[i].Value = OutParametersList[i].Value ;
        InParametersList[i].Name = OutParametersList[i].Name ;
#if TraceDataReady_ExecuteAction
        cdebug << "ArgOut->In" << InParametersList[ i].Name.c_str()
               << " " << AnyValue( InParametersList[ i].Value )
               << endl ;
#endif
      }
    }
    if ( LoopNode()->PyMoreMethod() && strlen( LoopNode()->PyMoreName() ) ) {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name()
             << " IsLoopNode PyDynInvoke '" << LoopNode()->PyMoreName()
             << "' " << LoopNode()->PyMoreMethod() << endl ;
#endif
      StsPyDynInvoke = PyDynInvoke( LoopNode()->PyMoreMethod() ,
                                    LoopNode()->PyMoreName() ,
                                    &InParametersList[1] , ServiceInParameter().length() ,
                                    &OutParametersList[0] , ServiceOutParameter().length()+1 ) ;
      if ( !StsPyDynInvoke ) {
        string anErrorMessage = string( "Dynamic Python call for node " ) +
                                string( Name() ) + " function " +
                                LoopNode()->PyMoreName() + " error." ;
        _OutNode->Graph()->SetMessages( anErrorMessage ) ;
        RemovePyDynInvoke( LoopNode()->PyMoreName() ) ;
      }
    }
    else {
#if TraceDataReady_ExecuteAction
      cdebug << ThreadNo() << " !ObjInterface " << Name()
             << " IsLoopNode PyDynInvoke '" << LoopNode()->PyMoreName()
             << "' No MoreMethod" << endl ;
#endif
      CopyInOut = true ;
    }
  }
  else {
    Err = true ;
    cdebug << ThreadNo() << " InLineNode " << Name() << " "
           << InLineNode()->PyFuncName() << "/" << LoopNode()->PyNextName()
           << " Python Dynamic Call Error"
           << endl ;
  }
#if TraceDataReady_ExecuteAction
  cdebug_out << pthread_self() << "/" << ThreadNo()
             << " DataReady_ExecuteActionLoopNodes " << Name() << endl;
#endif
  return Err ;
}

int GraphExecutor::InNode::Executing_SuspendAction() {
  _OutNode->PushEvent( this , GraphExecutor::SuspendedExecutingEvent ,
                       GraphExecutor::SuspendedExecutingState ) ; 
  cdebug << ThreadNo() << " Executing_SuspendAction " << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::SuspendedExecuting_ResumeAction() {
  cdebug << ThreadNo() << " SuspendedExecuting_ResumeAction " << Name() << endl;
  GraphExecutor::AutomatonState next_state ;
  next_state = Automaton()->NextState( State() , GraphExecutor::ExecutingEvent ) ;
  _OutNode->NewThread() ; // Only for Threads count
  _OutNode->PushEvent( this , GraphExecutor::ResumedExecutingEvent ,
                       next_state ) ; 
  State( next_state ) ;
  return 1 ;
}

int GraphExecutor::InNode::Executing_KillAction() {
  cdebug << ThreadNo() << " Executing_KillAction " << Name() << " Thread " << ThreadNo()<< endl;
  int RetVal = 0 ;
  if ( pthread_self() == ThreadNo() ) {
    cdebug << "Executing_KillAction would pthread_canceled itself" << endl ;
    KillAction() ;
    _OutNode->PushEvent( this , GraphExecutor::KilledExecutingEvent ,
                         GraphExecutor::KilledExecutingState ) ; 
    RetVal = 1 ;
  }
  else if ( pthread_cancel( ThreadNo() ) ) {
    perror("Executing_KillAction pthread_cancel error") ;
  }
  else {
    cdebug << pthread_self() << " Executing_KillAction : ThreadId " << ThreadNo()
           << " pthread_canceled" << endl ;
    KillAction() ;
    _OutNode->ExitThread( ThreadNo() ) ;
    _OutNode->PushEvent( this , GraphExecutor::KilledExecutingEvent ,
                         GraphExecutor::KilledExecutingState ) ; 
  }
  return RetVal ;
}

int GraphExecutor::InNode::Executing_StopAction() {
  cdebug << ThreadNo() << " Executing_StopAction " << Name() << " Thread " << ThreadNo() << endl;
  int RetVal = 0 ;
  if ( pthread_cancel( ThreadNo() ) ) {
    perror("Executing_KillAction pthread_cancel error") ;
  }
  else {
    cdebug << pthread_self() << " Executing_KillAction : ThreadId " << ThreadNo()
           << " pthread_canceled" << endl ;
    StopAction() ;
    _OutNode->ExitThread( ThreadNo() ) ;
    _OutNode->PushEvent( this , GraphExecutor::StoppedExecutingEvent ,
                         GraphExecutor::StoppedExecutingState ) ; 
  }
  return RetVal ;
}

int GraphExecutor::InNode::Executing_SuccessAction() {
//  cdebug << ThreadNo() << " --> Executing_SuccessAction " << Name() << endl;
  _OutNode->PushEvent( this , GraphExecutor::SuccessedExecutingEvent ,
                       GraphExecutor::SuccessedState ) ; 
//  MESSAGE(pthread_self() << "Executor::InNode::Executing_SuccessAction of " << Name()
//          << " ControlState " << Automaton()->ControlStateName( ControlState() )
//          << " AFTER execution ThreadNo " << ThreadNo() ) ;
  SUPERV::ControlState aControl = ControlState() ;
  switch ( aControl ) {
  case SUPERV::VoidState : {
    SendEvent( SuccessEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendState : {
    SendEvent( SuccessEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendDoneState : {
    SendEvent( GraphExecutor::SuspendEvent ) ;
    return 1 ;
  }
  case SUPERV::ToKillState : {
    SendEvent( GraphExecutor::KillEvent ) ;
    return 1 ;
  }
  case SUPERV::ToKillDoneState : {
    SendEvent( GraphExecutor::KillEvent ) ;
    return 1 ;
  }
  case SUPERV::ToStopState : {
    SendEvent( GraphExecutor::StopEvent ) ;
    return 1 ;
  }
  default : {
    cdebug << ThreadNo()
           << " GraphExecutor::InNodeThreads::Executing_SuccessAction Error Undefined Control : "
           << aControl << endl ;
    return 0;
  }
  }
//  cdebug << ThreadNo() << " <-- Executing_SuccessAction "  << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::Errored_ExecutingAction() {
  cdebug << ThreadNo() << " --> Errored_ExecutingAction " << Name() << endl;
  _OutNode->PushEvent( this , GraphExecutor::ErroredExecutingEvent ,
                       GraphExecutor::ErroredState ) ; 

  _OutNode->NodeAborted( Name() ) ;

  SUPERV::ControlState aControl = ControlState() ;
  switch ( aControl ) {
  case SUPERV::VoidState : {
    SendEvent( ErrorEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendState : {
    SendEvent( ErrorEvent ) ;
    break ;
  }
  case SUPERV::ToSuspendDoneState : {
    SendEvent( GraphExecutor::SuspendEvent ) ;
    return 1 ;
  }
  case SUPERV::ToKillState : {
    SendEvent( GraphExecutor::KillEvent ) ;
    return 1 ;
  }
  case SUPERV::ToKillDoneState : {
    SendEvent( GraphExecutor::KillEvent ) ;
    return 1 ;
  }
  case SUPERV::ToStopState : {
    SendEvent( GraphExecutor::StopEvent ) ;
    return 1 ;
  }
  default : {
    cdebug << ThreadNo()
           << " GraphExecutor::InNodeThreads::Errored_ExecutingAction Error Undefined Control : "
           << aControl << endl ;
    return 0;
  }
  }
  cdebug << ThreadNo() << " <-- Errored_ExecutingAction "  << Name() << endl;
  return 1 ;
}

#define SetWaitingStatesTrace 0
// Set SUPERV::WaitingState to all InPorts and Nodes
void GraphExecutor::InNode::SetWaitingStates(GraphExecutor::InNode * EndNode ) {
  int i ;
  int j ;
  bool docdebug = false ;
  State( GraphExecutor::DataWaitingState ) ;
#if SetWaitingStatesTrace
  cdebug << "SetWaitingStates " << Name() << " " << Automaton()->StateName( State() ) << endl ;
#endif
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GraphBase::InPort * anInPort = GetChangeNodeInPort( i ) ;
#if SetWaitingStatesTrace
    cdebug << "SetWaitingStates InPort " << Name() << "( " << anInPort->PortName() << " ) "
           << anInPort->PortStatus() << " " << anInPort->PortState() << endl ;
#endif
// PAL8513
// JR Debug 07.01.2005 : Close the Gates instead of open !!!
    if ( anInPort->IsGate() ) { // Loop : Close the doors
      GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
      if ( anOutPort ) {
//JR 21.02.2005 Debug Memory leak :        CORBA::Any * anAny = new CORBA::Any() ;
        CORBA::Any anAny = CORBA::Any() ;
//        *anAny <<= (CORBA::Long ) 1 ;
//JR 21.02.2005 Debug Memory leak :        *anAny <<= (CORBA::Long ) 0 ;
        anAny <<= (CORBA::Long ) 0 ;
        anOutPort->SetValue( anAny ) ;
        anInPort->PortState( SUPERV::WaitingState ) ;
//        delete anAny ;
      }
    }
    else if ( anInPort->PortState() != SUPERV::WaitingState &&
              !anInPort->IsDataConnected() ) {
      if ( !docdebug ) {
#if SetWaitingStatesTrace
        cdebug << ThreadNo()
               << " --> GraphExecutor::InNodeThreads::SetWaitingStates " << Name() << endl;
#endif
        docdebug = true ;
      }
      if ( !anInPort->IsDataStream() ) {
        anInPort->PortState( SUPERV::WaitingState ) ;
      }
    }
#if SetWaitingStatesTrace
    cdebug << "               --> InPort " << Name() << "( " << anInPort->PortName() << " ) "
           << anInPort->PortStatus() << " " << anInPort->PortState() << endl ;
#endif
//JR NPAL14110 09.02.2007 : SetWaitingStates was missing in the corresponding SubGraph !...
    if ( IsMacroNode() ) {
      GraphExecutor::DataFlow * aMacroGraph = GraphMacroNode()->CoupledNode()->GraphEditor()->Executor() ;
#if SetWaitingStatesTrace
      cdebug << ThreadNo()
             << " --> GraphExecutor::InNodeThreads::SetWaitingStates MacroNode " << Name()
             << " ---> MacroGraph " << aMacroGraph->Name() << " Inport "
             << anInPort->PortName() << endl;
#endif
      aMacroGraph->SetWaitingStates( anInPort->PortName() ) ;
    }
  }
  for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
    GraphBase::OutPort * anOutPort = GetChangeNodeOutPort( i ) ;
// PAL8514
//JR 07.03.2005 Debug : Reset of Done flag in OutPorts !... :
    if ( !anOutPort->IsDataStream() ) {
      anOutPort->PortDone( false ) ;
      anOutPort->PortState( SUPERV::WaitingState ) ;
    }
    for ( j = 0 ; j < anOutPort->InPortsSize() ; j++ ) {
      if ( !( IsGOTONode() && anOutPort->IsGate() ) &&
           !( IsEndLoopNode() && ( anOutPort->IsGate() || anOutPort->IsLoop() ) ) &&
           !anOutPort->IsDataStream() &&
           !anOutPort->ChangeInPorts( j )->IsDataStream() &&
           !anOutPort->ChangeInPorts( j )->IsExternConnected() ) {
#if SetWaitingStatesTrace
        cdebug << ThreadNo()
               << " InNodeThreads::SetWaitingStates OutPort "
               << Name() << "/" << anOutPort->ChangeInPorts( j )->NodeName() << "( "
               << anOutPort->PortName() << " " << anOutPort->PortStatus() << " ) --> InPort "
               << anOutPort->ChangeInPorts( j )->NodeName() << "( "
               << anOutPort->ChangeInPorts( j )->PortName() << " "
               << anOutPort->ChangeInPorts( j )->PortStatus() << " )" << endl;
#endif
	GraphBase::ComputingNode * aToNode ;
        aToNode = _OutNode->Graph()->GetChangeGraphNode( anOutPort->ChangeInPorts( j )->NodeName() ) ;
// JR 12.01.2005 Debug : the OutPort linked to the InPort of a EndSwitchNode was changed so final
//                       values of InPorts of EndSwitchNode may be wrong
//                       (depending of order of linkednodes)
        if ( !aToNode->IsEndSwitchNode() && 
             strcmp( anOutPort->ChangeInPorts( j )->GetOutPort()->NodeName() , Name() ) ) {
// After EndLoopNode or GOTONode the Input Ports of LoopNode or LabelNode have their values from
// EndLoopNode or GOTONode. But if there is several nested loops we should re-establish.
#if SetWaitingStatesTrace
          cdebug << ThreadNo()
                 << " InNodeThreads::SetWaitingStates Node " << Name() << " " 
                 << anOutPort->ChangeInPorts( j )->GetOutPort()->NodeName() << "( "
                 << anOutPort->ChangeInPorts( j )->GetOutPort()->PortName() << " ) != "
                 << Name() << " : Restored to " << anOutPort->NodeName() << "( "
                 << anOutPort->PortName() << " )" << endl ;
#endif
          anOutPort->ChangeInPorts( j )->ChangeOutPort( anOutPort ) ;
        }
//PAL8624
//JR 21.04.2005 Debug : the OutPort field of InPorts of EndSwitchNodes must be an OutPort
//                      of a SwitchBranch or of a NOTSwitchBranch if a link exist
//                      (if not there is no change)
        else if ( !IsSwitchNode() && aToNode->IsEndSwitchNode() &&
                  strcmp( anOutPort->ChangeInPorts( j )->GetOutPort()->NodeName() , Name() ) ) {
#if SetWaitingStatesTrace
          cdebug << ThreadNo()
                 << " InNodeThreads::SetWaitingStates Node " << Name() << " " 
                 << anOutPort->ChangeInPorts( j )->GetOutPort()->NodeName() << "( "
                 << anOutPort->ChangeInPorts( j )->GetOutPort()->PortName() << " ) != "
                 << Name() << " : Restored to " << anOutPort->NodeName() << "( "
                 << anOutPort->PortName() << " )" << endl ;
#endif
          anOutPort->ChangeInPorts( j )->ChangeOutPort( anOutPort ) ;
	}
        GraphExecutor::InNode * aNode = (GraphExecutor::InNode * ) aToNode->GetInNode() ;
        if ( aNode != EndNode ) {
          aNode->SetWaitingStates( EndNode ) ;
	}
      }
    }
#if SetWaitingStatesTrace
    cdebug << "               --> OutPort " << Name() << "( " << anOutPort->PortName() << " ) "
           << anOutPort->PortStatus() << " " << anOutPort->PortState() << endl ;
#endif
  }
}

#define SuccessActionTrace 1
//JR Step B
int GraphExecutor::InNode::Successed_SuccessAction() {
#if SuccessActionTrace
  cdebug_in << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "  << Name()
            << endl;
#endif
  int res = 1;
//PAL8624
//JR 15.04.2005 Debug RetroConception :
//The behavior of firstzeroNode and firsttoNode is not reliable and must be removed
//The real problem is :
// 1. send "SomeDataReady" event to linked nodes of the current node
// 2. DO NOT send "AllDataReady" event even if all data are ready in SomeDataReady Action
//    but register only that all data are ready :
//    MAJOR ENHANCEMENT OF GRAPHEXECUTOR
// 3. activate AllDataReady Action for each node (except the first one ) which have all
//    its data ready with a creation of a new thread
// 3. activate AllDataReady Action for the first node which have all its data
//    ready in the current thread
//The difficult problem (that I had in the past) was to clearly identify the above behavior ==>
// firstzeroNode, firsttoNode, createnewthreadif, loackdatawait, "would dead lock" etc...
// because if SomeDataReady Action see that all data were ready, it called immediately
// AllDataReady Action ==> bugs difficult to understand and to reproduce
//And the MAJOR DEBUG is (since the first "maquette") : we may have concurrent executions
// of "SomeDataReady" in several threads and there WAS NO MUTEX to protect that
// concurrent actions on the same node
//  int linkednodesnumber = LinkedNodesSize() ;
//  GraphExecutor::InNode *firstzeroNode = NULL ;
  GraphExecutor::InNode *firsttoNode = NULL ;
  GraphExecutor::InNode *toNode ;
  int i ;
  int j ;
  list<GraphExecutor::InNode *> SomeDataNodes ;

  DoneAction() ;

  if ( IsMacroNode() ) {
#if SuccessActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << Name() << " LinkedNodes->SomeDataReady already done for that MacroNode"
             << endl ;
#endif
    return 1;
  }

//JR 09.02.2005 : That complicated part of the code manages LOOPS and GOTO
  if ( IsGOTONode() ||
       ( IsEndLoopNode() && GetNodeInLoop()->GetOutPort()->BoolValue() ) ) {
#if SuccessActionTrace
    cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << Name()
           << " SetWaitingStates " << endl ;
#endif
    const GraphBase::OutPort * aGateOutPort ;
    if ( IsGOTONode() ) {
      aGateOutPort = GetNodeOutGate() ;
    }
    else {
      aGateOutPort = GetNodeOutLoop() ;
    }
    if ( aGateOutPort->InPortsSize() != 1 ) {
      cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction aGateOutPort->InPortsSize "
             << aGateOutPort->InPortsSize() << " != 1 ERROR " << Name() << endl ;
    }
    GraphExecutor::InNode * aLabelNode = NULL ;
    for ( i = 0 ; i < aGateOutPort->InPortsSize() ; i++ ) {
      const GraphBase::InPort * anInPort = aGateOutPort->InPorts( i ) ;
      aLabelNode = (GraphExecutor::InNode *) _OutNode->Graph()->GetChangeGraphNode( anInPort->NodeName() )->GetInNode() ;
#if SuccessActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << Name() << " will Loop to HeadNode "
             << aLabelNode->Name() << " from port " << anInPort->PortName() << endl ;
#endif
      State( GraphExecutor::DataWaitingState ) ;
      aLabelNode->SetWaitingStates( this ) ;
// JR 07.01.2005 Debug : Open the Gate of the coupledNode closed by SetWaitingStates
      GraphBase::OutPort * anOutPort = aLabelNode->GetChangeNodeInGate()->GetOutPort() ;
      if ( anOutPort ) {
//JR 21.02.2005 Debug Memory leak :        CORBA::Any * anAny = new CORBA::Any() ;
        CORBA::Any anAny = CORBA::Any() ;
//JR 21.02.2005 Debug Memory leak :        *anAny <<= (CORBA::Long ) 1 ;
        anAny <<= (CORBA::Long ) 1 ;
        anOutPort->SetValue( anAny ) ;
        aLabelNode->GetChangeNodeInGate()->PortState( SUPERV::ReadyState ) ;
//        delete anAny ;
      }
      for ( j = 0 ; j < aLabelNode->GetNodeInPortsSize() ; j++ ) {
        const GraphBase::InPort * anInPort = aLabelNode->GetNodeInPort( j ) ;
        if ( anInPort->GetOutPort() ) {
#if SuccessActionTrace
          cdebug << aLabelNode->Name() << "(" << anInPort->PortName() << ") value : "
                 << anInPort->GetOutPort()->NodeName() << "(" << anInPort->GetOutPort()->PortName() << ")"
                 << endl ;
#endif
	}
      }
//PAL8176 ==> PAL8516
//JR 24.03.2005 Debug : the number of OutPorts of a GOTONode and of InPorts of its linked
//                      InLine node must be the same
      if ( GetNodeOutPortsSize() != aLabelNode-> GetNodeInPortsSize() ) {
        cdebug << pthread_self() << "/" << ThreadNo()
               << " Successed_SuccessAction # number of ports " << GetNodeOutPortsSize()
               << " != " << aLabelNode-> GetNodeInPortsSize() << endl ;
        SendEvent( GraphExecutor::ErrorEvent ) ;
        return 0 ;
      }
      else {
        for ( j = 0 ; j < GetNodeOutPortsSize() ; j++ ) {
          GraphBase::OutPort * aBusParamOutPort = GetChangeNodeOutPort( j ) ;
          if ( !aBusParamOutPort->IsGate() ) {
            GraphBase::InPort * aBusParamChangeInPort = NULL ;
            if ( aBusParamOutPort->IsLoop() ) {
// For EndLoop do not copy EndLoop(DoLoop) in Loop(InLoop)
//            aBusParamChangeInPort = aLabelNode->GetChangeNodeInLoop() ;
    	    }
            else {
              aBusParamChangeInPort = aLabelNode->GetChangeInPort( aBusParamOutPort->PortName() ) ;
  	    }
            if ( aBusParamChangeInPort ) {
              aBusParamChangeInPort->ChangeOutPort( aBusParamOutPort ) ;
#if SuccessActionTrace
              cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << Name() << " ChangeOutPort to HeadNode "
                     << aLabelNode->Name() << "(" << aBusParamChangeInPort->PortName() << ") from port "
                     << aBusParamOutPort->PortName() << endl ;
#endif
	    }
            else if ( IsGOTONode() ) {
//PAL8176 ==> PAL8516
//JR 24.03.2005 Debug : the names of OutPorts of a GOTONode and of InPorts of its linked
//                      InLine node must be the same
              cdebug << pthread_self() << "/" << ThreadNo()
                     << " Successed_SuccessAction # names of ports "
                     << aBusParamOutPort->PortName() << endl ;
              SendEvent( GraphExecutor::ErrorEvent ) ;
              return 0 ;
	    }
	  }
	}
      }
    }

//JR 15.04.2005 Debug PAL8624 RetroConception :
// THERE IS ONLY ONE NODE COUPLED TO A GOTONODE OR AN ENDLOOPNODE BUT Mutex/Lock for consistency
    if ( aLabelNode ) {
      const GraphBase::InPort * aGateInPort = aLabelNode->GetNodeInGate() ;
      if ( aGateInPort ) {
        if ( aGateInPort->GetOutPort() ) {
//JR 21.02.2005 Debug Memory leak :          aGateInPort->GetOutPort()->Value( aGateOutPort->Value() ) ;
//JR 30.03.2005          aGateInPort->GetOutPort()->Value( *aGateOutPort->Value() ) ;
          aGateInPort->GetOutPort()->SetValue( aGateOutPort->Value() ) ;
	}
//JR 15.04.2005 Debug PAL8624 RetroConception :
        if ( !aLabelNode->SendSomeDataReady( Name() ) ) {
          cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "
                 << Name() << " SendSomeDataReady to " << aLabelNode->Name() << " ERROR"
                 << endl ;
//JR 30.06.2005 :
          SendEvent( GraphExecutor::ErrorEvent ) ;
          return 0 ;
	}
        if ( aLabelNode->HasAllDataReady() ) {
          aLabelNode->ThreadNo( pthread_self() ) ;
          aLabelNode->CreateNewThread( false ) ;
          aLabelNode->RewindStack( RewindStack() ) ;
          aLabelNode->HasAllDataReady( false ) ;
          res = aLabelNode->SendEvent( GraphExecutor::AllDataReadyEvent ); // ==> Ready to execute
	}
      }
      else {
        cdebug << pthread_self() << "/" << ThreadNo() << " ERROR in Successed_SuccessAction of " << Name()
               << " NO port " << aGateOutPort->PortName() << " in "
               << aLabelNode->Name() << endl;
      }
    }
  }

//JR Step B
//==================================================
// JR 09.02.2005 : this is not a EndLoop or a GOTO :
//==================================================
  else { // Not a EndLoop or a GOTO
#if SuccessActionTrace
    cdebug << ThreadNo() << " Successed_SuccessAction of " << Name()
           << " with " << LinkedNodesSize() << " linked nodes :" ;
    for ( i = 0 ; i < LinkedNodesSize() ; i++ ) {
      cdebug << " " << LinkedNodes( i )->Name() ;
    }
    cdebug << endl;
#endif
//JR 15.04.2005 Debug PAL8624 RetroConception :
// If this is a LoopNode and if DoLoopPort == false, we go directly to the EndOfLoopNode and
// we do not activate Nodes within the loop
    bool IgnoreForEndLoop = false ;
// If this is a SwitchNode and if DefaultOutPort == true, we do not activate Nodes within Switch
// We activate directly the EnSwitch
// BUT the NotSwitchBranch(es) are NOT activated :
    bool IgnoreForDefaultSwitch = false ;
    if ( IsLoopNode() ) {
      GraphBase::OutPort * fromLoopOutPort = GetChangeNodeOutLoop() ;
      if ( !fromLoopOutPort->BoolValue() ) { // Ne pas faire la boucle
        IgnoreForEndLoop = true ;
      }
    }
    else if ( IsSwitchNode() ) {
      const GraphBase::OutPort * anOutGatePort = GetNodeOutGate() ;
      if ( anOutGatePort->BoolValue() && anOutGatePort->InPortsSize() ) { // DefaultPort is activated
// The DefaultOutPort of that SwitchNode is true and is connected
        IgnoreForDefaultSwitch = true ;
      }
    }

//Loop of LinkedNodes for SendSomeDataReady :
    for ( i = 0 ; i < LinkedNodesSize() ; i++ ) {
      GraphBase::ComputingNode * aComputingNode ;
      aComputingNode = (GraphBase::ComputingNode * ) LinkedNodes( i ) ;
      toNode = (GraphExecutor::InNode *) aComputingNode->GetInNode() ;
#if SuccessActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction of " << Name()
             << " [" << i << "] " << LinkedNodes( i )->Name() << " toNode " << toNode
             << " IgnoreForEndLoop " << IgnoreForEndLoop ;
      if ( toNode ) {
        cdebug << " " << toNode->Kind() << endl ;
      }
#endif
//JR 15.04.2005 Debug PAL8624 RetroConception :
      if ( toNode ) {
//JR 18.05.2005 : we must lock because of concurrent SendEvent if NotAllDataReady :
        toNode->LockDataReady() ;
        if ( IsComputingNode() && toNode->IsInLineNode() ) {
          GraphBase::InPort * toGateInPort = toNode->GetChangeNodeInGate() ;
          toGateInPort->PortState( SUPERV::ReadyState ) ;
          GraphBase::OutPort * GateOutPort = toGateInPort->GetOutPort() ;
          if ( GateOutPort ) {
            GateOutPort->PortStatus( DataConnected );
            GateOutPort->PortState( SUPERV::ReadyState ) ;
            GateOutPort->PortDone( true ) ;
          }
        }

//JR 15.04.2005 Debug PAL8624 RetroConception :
        if ( IsLoopNode() ) {
          if ( IgnoreForEndLoop && !strcmp( toNode->Name() , CoupledNode()->Name() ) ) {
            GraphBase::InPort * toLoopInPort ;
            toLoopInPort = toNode->GetChangeNodeInLoop() ;
            if ( toLoopInPort->PortState() != SUPERV::ReadyState ) {
              toLoopInPort->PortState( SUPERV::ReadyState ) ;
	    }
	  }
        }
//JR 15.04.2005 Debug PAL8624 RetroConception :
        else if ( toNode->IsInLineNode() ) {
          int j ;
          for ( j = 0 ; j < toNode->GetNodeInPortsSize() ; j++ ) {
            toNode->GetChangeNodeInPort( j )->InitialOutPort() ;
	  }
        }

        bool activatetoNode = true ;
//We have to execute the loop :
        if ( !IgnoreForEndLoop ) {
//The loop is not finished :
          if (  toNode->IsLoopNode() ) {
//We enter in a new loop :
            GraphBase::InPort * toLoopInPort = toNode->GetChangeNodeInLoop() ;
            toLoopInPort->PortState( SUPERV::ReadyState ) ;
            GraphBase::OutPort * LoopOutPort = toLoopInPort->GetOutPort() ;
            LoopOutPort->PortStatus( DataConnected );
            LoopOutPort->PortState( SUPERV::ReadyState ) ;
            LoopOutPort->PortDone( true ) ;
//JR 21.02.2005 Debug Memory leak :          CORBA::Any * anAny = new CORBA::Any() ; // InitLoop
            CORBA::Any anAny = CORBA::Any() ; // InitLoop
//JR 21.02.2005 Debug Memory leak :          *anAny <<= (CORBA::Long ) 1 ;
            anAny <<= (CORBA::Long ) 1 ;
            LoopOutPort->SetValue( anAny ) ;
            int j ;
            for ( j = 0 ; j < toNode->GetNodeInPortsSize() ; j++ ) {
              toNode->GetChangeNodeInPort( j )->InitialOutPort() ;
	    }
          }
	}
//The loop is finished :
        else if ( IsLoopNode() ) {
          if ( toNode->IsEndLoopNode() ) {
//Not the corresponding EndLoopNode :
            if ( strcmp( toNode->Name() , CoupledNode()->Name() ) ) {
#if SuccessActionTrace
              cdebug << pthread_self() << "/" << ThreadNo()
                     << " Successed_SuccessAction NO activate EndLoopNode " << toNode->Name()
                     << endl ;
#endif
              activatetoNode = false ;
	    }
	  }
//Not a EndLoopNode :
          else {
#if SuccessActionTrace
            cdebug << pthread_self() << "/" << ThreadNo()
                   << " Successed_SuccessAction NO activate node " << toNode->Name() << endl ;
#endif
            activatetoNode = false ;
	  }
	}

// If the DefaultPort of that SwitchNode is connected to the DefaultPort of the EndSwitchNode
// the NotSwitchBranch(es) are NOT activated :
        if ( IgnoreForDefaultSwitch ) {
//We have to activate Default to EndSwitchNode
#if SuccessActionTrace
          cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction search "
                 << toNode->Name() << " among linked nodes to DefaultPort of " << Name()
                 << "IgnoreForDefaultSwitch" << IgnoreForDefaultSwitch << endl ;
#endif
          activatetoNode = false ;
          const GraphBase::OutPort * anOutGatePort = GetNodeOutGate() ;
          const GraphBase::InPort * anInPort = NULL ;
          int j ;
          for ( j = 0 ; j < anOutGatePort->InPortsSize() ; j++ ) {
            anInPort = anOutGatePort->InPorts( j ) ;
            const GraphBase::ComputingNode * aNode ;
            aNode = _OutNode->Graph()->GetGraphNode( anInPort->NodeName() ) ;
            if ( aNode ) {
#if SuccessActionTrace
              cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction try "
                     << aNode << " " << aNode->Name() << " == " << toNode << " " << toNode->Name()
                     << endl ;
#endif
	    }
            if ( aNode && (const GraphBase::ComputingNode * ) toNode->ComputingNode() == aNode ) {
// toNode is connected to the DefaultPort of that SwitchNode :
#if SuccessActionTrace
              cdebug << pthread_self() << "/" << ThreadNo()
                     << " Successed_SuccessAction activate node " << aNode->Name() << endl ;
#endif
              activatetoNode = true ;
              break ;
	    }
            else {
#if SuccessActionTrace
              cdebug << pthread_self() << "/" << ThreadNo()
                     << " Successed_SuccessAction NO activate node " << aNode->Name() << endl ;
#endif
	    }
	  }
	}
//JR 19.04.2005 Debug : Do not activate the EndSwitchNode if DefaultGate is close.
//JR 20.04.2005 : it is false : an outport of the SwitchNode may be connected to an
//                              input port of the EndSwitchNode
        if ( activatetoNode ) {
#if SuccessActionTrace
          cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "
                 << toNode->Name() << "->SendSomeDataReady( " << Name() << " )" << endl ;
#endif
          if ( !toNode->SendSomeDataReady( Name() ) ) {
            cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "
                 << Name() << " SendSomeDataReady to " << toNode->Name() << " ERROR"
                 << endl ;
//JR 30.06.2005 :
            toNode->UnLockDataReady() ;
            SendEvent( GraphExecutor::ErrorEvent ) ;
            return 0 ;
	  }
	}
        toNode->UnLockDataReady() ;
      }
    } //End of Loop of LinkedNodes for SendSomeDataReady

//JR 10.02.2005 : Debug at the end of execution of a SwitchNode :
// Here after we may start execution of only one SwitchBranch or of the Default
// But with activation of only one SwitchBranch we may activate several nodes of that SwitchBranch and
// we may activate several nodes of NotSwitchBranch ( a NotSwitchBranch is a Branch of the Switch
// where GatePorts of Nodes are not connected ; that Branches are always executed for each of SwitchBranch
// BUT are not executed when Default is activated).
    if ( IsSwitchNode() ) {
      GraphBase::InLineNode * anEndSwitchNode = GOTONode()->CoupledNode() ;
//The InPorts of the EndSwitchNode may be connected from that SwitchNode
//So at first, if we activate a SwitchBranch, we have to establish the correct OutPort in the InPorts
// of the EndSwitchNode (for the SwitchBranch and the NOTSwitchBranch[es] :
//PAL8517
// So the bug is that all input ports of the corresponding EndSwitchNode must have the status NOTDONE !
// (Only if Default OutPort is closed and Default InPort is closed) :
      if ( !GetNodeOutGate()->BoolValue() && anEndSwitchNode->GetNodeInGate()->GetOutPort() &&
           !anEndSwitchNode->GetNodeInGate()->GetOutPort()->BoolValue() ) {
#if SuccessActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << anEndSwitchNode->Name()
               << " reset of InPort()->OutPort()->Done flag in EndSwitch" << endl ;
#endif
        int i ;
        for ( i = 0 ; i < anEndSwitchNode->GetNodeInPortsSize() ; i++ ) {
          GraphBase::OutPort * anOutPort = anEndSwitchNode->GetChangeNodeInPort( i )->GetOutPort() ;
//PAL8519
//JR 08.03.2005 Debug : update of state only if not a StreamPort
          if ( anOutPort && strcmp( anOutPort->NodeName() , Name() ) &&
               !anOutPort->IsDataStream() ) {
#if SuccessActionTrace
            cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "
                   << anEndSwitchNode->Name() << " InPort "
                   << anEndSwitchNode->GetChangeNodeInPort( i )->PortName() << " NOTDONE from "
                   << anOutPort->NodeName() << " " << anOutPort->PortName() << endl ;
#endif
            anEndSwitchNode->GetChangeNodeInPort( i )->PortState( SUPERV::WaitingState ) ;
            anEndSwitchNode->GetChangeNodeInPort( i )->GetOutPort()->PortDone( false ) ;
          }
          else {
#if SuccessActionTrace
            cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "
                   << anEndSwitchNode->Name() << " InPort "
                   << anEndSwitchNode->GetChangeNodeInPort( i )->PortName() << " NOT Changed : directly from "
                   << anOutPort->NodeName() << " " << anOutPort->PortName() << endl ;
#endif
	  }
        }
      }
      else {
#if SuccessActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << Name()
               << " " << Kind() << " OutGate->Value " << GetNodeOutGate()->BoolValue()
               << " NO reset of InPort()->OutPort()->Done flag in EndSwitch" << endl ;
#endif
      }
    }

//JR 15.04.2005 Debug PAL8624 RetroConception :
//Make the list of nodes to activate :
    for ( i = 0 ; i < LinkedNodesSize() ; i++ ) {
      GraphBase::ComputingNode * aComputingNode ;
      aComputingNode = (GraphBase::ComputingNode * ) LinkedNodes( i ) ;
      toNode = (GraphExecutor::InNode *) aComputingNode->GetInNode() ;
      if ( toNode ) { // Only Not DataFlowNode :
        toNode->LockDataReady() ; // Only ONE Node may send AllDataReadyEvent to an other node
        if ( toNode->HasAllDataReady() ) {
          SomeDataNodes.push_back( toNode ) ;
          toNode->HasAllDataReady( false ) ;
#if SuccessActionTrace
          cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction "
                 << Name() << " push "
                 << toNode->Name() << " " << SomeDataNodes.size() << endl ;
#endif
	}
        toNode->UnLockDataReady() ;
      }
    }

// firsttoNode : node that has the same ThreadNo() as the current node and that have to be activated
//JR 15.04.2005 Debug PAL8624 RetroConception :
//Activation of nodes that have AllDataReady in a new thread except one :
    while ( SomeDataNodes.size() > 1 ) {
      GraphExecutor::InNode *aNode = SomeDataNodes.front() ;
      SomeDataNodes.pop_front() ;
#if SuccessActionTrace
      cdebug << pthread_self() << "/" << ThreadNo()
             << " Successed_SuccessAction pop size "
             << SomeDataNodes.size() << " " << aNode->Name() << endl ;
      cdebug << pthread_self() << "/" << ThreadNo() << " " << aNode->Name()
             << " Successed_SuccessAction poped and will start in a new thread" << endl ;
#endif
      aNode->CreateNewThread( true ) ;
      _OutNode->IncrCreatedThreads() ;
//JR 15.04.2005 Debug PAL8624 RetroConception :
      res = aNode->SendEvent( GraphExecutor::AllDataReadyEvent ); // ==> Ready to execute
    }

//Activation of the last node that have AllDataReady in the same thread :
    if ( SomeDataNodes.size() ) {
      firsttoNode = SomeDataNodes.front() ;
      SomeDataNodes.pop_front() ;
    }
    if ( firsttoNode ) {
#if SuccessActionTrace
      cdebug << pthread_self() << "/" << ThreadNo()
             << " Successed_SuccessAction start firsttoNode "
             << SomeDataNodes.size() << " " << firsttoNode->Name() << endl ;
#endif
//      firsttoNode->CreateNewThreadIf( false ) ;
      firsttoNode->CreateNewThread( false ) ;
      firsttoNode->RewindStack( RewindStack() ) ;
      if ( firsttoNode->State() == GraphExecutor::SuccessedState ) {
#if SuccessActionTrace
        cdebug << pthread_self() << "/" << ThreadNo() << " " << Name()
               << " : " << firsttoNode->Name() << " "
               << Automaton()->StateName( firsttoNode->State() )
               << " --> DataWaitingState for Thread "
               << firsttoNode->ThreadNo() << endl ;
#endif
        firsttoNode->State( GraphExecutor::DataWaitingState ) ;
      }
      firsttoNode->ThreadNo( pthread_self() ) ;
// On continue avec le meme thread
      ThreadNo( 0 ) ;
#if SuccessActionTrace
      cdebug << pthread_self() << "/" << ThreadNo() << " Successed_SuccessAction " << Name()
             << " for firsttoNode " << firsttoNode->Name()
             << " " << Automaton()->StateName( firsttoNode->State() ) << endl ;
#endif
//JR 15.04.2005 Debug PAL8624 RetroConception :
//No creation of thread and with LockDataReady, an other node in an other thread cannot be
// waiting for that lock ( if it was the case we could not find AllDataReady for firsttoNode
      res = firsttoNode->SendEvent( GraphExecutor::AllDataReadyEvent ); // ==> Ready to execute
    }
    else {
#if SuccessActionTrace
      cdebug << ThreadNo() << " Successed_SuccessAction " << Name()
             << " NO DataReady ==> ThreadNo( 0 ) firsttoNode == NULL" << endl ;
#endif
      ThreadNo( 0 ) ;
    }
  }
#if SuccessActionTrace
  cdebug_out << pthread_self() << "/" << ThreadNo()
             << " Successed_SuccessAction " << Name() << endl;
#endif
  return 1 ;
}

#define SendSomeDataReadyTrace 1
bool GraphExecutor::InNode::SendSomeDataReady( char * FromNodeName ) {
  bool RetVal = false ;
  if ( IsDataFlowNode() ) {
#if SendSomeDataReadyTrace
    cdebug_in << ThreadNo() << "InNode::SendSomeDataReady " << FromNodeName
              << " send Result to graph " << Name() << endl;
#endif
  }
  else {
#if SendSomeDataReadyTrace
    cdebug_in << pthread_self() << "/" << ThreadNo() << FromNodeName
              << " GraphExecutor::InNode::SendSomeDataReady to " << Name() << " State "
              << Automaton()->StateName( State() ) << endl;
#endif
    if ( State() == GraphExecutor::SuccessedState ||
         State() == GraphExecutor::SuspendedSuccessedState ||
         State() == GraphExecutor::SuspendedSuccessedToReStartState ) {
#if SendSomeDataReadyTrace
      cdebug << ThreadNo() << " " << FromNodeName
             << " : " << Name() << " " << Automaton()->StateName( State() )
             << " --> DataWaitingState for Thread "
             << ThreadNo() << " " << endl ;
#endif
      State( GraphExecutor::DataWaitingState ) ;
    }
// We begin that LoopNode if SendSomeDataReady does not come from the corresponding EndLoopNode
    if ( IsLoopNode() && strcmp( LoopNode()->CoupledNodeName() , FromNodeName ) ) {
#if SendSomeDataReadyTrace
      cdebug << ThreadNo() << "InNode::SendSomeDataReady " << Name() << " Set _InitLoop from "
             << FromNodeName << endl ;
#endif
      _InitLoop = true ;
    }
#if SendSomeDataReadyTrace
    cdebug << "SendEvent( SomeDataReadyEvent )" << endl ;
#endif
//JR 15.04.2005 Debug PAL8624 RetroConception :
    DataFromNode( FromNodeName ) ;
//    RetVal = !SendEvent( GraphExecutor::SomeDataReadyEvent );
    RetVal = SendEvent( GraphExecutor::SomeDataReadyEvent );
//JR 15.04.2005 Debug PAL8624 RetroConception :
  }
#if SendSomeDataReadyTrace
  cdebug_out << pthread_self() << "/" << ThreadNo() << FromNodeName
             << " GraphExecutor::InNode::SendSomeDataReady to " << Name() << " State "
             << Automaton()->StateName( State() ) << " " << RetVal << endl;
#endif
  return RetVal ;
}

int GraphExecutor::InNode::Errored_ErrorAction() {
  cdebug << ThreadNo() << " Errored_ErrorAction " << Name()
         << " will pthread_exit" << endl;

  _OutNode->NodeAborted( Name() ) ;

  DoneAction() ;
  return 1 ;
}

int GraphExecutor::InNode::Successed_SuspendAction() {
  cdebug << ThreadNo() << " Successed_SuspendAction -->Suspend " << Name()
         << " Threads " << _OutNode->Threads() << " SuspendedThreads "
         << _OutNode->SuspendedThreads() << endl;
  _OutNode->PushEvent( this , GraphExecutor::SuspendedSuccessedEvent ,
                       GraphExecutor::SuspendedSuccessedState ) ; 
  DoneAction() ;
  GraphExecutor::InNode * aReStartNode = SuspendAction() ;
  cdebug << ThreadNo() << " Successed_SuspendAction Resumed " << Name() ;
  if ( aReStartNode ) {
    _aReStartNode = NULL ;
    cdebug << " for " << aReStartNode->Name() << endl;
    aReStartNode->SendEvent( _aReStartEvent ) ;
  }
  else {
    cdebug << endl;
    SendEvent( GraphExecutor::ResumeEvent ) ;
  }
  return 1 ;
}

int GraphExecutor::InNode::Errored_SuspendAction() {
  cdebug << ThreadNo() << " Errored_SuspendAction -->Suspend " << Name()
         << " Threads " << _OutNode->Threads() << " SuspendedThreads "
         << _OutNode->SuspendedThreads() << endl;
  _OutNode->PushEvent( this , GraphExecutor::SuspendedErroredEvent ,
                       GraphExecutor::SuspendedErroredState ) ; 

  _OutNode->NodeAborted( Name() ) ;

  DoneAction() ;
  GraphExecutor::InNode * aReStartNode = SuspendAction() ;
  cdebug << ThreadNo() << " Errored_SuspendAction Resumed " << Name()
         << endl;
  if ( aReStartNode ) {
    _aReStartNode = NULL ;
    aReStartNode->SendEvent( _aReStartEvent ) ;
  }
  else {
    SendEvent( GraphExecutor::ResumeEvent ) ;
  }
  return 1 ;
}

int GraphExecutor::InNode::SuspendedSuccessed_ResumeAction() {
  cdebug << ThreadNo() << " SuspendedSuccessed_ResumeAction " << Name() << endl;
//  ResumeAction() ;
  _OutNode->PushEvent( this , GraphExecutor::ResumedSuccessedEvent ,
                       GraphExecutor::ResumedSuccessedState ) ; 
  SendEvent( ResumedSuccessedEvent ) ;
  return 1 ;
}

int GraphExecutor::InNode::SuspendedErrored_ResumeAction() {
  cdebug << ThreadNo() << " SuspendedErrored_ResumeAction " << Name() << endl;
//  ResumeAction() ;
  _OutNode->PushEvent( this , GraphExecutor::ResumedErroredEvent ,
                       GraphExecutor::ResumedErroredState ) ; 

  _OutNode->NodeAborted( Name() ) ;

  SendEvent( ResumedErroredEvent ) ;
  return 1 ;
}

int GraphExecutor::InNode::Successed_KillAction() {
  KillAction() ;
  _OutNode->PushEvent( this , GraphExecutor::KilledEvent ,
                       GraphExecutor::KilledSuccessedState ) ; 
  cdebug << ThreadNo() << " Successed_KillAction " << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::Errored_KillAction() {
  KillAction() ;
  _OutNode->PushEvent( this , GraphExecutor::KilledEvent ,
                       GraphExecutor::KilledErroredState ) ; 

  _OutNode->NodeAborted( Name() ) ;

  cdebug << ThreadNo() << " Errored_KillAction " << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::Successed_StopAction() {
  StopAction() ;
  _OutNode->PushEvent( this , GraphExecutor::StoppedEvent ,
                       GraphExecutor::StoppedSuccessedState ) ; 
  cdebug << ThreadNo() << " Successed_StopAction " << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::Errored_StopAction() {
  StopAction() ;
  _OutNode->PushEvent( this , GraphExecutor::StoppedEvent ,
                       GraphExecutor::StoppedErroredState ) ; 

  _OutNode->NodeAborted( Name() ) ;

  cdebug << ThreadNo() << " Errored_StopAction " << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::SuspendedSuccessed_ReStartAction() {
  cdebug << ThreadNo() << " SuspendedSuccessed_ReStartAction " << Name() << endl;
  _OutNode->PushEvent( this , GraphExecutor::ReStartedEvent ,
                       GraphExecutor::ReStartedState ) ;
  int i ;
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GetChangeNodeInPort( i )->PortState( SUPERV::ReadyState ) ;
  }
  SendEvent( ExecuteEvent ) ;
  cdebug << ThreadNo() << " SuspendedSuccessed_ReStartAction "  << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::SuspendedErrored_ReStartAction() {
  cdebug << ThreadNo() << " SuspendedErrored_ReStartAction " << Name() << endl;
  _OutNode->PushEvent( this , GraphExecutor::ReStartedEvent ,
                       GraphExecutor::ReStartedState ) ; 
  int i ;
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GetChangeNodeInPort( i )->PortState( SUPERV::ReadyState ) ;
  }
  SendEvent( ExecuteEvent ) ;
  cdebug << ThreadNo() << " SuspendedErrored_ReStartAction "  << Name() << endl;
  return 1 ;
}

int GraphExecutor::InNode::SuspendedSuccessed_ReStartAndSuspendAction() {
  cdebug << ThreadNo() << " SuspendedSuccessed_ReStartAndSuspendAction " << Name()
         << endl;
  _OutNode->PushEvent( this , GraphExecutor::ReStartedAndSuspendEvent ,
                       GraphExecutor::ReStartedState ) ; 
  State( GraphExecutor::DataWaitingState ) ;
  if ( !Suspend() ) {
    cdebug << "InNode::Suspend() Node " << Name() << endl ;
    return false ;
  }
  else if ( SendEvent( GraphExecutor::SomeDataReadyEvent ) ) {
    cdebug << "InNode::SendEvent( SomeDataReadyEvent ) Node "
           << Name() << endl ;
    return false ;
  }
  cdebug << ThreadNo() << " SuspendedSuccessed_ReStartAndSuspendAction "  << Name()
         << endl;
  return 1 ;
}

int GraphExecutor::InNode::SuspendedErrored_ReStartAndSuspendAction() {
  cdebug << ThreadNo() << " SuspendedErrored_ReStartAndSuspendAction " << Name()
         << endl;
  _OutNode->PushEvent( this , GraphExecutor::ReStartedAndSuspendEvent ,
                       GraphExecutor::ReStartedState ) ; 
  State( GraphExecutor::DataWaitingState ) ;
  if ( !Suspend() ) {
    cdebug << "InNode::Suspend() Node " << Name() << endl ;
    return false ;
  }
  else if ( SendEvent( GraphExecutor::SomeDataReadyEvent ) ) {
    cdebug << "InNode::SendEvent( SomeDataReadyEvent ) Node "
           << Name() << endl ;
    return false ;
  }
  cdebug << ThreadNo() << " SuspendedErrored_ReStartAndSuspendAction "  << Name()
         << endl;
  return 1 ;
}

#define InParametersSetTrace 1
void GraphExecutor::InNode::InParametersSet( bool & Err ,
                                             int  nInParams ,
                                             ServicesAnyData * InParametersList ) {
  int i ;
#if InParametersSetTrace
  cdebug << pthread_self() << "/" << ThreadNo() << " InParametersSet " << Name() << endl ;
#endif
  for ( i = 0 ; i < nInParams ; i++ ) {
    ServicesAnyData D = InParametersList[i];
    GraphBase::InPort * anInPort = GetChangeNodeInPort(i) ;
    GraphBase::OutPort * theOutPort = anInPort->GetOutPort() ;
    if ( anInPort->IsGate() && theOutPort == NULL ) {
#if InParametersSetTrace
      cdebug << ThreadNo() << " ArgIn" << i << " " << D.Name << " "
             << anInPort->GetServicesParameter().Parametertype
             << " is inactive. " << anInPort->Kind() << endl ;
#endif
    }
    else if ( anInPort->PortState() == SUPERV::ReadyState ) {
      if ( anInPort->IsGate() ) {
//JR 21.02.2005 Debug Memory leak :        CORBA::Any * anAny = new CORBA::Any() ;
        CORBA::Any anAny = CORBA::Any() ;
//JR 21.02.2005 Debug Memory leak :        *anAny <<= (CORBA::Long ) 0 ;
        anAny <<= (CORBA::Long ) 0 ;
        theOutPort->SetValue( anAny ) ;
//        delete anAny ;
      }
      if ( !anInPort->IsDataStream() 
	   && 
           !anInPort->IsDataConnected() ) {
          anInPort->PortState( SUPERV::WaitingState ) ;
      }
//JR 18.02.2005 Debug Memory leak : delete does not destroy that string ...
//      D.Name = CORBA::string_dup( anInPort->GetServicesParameter().Parametername ) ;
      D.Name = anInPort->PortName() ;
//JR 30.03.2005      const CORBA::Any * AnyPtr = theOutPort->Value() ;
      const CORBA::Any AnyRef = theOutPort->Value() ;
#if InParametersSetTrace
      cdebug << ThreadNo() << " ArgIn" << i << " " << anInPort->Kind() << " "
             << anInPort->PortState() << " " << D.Name << " "
             << anInPort->GetServicesParameter().Parametertype << endl ;
#endif
//JR 30.03.2005      D.Value = * AnyPtr ; // CORBA::Any
      D.Value = AnyRef ; // CORBA::Any
//JR 18.02.2005 Debug Memory leak :       string _Type = CORBA::string_dup( anInPort->GetServicesParameter().Parametertype ) ;
//      const char * Type = _Type.c_str() ;
      const char * Type = anInPort->GetServicesParameter().Parametertype ;
      switch ( D.Value.type()->kind() ) { // { string , long , double , objref }
      case CORBA::tk_string:
        const char * t;
        D.Value >>= t;
#if InParametersSetTrace
        cdebug << t << " (string)" ;
#endif
        if ( !strcmp( Type , "string" ) ) {
        }
        else if ( !strcmp( Type , "boolean" ) ) {
          bool b ;
          long d ;
          sscanf( t , "%ld" , &d ) ;
          b = (bool ) d ;
          D.Value <<=  (CORBA::Any::from_boolean ) b ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "char" ) ) {
          unsigned char c ;
          long d ;
          sscanf( t , "%ld" , &d ) ;
          c = (short ) d ;
          D.Value <<=  (CORBA::Any::from_char ) c ;
#if InParametersSetTrace
          cdebug << "string '" << t << "' --> " << d << " --> char " << c ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "short" ) ) {
          short s ;
          long d ;
          sscanf( t , "%ld" , &d ) ;
          s = (short ) d ;
          D.Value <<=  s ;
#if InParametersSetTrace
          cdebug << "string '" << t << "' --> " << d << " --> short " << s ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
          long ll ;
          sscanf( t , "%ld" , &ll ) ;
          CORBA::Long l = ll ;
          D.Value <<=  l ;
#if InParametersSetTrace
          cdebug << "string '" << t << " --> CORBA::Long " << l ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "float" ) ) {
          double d ;
          sscanf( t , "%lf" , &d ) ;
          float f = d ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= f ;
#else
//JR	  D.Value.replace(CORBA::TypeCode::PR_float_tc(), (void*)(&f));
          D.Value <<= (CORBA::Float) f ;
#endif
#if InParametersSetTrace
          cdebug << "string '" << t << "' --> " << setw(25) << setprecision(18) << d << " --> float " << " = "
                 << setw(25) << setprecision(18) << f ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "double" ) ) {
          double d ;
          sscanf( t , "%lf" , &d ) ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= d ;
#else
//JR	  D.Value.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
          D.Value <<= (CORBA::Double) d ;
#endif
#if InParametersSetTrace
          cdebug << "string '" << t << " --> double " << setw(25) << setprecision(18) << d ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
//        else if ( !strcmp( Type , "objref" ) ) {
        else { // Default
          CORBA::Object_ptr ObjRef ;
          try {
            ObjRef = StringToObject( t ) ;
            D.Value <<= ObjRef ;
	  }
          catch( ... ) {
            D.Value <<= CORBA::Object::_nil() ;
	  }
//          theOutPort->Value( D.Value ) ;
        }
//        else {
//          cdebug << " (other ERROR)" << endl ;
//        }
#if InParametersSetTrace
        cdebug << " --> call_kind " << D.Value.type()->kind() << endl ;
#endif
        break;
      case CORBA::tk_long:
#if InParametersSetTrace
        cdebug << ThreadNo() << " " << Name() << " ArgIn" << i << " " << D.Name << " "
               << anInPort->GetServicesParameter().Parametertype << " " << anInPort->Kind()
               << " " ;
        theOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
        cdebug << endl ;
#endif
        CORBA::Long l;
        D.Value >>= l;
#if InParametersSetTrace
        cdebug << l << " (CORBA::Long)" << endl ;
#endif
        if ( !strcmp( Type , "string" ) ) {
          char t[40] ;
          sprintf( t , "%ld" , (long)l ) ;
          D.Value <<= t ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "boolean" ) ) {
          bool b ;
          b = (bool ) l ;
          D.Value <<=  (CORBA::Any::from_boolean ) b ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "char" ) ) {
          unsigned char c ;
          c = (unsigned char ) l ;
          D.Value <<=  (CORBA::Any::from_char ) c ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "short" ) ) {
          short s ;
          s = (short ) l ;
          D.Value <<=  s ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
        }
        else if ( !strcmp( Type , "float" ) ) {
          float f ;
          f = (float ) l ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= f ;
#else
//JR	  D.Value.replace(CORBA::TypeCode::PR_float_tc(), (void*)(&f));
          D.Value <<= (CORBA::Float) f ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "double" ) ) {
          double d ;
          d = (double ) l ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= d ;
#else
//JR      D.Value.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
          D.Value <<= (CORBA::Double) d ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
//        else if ( !strcmp( Type , "objref" ) ) {
        else { // Default
          D.Value <<= CORBA::Object::_nil() ;
//          theOutPort->Value( D.Value ) ;
        }
//        else {
//          cdebug << " (other ERROR)" << endl ;
//        }
#if InParametersSetTrace
        cdebug << " --> call_kind " << D.Value.type()->kind() << endl ;
#endif
        break;
      case CORBA::tk_double:
        CORBA::Double d;
        D.Value >>= d;
#if InParametersSetTrace
        cdebug << d << " (double)" << endl ;
#endif
        if ( !strcmp( Type , "string" ) ) {
          char t[40] ;
          sprintf( t , "%lf" , d ) ;
          D.Value <<= t ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "boolean" ) ) {
          bool b ;
          b = (bool ) d ;
          D.Value <<=  (CORBA::Any::from_boolean ) b ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "char" ) ) {
          unsigned char c ;
          c = (unsigned char ) d ;
          D.Value <<=  (CORBA::Any::from_char ) c ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "short" ) ) {
          short s ;
          s = (short ) d ;
          D.Value <<=  s ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
          CORBA::Long l ;
          l = (CORBA::Long ) d ;
          D.Value <<= l ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "float" ) ) {
          float f ;
          f = (float ) d ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= f ;
#else
//JR      D.Value.replace(CORBA::TypeCode::PR_float_tc(), (void*)(&f));
          D.Value <<= (CORBA::Float) f ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "double" ) ) {
        }
//        else if ( !strcmp( Type , "objref" ) ) {
        else { // Default
          D.Value <<= CORBA::Object::_nil() ;
//          theOutPort->Value( D.Value ) ;
        }
//        else {
//          cdebug << " (other ERROR)" << endl ;
//        }
#if InParametersSetTrace
        cdebug << " --> call_kind " << D.Value.type()->kind() << endl ;
#endif
        break;
      case CORBA::tk_objref:
        if ( !strcmp( Type , "string" ) ) {
          CORBA::Object_ptr ObjRef ;
          char * retstr ;
          try {
#if OMNIORB_VERSION >= 4
            D.Value >>= (CORBA::Any::to_object ) ObjRef ;
#else
            D.Value >>= ObjRef ;
#endif
            retstr = ObjectToString( ObjRef ) ;
            D.Value <<= retstr ;
//            theOutPort->Value( D.Value ) ;
          }
          catch( ... ) {
            if ( i != 0 ) {
              Err = true ;
	    }
            cdebug << "ToString( object ) Catched ERROR" << endl ;
          }
        }
        else if ( !strcmp( Type , "boolean" ) ) {
          bool b = 0 ;
          D.Value <<=  (CORBA::Any::from_boolean ) b ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "char" ) ) {
          unsigned char c = 0 ;
          D.Value <<=  (CORBA::Any::from_char ) c ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "short" ) ) {
          short s = 0 ;
          D.Value <<=  s ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
          CORBA::Long l = 0 ;
          D.Value <<= l ;
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "float" ) ) {
          float f = 0 ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= f ;
#else
//JR      D.Value.replace(CORBA::TypeCode::PR_float_tc(), (void*)(&f));
          D.Value <<= (CORBA::Float) f ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
        else if ( !strcmp( Type , "double" ) ) {
          double d = 0 ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= d ;
#else
//JR      D.Value.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
          D.Value <<= (CORBA::Double) d ;
#endif
//          theOutPort->Value( D.Value ) ;
        }
//        else if ( !strcmp( Type , "objref" ) ) {
        else { // Default
          CORBA::Object_ptr obj ;
          char * retstr ;
          try {
#if OMNIORB_VERSION >= 4
            D.Value >>= (CORBA::Any::to_object ) obj ;
#else
            D.Value >>= obj ;
#endif
            retstr = ObjectToString( obj ) ;
#if InParametersSetTrace
            cdebug << retstr << endl ;
#endif
          }
          catch( ... ) {
            if ( i != 0 ) {
              Err = true ;
	    }
            cdebug << "ToString( object ) Catched ERROR" << endl ;
          }
        }
//        else {
//          cdebug << " (other ERROR)" << endl ;
//        }
#if InParametersSetTrace
        cdebug << " --> call_kind " << D.Value.type()->kind() << endl ;
#endif
        break;
      default:
        cdebug << " (other ERROR) " << D.Value.type()->kind() << endl ;
      }
    }
    else {
      cdebug << ThreadNo() << " In" << i << " : wrong state ERROR State "
             << anInPort->PortState() << " NameState "
             << Automaton()->StateName( anInPort->PortState() ) << " PortName "
             << anInPort->PortName() << " Parametername "
             << anInPort->GetServicesParameter().Parametername << endl ;
      Err = true ;
    }
    InParametersList[i] = D ;
  }
}

void GraphExecutor::InNode::InOutParametersSet( int nOutParams ,
                                                ServicesAnyData * OutParametersList ) {
#if InParametersSetTrace
  cdebug << pthread_self() << "/" << ThreadNo() << " InOutParametersSet " << Name() << endl ;
#endif
  int i ;
  for ( i = 0 ; i < nOutParams ; i++ ) {
    ServicesAnyData D = OutParametersList[i] ;

//JR 18.02.2005 Debug Memory leak : delete does not destroy that string ...
//    D.Name = CORBA::string_dup(GetChangeNodeOutPort(i)->GetServicesParameter().Parametername);
    D.Name = GetChangeNodeOutPort(i)->PortName() ;
//JR 18.02.2005 Debug Memory leak :     string _Type = CORBA::string_dup(GetChangeNodeOutPort(i)->GetServicesParameter().Parametertype) ;
    const char * Type = GetChangeNodeOutPort(i)->GetServicesParameter().Parametertype ;
#if InParametersSetTrace
    bool OutDone = GetChangeNodeOutPort(i)->PortDone() ;
    cdebug << ThreadNo() << " ArgOut" << i << " " << D.Name << " PortDone( " << OutDone << " ) Type : "
           << Type << endl ;
#endif
    if ( !strcmp( Type , "string" ) ) {
//      D.Value <<= (char *) NULL ;
      D.Value <<= "" ;
    }
    else if ( !strcmp( Type , "boolean" ) ) {
      bool b = 0 ;
      D.Value <<=  (CORBA::Any::from_boolean ) b ;
    }
    else if ( !strcmp( Type , "char" ) ) {
      unsigned char c = 0 ;
      D.Value <<=  (CORBA::Any::from_char ) c ;
    }
    else if ( !strcmp( Type , "short" ) ) {
      short s = 0 ;
      D.Value <<=  s ;
    }
    else if ( !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
      D.Value <<= (CORBA::Long ) 0 ;
    }
    else if ( !strcmp( Type , "float" ) ) {
      float f = 0 ;
#ifdef REDHAT // mkr : debug for PAL12255
      D.Value <<= f ;
#else
//JR  D.Value.replace(CORBA::TypeCode::PR_float_tc(), (void*)(&f));
      D.Value <<= (CORBA::Float) f ;
#endif
    }
    else if ( !strcmp( Type , "double" ) ) {
      double d = 0 ;
#ifdef REDHAT // mkr : debug for PAL12255
      D.Value <<= d ;
#else
//JR  D.Value.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
      D.Value <<= (CORBA::Double) d ;
#endif
    }
    else {
      D.Value <<= CORBA::Object::_nil() ;
    }
#if InParametersSetTrace
    switch (D.Value.type()->kind()) { // { string , long , double , objref }
    case CORBA::tk_string:
      const char * t;
      D.Value >>= t;
      cdebug << ThreadNo() << " " << t << "(string)" << endl ;
      break;
    case CORBA::tk_boolean:
      bool b ;
      D.Value >>= (CORBA::Any::to_boolean ) b;
      cdebug << ThreadNo() << " " << b << "(boolean)" << endl ;
      break;
    case CORBA::tk_char:
      unsigned char c ;
      D.Value >>= (CORBA::Any::to_char ) c;
      cdebug << ThreadNo() << " " << c << "(char)" << endl ;
      break;
    case CORBA::tk_short:
      CORBA::Short s;
      D.Value >>= s;
      cdebug << ThreadNo() << " " << s << "(short)" << endl ;
      break;
    case CORBA::tk_long:
      CORBA::Long l;
      D.Value >>= l;
      cdebug << ThreadNo() << " " << l << "(CORBA::Long)" << endl ;
      break;
    case CORBA::tk_float:
      CORBA::Float f;
      D.Value >>= f;
      cdebug << ThreadNo() << " " << f << "(float)" << endl ;
      break;
    case CORBA::tk_double:
      CORBA::Double d;
      D.Value >>= d;
      cdebug << ThreadNo() << " " << d << "(double)" << endl ;
      break;
    case CORBA::tk_objref:
      try {
        CORBA::Object_ptr obj ;
        char * retstr ;
#if OMNIORB_VERSION >= 4
            D.Value >>= (CORBA::Any::to_object ) obj ;
#else
        D.Value >>= obj ;
#endif
        retstr = ObjectToString( obj ) ;
        cdebug << ThreadNo() << retstr << endl ;
      }
      catch( ... ) {
        cdebug << "ToString( object ) Catched ERROR" << endl ;
      }
      break;
    default:
      cdebug << ThreadNo() << " " << "(other ERROR)" << endl ;
    }
#endif
    OutParametersList[i] = D ;
  }
}

#define OutParametersSetTrace 1
bool GraphExecutor::InNode::OutParametersSet( bool Err ,
                                              SUPERV::GraphState PortState ,
                                              int nOutParams ,
                                              ServicesAnyData * OutParametersList ) {
  bool RetVal = true ;
  int i ;
  GraphBase::OutPort * aGateOutPort = NULL ;
  bool OrSwitch = false ;
  bool DefaultSwitch = false ;
#if OutParametersSetTrace
  cdebug_in << "OutParametersSet " << Name() << " nOutParams " << nOutParams << " NewPortState "
            << PortState << endl ;
#endif
//  cout << "OutParametersSet " << Name() << " nOutParams " << nOutParams << " NewPortState " << PortState << endl ;
  if ( nOutParams && !IsMacroNode() ) {
    GraphBase::OutPort * anOutPort ;
    for ( i = 0 ; i < nOutParams ; i++ ) {
      anOutPort = GetChangeNodeOutPort(i) ;
      if ( Err ) {
        anOutPort->PortState( PortState ) ;
        anOutPort->PortDone( true ) ;
      }
      else {
#if OutParametersSetTrace
        cdebug << ThreadNo() << "OutParametersSet " << "Out" << i << " " << Name() << " "
               << anOutPort->PortName() << " " << anOutPort->Kind() ;
#endif
        ServicesAnyData D = OutParametersList[i] ;
        switch (D.Value.type()->kind()) { // { string , long , double , objref }
        case CORBA::tk_string: {
          const char * t;
          D.Value >>= t;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << t << "(string)" << endl ;
#endif
          break;
	}
        case CORBA::tk_boolean: {
          bool b ;
          D.Value >>= (CORBA::Any::to_boolean ) b;
          CORBA::Long l = (CORBA::Long ) b ;
          D.Value <<= l ;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << b << "(boolean)" << endl ;
#endif
          break;
	}
        case CORBA::tk_char: {
          unsigned char c ;
          D.Value >>= (CORBA::Any::to_char ) c;
          CORBA::Long l = (CORBA::Long ) c ;
          D.Value <<= l ;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << c << "(char)" << endl ;
#endif
          break;
	}
        case CORBA::tk_short: {
          CORBA::Short s;
          D.Value >>= s;
          CORBA::Long l = (CORBA::Long ) s ;
          D.Value <<= l ;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << s << "(short)" << endl ;
#endif
          break;
	}
        case CORBA::tk_long: {
          CORBA::Long l;
          D.Value >>= l;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << l << "(CORBA::Long)" << endl ;
#endif
          break;
	}
        case CORBA::tk_float: {
          CORBA::Float f;
          D.Value >>= f;
          double d = (double ) f ;
#ifdef REDHAT // mkr : debug for PAL12255
          D.Value <<= d ;
#else
//JR      D.Value.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
          D.Value <<= (CORBA::Double) d ;
#endif
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << f << "(float)" << endl ;
#endif
          break;
	}
        case CORBA::tk_double: {
          CORBA::Double d;
          D.Value >>= d;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << d << "(double)" << endl ;
#endif
          break;
	}
        case CORBA::tk_objref: {
          try {
            CORBA::Object_ptr obj ;
#if OMNIORB_VERSION >= 4
            D.Value >>= (CORBA::Any::to_object ) obj ;
#else
            D.Value >>= obj ;
#endif
#if OutParametersSetTrace
            char * retstr ;
            retstr = ObjectToString( obj ) ;
            cdebug << ThreadNo() << retstr << endl ;
#endif
          }
          catch( ... ) {
            cdebug << "ToString( object ) Catched ERROR" << endl ;
            RetVal = false ;
          }
          break;
	}
        default: {
          cdebug << ThreadNo() << " " << "(other ERROR)" << endl ;
          RetVal = false ;
	}
        }
        OutParametersList[i] = D ;
        if ( !anOutPort->IsDataStream() ) {
          if ( anOutPort->IsGate() ) {
            aGateOutPort = anOutPort ;
#if OutParametersSetTrace
            cdebug << " Gate " ;
#endif
            CORBA::Long l = 1;
            OutParametersList[i].Value <<= l;
            anOutPort->SetValue( OutParametersList[i].Value );
          }
          else if ( anOutPort->IsLoop() ) {
#if OutParametersSetTrace
            cdebug << " Loop " ;
#endif
            anOutPort->SetValue( OutParametersList[i].Value );
// InLoop Port of EndLoopNode is ready :
            anOutPort->ChangeInPorts(0)->PortState( SUPERV::ReadyState ) ;
          }
          else if ( anOutPort->IsSwitch() ) {
#if OutParametersSetTrace
            cdebug << " Switch " ;
#endif
            anOutPort->SetValue( OutParametersList[i].Value );
            if ( anOutPort->InPortsSize() && anOutPort->ChangeInPorts( 0 )->IsGate() ) {
//We have a SwitchBranch or the DefaultBranch .
//JR 09.02.2005 : OrSwitch is the OR of all SwitchBranches (SwitchParameters) :
//It controls that there is only one SwitchBranch activated
//If it's final value is false ==> activation of the Default to GOTO to EndSwitchNode
//DefaultSwitch is true if a SwitchPort is linked to the DefaultPort of the EndSwitchNode
              if ( OrSwitch && anOutPort->BoolValue() ) {
                string anErrorMessage = string( "More than one SwitchBranch should be activated in SwitchNode " ) +
                                        string( Name() ) + string( "( " ) +
                                        string( anOutPort->PortName() ) + string( " )" ) ;
                _OutNode->Graph()->SetMessages( anErrorMessage ) ;
                cdebug << "Executor::InNodeThreads::OutParameters more than one SwitchBranch is true ERROR"
                       << endl ;
                RetVal = false ;
	      }
              else if ( anOutPort->BoolValue() ) {
//JR 09.02.2005 Debug : case of a SwitchPort linked to the DefaultPort of the EndSwitchNode :
                if ( !strcmp( anOutPort->ChangeInPorts( 0 )->NodeName() , GOTONode()->CoupledNodeName() ) ) {
                  DefaultSwitch = anOutPort->BoolValue() ;
		}
                else {
                  OrSwitch = OrSwitch | anOutPort->BoolValue() ;
		}
#if OutParametersSetTrace
                cdebug << "InNodeThreads::OutParameters OrSwitch " << OrSwitch << "DefaultSwitch "
                       << DefaultSwitch << endl ;
#endif
	      }
    	    }
          }
          else {
#if OutParametersSetTrace
            cdebug << " Param " ;
#endif
            anOutPort->SetValue( OutParametersList[i].Value );
          }
          anOutPort->PortState( PortState ) ;
          anOutPort->PortDone( true ) ;
	}
#if OutParametersSetTrace
        cdebug << "OutParametersSet OrSwitch " << OrSwitch << "DefaultSwitch "
               << DefaultSwitch << endl ;
#endif
        int j ;
        for ( j = 0 ; j < anOutPort->InPortsSize() ; j++ ) {
#if OutParametersSetTrace
          cdebug << ThreadNo() << "OutParametersSet " << "Out" << i << " " << Name() << " "
                 << anOutPort->PortName() << " " << anOutPort->Kind() << " --> "
                 << anOutPort->ChangeInPorts( j )->NodeName() << "( "
                 << anOutPort->ChangeInPorts( j )->PortName() << anOutPort->ChangeInPorts( j )->Kind()
                 << " )" << endl ;
#endif
          bool fromGOTO = false ;
          const char * ToNodeName = anOutPort->ChangeInPorts( j )->NodeName() ;
          if ( !strcmp( ToNodeName , _OutNode->Name() ) &&
               _OutNode->Graph()->GraphMacroLevel() != 0 ) {
//JR NPAL14110 09.02.2007 : That must be done only at the end of SubGraph because we may
//                          have an output of the SubGraph in a Loop !...
//                          So that code must be done in CheckAllDone
#if 0
#if OutParametersSetTrace
            cdebug << "OutParametersSet ToNodeName " << _OutNode->Name() << " CoupledNode "
                   << _OutNode->Graph()->CoupledNodeName() << _OutNode->Graph()->CoupledNode()
                   << endl ;
            cdebug << "OutParametersSet GraphExecutor " << _OutNode->Graph()->CoupledNode()->GraphEditor()->Executor() << endl ;
#endif
            _OutNode->Graph()->CoupledNode()->GraphEditor()->Executor()->OutputOfAny( _OutNode->Graph()->CoupledNodeName() ,
                                                                            anOutPort->ChangeInPorts( j )->PortName() ,
//JR 30.03.2005                                                                            *anOutPort->Value() ) ;
                                                                            anOutPort->Value() ) ;
#if OutParametersSetTrace
            cdebug << "OutParametersSet OutputOfAny( "
                   << _OutNode->Graph()->CoupledNodeName() << " , "
                   << anOutPort->ChangeInPorts( j )->PortName() << " , value )" << endl ;
#endif
#endif
	  }
          else {
	    GraphBase::ComputingNode * ToNode = _OutNode->Graph()->GetChangeGraphNode( ToNodeName ) ;
            if ( ToNode ) {
//              cout << "OutParametersSet ToNodeName " << ToNodeName << endl ;
//              cdebug << "OutParametersSet ToNodeName " << ToNodeName << " " << ToNode->Name() << endl ;
              GraphBase::OutPort * aGOTOPort = ToNode->GetChangeNodeInGate()->GetOutPort() ;
              if ( aGOTOPort ) {
                fromGOTO = aGOTOPort->IsGOTO() ;
  	      }
              if ( anOutPort->ChangeInPorts( j )->IsEndSwitch() || fromGOTO ) {
#if OutParametersSetTrace
                cdebug << anOutPort->ChangeInPorts( j )->NodeName() << "("
                       << anOutPort->ChangeInPorts( j )->PortName() << ","
                       << anOutPort->ChangeInPorts( j )->Kind() << ") CHANGED from "
                       << anOutPort->ChangeInPorts( j )->GetOutPort()->NodeName()
                       << "("
                       << anOutPort->ChangeInPorts( j )->GetOutPort()->PortName()
                       << " to " << anOutPort->ChangeInPorts( j )->GetOutPort()->Kind()
                       << ") : Done " << anOutPort->PortDone() << " State "
                       << Automaton()->StateName( anOutPort->ChangeInPorts( j )->PortState() ) << endl ;
#endif
                anOutPort->ChangeInPorts( j )->ChangeOutPort( anOutPort ) ;
  	      }
              else {
#if OutParametersSetTrace
                cdebug << anOutPort->ChangeInPorts( j )->NodeName() << "("
                       << anOutPort->ChangeInPorts( j )->PortName() << ","
                       << anOutPort->ChangeInPorts( j )->Kind() << ") NOT changed from "
                       << anOutPort->ChangeInPorts( j )->GetOutPort()->NodeName()
                       << "("
                       << anOutPort->ChangeInPorts( j )->GetOutPort()->PortName()
                       << " " << anOutPort->ChangeInPorts( j )->GetOutPort()->Kind()
                       << ") " << endl ;
#endif
	      }
	    }
  	  }
        }
#if OutParametersSetTrace
        switch ( anOutPort->Value().type()->kind() ) {
        case CORBA::tk_string:
          const char * t;
          (anOutPort->Value()) >>= t;
          cdebug << ThreadNo() << " Out" << i << " : " << t << "(string)" << endl ;
          break;
        case CORBA::tk_boolean:
          bool b ;
          (anOutPort->Value()) >>= (CORBA::Any::to_boolean ) b;
          cdebug << ThreadNo() << " Out" << i << " : " << b << "(boolean)" << endl ;
          break;
        case CORBA::tk_char:
          unsigned char c ;
          (anOutPort->Value()) >>= (CORBA::Any::to_char ) c;
          cdebug << ThreadNo() << " Out" << i << " : " << c << "(char)" << endl ;
          break;
        case CORBA::tk_short:
          CORBA::Short s;
          (anOutPort->Value()) >>= s;
          cdebug << ThreadNo() << " Out" << i << " : " << s << "(short)" << endl ;
          break;
        case CORBA::tk_long:
          CORBA::Long l;
          (anOutPort->Value()) >>= l;
          cdebug << ThreadNo() << " Out" << i << " : " << l << "(CORBA::Long)" << endl ;
          break;
        case CORBA::tk_float:
          CORBA::Float f;
          (anOutPort->Value()) >>= f;
          cdebug << ThreadNo() << " Out" << i << " : " << f << "(float)" << endl ;
          break;
        case CORBA::tk_double:
          CORBA::Double d;
          (anOutPort->Value()) >>= d;
          cdebug << ThreadNo() << " Out" << i << " : " << d << "(double)" << endl ;
          break;
        case CORBA::tk_objref:
          CORBA::Object_ptr obj ;
          char * retstr ;
          try {
//JR 02.08.2005 Debug SEGV            anOutPort->Value() >>= obj ;
            CORBA::Any anAny ;
            anAny = anOutPort->Value() ;
#if OMNIORB_VERSION >= 4
            anAny >>= (CORBA::Any::to_object ) obj ;
#else
            anAny >>= obj ;
#endif
            retstr = ObjectToString( obj );
            cdebug << ThreadNo() << " Out" << i << " : " << "ToString( object ) "
                   << retstr << endl ;
  	  }
          catch ( ... ) {
            cdebug << ThreadNo() << " Out" << i << " : " << "ToString( object ) "
                   << "Catched ERROR" << endl ;
            RetVal = false ;
  	  }
          break;
        default:
          cdebug << ThreadNo() << " Out" << i << " : " << "(other ERROR)" << endl ;
          RetVal = false ;
        }
#endif
      }
    } // End of : for ( i = 0 ; i < nOutParams ; i++ ) {
#if OutParametersSetTrace
    cdebug << ThreadNo() << "OutParametersSet End of loop with " << nOutParams
           << " OutParams. aGateOutPort " << (void *) aGateOutPort << " IsSwitchNode "
           << IsSwitchNode() << " OrSwitch " << OrSwitch << " DefaultSwitch " << DefaultSwitch
           << endl ;
#endif

//In SwitchNodes :
    if ( IsSwitchNode() && aGateOutPort ) {
//JR 09.02.2005 : OrSwitch is the OR of all SwitchBranches :
//It controls that there is only one SwitchBranch activated
//If it's final value is false ==> activation of the Default in order to GOTO to EndSwitchNode
//DefaultSwitch is true if a SwitchPort (SwitchBranch) is linked to the DefaultPort of the EndSwitchNode
      if ( !OrSwitch && !DefaultSwitch ) {
        if ( aGateOutPort->InPortsSize() && aGateOutPort->ChangeInPorts( 0 ) ) {
//Dynamic activation of the Default OutPort :
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << "OutGate " << Name() << " Open of "
                 << aGateOutPort->PortName() << " " << aGateOutPort->Kind() << " WITH DefaultPort"
                 << endl ;
#endif
          CORBA::Long l = 1;
          OutParametersList[0].Value <<= l ;
          aGateOutPort->SetValue( OutParametersList[0].Value ) ;
	}
        else {
//The Default OutPort is not linked ==> error
          string anErrorMessage = string( "DefaultPort of SwitchNode " ) +
                                  string( Name() ) + " is not connected." ;
          _OutNode->Graph()->SetMessages( anErrorMessage ) ;
#if OutParametersSetTrace
          cdebug << ThreadNo() << " " << "OutGate " << Name() << " "
                 << aGateOutPort->PortName() << " " << aGateOutPort->Kind() << " NOT CONNECTED ERROR"
                 << endl ;
#endif
          RetVal = false ;
	}
      }
//JR 07.04.2005 Debug : reset only if it is not a default switch (SwitchBranch or
//                      SwitchParameter of SwitchNode connected to the DefaultInPort of
//                      EndSwitchNode)
//      else {
      else if ( !DefaultSwitch ) {
#if OutParametersSetTrace
        cdebug << ThreadNo() << " " << "OutGate " << Name() << " Close of "
               << aGateOutPort->PortName() << " " << aGateOutPort->Kind() << " NO DefaultPort"
               << " OrSwitch " << OrSwitch << " DefaultSwitch " << DefaultSwitch << endl ;
#endif
        CORBA::Long l = 0;
        OutParametersList[0].Value <<= l ;
        aGateOutPort->SetValue( OutParametersList[0].Value ) ;
      }
      if ( RetVal ) {
// The OutPort field of InPorts of EndSwitchNode may be updated from each OutPort of that SwitchNode :
        GraphBase::EndOfSwitchNode * anEndSwitchNode = (GraphBase::EndOfSwitchNode * ) CoupledNode() ;
        int i ;
//PAL8518
//JR 16.02.2005 Debug : At first Change InPorts of EndSwitchNode that have the same name as an OutPort of
// the SwitchNode even if it is the DefaultPort : GraphSwitchCheckDefault1.xml
//STEP A : InPorts of EndSwitchNode that have the same name as an OutPort of the SwitchNode
        for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
          GraphBase::OutPort * anOutPort = GetChangeNodeOutPort( i );
          GraphBase::InPort * anInPort = NULL ;
          anInPort = anEndSwitchNode->GetChangeInPort( anOutPort->PortName() ) ;
          if ( anInPort ) {
#if OutParametersSetTrace
            cdebug << "OutParametersSet " << Name() << " " << anInPort->NodeName()
                   << "( " << anInPort->PortName() << " , " << anInPort->Kind() << " ) with same name " ;
            if ( anInPort->GetOutPort() ) {
              cdebug << "linked from " << anInPort->GetOutPort()->NodeName()
                     << "( " << anInPort->GetOutPort()->PortName() << " ) " ;
            }
            else {
              cdebug << "NOT linked " ;
            }
            cdebug << "CHANGED TO linked from " << anOutPort->NodeName() << "( "
                   << anOutPort->PortName() << " )" << endl ;
#endif
            anInPort->ChangeOutPort( anOutPort ) ;
          }
        }
//STEP B : InPorts of EndSwitchNode directly connected from an OutPort of the SwitchNode
        for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
          GraphBase::OutPort * anOutPort = GetChangeNodeOutPort( i );
          GraphBase::InPort * anInPort ;
          int j ;
          for ( j = 0 ; j < anOutPort->InPortsSize() ; j++ ) {
            anInPort = anOutPort->ChangeInPorts( j ) ;
//Update the OutPort field in the Inports directly connected of the EndSwitchNode :
            if ( !strcmp( anInPort->NodeName() , anEndSwitchNode->Name() ) ) {
#if OutParametersSetTrace
              cdebug << "OutParametersSet " << Name() << " " << anInPort->NodeName()
                     << "( " << anInPort->PortName() << " , " << anInPort->Kind()
                     << " ) directly connected " ;
              if ( anInPort->GetOutPort() ) {
                cdebug << "linked from " << anInPort->GetOutPort()->NodeName()
                       << "( " << anInPort->GetOutPort()->PortName() << " ) " ;
              }
              else {
                cdebug << "NOT linked " ;
              }
              cdebug << "CHANGED TO linked from " << anOutPort->NodeName() << "( "
                     << anOutPort->PortName() << " )" << endl ;
#endif
              anInPort->ChangeOutPort( anOutPort ) ;
	    }
          }
        }
//STEP C : If it is not the DefaultBranch, explore the SwitchBranch and all NOTSwitchBranch[es]
//Change recursively InPorts of EndSwitchNode linked to that Branch (or that SwitchNode)
        if ( OrSwitch ) {
          for ( i = 0 ; i <  LinkedNodesSize() ; i++ ) {
            GraphBase::ComputingNode * aNode = (GraphBase::ComputingNode * ) LinkedNodes( i ) ;
            if ( aNode != anEndSwitchNode && !aNode->IsGOTONode() &&
                 !aNode->IsDataFlowNode() && !aNode->IsDataStreamNode()  ) {
	      const GraphBase::InPort * anInGate = aNode->GetNodeInGate() ;
              GraphExecutor::InNode * anInNode = (GraphExecutor::InNode * ) aNode->GetInNode() ;
//STEP C1 : SwitchBranch :
              if ( anInGate->GetOutPort() ) {
                if ( anInGate->GetOutPort()->BoolValue() ) {
#if OutParametersSetTrace
                  cdebug << "OutParametersSet " << Name() << " SWITCHBranch " << aNode->Name() << endl ;
#endif
                  int j ;
                  for ( j = 0 ; j < aNode->GetNodeOutPortsSize() ; j++ ) {
                    GraphBase::OutPort * anOutPort = aNode->GetChangeNodeOutPort( j );
                    anInNode->SetOutPortsOfInportsOfEndSwitch( anOutPort , anEndSwitchNode->Name() ) ;
		  }
	        }
	      }
//STEP C2 : NOTSwitchBranch :
              else {
#if OutParametersSetTrace
                cdebug << "OutParametersSet " << Name() << " NOTSWITCHBranch " << aNode->Name() << endl ;
#endif
                int j ;
                for ( j = 0 ; j < aNode->GetNodeOutPortsSize() ; j++ ) {
                  GraphBase::OutPort * anOutPort = aNode->GetChangeNodeOutPort( j );
                  anInNode->SetOutPortsOfInportsOfEndSwitch( anOutPort , anEndSwitchNode->Name() ) ;
		}
	      }
	    }
	  }
	}
      }
    }
  }
#if OutParametersSetTrace
  cdebug_out << "OutParametersSet " << Name() << " nOutParams " << nOutParams << " NewPortState "
             << PortState << " RetVal " << RetVal << endl ;
#endif
  return RetVal ;
}


void GraphExecutor::InNode::SetOutPortsOfInportsOfEndSwitch( GraphBase::OutPort * anOutPort ,
                                                             const char * anEndSwitchNodeName ) {
#if OutParametersSetTrace
  cdebug_in << "SetOutPortsOfInportsOfEndSwitch " << Name() << " " << anOutPort->NodeName() << "( "
            << anOutPort->PortName() << " ) with " << anOutPort->InPortsSize() << " links." << endl ;
#endif
  GraphBase::InPort * anInPort ;
  int i ;
  for ( i = 0 ; i < anOutPort->InPortsSize() ; i++ ) {
    anInPort = anOutPort->ChangeInPorts( i ) ;
    if ( !anInPort->IsDataStream() ) {
//Update the OutPort field in the Inports of the EndSwitchNode :
      if ( !strcmp( anInPort->NodeName() , anEndSwitchNodeName ) ) {
#if OutParametersSetTrace
        cdebug << "SetOutPortsOfInportsOfEndSwitch " << Name() << " " << anInPort->NodeName()
               << "( " << anInPort->PortName() << " , " << anInPort->Kind() << " ) " ;
        if ( anInPort->GetOutPort() ) {
          cdebug << "linked from " << anInPort->GetOutPort()->NodeName()
                 << "( " << anInPort->GetOutPort()->PortName() << " ) " ;
        }
        else {
          cdebug << "NOT linked ERROR " ;
        }
        cdebug << "CHANGED TO linked from "
               << anOutPort->NodeName() << "( "
               << anOutPort->PortName() << " )" << endl ;
#endif
        anInPort->ChangeOutPort( anOutPort ) ;
      }
      else {
#if OutParametersSetTrace
        cdebug << "SetOutPortsOfInportsOfEndSwitch " << Name() << " " << anInPort->NodeName()
               << "( " << anInPort->PortName() << " , " << anInPort->Kind() << " ) " << endl ;
#endif
        GraphBase::ComputingNode * aComputingNode ;
        aComputingNode = _OutNode->Graph()->GetChangeGraphNode( anInPort->NodeName() ) ;
        if ( aComputingNode && !aComputingNode->IsGOTONode() &&
             !( IsEndLoopNode() && GOTONode()->CoupledNode() == aComputingNode ) ) {
          GraphExecutor::InNode * aNode ;
          aNode = (GraphExecutor::InNode * ) aComputingNode->GetInNode() ;
          if ( aNode ) {
            int j ;
            for ( j = 0 ; j < aNode->GetNodeOutPortsSize() ; j++ ) {
              GraphBase::OutPort * anOutPort = aNode->GetChangeNodeOutPort( j ) ;
              aNode->SetOutPortsOfInportsOfEndSwitch( anOutPort , anEndSwitchNodeName ) ;
	    }
	  }
	}
      }
    }
  }
#if OutParametersSetTrace
  cdebug_out << "SetOutPortsOfInportsOfEndSwitch " << Name() << " OutPort " << anOutPort->PortName()
             << endl ;
#endif
}


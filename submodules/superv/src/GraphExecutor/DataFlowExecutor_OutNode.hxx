//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_OutNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWEXECUTOR_OUTNODE_HXX
#define _DATAFLOWEXECUTOR_OUTNODE_HXX

#include "SALOME_Component_i.hxx"

#include "DataFlowExecutor_InNode.hxx"

#include "DataFlowBase_StreamGraph.hxx"

namespace GraphExecutor {

//  class OutNode : public GraphBase::Graph {
  class OutNode : public GraphBase::Base {

    private :

      GraphBase::StreamGraph * _StreamGraph ;
      GraphBase::Graph       * _Graph ;

      int  _Graph_prof_debug ;

      bool                    _PyInitialized ;
      bool                    _Valid ;
      bool                    _Executable ;
      GraphExecutor::AutomatonState  _State ;
      SUPERV::ControlState    _ControlState ;
      bool                    _Done ;
      bool                    _NodeAborted ;

      // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
      //Engines_Container_i            * _SuperVisionContainer ;
      //pthread_t                        _MainThreadId ;
      pthread_mutex_t                  _PyMutexWait ;
      pthread_mutex_t                  _MutexWait ;
      pthread_cond_t                   _EventWait ;
      pthread_cond_t                   _JoinWait ;
      bool                             _JustStarted ;
      int                              _Threads ; // Number of threads
      long                             _CreatedThreads ; 
      int                              _SuspendedThreads ;
      list< char * >                   _EventNodes ;
      list< GraphExecutor::NodeEvent > _Events ;
      list< GraphExecutor::AutomatonState >   _States ;

      pthread_t _run_func_thread ;

      bool Valid() ;
      void UnValid() ;
      bool Executable() ;

      bool LoadNodes( const GraphBase::ListOfSNodes &aNodes ) ;
      bool LoadLinks( const GraphBase::ListOfSLinks &aLinks ) ;
      bool LoadDatas( const GraphBase::ListOfSLinks &aDatas ) ;

      CORBA::ORB_ptr _Orb ;

      // scan (reverse) the _EventNodes queue and leave only 1 (the last one that came) 
      // event for every node
      void siftEvents();

    public :
  
      OutNode() ;
      OutNode( CORBA::ORB_ptr ORB, SALOME_NamingService* ptrNamingService ,
               const char *DataFlowName ,
               const char * DebugFileName ,
               const SUPERV::KindOfNode aKindOfNode );
      OutNode( CORBA::ORB_ptr ORB, SALOME_NamingService* ptrNamingService ,
               const SALOME_ModuleCatalog::Service& DataFlowService ,
               const char *DataFlowComponentName ,
               const char *DataFlowInterfaceName ,
               const char *DataFlowName ,
               const SUPERV::KindOfNode DataFlowkind ,
               const SUPERV::SDate DataFlowFirstCreation ,
               const SUPERV::SDate DataFlowLastModification ,
               const char * DataFlowEditorRelease ,
               const char * DataFlowAuthor ,
               const char * DataFlowComputer ,
               const char * DataFlowComment ,
               const char * DebugFileName ) ;
      virtual ~OutNode() ;

      void Set_prof_debug( CORBA::ORB_ptr ORB , const char * DebugFileName ) ;
      GraphBase::StreamGraph * StreamGraph() {
                               return _StreamGraph ; } ;
      GraphBase::StreamGraph * StreamGraph() const {
                               return _StreamGraph ; } ;
      GraphBase::Graph * Graph() {
                         return _Graph ; } ;
      GraphBase::Graph * Graph() const {
                         return _Graph ; } ;

      char * Name() { return Graph()->Name() ; } ;

      bool LoadDataFlow( const GraphBase::SGraph * aDataFlow ) ;
//      bool LoadXml( const char* myFileName ) ;
      bool LoadInfo( const GraphBase::SNode &aDataFlowInfo ) ;

      GraphExecutor::InNode * AddNode(
                        const SALOME_ModuleCatalog::Service& NodeService ,
                        GraphBase::ListOfFuncName aFuncName ,
                        GraphBase::ListOfPythonFunctions aPythonFunction ,
                        const char * NodeComponentName ,
                        const char * NodeInterfaceName ,
                        const char * NodeName ,
                        const SUPERV::KindOfNode NodeKindOfNode = SUPERV::ComputingNode ,
                        const SUPERV::SDate NodeFirstCreation = SUPERV::SDate() ,
                        const SUPERV::SDate NodeLastModification = SUPERV::SDate() ,
                        const char * NodeEditorRelease = NULLSTRING ,
                        const char * NodeAuthor = NULLSTRING ,
                        const char * NodeComputer = NULLSTRING ,
                        const char * NodeComment = NULLSTRING ,
                        const int NodeX = 0 ,
                        const int NodeY = 0 ) ;

      bool IsValid() { if ( !_Valid )
                         return Valid() ;
                       return _Valid ; } ;
      bool IsExecutable() { if ( !_Executable )
                                return Executable() ;
                            return _Executable ; } ;

      bool Run( const bool AndSuspend ) ;

      void PyInitialized( bool init ) {
           _PyInitialized = init ; } ;
      bool PyInitialized() {
           return _PyInitialized ; } ;

      void CheckAllDone() ;

      // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
      //Engines_Container_i * SuperVisionContainer() ;
      //const pthread_t MainThreadId() const ;
      void PThreadLock( pthread_mutex_t * aMutex , char * errmsg ) ;
      void PThreadUnLock( pthread_mutex_t * aMutex , char * errmsg ) ;
      void PyThreadLock() ;
      void PyThreadUnLock() ;

      void NewThread() ;
      void ExitThread( pthread_t ThreadNumber = 0 ) ;
      void SuspendThread() ;
      void ResumeThread() ;
      long Threads() { return _Threads ; } ;
      long SuspendedThreads() { return _SuspendedThreads ; } ;
      void JoinedWait() ;

      pthread_t ThreadNo() {
                return _Graph->ThreadNo() ; } ;
      long Thread( const char * NodeName ) ;

      void IncrCreatedThreads() {
           _CreatedThreads += 1 ; } ;
      long CreatedThreads() {
           return _CreatedThreads ; } ;

      bool PushEvent( GraphExecutor::InNode * aNode ,
                      GraphExecutor::NodeEvent anEvent ,
                      GraphExecutor::AutomatonState aState ) ;
      int GetListSize();
      bool StateWait( SUPERV::GraphState aState ) ;
      bool Event( char ** aNodeName ,
                  SUPERV::GraphEvent & anEvent ,
                  SUPERV::GraphState & aState ,
                  bool WithWait ) ;
      bool EventW( char ** aNodeName ,
                   SUPERV::GraphEvent & anEvent ,
                   SUPERV::GraphState & aState ) ;
      bool EventWait( char ** aNodeName ,
                      SUPERV::GraphEvent & anEvent ,
                      SUPERV::GraphState & aState ) ;
      long EventQSize() ;
      void EventList() ;

      void SendSomeDataReady( const char * aNodeName ) ;

      void State(GraphExecutor::AutomatonState aState ) ;
      const char * StateName(GraphExecutor::AutomatonState aState ) {
             return theAutomaton->StateName( aState ) ; }

      SUPERV::GraphState State() ;
      SUPERV::GraphState State( const char * NodeName ) ;
      SUPERV::GraphState State( const char * NodeName ,
                                const char * ServiceParameterName ) ;

      SUPERV::GraphEvent AutomatonGraphEvent(GraphExecutor::NodeEvent anEvent) ;
      SUPERV::GraphState AutomatonGraphState(GraphExecutor::AutomatonState anAutomatonState) ;

      GraphExecutor::AutomatonState AutomatonState() ;
      GraphExecutor::AutomatonState AutomatonState( const char * NodeName ) ;

      SUPERV::ControlState ControlState() ;
      SUPERV::ControlState ControlState( const char * aNodeName ) ;
      void ControlClear() ;
      void ControlClear( const char * aNodeName ) ;

      bool ContainerKill() ;

      bool Suspend() ;
      bool Resume() ;
      bool Kill() ;
      bool Stop() ;

      void NodeAborted( const char * aNodeName ) {
           string aMsg = string( "The node " ) + string( aNodeName ) + " was aborted." ;
           Graph()->SetMessages( aMsg.c_str() ) ;
           _NodeAborted = true ; } ;
      bool IsNodeAborted() {
           return _NodeAborted ; } ;

      bool IsWaiting() ;
      bool IsReady() ;
      bool IsRunning() ;
      void Done( bool aDone ) { _Done = aDone ; } ;
      bool Done() { return _Done ; } ;
      bool IsDone() ;
      bool IsSuspended() ;
      bool IsKilled() ;
      bool IsStopped() ;
      bool IsWaiting( const char * NodeName ) ;
      bool IsReady( const char * NodeName ) ;
      bool IsRunning( const char * NodeName ) ;
      bool IsDone( const char * NodeName ) ;
      bool IsSuspended( const char * NodeName ) ;
      bool PortDone( const char * NodeName ,
                     const char * ServiceParameterName ) ;
      long LastLevelDone() ;

      bool ReadyWait() ;
      bool RunningWait() ;
      bool DoneWait() ;
      bool SuspendedWait() ;
      bool ReadyWait( const char * NodeName ) ;
      bool RunningWait( const char * NodeName ) ;
      bool DoneWait( const char * NodeName ) ;
      bool SuspendedWait( const char * NodeName ) ;

//JR 30.03.2005      const CORBA::Any *GetInData( const char *ToNodeName ,
      const CORBA::Any GetInData( const char *ToNodeName ,
                                  const char *ToParameterName ) ;
//JR 30.03.2005      const CORBA::Any *GetOutData( const char *FromNodeName ,
      const CORBA::Any GetOutData( const char *FromNodeName ,
                                   const char *FromParameterName ) ;
      const long CpuUsed() ;
      const long CpuUsed( const char *aNodeName ) ;

      void setRunFuncThread( pthread_t theThread ) { _run_func_thread = theThread; } ; // mkr : IPAL14711
  };
};

#endif

//  SUPERV GraphExecutor : contains classes that permit execution of graphs and particularly the execution automaton
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowExecutor_PyDynInvoke.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;

#include <stdarg.h>
#include <map>

#include "DataFlowExecutor_InNode.hxx"
#include "DataFlowExecutor_OutNode.hxx"

using namespace CORBA ;

extern GraphExecutor::FiniteStateMachine * theAutomaton ;

//static PyObject * MyPyObjSignal = NULL ;
static PyObject * MyPyObjRef = NULL ;
static PyObject * MyPyObjIor = NULL ;
static PyObject * MyPyRunMethod = NULL ;

static PyMethodDef MethodPyRunMethod[] = {
  { "RunMethod", PyRunMethod, METH_VARARGS },
  { NULL,        NULL }
};

bool GraphExecutor::InNode::InitPython() {
  cdebug_in << "InitPython" << endl ;
  
  Automaton()->PyLock() ;
//  PyObject * Module = Py_InitModule( "InitPyRunMethod" , MethodPyRunMethod ) ;
  if ( !Automaton()->PyInitModule() ) {
    Py_InitModule( "InitPyRunMethod" , MethodPyRunMethod ) ;
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
    //_OutNode->SuperVisionContainer()->ActivatePythonExecution( "InitPyRunMethod" , MethodPyRunMethod ) ;

    // PAL10310: patch on omniORB
    string aPyFunc;
    aPyFunc = "import omnipatch\n" ;
    if ( PyRun_SimpleString( (char *) aPyFunc.c_str() ) ) {
      cdebug << ThreadNo() << " " << Name() << " PyRun_SimpleString ERROR " << endl << aPyFunc << endl ;
      Automaton()->PyUnLock() ;
      return false ;
    }
  }
//  PyObject * Dictionnary = PyModule_GetDict( Module ) ;
//  InitPyDynInvokeError = PyErr_NewException( "InitPyDynInvokeError" , NULL , NULL ) ;
//  PyDict_SetItemString( Dictionnary , InitPyDynInvokeError ) ;
  cdebug << ThreadNo() << "Py_Initialized() " << endl ;

#if 0
//JR : Python documentation says that there is no sense to set a handler of synchronous
// signals as SIGSEGV and SIGFPE !!!
  if ( Automaton()->PyFunction( "PyObjSignal" ) == NULL ) {
    string aPyFunc ;
    aPyFunc = "import signal\n" ;
    aPyFunc += "import InitPyRunMethod\n" ;
    aPyFunc += "import sys\n" ;
    aPyFunc += "import CORBA\n" ;
    aPyFunc += "import omniORB\n" ;
    aPyFunc += "def PySignalHandler( aSignal , aStackFrame ) :\n" ;
    aPyFunc += "    print 'PySignalHandler(aSignal =',aSignal,',aStackFrame= ',aStackFrame,')'\n" ;
    aPyFunc += "InitPyRunMethod.RunMethod( PySignalHandler )\n" ;
    aPyFunc += "PrevHandler = signal.signal( signal.SIGSEGV , PySignalHandler )\n" ;
    aPyFunc += "print 'PyObjSignal PrevHandler of SIGSEGV :', PrevHandler\n" ;
    aPyFunc += "print 'PyObjSignal actual handler of SIGSEGV :', signal.getsignal( signal.SIGSEGV  )\n" ;
//    if ( PyRun_SimpleString( (char *) aPyFunc.c_str() ) ) {
    if ( PyRunSimpleString( (char *) aPyFunc.c_str() ) ) {
      cdebug << ThreadNo() << " " << Name() << " PyRun_SimpleString ERROR " << endl << aPyFunc << endl ;
      Automaton()->PyUnLock() ;
      return false ;
    }
    MyPyObjSignal = MyPyRunMethod ;
    Automaton()->PyFunction( "PyObjSignal" , MyPyObjSignal ) ;
  }
  else {
    MyPyObjSignal = Automaton()->PyFunction( "PyObjSignal" ) ;
  }
#endif

// PyObjRef convert an IOR (string) to an Python ObjectReference
  if ( Automaton()->PyFunction( "PyObjRef" ) == NULL ) {
    string aPyFunc ;
    aPyFunc = "import InitPyRunMethod\n" ;
    aPyFunc += "import sys\n" ;
    aPyFunc += "import CORBA\n" ;
    aPyFunc += "import omniORB\n" ;
    aPyFunc += "orb = CORBA.ORB_init( sys.argv , CORBA.ORB_ID )\n" ;
    aPyFunc += "def PyObjRef( IORObjStr ) :\n" ;
    //    aPyFunc += "    orb = CORBA.ORB_init( sys.argv , CORBA.ORB_ID )\n" ;
    aPyFunc += "    objref = orb.string_to_object( IORObjStr )\n" ;
    aPyFunc += "    return objref\n" ;
    aPyFunc += "InitPyRunMethod.RunMethod( PyObjRef )\n" ;
    if ( PyRun_SimpleString( (char *) aPyFunc.c_str() ) ) {
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL
    //if ( PyRunSimpleString( (char *) aPyFunc.c_str() ) ) {
      cdebug << ThreadNo() << " " << Name() << " PyRun_SimpleString ERROR " << endl << aPyFunc << endl ;
      Automaton()->PyUnLock() ;
      return false ;
    }
    MyPyObjRef = MyPyRunMethod ;
    Automaton()->PyFunction( "PyObjRef" , MyPyObjRef ) ;
  }
  else {
    MyPyObjRef = Automaton()->PyFunction( "PyObjRef" ) ;
  }

// PyObjRef convert an Python ObjectReference to an IOR (string)
  if ( Automaton()->PyFunction( "PyObjIor" ) == NULL ) {
    string aPyFunc ;
    aPyFunc = "import InitPyRunMethod\n" ;
    aPyFunc += "import sys\n" ;
    aPyFunc += "import CORBA\n" ;
    aPyFunc += "import omniORB\n" ;
    aPyFunc += "orb = CORBA.ORB_init( sys.argv , CORBA.ORB_ID )\n" ;
    aPyFunc += "def PyObjIor( ObjRef ) :\n" ;
    //    aPyFunc += "    orb = CORBA.ORB_init( sys.argv , CORBA.ORB_ID )\n" ;
    aPyFunc += "    objIor = orb.object_to_string( ObjRef )\n" ;
    aPyFunc += "    return objIor\n" ;
    aPyFunc += "InitPyRunMethod.RunMethod( PyObjIor )\n" ;
    if ( PyRun_SimpleString( (char *) aPyFunc.c_str() ) ) {
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL
    //if ( PyRunSimpleString( (char *) aPyFunc.c_str() ) ) {
      cdebug << ThreadNo() << " " << Name() << " PyRun_SimpleString ERROR " << endl << aPyFunc << endl ;
      Automaton()->PyUnLock() ;
      return false ;
    }
    MyPyObjIor = MyPyRunMethod ;
    Automaton()->PyFunction( "PyObjIor" , MyPyObjIor ) ;
  }
  else {
    MyPyObjIor = Automaton()->PyFunction( "PyObjIor" ) ;
  }
  Automaton()->PyUnLock() ;

  cdebug_out << "InitPython" << endl ;
  
  return true ;
}

void GraphExecutor::InNode::RemovePyDynInvoke( char * PyFuncName ) {
//  Automaton()->PyLock() ; // Already done ...
  if ( Automaton()->ErasePyFunction( PyFuncName ) ) {
    cdebug << "InNode::RemovePyDynInvoke( " << PyFuncName << " ) success" << endl ;
  }
  else {
    cdebug << "InNode::RemovePyDynInvoke( " << PyFuncName << " ) ERROR failed" << endl ;
  }
//  Automaton()->PyUnLock() ;
}

PyObject * GraphExecutor::InNode::InitPyDynInvoke( char * PyFuncName ,
                                                   const SUPERV::ListOfStrings * aPythonFunction ,
                                                   bool & Err ) {
  bool RetVal = true ;
  Err = false ;
  string aPyFunc ;
  PyObject * thePyRunMethod = NULL ;

  cdebug_in << "InitPyDynInvoke '" << PyFuncName << "' length " << (*aPythonFunction).length() << endl ;

  if ( strlen( PyFuncName ) ) {
    Automaton()->PyLock() ;
    thePyRunMethod = Automaton()->PyFunction( PyFuncName ) ;
    
    //thePyRunMethod = NULL; 
    // asv 28.02.05 : VERY BAD fix of the following problem: after change of a function, 
    // the changes are NOT taken into account by Automation - it returns PyObject of the OLD function.
    // so here we force re-automating the PyObject EVERY TIME, regardless if the function has changed or not.
    // Once again - it is a very bad solution, it fully discards the whole idea of automation,
    // here is it done as a quick fix for a bug. 
    // A better solution (to be implemented): store the PyObject NOT in Automation map, but in
    // InLine node itself!  And if the method is changed - remove the PyObject and force to regenerate it.
    // But this means that PyObject must be stored in Editor's data model.
    // asv 01.03.05 : the fix is not needed, the described bug is not reproduced.  To investigate:
    // WHERE PyObject is removed from Automation map on function change. 

    if ( (*aPythonFunction).length() ) {
      if ( thePyRunMethod == NULL ) {
	unsigned int i ;
        aPyFunc += "import InitPyRunMethod\n" ;
        for ( i = 0 ; i < (*aPythonFunction).length() ; i++ ) {
          aPyFunc += (*aPythonFunction)[ i ] ;
          aPyFunc += "\n" ;
        }
        aPyFunc += "InitPyRunMethod.RunMethod( " ;
        aPyFunc += PyFuncName ;
        aPyFunc += " )\n" ;
        /*cdebug *///cout<< "InitPyDynInvoke PyRun_SimpleString " << endl << aPyFunc << endl ;
//        if ( PyRun_SimpleString( (char *) aPyFunc.c_str() ) ) {
        if ( PyRunSimpleString( (char *) aPyFunc.c_str() ) ) {
          cdebug << ThreadNo() << " " << Name() << " PyRunSimpleString ERROR " << endl << aPyFunc << endl ;
	  PyFuncRunned( false ) ;
          RetVal = false ;
          Err = true ;
        }
        else {
	  PyFuncRunned( true ) ;
          thePyRunMethod = MyPyRunMethod ;
	  
          Automaton()->PyFunction( PyFuncName , thePyRunMethod ) ;
          cdebug << ThreadNo() << "PyRunSimpleString( " << (*aPythonFunction)[ 0 ] << " ) " << endl ;
        }
      }
      if ( RetVal ) {
        Automaton()->PyUnLock() ;
        cdebug_out << "InitPyDynInvoke '" << PyFuncName << "' thePyRunMethod " << thePyRunMethod << " "
                   << thePyRunMethod->ob_refcnt << endl ;
        return thePyRunMethod ;
      }
    }
    else if ( thePyRunMethod ) {
      Automaton()->PyUnLock() ;
      cdebug_out << "InitPyDynInvoke '" << PyFuncName << "' thePyRunMethod " << thePyRunMethod << " "
                 << thePyRunMethod->ob_refcnt << endl ;
      return thePyRunMethod ;
    }
    Automaton()->PyUnLock() ;
  }

  cdebug_out << "InitPyDynInvoke '" << PyFuncName << "' thePyRunMethod Null Err " << Err << endl ;
  
  return NULL ;
}

extern "C" PyObject * PyRunMethod( PyObject * dummy , PyObject * Args ) {
  PyObject * Result = NULL ;
  PyObject * Temp = NULL ;
  if ( PyArg_ParseTuple( Args , "O:set_callback" , & Temp ) ) {
//    cout << "PyArg_ParsedTuple" << endl ;
    if ( !PyCallable_Check( Temp ) ) {
//      PyErr_SetString( PyExc_TypeError , "PyRunMethod must be callable" ) ;
//      cout << "PyRunMethod must be callable" << endl ;
      return NULL ;
    }
    Py_XINCREF( Temp ) ;
    Py_XDECREF( MyPyRunMethod ) ;
    MyPyRunMethod = Temp ;
    Py_INCREF( Py_None ) ;
    Result = Py_None ;
  }
  return Result ;
}

#define PyDynInvokeTrace 0
bool GraphExecutor::InNode::PyDynInvoke( PyObject * MyPyRunMethod ,
		                         const char *method , 
		                         ServicesAnyData * InParametersList , int nInParams ,
		                         ServicesAnyData * OutParametersList , int nOutParams ) {
  int i ;

  bool RetVal = true ;

  const char * sname;

#if PyDynInvokeTrace
  cdebug_in << ThreadNo() << "GraphExecutor::InNode::PyDynInvoke Node " << Name()
            << " method " << method
            << " InParametersList " << InParametersList
            << " OutParametersList " << OutParametersList 
            << " " << nInParams << " InArgs " << nOutParams
            << " OutArgs MyPyRunMethod " ;
#endif
  if ( MyPyRunMethod ) {
#if PyDynInvokeTrace
    cdebug << MyPyRunMethod << " " << MyPyRunMethod->ob_refcnt << endl ;
#endif
  }
  else {
#if PyDynInvokeTrace
    cdebug << " NULL" << endl ;
#endif
    return false ;
  }

  CORBA::Any data ;

  PyObject * ArgsList = NULL ;
  PyObject * ArgValue = NULL ;
  PyObject * Result = NULL ;
  //PyObject * MyPyObjRefList = NULL ;
  PyObject * ResultObj = NULL ;
  //PyObject * MyPyObjIorList = NULL ;
  PyObject * ResultIor = NULL ;

  CORBA::Object_ptr ObjRef ;

  char * IORObjRef ;

  if ( !PyCallable_Check( MyPyRunMethod ) ) {
    Result = NULL ;
    RetVal = false ;
  }
  else {
    ArgsList = PyTuple_New( nInParams ) ;

    for ( i = 0 ; i < nInParams ; i++ ) {
      data = InParametersList[i].Value ;
      sname = InParametersList[i].Name.c_str() ;
      switch ( data.type()->kind() ) {
      case CORBA::tk_string : {
        const char * t ;
        data >>= t ;
        PyObject * ArgValue = Py_BuildValue( "s" , t ) ;
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << t << " (string) "
               << " ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_boolean : {
        bool b ;
        data >>= (CORBA::Any::to_boolean ) b ;
        PyObject * ArgValue = Py_BuildValue( "b" , b ) ;
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << b
               << " (boolean) ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_char : {
        unsigned char c ;
        data >>= (CORBA::Any::to_char ) c ;
        PyObject * ArgValue = Py_BuildValue( "c" , c ) ;
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << c
               << " (char) ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_short : {
        CORBA::Short s ;
        data >>= s ;
        PyObject * ArgValue = Py_BuildValue( "h" , s ) ;
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << s
               << " (short) ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_long : {
        CORBA::Long l ;
        data >>= l ;
//      epa - Porting to 64-bit Linux:CORBA::Long is incorrectly treated as unsigned 
//            integer by Py_BuildValue("l", val) in Python 2.4.1. Using Py_BuildValue("i",val) 
//            seemsto help
        PyObject * ArgValue = NULL;
        if ( SIZEOF_LONG == 4 )
           ArgValue = Py_BuildValue( "l" , l ) ;
	else
           ArgValue = Py_BuildValue( "i" , l ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << l
               << " ArgValue->ob_refcnt" << ArgValue->ob_refcnt << endl ;
#endif
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << l
               << " (CORBA::Long) ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_float : {
        CORBA::Float f ;
        data >>= f ;
        PyObject * ArgValue = Py_BuildValue( "f" , f ) ;
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << f
               << " (float) ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_double : {
        CORBA::Double d ;
        data >>= d ;
        PyObject * ArgValue = Py_BuildValue( "d" , d ) ;
        PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value " << d
               << " (double) ArgsList->ob_refcnt" << ArgsList->ob_refcnt << " ArgValue->ob_refcnt"
               << ArgValue->ob_refcnt << endl ;
#endif
        break ;
      }
      case CORBA::tk_objref : {
        //MyPyObjRefList = PyTuple_New( 1 ) ;
        //PyObject * ObjValue ;
#if OMNIORB_VERSION >= 4
        data >>= (CORBA::Any::to_object ) ObjRef ;
#else
        data >>= ObjRef ;
#endif
        IORObjRef = ObjectToString( ObjRef ) ;
        //ObjValue = Py_BuildValue( "s" , IORObjRef ) ;
        //PyTuple_SetItem( MyPyObjRefList , 0 , ObjValue ) ;
#if PyDynInvokeTrace
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " " << " Value " << IORObjRef << " (objref) "<< endl ;
	//	       << MyPyObjRef->ob_refcnt << "/" << MyPyObjRefList->ob_refcnt << endl ;
#endif
//        ResultObj = PyEval_CallObject( MyPyObjRef , MyPyObjRefList ) ;
	  //ResultObj = PyEvalCallObject( "PyObjRef" , MyPyObjRef , MyPyObjRefList ) ;
#if PyDynInvokeTrace
	  //cdebug << "ObjValue->ob_refcnt" << ObjValue->ob_refcnt << endl ;
#endif
	  //ArgValue = Py_BuildValue( "O" , ResultObj ) ;
	  //PyTuple_SetItem( ArgsList , i , ArgValue ) ;
#if PyDynInvokeTrace
	if ( ArgValue )
	  cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value  (objref) ArgsList->ob_refcnt"
		 << ArgsList->ob_refcnt << " ArgValue->ob_refcnt" << ArgValue->ob_refcnt << endl ;
	else
	  cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value  (objref) ArgsList->ob_refcnt"
		 << ArgsList->ob_refcnt << " ArgValue is NULL" << endl ;
        //cdebug << "MyPyObjRefList->ob_refcnt " << MyPyObjRefList->ob_refcnt-1 << endl ;
#endif
	ResultObj = PyObject_CallFunction( MyPyObjRef , "s",IORObjRef ) ;
	if(ResultObj == NULL){
	  PyErr_Print();
	  ResultObj=Py_None;
          Py_INCREF(ResultObj) ;
	}
        PyTuple_SetItem( ArgsList , i , ResultObj ) ;
        cdebug << "ArgIn" << i << " : " << sname << " " << method << " Value  (objref) ArgsList->ob_refcnt"
	       << ArgsList->ob_refcnt << " ArgValue->ob_refcnt" << ResultObj->ob_refcnt << endl ;
//       << ArgsList->ob_refcnt << " ArgValue->ob_refcnt" << ArgValue->ob_refcnt << endl ;
        //cdebug << "MyPyObjRefList->ob_refcnt " << MyPyObjRefList->ob_refcnt-1 << endl ;
        //Py_DECREF( MyPyObjRefList ) ;
        //if ( CORBA::is_nil( ObjRef ) ) {
        //  ResultObj = NULL ;
        //}
        //else {
	//#if PyDynInvokeTrace
	//if ( ResultObj )
	//cdebug << "ResultObj->ob_refcnt " << ResultObj->ob_refcnt-1 << endl ;
	//else
	//cdebug << "ResultObj is NULL" << endl ;
	//#endif
	//if ( ResultObj )
	//Py_DECREF( ResultObj ) ;
        //}
        break ;
      }
      default : {
        cdebug << "ArgIn" << i << " : " << sname << " Value " << "(other ERROR)" << endl ;
      }
      }
    }

//    Result = PyEval_CallObject( MyPyRunMethod , ArgsList ) ;
    Result = PyEvalCallObject( method , MyPyRunMethod , ArgsList ) ;

#if PyDynInvokeTrace
    cdebug << "ArgsList->ob_refcnt" << ArgsList->ob_refcnt << endl ;
#endif

    if ( Result == NULL ) {
      cdebug_out << "GraphExecutor::InNode::PyDynInvoke Node " << Name() << " " << method << " Error Result == NULL"
                 << endl ;
      PyErr_Print();
      RetVal = false ;
    }
    else {
      for ( i = 0 ; i < nOutParams ; i++ ) {
        data = OutParametersList[i].Value ;
        sname = OutParametersList[i].Name.c_str() ;
        switch ( data.type()->kind() ) {
        case CORBA::tk_string : {
          char * t = "" ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
    	  }
          else {
            ArgValue = Result ;
  	  }
          if ( !ArgValue || !PyString_Check( ArgValue ) ) {
            cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (string)" << endl ;
            RetVal = false ;
  	  }
          else {
            t = PyString_AsString( ArgValue ) ;
	  }
          data <<= t ;
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << t << " (string)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_boolean : {
          bool b = false ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
  	  }
          else {
            ArgValue = Result ;
	  }
          if ( !ArgValue || !PyInt_Check( ArgValue ) ) {
            cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (boolean)" << endl ;
            RetVal = false ;
	  }
          else {
            b = PyInt_AsLong( ArgValue ) ;
	  }
          data <<= (CORBA::Any::from_boolean ) b ;
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << b << " (boolean)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_char : {
          unsigned char c = 0 ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
  	  }
          else {
            ArgValue = Result ;
	  }
//JR 04.04.2005 Debug          if ( !PyInt_Check( ArgValue ) ) {
//Difficult to understand that behavior ... Python char type is a string of length 1 !
//                                          or type is int ...
          if ( !ArgValue || !PyString_Check( ArgValue ) && !PyInt_Check( ArgValue ) ) {
            cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (char)" << endl ;
            RetVal = false ;
	  }
          else {
            c = PyInt_AsLong( ArgValue ) ;
	  }
          data <<= (CORBA::Any::from_char ) c ;
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << c << " (char)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_short : {
          short s = 0 ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
  	  }
          else {
            ArgValue = Result ;
	  }
          if ( !ArgValue || !PyInt_Check( ArgValue ) ) {
            cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (short)" << endl ;
            RetVal = false ;
	  }
          else {
            s = PyInt_AsLong( ArgValue ) ;
	  }
          data <<= s ;
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << s << " (short)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_long : {
          CORBA::Long l = 0 ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
  	  }
          else {
            ArgValue = Result ;
	  }
          if ( ArgValue && PyLong_Check( ArgValue ) ) {
            l = PyLong_AsLong( ArgValue ) ;
	  }
          else if ( ArgValue && PyInt_Check( ArgValue ) ) {
            l = PyInt_AsLong( ArgValue ) ;
	  }
          else {
            cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (CORBA::tk_long)" << endl ;
            RetVal = false ;
	  }
          data <<= l ;
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << l << " (CORBA::Long)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_float : {
          float f = 0 ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
	  }
          else {
            ArgValue = Result ;
	  }
	  if ( ArgValue ) {
	    if ( !PyFloat_Check( ArgValue ) ) {
	      // mkr : bug PAL12454
	      if ( PyLong_Check( ArgValue ) ) {
		cdebug << "ArgOut" << i << " : " << sname << " " << method << " OK (CORBA::Long)" << endl ;
		CORBA::Long l = PyLong_AsLong( ArgValue );
		f = (float) l;
	      }
	      else if ( PyInt_Check( ArgValue ) ) {
		cdebug << "ArgOut" << i << " : " << sname << " " << method << " OK (int)" << endl ;
		CORBA::Long l = PyInt_AsLong( ArgValue );
		f = (float) l;
	      }
	      else {
		cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (float)" << endl ;
		RetVal = false ;
	      }
	    }
	    else {
	      f = PyFloat_AsDouble( ArgValue ) ;
	    }
	  }
	  else {
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (float)" << endl ;
	    RetVal = false ;
	  }
#ifdef REDHAT // mkr : debug for PAL12255
          data <<= f ;
#else
//JR	  data.replace(CORBA::TypeCode::PR_float_tc(), (void*)(&f));
          data <<= (CORBA::Float) f ;
#endif
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << f << " (float)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_double : {
          double d = 0 ;
          PyObject * ArgValue ;
          if ( PyTuple_Check( Result ) ) {
            ArgValue = PyTuple_GetItem( Result , i ) ;
	  }
          else {
            ArgValue = Result ;
	  }
	  if ( ArgValue ) {
	    if ( !PyFloat_Check( ArgValue ) ) {
	      // mkr : bug PAL12454
	      if ( PyLong_Check( ArgValue ) ) {
		cdebug << "ArgOut" << i << " : " << sname << " " << method << " OK (CORBA::Long)" << endl ;
		CORBA::Long l = PyLong_AsLong( ArgValue );
		d = (double) l;
	      }
	      else if ( PyInt_Check( ArgValue ) ) {
		cdebug << "ArgOut" << i << " : " << sname << " " << method << " OK (int)" << endl ;
		CORBA::Long l = PyInt_AsLong( ArgValue );
		d = (double) l;
	      }
	      else {
		cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (float)" << endl ;
		RetVal = false ;
	      }
	    }
	    else {
	      d = PyFloat_AsDouble( ArgValue ) ;
	    }
	  }
	  else {
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " ERROR (float)" << endl ;
	    RetVal = false ;
	  }
#ifdef REDHAT // mkr : debug for PAL12255
          data <<= d ;
#else
//JR	  data.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
          data <<= (CORBA::Double) d ;
#endif
#if PyDynInvokeTrace
	  if ( ArgValue )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << d << " (double)"
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ArgValue->ob_refcnt"
		   << ArgValue->ob_refcnt << endl ;
#endif
          break ;
        }
        case CORBA::tk_objref : {
          PyObject * ObjIor ;
          //MyPyObjIorList = PyTuple_New( 1 ) ;
          if ( PyTuple_Check( Result ) ) {
            ObjIor = PyTuple_GetItem( Result , i ) ;
	  }
          else {
            ObjIor = Result ;
	  }
#if PyDynInvokeTrace
	  if ( ObjIor )
	    cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << "(object reference) "
		   << " Result->ob_refcnt" << Result->ob_refcnt << " ObjIor->ob_refcnt"
		   << ObjIor->ob_refcnt << endl ;
#endif
          //Py_INCREF( ObjIor ) ;
//          PyObject_Print( ObjIor , stdout , 0 ) ;
          //PyTuple_SetItem( MyPyObjIorList , 0 , ObjIor ) ;
//          ResultIor = PyEval_CallObject( MyPyObjIor , MyPyObjIorList ) ;
          //ResultIor = PyEvalCallObject( "PyObjIor" , MyPyObjIor , MyPyObjIorList ) ;
	  ResultIor = PyObject_CallFunction( MyPyObjIor , "O",ObjIor ) ;
#if PyDynInvokeTrace
	  if ( ObjIor )
	    cdebug << "ObjIor->ob_refcnt " << ObjIor->ob_refcnt-1 << endl ;
#endif
          //Py_DECREF( ObjIor ) ;
#if PyDynInvokeTrace
          //cdebug << "MyPyObjIorList->ob_refcnt " << MyPyObjIorList->ob_refcnt-1 << endl ;
#endif
          //Py_DECREF( MyPyObjIorList ) ;
#if PyDynInvokeTrace
          //cdebug << "MyPyObjIor->ob_refcnt " << MyPyObjIor->ob_refcnt << endl ;
#endif
          if ( ResultIor ) {
            char * IOR = NULL ;
            IOR = PyString_AsString( ResultIor ) ;
            ObjRef = StringToObject( IOR ) ;
//JR 05.08.2005 BUG OMNIORB4 ? : that line does run with SALOME_3 (OMNIOrb4) for
//                               object references ... (It's Ok with OMNIOrb3)
//                               IOR and ObjRef and IORObjRef are Ok
//                               BUT data contains a nil reference !!!
#if !omniORB_4_0_5
            data <<= ObjRef ;
#else
            SUPERV::SuperG_var mySuperVisionComponent ;
            mySuperVisionComponent = theAutomaton->SuperVisionComponent() ;
            if ( CORBA::is_nil( mySuperVisionComponent ) ) {
              Engines::Container_var myContainer ;
              Engines::Component_var myObjComponent ;
              _OutNode->Graph()->StartComponent( ThreadNo() , "SuperVisionContainer" ,
                                                 "SUPERV" ,
                                                 myContainer , myObjComponent ) ;
              mySuperVisionComponent = SUPERV::SuperG::_narrow( myObjComponent ) ;
              theAutomaton->SuperVisionComponent( mySuperVisionComponent ) ;
            }
//JR 24.08.2005 BUG OMNIORB4 ? : That does not run also ==> Comments
//            CORBA::Object_ptr ObjRef1 ;
//            ObjRef1 = mySuperVisionComponent->ToObject( ObjRef ) ;
//            data <<= ObjRef1 ;
//JR 24.08.2005 BUG OMNIORB4 ? : BUT THAT RUNS WITH THE "IDENTITY METHOD ToObject of SUPERV.idl :
//                               Object ToObject( in Object anObject ) ;
//CODE of that method !!! :
//CORBA::Object_ptr SuperV_Impl::ToObject( CORBA::Object_ptr anObjectRef ) {
//  beginService( "SuperV_Impl::ToObject" );
//  CORBA::Object_ptr anObject ;
//  anObject = CORBA::Object::_duplicate( anObjectRef ) ;
//  endService( "SuperV_Impl::ToObject" );
//  return anObject ;
//}
//BUT THAT RUNS ONLY IF WE USE Dynamic invocation of CORBA (DynInvoke)
            ServicesAnyData anInParametersList ;
            anInParametersList.Name = "anObjRef" ;
            anInParametersList.Value <<= ObjRef ;
            DynInvoke( mySuperVisionComponent , "ToObject" ,
                       &anInParametersList , 1 , &OutParametersList[i] , 1 ) ;
            data = OutParametersList[i].Value ;
#endif
            IORObjRef = ObjectToString( ObjRef ) ;
#if PyDynInvokeTrace
            cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value IORPy "
                   << IOR << " IORObjRef " << IORObjRef << " (objref) " << endl ;
#endif
            if ( CORBA::is_nil( ObjRef ) ) {
#if PyDynInvokeTrace
              cdebug << "ObjRef ERROR(nil reference) ResultIor->ob_refcnt "
                     << ResultIor->ob_refcnt-1 << endl ;
#endif
              ResultIor = NULL ;
            }
            else {
#if PyDynInvokeTrace
              cdebug << "ObjRef NOT(nil reference)ResultIor->ob_refcnt "
                     << ResultIor->ob_refcnt-1 << endl ;
#endif
              Py_DECREF( ResultIor ) ;
	    }
	  }
          else {
	    PyErr_Print();
            cdebug_out << "GraphExecutor::InNode::PyDynInvoke Error ResultIor == NULL Node "
                       << Name() << " method " << method << " " << endl ;
            RetVal = false ;
	  }
          break ;
        }
        default : {
          cdebug << "ArgOut" << i << " : " << sname << " " << method << " Value " << "(other ERROR)" << endl ;
            RetVal = false ;
        }
        }
//JR 05.08.2005 BUG OMNIORB4 ? : that line does run with SALOME_3 (OMNIOrb4) for
//                               object references ...
        OutParametersList[i].Value = data ;
//        if ( data.type()->kind() == CORBA::tk_objref ) {
//          IORObjRef = ObjectToString( ObjRef ) ;
//#if PyDynInvokeTrace
//          cdebug << ThreadNo() << "GraphExecutor::InNode::PyDynInvoke Node " << Name()
//                 << "ArgOut" << i << " : " << sname << " " << method << " ObjRef "
//                 << IORObjRef << " (objref) " << endl ;
//          Engines::Component_var theObjComponent ;
//          theObjComponent = Engines::Component::_narrow( ObjRef ) ;
//          DynInvoke( theObjComponent , "ping" , NULL , 0 , NULL , 0 ) ;
//#endif
//          Engines::Container_var myContainer ;
//          Engines::Component_var myObjComponent ;
//          _OutNode->Graph()->StartComponent( ThreadNo() , "SuperVisionContainer" ,
//                                             "SUPERV" ,
//                                              myContainer , myObjComponent ) ;
//          SUPERV::SuperG_var mySuperVisionComponent ;
//          mySuperVisionComponent = SUPERV::SuperG::_narrow( myObjComponent ) ;
//          CORBA::Object_ptr ObjRef1 ;
//          ObjRef1 = mySuperVisionComponent->ToObject( IORObjRef ) ;
//#if PyDynInvokeTrace
//          IORObjRef = ObjectToString( ObjRef1 ) ;
//          cdebug << ThreadNo() << "GraphExecutor::InNode::PyDynInvoke Node " << Name()
//                 << "ArgOut" << i << " : " << sname << " " << method << " ToObject "
//                 << IORObjRef << " (objref) " << endl ;
//#endif

//          ServicesAnyData anInParametersList ;
//          anInParametersList.Name = "anIORObjRef" ;
//          anInParametersList.Value <<= IORObjRef ;
//          DynInvoke( mySuperVisionComponent , "ToObject" ,
//                     &anInParametersList , 1 , &OutParametersList[i] , 1 ) ;
//#if PyDynInvokeTrace
//          OutParametersList[i].Value >>= ObjRef1 ;
//          IORObjRef = ObjectToString( ObjRef1 ) ;
//          cdebug << ThreadNo() << "GraphExecutor::InNode::PyDynInvoke Node " << Name()
//                 << "ArgOut" << i << " : " << sname << " " << method
//                 << " DynInvoke(ToObject) " << IORObjRef << " (objref) " << endl ;
//#endif
//        }
      }

#if PyDynInvokeTrace
      cdebug << "InNode::PyDynInvoke Result->ob_refcnt " << Result->ob_refcnt-1 << endl ;
#endif
      Py_DECREF( Result ) ;
    }

#if PyDynInvokeTrace
    cdebug << "InNode::PyDynInvoke ArgsList->ob_refcnt " << ArgsList->ob_refcnt-1 << endl ;
#endif
    Py_DECREF( ArgsList ) ;
  }

  fflush(stdout);
  fflush(stderr);

#if 0
  {
    int i ;
    for ( i = 0 ; i < nInParams ; i++ ) { // Without Gates
      cdebug << "InParametersList[" << i << "] : "
             << InParametersList[i].Name << " "
             << AnyValue( InParametersList[i].Value ) << endl ;
    }
    for ( i = 0 ; i < nOutParams ; i++ ) { // Without Gates
      cdebug << "OutParametersList[" << i << "] : "
             << OutParametersList[i].Name << " "
             << AnyValue( OutParametersList[i].Value ) << endl ;
    }
  }
#endif

#if PyDynInvokeTrace
  cdebug_out << "GraphExecutor::InNode::PyDynInvoke Node " << Name() << " method " << method << " " << RetVal
             << endl ;
#endif

  return RetVal ;

}

bool GraphExecutor::InNode::PyRunSimpleString( char* thePyString )
{
  const bool ErrorValue = true;
  bool aRet;
  try {
#if PyDynInvokeTrace
    MESSAGE( pthread_self() << "Python method beginning : " << thePyString );
    cdebug_in << pthread_self() << "Python method beginning : " << thePyString << endl ;
#endif
    aRet = PyRun_SimpleString( thePyString );
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
    //aRet = _OutNode->SuperVisionContainer()->ActivatePythonExecution( thePyString ) ;
#if PyDynInvokeTrace
    MESSAGE( pthread_self() << "Python method finished." );
    cdebug_out << pthread_self() << "Python method finished." << endl ;
#endif
  } catch( ... ) {
    MESSAGE( pthread_self() << "ERROR: Exception caught running Python method." );
    cdebug_out << pthread_self() << "ERROR: Exception caught running Python method."
               << endl ;
    MESSAGE( "       Python was reinitialized.  Previous Python definitions are lost" );
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
    Py_Finalize();
    Py_Initialize();
    Py_InitModule( "InitPyRunMethod" , MethodPyRunMethod );
    _OutNode->PyInitialized( false );
    aRet = ErrorValue;
  }
  return aRet;
}

PyObject * GraphExecutor::InNode::PyEvalCallObject( const char *method ,
                                                    PyObject * MyPyRunMethod ,
                                                    PyObject * ArgsList ) {
//  cdebug_in << "Executor::InNode::PyEvalCallObject " << Name() << endl ;
  PyObject * Result = NULL ;
  try {
//    MESSAGE( pthread_self() << "PyEval_CallObject " << Name() << " method beginning : " << method );
//    cdebug << pthread_self() << "PyEval_CallObject " << Name() << " method beginning : " << method << endl ;
    Result = PyEval_CallObject( MyPyRunMethod , ArgsList ) ;
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
    //Result = _OutNode->SuperVisionContainer()->ActivatePythonExecution( MyPyRunMethod , ArgsList ) ;
//    MESSAGE( pthread_self() << "PyEval_CallObject method finished. Result " << Result );
//    cdebug << pthread_self() << "PyEval_CallObject method finished. Result " << Result << endl ;
//    cdebug_out << "Executor::InNode::PyEvalCallObject " << Name() << endl ;
  } catch( ... ) {
    MESSAGE( pthread_self() << "ERROR: Exception caught PyEval_CallObject " << Name()
             << " Python method " << method << ". Result "  << Result );
    cdebug << pthread_self() << "ERROR: Exception caught PyEval_CallObject " << Name()
           << " Python method " << method << ". Result " << Result << endl ;
    MESSAGE( "       Python was reinitialized.  Previous Python definitions are lost Py_IsInitialized " << Py_IsInitialized() );
//JR ===> fatal error in python : no current thread
    // asv : 20.01.05 : changes involved with switching to old (HEAD) KERNEL    
    Py_Finalize();
    Py_Initialize();
    Automaton()->PyInitModule( false ) ;
    Py_InitModule( "InitPyRunMethod" , MethodPyRunMethod );
    _OutNode->PyInitialized( false );

    if ( Result == NULL ) {
      Kill() ; // Reset of _ThreadId
    }
//    cdebug << "Executor::InNode::PyEvalCallObject ERROR catched " << Name()
//           << " Py_IsInitialized " << Py_IsInitialized() << endl ;
  }
//  cdebug_out << "Executor::InNode::PyEvalCallObject " << Name() << endl ;
  return Result ;
}

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_GanvasNodePrs.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI_CanvasNodePrs.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_Canvas.h"
#include "SUPERVGUI_CanvasCellNodePrs.h"
#include "SUPERVGUI_Main.h"

#include "SUIT_ResourceMgr.h"

//#define CHECKTIME

#ifdef CHECKTIME
#include <sys/timeb.h>
#endif

//#define PORT_MARGIN 2
#undef PORT_HEIGHT // to avoid warning message
#define PORT_HEIGHT LABEL_HEIGHT
#define TEXT_MARGIN 5


//=====================================================================
// Node presentation
//=====================================================================
SUPERVGUI_CanvasNodePrs::SUPERVGUI_CanvasNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, 
						  SUPERVGUI_CanvasNode* theNode,
						  bool theCellPrs):
  QCanvasPolygonalItem(theCanvas),
  myNode(theNode),
  myMgr( mgr )
{
  Trace("SUPERVGUI_CanvasNodePrs::SUPERVGUI_CanvasNodePrs");
  myWidth = LABEL_WIDTH;
  if (2*(PORT_WIDTH+PORT_MARGIN) > myWidth)
    myWidth = 2*(PORT_WIDTH+PORT_MARGIN);

  myTitleHeight = LABEL_HEIGHT;
  myLabelHeight = LABEL_HEIGHT;
  myStatusHeight = LABEL_HEIGHT;
  myPortHeight = 2*PORT_MARGIN;
  myStreamHeight = 0;
  myGateHeight = PORT_HEIGHT + 2*PORT_MARGIN;

  myLabelVisible = true;
  myPortVisible = true;
  myCellPrs = theCellPrs;

  myColor = mgr->colorValue( "SUPERVGraph", "NodeBody", DEF_MAIN_BACK );

  if (!myCellPrs) {
    // create in/out connection points prs
    myPointIn = new SUPERVGUI_CanvasHookPrs(theCanvas, this, true);
    myPointOut = new SUPERVGUI_CanvasHookPrs(theCanvas, this, false);

    setZ(0);
    setState(myNode->getEngine()->State());
    updatePorts();
  }
}


SUPERVGUI_CanvasNodePrs::~SUPERVGUI_CanvasNodePrs() 
{
  if ( !myCellPrs ) {
    if ( myPointIn ) {
      delete myPointIn;
      myPointIn = 0;
    }
    if ( myPointOut ) {
      delete myPointOut;
      myPointOut = 0;
    }
  }
}

int SUPERVGUI_CanvasNodePrs::rtti() const
{
  return SUPERVGUI_Canvas::Rtti_Node;
}

int SUPERVGUI_CanvasNodePrs::width() const
{
  return myWidth;
}

int SUPERVGUI_CanvasNodePrs::height() const
{
  return getTitleHeight() + getLabelHeight() + getStatusHeight() +
    getBodyHeight() + getGateHeight();
}

int SUPERVGUI_CanvasNodePrs::getTitleHeight() const
{
  return myTitleHeight;
}

int SUPERVGUI_CanvasNodePrs::getLabelHeight() const
{
  if (isLabelVisible())
    return myLabelHeight;
  return 0;
}

int SUPERVGUI_CanvasNodePrs::getStatusHeight() const
{
  return myStatusHeight;
}

int SUPERVGUI_CanvasNodePrs::getBodyHeight() const
{
  if (isPortVisible())
    return myPortHeight + myStreamHeight;
  return 0;
}

int SUPERVGUI_CanvasNodePrs::getPortHeight() const
{
  if (isPortVisible())
    return myPortHeight;
  return 0;
}

int SUPERVGUI_CanvasNodePrs::getStreamHeight() const
{
  if (isPortVisible())
    return myStreamHeight;
  return 0;
}

int SUPERVGUI_CanvasNodePrs::getGateHeight() const
{
  if (isPortVisible())
    return myGateHeight;
  return 0;
}

QPointArray SUPERVGUI_CanvasNodePrs::areaPoints() const
{
  int w = width();
  int h = height()+1;

  QPointArray aPnts(4);
  aPnts[0] = QPoint((int)x(), (int)y());
  aPnts[1] = aPnts[0] + QPoint(w, 0);
  aPnts[2] = aPnts[1] + QPoint(0, h);
  aPnts[3] = aPnts[0] + QPoint(0, h);
  return aPnts;
}

QObject* SUPERVGUI_CanvasNodePrs::getObject(const QPoint& thePos) const
{
  QObject* anObj = myNode;

  // check if it's a port
  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	if (aPort->getPrs()->getPortRect().contains(thePos, true)) {
	  anObj = aPort;
	  break;
	}
      }
    }
  }
  return anObj;
}

void SUPERVGUI_CanvasNodePrs::moveBy(double dx, double dy) 
{
  int aX = (int) (x()+dx);
  int aY = (int) (y()+dy);
  int xx = aX - (int)x();
  int yy = aY - (int)y();

  int w = aX + width() + GRAPH_MARGIN;
  int h = aY + height() + GRAPH_MARGIN;
  if (canvas()->width() > w) w = canvas()->width();
  if (canvas()->height() > h) h = canvas()->height();
  if (canvas()->width() < w || canvas()->height() < h)
    canvas()->resize(w, h);

  // save new coordinates only if node is really moving...
  if (isMoving()) {
    myNode->getEngine()->Coords(aX, aY);
  }

  QCanvasPolygonalItem::moveBy(dx, dy);

  // update port's rectangle
  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	aPort->moveBy(xx, yy);
      }
    }
  }

  if (!myCellPrs) {
    myPointIn->moveBy(dx, dy);
    myPointOut->moveBy(dx, dy);
  }
}

void SUPERVGUI_CanvasNodePrs::setZ(double z)
{
  QCanvasItem::setZ(z);

  // update port's 
  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	aPort->getPrs()->setZ(z);
      }
    }
  }
}

void SUPERVGUI_CanvasNodePrs::hideAll()
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  setLabelVisible(false);
  setPortVisible(false);

  if (aDisp) {
    show();
    //    canvas()->update();
  }
}

void SUPERVGUI_CanvasNodePrs::showAll()
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  setLabelVisible(true);
  setPortVisible(true);

  if (aDisp) {
    show();
    //    canvas()->update();
  }
}

void SUPERVGUI_CanvasNodePrs::setLabelVisible(bool b)
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  myLabelVisible = b;
  updatePorts();
  if (!isPortVisible() && !myCellPrs) updatePoints();

  if (aDisp) {
    show();
    canvas()->update();
  }
}

void SUPERVGUI_CanvasNodePrs::setPortVisible(bool b)
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  myPortVisible = b;
  if (b) {
    if (!myCellPrs) {
      myPointIn->hide();
      myPointOut->hide();
    }

    updateGates();
  }
  else {
    if (!myCellPrs) {
      updatePoints();
      
      myPointIn->show();
      myPointOut->show();
    }
  }

  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	aPort->getPrs()->setVisible(b);
      }
    }
  }

  if (aDisp) {
    show();
    canvas()->update();
  }
}

void SUPERVGUI_CanvasNodePrs::setNodeColor(const QColor& theColor)
{
  myColor = theColor;
  canvas()->setChanged(getRect());
  canvas()->update();
}

void SUPERVGUI_CanvasNodePrs::updateInfo()
{
  canvas()->setChanged(getTitleRect());
  canvas()->setChanged(getLabelRect()); //node's comment is saved in engine (we can see it only 
                                        //with help of ChangeInformation dialog),
                                        //node's label isn't change
  canvas()->update();
}

void SUPERVGUI_CanvasNodePrs::updatePoints()
{
  QPoint in = getInConnectionPoint();
  myPointIn->setCoords(in.x(), in.y());

  QPoint out = getOutConnectionPoint();
  myPointOut->setCoords(out.x(), out.y());
}

void SUPERVGUI_CanvasNodePrs::updatePorts() 
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  QRect r = getBodyRect();
  int w = (int)(r.width()/2) - PORT_MARGIN; 
  if (w < PORT_WIDTH) w = PORT_WIDTH;

  int ix = r.x() + PORT_MARGIN;
  int ih = r.y() + PORT_MARGIN;
  int ox = ix + w;
  int oh = r.y() + PORT_MARGIN;

  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	if (!aPort->getEngine()->IsGate() && !aPort->isStream()) {
	  if (aPort->getEngine()->IsInput()) {
	    aPort->getPrs()->setPortRect(QRect(ix, ih, w, PORT_HEIGHT));
	    ih += PORT_HEIGHT;
	  }
	  else {
	    aPort->getPrs()->setPortRect(QRect(ox, oh, w, PORT_HEIGHT));
	    oh += PORT_HEIGHT;
	  }
	}
      }
    }
  }

  myPortHeight = (ih>oh?ih:oh) - r.y() + PORT_MARGIN;

  // update stream ports
  int sy = r.y() + myPortHeight;
  ih = sy + PORT_MARGIN;
  oh = sy + PORT_MARGIN;

  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	if (!aPort->getEngine()->IsGate() && aPort->isStream()) {
	  if (aPort->getEngine()->IsInput()) {
	    aPort->getPrs()->setPortRect(QRect(ix, ih, w, PORT_HEIGHT));
	    ih += PORT_HEIGHT;
	  }
	  else {
	    aPort->getPrs()->setPortRect(QRect(ox, oh, w, PORT_HEIGHT));
	    oh += PORT_HEIGHT;
	  }
	}
      }
    }
  }

  myStreamHeight = (ih>oh?ih:oh) - sy + PORT_MARGIN;
  if (myStreamHeight == 2*PORT_MARGIN) myStreamHeight = 0;

  // can update gates only after body height will be defined
  updateGates();

  if (aDisp) {
    show();
    canvas()->update();
  }
}

void SUPERVGUI_CanvasNodePrs::updateGates() 
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  QRect r = getGateRect();
  int w = (int)(r.width()/2) - PORT_MARGIN; 
  if (w < PORT_WIDTH) w = PORT_WIDTH;

  int ix = r.x() + PORT_MARGIN;
  int ih = r.y() + PORT_MARGIN;
  int ox = ix + w;
  int oh = r.y() + PORT_MARGIN;

  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	if (aPort->getEngine()->IsGate()) {
	  if (aPort->getEngine()->IsInput()) {
	    aPort->getPrs()->setPortRect(QRect(ix, ih, w, PORT_HEIGHT));
	    ih += PORT_HEIGHT;
	  }
	  else {
	    aPort->getPrs()->setPortRect(QRect(ox, oh, w, PORT_HEIGHT));
	    oh += PORT_HEIGHT;
	  }
	}
      }
    }
  }
  
  myGateHeight = (ih>oh?ih:oh) - r.y() + PORT_MARGIN;

  if (aDisp) show();
}

QRect SUPERVGUI_CanvasNodePrs::getRect() const
{
  return QRect((int)x(), (int)y(), width(), height());
}

QRect SUPERVGUI_CanvasNodePrs::getTitleRect() const
{
  return QRect((int)x(), (int)y(), width(), getTitleHeight());
}

QRect SUPERVGUI_CanvasNodePrs::getLabelRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight(), width(), getLabelHeight());
}

QRect SUPERVGUI_CanvasNodePrs::getStatusRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight()+getLabelHeight(),
	       width(), getStatusHeight());
}

QRect SUPERVGUI_CanvasNodePrs::getBodyRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight()+getLabelHeight()+getStatusHeight(), 
	       width(), getBodyHeight());
}

QRect SUPERVGUI_CanvasNodePrs::getGateRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight()+getLabelHeight()+
	       getStatusHeight()+getBodyHeight(), 
	       width(), getGateHeight());
}

QPoint SUPERVGUI_CanvasNodePrs::getInConnectionPoint() const
{
  QRect r = getGateRect();
  int h;
  if (isPortVisible()) {
    h = (int)((r.top()+r.bottom())/2);
  }
  else {
    h = (int)(y()+height()/2);
  }
  return QPoint(r.left()-POINT_SIZE, h);
}

QPoint SUPERVGUI_CanvasNodePrs::getOutConnectionPoint() const
{
  QRect r = getGateRect();
  int h;
  if (isPortVisible()) {
    h = (int)((r.top()+r.bottom())/2);
  }
  else {
    h = (int)y() + (int)(height()/2);
  }
  return QPoint(r.right()+POINT_SIZE, h);
}

void drawText(QPainter& thePainter, const QString& theText, 
	      const QRect& theRect, int theHAlign = Qt::AlignAuto)
{
  int flags = theHAlign | Qt::AlignVCenter;
  QRect r(theRect.x() + TEXT_MARGIN, theRect.y(), 
	  theRect.width() - 2*TEXT_MARGIN, theRect.height());

  QWMatrix aMat = thePainter.worldMatrix();
  if (aMat.m11() != 1.0) { 
    // for scaled picture only
    QRect r1 = aMat.mapRect(r);
    QFont saved = thePainter.font();
    QFont f(saved);
    if (f.pointSize() == -1) {
      f.setPixelSize((int)(f.pixelSize()*aMat.m11()));
    }
    else {
      f.setPointSize((int)(f.pointSize()*aMat.m11()));
    }
    thePainter.save();
    QWMatrix m;
    thePainter.setWorldMatrix(m);
    thePainter.setFont(f);
    thePainter.drawText(r1, flags, theText);
    thePainter.setFont(saved);
    thePainter.restore();
  }
  else {
    thePainter.drawText(r, flags, theText);
  }
}

void SUPERVGUI_CanvasNodePrs::draw(QPainter& thePainter) 
{
  thePainter.setPen(pen());
  thePainter.setBrush(nodeColor());
  if ( !CORBA::is_nil( myNode->getMain()->getDataflow()->Node( myNode->name() ) ) ) // mkr : IPAL11360
    drawShape(thePainter);
}

void SUPERVGUI_CanvasNodePrs::drawShape(QPainter& thePainter) 
{
  drawTitle(thePainter);
  if (isLabelVisible()) {
    drawLabel(thePainter);
  }
  if (isPortVisible()) {
    drawPort(thePainter);
    drawGate(thePainter);
  }
  drawStatus(thePainter);
  drawFrame(thePainter);
}

void SUPERVGUI_CanvasNodePrs::drawFrame(QPainter& thePainter) 
{
  /* it was a good idea, but...
     drawed polyline is out of item boundaries :-(
  QPointArray pnts = areaPoints();
  int n = pnts.size();
  pnts.resize(n+1);
  pnts[n] = pnts[0];
  thePainter.drawPolyline(pnts);
  */

  QBrush saved = thePainter.brush();
  thePainter.setBrush(NoBrush);
  thePainter.drawRect(getRect());
  thePainter.setBrush(saved);
}

void SUPERVGUI_CanvasNodePrs::drawTitle(QPainter& thePainter) 
{
  QBrush saved = thePainter.brush();
  QBrush br( myMgr->colorValue( "SUPERVGraph", "Title", DEF_MAIN_TITLE ) );
  thePainter.setBrush(br);
  drawTitleShape(thePainter);
  thePainter.setBrush(saved);

  drawText(thePainter, myNode->getEngine()->Name(), getTitleRect(), Qt::AlignHCenter);
}

void SUPERVGUI_CanvasNodePrs::drawTitleShape(QPainter& thePainter) 
{
  thePainter.drawRect(getTitleRect());
}

void SUPERVGUI_CanvasNodePrs::drawLabel(QPainter& thePainter) 
{
  QRect r = getLabelRect();

  QPen saved = thePainter.pen();
  thePainter.setPen(NoPen);
  thePainter.drawRect(r);
  thePainter.setPen(saved);

  //  drawText(thePainter, myNode->getEngine()->Comment(), r);
  drawText(thePainter, myNode->getLabelText(), r);
}

void SUPERVGUI_CanvasNodePrs::drawStatus(QPainter& thePainter) 
{
  QRect r = getStatusRect();
  if (isPortVisible())
    r.setHeight(r.height()+1);

  QBrush savedB = thePainter.brush();
  thePainter.setBrush(myStatusColor);
  drawStatusShape(thePainter);
  thePainter.setBrush(savedB);

  QRect r1(r.x(), r.y(), (int)r.width()/2, r.height());
  drawText(thePainter, myStatus, r1, Qt::AlignHCenter);

  QRect r2(r.x()+r.width()-r1.width(), r.y(), r1.width(), r.height());
  drawText(thePainter, myTime, r2, Qt::AlignHCenter);
}

void SUPERVGUI_CanvasNodePrs::drawStatusShape(QPainter& thePainter) 
{
  QRect r = getStatusRect();
  if (isPortVisible())
    r.setHeight(r.height()+1);
  thePainter.drawRect(r);
}

void SUPERVGUI_CanvasNodePrs::drawPort(QPainter& thePainter) 
{
  QRect r = getBodyRect();
  r.setHeight(r.height()+1);

  thePainter.drawRect(r);
  int x0 = (r.left() + r.right())/2;
  thePainter.drawLine(x0, r.top(), x0, r.bottom());
  if (getStreamHeight() > 0) {
    int y0 = r.top() + getPortHeight();
    thePainter.drawLine(r.left(), y0, r.right(), y0);
  }

  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;

	SUPERV_Port aPortEng = aPort->getEngine();
	//	if ( CORBA::is_nil( aPortEng ) )
	//	  printf ( "---\n port engine IS NIL ---\n\n" );

	if ( !CORBA::is_nil( aPortEng ) && !aPortEng->IsGate() ) {
	  aPort->getPrs()->draw(thePainter);
	}
      }
    }
  }
}

void SUPERVGUI_CanvasNodePrs::drawGate(QPainter& thePainter) 
{
  QBrush saved = thePainter.brush();
  thePainter.setBrush(green.light(170));

  QRect r = getGateRect();
  //  r.setHeight(r.height()+1);
  thePainter.drawRect(r);
  //int x0 = (r.left() + r.right())/2;
  //  thePainter.drawLine(x0, r.top(), x0, r.bottom());

  const QObjectList* list = myNode->children();
  if (list) {
    QObjectListIt it(*list);
    SUPERVGUI_CanvasPort* aPort;
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	aPort = (SUPERVGUI_CanvasPort*) obj;
	if (aPort->getEngine()->IsGate()) {
	  aPort->getPrs()->draw(thePainter);
	}
      }
    }
  }

  thePainter.setBrush(saved);
}

void SUPERVGUI_CanvasNodePrs::setState(SUPERV::GraphState theState)
{
  switch(theState) {
  case SUPERV_Waiting:
    myStatus = "Waiting";
    myStatusColor = QColor(35, 192, 255);
    break;

  case SUPERV_Running:
  case SUPERV::ReadyState:
    myStatus = "Running";
    myStatusColor = QColor(32,210,32);
    break;

  case SUPERV_Suspend:
  case SUPERV::SuspendReadyState:
    myStatus = "Suspended";
    myStatusColor = QColor(255,180, 0);
    break;

  case SUPERV_Done:
    myStatus = "Finished";
    myStatusColor = QColor(255, 158, 255);
    break;

  case SUPERV_Error: 
    myStatus = "Aborted";
    myStatusColor = red;
    break;

  case SUPERV_Kill:
    myStatus = "Killed";
    myStatusColor = red;
    break;

  case SUPERV::LoadingState:
    myStatus = "Loading";
    myStatusColor = QColor(56,255,56);
    break;

  default:
    myStatus = "No Status";
    myStatusColor = myMgr->colorValue( "SUPERVGraph", "NodeBody", DEF_MAIN_BACK );
    break;
  }

  long sec = 0;
  // IPAL9273, 9369, 9731
  if ( theState != SUPERV_Kill && myNode->getMain()->getDataflow()->State() != SUPERV_Kill )
    sec = myNode->getEngine()->CpuUsed();
  char hms[9];
  long s = sec/3600;
  hms[0]=(char)(((s/10)%10)+48);
  hms[1]=(char)((s%10)+48);
  hms[2]=':';
  sec = sec%3600;
  s = sec/60;
  hms[3]=(char)((s/10)+48);
  hms[4]=(char)((s%10)+48);
  hms[5]=':';
  sec = sec%60;
  hms[6]=(char)((sec/10)+48);
  hms[7]=(char)((sec%10)+48);
  hms[8]='\0';
  myTime = QString(hms);

  canvas()->setChanged(getStatusRect());
  canvas()->update();
}


//=====================================================================
// Port presentation
//=====================================================================
SUPERVGUI_CanvasPortPrs::SUPERVGUI_CanvasPortPrs(QCanvas* theCanvas, 
						 SUPERVGUI_CanvasPort* thePort):
  myCanvas(theCanvas), myPort(thePort)
{
  myText = getText();
  myVisible = true;
  myPoint = new SUPERVGUI_CanvasHookPrs(theCanvas, this, myPort->getEngine()->IsInput());
}

SUPERVGUI_CanvasPortPrs::~SUPERVGUI_CanvasPortPrs()
{
  if (myPoint) delete myPoint;
}

void SUPERVGUI_CanvasPortPrs::setVisible(bool b)
{
  if (myPoint) myPoint->setVisible(b);
  myVisible = b;
  myPort->updateLinks();
}

void SUPERVGUI_CanvasPortPrs::setPortRect(const QRect& theRect)
{
  myRect = theRect;
  QPoint aPnt = getConnectionPoint();
//   int dx = 0;
//   if (myPort->getEngine()->IsInput()) 
//     dx = POINT_SIZE;
//   else
//     dx = - POINT_SIZE;
//   QPoint aPnt2(aPnt.x()+dx, aPnt.y());

//   if (myLine) {
//     myLine->move(0, 0);
//     myLine->setPoints(aPnt2.x(), aPnt2.y(), aPnt.x(), aPnt.y());
//     if (myVisible) myLine->show();
//   }
  if (myPoint) {
    myPoint->setCoords(aPnt.x(), aPnt.y());
    //    myPoint->move(aPnt.x(), aPnt.y());
    if (myVisible) myPoint->show();
  }
  myPort->updateLinks();
}

void SUPERVGUI_CanvasPortPrs::moveBy(int dx, int dy)
{
  myRect.moveBy(dx, dy);
  if (myPoint) myPoint->moveBy(dx, dy);
  //  if (myLine) myLine->moveBy(dx, dy);
}

void SUPERVGUI_CanvasPortPrs::setZ(double z)
{
  if (myPoint) myPoint->setZ(z);
  //  if (myLine) myLine->setZ(z);
}

void SUPERVGUI_CanvasPortPrs::update(bool theForce)
{
  QString aNewText = getText();
  if (theForce || myText.compare(aNewText) != 0) {
    myText = aNewText;
    myCanvas->setChanged(myRect);
  }
}

bool SUPERVGUI_CanvasPortPrs::isHilight() const
{
  SUPERV_Port aPort = myPort->getEngine();
  bool b = false;
  if (!aPort->IsGate()) {
    if (aPort->IsInput()) {
      if (aPort->HasInput() && !aPort->IsLinked())
	b = true;
    }
    else if (myPort->inherits("SUPERVGUI_CanvasPortOut")) {
      SUPERVGUI_CanvasPortOut* aPortOut = (SUPERVGUI_CanvasPortOut*) myPort;
      if (aPortOut->isInStudy())
	b = true;
    }
  }
  return b;
}

bool SUPERVGUI_CanvasPortPrs::isAlert() const
{
  bool b = false;
  SUPERV_Port aPort = myPort->getEngine();
  if (!aPort->IsGate()) {
    if (aPort->IsInput() && !aPort->HasInput() && !aPort->IsLinked())
      b = true;
  }
  return b;
}

QString SUPERVGUI_CanvasPortPrs::getText() const
{
  SUPERV_Port aPort = myPort->getEngine();
  QString aText;
  if ( !CORBA::is_nil( aPort ) ) {
    aText = aPort->Name();
    if (aPort->IsParam() || aPort->IsInLine() || myPort->isStream()) {
      // mkr : modifications to avoid displaying long IOR's values on nodes' ports,
      //       display ports type from corresponding XML catalog instead.
      QString aPortValue = aPort->ToString();
      QString aPortType = aPort->Type();
      if ( aPortType.compare("string") // not "string" type
	   &&
	   aPortValue.find("IOR:") >= 0 ) // has IOR:... in value
	aText = aText + "=" + aPortType;
      else
	aText = aText + "=" + aPortValue;
    }
  }
  //  printf( "--- return text of port : %s ---\n", aText.latin1() );
  return aText;
}

int SUPERVGUI_CanvasPortPrs::getAlignment() const
{
  int a = Qt::AlignAuto;
  SUPERV_Port aPort = myPort->getEngine();
  /*  Merge to Ecole_Ete
  if (QString(aPort->Name()).compare(OUTVOID) == 0)
    a = Qt::AlignRight;
  */
  if (aPort->IsGate()) {
    if (aPort->IsInput())
      a = Qt::AlignLeft;
    else
      a = Qt::AlignRight;
  }
  return a;
}

QPoint SUPERVGUI_CanvasPortPrs::getConnectionPoint() const
{
  int x, y;
  if (myPort->getEngine()->IsInput())
    x = myRect.left() - PORT_MARGIN - POINT_SIZE;
  else
    x = myRect.right() + PORT_MARGIN + POINT_SIZE;
  y = (int)(myRect.top() + myRect.bottom())/2;
  return QPoint(x, y);
}

void SUPERVGUI_CanvasPortPrs::draw(QPainter& thePainter)
{
  /*
  QPen savedP = thePainter.pen();
  QBrush savedB = thePainter.brush();

  QPen aPen(savedP.color(), getLineWidth());
  thePainter.setPen(aPen);
  thePainter.setBrush(Qt::NoBrush);
  thePainter.drawRect(myRect);
  thePainter.setPen(savedP);
  thePainter.setBrush(savedB);
  */
  QFont saved = thePainter.font();
  QFont f(saved);
  f.setBold(isHilight());
  thePainter.setFont(f);

  QPen savedP = thePainter.pen();
  if (myPort->isStream())
    thePainter.setPen(QColor(128, 64, 0));// Qt::darkCyan);
  else if (isHilight())
    thePainter.setPen(Qt::blue);
  else if (isAlert())
    thePainter.setPen(Qt::red.dark(120));

  drawText(thePainter, myText, myRect, getAlignment());

  thePainter.setPen(savedP);
  thePainter.setFont(saved);
}


//=====================================================================
// Node presentation
//=====================================================================
SUPERVGUI_CanvasHookPrs::SUPERVGUI_CanvasHookPrs(QCanvas* theCanvas, 
						 SUPERVGUI_CanvasNodePrs* theNode,
						 const bool& theIn):
  QCanvasEllipse(POINT_SIZE, POINT_SIZE, theCanvas),
  myNodePrs(theNode), myPortPrs(0), myIn(theIn), myLine(0)
{
  init(theCanvas);
}

SUPERVGUI_CanvasHookPrs::SUPERVGUI_CanvasHookPrs(QCanvas* theCanvas, 
						 SUPERVGUI_CanvasPortPrs* thePort,
						 const bool& theIn):
  QCanvasEllipse(POINT_SIZE, POINT_SIZE, theCanvas),
  myNodePrs(0), myPortPrs(thePort), myIn(theIn), myLine(0)
{
  init(theCanvas);
}

void SUPERVGUI_CanvasHookPrs::init(QCanvas* theCanvas)
{
  myLine = new QCanvasLine(theCanvas);

  setBrush(Qt::black);
  myLine->setPen(QPen(Qt::black, 1));

  setZ(0);
}

SUPERVGUI_CanvasHookPrs::~SUPERVGUI_CanvasHookPrs()
{
  hide();
  if ( myLine ) {
    delete myLine;
    myLine = 0;
  }
}

QObject* SUPERVGUI_CanvasHookPrs::getObject() const
{
  QObject* anObj = 0;
  if ( myNodePrs )
    anObj = myNodePrs->getNode();
  else if ( myPortPrs )
    anObj = myPortPrs->getPort();
  return anObj;
}

void SUPERVGUI_CanvasHookPrs::setCoords(int x, int y)
{
  move(x, y);
  if (myLine) {
    myLine->move(0, 0);
    myLine->setPoints(x+(myIn?POINT_SIZE:-POINT_SIZE), y, x, y);
  }
}

void SUPERVGUI_CanvasHookPrs::setVisible(bool b)
{
  QCanvasEllipse::setVisible(b);
  if (myLine) myLine->setVisible(b);
}

void SUPERVGUI_CanvasHookPrs::moveBy(double dx, double dy)
{
  QCanvasEllipse::moveBy(dx, dy);
  if (myLine) myLine->moveBy(dx, dy);
}

void SUPERVGUI_CanvasHookPrs::setZ(double z)
{
  QCanvasEllipse::setZ(z);
  if (myLine) myLine->setZ(z);
}

int SUPERVGUI_CanvasHookPrs::rtti() const
{
  return SUPERVGUI_Canvas::Rtti_Hook;
}

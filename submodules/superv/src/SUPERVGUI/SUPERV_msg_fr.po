#  SUPERV SUPERVGUI : GUI for Supervisor component
#
#  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
# 
#  This library is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU Lesser General Public 
#  License as published by the Free Software Foundation; either 
#  version 2.1 of the License. 
# 
#  This library is distributed in the hope that it will be useful, 
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
#  Lesser General Public License for more details. 
# 
#  You should have received a copy of the GNU Lesser General Public 
#  License along with this library; if not, write to the Free Software 
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
# 
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#
#
#  File   : SupervisionGUI_msg_fr.po
#  Module : SUPERV

msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2001-09-11 12:08:44 PM CEST\n"
"PO-Revision-Date: 2004-12-02 16:17+0300\n"
"Last-Translator: FULLNAME <EMAIL@ADRESS>\n"
"Content-Type: text/plain; charset=iso-8859-1\n"

#Title of error messages
msgid "ERROR"
msgstr "Supervision Error"

#:SUPERVGUI_Main.cxx:246
msgid "MSG_FILE_EXIST"
msgstr "File '%1' exists.\nReplace?"

#:SUPERVGUI_Main.cxx:252
msgid "MSG_BAD_WRITING"
msgstr "Export failed.  Backup copy of overwritten file is stored in '%1'"

#:SUPERVGUI_Main.cxx:242
msgid "TTL_EXPORT_DATAFLOW"
msgstr "Export Dataflow As..."

#:SUPERVGUI_Main.cxx:41
msgid "MSG_ACCESS_BAD_IOR"
msgstr "Can't Access Dataflow: bad IOR"

#:SUPERVGUI_Main.cxx:47
msgid "MSG_NOACCESS_BY_IOR"
msgstr "Can't Access Dataflow by IOR"

#:SUPERVGUI_Main.cxx:53
msgid "MSG_CANT_CREATE_DF"
msgstr "Can't Create a New Dataflow"

#:SUPERVGUI_Main.cxx:72
msgid "MSG_BAD_FILE"
msgstr "Bad File"

#:SUPERVGUI_Main.cxx:86
msgid "MSG_CANT_COPY"
msgstr "Can't Copy Dataflow"

#:SUPERVGUI_Main.cxx:102
msgid "MSG_DEL_LAST_PNT"
msgstr "Delete Last Point"

#:SUPERVGUI_Main.cxx:103
msgid "MSG_DEL_LINK"
msgstr "Delete Link"

#:SUPERVGUI_Main.cxx:104
msgid "MSG_ORTHO_LINE"
msgstr "Ortho Mode"

#:SUPERVGUI_Main.cxx:146
msgid "WARNING"
msgstr "Supervision Warning"

#:SUPERVGUI_Main.cxx:147
msgid "MSG_DF_RUNNING"
msgstr "Dataflow Is Currently Running"

#:SUPERVGUI_Main.cxx:148
msgid "MSG_DF_EXECUTION"
msgstr "Background Execution"

#:SUPERVGUI_Main.cxx:149
msgid "MSG_DF_KILL"
msgstr "Kill Execution"

#:SUPERVGUI_Main.cxx:185
msgid "GRAPH_TITLE"
msgstr "Supervision: "

#:SUPERVGUI_Main.cxx:195
msgid "MSG_GRAPH_STARTED"
msgstr "===> Dataflow Execution is started"

#:SUPERVGUI_Main.cxx:191
msgid "MSG_GRAPH_READONLY"
msgstr "===> Dataflow is Read Only"

#:SUPERVGUI_Main.cxx:191
msgid "MSG_GRAPH_EDITING"
msgstr "===> Dataflow is Modifiable"

#:SUPERVGUI_Main.cxx:200
msgid "MSG_GRAPH_SUSPENDED"
msgstr "===> Dataflow Execution is Suspended"

#:SUPERVGUI_Main.cxx:204
msgid "MSG_GRAPH_FINISHED"
msgstr "===> Dataflow Execution is Finished"

#:SUPERVGUI_Main.cxx:208
msgid "MSG_GRAPH_ABORTED"
msgstr "===> Dataflow Execution is Aborted"

#:SUPERVGUI_Main.cxx:212
msgid "MSG_GRAPH_KILLED"
msgstr "===> Dataflow Execution is Killed"

#:SUPERVGUI_Main.cxx:265
msgid "MSG_GRAPH_INSERT"
msgstr "Insert a Dataflow"

#:SUPERVGUI_Main.cxx:285
msgid "MSG_DF_NOTVALID"
msgstr "Dataflow Not Valid"

#:SUPERVGUI_Main.cxx:287
msgid "MSG_DF_NOTEXECUTABLE"
msgstr "Dataflow Not Executable"

#:SUPERVGUI_Main.cxx:289
msgid "MSG_DF_BADEXECUTE"
msgstr "Dataflow Bad Execute"

#:SUPERVGUI_Main.cxx:301
msgid "MSG_DF_NOTRUNNING"
msgstr "Dataflow Is Not Running"

#:SUPERVGUI_Main.cxx:305
msgid "MSG_CANTKILL_DF"
msgstr "Can't Kill Dataflow"

#:SUPERVGUI_Main.cxx:315
msgid "MSG_DF_RESUMED"
msgstr "===> Dataflow Execution is Resumed"

#:SUPERVGUI_Main.cxx:318
msgid "MSG_CANT_RESUME"
msgstr "Can't Resume Dataflow"

#:SUPERVGUI_Main.cxx:324
msgid "MSG_CANT_SUSPEND"
msgstr "Can't Suspend Dataflow"

#:SUPERVGUI_Main.cxx:335
msgid "MSG_DF_RESTARTED"
msgstr "===> Dataflow Execution is Restarted"

#:SUPERVGUI_Main.cxx:338
msgid "MSG_CANT_RESTART"
msgstr "Can't Restart Dataflow"

#:SUPERVGUI_Main.cxx:344
msgid "MSG_CANT_STOP"
msgstr "Can't Stop Dataflow"

#:SUPERVGUI_Main.cxx:477
msgid "MSG_CHOOSE_OUTPUT"
msgstr "Choose an Output Port"

#:SUPERVGUI_Main.cxx:477
msgid "MSG_CHOOSE_INPUT"
msgstr "Choose an Input Port"

#:SUPERVGUI_Main.cxx:486
msgid "MSG_CANT_CREATE_LINK"
msgstr "Can't Create This Link"

#:SUPERVGUI_Main.cxx:508
msgid "MSG_CANT_CREATE_POINT"
msgstr "Can't Create Point(s) For This Link"

#:SUPERVGUI_Main.cxx:541
msgid "MSG_RIGHTLINE"
msgstr "Right Line"

#:SUPERVGUI_Main.cxx:541
msgid "MSG_FREEPOINT"
msgstr "Free Point"

#:SUPERVGUI_Main.cxx:627
msgid "MSG_CHOOSE_DATA"
msgstr "Choose a Data in Study"

#:SUPERVGUI_Main.cxx:170
msgid "MSG_GRAPH_DISPLAYED"
msgstr "The DataFlow %1 \nis already displayed. Open again?"

#:SUPERVGUI_Main.cxx:627
msgid "MSG_NOTHING_COPY"
msgstr "Nothing to copy."

msgid "MSG_CANT_CHANGELINK"
msgstr "Can't Change Point Coordinates For a Link"

msgid "MSG_CANT_CHANGE_COORDLINK"
msgstr "Can't Change Coordinates For a Link"

msgid "MSG_CANT_DEL_POINT"
msgstr "Can't Delete this Point"

msgid "MSG_CANT_ADD_POINT"
msgstr "Can't Add this Point"

msgid "MSG_CANT_GET_LNKCOORD"
msgstr "Can't Get Coordinates From a Link"

msgid "MSG_NOT_IMPLEMENTED"
msgstr "Not Yet Implemented"

msgid "MSG_CANT_STOPNODE"
msgstr "Can't Stop Node"

msgid "MSG_CANT_RESTARTNODE"
msgstr "Can't Restart Node"

msgid "MSG_CANT_KILLNODE"
msgstr "Can't Kill Node"

msgid "MSG_CANT_SUSPENDNODE"
msgstr "Can't Suspend Node"

msgid "MSG_CANT_RESUMENODE"
msgstr "Can't Resume Node"

msgid "MSG_CANT_RENAMENODE"
msgstr "Can't Rename this Node"

msgid "MSG_CHANGE_NODENAME"
msgstr "Change the Name of a Node"

msgid "MSG_ENTER_NODENAME"
msgstr "Please, Enter a Node Name"

msgid "MSG_STOP"
msgstr "Stop"

msgid "MSG_RESTART"
msgstr "Restart"

msgid "MSG_SUSPEND"
msgstr "Suspend"

msgid "MSG_RESUME"
msgstr "Resume"

msgid "MSG_KILL"
msgstr "Kill"

msgid "MSG_DELETE"
msgstr "Delete"

msgid "MSG_RENAME"
msgstr "Rename"

msgid "MSG_CONFIGURE"
msgstr "Configure"

msgid "MSG_CHANGE_INFO"
msgstr "Change Informations"

msgid "MSG_SHOW_PYTHON"
msgstr "Show Python"

msgid "MSG_ADD_BEFORE"
msgstr "Add Before"

msgid "MSG_ADD_AFTER"
msgstr "Add After"

msgid "MSG_DEL_POINT"
msgstr "Delete Point"

msgid "MSG_NONODE_TOADD"
msgstr "No Node to Add in Dataflow"

msgid "MSG_CANT_CHOOSE_SERVICE"
msgstr "Can't Choose a Service in a Component"

msgid "MSG_NO_SUPERVISION_WINDOW"
msgstr "No Supervision Window Selected to Add Nodes in a Dataflow"

msgid "MSG_SKETCH_LINK"
msgstr "Sketch Link"

msgid "MSG_FROMSTUDY"
msgstr "Set From Study"

msgid "MSG_SETVALUE"
msgstr "Set Value"

msgid "MSG_BROWSE"
msgstr "Browse"

msgid "MSG_DELLINK"
msgstr "Delete Link"

msgid "MSG_CANT_SETVAL"
msgstr "Can't Set Input Value"

msgid "TIT_INPUT_PORT"
msgstr "Constant for Input Port"

msgid "MSG_ENTER_VALUE"
msgstr "Please, Enter a Value"

msgid "MSG_IPORT_VAL"
msgstr "Input Port Value : "

msgid "MSG_OPORT_VAL"
msgstr "Output Port Value : "

msgid "MSG_INFO"
msgstr "Supervision Information"

msgid "MSG_TERMINATE"
msgstr "Terminate"

msgid "MSG_SHOWDATA"
msgstr "Show Data"

msgid "MSG_CALL_DATASHOW"
msgstr "Call data show as soon ..."

msgid "MSG_PUT_INSTUDY"
msgstr "Put in Study"

msgid "MSG_NOT_INSTUDY"
msgstr "Not in Study"

msgid "MSG_GRAPH_ISRUN"
msgstr "Dataflow is still executing. Kill it?"

msgid "MSG_NOWINDOW_TO_SUSPEND"
msgstr "No Supervision Window Selected to Suspend or Resume a Dataflow"

msgid "MSG_NOWINDOW_TO_ADD"
msgstr "No Supervision Window Selected to add new components"

msgid "MSG_NOWINDOW_TO_EXPORT"
msgstr "No Supervision Window Selected to Export a Dataflow"

msgid "MSG_CANT_LOAD_SUPERV"
msgstr "Cannot Find or Load Supervision Component"

msgid "MSG_CANT_NARROW_SUPERV"
msgstr "Cannot _narrow Supervision Component"

msgid "MSG_NOWINDOW_TO_RELOAD"
msgstr "No Supervision Window Selected to Reload a Dataflow"

msgid "MSG_NOWINDOW_TO_RUN"
msgstr "No Supervision Window Selected to Run a Dataflow"

msgid "MSG_NOWINDOW_TO_KILL"
msgstr "No Supervision Window Selected to Kill a Dataflow"

msgid "MSG_IMPORT"
msgstr "Import Dataflow"

msgid "MSG_NOTEDITABLE"
msgstr "Current Dataflow is not editable"

msgid "MSG_ADD_STUDY"
msgstr "Add in Study"

msgid "MSG_NO_DELLINK"
msgstr "No Link to Delete"

msgid "MSG_NO_LINK_ADDPNT"
msgstr "No Link to Add a Point"

msgid "MSG_ADD_NODE"
msgstr "Add Node"

msgid "MSG_ADD_POINT"
msgstr "Add Point"

msgid "MSG_INS_FILE"
msgstr "Insert File"

msgid "MSG_COPY_DATAFLOW"
msgstr "Copy Dataflow"

msgid "MSG_SWITCH_ARRAY"
msgstr "Switch Array"

msgid "MSG_FILTER_NOTIFY"
msgstr "Filter Notification"

msgid "MSG_SWITCH_GRAPH"
msgstr "Switch Graph"

msgid "MSG_COPY_PREFIX"
msgstr "Copy%1_Of_"

msgid "TIT_RENAME"
msgstr "Rename Dataflow"

msgid "MSG_ASK_DELETE"
msgstr "Selected object and its subobjects will be deleted.\nContinue?"

msgid "POP_VIEW"
msgstr "View"

msgid "POP_FULLVIEW"
msgstr "Full"

msgid "POP_CONTROLVIEW"
msgstr "Control"

msgid "POP_TABLEVIEW"
msgstr "Table"

msgid "POP_SHOW"
msgstr "Show"

msgid "POP_SHOWTITLES"
msgstr "Titles"

msgid "POP_SHOWPORTS"
msgstr "Ports"

msgid "TIT_MANAGE_PORTS"
msgstr "Edit Ports : "

msgid "INPUT"
msgstr "Input"

msgid "OUTPUT"
msgstr "Output"

msgid "TYPE_LBL"
msgstr "Type:"

msgid "MNU_INPUT"
msgstr "Input"

msgid "MNU_OUTPUT"
msgstr "Output"

msgid "MNU_EDIT_FUNC"
msgstr "Edit Function"

msgid "MNU_ADD_PORT"
msgstr "Add Port"

msgid "MNU_MANAGE_PORTS"
msgstr "Edit Ports"

msgid "INLINE_COMP"
msgstr "Computation"

msgid "INLINE_SWTC"
msgstr "Switch"

msgid "INLINE_LOOP"
msgstr "Loop"

msgid "INLINE_GOTO"
msgstr "GoTo"

msgid "TLT_INVALID_LINK"
msgstr "Incompatible types"

msgid "MSG_INVALID_LINK"
msgstr "Warning: the link connect ports of incompatible types.\nYou may keep it and edit types of ports before execution\nor you may remove it now."


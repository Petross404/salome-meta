//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasPort.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasPort_H
#define SUPERVGUI_CanvasPort_H

#include "SUPERVGUI_Def.h"


class SUPERVGUI_Main;
class SUPERVGUI_CanvasLink;
class SUPERVGUI_CanvasPortPrs;

class SUPERVGUI_CanvasPort : public QObject {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasPort(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::Port_ptr thePort);
    virtual ~SUPERVGUI_CanvasPort();

    virtual void update();
    virtual void sync();

    virtual bool isStream() const { return false; }

    SUPERV_Port getEngine() const { return myPort; }
    SUPERVGUI_Main* getMain() const { return myMain; }
    SUPERVGUI_CanvasPortPrs* getPrs() const;

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

    virtual QPoint getConnectionPoint() const;

    void moveBy(int dx, int dy);
    void addLink(SUPERVGUI_CanvasLink* theLink);
    void removeLink(SUPERVGUI_CanvasLink* theLink);
    void updateLinks();

  signals:
    void objectDeleted(); // mkr : PAL8237

  public slots:
    void sketchLink();
    virtual void remove();
    void browse();
    void copy();

  protected:
    virtual SUPERVGUI_CanvasPortPrs* createPrs() const;

  private:
    SUPERV_Port      myPort;
    SUPERVGUI_Main*  myMain;

    SUPERVGUI_CanvasPortPrs* myPrs;
    QValueList<SUPERVGUI_CanvasLink*> myLinks;
    bool isIgnore;
};

class SUPERVGUI_GetValueDlg;
class SUPERVGUI_CanvasPortIn : public SUPERVGUI_CanvasPort {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasPortIn(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::Port_ptr thePort);
    virtual ~SUPERVGUI_CanvasPortIn();

    void setValue(const char* theValue);

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  public slots:
    void setInput();

  protected:
    bool eventFilter(QObject* o, QEvent* e);

  private:
    SUPERVGUI_GetValueDlg* myDlg;
};

class SUPERVGUI_CanvasPortOut : public SUPERVGUI_CanvasPort {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasPortOut(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::Port_ptr thePort);
    virtual ~SUPERVGUI_CanvasPortOut();

    void sync();
    bool isInStudy() const { return myInStudy; }
    void setStudyState(bool theInStudy) { myInStudy = theInStudy; }

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  signals:
    void putInStudy( QString* ); // mkr : PAL8150

  public slots:
    void toStudy();

  private:
    bool myInStudy;
};

class SUPERVGUI_CanvasStreamPortIn : public SUPERVGUI_CanvasPortIn {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasStreamPortIn(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::StreamPort_ptr thePort);
    virtual ~SUPERVGUI_CanvasStreamPortIn() {}

    virtual bool isStream() const { return true; }
    SUPERV_StreamPort getStreamEngine() const { return myStreamPort; }

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  public slots:
    void setParams();

  private:
    SUPERV_StreamPort myStreamPort;
};

class SUPERVGUI_CanvasStreamPortOut : public SUPERVGUI_CanvasPortOut {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasStreamPortOut(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::StreamPort_ptr thePort);
    virtual ~SUPERVGUI_CanvasStreamPortOut() {}

    virtual bool isStream() const { return true; }
    SUPERV_StreamPort getStreamEngine() const { return myStreamPort; }

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  public slots:
    void setParams();

  private:
    SUPERV_StreamPort myStreamPort;
};

#endif

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasNode.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasNodePrs.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_Clipboard.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI.h"
#include "SUPERVGUI_BrowseNodeDlg.h"
#include "SUPERVGUI_ManagePortsDlg.h"
#include "SUPERVGUI_Information.h"
#include "SUPERVGUI_Library.h"

#include "SUIT_MessageBox.h"
#include "LogWindow.h"
#include "SUIT_Session.h"

#include <qinputdialog.h>


SUPERVGUI_CanvasNode::SUPERVGUI_CanvasNode( SUIT_ResourceMgr* mgr, QObject* theParent,
                                            SUPERVGUI_Main* theMain, SUPERV_CNode theNode, bool theIsCell):
    QObject(theParent),
    myMain(theMain),
    myNode(theNode),
    myPrs(0),
    myDestroyed(false),
    warning(true),
    step(true),
    trace(true),
    verbose(true),
    myBrowseDlg(0),
    myMgr( mgr )
{
  Trace("SUPERVGUI_CanvasNode::SUPERVGUI_CanvasNode");

  setName(myNode->Name());

  QString aComment(myNode->Comment());
  QString aNewComment = aComment;
  if (getNodeType() == SUPERV::FactoryNode) {
    aNewComment = QString(myNode->Service()->ServiceName) +
      QString(tr("COMMENT_FROM")) +
      QString(getFactoryNode()->GetComponentName());
  }
  else if (myNode->IsMacro()) {
    aNewComment = tr("COMMENT_MNODE");
  }
  else {
    aNewComment = tr("COMMENT_CNODE");
  }
  if (aComment.isEmpty()) {
    myNode->SetComment(aNewComment.latin1());
  }

  myLabelText = aNewComment;

  // mkr : if the SUPERVGUI_CavasCellNode created (for CANVASTABLE view), 
  // we don't create the ports
  if (!theIsCell) {
    // create node ports
    isIgnore = true;
    SUPERV_Ports aPortList = myNode->Ports();
    for (int i = 0; i < aPortList->length(); i++)
      createPort(aPortList[i].in());
    
    SUPERV_StreamPorts aStreamPortList = myNode->StreamPorts();
    for (int i = 0; i < aStreamPortList->length(); i++) {
      createStreamPort(aStreamPortList[i].in());
    }
  }

  isIgnore = false;

  // mkr : PAL8237
  connect(this, SIGNAL(objectCreatedDeleted()), myMain, SLOT(onObjectCreatedDeleted()));
}

SUPERVGUI_CanvasNode::~SUPERVGUI_CanvasNode()
{
  isIgnore = true;
  if ( myPrs ) {
    myPrs->hide();
    delete myPrs;
    myPrs = 0;
  }
}

void SUPERVGUI_CanvasNode::setDestroyed()
{
  myDestroyed = true;
  getPrs()->hide();
}

SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasNode::getPrs()
{
  if ( !myPrs )
    myPrs = createPrs();

  return myPrs;
}

SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasNode::createPrs() const
{
  //
  //SUPERVGUI_PrsNode* glNode = new SUPERVGUI_PrsNode( (SUPERVGUI_CanvasNode*)this );
  //glNode->setFirstPoint( new GLViewer_Pnt( 10, 10 ) );
  //glNode->compute();
  //glNode->update();
  //getGLContext()->insertObject( glNode, true /*false*/ );
  //
  return new SUPERVGUI_CanvasNodePrs( myMgr, myMain->getCanvas(), (SUPERVGUI_CanvasNode*)this);
}

void SUPERVGUI_CanvasNode::createPort(SUPERV::Port_ptr thePort)
{
  SUPERVGUI_CanvasPort* aPort = 0;
  if (thePort->IsInput())
    aPort = new SUPERVGUI_CanvasPortIn((SUPERVGUI_CanvasNode*)this, myMain, thePort);
  else
    aPort = new SUPERVGUI_CanvasPortOut((SUPERVGUI_CanvasNode*)this, myMain, thePort);

  if (aPort) {
    connect(aPort, SIGNAL(destroyed(QObject*)), this, SLOT(onDestroyed(QObject*)));
    if (!isIgnore) getPrs()->updatePorts();
    
    emit objectCreatedDeleted(); // mkr : PAL8237
  }
}

void SUPERVGUI_CanvasNode::createStreamPort(SUPERV::StreamPort_ptr thePort)
{
  SUPERVGUI_CanvasPort* aPort = 0;
  if (thePort->IsInput())
    aPort = new SUPERVGUI_CanvasStreamPortIn((SUPERVGUI_CanvasNode*)this, myMain, thePort);
  else
    aPort = new SUPERVGUI_CanvasStreamPortOut((SUPERVGUI_CanvasNode*)this, myMain, thePort);

  if (aPort) {
    connect(aPort, SIGNAL(destroyed(QObject*)), this, SLOT(onDestroyed(QObject*)));
    if (!isIgnore) getPrs()->updatePorts();
  }
}

void SUPERVGUI_CanvasNode::onDestroyed(QObject* theObject)
{
  if (!isIgnore) {
    removeChild(theObject); // signal is sent before removing the object
    getPrs()->updatePorts();
  }
}

QPopupMenu* SUPERVGUI_CanvasNode::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = new QPopupMenu(theParent);

  if (myMain->getDataflow()->IsExecuting()) { // Execution time
    popup->insertItem((myNode->IsSuspended()?tr("MSG_RESUME"):tr("MSG_SUSPEND")), this, SLOT(suspendResume()));
    popup->insertItem(tr("MSG_KILL"), this, SLOT(kill()));
  }
  else { // Edition time
    const int type = getNodeType();

    // for all nodes except EndLoop and EndSwitch : Rename, Delete, Copy
    if ( myMain->isEditable() && type != SUPERV::EndLoopNode && type != SUPERV::EndSwitchNode &&
	// asv 26.01.05 : don't allow nodes "edition" commands in Table view
	myMain->getViewType() != CANVASTABLE ) { 
      popup->insertItem(tr("MSG_RENAME"), this, SLOT(rename()));
      popup->insertItem(tr("MSG_DELETE"), this, SLOT(remove()));
    }
    
    // tmp: Copy temporary does not work for Macro nodes...
    if ( type != SUPERV::MacroNode )
      popup->insertItem(tr("ITM_COPY_NODE"), this, SLOT(copy())); // Copy Node functionality
    popup->insertSeparator();
        
    // for all nodes : Browse
    popup->insertItem(tr("MSG_BROWSE"), this, SLOT(browse()));

    // for all nodes except EndLoop and EndSwitch : Change info
    if (type != SUPERV::EndLoopNode && type != SUPERV::EndSwitchNode)
      popup->insertItem(tr("MSG_CHANGE_INFO"), this, SLOT(changeInformation()));

    // for all InLine nodes
    if ( type != SUPERV::FactoryNode && type != SUPERV::ComputingNode && type != SUPERV::MacroNode ) {
      if ( myMain->isEditable() ) { 
	popup->insertSeparator();
	popup->insertItem( tr( "MNU_EDIT_FUNC" ), this, SLOT(editFunction()));
      }

      // for all InLine, regardless isEditable() : Export to Library
      popup->insertItem( tr( "MNU_EXPORT" ), this, SLOT( exportToLib()));

      // for all InLine except EndLoop : Add Ports menu, Paste, Manage Ports
      if ( myMain->isEditable() && type != SUPERV::EndLoopNode ) {
	// mkr : IPAL9815 : commented the following code
	/*QPopupMenu* addPortMenu = new QPopupMenu(theParent);
	addPortMenu->insertItem( tr( "MNU_INPUT" ), this, SLOT(addInputPort()));
	if (getNodeType() != SUPERV::LoopNode)
	  addPortMenu->insertItem( tr( "MNU_OUTPUT" ), this, SLOT(addOutputPort()));

	  popup->insertItem( tr( "MNU_ADD_PORT" ), addPortMenu);*/
	popup->insertItem( tr( "MNU_MANAGE_PORTS" ), this, SLOT(managePorts()));

	// Paste Port functionality
	int aPasteItem = popup->insertItem(tr("ITM_PASTE_PORT"), this, SLOT(pastePort()));
	SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
	if ( !aCB->isCopyPort() || (type == SUPERV::LoopNode && !aCB->getCopyPort()->IsInput()) )
	  popup->setItemEnabled( aPasteItem, false );
      }
    }
  }
  return popup;
}

void SUPERVGUI_CanvasNode::show() 
{
  getPrs()->show();
}

void SUPERVGUI_CanvasNode::showAll() 
{
  getPrs()->showAll();
}

void SUPERVGUI_CanvasNode::hideAll() 
{
  getPrs()->hideAll();
}

void SUPERVGUI_CanvasNode::switchLabel()
{
  getPrs()->setLabelVisible(!getPrs()->isLabelVisible());
}

void SUPERVGUI_CanvasNode::switchPorts()
{
  getPrs()->setPortVisible(!getPrs()->isPortVisible());
}

void SUPERVGUI_CanvasNode::move(int x, int y)
{
  //  myNode->Coords(x, y); //  <<<- done inside Prs to reflect item movement by mouse press
  if (x > GRAPH_MAX) x = (int) getPrs()->x();
  if (y > GRAPH_MAX) y = (int) getPrs()->y();
  getPrs()->move(x, y);
}

void SUPERVGUI_CanvasNode::merge() 
{
  // synchronize port list
  bool update = false;
  isIgnore = true;
  SUPERVGUI_CanvasPort* aPort;
  QObjectList* aPortList = queryList("SUPERVGUI_CanvasPort");
  SUPERV_Ports aPorts = myNode->Ports();
  for (int i = 0; i < aPorts->length(); i++) {
    aPort = (SUPERVGUI_CanvasPort*) 
      child(myMain->getCanvas()->getPortName(aPorts[i].in()), "SUPERVGUI_CanvasPort");
    if (aPort) {
      aPortList->removeRef(aPort);
      aPort->update();
    }
    else {
      createPort(aPorts[i].in());
      update = true;
    }
  }

  SUPERV_StreamPorts aStreamPorts = myNode->StreamPorts();
  for (int i = 0; i < aStreamPorts->length(); i++) {
    aPort = (SUPERVGUI_CanvasPort*) 
      child(myMain->getCanvas()->getPortName(aStreamPorts[i].in()), "SUPERVGUI_CanvasPort");
    if (aPort) {
      aPortList->removeRef(aPort);
      aPort->update();
    }
    else {
      createPort(aStreamPorts[i].in());
      update = true;
    }
  }

  QObjectListIt it(*aPortList);
  while ((aPort=(SUPERVGUI_CanvasPort*)it.current()) != 0) {
    ++it;
    aPortList->removeRef(aPort);
    delete aPort;
    update = true;
  }
  delete aPortList;
  isIgnore = false;
  if (update) getPrs()->updatePorts();

  sync(); // update node state also
}

void SUPERVGUI_CanvasNode::sync()  {
  const bool isExecuting = myMain->getDataflow()->IsExecuting();
  if ( myNode->IsMacro() && isExecuting ) {
    // get SubGraph from MacroNode
    SUPERV_Graph aMacro = SUPERV::Graph::_narrow(myNode);
    if (!SUPERV_isNull(aMacro)) {
      SUPERV_Graph aGraph;
      if (aMacro->IsStreamMacro())
	aGraph = aMacro->StreamObjRef();
      else
	aGraph = aMacro->FlowObjRef();
      if (!SUPERV_isNull(aGraph)) {
	if (aGraph->State() != SUPERV::UndefinedState && aGraph->State() != SUPERV::NoState)
	  getPrs()->setState(aGraph->State());
	else 
	  getPrs()->setState(myNode->State());
      }
    }
  }
  else {
    SUPERV::GraphState aState = myNode->State();

    // asv : 18.11.04 : fix for 6170 : after execution is finished, nodes' status must be reset.
    // asv : 13.12.04 : commented out setting EditingState.  See 2.21.
    //if ( !isExecuting && myMain->IsGUIEventLoopFinished() && (aState == SUPERV::DoneState || 
    //  aState == SUPERV::KillState || aState == SUPERV::StopState ) )
    //  aState = SUPERV::EditingState;
    getPrs()->setState(aState);
  }
  
  // update child ports
  const QObjectList* list = children();
  if (list) {
    QObjectListIt it(*list);
    while (QObject* obj = it.current()) {
      ++it;
      if (obj->inherits("SUPERVGUI_CanvasPort")) {
	((SUPERVGUI_CanvasPort*)obj)->sync();
      }
    }
  }
}

void SUPERVGUI_CanvasNode::syncOnEvent(SUPERV::GraphState theStateFromEvent) 
{
  getPrs()->setState(theStateFromEvent);
}

bool SUPERVGUI_CanvasNode::setNodeName(QString aName)  {
  bool result = myNode->SetName(aName.latin1());
  if (result) {
    setName(myNode->Name());
    getPrs()->updateInfo();
    // TODO: update name of all the links to this node
  } 
  else {
    QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr( "ERROR" ), tr( "MSG_CANT_RENAMENODE" ) );
  }
  return result;
}

void SUPERVGUI_CanvasNode::rename()  {
  bool ok;
   QString aName = QInputDialog::getText( tr( "Rename" ), tr( "Enter new name:" ), QLineEdit::Normal,
                                          myNode->Name(), &ok, SUIT_Session::session()->activeApplication()->desktop() );

  //mkr : modifications for fixing bug IPAL9972
  if( ok && !aName.isEmpty() && aName.compare( myNode->Name() ) != 0) {
    setNodeName(aName);
  }
}

void SUPERVGUI_CanvasNode::remove() {
  Trace("SUPERVGUI_CanvasNode::remove");
  myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 

  //set myCopyPort from Main object to empty if engine of this port is deleted
  //check if any port of this deleted node has been copied
  SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
  if ( aCB->isCopyPort() )
    if ( QString(myNode->Name()) == QString(aCB->getCopyPort()->Node()->Name()) )
      aCB->setCopyPort( 0 );

  //set myCopyNode from Main object to empty if engine of this node is deleted
  //check if myCopyNode and this node engine is equal
  if ( aCB->isCopyNode() )
    if ( QString(myNode->Name()) == QString(aCB->getCopyNode()->Name()) ) 
      aCB->setCopyNode( 0 );

  // mkr: since the deletion of the node allow only in CANVAS view,
  // it is necessary to remove the CanvasArray's children, which
  // have the same CNode engine as deleting node
  myMain->removeArrayChild(myNode);

  SUPERVGUI_Canvas* aCanvas = myMain->getCanvas();
  setDestroyed();
  myNode->destroy();

  emit objectCreatedDeleted(); // mkr : PAL8237
  
  delete this;
  aCanvas->update();
}

void SUPERVGUI_CanvasNode::copy() {
  Trace("SUPERVGUI_CanvasNode::copy()");
  SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
  const int aKind = getNodeType(); 
  if ( aKind == SUPERV::EndLoopNode || aKind == SUPERV::EndSwitchNode )
    aCB->setCopyNode( SUPERV::CNode::_duplicate(SUPERV::GNode::_narrow(getEngine())->Coupled()) );
  else
    aCB->setCopyNode( SUPERV::CNode::_duplicate( getEngine() ) );
}

void SUPERVGUI_CanvasNode::suspendResume() {
  Trace("SUPERVGUI_CanvasNode::suspendResume");
  int n = queryList("SUPERVGUI_CanvasNode")->count(); 
  if (myNode->IsSuspended()) {
    if (!((n==1)? myMain->getDataflow()->Resume() : myNode->Resume())) {
      QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_RESUMENODE") );
    }
    else {
      sync();
      myMain->getMyThread()->startThread(tr("MSG_NODE_RESUMED1")+myNode->Name()+tr("MSG_NODE_RESUMED2"));
    }
  } else {
    if (!((n==1)? myMain->getDataflow()->Suspend() : myNode->Suspend())) {
      QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_SUSPENDNODE") );
    } else {
      syncOnEvent(SUPERV::SuspendReadyState);
      myMain->getMessage()->putMessage(tr("MSG_NODE_SUSPENDED1")+myNode->Name()+tr("MSG_NODE_SUSPENDED2"));
    }
  }
}

void SUPERVGUI_CanvasNode::kill() {
  Trace("SUPERVGUI_CanvasNode::kill");
  int n = queryList("SUPERVGUI_CanvasNode")->count(); 
  if (!((n==1)? myMain->getDataflow()->Kill() : myNode->Kill())) {
    QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_KILLNODE") );
  } else {
    syncOnEvent(SUPERV_Kill);
    myMain->getMessage()->putMessage(tr("MSG_NODE_KILLED1")+myNode->Name()+tr("MSG_NODE_KILLED2"));
  }
}
/* asv : 15.12.04 : commented out stopRestart() in Main and CanvasNode because it's not called from anywhere,
   the comment from kloss (in Main.cxx) may be explaining it, but it's in French and I do not understand it..
void SUPERVGUI_CanvasNode::stopRestart() {
  Trace("SUPERVGUI_CanvasNode::stopRestart");
  
  int n = queryList("SUPERVGUI_CanvasNode")->count(); 
  if ((myNode->State() == SUPERV_Stop) || (myNode->State() == SUPERV_Kill)) {
    if (!((n==1)? myMain->getDataflow()->Run() : myNode->ReStart())) {
      QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(),  tr("ERROR"), tr("MSG_CANT_RESTARTNODE") );
    }
  } else {
    if (!((n==1)? myMain->getDataflow()->Stop() : myNode->Stop())) {
      QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(),  tr("ERROR"), tr("MSG_CANT_STOPNODE") );
    }
  }
}
*/
void SUPERVGUI_CanvasNode::changeInformation() {
  SUPERVGUI_Information* aDlg = new SUPERVGUI_Information(myNode, !myMain->isEditable());
  if (aDlg->exec()) {

    QString aName = myNode->Name();
    if (!aName.isEmpty() && myMain->isEditable()) {
      //mkr : modifications for fixing bug IPAL9972
      //setNodeName(aName);

      setName(myNode->Name());
      getPrs()->updateInfo();
      // TODO: update name of all the links to this node
    }

  }
  delete aDlg;
}

/* asv : 13.12.04 : The functions below are not called from anywhere, so commenting them out...
void SUPERVGUI_CanvasNode::configure() 
{
  Trace("SUPERVGUI_CanvasNode::configure");
  QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(),  tr("ERROR"), tr("MSG_NOT_IMPLEMENTED") ); // kloss : a faire : lancer l'ihm DATA
}

void SUPERVGUI_CanvasNode::showPython() 
{
  Trace("SUPERVGUI_CanvasNode::showPython");
  SUPERVGUI_Python cp(myMain->getStudy()->get_PyInterp(), !myMain->isEditable());
  cp.exec();
}
*/

bool SUPERVGUI_CanvasNode::isWarning() 
{
  Trace("SUPERVGUI_CanvasNode::isWarning");
  return(warning);
}

bool SUPERVGUI_CanvasNode::isStep() 
{
  Trace("SUPERVGUI_CanvasNode::isStep");
  return(step);
}

bool SUPERVGUI_CanvasNode::isTrace() 
{
  Trace("SUPERVGUI_CanvasNode::isTrace");
  return(trace);
}

bool SUPERVGUI_CanvasNode::isVerbose() 
{
  Trace("SUPERVGUI_CanvasNode::isVerbose");
  return(verbose);
}

void SUPERVGUI_CanvasNode::setWarning(bool b) 
{
  Trace("SUPERVGUI_CanvasNode::setWarning");
  warning = b;
}

void SUPERVGUI_CanvasNode::setStep(bool b) 
{
  Trace("SUPERVGUI_CanvasNode::setStep");
  step = b;
}

void SUPERVGUI_CanvasNode::setTrace(bool b) 
{
  Trace("SUPERVGUI_CanvasNode::setTrace");
  trace = b;
}

void SUPERVGUI_CanvasNode::setVerbose(bool b) 
{
  Trace("SUPERVGUI_CanvasNode::setVerbose");
  verbose = b;
}


void SUPERVGUI_CanvasNode::browse() 
{
  // asv 28.01.05 : set "Editing" flag only on "OK" pressed in BrowseDlg
  //myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
  if (!myBrowseDlg) {
    myBrowseDlg = new SUPERVGUI_BrowseNodeDlg(this);
    myBrowseDlg->installEventFilter(this);
  }
  if (!myBrowseDlg->isVisible())
    myBrowseDlg->show();
  else {
    myBrowseDlg->raise();
    myBrowseDlg->setActiveWindow();
    myBrowseDlg->setFocus();
  }
}

bool SUPERVGUI_CanvasNode::eventFilter( QObject* o, QEvent* e )
{
  if (o == myBrowseDlg && e->type() == QEvent::Close)
    myBrowseDlg = 0;
  return QObject::eventFilter(o, e);
}

QStringList SUPERVGUI_CanvasNode::getPortsNamesIN(SUPERV_INode theNode, bool theInputPorts)
{
  QStringList aPNList;
  if (!SUPERV_isNull(theNode)) {
    SUPERV_Ports aPorts = theNode->Ports();
    for (int i=0; i<aPorts->length(); i++) {
      if (theInputPorts) {
	if (aPorts[i]->IsInput()) aPNList.append(QString(aPorts[i]->Name()));
      }
      else 
	if (!aPorts[i]->IsInput()) aPNList.append(QString(aPorts[i]->Name()));
    }
  }
  return aPNList;
}

SUPERV_Port SUPERVGUI_CanvasNode::createInPort() 
{
  SUPERV_INode aNode = getInlineNode();
  if (SUPERV_isNull(aNode)) {
    MESSAGE("SUPERVGUI_CanvasNode::createInPort: Node is wrong type");
    return NULL;
  }
  SUPERVGUI_PortParamsDlg* aDlg = new SUPERVGUI_PortParamsDlg(getPortsNamesIN(aNode,true));
  if (aDlg->exec()) {
    myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
    SUPERV_Port aPort = aNode->InPort(aDlg->getName().latin1(),
				      aDlg->getType().latin1());    
    delete aDlg;
    return aPort;
  }
  delete aDlg;
  return NULL;
}

SUPERV_Port SUPERVGUI_CanvasNode::createOutPort() 
{
  SUPERV_INode aNode = getInlineNode();
  if (SUPERV_isNull(aNode)) {
    MESSAGE("SUPERVGUI_CanvasNode::createOutPort: Node is wrong type");
    return NULL;
  }
  
  SUPERVGUI_PortParamsDlg* aDlg = new SUPERVGUI_PortParamsDlg(getPortsNamesIN(aNode,false));
  if (aDlg->exec()) {
    myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
    SUPERV_Port aPort = aNode->OutPort(aDlg->getName().latin1(),
				       aDlg->getType().latin1());
    delete aDlg;
    return aPort;
  }
  delete aDlg;
  return NULL;
}

// mkr : IPAL9815 : commented the following code
/*void SUPERVGUI_CanvasNode::addInputPort() {
  SUPERV_Port aPort = createInPort();
  if (aPort == NULL || CORBA::is_nil( aPort ) ) return;

  createPort(aPort.in());
}


void SUPERVGUI_CanvasNode::addOutputPort() {
  SUPERV_Port aPort = createOutPort();
  if (aPort == NULL || CORBA::is_nil( aPort ) ) return;

  createPort(aPort.in());
}*/


void SUPERVGUI_CanvasNode::editFunction()  {
  if (getNodeType() == SUPERV::LoopNode) {
    SUPERVGUI_EditPythonDlg* aDlg = new SUPERVGUI_EditPythonDlg(true);
    SUPERV_LNode aLNode = getLoopNode();
    aDlg->setInitFunction(aLNode->PyInit());
    aDlg->setMoreFunction(aLNode->PyMore());
    aDlg->setNextFunction(aLNode->PyNext());
    if (aDlg->exec()) {
      myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
      aLNode->SetPyInit(aDlg->getInitFuncName().latin1(), (aDlg->getInitFunction()).in());
      aLNode->SetPyMore(aDlg->getMoreFuncName().latin1(), (aDlg->getMoreFunction()).in());
      aLNode->SetPyNext(aDlg->getNextFuncName().latin1(), (aDlg->getNextFunction()).in());
    }
    delete aDlg;
  } 
  else {
    SUPERVGUI_EditPythonDlg* aDlg = new SUPERVGUI_EditPythonDlg();
    SUPERV_INode aINode = getInlineNode();
    aDlg->setFunction(aINode->PyFunction());
    if (aDlg->exec()) {
      myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
      aINode->SetPyFunction(aDlg->getFuncName().latin1(), (aDlg->getFunction()).in());
    }
    delete aDlg;
  }
}

/** 
 * Called on "Paste port" command of popup menu
 */
void SUPERVGUI_CanvasNode::pastePort() {
  SUPERVGUI_Clipboard::getClipboard()->pastePort( this );
}

/** 
 * Called on "Edit ports" popup menu command. See SUPERVGUI_ManagePortsDlg.h
 * for detailed description of the functionality
 */
void SUPERVGUI_CanvasNode::managePorts() {
  SUPERVGUI_ManagePortsDlg* aDlg = new SUPERVGUI_ManagePortsDlg( this );
  aDlg->exec();
  delete aDlg;
}

/**
 * Called on "Export to Library" popup menu command.  See SUPERVGUI_Library.h
 * for details on InLine nodes library functionality
 */
void SUPERVGUI_CanvasNode::exportToLib() {
  SUPERV::INode_var anINode = SUPERV::INode::_narrow( getEngine() );
  if ( !CORBA::is_nil( anINode ) )
    SUPERVGUI_Library::getLibrary()->Export( anINode );
  else
    SUIT_MessageBox::error1( SUIT_Session::session()->activeApplication()->desktop(), tr( "ERROR" ), tr( "MSG_BAD_INODE" ), tr( "OK" ) );
}

QString SUPERVGUI_CanvasNode::getToolTipText() const {
  if ( myNode->IsFactory() )
    return QString("Name : ") + SUPERV::FNode::_narrow(myNode)->Name() +
      QString("\nContainer : ") + SUPERV::FNode::_narrow(myNode)->GetContainer() +
      QString("\nComponentName : ") + SUPERV::FNode::_narrow(myNode)->GetComponentName() +
      QString("\nInterfaceName : ") + SUPERV::FNode::_narrow(myNode)->GetInterfaceName();
  return QString("Name : ") + myNode->Name() + '\n' + getLabelText();
}

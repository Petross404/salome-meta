//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasControlNodePrs.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI_CanvasControlNodePrs.h"
#include "SUPERVGUI_CanvasControlNode.h"
#include "SUPERVGUI_Canvas.h"

#define SHIFT LABEL_HEIGHT/2

//=====================================================================
// Control node presentation
//=====================================================================
SUPERVGUI_CanvasControlNodePrs::SUPERVGUI_CanvasControlNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, SUPERVGUI_CanvasNode* theNode):
  SUPERVGUI_CanvasNodePrs( mgr, theCanvas, theNode)
{
  Trace("SUPERVGUI_CanvasControlNodePrs::SUPERVGUI_CanvasControlNodePrs");
  setLabelVisible(false);
  setNodeColor(Qt::cyan.light());
}

void SUPERVGUI_CanvasControlNodePrs::hideAll()
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  setPortVisible(false);

  if (aDisp) {
    show();
    canvas()->update();
  }
}

void SUPERVGUI_CanvasControlNodePrs::showAll()
{
  bool aDisp = isVisible();
  if (aDisp) hide();

  setPortVisible(true);

  if (aDisp) {
    show();
    canvas()->update();
  }
}

QRect SUPERVGUI_CanvasControlNodePrs::getStatusRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight()+getLabelHeight()+
	       getBodyHeight()+getGateHeight(),
	       width(), getStatusHeight());
}

QRect SUPERVGUI_CanvasControlNodePrs::getBodyRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight()+getLabelHeight(), 
	       width(), getBodyHeight());
}

QRect SUPERVGUI_CanvasControlNodePrs::getGateRect() const
{
  return QRect((int)x(), ((int)y())+getTitleHeight()+getLabelHeight()+getBodyHeight(), 
	       width(), getGateHeight());
}

//=====================================================================
// Start control node presentation
//=====================================================================
SUPERVGUI_CanvasStartNodePrs::SUPERVGUI_CanvasStartNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, SUPERVGUI_CanvasStartNode* theNode):
  SUPERVGUI_CanvasControlNodePrs( mgr, theCanvas, theNode)
{
  Trace("SUPERVGUI_CanvasStartNodePrs::SUPERVGUI_CanvasStartNodePrs");
}

QPointArray SUPERVGUI_CanvasStartNodePrs::areaPoints() const
{
  int w = width();
  int h = height()+2; // add width of pen 

  int h1 = getTitleHeight()+1;
  int h2 = getStatusHeight()+1;

  QPointArray aPnts(8);
  QPoint p((int)x(), (int)y()-1);
  aPnts[0] = p + QPoint(0, h1);
  aPnts[1] = p + QPoint(SHIFT, 0);
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);
  aPnts[4] = aPnts[3] + QPoint(0, h-h1-h2);
  aPnts[5] = aPnts[2] + QPoint(0, h);
  aPnts[6] = aPnts[1] + QPoint(0, h);
  aPnts[7] = aPnts[0] + QPoint(0, h-h1-h2);
  return aPnts;
}

void SUPERVGUI_CanvasStartNodePrs::drawFrame(QPainter& thePainter) 
{
  QRect r = getRect();

  int h1 = getTitleHeight();
  int h2 = getStatusHeight();

  QRect r0(r.x(), r.y()+h1, r.width(), r.height()-h1-h2);

  QBrush saved = thePainter.brush();
  thePainter.setBrush(NoBrush);
  thePainter.drawRect(r0);
  thePainter.setBrush(saved);
}

void SUPERVGUI_CanvasStartNodePrs::drawTitleShape(QPainter& thePainter) 
{
  QRect r = getTitleRect();
  int w = width()-1;

  QPointArray aPnts(4);
  aPnts[0] = r.topLeft() + QPoint(0, r.height());
  aPnts[1] = r.topLeft() + QPoint(SHIFT, 0);
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);

  thePainter.setBrush(Qt::red.light());
  thePainter.drawPolygon(aPnts);
}

void SUPERVGUI_CanvasStartNodePrs::drawStatusShape(QPainter& thePainter) 
{
  QRect r = getStatusRect();
  int w = width()-1;

  QPointArray aPnts(4);
  aPnts[0] = r.topLeft() + QPoint(SHIFT, r.height());
  aPnts[1] = r.topLeft();
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);

  thePainter.drawPolygon(aPnts);
}


void SUPERVGUI_CanvasStartNodePrs::setState(SUPERV::GraphState theState) 
{
  QRect r = getStatusRect();
  canvas()->setChanged(QRect(r.x(), r.y(), r.width()+SHIFT, r.height()));
  SUPERVGUI_CanvasNodePrs::setState(theState);
}


//=====================================================================
// End control node presentation
//=====================================================================
SUPERVGUI_CanvasEndNodePrs::SUPERVGUI_CanvasEndNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, SUPERVGUI_CanvasEndNode* theNode):
  SUPERVGUI_CanvasControlNodePrs(mgr,theCanvas, theNode)
{
  Trace("SUPERVGUI_CanvasEndNodePrs::SUPERVGUI_CanvasEndNodePrs");
}

QPointArray SUPERVGUI_CanvasEndNodePrs::areaPoints() const
{
  int w = width();
  int h = height()+2;

  int h1 = getTitleHeight()+1;
  int h2 = getStatusHeight()+1;

  QPointArray aPnts(8);
  QPoint p((int)x(), (int)y()-1);
  aPnts[0] = p + QPoint(0, h1);
  aPnts[1] = p + QPoint(-SHIFT, 0);
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);
  aPnts[4] = aPnts[3] + QPoint(0, h-h1-h2);
  aPnts[5] = aPnts[2] + QPoint(0, h);
  aPnts[6] = aPnts[1] + QPoint(0, h);
  aPnts[7] = aPnts[0] + QPoint(0, h-h1-h2);
  return aPnts;
}

void SUPERVGUI_CanvasEndNodePrs::drawFrame(QPainter& thePainter) 
{
  QRect r = getRect();

  int h1 = getTitleHeight();
  int h2 = getStatusHeight();

  QRect r0(r.x(), r.y()+h1, r.width(), r.height()-h1-h2);

  QBrush saved = thePainter.brush();
  thePainter.setBrush(NoBrush);
  thePainter.drawRect(r0);
  thePainter.setBrush(saved);
}

void SUPERVGUI_CanvasEndNodePrs::drawTitleShape(QPainter& thePainter) 
{
  QRect r = getTitleRect();
  int w = width()-1;

  QPointArray aPnts(4);
  aPnts[0] = r.topLeft() + QPoint(0, r.height());
  aPnts[1] = r.topLeft() + QPoint(-SHIFT, 0);
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);

  thePainter.setBrush(Qt::red.light());
  thePainter.drawPolygon(aPnts);
}

void SUPERVGUI_CanvasEndNodePrs::drawStatusShape(QPainter& thePainter) 
{
  QRect r = getStatusRect();
  int w = width()-1;

  QPointArray aPnts(4);
  aPnts[0] = r.topLeft() + QPoint(-SHIFT, r.height());
  aPnts[1] = r.topLeft();
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);

  thePainter.drawPolygon(aPnts);
}

void SUPERVGUI_CanvasEndNodePrs::setState(SUPERV::GraphState theState)
{
  QRect r = getStatusRect();
  canvas()->setChanged(QRect(r.x()-SHIFT, r.y(), r.width()+SHIFT, r.height()));
  SUPERVGUI_CanvasNodePrs::setState(theState);
}

//=====================================================================
// Goto control node presentation
//=====================================================================
SUPERVGUI_CanvasGotoNodePrs::SUPERVGUI_CanvasGotoNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, SUPERVGUI_CanvasGotoNode* theNode):
  SUPERVGUI_CanvasControlNodePrs(mgr, theCanvas, theNode)
{
  Trace("SUPERVGUI_CanvasGotoNodePrs::SUPERVGUI_CanvasGotoNodePrs");
}

QPointArray SUPERVGUI_CanvasGotoNodePrs::areaPoints() const
{
  int w = width();
  int h = height()+2;

  int h1 = getTitleHeight()+1;
  int h2 = getStatusHeight()+1;

  QPointArray aPnts(8);
  QPoint p((int)x(), (int)y()-1);
  aPnts[0] = p + QPoint(0, h1);
  aPnts[1] = p + QPoint(SHIFT, 0);
  aPnts[2] = aPnts[1] + QPoint(w-2*SHIFT, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);
  aPnts[4] = aPnts[3] + QPoint(0, h-h1-h2);
  aPnts[5] = aPnts[2] + QPoint(0, h);
  aPnts[6] = aPnts[1] + QPoint(0, h);
  aPnts[7] = aPnts[0] + QPoint(0, h-h1-h2);
  return aPnts;
}

void SUPERVGUI_CanvasGotoNodePrs::drawFrame(QPainter& thePainter) 
{
  QRect r = getRect();

  int h1 = getTitleHeight();
  int h2 = getStatusHeight();

  QRect r0(r.x(), r.y()+h1, r.width(), r.height()-h1-h2);

  QBrush saved = thePainter.brush();
  thePainter.setBrush(NoBrush);
  thePainter.drawRect(r0);
  thePainter.setBrush(saved);
}

void SUPERVGUI_CanvasGotoNodePrs::drawTitleShape(QPainter& thePainter) 
{
  QRect r = getTitleRect();
  int w = width()-1;

  QPointArray aPnts(4);
  aPnts[0] = r.topLeft() + QPoint(0, r.height());
  aPnts[1] = r.topLeft() + QPoint(SHIFT, 0);
  aPnts[2] = aPnts[1] + QPoint(w-2*SHIFT, 0);
  aPnts[3] = aPnts[0] + QPoint(w, 0);

  thePainter.setBrush(Qt::green.light());
  thePainter.drawPolygon(aPnts);
}

void SUPERVGUI_CanvasGotoNodePrs::drawStatusShape(QPainter& thePainter) 
{
  QRect r = getStatusRect();
  int w = width()-1;

  QPointArray aPnts(4);
  aPnts[0] = r.topLeft() + QPoint(SHIFT, r.height());
  aPnts[1] = r.topLeft();
  aPnts[2] = aPnts[1] + QPoint(w, 0);
  aPnts[3] = aPnts[0] + QPoint(w-2*SHIFT, 0);

  thePainter.drawPolygon(aPnts);
}


//=====================================================================
// Macro node presentation
//=====================================================================
SUPERVGUI_CanvasMacroNodePrs::SUPERVGUI_CanvasMacroNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, SUPERVGUI_CanvasMacroNode* theNode):
  SUPERVGUI_CanvasNodePrs( mgr, theCanvas, theNode)
{
  Trace("SUPERVGUI_CanvasControlNodePrs::SUPERVGUI_CanvasControlNodePrs");
  setNodeColor(QColor(255, 186, 149)); //QColor(255, 200, 170));
}

void SUPERVGUI_CanvasMacroNodePrs::drawFrame(QPainter& thePainter) 
{
  SUPERVGUI_CanvasNodePrs::drawFrame(thePainter);

  QPen saved = thePainter.pen();
  thePainter.setPen(QPen(saved.color(), 2));

  QRect r = getRect();
  thePainter.drawLine(r.left(), r.top(), r.left(), r.bottom());
  thePainter.drawLine(r.left()+3, r.top(), r.left()+3, r.bottom());
  thePainter.drawLine(r.right()-3, r.top(), r.right()-3, r.bottom());
  thePainter.drawLine(r.right(), r.top(), r.right(), r.bottom());
  thePainter.setPen(saved);
}

QPointArray SUPERVGUI_CanvasMacroNodePrs::areaPoints() const
{
  int w = width()+2; // add width of pen
  int h = height()+1;

  QPointArray aPnts(4);
  aPnts[0] = QPoint((int)x()-1, (int)y());
  aPnts[1] = aPnts[0] + QPoint(w, 0);
  aPnts[2] = aPnts[1] + QPoint(0, h);
  aPnts[3] = aPnts[0] + QPoint(0, h);
  return aPnts;
}

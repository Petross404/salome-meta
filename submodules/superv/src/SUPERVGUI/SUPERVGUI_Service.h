//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Service.h
//  Author : Francis KLOSS
//  Module : SUPERV

#ifndef SUPERVGUI_Service_H
#define SUPERVGUI_Service_H

#include "SUPERVGUI_Def.h"
#include <qtabwidget.h>
#include <qhgroupbox.h>
#include <qwidgetstack.h>

class QListBox; 

/*!
 * Pane for Python script editing
 */
class SUPERVGUI_PythonEditPane: public QFrame {

  Q_OBJECT

public:
  SUPERVGUI_PythonEditPane( QWidget* theParent, const bool isNodeCreation, int& theX, int& theY );
  ~SUPERVGUI_PythonEditPane() {};
  
  QString getFuncName();

  bool isDefined()
    { return (myText->paragraphs() > 1); }

  SUPERV_Strings getFunction();
  void setFunction(SUPERV_Strings theStr);

public slots:
  // load all functions from an XML file into internal structures 
  void loadFile();

  // when user presses "Enter" - automatically set indentation of next line
  void autoIndentLine(); 

  // when user select another function name in combo box - fill myText with its body
  void readFunction( int i ); 

  // when user presses "Library" button - open a "Create node from Library" dialog
  void library();

private:
  bool         myIsWithLibrary;
  QTextEdit*   myText;
  QComboBox*   myFunctionsCombo;

  // fills myPyFunctions list.  called from loadFile() after user selects a file.
  void         initPyFunctions( QTextStream& );

  // list of Python functions.  filled in loadFile(), strings (functions) retrieved 
  // in readFunction()
  QStringList  myPyFunctions; 

  int& myX; // comes from SUPERVGUI_Service 
  int& myY; // comes from SUPERVGUI_Service 

};

/*!
 * Dialog box for node creation
 */
class SUPERVGUI_Service: public QDialog {

  Q_OBJECT

public:
    SUPERVGUI_Service(SALOME_NamingService* ns);
    ~SUPERVGUI_Service();

    void choose();

    /*!
     * Do the following actions for newly created Engine's CNode:
     * 1. Create a presentation for it (CanvasNode)
     * 2. Place the CanvasNode to the current top-left corner or the current viewport
     * 3. Increment the coordinates of the next CanvasNode (new nodes are "cascaded" when several of them are created at once)
     * PS theEndNode is passed only for Loop and Switch nodes (EndLoop and EndSwitch)
     */ 
    static void addNode( SUPERV::CNode_var theNode, SUPERV::INode_var theEndNode, int& theX, int& theY );

protected:
    void showEvent(QShowEvent* theEvent);

private:
    void initialise();

private slots:
    void tabChanged(QWidget *);
    void addComputeNode();
    void addFactoryNodeDef();
    void addFactoryNodeCust();
    void addInlineNode();
    void addMacroNode();
    void typeNodeSelected(int theRow);
    void loadGraph();

private:
    QListView* components;
    SALOME_NamingService* naming;
    int myX, myY;
    QWidgetStack* myStackWidget;
    int myLoopId;
    int myOtherId;

    SUPERVGUI_PythonEditPane* myScriptPane;

    SUPERVGUI_PythonEditPane* myInitPane;
    SUPERVGUI_PythonEditPane* myMorePane;
    SUPERVGUI_PythonEditPane* myNextPane;

    QListView* myMacroPane;
    QFile*     myMFile;        

    QTabWidget* myTabPane;
    bool myIsInline;

    QComboBox* myTypeCombo;
};

/*!
 * Edit Python dialog
 */
class SUPERVGUI_EditPythonDlg: public QDialog {

  Q_OBJECT
    
public:
  SUPERVGUI_EditPythonDlg(bool isLoop = false);
  ~SUPERVGUI_EditPythonDlg() {};

  // Not Loop functions
  QString getFuncName()
    { return myEditPane->getFuncName(); }

  bool isDefined()
    { return myEditPane->isDefined(); }

  SUPERV_Strings getFunction()
    { return myEditPane->getFunction(); }

  void setFunction(SUPERV_Strings theStr)
    { myEditPane->setFunction(theStr); }

  // Init functions
  QString getInitFuncName()
    { return myInitPane->getFuncName(); }

  SUPERV_Strings getInitFunction()
    { return myInitPane->getFunction(); }

  void setInitFunction(SUPERV_Strings theStr)
    { myInitPane->setFunction(theStr); }

  // More functions
  QString getMoreFuncName()
    { return myMorePane->getFuncName(); }

  SUPERV_Strings getMoreFunction()
    { return myMorePane->getFunction(); }

  void setMoreFunction(SUPERV_Strings theStr)
    { myMorePane->setFunction(theStr); }

  // Next functions
  QString getNextFuncName()
    { return myNextPane->getFuncName(); }

  SUPERV_Strings getNextFunction()
    { return myNextPane->getFunction(); }

  void setNextFunction(SUPERV_Strings theStr)
    { myNextPane->setFunction(theStr); }

public slots:
  void clickOnOk();

private:
  bool  myIsLoop; // mkr : PAL12236

  SUPERVGUI_PythonEditPane* myEditPane;
  SUPERVGUI_PythonEditPane* myInitPane;
  SUPERVGUI_PythonEditPane* myMorePane;
  SUPERVGUI_PythonEditPane* myNextPane;
};

class SUPERVGUI_CustomSettings: public QDialog {
  Q_OBJECT

  public:
    SUPERVGUI_CustomSettings(QListViewItem* theItem, bool isMultiSel, bool isCimpl);
    virtual ~SUPERVGUI_CustomSettings();

    QString Author();
    QString Container();
    QString Comment();

  private slots:
    void okButton();
    void koButton();

  private:

    QLineEdit*      authV;
    QLineEdit*      contV;
    QLabel*         contL;

    QMultiLineEdit* commV;
};

#endif

// Copyright (C) 2005  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either 
// version 2.1 of the License.
// 
// This library is distributed in the hope that it will be useful 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public  
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#ifndef SUPERVGUI_DATAMODEL_H
#define SUPERVGUI_DATAMODEL_H

#include <SalomeApp_DataModel.h>

/*
  Class       : SUPERVGUI_DataModel
  Description : Base class of data model
*/

class SUPERVGUI_DataModel : public SalomeApp_DataModel
{
public:
                                      SUPERVGUI_DataModel ( CAM_Module* theModule );
  virtual                             ~SUPERVGUI_DataModel();

  virtual bool                        open( const QString&, CAM_Study*, QStringList );
  virtual bool                        save( QStringList& );
  virtual bool                        saveAs( const QString&, CAM_Study*, QStringList& );
  virtual bool                        close();

  virtual bool                        isModified() const ;
  virtual bool                        isSaved() const ;

signals:
  void                                opened();
  void                                saved();
  void                                closed();
};

#endif 

//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_CanvasArray.h
//  Author : 
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasArray_H
#define SUPERVGUI_CanvasArray_H

#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_CanvasControlNode.h"

#include <qpopupmenu.h>
#include <qcanvas.h>

class SUIT_ResourceMgr;
class SUPERVGUI_Main;

class SUPERVGUI_CanvasArray: public QCanvas {
  Q_OBJECT
    
  public:
    SUPERVGUI_CanvasArray(SUPERVGUI_Main* m, SUIT_ResourceMgr* );
    virtual ~SUPERVGUI_CanvasArray();

    void sync();
    bool create();
    void destroy();

    SUPERVGUI_CanvasCellNode* getCellNode(SUPERV::CNode_ptr theNode) const;

    SUPERVGUI_CanvasCellNode* addNode(SUPERV_CNode node);

  private:
    
    SUPERVGUI_Main* myMain;
    SUIT_ResourceMgr*  myMgr;

    int             nodeX;
    int             nodeY;

};

#endif

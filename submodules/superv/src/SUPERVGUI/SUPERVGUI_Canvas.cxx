//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_Ganvas.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI.h"
#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_CanvasLink.h"

#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

//#define CHECKTIME

#ifdef CHECKTIME
#include <sys/timeb.h>
#endif

#define UPDATECONTROLNODES(NodesName)  \
    n = nodes->NodesName.length(); \
    for (int i=0; i<n; i++) { \
      SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->NodesName[i]); \
      SUPERV_CNode aCNodeEnd = SUPERV::CNode::_narrow(nodes->NodesName[i]->Coupled()); \
      ihmNode = getNode(aCNode); \
      if (ihmNode == NULL) { \
	myMain->addControlNode(aCNode, aCNodeEnd, false); \
      } else { \
        ihmNode->move(aCNode->X(), aCNode->Y()); \
	ihmList->removeRef(ihmNode); \
	ihmNode->merge(); \
        ihmNode = getNode(aCNodeEnd); \
        if (ihmNode) { \
	  ihmList->removeRef(ihmNode); \
	  ihmNode->merge(); \
        } \
      } \
    }

#define UPDATENODES(NodesName, FuncName)   \
    n = nodes->NodesName.length(); \
    for (int i=0; i<n; i++) { \
      SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->NodesName[i]); \
      ihmNode = getNode(aCNode); \
      if (ihmNode == NULL) { \
	myMain->FuncName(aCNode); \
      } else { \
        ihmNode->move(aCNode->X(), aCNode->Y()); \
	ihmList->removeRef(ihmNode); \
	ihmNode->merge(); \
      } \
    }

#define UPDATELINK(Link)   \
    ihmLink = getLink(Link.in()); \
    if (ihmLink == NULL) { \
      ihmLink = new SUPERVGUI_CanvasLink(this, myMain, Link.in()); \
      ihmLink->show(); \
    } \
    else { \
      ihmList->removeRef(ihmLink); \
      ihmLink->merge(); \
    }

/*
#define UPDATESTREAMLINK(Link)   \
    ihmLink = getLink(Link.in()); \
    if (ihmLink == NULL) { \
      ihmLink = new SUPERVGUI_CanvasStreamLink(this, myMain, Link.in()); \
      ihmLink->show(); \
    } \
    else { \
      ihmList->removeRef(ihmLink); \
      ihmLink->merge(); \
    }
*/


SUPERVGUI_Canvas::SUPERVGUI_Canvas( SUPERVGUI_Main* m, SUIT_ResourceMgr* mgr ):
    QCanvas(),
    myMain(m)
{
  Trace("SUPERVGUI_Canvas::SUPERVGUI_Canvas");
  myIsControlView = false;
  // resize(GRAPH_WIDTH, GRAPH_HEIGHT);
  resize(1050, 750);
  setDoubleBuffering(true);

  // mkr : IPAL10825 -->
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }
  QColor aColor =  aSupMod->getIVFBackgroundColor();
  if ( !aColor.isValid() )
    aColor = mgr->colorValue( "SUPERVGraph", "Background", DEF_MAIN_COLOR );
  // <--
     
  //aColor = QColor(SUIT_Session::session()->resourceMgr()->integerValue( "SUPERVGraph", "BackgroundColorRed" ), 
  //		  SUIT_Session::session()->resourceMgr()->integerValue( "SUPERVGraph", "BackgroundColorGreen" ), 
  //		  SUIT_Session::session()->resourceMgr()->integerValue( "SUPERVGraph", "BackgroundColorBlue" ) );
  setBackgroundColor(aColor);
}


SUPERVGUI_Canvas::~SUPERVGUI_Canvas() {
  // asv : 17.01.05 : why delete its own children (CanvasNode-s)?
  // they must be destroyed automatically.
  QObjectList* aNodeList = queryList("SUPERVGUI_CanvasNode");
  QObjectListIt aIt(*aNodeList);
  QObject* anObj;
  while ( (anObj = aIt.current()) != 0 ) {
    ++aIt;
    aNodeList->removeRef(anObj);
    delete anObj;
  }
  // asv : list returned by queryList() must be removed
  delete aNodeList;
}

void SUPERVGUI_Canvas::addView(QCanvasView* theView)
{
  QCanvas::addView(theView);
  theView->setPaletteBackgroundColor(backgroundColor().light(120));
}

void SUPERVGUI_Canvas::setFullView() 
{
  myIsControlView = false;
  updateNodes(true);
  updateLinks();
  update();
}

void SUPERVGUI_Canvas::setControlView() 
{
  myIsControlView = true;
  updateNodes(false);
  updateLinks();
  update();
}

void SUPERVGUI_Canvas::updateNodes(bool withPorts) 
{
  QObjectList* aNodeList = queryList("SUPERVGUI_CanvasNode");
  QObjectListIt aIt(*aNodeList);
  SUPERVGUI_CanvasNode* aNode;
  while ((aNode=(SUPERVGUI_CanvasNode*)aIt.current()) != 0) {
    ++aIt;
    if (withPorts)
      aNode->showAll();
    else
      aNode->hideAll();
  }
  delete aNodeList;
}

void SUPERVGUI_Canvas::updateLinks() 
{
  QObjectList* aLinkList = queryList("SUPERVGUI_CanvasLink");
  QObjectListIt aIt(*aLinkList);
  SUPERVGUI_CanvasLink* aLink;
  while ((aLink=(SUPERVGUI_CanvasLink*)aIt.current()) != 0) {
    ++aIt;
    aLink->merge();
  }
  delete aLinkList;
}

void SUPERVGUI_Canvas::sync() {
  if (SUPERV_isNull(myMain->getDataflow())) return;
      
  SUPERVGUI_CanvasNode* ihmNode;
  QObjectList* ihmList = queryList("SUPERVGUI_CanvasNode");
  QObjectListIt it(*ihmList);
  while ((ihmNode=(SUPERVGUI_CanvasNode*)it.current()) != 0) {
    ++it;
    ihmNode->sync();
  }
  delete ihmList;
}

/** 
 * Synchronizes Graph presentation with internal graph structure
 */
void SUPERVGUI_Canvas::merge() {
  if (SUPERV_isNull(myMain->getDataflow())) return;

  SUPERVGUI_CanvasNode* ihmNode;
  QObjectList* ihmList = queryList("SUPERVGUI_CanvasNode");
  SUPERV_Nodes nodes = myMain->getDataflow()->Nodes();

  MESSAGE("CNodes="<<nodes->CNodes.length());
  MESSAGE("FNodes="<<nodes->FNodes.length());
  MESSAGE("INodes="<<nodes->INodes.length());
  MESSAGE("GNodes="<<nodes->GNodes.length());
  MESSAGE("LNodes="<<nodes->LNodes.length());
  MESSAGE("SNodes="<<nodes->SNodes.length());
  MESSAGE("Graphs="<<nodes->Graphs.length());

  int n;
  UPDATENODES(CNodes, addComputeNode);
  UPDATENODES(FNodes, addComputeNode);
  UPDATENODES(INodes, addComputeNode);
  UPDATENODES(Graphs, addMacroNode);
  UPDATENODES(GNodes, addGOTONode);

  UPDATECONTROLNODES(LNodes);    
  UPDATECONTROLNODES(SNodes);
  /*
  n = nodes->LNodes.length();
  for (int i=0; i<n; i++) {
    SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->LNodes[i]);
    SUPERV_CNode aCNodeEnd = SUPERV::CNode::_narrow(nodes->LNodes[i]->Coupled());
    ihmNode = getNode(aCNode);
    SUPERVGUI_CanvasNode* ihmNodeEnd = getNode(aCNodeEnd);
    if (ihmNode == NULL) {
      myMain->addControlNode(aCNode, aCNodeEnd, false);
    } else {
      if (ihmNodeEnd) {
        ihmNode->move(aCNode->X(), aCNode->Y());
        ihmList->removeRef(ihmNode);
        ihmNode->merge();
        ihmNode = getNode(aCNodeEnd);
        ihmList->removeRef(ihmNode);
        ihmNode->merge();
      }
    }
  }
  n = nodes->SNodes.length();
  for (int i=0; i<n; i++) {
    SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->SNodes[i]);
    SUPERV_CNode aCNodeEnd = SUPERV::CNode::_narrow(nodes->SNodes[i]->Coupled());
    ihmNode = getNode(aCNode);
    if (ihmNode == NULL) {
      myMain->addControlNode(aCNode, aCNodeEnd, false);
    } else {
      ihmNode->move(aCNode->X(), aCNode->Y());
      ihmList->removeRef(ihmNode);
      ihmNode->merge();
      ihmNode = getNode(aCNodeEnd);
      ihmList->removeRef(ihmNode);
      ihmNode->merge();
    }
  }
  //*/

  QObjectListIt it(*ihmList);
  while ((ihmNode=(SUPERVGUI_CanvasNode*)it.current()) != 0) {
    ++it;
    ihmList->removeRef(ihmNode);
    delete ihmNode;
  }
  delete ihmList;

  // update links
  SUPERVGUI_CanvasLink* ihmLink;
  ihmList = queryList("SUPERVGUI_CanvasLink");
  /*
  don't work... that's a pity!
  */
  //SUPERV_Links links = myMain->getDataflow()->Links();
  SUPERV_Links links = myMain->getDataflow()->GLinks();
  for (int i = 0; i < links->length(); i++) {
    UPDATELINK(links[i]);
  }

  /*
  SUPERV_StreamLinks slinks = myMain->getDataflow()->StreamLinks();
  for (int i = 0; i < slinks->length(); i++) {
    UPDATELINK(slinks[i]);
  }
  */
  /*
  if (myMain->getDataflow()->IsStreamGraph()) {
    SUPERV_StreamGraph aSGraph = myMain->getDataflow()->ToStreamGraph();
    if (!SUPERV_isNull(aSGraph)) {
      SUPERV_StreamLinks slinks = aSGraph->StreamLinks(); // <<- doesn't work!!!
      for (int i = 0; i < slinks->length(); i++) {
	UPDATELINK(slinks[i]);
      }
    }
  }
  */

  // update stream links by old method until StreamLinks() fuction works
  SUPERVGUI_CanvasPort* aPort;
  QObjectList* aPortList = queryList("SUPERVGUI_CanvasPort");
  QObjectListIt aPortIt(*aPortList);
  while ((aPort=(SUPERVGUI_CanvasPort*)aPortIt.current()) != 0) {
    ++aPortIt;
    if (aPort->getEngine()->IsInput() && aPort->getEngine()->IsLinked() 
	&& aPort->getEngine()->Kind() == SUPERV::DataStreamParameter) {
      if (aPort->getEngine()->Node()->Kind() == SUPERV::EndSwitchNode) {
	SUPERV_Links aLinks = aPort->getEngine()->Links();
	for (int i = 0; i < aLinks->length(); i++) {
	  UPDATELINK(aLinks[i]);
	}
      }
//    else if (aPort->isStream()) {
// 	SUPERV_StreamLink aStreamLink = ((SUPERVGUI_CanvasStreamPortIn*)aPort)->getStreamEngine()->StreamLink();
// 	UPDATESTREAMLINK(aStreamLink);
//    }
      else {
	SUPERV_Link aLink = aPort->getEngine()->Link();
	UPDATELINK(aLink);
      }
    }
  }
  delete aPortList;

  it = QObjectListIt(*ihmList);
  while ((ihmLink=(SUPERVGUI_CanvasLink*)it.current()) != 0) {
    ++it;
    ihmList->removeRef(ihmLink);
    delete ihmLink;
  }
  delete ihmList;
}

SUPERVGUI_CanvasNode* SUPERVGUI_Canvas::getNode(SUPERV::CNode_ptr theNode) const
{
  return (SUPERVGUI_CanvasNode*) 
    ((SUPERVGUI_Canvas*)this)->child(theNode->Name(), "SUPERVGUI_CanvasNode");
}

SUPERVGUI_CanvasPort* SUPERVGUI_Canvas::getPort(SUPERV::Port_ptr thePort) const
{
  SUPERVGUI_CanvasNode* aNode = getNode(thePort->Node());
  if (aNode == NULL) return NULL;

  return (SUPERVGUI_CanvasPort*) 
    aNode->child(getPortName(thePort), "SUPERVGUI_CanvasPort");
}

SUPERVGUI_CanvasLink* SUPERVGUI_Canvas::getLink(SUPERV::Link_ptr theLink) const
{
  return (SUPERVGUI_CanvasLink*) 
    ((SUPERVGUI_Canvas*)this)->child(getLinkName(theLink), "SUPERVGUI_CanvasLink");
}

QString SUPERVGUI_Canvas::getPortName(SUPERV::Port_ptr thePort) const
{
  QString aName(thePort->Name());
  if (thePort->IsInput()) aName = aName + ".In";
  else aName = aName + ".Out";
  return aName;
}

QString SUPERVGUI_Canvas::getLinkName(SUPERV::Link_ptr theLink) const
{
  SUPERV_Port aInPort = theLink->InPort();
  SUPERV_Port aOutPort = theLink->OutPort();
  QString aName = 
    QString(aOutPort->Node()->Name()) + "." +
    QString(aOutPort->Name()) + "-" +
    QString(aInPort->Node()->Name()) + "." +
    QString(aInPort->Name());
  return aName;
}

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI.cxx
//  Author : Francis KLOSS
//  Module : SUPERV

#include "SUPERVGUI.h"
#include "SUPERVGUI_DataModel.h"
#include "SUPERVGUI_Selection.h"

#include "CAM_Application.h"
#include "OB_Browser.h"
#include "OB_ListItem.h"
#include "SalomeApp_Application.h"
#include "SalomeApp_ImportOperation.h"
#include "LightApp_SelectionMgr.h"
#include "SalomeApp_Study.h"
#include "SUIT_Desktop.h"
#include "SUIT_FileDlg.h"
#include "SUIT_MessageBox.h"
#include "SUIT_Operation.h"
#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"
#include "SUIT_Study.h"

#include "SUPERVGraph_ViewManager.h"
#include "SUPERVGraph_ViewFrame.h"

#include "SALOME_ListIO.hxx"
#include "SALOME_ListIteratorOfListIO.hxx"
#include "SALOME_InteractiveObject.hxx"
#include "Utils_ORB_INIT.hxx"

#include CORBA_SERVER_HEADER(SALOMEDS_Attributes)

#include <qinputdialog.h>
#include <qmenubar.h>

#include "SALOMEDSClient.hxx"
#include <boost/shared_ptr.hpp>
using namespace boost;

int SUPERVGUI::factory = 0;

extern "C" {
  Standard_EXPORT CAM_Module* createModule() {
    return new SUPERVGUI();
  }
}


SUPERVGUI::SUPERVGUI()
     : SalomeApp_Module( "SUPERV" ),
       study(0),
       browser(0),
       myFirstActivation(true),
       myAllGraphsClosed(false),
       myInitialViewFrame(0),
       myIVFBackgroundColor(QColor())
  //info(0)
{
    Trace("SUPERVGUI::SUPERVGUI")
    if (factory==0) {
        factory = 1;
        MESSAGE("SUPERVGUI Info : factory build");
    } else {
        MESSAGE("SUPERVGUI Error : another call to factory constructor");
    };
}

SUPERVGUI::~SUPERVGUI() {
    Trace("SUPERVGUI::~SUPERVGUI")
    if (factory==1) {
        factory = 0;
        //if (!engine->_is_nil()) CORBA::release(engine);
        //info->close();
        if (browser) {
          if (browser->isVisible())
            browser->close();
          delete browser;
        }
        MESSAGE("SUPERVGUI Info : factory destroy");
    } else {
        MESSAGE("SUPERVGUI Error : another call to factory destructor");
    };
}

SUPERVGUI* SUPERVGUI::Supervision() {
  CAM_Application* anApp = dynamic_cast<CAM_Application*>( SUIT_Session::session()->activeApplication() );
  if ( anApp ) {
    SUPERVGUI* aSupervModule = dynamic_cast<SUPERVGUI*>( anApp->activeModule() );
    return aSupervModule;
  }
  return NULL;
}

void SUPERVGUI::createSupervAction (const int id, const QString& po_id, const QString& icon_id,
                                    const int key, const bool toggle)
{
  QIconSet icon;
  QWidget* parent = application()->desktop();
  SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();
  QPixmap pix;
  if (!icon_id.isEmpty())
    pix = resMgr->loadPixmap("SUPERV", tr(icon_id));
  //else
  //  pix = resMgr->loadPixmap("SUPERV", tr(QString("ICO_") + po_id));

  if (!pix.isNull())
    icon = QIconSet(pix);

  QString tooltip    = tr(QString("TOP_") + po_id),
          menu       = tr(QString("MEN_") + po_id),
          status_bar = tr(QString("STB_") + po_id);

  createAction(id, tooltip, icon, menu, status_bar, key, parent, toggle, this, SLOT(OnGUIEvent()));
}

void SUPERVGUI::initialize( CAM_Application* app )
{
  SalomeApp_Module::initialize( app );

  SALOME_NamingService* namingService = (( SalomeApp_Application* )application())->namingService();
  loadEngine(namingService);

  browser = new SUPERVGUI_Service(namingService);
  //info    = new SUPERVGUI_Information();
  cursor  = application()->desktop()->cursor();

  fillInterfaceNameMap();

  // ----- create actions --------------
  createSupervAction( 301, "IMPORT"        , "ICO_IMPORT" );
  createSupervAction( 302, "EXPORT"        , "ICO_EXPORT" );

  createSupervAction( 303, "NEW"           , "ICO_NEW" );
  createSupervAction( 304, "MODIFY"        , "ICO_MODIFY" );

  createSupervAction( 305, "RELOAD"        , "ICO_RELOAD" );
  createSupervAction( 306, "RUN"           , "ICO_RUN" );
  createSupervAction( 307, "KILL"          , "ICO_KILL" );
  createSupervAction( 308, "SUSPEND/RESUME", "ICO_SUSPEND/RESUME" );
  createSupervAction( 309, "ADDNODE"       , "ICO_ADDNODE" );
  createSupervAction( 310, "STEPBYSTEP"    , "ICO_STEPBYSTEP" );

  createSupervAction( 311, "POP_RENAME", "", Key_F2 );
  createSupervAction( 312, "POP_DISPLAY" );
  createSupervAction( 313, "POP_DELETE", "", Key_Delete );

  // ----- create menu -----------------
  int fileId = createMenu( tr( "MEN_FILE" ), -1, -1 );
  createMenu( 301, fileId, 10 );
  createMenu( 302, fileId, 10 );
  createMenu( separator(), fileId, 10 );

  int editId = createMenu( tr( "MEN_EDIT" ), -1, -1 );
  createMenu( separator(), fileId, 7 );
  createMenu( 303, editId, -1 );
  createMenu( 304, editId, -1 );

  int supervId = createMenu( tr( "MEN_SUPERV" ), -1, -1, 10/*, 300*/ );
  createMenu( 305, supervId, -1 );
  createMenu( 309, supervId, -1 );
  createMenu( separator(), fileId, 3 );
  createMenu( 306, supervId, -1 );
  createMenu( 310, supervId, -1 );
  createMenu( 307, supervId, -1 );
  createMenu( 308, supervId, -1 );

  // ----- create toolbars -------------
  int supervisorId = createTool( tr( "TOOL_SUPERVISOR" ) );
  createTool( 301, supervisorId );
  createTool( 302, supervisorId );
  createTool( 303, supervisorId );
  createTool( 304, supervisorId );
  createTool( 305, supervisorId );
  createTool( 309, supervisorId );

  int executionId = createTool( tr( "TOOL_EXECUTION" ) );
  createTool( 306, executionId );
  createTool( 310, executionId );
  createTool( 307, executionId );
  createTool( 308, executionId );

  // ----- create popup for object browser items -------------
  QtxPopupMgr* mgr = popupMgr();
  mgr->insert( action(  311 ), -1, -1 ); // rename dataflow
  mgr->setRule( action( 311 ), "$type in {'Dataflow'} and selcount=1", true );
  mgr->insert( action(  312 ), -1, -1 ); // display dataflow
  mgr->setRule( action( 312 ), "$type in {'Dataflow'} and selcount=1", true );
  mgr->insert( separator(), -1, -1 ); // -----------
  mgr->insert( action(  313 ), -1, -1 ); // delete object
  mgr->setRule( action( 313 ), "client='ObjectBrowser' and $type in {'Dataflow' 'SupervisorObject'} and selcount>0", true );
}

bool SUPERVGUI::activateModule( SUIT_Study* theStudy )
{
  if ( CORBA::is_nil( engine ) )
    return false;

  if ( !SalomeApp_Module::activateModule( theStudy ) )
    return false;



  //---------------------------------------
  setMenuShown( true );
  setToolShown( true );

  study = application()->activeStudy();

  connect( application()->desktop(), SIGNAL( windowActivated( SUIT_ViewWindow* ) ),
           this, SLOT( setMain( SUIT_ViewWindow* ) ) );

  if ( myFirstActivation ) {
    myAllGraphsClosed = false;
    // create new empty dataflow
    createDataflow( New, true );
  }

  return true;
}

QString SUPERVGUI::engineIOR() const
{
 if ( !CORBA::is_nil( engine ) )
    return QString( getApp()->orb()->object_to_string( engine));
  return QString( "" );
}

bool SUPERVGUI::deactivateModule( SUIT_Study* theStudy )
{
  setMenuShown( false );
  setToolShown( false );

  disconnect( application()->desktop(), SIGNAL( windowActivated( SUIT_ViewWindow* ) ),
              this, SLOT( setMain( SUIT_ViewWindow* ) ) );
  //---------------------------------------

  if ( !myAllGraphsClosed )
    myFirstActivation = false;

  return SalomeApp_Module::deactivateModule( theStudy );
}

CAM_DataModel* SUPERVGUI::createDataModel()
{
  return new SUPERVGUI_DataModel( this );
}

LightApp_Selection* SUPERVGUI::createSelection() const
{
  return new SUPERVGUI_Selection();
}

SUIT_ViewWindow* SUPERVGUI::createGraph() {
  SUPERVGraph_ViewManager* aVM = new SUPERVGraph_ViewManager( study, application()->desktop(), new SUPERVGraph_Viewer() );
  (( SalomeApp_Application* )application())->addViewManager( aVM );
  SUPERVGraph_ViewFrame* aVW = dynamic_cast<SUPERVGraph_ViewFrame*>( aVM->createViewWindow() );
  if ( aVW ) {
    QColor back = getApp()->resourceMgr()->colorValue( "SUPERVGraph", "Background", DEF_MAIN_COLOR );
    qDebug( QString( "background: %1 %2 %3" ).arg( back.red() ).arg( back.green() ).arg( back.blue() ) );
    aVW->setBackgroundColor( back );
    connect( aVM,
             SIGNAL( deleteView( SUIT_ViewWindow* ) ),
             this,
             SLOT( onGraphClosed( SUIT_ViewWindow* ) ) );
  }
  return aVW;
}



void SUPERVGUI::loadEngine(SALOME_NamingService* namingService) {
    Trace("SUPERVGUI::loadEngine")
    SALOME_LifeCycleCORBA LCC(namingService);
    Engines::Component_var objComponent = LCC.FindOrLoad_Component("SuperVisionContainer", "SUPERV");
    if (CORBA::is_nil(objComponent)) {
        QMessageBox::warning(application()->desktop(), tr("ERROR"), tr("MSG_CANT_LOAD_SUPERV"));
        return;
    };

    SUPERV::SuperG_var aSuperVisionComponent;
    aSuperVisionComponent = SUPERV::SuperG::_narrow(objComponent);
    if (CORBA::is_nil(aSuperVisionComponent)) {
        QMessageBox::warning(application()->desktop(), tr("ERROR"), tr("MSG_CANT_NARROW_SUPERV"));
        return;
    };

    //    engine  = new SUPERV::SuperG_var;
    engine = aSuperVisionComponent;
}

/*
bool SUPERVGUI::information(SUPERV_CNode node, bool isReadOnly) {
    Trace("SUPERVGUI::information")
    return(info->run(node, isReadOnly));
}
*/
void SUPERVGUI::putInfo(const char* mess) {
    Trace("SUPERVGUI::putInfo")
    application()->putInfo(mess);
}

SUPERV_Engine SUPERVGUI::getEngine() {
    Trace("SUPERVGUI::getEngine")
    return(engine);
}

SUPERVGUI_Service* SUPERVGUI::getBrowser() {
    Trace("SUPERVGUI::getBrowser")
    return(browser);
}

QCursor SUPERVGUI::getCursor() {
    Trace("SUPERVGUI::getCursor")
    return(cursor);
}

SUPERVGUI_Main* SUPERVGUI::getMain() {
    Trace("SUPERVGUI::getMain")
    return main;
}


void SUPERVGUI::registerGraph(QString theName, SUPERVGUI_Main* theGraph){
  theGraph->setHashCode(theName);
  myGraphList.append(theGraph);
}


void SUPERVGUI::unregisterGraph(SUPERVGUI_Main* theGraph) {
  SUPERVGUI_Main* aMain;
  for (aMain = myGraphList.first(); aMain; aMain = myGraphList.next()) {
    if (aMain == theGraph)
      myGraphList.remove(aMain);
  }
}


bool SUPERVGUI::isContains(SUIT_Study* theStudy, const QString theKey) {
  SUPERVGUI_Main* aMain;
  for (aMain = myGraphList.first(); aMain; aMain = myGraphList.next()) {
    if ((aMain->getStudy() == theStudy) && (aMain->getHashCode() == theKey))
      return true;
  }
  return false;
}


void SUPERVGUI::onGraphClosed(SUIT_ViewWindow* theViewWindow) {
  if( SUPERVGraph_ViewFrame* supervFrame = dynamic_cast<SUPERVGraph_ViewFrame*>(theViewWindow) ) {
    disconnect(supervFrame->getViewManager(), 0 , this, 0);
    SUPERVGraph_View* view = supervFrame->getViewWidget();
    SUPERVGUI_Main* aGraph = dynamic_cast<SUPERVGUI_Main*>(view);
    if ( aGraph ) {
      // mkr: PAL12449 --->
      if ( (( SalomeApp_Study* )(aGraph->getStudy()))->studyDS() && !aGraph->isDataflowInStudy() ) {
	const int aWarnResult = QMessageBox::information(application()->desktop(), tr("MSG_INFO"),
							 tr("MSG_GRAPH_UNSAVED"),
							 QMessageBox::Yes, QMessageBox::No);
	if ( aWarnResult == QMessageBox::Yes ) {
	  aGraph->addDataflowToStudy();
	  updateObjBrowser();
	}
      }
      // mkr: PAL12449 <---
      
      unregisterGraph(aGraph);
      SUPERV_Graph aDataFlow = aGraph->getDataflow();
      if ( !SUPERV_isNull( aDataFlow ) ) {
	if ( aDataFlow->IsExecuting() ) {
	  const int aMsgResult = QMessageBox::warning(application()->desktop(),
						      tr("WARNING"),
						      tr("MSG_DF_RUNNING"),
						      tr("MSG_DF_EXECUTION"),
						      tr("MSG_DF_KILL"));
	  if ( aMsgResult == 1 ) {
	    // KILL EXECUTION
	    if ( aDataFlow->Kill() ) {
	      // Kill() sends KillEvent and KillState to SUPERVGUI_Thread
	      // while sets myIsActive flag to false when it receives such event/state
	      // after myIsActive is false it calls QThread::exit() to terminate.
	      
	      // why while() { qApp->processEvents() } ?
	      // because: SUPERVGUI_Thread::run() receives events, and calls myMain->execute()
	      // method using SALOME_Event paradigm, ProcessVoidEvent() function -
	      // it puts this event to the main application event loop, in which we are being now.
	      // So if we block main GUI application thread (by calling aGraph->getMyThread()->wait() here)
	      // then we will have a deadlock of 2 threads waiting for one another
	      while (aGraph->getMyThread()->running())
		qApp->processEvents();
	    }
	  }
	  else { // BACKGROUND EXECUTION
	    emit KillMainThread(true); // set SUPERVGUI_Thread::myIsActive to false
	    while ( aGraph->getMyThread()->running() )
	      qApp->processEvents();
	  }
	}
	else { // EXECUTION IS FINISHED, JUST DESTROY THE SUPERVGUI_Thread object and return.
	  emit KillMainThread(true);
	  while ( aGraph->getMyThread()->running() )
	    qApp->processEvents();
	}
      }
    }
  }
  if ( myGraphList.isEmpty() ) {
    myAllGraphsClosed = true;
    myFirstActivation = true;
    myInitialViewFrame = 0;
    myIVFBackgroundColor = QColor();
    main = 0;
  }
}

void SUPERVGUI::setMain( SUIT_ViewWindow* w) {
  main = 0;
  if ( !w ) return;

  study = w->getViewManager()->study();
  if (!study)
    return;

  if(SUPERVGraph_ViewFrame* supervFrame = dynamic_cast<SUPERVGraph_ViewFrame*>(w)){
    SUPERVGraph_View* view = supervFrame->getViewWidget();
    if (view) {
      main = dynamic_cast<SUPERVGUI_Main*>(view);
      connect(this, SIGNAL(KillMainThread(bool)), main->getMyThread(), SLOT(KillThread(bool)));
    }
  }
  Trace("SUPERVGUI::setMain - main: "<<main);
}

void SUPERVGUI::displayDataflow() {
  Trace("SUPERVGUI::displayDataflow");

  OB_Browser* aBrowser = getApp()->objectBrowser();
  OB_ListItem* item = (OB_ListItem*)(aBrowser->listView()->currentItem());

  SalomeApp_Study* anAppStudy = (SalomeApp_Study*)(getApp()->activeStudy());
  _PTR(SObject) aObj (anAppStudy->studyDS()->FindObjectID(item->text(2).latin1()));

  SUPERV_Graph aDataFlow;
  QString aIORName;

  _PTR(GenericAttribute) anAttr;
  if (aObj->FindAttribute(anAttr, "AttributeIOR")) {
    _PTR(AttributeIOR) anIOR ( anAttr );
    aIORName = QString(anIOR->Value().c_str());
    if (isContains(study, aIORName)) {
      if (QMessageBox::warning(application()->desktop(), tr("WARNING"),
                               tr("MSG_GRAPH_DISPLAYED").arg(""),
                               QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
        return;
    }

    //SUPERV_Graph aDataFlow = engine->getStreamGraph(anIOR->Value().c_str());
    aDataFlow = engine->getGraph(anIOR->Value().c_str());
    if (SUPERV_isNull(aDataFlow)) {
      QMessageBox::warning(0, tr("ERROR"), tr("MSG_ACCESS_BAD_IOR"));
      return;
    }

    /* ASV: 20.10.04: fix for 6896
    if (aDataFlow->IsStreamGraph()) {
      SUPERV_StreamGraph aDataFlowStream = aDataFlow->ToStreamGraph();
      if (SUPERV_isNull(aDataFlowStream)) {
        QMessageBox::warning(0, tr("ERROR"), tr("MSG_ACCESS_BAD_IOR"));
        return;
      }
      aDataFlow = aDataFlowStream;
    }
    */
  }

  SUPERVGraph_ViewFrame* aViewFrame;
  if ( !myInitialViewFrame ) {
    // first case : create a ViewFrame object
    aViewFrame = dynamic_cast<SUPERVGraph_ViewFrame*>( createGraph() );
  }
  else {
    // second case : get empty initial view frame as new ViewFrame object
    aViewFrame = myInitialViewFrame;
    // mkr : IPAL12991 -->
    SUPERVGraph_View* view = myInitialViewFrame->getViewWidget();
    if (view)
      unregisterGraph(dynamic_cast<SUPERVGUI_Main*>(view));
    // mkr : IPAL12991 <--
    // mkr : PAL8237 : remove first "gag" Main from children
    // srn: commented as it cases on application exit "X Windows error"
    //aViewFrame->removeChild( aViewFrame->queryList("SUPERVGUI_Main")->first() );
    delete view;
  }

  if ( aViewFrame ) {
    main = new SUPERVGUI_Main( aViewFrame, application()->desktop(), aDataFlow );
    registerGraph( aIORName, main );
    aViewFrame->show();
  }

  if ( myInitialViewFrame ) {
    myInitialViewFrame = 0;
    myIVFBackgroundColor = QColor();
  }
}


void SUPERVGUI::renameDataflow() {
  SALOME_ListIO aList;
  aList.Clear();
  (( SalomeApp_Application* )application())->selectionMgr()->selectedObjects( aList );

  SALOME_ListIteratorOfListIO It( aList );
  for ( ; It.More(); It.Next() ) {
    Handle(SALOME_InteractiveObject) IObject = It.Value();

    _PTR(Study) aStudy = (( SalomeApp_Study* )study)->studyDS();
    _PTR(SObject) obj ( aStudy->FindObjectID( IObject->getEntry() ) );
    _PTR(GenericAttribute) anAttr;
    if ( obj ) {
      if ( obj->FindAttribute(anAttr, "AttributeName") ) {
        _PTR(AttributeName) aName ( anAttr );
        QString nm = QString( aName->Value().c_str() );
	bool ok;
	nm = QInputDialog::getText( tr( "Rename" ), tr( "Enter new name:" ), QLineEdit::Normal,
                                    nm, &ok, application()->desktop() );
        if ( ok && !nm.isEmpty() ) {
          // sak : 24.11.04 : fix for PAL6898 : if rename fails (study locked),
          // a message box is displayed, and cursor is "wait cursor".  We think that "wait cursor"
          // is not neccessary here, because the rename operation is fast.
          //QApplication::setOverrideCursor( Qt::waitCursor );
          //study->renameIObject( IObject, nm );

          //--->
          aName->SetValue( nm.latin1() );
          //rename Interactive object
          IObject->setName( ( char* )nm.latin1() );
          //<---

          updateObjBrowser();
          //QApplication::restoreOverrideCursor();

	  // mkr : PAL7037 => rename engine of the graph (i.e. corresponding SUPERV_Graph) -->
          if ( obj->FindAttribute(anAttr, "AttributeIOR") ) {
            _PTR(AttributeIOR) anIOR ( anAttr );
            SUPERV_Graph aDataFlow = engine->getGraph(anIOR->Value().c_str());
            if ( !SUPERV_isNull(aDataFlow) ) {
              if ( aDataFlow->IsStreamGraph() ) {
		SUPERV_StreamGraph aStreamDataFlow = aDataFlow->ToStreamGraph();
                if ( !SUPERV_isNull(aStreamDataFlow) )
                  aStreamDataFlow->SetName( nm.latin1() );
              }
              else {
                aDataFlow->SetName( nm.latin1() );
              }

              // update "Save" icon and menu state, if dataflow is published
              SUPERVGUI_Main::setModifiedFlag();  
            }
          }
	  // mkr : PAL7037 <--
        }
      }
    }
  }
}

void SUPERVGUI::exportDataflow() {
    Trace("SUPERVGUI::exportDataflow")
    if ( main==0 ) {
      QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_EXPORT"));
    }
    else if ( SUPERV_isNull( main->getDataflow() ) ) { // should not normally happen..
      QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_EXPORT"));
    }
    else {
      QString f = SUIT_FileDlg::getFileName(application()->desktop(),
                                            "",
                                            "*.xml",
                                            tr("TTL_EXPORT_DATAFLOW"),
                                            false);
      if ( !f.isEmpty() ) {

        // asv : bug [VSR Bugs and Improvements in Supervisor] 1.8 : when exporting a file,
        // a backup copy of an existing file must be created (in case Export fails..)
        QString aBackupFile = SUPERVGUI::createBackupFile( f );

        if ( main->getDataflow()->Export(f.latin1()) ) {
	  // mkr : PAL8110 : if we re-register graph with its xml-file name the "Supervisor Warning"
	  //       after object browser popup "Display" item will not appear, it's an error,
	  //       because of this dataflow is already displayed.
          //unregisterGraph(main);
          //registerGraph(f, main);

          // remove a backup file if export was successfull
          if ( !aBackupFile.isNull() && !aBackupFile.isEmpty() )
            QFile::remove( aBackupFile );
        }
        else {
          QMessageBox::warning(application()->desktop(), tr("ERROR"), tr("MSG_BAD_WRITING").arg(aBackupFile));
        }
      }
    }
}

/**
 *If a file with theOriginalFileName exists, then the function tries to create
 * its backup copy of it (the_name + ".bakX", X = 0,1,2,..,99).  Returns backup file's name
 * If a file with theOriginalFileName does not exist, or creation fails - Return empty string.
 */
QString SUPERVGUI::createBackupFile( const QString& theOriginalFileName ) {
  QString theBackupFileName("");
  if ( QFile::exists( theOriginalFileName ) ) {
    // 1. construct a backup file name
    int i = 0;
    do {
      theBackupFileName = theOriginalFileName;
      theBackupFileName += ".bak";
      theBackupFileName += QString::number(i++);
    }
    while ( QFile::exists( theBackupFileName ) && i < 100 ); // max 99 backup files
    // if *.bak99 exists -- it will be overwritten

    // 2. copy the original file to theBackupFileName
    QString cmd( "cp \"");
    cmd += theOriginalFileName;
    cmd += "\" \"";
    cmd += theBackupFileName;
    cmd += "\"";
    bool res = system( cmd.latin1() );
    if ( res )
      theBackupFileName = QString("");
  }
  return theBackupFileName;
}

void SUPERVGUI::windows( QMap<int, int>& mappa ) const
{
  mappa.insert( SalomeApp_Application::WT_ObjectBrowser, Qt::DockLeft );
  mappa.insert( SalomeApp_Application::WT_PyConsole, Qt::DockBottom );
  mappa.insert( SalomeApp_Application::WT_LogWindow, Qt::DockBottom );
}

void SUPERVGUI::newDataflow() {
  createDataflow( New );
}

void SUPERVGUI::importDataflow() {
  createDataflow( Import );
}

void SUPERVGUI::modifyDataflow() {
  createDataflow( Modify );
}

bool SUPERVGUI::createDataflow( const NEW_DF_MODE mode, bool theInitialDF ) {
  Trace("SUPERVGUI::createDataflow, mode = " << mode );

  // asv : 27.01.05 : fix for PAL7823 : if the study is locked - warn the user and return.
  _PTR(Study) aStudy = (( SalomeApp_Study* )study)->studyDS();
  if ( aStudy->GetProperties()->IsLocked() ) {
    QMessageBox::warning(application()->desktop(), tr("WRN_WARNING"), tr("WRN_STUDY_LOCKED"));
    return false;
  }

  SUPERV::Graph_var aGraph;

  QString f; // file name.  declared here because may be used below in error msg construction..
  if ( mode == Import || mode == Modify ) { // 0. import or modify existing dataflow - select XML file
    QString title = QString( "TLT_CREATE_DF_%1" ).arg( mode );
    f = SUIT_FileDlg::getFileName( application()->desktop(), "", "*.xml", tr( title ), true );
    if ( f.isEmpty() ) // pressed Cancel in file select dialog
      return false;

    // check if study already contains a graph imported from the same file
    if ( isContains( study, f ) ) {
      if ( QMessageBox::warning( application()->desktop(), tr("WARNING"), tr("MSG_GRAPH_DISPLAYED").arg(f),
                                 QMessageBox::Yes, QMessageBox::No) == QMessageBox::No )
        return false;
    }

    // 1. create a graph
    aGraph = ( mode == Import ) ? engine->StreamGraphE( f ) : engine->StreamGraph( f );
  }
  else
    aGraph = engine->StreamGraph( MAIN_NEW );

  if ( CORBA::is_nil( aGraph ) ) {
    QString msg( mode == New ? tr("MSG_CANT_CREATE_DF") : tr("MSG_BAD_FILE").arg(f) );
    SUIT_MessageBox::warn1( application()->desktop(), tr("ERROR"), tr(msg), tr("BUT_OK") );
    return false;
  }

  SUPERVGraph_ViewFrame* aViewFrame;
  if ( theInitialDF || !myInitialViewFrame ) {
    // 2. create a ViewFrame object
    aViewFrame = dynamic_cast<SUPERVGraph_ViewFrame*>( createGraph() );
  }
  else {
    // 2. get empty initial view frame as new ViewFrame object
    aViewFrame = myInitialViewFrame;
    // mkr : IPAL12991 -->
    if ( myInitialViewFrame ) {
      SUPERVGraph_View* view = myInitialViewFrame->getViewWidget();
      if (view)
	unregisterGraph(dynamic_cast<SUPERVGUI_Main*>(view));
    }
    // mkr : IPAL12991 <--
    // mkr : PAL8237 : remove first "gag" Main from children
    // srn: commented as it cases on application exit "X Windows error"
    //aViewFrame->removeChild( aViewFrame->queryList("SUPERVGUI_Main")->first() );
  }

  if ( aViewFrame ) {

    // 3. bind Graph and ViewFrame == create SUPERVGUI_Main object
    main = new SUPERVGUI_Main( aViewFrame, application()->desktop(), aGraph );
    if ( mode == New ) {
      if ( !theInitialDF )	
        main->addNode();
      else
        myInitialViewFrame = aViewFrame;
    }
    // mkr : IPAL11550 : register graph not depend on creation mode
    registerGraph( f, main );
    main->resizeView( new QResizeEvent( aViewFrame->size(), aViewFrame->size() ) );
    aViewFrame->show();
    QFileInfo inf( f );
    if( !f.isEmpty() )
      aViewFrame->setCaption( inf.fileName() );
  }

  if ( !theInitialDF ) {
    myInitialViewFrame = 0;
    myIVFBackgroundColor = QColor();
  }

  return true;
}

void SUPERVGUI::reloadDataflow() {
  Trace("SUPERVGUI::reloadDataflow");
  if ( main )
    main->sync();
  else
    QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_RELOAD"));
}

void SUPERVGUI::runDataflow() {
  Trace("SUPERVGUI::runDataflow");
  if ( main )
    main->run( /*andSuspend=*/false );
  else
    QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_RUN"));
}

void SUPERVGUI::stepByStep() {
  Trace("SUPERVGUI::stepByStep");
  if ( main )
    main->run( /*andSuspend=*/true );
  else
    QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_RUN"));
}

void SUPERVGUI::killDataflow() {
  Trace("SUPERVGUI::killDataflow");
  if ( main )
    main->kill();
  else
    QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_KILL"));
}

void SUPERVGUI::suspendResumeDataflow() {
  Trace("SUPERVGUI::suspendResumeDataflow");
  if ( main )
    main->suspendResume();
  else
    QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_SUSPEND"));
}

void SUPERVGUI::showComponents() {
  Trace("SUPERVGUI::showComponents");
  if ( main ) {
    if ( main->isEditable() )
      main->addNode();
    else
      QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOTEDITABLE"));
  }
  else
    QMessageBox::warning(application()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_ADD"));

}


/*bool SUPERVGUI::CustomPopup(QPopupMenu* popup, const QString& theContext, const QString& theParent, const QString& theObject) {
  Trace("SUPERVGUI::customPopup");
  Supervision.init();
  if(popup->count() > 3) {
    // Temporary code: is to be deleted soon
    popup->removeItem(QAD_Display_Popup_ID);
    popup->removeItem(QAD_DisplayOnly_Popup_ID);
    popup->removeItem(QAD_Erase_Popup_ID);
    int id = popup->idAt(popup->count() - 1);
    if (id < 0 && id != -1) popup->removeItem(id); // separator
    // Temporary code end
  }

  SALOME_ListIO aList;
  aList.Clear();
  (( SalomeApp_Application* )application())->selectionMgr()->selectedObjects( aList );
  if ( aList.Extent() == 0 ) return false;

  if ( aList.Extent() == 1 ) {
    Handle(SALOME_InteractiveObject) anIObj = aList.First();

    // insert SUPERV-specific popup items here

    bool isDataflow, ownObj;
    whatIsSelected(anIObj, ownObj, isDataflow);

    if (isDataflow) {
      popup->insertItem(tr( "MSG_RENAME" ), this, SLOT(renameDataflow()));
      popup->insertItem("Display", this, SLOT(displayDataflow()));
      popup->insertSeparator();
    }
    if (ownObj)
      popup->insertItem(tr("MSG_DELETE"), this, SLOT(deleteObject()));
  }

  if ( aList.Extent() > 1 ) {
    int aIObjCount = 0;
    SALOME_ListIteratorOfListIO It_forCheckOwner( aList );
    for(;It_forCheckOwner.More();It_forCheckOwner.Next()) {
      Handle(SALOME_InteractiveObject) anIObj = It_forCheckOwner.Value();
      bool aIsOwner, aIsDataflow;
      whatIsSelected(anIObj, aIsOwner, aIsDataflow);

      if (aIsOwner) aIObjCount++;
    }
    if ( aIObjCount == aList.Extent() )  //all selected objects belong to Supervision
      popup->insertItem(tr("MSG_DELETE"), this, SLOT(deleteObject()));
  }

  return(true);
  }*/



/** Returns:
    theIsOwner = true if Selected object belongs to Supervision.
    theIsDataflow = true if Selected object is Dataflow
*/
void SUPERVGUI::whatIsSelected(const _PTR(SObject)& theObj, bool& theIsOwner, bool& theIsDataflow) {
  theIsOwner = false;
  theIsDataflow = false;

  if ( theObj ) {
    _PTR(SComponent) comp ( theObj->GetFatherComponent() );
    if ( comp ) {
      _PTR(GenericAttribute) anAttr;
      if (comp->FindAttribute(anAttr, "AttributeName")) {
        _PTR(AttributeName) aName ( anAttr );
        QString compName(aName->Value().c_str());
        if ( compName.compare( moduleName() ) == 0 ) {
          _PTR(GenericAttribute) anAttr;
          if (theObj->FindAttribute(anAttr, "AttributeIOR")) {
            _PTR(AttributeIOR) anIOR ( anAttr );
            SUPERV_Graph aDataFlow = engine->getGraph(anIOR->Value().c_str());
            if (!SUPERV_isNull(aDataFlow)) {
              if (aDataFlow->IsStreamGraph()) {
                if (!SUPERV_isNull(aDataFlow->ToStreamGraph()))
                  theIsDataflow = true;
              }
              else
                theIsDataflow = true;
            }
          }
          CORBA::String_var anObjectID = theObj->GetID().c_str();
          CORBA::String_var aComponentID = comp->GetID().c_str();
          if (strcmp(anObjectID, aComponentID))
            theIsOwner = true;
        }
      }
    }
  }
}

void SUPERVGUI::deleteObject() {
  SALOME_ListIO aList;
  aList.Clear();
  (( SalomeApp_Application* )application())->selectionMgr()->selectedObjects( aList, QString::null, false );
  if ( aList.Extent() == 0 ) return;

  // sak : 24.11.04 : fix for PAL6899 : if the study is locked - warn the user and return.
  _PTR(Study) aStudy = (( SalomeApp_Study* )study)->studyDS();
  if ( aStudy->GetProperties()->IsLocked() ) {
    QMessageBox::warning(application()->desktop(), tr("WRN_WARNING"), tr("WRN_STUDY_LOCKED"));
    return;
  }

  // sak : 24.11.04 : fix for PAL6901 : Incorrect deletion of the objects : pressing ESC button
  // emitted "YES" button click, now it is corrected, and treated as "NO" button click.
  if (QMessageBox::warning(application()->desktop(),
                           tr("WARNING"),
                           tr("MSG_ASK_DELETE"),
                           QMessageBox::Yes, QMessageBox::No) != QMessageBox::Yes)
    return;

  SALOME_ListIteratorOfListIO It( aList );
  for(;It.More();It.Next()) {
    Handle(SALOME_InteractiveObject) anIObj = It.Value();
    //bool aIsOwner, aIsDataflow;
    //whatIsSelected(anIObj, aIsOwner, aIsDataflow);

    _PTR(SObject) aObj ( aStudy->FindObjectID( anIObj->getEntry() ) );
    if ( aObj ) {
      SUIT_Operation* op = new SalomeApp_ImportOperation( application() );
      _PTR(StudyBuilder) aBuilder ( aStudy->NewBuilder() );
      op->start();
      aBuilder->RemoveObjectWithChildren( aObj );
      op->commit();
    }
  }
  (( SalomeApp_Application* )application())->selectionMgr()->clearSelected();
  updateObjBrowser();
}

void SUPERVGUI::OnGUIEvent()
{
  const QObject* obj = sender();
  if ( !obj || !obj->inherits( "QAction" ) )
    return;
  int id = actionId((QAction*)obj);
  if ( id != -1 )
    OnGUIEvent( id );
}

bool SUPERVGUI::OnGUIEvent(int command) {
  switch (command) {
  case 301:
    importDataflow();
    return(false);

  case 302:
    exportDataflow();
    return(false);

  case 303:
    newDataflow();
    return(false);

  case 304:
    modifyDataflow();
    return(false);

  case 305:
    reloadDataflow();
    return(false);

  case 306:
    runDataflow();
    return(false);

  case 307:
    killDataflow();
    return(false);

  case 308:
    suspendResumeDataflow();
    return(false);

  case 309:
    showComponents();
    return(false);

  case 310:
    stepByStep();
    return(false);

  case 311:
    renameDataflow();
    return(false);

  case 312:
    displayDataflow();
    return(false);

  case 313:
    deleteObject();
    return(false);

  default:
    QMessageBox::warning(application()->desktop(), "Supervision Error", "Unknown Command From Salome");
    return(false);
  }
}

/*bool SUPERVGUI::SetSettings() {
  Trace("SUPERVGUI::SetSettings");
  Supervision.init();
  return(true);
  }*/

/*void SUPERVGUI::DefinePopup(QString& theContext, QString& theParent, QString& theObject) {
  theObject  = "";
  theContext = "";
  }*/

bool SUPERVGUI::ActiveStudyChanged() {
  Trace("SUPERVGUI::activeStudyChanged");
  study = application()->activeStudy();
  return true;
}

/*void SUPERVGUI::SupportedViewType (int* buffer, int bufferSize)
{
  if (!buffer || !bufferSize) return;
  buffer[0] = (int)VIEW_GRAPHSUPERV;
}*/

void SUPERVGUI::fillInterfaceNameMap() {
  //fill map: interface_name <-> component_name for all
  //interfaces of all loaded components, it is necessary
  //for correct adding factory nodes from AddComponent,
  //SubComponent, MulComponent, etc. components.
  myInterfaceNameMap.clear();

  //get naming service
  SALOME_NamingService* namingService = (( SalomeApp_Application* )application())->namingService();
  //get module catalog
  CORBA::Object_ptr obj  = namingService->Resolve("/Kernel/ModulCatalog");
  SALOME_ModuleCatalog::ModuleCatalog_var* aModuleCatalog = new SALOME_ModuleCatalog::ModuleCatalog_var;
  *aModuleCatalog = SALOME_ModuleCatalog::ModuleCatalog::_narrow(obj);
  if (!CORBA::is_nil(*aModuleCatalog)) {
    //get component list
    SALOME_ModuleCatalog::ListOfComponents_var aCompList = (*aModuleCatalog)->GetComponentList();
    for (int ind1 = 0; ind1 < aCompList->length(); ind1++) {
      //get component
      SALOME_ModuleCatalog::Acomponent_ptr aComponent = (*aModuleCatalog)->GetComponent(aCompList[ind1]);
      if ( aComponent != NULL ) {
	// mkr : PAL13135
	myInterfaceNameMap.insert(QString(aComponent->componentusername()), QString(aComponent->componentname()));
      }
    }
  }
}

QMap<QString, QString> SUPERVGUI::getInterfaceNameMap() const {
  return myInterfaceNameMap;
}

bool SUPERVGUI::updateDataFlowSOName( SUPERV::Graph_ptr theDataflow ) {
  if ( !SUPERV_isNull(theDataflow) ) {
    _PTR(Study) aStudy = (( SalomeApp_Study* )study)->studyDS();
    _PTR(SObject) aSO ( aStudy->FindObjectIOR( theDataflow->getIOR() ) );

    _PTR(GenericAttribute) anAttr;
    if ( aSO ) // i.e. if theDataflow is in study
      if ( aSO->FindAttribute(anAttr, "AttributeName") ) {
        _PTR(AttributeName) aName ( anAttr );
	aName->SetValue( theDataflow->Name() );
	updateObjBrowser();
	return true;
      }    
  }
  return false;
}

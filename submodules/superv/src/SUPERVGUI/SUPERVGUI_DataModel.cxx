// Copyright (C) 2005  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either 
// version 2.1 of the License.
// 
// This library is distributed in the hope that it will be useful 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public  
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#include "SUPERVGUI_DataModel.h"
#include <SalomeApp_Study.h>

//=======================================================================
// name    : SUPERVGUI_DataModel::SUPERVGUI_DataModel
// Purpose : Constructor
//=======================================================================
SUPERVGUI_DataModel::SUPERVGUI_DataModel( CAM_Module* theModule )
: SalomeApp_DataModel( theModule )
{
}

//=======================================================================
// name    : SUPERVGUI_DataModel::~SUPERVGUI_DataModel
// Purpose : Destructor
//=======================================================================
SUPERVGUI_DataModel::~SUPERVGUI_DataModel()
{
}

//================================================================
// Function : open
// Purpose  : 
//================================================================
bool SUPERVGUI_DataModel::open( const QString& str, CAM_Study* study, QStringList list )
{
  return SalomeApp_DataModel::open( str, study, list );
}

//================================================================
// Function : save
// Purpose  : 
//================================================================
bool SUPERVGUI_DataModel::save(QStringList& list)
{
  return SalomeApp_DataModel::save(list);
}

//================================================================
// Function : saveAs
// Purpose  : 
//================================================================
bool SUPERVGUI_DataModel::saveAs( const QString& str, CAM_Study* study, QStringList& list )
{
  return SalomeApp_DataModel::saveAs( str, study, list );
}

//================================================================
// Function : close
// Purpose  : 
//================================================================
bool SUPERVGUI_DataModel::close()
{
  return SalomeApp_DataModel::close();
}

//================================================================
// Function : close
// Purpose  : 
//================================================================
bool SUPERVGUI_DataModel::isModified() const
{
  //SalomeApp_Study* study = getStudy();
  //if(study) return study->studyDS()->IsModified();
  return false; 
}

//================================================================
// Function : isSaved
// Purpose  : 
//================================================================
bool SUPERVGUI_DataModel::isSaved() const
{
  return true; //SRN: BugID IPAL9377, changed from false till the more sophisticated code is implemented 
}

//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_BrowseNodeDlg.cxx
//  Author : Vitaly SMETANNIKOV
//  Module : SUPERV

#include "SALOMEDSClient.hxx"
#include "SALOMEDS_SObject.hxx"
#include "SALOMEDS_Study.hxx"

#include "SalomeApp_Application.h"
#include "LightApp_SelectionMgr.h"
#include "SalomeApp_Study.h"
#include "SUIT_Session.h"

#include "SALOME_ListIO.hxx"

#include "SUPERVGUI_BrowseNodeDlg.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI.h"

#include <qlayout.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qhbox.h>
#include <qgroupbox.h>
#include <qvalidator.h>

#include <boost/shared_ptr.hpp>
using namespace boost;

/**
 * Constructor
 */
SUPERVGUI_PortField::SUPERVGUI_PortField( QWidget* theParent, SUPERV_Port thePort ) {
  myPort = thePort;
  myIsEditable = myPort->IsInput() && ( !myPort->IsLinked() );

  QString aLabelTxt(myPort->Name());

  /*SUPERV_CNode aNode = myPort->Node();
  SALOME_ModuleCatalog::Service* aService = aNode->Service();
  SALOME_ModuleCatalog::ListOfServicesParameter aList;
  aList = (myPort->IsInput())? aService->ServiceinParameter : aService->ServiceoutParameter;
  for (int i = 0; i < aList.length(); i++) {
    SALOME_ModuleCatalog::ServicesParameter* aParam = &(aList[i]);
    if (aLabelTxt == aParam->Parametername) {
      aLabelTxt += QString(" (") + QString(aParam->Parametertype) + QString(")");
      break;
    }
  }
  aLabelTxt += QString(":");*/

  // mkr : the following way is easyer to get ports' type than commented above
  QString aPortType = QString(myPort->Type());
  aLabelTxt += QString(" (") + aPortType + QString("):");

  myLabel = new QLabel(aLabelTxt, theParent );

  myValue = new QLineEdit( theParent );
  myLabel->setBuddy( myValue );
  if (!myIsEditable) {
    myValue->setReadOnly( !myIsEditable );
    QPalette aPalette = myValue->palette();
    aPalette.setInactive(aPalette.disabled());
    aPalette.setActive(aPalette.disabled());
    myValue->setPalette(aPalette);
  }
  // mkr : PAL6276 -->
  else {
    if ( !aPortType.compare(QString("boolean")) )
      myValue->setValidator( new QIntValidator( 0, 1, theParent ) );

    if ( !aPortType.compare(QString("short")) || !aPortType.compare(QString("int")) || !aPortType.compare(QString("long")) )
      myValue->setValidator( new QIntValidator( theParent ) );
    
    if ( !aPortType.compare(QString("float")) || !aPortType.compare(QString("double")) )
      myValue->setValidator( new QDoubleValidator( theParent ) );

    if ( !aPortType.compare(QString("char")) ) {
      QRegExp aRX( "." );
      myValue->setValidator( new QRegExpValidator( aRX, theParent ) );

    }
  }
  // mkr : PAL6276 <--
  myValue->installEventFilter( this );
}

/**
 * Sets value from text edit control to engine of port
 */
bool SUPERVGUI_PortField::setNewValue() {
  if ( !myIsEditable ) return false;

  QString aTxt = myValue->text();
  //mkr : fix for bug IPAL9441
  //if ( aTxt.isNull() || aTxt.isEmpty() ) return false;
  
  if ( aTxt.find( "Unknown" ) < 0 ) {
    SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
    if ( !aSupMod ) {
      MESSAGE("NULL Supervision module!");
      return false;
    }
    return myPort->Input( aSupMod->getEngine()->StringValue( aTxt ) );
  }
  return false;
}
 
/** 
 * Event filter
 */ 
bool SUPERVGUI_PortField::eventFilter( QObject* o, QEvent* e )
{
  if ( o == myValue ) {
    if ( e->type() == QEvent::FocusIn ) {
      emit activated();
    }
  }
  return QObject::eventFilter( o, e );
}


/**
 * Constructor (SUPERVGUI_CanvasNode)
 */
SUPERVGUI_BrowseNodeDlg::SUPERVGUI_BrowseNodeDlg( SUPERVGUI_CanvasNode* theNode )
  : QDialog( SUIT_Session::session()->activeApplication()->desktop(), 0, false, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu | WDestructiveClose )
{
  myNodeCanvas = theNode;

  init();
}

void SUPERVGUI_BrowseNodeDlg::init()
{
  myActiveField = 0;
  setSizeGripEnabled( true );
  myPortsList.setAutoDelete( true );

  SUPERV_CNode aEngine = myNodeCanvas->getEngine();
  
  setName( "SUPERVGUI_BrowseNodeDlg" );
  setCaption( tr( "TIT_BROWSENODE" ) + aEngine->Name() );

  QGridLayout* aBaseLayout = new QGridLayout( this );
  aBaseLayout->setMargin( 11 ); aBaseLayout->setSpacing( 6 );

  QGroupBox* aInBox = new QGroupBox( 2, Qt::Horizontal, "Input", this );
  aInBox->setMargin( 11 );
  aBaseLayout->addWidget( aInBox, 0, 0 );

  QGroupBox* aOutBox = new QGroupBox( 2, Qt::Horizontal, "Output", this);
  aOutBox->setMargin( 11 );
  aBaseLayout->addWidget( aOutBox, 0, 1 );

  myIsEditable = false;
  int                aPIcount = 0;
  int                aPOcount = 0;
  SUPERV_Ports       ports = aEngine->Ports();
  int                n     = ports->length();

  for ( int i = 0; i < n; i++ ) {
    if ( ports[i]->IsInput() ) {
      if ( !ports[i]->IsGate() ) { // not a gate
	SUPERVGUI_PortField* aField = new SUPERVGUI_PortField( aInBox, ports[i] );
	if ( aField->isEditable() ) 
	  myIsEditable = true;
	myPortsList.append( aField );
	if ( !myActiveField )
	  myActiveField = aField;
	connect( aField, SIGNAL( activated() ), this, SLOT( onFieldActivated() ) );
      }
      aPIcount++;
    } 
    else {
      if ( !ports[i]->IsGate() ) { // not a gate
	myPortsList.append( new SUPERVGUI_PortField( aOutBox, ports[i] ) );
      }
      aPOcount++;
    }
  }
  
  QGroupBox* aBtnBox = new QGroupBox( this );
  aBtnBox->setColumnLayout( 0, Qt::Vertical );
  aBtnBox->layout()->setSpacing( 0 ); 
  aBtnBox->layout()->setMargin( 0 );
  
  QHBoxLayout* aBtnLayout = new QHBoxLayout( aBtnBox->layout() );
  aBtnLayout->setAlignment( Qt::AlignTop );
  aBtnLayout->setSpacing( 6 ); 
  aBtnLayout->setMargin( 11 );

  if ( myIsEditable ) {
    QLabel* aInfoLab = new QLabel( tr( "ENTER_OR_SELECT_LBL" ), this );
    aBaseLayout->addMultiCellWidget( aInfoLab, 1, 1, 0, 1 );

    QPushButton* aOkBtn = new QPushButton( tr( "BUT_OK" ), aBtnBox );
    connect( aOkBtn, SIGNAL( clicked() ), this, SLOT( accept() ) );
    aBtnLayout->addWidget( aOkBtn );
    aBaseLayout->addMultiCellWidget( aBtnBox, 2, 2, 0, 1 );
  } else
    aBaseLayout->addMultiCellWidget( aBtnBox, 1, 1, 0, 1 );

  aBtnLayout->addStretch();
  QPushButton* aCancelBtn = new QPushButton( myIsEditable ? tr( "BUT_CANCEL" ) : tr( "BUT_CLOSE" ), aBtnBox );
  aBtnLayout->addWidget( aCancelBtn );
  connect( aCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  
  if ( !myIsEditable )
    aBtnLayout->addStretch();

  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( aSupMod ) connect( (( SalomeApp_Application* )(aSupMod->getActiveStudy()->application()))->selectionMgr(), 
			  SIGNAL( currentSelectionChanged() ), this, SLOT( onSelectionChanged() ) );
  else MESSAGE("NULL Supervision module!");
  
  myNodeCanvas->getMain()->lockedGraph(true);
}

/**
 * Destructor
 */
SUPERVGUI_BrowseNodeDlg::~SUPERVGUI_BrowseNodeDlg() {
}

/**
 * Set current values of editable ports
 */
void SUPERVGUI_BrowseNodeDlg::setValues() {
  SUPERVGUI_PortField* aField;
  for ( aField = myPortsList.first(); aField; aField = myPortsList.next() ) {
    if ( aField->getData().isEmpty() || aField->getData().isNull() ) // mkr : PAL11406
      aField->updateGUI();
  }
}

/** 
 * Set inputed values of editable ports and then closes and destroys dialog
 */
void SUPERVGUI_BrowseNodeDlg::accept() {
  myNodeCanvas->getMain()->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
  if ( myIsEditable ) {
    SUPERVGUI_PortField* aField;
    for ( aField = myPortsList.first(); aField; aField = myPortsList.next() ) {
      aField->setNewValue();
    }
    myNodeCanvas->sync();
    myNodeCanvas->getMain()->getCanvas()->update();
  }
  myNodeCanvas->getMain()->lockedGraph(false);
  QDialog::accept();
  close();
}


/** 
 * Closes and destroys dialog
 */
void SUPERVGUI_BrowseNodeDlg::reject() {
  myNodeCanvas->getMain()->lockedGraph(false);
  QDialog::reject();
  close();
}

/**
 * Update current values on show event
 */
void SUPERVGUI_BrowseNodeDlg::showEvent( QShowEvent* theEvent ) {
  setValues();
  QDialog::showEvent( theEvent );
}

/**
 * Slot, called when field is focused
 */
void SUPERVGUI_BrowseNodeDlg::onFieldActivated()
{
  SUPERVGUI_PortField* aField = (SUPERVGUI_PortField*)sender();
  myActiveField = (aField->isEditable())? aField : 0;
}

namespace {

  QString getIORfromIO (const Handle(SALOME_InteractiveObject)& theIO,
                        SUPERVGUI * theModule)
  {
    QString ior ("");
    if (!theIO->hasEntry()) return ior;

    SalomeApp_Study* anAppStudy = dynamic_cast<SalomeApp_Study*>(theModule->getActiveStudy());
    _PTR(SObject) aSObj (anAppStudy->studyDS()->FindObjectID(theIO->getEntry()));

    _PTR(GenericAttribute) anAttr;
    if (aSObj->FindAttribute(anAttr, "AttributeIOR")) {
      _PTR(AttributeIOR) anIOR (anAttr);
      ior = anIOR->Value().c_str();
      return ior;
    }

    // old code, it is useless, because <aSSObj->GetObject()> here will be NULL
    // (because it is retrieved from <aSSObj> by stored IOR)
    /*
      SALOMEDS_Study* aSStudy = dynamic_cast<SALOMEDS_Study*>( aSObj->GetStudy().get() );
      SALOMEDS_SObject* aSSObj = dynamic_cast<SALOMEDS_SObject*>( aSObj.get() );
      if ( aSStudy && aSSObj )
      ior = aSStudy->ConvertObjectToIOR( aSSObj->GetObject() ).c_str();
    //*/

    // new code

    // default value: null IOR (IOR:01000000010000000000...)
    SalomeApp_Application* anApp = theModule->getApp();
    CORBA::Object_var aNullObj;
    ior = anApp->orb()->object_to_string(aNullObj);

    // try to load a component data from an opened (presumably) study
    _PTR(SComponent) aSComp = aSObj->GetFatherComponent();
    std::string aCompIOR;
    if (!aSComp->ComponentIOR(aCompIOR)) {
      std::string aCompDataType = aSComp->ComponentDataType();

      // obtain a driver by a component data type
      // like it is done in SALOMEDS_DriverFactory_i::GetDriverByType
      SALOMEDS::Driver_var anEngine = SALOMEDS::Driver::_nil();
      SALOME_LifeCycleCORBA * LCC = anApp->lcc();
      try {
        CORBA::Object_var
          anEngineObj = LCC->FindOrLoad_Component("FactoryServer", aCompDataType.c_str());
        if (CORBA::is_nil(anEngineObj))
          anEngineObj = LCC->FindOrLoad_Component("FactoryServerPy", aCompDataType.c_str());

        if (!CORBA::is_nil(anEngineObj))
          anEngine = SALOMEDS::Driver::_narrow(anEngineObj);
      }
      catch (...) {
      }

      if (!CORBA::is_nil(anEngine)) {
        // try to load
        _PTR(StudyBuilder) aStudyBuilder = aSObj->GetStudy()->NewBuilder();
        bool isOk = true;
        try {
          CORBA::String_var aDriverIOR = anApp->orb()->object_to_string(anEngine);
          aStudyBuilder->LoadWith(aSComp, aDriverIOR.in());
        }
        catch (...) {
          isOk = false;
        }

        if (isOk) {
          // now try to obtain the IOR once again (after successfull component data loading)
          if (aSObj->FindAttribute( anAttr, "AttributeIOR" )) {
            _PTR(AttributeIOR) anIOR ( anAttr );
            ior = anIOR->Value().c_str();
          }
        }
      } // if (!CORBA::is_nil(anEngine))
    } // if (!aSComp->ComponentIOR(aCompIOR))

    return ior;
  }

} // no name namespace

/**
 * Slot, called when selection is changed
 */
void SUPERVGUI_BrowseNodeDlg::onSelectionChanged()
{
  if ( myActiveField ) {

    SALOME_ListIO aList;
    aList.Clear();

    SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
    if ( !aSupMod ) {
      MESSAGE("NULL Supervision module!");
      return;
    }

    SalomeApp_Application* anApp = aSupMod->getApp();
    anApp->selectionMgr()->selectedObjects( aList );

    if ( aList.Extent() == 1 ) {
      Handle( SALOME_InteractiveObject ) anIO = aList.First();
      if ( anIO->hasEntry() ) {
        QString ior = ::getIORfromIO(anIO, aSupMod);
	myActiveField->setData( ior );
      }
    }
  }
}


/**
 * Constructor (SUPERVGUI_CanvasPort)
 */
SUPERVGUI_GetValueDlg::SUPERVGUI_GetValueDlg( SUPERVGUI_CanvasPort* thePort )
  : QDialog( SUIT_Session::session()->activeApplication()->desktop(), 0, false, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu | WDestructiveClose )
{
  myPortCanvas = thePort;

  init();
}

void SUPERVGUI_GetValueDlg::init()
{
  myOKBtn = 0;
  setSizeGripEnabled( true );

  setName( "SUPERVGUI_GetValueDlg" );
  setCaption( tr( "TIT_SETVALUE_PORT" ) );
  
  QGridLayout* aBaseLayout = new QGridLayout( this );
  aBaseLayout->setMargin( 11 ); aBaseLayout->setSpacing( 6 );
  
  QGroupBox* aBox = new QGroupBox( this );
  aBox->setColumnLayout( 0, Qt::Vertical );
  aBox->layout()->setSpacing( 0 ); aBox->layout()->setMargin( 0 );
  QGridLayout* aBoxLayout = new QGridLayout( aBox->layout() );
  aBoxLayout->setAlignment( Qt::AlignTop );
  aBoxLayout->setSpacing( 6 ); aBoxLayout->setMargin( 11 );
  aBaseLayout->addWidget( aBox, 0, 0 );
  aBox->setMinimumWidth( 200 );
  
  QLabel* aInfoLab = new QLabel( tr( "ENTER_OR_SELECT_LBL" ), aBox );
  QFont fnt = aInfoLab->font(); fnt.setBold( true ); aInfoLab->setFont( fnt );
  aBoxLayout->addMultiCellWidget( aInfoLab, 0, 0, 0, 1 );
  SUPERV_Port aEngine = myPortCanvas->getEngine();

  myField = new SUPERVGUI_PortField( aBox, aEngine );
  bool myIsEditable = myField->isEditable();
  aBoxLayout->addWidget( myField->myLabel, 1, 0 ); 
  aBoxLayout->addWidget( myField->myValue, 1, 1 );
  
  QGroupBox* aBtnBox = new QGroupBox( this );
  aBtnBox->setColumnLayout( 0, Qt::Vertical );
  aBtnBox->layout()->setSpacing( 0 ); aBtnBox->layout()->setMargin( 0 );
  QHBoxLayout* aBtnLayout = new QHBoxLayout( aBtnBox->layout() );
  aBtnLayout->setAlignment( Qt::AlignTop );
  aBtnLayout->setSpacing( 6 ); aBtnLayout->setMargin( 11 );

  aBaseLayout->addWidget( aBtnBox, 1, 0 );

  if ( myIsEditable ) {
    myOKBtn = new QPushButton( tr( "BUT_OK" ), aBtnBox );
    connect( myOKBtn, SIGNAL( clicked() ), this, SLOT( accept() ) );
    aBtnLayout->addWidget( myOKBtn );
  }

  aBtnLayout->addStretch();
  myCancelBtn = new QPushButton( myIsEditable ? tr( "BUT_CANCEL" ) : tr( "BUT_CLOSE" ), aBtnBox );
  aBtnLayout->addWidget( myCancelBtn );
  connect( myCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  
  if ( !myIsEditable )
    aBtnLayout->addStretch();

  myField->updateGUI();
  
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( aSupMod ) connect( (( SalomeApp_Application* )(aSupMod->getActiveStudy()->application()))->selectionMgr(), 
			  SIGNAL( currentSelectionChanged() ), this, SLOT( onSelectionChanged() ) );
  else MESSAGE("NULL Supervision module!");
 
  myPortCanvas->getMain()->lockedGraph(true);
}

/**
 * Destructor
 */
SUPERVGUI_GetValueDlg::~SUPERVGUI_GetValueDlg() {
}

/** 
 * Set entered value into port and then closes and destroys dialog
 */
void SUPERVGUI_GetValueDlg::accept() {
  myPortCanvas->getMain()->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
  if ( myField->setNewValue() ) {
    
    myPortCanvas->sync();

    // asv : 19.11.04 : fix for 6170, last comment of it about BrowsePort - update node status
    // asv : 13.12.04 : commented out sync() of Node.  See 2.21.
    //if ( myPortCanvas->parent() && myPortCanvas->parent()->inherits( "SUPERVGUI_CanvasNode" ) ) {
    //  SUPERVGUI_CanvasNode* aNode = (SUPERVGUI_CanvasNode*)myPortCanvas->parent();
    //  aNode->sync();
    //}

    myPortCanvas->getMain()->getCanvas()->update();
  }
  else {
    if ( QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), 
			       tr( "ERROR" ), tr( "MSG_CANT_SETVAL" ),
			       QMessageBox::Retry, QMessageBox::Abort) == QMessageBox::Retry )
      return;
  }

  myPortCanvas->getMain()->lockedGraph(false);
  
  QDialog::accept();
  close();
}

/** 
 * Closes and destroys dialog
 */
void SUPERVGUI_GetValueDlg::reject() {
  myPortCanvas->getMain()->lockedGraph(false);
  QDialog::reject();
  close();
}

/**
 * Slot, called when selection is changed
 */
void SUPERVGUI_GetValueDlg::onSelectionChanged()
{
  SALOME_ListIO aList;
  aList.Clear();

  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }

  SalomeApp_Application* anApp = aSupMod->getApp();
  anApp->selectionMgr()->selectedObjects( aList );

  if ( aList.Extent() == 1 ) {
    Handle( SALOME_InteractiveObject ) anIO = aList.First();
    if ( anIO->hasEntry() ) {
      QString ior = ::getIORfromIO(anIO, aSupMod);
      myField->setData( ior );
    }
  }
}



// ----------------------------
// Stream Ports
// ----------------------------

SUPERVGUI_StreamInDlg::SUPERVGUI_StreamInDlg(SUPERVGUI_CanvasStreamPortIn* thePort)
  :QDialog( SUIT_Session::session()->activeApplication()->desktop(), "", true, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu )
{
  myPortCanvas = thePort;
  init();
}

void SUPERVGUI_StreamInDlg::init()
{
  setSizeGripEnabled( true );
  setCaption( tr( "MSG_STREAM_DLG_TIT" ) );

  QVBoxLayout* TopLayout = new QVBoxLayout( this, 11, 6 );

  QFrame* aCtrlPane = new QFrame(this);
  QGridLayout* aCtrlLayout = new QGridLayout( aCtrlPane, 4, 10 );

  // Schema
  QLabel* aSchemaLbl = new QLabel(tr("MSG_STREAM_SCHEMA"),aCtrlPane);
  aCtrlLayout->addWidget(aSchemaLbl, 0, 0);

  mySchemaCombo = new QComboBox(aCtrlPane, "SchemaBox" );
  mySchemaCombo->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  mySchemaCombo->insertItem("SCHENULL"); 
  mySchemaCombo->insertItem("TI");  
  mySchemaCombo->insertItem("TF");
  mySchemaCombo->insertItem("DELTA");
  aCtrlLayout->addWidget(mySchemaCombo, 0, 1);

  // Interpolation
  QLabel* aInterLbl = new QLabel(tr("MSG_STREAM_INTER"),aCtrlPane);
  aCtrlLayout->addWidget(aInterLbl, 1, 0);

  myInterCombo = new QComboBox(aCtrlPane, "InterBox" );
  myInterCombo->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myInterCombo->insertItem("INTERNULL"); 
  myInterCombo->insertItem("L0"); 
  myInterCombo->insertItem("L1");
  aCtrlLayout->addWidget(myInterCombo, 1, 1);

  // Extrapolation
  QLabel* aExtraLbl = new QLabel(tr("MSG_STREAM_EXTRA"),aCtrlPane);
  aCtrlLayout->addWidget(aExtraLbl, 2, 0);

  myExterCombo = new QComboBox(aCtrlPane, "ExtraBox" );
  myExterCombo->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myExterCombo->insertItem("EXTRANULL");  
  myExterCombo->insertItem("E0");
  myExterCombo->insertItem("E1");
  aCtrlLayout->addWidget(myExterCombo, 2, 1);
  
  TopLayout->addWidget( aCtrlPane );

  // Buttons
  QGroupBox* GroupButtons = new QGroupBox( this, "GroupButtons" );
  GroupButtons->setColumnLayout(0, Qt::Vertical );
  GroupButtons->layout()->setSpacing( 0 );
  GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons->layout() );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 5 );
  GroupButtonsLayout->setMargin( 8 );
  
  QPushButton* okB     = new QPushButton( tr( "BUT_OK" ),     GroupButtons );
  QPushButton* cancelB = new QPushButton( tr( "BUT_CANCEL" ), GroupButtons );

  GroupButtonsLayout->addWidget( okB, 0, 0 );
  GroupButtonsLayout->addItem  ( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );
  GroupButtonsLayout->addWidget( cancelB, 0, 2 );

  TopLayout->addWidget( GroupButtons );

  connect( okB,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( cancelB, SIGNAL( clicked() ), this, SLOT( reject() ) );
  setData();
}

void SUPERVGUI_StreamInDlg::setData() {
  SUPERV::KindOfSchema aSchema;
  SUPERV::KindOfInterpolation aInterpolat;
  SUPERV::KindOfExtrapolation aExtrapolat;

  myPortCanvas->getStreamEngine()->Params(aSchema, aInterpolat, aExtrapolat);

  mySchemaCombo->setCurrentItem((int)aSchema);
  myInterCombo->setCurrentItem((int)aInterpolat);
  myExterCombo->setCurrentItem((int)aExtrapolat);
}

void SUPERVGUI_StreamInDlg::accept() {
  myPortCanvas->getMain()->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
  myPortCanvas->getStreamEngine()->SetParams((SUPERV::KindOfSchema) mySchemaCombo->currentItem(), 
					     (SUPERV::KindOfInterpolation) myInterCombo->currentItem(),
					     (SUPERV::KindOfExtrapolation) myExterCombo->currentItem());
  QDialog::accept();
}


//-------------------------------------------------------------------------
SUPERVGUI_StreamOutDlg::SUPERVGUI_StreamOutDlg(SUPERVGUI_CanvasStreamPortOut* thePort)
  :QDialog( SUIT_Session::session()->activeApplication()->desktop(), "", true, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu )
{
  myPortCanvas = thePort;
  init();
}

void SUPERVGUI_StreamOutDlg::init()
{
  setSizeGripEnabled( true );
  setCaption( tr( "MSG_STREAM_DLG_TIT" ) );

  QVBoxLayout* TopLayout = new QVBoxLayout( this, 11, 6 );

  QFrame* aCtrlPane = new QFrame(this);
  QGridLayout* aCtrlLayout = new QGridLayout( aCtrlPane, 4, 10 );

  QLabel* aLbl = new QLabel(tr("MSG_STREAM_LEVEL"),aCtrlPane);
  aCtrlLayout->addWidget(aLbl, 0, 0);

  myValEdit = new QLineEdit( aCtrlPane, "ValEdit" );
  myValEdit->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myValEdit->setValidator( new QIntValidator(this) );
  SUPERV_StreamPort aEngine = myPortCanvas->getStreamEngine();
  myValEdit->setText(QString("%1").arg(aEngine->NumberOfValues()));
  aCtrlLayout->addWidget(myValEdit, 0, 1);
  TopLayout->addWidget( aCtrlPane );
  
  // Buttons
  QGroupBox* GroupButtons = new QGroupBox( this, "GroupButtons" );
  GroupButtons->setColumnLayout(0, Qt::Vertical );
  GroupButtons->layout()->setSpacing( 0 );
  GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons->layout() );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 5 );
  GroupButtonsLayout->setMargin( 8 );
  
  QPushButton* okB     = new QPushButton( tr( "BUT_OK" ),     GroupButtons );
  QPushButton* cancelB = new QPushButton( tr( "BUT_CANCEL" ), GroupButtons );

  GroupButtonsLayout->addWidget( okB, 0, 0 );
  GroupButtonsLayout->addItem  ( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );
  GroupButtonsLayout->addWidget( cancelB, 0, 2 );

  TopLayout->addWidget( GroupButtons );

  connect( okB,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( cancelB, SIGNAL( clicked() ), this, SLOT( reject() ) );
}

void SUPERVGUI_StreamOutDlg::accept() {
  int aRes = 0;
  QString aStr = myValEdit->text();
  if (!aStr.isEmpty())
    aRes = aStr.toLong();
  myPortCanvas->getStreamEngine()->SetNumberOfValues(aRes);
  QDialog::accept();
}


//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasPort.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasLink.h"
#include "SUPERVGUI_CanvasNodePrs.h"
#include "SUPERVGUI_Clipboard.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI.h"
#include "SUPERVGUI_BrowseNodeDlg.h"

#include "SalomeApp_Study.h"

SUPERVGUI_CanvasPort::SUPERVGUI_CanvasPort (QObject* theParent, SUPERVGUI_Main* theMain,
                                            SUPERV::Port_ptr thePort):
    QObject(theParent),
    myMain(theMain),
    myPrs(0),
    isIgnore(false)
{
  Trace("SUPERVGUI_CanvasPort::SUPERVGUI_CanvasPort");
  myPort = SUPERV::Port::_duplicate(thePort);

  // setName(myPort->Name());
  setName(myMain->getCanvas()->getPortName(thePort));

  // mkr : PAL8237
  connect(this, SIGNAL(objectDeleted()), myMain, SLOT(onObjectCreatedDeleted()));
}

SUPERVGUI_CanvasPort::~SUPERVGUI_CanvasPort()
{
  Trace("SUPERVGUI_CanvasPort::~SUPERVGUI_CanvasPort");
  if (myPrs) delete myPrs;

  isIgnore = true;
  QValueList<SUPERVGUI_CanvasLink*>::Iterator it;
  for (it = myLinks.begin(); it != myLinks.end(); ++it) {
    delete *it;
  }
}

SUPERVGUI_CanvasPortPrs* SUPERVGUI_CanvasPort::getPrs() const
{
  if (myPrs == 0) ((SUPERVGUI_CanvasPort*)this)->myPrs = createPrs();
  return myPrs;
}

SUPERVGUI_CanvasPortPrs* SUPERVGUI_CanvasPort::createPrs() const
{
  return new SUPERVGUI_CanvasPortPrs(myMain->getCanvas(), (SUPERVGUI_CanvasPort*)this);
}

QPopupMenu* SUPERVGUI_CanvasPort::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = new QPopupMenu(theParent);
  if ( myMain->isEditable() && !myMain->getDataflow()->IsExecuting() ) {
    int anItem = popup->insertItem(tr("MSG_SKETCH_LINK"), this, SLOT(sketchLink()));
    if (myMain->getDataflow()->IsExecuting())
      popup->setItemEnabled(anItem, false);
    else
      popup->setItemEnabled(anItem, !myPort->IsInput() || !myPort->IsLinked()
    			    || myPort->Kind() == SUPERV::EndSwitchParameter);
    popup->insertSeparator();
  }
  if (myMain->isEditable() && !myMain->getDataflow()->IsExecuting()
      &&
      ((myPort->IsEndSwitch() && myPort->IsInput()) 
       ||
       (myPort->IsInLine() && myPort->Node()->Kind() != SUPERV::EndLoopNode
	&& 
	!(myPort->Node()->Kind() == SUPERV::LoopNode && !myPort->IsInput())))) {
    popup->insertItem(tr("ITM_DEL_PORT"), this, SLOT(remove()));    
  }
  // Copy Port functionality
  if (myMain->isEditable() && !myPort->Node()->IsFactory()
                           && !myPort->Node()->IsComputing()
                           && !myPort->Node()->IsMacro())
    popup->insertItem(tr("ITM_COPY_PORT"), this, SLOT(copy()));

  /*int anItem = */popup->insertItem(tr("MSG_BROWSE"), this, SLOT(browse()));
//   if (getEngine()->IsLinked())
//     popup->setItemEnabled(anItem, getEngine()->State() == SUPERV_Ready);
//   else 
//     popup->setItemEnabled(anItem, getEngine()->HasInput());

  return popup;
}

QPoint SUPERVGUI_CanvasPort::getConnectionPoint() const
{
  QPoint p = getPrs()->getConnectionPoint();

  if (!getPrs()->isVisible()) {
    if (parent() && parent()->inherits("SUPERVGUI_CanvasNode")) {
      SUPERVGUI_CanvasNodePrs* aNode = ((SUPERVGUI_CanvasNode*) parent())->getPrs();
      if (myPort->IsInput())
	p = aNode->getInConnectionPoint();
      else
	p = aNode->getOutConnectionPoint();
    }
  }
  return p;
}

void SUPERVGUI_CanvasPort::update() 
{
  // ignore update if node itself is destroyed (critical for Start/End control nodes)
  if (!((SUPERVGUI_CanvasNode*) parent())->isDestroyed())
    getPrs()->update(true);
}

void SUPERVGUI_CanvasPort::sync() 
{
  getPrs()->update();
}

void SUPERVGUI_CanvasPort::sketchLink() {
  myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
  
  myMain->getCanvasView()->startSketch(this);
}

void SUPERVGUI_CanvasPort::remove() {
  myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 

  //set myCopyPort from Main object to empty if engine of this port is deleted
  //check if myCopyPort and this port engine is equal
  SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
  if ( aCB->isCopyPort() )
    if ( QString(myPort->Node()->Name()) == QString(aCB->getCopyPort()->Node()->Name()) &&
	 QString(myPort->Name()) == QString(aCB->getCopyPort()->Name())	&&
	 myPort->IsInput() == aCB->getCopyPort()->IsInput() )
	aCB->setCopyPort( 0 );

  Trace("SUPERVGUI_CanvasPort::remove");
  myPort->destroy();
  
  emit objectDeleted(); // mkr : PAL8237

  delete this;
}

void SUPERVGUI_CanvasPort::moveBy(int dx, int dy) 
{
  getPrs()->moveBy(dx, dy);

  QValueList<SUPERVGUI_CanvasLink*>::Iterator it;
  for (it = myLinks.begin(); it != myLinks.end(); ++it) {
    (*it)->moveByPort(this, dx, dy);
  }
}

void SUPERVGUI_CanvasPort::addLink(SUPERVGUI_CanvasLink* theLink)
{
  myLinks.append(theLink);
  update();
}

void SUPERVGUI_CanvasPort::removeLink(SUPERVGUI_CanvasLink* theLink)
{
  if (!isIgnore) {
    myLinks.remove(theLink);
    update();
  }
}

void SUPERVGUI_CanvasPort::updateLinks() 
{
  QValueList<SUPERVGUI_CanvasLink*>::Iterator it;
  for (it = myLinks.begin(); it != myLinks.end(); ++it) {
    (*it)->moveByPort(this);
  }
}

void SUPERVGUI_CanvasPort::browse() 
{
  QString aMes(getEngine()->IsInput()? tr("MSG_IPORT_VAL") : tr("MSG_OPORT_VAL"));
  aMes += getEngine()->ToString();
  QMessageBox::information(SUIT_Session::session()->activeApplication()->desktop(), tr("MSG_INFO"), aMes);
}

void SUPERVGUI_CanvasPort::copy()
{
  SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
  aCB->setCopyPort(SUPERV::Port::_duplicate(getEngine()));
}

//***********************************************************
// Input Port
//***********************************************************
SUPERVGUI_CanvasPortIn::SUPERVGUI_CanvasPortIn (QObject* theParent, SUPERVGUI_Main* theMain,
                                                SUPERV::Port_ptr thePort):
  SUPERVGUI_CanvasPort(theParent, theMain, thePort)
{
  Trace("SUPERVGUI_CanvasPortIn::SUPERVGUI_CanvasPortIn");
  myDlg = 0;
}

SUPERVGUI_CanvasPortIn::~SUPERVGUI_CanvasPortIn()
{
  Trace("SUPERVGUI_CanvasPortIn::~SUPERVGUI_CanvasPortIn");
}

QPopupMenu* SUPERVGUI_CanvasPortIn::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasPort::getPopupMenu(theParent);
  bool editable = (getEngine()->IsInput() &&
                   !getEngine()->IsLinked() &&
                   !getMain()->getDataflow()->IsExecuting());

  if (!getEngine()->IsGate() && editable)
    popup->insertItem(tr("MSG_SETVALUE"), this, SLOT(setInput()));

  return popup;
}

void SUPERVGUI_CanvasPortIn::setValue(const char* theValue) 
{
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if (aSupMod && getEngine()->Input(aSupMod->getEngine()->StringValue(theValue)))
    update(); // sync();
  else
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_SETVAL"));
}

void SUPERVGUI_CanvasPortIn::setInput() 
{
  // asv 28.01.05 : set "Editing" flag only on "OK" pressed in BrowseDlg
  //getMain()->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 
  if (!myDlg) {
    myDlg = new SUPERVGUI_GetValueDlg(this);
    myDlg->installEventFilter(this);
  }
  if (!myDlg->isVisible())
    myDlg->show();
  else {
    myDlg->raise();
    myDlg->setActiveWindow();
    myDlg->setFocus();
  }
}

bool SUPERVGUI_CanvasPortIn::eventFilter(QObject* o, QEvent* e)
{
  if (o == myDlg && e->type() == QEvent::Close)
    myDlg = 0;
  return SUPERVGUI_CanvasPort::eventFilter(o, e);
}


//***********************************************************
// Output Port
//***********************************************************
SUPERVGUI_CanvasPortOut::SUPERVGUI_CanvasPortOut (QObject* theParent, SUPERVGUI_Main* theMain,
                                                  SUPERV::Port_ptr thePort):
  SUPERVGUI_CanvasPort(theParent, theMain, thePort)
{
  Trace("SUPERVGUI_CanvasPortOut::SUPERVGUI_CanvasPortOut");
  myInStudy = false;

  // mkr : PAL8150
  connect(this, SIGNAL(putInStudy( QString* )), getMain(), SLOT(onPutInStudy( QString* )));
}

SUPERVGUI_CanvasPortOut::~SUPERVGUI_CanvasPortOut()
{
  Trace("SUPERVGUI_CanvasPortOut::~SUPERVGUI_CanvasPortOut");
}

QPopupMenu* SUPERVGUI_CanvasPortOut::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasPort::getPopupMenu(theParent);

  if (!getEngine()->IsGate()) {
    popup->insertItem(myInStudy?tr("MSG_NOT_INSTUDY"):tr("MSG_PUT_INSTUDY"), 
		      this, SLOT(toStudy()));
  }

  return popup;
}

void SUPERVGUI_CanvasPortOut::sync() 
{
  /* asv : 26.01.05 : Bug PAL7164 : sometimes CanvasPortOut::sync() is called twice (or maybe even more)
           by mistake.  It happens because of incorrect Qt events, or other reason, but it is not a
	   stable feature (bug). Adding an object in the study in sync() is therefore called more than once
	   which is a BUG.  I decided to move call to putDataStudy() method to Event handling function.
	   When a node successfully finishes execution - check the ports and put out-value to study,
	   if the corresponding flag is set.
  bool ok = getEngine()->State() == SUPERV_Ready;
  if (ok && myInStudy) {
    myInStudy = getMain()->putDataStudy(getEngine(), STUDY_PORT_OUT);
  }
  */
  SUPERVGUI_CanvasPort::update();
}

void SUPERVGUI_CanvasPortOut::toStudy() 
{
  Trace("SUPERVGUI_CanvasPortOut::toStudy");

  // asv 08.02.05 : added && !myInStudy - fix for PAL8105
  if ( (( SalomeApp_Study* )(getMain()->getStudy()))->studyDS()->GetProperties()->IsLocked() && !myInStudy ) {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("WRN_WARNING"), 
			 tr("WRN_STUDY_LOCKED"));
    return;
  }

  myInStudy = !myInStudy;
  sync();
  getMain()->getCanvas()->update();

  if ( myInStudy ) // put values to study (supervision, supervision->dataflow, supervision->dataflow->runXXX, etc.
    if ( getMain()->putDataStudy( getEngine(), STUDY_PORT_OUT ) ) {
      // mkr : PAL8110 : re-register dataflow in object browser with 
      //       changing its key to IOR name (from xml-file name, for example)
      SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
      if ( !aSupMod ) {
	MESSAGE("NULL Supervision module!");
	return;
      }
      aSupMod->unregisterGraph(getMain());
      aSupMod->registerGraph(getMain()->getDataflow()->getIOR(), getMain());
    }
  
  // mkr : PAL8150
  QString aNodePortName = QString( getEngine()->Node()->Name() ) + QString( "_" ) + QString( getEngine()->Name() );
  emit putInStudy( &aNodePortName );

}



//***********************************************************
// Stream Input Port
//***********************************************************
SUPERVGUI_CanvasStreamPortIn::SUPERVGUI_CanvasStreamPortIn(QObject* theParent, SUPERVGUI_Main* theMain, 
							   SUPERV::StreamPort_ptr thePort):
  SUPERVGUI_CanvasPortIn(theParent, theMain, thePort)
{
  myStreamPort = SUPERV::StreamPort::_duplicate(thePort);
}

QPopupMenu* SUPERVGUI_CanvasStreamPortIn::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasPortIn::getPopupMenu(theParent);
  popup->insertItem(tr("MSG_STREAM_PARAM"),this, SLOT(setParams()));
  return popup;
}

void SUPERVGUI_CanvasStreamPortIn::setParams()
{
  SUPERVGUI_StreamInDlg* aDlg = new SUPERVGUI_StreamInDlg(this);
  aDlg->exec();
  delete aDlg;
}


//***********************************************************
// Stream Output Port
//***********************************************************
SUPERVGUI_CanvasStreamPortOut::SUPERVGUI_CanvasStreamPortOut(QObject* theParent, SUPERVGUI_Main* theMain, 
							     SUPERV::StreamPort_ptr thePort):
  SUPERVGUI_CanvasPortOut(theParent, theMain, thePort)
{
  myStreamPort = SUPERV::StreamPort::_duplicate(thePort);
}


QPopupMenu* SUPERVGUI_CanvasStreamPortOut::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasPortOut::getPopupMenu(theParent);
  popup->insertItem(tr("MSG_STREAM_PARAM"),this, SLOT(setParams()));
  return popup;
}

void SUPERVGUI_CanvasStreamPortOut::setParams()
{
  SUPERVGUI_StreamOutDlg* aDlg = new SUPERVGUI_StreamOutDlg(this);
  aDlg->exec();
  delete aDlg;
}

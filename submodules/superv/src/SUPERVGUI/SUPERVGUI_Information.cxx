//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Information.cxx
//  Author : Francis KLOSS
//  Module : SUPERV

#include "SUPERVGUI_Information.h"
#include "SUPERVGUI.h"

#include "CAM_Application.h"
#include "SUIT_Desktop.h"
#include "SUIT_Session.h"

#include <qapplication.h>
#include <qlabel.h>
#include <qgroupbox.h>
#include <qlayout.h>

/*!
  Constructor
*/
SUPERVGUI_Information::SUPERVGUI_Information(SUPERV_CNode node, bool isReadOnly)
     : QDialog( SUIT_Session::session()->activeApplication()->desktop(), "", true, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu ) 
{
  Trace("SUPERVGUI_Information::SUPERVGUI_Information");
  setCaption( tr( "TLT_INFORMATIONS" ) );
  setSizeGripEnabled( true );
  myNode = node;

  QGridLayout* TopLayout = new QGridLayout( this );
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin( 11 );
    
  QGroupBox* TopGroup = new QGroupBox( this, "TopGroup" );
  TopGroup->setColumnLayout(0, Qt::Vertical );
  TopGroup->layout()->setSpacing( 0 );
  TopGroup->layout()->setMargin( 0 );
  QGridLayout* TopGroupLayout = new QGridLayout( TopGroup->layout() );
  TopGroupLayout->setAlignment( Qt::AlignTop );
  TopGroupLayout->setSpacing( 6 );
  TopGroupLayout->setMargin( 11 );

  QLabel* nameL = new QLabel( tr( "NAME_LBL" ), TopGroup );  
  nameV = new QLineEdit( TopGroup );      
  nameV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  nameV->setMinimumSize( 200, 0 );
  nameV->setReadOnly( isReadOnly );
  nameV->setText( node->Name() );
  
  QLabel* authL = new QLabel( tr( "AUTHOR_LBL" ), TopGroup); 
  authV = new QLineEdit( TopGroup );
  authV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  authV->setMinimumSize( 200, 0 );
  authV->setReadOnly( isReadOnly );
  authV->setText( node->Author() );

  
  if (node->IsFactory()) {
    contL = new QLabel( tr( "CONTAINER_LBL" ), TopGroup ); 
    contV = new QLineEdit( TopGroup );
    contV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
    contV->setMinimumSize( 200, 0 );
    contV->setReadOnly( false/*isReadOnly*/ );
    contV->setText( SUPERV::FNode::_narrow(node)->GetContainer() );

    compnameL = new QLabel( tr( "COMPONENT_NAME_LBL" ), TopGroup ); 
    compnameV = new QLineEdit( TopGroup );      
    compnameV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
    compnameV->setMinimumSize( 200, 0 );
    compnameV->setReadOnly( true/*isReadOnly*/ );
    compnameV->setText( SUPERV::FNode::_narrow(node)->GetComponentName() ); // mkr : IPAL10198

    intnameL = new QLabel( tr( "INTERFACE_NAME_LBL" ), TopGroup ); 
    intnameV = new QLineEdit( TopGroup );      
    intnameV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
    intnameV->setMinimumSize( 200, 0 );
    intnameV->setReadOnly( true/*isReadOnly*/ );
    intnameV->setText( SUPERV::FNode::_narrow(node)->GetInterfaceName() );

  }
  QLabel* cdatL = new QLabel( tr( "DATE_CREATION_LBL" ), TopGroup ); 
  cdatV = new QLabel( TopGroup );
  cdatV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  cdatV->setMinimumSize( 200, 0 );
  cdatV->setText( date( node->CreationDate() ) );

  QLabel* udatL = new QLabel( tr( "DATE_MODIFICATION_LBL" ), TopGroup ); 
  udatV = new QLabel( TopGroup );
  udatV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  udatV->setMinimumSize( 200, 0 );
  udatV->setText( date( node->LastUpdateDate() ) );

  QLabel* commL = new QLabel( tr( "COMMENT_LBL" ), TopGroup); 
  commV = new QMultiLineEdit( TopGroup );
  commV->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding ) );
  commV->setMinimumSize( 200, 100 );
  commV->setReadOnly( isReadOnly );
  commV->setText( node->Comment() );

  TopGroupLayout->addWidget( nameL, 0, 0 );
  TopGroupLayout->addWidget( nameV, 0, 1 );
  TopGroupLayout->addWidget( authL, 1, 0 );
  TopGroupLayout->addWidget( authV, 1, 1 );
  if (node->IsFactory()) {
    TopGroupLayout->addWidget( contL, 2, 0 );
    TopGroupLayout->addWidget( contV, 2, 1 );
    TopGroupLayout->addWidget( compnameL, 3, 0 );
    TopGroupLayout->addWidget( compnameV, 3, 1 );
    TopGroupLayout->addWidget( intnameL, 4, 0 );
    TopGroupLayout->addWidget( intnameV, 4, 1 );
  }
  TopGroupLayout->addWidget( cdatL, 5, 0 );
  TopGroupLayout->addWidget( cdatV, 5, 1 );
  TopGroupLayout->addWidget( udatL, 6, 0 );
  TopGroupLayout->addWidget( udatV, 6, 1 );
  TopGroupLayout->addWidget( commL, 7, 0 );
  TopGroupLayout->addMultiCellWidget( commV, 7, 8, 1, 1 );
  TopGroupLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Minimum, QSizePolicy::Expanding  ), 8, 0 );
  TopGroupLayout->setColStretch( 1, 5 );

  QGroupBox* GroupButtons = new QGroupBox( this, "GroupButtons" );
  GroupButtons->setColumnLayout(0, Qt::Vertical );
  GroupButtons->layout()->setSpacing( 0 );
  GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons->layout() );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );

  // fix for PAL6904: when isReadOnly is set ON (the dataflow was imported) then only "CLOSE" button
  // should be present instead of 2 buttons OK and Cancel.
  QPushButton* cancelB;
  if ( !isReadOnly || node->IsFactory() )  {
    /*added node->IsFactory() because only for Factory nodes we ALWAYS have editable "Container" field  */
    QPushButton* okB = new QPushButton( tr( "BUT_OK" ), GroupButtons );
    connect( okB,     SIGNAL( clicked() ), this, SLOT( okButton() ) );
    cancelB = new QPushButton( tr( "BUT_CANCEL" ), GroupButtons );
    GroupButtonsLayout->addWidget( okB, 0, 0 );
    GroupButtonsLayout->addItem  ( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );
    GroupButtonsLayout->addWidget( cancelB, 0, 2 );
  }
  else {
    cancelB = new QPushButton( tr( "BUT_CLOSE" ), GroupButtons );
    GroupButtonsLayout->addItem  ( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 0 );
    GroupButtonsLayout->addWidget( cancelB, 0, 1 );
    GroupButtonsLayout->addItem  ( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 2 );
  }

  TopLayout->addWidget( TopGroup,     0, 0 );
  TopLayout->addWidget( GroupButtons, 1, 0 );
  TopLayout->setRowStretch( 0, 1 );
  TopLayout->setRowStretch( 1, 0 );

  connect( cancelB, SIGNAL( clicked() ), this, SLOT( koButton() ) );
}

/*!
  Destructor
*/
SUPERVGUI_Information::~SUPERVGUI_Information() {
  Trace("SUPERVGUI_Information::~SUPERVGUI_Information");
}

/*!
  Executes dialog
*/
/*
bool SUPERVGUI_Information::run( SUPERV_CNode node, bool isReadOnly ) {
  Trace("SUPERVGUI_Information::run");
  nameV->setReadOnly( isReadOnly );
  authV->setReadOnly( isReadOnly );
  commV->setReadOnly( isReadOnly );

  nameV->setText( node->Name() );
  authV->setText( node->Author() );
  commV->setText( node->Comment() );
  if (node->IsFactory()) {
    SUPERV_FNode aFNode = SUPERV::FNode::_narrow(node);
    contV->setText( aFNode->GetContainer());
    contL->show();
    contV->show();
  }
  else {
    contL->hide();
    contV->hide();
  }
  qApp->processEvents();
  adjustSize();

  //commV->setText( node->Comment() );
  cdatV->setText( date( node->CreationDate() ) );
  udatV->setText( date( node->LastUpdateDate() ) );

  bool b1     = true;
  bool b2     = true;
  bool b3     = true;
  bool b4     = true;
  bool result = false;
  if ( exec() == Accepted ) {
    if (! isReadOnly ) {
      //b3 = node->SetContainer( contV->text().latin1() );
      //} 
      //else {
      if ( strcmp( node->Name(), nameV->text().latin1() ) != 0 ) {
	b1 = node->SetName( nameV->text().latin1() );
      }
      b2 = node->SetAuthor   ( authV->text().latin1() );
      if (node->IsFactory()) {
	SUPERV_FNode aFNode = SUPERV::FNode::_narrow(node);
	b3 = aFNode->SetContainer( contV->text().latin1() );
      }
      b4 = node->SetComment  ( commV->text().latin1() );
    }
    result = b1 && b2 && b3 && b4;
    if ( !result ) {
      QAD_MessageBox::warn1( this, tr( "ERROR" ), tr( "MSG_CANT_CHANGE_INFO" ), tr( "BUT_OK" ) );
    }
  }
  return result;
  }*/

/*!
  Returns string representation of date
*/
QString SUPERVGUI_Information::date( SUPERV_Date d ) {
  Trace("SUPERVGUI_Information::date");
  QString dt( "" );

  if ( d.Day != 0 ) {
    dt += ( char )( d.Day / 10 + 48 );
    dt += ( char )( d.Day % 10 + 48 );
    dt += '/';
    dt += ( char )( d.Month / 10 + 48 );
    dt += ( char )( d.Month % 10 + 48 );
    dt += '/';
    dt += ( char )( ( d.Year / 1000 ) %10 + 48 );
    dt += ( char )( ( d.Year / 100 ) % 10 + 48 );
    dt += ( char )( ( d.Year / 10  ) % 10 + 48 );
    dt += ( char )( d.Year %10 + 48 );
    dt += ' ';
    dt += ( char )( d.Hour / 10 + 48 );
    dt += ( char )( d.Hour % 10 + 48 );
    dt += ':';
    dt += ( char )( d.Minute / 10 + 48 );
    dt += ( char )( d.Minute % 10 + 48 );
    dt += ':';
    dt += ( char )( d.Second / 10 + 48 );
    dt += ( char )( d.Second % 10 + 48 );
  };
  return( dt );
}

/*!
  <OK> button slot
*/
void SUPERVGUI_Information::okButton() {
  Trace("SUPERVGUI_Information::okButton");
  //mkr : modifications for fixing bug IPAL9972
  bool aIsAccept = true;
  if ( QString( myNode->Name() ).compare( nameV->text() ) != 0 ) {
    if ( !myNode->SetName( nameV->text().latin1()) ) {
      QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr( "ERROR" ), tr( "MSG_CANT_RENAMENODE" ) );
      aIsAccept = false;
    }
    // mkr : PAL7037 => update dataflow name in object browser if this dialog called for the dataflow and the dataflow is in study -->
    if ( myNode->IsGraph() || myNode->IsStreamGraph() ) {
      SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
      if ( aSupMod ) aSupMod->updateDataFlowSOName(SUPERV::Graph::_narrow(myNode));
    }
    // mkr : PAL7037 <--
  }
  myNode->SetAuthor( authV->text().latin1() );
  if (myNode->IsFactory()) {
    SUPERV_FNode aFNode = SUPERV::FNode::_narrow(myNode);
    aFNode->SetContainer( contV->text().latin1() );
  }
  myNode->SetComment( commV->text().latin1() );
  if ( aIsAccept ) accept();
}

/*!
  <Cancel> button slot
*/
void SUPERVGUI_Information::koButton() {
  Trace("SUPERVGUI_Information::koButton");
  reject();
}

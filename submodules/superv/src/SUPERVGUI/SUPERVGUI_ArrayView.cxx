//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_ArrayView.cxx
//  Author : 
//  Module : SUPERV

#include "SUPERVGUI.h"
#include "SUPERVGUI_ArrayView.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI_CanvasCellNodePrs.h"
#include "SUPERVGUI_CanvasLink.h"
#include "SUPERVGUI_CanvasPort.h"

#include <qcolordialog.h>

#if QT_VERSION >= 0x030005
QCursor PanCursor(Qt::SizeAllCursor);
#else
QCursor PanCursor(SizeAllCursor);
#endif

SUPERVGUI_ArrayView::SUPERVGUI_ArrayView(SUPERVGUI_CanvasArray* theArray, SUPERVGUI_Main* theMain):
  QCanvasView(theArray, theMain),
  myMain(theMain)
{
  setName("TableView");

  myIsPanActivated = false;

  myAddStudyItem = 0;
  myCursor = cursor();

  viewport()->setMouseTracking(true); //widget receives mouse move events 
                                      //even if no buttons are pressed down

  myPopup = new QPopupMenu(viewport());

  if (myMain->isEditable()) {
    myPopup->insertItem(tr("MSG_ADD_NODE"), myMain, SLOT(addNode()));
    myPopup->insertItem(tr("MSG_INS_FILE"), myMain, SLOT(insertFile()));
    myPopup->insertSeparator();
  }

  QPopupMenu* aViewPopup = new QPopupMenu(viewport());
  aViewPopup->insertItem(tr("POP_FULLVIEW"), myMain, SLOT(showCanvas()));
  aViewPopup->insertItem(tr("POP_CONTROLVIEW"), myMain, SLOT(showContolFlow()));
  aViewPopup->insertItem(tr("POP_TABLEVIEW"), myMain, SLOT(showCanvasTable()));

  myPopup->insertItem(tr("POP_VIEW"), aViewPopup);
  myPopup->insertSeparator();

  /*QPopupMenu* aZoomPopup = new QPopupMenu(viewport());
  aZoomPopup->insertItem("200%", this, SLOT(zoomIn()));
  aZoomPopup->insertItem("100%", this, SLOT(zoomReset()));
  aZoomPopup->insertItem("50%", this, SLOT(zoomOut()));
  aZoomPopup->insertSeparator();
  aZoomPopup->insertItem("Fit All", this, SLOT(fitAll()));

  myPopup->insertItem("Zoom", aZoomPopup);
  myPopup->insertSeparator();*/

  myAddStudyItem = myPopup->insertItem(tr("MSG_ADD_STUDY"), myMain, SLOT(addDataflowToStudy()));
  myPopup->insertItem(tr("MSG_CHANGE_INFO"), myMain, SLOT(changeInformation()));
  myPopup->insertSeparator();

  myPopup->insertItem(tr("MSG_COPY_DATAFLOW"), myMain, SLOT(copy()));
  myPopup->insertItem(tr("MSG_FILTER_NOTIFY"), myMain, SLOT(filterNotification()));

  myPopup->insertSeparator();
  myPopup->insertItem(tr("MSG_CHANGE_BACKGROUND"), this, SLOT(changeBackground()));

  SUPERVGraph_ViewFrame* anActiveVF = (SUPERVGraph_ViewFrame*)myMain->parent();
  if ( anActiveVF ) {
    myPopup->insertSeparator();
    myShowToolBarItem = myPopup->insertItem( tr( "MEN_SHOW_TOOLBAR" ), myMain, SLOT( onShowToolbar() ) );
  }

  hide();
}
 

SUPERVGUI_ArrayView::~SUPERVGUI_ArrayView()
{
}

void SUPERVGUI_ArrayView::ActivatePanning()
{
  myIsPanActivated = true;
  viewport()->setMouseTracking(false);
}

void SUPERVGUI_ArrayView::ResetView()
{
  setContentsPos(0,0);
  QWMatrix m;
  setWorldMatrix(m);
}

void SUPERVGUI_ArrayView::changeBackground()
{
  QColor aColor = QColorDialog::getColor(canvas()->backgroundColor(), this );
  if ( aColor.isValid() ) {
    // change background color for array view
    canvas()->setBackgroundColor(aColor);
    setPaletteBackgroundColor(aColor.light(120));
    // change background color for canvas view
    getMain()->getCanvasView()->canvas()->setBackgroundColor(aColor);
    getMain()->getCanvasView()->setPaletteBackgroundColor(aColor.light(120));

    // mkr : IPAL10825 -->
    SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
    if ( !aSupMod ) {
      MESSAGE("NULL Supervision module!");
      return;
    }
    aSupMod->setIVFBackgroundColor(aColor);
    // <--
  }
}

void SUPERVGUI_ArrayView::resizeEvent( QResizeEvent* e) {
  QCanvasView::resizeEvent( e );
  canvas()->resize( e->size().width() > canvas()->width() ? e->size().width() : canvas()->width(), 
		    e->size().height() > canvas()->height() ? e->size().height() : canvas()->height() );
}

void SUPERVGUI_ArrayView::contentsMousePressEvent(QMouseEvent* theEvent) {
  myGlobalPoint = theEvent->globalPos();

  if (((theEvent->button() == Qt::MidButton)
       &&
       (theEvent->state() == Qt::ControlButton)) || myIsPanActivated) {
    myIsPanActivated = true;
    viewport()->setMouseTracking(false); //widget only receives mouse move events when at least one 
                                         //mouse button is pressed down while the mouse is being moved
    myCursor = cursor();
    setCursor(PanCursor);
    return;
  } 

  QPoint thePoint = inverseWorldMatrix().map(theEvent->pos());
  // compute collision rectangle
  QRect aSel(thePoint.x()-MARGIN, thePoint.y()-MARGIN, 1+2*MARGIN, 1+2*MARGIN);

  if (theEvent->button() == RightButton) {
    QCanvasItemList l = canvas()->collisions(aSel);
    for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
      if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Node) {
	SUPERVGUI_CanvasCellNodePrs* aNodePrs = (SUPERVGUI_CanvasCellNodePrs*) (*it);
	QObject* anObj = aNodePrs->getObject(thePoint);
	if (anObj->inherits("SUPERVGUI_CanvasCellNode")) {
	  myMain->showPopup(((SUPERVGUI_CanvasCellNode*)anObj)->getPopupMenu(viewport()), 
			    theEvent);
	  return;
	}
      }
    }

    myPopup->setItemEnabled(myAddStudyItem, !myMain->isDataflowInStudy());
    myPopup->setItemEnabled(myShowToolBarItem, !((SUPERVGraph_ViewFrame*)myMain->parent())->getToolBar()->isVisible());
    myMain->showPopup(myPopup, theEvent);
    return;
  }
}

void SUPERVGUI_ArrayView::contentsMouseMoveEvent(QMouseEvent* theEvent) {
  QPoint g = theEvent->globalPos();
  if (myIsPanActivated) {
    scrollBy(myGlobalPoint.x() - g.x(),
	     myGlobalPoint.y() - g.y());
    myGlobalPoint = g;
    return;
  }

  // QToolTip for title and label for SUPERVGUI_CanvasCellNode
  SUPERVGUI_ToolTip* aTT = new SUPERVGUI_ToolTip(this);
  aTT->maybeTip(theEvent->pos());
}

void SUPERVGUI_ArrayView::contentsMouseReleaseEvent(QMouseEvent* theEvent) {
  if (myIsPanActivated) {
    myIsPanActivated = false;
    viewport()->setMouseTracking(true);
    setCursor(myCursor);
  }
}

void SUPERVGUI_ToolTip::maybeTip(const QPoint& thePos) {
  // mkr : 23.11.2006 - PAL13957 - modifications for correct tooltips displaying after scroll or zoom
  QPoint theP = ((QCanvasView*)parentWidget())->inverseWorldMatrix().map(thePos);

  int avX, avY;
  ((QCanvasView*)parentWidget())->contentsToViewport(theP.x(), theP.y(), avX, avY);
  QPoint avP(avX, avY);

  QWMatrix aWM = ((QCanvasView*)parentWidget())->worldMatrix();
  
  // compute collision rectangle for nodes, ports
  QRect aNodeSel(theP.x()-MARGIN, theP.y()-MARGIN, 1+2*MARGIN, 1+2*MARGIN);

  QCanvasItemList lN = ((QCanvasView*)parentWidget())->canvas()->collisions(aNodeSel);
  for (QCanvasItemList::Iterator it = lN.begin(); it != lN.end(); ++it)
  {
    // tooltip for node
    if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Node)
    {
      SUPERVGUI_CanvasNodePrs* aNodePrs = (SUPERVGUI_CanvasNodePrs*) (*it);
      QObject* anObj = aNodePrs->getObject(theP);
      if (anObj->inherits("SUPERVGUI_CanvasNode"))
      {
	QRect aTitleRect = aNodePrs->getTitleRect();
	QRect aLabelRect = aNodePrs->getLabelRect();
	QRect aStatusRect = aNodePrs->getStatusRect();

	if (aTitleRect.bottom()+1 == aLabelRect.top() &&
	    aLabelRect.bottom()+1 == aStatusRect.top())
	{
	  ((QCanvasView*)parentWidget())->contentsToViewport((int)(aTitleRect.left()*aWM.m11()), 
							     (int)(aTitleRect.top()*aWM.m22()), 
							     avX, avY);
	  QRect avTipRect(avX, avY,
			  (int)(aTitleRect.width()*aWM.m11()),
			  (int)((aTitleRect.height() + aLabelRect.height() + aStatusRect.height())*aWM.m22()));
	  tip(avTipRect, ((SUPERVGUI_CanvasNode*)anObj)->getToolTipText());
	  return;
	}

	if (aTitleRect.contains(theP, true))
	{
	  ((QCanvasView*)parentWidget())->contentsToViewport((int)(aTitleRect.left()*aWM.m11()), 
							     (int)(aTitleRect.top()*aWM.m22()), 
							     avX, avY);
	  QRect avTipRect(avX, avY, 
			  (int)(aTitleRect.width()*aWM.m11()), (int)(aTitleRect.height()*aWM.m22()));
	  tip(avTipRect, ((SUPERVGUI_CanvasNode*)anObj)->getToolTipText());
	  return;
	}

	if (aLabelRect.contains(theP, true))
	{
	  ((QCanvasView*)parentWidget())->contentsToViewport((int)(aLabelRect.left()*aWM.m11()), 
							     (int)(aLabelRect.top()*aWM.m22()), 
							     avX, avY);
	  QRect avTipRect(avX, avY, 
			  (int)(aLabelRect.width()*aWM.m11()), (int)(aLabelRect.height()*aWM.m22()));
	  tip(avTipRect, ((SUPERVGUI_CanvasNode*)anObj)->getToolTipText());
	  return;
	}

	if (aStatusRect.contains(theP, true))
	{
	  ((QCanvasView*)parentWidget())->contentsToViewport((int)(aStatusRect.left()*aWM.m11()), 
							     (int)(aStatusRect.top()*aWM.m22()), 
							     avX, avY);
	  QRect avTipRect(avX, avY, 
			  (int)(aStatusRect.width()*aWM.m11()), (int)(aStatusRect.height()*aWM.m22()));
	  tip(avTipRect, ((SUPERVGUI_CanvasNode*)anObj)->getToolTipText());
	  return;
	}
	
      }
      // tooltip for nodes' port
      if (anObj->inherits("SUPERVGUI_CanvasPort"))
      {
	SUPERVGUI_CanvasPort* aPort = (SUPERVGUI_CanvasPort*)anObj;
	QRect aPortRect = aPort->getPrs()->getPortRect();
	((QCanvasView*)parentWidget())->contentsToViewport((int)(aPortRect.left()*aWM.m11()), 
							   (int)(aPortRect.top()*aWM.m22()), 
							   avX, avY);
	QRect avTipRect(avX, avY, 
			(int)(aPortRect.width()*aWM.m11()), (int)(aPortRect.height()*aWM.m22()));
	tip(avTipRect, aPort->getEngine()->Type() + QString(" ") + aPort->getPrs()->getText());
	return;
      }
    }
  }

  // compute collision rectangle for links
  QRect aLinkSel((int)( theP.x() + (theP.x()-avP.x())/aWM.m11() ) - MARGIN, 
		 (int)( theP.y() + (theP.y()-avP.y())/aWM.m22() ) - MARGIN, 
		 1+2*MARGIN, 1+2*MARGIN);

  QCanvasItemList lL = ((QCanvasView*)parentWidget())->canvas()->collisions(aLinkSel);
  for (QCanvasItemList::Iterator it = lL.begin(); it != lL.end(); ++it)
  {
    // tootip for links' point
    if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint)
    {
      SUPERVGUI_CanvasPointPrs* aPrs = (SUPERVGUI_CanvasPointPrs*) (*it);
      if (aPrs->getLink()->getMain()->getViewType() == CANVAS)
      {
	((QCanvasView*)parentWidget())->contentsToViewport((int)(aLinkSel.left()*aWM.m11()), 
							   (int)(aLinkSel.top()*aWM.m22()), 
							   avX, avY);
	QRect avTipRect(avX, avY, 
			(int)(aLinkSel.width()*aWM.m11()), (int)(aLinkSel.height()*aWM.m22()));
	tip(avTipRect, aPrs->getLink()->getToolTipText());
      }
      return;
    }
    // tooltip for links' edge
    if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge)
    {
      SUPERVGUI_CanvasEdgePrs* aPrs = (SUPERVGUI_CanvasEdgePrs*) (*it);
      if (aPrs->getLink()->getMain()->getViewType() == CANVAS)
      {
	((QCanvasView*)parentWidget())->contentsToViewport((int)(aLinkSel.left()*aWM.m11()), 
							   (int)(aLinkSel.top()*aWM.m22()), 
							   avX, avY);
	QRect avTipRect(avX, avY, 
			(int)(aLinkSel.width()*aWM.m11()), (int)(aLinkSel.height()*aWM.m22()));
	tip(avTipRect, aPrs->getLink()->getToolTipText());
      }
      return;
    }
  }  
}

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_ManagePortsDlg.cxx
//  Author : Alexander SLADKOV
//  Module : SUPERV

#include "SUIT_Desktop.h"
#include "SUIT_Session.h"

#include "SUPERVGUI_ManagePortsDlg.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasNodePrs.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_CanvasControlNode.h"
#include "SUPERVGUI_CanvasLink.h"

#include <qlayout.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qhbox.h>
#include <qgroupbox.h>

static const char* gTypes[] = 
  {"string", "boolean", "char", "short", "int", "long", "float", "double", "objref"};

static const char* const add_pix_data[] = { 
"20 20 4 1",
". c None",
"b c #084818",
"a c #186929",
"# c #208541",
"....................",
"....................",
"....................",
"....................",
"........##a.........",
"........##b.........",
"........##b.........",
"........##b.........",
"........##b.........",
"...############a....",
"...############b....",
"...abbbb##bbbbbb....",
"........##b.........",
"........##b.........",
"........##b.........",
"........##b.........",
"........abb.........",
"....................",
"....................",
"...................."};

static const char* const del_pix_data[] = { 
"16 16 3 1",
". c None",
"# c #800000",
"a c #ffffff",
"................",
"................",
".##a........##a.",
".####a.....##a..",
"..####a...##a...",
"....###a.#a.....",
".....#####a.....",
"......###a......",
".....#####a.....",
"....###a.##a....",
"...###a...##a...",
"..###a.....#a...",
"..###a......#a..",
"...#a........#a.",
"................",
"................"};

static const char* const up_pix_data[] = { 
"16 16 5 1",
". c None",
"# c #000000",
"a c #838183",
"c c #c5c2c5",
"b c #ffffff",
"................",
"................",
"........#.......",
"......a#b#......",
".....a#bbb#.....",
"....a#bccbb#....",
"...a#bccccbb#...",
"..a####ccb####..",
".aaaaa#ccb#.....",
".....a#ccb#.....",
".....a#ccb#.....",
".....a#ccb#.....",
".....a#ccb#.....",
".....a#ccb#.....",
".....a#####.....",
"................"};

static const char* const down_pix_data[] = { 
"16 16 5 1",
". c None",
"# c #000000",
"a c #838183",
"c c #c5c2c5",
"b c #ffffff",
"................",
".....#####a.....",
".....#bcc#a.....",
".....#bcc#a.....",
".....#bcc#a.....",
".....#bcc#a.....",
".....#bcc#a.....",
".....#bcc#aaaaa.",
"..####bcc####a..",
"...#bbccccb#a...",
"....#bbccb#a....",
".....#bbb#a.....",
"......#b#a......",
".......#........",
"................",
"................"};


/**
 * Constructor
 */
SUPERVGUI_ManagePortsDlg::SUPERVGUI_ManagePortsDlg( SUPERVGUI_CanvasNode* theNode )
  : QDialog( SUIT_Session::session()->activeApplication()->desktop(), 0, false, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu )
{
  myNode = theNode;
  myLastItem = 0;
  init();
}

void SUPERVGUI_ManagePortsDlg::init()
{
  setName( "SUPERVGUI_ManagePortsDlg" );

  SUPERV_CNode aCNode = myNode->getEngine();
  setCaption( tr( "TIT_MANAGE_PORTS" ) + aCNode->Name() );

  QGridLayout* aBaseLayout = new QGridLayout( this );
  aBaseLayout->setMargin( 11 ); 
  aBaseLayout->setSpacing( 6 );

  // -- name and type upper line widgets --
  QLabel* aNameLbl = new QLabel( tr( "NAME_LBL") , this );
  myNameEdt = new QLineEdit( this );
  QLabel* aTypeLbl = new QLabel( tr( "TYPE_LBL" ), this );
  myTypeCombo = new QComboBox( false, this ); // input field not editable

  aBaseLayout->addWidget( aNameLbl, 0, 0 );
  aBaseLayout->addWidget( myNameEdt, 0, 1 );
  aBaseLayout->addWidget( aTypeLbl, 0, 2 );  
  aBaseLayout->addWidget( myTypeCombo, 0, 3 );
  aBaseLayout->setColStretch( 1, 1 );
  aBaseLayout->setColStretch( 3, 1 );

  // -- common Input/Output variables --
  QPixmap adPix( (const char**) add_pix_data );
  QPixmap rmPix( (const char**) del_pix_data );
  QPixmap upPix( (const char**) up_pix_data );
  QPixmap dnPix( (const char**) down_pix_data );

  // -- Input ports group box --
  QGroupBox* anInputGr = new QGroupBox( 1, Qt::Vertical, tr( "INPUT" ), this );
  anInputGr->layout()->setSpacing( 6 ); 
  anInputGr->layout()->setMargin( 11 );

  myInList = new QListBox( anInputGr );
  myInList->setSelectionMode( QListBox::Single );

  QGroupBox* anInputBtnGr = new QGroupBox( 0, Qt::Vertical, anInputGr );
  anInputBtnGr->setFrameStyle( QFrame::NoFrame );
  anInputBtnGr->layout()->setSpacing( 0 ); 
  anInputBtnGr->layout()->setMargin( 0 );  

  QVBoxLayout* anInputBtnLay = new QVBoxLayout( anInputBtnGr->layout() );
  anInputBtnLay->layout()->setSpacing( 6 ); 
  anInputBtnLay->layout()->setMargin( 0 );
  
  QPushButton* anAddInputBtn = new QPushButton( anInputBtnGr );
  anAddInputBtn->setPixmap( adPix );
  QPushButton* aDeleteInputBtn = new QPushButton( anInputBtnGr );
  aDeleteInputBtn->setPixmap( rmPix );
  QPushButton* anUpInputBtn = new QPushButton( anInputBtnGr );
  anUpInputBtn->setPixmap( upPix );
  QPushButton* aDownInputBtn = new QPushButton( anInputBtnGr );
  aDownInputBtn->setPixmap( dnPix );

  connect( anAddInputBtn, SIGNAL( clicked() ), this, SLOT( addInput() ) );
  connect( aDeleteInputBtn, SIGNAL( clicked() ), this, SLOT( removeInput() ) );
  connect( anUpInputBtn, SIGNAL( clicked() ), this, SLOT( upInput() ) );
  connect( aDownInputBtn, SIGNAL( clicked() ), this, SLOT( downInput() ) );

  anInputBtnLay->addWidget( anAddInputBtn );
  anInputBtnLay->addWidget( aDeleteInputBtn );
  anInputBtnLay->addItem( new QSpacerItem( 1, 16, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding ) );
  anInputBtnLay->addWidget( anUpInputBtn );
  anInputBtnLay->addWidget( aDownInputBtn );
  anInputBtnLay->addItem( new QSpacerItem( 1, 16, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding ) );

  aBaseLayout->addMultiCellWidget( anInputGr, 1, 1, 0, 1 );

  // -- Output ports group box --
  QGroupBox* anOutputGr = new QGroupBox( 1, Qt::Vertical, tr( "OUTPUT" ), this );
  anOutputGr->layout()->setSpacing( 6 ); 
  anOutputGr->layout()->setMargin( 11 );

  myOutList = new QListBox( anOutputGr );
  myOutList->setSelectionMode( QListBox::Single );

  QGroupBox* anOutputBtnGr = new QGroupBox( 0, Qt::Vertical, anOutputGr );
  anOutputBtnGr->setFrameStyle( QFrame::NoFrame );
  anOutputBtnGr->layout()->setSpacing( 0 ); 
  anOutputBtnGr->layout()->setMargin( 0 );  

  QVBoxLayout* anOutputBtnLay = new QVBoxLayout( anOutputBtnGr->layout() );
  anOutputBtnLay->layout()->setSpacing( 6 ); 
  anOutputBtnLay->layout()->setMargin( 0 );
  
  QPushButton* anAddOutputBtn = new QPushButton( anOutputBtnGr );
  anAddOutputBtn->setPixmap( adPix );
  QPushButton* aDeleteOutputBtn = new QPushButton( anOutputBtnGr );
  aDeleteOutputBtn->setPixmap( rmPix );
  QPushButton* anUpOutputBtn = new QPushButton( anOutputBtnGr );
  anUpOutputBtn->setPixmap( upPix );
  QPushButton* aDownOutputBtn = new QPushButton( anOutputBtnGr );
  aDownOutputBtn->setPixmap( dnPix );

  connect( anAddOutputBtn, SIGNAL( clicked() ), this, SLOT( addOutput() ) );
  connect( aDeleteOutputBtn, SIGNAL( clicked() ), this, SLOT( removeOutput() ) );
  connect( anUpOutputBtn, SIGNAL( clicked() ), this, SLOT( upOutput() ) );
  connect( aDownOutputBtn, SIGNAL( clicked() ), this, SLOT( downOutput() ) );

  anOutputBtnLay->addWidget( anAddOutputBtn );
  anOutputBtnLay->addWidget( aDeleteOutputBtn );
  anOutputBtnLay->addItem( new QSpacerItem( 1, 16, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding ) );
  anOutputBtnLay->addWidget( anUpOutputBtn );
  anOutputBtnLay->addWidget( aDownOutputBtn );
  anOutputBtnLay->addItem( new QSpacerItem( 1, 16, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding ) );

  aBaseLayout->addMultiCellWidget( anOutputGr, 1, 1, 2, 3 );

  // -- Ok / Cancel buttons line at the bottom of dialog -- 
  QGroupBox* aBtnBox = new QGroupBox( this );
  aBtnBox->setColumnLayout( 0, Qt::Vertical );
  aBtnBox->layout()->setSpacing( 0 ); 
  aBtnBox->layout()->setMargin( 0 );
  
  QHBoxLayout* aBtnLayout = new QHBoxLayout( aBtnBox->layout() );
  aBtnLayout->setAlignment( Qt::AlignTop );
  aBtnLayout->setSpacing( 6 ); 
  aBtnLayout->setMargin( 11 );

  QPushButton* aOkBtn = new QPushButton( tr( "BUT_OK" ), aBtnBox );
  QPushButton* aCancelBtn = new QPushButton( tr( "BUT_CANCEL" ), aBtnBox );
  connect( aOkBtn, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( aCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );

  aBtnLayout->addWidget( aOkBtn );
  aBtnLayout->addStretch();
  aBtnLayout->addWidget( aCancelBtn );
  
  aBaseLayout->addMultiCellWidget( aBtnBox, 2, 2, 0, 3 );
  // -----------

  connect( myInList,SIGNAL(currentChanged(QListBoxItem*)), this, SLOT(itemChanged(QListBoxItem*)));
  connect( myOutList, SIGNAL(currentChanged(QListBoxItem*)), this, SLOT(itemChanged(QListBoxItem*)));
  connect( myNameEdt, SIGNAL(textChanged(const QString&)), this, SLOT(nameChanged(const QString&)));
  connect( myTypeCombo, SIGNAL(activated(const QString&)), this, SLOT(typeChanged(const QString&)));

  // mkr: PAL12448
  connect( myInList,  SIGNAL(rightButtonClicked(QListBoxItem*, const QPoint&)),
           this, SLOT(itemDeselect(QListBoxItem*, const QPoint&)));
  connect( myOutList, SIGNAL(rightButtonClicked(QListBoxItem*, const QPoint&)),
           this, SLOT(itemDeselect(QListBoxItem*, const QPoint&)));

  myNode->getMain()->lockedGraph( true );

  // asv : 11.01.05 : if a node is a loop node, then only INPUT ports can be added/removed
  if ( myNode->getNodeType() == SUPERV::LoopNode ) {
    myOutList->setEnabled( false );
    anAddOutputBtn->setEnabled( false );
    aDeleteOutputBtn->setEnabled( false );
    anUpOutputBtn->setEnabled( false );
    aDownOutputBtn->setEnabled( false );
  }

  // NPAL15537: initialization moved here from showEvent()
  init( myNode );
}

/**
 * Destructor
 */
SUPERVGUI_ManagePortsDlg::~SUPERVGUI_ManagePortsDlg() {
}

// mkr : PAL8060
void SUPERVGUI_ManagePortsDlg::createLinkEngine (SUPERV::Port_ptr thePort, 
                                                 QValueList< QPair< QString,QString > >& theOwnList, 
                                                 QValueList< QPair< QString,QString > >& theCorrespList) {
  // pair <NodeName, PortName> for the given port
  QPair<QString, QString> anOwnPair(QString(thePort->Node()->Name()), QString(thePort->Name()));
  int aNum = theOwnList.contains(anOwnPair);
  while ( aNum > 0 ) {
    int anId = theOwnList.findIndex(anOwnPair); // get index in theOwnList
    QPair<QString, QString> aCorrespPair = theCorrespList[anId]; // corresponding pair in theCorrespList
    
    theOwnList.remove(theOwnList.at(anId));
    theCorrespList.remove(theCorrespList.at(anId));
    
    SUPERV_Port aCorrespPort =
      myNode->getMain()->getDataflow()->Node(aCorrespPair.first)->Port(aCorrespPair.second);
    SUPERV_Link aLinkEngine;
    if ( thePort->IsInput() )
      aLinkEngine = myNode->getMain()->getDataflow()->Link(aCorrespPort, thePort);
    else
      aLinkEngine = myNode->getMain()->getDataflow()->Link(thePort, aCorrespPort);
    SUPERVGUI_CanvasLink* aLink = new SUPERVGUI_CanvasLink(myNode->getMain()->getCanvas(),
                                                           myNode->getMain(), aLinkEngine);
    aLink->show();

    aNum--;
  }
}

/** 
 * Set the ports as entered by user (order, name/type, etc.) and close the dialog
 */
void SUPERVGUI_ManagePortsDlg::accept()
{
  myNode->getMain()->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 

  // 1. set the ports to Engine's CNode
  SUPERV_INode aINode = myNode->getInlineNode();
  if ( !SUPERV_isNull( aINode ) ) {
    int i; 
    const bool isLoop = ( myNode->getNodeType() == SUPERV::LoopNode );

    // 1.1 form a list of ports to be removed
    QObjectList* oldPorts = myNode->queryList("SUPERVGUI_CanvasPort");

    // asv : 11.01.05 : fix for a bug: for Loop node, removal of an Input Port
    // automatically removes the corresponding Output Port. So for Loops the
    // oldPorts list should be filtered to include only Input Ports.
    // But I'll filter Gate-s as well..
    QObjectList portsToRemove;
    QObject *obj;
    QObjectListIt it ( *oldPorts ); // iterate over the old ports
    for (; (obj = it.current()) != 0; ++it) {
      SUPERV::Port_var aPort = ((SUPERVGUI_CanvasPort*)obj)->getEngine();
      if ( !aPort->IsGate() ) {
        if ( !isLoop || aPort->IsInput() ) {
          portsToRemove.append( obj );
        }
      }
    }
    delete oldPorts; // delete the list, not the objects

    // 1.2 create new ports in INode and CanvasPort in myNode
    //     or just reorder existing ports
    PortListItem* item;
    for (i = 0; i < myInList->count(); i++) {
      item = (PortListItem*)myInList->item( i );
      // try existing ports
      bool isExisting = false;
      QObjectListIt itRem (portsToRemove);
      QObject *objRem;
      for (; (objRem = itRem.current()) != 0 && !isExisting; ++itRem) {
        SUPERV::Port_var aPort = ((SUPERVGUI_CanvasPort*)objRem)->getEngine();
        if (item->PortName.compare(aPort->Name()) == 0 &&
            item->PortType.compare(aPort->Type()) == 0) {
          // ports reordering
          myNode->removeChild(objRem);
          myNode->insertChild(objRem);
          portsToRemove.remove(objRem);
          isExisting = true;
        }
      }
      if (!isExisting) {
        // create a new port
        SUPERV::Port_var aPort = aINode->InPort( item->PortName.latin1(), item->PortType.latin1() );
        myNode->createPort( aPort.in() );
      }
    }

    // 1.3 create/reorder Out-ports
    if (isLoop) {
      // asv : 11.01.05 : for Loop nodes do the same as in SUPERVGUI_CanvasStartNode::addInputPort()
      SUPERVGUI_CanvasStartNode* aStartLoopNode = (SUPERVGUI_CanvasStartNode*)myNode;
      aStartLoopNode->merge();
      aStartLoopNode->getCoupled()->merge();
    }
    else {
      // for non-loop nodes manage out-ports in the same way as in-ports
      for (i = 0; i < myOutList->count(); i++) {
        item = (PortListItem*)myOutList->item( i );
        // try existing ports
        bool isExisting = false;
        QObjectListIt itRem (portsToRemove);
        QObject *objRem;
        for (; (objRem = itRem.current()) != 0 && !isExisting; ++itRem) {
          SUPERV::Port_var aPort = ((SUPERVGUI_CanvasPort*)objRem)->getEngine();
          if (item->PortName.compare(aPort->Name()) == 0 &&
              item->PortType.compare(aPort->Type()) == 0) {
            // ports reordering
            myNode->removeChild(objRem);
            myNode->insertChild(objRem);
            portsToRemove.remove(objRem);
            isExisting = true;
          }
        }
        if (!isExisting) {
          // create a new port
          SUPERV::Port_var aPort = aINode->OutPort(item->PortName.latin1(), item->PortType.latin1());
          myNode->createPort(aPort.in());
        }
      }
    }

    // 1.4 remove disappeared ports
    QObjectListIt itRem ( portsToRemove );
    QObject *objRem;
    for (; (objRem = itRem.current()) != 0; ++itRem) {
      ((SUPERVGUI_CanvasPort*)objRem)->remove();
    }

    // 2. update the node's presentation
    myNode->getPrs()->updatePorts();
    myNode->getMain()->getCanvas()->update();
  }

  // 3. close the dialog
  myNode->getMain()->lockedGraph( false );
  QDialog::accept();
}

/** 
 * Simply close the dialog
 */
void SUPERVGUI_ManagePortsDlg::reject() {
  myNode->getMain()->lockedGraph( false );
  QDialog::reject();
  close();
}

/**
 * Initialize In/Out tables with values from the given node
 */
void SUPERVGUI_ManagePortsDlg::init( const SUPERVGUI_CanvasNode* theNode ) {
  if ( !theNode ) {
    MESSAGE("ERROR: SUPERVGUI_ManagePortsDlg: the CanvasNode is nil!");
    return;
  }

  SUPERV_INode aINode = myNode->getInlineNode();
  if ( SUPERV_isNull( aINode ) ) {
    MESSAGE("ERROR: SUPERVGUI_ManagePortsDlg: the node of wrong type is found (not InLine node)!");
    return;
  }
  
  // 1. fill myTypeCombo with all available types
  myTypeCombo->insertStrList( gTypes );
  
  // 2. fill Input and Output listboxes with Ports' names/types
  QStringList aTypes;
  SUPERV_Ports aPorts = aINode->Ports();
  for ( int i=0; i < aPorts->length(); i++ ) {
    if ( aPorts[i]->IsGate() )
      continue;
    if ( aPorts[i]->IsInput()
         &&
         !myInList->findItem( QString( "%1 (%2)" ).arg( aPorts[i]->Name() ).arg( aPorts[i]->Type() ) ) ) // mkr : PAL11332
      new PortListItem( myInList, aPorts[i]->Name(), aPorts[i]->Type() );
    if ( !aPorts[i]->IsInput()
         &&
         !myOutList->findItem( QString( "%1 (%2)" ).arg( aPorts[i]->Name() ).arg( aPorts[i]->Type() ) ) ) // mkr : PAL11332 
      new PortListItem( myOutList, aPorts[i]->Name(), aPorts[i]->Type() );
  }
}

void SUPERVGUI_ManagePortsDlg::addPort( QListBox* theLB ) {
  if ( !theLB )
    return;
  QString name = myNameEdt->text();
  if ( !name.isNull() && name.length() ) {
    bool found = false; // check if already exists -- and don't allow if yes
    for ( int i = 0; i < theLB->count(); i++ ) {
      if ( ((PortListItem*)theLB->item(i))->PortName == name ) {
        found = true;
        break;
      }
    }
    if ( found )
      QMessageBox::warning( this, tr("ERROR"), tr("MSG_PORT_EXIST") ); // mkr : IPAL10386
    else
      new PortListItem( theLB, name, myTypeCombo->currentText() );
  }
}

/**
 * called on 'X' button press - remove selected port item
 */
void SUPERVGUI_ManagePortsDlg::removePort( QListBox* theLB ) {
  if ( !theLB )
    return;

  const int i = theLB->currentItem();
  if ( i >=0 )
    delete theLB->item( i );    
}

/**
 * called on up/down key press - move the selected list box item from position 'from' to position 'to'
 */
void SUPERVGUI_ManagePortsDlg::moveItem( QListBox* theLB, const int from, const int to ) {
  if ( !theLB )
    return;

  // disconnect itemChanged - it leads to crash if there was only 1 item left
  disconnect( theLB, SIGNAL(currentChanged(QListBoxItem*)), this, SLOT(itemChanged(QListBoxItem*)));

  if ( from >= 0 && from < theLB->count() && to >=0 && to < theLB->count() ) {
    QListBoxItem* item = theLB->item( from );
    theLB->takeItem( item );
    theLB->insertItem( item, to );
    theLB->setCurrentItem( item );
    theLB->setSelected( item, true );
  }

  // connect back..
  connect( theLB, SIGNAL(currentChanged(QListBoxItem*)), this, SLOT(itemChanged(QListBoxItem*)));
}

/**
 * move the selected item UP
 */
void SUPERVGUI_ManagePortsDlg::moveUp( QListBox* theLB ) {
  if ( !theLB )
    return;
  const int i = theLB->currentItem();
  moveItem( theLB, i, i-1 );
}

/**
 * move the selected item DOWN
 */
void SUPERVGUI_ManagePortsDlg::moveDown( QListBox* theLB ) {
  if ( !theLB )
    return;
  const int i = theLB->currentItem();
  moveItem( theLB, i, i+1 );
}


/**
 * returns true if there is a current and selected item in the given listbox and it
 * equals to the given item.
 */
bool isEditingItem( const QListBoxItem* item, const QListBox* theLB ) {
  return item && item == theLB->item( theLB->currentItem() ) && theLB->isSelected( item );
}

/**
 * called when name is changed.  Tries to change name of the item, which was
 * the last selected by user item in myInList or myOutList (myLastItem)
 */
void SUPERVGUI_ManagePortsDlg::nameChanged( const QString& name ) {
  // mkr: IPAL12512
  if ( isEditingItem( myLastItem, myInList ) && myInList->findItem( QString( "%1 (%2)" ).arg( name ).arg( ((PortListItem*)myLastItem)->PortType ))
       ||
       isEditingItem( myLastItem, myOutList ) && myOutList->findItem( QString( "%1 (%2)" ).arg( name ).arg( ((PortListItem*)myLastItem)->PortType ))) {
    disconnect( myNameEdt, SIGNAL(textChanged(const QString&)), this, SLOT(nameChanged(const QString&)));
    
    myNameEdt->setText( ((PortListItem*)myLastItem)->PortName );
    
    connect( myNameEdt, SIGNAL(textChanged(const QString&)), this, SLOT(nameChanged(const QString&)));
    QMessageBox::warning( this, tr("ERROR"), tr("MSG_PORT_EXIST") );
    return;
  }
  
  if ( isEditingItem( myLastItem, myInList ) || isEditingItem( myLastItem, myOutList ) ) {
    ((PortListItem*)myLastItem)->PortName = name;
    //myLastItem->listBox()->repaint( true );
    // it would be much better to change the item and repaint it -- but -- repaint() 
    // is done only after a while, which is not good..  so -- "cheating" around
    const int i = myLastItem->listBox()->index( myLastItem );
    moveItem( myLastItem->listBox(), i, i );
  }
}

/**
 * called when type item in myTypeCombo is changed.  Tries to change type
 * of the last selected by user item in myInList or myOutList (myLastItem)
 */
void SUPERVGUI_ManagePortsDlg::typeChanged( const QString& type ) {
  if ( isEditingItem( myLastItem, myInList ) || isEditingItem( myLastItem, myOutList ) ) {
    ((PortListItem*)myLastItem)->PortType = type;
    //myLastItem->listBox()->repaint( true );
    // it would be much better to change the item and repaint it -- but -- repaint() 
    // is done only after a while, which is not good..  so -- "cheating" around
    const int i = myLastItem->listBox()->index( myLastItem );
    moveItem( myLastItem->listBox(), i, i );
  }
}

/**
 * called when new item became "currentItem" in any myInList or myOutList listboxes.
 */
void SUPERVGUI_ManagePortsDlg::itemChanged( QListBoxItem * item ) {
  if ( !item ) // after remove, etc..
    return;

  // disable nameChanged, typeChanged callbacks
  disconnect( myNameEdt, SIGNAL(textChanged(const QString&)), this, SLOT(nameChanged(const QString&)));
  disconnect( myTypeCombo, SIGNAL(activated(const QString&)), this, SLOT(typeChanged(const QString&)));
  
  myLastItem = item;
  myNameEdt->setText( ((PortListItem*)item)->PortName );
  for ( int i = 0; i < myTypeCombo->count(); i++ )
    if ( myTypeCombo->text( i ) == ((PortListItem*)item)->PortType ) {
      myTypeCombo->setCurrentItem( i );
      break;
    }

  // connect back nameChanged, typeChanged
  connect( myNameEdt, SIGNAL(textChanged(const QString&)), this, SLOT(nameChanged(const QString&)));
  connect( myTypeCombo, SIGNAL(activated(const QString&)), this, SLOT(typeChanged(const QString&)));
}

/*!
 * Deselect the current selected item
 */
void SUPERVGUI_ManagePortsDlg::itemDeselect(QListBoxItem* item, const QPoint& point) {
  // mkr: PAL12448
  if ( item->isSelected() )
    item->listBox()->setSelected(item, false);
}

/*!
 * Port parameters dialog definition (taken from SUPERVGUI_Node.cxx without change)
 */
SUPERVGUI_PortParamsDlg::SUPERVGUI_PortParamsDlg(const QStringList& thePortsNames)
  : QDialog(SUIT_Session::session()->activeApplication()->desktop(), 0, true, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu  ),
    myPortsNames( thePortsNames )
{
  setSizeGripEnabled( true );
  setCaption("Port parameters");

  QGridLayout* aBaseLayout = new QGridLayout( this );
  aBaseLayout->setMargin( 11 ); aBaseLayout->setSpacing( 6 );

  QLabel* aNameLbl = new QLabel("Port Name", this );
  aBaseLayout->addWidget(aNameLbl, 0, 0);

  myNameTxt = new QLineEdit( this );
  aNameLbl->setBuddy( myNameTxt );
  aBaseLayout->addWidget(myNameTxt, 0, 1);
  
  QLabel* aTypeLbl = new QLabel("Value Type", this );
  aBaseLayout->addWidget(aTypeLbl, 1, 0);
 
  myTypeTxt = new QComboBox( this );
  aTypeLbl->setBuddy( myTypeTxt );
  myTypeTxt->insertStrList( gTypes );
  aBaseLayout->addWidget(myTypeTxt, 1, 1);
 
  QGroupBox* aBtnBox = new QGroupBox( this );
  aBtnBox->setColumnLayout( 0, Qt::Vertical );
  aBtnBox->layout()->setSpacing( 0 ); aBtnBox->layout()->setMargin( 0 );
  QHBoxLayout* aBtnLayout = new QHBoxLayout( aBtnBox->layout() );
  aBtnLayout->setAlignment( Qt::AlignTop );
  aBtnLayout->setSpacing( 6 ); aBtnLayout->setMargin( 11 );

  aBaseLayout->addMultiCellWidget( aBtnBox, 2, 2, 0, 1 );

  QPushButton* aOKBtn = new QPushButton( tr( "BUT_OK" ), aBtnBox );
  connect( aOKBtn, SIGNAL( clicked() ), this, SLOT( clickOnOk() ) );
  aBtnLayout->addWidget( aOKBtn );

  aBtnLayout->addStretch();

  QPushButton* aCancelBtn = new QPushButton( tr("BUT_CANCEL"), aBtnBox );
  connect( aCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  aBtnLayout->addWidget( aCancelBtn );
}

void SUPERVGUI_PortParamsDlg::clickOnOk()
{
  if (getName().isEmpty() || getType().isEmpty())
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(),
                         tr("ERROR"), tr("MSG_CANT_CREATE_PORT"));
  else if (myPortsNames.contains(getName()))
    QMessageBox::warning( this, tr("ERROR"), tr("MSG_PORT_EXIST") ); // mkr : IPAL10386
  else {
    accept();
    close();
  }
}

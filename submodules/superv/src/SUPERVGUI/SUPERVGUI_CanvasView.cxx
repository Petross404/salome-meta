//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasView.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI.h"
#include "SUPERVGUI_CanvasView.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_CanvasLink.h"
#include "SUPERVGUI_CanvasNodePrs.h"
#include "SUPERVGUI_Clipboard.h"

#include "SUPERVGUI_ArrayView.h" //for tooltip testing

#include "SUIT_MessageBox.h"

#include <qpixmap.h>
#include <qcolordialog.h>

#define MARGIN 2


/* XPM */
const char* imageZoomCursor[] = { 
"32 32 3 1",
". c None",
"a c #000000",
"# c #ffffff",
"................................",
"................................",
".#######........................",
"..aaaaaaa.......................",
"................................",
".............#####..............",
"...........##.aaaa##............",
"..........#.aa.....a#...........",
".........#.a.........#..........",
".........#a..........#a.........",
"........#.a...........#.........",
"........#a............#a........",
"........#a............#a........",
"........#a............#a........",
"........#a............#a........",
".........#...........#.a........",
".........#a..........#a.........",
".........##.........#.a.........",
"........#####.....##.a..........",
".......###aaa#####.aa...........",
"......###aa...aaaaa.......#.....",
".....###aa................#a....",
"....###aa.................#a....",
"...###aa...............#######..",
"....#aa.................aa#aaaa.",
".....a....................#a....",
"..........................#a....",
"...........................a....",
"................................",
"................................",
"................................",
"................................"};

QPixmap zoomPix(imageZoomCursor);
QCursor zoom2Cursor(zoomPix);
QCursor handCursor(Qt::PointingHandCursor);

#if QT_VERSION >= 0x030005
QCursor pan2Cursor(Qt::SizeAllCursor);
#else
QCursor pan2Cursor(SizeAllCursor);
#endif

SUPERVGUI_CanvasView::SUPERVGUI_CanvasView(SUPERVGUI_Canvas* theCanvas, SUPERVGUI_Main* theMain):
  QCanvasView(theCanvas, theMain),
  myMain(theMain), myCurrentItem(0), myHilighted(0), myLinkBuilder(0)
{
  setName("CanvasView");

  myIsPanBtnClicked = false;
  myIsPanActivated = false;
  myIsFitWRActivated = false;
  myIsZoomActivated = false;
  myIsLinkCreating = false;

  myAddStudyItem = 0;
  mySelectedRect = 0;
  myCursor = cursor();

  myTimer = new QTimer(this);
  connect(myTimer, SIGNAL(timeout()), this, SLOT(onTimeout()));

  myPopup = new QPopupMenu(viewport());

  const bool isEdit = myMain->isEditable();
  if (isEdit) {
    myPopup->insertItem(tr("MSG_ADD_NODE"), myMain, SLOT(addNode()));

    // Paste Node functionality
    SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
    myPasteNodeItem = myPopup->insertItem(tr("ITM_PASTE_NODE"), aCB, SLOT(pasteNode()));

    myPopup->insertItem(tr("MSG_INS_FILE"), myMain, SLOT(insertFile()));
    myPopup->insertSeparator();
  }

  QPopupMenu* aViewPopup = new QPopupMenu(viewport());
  aViewPopup->insertItem(tr("POP_FULLVIEW"), myMain, SLOT(showCanvas()));
  aViewPopup->insertItem(tr("POP_CONTROLVIEW"), myMain, SLOT(showContolFlow()));
  aViewPopup->insertItem(tr("POP_TABLEVIEW"), myMain, SLOT(showCanvasTable()));

  myPopup->insertItem(tr("POP_VIEW"), aViewPopup);
  myPopup->insertSeparator();

  QPopupMenu* aZoomPopup = new QPopupMenu(viewport());
  aZoomPopup->insertItem("200%", this, SLOT(zoomIn()));
  aZoomPopup->insertItem("100%", this, SLOT(zoomReset()));
  aZoomPopup->insertItem("50%", this, SLOT(zoomOut()));
  aZoomPopup->insertSeparator();
  //mkr: "Fit within rectangle" functionality
  aZoomPopup->insertItem(tr("POP_FITWITHINRECT"), this, SLOT(fitWithinRect()));
  aZoomPopup->insertSeparator();
  aZoomPopup->insertItem(tr("POP_FITALL"), this, SLOT(fitAll()));
  

  myPopup->insertItem(tr("POP_ZOOM"), aZoomPopup);
  myPopup->insertSeparator();

  myAddStudyItem = myPopup->insertItem(tr("MSG_ADD_STUDY"), myMain, SLOT(addDataflowToStudy()));
  myPopup->insertItem(tr(isEdit ? "MSG_CHANGE_INFO" : "MSG_INFO"), myMain, SLOT(changeInformation()));
  myPopup->insertSeparator();

  myPopup->insertItem(tr("MSG_COPY_DATAFLOW"), myMain, SLOT(copy()));
  myPopup->insertItem(tr("MSG_FILTER_NOTIFY"), myMain, SLOT(filterNotification()));

  myPopup->insertSeparator();
  myPopup->insertItem(tr("MSG_CHANGE_BACKGROUND"), this, SLOT(changeBackground()));

  viewport()->setMouseTracking(true);

  //create sketching popup menu
  mySketchPopup = new QPopupMenu(viewport());
  mySketchPopup->setCheckable(true);
  myDelPntItem = mySketchPopup->insertItem(tr("MSG_DEL_LAST_PNT"), this, SLOT(deletePoint()));
  mySketchPopup->insertItem(tr("MSG_DEL_LINK"), this, SLOT(cancelSketch()));
  myOrtoItem = mySketchPopup->insertItem(tr("MSG_ORTHO_LINE"), this, SLOT(setOrthoMode()));

  
  // add "change dataflow parameters" popup item
  myPopup->insertSeparator();
  myDSParamsItem = myPopup->insertItem( tr( "MSG_SET_GRAPHPARAMS" ), myMain, SLOT( changeDSGraphParameters() ) );

  SUPERVGraph_ViewFrame* anActiveVF = (SUPERVGraph_ViewFrame*)myMain->parent();
  if ( anActiveVF ) {
    myPopup->insertSeparator();
    myShowToolBarItem = myPopup->insertItem( tr( "MEN_SHOW_TOOLBAR" ), myMain, SLOT( onShowToolbar() ) );
  }

  // mkr : PAL8237
  connect(this, SIGNAL(objectCreated()), myMain, SLOT(onObjectCreatedDeleted()));
}
 

SUPERVGUI_CanvasView::~SUPERVGUI_CanvasView()
{
  SUPERVGUI_Clipboard::setNullClipboard();
}

void SUPERVGUI_CanvasView::contentsMousePressEvent(QMouseEvent* theEvent) 
{
  myPoint = inverseWorldMatrix().map(theEvent->pos());
  myGlobalPoint = theEvent->globalPos();
  myCurrentItem = 0;

  // compute collision rectangle
  QRect aSel(myPoint.x()-MARGIN, myPoint.y()-MARGIN, 1+2*MARGIN, 1+2*MARGIN);

  if (((theEvent->button() == Qt::MidButton) &&
       (theEvent->state() == Qt::ControlButton)) || 
      myIsPanBtnClicked) {
    myIsPanActivated = true;
    myCursor = cursor();
    setCursor(pan2Cursor);
    return;
  }

  if (((theEvent->button() == Qt::LeftButton) &&
       (theEvent->state() == Qt::ControlButton)) || 
      myIsZoomActivated) {
    myIsZoomActivated = true;
    myCursor = cursor();
    setCursor(zoom2Cursor);
    return;
  }

  if ( theEvent->button() == Qt::RightButton) {
    if (myIsLinkCreating) {
      myMain->showPopup(mySketchPopup, theEvent);
      return;
    }

    QCanvasItemList l = canvas()->collisions(aSel);
    for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
      if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Node) {
	SUPERVGUI_CanvasNodePrs* aNodePrs = (SUPERVGUI_CanvasNodePrs*) (*it);
	QObject* anObj = aNodePrs->getObject(myPoint);
	if (anObj->inherits("SUPERVGUI_CanvasNode")) {
	  myMain->showPopup(((SUPERVGUI_CanvasNode*)anObj)->getPopupMenu(viewport()), 
			    theEvent);
	  return;
	}
	else if (anObj->inherits("SUPERVGUI_CanvasPort")) {
	  myMain->showPopup(((SUPERVGUI_CanvasPort*)anObj)->getPopupMenu(viewport()), 
			    theEvent);
	  return;
	}
      }
      if (myMain->isEditable() && !myMain->getCanvas()->isControlView()) {
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
	  SUPERVGUI_CanvasPointPrs* aPrs = (SUPERVGUI_CanvasPointPrs*) (*it);
	  aPrs->getLink()->setSelectedObject(aPrs, myPoint);
	  myMain->showPopup(aPrs->getLink()->getPopupMenu(viewport()), theEvent);
	  return;
	}
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge) {
	  SUPERVGUI_CanvasEdgePrs* aPrs = (SUPERVGUI_CanvasEdgePrs*) (*it);
	  aPrs->getLink()->setSelectedObject(aPrs, myPoint);
	  myMain->showPopup(aPrs->getLink()->getPopupMenu(viewport()), theEvent);
	  return;
	}
      }
    }
	
    myPopup->setItemEnabled(myAddStudyItem, !myMain->isDataflowInStudy());
    // Paste Node functionality
    SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard(); 
    myPopup->setItemEnabled(myPasteNodeItem, aCB->isCopyNode() );
       
    myPopup->setItemEnabled( myDSParamsItem, isHavingStreamPort()/*myMain->getDataflow()->IsStreamGraph()*/ );

    myPopup->setItemEnabled( myShowToolBarItem, !((SUPERVGraph_ViewFrame*)myMain->parent())->getToolBar()->isVisible() );

    myMain->showPopup(myPopup, theEvent);
    return;
  }

  if (theEvent->button() == Qt::LeftButton) {
    if (!myIsFitWRActivated) {//not moving items if fit within rectangle 
                              //functionality is enable
      QCanvasItemList l = canvas()->collisions(myPoint);
      if (myIsLinkCreating) {
	for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
	  if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Node) {
	    SUPERVGUI_CanvasNodePrs* aNodePrs = (SUPERVGUI_CanvasNodePrs*) (*it);
	    QObject* anObj = aNodePrs->getObject(myPoint);
	    if (anObj->inherits("SUPERVGUI_CanvasPort")) {
	      endSketch((SUPERVGUI_CanvasPort*)anObj);
	      return;
	    }
	    else {
	      myCurrentItem = *it;
	      ((SUPERVGUI_CanvasNodePrs*)myCurrentItem)->setZ(2);
	      ((SUPERVGUI_CanvasNodePrs*)myCurrentItem)->setMoving(true);
	      return;
	    }
	  }
	  if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Hook) {
	    SUPERVGUI_CanvasHookPrs* aHookPrs = (SUPERVGUI_CanvasHookPrs*) (*it);
	    QObject* anObj = aHookPrs->getObject();
	    if (anObj->inherits("SUPERVGUI_CanvasPort")) {
	      endSketch((SUPERVGUI_CanvasPort*)anObj);
	      return;
	    }
	  }
	}
	if (myLinkBuilder) {
	  myLinkBuilder->addNextPoint(myPoint, mySketchPopup->isItemChecked(myOrtoItem));
	  canvas()->update();
	  mySketchPopup->setItemEnabled(myDelPntItem, true);
	  return;
	}
      }
      
//  if (myMain->isEditable()) { // allow to move nodes and link point on imported graph
      for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Node) {
	  myCurrentItem = *it;
	  ((SUPERVGUI_CanvasNodePrs*)myCurrentItem)->setZ(2);
	  ((SUPERVGUI_CanvasNodePrs*)myCurrentItem)->setMoving(true);
	  return;
	}
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
	  SUPERVGUI_CanvasPointPrs* aPrs = (SUPERVGUI_CanvasPointPrs*) (*it);
	  if (aPrs->getIndex() > 0) {
	    myCurrentItem = *it;
	    aPrs->setMoving(true);
	    return;
	  }
	}
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge) {
	  //mkr: for moving segment of link
	  SUPERVGUI_CanvasEdgePrs* aPrs = (SUPERVGUI_CanvasEdgePrs*) (*it);
	  if (aPrs->getLink()->getInputPort()->getConnectionPoint() == aPrs->startPoint()
	      ||
	    aPrs->getLink()->getInputPort()->getConnectionPoint() == aPrs->endPoint()
	      ||
	      aPrs->getLink()->getOutputPort()->getConnectionPoint() == aPrs->startPoint()
	      ||
	      aPrs->getLink()->getOutputPort()->getConnectionPoint() == aPrs->endPoint()) {
	    return;
	  }
	  myCurrentItem = *it;
	  aPrs->setMoving(true);
	  return;
	}
      }
//  }
    } 
  }
}

bool busy = false;

void SUPERVGUI_CanvasView::contentsMouseMoveEvent(QMouseEvent* theEvent) 
{
  if (busy) return;
      
  busy = true;
  QPoint p = inverseWorldMatrix().map(theEvent->pos());
  QPoint g = theEvent->globalPos();
      
  if (myTimer->isActive()) myTimer->stop();

  if (myIsLinkCreating && myLinkBuilder) {
    myLinkBuilder->setFloatPoint(p);
    canvas()->update();
  }
   
  if (myCurrentItem) {
    //    setHilighted(0);
    if (myCurrentItem->x() && myCurrentItem->y()) {
      double cx = myCurrentItem->x() - myPoint.x();
      double cy = myCurrentItem->y() - myPoint.y();
      
      if (p.x()+cx < 0) p.setX(-(int)cx);
      if (p.y()+cy < 0) p.setY(-(int)cy);
    }
    myCurrentItem->moveBy(p.x() - myPoint.x(), 
			  p.y() - myPoint.y());
    myPoint = p;
    canvas()->update();
	
    // scroll contents if mouse is outside
    QRect r(contentsX(), contentsY(), visibleWidth(), visibleHeight());
    if (!r.contains(theEvent->pos())) {
      int dx = 0, dy = 0;
      if (theEvent->pos().x() < r.left()) dx = theEvent->pos().x() - r.left();
      if (theEvent->pos().x() > r.right()) dx = theEvent->pos().x() - r.right();
      if (theEvent->pos().y() < r.top()) dy = theEvent->pos().y() - r.top();
      if (theEvent->pos().y() > r.bottom()) dy = theEvent->pos().y() - r.bottom();
      scrollBy(dx, dy);
      // start timer to scroll in silent mode
      myDX = dx; myDY = dy;
      myTimer->start(100);
    }
    busy = false;
    return;
  }

  if (myIsPanActivated) {
    setHilighted(0);
    scrollBy(myGlobalPoint.x() - g.x(),
	     myGlobalPoint.y() - g.y());
    myGlobalPoint = g;
    busy = false;
    return;
  }
      
  if (myIsZoomActivated) {
    setHilighted(0);
    double dx = g.x() - myGlobalPoint.x();
    double s = 1. + fabs(dx)/10.;
    if (dx < 0) s = 1./s;
    
    QWMatrix m = worldMatrix();
    m.scale(s, s);
    setWorldMatrix(m);
	
    myGlobalPoint = g;
    busy = false;
    return;
  }

  //mkr: "Fit within rectangle" functionality
  if (myIsFitWRActivated) {
    int aLX, aTY; //left x and top y
    if (myPoint.x() < p.x()) aLX = myPoint.x();
    else aLX = p.x();
    if (myPoint.y() < p.y()) aTY = myPoint.y();
    else aTY = p.y();
    QRect aRect(aLX, aTY, abs(myPoint.x()-p.x()), abs(myPoint.y()-p.y()));
    QCanvasRectangle* aRect1 = new QCanvasRectangle(aRect, canvas());

    //hide old selected rectangle
    if (mySelectedRect)
      mySelectedRect->hide();
    //draw new selected rectangle
    QPen pen(Qt::SolidLine);
    pen.setWidth(1);
    aRect1->setPen(pen);
    aRect1->setZ(3);
    aRect1->show();

    mySelectedRect = aRect1;
    canvas()->update();
  }
  
  if (!myIsLinkCreating && myMain->isEditable() &&
      !myMain->getCanvas()->isControlView()) {
    // compute collision rectangle
    QRect aSel(p.x()-MARGIN, p.y()-MARGIN, 1+2*MARGIN, 1+2*MARGIN);
    QCanvasItemList l = canvas()->collisions(aSel);
    for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
      if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
	SUPERVGUI_CanvasPointPrs* aPrs = (SUPERVGUI_CanvasPointPrs*) (*it);
	setHilighted(aPrs->getLink());
	busy = false;
	return;
      }
      if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge) {
	SUPERVGUI_CanvasEdgePrs* aPrs = (SUPERVGUI_CanvasEdgePrs*) (*it);
	setHilighted(aPrs->getLink());
	busy = false;
	return;
      }
    }
    setHilighted(0);
  }

  // QToolTip for title and label for SUPERVGUI_CanvasNode
  SUPERVGUI_ToolTip* aTT = new SUPERVGUI_ToolTip(this);
  aTT->maybeTip(theEvent->pos());

  busy = false;
}

void SUPERVGUI_CanvasView::contentsMouseReleaseEvent(QMouseEvent* theEvent) 
{
  if (myTimer->isActive()) myTimer->stop();

  if (myCurrentItem) {
    if (myCurrentItem->rtti() == SUPERVGUI_Canvas::Rtti_Node) {
      ((SUPERVGUI_CanvasNodePrs*)myCurrentItem)->setZ(0);
      ((SUPERVGUI_CanvasNodePrs*)myCurrentItem)->setMoving(false);
      canvas()->update();
    }
    if (myCurrentItem->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
      ((SUPERVGUI_CanvasPointPrs*)myCurrentItem)->setMoving(false);
    }
  }
  myCurrentItem = 0;

  if (myIsPanActivated) {
    myIsPanActivated = false;
    myIsPanBtnClicked = false;
    setCursor(myCursor);
  }

  if (myIsZoomActivated) {
    myIsZoomActivated = false;
    setCursor(myCursor);
  }

  //mkr: "Fit within rectangle" functionality
  if (myIsFitWRActivated) {
    if (mySelectedRect) {
      mySelectedRect->hide();
      mySelectedRect = 0;
      canvas()->update();
    }

    //myPoint is the start point for selecting rectangle now
    QPoint anEndPoint = inverseWorldMatrix().map(theEvent->pos());
    int aLX, aTY; //left x and top y
    if (myPoint.x() < anEndPoint.x()) aLX = myPoint.x();
    else aLX = anEndPoint.x();
    if (myPoint.y() < anEndPoint.y()) aTY = myPoint.y();
    else aTY = anEndPoint.y();

    //calculate width and height for new view and new zoom factor
    double aXzoom = ((double)visibleWidth())/((double)(abs(myPoint.x()-anEndPoint.x())));
    double aYzoom = ((double)visibleHeight())/((double)(abs(myPoint.y()-anEndPoint.y())));
    if (aXzoom > aYzoom) aXzoom = aYzoom;
    
    QWMatrix m;
    m.scale(aXzoom, aXzoom);
    setWorldMatrix(m);
    setContentsPos((int)(aLX*aXzoom), (int)(aTY*aYzoom));

    canvas()->update();
    
    myIsFitWRActivated = false;
    viewport()->setMouseTracking(true);
    setCursor(myCursor);
  }
}

void SUPERVGUI_CanvasView::contentsMouseDoubleClickEvent(QMouseEvent* theEvent)
{
  QPoint p = inverseWorldMatrix().map(theEvent->pos());

  // compute collision rectangle
  //QRect aSel(p.x()-MARGIN, p.y()-MARGIN, 1+2*MARGIN, 1+2*MARGIN);

  if (theEvent->button() == Qt::LeftButton) {
    QCanvasItemList l = canvas()->collisions(p);
    for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
      if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_Node) {
	SUPERVGUI_CanvasNodePrs* aNodePrs = (SUPERVGUI_CanvasNodePrs*) (*it);
	SUPERVGUI_CanvasNode* aNode = aNodePrs->getNode();
	if (aNode->getEngine()->IsMacro() && !myIsLinkCreating) {
	  myMain->openSubGraph(aNode->getEngine(), true);
	  return;
	}
      }
    }
  }
}

void SUPERVGUI_CanvasView::onDestroyed(QObject* theObject)
{
  if (theObject == myHilighted)
    myHilighted = 0;
}

void SUPERVGUI_CanvasView::setHilighted(SUPERVGUI_CanvasLink* theLink)
{
  if (theLink == myHilighted) return;
  if (myHilighted) {
    disconnect(myHilighted, SIGNAL(destroyed(QObject*)), this, SLOT(onDestroyed(QObject*)));
    myHilighted->setHilighted(false);
    myHilighted = 0;
  }
  if (theLink) {
    myHilighted = theLink;
    myHilighted->setHilighted(true);
    connect(myHilighted, SIGNAL(destroyed(QObject*)), this, SLOT(onDestroyed(QObject*)));
  }
}

void SUPERVGUI_CanvasView::onTimeout() 
{
  if (myCurrentItem) {
    scrollBy(myDX, myDY);

    double cx, cy;
    inverseWorldMatrix().map((double)myDX, (double)myDY, &cx, &cy);
    if (myCurrentItem->x()+cx < 0) cx = -myCurrentItem->x();
    if (myCurrentItem->y()+cy < 0) cy = -myCurrentItem->y();
    myCurrentItem->moveBy(cx, cy);
    myPoint.setX(myPoint.x()+(int)cx);
    myPoint.setY(myPoint.y()+(int)cy);
    canvas()->update();
  }
}

void SUPERVGUI_CanvasView::changeBackground()
{
  QColor aColor = QColorDialog::getColor(canvas()->backgroundColor(), this );
  if ( aColor.isValid() ) {
    // change background color for canvas view
    canvas()->setBackgroundColor(aColor);
    setPaletteBackgroundColor(aColor.light(120));
    // change background color for array view
    getMain()->getArrayView()->canvas()->setBackgroundColor(aColor);
    getMain()->getArrayView()->setPaletteBackgroundColor(aColor.light(120));

    // mkr : IPAL10825 -->
    SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
    if ( !aSupMod ) {
      MESSAGE("NULL Supervision module!");
      return;
    }
    aSupMod->setIVFBackgroundColor(aColor); 
    // <--
  }
}

void SUPERVGUI_CanvasView::ActivatePanning()
{
  myIsPanBtnClicked = true;
}

void SUPERVGUI_CanvasView::ResetView()
{
  setContentsPos(0,0);
  QWMatrix m;
  setWorldMatrix(m);
}

void SUPERVGUI_CanvasView::startSketch(SUPERVGUI_CanvasPort* thePort)
{
  if (myIsLinkCreating) return;

  myIsLinkCreating = true;
  myLinkBuilder = new SUPERVGUI_CanvasLinkBuilder(canvas(), myMain, thePort);
  mySketchPopup->setItemEnabled(myDelPntItem, false);
}

void SUPERVGUI_CanvasView::endSketch(SUPERVGUI_CanvasPort* thePort)
{
  if (!myIsLinkCreating) return;

  if (myLinkBuilder && myLinkBuilder->canCreateEngine(thePort)) {
    bool input = thePort->getEngine()->IsInput();
    SUPERV_Link aLinkEngine;
    if (thePort->getEngine()->Kind() == SUPERV::DataStreamParameter) {
      SUPERVGUI_CanvasStreamPortIn* aInPort = (SUPERVGUI_CanvasStreamPortIn*) 
	(input ? thePort : myLinkBuilder->getStartPort());
      SUPERVGUI_CanvasStreamPortOut* aOutPort = (SUPERVGUI_CanvasStreamPortOut*) 
	(input ? myLinkBuilder->getStartPort() : thePort);
//       aLinkEngine = myMain->getDataflow()->StreamLink(aOutPort->getStreamEngine(), 
// 						      aInPort->getStreamEngine());
      if (myMain->getDataflow()->IsStreamGraph()) {
	SUPERV_StreamGraph aSGraph = myMain->getDataflow()->ToStreamGraph();
	if (!SUPERV_isNull(aSGraph))
	  aLinkEngine = aSGraph->StreamLink(aOutPort->getStreamEngine(), 
					    aInPort->getStreamEngine());
      }
    }
    else {
      SUPERVGUI_CanvasPort* aInPort = (input ? thePort : myLinkBuilder->getStartPort());
      SUPERVGUI_CanvasPort* aOutPort = (input ? myLinkBuilder->getStartPort() : thePort);
      aLinkEngine = myMain->getDataflow()->Link(aOutPort->getEngine(), aInPort->getEngine());
    }
    if (SUPERV_isNull(aLinkEngine)) 
      return;    

    // here, in fact, aLinkEngine may NOT be a newly created link.  If a link already existed between the
    // the 2 given ports - it will be return (NOT created again).
    // this should be checked and new presentation should NOT be created for existing link.
    // Solution 1: NOT to allow creation of a link if it already exists between the ports in 
    //             myLinkBuilder->canCreateEngine()
    // Solution 2: check here if aLinkEngine is "new" or "old"
    // Implement 2nd solution, because canCreateEngine() checks for types of ports, etc.. - it's another thing
    // THE CHECK:
    QObjectList* canvasLinks = canvas()->queryList("SUPERVGUI_CanvasLink");
    SUPERVGUI_CanvasLink* canvasLink = 0;
    QObjectListIt it(*canvasLinks);
    bool prsAlreadyExists = false;
    while ( (canvasLink=(SUPERVGUI_CanvasLink*)it.current()) ) {
      ++it;
      SUPERV_Link existingLinkWithPrs = canvasLink->getEngine();
      if ( !SUPERV_isNull( existingLinkWithPrs ) ) {
	if ( existingLinkWithPrs->IsEqual( aLinkEngine ) ) {
	  prsAlreadyExists = true;
	  break;
	}	
      }
    }
    delete canvasLinks;
    if ( prsAlreadyExists ) { // aLinkEngine is already bound with a SUPERVGUI_CanvasLink object
      //return;  // -> if return here, than the old CanvasLink is kept

      // we want to delete old and create a new CanvasLink for this Link
      delete canvasLink;
      canvasLink = 0;
      // clear old corrdinates in Engine link
      for ( int i = 1; i <= aLinkEngine->CoordsSize(); i++ )
	aLinkEngine->RemoveCoord( i );
      // now we are ready to set coords for a link and create a new CanvasLink presentation that will use them.
    }

    myLinkBuilder->setCoords(aLinkEngine.in());

    delete myLinkBuilder;
    myLinkBuilder = 0;

    SUPERVGUI_CanvasLink* aLink = new SUPERVGUI_CanvasLink(canvas(), myMain, aLinkEngine);
    aLink->show();

    emit objectCreated(); // mkr : PAL8237

    canvas()->update();
    myIsLinkCreating = false;

    // asv : 13.12.04 : introducing a check for ports' types compatibility (Bugs and Improvements p.1.16, PAL7380)
    if ( !aLinkEngine->IsValid() ) { 
      const int id = SUIT_MessageBox::warn2( this, tr( "TLT_INVALID_LINK" ), 
					     tr( "MSG_INVALID_LINK" ) + QString(" : ") + QString(myMain->getDataflow()->Messages()), 
					     tr( "Keep" ), tr( "Remove" ), 0, 1, 0 );
      if ( id == 1 ) { // "Remove" was selected in Message Box
	aLink->remove(); // the new link did not live long...
      }
    }
  }
}

void SUPERVGUI_CanvasView::cancelSketch()
{
  if (myLinkBuilder) {
    delete myLinkBuilder;
    myLinkBuilder = 0;
    canvas()->update();
  }
  myIsLinkCreating = false;
}

void SUPERVGUI_CanvasView::deletePoint()
{
  if (myIsLinkCreating && myLinkBuilder) {
    myLinkBuilder->removeLastPoint();
    canvas()->update();
    mySketchPopup->setItemEnabled(myDelPntItem, myLinkBuilder->getPointCount());
  }
}

void SUPERVGUI_CanvasView::setOrthoMode()
{
  bool aIsOrtho = !mySketchPopup->isItemChecked(myOrtoItem);
  mySketchPopup->setItemChecked(myOrtoItem, aIsOrtho);
}

void SUPERVGUI_CanvasView::zoomIn() 
{
  QWMatrix m;
  m.scale(2.0, 2.0);
  setWorldMatrix(m);
  canvas()->update();
}

void SUPERVGUI_CanvasView::zoomReset() 
{
  QWMatrix m;
  m.scale(1.0, 1.0);
  setWorldMatrix(m);
  canvas()->update();
}

void SUPERVGUI_CanvasView::zoomOut() 
{
  QWMatrix m;
  m.scale(0.5, 0.5);
  setWorldMatrix(m);
  canvas()->update();
}

void SUPERVGUI_CanvasView::fitAll() 
{
  int w = 0, h = 0;
  QCanvasItemList l = canvas()->allItems();
  for (QCanvasItemList::Iterator it = l.begin(); it != l.end(); ++it) {
    QRect r = (*it)->boundingRect();
    if (w < r.right()) w = r.right();
    if (h < r.bottom()) h = r.bottom();
  }
  w += GRAPH_MARGIN; h += GRAPH_MARGIN;
  double s = ((double)visibleWidth())/((double)w);
  double s1 = ((double)visibleHeight())/((double)h);
  if (s > s1) s = s1;

  setContentsPos(0,0);
  QWMatrix m;
  m.scale(s, s);
  setWorldMatrix(m);
  canvas()->update();
}

void SUPERVGUI_CanvasView::fitWithinRect() 
{
  //mkr: "Fit within rectangle" functionality
  myIsFitWRActivated = true;
  viewport()->setMouseTracking(false);
  myCursor = cursor();
  setCursor(handCursor);
}

bool SUPERVGUI_CanvasView::isHavingStreamPort() const
{
  SUPERV::ListOfNodes* aNodesList = myMain->getDataflow()->Nodes();

  //Computing Nodes
  for ( int i = 0 ; i < (int) aNodesList->CNodes.length() ; i++ ) {
    SUPERV::ListOfStreamPorts aStrPortsList = *( (aNodesList->CNodes)[i]->StreamPorts() );
    if ((int) aStrPortsList.length() > 0) {
      return true;
    }
  }
   
  //FactoryNodes
  for ( int i = 0 ; i < (int) aNodesList->FNodes.length() ; i++ ) {
    SUPERV::ListOfStreamPorts aStrPortsList = *( (aNodesList->FNodes)[i]->StreamPorts() );
    if ((int) aStrPortsList.length() > 0) {
      return true;
    }
  }
	
  //InLineNodes
  for ( int i = 0 ; i < (int) aNodesList->INodes.length() ; i++ ) {
    SUPERV::ListOfStreamPorts aStrPortsList = *( (aNodesList->INodes)[i]->StreamPorts() );
    if ((int) aStrPortsList.length() > 0) {
      return true;
    }
  }
	
  //GOTONodes
  for ( int i = 0 ; i < (int) aNodesList->GNodes.length() ; i++ ) {
    SUPERV::ListOfStreamPorts aStrPortsList = *( (aNodesList->GNodes)[i]->StreamPorts() );
    if ((int) aStrPortsList.length() > 0) {
      return true;
    }
  }
	
  //LoopNodes
  for ( int i = 0 ; i < (int) aNodesList->LNodes.length() ; i++ ) {
    SUPERV::ListOfStreamPorts aStrPortsList = *( (aNodesList->LNodes)[i]->StreamPorts() );
    if ((int) aStrPortsList.length() > 0) {
     return true;
    }
  }
	
  //SwitchNodes
  for ( int i = 0 ; i < (int) aNodesList->SNodes.length() ; i++ ) {
    SUPERV::ListOfStreamPorts aStrPortsList = *( (aNodesList->SNodes)[i]->StreamPorts() );
    if ((int) aStrPortsList.length() > 0) {
      return true;
    }
  }
	
  return false;
}

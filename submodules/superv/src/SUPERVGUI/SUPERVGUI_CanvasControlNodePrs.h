//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasControlNodePrs.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasControlNodePrs_H
#define SUPERVGUI_CanvasControlNodePrs_H

#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_CanvasNodePrs.h"


class SUPERVGUI_CanvasControlNodePrs : public SUPERVGUI_CanvasNodePrs 
{
  public:
    SUPERVGUI_CanvasControlNodePrs( SUIT_ResourceMgr*, QCanvas* theCanvas, SUPERVGUI_CanvasNode* theNode);
    virtual ~SUPERVGUI_CanvasControlNodePrs() {}

    virtual void hideAll();
    virtual void showAll();

    virtual QRect getStatusRect() const;
    virtual QRect getBodyRect() const;
    virtual QRect getGateRect() const;
};

class SUPERVGUI_CanvasStartNode;
class SUPERVGUI_CanvasStartNodePrs : public SUPERVGUI_CanvasControlNodePrs 
{
  public:
    SUPERVGUI_CanvasStartNodePrs( SUIT_ResourceMgr*, QCanvas* theCanvas, SUPERVGUI_CanvasStartNode* theNode);
    virtual ~SUPERVGUI_CanvasStartNodePrs() {}
    
    QPointArray areaPoints() const;

    virtual void setState(SUPERV::GraphState theState);

  protected:
    virtual void drawFrame(QPainter& thePainter);
    virtual void drawTitleShape(QPainter& thePainter);
    virtual void drawStatusShape(QPainter& thePainter);
};

class SUPERVGUI_CanvasEndNode;
class SUPERVGUI_CanvasEndNodePrs : public SUPERVGUI_CanvasControlNodePrs 
{
  public:
    SUPERVGUI_CanvasEndNodePrs( SUIT_ResourceMgr*, QCanvas* theCanvas, SUPERVGUI_CanvasEndNode* theNode);
    virtual ~SUPERVGUI_CanvasEndNodePrs() {}
    
    QPointArray areaPoints() const;

    virtual void setState(SUPERV::GraphState theState);

  protected:
    virtual void drawFrame(QPainter& thePainter);
    virtual void drawTitleShape(QPainter& thePainter);
    virtual void drawStatusShape(QPainter& thePainter);
};

class SUPERVGUI_CanvasGotoNode;
class SUPERVGUI_CanvasGotoNodePrs : public SUPERVGUI_CanvasControlNodePrs 
{
  public:
    SUPERVGUI_CanvasGotoNodePrs( SUIT_ResourceMgr*, QCanvas* theCanvas, SUPERVGUI_CanvasGotoNode* theNode);
    virtual ~SUPERVGUI_CanvasGotoNodePrs() {}
    
    QPointArray areaPoints() const;

  protected:
    virtual void drawFrame(QPainter& thePainter);
    virtual void drawTitleShape(QPainter& thePainter);
    virtual void drawStatusShape(QPainter& thePainter);
};


class SUPERVGUI_CanvasMacroNode;
class SUPERVGUI_CanvasMacroNodePrs : public SUPERVGUI_CanvasNodePrs 
{
  public:
    SUPERVGUI_CanvasMacroNodePrs( SUIT_ResourceMgr*, QCanvas* theCanvas, SUPERVGUI_CanvasMacroNode* theNode);
    virtual ~SUPERVGUI_CanvasMacroNodePrs() {}
    
    QPointArray areaPoints() const;

  protected:
    void drawFrame(QPainter& thePainter);

};

#endif

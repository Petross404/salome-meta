//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_Canvas.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_Canvas_H
#define SUPERVGUI_Canvas_H

#include "SUPERVGUI_Def.h"
#include <qcanvas.h>

class SUPERVGUI_Main;
class SUPERVGUI_CanvasNode;
class SUPERVGUI_CanvasPort;
class SUPERVGUI_CanvasLink;
class SUIT_ResourceMgr;

class SUPERVGUI_Canvas: public QCanvas {
  Q_OBJECT

  public:

    enum {
      Rtti_Node = 10,
      Rtti_LinkPoint = 11,
      Rtti_LinkEdge = 12,
      Rtti_Hook = 13
    };

    SUPERVGUI_Canvas( SUPERVGUI_Main* m, SUIT_ResourceMgr* );
    virtual ~SUPERVGUI_Canvas();

    void merge();
    void sync();
    //void setAsFromStudy(bool theToStudy);

    void addView(QCanvasView* theView);

    void setFullView();
    void setControlView();
    bool isControlView() { return myIsControlView; }

    SUPERVGUI_CanvasNode* getNode(SUPERV::CNode_ptr theNode) const;
    SUPERVGUI_CanvasPort* getPort(SUPERV::Port_ptr thePort) const;
    SUPERVGUI_CanvasLink* getLink(SUPERV::Link_ptr theLink) const;

    QString getPortName(SUPERV::Port_ptr thePort) const;
    QString getLinkName(SUPERV::Link_ptr theLink) const;

 private:
    void updateNodes(bool withPorts);
    void updateLinks();

    SUPERVGUI_Main* myMain;
    bool myIsControlView;

};

#endif

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Main.cxx
//  Author : Francis KLOSS
//  Module : SUPERV

#include "SALOMEDSClient.hxx"
#include "SALOMEDS_Study.hxx"
#include "SALOMEDS_SObject.hxx"

#include "NOTIFICATION.hxx"
#include "SALOME_Event.hxx"
#include "SUPERVGraph_ViewFrame.h"
#include "SUPERVGraph_ViewModel.h"

#include "LogWindow.h"
#include "OB_Browser.h"
#include "OB_ListItem.h"
#include "SalomeApp_Application.h"
#include "SalomeApp_ImportOperation.h"
#include "SalomeApp_Study.h"
#include "SUIT_FileDlg.h"
#include "SUIT_MessageBox.h"
#include "SUIT_Operation.h"
#include "SUIT_Session.h"
#include "SUIT_Study.h"
#include "QtxDblSpinBox.h"

#include "SUPERVGUI.h"
#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI_Notification.h"
#include "SUPERVGUI_Information.h"
#include "SUPERVGUI_CanvasControlNode.h"
#include "SUPERVGUI_CanvasPort.h"

#include <qvalidator.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qfile.h>
#include <qlineedit.h>
#include <qcombobox.h>

#include <boost/shared_ptr.hpp>
using namespace boost;

// mkr : PAL8150
#define SYNCPUTINSTUDY(NodesName)  \
    for ( int n = 0; n < nodes->NodesName.length(); n++ ) { \
      SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->NodesName[n]); \
      SUPERV_Ports aPortList = aCNode->Ports(); \
      for (int p = 0; p < aPortList->length(); p++) { \
        SUPERV::Port_ptr aPort = aPortList[p].in(); \
        if ( !aPort->IsInput() && !aPort->IsGate() ) { \
          bool aInStudy = dynamic_cast<SUPERVGUI_CanvasPortOut*>( aMain->getCanvas()->getPort(aPort) )->isInStudy(); \
          dynamic_cast<SUPERVGUI_CanvasPortOut*>( getCanvas()->getPort( getDataflow()->Node(aCNode->Name())->GetOutPort(aPort->Name()) ) )->setStudyState(aInStudy); \
        } \
      } \
    }

SUPERVGUI_Main::SUPERVGUI_Main( SUPERVGraph_ViewFrame* theParent, 
				SUIT_Desktop* theDesktop, SUPERV_Graph theDataFlow )
     : SUPERVGraph_View(theParent),
       myLogged( false ),
       myFiltered( false ),
       myLogFileName( QString::null ),
       myLogFile( NULL ),
       myWarning( false ),
       myStep( false ),
       myTrace( false ),
       myVerbose( false )
  //myExecuted( false )
{
  Trace("SUPERVGUI_Main::SUPERVGUI_Main (copy)");
  setFocusPolicy( StrongFocus );  // mkr : IPAL11388
  theParent->setViewWidget(this);
  dataflow = theDataFlow;
  if (SUPERV_isNull(dataflow)) {
    QMessageBox::warning(0, tr("ERROR"), tr("MSG_CANT_COPY"));
    close();
  } else {
    init(theDesktop);
  }

  syncPutInStudy(); // mkr : PAL8150

  // mkr : IPAL11362
  connect(this, SIGNAL(EventToSync()), this, SLOT(onObjectCreatedDeleted()));
}

void SUPERVGUI_Main::init(SUIT_Desktop* theDesktop) {
  Trace("SUPERVGUI_Main::init");
  SUIT_Application* anActApp = SUIT_Session::session()->activeApplication();

  if (theDesktop) 
    myNService = dynamic_cast<SalomeApp_Application*>( anActApp )->namingService();
  myHashCode = "New";
  myCopyNum = 0;
  choosing  = false;
  myIsLocked = false;

  myThread = new SUPERVGUI_Thread();
  myThread->setMain(this);

  myCurrentView = CANVAS;
  myIsFromStudy = false;
  myLastGraph = 0;
  study = anActApp->activeStudy();

  //mkr: Supervision module already connected to the 
  //active study. So, we have to commented this code!
  /*_PTR(Study) studyDoc = (( SalomeApp_Study* )study)->studyDS();
  bool aLocked = studyDoc->GetProperties()->IsLocked();
  _PTR(StudyBuilder) builder ( studyDoc->NewBuilder() );
  _PTR(SComponent) father = studyDoc->FindComponent(STUDY_SUPERVISION);
  if ( !father ) {
    SUIT_Operation* op = new SalomeApp_ImportOperation( study->application() );
    op->start();
    if (aLocked) studyDoc->GetProperties()->SetLocked(false);
    father = builder->NewComponent(STUDY_SUPERVISION);
    _PTR(GenericAttribute) anAttr = builder->FindOrCreateAttribute(father, "AttributeName");
    _PTR(AttributeName) aName ( anAttr );
    aName->SetValue( dynamic_cast<CAM_Application*>( study->application() )->moduleTitle( "SUPERV" ).latin1() );
    
    anAttr = builder->FindOrCreateAttribute(father, "AttributePixMap");
    _PTR(AttributePixMap) aPixmap ( anAttr );
    aPixmap->SetPixMap( "ICON_OBJBROWSER_Supervision" );
    
    SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
    if ( !aSupMod ) {
      MESSAGE("NULL Supervision module!");
      return;
    }

    builder->DefineComponentInstance(father, SalomeApp_Application::orb()->object_to_string(aSupMod->getEngine()));
                                             //dynamic_cast<SALOMEDS_Study*>( studyDoc )->ConvertObjectToIOR(aSupMod->getEngine()));
    if (aLocked) studyDoc->GetProperties()->SetLocked(true);
    op->commit();
  };
  */

  SalomeApp_Application* app = dynamic_cast<SalomeApp_Application*>( study->application() );

  objectBrowser = app->objectBrowser();

  myArray = new SUPERVGUI_CanvasArray( this, resMgr() );
  myArrayView = new SUPERVGUI_ArrayView( myArray, this );

  myCanvas = new SUPERVGUI_Canvas( this, resMgr() );
  myCanvasView = new SUPERVGUI_CanvasView(myCanvas, this);

  message = app->logWindow();
  notification = new NOTIFICATION_Consumer();

  QBoxLayout * layout = new QVBoxLayout(this);
  layout->setMargin(0);
  layout->setSpacing(0);
  layout->addWidget(myCanvasView);
  layout->addWidget(myArrayView);

  if (myCurrentView == CANVAS || myCurrentView == CONTROLFLOW) {
    myCanvas->merge();
  }

  sync();
  show();
  if ( myLogged && !myLogFileName.isEmpty() /*&& QFile::exists( myLogFileName )*/ ) { // mkr : PAL7037
    myLogFile = fopen( myLogFileName.latin1(), "a" );
    if (  myLogFile == NULL )
      myLogged = false;
  }
  myTimer = new QTimer( this );
  connect( myTimer, SIGNAL(timeout()), this, SLOT(checkExecution()) );
}

SUPERVGUI_Main::~SUPERVGUI_Main() {
  Trace("SUPERVGUI_Main::~SUPERVGUI_Main");

  // close all opened SubGraphs 
  QMap<QString, SUIT_ViewWindow*/*QAD_StudyFrame**/>::iterator it;
  for (it = mySubGraphs.begin(); it != mySubGraphs.end(); ++it) {
    it.data()->removeEventFilter(this);
    it.data()->disconnect();
    //it.data()->close();

    //QAD_Study* aStudy = it.data()->getStudy();
    //aStudy->removeStudyFrame(it.data());
    //remove view from internal view manager's list of views
    //it.data()->getViewManager()->removeView(it.data());
    SUIT_ViewManager* aVM = it.data()->getViewManager();
    STD_Application* anApp = dynamic_cast<STD_Application*>( SUIT_Session::session()->activeApplication() );
    if ( anApp ) anApp->removeViewManager(aVM);
    //if ( aVM ) delete aVM; // mkr : IPAL11547
    
  }
  mySubGraphs.clear();
  mySubGraphMap.clear();
  /*
  QAD_StudyFrame* aSubGraph;
  for (aSubGraph = mySubGraphs.first(); aSubGraph; aSubGraph = mySubGraphs.next()) {
    aSubGraph->removeEventFilter(this);
    aSubGraph->close();
  }
  */

  if ( myLogFile != NULL) {
    fclose( myLogFile );
  }
  
  //  delete notification; // kloss : nota bene : quand un datalow est detruit : verifier que les canaux de notification sont aussi detruit
  notification->_remove_ref();  // kloss : nota bene : quand un datalow est detruit : verifier que les canaux de notification sont aussi detruit

  delete myCanvas;
  delete myArray;
}

SUIT_ResourceMgr* SUPERVGUI_Main::resMgr() const
{
  SalomeApp_Application* app = dynamic_cast<SalomeApp_Application*>( study->application() );
  return app ? app->resourceMgr() : 0;
}

void SUPERVGUI_Main::filterNotification() {
  Trace("SUPERVGUI_Main::filterNotification");
  SUPERVGUI_Notification* dlg = new SUPERVGUI_Notification(this);
  dlg->setFiltered( myFiltered );
  dlg->setLogged( myLogged, myLogFileName );
  dlg->setWarning( myWarning );
  dlg->setStep( myStep );
  dlg->setTrace( myTrace );
  dlg->setVerbose( myVerbose );
  if ( dlg->exec() == QDialog::Accepted ) {
    myLogged      = dlg->getLogged();
    myLogFileName = dlg->getLogFile();
    myFiltered    = dlg->getFiltered();
    myWarning     = dlg->getWarning();
    myStep        = dlg->getStep();
    myTrace       = dlg->getTrace();
    myVerbose     = dlg->getVerbose();
    delete dlg;
    if ( myLogFile != NULL) {
      fclose( myLogFile );
    }
    myLogFile = NULL;
    if ( myLogged && !myLogFileName.isEmpty() /*&& QFile::exists( myLogFileName )*/ ) { // mkr : PAL7037
      myLogFile = fopen( myLogFileName.latin1(), "a" );
      if (  myLogFile == NULL ) {
	myLogged = false;
	QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr( "ERR_CANT_OPEN_LOG_FILE" ) );
      }
    }
  }
}

void SUPERVGUI_Main::changeDSGraphParameters() {
  SUPERVGUI_DSGraphParameters* aDlg = new SUPERVGUI_DSGraphParameters(dataflow, dataflow->IsReadOnly());
  if (aDlg->exec() )
    sync();
  delete aDlg;
}

void SUPERVGUI_Main::syncAsync() {
  Trace("SUPERVGUI_Main::syncAsync");
  QTimer::singleShot(1, this, SLOT(sync()));
}


/**
 * Called by thread when dataflow is executing
 */
void SUPERVGUI_Main::execute( char *  theNodeName, SUPERV::GraphState theNodeState ) {
  if (myCurrentView == CANVAS || myCurrentView == CONTROLFLOW) {
    SUPERVGUI_CanvasNode* aNode = (SUPERVGUI_CanvasNode*) myCanvas->child(theNodeName, "SUPERVGUI_CanvasNode");
    if ( aNode ) {
      aNode->sync();
      emit EventToSync(); // mkr : IPAL11362
    }
  }
  else if (myCurrentView == CANVASTABLE) {
    SUPERVGUI_CanvasCellNode* aNode = (SUPERVGUI_CanvasCellNode*) myArray->child(theNodeName, "SUPERVGUI_CanvasCellNode");
    if (aNode) {
      aNode->sync();
      emit EventToSync(); // mkr : IPAL11362
    }
  }

  // asv : 26.01.05 : Bug PAL7164 : puting out-value to study if the "put_to_Study" flag is set on a 
  // CanvasPort the functionality was moved from SUPERVGUI_CanvasPortOut::sync() method please, see 
  // comment in that method for details.
  if ( theNodeState == SUPERV::DoneState ) {
    SUPERVGUI_CanvasNode* aNode = (SUPERVGUI_CanvasNode*) myCanvas->child(theNodeName, "SUPERVGUI_CanvasNode");
    if ( aNode ) {
      //cout << " *** theNode " << theNodeName << " received DONE_STATE *** " << endl;
      SUPERVGUI_CanvasPortOut* aPortOut;
      QObjectList* aPortList = aNode->queryList("SUPERVGUI_CanvasPortOut");
      QObjectListIt aPortIt(*aPortList);
      while ((aPortOut=(SUPERVGUI_CanvasPortOut*)aPortIt.current()) != 0) {
	++aPortIt;
	if ( aPortOut->isInStudy() && aPortOut->getEngine()->State() == SUPERV::ReadyState ) {
	  //cout << " *** " << theNodeName << "[" << aPortOut->name() << "]--> goes into study *** " << endl;
	  putDataStudy( aPortOut->getEngine(), STUDY_PORT_OUT );
	}
      }
      delete aPortList;      
    } 
    //else //normal case if Node is not found is when node is a graph!  it can receive DoneState as well!  
    //  MESSAGE( "ERROR in SUPERVGUI_Main::execute() : CanvasNode \"" << theNodeName << "\" NOT FOUND in myCanvas" );
  }
}


void SUPERVGUI_Main::sync() {
  Trace("SUPERVGUI_Main::sync");
  if ((SUPERV_isNull(dataflow))) 
    return;
  QString t = tr("GRAPH_TITLE");
  
  t += dataflow->Name();
  setCaption(t);

  //study->updateObjBrowser();
  //mkr: we have to update object browser only if there are some ports datas in the study
  //or if dataflow is in the study itself. It is done in the 
  //1) putDataStudy(...) method (it called when we put in study data of port
  //                             or if port is in study and execution is finished)
  //2) addDataflowToStudy() method (it is called as a slot when we add in study dataflow itself).
  //So, we commented updateObjBrowser function here!
  //SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  //if ( aSupMod ) aSupMod->updateObjBrowser();
  //else MESSAGE("NULL Supervision module!");

  // update "Save" icon and menu state, if dataflow is published
  _PTR(Study) aStudy = ((SalomeApp_Study*)study)->studyDS();
  _PTR(SObject)  aSO = aStudy->FindObjectIOR(dataflow->getIOR());
  if (aSO) SUPERVGUI_Main::setModifiedFlag();  

  if (myCurrentView == CANVASTABLE) {
    myArray->sync();
    myArray->update();
  } else {
    myCanvas->sync();
    myCanvas->update();
  }
}

void SUPERVGUI_Main::showCanvasTable() {
  if (myCurrentView == CANVASTABLE) 
    return;

  if (myArray->create()) {
    myCanvasView->hide();
    myArrayView->show();
    myCurrentView = CANVASTABLE;
  }
  sync();
}

void SUPERVGUI_Main::showContolFlow() {
  bool merge = false;
  if (myCurrentView == CANVASTABLE) {
    myArrayView->hide();
    myArray->destroy();
    merge = true;
  }
  myCurrentView = CONTROLFLOW;
  myCanvas->setControlView();
  if (merge) {
    myCanvas->merge();
    myCanvasView->show();
  }
}

void SUPERVGUI_Main::showCanvas() {
  if (myCurrentView == CANVAS) 
    return;

  bool merge = false;
  if (myCurrentView == CANVASTABLE) {
    myArrayView->hide();
    myArray->destroy();
    merge = true;
  }
  myCurrentView = CANVAS;
  myCanvas->setFullView();
  if (merge) {
    myCanvas->merge();
    myCanvasView->show();
  }
}

void SUPERVGUI_Main::insertFile() {
  Trace("SUPERVGUI_Main::insertFile");

  if ( !ReadyToModify() ) // null dataflow or executing, ..
    return;

  QString f = SUIT_FileDlg::getFileName(SUIT_Session::session()->activeApplication()->desktop(),
					"",
					"*.xml",
					tr("MSG_GRAPH_INSERT"),
					true);
  if ( !f.isEmpty() ) { // not Cancel, but "OK" was pressed with valid file name
      
    Editing(); // PAL6170: GUI->Engine: setting "Editing" flag, why here? -> see 7960

    if (dataflow->Import(f.latin1())) {
      if (myCurrentView == CANVASTABLE) {
	myArray->destroy();
	myArray->create();
      }
      else { // (myCurrentView == CANVAS || myCurrentView == CONTROLFLOW) {
	myCanvas->merge();
      }
      sync();
    } else {
      QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_BAD_FILE").arg(f));
    }
  }
}

void SUPERVGUI_Main::copy() {
  Trace("SUPERVGUI_Main::copy");
  if (dataflow->ThreadsMax() == 0) {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("WARNING"), tr("MSG_NOTHING_COPY"));
    return;
  }
  
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }

  SUPERVGraph_ViewFrame* aViewFrame = dynamic_cast<SUPERVGraph_ViewFrame*>( aSupMod->createGraph() );
  if( aViewFrame ) {
    SUPERV_Graph aNewGraph; //  = dataflow->StreamCopy();
    if ( dataflow->IsStreamGraph() ) {
      SUPERV_StreamGraph aSGraph = dataflow->ToStreamGraph();
      if ( SUPERV_isNull(aSGraph) ) {
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("WARNING"), tr("MSG_CANT_COPY"));
	return;
      }
      aNewGraph = aSGraph->StreamCopy();
    }
    else {
      aNewGraph = dataflow->Copy();
    }
    QString aNewName(tr("MSG_COPY_PREFIX").arg(++myCopyNum));
    aNewName += dataflow->Name();
    aNewGraph->SetName(aNewName);
    /*SUPERVGUI_Main* m = */new SUPERVGUI_Main(aViewFrame, 
					       aSupMod->application()->desktop(),//getDesktop(), 
					       aNewGraph);
    aViewFrame->show();
  }
}

void SUPERVGUI_Main::openSubGraph(SUPERV_CNode theNode, bool correct)
{
  if (theNode->IsMacro()) {
    // get SubGraph from MacroNode
    SUPERV_Graph aMacro = SUPERV::Graph::_narrow(theNode);
    SUPERV_Graph aGraph;
    if (aMacro->IsStreamMacro())
      aGraph = aMacro->StreamObjRef();
    else
      aGraph = aMacro->FlowObjRef();

    // display SubGraph
    if (SUPERV_isNull(aGraph)) {
      QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("WARNING"), tr("MSG_NOACCESS"));
      return;
    }
    else {
      QString aGraphName = aGraph->Name();
      SUIT_ViewWindow* aStudyFrame;
      if (mySubGraphs.contains(aGraphName)) {
	aStudyFrame = mySubGraphs[aGraphName];
	aStudyFrame->setActiveWindow();
	aStudyFrame->setFocus();
      }
      else {
	SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
	if ( !aSupMod ) {
	  MESSAGE("NULL Supervision module!");
	  return;
	}
	
	aStudyFrame = aSupMod->createGraph();
	if ( aStudyFrame ) {
	  SUPERVGraph_ViewFrame* aViewFrame = dynamic_cast<SUPERVGraph_ViewFrame*>( aStudyFrame );
	  if( aViewFrame ) {
	    new SUPERVGUI_Main(aViewFrame, 
			       aSupMod->application()->desktop(),//getDesktop(), 
			       aGraph);
	    // 	  connect(aStudyFrame, SIGNAL(sfStudyFrameClosing(QAD_StudyFrame*)), 
	    // 		  this,  SLOT(onSubGraphClosed(QAD_StudyFrame*)));
	    connect(aSupMod->application()->desktop(), SIGNAL(windowActivated( SUIT_ViewWindow* )), 
		    this,  SLOT(onSubGraphActivated( SUIT_ViewWindow* )));
	    aStudyFrame->installEventFilter(this);

	    mySubGraphs.insert(aGraphName, aStudyFrame);
	    mySubGraphMap.insert(aGraphName, theNode->Name());

	  }
	}
      }
      aStudyFrame->show();
      if (!correct) myLastGraph = aStudyFrame;
    }
  }
}

// mkr : IPAL11549
void SUPERVGUI_Main::destroySubGraph(QString theSubGraphName)
{
  if ( mySubGraphs.contains(theSubGraphName) ) {
    SUIT_ViewWindow* aStudyFrame = mySubGraphs[theSubGraphName];
    onSubGraphClosed(aStudyFrame);
    SUIT_ViewManager* aVM = aStudyFrame->getViewManager();
    STD_Application* anApp = dynamic_cast<STD_Application*>( SUIT_Session::session()->activeApplication() );
    if ( anApp ) anApp->removeViewManager(aVM);
  }
}

bool SUPERVGUI_Main::eventFilter( QObject* o, QEvent* e)
{
  // workaround to get close event
  if (o->inherits("SUIT_ViewWindow"/*"QAD_StudyFrame"*/) && e->type() == QEvent::Close) {
    SUIT_ViewWindow* aFrame = (SUIT_ViewWindow*) o;
    onSubGraphClosed(aFrame);
  }
  return SUPERVGraph_View::eventFilter(o, e);
}

// workaround to fix PAL6255 -> opened SubGraph is not on top
void SUPERVGUI_Main::onSubGraphActivated(SUIT_ViewWindow* theStudyFrame)
{
  if (myLastGraph) {
    SUIT_ViewWindow* aFrame = myLastGraph;
    myLastGraph = 0;
    aFrame->setActiveWindow();
    aFrame->setFocus();
  }
}

void SUPERVGUI_Main::onSubGraphClosed(SUIT_ViewWindow* theStudyFrame)
{
  if ( SUPERVGraph_ViewFrame* supervFrame = dynamic_cast<SUPERVGraph_ViewFrame*>( theStudyFrame ) ) {
    theStudyFrame->removeEventFilter(this);
    disconnect(theStudyFrame, 0, this, 0);

    SUPERVGraph_View* view = supervFrame->getViewWidget();
    SUPERVGUI_Main* aGraph = dynamic_cast<SUPERVGUI_Main*>(view);
    if (aGraph) {
      QString aGraphName = aGraph->getDataflow()->Name();
      QMap<QString, QString>::iterator it = mySubGraphMap.find(aGraphName);
      if (it != mySubGraphMap.end()) {
	QString aNodeName = it.data();
	SUPERVGUI_CanvasNode* aNode = (SUPERVGUI_CanvasNode*) 
	  myCanvas->child(aNodeName, "SUPERVGUI_CanvasNode");
	if (aNode) {
	  aNode->merge();
	  myCanvas->update();
	}
	mySubGraphMap.remove(it);
      }
      mySubGraphs.remove(aGraphName);
    }
  }
}

void SUPERVGUI_Main::onShowToolbar()
{
  SUPERVGraph_ViewFrame* aVF = (SUPERVGraph_ViewFrame*)this->parent();
  if ( aVF )
    aVF->getToolBar()->show();
}

// mkr : PAL8237 : synchronize many views of the same dataflow (objects creation/deletion)
void SUPERVGUI_Main::onObjectCreatedDeleted()
{
  if ( study ) {
    if ( LightApp_Application* app = dynamic_cast<LightApp_Application*>( study->application() ) ) {
      ViewManagerList aVMList;
      app->viewManagers( SUPERVGraph_Viewer::Type(), aVMList );
      SUIT_ViewManager* aVM;
      for ( aVM = aVMList.first(); aVM; aVM = aVMList.next() ) {
	QPtrVector<SUIT_ViewWindow> aVWList = aVM->getViews();
	for ( int i = 0; i < aVWList.count(); i++ ) {
	  SUPERVGraph_ViewFrame* aVW = dynamic_cast<SUPERVGraph_ViewFrame*>( aVWList[i] );
	  QObjectList* aMainList = aVW->queryList("SUPERVGUI_Main");
	  if ( aMainList->count() == 1 ) {
	    SUPERVGUI_Main* aMain = dynamic_cast<SUPERVGUI_Main*>( aMainList->first() );
	    if ( aMain ) {
	      if (aMain != this &&
                  !CORBA::is_nil(aMain->getDataflow()) &&
                  !QString(aMain->getDataflow()->getIOR()).compare(getDataflow()->getIOR())) // equal dataflows
		aMain->getCanvas()->merge();
            }
	  }
	}
      }

      // update "Save" icon and menu state, if dataflow is published
      //_PTR(Study) aStudy = ((SalomeApp_Study*)study)->studyDS();
      //_PTR(SObject)  aSO = aStudy->FindObjectIOR(dataflow->getIOR());
      //if (aSO) SUPERVGUI_Main::setModifiedFlag();  
    }
  }
}

// mkr : PAL8150 : synchronize many views of the same dataflow ("Put In Study"/"Not In Study" operation)
void SUPERVGUI_Main::onPutInStudy( QString* theNodePortName )
{
  if ( study ) {
    if ( STD_Application* app = dynamic_cast<STD_Application*>( study->application() ) ) {
      ViewManagerList aVMList;
      app->viewManagers( SUPERVGraph_Viewer::Type(), aVMList );
      SUIT_ViewManager* aVM;
      for ( aVM = aVMList.first(); aVM; aVM = aVMList.next() ) {
	QPtrVector<SUIT_ViewWindow> aVWList = aVM->getViews();
	for ( int i = 0; i < aVWList.count(); i++ ) {
	  SUPERVGraph_ViewFrame* aVW = dynamic_cast<SUPERVGraph_ViewFrame*>( aVWList[i] );
	  QObjectList* aMainList = aVW->queryList("SUPERVGUI_Main");
	  if ( aMainList->count() == 1 ) {
	    SUPERVGUI_Main* aMain = dynamic_cast<SUPERVGUI_Main*>( aMainList->first() );
	    if ( aMain )
	      if ( !QString(aMain->getDataflow()->getIOR()).compare(getDataflow()->getIOR()) // equal dataflows
		   && 
		   aMain != this ) { // not equal mains
		QString aNodeName = theNodePortName->left( theNodePortName->find('_') );
		QString aPortName = theNodePortName->right( theNodePortName->length() - theNodePortName->find('_') - 1 );
		bool anIsInStudy = dynamic_cast<SUPERVGUI_CanvasPortOut*>( getCanvas()->getPort( getDataflow()->Node( aNodeName )->GetOutPort( aPortName ) ) )->isInStudy();
		dynamic_cast<SUPERVGUI_CanvasPortOut*>( aMain->getCanvas()->getPort( aMain->getDataflow()->Node( aNodeName )->GetOutPort( aPortName ) ) )->setStudyState( anIsInStudy );
	      }
	  }
	}
      }
      //SUPERVGUI_Main::setModifiedFlag();
    }
  }
}

// mkr : PAL8150 : synchronize many views of the same dataflow ("Display" operation for dataflow)
void SUPERVGUI_Main::syncPutInStudy()
{
  if ( study ) {
    if ( STD_Application* app = dynamic_cast<STD_Application*>( study->application() ) ) {
      ViewManagerList aVMList;
      app->viewManagers( SUPERVGraph_Viewer::Type(), aVMList );
      SUIT_ViewManager* aVM;
      for ( aVM = aVMList.first(); aVM; aVM = aVMList.next() ) {
	QPtrVector<SUIT_ViewWindow> aVWList = aVM->getViews();
	for ( int i = 0; i < aVWList.count(); i++ ) {
	  SUPERVGraph_ViewFrame* aVW = dynamic_cast<SUPERVGraph_ViewFrame*>( aVWList[i] );
	  QObjectList* aMainList = aVW->queryList("SUPERVGUI_Main");
	  if ( aMainList->count() == 1 ) {
	    SUPERVGUI_Main* aMain = dynamic_cast<SUPERVGUI_Main*>( aMainList->first() );
	    if ( aMain )
	      if ( !CORBA::is_nil( aMain->getDataflow() )
		   &&
		   !QString(aMain->getDataflow()->getIOR()).compare(getDataflow()->getIOR()) // equal dataflows
		   && 
		   aMain != this ) { // not equal mains
	
		// check "myInStudy" field of output ports of original main (i.e. this main)
		// and synchronize this property for nodes' output ports of dublicate main (i.e. of aMain)
		SUPERV_Nodes nodes = aMain->getDataflow()->Nodes();
		
		SYNCPUTINSTUDY(CNodes);
      		SYNCPUTINSTUDY(FNodes);
		SYNCPUTINSTUDY(INodes);
		SYNCPUTINSTUDY(GNodes);
		SYNCPUTINSTUDY(LNodes);
      		SYNCPUTINSTUDY(ELNodes);
		SYNCPUTINSTUDY(SNodes);
		SYNCPUTINSTUDY(ESNodes);
		SYNCPUTINSTUDY(Graphs);

		setRunTime( aMain->getRunTime() );

		break;
	      }
	  }
	}
      }
    }
  }
}

void SUPERVGUI_Main::run( const bool andSuspend ) {
  Trace("SUPERVGUI_Main::run");
  if ( SUPERV_isNull(dataflow) ) 
    return;
  
  if ( dataflow->IsEditing() ) { // not currently Executing

    // asv 31.01.05 : fix for PAL7854, Editing moved from SUPERVGUI.cxx runDataflow(), stepByStep() to here..
    Editing(); // remove old executor, update GUI (all nodes to "No Status")

    if ( !dataflow->IsValid() ) {
      QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_NOTVALID") + QString(" : ") + QString(dataflow->Messages()));
    } 
    else if (!dataflow->IsExecutable()) {
      QString aMessage = QString(dataflow->Messages());
      if ( aMessage.isEmpty() ) // mkr : IPAL11471
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_NOTEXECUTABLE"));
      else
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_NOTEXECUTABLE") + QString(" : ") + aMessage);
    } 
    else if (myCanvasView->isAnyLinkCreating()) {
      QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CRL_NOTCOMPLETE"));
    } 
    else {
      myRunTime = QDateTime::currentDateTime();
      const bool result = andSuspend ? dataflow->Start() : dataflow->Run();
      if ( !result ) {
	// PAL12902
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_BADEXECUTE") + QString(" : ") + QString(dataflow->Messages()));
	if ( dataflow->State() == SUPERV::ErrorState ) {
	  // PAL12902
	  if ( !dataflow->IsEditing() ) kill();
	}
      } 
      else {
	//myExecuted = true; // set to true on first execution. for correct publishing in Study
	myThread->startThread(tr("MSG_GRAPH_STARTED"));
      }
    }
  } 
  else {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_RUNNING"));
  }
}


void SUPERVGUI_Main::kill() {
  Trace("SUPERVGUI_Main::kill");
  if ((SUPERV_isNull(dataflow))) 
    return;

  if (dataflow->IsEditing()) {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_NOTRUNNING"));
  } 
  else if (dataflow->Kill()) {
    // IPAL9273, 9369, 9731 : to avoid two equal messages about killing dataflow
    //                        execution in the Message Console
    //getMessage()->putMessage( tr("MSG_GRAPH_KILLED") );
    sync();
  } 
  else {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANTKILL_DF"));
  }
}

void SUPERVGUI_Main::suspendResume() {
    Trace("SUPERVGUI_Main::suspendResume");

    if ((SUPERV_isNull(dataflow))) return;

    if (dataflow->IsEditing()) {
        QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_DF_NOTRUNNING"));
    } else if (dataflow->State() == SUPERV_Suspend) {
      if (dataflow->Resume()) {
	myThread->startThread(tr("MSG_DF_RESUMED"));
      } else {
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_RESUME"));
      }
    } else {
      if (dataflow->Suspend()) {
	sync();
	getMessage()->putMessage( tr("MSG_GRAPH_SUSPENDED") );
      } else {
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_SUSPEND"));
      }
    }
}
/* asv : 15.12.04 : commented out stopRestart() in Main and CanvasNode because it's not called from anywhere,
   the comment from kloss below may be explaining it, but it's in French and I do not understand it..
   It also calls deprecated method of Engine: ReStart(). 
void SUPERVGUI_Main::stopRestart() { // kloss : a reviser et a connecter dans le popup du dataflow (pas de creation de bouton)
    Trace("SUPERVGUI_Main::stopRestart");
    if ((SUPERV_isNull(dataflow))) return;

    if (dataflow->IsEditing()) {
        QMessageBox::warning(0, tr("ERROR"),  tr("MSG_DF_NOTRUNNING"));
    } else if (dataflow->State() == SUPERV_Stop) {
        if (dataflow->ReStart()) {
            message->setMessage(tr("MSG_DF_RESTARTED"));
            sync();
        } else {
            QMessageBox::warning(QAD_Application::getDesktop(), tr("ERROR"), tr("MSG_CANT_RESTART"));
        };
    } else {
        if (dataflow->Stop()) {
            sync();
        } else {
            QMessageBox::warning(QAD_Application::getDesktop(), tr("ERROR"), tr("MSG_CANT_STOP"));
        };
    };
}
*/
void SUPERVGUI_Main::addNode() {
  Trace("SUPERVGUI_Main::addNode");
  
  if ( !ReadyToModify() ) // null dataflow or executing, ..
    return;
  
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }

  //Editing(); // PAL6170: GUI->Engine: setting "Editing" flag, commented: PAL7960
  aSupMod->getBrowser()->choose();
}

/**
 * Add Computation node
 */
void SUPERVGUI_Main::addComputeNode(SUPERV_CNode theNode) {
  switch (myCurrentView) {
  case CANVASTABLE:
    myArray->destroy();
    myArray->create();
    break;  
  case CANVAS:
  case CONTROLFLOW: 
    {
      SUPERVGUI_CanvasNode* aNode = SUPERVGUI_CanvasComputeNode::Create(resMgr(), myCanvas, this, theNode);
      aNode->move(theNode->X(), theNode->Y());
      if (myCurrentView == CONTROLFLOW) aNode->hideAll();
      aNode->show();
      myCanvas->update();
    }
    break;
  }
}

/**
 * Add GOTO node
 */
void SUPERVGUI_Main::addGOTONode(SUPERV_CNode theNode) {
  switch (myCurrentView) {
  case CANVASTABLE:
    myArray->destroy();
    myArray->create();
    break;
  case CANVAS:
  case CONTROLFLOW: 
    {
      SUPERVGUI_CanvasNode* aNode = SUPERVGUI_CanvasGotoNode::Create(resMgr(), myCanvas, this, theNode);
      aNode->move(theNode->X(), theNode->Y());
      if (myCurrentView == CONTROLFLOW) aNode->hideAll();
      aNode->show();
      myCanvas->update();
    }
    break;
  }
}

/**
 * Add Control node
 */
void SUPERVGUI_Main::addControlNode(SUPERV_CNode theStartNode, SUPERV_CNode theEndNode, bool Update)
{
  switch (myCurrentView) {
  case CANVASTABLE:
    myArray->destroy();
    myArray->create();
    break;
  case CANVAS:
  case CONTROLFLOW: 
    {
      SUPERVGUI_CanvasStartNode* aStartNode =
        SUPERVGUI_CanvasStartNode::Create(resMgr(), myCanvas, this, theStartNode);
      aStartNode->move(theStartNode->X(), theStartNode->Y());
      if (myCurrentView == CONTROLFLOW) aStartNode->hideAll();

      SUPERVGUI_CanvasEndNode* aEndNode =
        SUPERVGUI_CanvasEndNode::Create(resMgr(), myCanvas, this, theEndNode, aStartNode);
      aEndNode->move(theEndNode->X(), theEndNode->Y());
      if (myCurrentView == CONTROLFLOW) aEndNode->hideAll();

      aStartNode->show();
      aEndNode->show();
      myCanvas->update();
    }
    break;
  }
}


/**
 * Add Macro node
 */
void SUPERVGUI_Main::addMacroNode(SUPERV_CNode theNode) {
  switch (myCurrentView) {
  case CANVASTABLE:
    myArray->destroy();
    myArray->create();
    break;
  case CANVAS:
  case CONTROLFLOW: 
    {
      SUPERVGUI_CanvasNode* aNode = SUPERVGUI_CanvasMacroNode::Create(resMgr(), myCanvas, this, theNode);
      aNode->move(theNode->X(), theNode->Y());
      if (myCurrentView == CONTROLFLOW) aNode->hideAll();
      aNode->show();
      myCanvas->update();
    }
    break;
  }
}

SUPERVGUI_CanvasArray*  SUPERVGUI_Main::getCanvasArray() {
  Trace("SUPERVGUI_Main::getCanvasArray");
  return(myArray);
}

SUPERVGUI_ArrayView* SUPERVGUI_Main::getArrayView() {
  Trace("SUPERVGUI_Main::getArrayView");
  return(myArrayView);
}

SUPERVGUI_Canvas* SUPERVGUI_Main::getCanvas() {
  Trace("SUPERVGUI_Main::getCanvas");
  return(myCanvas);
}

SUPERVGUI_CanvasView* SUPERVGUI_Main::getCanvasView() {
    Trace("SUPERVGUI_Main::getCanvasView");
    return(myCanvasView);
}

SUPERV_Graph SUPERVGUI_Main::getDataflow() {
  Trace("SUPERVGUI_Main::getDataflow");
  return(dataflow);
}

LogWindow* SUPERVGUI_Main::getMessage() {
  Trace("SUPERVGUI_Main::getMessage");
  return(message);
}

SUIT_Study* SUPERVGUI_Main::getStudy() {
  Trace("SUPERVGUI_Main::getStudy");
  return(study);
}

bool SUPERVGUI_Main::isArrayShown() {
  Trace("SUPERVGUI_Main::isArrayShown");
  return(myCurrentView == CANVASTABLE);  
}

void SUPERVGUI_Main::showPopup(QPopupMenu* p, QMouseEvent* e) {
  Trace("SUPERVGUI_Main::showPopup");
  // To check is Supervision active?
  if (myIsLocked) return;
  //if (dataflow->IsExecuting()) return;

  CAM_Application* anApp = ( CAM_Application* )(SUIT_Session::session()->activeApplication());
  if ( !anApp->activeModule() ) return;
  if ( anApp->activeModule()->moduleName().compare( anApp->moduleTitle( "SUPERV" ) ) !=0 ) return;

  if (e->button() == RightButton) {
    p->exec(e->globalPos());
  }
}

void SUPERVGUI_Main::changeInformation() {
  SUPERVGUI_Information* aDlg = new SUPERVGUI_Information(SUPERV::CNode::_narrow(dataflow), dataflow->IsReadOnly());
  if (aDlg->exec() )
    sync();
  delete aDlg;
}

void SUPERVGUI_Main::chooseData(QListViewItem* item) {
    Trace("SUPERVGUI_Main::chooseData");
    if (choosing) {
      QString id = ((OB_ListItem*)item)->text(2);// <=> get entry
      if (!id.isEmpty()) {
	_PTR(SObject) object ( (( SalomeApp_Study* )study)->studyDS()->FindObjectID(id.latin1()) );
	_PTR(GenericAttribute) anAttr;
	if (object->FindAttribute(anAttr, "AttributeIOR")) {
	  _PTR(AttributeIOR) anIOR ( anAttr );
	  
	  // asv : 13.12.04 : commented out use of portIn field, but it seems that it 
	  // should be replaced with some analogious code... selection of IOR was done -
	  // put its value into port.
	  //portIn->setValue(ior);
	  
	  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
	  if ( !aSupMod ) {
	    MESSAGE("NULL Supervision module!");
	    return;
	  }

	  // stop selection
	  choosing = false;
	  setCursor(aSupMod->getCursor());
	  objectBrowser->setCursor(aSupMod->getCursor());
	  aSupMod->putInfo("");
	}
      }
    }
}

_PTR(SObject) SearchOrCreateSOWithName( _PTR(Study) const theStudy,
					_PTR(StudyBuilder) const theBuilder,
					_PTR(SObject) const theSO,
					const char* theName,
					bool* theDoneSomething ) {
  _PTR(SObject) aResult;
  _PTR(AttributeName) aName;
  _PTR(GenericAttribute) anAttr;
  _PTR(ChildIterator) anIterator ( theStudy->NewChildIterator(theSO) );
  for (; anIterator->More(); anIterator->Next()) {
    if (anIterator->Value()->FindAttribute(anAttr, "AttributeName")) {
      aName = _PTR(AttributeName) ( anAttr );
      if (strcmp(aName->Value().c_str(), theName) == 0) {
	aResult = anIterator->Value();
	break;
      }
    }
  }

  // if aResule was found then theDoneSomething=false and we return
  *theDoneSomething = !aResult ? 1 : 0;//->_is_nil();
  if ( !*theDoneSomething )
    return aResult;

  // add new SObject
  aResult = theBuilder->NewObject( theSO );
  anAttr = theBuilder->FindOrCreateAttribute(aResult, "AttributeName");
  aName = _PTR(AttributeName) ( anAttr );
  aName->SetValue(theName);
  return aResult;
}
    
/**
 * Return true if dataflow is already in the study
 */
bool SUPERVGUI_Main::isDataflowInStudy() const {
  _PTR(Study) aStudy = (( SalomeApp_Study* )study)->studyDS();
  _PTR(SObject) aSO ( aStudy->FindObjectIOR( dataflow->getIOR() ) );
  return ( aSO ? true : false );
}

/**
 * Create a "Supervision" object in the Study and a "dataflow" object under it
 * aDoneSomething will be true if "Supervision" or "dataflow" object were created (not found).
 */
_PTR(SObject) createDataflowSObj( SUIT_Study* study, 
				  SUPERV::Graph_var dataflow, 
				  _PTR(StudyBuilder) aBuilder,
				  bool& aDoneSomething ) {
  _PTR(Study)            aStudy = (( SalomeApp_Study* )study)->studyDS();
  _PTR(GenericAttribute) anAttr;
  _PTR(AttributeName)    aName;

  // Find or create "Supervisor -> aNewDataflow_1" SObjects in the study
  _PTR(SObject) aSO = aStudy->FindObjectIOR( dataflow->getIOR() );
  if ( !aSO ) { // dataflow SObject not found in the study
    aDoneSomething = true; 
    _PTR(SComponent) aComponent = aStudy->FindComponent(STUDY_SUPERVISION);
    if ( !aComponent ) { // is supervision component not found, then create it
      aComponent = aBuilder->NewComponent(STUDY_SUPERVISION);
      anAttr = aBuilder->FindOrCreateAttribute(aComponent, "AttributeName");
      aName = _PTR(AttributeName) ( anAttr );
      aName->SetValue( (( CAM_Application* )(study->application()))->moduleTitle( "SUPERV" ).latin1() );
	
      anAttr = aBuilder->FindOrCreateAttribute(aComponent, "AttributePixMap");
      _PTR(AttributePixMap) aPixmap ( anAttr );
      aPixmap->SetPixMap( "ICON_OBJBROWSER_Supervision" );

      SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
      if ( aSupMod )
	aBuilder->DefineComponentInstance(aComponent, SalomeApp_Application::orb()->object_to_string(aSupMod->getEngine()));
                                                    //dynamic_cast<SALOMEDS_Study*>( aStudy )->ConvertObjectToIOR(aSupMod->getEngine()));
      else MESSAGE("NULL Supervision module!");
      
    }
    // create dataflow SObject ("aNewDataflow_1") 
    aSO = aBuilder->NewObject(aComponent);
    anAttr =  aBuilder->FindOrCreateAttribute(aSO, "AttributeName");
    aName = _PTR(AttributeName) ( anAttr );
    aName->SetValue(dataflow->Name());
    anAttr =  aBuilder->FindOrCreateAttribute(aSO, "AttributeIOR");
    _PTR(AttributeIOR) anIORAttr ( anAttr );
    anIORAttr->SetValue(dataflow->getIOR());
  }

  return aSO;
}

void SUPERVGUI_Main::addDataflowToStudy() {
  bool isCreated;
  _PTR(StudyBuilder) aBuilder ( (( SalomeApp_Study* )study)->studyDS()->NewBuilder() );
  aBuilder->NewCommand();
  _PTR(SObject) aDF ( createDataflowSObj( study, dataflow, aBuilder, isCreated ) );
  if ( aDF ) {
    aBuilder->CommitCommand();
    // what is this register/unregister?? don't know..
    SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
    if ( !aSupMod ) {
      MESSAGE("NULL Supervision module!");
      return;
    }
    aSupMod->unregisterGraph(this);
    aSupMod->registerGraph(dataflow->getIOR(), this);

    if ( !myThread->running() )
      aSupMod->updateObjBrowser();

    aSupMod->getApp()->updateActions();  
  }
  else {
    MESSAGE( "ERROR: failed to find or create dataflow SObject" );
    aBuilder->AbortCommand();
  }
}

bool SUPERVGUI_Main::putDataStudy( SUPERV_Port p, const char* inout ) {
  Trace("SUPERVGUI_Main::putDataStudy");

  bool PublishOtherComponent = false;

  // static variable to ensure that only one instance (thread) is executing this function 
  static bool isIn = false;
  if (isIn)   return true; 
  else        isIn = true;

  _PTR(Study)        aStudy = (( SalomeApp_Study* )study)->studyDS();
  const bool         aLocked = aStudy->GetProperties()->IsLocked();
  _PTR(StudyBuilder) aBuilder ( aStudy->NewBuilder() );
  bool                           aDoneSomething = false;

  // asv 23.11.04 : fix for PAL6852 if the study is locked -- then we can't put anything in it.
  if ( aLocked ) {
    MESSAGE( "The study is locked and can not be modified!" );
    isIn = false;
    return false;
  }

  // check if the port and its node are good
  if ( CORBA::is_nil( p ) || CORBA::is_nil( p->Node() ) ) {
    MESSAGE( "putInStudy ERROR: port or node are NULL!" );
    isIn = false;
    return false;
  }

  // open new command.  Commit or Abort it depending on aDoneSomething variable or error
  aBuilder->NewCommand();
  
  // Find or create "Supervisor -> aNewDataflow_1" SObjects in the study
  _PTR(SObject) aSO = createDataflowSObj( study, dataflow, aBuilder, aDoneSomething );

  if ( !aSO ) {
    MESSAGE( "ERROR: putDataStudy() could not find or create dataflow SObject" ); 
    aBuilder->AbortCommand();
    isIn = false;
    return false;
  }

  // Create "Run -> Time and date, etc." SObjects
  if  ( p->State() == SUPERV::ReadyState ) { // if port contains valid computed value

    aSO = SearchOrCreateSOWithName( aStudy, aBuilder, aSO, QString("Run ") + myRunTime.toString(), &aDoneSomething ); // get run time SO
    aSO = SearchOrCreateSOWithName( aStudy, aBuilder, aSO, p->Node()->Name(), &aDoneSomething ); // get node SO
    aSO = SearchOrCreateSOWithName( aStudy, aBuilder, aSO, inout, &aDoneSomething ); // get in/out SO
    aSO = SearchOrCreateSOWithName( aStudy, aBuilder, aSO, p->Name(), &aDoneSomething ); // get port SO

    // create IOR attribute for port SObject (usually with "return" name)
    _PTR(GenericAttribute) anAttr ( aBuilder->FindOrCreateAttribute(aSO, "AttributeIOR") );
    _PTR(AttributeIOR) anIORAttr  ( anAttr );

    // if we have not created anything (all SObject already existed) and we have the same IORAttribute
    // on the needed SObject -> then don't do anything! it's already there!
    if ( !aDoneSomething && strcmp(anIORAttr->Value().c_str(), p->ToString()) == 0 ) {
      aBuilder->AbortCommand();
      isIn = false;
      return true;
    }

    aDoneSomething = true; // going to set some value to anIORAttr any way from this point..

    // set object value to the study: if object is external, then put it with
    //  help of the specific component - owner
    if ( p->IsIOR() ) {
      // get according component driver for result object
      SALOME_LifeCycleCORBA aLCC( myNService );
      SUPERV_FNode aFNode = SUPERV::FNode::_narrow( p->Node() );
      if ( !aFNode->_is_nil() ) {
	Engines::Component_var aComponent = aLCC.FindOrLoad_Component( aFNode->GetContainer(), aFNode->GetComponentName() );
	SALOMEDS::Driver_var aDriver = SALOMEDS::Driver::_narrow( aComponent );
	if ( !CORBA::is_nil( aDriver ) ) { // if driver was found, publish object
	  CORBA::Object_ptr anObject = new CORBA::Object();
	  CORBA::Any* anAny = p->ToAny();
	  (*anAny) >>= anObject;
	
	  if ( aDriver->CanPublishInStudy( anObject ) ) {
	    SALOMEDS::SObject_var aTmpSO;// = aSO;
	    SALOMEDS_Study* aSStudy = dynamic_cast<SALOMEDS_Study*>( aStudy.get() );
	    if ( !aSStudy ) return false;
	    try {
	      aTmpSO = aDriver->PublishInStudy( aSStudy->GetStudy(), aTmpSO, anObject, "" );
	      aBuilder->Addreference(aSO, _PTR(SObject)(new SALOMEDS_SObject( aTmpSO )) );
	      PublishOtherComponent = true;
	    }
	    catch ( ... ) { // mkr : PAL8150
	    }
	  } 
	  else { // can't publish object: abort transaction
	    MESSAGE( "CanPublishInStudy() returned FALSE.  ok, AbortCommand.." );
	    aBuilder->AbortCommand();
	    isIn = false;
	    return false;
	  }
	}
	else { // component has no driver, but could store IORs (like Calculator)
	  _PTR(SObject) anIORSO ( aStudy->FindObjectIOR( p->ToString() ) );
	  if ( anIORSO ) {
	    aBuilder->Addreference(aSO, anIORSO);
	    // mkr : IPAL9672
	    PublishOtherComponent = true;
	  }
	  else { // Hm... the object (==port value) was not found, so we don't publish it.
	    MESSAGE( "The object (==port value) was not found, so we don't publish it" );
	    aBuilder->AbortCommand();
	    isIn = false;
	    return false;
	  }
	}
      }
      else { // FNode is NULL -> bad
	MESSAGE( "FNode is NULL." );
	if ( SUPERV::INode::_narrow( p->Node() ) ) { // mkr : IPAL10175
	  _PTR(SObject) anIORSO ( aStudy->FindObjectIOR( p->ToString() ) );
	  if ( anIORSO )
	    aBuilder->Addreference(aSO, anIORSO);
	}
	else {
	  MESSAGE( "FNode and INode are NULL.  Not good at all.  Aborting command." );
	  aBuilder->AbortCommand();
	  isIn = false;
	  return false;
	}
      }
    } 
    else {
      anIORAttr->SetValue( p->ToString() ); // ior attribute already set for the prevoius condition
    }
  }

  if ( aDoneSomething )
    aBuilder->CommitCommand();
  else
    aBuilder->AbortCommand();

  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( aSupMod ) {
    if ( PublishOtherComponent )
      dynamic_cast<SalomeApp_Application*>( study->application() )->updateObjectBrowser(true);
    else
      aSupMod->updateObjBrowser();
  }
  else MESSAGE("NULL Supervision module!");

  SalomeApp_Application* app = (SalomeApp_Application*)(SUIT_Session::session()->activeApplication());
  if(app) app->updateActions();  
  isIn = false;
  return true;
}


void SUPERVGUI_Main::ActivatePanning()
{
  if (myCanvasView->isVisible()) {
    myCanvasView->ActivatePanning();
  } else if (myArrayView->isVisible()) {
    myArrayView->ActivatePanning();
  }
}


void SUPERVGUI_Main::ResetView()
{
  if (myCanvasView->isVisible()) {
    myCanvasView->ResetView();
  } else if (myArrayView->isVisible()) {
    myArrayView->ResetView();
  }
}

void SUPERVGUI_Main::syncNotification() {
  char* graph;
  char* node;
  char* type;
  char* message;
  char* sender;
  long  counter;
  char* date;
  long  stamp;

  while (notification->Receive(&graph, &node, &type, &message, &sender, &counter, &date, &stamp)) {
    if (isFiltered(graph, node, type, message, sender, counter, date, stamp)) { // mkr : PAL7037
      QString mess("");
      mess += "NOTIF: "; mess += graph;
      mess += " / "    ; mess += node;
      mess += " / "    ; mess += type;
      mess += " / "    ; mess += message;
      getMessage()->putMessage(mess.latin1());
    };
  };
}
  
bool SUPERVGUI_Main::isFiltered(char* graph,  char* node,   char* type, char* message, 
				char* sender, long counter, char* date, long stamp) {
  Trace("SUPERVGUI_Main::isFiltered");
  bool b = false;
  if (strcmp(getDataflow()->Name(), graph) == 0) {
    SUPERVGUI_CanvasNode* n;
    QObjectList* nodes;
    // mkr : PAL7037 -->
    if (myCurrentView == CANVASTABLE)
      nodes = myArray->queryList("SUPERVGUI_CanvasNode");
    else
      nodes = myCanvas->queryList("SUPERVGUI_CanvasNode");
    // mkr : PAL7037 <--
    QObjectListIt i(*nodes);
    while ((n=(SUPERVGUI_CanvasNode*)i.current()) != 0) {
      ++i;
      if (strcmp(n->name(), node) == 0) {
	if (strcmp(type, NOTIF_WARNING) == 0) {
	  b = n->isWarning();
	} else if (strcmp(type, NOTIF_STEP) == 0) {
	  b = n->isStep();
	} else if (strcmp(type, NOTIF_TRACE) == 0) {
	  b = n->isTrace();
	} else if (strcmp(type, NOTIF_VERBOSE) == 0) {
	  b = n->isVerbose();
	};
	break;
      };
    };
    delete nodes;
    if ( myLogged && myLogFile && ( ( !myFiltered ) || b ) ) {
      fprintf( myLogFile, "NOTIF %ld\t%s\t%s\t%ld\t%s\t%s\t%s\t%s\n", stamp, date, sender, counter, graph, node, type, message );
      fflush( myLogFile );
    };
  };
  return( b );
}

void SUPERVGUI_Main::closeEvent(QCloseEvent* e) {
  e->accept();
}


void SUPERVGUI_Main::setPaletteBackgroundColor(const QColor& color) { 

  myCanvas->setBackgroundColor(color);
  myCanvasView->setPaletteBackgroundColor(color.light());
  myArray->setBackgroundColor(color);
  myArrayView->setPaletteBackgroundColor(color.light());
  
  SUPERVGraph_View::setPaletteBackgroundColor(color); 
}

QPtrList< char * > SUPERVGUI_Main::getEventNodes() {
  return myEventNodes;
}

void SUPERVGUI_Main::setEventNodes(QPtrList< char * > theEventNodes) {
  myEventNodes = theEventNodes;
}

QPtrList< SUPERV::GraphState > SUPERVGUI_Main::getStates() {
  return myStates;
}

void SUPERVGUI_Main::setStates(QPtrList< SUPERV::GraphState > theStates) {
  myStates = theStates;
}

int SUPERVGUI_Main::getNodesNumber() {
  //create a list of nodes of the graph
  SUPERV_Nodes nodes = getDataflow()->Nodes();
  int RetVal = nodes->CNodes.length() + nodes->FNodes.length() +
               nodes->INodes.length() + nodes->GNodes.length() +
	       nodes->LNodes.length() + nodes->SNodes.length();
  return RetVal;
}

SUPERVGUI_Thread* SUPERVGUI_Main::getMyThread() {
  return myThread;
}

void SUPERVGUI_Main::startTimer() {
  myTimer->start(500);
}

void SUPERVGUI_Main::executionFinished() {
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( aSupMod ) 
    //aSupMod->updateObjBrowser();
    // PAL10611: update all modules data
    aSupMod->getApp()->updateObjectBrowser(true);
  else MESSAGE("NULL Supervision module!");
  
  myCanvas->update();
  myArray->update();
}

void SUPERVGUI_Main::checkExecution() {
  if (myThread->finished()) {
    myTimer->stop();
    executionFinished();
  }
}

/**
 * Editing() is to be called by any operation in GUI before modification of a datamodel
 * (add/remove ports or nodes, etc.).  It is used to resolve inconsistancies between 2 data models
 * in Engine: Editor and Executor.  During and after execution, the values of ports and statuses of nodes
 * are taken from Executor data model.  But when user starts editing the graph - these changes must
 * be applied to Editor data model.  This function destroys Executor data model and moves to Editor.   
 */ 
void SUPERVGUI_Main::Editing() {
  if ( !SUPERV_isNull( dataflow ) )
    dataflow->Editing();
  
  // updata GUI, Nodes' statuses and Ports' values could change.
  sync();
}

void SUPERVGUI_Main::removeArrayChild(SUPERV::CNode_ptr theNode)
{
  // mkr: since the deletion of the node allow only in CANVAS view,
  // it is necessary to remove the CanvasArray's children, which
  // have the same CNode engine as deleting node. This CNode is given
  // as argument
  if (myArray) {
    const QObjectList* aChList = myArray->children();
    if ( aChList ) { // asv 27.01.05 : fix for 7817
      QObjectListIt aItChList(*aChList);
      SUPERVGUI_CanvasNode* anObjNode;
      while ((anObjNode = (SUPERVGUI_CanvasNode*)aItChList.current()) != 0) {
	++aItChList;
	if ((QString(anObjNode->getEngine()->Name())).compare(QString(theNode->Name())) == 0) {
	  myArray->removeChild(anObjNode);
	  delete anObjNode;
	}
      }
    } // end of if ( ChList )
  }
}

/**
 * ReadyToModify() must be called before any modification
 * operation - asks to kill execution of dataflow.  If returns false - 
 * modification (==Editing() ) is not allowed.
 */
bool SUPERVGUI_Main::ReadyToModify() {
  if ( CORBA::is_nil( dataflow ) )
    return false;
  if ( dataflow->IsExecuting() ) {
    bool quit = SUIT_MessageBox::warn2( SUIT_Session::session()->activeApplication()->desktop(), // 0=Yes, 1=No
      tr("WARNING"), tr("MSG_GRAPH_ISRUN"), tr( "BUT_YES" ), tr( "BUT_CANCEL" ), 0, 1, 0 );
    if ( quit ) // user selected NOT to kill dataflow and NOT to add new node
      return false;
    else  if ( dataflow->IsExecuting() ) // user selected to kill the dataflow and add new node after that
      kill();   // checking again for IsExecuting to be sure that it was not finished while MB was up
  }
  return true;
}

void SUPERVGUI_Main::resizeView( QResizeEvent* theEvent )
{
  if ( (myCurrentView == CANVAS || myCurrentView == CONTROLFLOW) && getCanvas() )
    if ( getCanvas()->width() < theEvent->size().width()
	 ||
	 getCanvas()->height() < theEvent->size().height() )
      getCanvas()->resize( theEvent->size().width(), theEvent->size().height() );
  if ( myCurrentView == CANVASTABLE && getCanvasArray() )
    if ( getCanvasArray()->width() < theEvent->size().width()
	 ||
	 getCanvasArray()->height() < theEvent->size().height() )
      getCanvasArray()->resize( theEvent->size().width(), theEvent->size().height() );
}

/*!
  Sets a modified flag in an active study
 */
void SUPERVGUI_Main::setModifiedFlag()
{
  SalomeApp_Application* app = dynamic_cast<SalomeApp_Application*>(SUIT_Session::session()->activeApplication());
  if(app) {
    SalomeApp_Study* study = dynamic_cast<SalomeApp_Study*>(app->activeStudy());
    if(study) {
      _PTR(Study) studyDS = study->studyDS();
      _PTR(SComponent) sco = studyDS->FindComponent(STUDY_SUPERVISION);
      if(sco) {
	_PTR(StudyBuilder) builder = studyDS->NewBuilder();
	_PTR(GenericAttribute) ga;
	ga = builder->FindOrCreateAttribute(sco, "AttributeParameter");
	_PTR(AttributeParameter) ap(ga);
	if(ap) {
	  ap->SetBool("SUPERVISOR_MODIFIED_FLAG",1);
	  app->updateActions();
	}
      }
    }
  }
}


/******************************* SUPERVGUI_Thread class ****************************************/
SUPERVGUI_Thread::SUPERVGUI_Thread()
     :QThread()
{
  myIsActive = false;
}

SUPERVGUI_Thread::~SUPERVGUI_Thread()
{
  //it is a virtual destructor and needs to be determine here
}

void SUPERVGUI_Thread::startThread(const char* m)
{
  if (!myIsActive) {
    myIsActive = true;
    
    myMain->getMessage()->putMessage(m); 
    myMain->sync();

    start();
  }
}

void SUPERVGUI_Thread::setMain( SUPERVGUI_Main* theMain )
{
  myMain = theMain;
}

void SUPERVGUI_Thread::KillThread( bool theValue )
{
  myMutex.lock();
  myIsActive = !(theValue);
  myMutex.unlock();
}

template<class TObject, typename TArg, typename TArg1, typename TArg2,
	 typename TStoreArg = TArg, typename TStoreArg1 = TArg1, typename TStoreArg2 = TArg2>
class TVoidMemFun3ArgEvent: public SALOME_Event{
public:
  typedef void (TObject::* TAction)(TArg,TArg1,TArg2);
  TVoidMemFun3ArgEvent(TObject* theObject, TAction theAction, TArg theArg, TArg1 theArg1, TArg2 theArg2):
    myObject(theObject),
    myAction(theAction),
    myArg(theArg),
    myArg1(theArg1),
    myArg2(theArg2)
  {}
  virtual void Execute(){
    (myObject->*myAction)(myArg,myArg1,myArg2);
  }
private:
  TObject* myObject;
  TAction myAction;
  TStoreArg myArg;
  TStoreArg1 myArg1;
  TStoreArg2 myArg2;
};

typedef TVoidMemFun3ArgEvent<SUPERVGUI_Thread, SUPERV_CNode&, SUPERV::GraphEvent&, SUPERV::GraphState&> TMainRunEvent;

/**
 * main_thread_run must be executed in the qt main thread
 * It is activated by calling ProcessVoidEvent
 */
void SUPERVGUI_Thread::main_thread_run(SUPERV_CNode& aNode, SUPERV::GraphEvent& aEvent, SUPERV::GraphState& aState)
{
    // in case node "said" something during changing state through notification mechanism - output it
    myMain->syncNotification();
    myMain->sync(); // mkr : NPAL14881
    
    // "kill" or undefined event came
    if (( aEvent == SUPERV::UndefinedEvent && aState == SUPERV::UndefinedState ) ||
       ( aEvent == SUPERV::NoEvent && aState == SUPERV::NoState ) ||
       ( aEvent == SUPERV::KillEvent && aState == SUPERV::KillState )) {

      myIsActive = false;
    }
    else { // a "normal" execution event came
      char* aName = NULL;
      if ( aNode != NULL && !CORBA::is_nil( aNode ) ) 
       aName = aNode->Name();      

      // What follow is not quite sure. The entire function is posted to the main qt thread.
      // So all executions are serialized. Is it really possible to call execute when another
      // execute is running. I don't think so (C Caremoli)
      // this function is asynchronious.  The call does NOT wait when SUPERVGUI_Main::execute finishes
      // handling the event.  So: SUPERVGUI_Main::execute must be fast, in order we don't get here again
      // on the next loop iteration, BEFORE  previous SUPERVGUI_Main::execute finished.
      myMain->execute(aName, aState );
    }    

    // execution is finished.  just set a "finished" message(s)
    if ( !myIsActive ) {
      QString aMess;
      switch ( myMain->getDataflow()->State() ) {
      case SUPERV_Editing :     
       myMain->getMessage()->putMessage( myMain->getDataflow()->IsReadOnly()? 
                                        tr("MSG_GRAPH_READONLY"): tr("MSG_GRAPH_EDITING") );
       break;  
      case SUPERV_Suspend : 
       myMain->getMessage()->putMessage( tr("MSG_GRAPH_SUSPENDED") );
       break; 
      case SUPERV_Done : 
       myMain->getMessage()->putMessage( tr("MSG_GRAPH_FINISHED") );
       break;  
      case SUPERV_Error :
       myMain->getMessage()->putMessage( tr("MSG_GRAPH_ABORTED") );
       // PAL12866 -->
       aMess = QString(myMain->getDataflow()->Messages());
       if ( !aMess.isEmpty() ) {
	 myMain->getMessage()->putMessage( tr("MSG_DF_BADEXECUTE") + QString(" : ") + aMess );
	 QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), 
			      tr("MSG_DF_BADEXECUTE") + QString(" : ") + aMess);
       }
       // PAL12866 <--
       break;  
      case SUPERV_Kill:
       myMain->getMessage()->putMessage( tr("MSG_GRAPH_KILLED") );
       break;
      } // end of switch

      // asv 03.02.05 : fix for PAL6859, not very good, but works..
      myMain->sync();
    } // end of if !myIsActive
}

void SUPERVGUI_Thread::run()
{
  myMain->startTimer();
  
  // GUI cycle to handle events coming for Engine
  while ( myIsActive ) {
    
    SUPERV_CNode aNode = NULL;
    SUPERV::GraphEvent aEvent = SUPERV::UndefinedEvent ;
    SUPERV::GraphState aState = SUPERV::UndefinedState ;

    // blocking function of Engine.  Return from there only after anEvent happens on node aNode
    myMain->getDataflow()->Event(aNode, aEvent, aState);

    ProcessVoidEvent( new TMainRunEvent( this, &SUPERVGUI_Thread::main_thread_run,aNode, aEvent, aState ) );
    
  } // end of while( myIsActive )
  
  QThread::exit();
}

/******************************* SUPERVGUI_DSGraphParameters class ****************************************/
/*!
  Constructor
*/
SUPERVGUI_DSGraphParameters::SUPERVGUI_DSGraphParameters(SUPERV_Graph theGraph, bool isReadOnly)
     : QDialog( SUIT_Session::session()->activeApplication()->desktop(), "", true, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu ) 
{
  Trace("SUPERVGUI_DSGraphParameters::SUPERVGUI_DSGraphParameters");
  setCaption( tr( "TLT_DSGRAPHPARAMS" ) );
  setSizeGripEnabled( true );
  myGraph = theGraph;

  QGridLayout* TopLayout = new QGridLayout( this );
  TopLayout->setSpacing( 6 );
  TopLayout->setMargin( 11 );
    
  QGroupBox* TopGroup = new QGroupBox( this, "TopGroup" );
  TopGroup->setColumnLayout(0, Qt::Vertical );
  TopGroup->layout()->setSpacing( 0 );
  TopGroup->layout()->setMargin( 0 );
  QGridLayout* TopGroupLayout = new QGridLayout( TopGroup->layout() );
  TopGroupLayout->setAlignment( Qt::AlignTop );
  TopGroupLayout->setSpacing( 6 );
  TopGroupLayout->setMargin( 11 );

  // DeltaTime
  QLabel* DeltaTimeL = new QLabel( tr( "DELTATIME_LBL" ), TopGroup );  
  TopGroupLayout->addWidget( DeltaTimeL, 0, 0 );
  
  myDeltaTime = new QtxDblSpinBox( 0.0, 1.0, 0.1, TopGroup );
  myDeltaTime->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  TopGroupLayout->addWidget( myDeltaTime, 0, 1 );

  // TimeOut
  QLabel* TimeOutL = new QLabel( tr( "TIMEOUT_LBL" ), TopGroup); 
  TopGroupLayout->addWidget( TimeOutL, 1, 0 );

  myTimeOut = new QLineEdit( TopGroup );
  myTimeOut->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myTimeOut->setValidator( new QIntValidator(this) );
  myTimeOut->setMinimumSize( 100, 0 );
  myTimeOut->setReadOnly( isReadOnly );
  TopGroupLayout->addWidget( myTimeOut, 1, 1 );

  // DataStreamTrace
  QLabel* DataStreamTraceL = new QLabel( tr( "DATASTREAMTRACE_LBL" ), TopGroup); 
  TopGroupLayout->addWidget( DataStreamTraceL, 2, 0 );

  myDataStreamTrace = new QComboBox( TopGroup );
  myDataStreamTrace->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed ) );
  myDataStreamTrace->insertItem("WithoutTrace");
  myDataStreamTrace->insertItem("SummaryTrace");
  myDataStreamTrace->insertItem("DetailedTrace");
  TopGroupLayout->addWidget( myDataStreamTrace, 2, 1 );

  QGroupBox* GroupButtons = new QGroupBox( this, "GroupButtons" );
  GroupButtons->setColumnLayout(0, Qt::Vertical );
  GroupButtons->layout()->setSpacing( 0 );
  GroupButtons->layout()->setMargin( 0 );
  QGridLayout* GroupButtonsLayout = new QGridLayout( GroupButtons->layout() );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( 6 );
  GroupButtonsLayout->setMargin( 11 );
  
  QPushButton* okB     = new QPushButton( tr( "BUT_OK" ),     GroupButtons );
  QPushButton* cancelB = new QPushButton( tr( "BUT_CANCEL" ), GroupButtons );

  GroupButtonsLayout->addWidget( okB, 0, 0 );
  GroupButtonsLayout->addItem  ( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ), 0, 1 );
  GroupButtonsLayout->addWidget( cancelB, 0, 2 );

  TopLayout->addWidget( TopGroup,     0, 0 );
  TopLayout->addWidget( GroupButtons, 1, 0 );

  connect( okB,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( cancelB, SIGNAL( clicked() ), this, SLOT( reject() ) );

  setData();
}

/*!
  Destructor
*/
SUPERVGUI_DSGraphParameters::~SUPERVGUI_DSGraphParameters() {
  Trace("SUPERVGUI_DSGraphParameters::~SUPERVGUI_DSGraphParameters");
}

/*!
  Sets data function
*/
void SUPERVGUI_DSGraphParameters::setData() {
  CORBA::Double aDeltaTime;
  CORBA::Long aTimeOut;
  SUPERV::KindOfDataStreamTrace aDataStreamTrace;

//  myGraph->StreamParams(aTimeOut, aDataStreamTrace, aDeltaTime);
  if (myGraph->IsStreamGraph()) {
    SUPERV_StreamGraph aSGraph = myGraph->ToStreamGraph();
    if (!SUPERV_isNull(aSGraph)) 
      aSGraph->StreamParams(aTimeOut, aDataStreamTrace, aDeltaTime);
  }

  myDeltaTime->setValue(aDeltaTime);
  myTimeOut->setText(QString("%1").arg(aTimeOut));
  myDataStreamTrace->setCurrentItem((int)aDataStreamTrace);
}

/*!
  <OK> button slot
*/
void SUPERVGUI_DSGraphParameters::accept() {
//   myGraph->SetStreamParams( myTimeOut->text().toLong(),
// 			    (SUPERV::KindOfDataStreamTrace) myDataStreamTrace->currentItem(),
// 			    myDeltaTime->value());
  if (myGraph->IsStreamGraph()) {
    SUPERV_StreamGraph aSGraph = myGraph->ToStreamGraph();
    if (!SUPERV_isNull(aSGraph)) 
      aSGraph->SetStreamParams( myTimeOut->text().toLong(),
				(SUPERV::KindOfDataStreamTrace) myDataStreamTrace->currentItem(),
				myDeltaTime->value());
  }
  QDialog::accept();
}


//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasNode.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasNode_H
#define SUPERVGUI_CanvasNode_H

#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_BrowseNodeDlg.h"
#include <qstringlist.h>

class SUPERVGUI_Main;
class SUPERVGUI_CanvasNodePrs;
class SUIT_ResourceMgr;

class SUPERVGUI_CanvasNode : public QObject {
  friend class SUPERVGUI_ManagePortsDlg;
  friend class SUPERVGUI_Clipboard;

  Q_OBJECT

  public:
    virtual ~SUPERVGUI_CanvasNode();

    // done to ignore port update when node is removed
    bool isDestroyed() const { return myDestroyed; }
    void setDestroyed();

    virtual void merge();
    virtual void sync();
    virtual void syncOnEvent(SUPERV::GraphState theStateFromEvent);

    virtual void show();
    virtual void move(int x, int y);

    virtual void hideAll();
    virtual void showAll();
    virtual bool setNodeName(QString aName);

    SUPERV_CNode             getEngine() const { return myNode; }
    SUPERVGUI_Main*          getMain()   const { return myMain; }
    SUPERVGUI_CanvasNodePrs* getPrs();

    bool isWarning();
    bool isStep();
    bool isTrace();
    bool isVerbose();

    SUPERV::KindOfNode getNodeType() const { return myNode->Kind(); }

    SUPERV_CNode  getComputingNode() const { return myNode; }
    SUPERV_FNode  getFactoryNode()   const { return SUPERV::FNode::_narrow(myNode); }
    SUPERV_INode  getInlineNode()    const { return SUPERV::INode::_narrow(myNode); }
    SUPERV_GNode  getGotoNode()      const { return SUPERV::GNode::_narrow(myNode); }
    SUPERV_LNode  getLoopNode()      const { return SUPERV::LNode::_narrow(myNode); }
    SUPERV_ELNode getEndLoopNode()   const { return SUPERV::ELNode::_narrow(myNode);}
    SUPERV_SNode  getSwitchNode()    const { return SUPERV::SNode::_narrow(myNode); }
    SUPERV_ESNode getEndSwitchNode() const { return SUPERV::ESNode::_narrow(myNode);}
    SUPERV_Graph  getMacroNode()     const { return SUPERV::Graph::_narrow(myNode); }

    SUPERV_Port createInPort();
    SUPERV_Port createOutPort();
    QStringList getPortsNamesIN(SUPERV_INode theNode, bool theInputPorts);

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

    QString getLabelText() const { return myLabelText; }

    virtual QString getToolTipText() const;

  signals:
    void objectCreatedDeleted(); // mkr : PAL8237

  public slots:
    void suspendResume();
    void kill();
    //void stopRestart();

    //asv: 13.12.04: ??? --> void configure();
    //asv: 13.12.04: ??? --> void showPython();

    virtual void rename();
    virtual void remove();
    void copy();
    void changeInformation();
    void browse();

    void switchLabel();
    void switchPorts();

    void setWarning(bool b);
    void setStep(bool b);
    void setTrace(bool b);
    void setVerbose(bool b);

    // mkr : IPAL9815 : commented the following code
    /*virtual void addInputPort();
      virtual void addOutputPort();*/
    virtual void editFunction();
    virtual void pastePort();
    void managePorts();
    void exportToLib();
  
  protected:
    SUPERVGUI_CanvasNode (SUIT_ResourceMgr*, QObject*, SUPERVGUI_Main*,
			  SUPERV_CNode theNode, bool theIsCell=false);

    // redefined by most of CanvasNode successors
    virtual SUPERVGUI_CanvasNodePrs* createPrs() const;
    SUIT_ResourceMgr* resMgr() const { return myMgr; }
    
    void createPort(SUPERV::Port_ptr thePort);
    void createStreamPort(SUPERV::StreamPort_ptr thePort);
    
    virtual bool eventFilter(QObject* o, QEvent* e);
    SUPERVGUI_BrowseNodeDlg* myBrowseDlg;

    bool isIgnore;

  protected slots:
    virtual void onDestroyed(QObject*);

  private:
    SUPERV_CNode     myNode;
    SUPERVGUI_Main*  myMain;

    QString          myLabelText;
    SUPERVGUI_CanvasNodePrs* myPrs;
    SUIT_ResourceMgr* myMgr;

    bool myDestroyed;

    bool        warning;
    bool        step;
    bool        trace;
    bool        verbose;
};

#endif

//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_ArrayView.h
//  Author : 
//  Module : SUPERV

#ifndef SUPERVGUI_ArrayView_H
#define SUPERVGUI_ArrayView_H

#include "SUPERVGUI_CanvasArray.h"
#include <qpopupmenu.h>
#include <qtooltip.h>

//VRV: porting on Qt 3.0.5
#if QT_VERSION >= 0x030005
#include <qcursor.h> 
#endif
//VRV: porting on Qt 3.0.5

class SUPERVGUI_Main;

class SUPERVGUI_ArrayView: public QCanvasView {
  Q_OBJECT

  public:
    SUPERVGUI_ArrayView(SUPERVGUI_CanvasArray* theArray, SUPERVGUI_Main* theMain);
    virtual ~SUPERVGUI_ArrayView();

    SUPERVGUI_Main* getMain() const { return myMain; }

    void ActivatePanning();
    void ResetView();

  public slots:
//VSR: 08/06/06    void addToStudy();
    void changeBackground();

  protected:
    void resizeEvent( QResizeEvent* e);

  private:
    void contentsMousePressEvent(QMouseEvent* theEvent);
    void contentsMouseMoveEvent(QMouseEvent* theEvent);
    void contentsMouseReleaseEvent(QMouseEvent* theEvent);

    SUPERVGUI_Main* myMain;
    QPopupMenu*     myPopup;
    int             myAddStudyItem;
    int             myShowToolBarItem;

    QCursor         myCursor;

    QPoint   myGlobalPoint;

    bool myIsPanActivated;

};

class SUPERVGUI_ToolTip: public QToolTip {

  public:
    SUPERVGUI_ToolTip(QWidget* theWidget, QToolTipGroup* theGroup = 0):
      QToolTip(theWidget, theGroup) {}
    ~SUPERVGUI_ToolTip() { remove(parentWidget()); }
    
    virtual void maybeTip(const QPoint& theP);
};

#endif

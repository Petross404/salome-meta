//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Notification.h
//  Author : Francis KLOSS
//  Module : SUPERV

#ifndef SUPERVGUI_Notification_H
#define SUPERVGUI_Notification_H

#include "SUPERVGUI_Def.h"
#include <qdialog.h>
#include <qlist.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qvbox.h>

class SUPERVGUI_Main;
class SUPERVGUI_CanvasNode;
class SUPERVGUI_NotifWidget;

class SUPERVGUI_Notification: public QDialog 
{
  Q_OBJECT

public:
  SUPERVGUI_Notification(SUPERVGUI_Main* m);
  ~SUPERVGUI_Notification();

  bool    getLogged();
  void    setLogged( bool on, QString logFile = QString::null );
  QString getLogFile();
  bool    getFiltered();
  void    setFiltered( bool on );
  bool    getWarning();
  void    setWarning( bool on );
  bool    getStep();
  void    setStep( bool on );
  bool    getTrace();
  void    setTrace( bool on );
  bool    getVerbose();
  void    setVerbose( bool on );
  
private slots:
  void    accept();
  void    reject();
  void    loggedClicked();
  void    browseClicked();

private:
  SUPERVGUI_Main*              myMain;
  QScrollView*                 myView;
  QVBox*                       myBox;
  QPushButton*                 myOkBtn;
  QPushButton*                 myCancelBtn;
  QCheckBox*                   myLogCheck;
  QCheckBox*                   myFilterCheck;
  QLineEdit*                   myFileEdit;
  QPushButton*                 myBrowseBtn;
  SUPERVGUI_NotifWidget*       myMainNotif;
  QList<SUPERVGUI_NotifWidget> myNotifList;
};

class SUPERVGUI_NotifWidget : public QWidget 
{
public:
  SUPERVGUI_NotifWidget( QWidget* parent, SUPERVGUI_CanvasNode* n );
  SUPERVGUI_NotifWidget( QWidget* parent, QString title );
  ~SUPERVGUI_NotifWidget();

  SUPERVGUI_CanvasNode* getNode() { return myNode; }

  bool getWarning() { return myWarningCheck->isChecked(); }
  bool getStep()    { return myStepCheck->isChecked();    }
  bool getTrace()   { return myTraceCheck->isChecked();   }
  bool getVerbose() { return myVerboseCheck->isChecked(); }
  
  void setWarning( bool on ) { myWarningCheck->setChecked( on ); }
  void setStep( bool on )    { myStepCheck->setChecked( on );    }
  void setTrace( bool on )   { myTraceCheck->setChecked( on );   }
  void setVerbose( bool on ) { myVerboseCheck->setChecked( on ); }

private:
  void init();
  
private:
  QString         myTitle;
  SUPERVGUI_CanvasNode* myNode;
  QCheckBox*      myWarningCheck;
  QCheckBox*      myStepCheck;
  QCheckBox*      myTraceCheck;
  QCheckBox*      myVerboseCheck;
};

#endif








//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_GanvasNodePrs.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI_CanvasLink.h"
#include "SUPERVGUI_Canvas.h"
#include "SUPERVGUI_CanvasPort.h"
#include "SUPERVGUI_Main.h"

//#define CHECKTIME

#ifdef CHECKTIME
#include <sys/timeb.h>
#endif

#define DRAW_COLOR Qt::black
#define SELECT_COLOR Qt::magenta
#define SKETCH_COLOR Qt::darkGreen
#define STREAM_COLOR Qt::darkRed // QColor(0, 64, 128) // Qt::blue

#define LINE_WIDTH 1


SUPERVGUI_CanvasLink::SUPERVGUI_CanvasLink(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::Link_ptr theLink):
  QObject(theParent),
  myMain(theMain),
  myLink(0),
  myInputPort(0),
  myOutputPort(0),
  myHilighted(false),
  mySelectedItem(0)
{
  if (theLink && !SUPERV_isNull(theLink)) {
    myLink = SUPERV::Link::_duplicate(theLink);

    SUPERVGUI_Canvas* aCanvas = myMain->getCanvas();
    setName(aCanvas->getLinkName(theLink));

    myInputPort = aCanvas->getPort(myLink->InPort());
    if (myInputPort) myInputPort->addLink(this);

    myOutputPort = aCanvas->getPort(myLink->OutPort());
    if (myOutputPort) myOutputPort->addLink(this);

    if (myInputPort->isStream())
      myColor = STREAM_COLOR;
    else
      myColor = DRAW_COLOR;
  }

  // mkr : PAL8237
  connect(this, SIGNAL(objectCreatedDeleted()), myMain, SLOT(onObjectCreatedDeleted()));
}

SUPERVGUI_CanvasLink::~SUPERVGUI_CanvasLink()
{
  for (QCanvasItemList::Iterator it = myPrs.begin(); it != myPrs.end(); ++it) {
    (*it)->hide();
    delete *it;
  }
  if (myInputPort) myInputPort->removeLink(this);
  if (myOutputPort) myOutputPort->removeLink(this);
}

void SUPERVGUI_CanvasLink::createPrs()
{
  if (myLink && !SUPERV_isNull(myLink)) {
    if (myInputPort) {
      addPoint(myInputPort->getConnectionPoint());
    }
    if (!myMain->getCanvas()->isControlView()) {
      CORBA::Long x, y;
      for (CORBA::Long i = 0; i < myLink->CoordsSize(); i++) {
	myLink->Coords(i+1, x, y);
	addPoint(QPoint(x, y), i+1);
      }
    }
    if (myOutputPort) {
      addPoint(myOutputPort->getConnectionPoint());
    }
  }
  setColor(myHilighted ? SELECT_COLOR : myColor);
}

void SUPERVGUI_CanvasLink::addPoint(const QPoint& thePoint, const int& theIndex)
{
  SUPERVGUI_CanvasPointPrs* aPoint;
  if (myPrs.empty()) {
    aPoint = new SUPERVGUI_CanvasPointPrs(myMain->getCanvas(), this, theIndex);
    aPoint->setColor(myColor);
    aPoint->move(thePoint.x(), thePoint.y());
  }
  else {
    SUPERVGUI_CanvasPointPrs* aPrev = (SUPERVGUI_CanvasPointPrs*) myPrs.last();

    SUPERVGUI_CanvasEdgePrs* anEdge = new SUPERVGUI_CanvasEdgePrs(myMain->getCanvas(), this);
    anEdge->setColor(myColor);
    myPrs.append(anEdge);

    aPoint = new SUPERVGUI_CanvasPointPrs(myMain->getCanvas(), this, theIndex);
    aPoint->setColor(myColor);
    aPoint->move(thePoint.x(), thePoint.y());

    aPrev->setOutEdge(anEdge);
    aPoint->setInEdge(anEdge);
  }
  myPrs.append(aPoint);
}

void SUPERVGUI_CanvasLink::setSelectedObject(QCanvasItem* theItem, const QPoint& thePoint)
{
  mySelectedItem = theItem;
  mySelectedPoint = thePoint;
}

QPopupMenu* SUPERVGUI_CanvasLink::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = new QPopupMenu(theParent);
  int anItem;
  anItem = popup->insertItem(tr("MSG_DELLINK"), this, SLOT(remove()));
  if (myInputPort && 
      (myInputPort->getEngine()->Kind() != SUPERV::EndSwitchParameter ||
       myInputPort->getEngine()->Node()->Kind() != SUPERV::EndSwitchNode))
    if (myInputPort->getEngine()->Kind() == SUPERV::LoopParameter ||
	myOutputPort && myOutputPort->getEngine()->Kind() == SUPERV::LoopParameter)
      popup->setItemEnabled(anItem, false);

  popup->insertSeparator();
  if (mySelectedItem) {
    anItem = popup->insertItem(tr("MSG_ADD_POINT"), this, SLOT(addPoint()));
    if (mySelectedItem->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint)
      popup->setItemEnabled(anItem, false);

    anItem = popup->insertItem(tr("MSG_DEL_POINT"), this, SLOT(removePoint()));
    if (mySelectedItem->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge)
      popup->setItemEnabled(anItem, false);
  }
  return popup;
}

void SUPERVGUI_CanvasLink::show()
{
  if (myPrs.isEmpty()) createPrs();

  for (QCanvasItemList::Iterator it = myPrs.begin(); it != myPrs.end(); ++it) {
    (*it)->show();
  }
}

void SUPERVGUI_CanvasLink::merge()
{
  // remove old presentation
  for (QCanvasItemList::Iterator it = myPrs.begin(); it != myPrs.end(); ++it) {
    delete *it;
  }
  myPrs.clear();

  // display a new one
  show();
}

void SUPERVGUI_CanvasLink::setHilighted(bool state)
{
  myHilighted = state;
  setColor(myHilighted ? SELECT_COLOR : myColor);
  if (!myPrs.isEmpty()) {
    bool disp = myPrs.first()->isVisible();
    if (disp) {
      for (QCanvasItemList::Iterator it = myPrs.begin(); it != myPrs.end(); ++it) {
	(*it)->hide(); (*it)->show();
      }
      myMain->getCanvas()->update();
    }
  }
}

void SUPERVGUI_CanvasLink::moveByPort(SUPERVGUI_CanvasPort* thePort, int dx, int dy)
{
  if (myInputPort && myInputPort == thePort) {
    myPrs.first()->moveBy(dx, dy);
    return;
  }
  if (myOutputPort && myOutputPort == thePort) {
    myPrs.last()->moveBy(dx, dy);
    return;
  }
}

void SUPERVGUI_CanvasLink::moveByPort(SUPERVGUI_CanvasPort* thePort)
{
  QPoint p = thePort->getConnectionPoint();
  if (myInputPort && myInputPort == thePort) {
    myPrs.first()->move(p.x(), p.y());
    return;
  }
  if (myOutputPort && myOutputPort == thePort) {
    myPrs.last()->move(p.x(), p.y());
    return;
  }
}

void SUPERVGUI_CanvasLink::setColor(const QColor& theColor)
{
  for (QCanvasItemList::Iterator it = myPrs.begin(); it != myPrs.end(); ++it) {
    if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
      ((SUPERVGUI_CanvasPointPrs*)(*it))->setColor(theColor);
    }
    else if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge) {
      ((SUPERVGUI_CanvasEdgePrs*)(*it))->setColor(theColor);
    }
  }
}

void SUPERVGUI_CanvasLink::remove() {
  // IPAL9369 : check the dataflow readiness to modify
  if ( !myMain->ReadyToModify() ) // null dataflow or executing, ..
    return;

  myMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag 

  QString aValue;
  SUPERVGUI_CanvasPortIn* aPort = 0;
  SUPERVGUI_Canvas* aCanvas = myMain->getCanvas();
  if (myLink && !SUPERV_isNull(myLink)) {
    if (myInputPort) {
      if (myInputPort->getEngine()->IsParam() || myInputPort->getEngine()->IsInLine()) {
	aValue = QString(myInputPort->getEngine()->ToString());
	aPort = (SUPERVGUI_CanvasPortIn*) myInputPort;
      }
    }
    myLink->destroy();

    emit objectCreatedDeleted(); // mkr : PAL8237
  }

  delete this;
  if (aPort && !aValue.isEmpty() && myMain->getDataflow()->GraphLevel() == 0) {
    aPort->setValue(aValue);
  }
  aCanvas->update();
}

void SUPERVGUI_CanvasLink::addPoint() {
  if (mySelectedItem && mySelectedItem->rtti() == SUPERVGUI_Canvas::Rtti_LinkEdge) {
    SUPERVGUI_CanvasEdgePrs* anEdge = (SUPERVGUI_CanvasEdgePrs*) mySelectedItem;

    int anIndex = 1;
    QCanvasItemList::Iterator it;
    for (it = myPrs.begin(); it != myPrs.end(); ++it) {
      if ((*it) == anEdge) break;
    }
    if (it != myPrs.begin()) {
      --it;
      SUPERVGUI_CanvasPointPrs* aPoint = (SUPERVGUI_CanvasPointPrs*) (*it);
      anIndex = aPoint->getIndex()+1;
      if (anIndex < 1) anIndex = 1;
    }
    if (myLink && !SUPERV_isNull(myLink)) {
      myLink->AddCoord(anIndex, mySelectedPoint.x(), mySelectedPoint.y());
      emit objectCreatedDeleted(); // mkr : PAL8237
    }
    merge();
    myMain->getCanvas()->update();
  }
}

void SUPERVGUI_CanvasLink::removePoint() {
  if (mySelectedItem && mySelectedItem->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
    SUPERVGUI_CanvasPointPrs* aPoint = (SUPERVGUI_CanvasPointPrs*) mySelectedItem;
    if (myLink && !SUPERV_isNull(myLink)) {
      myLink->RemoveCoord(aPoint->getIndex());
      emit objectCreatedDeleted(); // mkr : PAL8237
    }
    merge();
    myMain->getCanvas()->update();
  }
}

QString SUPERVGUI_CanvasLink::getToolTipText() const {
  QString aTTT;
  if (myInputPort && myOutputPort)
    aTTT = myOutputPort->getEngine()->Node()->Name() + QString(" : ") + 
      myOutputPort->getEngine()->Name() + QString(" => ") +
      myInputPort->getEngine()->Node()->Name() + QString(" : ") + 
      myInputPort->getEngine()->Name();
  return aTTT;
}

/*
//===============================================================================
//  SUPERVGUI_CanvasStreamLink: new link to be created
//===============================================================================
SUPERVGUI_CanvasStreamLink::SUPERVGUI_CanvasStreamLink(QObject* theParent, SUPERVGUI_Main* theMain, 
						       SUPERV::StreamLink_ptr theLink):
  SUPERVGUI_CanvasLink(theParent, theMain, theLink)
{
  if (theLink && !SUPERV_isNull(theLink)) {
    myStreamLink = SUPERV::StreamLink::_duplicate(theLink);
  }
}


void SUPERVGUI_CanvasStreamLink::remove() {
  QString aValue;
  SUPERVGUI_CanvasPortIn* aPort = 0;
  SUPERVGUI_Canvas* aCanvas = getMain()->getCanvas();
  if (myStreamLink && !SUPERV_isNull(myStreamLink)) {
    if (getInputPort()) {
      if (getInputPort()->getEngine()->IsParam() || getInputPort()->getEngine()->IsInLine()) {
	aPort = (SUPERVGUI_CanvasPortIn*) getInputPort();
	aValue = QString(aPort->getEngine()->ToString());
      }
    }
    myStreamLink->destroy();
  }
  delete this;
  if (aPort && !aValue.isEmpty()) {
    aPort->setValue(aValue);
  }
  aCanvas->update();
}
*/

//===============================================================================
//  SUPERVGUI_CanvasLinkBuilder: new link to be created
//===============================================================================
SUPERVGUI_CanvasLinkBuilder::SUPERVGUI_CanvasLinkBuilder(QObject* theParent, SUPERVGUI_Main* theMain, 
							 SUPERVGUI_CanvasPort* thePort):
  SUPERVGUI_CanvasLink(theParent, theMain),
  myPort(thePort),
  myFloatingEdge(0)
{
  if (myPort) {
    myPort->addLink(this);
    addPoint(myPort->getConnectionPoint());
  }
  myColor = SKETCH_COLOR;
}

SUPERVGUI_CanvasLinkBuilder::~SUPERVGUI_CanvasLinkBuilder()
{
  if (myFloatingEdge) delete myFloatingEdge;
  if (myPort) myPort->removeLink(this);
}

bool SUPERVGUI_CanvasLinkBuilder::canCreateEngine(SUPERVGUI_CanvasPort* thePort)
{
  if (thePort && myPort) {
    SUPERVGUI_CanvasPort* aInPort;
    SUPERVGUI_CanvasPort* aOutPort;

    // check if ports are couple of input and output
    if (myPort->getEngine()->IsInput()) {
      aInPort = myPort;
      if (thePort->getEngine()->IsInput())
	return false;
      aOutPort = thePort;
    }
    else {
      aOutPort = myPort;
      if (!thePort->getEngine()->IsInput())
	return false;
      aInPort = thePort;
    }

    // control if nodes are different, not the same node
    QString aInNode(aInPort->getEngine()->Node()->Name());
    QString aOutNode(aOutPort->getEngine()->Node()->Name());
    if (aInNode.compare(aOutNode) == 0) // linking outport and inport of the same node
      return false;

    // control types of ports
    int aInKind = aInPort->getEngine()->Kind();
    int aOutKind = aOutPort->getEngine()->Kind();

    // connect stream port with stream port only
    if ((aInKind == SUPERV::DataStreamParameter && aOutKind != SUPERV::DataStreamParameter) ||
	(aOutKind == SUPERV::DataStreamParameter && aInKind != SUPERV::DataStreamParameter))
      return false;

    // asv : 15.12.04 : NOT allow to connect Gate-to-InLine --> it does not make sence!
    // Out Gate port can be connected only to In Gate port 
    // mkr : NPAL14823 : Out Gate port can be connected to Inline port too
    if ( aOutKind == SUPERV::GateParameter && aInKind != SUPERV::GateParameter && aInKind != SUPERV::InLineParameter )
      return false;

    // In Gate can be connected to (receive links from) Gate port and InLine ports (important for Switch nodes)
    if ( aInKind == SUPERV::GateParameter && aOutKind != SUPERV::GateParameter && aOutKind != SUPERV::InLineParameter )
	return false;

    // asv : 15.12.04 : PAL7374, p.2.2 "Bugs and Improvements": multiple links into Gate port 
    //                  for InGate it's OK to accept more than 1 link
    // THESE NEEDS REVISION, ALSO DON'T ALLOW MANY LINKS TO "DEFAULT" PORT OF EndSwitch
    //if ( aInPort->getEngine()->IsLinked() && aInKind != SUPERV::GateParameter ) 
    
    // control if port is already linked except for input inline ports of end switch node (check for EndSwitchParameter)
    // and "Default" port of Switch node (check for aNode->isEndSwitch()).  "Default" port is linked by default, but we
    // let it to be "re-linked" to another port. 
    const bool isEndSwitch = ( aInKind == SUPERV::EndSwitchParameter || aInPort->getEngine()->Node()->IsEndSwitch() );
    if ( !isEndSwitch && aInPort->getEngine()->IsLinked() ) 
      return false;
    
    return true;
  }
  return false;
}

void SUPERVGUI_CanvasLinkBuilder::setCoords(SUPERV::Link_ptr theLink)
{
  if (theLink) {
    QCanvasItemList::Iterator it;
    int anIndex = 1;
    if (myPort->getEngine()->IsInput()) {
      it = myPrs.begin(); ++it; // ignore the first point
      for (; it != myPrs.end(); ++it) {
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
	  theLink->AddCoord(anIndex++, (int)(*it)->x(), (int)(*it)->y());
	}
      }
    }
    else {
      it = myPrs.end(); --it;
      for (; it != myPrs.begin(); --it) {
	if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
	  theLink->AddCoord(anIndex++, (int)(*it)->x(), (int)(*it)->y());
	}
      }
    }
  }
}

void SUPERVGUI_CanvasLinkBuilder::addNextPoint(const QPoint& thePoint, bool theOrtho)
{
  if (myFloatingEdge) myFloatingEdge->hide();

  if (!theOrtho || myPrs.empty()) {
    addPoint(thePoint);
  }
  else {
    SUPERVGUI_CanvasPointPrs* aPrev = (SUPERVGUI_CanvasPointPrs*) myPrs.last();
    int x = (int)aPrev->x(); int y = (int)aPrev->y();
    if (thePoint.x() != x && thePoint.y() != y) {
      addPoint(QPoint(thePoint.x(), y), -2);
    }
    addPoint(thePoint);
  }
  show();
}

void SUPERVGUI_CanvasLinkBuilder::setFloatPoint(const QPoint& thePoint)
{
  if (!myFloatingEdge) {
    myFloatingEdge = new QCanvasLine(getMain()->getCanvas());
    myFloatingEdge->setPen(QPen(myColor, LINE_WIDTH));
  }
  if (!myPrs.empty()) {
    myFloatingEdge->setPoints((int)myPrs.last()->x(), (int)myPrs.last()->y(), 
			      thePoint.x(), thePoint.y());
    myFloatingEdge->show();
  }
}

void SUPERVGUI_CanvasLinkBuilder::removeLastPoint()
{
  if (myPrs.count() > 1) {
    QPoint aLast((int)myPrs.last()->x(), (int)myPrs.last()->y());
    QCanvasItemList::Iterator it = myPrs.end();
    bool removed = false;
    --it;
    for (; it != myPrs.begin(); --it) {
      if ((*it)->rtti() == SUPERVGUI_Canvas::Rtti_LinkPoint) {
	SUPERVGUI_CanvasPointPrs* aPoint = (SUPERVGUI_CanvasPointPrs*) (*it);
	if (aPoint->getIndex() == -1 && removed) break;
      }
      QCanvasItem* anItem = (*it);
      it = myPrs.remove(it);
      delete anItem;
      removed = true;
    }
    if (removed) {
      if (myFloatingEdge) {
	QPoint aPoint = myFloatingEdge->endPoint();
	myFloatingEdge->setPoints((int)myPrs.last()->x(), (int)myPrs.last()->y(),
				  aPoint.x(), aPoint.y());
      }
      else
	setFloatPoint(aLast);
    }
  }
}

void SUPERVGUI_CanvasLinkBuilder::moveByPort(SUPERVGUI_CanvasPort* thePort, int dx, int dy)
{
  if (myPort && myPort == thePort) {
    myPrs.first()->moveBy(dx, dy);
    return;
  }
}

void SUPERVGUI_CanvasLinkBuilder::moveByPort(SUPERVGUI_CanvasPort* thePort)
{
  QPoint p = thePort->getConnectionPoint();
  if (myPort && myPort == thePort) {
    myPrs.first()->move(p.x(), p.y());
    return;
  }
}


//===============================================================================
//  SUPERVGUI_CanvasPointPrs: link point presentation
//===============================================================================
SUPERVGUI_CanvasPointPrs::SUPERVGUI_CanvasPointPrs(QCanvas* theCanvas, 
						   SUPERVGUI_CanvasLink* theLink,
						   const int& theIndex):
  QCanvasEllipse(theCanvas),
  myLink(theLink), myIndex(theIndex),
  myInEdge(0), myOutEdge(0), myMoving(false)
{
  setSize(POINT_SIZE, POINT_SIZE);
  setZ(-1);
}

int SUPERVGUI_CanvasPointPrs::rtti() const
{
  return SUPERVGUI_Canvas::Rtti_LinkPoint;
}

void SUPERVGUI_CanvasPointPrs::setInEdge(SUPERVGUI_CanvasEdgePrs* theEdge)
{
  myInEdge = theEdge;
  theEdge->setFromPoint(this);
}

void SUPERVGUI_CanvasPointPrs::setOutEdge(SUPERVGUI_CanvasEdgePrs* theEdge)
{
  myOutEdge = theEdge;
  theEdge->setToPoint(this); 
}

void SUPERVGUI_CanvasPointPrs::moveBy(double dx, double dy)
{
  QCanvasEllipse::moveBy(dx, dy);
  if (myInEdge) myInEdge->setFromPoint(this);
  if (myOutEdge) myOutEdge->setToPoint(this);
  //resize canvas view if mouse is outside
  int w = (int)(x()+dx) + width() + GRAPH_MARGIN;
  int h = (int)(y()+dy) + height() + GRAPH_MARGIN;
  if (canvas()->width() > w) w = canvas()->width();
  if (canvas()->height() > h) h = canvas()->height();
  if (canvas()->width() < w || canvas()->height() < h) canvas()->resize(w, h);
  if (myIndex > 0 && isMoving()) {
    myLink->getEngine()->ChangeCoord(myIndex, (int)x(), (int)y());
  }
}

void SUPERVGUI_CanvasPointPrs::setColor(const QColor& theColor)
{
  setBrush(theColor);
}

//===============================================================================
//  SUPERVGUI_CanvasEdgePrs: link edge presentation
//===============================================================================
SUPERVGUI_CanvasEdgePrs::SUPERVGUI_CanvasEdgePrs(QCanvas* theCanvas, 
						 SUPERVGUI_CanvasLink* theLink):
  QCanvasLine(theCanvas),
  myLink(theLink)
{
  setZ(-2);
}

int SUPERVGUI_CanvasEdgePrs::rtti() const
{
  return SUPERVGUI_Canvas::Rtti_LinkEdge;
}

void SUPERVGUI_CanvasEdgePrs::setFromPoint(SUPERVGUI_CanvasPointPrs* thePoint)
{
  myStartPoint = thePoint;
  setPoints((int)(thePoint->x()), (int)(thePoint->y()), endPoint().x(), endPoint().y());
}

void SUPERVGUI_CanvasEdgePrs::setToPoint(SUPERVGUI_CanvasPointPrs* thePoint)
{
  myEndPoint = thePoint;
  setPoints(startPoint().x(), startPoint().y(), (int)(thePoint->x()), (int)(thePoint->y()));
}

void SUPERVGUI_CanvasEdgePrs::setColor(const QColor& theColor)
{
  setPen(QPen(theColor, LINE_WIDTH));
}

void SUPERVGUI_CanvasEdgePrs::moveBy(double dx, double dy)
{
  //mkr: for moving segment of link
  if (myStartPoint && myEndPoint) {
    myStartPoint->setMoving(true);
    myStartPoint->moveBy(dx, dy);
    
    myEndPoint->setMoving(true);
    myEndPoint->moveBy(dx,dy);
  }
}


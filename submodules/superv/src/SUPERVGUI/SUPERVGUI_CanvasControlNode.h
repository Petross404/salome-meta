//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasControlNode.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasControlNode_H
#define SUPERVGUI_CanvasControlNode_H

#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_CanvasNode.h"
#include <qdialog.h>
#include <qcombobox.h>

class SUPERVGUI_CanvasComputeNode : public SUPERVGUI_CanvasNode {
  Q_OBJECT

  public:
    static SUPERVGUI_CanvasComputeNode* Create (SUIT_ResourceMgr*, QObject* theParent,
						SUPERVGUI_Main* theMain, SUPERV_CNode theNode);
    virtual ~SUPERVGUI_CanvasComputeNode() {}

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  protected:
    SUPERVGUI_CanvasComputeNode (SUIT_ResourceMgr*, QObject* theParent,
				 SUPERVGUI_Main* theMain, SUPERV_CNode theNode);
};

class SUPERVGUI_CanvasMacroNode : public SUPERVGUI_CanvasComputeNode {
  Q_OBJECT

  public:
    static SUPERVGUI_CanvasMacroNode* Create (SUIT_ResourceMgr*, QObject* theParent,
					      SUPERVGUI_Main* theMain, SUPERV_CNode theNode);
    virtual ~SUPERVGUI_CanvasMacroNode() {}

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  public slots:
    virtual void remove(); // mkr : IPAL11549
    void openSubGraph();
    void exportDataflow();

  protected:
    SUPERVGUI_CanvasMacroNode (SUIT_ResourceMgr*, QObject* theParent,
			       SUPERVGUI_Main* theMain, SUPERV_CNode theNode);

    virtual SUPERVGUI_CanvasNodePrs* createPrs() const;
};


class SUPERVGUI_CanvasEndNode;
class SUPERVGUI_CanvasStartNode : public SUPERVGUI_CanvasNode
{
  Q_OBJECT

  public:
    static SUPERVGUI_CanvasStartNode* Create (SUIT_ResourceMgr*, QObject* theParent,
					      SUPERVGUI_Main* theMain, SUPERV_CNode theNode);
    virtual ~SUPERVGUI_CanvasStartNode() {}

    virtual bool setNodeName(QString aName);

    SUPERVGUI_CanvasEndNode* getCoupled() const { return myCoupled; }

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  public slots:
    virtual void remove();

    // mkr : IPAL9815 : commented the following code
    /*virtual void addInputPort();
      virtual void addOutputPort();*/
    virtual void pastePort();

  protected:
    SUPERVGUI_CanvasStartNode (SUIT_ResourceMgr*, QObject* theParent,
			       SUPERVGUI_Main* theMain, SUPERV_CNode theNode);

    virtual SUPERVGUI_CanvasNodePrs* createPrs() const;

  protected slots:
    virtual void onDestroyed(QObject*);

  private:
    friend class SUPERVGUI_CanvasEndNode;
    void setCoupled(SUPERVGUI_CanvasEndNode* theEnd) { myCoupled = theEnd; }

    SUPERVGUI_CanvasEndNode* myCoupled;
};


class SUPERVGUI_CanvasEndNode : public SUPERVGUI_CanvasNode {
  Q_OBJECT

  public:
    static SUPERVGUI_CanvasEndNode* Create (SUIT_ResourceMgr*, QObject* theParent,
					    SUPERVGUI_Main* theMain, SUPERV_CNode theNode,
					    SUPERVGUI_CanvasStartNode* theStart);
    virtual ~SUPERVGUI_CanvasEndNode() {}

    SUPERVGUI_CanvasStartNode* getCoupled() const { return myCoupled; }

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  // mkr : IPAL9815 : commented the following code
  /*public slots:
    virtual void addInputPort();*/

  protected:
    SUPERVGUI_CanvasEndNode (SUIT_ResourceMgr*, QObject* theParent,
			     SUPERVGUI_Main* theMain, SUPERV_CNode theNode,
 			     SUPERVGUI_CanvasStartNode* theStart);

    virtual SUPERVGUI_CanvasNodePrs* createPrs() const;

  private:
    SUPERVGUI_CanvasStartNode* myCoupled;
};


class SUPERVGUI_CanvasGotoNode : public SUPERVGUI_CanvasNode {
  Q_OBJECT

  public:
    static SUPERVGUI_CanvasGotoNode* Create (SUIT_ResourceMgr*, QObject* theParent,
					     SUPERVGUI_Main* theMain, SUPERV_CNode theNode);
    virtual ~SUPERVGUI_CanvasGotoNode() {}

    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

  public slots:
    void linkToNode();

  protected:
    SUPERVGUI_CanvasGotoNode (SUIT_ResourceMgr*, QObject* theParent,
			      SUPERVGUI_Main* theMain, SUPERV_CNode theNode);

    virtual SUPERVGUI_CanvasNodePrs* createPrs() const;
};

class SUPERVGUI_CanvasCellNode : public SUPERVGUI_CanvasNode {
  Q_OBJECT

  public:
    static SUPERVGUI_CanvasCellNode* Create (SUIT_ResourceMgr*, QObject* theParent, 
					     SUPERVGUI_Main* theMain, 
					     SUPERV_CNode theNode);
    virtual ~SUPERVGUI_CanvasCellNode();

    // asv : 26.01.05 : everything moved to CanvasNode::getPopupMenu()
    //virtual QPopupMenu* getPopupMenu(QWidget* theParent);

    void setPairCell(SUPERVGUI_CanvasCellNode* thePairCell);
    SUPERVGUI_CanvasCellNode* getPairCell();

    virtual void sync();

    // asv : 26.01.05 : decided to remove all "edition" operations on nodes in Table view
    //virtual bool setNodeName(QString aName);

  protected:
    SUPERVGUI_CanvasCellNode (SUIT_ResourceMgr*, QObject* theParent, 
			      SUPERVGUI_Main* theMain, 
			      SUPERV_CNode theNode);

    virtual SUPERVGUI_CanvasNodePrs* createPrs() const;

 protected:
    SUPERVGUI_CanvasCellNode*  myPairCell;  //for ControlNodes

 private:
    bool        myIsControl;
    bool        myIsStart;
};

class SUPERVGUI_CanvasCellEndNode : public SUPERVGUI_CanvasCellNode {
 
  public:
    static SUPERVGUI_CanvasCellEndNode* Create (SUIT_ResourceMgr*, QObject* theParent, 
						SUPERVGUI_Main* theMain, 
						SUPERV_CNode theNode, 
						SUPERVGUI_CanvasCellNode* theStart);
    virtual ~SUPERVGUI_CanvasCellEndNode() {}

    // asv : 26.01.05 : decided to remove all "edition" operations on nodes in Table view
    //virtual bool setNodeName(QString theName);

  protected:
    SUPERVGUI_CanvasCellEndNode (SUIT_ResourceMgr*, QObject* theParent, 
				 SUPERVGUI_Main* theMain, 
				 SUPERV_CNode theNode, 
				 SUPERVGUI_CanvasCellNode* theStart);
};

/**
 * Taken from SUPERVGUI_ControlNode.h without change
 */
class SUPERVGUI_SelectInlineDlg : public QDialog {
  Q_OBJECT

 public:
  SUPERVGUI_SelectInlineDlg(SUPERVGUI_Main* theMain);
  virtual ~SUPERVGUI_SelectInlineDlg() {};

  QString getName() { return myCombo->currentText(); }

 private:
  QComboBox* myCombo;
};


#endif

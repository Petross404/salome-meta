//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasCellNodePrs.cxx
//  Author : 
//  Module : SUPERV

#include "SUPERVGUI_CanvasCellNodePrs.h"
#include "SUPERVGUI_Canvas.h"

#include "SUIT_ResourceMgr.h"

#define TEXT_MARGIN 5

// ----------------------------------------------------------
// Cell widget of the table on QCanvas
// ----------------------------------------------------------
SUPERVGUI_CanvasCellNodePrs::SUPERVGUI_CanvasCellNodePrs( SUIT_ResourceMgr* mgr, QCanvas* theCanvas, SUPERVGUI_CanvasCellNode* theNode):
  myTitleWidth(CELL_WIDTH_PART),
  myLabelWidth(CELL_WIDTH_PART),
  myStatusWidth(CELL_WIDTH_PART), 
  SUPERVGUI_CanvasNodePrs( mgr, theCanvas, theNode, true)
{
  myPortVisible = false;
}

SUPERVGUI_CanvasCellNodePrs::~SUPERVGUI_CanvasCellNodePrs()
{
}

void SUPERVGUI_CanvasCellNodePrs::moveBy(double dx, double dy) {
//Trace("SUPERVGUI_CanvasCellNodePrs::moveBy");
  int aX = (int) (x()+dx);
  int aY = (int) (y()+dy);

  int w = aX + width() + GRAPH_MARGIN;
  int h = aY + height() + GRAPH_MARGIN;
  if (canvas()->width() > w) w = canvas()->width();
  if (canvas()->height() > h) h = canvas()->height();
  if (canvas()->width() < w || canvas()->height() < h)
    canvas()->resize(w, h);
  QCanvasPolygonalItem::moveBy(dx, dy);
}

void SUPERVGUI_CanvasCellNodePrs::drawShape(QPainter& thePainter) {
  drawTitle(thePainter);
  drawLabel(thePainter);
  drawStatus(thePainter);
  drawFrame(thePainter);
}

void drawCellText(QPainter& thePainter, const QString& theText, 
		  const QRect& theRect, int theHAlign = Qt::AlignAuto)
{
  int flags = theHAlign | Qt::AlignVCenter;
  QRect r(theRect.x() + TEXT_MARGIN, theRect.y(), 
	  theRect.width() - 2*TEXT_MARGIN, theRect.height());

  QWMatrix aMat = thePainter.worldMatrix();
  if (aMat.m11() != 1.0) { 
    // for scaled picture only
    QRect r1 = aMat.mapRect(r);
    QFont saved = thePainter.font();
    QFont f(saved);
    if (f.pointSize() == -1) {
      f.setPixelSize((int)(f.pixelSize()*aMat.m11()));
    }
    else {
      f.setPointSize((int)(f.pointSize()*aMat.m11()));
    }
    thePainter.save();
    QWMatrix m;
    thePainter.setWorldMatrix(m);
    thePainter.setFont(f);
    thePainter.drawText(r1, flags, theText);
    thePainter.setFont(saved);
    thePainter.restore();
  }
  else {
    thePainter.drawText(r, flags, theText);
  }
}

void SUPERVGUI_CanvasCellNodePrs::drawTitle(QPainter& thePainter) {
  QBrush saved = thePainter.brush();
  if (getNode()->getEngine()->IsLoop() || getNode()->getEngine()->IsEndLoop()
      ||
      getNode()->getEngine()->IsSwitch() || getNode()->getEngine()->IsEndSwitch())
    thePainter.setBrush(Qt::red.light());
  else if (getNode()->getEngine()->IsGOTO())
    thePainter.setBrush(Qt::green.light());
  else {
    QBrush br( resMgr()->colorValue( "SUPERVGraph", "Title", DEF_MAIN_TITLE ) );
    thePainter.setBrush(br);
  }
  drawTitleShape(thePainter);
  thePainter.setBrush(saved);

  drawCellText(thePainter, getNode()->getEngine()->Name(), getTitleRect(), Qt::AlignLeft);
}

void SUPERVGUI_CanvasCellNodePrs::drawLabel(QPainter& thePainter) 
{
  QRect r = getLabelRect();

  QPen saved = thePainter.pen();
  thePainter.setPen(NoPen);
  thePainter.drawRect(r);
  thePainter.setPen(saved);

  drawCellText(thePainter, getNode()->getLabelText(), r, Qt::AlignLeft);
}

void SUPERVGUI_CanvasCellNodePrs::drawStatus(QPainter& thePainter) 
{
  QRect r = getStatusRect();

  QBrush savedB = thePainter.brush();
  thePainter.setBrush(getStatusColor());
  drawStatusShape(thePainter);
  thePainter.setBrush(savedB);

  drawCellText(thePainter, getStatus(), r, Qt::AlignHCenter);
}

QRect SUPERVGUI_CanvasCellNodePrs::getTitleRect() const
{
  return QRect((int)x(), (int)y(), getTitleWidth(), getTitleHeight());
}

QRect SUPERVGUI_CanvasCellNodePrs::getLabelRect() const
{
  return QRect(((int)x())+getTitleWidth(), (int)y(), getLabelWidth(), getLabelHeight());
}

QRect SUPERVGUI_CanvasCellNodePrs::getStatusRect() const
{
  return QRect(((int)x())+getTitleWidth()+getLabelWidth(), (int)y(),
	       getStatusWidth(), getStatusHeight());
}

int SUPERVGUI_CanvasCellNodePrs::getTitleWidth() const {
  return myTitleWidth;
}

int SUPERVGUI_CanvasCellNodePrs::getLabelWidth() const {
  return myLabelWidth;
}

int SUPERVGUI_CanvasCellNodePrs::getStatusWidth() const {
  return myStatusWidth;
}

int SUPERVGUI_CanvasCellNodePrs::width() const
{
  return myTitleWidth + myLabelWidth + myStatusWidth;
}

int SUPERVGUI_CanvasCellNodePrs::height() const
{
  return getTitleHeight();
}

void SUPERVGUI_CanvasCellNodePrs::setState(SUPERV::GraphState theState)
{
  switch(theState) {
  case SUPERV_Waiting:
    setStatus("Waiting");
    setStatusColor(QColor(35, 192, 255));
    break;

  case SUPERV_Running:
  case SUPERV::ReadyState:
    setStatus("Running");
    setStatusColor(QColor(32,210,32));
    break;

  case SUPERV_Suspend:
  case SUPERV::SuspendReadyState:
    setStatus("Suspended");
    setStatusColor(QColor(255,180, 0));
    break;

  case SUPERV_Done:
    setStatus("Finished");
    setStatusColor(QColor(255, 158, 255));
    break;

  case SUPERV_Error: 
    setStatus("Aborted");
    setStatusColor(Qt::red);
    break;

  case SUPERV_Kill:
    setStatus("Killed");
    setStatusColor(Qt::red);
    break;

  case SUPERV::LoadingState:
    setStatus("Loading");
    setStatusColor(QColor(56,255,56));
    break;

  default:
    setStatus("No Status");
    setStatusColor( resMgr()->colorValue( "SUPERVGraph", "NodeBody", DEF_MAIN_BACK ) ); // mkr : IPAL10829
    break;
  }

  canvas()->setChanged(getStatusRect());
  canvas()->update();
}

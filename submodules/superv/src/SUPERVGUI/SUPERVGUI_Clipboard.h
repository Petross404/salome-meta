//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Clipboard.h
//  Author : Alexander SLADKOV
//  Module : SUPERV

#ifndef SUPERVGUI_Clipboard_H
#define SUPERVGUI_Clipboard_H

#include <qobject.h>

#include "utilities.h"
#include "SALOME_LifeCycleCORBA.hxx"
#include "SUIT_Session.h"
#include "SUIT_Desktop.h"
#include CORBA_CLIENT_HEADER(SUPERV)

class SUPERVGUI_CanvasNode;

class SUPERVGUI_Clipboard : public QObject {

Q_OBJECT

protected:
  // constructor made protected.  Access via getClipboard() member function.
  SUPERVGUI_Clipboard( QObject* parent );
  virtual ~SUPERVGUI_Clipboard();

public:

  static SUPERVGUI_Clipboard* getClipboard() {
    if ( !myCB )
      myCB = new SUPERVGUI_Clipboard( SUIT_Session::session()->activeApplication()->desktop() );
    return myCB;
  }

  static void setNullClipboard() {
    if ( myCB ) myCB = 0;
  }

  bool isCopyPort() const { return ( myCopyPort && !CORBA::is_nil( myCopyPort ) ); }
  SUPERV::Port_var getCopyPort() const            { return myCopyPort;   }
  void setCopyPort( SUPERV::Port_var theObj )     { myCopyPort = theObj; }

  bool isCopyNode() const { return ( myCopyNode && !CORBA::is_nil( myCopyNode ) ); }
  SUPERV::CNode_var getCopyNode() const           { return myCopyNode;   }
  void setCopyNode( SUPERV::CNode_var theObj )    { myCopyNode = theObj; }

public slots:
  void pasteNode();
  void pastePort( SUPERVGUI_CanvasNode* node );

private:
  static SUPERVGUI_Clipboard* myCB;

  SUPERV::Port_var        myCopyPort;
  SUPERV::CNode_var       myCopyNode;
  int                     myXCopyNode;
  int                     myYCopyNode;
};

#endif

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasLink.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasLink_H
#define SUPERVGUI_CanvasLink_H

#include "SUPERVGUI_Def.h"
#include <qcanvas.h>
#include <qvaluevector.h>

class SUPERVGUI_Main;
class SUPERVGUI_CanvasPort;

class SUPERVGUI_CanvasLink : public QObject {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasLink(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::Link_ptr theLink=0);
    virtual ~SUPERVGUI_CanvasLink();

    void show();
    void merge();

    void setHilighted(bool state);
    void setColor(const QColor& theColor);

    virtual void moveByPort(SUPERVGUI_CanvasPort* thePort);
    virtual void moveByPort(SUPERVGUI_CanvasPort* thePort, int dx, int dy);

    SUPERV_Link getEngine() const { return myLink; }
    SUPERVGUI_Main* getMain() const { return myMain; }

    SUPERVGUI_CanvasPort* getInputPort() const { return myInputPort; }
    SUPERVGUI_CanvasPort* getOutputPort() const { return myOutputPort; }

    void setSelectedObject(QCanvasItem* theItem, const QPoint& thePoint);
    virtual QPopupMenu* getPopupMenu(QWidget* theParent);

    QString getToolTipText() const;

  signals:
    void objectCreatedDeleted(); // mkr : PAL8237

  public slots:
    virtual void remove();
    void addPoint();
    void removePoint();

  protected:
    void addPoint(const QPoint& thePoint, const int& theIndex = -1);

    QColor myColor;
    QCanvasItemList myPrs;

  private:
    void createPrs();

    SUPERV_Link     myLink;
    SUPERVGUI_Main* myMain;

    SUPERVGUI_CanvasPort* myInputPort;
    SUPERVGUI_CanvasPort* myOutputPort;

    bool myHilighted;
    QCanvasItem* mySelectedItem;
    QPoint       mySelectedPoint;
};

/*
class SUPERVGUI_CanvasStreamLink : public SUPERVGUI_CanvasLink {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasStreamLink(QObject* theParent, SUPERVGUI_Main* theMain, SUPERV::StreamLink_ptr theLink=0);
    ~SUPERVGUI_CanvasStreamLink() {}

    SUPERV_StreamLink getStreamEngine() const { return myStreamLink; }

  public slots:
    virtual void remove();

  private:
    SUPERV_StreamLink myStreamLink;
};
*/

class SUPERVGUI_CanvasLinkBuilder : public SUPERVGUI_CanvasLink {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasLinkBuilder(QObject* theParent, SUPERVGUI_Main* theMain, SUPERVGUI_CanvasPort* thePort=0);
    virtual ~SUPERVGUI_CanvasLinkBuilder();

    bool canCreateEngine(SUPERVGUI_CanvasPort* thePort);
    void setCoords(SUPERV::Link_ptr theLink);

    SUPERVGUI_CanvasPort* getStartPort() const { return myPort; }

    int getPointCount() const { return (int) (myPrs.count()/2); }
    void addNextPoint(const QPoint& thePoint, bool theOrtho = false);
    void setFloatPoint(const QPoint& thePoint);
    void removeLastPoint();

    virtual void moveByPort(SUPERVGUI_CanvasPort* thePort);
    virtual void moveByPort(SUPERVGUI_CanvasPort* thePort, int dx, int dy);

  private:
    SUPERVGUI_CanvasPort* myPort;

    QCanvasLine* myFloatingEdge;
};


class SUPERVGUI_CanvasEdgePrs;
class SUPERVGUI_CanvasPointPrs : public QCanvasEllipse 
{
  public:
    SUPERVGUI_CanvasPointPrs(QCanvas* theCanvas, SUPERVGUI_CanvasLink* theLink, const int& theIndex = -1);
    ~SUPERVGUI_CanvasPointPrs() {}

    SUPERVGUI_CanvasLink* getLink() const { return myLink; }
    int getIndex() const { return myIndex; }

    void setInEdge(SUPERVGUI_CanvasEdgePrs* theEdge);
    void setOutEdge(SUPERVGUI_CanvasEdgePrs* theEdge);

    void moveBy(double dx, double dy);
    void setColor(const QColor& theColor);

    void setMoving(bool b) { myMoving = b; }
    bool isMoving() const { return myMoving; }

    virtual int rtti() const;

  private:
    SUPERVGUI_CanvasLink* myLink;
    int myIndex;
    bool myMoving;

    SUPERVGUI_CanvasEdgePrs* myInEdge;
    SUPERVGUI_CanvasEdgePrs* myOutEdge;
};


class SUPERVGUI_CanvasEdgePrs : public QCanvasLine 
{
  public:
    SUPERVGUI_CanvasEdgePrs(QCanvas* theCanvas, SUPERVGUI_CanvasLink* theLink);
    ~SUPERVGUI_CanvasEdgePrs() {}

    SUPERVGUI_CanvasLink* getLink() const { return myLink; }

    void setFromPoint(SUPERVGUI_CanvasPointPrs* thePoint);
    void setToPoint(SUPERVGUI_CanvasPointPrs* thePoint);

    void moveBy(double dx, double dy);
    void setColor(const QColor& theColor);

    void setMoving(bool b) { myMoving = b; }
    bool isMoving() const { return myMoving; }

    virtual int rtti() const;

  private:
    bool myMoving;
    SUPERVGUI_CanvasLink* myLink;

    //mkr: for moving segment of link
    SUPERVGUI_CanvasPointPrs* myStartPoint;
    SUPERVGUI_CanvasPointPrs* myEndPoint;
};

#endif

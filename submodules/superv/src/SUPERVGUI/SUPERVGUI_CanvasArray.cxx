//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_CanvasArray.cxx
//  Author : 
//  Module : SUPERV

#include "SUPERVGUI.h"
#include "SUPERVGUI_CanvasArray.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI_BrowseNodeDlg.h"
#include "SUPERVGUI_CanvasCellNodePrs.h"

#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include <qtooltip.h>
#define TEXT_MARGIN 5

#include <iostream.h> //for debug only

#define ADDNODES(NodesName,LevelHasDiffNT) \
    ncols = nodes->NodesName.length(); \
    for (int co=0; co<ncols; co++) {  \
      SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->NodesName[co]); \
      if (!LevelHasDiffNT) x = 50 + co * (CELL_WIDTH  + CELL_SPACE); \
      else x += (CELL_WIDTH  + CELL_SPACE); \
      cell = getCellNode(aCNode); \
      if (cell == NULL) { \
        SUPERVGUI_CanvasCellNode* aNode = SUPERVGUI_CanvasCellNode::Create(myMgr, this, myMain, aCNode); \
        aNode->move(x, y); \
        aNode->show(); \
        aNode->sync(); \
      } else { \
        cell->move(x, y); \
        cell->show(); \
      } \
      update(); \
    }



#define ADDCONTROLNODES(NodesName,LevelHasDiffNT) \
    ncols = nodes->NodesName.length(); \
    if (!LevelHasDiffNT) x = 50; \
    else x += (CELL_WIDTH  + CELL_SPACE); \
    for (int co=0; co<ncols; co++) {  \
      SUPERV_CNode aCNode = SUPERV::CNode::_narrow(nodes->NodesName[co]); \
      SUPERV_CNode aCNodeEnd = SUPERV::CNode::_narrow(nodes->NodesName[co]->Coupled()); \
      cell = getCellNode(aCNode); \
      if (cell == NULL) { \
        SUPERVGUI_CanvasCellNode* aNode = SUPERVGUI_CanvasCellNode::Create(myMgr, this, myMain, aCNode); \
        aNode->move(x, y); \
        aNode->show(); \
        aNode->sync(); \
        y += (CELL_HEIGHT + CELL_SPACE); \
        SUPERVGUI_CanvasCellNode* aNodeEnd = SUPERVGUI_CanvasCellEndNode::Create(myMgr, this, myMain, aCNodeEnd, aNode); \
        aNodeEnd->move(x, y); \
        aNodeEnd->show(); \
        aNodeEnd->sync(); \
      } else { \
        cell->move(x, y); \
        cell->show(); \
        y += (CELL_HEIGHT + CELL_SPACE); \
        cell = getCellNode(aCNodeEnd); \
        cell->move(x, y); \
        cell->show(); \
      } \
      update(); \
      if (co < (ncols-1)) x += (CELL_WIDTH + CELL_SPACE); \
    }


// ----------------------------------------------------------
// SUPERVGUI_Array on QCanvas
// ----------------------------------------------------------

SUPERVGUI_CanvasArray::SUPERVGUI_CanvasArray(SUPERVGUI_Main* m, SUIT_ResourceMgr* mgr ):
  QCanvas(),
  myMain(m),
  myMgr( mgr )
{
  Trace("SUPERVGUI_CanvasArray::SUPERVGUI_CanvasArray");
  //resize(GRAPH_WIDTH, GRAPH_HEIGHT);
  resize(1000, 725);
  setDoubleBuffering(true);

  // mkr : IPAL10825 -->
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }
  QColor aColor =  aSupMod->getIVFBackgroundColor();
  if ( !aColor.isValid() )
    aColor = mgr->colorValue( "SUPERVGraph", "Background", DEF_MAIN_COLOR );
  // <--
  
  //aColor = QColor(SUIT_Session::session()->resourceMgr()->integerValue( "SUPERVGraph", "BackgroundColorRed" ), 
  //		  SUIT_Session::session()->resourceMgr()->integerValue( "SUPERVGraph", "BackgroundColorGreen" ), 
  //		  SUIT_Session::session()->resourceMgr()->integerValue( "SUPERVGraph", "BackgroundColorBlue" ) );
  setBackgroundColor(aColor);
}

SUPERVGUI_CanvasArray::~SUPERVGUI_CanvasArray() {
  Trace("SUPERVGUI_Array::~SUPERVGUI_CanvasArray");
  //delete all cells which SUPERVGUI_CanvasArray contains
  //destroy();
  QObjectList* aCellList = queryList("SUPERVGUI_CanvasCellNode");
  QObjectListIt aIt(*aCellList);
  QObject* anObj;
  while ((anObj = aIt.current()) != 0) {
    ++aIt;
    delete anObj;
  }
  delete aCellList;
}

void SUPERVGUI_CanvasArray::sync() {
  if (SUPERV_isNull(myMain->getDataflow())) return;
  
  SUPERVGUI_CanvasCellNode* ihmNode;
  QObjectList* ihmList = queryList("SUPERVGUI_CanvasCellNode");
  QObjectListIt it(*ihmList);
  while ((ihmNode=(SUPERVGUI_CanvasCellNode*)it.current()) != 0) {
    ++it;
    ihmNode->sync();
  }
  delete ihmList;
}

bool SUPERVGUI_CanvasArray::create() {
  Trace("SUPERVGUI_Array::create");
  SUPERV_Nodes nodes = myMain->getDataflow()->Nodes();
  int aCount = 0;
  aCount += nodes->CNodes.length();
  aCount += nodes->FNodes.length();
  aCount += nodes->INodes.length();
  aCount += nodes->GNodes.length();
  aCount += nodes->LNodes.length();
  aCount += nodes->SNodes.length();
  if (aCount == 0) return true;

  SUPERVGUI_CanvasCellNode* cell;
  QString         aLabel;
  int             x, y;
 
  int             nligs = myMain->getDataflow()->LevelMax();
  int             ncols = myMain->getDataflow()->ThreadsMax();
  
  // there is no any calculations
  if (ncols == 0) return false;

  QPen pen(Qt::SolidLine);
  pen.setWidth(1);

  QBrush br( myMgr->colorValue( "SUPERVGraph", "Title", DEF_MAIN_TITLE ) );
  
  // variables to resize canvas in table view
  int aMaxWidth = 0;
  int aMaxHeight = 0;
  
  for (int co = 0; co < ncols; co++) {
    aLabel = QString("Thread %1").arg(co);
    QRect aRect = QRect(50 + co * (CELL_WIDTH  + CELL_SPACE), 20, CELL_WIDTH, CELL_HEIGHT);
    QCanvasRectangle* aThread = new QCanvasRectangle(aRect, this);
    aThread->setPen(pen);
    aThread->setBrush(br);
    aThread->setZ(0);
    aThread->show();
    
    QCanvasText* aText = new QCanvasText(aLabel, this);
    QRect aBRect = aText->boundingRect();
    aText->setX(aRect.x() + aRect.width()/2 - aBRect.width()/2);
    aText->setY(aRect.y() + aRect.height()/2 - aBRect.height()/2);
    aText->setZ(1);
    aText->show();
    
    aMaxWidth = aRect.x() + aRect.width() + 50;
  }
  
  y = 60;
  bool LevelHasDiffNT = false;
  for (int li = 0; li <= nligs; li++) {
    nodes = myMain->getDataflow()->LevelNodes(li);
    int aSumNum = nodes->CNodes.length()+nodes->FNodes.length()+nodes->INodes.length()+
                  nodes->GNodes.length()+nodes->LNodes.length()+nodes->SNodes.length();
    if (nodes->CNodes.length() != 0 ) {
      ADDNODES(CNodes,LevelHasDiffNT);
      if (aSumNum > nodes->CNodes.length())
	LevelHasDiffNT = true;
    }
    if (nodes->FNodes.length() != 0 ) {
      ADDNODES(FNodes,LevelHasDiffNT);
      if (aSumNum > nodes->FNodes.length())
	LevelHasDiffNT = true;
    }
    if (nodes->INodes.length() != 0 ) {
      ADDNODES(INodes,LevelHasDiffNT);
      if (aSumNum > nodes->INodes.length())
	LevelHasDiffNT = true;
    }
    if (nodes->GNodes.length() != 0 ) {
      ADDNODES(GNodes,LevelHasDiffNT);
      if (aSumNum > nodes->GNodes.length())
	LevelHasDiffNT = true;
    }
    if (nodes->LNodes.length() != 0 ) {
      ADDCONTROLNODES(LNodes,LevelHasDiffNT);
      if (aSumNum > nodes->LNodes.length())
	LevelHasDiffNT = true;
    }
    if (nodes->SNodes.length() != 0 ) {
      ADDCONTROLNODES(SNodes,LevelHasDiffNT);
      if (aSumNum > nodes->SNodes.length())
	LevelHasDiffNT = true;
    }
    y += (CELL_HEIGHT + CELL_SPACE);

    LevelHasDiffNT = false;
  }
  nodeX = 50;
  nodeY = y + CELL_HEIGHT*2;

  aMaxHeight = nodeY;
  
  // if aMaxWidth and aMaxHeight is greater than the current size 
  // of the canvas, we resize canvas to these new width and height
  if ( aMaxWidth > this->width() || aMaxHeight > this->height() )
    this->resize( aMaxWidth > this->width() ? aMaxWidth : this->width(), 
		  aMaxHeight > this->height() ? aMaxHeight : this->height() );
  return true;

}

void SUPERVGUI_CanvasArray::destroy() {
  Trace("SUPERVGUI_Array::destroy");
  
  QObjectList* aCellList = queryList("SUPERVGUI_CanvasCellNode");
  QObjectListIt aIt(*aCellList);
  QObject* anObj;
  while ((anObj = aIt.current()) != 0) {
    ++aIt;
    ((SUPERVGUI_CanvasCellNode*)anObj)->getPrs()->hide();
  }
  delete aCellList;

  // mkr : delete QCanvasRectangle and QCanvasText canvas items for each Thread.
  // Threads will be recreate when create() function is called
  QCanvasItemList aTextList = allItems();
  for (QCanvasItemList::Iterator aItText = aTextList.begin(); aItText != aTextList.end(); ++aItText) {
    SUPERVGUI_CanvasCellNodePrs* aPrs = dynamic_cast<SUPERVGUI_CanvasCellNodePrs*>(*aItText);
    if (!aPrs) {
      (*aItText)->hide();
      delete *aItText;
    }
  }
}

SUPERVGUI_CanvasCellNode* SUPERVGUI_CanvasArray::getCellNode(SUPERV::CNode_ptr theNode) const
{
  return (SUPERVGUI_CanvasCellNode*) 
    ((SUPERVGUI_CanvasArray*)this)->child(theNode->Name(), "SUPERVGUI_CanvasCellNode");
}

SUPERVGUI_CanvasCellNode* SUPERVGUI_CanvasArray::addNode(SUPERV_CNode node) {
  Trace("SUPERVGUI_CanvasArray::addNode");
  SUPERVGUI_CanvasCellNode* n = SUPERVGUI_CanvasCellNode::Create(myMgr, this, myMain, node);
  n->move(nodeX, nodeY);
  n->show();
  update();
  nodeX += NODE_DX;
  nodeY += NODE_DY;
  n->sync();
  return(n);
}

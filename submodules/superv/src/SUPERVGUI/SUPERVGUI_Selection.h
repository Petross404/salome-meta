//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Selection.h
//  Author : Margarita KARPUNINA
//  Module : SUPERV

#ifndef SUPERVGUI_SELECTION_HeaderFile
#define SUPERVGUI_SELECTION_HeaderFile

#include <LightApp_Selection.h>

#include <SALOMEconfig.h>

#include "SALOMEDSClient.hxx"
#include <boost/shared_ptr.hpp>

class SALOME_InteractiveObject;

class SUPERVGUI_Selection : public LightApp_Selection
{
public:
  SUPERVGUI_Selection();
  virtual ~SUPERVGUI_Selection();

  virtual QtxValue      param( const int, const QString& ) const;

private:
  QString               typeName( const int ) const;  

  bool                  isComponent( const int ) const;
  _PTR(SObject)         getObject( const int ) const;
};

#endif

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_Def.h
//  Author : Vitaly SMETANNIKOV
//  Module : SUPERV

#ifndef SUPERVGUI_DEF_H
#define SUPERVGUI_DEF_H


// Nom du noeud de supervision dans l'etude
// ----------------------------------------

#define STUDY_SUPERVISION "SUPERV"

#define STUDY_PORT_IN  "In"
#define STUDY_PORT_OUT "Out"

// Nom d'un nouveau Dataflow
// -------------------------

#define MAIN_NEW "aNewDataFlow"

//ASL: these values are default if resource manager doesn't contain corresponding values
#define DEF_MAIN_COLOR QColor(144, 208, 211)
#define DEF_MAIN_TITLE QColor( 63, 213, 255)
#define DEF_MAIN_CTRL  QColor(255, 108, 106)
#define DEF_MAIN_BACK  QColor(NODE_RED, NODE_GREEN, NODE_BLUE)

// Taille du dataflow sous forme de graphe
// ---------------------------------------

#define GRAPH_WIDTH  3500
#define GRAPH_HEIGHT 3500

#define GRAPH_MARGIN 50

#define GRAPH_MAX    245e5

// Taille du dataflow sous forme de tableau
// ----------------------------------------

#define ARRAY_WIDTH  1500
#define ARRAY_HEIGHT 3000

#define CELL_WIDTH      270
#define CELL_WIDTH_PART  90
#define CELL_HEIGHT      25

#define CELL_SPACE    5

#define MARGIN 2

// Noeuds
// ------
/*
NODE_Waiting   "Waiting",   035,      192,        255,       true,  true,   true,  true   bleu   
NODE_Running   "Running",   032,      210,        032,       true,  true,   true,  true   vert   
NODE_Suspended "Suspended", 255,      180,        000,       false, true,   true,  true   orange 
NODE_Done      "Finished",  255,      158,        255,       true,  false,  true,  false  violet 
NODE_Errored   "Aborted",   255,      000,        000,       true,  false,  true,  false  rouge  
NODE_Killed    "Killed",    255,      000,        000,       true,  false,  b,     !b     rouge  
NODE_Editing   "",          NODE_RED, NODE_GREEN, NODE_BLUE, true,  false,  true,  false  fond   
*/


#define NODE_RED   255
#define NODE_GREEN 249
#define NODE_BLUE  147

#define NODE_DX 35
#define NODE_DY 40

// Ports
// -----

#define PORT_WIDTH  75
#define PORT_HEIGHT 20
#define PORT_MARGIN 2

// Taille des points carres constituant les lignes entre les noeuds
// ----------------------------------------------------------------

#define POINT_SIZE      5
#define POINT_SIZE_HALF 2

// Taille et couleurs des liens
// ----------------------------

#define LINK_WIDTH  2

#define LINK_DATA QColor(  0,   0,   0), LINK_WIDTH
#define LINK_CTRL MAIN_CTRL            , LINK_WIDTH
#define LINK_SKCH QColor(255,   0, 230), LINK_WIDTH

// Taille des labels des noeuds
// ----------------------------

#define LABEL_WIDTH  160
#define LABEL_HEIGHT  20

// Taille de la boite de filtre de notification
// --------------------------------------------

#define NOTIFICATION_WIDTH   250
#define NOTIFICATION_HEIGHT  450

// Names of control ports
// -------------------------
#define OUTVOID "OutVoid"
#define INVOID  "InVoid"

// Convention pour la souris
// -------------------------

#define MouseNo (-2)

// Trace pour l'aide a la mise au point
// ------------------------------------

//#define DEBUG_MODE_COMPIL

#ifdef DEBUG_MODE_COMPIL

#define Trace(m) MESSAGE(m);

#else

#define Trace(m) 

#endif


// Includes de base
// ----------------

#include <stdio.h>

// Includes Qt
// -----------

#include <qobjectlist.h>

#include <qmainwindow.h>
#include <qscrollview.h>

#include <qdialog.h>
#include <qfiledialog.h>
#include <qinputdialog.h>
#include <qmessagebox.h>

#include <qpopupmenu.h>
#include <qtimer.h>
#include <qpainter.h>

#include <qmultilineedit.h>
#include <qlistview.h>
#include <qpushbutton.h>
#include <qcheckbox.h>

//VRV: porting on Qt 3.0.5
#if QT_VERSION >= 0x030005
#include <qcursor.h>
#endif
//VRV: porting on Qt 3.0.5

// Acces au moteur de supervision
// ------------------------------

#include "utilities.h"

#include "SALOME_LifeCycleCORBA.hxx"
#include CORBA_CLIENT_HEADER(SUPERV)
#include CORBA_SERVER_HEADER(SALOMEDS)
#include CORBA_SERVER_HEADER(SALOMEDS_Attributes)

#define SUPERV_Engine  SUPERV::SuperG_var

//#define SUPERV_Graph   SUPERV::StreamGraph_var
#define SUPERV_Graph   SUPERV::Graph_var
#define SUPERV_StreamGraph   SUPERV::StreamGraph_var

#define SUPERV_CNode   SUPERV::CNode_var
#define SUPERV_FNode   SUPERV::FNode_var
#define SUPERV_INode   SUPERV::INode_var
#define SUPERV_SNode   SUPERV::SNode_var
#define SUPERV_LNode   SUPERV::LNode_var
#define SUPERV_GNode   SUPERV::GNode_var
#define SUPERV_ELNode  SUPERV::ELNode_var
#define SUPERV_ESNode  SUPERV::ESNode_var

#define SUPERV_Links   SUPERV::ListOfLinks_var
#define SUPERV_StreamLinks   SUPERV::ListOfStreamLinks_var
#define SUPERV_Ports   SUPERV::ListOfPorts_var
#define SUPERV_StreamPorts   SUPERV::ListOfStreamPorts_var
#define SUPERV_Nodes   SUPERV::ListOfNodes_var
#define SUPERV_Strings SUPERV::ListOfStrings_var

#define SUPERV_Link    SUPERV::Link_var
#define SUPERV_Date    SUPERV::SDate
#define SUPERV_Port    SUPERV::Port_var
#define SUPERV_StreamPort    SUPERV::StreamPort_var
#define SUPERV_StreamLink    SUPERV::StreamLink_var

#define SUPERV_Editing SUPERV::EditingState
#define SUPERV_Running SUPERV::RunningState
#define SUPERV_Suspend SUPERV::SuspendState
#define SUPERV_Done    SUPERV::DoneState
#define SUPERV_Error   SUPERV::ErrorState
#define SUPERV_Kill    SUPERV::KillState
#define SUPERV_Stop    SUPERV::StopState
#define SUPERV_Ready   SUPERV::ReadyState
#define SUPERV_Waiting SUPERV::WaitingState

//#define SUPERV_Event   SUPERV::GraphEvent
//#define SUPERV_State   SUPERV::GraphState

#define SUPERV_isNull(oc) (CORBA::is_nil(oc))


enum GraphViewType { CONTROLFLOW, CANVAS, CANVASTABLE };
//enum NodeType { COMPUTE, SWITCH, LOOP, GOTO, LABEL };


#endif

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI.h
//  Author : Francis KLOSS
//  Module : SUPERV

#ifndef SUPERVGUI_H
#define SUPERVGUI_H

// Acces a l`ihm de SALOME
// -----------------------

#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_Service.h"
#include "SUPERVGUI_Main.h"

#include "SalomeApp_Module.h"
#include "SALOMEDSClient.hxx"

class SUIT_Study;

// Definition de la classe principale du module de supervision
// -----------------------------------------------------------

class SUPERVGUI: public SalomeApp_Module {
  Q_OBJECT

  public:
    SUPERVGUI();
    virtual ~SUPERVGUI();

    static SUPERVGUI*  Supervision();

    virtual void       initialize( CAM_Application* );
    virtual QString    engineIOR() const;
    SUPERV_Engine      getEngine();
    SUIT_Study*        getActiveStudy() { return study; }

    SUPERVGUI_Service* getBrowser();
    QCursor            getCursor();
    //bool information(SUPERV_CNode node, bool isReadOnly);
    void putInfo(const char* mess);

    void importDataflow();
    void exportDataflow();
    void newDataflow();
    void modifyDataflow();
    void reloadDataflow();
    void runDataflow();
    void stepByStep();
    void killDataflow();
    void suspendResumeDataflow();
    void showComponents();

    SUPERVGUI_Main* getMain();

    bool OnGUIEvent(int theCommandID);
    //bool SetSettings();
    //void DefinePopup(QString & theContext,
    //                 QString & theParent, 
    //                 QString & theObject ) ;
    //bool CustomPopup(QPopupMenu* popup,
    //                 const QString & theContext,
    //                 const QString & theParent,
    //                 const QString & theObject);
    bool ActiveStudyChanged();
    //void SupportedViewType (int* buffer, int bufferSize);

    bool isContains(SUIT_Study* theStudy, const QString theKey);

    void registerGraph(QString theName, SUPERVGUI_Main* theGraph);
    void unregisterGraph(SUPERVGUI_Main* theGraph);
    SUIT_ViewWindow* createGraph();

    /** Returns:
	theIsOwner = true if Selected object belongs to Suipervision.
	theIsDataflow = true if Selected object is Dataflow
    */
    void whatIsSelected(const _PTR(SObject)& theObj, bool& theIsOwner, bool& theIsDataflow);

    /* asv : bug [VSR Bugs and Improvements in Supervisor] 1.8 : when exporting a file, 
       a backup copy of an existing file must be created (in case Export fails..)
       This method creates a backup file and returns its name or QString::null if it fails
       Function is static because it is called from 
       SUPERVGUI::exportDataflow() and SUPERVGUI_CanvasMacroNode::exportDataflow()    
    */
    static QString createBackupFile( const QString& theOriginalFileName );

    virtual void windows( QMap<int, int>& ) const;

    QMap<QString, QString> getInterfaceNameMap() const;

    /*!
     * \brief The method nullify myInitalViewFrame variable
     * to avoid using this view frame once more, it is using
     * after add any node into first new dataflow in study.
     */
    void nullifyInitialVF() { if ( myInitialViewFrame ) myInitialViewFrame = 0; }

    // mkr : IPAL10825
    QColor getIVFBackgroundColor() { return myIVFBackgroundColor; }
    void   setIVFBackgroundColor( QColor theColor ) { if ( myInitialViewFrame ) myIVFBackgroundColor = theColor; }

    // mkr : PAL7037
    /*!
     * \brief The method update name of the given dataflow, if this dataflow is in study.
     * Returns true if update was done.
     */
    bool updateDataFlowSOName( SUPERV::Graph_ptr theDataflow );

  signals:
    void KillMainThread(bool theValue);

  private slots:
    void setMain(SUIT_ViewWindow* w);
    void displayDataflow();
    void renameDataflow();
    void deleteObject();
    void onGraphClosed( SUIT_ViewWindow* );
    void OnGUIEvent();

  public slots:
    virtual bool           activateModule( SUIT_Study* );
    virtual bool           deactivateModule( SUIT_Study* );
      
  protected:
    virtual CAM_DataModel* createDataModel();
    virtual LightApp_Selection* createSelection() const;

  private:
    void loadEngine(SALOME_NamingService* namingService);

    // enumeration used in newDataflow() method
    enum NEW_DF_MODE { Import, Modify, New }; 
    // method called from importDataflow(), modifyDataflow(), newDataflow() with
    // different modes.  The behavior of the function is very similar in all 3 cases, 
    // but some differences still exist.  These differences are implemented using NEW_DF_MODE param.
    bool createDataflow( const NEW_DF_MODE f, bool theInitialDF = false );  

    void createSupervAction( const int id, const QString& po_id, const QString& icon_id = QString(""), 
			     const int key = 0, const bool toggle = false );

    void fillInterfaceNameMap();

    static int             factory;
    SUIT_Study*            study;
    SUPERV_Engine          engine;
    SUPERVGUI_Service*     browser;
    //SUPERVGUI_Information* info;
    QCursor                cursor;

    SUPERVGUI_Main*        main;  // active main    
    QPtrList<SUPERVGUI_Main> myGraphList;
    QMap<QString, QString> myInterfaceNameMap;

    bool                   myFirstActivation;
    bool                   myAllGraphsClosed;
    SUPERVGraph_ViewFrame* myInitialViewFrame;
    QColor                 myIVFBackgroundColor; // mkr : IPAL10825
};

#endif

// Copyright (C) 2005  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either 
// version 2.1 of the License.
// 
// This library is distributed in the hope that it will be useful 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public  
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
#include "SUPERVGUI_Selection.h"
#include "SUPERVGUI.h"

#include <LightApp_DataOwner.h>
#include <SalomeApp_Study.h>

#include <SUIT_Session.h>
#include <SUIT_ViewWindow.h>
#include <SUIT_ViewManager.h>
#include <SUIT_ViewModel.h>

#include <SALOMEDSClient_SObject.hxx>
#include <SALOMEDSClient_Study.hxx>
#include <SALOMEDS_SObject.hxx>
#include <SALOME_Prs.h>
#include <SALOME_InteractiveObject.hxx>

#include <SOCC_Prs.h>

#include <OCCViewer_ViewModel.h>

SUPERVGUI_Selection::SUPERVGUI_Selection()
{
}

SUPERVGUI_Selection::~SUPERVGUI_Selection()
{
}

QtxValue SUPERVGUI_Selection::param( const int ind, const QString& p ) const
{
  QtxValue val( LightApp_Selection::param( ind, p ) );
  if ( !val.isValid() ) {
    if ( p == "type" )  val = QtxValue( typeName( ind ) );
  }

  //printf( "--> param() : [%s] = %s\n", p.latin1(), val.toString ().latin1() );

  return val;
}

QString SUPERVGUI_Selection::typeName( const int index ) const
{
  if ( isComponent( index ) )
    return "Component";
  
  _PTR(SObject) anIObj = getObject( index );
  if ( anIObj ) {
    bool aIsOwner, aIsDataflow;
    SUPERVGUI::Supervision()->whatIsSelected(anIObj, aIsOwner, aIsDataflow);
    if ( aIsDataflow ) //selected dataflow object
      return "Dataflow";
    if ( aIsOwner ) //selected object belongs to Supervisor
      return "SupervisorObject";
  }
  return "Unknown";
}

bool SUPERVGUI_Selection::isComponent( const int index ) const
{
  SalomeApp_Study* appStudy = dynamic_cast<SalomeApp_Study*>
    (SUIT_Session::session()->activeApplication()->activeStudy());

  if ( appStudy && index >= 0 && index < count() )  {
    _PTR(Study) study = appStudy->studyDS();
    QString anEntry = entry( index );

    if ( study && !anEntry.isNull() ) { 
      _PTR(SObject) aSO( study->FindObjectID( anEntry.latin1() ) );
      if ( aSO && aSO->GetFatherComponent() ) 
	return aSO->GetFatherComponent()->GetIOR() == aSO->GetIOR();
    }
  }
  return false;
}

_PTR(SObject) SUPERVGUI_Selection::getObject( const int index ) const
{
  SalomeApp_Study* appStudy = dynamic_cast<SalomeApp_Study*>
    (SUIT_Session::session()->activeApplication()->activeStudy());

  if ( appStudy && index >= 0 && index < count() )  {
    _PTR(Study) study = appStudy->studyDS();
    QString anEntry = entry( index );

    if ( study && !anEntry.isNull() ) { 
      _PTR(SObject) aSO( study->FindObjectID( anEntry.latin1() ) );
      return aSO;
    }
  }
  
  _PTR(SObject) aObj1;
  return aObj1;
}

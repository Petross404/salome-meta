//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Clipboard.cxx
//  Author : Alexander SLADKOV
//  Module : SUPERV


#include "SUPERVGUI_Clipboard.h"
#include "SUPERVGUI_CanvasNode.h"
#include "SUPERVGUI.h"


SUPERVGUI_Clipboard* SUPERVGUI_Clipboard::myCB = 0;


/**
 * Compute the next valid name for a Python function (add "_N" if a function with given name already exists)
 */
QString getNewName( QStringList& allNames, const QString& oldName ) {
  QString newName;
  int id = 1; //increment index
  newName = oldName + QString("_") + QString::number( id );
  while ( allNames.contains( newName ) )
    newName = oldName + QString("_") + QString::number( ++id );
  
  return newName;
}

/**
 * Replaces origName string with newName string in all lines of theFunc
 * origName must be preceeded by space and end by space or '('.
 * asv : 14.01.05 : fix of a bug (exception on node creation): 
 *      if origName and theFunc is null, return non-null empty strings!
 */
void replaceName( SUPERV::ListOfStrings_var& theFunc, const QString& origName, const QString& newName ) {
  for ( int i = 0, n = theFunc->length(); i < n; i++ ) {
    QString aLine( theFunc[i] );
    int index = aLine.find( origName, 0 ); // find FIRST occurance of origName in aLine
    while ( index >= 0 ) {
      bool preceedingCharOk = ( index==0 || ( index > 0 && aLine[index-1].isSpace() ) );
      const int ll = aLine.length();
      const int ol = origName.length();
      const int ni = index + ol;
      bool nextCharOk = ( ll==ni || ( ll>ni && ( aLine[ni].isSpace() || aLine[ni]=='(' ) ) );
      if ( preceedingCharOk && nextCharOk ) {
	aLine = aLine.replace( index, origName.length(), newName );
	theFunc[i] = aLine.latin1();
      }
      index = aLine.find( origName, index+newName.length() ); // find NEXT occurance of origName in aLine
    }
  }
}

/**
 * "Copies" all ports from fromNode to toNode: creates the sames ports in toNode in fact
 */
void copyPorts( const SUPERV::CNode_var& fromNode, const SUPERV::INode_var& toNode ) {
  if ( CORBA::is_nil( fromNode ) || CORBA::is_nil( toNode ) )
    return;
  SUPERV::ListOfPorts_var aPList = fromNode->Ports();
  QString aName, aType;
  for (int i = 0; i < aPList->length(); i++) {
    aName = aPList[i].in()->Name();
    aType = aPList[i].in()->Type();
    if ( aPList[i].in()->IsInput() )
      toNode->InPort( aName.latin1(), aType.latin1() );
    else
      toNode->OutPort( aName.latin1(), aType.latin1() );
  }
}

/**
 * Constructor
 */
SUPERVGUI_Clipboard::SUPERVGUI_Clipboard( QObject* parent ) 
: QObject( parent ) {
}

/**
 * Destructor
 */
SUPERVGUI_Clipboard::~SUPERVGUI_Clipboard() {
}

/**
 * Returns all inline functions defined in inline (switch, loop, goto) nodes of given dataflow
 */
QStringList getAllFunctions( SUPERV::Graph_var dataflow ) {
  QStringList aFuncNames;
  if ( !CORBA::is_nil( dataflow ) ) {
    SUPERV::ListOfNodes_var nodes = dataflow->Nodes();
    //InLine nodes
    for(int i = 0; i < nodes->INodes.length(); i++)
      aFuncNames.append(nodes->INodes[i]->PyFuncName());
    //Loop nodes
    for(int i = 0; i < nodes->LNodes.length(); i++) {
      aFuncNames.append(nodes->LNodes[i]->PyInitName());
      aFuncNames.append(nodes->LNodes[i]->PyMoreName());
      aFuncNames.append(nodes->LNodes[i]->PyNextName());
    }
    //Switch nodes
    for(int i = 0; i < nodes->SNodes.length(); i++)
      aFuncNames.append(nodes->SNodes[i]->PyFuncName());
    //GOTO nodes
    for(int i = 0; i < nodes->GNodes.length(); i++)
      aFuncNames.append(nodes->GNodes[i]->PyFuncName());
  }
  return aFuncNames;
}

/**
 * Called on Paste Node command.  Inserts a new node to the dataflow equal to the copied node.
 * For InLine nodes the Python function name is changed ("_N" added).
 */
void SUPERVGUI_Clipboard::pasteNode() {
  Trace("SUPERVGUI_Main::pasteNode");
  SUPERV::CNode_var aNode = getCopyNode();
  
  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }
  
  SUPERVGUI_Main* aMain = aSupMod->getMain();
  if ( !CORBA::is_nil( aNode ) && aMain ) {
    
    if ( !aMain->ReadyToModify() ) // null dataflow or executing, ..
      return;

    aMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag, why here? -> PAL7960

    SUPERV::Graph_var dataflow = aMain->getDataflow();

    switch ( aNode->Kind() ) {

    case SUPERV::FactoryNode : 
      {
        SUPERV::FNode_var aNodeFNode = SUPERV::FNode::_narrow(aNode);
        SALOME_ModuleCatalog::ImplType anImplType = SALOME_ModuleCatalog::SO;
        if (!aNodeFNode->IsCimpl()) anImplType = SALOME_ModuleCatalog::PY;
	SUPERV::FNode_var aFNode = dataflow->FNode(aNodeFNode->GetComponentName(),
                                                   aNodeFNode->GetInterfaceName(),
                                                   *aNodeFNode->Service(),
                                                   anImplType); // mkr : PAL11273
	if (CORBA::is_nil(aFNode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}

	SUPERV::INode_var aDummyEndNode;
	aSupMod->getBrowser()->addNode(SUPERV::CNode::_narrow(aFNode), aDummyEndNode, myXCopyNode, myYCopyNode);
      }
      break;

    case SUPERV::ComputingNode : 
      {
	SUPERV::CNode_var aCNode = dataflow->CNode(*SUPERV::CNode::_narrow(aNode)->Service());
	if (CORBA::is_nil(aCNode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}
	
	SUPERV::INode_var aDummyEndNode;
	aSupMod->getBrowser()->addNode(aCNode, aDummyEndNode, myXCopyNode, myYCopyNode);
      }
      break;

    case SUPERV::InLineNode : 
      {
	QString aFName;                  // new node's Py_function name
	SUPERV::ListOfStrings_var aFunc; // new node's Py_function body

	// Automatic change of Py_function name ( + "_1", etc.)
	// 1. collect all python functions names of allready exist InLine nodes 
	QStringList aFuncNames = getAllFunctions( dataflow );
	// 2. "fix" Main function_name and Main function_strings
	QString aOriginalName = SUPERV::INode::_narrow(aNode)->PyFuncName();
	if ( !aOriginalName.isEmpty() ) {
	  aFName = getNewName( aFuncNames, aOriginalName );
	  aFunc = SUPERV::INode::_narrow(aNode)->PyFunction();
	  replaceName( aFunc, aOriginalName, aFName );
	} 
	else { // empty function name and body
	  aFName = QString( "" ); 
	  aFunc = new SUPERV::ListOfStrings(); 
	}

	// create the Engine's node
	SUPERV::INode_var aINode = dataflow->INode( aFName, aFunc );

	if (CORBA::is_nil(aINode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}
	copyPorts( aNode, aINode );

	SUPERV::INode_var aDummyEndNode;
	aSupMod->getBrowser()->addNode(SUPERV::CNode::_narrow(aINode), aDummyEndNode, myXCopyNode, myYCopyNode);
      }
      break;

    case SUPERV::LoopNode :
      {
	QString aInitFName, aMoreFName, aNextFName;                // new node's Py_functions names
	SUPERV::ListOfStrings_var aInitFunc, aMoreFunc, aNextFunc; // new node's Py_functions bodies

	// Automatic change of Py_function name ( + "_1", etc.)
	// 1. collect all python functions names of allready exist InLine nodes 
	QStringList aFuncNames = getAllFunctions( dataflow );
	// 2.1 "fix" Init function_name and Init function_strings
	QString aOriginalName = SUPERV::LNode::_narrow(aNode)->PyInitName();
	if (!aOriginalName.isEmpty()) {
	  aInitFName = getNewName( aFuncNames, aOriginalName );
	  aInitFunc = SUPERV::LNode::_narrow(aNode)->PyInit();
	  replaceName( aInitFunc, aOriginalName, aInitFName );
	}
	else { // empty function name and body
	  aInitFName = QString( "" ); 
	  aInitFunc = new SUPERV::ListOfStrings(); 
	}
	// 2.2 "fix" More function_name and More function_strings
	aOriginalName = SUPERV::LNode::_narrow(aNode)->PyMoreName();
	if (!aOriginalName.isEmpty()) {
	  aMoreFName = getNewName( aFuncNames, aOriginalName );
	  aMoreFunc = SUPERV::LNode::_narrow(aNode)->PyMore();
	  replaceName( aMoreFunc, aOriginalName, aMoreFName );
	}
	else { // empty function name and body
	  aMoreFName = QString( "" ); 
	  aMoreFunc = new SUPERV::ListOfStrings(); 
	}
	// 2.3 "fix" Next function_name and Next function_strings
	aOriginalName = SUPERV::LNode::_narrow(aNode)->PyNextName();
	if (!aOriginalName.isEmpty()) {
	  aNextFName = getNewName( aFuncNames, aOriginalName );
	  aNextFunc = SUPERV::LNode::_narrow(aNode)->PyNext();
	  replaceName( aNextFunc, aOriginalName, aNextFName );
	}
	else { // empty function name and body
	  aNextFName = QString( "" ); 
	  aNextFunc = new SUPERV::ListOfStrings(); 
	}

	// create the Engine's node
	SUPERV::INode_var aEndNode;
	SUPERV::LNode_var aStartNode = dataflow->LNode(aInitFName, aInitFunc, aMoreFName, aMoreFunc, aNextFName, aNextFunc, aEndNode);
	if (CORBA::is_nil(aStartNode) || CORBA::is_nil(aEndNode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}
	copyPorts( aNode, SUPERV::INode::_narrow( aStartNode ) );

	aSupMod->getBrowser()->addNode(SUPERV::CNode::_narrow(aStartNode), aEndNode, myXCopyNode, myYCopyNode);
      }
      break;

    case SUPERV::SwitchNode :
      {
	QString aFName;                  // new node's Py_function name
	SUPERV::ListOfStrings_var aFunc; // new node's Py_function body

	// Automatic change of Py_function name ( + "_1", etc.)
	// 1. collect all python functions names of allready exist InLine nodes 
	QStringList aFuncNames = getAllFunctions( dataflow );
	// 2. "fix" Main function_name and Main function_strings
	QString aOriginalName = SUPERV::INode::_narrow(aNode)->PyFuncName();
	if ( !aOriginalName.isEmpty() ) {
	  aFName = getNewName( aFuncNames, aOriginalName );
	  aFunc = SUPERV::INode::_narrow(aNode)->PyFunction();
	  replaceName( aFunc, aOriginalName, aFName );
	} 
	else { // empty function name and body
	  aFName = QString( "" ); 
	  aFunc = new SUPERV::ListOfStrings(); 
	}

	// create the Engine's node
	SUPERV::INode_var aEndNode;
	SUPERV::SNode_var aStartNode = dataflow->SNode(aFName, aFunc, aEndNode);
	if (CORBA::is_nil(aStartNode) || CORBA::is_nil(aEndNode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}
	copyPorts( aNode, SUPERV::INode::_narrow( aStartNode ) );
	SUPERV::INode_var aNodeEnd = SUPERV::SNode::_narrow(aNode)->Coupled();
	copyPorts( SUPERV::CNode::_narrow( aNodeEnd ), aEndNode );

	aSupMod->getBrowser()->addNode(SUPERV::CNode::_narrow(aStartNode), aEndNode, myXCopyNode, myYCopyNode);
      }
      break;

    case SUPERV::GOTONode :
      {
	QString aFName;                  // new node's Py_function name
	SUPERV::ListOfStrings_var aFunc; // new node's Py_function body

	// Automatic change of Py_function name ( + "_1", etc.)
	// 1. collect all python functions names of allready exist InLine nodes 
	QStringList aFuncNames = getAllFunctions( dataflow );
	// 2. "fix" Main function_name and Main function_strings
	QString aOriginalName = SUPERV::INode::_narrow(aNode)->PyFuncName();
	if ( !aOriginalName.isEmpty() ) {
	  aFName = getNewName( aFuncNames, aOriginalName );
	  aFunc = SUPERV::INode::_narrow(aNode)->PyFunction();
	  replaceName( aFunc, aOriginalName, aFName );
	} 
	else { // empty function name and body
	  aFName = QString( "" ); 
	  aFunc = new SUPERV::ListOfStrings(); 
	}

	// create the Engine's node
	SUPERV::GNode_var aGNode = dataflow->GNode(aFName, aFunc, "");
	if (CORBA::is_nil(aGNode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}
	copyPorts( aNode, SUPERV::INode::_narrow( aGNode ) );

	SUPERV::INode_var aDummyEndNode;
	aSupMod->getBrowser()->addNode(SUPERV::CNode::_narrow(aGNode), aDummyEndNode, myXCopyNode, myYCopyNode);
      }
      break;

    case SUPERV::MacroNode :
      {
	/* to implement in the future */
	/*
	//get SubGraph from MacroNode
	SUPERV::Graph_var aMacro = SUPERV::Graph::_narrow(aNode);
	SUPERV::Graph_var aGraph;
	if (aMacro->IsStreamMacro())
	  aGraph = aMacro->StreamObjRef();
	else
	  aGraph = aMacro->FlowObjRef();
	SUPERV::Graph_var aMacroNode = dataflow->GraphMNode(aGraph);
	
	if (CORBA::is_nil(aMacroNode)) {
	  QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_CANT_CREATE_NODE"));
	  return;
	}

	SUPERV::INode_var aDummyEndNode;
	aSupMod->getBrowser()->addNode(SUPERV::CNode::_narrow(aMacroNode), aDummyEndNode, myXCopyNode, myYCopyNode);
	*/
      }
      break;
    }
  }
}


/** 
 * Called from CanvasNode on "Paste port" command of popup menu
 */
void SUPERVGUI_Clipboard::pastePort( SUPERVGUI_CanvasNode* node )
{
  SUPERV::Port_var aPort = getCopyPort();

  SUPERVGUI* aSupMod = SUPERVGUI::Supervision();
  if ( !aSupMod ) {
    MESSAGE("NULL Supervision module!");
    return;
  }

  SUPERVGUI_Main* aMain = aSupMod->getMain();
  if ( !CORBA::is_nil(aPort) && aMain ) {

    SUPERV::INode_var aNode = node->getInlineNode();
    if (!CORBA::is_nil(aNode)) {
      QString aName = aPort->Name();
      QString aType = aPort->Type();
      SUPERV::Port_var aPastePort;
      if (aPort->IsInput()) {
	//check if port with such name is alredy exists
	QStringList aNames = node->getPortsNamesIN(aNode, true);
	if (aNames.contains(aName))
	  QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_PORT_EXIST") );
	else {
	  aMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag, why here? -> PAL7960
	  aPastePort = aNode->InPort(aName.latin1(), aType.latin1());
	}
      }
      else {
	//check if port with such name is already exists
	QStringList aNames = node->getPortsNamesIN(aNode, false);
	if (aNames.contains(aName))
	  QMessageBox::warning( SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_PORT_EXIST") );
	else {
	  aMain->Editing(); // PAL6170: GUI->Engine: setting "Editing" flag, why here? -> PAL7960
	  aPastePort = aNode->OutPort(aName.latin1(), aType.latin1());
	}
      }
      if ( !CORBA::is_nil(aPastePort) )
	node->createPort( aPastePort.in() );
    }
  }
}

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Main.h
//  Author : Francis KLOSS
//  Module : SUPERV

#ifndef SUPERVGUI_Main_H
#define SUPERVGUI_Main_H

#include "SUPERVGraph_ViewFrame.h"
#include "SUPERVGUI_Canvas.h"
#include "SUPERVGUI_CanvasView.h"
#include "SUPERVGUI_ArrayView.h"

#include <qobject.h>
#include <qapplication.h>
#include <qthread.h>

class SUIT_Study;
class OB_Browser;
class LogWindow;
class QtxDblSpinBox;
class QLineEdit;
class QComboBox;
class NOTIFICATION_Consumer;
class SUPERVGUI_Thread;

class SUPERVGUI_Main: public SUPERVGraph_View {
  Q_OBJECT

  // asv : 18.11.04 : making Thread class a friend in order to allow it to modify
  // a private field myGUIEventLoopFinished
  friend class SUPERVGUI_Thread;

  public:
    SUPERVGUI_Main(SUPERVGraph_ViewFrame*, SUIT_Desktop*/*QAD_Desktop**/, SUPERV_Graph);
    virtual ~SUPERVGUI_Main();

    SUIT_ResourceMgr* resMgr() const;

    void run( const bool andSuspend );
    void kill();
    void suspendResume();
    //void stopRestart();
    void openSubGraph(SUPERV_CNode theNode, bool correct = false);
    void destroySubGraph(QString theSubGraphName); // mkr : IPAL11549

    bool putDataStudy(SUPERV_Port port, const char* inout);

    SUPERVGUI_CanvasArray*  getCanvasArray();
    SUPERVGUI_ArrayView* getArrayView();
    SUPERVGUI_Canvas* getCanvas();
    SUPERVGUI_CanvasView* getCanvasView();
    SUPERV_Graph getDataflow();
    LogWindow* getMessage();
    SUIT_Study* getStudy();
    bool isArrayShown();
    void showPopup(QPopupMenu* p, QMouseEvent* e);

    void ActivatePanning();
    void ResetView();

    void setHashCode(QString theCode) 
      { myHashCode = theCode; };

    QString getHashCode() 
      { return myHashCode; };
    
    bool isDataflowInStudy() const;
    
    bool isEditable() 
      { if (SUPERV_isNull(dataflow)) return false;
      return !dataflow->IsReadOnly(); };

    void addComputeNode(SUPERV_CNode theNode);
    void addControlNode(SUPERV_CNode theStartNode, SUPERV_CNode theEndNode, bool Update);
    void addGOTONode(SUPERV_CNode theNode);
    void addMacroNode(SUPERV_CNode theNode);

    void setPaletteBackgroundColor(const QColor& color);

    void lockedGraph(bool theLock) { myIsLocked = theLock; }
    bool isLocked() { return myIsLocked; }

    SUPERV::GraphState getNodeExecState();
    void setNodeExecState(SUPERV::GraphState theNodeExecState);

    QPtrList< char * > getEventNodes();
    void setEventNodes(QPtrList< char * > theEventNodes);
    bool removeFirstEN() { return myEventNodes.removeFirst(); }
    void removeEventNodes() { myEventNodes.clear(); }

    QPtrList< SUPERV::GraphState > getStates();
    void setStates(QPtrList< SUPERV::GraphState > theStates);
    bool removeFirstS() { return myStates.removeFirst(); }
    void removeStates() { myStates.clear(); }

    int getNodesNumber();
    SUPERVGUI_Thread* getMyThread();

    void startTimer();
    void executionFinished();

    bool eventFilter( QObject* o, QEvent* e);

    void Editing(); // any Editing operation is to be performed ->
		    // activate Editors data model in Engine
    bool ReadyToModify(); // must be called before any modification
		    // operation - asks to kill execution of dataflow.  If returns false -
		    // modification (==Editing() ) is not allowed.

    void removeArrayChild(SUPERV::CNode_ptr theNode);

    GraphViewType getViewType() { return myCurrentView; }

    virtual void resizeView( QResizeEvent* theEvent );

    // mkr : PAL8150
    void setRunTime( QDateTime theRunTime ) { myRunTime = theRunTime; }
    QDateTime getRunTime() const { return myRunTime; }

    // srn: marks an active study as being modified by the Supervisor
    // Note: to set the modified flag the method requires that Supervision
    //       component was previously added to study.
    static void setModifiedFlag();

  signals:
    void KillMyThread(bool theValue);
    void EventToSync(); // mkr : IPAL11362

  public slots:
    void execute(char * theNodeNode, SUPERV::GraphState theNodeState);
    void sync();
    void syncAsync();
    void addDataflowToStudy();
    void insertFile();
    void addNode();
    void changeInformation();
    void copy();

    void showCanvasTable();
    void showContolFlow();
    void showCanvas();

    void filterNotification();
    void changeDSGraphParameters();
    void onSubGraphClosed( SUIT_ViewWindow* );
    void onSubGraphActivated( SUIT_ViewWindow* );

    void onShowToolbar();

    void onObjectCreatedDeleted(); // mkr : PAL8237
    void onPutInStudy( QString* ); // mkr : PAL8150
 
  private slots:
    void chooseData(QListViewItem* item);
    void checkExecution();

  private:
    void init(SUIT_Desktop* parent);
    void syncNotification();
    bool isFiltered(char* graph, char* node, char* type, char* message, char* sender, long counter, char* date, long stamp);
    void closeEvent(QCloseEvent*);
    void syncPutInStudy(); // mkr : PAL8150
    
    SUPERV_Graph            dataflow;

    QMap<QString, SUIT_ViewWindow*> mySubGraphs;
    QMap<QString, QString>  mySubGraphMap;
    SUIT_ViewWindow*        myLastGraph;

    SUIT_Study*             study;
    OB_Browser*             objectBrowser;
    LogWindow*              message;

    GraphViewType           myCurrentView;
    SUPERVGUI_CanvasArray*  myArray;
    SUPERVGUI_ArrayView*    myArrayView;
    SUPERVGUI_Canvas*       myCanvas;
    SUPERVGUI_CanvasView*   myCanvasView;

    bool                    choosing;

    QString                 myHashCode;
    
    bool                    myIsFromStudy;
    int                     myCopyNum;

    SALOME_NamingService*   myNService;
    QDateTime               myRunTime; // mpv 23.12.2002: we need time of running of dataflow
                                       // for correct Study document modification
    /* notification data */			       
    NOTIFICATION_Consumer*  notification;
    bool                    myFiltered;
    bool                    myLogged;
    QString                 myLogFileName;
    FILE*                   myLogFile;
    bool                    myWarning;
    bool                    myStep;
    bool                    myTrace;
    bool                    myVerbose;
    bool                    myIsLocked;

    SUPERVGUI_Thread*       myThread; 

    QPtrList<char*>         myEventNodes ;
    QPtrList<SUPERV::GraphState> myStates ;
    QTimer*                 myTimer;
};

class SUPERVGUI_Thread : public QObject, public QThread {
  Q_OBJECT;
 public:
  SUPERVGUI_Thread();
  ~SUPERVGUI_Thread();

  void startThread(const char* m);
  void setMain(SUPERVGUI_Main* theMain);

 public slots:
   void KillThread(bool theValue); 
  
 protected:
  virtual void run();
  void main_thread_run(SUPERV_CNode& aNode, SUPERV::GraphEvent& aEvent, SUPERV::GraphState& aState);

 private:
  bool                myIsActive;
  SUPERVGUI_Main*     myMain;
  QMutex              myMutex;

};

/**
 * A dialog for setting Graph execution parameters
 */
class SUPERVGUI_DSGraphParameters: public QDialog {
  Q_OBJECT

  public:
    SUPERVGUI_DSGraphParameters(SUPERV_Graph theGraph, bool isReadOnly);
    virtual ~SUPERVGUI_DSGraphParameters();

  private slots:
    void accept();

  private:

    void setData();

    QtxDblSpinBox*       myDeltaTime;
    QLineEdit*           myTimeOut;
    QComboBox*           myDataStreamTrace;

    SUPERV_Graph    myGraph;
};

#endif

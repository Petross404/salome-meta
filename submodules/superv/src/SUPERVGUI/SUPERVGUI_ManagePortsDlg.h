//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_ManagePortsDlg.h
//  Author : Alexander SLADKOV
//  Module : SUPERV

#ifndef SUPERVGUI_ManagePortsDlg_H
#define SUPERVGUI_ManagePortsDlg_H

#include "SUPERVGUI.h"

#include <qdialog.h>
#include <qlistbox.h>
#include <qstringlist.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qpair.h>

class SUPERVGUI_CanvasNode;

/** 
 * Simple redefinition of ListBoxItem for storing ports name and type without having to parce it
 */
class PortListItem : QListBoxText {
public:
  PortListItem ( QListBox* lb, const QString& name, const QString& type )
  : QListBoxText( lb/*, QString( "%1 (%2)" ).arg( name ).arg( type )*/ ),
    PortName( name ),
    PortType( type ) {}
  QString PortName;
  QString PortType;
  virtual QString text() const { return QString( "%1 (%2)" ).arg( PortName ).arg( PortType ); }
};

/**
 * The dialog class
 */
class SUPERVGUI_ManagePortsDlg: public QDialog {
  
  Q_OBJECT

public:
  SUPERVGUI_ManagePortsDlg( SUPERVGUI_CanvasNode* theNode );
  virtual ~SUPERVGUI_ManagePortsDlg();

protected slots:
  void accept();
  void reject();

  void addInput()     { addPort( myInList ); }    
  void addOutput()    { addPort( myOutList );}
  void removeInput()  { removePort( myInList ); }
  void removeOutput() { removePort( myOutList );}
  void upInput()      { moveUp( myInList ); }
  void upOutput()     { moveUp( myOutList );}
  void downInput()    { moveDown( myInList ); }
  void downOutput()   { moveDown( myOutList );}

  void nameChanged( const QString& name );
  void typeChanged( const QString& type );
  void itemChanged( QListBoxItem * item );
  void itemDeselect(QListBoxItem* item, const QPoint& point); // mkr: PAL12448

protected:
  void addPort( QListBox* );
  void removePort( QListBox* );
  void moveUp( QListBox* );
  void moveDown( QListBox* );
  void moveItem( QListBox* theLB, const int from, const int to );
  // mkr : PAL8060
  void createLinkEngine( SUPERV::Port_ptr thePort, 
			 QValueList< QPair< QString,QString > >& theOwnList, 
			 QValueList< QPair< QString,QString > >& theCorrespList );
  
private:
  void init();
  void init( const SUPERVGUI_CanvasNode* theNode );

  SUPERVGUI_CanvasNode*         myNode;

  QLineEdit*                    myNameEdt;
  QComboBox*                    myTypeCombo;
  QListBox*                     myInList;
  QListBox*                     myOutList;
  QListBoxItem*                 myLastItem;
};

/*!
 * Port parameters dialog definition (taken from SUPERVGUI_Node.h without change)
 */
class SUPERVGUI_PortParamsDlg: public QDialog {
  Q_OBJECT
    
 public:
  SUPERVGUI_PortParamsDlg(const QStringList& thePortsNames);
  ~SUPERVGUI_PortParamsDlg() {};

  QString getName() { return myNameTxt->text(); }
  QString getType() { return myTypeTxt->currentText(); }

 public slots:
  void clickOnOk();
    
   
 private:
  QLineEdit* myNameTxt;
  QComboBox* myTypeTxt;
  QStringList myPortsNames;
};


#endif

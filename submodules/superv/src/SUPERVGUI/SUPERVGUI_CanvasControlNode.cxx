//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasControlNode.cxx
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#include "SUPERVGUI_CanvasControlNode.h"
#include "SUPERVGUI_CanvasControlNodePrs.h"
#include "SUPERVGUI_CanvasCellNodePrs.h"
#include "SUPERVGUI_CanvasLink.h"
#include "SUPERVGUI_Clipboard.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI.h"
#include "SUPERVGUI_Canvas.h"

#include "SUIT_FileDlg.h"
#include "SUIT_Session.h"

#include <qlabel.h>
#include <qlayout.h>

//=====================================================================
// Compute node
//=====================================================================
SUPERVGUI_CanvasComputeNode::SUPERVGUI_CanvasComputeNode (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                          SUPERVGUI_Main* theMain, SUPERV_CNode theNode):
  SUPERVGUI_CanvasNode(mgr, theParent, theMain, theNode)
{
  Trace("SUPERVGUI_CanvasComputeNode::SUPERVGUI_CanvasComputeNode");
}

SUPERVGUI_CanvasComputeNode* SUPERVGUI_CanvasComputeNode::Create
                             (SUIT_ResourceMgr* mgr, QObject* theParent,
                              SUPERVGUI_Main* theMain, SUPERV_CNode theNode)
{
  SUPERVGUI_CanvasComputeNode* aRet = new SUPERVGUI_CanvasComputeNode (mgr, theParent, theMain, theNode);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

QPopupMenu* SUPERVGUI_CanvasComputeNode::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasNode::getPopupMenu(theParent);

  QPopupMenu* aShowPopup = new QPopupMenu(theParent);
  int anItem;
  anItem = aShowPopup->insertItem(tr("POP_SHOWTITLES"), this, SLOT(switchLabel()));
  aShowPopup->setItemChecked(anItem, getPrs()->isLabelVisible());
  anItem = aShowPopup->insertItem(tr("POP_SHOWPORTS"), this, SLOT(switchPorts()));
  aShowPopup->setItemChecked(anItem, getPrs()->isPortVisible());

  popup->insertSeparator();
  popup->insertItem(tr("POP_SHOW"), aShowPopup);

  return popup;
}


//=====================================================================
// Start control node
//=====================================================================
SUPERVGUI_CanvasStartNode::SUPERVGUI_CanvasStartNode (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                      SUPERVGUI_Main* theMain, SUPERV_CNode theNode):
  SUPERVGUI_CanvasNode( mgr, theParent, theMain, theNode), 
  myCoupled(0)
{
  Trace("SUPERVGUI_CanvasStartNode::SUPERVGUI_CanvasStartNode");
}

SUPERVGUI_CanvasStartNode* SUPERVGUI_CanvasStartNode::Create (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                              SUPERVGUI_Main* theMain, SUPERV_CNode theNode)
{
  SUPERVGUI_CanvasStartNode* aRet = new SUPERVGUI_CanvasStartNode (mgr, theParent, theMain, theNode);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasStartNode::createPrs() const
{
  return new SUPERVGUI_CanvasStartNodePrs (resMgr(), getMain()->getCanvas(), 
					   (SUPERVGUI_CanvasStartNode*)this);
}

bool SUPERVGUI_CanvasStartNode::setNodeName(QString aName)
{
  bool result = SUPERVGUI_CanvasNode::setNodeName(aName);
  if (result && myCoupled) {
    myCoupled->setNodeName(QString(tr("ENDNODE_PREFIX"))+aName);
  }
  return result;
}

QPopupMenu* SUPERVGUI_CanvasStartNode::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasNode::getPopupMenu(theParent);

  popup->insertSeparator();
  int anItem = popup->insertItem(tr("POP_HIDEPORTS"), this, SLOT(switchPorts()));
  popup->setItemChecked(anItem, !getPrs()->isPortVisible());

  return popup;
}

void SUPERVGUI_CanvasStartNode::remove()
{
  SUPERVGUI_Canvas* aCanvas = getMain()->getCanvas();

  setDestroyed();
  SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
  if (myCoupled) {

    //set myCopyPort from Main object to empty if engine of this port is deleted
    //check if any port of this deleted node has been copied
    if ( aCB->isCopyPort() )
      if ( QString(getEngine()->Name()) == QString(aCB->getCopyPort()->Node()->Name())	  ||
	   QString(myCoupled->getEngine()->Name()) == QString(aCB->getCopyPort()->Node()->Name()) )
	aCB->setCopyPort( 0 );
    
    myCoupled->setDestroyed();
  }

  //set myCopyNode from Main object to empty if engine of this node is deleted
  //check if myCopyNode and this node engine is equal
  if ( aCB->isCopyNode() )
    if ( QString(getEngine()->Name()) == QString(aCB->getCopyNode()->Name()) ) 
      aCB->setCopyNode( 0 );

  // mkr: since the deletion of the node allow only in CANVAS view,
  // it is necessary to remove the CanvasArray's children, which
  // have the same CNode engine as deleting node
  getMain()->removeArrayChild(getEngine());
  getMain()->removeArrayChild(myCoupled->getEngine());

  getEngine()->destroy();

  if (myCoupled) 
    delete myCoupled;

  emit objectCreatedDeleted(); // jfa : NPAL15529

  delete this;

  aCanvas->update();
}

void SUPERVGUI_CanvasStartNode::onDestroyed(QObject* theObject)
{
  if (!isIgnore) {
    SUPERVGUI_CanvasNode::onDestroyed(theObject);
    if (getEngine()->IsLoop()) merge();
    if (myCoupled) myCoupled->merge();
  }
}

// mkr : IPAL9815 : commented the following code
/*void SUPERVGUI_CanvasStartNode::addInputPort()
{
  SUPERVGUI_CanvasNode::addInputPort();
  if (getEngine()->IsLoop()) merge();
  if (myCoupled) myCoupled->merge();
}

void SUPERVGUI_CanvasStartNode::addOutputPort()
{
  SUPERVGUI_CanvasNode::addOutputPort();
  if (myCoupled) myCoupled->merge();
}*/
 
void SUPERVGUI_CanvasStartNode::pastePort()
{
  SUPERVGUI_Clipboard* aCB = SUPERVGUI_Clipboard::getClipboard();
  SUPERVGUI_CanvasNode::pastePort();
  if ( aCB->getCopyPort()->IsInput() && getEngine()->IsLoop() ) 
    merge();
  if ( myCoupled )
    myCoupled->merge();
}

//=====================================================================
// End control node
//=====================================================================
SUPERVGUI_CanvasEndNode::SUPERVGUI_CanvasEndNode (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                  SUPERVGUI_Main* theMain, SUPERV_CNode theNode,
                                                  SUPERVGUI_CanvasStartNode* theStart):
  SUPERVGUI_CanvasNode(mgr, theParent, theMain, theNode), 
  myCoupled(theStart)
{
  Trace("SUPERVGUI_CanvasEndNode::SUPERVGUI_CanvasEndNode");
  myCoupled->setCoupled(this);
}

SUPERVGUI_CanvasEndNode* SUPERVGUI_CanvasEndNode::Create (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                          SUPERVGUI_Main* theMain, SUPERV_CNode theNode,
                                                          SUPERVGUI_CanvasStartNode* theStart)
{
  SUPERVGUI_CanvasEndNode* aRet = new SUPERVGUI_CanvasEndNode (mgr, theParent, theMain, theNode, theStart);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasEndNode::createPrs() const
{
  return new SUPERVGUI_CanvasEndNodePrs( resMgr(), getMain()->getCanvas(), 
					 (SUPERVGUI_CanvasEndNode*)this);
}

QPopupMenu* SUPERVGUI_CanvasEndNode::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasNode::getPopupMenu(theParent);

  popup->insertSeparator();
  int anItem = popup->insertItem(tr("POP_HIDEPORTS"), this, SLOT(switchPorts()));
  popup->setItemChecked(anItem, !getPrs()->isPortVisible());

  return popup;
}

// mkr : IPAL9815 : commented the following code
/*void SUPERVGUI_CanvasEndNode::addInputPort()
{
  SUPERVGUI_CanvasNode::addInputPort();
  if (getEngine()->IsEndSwitch()) merge();
}*/


//=====================================================================
// Goto control node
//=====================================================================
SUPERVGUI_CanvasGotoNode::SUPERVGUI_CanvasGotoNode (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                    SUPERVGUI_Main* theMain, SUPERV_CNode theNode):
  SUPERVGUI_CanvasNode(mgr, theParent, theMain, theNode)
{
  Trace("SUPERVGUI_CanvasGotoNode::SUPERVGUI_CanvasGotoNode");
}

SUPERVGUI_CanvasGotoNode* SUPERVGUI_CanvasGotoNode::Create (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                            SUPERVGUI_Main* theMain, SUPERV_CNode theNode)
{
  SUPERVGUI_CanvasGotoNode* aRet = new SUPERVGUI_CanvasGotoNode (mgr, theParent, theMain, theNode);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasGotoNode::createPrs() const
{
  return new SUPERVGUI_CanvasGotoNodePrs( resMgr(), getMain()->getCanvas(), 
					  (SUPERVGUI_CanvasGotoNode*)this);
}

QPopupMenu* SUPERVGUI_CanvasGotoNode::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasNode::getPopupMenu(theParent);

  popup->insertSeparator();
  popup->insertItem("Link to Node...", this, SLOT(linkToNode()));

  popup->insertSeparator();
  int anItem = popup->insertItem(tr("POP_HIDEPORTS"), this, SLOT(switchPorts()));
  popup->setItemChecked(anItem, !getPrs()->isPortVisible());

  return popup;
}

void SUPERVGUI_CanvasGotoNode::linkToNode() {
  SUPERVGUI_SelectInlineDlg* aDlg = new SUPERVGUI_SelectInlineDlg(getMain());
  if (aDlg->exec()) {
    QString aNodeName = aDlg->getName();
    if (!aNodeName.isEmpty()) { //implement additional check from GUI side for bug PAL7007
      // mkr : here we should re/create (if it is not yet exists) a presentation for new
      //       Goto(OutGate)->CoupledNode(InGate) link, which will be created in SetCoupled method

      // check if GoTo node have coupled node
      SUPERV_INode aCoupledNode = getGotoNode()->Coupled();
      if ( !SUPERV_isNull(aCoupledNode) )
      { // we got old coupled node
	SUPERV_Port anInGatePort = aCoupledNode->GetInPort("InGate");
	SUPERV_Link anOldLinkEngine = anInGatePort->Link();
	if ( !SUPERV_isNull(anOldLinkEngine) )
	{ // we got old link engine
	  SUPERVGUI_CanvasLink* anOldLinkPrs = getMain()->getCanvas()->getLink(anOldLinkEngine);
	  if ( anOldLinkPrs )
	  { // delete old link presentation
	    delete anOldLinkPrs;
	    anOldLinkPrs = 0;
	  }
	}
      }

      getGotoNode()->SetCoupled(aNodeName.latin1());

      aCoupledNode = SUPERV::INode::_narrow(getMain()->getDataflow()->Node(aNodeName.latin1()));
      if ( !SUPERV_isNull(aCoupledNode) )
      { // we got new coupled node
	SUPERV_Port anInGatePort = aCoupledNode->GetInPort("InGate");
	SUPERV_Link aLinkEngine = anInGatePort->Link();
	if ( !SUPERV_isNull(aLinkEngine) )
	{ // we got new link engine
	  if ( !getMain()->getCanvas()->getLink(aLinkEngine) )
	  { // there is no presentation for such link engine => create it
	    SUPERVGUI_CanvasLink* aLink = new SUPERVGUI_CanvasLink(getMain()->getCanvas(), getMain(), aLinkEngine);
	    aLink->show();
	  }
	}
      }

      getMain()->getCanvas()->sync();
    }
  }
  delete aDlg;
}

//=====================================================================
// Macro node
//=====================================================================
SUPERVGUI_CanvasMacroNode::SUPERVGUI_CanvasMacroNode (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                      SUPERVGUI_Main* theMain, SUPERV_CNode theNode):
  SUPERVGUI_CanvasComputeNode(mgr, theParent, theMain, theNode)
{
  Trace("SUPERVGUI_CanvasMacroNode::SUPERVGUI_CanvasMacroNode");
}

SUPERVGUI_CanvasMacroNode* SUPERVGUI_CanvasMacroNode::Create (SUIT_ResourceMgr* mgr, QObject* theParent,
                                                              SUPERVGUI_Main* theMain, SUPERV_CNode theNode)
{
  SUPERVGUI_CanvasMacroNode* aRet = new SUPERVGUI_CanvasMacroNode (mgr, theParent, theMain, theNode);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasMacroNode::createPrs() const
{
  return new SUPERVGUI_CanvasMacroNodePrs( resMgr(), getMain()->getCanvas(), 
					   (SUPERVGUI_CanvasMacroNode*)this);
}

QPopupMenu* SUPERVGUI_CanvasMacroNode::getPopupMenu(QWidget* theParent) 
{
  QPopupMenu* popup = SUPERVGUI_CanvasComputeNode::getPopupMenu(theParent);

  popup->insertSeparator(0);
  popup->insertItem(tr("MSG_EXPORT"), this, SLOT(exportDataflow()), 0, -1, 0);
  popup->insertItem("Open SubGraph", this, SLOT(openSubGraph()), 0, -1, 0);

  return popup;
}

void SUPERVGUI_CanvasMacroNode::openSubGraph()
{
  getMain()->openSubGraph(getEngine());
}

void SUPERVGUI_CanvasMacroNode::exportDataflow()
{
  SUPERV_Graph aGraph;
  if (getEngine()->IsMacro()) {
    SUPERV_Graph aMacro = getMacroNode();
    if (aMacro->IsStreamMacro())
      aGraph = aMacro->StreamObjRef();
    else
      aGraph = aMacro->FlowObjRef();
  }
  if (SUPERV_isNull(aGraph)) {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("WARNING"), tr("MSG_NOWINDOW_TO_EXPORT"));
    return;
  }
  else {
    QString aFileName = SUIT_FileDlg::getFileName(SUIT_Session::session()->activeApplication()->desktop(),
						  "",
						  "*.xml",
						  tr("TTL_EXPORT_DATAFLOW"),
						  false);
    if ( !aFileName.isEmpty() ) {
      // asv : bug [VSR Bugs and Improvements in Supervisor] 1.8 : when exporting a file, 
      // a backup copy of an existing file must be created (in case Export fails..)
      QString aBackupFile = SUPERVGUI::createBackupFile( aFileName );

      if (!aGraph->Export(aFileName.latin1())) {
	QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("ERROR"), tr("MSG_BAD_WRITING").arg(aBackupFile));
      }
      // remove a backup file if export was successfull
      else if ( !aBackupFile.isNull() && !aBackupFile.isEmpty() ) {
	QFile::remove( aBackupFile );
      }
    }
  }
}

// mkr : IPAL11549 : before remove Macro Node we have to destroy
//                   all its opened sub-graphs
void SUPERVGUI_CanvasMacroNode::remove()
{
  SUPERV_Graph aGraph;
  if (getEngine()->IsMacro()) {
    SUPERV_Graph aMacro = getMacroNode();
    if (aMacro->IsStreamMacro())
      aGraph = aMacro->StreamObjRef();
    else
      aGraph = aMacro->FlowObjRef();
  }
  if (SUPERV_isNull(aGraph)) {
    QMessageBox::warning(SUIT_Session::session()->activeApplication()->desktop(), tr("WARNING"), tr("MSG_NOSUBGRAPH_TO_REMOVE"));
    return;
  }
  else {
    getMain()->destroySubGraph(aGraph->Name());
  }

  SUPERVGUI_CanvasNode::remove();
}

//=====================================================================
// Cell node: node for table view
//=====================================================================
SUPERVGUI_CanvasCellNode::SUPERVGUI_CanvasCellNode (SUIT_ResourceMgr* mgr, QObject* theParent, 
                                                    SUPERVGUI_Main* theMain, SUPERV_CNode theNode):
  SUPERVGUI_CanvasNode(mgr, theParent, theMain, theNode, true)
{
  Trace("SUPERVGUI_CanvasCellNode::SUPERVGUI_CanvasCellNode");
  
  myIsControl = false;
  myIsStart = false;

  //check for control nodes
  if (getEngine()->IsLoop() || getEngine()->IsSwitch()) {
    myIsControl = true;  
    myIsStart = true;
  }
  if (getEngine()->IsEndLoop() || getEngine()->IsEndSwitch())
    myIsControl = true;
}

SUPERVGUI_CanvasCellNode* SUPERVGUI_CanvasCellNode::Create (SUIT_ResourceMgr* mgr, QObject* theParent, 
                                                            SUPERVGUI_Main* theMain, SUPERV_CNode theNode)
{
  SUPERVGUI_CanvasCellNode* aRet = new SUPERVGUI_CanvasCellNode (mgr, theParent, theMain, theNode);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

SUPERVGUI_CanvasCellNode::~SUPERVGUI_CanvasCellNode()
{
}
/*
QPopupMenu* SUPERVGUI_CanvasCellNode::getPopupMenu(QWidget* theParent)
{
  QPopupMenu* popup = SUPERVGUI_CanvasNode::getPopupMenu(theParent);
  popup->setItemEnabled(myDeleteItem, false);
  return popup;
}
*/
void SUPERVGUI_CanvasCellNode::setPairCell(SUPERVGUI_CanvasCellNode* thePairCell) {
  if (myIsControl) { //only for ControlNode
    myPairCell = thePairCell;
  }
  else 
    myPairCell = 0;
}

SUPERVGUI_CanvasCellNode* SUPERVGUI_CanvasCellNode::getPairCell() {
  return myPairCell;
}

void SUPERVGUI_CanvasCellNode::sync() {
  const bool isExecuting = getMain()->getDataflow()->IsExecuting();
  //if getEngine() is a MacroNode then set it state to state of its subgraph
  if ( getEngine()->IsMacro() && isExecuting ) {
    // get SubGraph from MacroNode
    SUPERV_Graph aMacro = SUPERV::Graph::_narrow(getEngine());
    if (!SUPERV_isNull(aMacro)) {
      SUPERV_Graph aGraph;
      if (aMacro->IsStreamMacro())
	aGraph = aMacro->StreamObjRef();
      else
	aGraph = aMacro->FlowObjRef();
      if (!SUPERV_isNull(aGraph)) {
	if (aGraph->State() != SUPERV::UndefinedState && aGraph->State() != SUPERV::NoState)
	  getPrs()->setState(aGraph->State());
	else 
	  getPrs()->setState(getEngine()->State());
      }
    }
  }
  else {
    getPrs()->setState(getEngine()->State());
  }
}
/*
bool SUPERVGUI_CanvasCellNode::setNodeName(QString aName) 
{
  bool result = SUPERVGUI_CanvasNode::setNodeName(aName);
  if (result && myPairCell) {
    result = myPairCell->setNodeName(QString(tr("ENDNODE_PREFIX"))+aName);
  }
  return result;
}
*/
SUPERVGUI_CanvasNodePrs* SUPERVGUI_CanvasCellNode::createPrs() const
{
  SUPERVGUI_CanvasNodePrs* aPrs = 
    new SUPERVGUI_CanvasCellNodePrs( resMgr(), getMain()->getCanvasArray(), 
				     (SUPERVGUI_CanvasCellNode*)this);
  return aPrs;
}

SUPERVGUI_CanvasCellEndNode::SUPERVGUI_CanvasCellEndNode (SUIT_ResourceMgr* mgr, QObject* theParent, 
							  SUPERVGUI_Main* theMain, SUPERV_CNode theNode, 
							  SUPERVGUI_CanvasCellNode* theStart):
  SUPERVGUI_CanvasCellNode(mgr, theParent, theMain, theNode)
{
  //set start cell for end cell as pair 
  myPairCell = theStart; 
  //set end cell for start cell as pair
  myPairCell->setPairCell(dynamic_cast<SUPERVGUI_CanvasCellNode*>(this));
}

SUPERVGUI_CanvasCellEndNode* SUPERVGUI_CanvasCellEndNode::Create (SUIT_ResourceMgr* mgr, QObject* theParent, 
                                                                  SUPERVGUI_Main* theMain,
                                                                  SUPERV_CNode theNode, 
                                                                  SUPERVGUI_CanvasCellNode* theStart)
{
  SUPERVGUI_CanvasCellEndNode* aRet =
    new SUPERVGUI_CanvasCellEndNode (mgr, theParent, theMain, theNode, theStart);
  emit aRet->objectCreatedDeleted();
  return aRet;
}

/*
bool SUPERVGUI_CanvasCellEndNode::setNodeName(QString theName)
{
  return SUPERVGUI_CanvasNode::setNodeName(theName);
}
*/

//-----------------------------------------------------------
//*************** Select Inline node dialog******************
// Taken from SUPERVGUI_ControlNode.cxx without change
//-----------------------------------------------------------

SUPERVGUI_SelectInlineDlg::SUPERVGUI_SelectInlineDlg(SUPERVGUI_Main* theMain)  
  :QDialog(theMain, 0, true, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu)
{
  setSizeGripEnabled( true );
  setCaption(tr("TIT_FUNC_PYTHON"));
  QGridLayout* aMainLayout = new QGridLayout(this, 2, 2, 7, 4);

  QLabel* aLabel = new QLabel("Connect to", this );
  aMainLayout->addWidget(aLabel, 0, 0);

  myCombo = new QComboBox(this);
  myCombo->clear(); //for the additional check from GUI side for bug PAL7007
  SUPERV_Nodes aNodesList = theMain->getDataflow()->Nodes();  
  int i;
  for (i = 0; i < aNodesList->INodes.length(); i++) {
    myCombo->insertItem(QString(aNodesList->INodes[i]->Name()));
  }
  for (i = 0; i < aNodesList->LNodes.length(); i++) {
    myCombo->insertItem(QString(aNodesList->LNodes[i]->Name()));
  }
  for (i = 0; i < aNodesList->SNodes.length(); i++) {
    myCombo->insertItem(QString(aNodesList->SNodes[i]->Name()));
  }
  aMainLayout->addWidget(myCombo, 0, 1);

  QGroupBox* aBtnBox = new QGroupBox( this );
  aBtnBox->setColumnLayout( 0, Qt::Vertical );
  aBtnBox->layout()->setSpacing( 0 ); aBtnBox->layout()->setMargin( 0 );
  QHBoxLayout* aBtnLayout = new QHBoxLayout( aBtnBox->layout() );
  aBtnLayout->setAlignment( Qt::AlignTop );
  aBtnLayout->setSpacing( 6 ); aBtnLayout->setMargin( 11 );
  
  QPushButton* aOKBtn = new QPushButton( tr( "BUT_OK" ), aBtnBox );
  connect( aOKBtn, SIGNAL( clicked() ), this, SLOT( accept() ) );
  aBtnLayout->addWidget( aOKBtn );

  aBtnLayout->addStretch();

  QPushButton* aCancelBtn = new QPushButton( tr("BUT_CANCEL"), aBtnBox );
  connect( aCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  aBtnLayout->addWidget( aCancelBtn );

  aMainLayout->addMultiCellWidget(aBtnBox, 1, 1, 0, 1);
}

//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasNodePrs.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasNodePrs_H
#define SUPERVGUI_CanvasNodePrs_H

#include "SUPERVGUI_Def.h"

#include <qcanvas.h>


class SUPERVGUI_CanvasNodePrs;
class SUPERVGUI_CanvasPortPrs;
class SUIT_ResourceMgr;
class SUPERVGUI_CanvasHookPrs : public QCanvasEllipse
{
  public:
    SUPERVGUI_CanvasHookPrs(QCanvas* theCanvas, SUPERVGUI_CanvasNodePrs* theNode, const bool& theIn);
    SUPERVGUI_CanvasHookPrs(QCanvas* theCanvas, SUPERVGUI_CanvasPortPrs* thePort, const bool& theIn);
    ~SUPERVGUI_CanvasHookPrs();

    QObject* getObject() const;

    void setCoords(int x, int y);

    void setVisible(bool b);
    void moveBy(double dx, double dy);
    void setZ(double z);

    virtual int rtti() const;

  private:
    void init(QCanvas* theCanvas);

    SUPERVGUI_CanvasNodePrs* myNodePrs;
    SUPERVGUI_CanvasPortPrs* myPortPrs;

    bool myIn;

    QCanvasLine* myLine;
};


class SUPERVGUI_CanvasNode;
class SUPERVGUI_CanvasNodePrs: public QCanvasPolygonalItem {

  public:
    SUPERVGUI_CanvasNodePrs( SUIT_ResourceMgr*, QCanvas*, SUPERVGUI_CanvasNode*, bool theCellPrs=false);
    virtual ~SUPERVGUI_CanvasNodePrs();

    SUPERVGUI_CanvasNode* getNode() const { return myNode; }
    QObject* getObject(const QPoint& thePos) const;

    QString getStatus() const { return myStatus; }
    QColor getStatusColor() const { return myStatusColor; }

    void setStatus(QString theStatus) { myStatus = theStatus; }
    void setStatusColor(QColor theStatusColor) { myStatusColor = theStatusColor; }
    
    virtual int width() const;
    virtual int height() const;

    virtual int getTitleHeight() const;
    virtual int getLabelHeight() const;
    virtual int getStatusHeight() const;
    virtual int getBodyHeight() const;
    virtual int getPortHeight() const;
    virtual int getStreamHeight() const;
    virtual int getGateHeight() const;
    
    QPointArray areaPoints() const;
    void moveBy(double dx, double dy);
    void setZ(double z);

    void setMoving(bool b) { myMoving = b; }
    bool isMoving() const { return myMoving; }

    virtual int rtti() const;

    virtual void hideAll();
    virtual void showAll();
    virtual void setLabelVisible(bool b);
    virtual void setPortVisible(bool b);
    bool isLabelVisible() const { return myLabelVisible; }
    bool isPortVisible() const { return myPortVisible; }

    virtual void setNodeColor(const QColor& theColor);
    virtual QColor nodeColor() const { return myColor; }

    virtual void setState(SUPERV::GraphState theState);
    virtual void updateInfo();
    virtual void updatePorts();
    virtual void updateGates();
    virtual void updatePoints();

    virtual QPoint getInConnectionPoint() const;
    virtual QPoint getOutConnectionPoint() const;

    virtual QRect getRect() const;
    virtual QRect getTitleRect() const;
    virtual QRect getStatusRect() const;
    virtual QRect getLabelRect() const;
    virtual QRect getBodyRect() const;
    virtual QRect getGateRect() const;

  protected:
    void draw(QPainter& thePainter);
    virtual void drawShape(QPainter& thePainter);

    virtual void drawTitle(QPainter& thePainter);
    virtual void drawLabel(QPainter& thePainter);
    virtual void drawPort(QPainter& thePainter);
    virtual void drawGate(QPainter& thePainter);
    virtual void drawStatus(QPainter& thePainter);

    virtual void drawFrame(QPainter& thePainter);
    virtual void drawTitleShape(QPainter& thePainter);
    virtual void drawStatusShape(QPainter& thePainter);

    SUIT_ResourceMgr* resMgr() const { return myMgr; }
    bool myPortVisible;

  private:
    SUPERVGUI_CanvasNode* myNode;
    SUIT_ResourceMgr* myMgr;

    int myWidth;

    int myTitleHeight;
    int myLabelHeight;
    int myStatusHeight;
    int myPortHeight;
    int myStreamHeight;
    int myGateHeight;

    bool myLabelVisible;
    bool myCellPrs;
    bool myMoving;

    QString myStatus;
    QString myTime;

    QColor myColor;
    QColor myStatusColor;

    SUPERVGUI_CanvasHookPrs* myPointIn;
    SUPERVGUI_CanvasHookPrs* myPointOut;
};


class SUPERVGUI_CanvasPort;
class SUPERVGUI_CanvasPortPrs {

 public:
  SUPERVGUI_CanvasPortPrs(QCanvas* theCanvas, SUPERVGUI_CanvasPort* thePort);
  ~SUPERVGUI_CanvasPortPrs();

  SUPERVGUI_CanvasPort* getPort() { return myPort; }
  virtual void update(bool theForce = false);

  virtual bool isHilight() const;
  virtual bool isAlert() const;

  virtual QString getText() const;
  virtual int getAlignment() const;

  void setPortRect(const QRect& theRect);
  QRect getPortRect() const { return myRect; }

  void setVisible(bool b);
  bool isVisible() const { return myVisible; }

  void moveBy(int dx, int dy);
  void setZ(double z);

  QPoint getConnectionPoint() const;
  void draw(QPainter& thePainter);

 private:
  SUPERVGUI_CanvasPort* myPort;
  QCanvas*              myCanvas;
  QRect                 myRect;
  QString               myText;

  SUPERVGUI_CanvasHookPrs* myPoint;
  bool                  myVisible;
};

#endif

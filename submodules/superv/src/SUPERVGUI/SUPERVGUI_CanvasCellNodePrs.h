//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasCellNodePrs.h
//  Author : 
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasCellNodePrs_H
#define SUPERVGUI_CanvasCellNodePrs_H

#include "SUPERVGUI_Def.h"
#include "SUPERVGUI_CanvasNodePrs.h"
#include "SUPERVGUI_CanvasControlNode.h"

class SUPERVGUI_CanvasCellNodePrs: public SUPERVGUI_CanvasNodePrs {

  public:
    SUPERVGUI_CanvasCellNodePrs( SUIT_ResourceMgr*, QCanvas* theCanvas, SUPERVGUI_CanvasCellNode* theNode);
    virtual ~SUPERVGUI_CanvasCellNodePrs();

    virtual void moveBy(double dx, double dy);
 
    virtual QRect getTitleRect() const;
    virtual QRect getLabelRect() const;
    virtual QRect getStatusRect() const;

    int getTitleWidth() const;
    int getLabelWidth() const;
    int getStatusWidth() const;

    virtual int width() const;
    virtual int height() const;

    virtual void setState(SUPERV::GraphState theState);

  protected:
    virtual void drawShape(QPainter& thePainter);

    virtual void drawTitle(QPainter& thePainter);
    virtual void drawLabel(QPainter& thePainter);
    virtual void drawStatus(QPainter& thePainter);

  private:
    int myTitleWidth;
    int myLabelWidth;
    int myStatusWidth;
    
};

#endif

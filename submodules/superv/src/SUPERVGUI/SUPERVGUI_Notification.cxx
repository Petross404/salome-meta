//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Notification.cxx
//  Author : Francis KLOSS
//  Module : SUPERV

#include "SUPERVGUI_Notification.h"
#include "SUPERVGUI_Main.h"
#include "SUPERVGUI_CanvasNode.h"

#include "NOTIFICATION.hxx"
#include "SUIT_Desktop.h"
#include "SUIT_FileDlg.h"
#include "SUIT_Session.h"

#include <qlayout.h>
#include <qlabel.h>
#include <qgroupbox.h>

#define SPACING_SIZE   6
#define MARGIN_SIZE   11
#define MAX( x, y ) ( x ) > ( y ) ? ( x ) : ( y )
#define DLG_SIZE_WIDTH   300
#define DLG_SIZE_HEIGHT  300

/******************************************
 * Notification settings dialog box
 ******************************************/
/*!
  Constructor
*/
SUPERVGUI_Notification::SUPERVGUI_Notification( SUPERVGUI_Main* m )
     : QDialog( SUIT_Session::session()->activeApplication()->desktop(), "", TRUE, WStyle_Customize | WStyle_NormalBorder | WStyle_Title | WStyle_SysMenu ),
       myMain( m )
{
  setCaption( tr( "TLT_FILTER_NOTIFY" ) );
  setSizeGripEnabled( TRUE );
  QVBoxLayout* topLayout = new QVBoxLayout( this ); 
  topLayout->setSpacing( SPACING_SIZE );
  topLayout->setMargin( MARGIN_SIZE );

  /* Creating notofication widgets*/
  /* Top scroll view */
  myView = new QScrollView( this );
//  myView->setMinimumSize( NOTIFICATION_WIDTH, NOTIFICATION_HEIGHT );
  myBox = new QVBox( myView );
  myBox->setMargin( MARGIN_SIZE );
  myBox->setSpacing( SPACING_SIZE );
  QFrame* lin;

  /* Notification logging */
  QLabel* logg = new QLabel( tr( "NOTIFICATION_LOGGING_LBL" ), myBox );
  QFont fnt = logg->font(); fnt.setBold( true ); logg->setFont( fnt );
  QWidget* dumb = new QWidget( myBox );
  QHBoxLayout* dumbLayout = new QHBoxLayout( dumb ); dumbLayout->setMargin( 0 ); dumbLayout->setSpacing( SPACING_SIZE );
  myLogCheck = new QCheckBox( tr( "SAVE_LOG_LBL" ), dumb );
  myFileEdit = new QLineEdit( dumb ); 
  myFileEdit->setMinimumSize( 200, 0 );
  myBrowseBtn = new QPushButton( tr( "BUT_BROWSE"), dumb );
  dumbLayout->addWidget( myLogCheck );
  dumbLayout->addWidget( myFileEdit );
  dumbLayout->addWidget( myBrowseBtn );
  myFilterCheck = new QCheckBox( tr( "FILTER_LOG_LBL" ), myBox );

  lin = new QFrame( myBox );
  lin->setFrameStyle( QFrame::HLine | QFrame::Plain );

  /* dataflow notification */
  myMainNotif = new SUPERVGUI_NotifWidget( myBox, QString( myMain->getDataflow()->Name() ) );
  myMainNotif->setWarning( false );
  myMainNotif->setStep( false );
  myMainNotif->setTrace( false );
  myMainNotif->setVerbose( false );

  /* nodes notification */
  // mkr : PAL7037 -->
  //QObjectList* ihmList = myMain->/*getGraph()->*/queryList( "SUPERVGUI_CanvasNode" );
  QObjectList* ihmList;
  if ( myMain->getViewType() == CANVASTABLE )
    ihmList = myMain->getCanvasArray()->queryList( "SUPERVGUI_CanvasNode" );
  else
    ihmList = myMain->getCanvas()->queryList( "SUPERVGUI_CanvasNode" );
  // mkr : PAL7037 <--
  QObjectListIt i( *ihmList );
  SUPERVGUI_CanvasNode* theNode;
  while ( ( theNode = ( ( SUPERVGUI_CanvasNode* )i.current() ) ) != 0) {
    lin = new QFrame( myBox );
    lin->setFrameStyle( QFrame::HLine | QFrame::Plain );

    SUPERVGUI_NotifWidget* nw = new SUPERVGUI_NotifWidget( myBox, theNode );
    nw->setWarning( theNode->isWarning() );
    nw->setStep( theNode->isStep() );
    nw->setTrace( theNode->isTrace() );
    nw->setVerbose( theNode->isVerbose() );
    myNotifList.append( nw );
    ++i;
  };
  delete ihmList;

  QWidget* dumb1 = new QWidget( myBox );
  myBox->setStretchFactor( dumb1, 10 );

  myView->addChild( myBox, 0, 0 );
  myView->setResizePolicy( QScrollView::AutoOneFit );

  int w = myLogCheck->sizeHint().width() + myFileEdit->minimumSize().width() + myBrowseBtn->sizeHint().width() + SPACING_SIZE * 2;
  w = MAX( w, logg->sizeHint().width() );
  w = MAX( w, myMainNotif->sizeHint().width() );
  w = MAX( w, myFilterCheck->sizeHint().width() );
  myView->setMinimumWidth( w + MARGIN_SIZE * 4 );

  /* OK/Cancel buttons */
  QGroupBox* GroupButtons = new QGroupBox( this );
  GroupButtons->setColumnLayout(0, Qt::Vertical );
  GroupButtons->layout()->setSpacing( 0 );  GroupButtons->layout()->setMargin( 0 );
  QHBoxLayout* GroupButtonsLayout = new QHBoxLayout( GroupButtons->layout() );
  GroupButtonsLayout->setAlignment( Qt::AlignTop );
  GroupButtonsLayout->setSpacing( SPACING_SIZE );
  GroupButtonsLayout->setMargin( MARGIN_SIZE );
  myOkBtn = new QPushButton( tr( "BUT_OK" ), GroupButtons, "buttonOk" );
  myOkBtn->setAutoDefault( TRUE );
  myOkBtn->setDefault( TRUE );
  GroupButtonsLayout->addWidget( myOkBtn );
  GroupButtonsLayout->addItem( new QSpacerItem( 5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum ) );
  myCancelBtn = new QPushButton( tr( "BUT_CANCEL" ), GroupButtons, "buttonCancel" );
  myCancelBtn->setAutoDefault( TRUE );
  GroupButtonsLayout->addWidget( myCancelBtn );

  topLayout->addWidget( myView );
  topLayout->addWidget( GroupButtons );
  
  connect( myOkBtn,     SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( myCancelBtn, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( myLogCheck,  SIGNAL( clicked() ), this, SLOT( loggedClicked() ) );
  connect( myBrowseBtn, SIGNAL( clicked() ), this, SLOT( browseClicked() ) );
  
  QString file = QString( "/tmp/Notif_" );
  file += myMain->getDataflow()->Name();
  file += ".log";
  myFileEdit->setText( file );

  loggedClicked();
  resize( MAX( w + MARGIN_SIZE * 4, DLG_SIZE_WIDTH ), DLG_SIZE_HEIGHT  );
}

/*!
  Destructor
*/
SUPERVGUI_Notification::~SUPERVGUI_Notification() 
{
}

/*!
  <OK> button slot
*/
void SUPERVGUI_Notification::accept() 
{
  Trace("SUPERVGUI_Notification::accept");
  for ( int i = 0; i < myNotifList.count(); i++ ) {
    if ( myNotifList.at( i )->getNode() ) {
      myNotifList.at( i )->getNode()->setWarning( myNotifList.at( i )->getWarning() );
      myNotifList.at( i )->getNode()->setStep( myNotifList.at( i )->getStep() );
      myNotifList.at( i )->getNode()->setTrace( myNotifList.at( i )->getTrace() );
      myNotifList.at( i )->getNode()->setVerbose( myNotifList.at( i )->getVerbose() );
    }
  }
  QDialog::accept();
}

/*!
   <Cancel> button slot
*/
void SUPERVGUI_Notification::reject() 
{
  Trace("SUPERVGUI_Notification::reject");
  QDialog::reject();
}

/*!
  Called when <Save to file> check box is clicked
*/
void SUPERVGUI_Notification::loggedClicked()
{
  myFileEdit->setEnabled( myLogCheck->isChecked() );
  myBrowseBtn->setEnabled( myLogCheck->isChecked() );
  myFilterCheck->setEnabled( myLogCheck->isChecked() );
}

/*!
  <Browse> button slot
*/
void SUPERVGUI_Notification::browseClicked()
{
  QString fn = SUIT_FileDlg::getFileName( this, 
					  myFileEdit->text(), 
					  tr( "ALL_FILES" ),
					  tr( "CHOOSE_LOG_FILES_TLT" ),
					  false );
  if ( !fn.isEmpty() )
    myFileEdit->setText( fn );
}

/*!
  Set/get methods
*/
bool SUPERVGUI_Notification::getLogged() 
{
  return myLogCheck->isChecked();
}
void SUPERVGUI_Notification::setLogged( bool on, QString logFile ) 
{
  myLogCheck->setChecked( on );
  if ( !logFile.isNull() ) {
    myFileEdit->setText( logFile );
    myFileEdit->home( false ); 
  } 
  loggedClicked();
}
QString SUPERVGUI_Notification::getLogFile() 
{
  return myFileEdit->text().stripWhiteSpace();
}
bool SUPERVGUI_Notification::getFiltered() 
{
  return myFilterCheck->isChecked();
}
void SUPERVGUI_Notification::setFiltered( bool on ) 
{
  myFilterCheck->setChecked( on );
}
bool SUPERVGUI_Notification::getWarning()
{
  return myMainNotif->getWarning();
}
void SUPERVGUI_Notification::setWarning( bool on )
{
  myMainNotif->setWarning( on );
}
bool SUPERVGUI_Notification::getStep()
{
  return myMainNotif->getStep();
}
void SUPERVGUI_Notification::setStep( bool on )
{
  myMainNotif->setStep( on );
}
bool SUPERVGUI_Notification::getTrace()
{
  return myMainNotif->getTrace();
}
void SUPERVGUI_Notification::setTrace( bool on )
{
  myMainNotif->setTrace( on );
}
bool SUPERVGUI_Notification::getVerbose()
{
  return myMainNotif->getVerbose();
}
void SUPERVGUI_Notification::setVerbose( bool on )
{
  myMainNotif->setVerbose( on );
}

/******************************************
 * Widget containing title and  checkboxes
 ******************************************/
/*!
  Constructor
*/
SUPERVGUI_NotifWidget::SUPERVGUI_NotifWidget( QWidget* parent, SUPERVGUI_CanvasNode* n )
     : QWidget( parent ), myNode ( n )
{
  init();
}
/*!
  Another constructor
*/
SUPERVGUI_NotifWidget::SUPERVGUI_NotifWidget( QWidget* parent, QString title )
     : QWidget( parent ), myNode ( 0 ), myTitle( title )
{
  init();
}
/*!
  Destructor
*/
SUPERVGUI_NotifWidget::~SUPERVGUI_NotifWidget()
{
}
/*!
  Creates widget's layout 
*/
void SUPERVGUI_NotifWidget::init()
{
  QGridLayout* topLayout = new QGridLayout( this );
  topLayout->setMargin( 0 );
  topLayout->setSpacing( SPACING_SIZE );

  QLabel* tltLab = new QLabel( this );
  if ( myNode )
    tltLab->setText( myNode->name() );
  else
    tltLab->setText( myTitle );
  QFont fnt = tltLab->font(); fnt.setBold( true ); tltLab->setFont( fnt );

  myWarningCheck = new QCheckBox( tr( NOTIF_WARNING ), this );
  myStepCheck    = new QCheckBox( tr( NOTIF_STEP ),    this );
  myTraceCheck   = new QCheckBox( tr( NOTIF_TRACE ),   this );
  myVerboseCheck = new QCheckBox( tr( NOTIF_VERBOSE ), this );
  
  topLayout->addMultiCellWidget( tltLab, 0, 0, 0, 4 );
  topLayout->addWidget( myWarningCheck, 1, 1 );
  topLayout->addWidget( myStepCheck,    1, 2 );
  topLayout->addWidget( myTraceCheck,   1, 3 );
  topLayout->addWidget( myVerboseCheck, 1, 4 );
  topLayout->addColSpacing( 0, 15 );
  topLayout->setColStretch( 1, 5 );
  topLayout->setColStretch( 2, 5 );
  topLayout->setColStretch( 3, 5 );
  topLayout->setColStretch( 4, 5 );
}


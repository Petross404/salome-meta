//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SUPERVGUI_Library.h
//  Author : Alexander SLADKOV
//  Module : SUPERV

#ifndef SUPERVGUI_Library_H
#define SUPERVGUI_Library_H

#include <qdialog.h>
#include <qdom.h>

#include "utilities.h"
#include "SALOME_LifeCycleCORBA.hxx"
#include CORBA_CLIENT_HEADER(SUPERV)

class QListBox;

/**
 * SUPERVGUI_Library class is intended for management of InLine nodes library.
 * InLine nodes library is an XML repository (currently it is a predefined user-dependant XML file)
 * with InLine nodes stored in it.  InLine node in a library contains its Python code and lists of 
 * input and output ports.
 */
class SUPERVGUI_Library : QObject { // extending QObject to enable slot/signals

  Q_OBJECT

protected:
  SUPERVGUI_Library(); // constructor is protected, use static function getLibrary()
 
public:
  static SUPERVGUI_Library* getLibrary() { 
    if ( !myLibrary )
      myLibrary = new SUPERVGUI_Library();
    return myLibrary; 
  }

  // Returns the XML file name used as InLine nodes repository
  const char* GetLibraryFileName() const;

  // Export an InLine node to Library 
  bool Export( SUPERV::INode_var theNode ) const;

  // Import an InLine node from Library into the dataflow  
  bool Import( SUPERV::Graph_var theDataflow, SUPERV::INode_var& theNode, 
	       SUPERV::INode_var& theEndNode, const int theLibIndex ) const; 

  // Remove an InLine node with given index from Library
  bool Remove( const int i ) const;

  // returns a list of node names currently stored in the library.  Indexes of the nodes in
  // this list can be used for futher calls to Import(.., index)
  SUPERV::ListOfStrings GetLibraryNodesNames() const;

  // returns status of library: false indicates that library file does not exist or can not be opened
  bool CanImport() const;

private:
  static SUPERVGUI_Library* myLibrary;

  bool createLibFile() const; // returns false on file creation error (also displays MB)

  // returns list of NODE_ELEMENT-s and error result (false) if failed (also displays MB)
  bool getNodes( QDomDocument& theDoc, QDomNodeList& theNodes ) const; 

};

/**
 * Dialog box for Add/Remove InLine nodes to Library, Add node to Graph from Library
 */
class SUPERVGUI_LibDlg: public QDialog {

  Q_OBJECT

public:
  SUPERVGUI_LibDlg( QWidget* parent, int& theX, int& theY );
  ~SUPERVGUI_LibDlg();

private slots:
  void add(); // add selected node from library to graph
  void remove(); // remove selected node from library

private:
  void initList(); // clear and fill list with nodes from Library

  QListBox* myLB;
  int& myX; // comes from caller - SUPERVGUI_Service, used for calculating next node_s position
  int& myY; // comes from caller - SUPERVGUI_Service
};

#endif

//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  File   : SUPERVGUI_BrowseNodeDlg.h
//  Author : Vitaly SMETANNIKOV
//  Module : SUPERV

#ifndef SUPERVGUI_BrowseNodeDlg_H
#define SUPERVGUI_BrowseNodeDlg_H

#include <qdialog.h>
#include "SUPERVGUI_Def.h"

/** 
 * Class for ports values management (for internal using)
 */
class SUPERVGUI_PortField : public QObject {

  Q_OBJECT

public:
  SUPERVGUI_PortField( QWidget* theParent, SUPERV_Port thePort );
  virtual ~SUPERVGUI_PortField() {};

  bool eventFilter( QObject* o, QEvent* e );

  bool isEditable () { return myIsEditable; }

  QString getData() const { return myValue->text(); } // mkr : PAL11406
  void    setData( const QString& data ) { myValue->setText( data ); myValue->home( false ); }
  void    updateGUI()   { myValue->setText( myPort->ToString() ); }

  bool setNewValue();
  
signals :
  void activated();

public:
  SUPERV_Port myPort;
  QLabel*     myLabel;
  QLineEdit*  myValue;
  bool        myIsEditable;
};


/**
 * Browse Node Dialog box
 */
class SUPERVGUI_CanvasNode;

class SUPERVGUI_BrowseNodeDlg: public QDialog {
  
  Q_OBJECT

public:
  SUPERVGUI_BrowseNodeDlg( SUPERVGUI_CanvasNode* theNode );
  virtual ~SUPERVGUI_BrowseNodeDlg();

  void setValues();

protected slots:
  void accept();
  void reject();
  void onFieldActivated();
  void onSelectionChanged();

protected:
  void showEvent( QShowEvent* theEvent );

private:
  void init();

  SUPERVGUI_CanvasNode*         myNodeCanvas;
  
  bool                          myIsEditable;
  QPtrList<SUPERVGUI_PortField> myPortsList;
  SUPERVGUI_PortField*          myActiveField;
};

/**
 * Get Value dialog box
 */
class SUPERVGUI_CanvasPort;

class SUPERVGUI_GetValueDlg: public QDialog {
  Q_OBJECT

public:
  SUPERVGUI_GetValueDlg( SUPERVGUI_CanvasPort* thePort );
  ~SUPERVGUI_GetValueDlg();

private slots:
  void accept();
  void reject();
  void onSelectionChanged();
  
private:
  void init();

  QPushButton*         myOKBtn;
  QPushButton*         myCancelBtn;    
  SUPERVGUI_PortField* myField;

  SUPERVGUI_CanvasPort*      myPortCanvas;

};


/**
 * Stream Port Parameter dialog box
 */
class SUPERVGUI_CanvasStreamPortIn;
class SUPERVGUI_StreamInDlg: public QDialog
{
  Q_OBJECT
public:
  SUPERVGUI_StreamInDlg(SUPERVGUI_CanvasStreamPortIn* thePort);
  virtual ~SUPERVGUI_StreamInDlg() {};

protected slots:
  void accept();

private:
  void init();
  void setData(); 

  SUPERVGUI_CanvasStreamPortIn* myPortCanvas;
  QComboBox* mySchemaCombo;
  QComboBox* myInterCombo;
  QComboBox* myExterCombo;
};

class SUPERVGUI_CanvasStreamPortOut;
class SUPERVGUI_StreamOutDlg: public QDialog
{
  Q_OBJECT
public:
  SUPERVGUI_StreamOutDlg(SUPERVGUI_CanvasStreamPortOut* thePort);
  virtual ~SUPERVGUI_StreamOutDlg() {};

protected slots:
  void accept();

private:
  void init();

  SUPERVGUI_CanvasStreamPortOut* myPortCanvas;
  QLineEdit* myValEdit;
};

#endif

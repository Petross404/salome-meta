//  SUPERV SUPERVGUI : GUI for Supervisor component
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SUPERVGUI_CanvasView.h
//  Author : Natalia KOPNOVA
//  Module : SUPERV

#ifndef SUPERVGUI_CanvasView_H
#define SUPERVGUI_CanvasView_H

#include "SUPERVGUI_Canvas.h"
#include <qpopupmenu.h>

//VRV: porting on Qt 3.0.5
#if QT_VERSION >= 0x030005
#include <qcursor.h> 
#endif
//VRV: porting on Qt 3.0.5

class SUPERVGUI_Main;
class SUPERVGUI_CanvasLink;
class SUPERVGUI_CanvasLinkBuilder;

class SUPERVGUI_CanvasView: public QCanvasView {
  Q_OBJECT

  public:
    SUPERVGUI_CanvasView(SUPERVGUI_Canvas* theCanvas, SUPERVGUI_Main* theMain);
    virtual ~SUPERVGUI_CanvasView();

    SUPERVGUI_Main* getMain() const { return myMain; }
    
    void ActivatePanning();
    void ResetView();

    bool isAnyLinkCreating() const { return myIsLinkCreating; }
    void startSketch(SUPERVGUI_CanvasPort* thePort);

  signals:
    void objectCreated(); // mkr : PAL8237

  public slots:
//VSR: 08/06/06    void addToStudy();
    void onTimeout();
    void zoomIn();
    void zoomOut();
    void zoomReset();
    void fitAll();
    void fitWithinRect();
    void changeBackground();

  protected:
    void contentsMouseMoveEvent(QMouseEvent* theEvent);
    void contentsMouseReleaseEvent(QMouseEvent* theEvent); 
    void contentsMousePressEvent(QMouseEvent* theEvent); 
    void contentsMouseDoubleClickEvent(QMouseEvent* theEvent); 

  private slots:
    void onDestroyed(QObject* theObject);
  // new link creation
    void cancelSketch();
    void deletePoint();
    void setOrthoMode();

  private:
    void setHilighted(SUPERVGUI_CanvasLink* theLink);
    void endSketch(SUPERVGUI_CanvasPort* thePort);

    bool isHavingStreamPort() const;

    SUPERVGUI_Main* myMain;
    QPopupMenu*     myPopup;
    int             myAddStudyItem;
    int             myPasteNodeItem;
    int             myDSParamsItem;
    QPopupMenu*     mySketchPopup;
    int             myDelPntItem;
    int             myOrtoItem;
    int             myShowToolBarItem;

    bool         myIsPanBtnClicked;
    bool         myIsPanActivated;
    bool         myIsFitWRActivated;
    bool         myIsZoomActivated;
    bool         myIsLinkCreating;

    QCanvasItem*      myCurrentItem;
    QPoint            myPoint;
    QCanvasRectangle* mySelectedRect;
    QCursor           myCursor;

    QTimer*  myTimer;   
    QPoint   myGlobalPoint;
    int myDX;
    int myDY;

    SUPERVGUI_CanvasLink* myHilighted;
    SUPERVGUI_CanvasLinkBuilder* myLinkBuilder;
};

#endif

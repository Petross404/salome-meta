//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : StreamGraph_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

using namespace std;

#include "StreamGraph_Impl.hxx"

StreamGraph_Impl::StreamGraph_Impl( CORBA::ORB_ptr orb ,
			            PortableServer::POA_ptr poa ,
	       	                    PortableServer::ObjectId * contId , 
			            const char *instanceName ,
                                    const char *interfaceName ,
                                    const char *aDataFlowName ,
                                    const SUPERV::KindOfNode aKindOfGraphNode ) :
//  Graph_Impl(orb, poa, contId, instanceName, interfaceName, aDataFlowName , SUPERV::DataStreamGraph ) {
  Graph_Impl(orb, poa, contId, instanceName, interfaceName, aDataFlowName , aKindOfGraphNode ) {
//  MESSAGE("StreamGraph_Impl::StreamGraph_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
  beginService( "StreamGraph_Impl::StreamGraph_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;

  endService( "StreamGraph_Impl::StreamGraph_Impl" );
}

StreamGraph_Impl::StreamGraph_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        GraphEditor::InNode * aDataFlowNode ) :
  Graph_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
  beginService( "StreamGraph_Impl::StreamGraph_Impl" );
  if ( aDataFlowEditor->Graph()->IsDataStreamNode() ||
       ( aDataFlowNode && aDataFlowNode->IsMacroNode() ) ) {
    MESSAGE( "StreamGraph_Impl::StreamGraph_Impl _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  else {
    MESSAGE( "StreamGraph_Impl::StreamGraph_Impl NO _poa->activate_object " );
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  endService( "StreamGraph_Impl::StreamGraph_Impl" );  
}

StreamGraph_Impl::StreamGraph_Impl() {
}

StreamGraph_Impl::~StreamGraph_Impl() {
  beginService( "StreamGraph_Impl::~StreamGraph_Impl" );
  endService( "StreamGraph_Impl::~StreamGraph_Impl" );
}

void StreamGraph_Impl::destroy() {
  beginService( "StreamGraph_Impl::destroy" );
  _poa->deactivate_object(*_id) ;
//  CORBA::release(_poa) ;
  delete(_id) ;
//  _thisObj->_remove_ref();
  endService( "StreamGraph_Impl::destroy" );
}

SUPERV::StreamGraph_ptr StreamGraph_Impl::StreamCopy() {
  beginService( "StreamGraph_Impl::StreamCopy" );
  SUPERV::StreamGraph_var iobject = SUPERV::StreamGraph::_nil() ;
  if ( !IsMacro() ) {
    StreamGraph_Impl * myStreamGraph ;
    myStreamGraph = new StreamGraph_Impl( _Orb , _Poa, _ContId,
                                          instanceName() , interfaceName() ,
                                          DataFlowEditor()->Graph()->Name() ) ;
    PortableServer::ObjectId * id = myStreamGraph->getId() ;
    CORBA::Object_var obj = _poa->id_to_reference(*id);
    iobject = SUPERV::StreamGraph::_narrow(obj) ;
    GraphBase::ListOfSGraphs * aListOfSGraphs = GetGraphs() ;
    myStreamGraph->LoadGraphs( aListOfSGraphs ) ;
  }
  endService( "StreamGraph_Impl::StreamCopy" );
  return SUPERV::StreamGraph::_duplicate(iobject) ;
}

SUPERV::StreamGraph_ptr StreamGraph_Impl::StreamMNode( const char * aXmlFileName ) {
  beginService( "StreamGraph_Impl::StreamMNode" );
  SUPERV::StreamGraph_var streamiobject = SUPERV::StreamGraph::_nil() ;
  SUPERV::StreamGraph_var macroiobject = SUPERV::StreamGraph::_nil() ;
#if 0
  string dbgfile ;
  GraphEditor::DataFlow * aDataFlowEditor ;
  CreateEditor( _Orb , instanceName() , aXmlFileName , SUPERV::DataStreamGraph ,
                dbgfile , &aDataFlowEditor ) ;
  int lenname = strlen( aXmlFileName ) ;
  bool created = false ;
  if ( lenname > 4 && !strcmp( &aXmlFileName[ lenname - 4 ] , ".xml" ) ) {
    created = aDataFlowEditor->LoadXml( aXmlFileName ) ;
  }
  if ( created ) {
    StreamGraph_Impl * myStreamGraph ;
    myStreamGraph = new StreamGraph_Impl( _Orb , _Poa, _ContId,
                                          instanceName() , interfaceName() ,
                                          aDataFlowEditor , NULL ) ;
    PortableServer::ObjectId * id = myStreamGraph->getId() ;
    CORBA::Object_var obj = _poa->id_to_reference(*id);
    streamiobject = SUPERV::StreamGraph::_narrow( obj ) ;
    myStreamGraph->SetObjRef( SUPERV::StreamGraph::_duplicate( streamiobject ) ) ;

    StreamGraph_Impl * myStreamMacroNode ;
    myStreamMacroNode = new StreamGraph_Impl( _Orb , _Poa, _ContId,
                                              instanceName() , interfaceName() ,
                                              aXmlFileName , SUPERV::StreamMacroNode ) ;
    id = myStreamMacroNode->getId() ;
    obj = _poa->id_to_reference(*id);
    macroiobject = SUPERV::StreamGraph::_narrow( obj ) ;
    myStreamMacroNode->DataFlowEditor( DataFlowEditor() ) ;
    myStreamMacroNode->DataFlowEditor()->Graph()->SetObjImpl( this ) ;
    myStreamMacroNode->SetObjRef( SUPERV::CNode::_duplicate( SUPERV::CNode::_narrow( obj ) ) ) ;
    char * aMacroNodeName = myStreamGraph->Name() ;
    GraphBase::ListOfFuncName aFuncName ;
    aFuncName.resize( 1 ) ;
    aFuncName[0] = "" ;
    SUPERV::ListOfStrings aListOfStrings ;
    aListOfStrings.length( 1 ) ;
    aListOfStrings[0] = "" ;
    GraphBase::ListOfPythonFunctions aPythonFunction ;
    aPythonFunction.resize( 1 ) ;
    aPythonFunction[0] = &aListOfStrings ;
    SALOME_ModuleCatalog::Service * aMacroNodeService = myStreamGraph->Service() ;
    GraphEditor::InNode * aDataFlowNode = DataFlowEditor()->AddNode( *aMacroNodeService , "" , "" ,
                                                                     aMacroNodeName , SUPERV::StreamMacroNode ,
                                                                     aFuncName , aPythonFunction ) ;
    myStreamMacroNode->DataFlowNode( aDataFlowNode ) ;
    myStreamGraph->MacroObject( SUPERV::StreamGraph::_duplicate( macroiobject ) ) ;
    myStreamMacroNode->MacroObject( SUPERV::StreamGraph::_duplicate( streamiobject ) ) ;
    MESSAGE( "DataFlowNode Graph " << this << " DataFlowEditor " << DataFlowEditor() << " aDataFlowNode "
             << aDataFlowNode << " " << aDataFlowNode->Name()
             << " created" ) ;
  }
  else {
    delete aDataFlowEditor ;
  }
#endif
  endService( "StreamGraph_Impl::StreamMNode" );
  return SUPERV::StreamGraph::_duplicate( macroiobject ) ;
}

SUPERV::StreamGraph_ptr StreamGraph_Impl::StreamGraphMNode( SUPERV::StreamGraph_ptr myMacroStreamGraph ) {
  SUPERV::StreamGraph_var iobject = myMacroStreamGraph ;
//  GraphBase::Graph * myMacroGraph = aGraph;
  SUPERV::StreamGraph_var macroiobject = SUPERV::StreamGraph::_nil() ;
  beginService( "StreamGraph_Impl::StreamGraphMNode" ) ;
  endService( "StreamGraph_Impl::StreamGraphMNode" ) ;
  return SUPERV::StreamGraph::_duplicate( macroiobject ) ;
}

SUPERV::StreamLink_ptr StreamGraph_Impl::StreamLink(  SUPERV::StreamPort_ptr OutStreamPort ,
                                                      SUPERV::StreamPort_ptr InStreamPort ) {
  beginService( "StreamGraph_Impl::StreamLink" );
  SUPERV::StreamLink_var iobject = SUPERV::StreamLink::_nil() ;
  if ( DataFlowEditor()->IsEditing() && !DataFlowEditor()->IsReadOnly() && !IsMacro() &&
       OutStreamPort->IsDataStream() && InStreamPort->IsDataStream() ) {
    GraphBase::InPort * anInPort = DataFlowEditor()->GetNode( InStreamPort->Node()->Name() )->ComputingNode()->GetChangeInPort( InStreamPort->Name() ) ;
    GraphBase::OutPort * anOutPort = DataFlowEditor()->GetNode( OutStreamPort->Node()->Name() )->ComputingNode()->GetChangeOutPort( OutStreamPort->Name() ) ;
    if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
      const char * DataFlowOutNodeName = OutStreamPort->Node()->Name() ;
      GraphEditor::InNode * DataFlowOutNode = DataFlowEditor()->GetNode( DataFlowOutNodeName ) ;
      const char * DataFlowInNodeName = InStreamPort->Node()->Name() ;
      GraphEditor::InNode * DataFlowInNode = DataFlowEditor()->GetNode( DataFlowInNodeName ) ;
      if ( DataFlowOutNode && DataFlowInNode ) {
        bool Success ;
        StreamLink_Impl * myStreamLink = new StreamLink_Impl( _Orb , _Poa , _ContId ,
                                            instanceName() , interfaceName() ,
                                            DataFlowEditor() ,
                                            DataFlowInNode ,
                                            InStreamPort->Name() ,
                                            DataFlowOutNode ,
                                            OutStreamPort->Name() ,
                                            true , Success ) ;
        if ( Success ) {
          PortableServer::ObjectId * id = myStreamLink->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          iobject = SUPERV::StreamLink::_narrow(obj) ;
          anOutPort->AddInPortObjRef( anInPort , SUPERV::StreamLink::_duplicate(iobject) ) ;
          DataFlowEditor()->UnValid() ;
        }
      }
    }
    else {
      iobject = SUPERV::StreamLink::_narrow( anOutPort->InPortObjRef( anInPort ) ) ;
    }
  }
  endService( "StreamGraph_Impl::StreamLink" );
  return SUPERV::StreamLink::_duplicate(iobject) ;
}

SUPERV::ListOfStreamLinks * StreamGraph_Impl::GStreamLinks() {
  return StreamLinks( NULL , NULL ) ;
}

SUPERV::ListOfStreamLinks * StreamGraph_Impl::StreamLinks( GraphBase::ComputingNode * theNode ,
                                                           const char * anInputParam ) {
  bool begin = true ;
  SUPERV::ListOfStreamLinks_var RetVal = new SUPERV::ListOfStreamLinks ;
  if ( !IsMacro() ) {
    int i , j , countlink ;
    countlink = 0 ;
    for ( i = 0 ; i < DataFlowEditor()->Graph()->GraphNodesSize() ; i++ ) {
      GraphEditor::InNode * aNode = NULL ;
      aNode = (GraphEditor::InNode * ) DataFlowEditor()->Graph()->GraphNodes( i )->GetInNode() ;
      bool ToProcess = false ;
      if ( theNode == NULL ) {
        ToProcess = true ;
      }
      else {
        if ( !strcmp( theNode->Name() , aNode->Name() ) ) {
          if ( !theNode->IsEndSwitchNode() ) {
            ToProcess = true ;
  	  }
        }
        else if ( theNode->IsEndSwitchNode() ) {
          ToProcess = true ;
        }
      }
      if ( ToProcess ) {
        for ( j = 0 ; j < aNode->GetNodeInPortsSize() ; j++ ) {
          GraphBase::InPort * anInPort = NULL ;
          anInPort = aNode->GetChangeNodeInPort( j ) ;
          if ( anInputParam == NULL ||
               !strcmp( anInPort->PortName() , anInputParam ) ) {
            GraphBase::OutPort * anOutPort = NULL ;
            anOutPort = anInPort->GetOutPort() ;
            if ( anOutPort && anOutPort->IsDataStream() ) {
              if ( strcmp( anOutPort->NodeName() , Name() ) ) {
                MESSAGE("StreamGraph_Impl::StreamLinks " << anOutPort->NodeName() << "("
                        << anOutPort->PortName() << ") --> " << aNode->Name() << "("
                        << anInPort->PortName() << ")" ) ;
                if ( theNode == NULL ||
//PAL8521
//JR 14.02.2005 : Debug : we must see also that links !
//                     ( theNode != NULL && !theNode->IsEndSwitchNode() &&
                       !strcmp( theNode->Name() , aNode->Name() ) ) {
                  if ( anInPort->IsLoop() || anOutPort->IsLoop() ||
                       ( aNode->IsEndLoopNode() && !strcmp( aNode->CoupledNode()->Name() ,
                                                            anOutPort->NodeName() ) ) ) {
                    MESSAGE( "StreamLink " << anOutPort->NodeName() << "("
                            << anOutPort->PortName() << ") --> " << aNode->Name() << "("
                            << anInPort->PortName() << ")" << " ignored" ) ;
	    	  }
                  else if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
                    if ( begin ) {
                      beginService( "StreamGraph_Impl::StreamLinks" );
                      begin = false ;
                    }
                    GraphEditor::InNode * anOutNode = NULL ;
                    anOutNode = (GraphEditor::InNode * ) DataFlowEditor()->Graph()->GetChangeGraphNode( anOutPort->NodeName() )->GetInNode() ;
                    if ( anOutNode ) {
                      bool Success ;
                      StreamLink_Impl * myStreamLink = new StreamLink_Impl(
                                            _Orb , _Poa , _ContId ,
                                            instanceName() , interfaceName() ,
                                            DataFlowEditor() ,
                                            aNode ,
                                            anInPort->PortName() ,
                                            anOutNode ,
                                            anOutPort->PortName() ,
                                            false , Success ) ;
                      if ( Success ) {
                        PortableServer::ObjectId * id = myStreamLink->getId() ;
                        CORBA::Object_var obj = _poa->id_to_reference(*id);
                        SUPERV::StreamLink_var iobject ;
                        iobject = SUPERV::StreamLink::_narrow(obj) ;
                        RetVal->length( countlink + 1 ) ;
                        RetVal[ countlink++ ] = SUPERV::StreamLink::_duplicate( iobject ) ;
                        anOutPort->AddInPortObjRef( anInPort , SUPERV::StreamLink::_duplicate( iobject ) ) ;
                        MESSAGE( "Link" << countlink-1 << " "
                                 << RetVal[countlink-1]->OutStreamPort()->Node()->Name() << "("
                                 << RetVal[countlink-1]->OutStreamPort()->Name() << ") --> "
                                 << RetVal[countlink-1]->InStreamPort()->Node()->Name() << "("
                                 << RetVal[countlink-1]->InStreamPort()->Name() << ")" ) ;
		      }
	            }
	          }
                  else {
                    RetVal->length( countlink + 1 ) ;
                    RetVal[ countlink++ ] = SUPERV::StreamLink::_duplicate( SUPERV::StreamLink::_narrow( anOutPort->InPortObjRef( anInPort ) ) ) ;
                    MESSAGE( "Link" << countlink-1 << " "
                             << RetVal[countlink-1]->OutStreamPort()->Node()->Name() << "("
                             << RetVal[countlink-1]->OutStreamPort()->Name() << ") --> "
                             << RetVal[countlink-1]->InStreamPort()->Node()->Name() << "("
                             << RetVal[countlink-1]->InStreamPort()->Name() << ")" ) ;
	          }
	        }
	      }
            }
	  }
        }
      }
      for ( j = 0 ; j < aNode->GetNodeOutPortsSize() ; j++ ) {
        GraphBase::OutPort * anOutPort = aNode->GetChangeNodeOutPort( j ) ;
        int k ;
        for ( k = 0 ; k < anOutPort->InPortsSize() ; k++ ) {
          GraphBase::InPort * anInPort = anOutPort->ChangeInPorts( k ) ;
          GraphEditor::InNode * toNode = (GraphEditor::InNode * ) DataFlowEditor()->Graph()->GetChangeGraphNode( anInPort->NodeName() )->GetInNode() ;
          if ( theNode == NULL ||
               !strcmp( theNode->Name() , aNode->Name() ) ) {
            if ( anInPort->IsDataStream() ) {
//PAL8521
//JR 14.02.2005 : Debug : we must see also that links !
//              if ( theNode || ( toNode->IsEndSwitchNode() && !aNode->IsSwitchNode() ) ) {
                if ( anInputParam == NULL ||
                     !strcmp( anInPort->PortName() , anInputParam ) ) {
                  MESSAGE( "StreamLink " << anOutPort->NodeName() << "("
                           << anOutPort->PortName() << ") --> " << toNode->Name() << "("
                           << anInPort->PortName() << ")" ) ;
                  MESSAGE( "           IOR " << DataFlowEditor()->ObjectToString( anOutPort->InPortObjRef( anInPort ) ) ) ;
                  if ( anInPort->IsLoop() || anOutPort->IsLoop() ||
                       ( toNode->IsEndLoopNode() && !strcmp( toNode->CoupledNode()->Name() ,
                                                             anOutPort->NodeName() ) ) ) {
                    MESSAGE( "StreamLink " << anOutPort->NodeName() << "("
                             << anOutPort->PortName() << ") --> " << toNode->Name() << "("
                             << anInPort->PortName() << ")" << " ignored" ) ;
	          }
                  else if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
                    if ( begin ) {
                      beginService( "Graph_Impl::StreamLinks" );
                      begin = false ;
                    }
                    bool Success ;
                    StreamLink_Impl * myStreamLink = new StreamLink_Impl(
                                          _Orb , _Poa , _ContId ,
                                          instanceName() , interfaceName() ,
                                          DataFlowEditor() ,
                                          toNode ,
                                          anInPort->PortName() ,
                                          aNode ,
                                          anOutPort->PortName() ,
                                          false , Success ) ;
                    if ( Success ) {
                      PortableServer::ObjectId * id = myStreamLink->getId() ;
                      CORBA::Object_var obj = _poa->id_to_reference(*id);
                      SUPERV::StreamLink_var iobject ;
                      iobject = SUPERV::StreamLink::_narrow(obj) ;
                      RetVal->length( countlink + 1 ) ;
                      RetVal[ countlink++ ] = SUPERV::StreamLink::_duplicate( iobject ) ;
                      anOutPort->AddInPortObjRef( anInPort , SUPERV::StreamLink::_duplicate( iobject ) ) ;
                      MESSAGE( "Link" << countlink-1 << " "
                               << RetVal[countlink-1]->OutStreamPort()->Node()->Name() << "("
                               << RetVal[countlink-1]->OutStreamPort()->Name() << ") --> "
                               << RetVal[countlink-1]->InStreamPort()->Node()->Name() << "("
                               << RetVal[countlink-1]->InStreamPort()->Name() << ")" ) ;
		    }
	          }
                  else {
                    RetVal->length( countlink + 1 ) ;
                    RetVal[ countlink++ ] = SUPERV::StreamLink::_duplicate( SUPERV::StreamLink::_narrow( anOutPort->InPortObjRef( anInPort ) ) ) ;
                    MESSAGE( "Link" << countlink-1 << " "
                             << RetVal[countlink-1]->OutStreamPort()->Node()->Name() << "("
                             << RetVal[countlink-1]->OutStreamPort()->Name() << ") --> "
                             << RetVal[countlink-1]->InStreamPort()->Node()->Name() << "("
                             << RetVal[countlink-1]->InStreamPort()->Name() << ")" ) ;
	          }
	        }
                else {
                  MESSAGE( "StreamLink " << anOutPort->NodeName() << "("
                           << anOutPort->PortName() << ") --> " << toNode->Name() << "("
                           << anInPort->PortName() << ")" << " skipped" ) ;
	        }
//	      }
	    }
	  }
        }
      }
    }
//#if 0
    const char * NodeName = "" ;
    const char * InputParamName = "" ;
    if ( theNode ) {
      NodeName = theNode->Name() ;
    }
    if ( anInputParam ) {
      InputParamName = anInputParam ;
    }
    MESSAGE( RetVal->length() << " StreamLinks of Node " << NodeName << " and of InPort " << InputParamName ) ;
    for ( i = 0 ; i < (int ) RetVal->length() ; i++ ) {
      MESSAGE( "Link" << i << " " << RetVal[i]->OutStreamPort()->Node()->Name() << "("
               << RetVal[i]->OutStreamPort()->Name() << ") --> "
               << RetVal[i]->InStreamPort()->Node()->Name() << "("
               << RetVal[i]->InStreamPort()->Name() << ")" ) ;
    }
//#endif
    if ( !begin ) {
      endService( "StreamGraph_Impl::StreamLinks" );
    }
  }
  return ( RetVal._retn() ) ;
}

bool StreamGraph_Impl::SetStreamParams( CORBA::Long Timeout ,
                                        const SUPERV::KindOfDataStreamTrace DataStreamTrace ,
                                        CORBA::Double  DeltaTime ) {
  bool sts = false ;
  if ( !IsMacro() ) {
    sts = DataFlowEditor()->StreamGraph()->SetStreamParams( Timeout , DataStreamTrace , DeltaTime ) ;
    if ( sts ) {
      DataFlowEditor()->UnValid() ;
    }
  }
  return sts ;
}

void StreamGraph_Impl::StreamParams( CORBA::Long & Timeout ,
                                     SUPERV::KindOfDataStreamTrace & DataStreamTrace ,
                                     CORBA::Double & DeltaTime ) {
  if ( !IsMacro() ) {
    DataFlowEditor()->StreamGraph()->StreamParams( Timeout , DataStreamTrace , DeltaTime ) ;
  }
}

CORBA::Long StreamGraph_Impl::SubStreamGraphsNumber() {
//  beginService( "StreamGraph_Impl::StreamGraphsNumber" );
  CORBA::Long RetVal = 0 ;
  if ( DataFlowEditor()->IsExecutable() && !IsMacro() ) {
    RetVal =  DataFlowEditor()->SubStreamGraphsNumber() ;
  }
//  endService( "StreamGraph_Impl::SubStreamGraphsNumber" );
  return RetVal ;
}

SUPERV::ListOfNodes * StreamGraph_Impl::SubStreamGraphsNodes( CORBA::Long aSubStreamGraphNumber ) {
  beginService( "StreamGraph_Impl::SubStreamGraphsNodes" );
  SUPERV::ListOfNodes_var RetVal = new SUPERV::ListOfNodes ;
  if ( DataFlowEditor()->IsEditing() && !IsMacro() ) {
    SUPERV::ListOfNodes * aGraphNodes = Nodes() ;
    int i ;
// ComputingNodes
    for ( i = 0 ; i < (int ) aGraphNodes->CNodes.length() ; i++ ) {
      SUPERV::CNode_var aNode = (aGraphNodes->CNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// FactoryNodes
    for ( i = 0 ; i < (int ) aGraphNodes->FNodes.length() ; i++ ) {
      SUPERV::FNode_var aNode = (aGraphNodes->FNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// InLineNodes
    for ( i = 0 ; i < (int ) aGraphNodes->INodes.length() ; i++ ) {
      SUPERV::INode_var aNode = (aGraphNodes->INodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// GOTONodes
    for ( i = 0 ; i < (int ) aGraphNodes->GNodes.length() ; i++ ) {
      SUPERV::GNode_var aNode = (aGraphNodes->GNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// LoopNodes
    for ( i = 0 ; i < (int ) aGraphNodes->LNodes.length() ; i++ ) {
      SUPERV::LNode_var aNode = (aGraphNodes->LNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// EndLoopNodes
    for ( i = 0 ; i < (int ) aGraphNodes->ELNodes.length() ; i++ ) {
      SUPERV::ELNode_var aNode = (aGraphNodes->ELNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// SwitchNodes
    for ( i = 0 ; i < (int ) aGraphNodes->SNodes.length() ; i++ ) {
      SUPERV::SNode_var aNode = (aGraphNodes->SNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
// EndSwitchNodes
    for ( i = 0 ; i < (int ) aGraphNodes->ESNodes.length() ; i++ ) {
      SUPERV::ESNode_var aNode = (aGraphNodes->ESNodes)[ i ] ;
      if ( aNode->SubStreamGraph() == aSubStreamGraphNumber ) {
        RetVal = SetNode( RetVal , DataFlowEditor()->Graph()->GetChangeGraphNode( aNode->Name() ) ) ;
      }
    }
  }
  endService( "StreamGraph_Impl::SubStreamGraphsNodes" );
  return ( RetVal._retn() ) ;
}

SUPERV::Graph_ptr StreamGraph_Impl::ToFlowGraph() {
  SUPERV::Graph_var iobject = SUPERV::Graph::_nil() ;
  beginService( "StreamGraph_Impl::ToFlowGraph" );
  if ( !IsMacro() ) {
//    Graph_Impl * myFlowGraph = new Graph_Impl( _Orb , _Poa , _ContId ,
//                                               instanceName() , interfaceName() ) ;
//    PortableServer::ObjectId * id = myFlowGraph->getId() ;
//    CORBA::Object_var obj = _poa->id_to_reference(*id);
    if ( !CORBA::is_nil( DataFlowEditor()->StreamGraph()->ObjRef() ) ) {
      iobject = SUPERV::Graph::_narrow( DataFlowEditor()->StreamGraph()->ObjRef() ) ;
    }
  }
  endService( "StreamGraph_Impl::ToFlowGraph" );
  return SUPERV::Graph::_duplicate( iobject ) ;
}

bool StreamGraph_Impl::StreamMerge(const SUPERV::StreamGraph_ptr aStreamGraph ) {
  beginService( "StreamGraph_Impl::StreamMerge" );
  bool RetVal = false ;
  if ( !IsMacro() ) {
    map< string , int > aMapOfNodes ;
    RetVal = Merge( aStreamGraph , aMapOfNodes ) ;
    if ( RetVal ) {
      SUPERV::ListOfStreamLinks * aGraphLinks = aStreamGraph->GStreamLinks() ;
      SUPERV::ListOfStreamPorts * aGraphPorts = aStreamGraph->StreamPorts() ;
  //cout << "Graph_Impl::StreamMerge " << aGraphLinks->length() << " links " << aGraphPorts->length()
//           << " GraphPorts" << endl ;
      int i ;
      for ( i = 0 ; i < (int ) aGraphLinks->length() ; i++ ) {
        SUPERV::StreamLink_var aLink = (*aGraphLinks)[ i ] ;
        SUPERV::StreamPort_var OutPort = aLink->OutStreamPort() ;
        SUPERV::StreamPort_var InPort = aLink->InStreamPort() ;
        string * aLinkFromNodeName = new string( OutPort->Node()->Name() ) ;
        string * aLinkToNodeName = new string( InPort->Node()->Name() ) ;
  //cout << "Graph_Impl::StreamMerge " << aLinkFromNodeName << "(" << OutPort->Name() << ") ---> "
//             << aLinkToNodeName << "(" << InPort->Name() << ")" << endl ;
        RetVal = DataFlowEditor()->AddLink( DataFlowEditor()->Graph()->GetGraphNode( aMapOfNodes[ aLinkFromNodeName->c_str() ] )->Name() ,
                                            OutPort->Name() ,
                                            DataFlowEditor()->Graph()->GetGraphNode( aMapOfNodes[ aLinkToNodeName->c_str() ] )->Name() ,
                                            InPort->Name() ) ;
  //cout << "Graph_Impl::StreamMerge " << aLinkFromNodeName << "(" << OutPort->Name() << ") ---> "
//           << aLinkToNodeName << "(" << InPort->Name() << ") RetVal" << RetVal << endl ;
        if ( RetVal ) {
          CORBA::Long j ;
          for ( j = 1 ; j <= aLink->CoordsSize() ; j++ ) {
            CORBA::Long X , Y ;
            RetVal = aLink->Coords( j , X , Y ) ;
            if ( !RetVal )
              break ;
            RetVal = DataFlowEditor()->AddLinkCoord( DataFlowEditor()->Graph()->GetGraphNode( aMapOfNodes[ aLinkFromNodeName->c_str() ] )->Name() ,
                                                     OutPort->Name() ,
                                                     DataFlowEditor()->Graph()->GetGraphNode( aMapOfNodes[ aLinkToNodeName->c_str() ] )->Name() ,
                                                     InPort->Name() ,
                                                     j , X , Y ) ;
            if ( !RetVal ) {
              break ;
	    }
	  }
        }
        delete aLinkFromNodeName ;
        delete aLinkToNodeName ;
        if ( !RetVal ) {
          break ;
        }
      }
      if ( RetVal ) {
        for ( i = 0 ; i < (int ) aGraphPorts->length() ; i++ ) {
          SUPERV::StreamPort_var aPort = (*aGraphPorts)[ i ] ;
          if ( !aPort->IsGate() ) {
            MESSAGE( "Graph_Impl::StreamMerge " << i << ". " << aPort->Node()->Name() << " " << aPort->Name() ) ;
            char * aPortName = aPort->Name() ;
            char * aNodeName = new char[ strlen( aPortName ) + 1 ] ;
            strcpy( aNodeName , aPortName ) ;
//            char * thePortName = strchr( aNodeName , '\\' ) ;
            char * thePortName = strchr( aNodeName , '_' ) ;
            thePortName[ 0 ] = '\0' ;
            bool hasinput = aStreamGraph->Node( aNodeName )->Port( thePortName + 1 )->HasInput() ;
//            cout << "Graph_Impl::StreamMerge " << " aNodeName " << aNodeName << " aPort " << thePortName + 1
//                 << " HasInput " << hasinput << endl ;
            if ( hasinput ) {
              RetVal = DataFlowEditor()->AddInputData( DataFlowEditor()->StreamGraph()->GetGraphNode( aMapOfNodes[ aNodeName ] )->Name() ,
                                                       thePortName + 1 ,
                                                       *(aPort->ToAny()) ) ;
	    }
            delete [] aNodeName ;
            if ( !RetVal ) {
              break ;
	    }
  	  }
        }
      }
    }
  }
  endService( "StreamGraph_Impl::StreamMerge" );
  return RetVal ;
}


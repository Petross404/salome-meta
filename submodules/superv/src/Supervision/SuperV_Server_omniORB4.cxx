// Copyright (C) 2005  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either 
// version 2.1 of the License.
// 
// This library is distributed in the hope that it will be useful 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public  
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <iostream>
#include <CORBA.h>
#include "ObjRef_omniORB4.hh"

using namespace std;

class ObjRef_i : public POA_ObjRef_omniORB4::ObjRef {

public:

ObjRef_i() {} ;
virtual ~ObjRef_i() {} ;

virtual ObjRef_omniORB4::ObjRef_ptr getObjRef( const ObjRef_omniORB4::ObjRef_ptr anObjRef ) ;

virtual bool ping() ;

} ;


ObjRef_omniORB4::ObjRef_ptr ObjRef_i::getObjRef( const ObjRef_omniORB4::ObjRef_ptr anObjRef ) {

  cout << "ObjRef_i::getObjRef" << endl ;
  anObjRef->ping() ;
  cout << "ObjRef_i::getObjRef will return the same object reference" << endl ;
  return ObjRef_omniORB4::ObjRef::_duplicate( anObjRef ) ;

} ;

bool ObjRef_i::ping() {
  cout << "ping" << endl ;
  return true ;
}

int main( int argc , char ** argv ) {

  try {
    CORBA::ORB_var orb = CORBA::ORB_init( argc , argv , "omniORB4" ) ;
    CORBA::Object_var obj = orb->resolve_initial_references( "RootPOA" ) ;
    PortableServer::POA_var poa = PortableServer::POA::_narrow( obj ) ;
    ObjRef_i * myObjRef = new ObjRef_i() ;
    PortableServer::ObjectId_var myObjectId = poa->activate_object( myObjRef ) ;
    obj = myObjRef->_this() ;
    CORBA::String_var myIOR = orb->object_to_string( obj ) ;
    cout << "myIOR : '" << (char * ) myIOR << "'" << endl ;
    myObjRef->_remove_ref() ;
    PortableServer::POAManager_var pman = poa->the_POAManager() ;
    pman->activate() ;
    orb->run() ;
    orb->destroy() ;
  }
  catch(CORBA::SystemException & ) {
    cout << "Caught CORBA::SystemException" << endl ;
  }
  catch(CORBA::Exception & ) {
    cout << "Caught CORBA::Exception" << endl ;
  }
  catch(omniORB::fatalException & fe ) {
    cout << "Caught omniORB::fatalException" << endl ;
    cout << "      file   " << fe.file() << endl ;
    cout << "      line   " << fe.line() << endl ;
    cout << "      errmsg " << fe.errmsg() << endl ;
  }
  catch( ... ) {
    cout << "Caught unknown exception" << endl ;
  }
  return 0 ;
}

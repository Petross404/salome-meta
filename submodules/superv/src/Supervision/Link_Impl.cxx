//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : Link_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "StreamLink_Impl.hxx"

Link_Impl::Link_Impl( CORBA::ORB_ptr orb ,
		      PortableServer::POA_ptr poa ,
	       	      PortableServer::ObjectId * contId , 
		      const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * DataFlowEditor ,
                      GraphEditor::InNode * DataFlowNode ,
                      const char *InputParameterName ,
                      GraphEditor::InNode * DataFlowOutNode ,
                      const char *OutputParameterName ,
                      const bool Create ,
                      const bool Get ,
                      bool & Success ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
//  MESSAGE("Link_Impl::Link_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
//  beginService( "Link_Impl::Link_Impl" );
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = DataFlowEditor ;
  _DataFlowNode = DataFlowNode ;
  _DataFlowNodeName = DataFlowNode->Name() ;
  _InputParameterName = new char[ strlen( InputParameterName ) + 1 ] ;
  strcpy( _InputParameterName , InputParameterName ) ;
  _DataFlowOutNode = DataFlowOutNode ;
  _DataFlowOutNodeName = DataFlowOutNode->Name() ;
  _OutputParameterName = new char[ strlen( OutputParameterName ) + 1 ] ;
  strcpy( _OutputParameterName , OutputParameterName ) ;
  if ( Create ) {
    Success = _DataFlowEditor->AddLink( _DataFlowOutNode->Name() ,
                                        _OutputParameterName , 
                                        _DataFlowNode->Name() ,
                                        _InputParameterName ) ;
  }
  else if ( Get ) {
    char * DataFlowOutNodeName = NULL ;
    char * OutParamName = NULL ;
    Success = _DataFlowEditor->GetLink( _DataFlowNode->Name() ,
                                        _InputParameterName , 
                                        &DataFlowOutNodeName ,
                                        &OutParamName ) ;
  }
  else {
    Success = false ;
  }
  if ( Success ) {
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj) ;
  }
//  endService( "Link_Impl::Link_Impl" );
}

Link_Impl::Link_Impl() {
}

Link_Impl::~Link_Impl() {
  beginService( "Link_Impl::~Link_Impl" );
  endService( "Link_Impl::~Link_Impl" );
}

void Link_Impl::destroy() {
  beginService( "Link_Impl::destroy" );
  Delete() ;
  _poa->deactivate_object(*_id) ;
//  CORBA::release(_poa) ;
  delete(_id) ;
//  _thisObj->_remove_ref();
  endService( "Link_Impl::destroy" );
}

bool Link_Impl::Delete() {
//  beginService( "Link_Impl::Delete" ) ;
  bool RetVal = _DataFlowEditor->RemoveLink( _DataFlowOutNode->Name() ,
                                             _OutputParameterName ,
                                             _DataFlowNode->Name() ,
                                             _InputParameterName ) ;
  if ( RetVal ) {
    DataFlowEditor()->UnValid() ;
  }
//  endService( "Link_Impl::Delete" );
  return RetVal ;
}

SUPERV::Port_ptr Link_Impl::InPort() {
//  beginService( "Link_Impl::InPort" );
  SUPERV::Port_var iobject = SUPERV::Port::_nil() ;
  GraphBase::InPort * anInPort = _DataFlowNode->GetChangeInPort( _InputParameterName ) ;
  if ( anInPort ) {
    iobject = anInPort->ObjRef() ;
    if ( CORBA::is_nil( iobject ) ) {
      if ( !anInPort->IsDataStream() ) {
        Port_Impl * myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                             instanceName() , interfaceName() ,
                                             _DataFlowEditor ,
                                             _DataFlowNode ,
                                             (GraphBase::Port * ) anInPort ,
                                             true ) ;
        PortableServer::ObjectId * id = myPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        iobject = SUPERV::Port::_narrow(obj) ;
        anInPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
      }
      else {
        StreamPort_Impl * myStreamPort = new StreamPort_Impl(
                                       _Orb , _Poa , _ContId ,
                                       instanceName() , interfaceName() ,
                                       DataFlowEditor() ,
                                       DataFlowOutNode() ,
                                       (GraphBase::Port * ) anInPort ,
                                       false ) ;
        PortableServer::ObjectId * id = myStreamPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        SUPERV::StreamPort_var iStreamobject ;
        iStreamobject = SUPERV::StreamPort::_narrow(obj) ;
        anInPort->ObjRef( SUPERV::StreamPort::_duplicate( iStreamobject ) ) ;
        iobject = SUPERV::Port::_narrow( iStreamobject ) ;
      }
    }
    else {
      iobject = anInPort->ObjRef() ;
    }
  }
//  endService( "Link_Impl::InPort" );
  return SUPERV::Port::_duplicate( iobject ) ;
}

SUPERV::Port_ptr Link_Impl::OutPort() {
//  beginService( "Link_Impl::OutPort" );
  SUPERV::Port_var iobject = SUPERV::Port::_nil() ;
  GraphBase::OutPort * anOutPort = _DataFlowOutNode->GetChangeOutPort( _OutputParameterName ) ;
  if ( anOutPort ) {
    iobject = anOutPort->ObjRef() ;
    if ( CORBA::is_nil( iobject ) ) {
      if ( !anOutPort->IsDataStream() ) {
        Port_Impl * myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                             instanceName() , interfaceName() ,
                                             _DataFlowEditor ,
                                             _DataFlowOutNode ,
                                             (GraphBase::Port * ) anOutPort ,
                                             false ) ;
        PortableServer::ObjectId * id = myPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        iobject = SUPERV::Port::_narrow(obj) ;
        anOutPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
      }
      else {
        StreamPort_Impl * myStreamPort = new StreamPort_Impl(
                                       _Orb , _Poa , _ContId ,
                                       instanceName() , interfaceName() ,
                                       DataFlowEditor() ,
                                       DataFlowOutNode() ,
                                       (GraphBase::Port * ) anOutPort ,
                                       false ) ;
        PortableServer::ObjectId * id = myStreamPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        SUPERV::StreamPort_var iStreamobject ;
        iStreamobject = SUPERV::StreamPort::_narrow(obj) ;
        anOutPort->ObjRef( SUPERV::StreamPort::_duplicate( iStreamobject ) ) ;
        iobject = SUPERV::Port::_narrow( iStreamobject ) ;
      }
    }
    else {
      iobject = anOutPort->ObjRef() ;
    }
  }
//  endService( "Link_Impl::OutPort" );
  return SUPERV::Port::_duplicate( iobject ) ;
}

GraphBase::SLink * Link_Impl::Info() {
//  beginService( "Link_Impl::Info" );
//  char* FromNodeName ;
//  char* FromServiceParameterName ;
  GraphBase::SLink * RetVal = _DataFlowEditor->GetLink( _DataFlowNode->ComputingNode() ,
                                                        _InputParameterName ) ;
//  endService( "Link_Impl::Info" );
  return RetVal ;
}

CORBA::Long Link_Impl::CoordsSize() {
//  beginService( "Link_Impl::CoordsSize" ) ;
  CORBA::Long RetVal = _DataFlowEditor->GetLinkCoordSize( _DataFlowOutNode->Name() ,
                                                   _OutputParameterName ,
                                                   _DataFlowNode->Name() ,
                                                   _InputParameterName ) ;
//  endService( "Link_Impl::CoordsSize" );
  return RetVal ;
}
bool Link_Impl::AddCoord( CORBA::Long index ,
                          CORBA::Long X ,
                          CORBA::Long Y ) {
//  beginService( "Link_Impl::AddCoord" ) ;
  bool RetVal = _DataFlowEditor->AddLinkCoord( _DataFlowOutNode->Name() ,
                                               _OutputParameterName ,
                                               _DataFlowNode->Name() ,
                                               _InputParameterName,
                                               index , X , Y ) ;
//  endService( "Link_Impl::AddCoord" );
  return RetVal ;
}
bool Link_Impl::ChangeCoord( CORBA::Long index ,
                             CORBA::Long X ,
                             CORBA::Long Y ) {
//  beginService( "Link_Impl::ChangeCoord" ) ;
  bool RetVal = _DataFlowEditor->ChangeLinkCoord( _DataFlowOutNode->Name() ,
                                                  _OutputParameterName ,
                                                  _DataFlowNode->Name() ,
                                                  _InputParameterName,
                                                  index , X , Y ) ;
//  endService( "Link_Impl::ChangeCoord" );
  return RetVal ;
}
bool Link_Impl::RemoveCoord( CORBA::Long index ) {
//  beginService( "Link_Impl::RemoveCoord" ) ;
  bool RetVal = _DataFlowEditor->RemoveLinkCoord( _DataFlowOutNode->Name() ,
                                                  _OutputParameterName ,
                                                  _DataFlowNode->Name() ,
                                                  _InputParameterName,
                                                  index ) ;
//  endService( "Link_Impl::RemoveCoord" );
  return RetVal ;
}
bool Link_Impl::Coords( CORBA::Long index , CORBA::Long & X , CORBA::Long & Y ) {
//  beginService( "Link_Impl::Coords" ) ;
  bool RetVal = _DataFlowEditor->GetLinkCoord( _DataFlowOutNode->Name() ,
                                               _OutputParameterName ,
                                               _DataFlowNode->Name() ,
                                               _InputParameterName,
                                               index ,  X , Y ) ;
//  endService( "Link_Impl::Coords" );
  return RetVal ;
}

/** 
 * Return true if InPort's and OutPort's types are compatible
 * {"string", "boolean", "char", "short", "int", "long", "float", "double", "objref"};
 * Currently considered compatible ALL types except for objref - they must match exactly
 * To make sure no other rule of compatibility is used - see Editor::OutNode::IsCompatible().
 */ 
bool Link_Impl::IsValid() {
  if ( InPort() && OutPort() ) {
    return DataFlowEditor()->IsCompatible( OutPort()->Type(), InPort()->Type() );
  }
  return false;
}

/** 
 * Return true the ports of this link and theLink are equal.  If you do on the GUI side - it does not
 * return the correct value..
 */ 
bool Link_Impl::IsEqual( SUPERV::Link_ptr theLink ) {
  if ( InPort() && OutPort() && !CORBA::is_nil(theLink) ) {
    return ( InPort() == theLink->InPort() && OutPort() == theLink->OutPort() );
  }
  return false;
}

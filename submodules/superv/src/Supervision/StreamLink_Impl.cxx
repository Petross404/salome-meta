//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : StreamLink_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

using namespace std;

#include "StreamLink_Impl.hxx"

StreamLink_Impl::StreamLink_Impl( CORBA::ORB_ptr orb ,
		                  PortableServer::POA_ptr poa ,
	       	                  PortableServer::ObjectId * contId , 
		                  const char *instanceName ,
                                  const char *interfaceName ,
                                  GraphEditor::DataFlow * DataFlowEditor ,
                                  GraphEditor::InNode * DataFlowNode ,
                                  const char *InputParameterName ,
                                  GraphEditor::InNode * DataFlowOutNode ,
                                  const char *OutputParameterName ,
                                  const bool Create ,
                                  bool & Success ) :
  Link_Impl( orb , poa , contId , instanceName , interfaceName , DataFlowEditor ,
             DataFlowNode , InputParameterName , DataFlowOutNode ,
             OutputParameterName , false , false , Success ) {
//  MESSAGE("StreamLink_Impl::StreamLink_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
//  bool linksts ;
//  beginService( "StreamLink_Impl::Link_Impl" );
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  if ( Create ) {
    Success = DataFlowEditor->AddLink( DataFlowOutNode->Name() ,
                                       OutputParameterName , 
                                       DataFlowNode->Name() ,
                                       InputParameterName ) ;
  }
  else {
    char * DataFlowOutNodeName = NULL ;
    char * OutParamName = NULL ;
    Success = DataFlowEditor->GetLink( DataFlowNode->Name() ,
                                       InputParameterName , 
                                       &DataFlowOutNodeName ,
                                       &OutParamName ) ;
  }
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
//  endService( "StreamLink_Impl::Link_Impl" );
}

StreamLink_Impl::StreamLink_Impl() {
}

StreamLink_Impl::~StreamLink_Impl() {
  beginService( "Link_Impl::~Link_Impl" );
  endService( "Link_Impl::~Link_Impl" );
}

void StreamLink_Impl::destroy() {
  beginService( "StreamLink_Impl::destroy" );
  Delete() ;
  _poa->deactivate_object(*_id) ;
//  CORBA::release(_poa) ;
  delete(_id) ;
//  _thisObj->_remove_ref();
  endService( "StreamLink_Impl::destroy" );
}

bool StreamLink_Impl::Delete() {
//  beginService( "StreamLink_Impl::Delete" ) ;
  bool RetVal = DataFlowEditor()->RemoveLink( DataFlowOutNode()->Name() ,
                                              OutputParameterName() ,
                                              DataFlowNode()->Name() ,
                                              InputParameterName() ) ;
  if ( RetVal ) {
    DataFlowEditor()->UnValid() ;
  }
//  endService( "StreamLink_Impl::Delete" );
  return RetVal ;
}

SUPERV::StreamPort_ptr StreamLink_Impl::InStreamPort() {
//  beginService( "Link_Impl::InStreamPort" );
  SUPERV::StreamPort_var iobject = SUPERV::StreamPort::_nil() ;
  GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( InputParameterName() ) ;
  if ( anInPort && anInPort->IsDataStream() ) {
    iobject = SUPERV::StreamPort::_narrow( anInPort->ObjRef() ) ;
    if ( CORBA::is_nil( iobject ) ) {
//      MESSAGE( "StreamLink_Impl::InStreamPort creates myStreamPort "
//               << DataFlowNode()->Name() << "( " << anInPort->PortName() << " )" ) ;
      StreamPort_Impl * myStreamPort = new StreamPort_Impl(
                                       _Orb , _Poa , _ContId ,
                                       instanceName() , interfaceName() ,
                                       DataFlowEditor() ,
                                       DataFlowNode() ,
                                       (GraphBase::Port * ) anInPort ,
                                       true ) ;
      PortableServer::ObjectId * id = myStreamPort->getId() ;
      CORBA::Object_var obj = _poa->id_to_reference(*id);
      iobject = SUPERV::StreamPort::_narrow(obj) ;
      anInPort->ObjRef( SUPERV::StreamPort::_duplicate( iobject ) ) ;
    }
    else {
//      MESSAGE( "StreamLink_Impl::InStreamPort Gets ObjRef "
//               << iobject->Node()->Name() << "( " << iobject->Name() << " )" ) ;
    }
  }
//  endService( "StreamLink_Impl::InStreamPort" );
  return SUPERV::StreamPort::_duplicate( iobject ) ;
}

SUPERV::StreamPort_ptr StreamLink_Impl::OutStreamPort() {
//  beginService( "StreamLink_Impl::OutStreamPort" );
  SUPERV::StreamPort_var iobject ;
  GraphBase::OutPort * anOutPort = DataFlowOutNode()->GetChangeOutPort( OutputParameterName() ) ;
  if ( anOutPort && anOutPort->IsDataStream() ) {
    iobject = SUPERV::StreamPort::_narrow( anOutPort->ObjRef() ) ;
    if ( CORBA::is_nil( iobject ) ) {
//      MESSAGE( "StreamLink_Impl::OutStreamPort creates myStreamPort "
//               << DataFlowNode()->Name() << "( " << anOutPort->PortName() << " )" ) ;
      StreamPort_Impl * myStreamPort = new StreamPort_Impl(
                                       _Orb , _Poa , _ContId ,
                                       instanceName() , interfaceName() ,
                                       DataFlowEditor() ,
                                       DataFlowOutNode() ,
                                       (GraphBase::Port * ) anOutPort ,
                                       false ) ;
      PortableServer::ObjectId * id = myStreamPort->getId() ;
      CORBA::Object_var obj = _poa->id_to_reference(*id);
      iobject = SUPERV::StreamPort::_narrow(obj) ;
      anOutPort->ObjRef( SUPERV::StreamPort::_duplicate( iobject ) ) ;
    }
    else {
//      MESSAGE( "StreamLink_Impl::OutStreamPort Gets ObjRef "
//               << iobject->Node()->Name() << "( " << iobject->Name() << " )" ) ;
    }
  }
//  endService( "StreamLink_Impl::OutStreamPort" );
  return SUPERV::StreamPort::_duplicate( iobject ) ;
}


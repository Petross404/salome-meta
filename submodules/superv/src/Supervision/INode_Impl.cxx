//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : INode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "LNode_Impl.hxx"

INode_Impl::INode_Impl( CORBA::ORB_ptr orb ,
		       PortableServer::POA_ptr poa ,
	       	       PortableServer::ObjectId * contId , 
	               const char *instanceName ,
                       const char *interfaceName ,
                       const char *aDataFlowName ,
                       const SUPERV::KindOfNode aKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  CNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowName , aKindOfNode ) {
//  MESSAGE("INode_Impl::INode_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
//  _thisObj = this ;
//  _id = _poa->activate_object(_thisObj);
//  MESSAGE( "INode_Impl::INode_Impl " << aDataFlowName );
//  beginService( "INode_Impl::INode_Impl" );
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  endService( "INode_Impl::INode_Impl" );  
}

INode_Impl::INode_Impl() {
}

INode_Impl::INode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const char * FuncName , 
                        const SUPERV::ListOfStrings & aPythonFunction ,
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  CNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , SALOME_ModuleCatalog::Service() , FuncName , NodeKindOfNode , FuncName , aPythonFunction ) {
//  beginService( "INode_Impl::INode_Impl" );
  if ( FuncName ) {
//    cout << "INode_Impl::CNode_Impl " << (void *) FuncName << " " << FuncName
//         << " " << strlen( FuncName ) << endl ;
  }
  if ( NodeKindOfNode == SUPERV::InLineNode ) {
//    MESSAGE( "INode_Impl::INode_Impl " << FuncName << " _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
//  MESSAGE("INode_Impl::INode_Impl " << FuncName << " " << strlen( FuncName ) ) ;
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode()->SetPythonFunction( FuncName , aPythonFunction ) ;
//  endService( "INode_Impl::INode_Impl" );  
}

INode_Impl::INode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const GraphBase::ListOfFuncName FuncName , 
                        const GraphBase::ListOfPythonFunctions aPythonFunction ,
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  CNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , SALOME_ModuleCatalog::Service() , FuncName[0].c_str() , NodeKindOfNode , FuncName , aPythonFunction ) {
//  beginService( "INode_Impl::INode_Impl" );
  if ( NodeKindOfNode == SUPERV::InLineNode ) {
//    MESSAGE( "INode_Impl::INode_Impl " << FuncName[0] << " _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
//  MESSAGE("INode_Impl::INode_Impl " << FuncName[0].c_str() << " " << strlen( FuncName[0].c_str() ) ) ;
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode()->SetPythonFunction( FuncName , aPythonFunction ) ;
//  endService( "INode_Impl::INode_Impl" );  
}

INode_Impl::INode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  CNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
//  beginService( "INode_Impl::INode_Impl" );
  if ( DataFlowNode() && DataFlowNode()->IsInLineNode() ) {
//    MESSAGE( "INode_Impl::INode_Impl " << DataFlowNode()->PyFuncName()
//             << " _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  else {
//    MESSAGE( "INode_Impl::INode_Impl NO _poa->activate_object" );
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  endService( "INode_Impl::INode_Impl" );  
}

INode_Impl::~INode_Impl() {
  beginService( "INode_Impl::~INode_Impl" );
  endService( "INode_Impl::~INode_Impl" );
}

void INode_Impl::destroy() {
  beginService( "INode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("INode_Impl::destroy ERROR ") ;
    }
  }
  endService( "INode_Impl::Destroy" );
}

bool INode_Impl::Delete() {
//  beginService( "INode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
    if ( RetVal )
      DataFlowEditor()->UnValid() ;
  }
//  endService( "INode_Impl::Delete" );
  return RetVal ;
}

void INode_Impl::SetPyFunction( const char * FuncName ,
                                const SUPERV::ListOfStrings & aPyFunction ) {
  beginService( "INode_Impl::SetPyFunction" );
  if ( DataFlowEditor()->IsEditing() ) {
    DataFlowNode()->SetPythonFunction( FuncName , aPyFunction ) ;
  }
  endService( "INode_Impl::SetPyFunction" );
  return ;
}

SUPERV::ListOfStrings * INode_Impl::PyFunction() {
  beginService( "INode_Impl::PyFunction" );
  SUPERV::ListOfStrings * aPyFunc = NULL ;
  if ( DataFlowEditor()->IsEditing() ) {
    aPyFunc = new SUPERV::ListOfStrings( *DataFlowNode()->PythonFunction() ) ;
  }
  else {
    aPyFunc = new SUPERV::ListOfStrings() ;
  }
  endService( "INode_Impl::PyFunction" );
  return aPyFunc ;
}

char * INode_Impl::PyFuncName() {
  return CORBA::string_dup( DataFlowNode()->PyFuncName() ) ;
}

SUPERV::Port_ptr INode_Impl::InPort( const char *aParameterName ,
                                     const char *aParameterType ) {
  SUPERV::Port_ptr Inobject = SUPERV::Port::_nil() ;
  SUPERV::Port_ptr Outobject = SUPERV::Port::_nil() ;
  SUPERV::Port_ptr InEndobject = SUPERV::Port::_nil() ;
  Port_Impl * myInPort = NULL ;
  bool InPortCreated = false ;
//  MESSAGE( "CNode_Impl::InPort " << DataFlowNode()->Name() << "->AddInPort( " << aParameterName << " , "
//           << aParameterType << " )" ) ;
  GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( aParameterName ) ;
  if ( anInPort == NULL ) {
    anInPort = DataFlowNode()->AddInPort( aParameterName , aParameterType , SUPERV::InLineParameter ) ;
    InPortCreated = true ;
  }
  if ( anInPort ) {
    Inobject = anInPort->ObjRef() ;
    if ( CORBA::is_nil( Inobject ) ) {
      myInPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
                                (GraphBase::Port * ) anInPort ,
                                true ) ;
      if ( myInPort ) {
        PortableServer::ObjectId * id = myInPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id) ;
        Inobject = SUPERV::Port::_narrow(obj) ;
//JR 02.09.2005 : set of objref is better
        anInPort->ObjRef( Inobject ) ;
      }      
    }
  }

  if ( !CORBA::is_nil( Inobject ) && InPortCreated &&
       ( DataFlowNode()->IsLoopNode() || DataFlowNode()->IsEndLoopNode() ) ) {
//         || DataFlowNode()->IsEndSwitchNode() ) ) {
    Outobject = OutPort( aParameterName , aParameterType ) ;
// ==> The OutPort (and ObjRef) with the same name is created in the LoopNode
    if ( DataFlowNode()->IsLoopNode() ) {
      LNode_Impl * myLoopNode = (LNode_Impl * ) this ;
      InEndobject = myLoopNode->Coupled()->InPort( aParameterName , aParameterType ) ;
// ==> The In/OutPort (and ObjRef) with the same name are created in the EndLoopNode
      DataFlowEditor()->AddLink( Outobject->Node()->Name() , Outobject->Name() ,
                                 InEndobject->Node()->Name() , InEndobject->Name() ) ;
    }
  }
  DataFlowEditor()->UnValid() ;
  return SUPERV::Port::_duplicate( Inobject ) ;
}

SUPERV::Port_ptr INode_Impl::OutPort( const char *aParameterName ,
                                      const char *aParameterType ) {
  Port_Impl * myOutPort = NULL ;
  SUPERV::Port_ptr Outobject = SUPERV::Port::_nil() ;
//  MESSAGE( "CNode_Impl::OutPort " << DataFlowNode()->Name() << "->AddOutPort( " << aParameterName << " , "
//           << aParameterType << " )" ) ;
  GraphBase::OutPort * anOutPort = DataFlowNode()->GetChangeOutPort( aParameterName ) ;
  if ( anOutPort == NULL ) {
    anOutPort = DataFlowNode()->AddOutPort( aParameterName , aParameterType , SUPERV::InLineParameter ) ;
  }
  if ( anOutPort ) {
    Outobject = anOutPort->ObjRef() ;
    if ( CORBA::is_nil( Outobject ) ) {
//JR 30.03.2005      const CORBA::Any * anAny = anOutPort->Value() ;
      const CORBA::Any anAny = anOutPort->Value() ;
      myOutPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 DataFlowEditor() ,
                                 DataFlowNode() ,
                                 (GraphBase::Port * ) anOutPort ,
                                 false ,
                                 &anAny ) ;
      if ( myOutPort ) {
        PortableServer::ObjectId * id = myOutPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id) ;
        Outobject = SUPERV::Port::_narrow(obj) ;
//JR 02.09.2005 : set of objref is better
        anOutPort->ObjRef( Outobject ) ;
      }
    }
  }
  DataFlowEditor()->UnValid() ;
  return SUPERV::Port::_duplicate( Outobject );
}

SUPERV::StreamPort_ptr INode_Impl::InStreamPort( const char * aParameterName ,
                                                 const char * aParameterType ,
                                                 const SALOME_ModuleCatalog::DataStreamDependency aDependency ) {
  SUPERV::StreamPort_ptr Inobject = SUPERV::StreamPort::_nil() ;
  if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    StreamPort_Impl * myInStreamPort = NULL ;
    bool InPortCreated = false ;
    MESSAGE( "INode_Impl::InStreamPort " << DataFlowNode()->Name() << "->AddInPort( " << aParameterName << " , "
             << aParameterType << " )" ) ;
    GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( aParameterName ) ;
    if ( anInPort && !anInPort->IsDataStream() ) {
      MESSAGE( "INode_Impl::InStreamPort ERROR !IsDataStream " ) ;
    }
    else if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
      if ( anInPort == NULL ) {
        anInPort = DataFlowNode()->ComputingNode()->AddInDataStreamPort( aParameterName , aParameterType , aDependency , SUPERV::DataStreamParameter ) ;
        InPortCreated = true ;
      }

      if ( anInPort ) {
        Inobject = SUPERV::StreamPort::_narrow( anInPort->ObjRef() ) ;
        if ( CORBA::is_nil( Inobject ) ) {
          myInStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
                                (GraphBase::Port * ) anInPort ,
                                true ) ;
          if ( myInStreamPort ) {
            PortableServer::ObjectId * id = myInStreamPort->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id) ;
            Inobject = SUPERV::StreamPort::_narrow(obj) ;
            anInPort->ObjRef( SUPERV::StreamPort::_duplicate( Inobject ) ) ;
  	  }
	}
      }
    }

//    if ( !CORBA::is_nil( Inobject ) && InPortCreated &&
//         ( DataFlowNode()->IsLoopNode() || DataFlowNode()->IsEndLoopNode() ||
//           DataFlowNode()->IsEndSwitchNode() ) ) {
//      Outobject = OutPort( aParameterName , aParameterType ) ;
//      if ( DataFlowNode()->IsLoopNode() ) {
//        LNode_Impl * myLoopNode = (LNode_Impl * ) this ;
//        InEndobject = myLoopNode->Coupled()->InPort( aParameterName , aParameterType ) ;
//        DataFlowEditor()->AddLink( Outobject->Node()->Name() , Outobject->Name() ,
//                                   InEndobject->Node()->Name() , InEndobject->Name() ) ;
//      }
//    }
    DataFlowEditor()->UnValid() ;
  }
  return SUPERV::StreamPort::_duplicate( Inobject ) ;
}

SUPERV::StreamPort_ptr INode_Impl::OutStreamPort( const char * aParameterName ,
                                                  const char * aParameterType ,
                                                  const SALOME_ModuleCatalog::DataStreamDependency aDependency ) {
  StreamPort_Impl * myOutStreamPort = NULL ;
  SUPERV::StreamPort_ptr Outobject = SUPERV::StreamPort::_nil() ;
  if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    MESSAGE( "INode_Impl::OutStreamPort " << DataFlowNode()->Name() << "->AddOutPort( " << aParameterName << " , "
             << aParameterType << " )" ) ;
    GraphBase::OutPort * anOutPort = DataFlowNode()->GetChangeOutPort( aParameterName ) ;
    if ( anOutPort && !anOutPort->IsDataStream() ) {
      MESSAGE( "INode_Impl::OutStreamPort ERROR !IsDataStream " ) ;
    }
    else if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
      if ( anOutPort == NULL ) {
        anOutPort = DataFlowNode()->ComputingNode()->AddOutDataStreamPort( aParameterName , aParameterType , aDependency , SUPERV::DataStreamParameter ) ;
      }

      if ( anOutPort ) {
        Outobject = SUPERV::StreamPort::_narrow( anOutPort->ObjRef() ) ;
        if ( CORBA::is_nil( Outobject ) ) {
//JR 30.03.2005          const CORBA::Any * anAny = anOutPort->Value() ;
          const CORBA::Any anAny = anOutPort->Value() ;
          myOutStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 DataFlowEditor() ,
                                 DataFlowNode() ,
                                 (GraphBase::Port * ) anOutPort ,
                                 false ,
                                 &anAny ) ;
          if ( myOutStreamPort ) {
            PortableServer::ObjectId * id = myOutStreamPort->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id) ;
            Outobject = SUPERV::StreamPort::_narrow(obj) ;
            anOutPort->ObjRef( SUPERV::StreamPort::_duplicate( Outobject ) ) ;
  	  }
	}
      }
    }
    DataFlowEditor()->UnValid() ;
  }
  return SUPERV::StreamPort::_duplicate( Outobject );
}



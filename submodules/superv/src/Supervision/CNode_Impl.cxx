//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : CNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "StreamGraph_Impl.hxx"

#include "CNode_Impl.hxx"

#include "StreamPort_Impl.hxx"

CNode_Impl::CNode_Impl( CORBA::ORB_ptr orb ,
		        PortableServer::POA_ptr poa ,
	       	        PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        const char *aDataFlowName ,
                        const SUPERV::KindOfNode aKindOfNode ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
//  MESSAGE("CNode_Impl::CNode_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
//  _thisObj = this ;
//  _id = _poa->activate_object(_thisObj);
//  MESSAGE( "CNode_Impl::CNode_Impl " << aDataFlowName << " " );
//  beginService( "CNode_Impl::CNode_Impl" );
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = NULL ;
  _DataFlowNode = NULL ;
  if ( aKindOfNode == SUPERV::MacroNode ) {
    _IsNode = true ;
  }
  else {
    _IsNode = false ;
  }
//  endService( "CNode_Impl::CNode_Impl" );  
}

CNode_Impl::CNode_Impl() {
}

CNode_Impl::CNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const SALOME_ModuleCatalog::Service &NodeService ,
                        const char * NodeName ,
                        const SUPERV::KindOfNode NodeKindOfNode ,
                        const char * FuncName  ,
                        const SUPERV::ListOfStrings & PythonFunction ,
			bool isCimpl ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
//  beginService( "CNode_Impl::CNode_Impl" );
//  cout << "CNode_Impl::CNode_Impl -->" << endl ;
//  if ( FuncName && NodeName ) {
//    cout << "CNode_Impl::CNode_Impl " << (void *) NodeName << " " << NodeName
//         << " " << strlen(NodeName) << " " << (void *) FuncName << " " << FuncName
//         << " " << strlen( FuncName ) << endl ;
//  }
  if ( NodeKindOfNode == SUPERV::ComputingNode ) {
//    MESSAGE( "CNode_Impl::CNode_Impl " << FuncName << " _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = aDataFlowEditor ;
  DataFlowEditor()->Graph()->GraphEditor( aDataFlowEditor ) ;
  GraphBase::ListOfFuncName aFuncName ;
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  if ( FuncName ) {
    aFuncName.resize(1) ;
    aFuncName[0] = my_strdup( FuncName ) ;
    aPythonFunction.resize(1) ;
    aPythonFunction[0] = &PythonFunction ;
  }

  // mkr : PAL11273 -->
  if ( isCimpl ) // C++ implementation
    _DataFlowNode = DataFlowEditor()->AddNode( NodeService , "" , "" , NodeName ,
					       NodeKindOfNode ,
					       aFuncName ,
					       aPythonFunction ) ;
  else // Python implementation
    _DataFlowNode = DataFlowEditor()->AddNode( NodeService , "" , "" , NodeName ,
					       NodeKindOfNode ,
					       aFuncName ,
					       aPythonFunction ,
					       SUPERV::SDate() ,
					       SUPERV::SDate() ,
					       NULLSTRING ,
					       NULLSTRING ,
					       FACTORYSERVERPY ) ;
  // mkr : PAL11273 <--

  _IsNode = true ;
//  endService( "CNode_Impl::CNode_Impl" );  
//  cout << "<-- CNode_Impl::CNode_Impl" << endl ;
}

CNode_Impl::CNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const SALOME_ModuleCatalog::Service &NodeService ,
                        const char * NodeName ,
                        const SUPERV::KindOfNode NodeKindOfNode ,
                        const GraphBase::ListOfFuncName & aFuncName  ,
                        const GraphBase::ListOfPythonFunctions & aPythonFunction ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
//  MESSAGE( NodeName << " " );
//  beginService( "CNode_Impl::CNode_Impl" );
  if ( NodeName ) {
//    MESSAGE("CNode_Impl::CNode_Impl " << NodeName << " " << strlen( NodeName ) ) ;
  }
  if ( NodeKindOfNode == SUPERV::ComputingNode ) {
//    MESSAGE( "CNode_Impl::CNode_Impl " << aFuncName[0] << " _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = aDataFlowEditor ;
  DataFlowEditor()->Graph()->GraphEditor( aDataFlowEditor ) ;
  _DataFlowNode = DataFlowEditor()->AddNode( NodeService , "" , "" , NodeName ,
                                            NodeKindOfNode ,
                                            aFuncName ,
                                            aPythonFunction ) ;
  _IsNode = true ;
//  endService( "CNode_Impl::CNode_Impl" );  
}

CNode_Impl::CNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        GraphEditor::InNode * aDataFlowNode ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
//  beginService( "CNode_Impl::CNode_Impl" );
  if ( aDataFlowNode && aDataFlowNode->IsComputingNode() ) {
//    MESSAGE( "CNode_Impl::CNode_Impl _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  else {
//    MESSAGE( "CNode_Impl::CNode_Impl NO _poa->activate_object " );
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = aDataFlowEditor ;
  DataFlowEditor()->Graph()->GraphEditor( aDataFlowEditor ) ;
  _DataFlowNode = aDataFlowNode ;
  if ( aDataFlowNode ) {
    _IsNode = true ;
  }
  else {
    _IsNode = false ;
  }
//  endService( "CNode_Impl::CNode_Impl" );  
}

CNode_Impl::~CNode_Impl() {
  beginService( "CNode_Impl::~CNode_Impl" );
  endService( "CNode_Impl::~CNode_Impl" );
}

void CNode_Impl::destroy() {
  beginService( "CNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
    if ( Delete() ) {
      _DataFlowNode = NULL ;
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("CNode_Impl::destroy ERROR ") ;
    }
  }
//  endService( "CNode_Impl::Destroy" );
}

bool CNode_Impl::Delete() {
  beginService( "CNode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
    if ( RetVal ) {
      DataFlowEditor()->UnValid() ;
    }
  }
  endService( "CNode_Impl::Delete" );
  return RetVal ;
}

void CNode_Impl::DeletePorts() {
  beginService( "CNode_Impl::DeletePorts" );
  if ( DataFlowEditor()->IsEditing() && DataFlowNode() ) {
    int i ;
//JR Debug 06.09.2005 : Ports must be explored in the reversed side because deletion
//                      of a port changes indexes !!!...
//    for ( i = 0 ; i < DataFlowNode()->ComputingNode()->GetNodeInPortsSize() ; i++ ) {
    for ( i = DataFlowNode()->ComputingNode()->GetNodeInPortsSize()-1 ; i >= 0 ; i-- ) {
      SUPERV::Port_var aPort = DataFlowNode()->ComputingNode()->GetChangeNodeInPort( i )->ObjRef() ;
      if ( !CORBA::is_nil( aPort ) ) {
        aPort->Remove() ;
      }
    }
//    for ( i = 0 ; i < DataFlowNode()->ComputingNode()->GetNodeOutPortsSize() ; i++ ) {
    for ( i = DataFlowNode()->ComputingNode()->GetNodeOutPortsSize()-1 ; i >= 0 ; i-- ) {
      SUPERV::Port_var aPort = DataFlowNode()->ComputingNode()->GetChangeNodeOutPort( i )->ObjRef() ;
      if ( !CORBA::is_nil( aPort ) ) {
        aPort->Remove() ;
      }
    }
  }
  endService( "CNode_Impl::DeletePorts" );
}

SALOME_ModuleCatalog::Service * CNode_Impl::Service() {
//  beginService( "CNode_Impl::Service" );
  SALOME_ModuleCatalog::Service * RetVal ;
  if ( _IsNode ) {
    RetVal = new SALOME_ModuleCatalog::Service( *DataFlowNode()->GetService() ) ;
  }
  else {
    RetVal = new SALOME_ModuleCatalog::Service( *DataFlowEditor()->GetService() ) ;
  }
//  endService( "CNode_Impl::Service" );
  return RetVal ;
}

char * CNode_Impl::Name() {
//  beginService( "CNode_Impl::Name" );
  char * RetVal = NULL ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->Name() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->Name() ;
  }
//  endService( "CNode_Impl::Name" );
  return CORBA::string_dup( RetVal );
}
bool CNode_Impl::SetName( const char * aDataFlowName ) {
//  beginService( "CNode_Impl::SetName" );
  bool RetVal = false ;
  bool SameName ;
  if ( DataFlowEditor()->IsEditing() ) {
    if ( _IsNode ) {
//JR 29.06.2005 debug :
      char * OldNodeName = my_strdup( DataFlowNode()->Name() ) ;
//      RetVal = DataFlowEditor()->ReNameNode( DataFlowNode()->Name() ,
      SameName = !strcmp( OldNodeName , aDataFlowName ) ;
      if ( !SameName ) {
        RetVal = DataFlowEditor()->ReNameNode( OldNodeName ,
                                               aDataFlowName ) ;
      }
      delete [] OldNodeName ;
    }
    else {
//      RetVal = DataFlowEditor()->Graph()->Name( aDataFlowName ) ;
      SameName = !strcmp( DataFlowEditor()->Graph()->Name() , aDataFlowName ) ;
      if ( !SameName ) {
        RetVal = DataFlowEditor()->Name( aDataFlowName ) ;
      }
    }
//JR Debug 06.09.2005 : UnValid was missing
    if ( !SameName ) {
      DataFlowEditor()->UnValid() ;
      DataFlowEditor()->IsValid() ;
    }
  }
//  endService( "CNode_Impl::SetName" );
  return RetVal ;
}
SUPERV::KindOfNode CNode_Impl::Kind() {
//  beginService( "CNode_Impl::Kind" );
  SUPERV::KindOfNode RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->Kind() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->Kind() ;
  }
//  endService( "CNode_Impl::Kind" );
  return RetVal ;
}
bool CNode_Impl::IsGraph() {
//  beginService( "CNode_Impl::IsGraph" );
  bool RetVal = false ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsDataFlowNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsDataFlowNode() ;
  }
//  endService( "CNode_Impl::IsGraph" );
  return RetVal ;
}
bool CNode_Impl::IsStreamGraph() {
  beginService( "CNode_Impl::IsStreamGraph" );
  bool RetVal = false ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsDataStreamNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsDataStreamNode() ;
  }
  MESSAGE( "CNode_Impl::IsStreamGraph " << RetVal );
  endService( "CNode_Impl::IsStreamGraph" );
  return RetVal ;
}
bool CNode_Impl::IsMacro() {
//  beginService( "CNode_Impl::IsMacro" );
  bool RetVal = false ;
  if ( _IsNode && DataFlowNode()->IsMacroNode() ) {
    RetVal = true ;
  }
//  endService( "CNode_Impl::IsMacro" );
  return RetVal ;
}
bool CNode_Impl::IsFlowMacro() {
//  beginService( "CNode_Impl::IsFlowMacro" );
  bool RetVal = false ;
  if ( _IsNode && DataFlowNode()->IsMacroNode() && DataFlowNode()->ComputingNode()->MacroObject()->IsGraph() ) {
    RetVal = true ;
  }
//  endService( "CNode_Impl::IsFlowMacro" );
  return RetVal ;
}
bool CNode_Impl::IsStreamMacro() {
//  beginService( "CNode_Impl::IsStreamMacro" );
  bool RetVal = false ;
  if ( _IsNode && DataFlowNode()->IsMacroNode() && DataFlowNode()->ComputingNode()->MacroObject()->IsStreamGraph() ) {
    RetVal = true ;
  }
//  endService( "CNode_Impl::IsStreamMacro" );
  return RetVal ;
}
bool CNode_Impl::IsHeadGraph() {
  bool RetVal = false ;
  beginService( "CNode_Impl::IsHeadGraph" );
  endService( "CNode_Impl::IsHeadGraph" );
  return RetVal ;
}
CORBA::Long CNode_Impl::GraphLevel() {
  CORBA::Long RetVal = 0 ;
  beginService( "CNode_Impl::GraphLevel" );
  RetVal = DataFlowEditor()->Graph()->GraphMacroLevel() ;
  endService( "CNode_Impl::GraphLevel" );
  return RetVal ;
}

bool CNode_Impl::IsComputing() {
//  beginService( "CNode_Impl::IsComputing" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsComputingNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsComputingNode() ;
  }
//  endService( "CNode_Impl::IsComputing" );
  return RetVal ;
}
bool CNode_Impl::IsFactory() {
//  beginService( "CNode_Impl::IsFactory" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsFactoryNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsFactoryNode() ;
  }
//  endService( "CNode_Impl::IsFactory" );
  return RetVal ;
}
bool CNode_Impl::IsInLine() {
//  beginService( "CNode_Impl::IsInLine" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsInLineNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsInLineNode() ;
  }
//  endService( "CNode_Impl::IsInLine" );
  return RetVal ;
}
bool CNode_Impl::IsGOTO() {
//  beginService( "CNode_Impl::IsGOTO" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsGOTONode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsGOTONode() ;
  }
//  endService( "CNode_Impl::IsGOTO" );
  return RetVal ;
}
bool CNode_Impl::IsLoop() {
//  beginService( "CNode_Impl::IsLoop" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsLoopNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsLoopNode() ;
  }
//  endService( "CNode_Impl::IsLoop" );
  return RetVal ;
}
bool CNode_Impl::IsEndLoop() {
//  beginService( "CNode_Impl::IsEndLoop" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsEndLoopNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsEndLoopNode() ;
  }
//  endService( "CNode_Impl::IsEndLoop" );
  return RetVal ;
}
bool CNode_Impl::IsSwitch() {
//  beginService( "CNode_Impl::IsSwitch" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsSwitchNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsSwitchNode() ;
  }
//  endService( "CNode_Impl::IsSwitch" );
  return RetVal ;
}
bool CNode_Impl::IsEndSwitch() {
//  beginService( "CNode_Impl::IsEndSwitch" );
  bool RetVal = SUPERV::UnknownNode ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->IsEndSwitchNode() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->IsEndSwitchNode() ;
  }
//  endService( "CNode_Impl::IsEndSwitch" );
  return RetVal ;
}

SUPERV::SDate CNode_Impl::CreationDate() {
//  beginService( "CNode_Impl::CreationDate" );
  SUPERV::SDate_var RetVal ;
  if ( _IsNode ) {
    RetVal = new SUPERV::SDate( DataFlowNode()->FirstCreation() ) ;
  }
  else {
//    cout << " CNode_Impl::CreationDate " << DataFlowEditor()->FirstCreation()
//         << endl ;
    RetVal = new SUPERV::SDate( DataFlowEditor()->Graph()->FirstCreation() ) ;
  }
//  endService( "CNode_Impl::CreationDate" );
  return (RetVal._retn()) ;
}
SUPERV::SDate CNode_Impl::LastUpdateDate() {
//  beginService( "CNode_Impl::LastUpdateDate" );
  SUPERV::SDate_var RetVal ;
  if ( _IsNode ) {
    RetVal = new SUPERV::SDate( DataFlowNode()->LastModification() ) ;
  }
  else {
    RetVal = new SUPERV::SDate( DataFlowEditor()->Graph()->LastModification() ) ;
  }
//  endService( "CNode_Impl::LastUpdateDate" );
  return  (RetVal._retn()) ;
}
char * CNode_Impl::Version() {
//  beginService( "CNode_Impl::Version" );
  char * RetVal ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->EditorRelease() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->EditorRelease() ;
  }
//  endService( "CNode_Impl::Version" );
  return CORBA::string_dup( RetVal ) ;
}
char * CNode_Impl::Author() {
//  beginService( "CNode_Impl::Author" );
  char * RetVal ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->Author() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->Author() ;
  }
//  endService( "CNode_Impl::Author" );
  return CORBA::string_dup( RetVal ) ;
}
bool CNode_Impl::SetAuthor( const char * aDataFlowAuthor ) {
//  beginService( "CNode_Impl::SetAuthor" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    if ( _IsNode ) {
      RetVal = DataFlowNode()->Author( aDataFlowAuthor ) ;
    }
    else {
      RetVal = DataFlowEditor()->Graph()->Author( aDataFlowAuthor ) ;
    }
  }
//  endService( "CNode_Impl::SetAuthor" );
  return RetVal ;
}
char * CNode_Impl::Comment() {
//  beginService( "CNode_Impl::Comment" );
  char * RetVal ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->Comment() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->Comment() ;
  }
//  endService( "CNode_Impl::Comment" );
  return CORBA::string_dup( RetVal ) ;
}
bool CNode_Impl::SetComment( const char * aDataFlowComment ) {
//  beginService( "CNode_Impl::SetComment" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    if ( _IsNode ) {
      RetVal = DataFlowNode()->Comment( aDataFlowComment ) ;
    }
    else {
      RetVal = DataFlowEditor()->Graph()->Comment( aDataFlowComment ) ;
    }
  }
//  endService( "CNode_Impl::SetComment" );
  return RetVal ;
}

void CNode_Impl::Coords(CORBA::Long X , CORBA::Long Y ) {
//  beginService( "CNode_Impl::Coords" );
  if ( DataFlowEditor()->IsEditing() ) {
    if ( _IsNode ) {
      ((GraphEditor::InNode *) DataFlowNode())->Coordinates( X , Y ) ;
    }
    else {
      DataFlowEditor()->Graph()->Coordinates( X , Y ) ;
    }
  }
//  endService( "CNode_Impl::Coords" );
}
CORBA::Long CNode_Impl::X() {
//  beginService( "CNode_Impl::X" );
  CORBA::Long RetVal ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->XCoordinate() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->XCoordinate() ;
  }
//  endService( "CNode_Impl::X" );
  return RetVal ;
}
CORBA::Long CNode_Impl::Y() {
//  beginService( "CNode_Impl::Y" );
  CORBA::Long RetVal ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->YCoordinate() ;
  }
  else {
    RetVal = DataFlowEditor()->Graph()->YCoordinate() ;
  }
//  endService( "CNode_Impl::Y" );
  return RetVal ;
}

SUPERV::Port_ptr CNode_Impl::Port( const char * ServiceParameterName ) {
  bool begin = true ;
  Port_Impl * myPort = NULL ;
  SUPERV::Port_var iobject = SUPERV::Port::_nil() ;
  bool aninport ;
  if ( _IsNode ) {
    aninport = DataFlowNode()->GetInPort( ServiceParameterName ) ;
  }
  else {
    aninport = DataFlowEditor()->GetInPort( ServiceParameterName ) ;
  }  
  bool anoutport ;
  if ( _IsNode ) {
    anoutport = DataFlowNode()->GetOutPort( ServiceParameterName ) ;
  }
  else {
    anoutport = DataFlowEditor()->GetOutPort( ServiceParameterName ) ;
  }
  if ( aninport ) {
    GraphBase::InPort * anInPort ;
    if ( _IsNode ) {
      anInPort = DataFlowNode()->GetChangeInPort( ServiceParameterName ) ;
    }
    else {
      anInPort = DataFlowEditor()->GetChangeInPort( ServiceParameterName ) ;
    }
    if ( anInPort->IsDataStream() ) {
      MESSAGE( "CNode_Impl::Port ERROR IsDataStream " ) ;
    }
    else if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
      if ( begin ) {
        beginService( "CNode_Impl::Port" );
        begin = false ;
      }
      bool hasinput ;
      if ( _IsNode ) {
        hasinput = DataFlowNode()->HasInput( anInPort->PortName() ) ;
      }
      else {
        hasinput = DataFlowEditor()->HasInput( anInPort->PortName() ) ;
      }
      if ( hasinput ) {
//JR 30.03.2005        const CORBA::Any * anAny = anInPort->GetOutPort()->Value() ;
        const CORBA::Any anAny = anInPort->GetOutPort()->Value() ;
        myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
//                                ServiceParameterName ,
                                (GraphBase::Port * ) anInPort ,
                                true ,
                                &anAny ) ;
      }
      else {
        myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
//                                ServiceParameterName ) ;
                                (GraphBase::Port * ) anInPort ,
                                true ) ;
      }
      PortableServer::ObjectId * id = myPort->getId() ;
      CORBA::Object_var obj = _poa->id_to_reference(*id);
      iobject = SUPERV::Port::_narrow(obj) ;
      anInPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
    }
    else {
      iobject = anInPort->ObjRef() ;
    }
  }
  else if ( anoutport ) {
    GraphBase::OutPort * anOutPort ;
    if ( _IsNode ) {
      anOutPort = DataFlowNode()->GetChangeOutPort( ServiceParameterName ) ;
    }
    else {
      anOutPort = DataFlowEditor()->GetChangeOutPort( ServiceParameterName ) ;
    }
    if ( anOutPort->IsDataStream() ) {
      MESSAGE( "CNode_Impl::Port ERROR IsDataStream " ) ;
    }
    else if ( CORBA::is_nil( anOutPort->ObjRef() ) ) {
      if ( begin ) {
        beginService( "CNode_Impl::Port" );
        begin = false ;
      }
//JR 30.03.2005      const CORBA::Any * anAny = anOutPort->Value() ;
      const CORBA::Any anAny = anOutPort->Value() ;
      myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                              instanceName() , interfaceName() ,
                              DataFlowEditor() ,
                              DataFlowNode() ,
//                              ServiceParameterName ,
                              (GraphBase::Port * ) anOutPort ,
                              false ,
                              &anAny ) ;
      PortableServer::ObjectId * id = myPort->getId() ;
      CORBA::Object_var obj = _poa->id_to_reference(*id);
      iobject = SUPERV::Port::_narrow(obj) ;
      anOutPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
    }
    else {
      iobject = anOutPort->ObjRef() ;
    }
  }
  if ( !begin ) {
    endService( "CNode_Impl::Port" );
  }
  DataFlowEditor()->UnValid() ;
  return SUPERV::Port::_duplicate( iobject ) ;
}

SUPERV::Port_ptr CNode_Impl::Input( const char * ToServiceParameterName ,
                                    const SUPERV::Value_ptr aValue ) {
  bool begin = true ;
  SUPERV::Port_var iobject = SUPERV::Port::_nil() ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  bool sts = false ;
  GraphBase::InPort * anInPort = NULL ;
if ( DataFlowEditor()->IsEditing() /*&& DataFlowEditor()->Graph()->GraphMacroLevel() == 0*/ ) {
    if ( _IsNode ) {
      sts = DataFlowEditor()->AddInputData( DataFlowNode()->Name() ,
                                           ToServiceParameterName ,
                                           *aValue->ToAny() ) ;
      anInPort = DataFlowNode()->GetChangeInPort( ToServiceParameterName ) ;
    }
    else {
      sts = DataFlowEditor()->AddInputData( DataFlowEditor()->Graph()->Name() ,
                                           ToServiceParameterName ,
                                           *aValue->ToAny() ) ;
      anInPort = DataFlowEditor()->Graph()->GetChangeInPort( ToServiceParameterName ) ;
    }
    if ( sts && anInPort ) {
      if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
        if ( begin ) {
          beginService( "CNode_Impl::Input" );
          begin = false ;
        }
        Port_Impl * myPort ;
        myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
//                                ToServiceParameterName ,
                                (GraphBase::Port * ) anInPort ,
                                true ,
                                aValue->ToAny() ) ;
        PortableServer::ObjectId * id = myPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        iobject = SUPERV::Port::_narrow(obj) ;
        anInPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
      }
      else {
        iobject = anInPort->ObjRef() ;
      }
    }
    DataFlowEditor()->UnValid() ;
  }
  else if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      sts = aDataFlowExecutor->ChangeInputData( DataFlowNode()->Name() ,
                                                ToServiceParameterName ,
                                                *aValue->ToAny() ) ;
      anInPort = DataFlowNode()->GetChangeInPort( ToServiceParameterName ) ;
    }
    else {
      sts = aDataFlowExecutor->ChangeInputData( DataFlowEditor()->Graph()->Name() ,
                                                ToServiceParameterName ,
                                                *aValue->ToAny() ) ;
      anInPort = DataFlowEditor()->Graph()->GetChangeInPort( ToServiceParameterName ) ;
    }
    if ( sts && anInPort ) {
      if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
        if ( begin ) {
          beginService( "CNode_Impl::Input" );
          begin = false ;
        }
        Port_Impl * myPort ;
        myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
//                                ToServiceParameterName ,
                                (GraphBase::Port * ) anInPort ,
                                true ,
                                aValue->ToAny() ) ;
        PortableServer::ObjectId * id = myPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        iobject = SUPERV::Port::_narrow(obj) ;
        anInPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
      }
      else {
        iobject = anInPort->ObjRef() ;
      }
    }
  }
  if ( !begin ) {
    endService( "CNode_Impl::Input" );
  }
  if ( CORBA::is_nil( iobject ) ) {
    MESSAGE( "CNode_Impl::Input returns nil object _IsNode " << _IsNode << " sts " << sts
             << " Node " << Name() << " ToServiceParameterName " << ToServiceParameterName
             << " anInPort " << anInPort ) ;
  }
  return SUPERV::Port::_duplicate( iobject ) ;
}


SUPERV::Port_ptr CNode_Impl::GetInPort( const char *aParameterName ) {
  SUPERV::Port_ptr Inobject = SUPERV::Port::_nil() ;
  Port_Impl * myInPort = NULL ;
  GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( aParameterName ) ;
  if ( anInPort && !anInPort->IsDataStream() ) {
    Inobject = anInPort->ObjRef() ;
    if ( CORBA::is_nil( Inobject ) ) {
      myInPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
                                (GraphBase::Port * ) anInPort ,
                                true ) ;
      if ( myInPort ) {
        PortableServer::ObjectId * id = myInPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id) ;
        Inobject = SUPERV::Port::_narrow(obj) ;
        anInPort->ObjRef( SUPERV::Port::_duplicate( Inobject ) ) ;
      }
    }
  }
  return SUPERV::Port::_duplicate( Inobject ) ;
}

SUPERV::Port_ptr CNode_Impl::GetOutPort( const char *aParameterName ) {
  Port_Impl * myOutPort = NULL ;
  SUPERV::Port_ptr Outobject = SUPERV::Port::_nil() ;
  GraphBase::OutPort * anOutPort = DataFlowNode()->GetChangeOutPort( aParameterName ) ;
  if ( anOutPort && !anOutPort->IsDataStream() ) {
    Outobject = anOutPort->ObjRef() ;
    if ( CORBA::is_nil( Outobject ) ) {
//JR 30.03.2005      const CORBA::Any * anAny = anOutPort->Value() ;
      const CORBA::Any anAny = anOutPort->Value() ;
      myOutPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 DataFlowEditor() ,
                                 DataFlowNode() ,
                                 (GraphBase::Port * ) anOutPort ,
                                 false ,
                                 &anAny ) ;
      if ( myOutPort ) {
        PortableServer::ObjectId * id = myOutPort->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id) ;
        Outobject = SUPERV::Port::_narrow(obj) ;
        anOutPort->ObjRef( SUPERV::Port::_duplicate( Outobject ) ) ;
      }
    }
  }
  return SUPERV::Port::_duplicate( Outobject );
}

SUPERV::StreamPort_ptr CNode_Impl::GetInStreamPort( const char *aParameterName ) {
  SUPERV::StreamPort_ptr Inobject = SUPERV::StreamPort::_nil() ;
  if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    StreamPort_Impl * myInStreamPort = NULL ;
    GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( aParameterName ) ;
    if ( anInPort && anInPort->IsDataStream() ) {
      Inobject = SUPERV::StreamPort::_narrow( anInPort->ObjRef() ) ;
      if ( CORBA::is_nil( Inobject ) ) {
        myInStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                instanceName() , interfaceName() ,
                                DataFlowEditor() ,
                                DataFlowNode() ,
                                (GraphBase::Port * ) anInPort ,
                                true ) ;
        if ( myInStreamPort ) {
          PortableServer::ObjectId * id = myInStreamPort->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id) ;
          Inobject = SUPERV::StreamPort::_narrow(obj) ;
          anInPort->ObjRef( SUPERV::StreamPort::_duplicate( Inobject ) ) ;
	}
      }      
    }
  }
  return SUPERV::StreamPort::_duplicate( Inobject ) ;
}

SUPERV::StreamPort_ptr CNode_Impl::GetOutStreamPort( const char *aParameterName ) {
  StreamPort_Impl * myOutStreamPort = NULL ;
  SUPERV::StreamPort_ptr Outobject = SUPERV::StreamPort::_nil() ;
  if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    GraphBase::OutPort * anOutPort = DataFlowNode()->GetChangeOutPort( aParameterName ) ;
    if ( anOutPort && anOutPort->IsDataStream() ) {
      Outobject = SUPERV::StreamPort::_narrow( anOutPort->ObjRef() ) ;
      if ( CORBA::is_nil( Outobject ) ) {
//JR 30.03.2005        const CORBA::Any * anAny = anOutPort->Value() ;
        const CORBA::Any anAny = anOutPort->Value() ;
        myOutStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 DataFlowEditor() ,
                                 DataFlowNode() ,
                                 (GraphBase::Port * ) anOutPort ,
                                 false ,
                                 &anAny ) ;
        if ( myOutStreamPort ) {
          PortableServer::ObjectId * id = myOutStreamPort->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id) ;
          Outobject = SUPERV::StreamPort::_narrow(obj) ;
          anOutPort->ObjRef( SUPERV::StreamPort::_duplicate( Outobject ) ) ;
        }
      }
    }
  }
  return SUPERV::StreamPort::_duplicate( Outobject );
}

bool CNode_Impl::HasStreamPort() {
  bool RetVal = false ;
  beginService( "CNode_Impl::HasStreamPort" );
  endService( "CNode_Impl::HasStreamPort" );
  return RetVal ;
}

SUPERV::Link_ptr CNode_Impl::GetLink(const char * ToServiceParameterName ) {
  bool begin = true ;
  SUPERV::Link_var iobject = SUPERV::Link::_nil() ;
  char* FromNodeName ;
  char* FromServiceParameterName ;
  bool status = DataFlowEditor()->GetLink( DataFlowNode()->Name() ,
                                          ToServiceParameterName ,
                                          & FromNodeName ,
                                          & FromServiceParameterName ) ;
  if ( status ) {
    GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( ToServiceParameterName ) ;
    if ( !anInPort->IsDataStream() && anInPort->GetOutPort() ) {
      GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
      if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
        if ( begin ) {
          beginService( "CNode_Impl::GetLink" );
          begin = false ;
	}
        bool Success ;
        Link_Impl * myLink = new Link_Impl( _Orb , _Poa , _ContId ,
                                            instanceName() , interfaceName() ,
                                            DataFlowEditor() ,
                                            DataFlowNode() ,
                                            ToServiceParameterName ,
                                            (GraphEditor::InNode *) DataFlowEditor()->Graph()->GetChangeGraphNode( FromNodeName )->GetInNode() ,
                                            FromServiceParameterName ,
                                            true , false , Success ) ;
        if ( Success ) {
          PortableServer::ObjectId * id = myLink->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          iobject = SUPERV::Link::_narrow(obj) ;
          anOutPort->AddInPortObjRef( anInPort , SUPERV::Link::_duplicate( iobject ) ) ;
	}
      }
      else {
        iobject = anOutPort->InPortObjRef( anInPort ) ;
      }
    }
  }
  if ( !begin ) {
    endService( "CNode_Impl::GetLink" );
  }
  return SUPERV::Link::_duplicate( iobject ) ;
}

SUPERV::StreamLink_ptr CNode_Impl::GetStreamLink(const char * ToServiceParameterName ) {
  bool begin = true ;
  SUPERV::StreamLink_var iobject = SUPERV::StreamLink::_nil() ;
  char* FromNodeName ;
  char* FromServiceParameterName ;
  if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    bool status = DataFlowEditor()->GetLink( DataFlowNode()->Name() ,
                                            ToServiceParameterName ,
                                            & FromNodeName ,
                                            & FromServiceParameterName ) ;
    if ( status ) {
      GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( ToServiceParameterName ) ;
      if ( anInPort->IsDataStream() && anInPort->GetOutPort() ) {
        GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
        if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
          if ( begin ) {
            beginService( "CNode_Impl::GetLink" );
            begin = false ;
  	  }
          bool Success ;
          StreamLink_Impl * myStreamLink = new StreamLink_Impl(
                                            _Orb , _Poa , _ContId ,
                                            instanceName() , interfaceName() ,
                                            DataFlowEditor() ,
                                            DataFlowNode() ,
                                            ToServiceParameterName ,
                                            (GraphEditor::InNode *) DataFlowEditor()->Graph()->GetChangeGraphNode( FromNodeName )->GetInNode() ,
                                            FromServiceParameterName ,
                                            true , Success ) ;
          if ( Success ) {
            PortableServer::ObjectId * id = myStreamLink->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id);
            iobject = SUPERV::StreamLink::_narrow(obj) ;
            anOutPort->AddInPortObjRef( anInPort , SUPERV::StreamLink::_duplicate( iobject ) ) ;
	  }
        }
        else {
          iobject = SUPERV::StreamLink::_narrow( anOutPort->InPortObjRef( anInPort ) ) ;
        }
      }
    }
  }
  if ( !begin ) {
    endService( "CNode_Impl::GetLink" );
  }
  return SUPERV::StreamLink::_duplicate( iobject ) ;
}

SUPERV::ListOfPorts * CNode_Impl::Ports() {
  bool begin = true ;
  int i , j ;
  int PortCount = 0 ;
  SUPERV::ListOfPorts_var RetVal = new SUPERV::ListOfPorts ;
  if ( _IsNode ) {
    for ( i = 0 ; i < DataFlowNode()->GetNodeInPortsSize() ; i++ ) {
      GraphBase::InPort * anInPort = DataFlowNode()->GetChangeNodeInPort( i ) ;
      if ( !anInPort->IsDataStream() ) {
        if ( begin ) {
          beginService( "CNode_Impl::Ports" );
          begin = false ;
        }
        if ( anInPort->IsLoop() || ( anInPort->IsGate() && anInPort->IsNotConnected() && 
                                     ( /*IsExecuting() || */DataFlowEditor()->IsReadOnly() ) ) ) { // mkr : IPAL11362
//          MESSAGE( "InPort " << i << " " << anInPort->PortName() << " of Node " << Name() << " ignored" ) ;
        }
        else if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
//          MESSAGE( "InPort " << i << " " << anInPort->PortName() << " of Node " << Name() << " IsExecuting "
//                   << IsExecuting() << " IsGate/IsConnected " << anInPort->IsGate()
//                   << "/" << anInPort->IsNotConnected() ) ;
          Port_Impl * myPort ;
          if ( DataFlowNode()->HasInput( anInPort->PortName() ) ) {
//JR 30.03.2005            const CORBA::Any * anAny = anInPort->GetOutPort()->Value() ;
            const CORBA::Any anAny = anInPort->GetOutPort()->Value() ;
            myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                    instanceName() , interfaceName() ,
                                    DataFlowEditor() ,
                                    DataFlowNode() ,
                                    (GraphBase::Port * ) anInPort ,
                                    true ,
                                    &anAny ) ;
          }
          else {
            myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                    instanceName() , interfaceName() ,
                                    DataFlowEditor() ,
                                    DataFlowNode() ,
                                    (GraphBase::Port * ) anInPort ,
                                    true ) ;
          }
          PortableServer::ObjectId * id = myPort->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          SUPERV::Port_var iobject ;
          iobject = SUPERV::Port::_narrow(obj) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( iobject ) ;
          anInPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
        }
        else {
//          MESSAGE( "InPort " << i << " " << anInPort->PortName() << " of Node " << Name() ) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( anInPort->ObjRef() ) ;
        }
      }
      else {
//        MESSAGE( "InPort " << i << " " << anInPort->PortName() << " of Node " << Name() << " IsDataStream" ) ;
      }
    }
    for ( i = 0 ; i < DataFlowNode()->GetNodeOutPortsSize() ; i++ ) {
      GraphBase::OutPort * anOutPort = DataFlowNode()->GetChangeNodeOutPort( i ) ;
      if ( !anOutPort->IsDataStream() ) {
        if ( begin ) {
          beginService( "CNode_Impl::Ports" );
          begin = false ;
        }
        if ( anOutPort->IsLoop() || ( anOutPort->IsGate() && anOutPort->IsNotConnected() &&
                                      ( /*IsExecuting() || */DataFlowEditor()->IsReadOnly() ) ) ) { // mkr : IPAL11362
//          MESSAGE( "OutPort " << i << " " << anOutPort->PortName() << " of Node " << Name() << " ignored" ) ;
        }
        else if ( CORBA::is_nil( anOutPort->ObjRef() ) ) {
//          MESSAGE( "OutPort " << i << " " << anOutPort->PortName() << " of Node " << Name() ) ;
//JR 30.03.2005          const CORBA::Any * anAny = anOutPort->Value() ;
          const CORBA::Any anAny = anOutPort->Value() ;
          Port_Impl * myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                              instanceName() , interfaceName() ,
                                              DataFlowEditor() ,
                                              DataFlowNode() ,
                                              (GraphBase::Port * ) anOutPort ,
                                              false ,
                                              &anAny ) ;
          PortableServer::ObjectId * id = myPort->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          SUPERV::Port_var iobject ;
          iobject = SUPERV::Port::_narrow(obj) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( iobject ) ;
          anOutPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
        }
        else {
//          MESSAGE( "OutPort " << i << " " << anOutPort->PortName() << " of Node " << Name() ) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( anOutPort->ObjRef() ) ;
        }
      }
      else {
//        MESSAGE( "OutPort " << i << " " << anOutPort->PortName() << " of Node " << Name() << " IsDataStream" ) ;
      }
    }
  }
  else {
    if ( DataFlowEditor()->IsValid() ) {
      //DataFlowEditor()->Graph()->Values() ;
      RetVal->length( DataFlowEditor()->Graph()->GetNodeInDataNodePortsSize() +
                      DataFlowEditor()->Graph()->GetNodeOutDataNodePortsSize() ) ;
      for ( i = 0 ; i < DataFlowEditor()->Graph()->GetNodeInDataNodePortsSize() ; i++ ) {
        GraphBase::OutPort * anOutPort = DataFlowEditor()->Graph()->GetChangeNodeInDataNodePort(i) ;
        if ( !anOutPort->IsDataStream() ) {
          if ( CORBA::is_nil( anOutPort->ObjRef() ) ) {
            if ( begin ) {
              beginService( "CNode_Impl::Ports" );
              begin = false ;
	    }
            Port_Impl * myPort ;
            if ( anOutPort->IsDataConnected() ) {
//JR 30.03.2005              const CORBA::Any * anAny = anOutPort->Value() ;
              const CORBA::Any anAny = anOutPort->Value() ;
              myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      (GraphBase::Port * ) anOutPort ,
                                      true ,
                                      &anAny ) ;
            }
            else {
              myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      (GraphBase::Port * ) anOutPort ,
                                      true ) ;
            }
            PortableServer::ObjectId * id = myPort->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id);
            SUPERV::Port_var iobject ;
            iobject = SUPERV::Port::_narrow(obj) ;
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( iobject ) ;
            anOutPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
          }
          else {
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( anOutPort->ObjRef() ) ;
	  }
        }
        else {
        }
      }
      j = DataFlowEditor()->Graph()->GetNodeInDataNodePortsSize() ;
      for ( i = 0 ; i < DataFlowEditor()->Graph()->GetNodeOutDataNodePortsSize() ; i++ ) {
        GraphBase::InPort * anInPort = DataFlowEditor()->Graph()->GetChangeNodeOutDataNodePort(i) ;
        if ( !anInPort->IsDataStream() ) {
          if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
            if ( begin ) {
              beginService( "CNode_Impl::Ports" );
              begin = false ;
	    }
            Port_Impl * myPort ;
            if ( anInPort->IsDataConnected() ) {
//JR 30.03.2005              const CORBA::Any * anAny = anInPort->GetOutPort()->Value() ;
              const CORBA::Any anAny = anInPort->GetOutPort()->Value() ;
              myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      (GraphBase::Port * ) anInPort ,
                                      false ,
                                      &anAny ) ;
            }
            else {
              myPort = new Port_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      anInPort ,
                                      false ) ;
            }
            PortableServer::ObjectId * id = myPort->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id);
            SUPERV::Port_var iobject ;
            iobject = SUPERV::Port::_narrow(obj) ;
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( iobject ) ;
            anInPort->ObjRef( SUPERV::Port::_duplicate( iobject ) ) ;
          }
          else {
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::Port::_duplicate( anInPort->ObjRef() ) ;
	  }
        }
        else {
        }
      }
    }
  }
#if 0
  int RetVallength = (unsigned int ) RetVal->length() ;
  for ( i = 0 ; i < RetVallength ; i++ ) {
    MESSAGE( "Port " << i << " " <<  RetVal[i]->Name() << " of Node " << Name() ) ;
  }
#endif
  if ( !begin ) {
    endService( "CNode_Impl::Ports " ) ;
  }
  return ( RetVal._retn() ) ;
}

SUPERV::ListOfStreamPorts * CNode_Impl::StreamPorts() {
  bool begin = true ;
  int i , j ;
  int PortCount = 0 ;
  SUPERV::ListOfStreamPorts_var RetVal = new SUPERV::ListOfStreamPorts ;
  if ( !DataFlowEditor()->Graph()->IsDataStreamNode() ) {
//    MESSAGE("CNode_Impl::StreamPorts NOT a DataStreamNode" ) ;
  }
  else if ( _IsNode ) {
//    RetVal->length( DataFlowNode()->GetNodeInPortsSize() +
//                    DataFlowNode()->GetNodeOutPortsSize() ) ;
    for ( i = 0 ; i < DataFlowNode()->GetNodeInPortsSize() ; i++ ) {
      GraphBase::InPort * anInPort = DataFlowNode()->GetChangeNodeInPort( i ) ;
      if ( anInPort->IsDataStream() ) {
        if ( begin ) {
          beginService( "CNode_Impl::StreamPorts" );
          begin = false ;
        }
        if ( anInPort->IsLoop() || ( anInPort->IsGate() && anInPort->IsNotConnected() && 
                                     ( IsExecuting() || DataFlowEditor()->IsReadOnly() ) ) ) {
          MESSAGE( "InStreamPort " << i << " " << anInPort->PortName() << " of Node " << Name() << " ignored" ) ;
//          RetVal[ i ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( CORBA::Object::_nil() ) ) ;
        }
        else if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
          MESSAGE( "InStreamPort " << i << " " << anInPort->PortName() << " of Node " << Name() << " IsExecuting "
                   << IsExecuting() << " IsGate/IsConnected " << anInPort->IsGate()
                   << "/" << anInPort->IsNotConnected() ) ;
          StreamPort_Impl * myStreamPort ;
          if ( DataFlowNode()->HasInput( anInPort->PortName() ) ) {
//JR 30.03.2005            const CORBA::Any * anAny = anInPort->GetOutPort()->Value() ;
            const CORBA::Any anAny = anInPort->GetOutPort()->Value() ;
            myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                    instanceName() , interfaceName() ,
                                    DataFlowEditor() ,
                                    DataFlowNode() ,
                                    (GraphBase::Port * ) anInPort ,
                                    true ,
                                    &anAny ) ;
          }
          else {
            myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                    instanceName() , interfaceName() ,
                                    DataFlowEditor() ,
                                    DataFlowNode() ,
                                    (GraphBase::Port * ) anInPort ,
                                    true ) ;
          }
          PortableServer::ObjectId * id = myStreamPort->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          SUPERV::StreamPort_var iobject ;
          iobject = SUPERV::StreamPort::_narrow(obj) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( iobject ) ;
          anInPort->ObjRef( SUPERV::StreamPort::_duplicate( iobject ) ) ;
        }
        else {
          MESSAGE( "InStreamPort " << i << " " << anInPort->PortName() << " of Node " << Name() ) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( anInPort->ObjRef() ) ) ;
        }
      }
      else {
//        RetVal[ i ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( CORBA::Object::_nil() ) ) ;
      }
    }
    for ( i = 0 ; i < DataFlowNode()->GetNodeOutPortsSize() ; i++ ) {
      GraphBase::OutPort * anOutPort = DataFlowNode()->GetChangeNodeOutPort( i ) ;
      if ( anOutPort->IsDataStream() ) {
        if ( begin ) {
          beginService( "CNode_Impl::StreamPorts" );
          begin = false ;
        }
        if ( anOutPort->IsLoop() || ( anOutPort->IsGate() && anOutPort->IsNotConnected() &&
                                      ( IsExecuting() || DataFlowEditor()->IsReadOnly() ) ) ) {
          MESSAGE( "OutStreamPort " << i << " " << anOutPort->PortName() << " of Node " << Name() << " ignored" ) ;
//          RetVal[ DataFlowNode()->GetNodeInPortsSize() + i ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( CORBA::Object::_nil() ) ) ;
        }
        else if ( CORBA::is_nil( anOutPort->ObjRef() ) ) {
          MESSAGE( "OutStreamPort " << i << " " << anOutPort->PortName() << " of Node " << Name() ) ;
//JR 30.03.2005          const CORBA::Any * anAny = anOutPort->Value() ;
          const CORBA::Any anAny = anOutPort->Value() ;
          StreamPort_Impl * myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                              instanceName() , interfaceName() ,
                                              DataFlowEditor() ,
                                              DataFlowNode() ,
                                              (GraphBase::Port * ) anOutPort ,
                                              false ,
                                              &anAny ) ;
          PortableServer::ObjectId * id = myStreamPort->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          SUPERV::StreamPort_var iobject ;
          iobject = SUPERV::StreamPort::_narrow(obj) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( iobject ) ;
          anOutPort->ObjRef( SUPERV::StreamPort::_duplicate( iobject ) ) ;
        }
        else {
          MESSAGE( "OutStreamPort " << i << " " << anOutPort->PortName() << " of Node " << Name() ) ;
          PortCount += 1 ;
          RetVal->length( PortCount ) ;
          RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( anOutPort->ObjRef() ) ) ;
        }
      }
      else {
//        RetVal[ i ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( CORBA::Object::_nil() ) ) ;
      }
    }
  }
  else {
    if ( DataFlowEditor()->IsValid() ) {
//      RetVal->length( DataFlowEditor()->Graph()->GetNodeInDataNodePortsSize() +
//                      DataFlowEditor()->Graph()->GetNodeOutDataNodePortsSize() ) ;
      for ( i = 0 ; i < DataFlowEditor()->Graph()->GetNodeInDataNodePortsSize() ; i++ ) {
        GraphBase::OutPort * anOutPort = DataFlowEditor()->Graph()->GetChangeNodeInDataNodePort(i) ;
        if ( anOutPort->IsDataStream() ) {
          if ( CORBA::is_nil( anOutPort->ObjRef() ) ) {
            if ( begin ) {
              beginService( "CNode_Impl::StreamPorts" );
              begin = false ;
	    }
            StreamPort_Impl * myStreamPort ;
            if ( anOutPort->IsDataConnected() ) {
//JR 30.03.2005              const CORBA::Any * anAny = anOutPort->Value() ;
              const CORBA::Any anAny = anOutPort->Value() ;
              myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      (GraphBase::Port * ) anOutPort ,
                                      true ,
                                      &anAny ) ;
            }
            else {
              myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      (GraphBase::Port * ) anOutPort ,
                                      true ) ;
            }
            PortableServer::ObjectId * id = myStreamPort->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id);
            SUPERV::StreamPort_var iobject ;
            iobject = SUPERV::StreamPort::_narrow(obj) ;
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( iobject ) ;
            anOutPort->ObjRef( SUPERV::StreamPort::_duplicate( iobject ) ) ;
          }
          else {
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( anOutPort->ObjRef() ) ) ;
	  }
        }
        else {
//          RetVal[ i ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( CORBA::Object::_nil() ) ) ;
        }
      }
      j = DataFlowEditor()->Graph()->GetNodeInDataNodePortsSize() ;
      for ( i = 0 ; i < DataFlowEditor()->Graph()->GetNodeOutDataNodePortsSize() ; i++ ) {
        GraphBase::InPort * anInPort = DataFlowEditor()->Graph()->GetChangeNodeOutDataNodePort(i) ;
        if ( anInPort->IsDataStream() ) {
          if ( CORBA::is_nil( anInPort->ObjRef() ) ) {
            if ( begin ) {
              beginService( "CNode_Impl::StreamPorts" );
              begin = false ;
	    }
            StreamPort_Impl * myStreamPort ;
            if ( anInPort->IsDataConnected() ) {
//JR 30.03.2005              const CORBA::Any * anAny = anInPort->GetOutPort()->Value() ;
              const CORBA::Any anAny = anInPort->GetOutPort()->Value() ;
              myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      (GraphBase::Port * ) anInPort ,
                                      false ,
                                      &anAny ) ;
            }
            else {
              myStreamPort = new StreamPort_Impl( _Orb , _Poa , _ContId ,
                                      instanceName() , interfaceName() ,
                                      DataFlowEditor() ,
                                      DataFlowNode() ,
                                      anInPort ,
                                      false ) ;
            }
            PortableServer::ObjectId * id = myStreamPort->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id);
            SUPERV::StreamPort_var iobject ;
            iobject = SUPERV::StreamPort::_narrow(obj) ;
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( iobject ) ;
            anInPort->ObjRef( SUPERV::StreamPort::_duplicate( iobject ) ) ;
          }
          else {
            PortCount += 1 ;
            RetVal->length( PortCount ) ;
            RetVal[ PortCount - 1 ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( anInPort->ObjRef() ) ) ;
	  }
        }
        else {
//          RetVal[ i ] = SUPERV::StreamPort::_duplicate( SUPERV::StreamPort::_narrow( CORBA::Object::_nil() ) ) ;
        }
      }
    }
  }
//  int RetVallength = (unsigned int ) RetVal->length() ;
//  for ( i = 0 ; i < RetVallength ; i++ ) {
//    MESSAGE( "StreamPort " << i << " " <<  RetVal[i]->Name() << " of Node " << Name() ) ;
//  }
  if ( !begin ) {
    endService( "CNode_Impl::StreamPorts " ) ;
  }
  return ( RetVal._retn() ) ;
}

SUPERV::ListOfLinks * CNode_Impl::Links() {
  beginService( "CNode_Impl::Links" ) ;
  SUPERV::ListOfLinks_var RetVal = new SUPERV::ListOfLinks ;
  if ( DataFlowNode() ) {
//    MESSAGE( "CNode_Impl::Links " << DataFlowEditor() << " " << DataFlowEditor()->Graph() << " " << DataFlowEditor()->Graph()->ObjImpl() << " " << DataFlowNode()->ComputingNode() << " " << DataFlowNode()->ComputingNode()->Name() ) ;
    RetVal = ((Graph_Impl * ) DataFlowEditor()->Graph()->ObjImpl())->Links( DataFlowNode()->ComputingNode() , NULL ) ;
  }
  else {
    RetVal = ((Graph_Impl * ) DataFlowEditor()->Graph()->ObjImpl())->Links( NULL , NULL ) ;
  }
  MESSAGE( "CNode_Impl::Links " << RetVal->length() << " Links" ) ;
  endService( "CNode_Impl::Links" ) ;
  return ( RetVal._retn() ) ;
}

SUPERV::ListOfStreamLinks * CNode_Impl::StreamLinks() {
  beginService( "CNode_Impl::StreamLinks" ) ;
  SUPERV::ListOfStreamLinks_var RetVal = new SUPERV::ListOfStreamLinks ;
  if ( DataFlowNode() && DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    RetVal = ((StreamGraph_Impl *) (DataFlowEditor()->StreamGraph()->ObjImpl()))->StreamLinks( DataFlowNode()->ComputingNode() , NULL ) ;
  }
  else if ( DataFlowEditor()->Graph()->IsDataStreamNode() ) {
    RetVal = ((StreamGraph_Impl *) (DataFlowEditor()->StreamGraph()->ObjImpl()))->StreamLinks( NULL , NULL ) ;
  }
  MESSAGE( "CNode_Impl::StreamLinks " << RetVal->length() << " StreamLinks" ) ;
  endService( "CNode_Impl::StreamLinks" ) ;
  return ( RetVal._retn() ) ;
}

CORBA::Long CNode_Impl::SubGraph() {
//  beginService( "CNode_Impl::SubGraph" );
  CORBA::Long RetVal = 0 ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->SubGraph() ;
  }
//  endService( "CNode_Impl::SubGraph" );
  return RetVal ;
}

CORBA::Long CNode_Impl::SubStreamGraph() {
//  beginService( "CNode_Impl::SubStreamGraph" );
  CORBA::Long RetVal = 0 ;
  if ( _IsNode ) {
    RetVal = DataFlowNode()->SubStreamGraph() ;
  }
//  endService( "CNode_Impl::SubStreamGraph" );
  return RetVal ;
}

// mkr : PAL8060 : this method is not used
/*bool CNode_Impl::IsLinked(const char * ServiceParameterName ) {
  beginService( "CNode_Impl::IsLinked" );
  bool RetVal = DataFlowNode()->IsLinked( ServiceParameterName ) ;
  MESSAGE( Name() << "->IsLinked( '" << ServiceParameterName << "' )" ) ;
  endService( "CNode_Impl::IsLinked" );
  return RetVal ;
  }*/

bool CNode_Impl::HasInput(const char * ServiceParameterName ) {
//  beginService( "CNode_Impl::HasInput" );
  bool RetVal = DataFlowNode()->HasInput( ServiceParameterName ) ;
//  endService( "CNode_Impl::HasInput" );
  return RetVal ;
}

SUPERV::GraphState CNode_Impl::State() {
//  beginService( "CNode_Impl::State" );
  SUPERV::GraphState RetVal = SUPERV::EditingState ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor && !DataFlowEditor()->EditedAfterExecution() ) {
    //JR : 12/06/03  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->State( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->State() ;
    }
  }
//  endService( "CNode_Impl::State" );
  return RetVal ;
}
CORBA::Long CNode_Impl::Thread() {
//  beginService( "CNode_Impl::Thread" );
  CORBA::Long RetVal = 0 ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->Thread( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->Thread() ;
    }
  }
//  endService( "CNode_Impl::Thread" );
  return RetVal ;
}
GraphExecutor::AutomatonState CNode_Impl::AutoState() {
//  beginService( "CNode_Impl::AutoState" );
  GraphExecutor::AutomatonState RetVal = GraphExecutor::UnKnownState ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->AutomatonState( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->AutomatonState() ;
    }
  }
//  endService( "CNode_Impl::AutoState" );
  return RetVal ;
}
SUPERV::ControlState CNode_Impl::Control() {
//  beginService( "CNode_Impl::Control" );
  SUPERV::ControlState RetVal = SUPERV::VoidState ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->ControlState( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->ControlState() ;
    }
  }
//  endService( "CNode_Impl::Control" );
  return RetVal ;
}
void CNode_Impl::ControlClear() {
//  beginService( "CNode_Impl::ControlClear" );
//  SUPERV::ControlState RetVal = SUPERV::VoidState ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      aDataFlowExecutor->ControlClear( Name() ) ;
    }
    else {
      aDataFlowExecutor->ControlClear() ;
    }
  }
//  endService( "CNode_Impl::ControlClear" );
  return ;
}

bool CNode_Impl::IsReady() {
//  beginService( "CNode_Impl::IsReady" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->IsReady( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->IsReady() ;
    }
  }
//  endService( "CNode_Impl::IsReady" );
  return RetVal ;
}

bool CNode_Impl::IsWaiting() {
//  beginService( "CNode_Impl::IsWaiting" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->IsWaiting( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->IsWaiting() ;
    }
  }
//  endService( "CNode_Impl::IsWaiting" );
  return RetVal ;
}

bool CNode_Impl::IsRunning() {
//  beginService( "CNode_Impl::IsRunning" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->IsRunning( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->IsRunning() ;
    }
  }
//  endService( "CNode_Impl::IsRunning" );
  return RetVal ;
}

bool CNode_Impl::IsDone() {
//  beginService( "CNode_Impl::IsDone" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->IsDone( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->IsDone() ;
    }
  }
//  endService( "CNode_Impl::IsDone" );
  return RetVal ;
}

bool CNode_Impl::IsSuspended() {
//  beginService( "CNode_Impl::IsSuspended" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->IsSuspended( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->IsSuspended() ;
    }
  }
//  endService( "CNode_Impl::IsSuspended" );
  return RetVal ;
}

bool CNode_Impl::ReadyW() {
//  beginService( "CNode_Impl::ReadyW" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->ReadyWait( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->ReadyWait() ;
    }
  }
//  endService( "CNode_Impl::ReadyW" );
  return RetVal ;
}

bool CNode_Impl::RunningW() {
//  beginService( "CNode_Impl::RunningW" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->RunningWait( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->RunningWait() ;
    }
  }
//  endService( "CNode_Impl::RunningW" );
  return RetVal ;
}

bool CNode_Impl::DoneW() {
//  beginService( "CNode_Impl::DoneW" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->DoneWait( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->DoneWait() ;
    }
  }
//  endService( "CNode_Impl::DoneW" );
  return RetVal ;
}

bool CNode_Impl::SuspendedW() {
//  beginService( "CNode_Impl::SuspendedW" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->SuspendedWait( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->SuspendedWait() ;
    }
  }
//  endService( "CNode_Impl::SuspendedW" );
  return RetVal ;
}

void CNode_Impl::ping() {
//  beginService( "CNode_Impl::ping" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->Ping( Name() ) ;
    }
    else {
      RetVal = false ;
    }
  }
  if ( IsGraph() || IsStreamGraph() ) {
    theAutomaton->GraphImpl( (Graph_Impl * ) this ) ;
  }
//  endService( "CNode_Impl::ping" );
  return ;
}

bool CNode_Impl::ContainerKill() {
  beginService( "CNode_Impl::ContainerKill" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor && IsExecuting() ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->ContainerKill( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->ContainerKill() ;
    }
  }
  endService( "CNode_Impl::ContainerKill" );
  return RetVal ;
}
bool CNode_Impl::Kill() {
  beginService( "CNode_Impl::Kill" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->Kill( Name() ) ;
      MESSAGE( "Node "  << Name() << " execution was killed : " << RetVal ) ;
    }
    else {
      RetVal = aDataFlowExecutor->Kill() ;
      MESSAGE( "Graph execution was killed : " << RetVal ) ;
    }
  }
  endService( "CNode_Impl::Kill" );
  return RetVal ;
}
bool CNode_Impl::KillDone() {
//  beginService( "CNode_Impl::KillDone" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->KillDone( Name() ) ;
    }
  }
//  endService( "CNode_Impl::KillDone" );
  return RetVal ;
}
bool CNode_Impl::Stop() {
//  beginService( "CNode_Impl::Stop" );
  bool RetVal ; // = aDataFlowExecutor->Stop() ;
//  endService( "CNode_Impl::Stop" );
  return RetVal ;
}
bool CNode_Impl::Suspend() {
  beginService( "CNode_Impl::Suspend" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      MESSAGE( "CNode_Impl::Suspend " << Name() ) ;
      RetVal = aDataFlowExecutor->Suspend( Name() ) ;
    }
    else {
      MESSAGE( "CNode_Impl::Suspend " << aDataFlowExecutor->Graph()->Name() ) ;
      RetVal = aDataFlowExecutor->Suspend() ;
    }
  }
  endService( "CNode_Impl::Suspend" );
  return RetVal ;
}
bool CNode_Impl::SuspendDone() {
//  beginService( "CNode_Impl::SuspendDone" );
  bool RetVal = false ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->SuspendDone( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->SuspendDone() ;
    }
  }
//  endService( "CNode_Impl::SuspendDone" );
  return RetVal ;
}
bool CNode_Impl::Resume() {
  bool RetVal = false ;
  beginService( "CNode_Impl::Resume" );
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      MESSAGE( "CNode_Impl::Resume " << Name() ) ;
      RetVal = aDataFlowExecutor->Resume( Name() ) ;
    }
    else {
      MESSAGE( "CNode_Impl::Resume " << aDataFlowExecutor->Graph()->Name() ) ;
      RetVal = aDataFlowExecutor->Resume() ;
    }
  }
  endService( "CNode_Impl::Resume" );
  return RetVal ;
}

CORBA::Long CNode_Impl::CpuUsed() {
  CORBA::Long RetVal = 0 ;
  GraphExecutor::DataFlow * aDataFlowExecutor = DataFlowEditor()->Executor() ;
  if ( aDataFlowExecutor ) {
    if ( _IsNode ) {
      RetVal = aDataFlowExecutor->CpuUsed( Name() ) ;
    }
    else {
      RetVal = aDataFlowExecutor->CpuUsed() ;
    }
  }
  return RetVal ;
}

bool CNode_Impl::IsExecuting() {
  bool RetVal = false;
  if ( !IsMacro() && DataFlowEditor() && DataFlowEditor()->Executor() ) {
    // asv : the statement below normally does not return true, Executor_OutNode
    //       sets Editor->Editing() after finishing of execution (see Executor_OutNode.cxx)
    if ( DataFlowEditor()->IsExecuting() && DataFlowEditor()->Executor()->IsDone() ) {
      DataFlowEditor()->Editing();
    }

    RetVal = DataFlowEditor()->IsExecuting();
  }
  return RetVal;
}

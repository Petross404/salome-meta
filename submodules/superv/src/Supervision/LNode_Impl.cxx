//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : LNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "LNode_Impl.hxx"

LNode_Impl::LNode_Impl() {
}

LNode_Impl::LNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        GraphBase::ListOfFuncName FuncNameList ,
                        GraphBase::ListOfPythonFunctions PythonFunctionList ,
//                        const char * InitName , 
//                        const SUPERV::ListOfStrings & anInitPythonFunction ,
//                        const char * MoreName , 
//                        const SUPERV::ListOfStrings & aMorePythonFunction ,
//                        const char * NextName , 
//                        const SUPERV::ListOfStrings & aNextPythonFunction ,
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
//  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , InitName , anInitPythonFunction , NodeKindOfNode ) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , FuncNameList , PythonFunctionList , NodeKindOfNode ) {
//  MESSAGE( NodeName << " " );
  beginService( "LNode_Impl::LNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode()->SetPyMorePythonFunction( MoreName , aMorePythonFunction ) ;
//  DataFlowNode()->SetPyNextPythonFunction( NextName , aNextPythonFunction ) ;
  endService( "LNode_Impl::LNode_Impl" );  
}

LNode_Impl::LNode_Impl( CORBA::ORB_ptr orb ,
	              PortableServer::POA_ptr poa ,
	              PortableServer::ObjectId * contId , 
	              const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * aDataFlowEditor ,
                      GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
  MESSAGE( DataFlowNode()->Name() << " " );
  beginService( "LNode_Impl::LNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  endService( "LNode_Impl::LNode_Impl" );  
}

LNode_Impl::~LNode_Impl() {
  beginService( "LNode_Impl::~LNode_Impl" );
  endService( "LNode_Impl::~LNode_Impl" );
}

void LNode_Impl::destroy() {
  beginService( "LNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
    SUPERV::GNode_ptr aCoupled = SUPERV::GNode::_narrow( Coupled() ) ;
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("LNode_Impl::destroy ERROR ") ;
    }
    if ( !CORBA::is_nil( aCoupled ) ) {
//      aCoupled->SetCoupled( "" ) ; : Used in GraphBase::Graph::RemoveNode( EndLoopNode ) :
      aCoupled->destroy() ;
    }
// Valid() only after deletion of the corresponding EndLoopNode :
    DataFlowEditor()->UnValid() ;
  }
  endService( "LNode_Impl::Destroy" );
}

bool LNode_Impl::Delete() {
  beginService( "LNode_Impl::Delete" );
  bool RetVal = false ;  
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
// Valid() only after deletion of the corresponding EndLoopNode :
//    if ( RetVal ) {
//      RetVal = DataFlowEditor()->IsValid() ;
//    }
  }
  endService( "LNode_Impl::Delete" );
  return RetVal ;
}

void LNode_Impl::SetPyInit( const char * InitName ,
                            const SUPERV::ListOfStrings & aPyInit ) {
  beginService( "LNode_Impl::SetPyInit" );
  SetPyFunction( InitName , aPyInit ) ;
  endService( "LNode_Impl::SetPyInit" );
  return ;
}

SUPERV::ListOfStrings * LNode_Impl::PyInit() {
  SUPERV::ListOfStrings * aPythonFunction ;
  beginService( "LNode_Impl::PyInit" );
  aPythonFunction = PyFunction() ;
  endService( "LNode_Impl::PyInit" );
  return aPythonFunction ;
}

char * LNode_Impl::PyInitName() {
  beginService( "LNode_Impl::PyInitName" );
  char * RetVal = PyFuncName() ;
  endService( "LNode_Impl::PyInitName" );
  return CORBA::string_dup( RetVal ) ;
}

void LNode_Impl::SetPyMore( const char * MoreName ,
                            const SUPERV::ListOfStrings & aPyMore ) {
  beginService( "LNode_Impl::SetPyMore" );
  DataFlowNode()->SetPyMorePythonFunction( MoreName , aPyMore ) ;
  endService( "LNode_Impl::SetPyMore" );
  return ;
}

SUPERV::ListOfStrings * LNode_Impl::PyMore() {
  SUPERV::ListOfStrings * aPythonFunction ;
  beginService( "LNode_Impl::PyMore" );
  aPythonFunction = DataFlowNode()->PyMorePythonFunction() ;
  endService( "LNode_Impl::PyMore" );
  return aPythonFunction ;
}

char * LNode_Impl::PyMoreName() {
  beginService( "LNode_Impl::PyMoreName" );
  char * RetVal = DataFlowNode()->PyMoreName() ;
  endService( "LNode_Impl::PyMoreName" );
  return CORBA::string_dup( RetVal ) ;
}

void LNode_Impl::SetPyNext( const char * NextName ,
                            const SUPERV::ListOfStrings & aPyNext ) {
  beginService( "LNode_Impl::SetPyNext" );
  DataFlowNode()->SetPyNextPythonFunction( NextName , aPyNext ) ;
  endService( "LNode_Impl::SetPyNext" );
  return ;
}

SUPERV::ListOfStrings * LNode_Impl::PyNext() {
  SUPERV::ListOfStrings * aPythonFunction ;
  beginService( "LNode_Impl::PyNext" );
  aPythonFunction = DataFlowNode()->PyNextPythonFunction() ;
  endService( "LNode_Impl::PyNext" );
  return aPythonFunction ;
}

char * LNode_Impl::PyNextName() {
  beginService( "LNode_Impl::PyNextName" );
  char * RetVal = DataFlowNode()->PyNextName() ;
  endService( "LNode_Impl::PyNextName" );
  return CORBA::string_dup( RetVal );
}



//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : SNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "SNode_Impl.hxx"

SNode_Impl::SNode_Impl() {
}

SNode_Impl::SNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const char * FuncName , 
                        const SUPERV::ListOfStrings & PythonFunction ,
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , FuncName , PythonFunction , NodeKindOfNode ) {
//  MESSAGE( NodeName << " " );
  beginService( "SNode_Impl::SNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode( DataFlowEditor()->AddNode( NodeService , NodeComponentName ,
//                                           NodeInterfaceName , NodeName ,
//                                           NodeKindOfNode ) ) ;
//  DataFlowNode( DataFlowEditor()->AddNode( SALOME_ModuleCatalog::Service() ,
//                                           "" , "" , "" ,
//                                           NodeKindOfNode ) ) ;
  endService( "SNode_Impl::SNode_Impl" );  
}

SNode_Impl::SNode_Impl( CORBA::ORB_ptr orb ,
	              PortableServer::POA_ptr poa ,
	              PortableServer::ObjectId * contId , 
	              const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * aDataFlowEditor ,
                      GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
  MESSAGE( DataFlowNode()->Name() << " " );
  beginService( "SNode_Impl::SNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  endService( "SNode_Impl::SNode_Impl" );  
}

SNode_Impl::~SNode_Impl() {
  beginService( "SNode_Impl::~SNode_Impl" );
  endService( "SNode_Impl::~SNode_Impl" );
}

void SNode_Impl::destroy() {
  beginService( "SNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
    SUPERV::GNode_ptr aCoupled = SUPERV::GNode::_narrow( Coupled() ) ;
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("SNode_Impl::destroy ERROR ") ;
    }
    if ( !CORBA::is_nil( aCoupled ) ) {
//      aCoupled->SetCoupled( "" ) ; : Used in GraphBase::Graph::RemoveNode( EndSwitchNode ) :
      aCoupled->destroy() ;
    }
// Valid() only after deletion of the corresponding EndSwitchNode :
    DataFlowEditor()->UnValid() ;
  }
  endService( "SNode_Impl::Destroy" );
}

bool SNode_Impl::Delete() {
  beginService( "SNode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
// Valid() only after deletion of the corresponding EndLoopNode :
//    if ( RetVal ) {
//      RetVal = DataFlowEditor()->IsValid() ;
//    }
  }
  endService( "SNode_Impl::Delete" );
  return RetVal ;
}


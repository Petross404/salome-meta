//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : INode_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _INODE_IMPL_HXX_
#define _INODE_IMPL_HXX_

//#include <iostream.h>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "CNode_Impl.hxx"

#include "StreamPort_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

class INode_Impl : public CNode_Impl ,
                   public POA_SUPERV::INode {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;

  public:
    INode_Impl();
    INode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                const char *aDataFlowName ,
                const SUPERV::KindOfNode aKindOfNode ) ;
    INode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                const char * FuncName , 
                const SUPERV::ListOfStrings & PythonFunction ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::InLineNode ) ;
    INode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                const GraphBase::ListOfFuncName FuncName , 
                const GraphBase::ListOfPythonFunctions PythonFunction ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::InLineNode ) ;
    INode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                GraphEditor::InNode * DataFlowNode ) ;
    virtual ~INode_Impl() ;
    virtual void destroy() ;
    virtual bool Delete() ;

    virtual void SetPyFunction( const char * FuncName ,
                                const SUPERV::ListOfStrings & aPyFunction ) ;
    virtual SUPERV::ListOfStrings * PyFunction() ;
    virtual char * PyFuncName() ;

    virtual GraphBase::InLineNode * BaseNode() {
            return DataFlowNode()->InLineNode() ; } ;

    virtual SUPERV::Port_ptr InPort( const char * aParameterName ,
                                     const char * aParameterType ) ;
    virtual SUPERV::Port_ptr OutPort( const char * aParameterName ,
                                      const char * aParameterType ) ;

    virtual SUPERV::StreamPort_ptr InStreamPort( const char * aParameterName ,
                                                 const char * aParameterType ,
                                                 const SALOME_ModuleCatalog::DataStreamDependency aDependency ) ;
    virtual SUPERV::StreamPort_ptr OutStreamPort( const char * aParameterName ,
                                                  const char * aParameterType ,
                                                  const SALOME_ModuleCatalog::DataStreamDependency aDependency ) ;

} ;

#endif

//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : FNode_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _FNODE_IMPL_HXX_
#define _FNODE_IMPL_HXX_

//#include <iostream.h>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "CNode_Impl.hxx"

#include "Port_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

class FNode_Impl : public CNode_Impl ,
                   public POA_SUPERV::FNode {
  private:

    CORBA::ORB_ptr _Orb ;
    PortableServer::POA_ptr _Poa ;
    PortableServer::ObjectId * _ContId ;
    bool _isCimpl;

  public:
    FNode_Impl();
    FNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char * instanceName ,
                const char * interfaceName ,
                const char * aDataFlowName ) ;
    FNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char * instanceName ,
                const char * interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                const SALOME_ModuleCatalog::Service &NodeService ,
                const char * NodeComponentName ,
                const char * NodeInterfaceName ,
                const char * NodeName = NULLSTRING ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::FactoryNode ,
		bool isCimpl = true ) ; // mkr : PAL11273 : C++ implementation by default
    FNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char * instanceName ,
                const char * interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                GraphEditor::InNode * DataFlowNode ) ;
    virtual ~FNode_Impl() ;
    virtual void destroy() ;
    virtual bool Delete() ;

    virtual char * GetComponentName() ;
    virtual bool SetComponentName(const char *) ;
    virtual char * GetInterfaceName() ;
    virtual bool SetInterfaceName(const char *) ;
    virtual char * GetContainer() ;
    virtual bool SetContainer( const char * aDataFlowContainer ) ;

    virtual GraphBase::FactoryNode * BaseNode() {
            return DataFlowNode()->FactoryNode() ; } ;

    virtual bool IsCimpl() { return _isCimpl; } ; // mkr : PAL11273
  
} ;

#endif

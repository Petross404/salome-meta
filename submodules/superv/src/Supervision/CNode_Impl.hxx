//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : CNode_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _CNODE_IMPL_HXX_
#define _CNODE_IMPL_HXX_

#include <iostream>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

class CNode_Impl : public POA_SUPERV::CNode ,
                   public Engines_Component_i  {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;

    GraphEditor::DataFlow   * _DataFlowEditor ;
    GraphEditor::InNode     * _DataFlowNode ;
    bool _IsNode ;

  public:
    CNode_Impl();
    CNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
	        const char *aDataFlowName ,
                const SUPERV::KindOfNode aKindOfNode ) ;
    CNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                const SALOME_ModuleCatalog::Service &NodeService ,
                const char * NodeName = NULLSTRING ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::ComputingNode ,
                const char * aFuncName = NULLSTRING ,
                const SUPERV::ListOfStrings & aPythonFunction = SUPERV::ListOfStrings() ,
		bool isCimpl = true ) ; // mkr : PAL11273 : C++ implementation by default
    CNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                const SALOME_ModuleCatalog::Service &NodeService ,
                const char * NodeName = NULLSTRING ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::InLineNode ,
                const GraphBase::ListOfFuncName & aFuncName = GraphBase::ListOfFuncName() ,
                const GraphBase::ListOfPythonFunctions & aPythonFunction = GraphBase::ListOfPythonFunctions() ) ;
    CNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                GraphEditor::InNode * DataFlowNode ) ;
    virtual ~CNode_Impl() ;
    virtual void destroy() ;
    virtual bool Delete() ;

    void DeletePorts() ;

    virtual void DataFlowEditor( GraphEditor::DataFlow * DataFlowEditor ) {
            _DataFlowEditor = DataFlowEditor ; } ;
    virtual GraphEditor::DataFlow * DataFlowEditor() {
            return _DataFlowEditor ; } ;

    virtual GraphExecutor::DataFlow * DataFlowExecutor() {
            return DataFlowEditor()->Executor(); } ;

    virtual void DataFlowNode( GraphEditor::InNode * DataFlowNode ) {
            _DataFlowNode = DataFlowNode ; } ;
    virtual GraphEditor::InNode * DataFlowNode() {
            return _DataFlowNode ; } ;

    virtual char * Name() ;
    virtual bool SetName( const char * aDataFlowName ) ;
    virtual SALOME_ModuleCatalog::Service * Service() ;
    virtual SUPERV::KindOfNode Kind() ;
    virtual SUPERV::SDate CreationDate() ;
    virtual SUPERV::SDate LastUpdateDate() ;
    virtual char * Version() ;
    virtual char * Author() ;
    virtual bool SetAuthor( const char * aDataFlowAuthor ) ;
    virtual char * Comment() ;
    virtual bool SetComment( const char * aDataFlowComment ) ;

    virtual GraphEditor::InNode * DataFlowNode() const {
            return _DataFlowNode ; } ;
    virtual GraphBase::ComputingNode * BaseNode() {
            return DataFlowNode()->ComputingNode() ; } ;

    virtual SUPERV::CNode_var ObjRef() {
            SUPERV::CNode_var iobject = SUPERV::GNode::_nil() ;
            if ( DataFlowNode() && _IsNode ) {
              iobject = SUPERV::CNode::_narrow( DataFlowNode()->ObjRef() ) ;
	    }
            else {
              iobject = SUPERV::CNode::_narrow( DataFlowEditor()->Graph()->ObjRef() ) ;
            }
            return iobject ; } ;
    virtual void SetObjRef(SUPERV::CNode_var aNode ) {
            if ( DataFlowNode() && _IsNode ) {
              DataFlowNode()->SetObjRef( aNode ) ;
	    }
            else {
              if ( DataFlowEditor() ) {
                DataFlowEditor()->Graph()->SetObjRef( SUPERV::Graph::_narrow( aNode ) ) ;
	      }
              if ( DataFlowExecutor() ) {
                DataFlowExecutor()->Graph()->SetObjRef( SUPERV::Graph::_narrow( aNode ) ) ;
	      }

            }
          } ;

    virtual CNode_Impl * ObjImpl() {
            CNode_Impl * objimpl = NULL ;
            if ( DataFlowNode() && _IsNode ) {
              objimpl = DataFlowNode()->ObjImpl() ;
	    }
            else {
              objimpl = DataFlowEditor()->Graph()->ObjImpl() ;
            }
            return objimpl ; } ;
    virtual void SetObjImpl( CNode_Impl * objimpl  ) {
            if ( DataFlowNode() && _IsNode ) {
              DataFlowNode()->SetObjImpl( objimpl ) ;
	    }
            else {
              if ( DataFlowEditor() ) {
                DataFlowEditor()->Graph()->SetObjImpl( objimpl ) ;
	      }
              if ( DataFlowExecutor() ) {
                DataFlowExecutor()->Graph()->SetObjImpl( objimpl ) ;
	      }

            }
          } ;

    virtual void Coords( CORBA::Long X , CORBA::Long Y ) ;
    virtual CORBA::Long X() ;
    virtual CORBA::Long Y() ;

    virtual SUPERV::Port_ptr Port( const char * aParameterName ) ;

//    virtual bool BusPorts( const char * InputParameterName ,
//                           const char * InputParameterType ,
//                           const char * OutputParameterName ,
//                           const char * OutputParameterType ,
//                           SUPERV::Port_out InputPort ,
//                           SUPERV::Port_out OutputPort ) ;

//    virtual SUPERV::Link_ptr Link( const char * ToServiceParameterName ,
//                                    const SUPERV::Value_ptr aValue ) ;

    virtual SUPERV::Port_ptr Input( const char * ToServiceParameterName ,
                                    const SUPERV::Value_ptr aValue ) ;
//    virtual bool InputOfAny( const char * ToServiceParameterName ,
//                             const CORBA::Any & aValue ) ;

    virtual SUPERV::Port_ptr GetInPort( const char * aParameterName ) ;
    virtual SUPERV::Port_ptr GetOutPort( const char * aParameterName ) ;

    virtual SUPERV::StreamPort_ptr GetInStreamPort( const char * aParameterName ) ;
    virtual SUPERV::StreamPort_ptr GetOutStreamPort( const char * aParameterName ) ;

    virtual SUPERV::ListOfPorts * Ports() ;
    virtual SUPERV::ListOfStreamPorts * StreamPorts() ;

    virtual bool HasStreamPort() ;

    virtual SUPERV::ListOfLinks * Links() ;
    virtual SUPERV::ListOfStreamLinks * StreamLinks() ;

    virtual SUPERV::Link_ptr GetLink( const char * ToServiceParameterName ) ;
    virtual SUPERV::StreamLink_ptr GetStreamLink( const char * ToServiceParameterName ) ;

    virtual bool IsStreamGraph() ;
    virtual bool IsGraph() ;
    virtual bool IsMacro() ;
    virtual bool IsFlowMacro() ;
    virtual bool IsStreamMacro() ;
    virtual bool IsHeadGraph() ;
    virtual CORBA::Long GraphLevel() ;
    virtual bool IsComputing() ;
    virtual bool IsFactory() ;
    virtual bool IsInLine() ;
    virtual bool IsGOTO() ;
    virtual bool IsLoop() ;
    virtual bool IsEndLoop() ;
    virtual bool IsSwitch() ;
    virtual bool IsEndSwitch() ;

    virtual CORBA::Long SubGraph() ;
    virtual CORBA::Long SubStreamGraph() ;

  // mkr : PAL8060 : this method is not used
  //virtual bool IsLinked(const char * ToServiceParameterName ) ;
    virtual bool HasInput(const char * ToServiceParameterName ) ;

//    virtual SUPERV::Link_ptr GetLink(const char * ToServiceParameterName ) ;
//    virtual SUPERV::Value_ptr GetValue(const char * FromServiceParameterName ) ;

    virtual bool IsReady() ;
    virtual bool IsWaiting() ;
    virtual bool IsRunning() ;
    virtual bool IsDone() ;
    virtual bool IsSuspended() ;

    virtual SUPERV::GraphState State() ;
    virtual SUPERV::ControlState Control() ;
    virtual void ControlClear() ;

    virtual CORBA::Long Thread() ;

    GraphExecutor::AutomatonState AutoState() ;

    virtual bool ReadyW() ;
    virtual bool RunningW() ;
    virtual bool DoneW() ;
    virtual bool SuspendedW() ;

    virtual void ping() ;
    virtual bool ContainerKill() ;

    virtual bool Kill() ;
    virtual bool KillDone() ;
    virtual bool Stop() ;
    virtual bool Suspend() ;
    virtual bool SuspendDone() ;
    virtual bool Resume() ;

    virtual CORBA::Long CpuUsed() ;

    virtual CORBA::Boolean IsExecuting();

} ;

#endif

//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : LNode_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _LNODE_IMPL_HXX_
#define _LNODE_IMPL_HXX_

//#include <iostream.h>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "GNode_Impl.hxx"

#include "Port_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

class LNode_Impl : public GNode_Impl ,
                   public POA_SUPERV::LNode {
  private:

    CORBA::ORB_ptr _Orb ;
    PortableServer::POA_ptr _Poa ;
    PortableServer::ObjectId * _ContId ;

  public:
    LNode_Impl();
    LNode_Impl( CORBA::ORB_ptr orb ,
	       PortableServer::POA_ptr poa ,
	       PortableServer::ObjectId * contId , 
	       const char *instanceName ,
               const char *interfaceName ,
               GraphEditor::DataFlow * DataFlowEditor ,
               GraphBase::ListOfFuncName FuncName ,
               GraphBase::ListOfPythonFunctions PythonFunction ,
//               const char * InitName , 
//               const SUPERV::ListOfStrings & InitFunction ,
//               const char * MoreName , 
//               const SUPERV::ListOfStrings & MoreFunction ,
//               const char * NextName , 
//               const SUPERV::ListOfStrings & NextFunction ,
               const SUPERV::KindOfNode NodeKindOfNode = SUPERV::LoopNode ) ;
    LNode_Impl( CORBA::ORB_ptr orb ,
	       PortableServer::POA_ptr poa ,
	       PortableServer::ObjectId * contId , 
	       const char *instanceName ,
               const char *interfaceName ,
               GraphEditor::DataFlow * DataFlowEditor ,
               GraphEditor::InNode * DataFlowNode ) ;
    virtual ~LNode_Impl() ;
    virtual void destroy() ;
    virtual bool Delete() ;

    virtual void SetPyInit( const char * InitName ,
                            const SUPERV::ListOfStrings & aPyMoreFunction ) ;
    virtual SUPERV::ListOfStrings * PyInit() ;
    virtual char * PyInitName() ;

    virtual void SetPyMore( const char * MoreName ,
                            const SUPERV::ListOfStrings & aPyMoreFunction ) ;
    virtual SUPERV::ListOfStrings * PyMore() ;
    virtual char * PyMoreName() ;

    virtual void SetPyNext( const char * NextName ,
                            const SUPERV::ListOfStrings & aPyNextFunction ) ;
    virtual SUPERV::ListOfStrings * PyNext() ;
    virtual char * PyNextName() ;

    virtual GraphBase::LoopNode * BaseNode() {
            return DataFlowNode()->LoopNode() ; } ;

} ;

#endif

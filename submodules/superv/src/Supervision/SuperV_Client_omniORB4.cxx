// Copyright (C) 2005  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either 
// version 2.1 of the License.
// 
// This library is distributed in the hope that it will be useful 
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public  
// License along with this library; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include <iostream>
#include <CORBA.h>
#include "ObjRef_omniORB4.hh"

#include <stdarg.h>
#include <map>

using namespace std;

using namespace CORBA ;

CORBA::ORB_var _ORB ; 

static void DynInvoke( ObjRef_omniORB4::ObjRef_ptr objComponent ,
		       const char *method , 
		       CORBA::Any * inParams , int nInParams ,
		       CORBA::Any * outParams , int nOutParams ) {

  Request_var req = objComponent->_request( method ) ;
  const char *ArgName ;
  int Type ;
  int i ;

  NVList_ptr arguments = req->arguments() ;

  for ( i = 0 ; i < nInParams ; i++ ) {
    CORBA::Any & data = inParams[i] ;
    ArgName           = "" ;
    Type              = data.type()->kind() ;
    cout << "DynInvoke CORBA::ARG_IN" << i << " Type " << Type << endl ;
    arguments->add_value( ArgName , data , CORBA::ARG_IN ) ;
  }
//Check ObjectReference in ArgInput1
  CORBA::Object_ptr myObjRefFromAny ;
//The folowing line runs with omniORB3 but not with omniORB4 :
//  inParams[0] >>= myObjRefFromAny ;
//The folowing line runs with omniORB4 !!!...
  inParams[0] >>= (CORBA::Any::to_object ) myObjRefFromAny ;
  CORBA::String_var myObjRefIORFromAny ;
  myObjRefIORFromAny = _ORB->object_to_string( myObjRefFromAny ) ;
  if ( CORBA::is_nil( myObjRefFromAny ) ) {
    cout << "The object reference from the Any is a nil reference in DynInvoke "
         << myObjRefIORFromAny << endl ;
  }
  else {
    cout << "The object reference from the Any in DynInvoke is "
         << myObjRefIORFromAny << endl ;
  }


  for ( i = 0 ; i < nOutParams ; i++ ) {
    CORBA::Any & data = outParams[i] ;
    ArgName           = "" ;
    Type              = data.type()->kind() ;
    cout << "DynInvoke CORBA::ARG_IN" << i << " Type " << Type << endl ;
    arguments->add_value( ArgName , data , CORBA::ARG_OUT ) ;
  }

  req->invoke();

  if( req->env()->exception() ) {
    req->env()->exception()->_raise() ;
    return ; // pas utile ?
  }

  for ( i = 0 ; i < nOutParams ; i++ ) {
    outParams[i] = *( arguments->item( i + nInParams )->value() ) ;
  }

  return;

}

int main( int argc , char ** argv ) {

  try {
    if ( argc != 2 ) {
      cout << "Usage : SuperV_Client_omniORB4  IOR" << endl ;
      return 1 ;
    }
    CORBA::ORB_var orb = CORBA::ORB_init( argc , argv , "omniORB4" ) ;
    _ORB = orb ;
    CORBA::Object_var obj = orb->string_to_object( argv[1] ) ;
    ObjRef_omniORB4::ObjRef_var myObjRef = ObjRef_omniORB4::ObjRef::_narrow( obj ) ;
    if ( CORBA::is_nil( myObjRef ) ) {
      cout << "Cannot narrow the object reference or it was a nil reference" << endl ;
      return 1 ;
    }

//ArgInput1 :
    CORBA::Any myAnyObjRef ;
    myAnyObjRef <<= myObjRef ;
//Check ObjectReference in ArgInput1
    CORBA::Object_ptr myObjRefFromAny ;
//The folowing line runs with omniORB3 but not with omniORB4 :
//    myAnyObjRef >>= myObjRefFromAny ;
//The folowing line runs with omniORB4 !!!...
    myAnyObjRef >>= (CORBA::Any::to_object ) myObjRefFromAny ;
    CORBA::String_var myObjRefIORFromAny ;
    myObjRefIORFromAny = orb->object_to_string( myObjRefFromAny ) ;
    if ( CORBA::is_nil( myObjRefFromAny ) ) {
      cout << "The object reference from the Any is a nil reference before DynInvoke "
           << myObjRefIORFromAny << endl ;
    }
    else {
      cout << "The object reference from the Any before DynInvoke is "
           << myObjRefIORFromAny << endl ;
    }

//ArgOutput1 :
    CORBA::Any myAnygetObjRef ;
    myAnygetObjRef <<= ObjRef_omniORB4::ObjRef::_nil() ;

//Dynamic invocation
    DynInvoke( myObjRef , "getObjRef" , &myAnyObjRef , 1 , &myAnygetObjRef , 1 ) ;

//Check ObjectReference in ArgInput1
    if ( CORBA::is_nil( myObjRefFromAny ) ) {
      cout << "The object reference from the Any is a nil reference after DynInvoke "
           << myObjRefIORFromAny << endl ;
    }
    else {
      cout << "The object reference from the Any after DynInvoke is "
           << myObjRefIORFromAny << endl ;
    }

//ArgOutput1 :
    CORBA::Object_ptr mygetObjRef ;
    myAnygetObjRef >>= mygetObjRef ;
    CORBA::String_var mygetObjRefIOR = orb->object_to_string( mygetObjRef ) ;
    if ( CORBA::is_nil( mygetObjRef ) ) {
      cout << "The returned object reference is a nil reference "
           << (char * ) mygetObjRefIOR << endl ;
      return 1 ;
    }
    cout << "The returned object reference is " << mygetObjRefIOR << endl ;
    ObjRef_omniORB4::ObjRef_var mygetObjRefnarrowed ;
    mygetObjRefnarrowed = ObjRef_omniORB4::ObjRef::_narrow( mygetObjRef ) ;
    if ( CORBA::is_nil( mygetObjRef ) ) {
      cout << "The returned narrowed object reference is a nil reference "
           << mygetObjRefIOR << endl ;
      return 1 ;
    }

    mygetObjRefnarrowed->ping() ;

    orb->destroy() ;
  }
  catch(CORBA::COMM_FAILURE & ex ) {
    cout << "Caught CORBA::COMM_FAILURE" << endl ;
  }
  catch(CORBA::SystemException & ) {
    cout << "Caught CORBA::SystemException" << endl ;
  }
  catch(CORBA::Exception & ) {
    cout << "Caught CORBA::Exception" << endl ;
  }
  catch(omniORB::fatalException & fe ) {
    cout << "Caught omniORB::fatalException" << endl ;
    cout << "      file   " << fe.file() << endl ;
    cout << "      line   " << fe.line() << endl ;
    cout << "      errmsg " << fe.errmsg() << endl ;
  }
  catch( ... ) {
    cout << "Caught unknown exception" << endl ;
  }
  return 0 ;
}

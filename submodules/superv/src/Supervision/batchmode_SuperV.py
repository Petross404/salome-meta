#! /usr/bin/env python
#
#  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
#
#  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
# 
#  This library is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU Lesser General Public 
#  License as published by the Free Software Foundation; either 
#  version 2.1 of the License. 
# 
#  This library is distributed in the hope that it will be useful, 
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
#  Lesser General Public License for more details. 
# 
#  You should have received a copy of the GNU Lesser General Public 
#  License along with this library; if not, write to the Free Software 
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
# 
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
#
#  File   : batchmode_SuperV.py
#  Module : SUPERV

try :
    from batchmode_salome import orb
    from batchmode_salome import lcc
    from batchmode_salome import naming_service
    from batchmode_salome import CORBA
    from batchmode_salome import myStudy
    import SALOME_ModuleCatalog
    import SUPERV
    import SUPERV_idl
except :
    import curses
    from LifeCycleCORBA import *
    from SALOME_NamingServicePy import *
    import SUPERV
    import SUPERV_idl
    import SALOME_ModuleCatalog
    from batchmode_salome import *
#    try :
#        from libSuperVision_Swig import SuperV_Swig
#        print 'SuperV_Swig imported'
#    except :
#        SuperV_Swig = None
    orb = CORBA.ORB_init([''], CORBA.ORB_ID)
    #lcc = LifeCycleCORBA(orb)
    while lcc._catalog == None:
	    lcc = LifeCycleCORBA(orb)
    naming_service = SALOME_NamingServicePy_i(orb)

import os
import re
from types import *
import SALOMEDS
import SALOMEDS_Attributes_idl
#try :
#    SuperVision = SuperV_Swig( len(sys.argv) , sys.argv )
#    SuperVision.Swig = 1
#    print 'SuperV_Swig created'
#except :
#    SuperVision = lcc.FindOrLoadComponent("SuperVisionContainer","SUPERV")
#    SuperVision.Swig = 0
#    print 'Supervision Component loaded'
                    
SuperVision = lcc.FindOrLoadComponent("SuperVisionContainer","SUPERV")
#SuperVision.Swig = 0

modulecatalog = None
while modulecatalog == None:
	modulecatalog = naming_service.Resolve("/Kernel/ModulCatalog")

myBuilder = myStudy.NewBuilder()
father = myStudy.FindComponent("SUPERV")
if father is None:
        father = myBuilder.NewComponent("SUPERV")
        A1 = myBuilder.FindOrCreateAttribute(father, "AttributeName");
        FName = A1._narrow(SALOMEDS.AttributeName)
        #FName.SetValue("Supervision")
      	Comp = modulecatalog.GetComponent( "SUPERV" )
	FName.SetValue(Comp._get_componentusername())
	A2 = myBuilder.FindOrCreateAttribute(father, "AttributePixMap");
      	aPixmap = A2._narrow(SALOMEDS.AttributePixMap);
	aPixmap.SetPixMap( "ICON_OBJBROWSER_Supervision" );
	myBuilder.DefineComponentInstance(father,SuperVision)


def PortInputFloat(obj, x):
    return obj.Input( SuperVision.AnyValue( CORBA.Any(CORBA.TC_double, x)))

def PortInputLong(obj, x):
    return obj.Input( SuperVision.AnyValue( CORBA.Any(CORBA.TC_long, x)))

def PortInputObject(obj, x):
    try:
        return obj.Input( SuperVision.AnyValue( CORBA.Any(CORBA.TC_Object, x)))
    except:
        return obj.Input( SuperVision.AnyValue( x ) )

def PortInputString(obj, x):
    return obj.Input( SuperVision.AnyValue( CORBA.Any(CORBA.TC_string, x)))

def InputFloat(obj, nodein_port, x):
    return obj.Input(nodein_port, SuperVision.AnyValue( CORBA.Any(CORBA.TC_double, x)))

def InputLong(obj, nodein_port, x):
    return obj.Input(nodein_port, SuperVision.AnyValue( CORBA.Any(CORBA.TC_long, x)))

def InputObject(obj, nodein_port, x):
    try:
        return obj.Input(nodein_port, SuperVision.AnyValue( CORBA.Any(CORBA.TC_Object, x)))
    except:
        return obj.Input(nodein_port, SuperVision.AnyValue( x ) )

def InputString(obj, nodein_port, x):
        return obj.Input(nodein_port, SuperVision.AnyValue( CORBA.Any(CORBA.TC_string, x)))


def addStudy(ior):
    dataflow = SuperVision.getGraph(ior)
    name=dataflow.Name()
    itr = myStudy.NewChildIterator(father)
    while itr.More():
        item=itr.Value()
        res,A=item.FindAttribute("AttributeName")
        if res:
            aName = A._narrow(SALOMEDS.AttributeName)
            if aName.Value() == name :
                res, A = myBuilder.FindOrCreateAttribute(item, "AttributeIOR")
                anIOR  = A._narrow(SALOMEDS.AttributeIOR);
                anIOR.SetValue(dataflow.getIOR()) 
                return
        itr.Next()
    obj = myBuilder.NewObject(father)
    A=myBuilder.FindOrCreateAttribute(obj, "AttributeName")
    aName=A._narrow(SALOMEDS.AttributeName)
    aName.SetValue(name)
    A=myBuilder.FindOrCreateAttribute(obj, "AttributeIOR")
    anIOR  = A._narrow(SALOMEDS.AttributeIOR)
    anIOR.SetValue(dataflow.getIOR())

typestring = re.compile(r"_[^_]+_([^_]+)_.*")
 
def getRealArgs(self,args):
    types = []
    realargs = []
 
    try:
        t = self.this
        types.append(typestring.match(self).group(1))
        realargs.append(t)
    except:
        types.append(type(self))
        realargs.append(self)
    for a in args:
        try:
            t = a.this
            types.append(typestring.match(a).group(1))
            realargs.append(t)
        except:
            types.append(type(a))
            realargs.append(a)
 
    return tuple(types), tuple(realargs)

bs_AddInput_valuedict = {
     (InstanceType, FloatType) : PortInputFloat,
     (InstanceType, IntType) : PortInputLong,
     (InstanceType, StringType) : PortInputString,
     (InstanceType, InstanceType) : PortInputObject,
     (InstanceType, StringType, FloatType) : InputFloat,
     (InstanceType, StringType, IntType) : InputLong,
     (InstanceType, StringType, StringType) : InputString,
     (InstanceType, StringType, InstanceType) : InputObject,
     (InstanceType, StringType, ListType) : InputObject,
}


def Args( aService , aNodeName , verbose ):
    lenin = len( aService.ServiceinParameter )
    infos = aNodeName + 'info = "'
    infos = infos + 'NodeName    : ' + aNodeName + '\\n'
    infos = infos + 'ServiceName : ' + aService.ServiceName + '\\n'
    infos = infos + 'PortNames   : \\n'
    defnames = aNodeName + " = " + "'" + aNodeName + "' ; "
    i = 0
    while i < lenin :
        argname = aNodeName + aService.ServiceinParameter[ i ].Parametername
        valname = aNodeName + "\\\\"
        valname = valname + aService.ServiceinParameter[ i ].Parametername
        defnames = defnames + argname + " = " + "'" + valname + "' ; "
        infos = infos + str(i) + '. ' + argname + ' : Input'
        infos = infos + str(i) + ' '
        infos = infos + aService.ServiceinParameter[ i ].Parametername + ' '
        infos = infos + aService.ServiceinParameter[ i ].Parametertype + '\\n'
        i = i + 1
    lenout = len( aService.ServiceoutParameter )
    i = 0
    while i < lenout :
        argname = aNodeName +  aService.ServiceoutParameter[ i ].Parametername
        valname = aNodeName + "\\\\"
        valname = valname + aService.ServiceoutParameter[ i ].Parametername
        defnames = defnames + argname + " = " + "'" + valname + "' ; "
        infos = infos + str(lenin+i) + '. ' + argname + ' : Output'
        infos = infos + str(i) + ' '
        infos = infos + aService.ServiceoutParameter[ i ].Parametername + ' '
        infos = infos + aService.ServiceoutParameter[ i ].Parametertype + '\\n'
        i = i + 1
    infos = infos + '"'
    defnames = defnames + infos
    if verbose :
        defnames = defnames + " ; print " + aNodeName + "info"
    return defnames

def Service_Swig( aCatalogService ) :
    aService = SuperVision.Service()
    aService.ServiceName = aCatalogService.ServiceName
    i = 0
    while i < len( aCatalogService.ServiceinParameter ) :
        p = SuperVision.ServicesParameter( aCatalogService.ServiceinParameter[i].Parametertype , aCatalogService.ServiceinParameter[i].Parametername )
        SuperVision.AddInParameter( aService , p )
        i = i + 1
    i = 0
    while i < len( aCatalogService.ServiceoutParameter ) :
        p = SuperVision.ServicesParameter( aCatalogService.ServiceoutParameter[i].Parametertype , aCatalogService.ServiceoutParameter[i].Parametername )
        SuperVision.AddOutParameter( aService , p )
        i = i + 1
    return aService

##########################################################
class CNode:
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose
    def Name( self ):
        return self.N.Name()
    def Service( self ):
        return self.N.Service()
    def Kind( self ):
        return self.N.Kind()
    def CreationDate( self ):
        return self.N.CreationDate()
    def LastUpdateDate( self ):
        return self.N.LastUpdateDate()
    def Version( self ):
        return self.N.Version()
    def Author( self ):
        return self.N.Author()
    def Comment( self ):
        return self.N.Comment()

    def SetName( self , aNewName ):
        return self.N.SetName( aNewName )
    def SetAuthor( self , anAuthor ):
        return self.N.SetAuthor( anAuthor )
    def SetComment( self , aComment ):
        return self.N.SetComment( aComment )

    def Print( self ):
        print "Name          ",self.N.Name()
        if self.N.IsFactory() :
            print "ComponentName ",self.N.GetComponentName()
            print "InterfaceName ",self.N.GetInterfaceName()
        if self.N.IsStreamGraph() :
            Timeout,DataStreamTrace,DeltaTime = self.G.StreamParams()
            print "Timeout",Timeout
            print "DataStreamTrace",DataStreamTrace
            print "DeltaTime",DeltaTime
        aService = self.N.Service()
        print "Service Name  ",aService.ServiceName
        lenin = len( aService.ServiceinParameter )
        print "   ",lenin," input argument(s) :"
        i = 0
        while i < lenin :
            print "      ",i,". ",aService.ServiceinParameter[ i ].Parametername," ",aService.ServiceinParameter[ i ].Parametertype
            i = i + 1
        lenout = len( aService.ServiceoutParameter )
        print "   ",lenout," output argument(s) :"
        i = 0
        while i < lenout :
            print "      ",i,". ",aService.ServiceoutParameter[ i ].Parametername," ",aService.ServiceoutParameter[ i ].Parametertype
            i = i + 1
        aKind = self.N.Kind()
        print "KindOfNode",aKind
        aFirstCreation = self.N.CreationDate()
        print "Creation Date ",aFirstCreation.Day,"-",aFirstCreation.Month,"-",aFirstCreation.Year," ",aFirstCreation.Hour,":",aFirstCreation.Minute,":",aFirstCreation.Second
        aLastModification = self.N.LastUpdateDate()
        print "Update Date   ",aLastModification.Day,"-",aLastModification.Month,"-",aLastModification.Year," ",aLastModification.Hour,":",aLastModification.Minute,":",aLastModification.Second
        print "Version       ",self.N.Version()
        print "Author        ",self.N.Author()
        if self.N.IsFactory() :
            print "Container     ",self.N.GetContainer()
        print "Comment       ",self.N.Comment()
        print "Coords        ",self.N.X()," ",self.N.Y()
    def Port( self , aPortName ):
        aPort = self.N.Port( aPortName )
        if aPort != None :
            myPort = Port( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aPortName
        return None
    def Input( self , *args ):
        types, realargs = getRealArgs(self.N,args)
        fn = bs_AddInput_valuedict[types]
        anInput = apply( fn, realargs)
        if anInput != None :
            myInput = Port( anInput , self.N.Verbose )
            return myInput
        ErrMsg = "Failed to create the Input"
        if self.N.Verbose :
            print ErrMsg
        return anInput
#    def BusPort( self , InOutParameterName , InOutParameterType ):
#        sts,inp,outp = self.N.BusPort( InOutParameterName ,
#                                       InOutParameterType )
#        inp = Port( inp , self.N.Verbose )
#        outp = Port( outp , self.N.Verbose )
#        return sts,inp,outp
#    def BusPorts( self , InputParameterName , InputParameterType ,
#                  OutputParameterName , OutputParameterType ):
#        sts,inp,outp = self.N.BusPorts( InputParameterName ,
#                                        InputParameterType ,
#                                        OutputParameterName ,
#                                        OutputParameterType )
#        inp = Port( inp , self.N.Verbose )
#        outp = Port( outp , self.N.Verbose )
#        return sts,inp,outp
    def InStreamPort( self , aParameterName , aParameterType , aDependency ):
        aPort = self.N.InStreamPort( aParameterName , aParameterType , aDependency )
        if aPort != None :
            myPort = StreamPort( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def OutStreamPort( self , aParameterName , aParameterType , aDependency ):
        aPort = self.N.OutStreamPort( aParameterName , aParameterType , aDependency )
        if aPort != None :
            myPort = StreamPort( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def GetInPort( self , aParameterName ):
        aPort = self.N.GetInPort( aParameterName )
        if aPort != None :
            myPort = Port( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def GetOutPort( self , aParameterName ):
        aPort = self.N.GetOutPort( aParameterName )
        if aPort != None :
            myPort = Port( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def GetInStreamPort( self , aParameterName ):
        aPort = self.N.GetInStreamPort( aParameterName )
        if aPort != None :
            myPort = StreamPort( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def GetOutStreamPort( self , aParameterName ):
        aPort = self.N.GetOutStreamPort( aParameterName )
        if aPort != None :
            myPort = StreamPort( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def Ports( self ):
        ports = self.N.Ports()
        i = 0
        while i < len( ports ) :
            ports[ i ] = Port( ports[i] , self.N.Verbose )
            i = i + 1
        return ports
    def StreamPorts( self ):
        ports = self.N.StreamPorts()
        i = 0
        while i < len( ports ) :
            ports[ i ] = StreamPort( ports[i] , self.N.Verbose )
            i = i + 1
        return ports
    def AllPorts( self ):
        allports = self.N.Ports()
        lenports = len( allports )
        i = 0
        while i < lenports :
            allports[ i ] = Port( allports[i] , self.N.Verbose )
            i = i + 1
        ports = self.N.StreamPorts()
        i = 0
        while i < len( ports ) :
            allports.append( StreamPort( ports[i] , self.N.Verbose ) )
            i = i + 1
        return allports
    def ListPorts( self , *args ):
        if len( args ) == 0 :
            aName = self.N.Name()
        else :
            aName = args[ 0 ]
        ports = self.AllPorts()
        listofports = ""
        ilst = 0
        if len( ports ) :
            portsname = ports[ 0 ].Name()
            bs = portsname.find('\\')
            if bs != -1 :
                portsname1,portsname2 = portsname.split('\\')
                portsname = portsname1 + portsname2
            if ports[ 0 ].IsInput():
                listofports = 'I' + aName + portsname
            else :
                listofports = 'O' + aName + portsname
            i = 1
            while i < len( ports ) :
                portsname = ports[ i ].Name()
                bs = portsname.find('\\')
                if bs != -1 :
                    portsname1,portsname2 = portsname.split('\\')
                    portsname = portsname1 + portsname2
                if ports[ i ].IsInput():
                    listofports = listofports + ',' + 'I' + aName + portsname
                else :
                    listofports = listofports + ',' + 'O' + aName + portsname
                i = i + 1
        print listofports
        if len( ports ) == 1 :
            listofports = "[" + listofports + "] = " + aName + ".AllPorts()"
        else :
            listofports = listofports + " = " + aName + ".AllPorts()"
        return listofports
    def PrintPorts( self ):
        ports = self.AllPorts()
        i = 0
        while i < len( ports ) :
            Direction = "Out"
            if ports[ i ].IsInput() :
                Direction = "In"
            if ( ports[ i ].IsDataStream() ) & ( self.N.IsStreamGraph() == 0 ) :
                if ports[ i ].IsInput() :
                    sts,aKindOfSchema,aKindOfInterpolation,aKindOfExtrapolation = ports[ i ].Params()
                    print Direction,self.N.Name(),'(',ports[ i ].Name(),ports[ i ].Kind(),ports[ i ].Type(),') = ',ports[ i ].ToString(),ports[ i ].Dependency(),aKindOfSchema,aKindOfInterpolation,aKindOfExtrapolation
                else :
                    numberofvalues = ports[ i ].NumberOfValues()
                    print Direction,self.N.Name(),'(',ports[ i ].Name(),ports[ i ].Kind(),ports[ i ].Type(),') = ',ports[ i ].ToString(),ports[ i ].Dependency(),'NumberOfValues',numberofvalues
            else :
                print Direction,self.N.Name(),'(',ports[ i ].Name(),ports[ i ].Kind(),ports[ i ].Type(),') = ',ports[ i ].ToString()
            i = i + 1
    def Links( self ) :
        links = self.N.Links()
        i = 0
        while i < len( links ) :
            links[ i ] = Link( links[i] , self.N.Verbose )
            i = i + 1
        return links
    def StreamLinks( self ) :
        links = self.N.StreamLinks()
        i = 0
        while i < len( links ) :
            links[ i ] = StreamLink( links[i] , self.N.Verbose )
            i = i + 1
        return links
    def AllLinks( self ) :
        alllinks = self.N.Links()
        lenlinks = len( alllinks )
        i = 0
        while i < lenlinks :
            alllinks[ i ] = Link( alllinks[i] , self.N.Verbose )
            i = i + 1
        links = self.N.StreamLinks()
        i = 0
        while i < len( links ) :
            alllinks.append( StreamLink( links[i] , self.N.Verbose ) )
            i = i + 1
        return alllinks
    def ListLinks( self , *args ):
        links = self.AllLinks()
        if len( links ) :
            listoflinks = 'L' + links[ 0 ].OutPort().Node().Name()
            listoflinks = listoflinks + links[ 0 ].OutPort().Name()
            listoflinks = listoflinks + links[ 0 ].InPort().Node().Name()
            listoflinks = listoflinks + links[ 0 ].InPort().Name()
            i = 1
            while i < len( links ) :
                listoflinks = listoflinks + ',' + 'L' + links[ i ].OutPort().Node().Name()
                listoflinks = listoflinks + links[ i ].OutPort().Name()
                listoflinks = listoflinks + links[ i ].InPort().Node().Name()
                listoflinks = listoflinks + links[ i ].InPort().Name()
                i = i + 1
        print listoflinks
        if len( args ) == 0 :
            aName = self.N.Name()
        else :
            aName = args[ 0 ]
        if len( links ) == 1 :
            listoflinks = "[" + listoflinks + "] = " + aName + ".AllLinks()"
        else :
            listoflinks = listoflinks + " = " + aName + ".AllLinks()"
        return listoflinks
    def PrintLinks( self ):
        links = self.AllLinks()
        i = 0
        while i < len( links ) :
            links[i].Print()
            i = i + 1
    def IsStreamGraph( self ):
        return self.N.IsStreamGraph()
    def IsGraph( self ):
        return self.N.IsGraph()
    def IsMacro( self ):
        return self.N.IsMacro()
    def IsFlowMacro( self ):
        return self.N.IsFlowMacro()
    def IsStreamMacro( self ):
        return self.N.IsStreamMacro()
    def IsComputing( self ):
        return self.N.IsComputing()
    def IsFactory( self ):
        return self.N.IsFactory()
    def IsInLine( self ):
        return self.N.IsInLine()
    def IsGOTO( self ):
        return self.N.IsGOTO()
    def IsLoop( self ):
        return self.N.IsLoop()
    def IsEndLoop( self ):
        return self.N.IsEndLoop()
    def IsSwitch( self ):
        return self.N.IsSwitch()
    def IsEndSwitch( self ):
        return self.N.IsEndSwitch()
    def GraphLevel( self ) :
        return self.N.GraphLevel()
    def SubGraph( self ):
        return self.N.SubGraph()
    def SubStreamGraph( self ):
        return self.N.SubStreamGraph()
    def Thread( self ):
        return self.N.Thread()
    def IsWaiting( self ):
        return self.N.IsWaiting()
    def IsRunning( self ):
        return self.N.IsRunning()
    def IsDone( self ):
        return self.N.IsDone()
    def IsSuspended( self ):
        return self.N.IsSuspended()
    def State( self ):
        return self.N.State()
    def Control( self ):
        return self.N.Control()
    def ControlClear( self ):
        return self.N.ControlClear()
    def ReadyW( self ):
        return self.N.ReadyW()
    def RunningW( self ):
        return self.N.RunningW()
    def DoneW( self ):
        return self.N.DoneW()
    def SuspendedW( self ):
        return self.N.SuspendedW()
    #def ReRun( self ):
    #    return self.N.ReRun()
    #def ReStart( self ):
    #    return self.N.ReStart()
    #def ReRunAt( self , aNodeName ):
    #    return self.N.ReRunAt( aNodeName )
    #def ReStartAt( self , aNodeName ):
    #    return self.N.ReStartAt( aNodeName )
    def Ping( self ):
        return self.N.ping()
    def Kill( self ):
        return self.N.Kill()
    def KillDone( self ):
        return self.N.KillDone()
    def Suspend( self ):
        return self.N.Suspend()
    def SuspendDone( self ):
        return self.N.SuspendDone()
    def Resume( self ):
        return self.N.Resume()
    def Stop( self ):
        return self.N.Stop()
    def Coords( self , X , Y ):
        return self.N.Coords( X , Y )
    def X( self ):
        return self.N.X()
    def Y( self ):
        return self.N.Y()
    def destroy( self ):
        self.N.destroy()
    def CpuUsed( self ):
        return self.N.CpuUsed()

##########################################################
class FNode(CNode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose
    def GetComponentName( self ):
        return self.N.GetComponentName()
    def GetInterfaceName( self ):
        return self.N._get_interfaceName()
    def GetContainer( self ):
        return self.N.GetContainer()
    def SetComponentName( self , aComponentName ):
        return self.N.SetComponentName( aComponentName )
    def SetInterfaceName( self , anInterfaceName ):
        return self.N.SetInterfaceName( anInterfaceName )
    def SetContainer( self , aComputer ):
        return self.N.SetContainer( aComputer )
    def IsCimpl( self ):
        return self.N.isCimpl

##########################################################
class INode(CNode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose
    def Edit( self , FuncName , PyFunc ) :
        import os
        import random
        suf = str(random.randrange(1,10000))
        file = '/tmp/' + FuncName + '_' + suf + '.py' 
        try :
            #d = dir ()
            #print "dir()",d
            fd = os.open( file , os.O_CREAT | os.O_WRONLY | os.O_TRUNC )
            #d = dir (fd)
            #print "dir(fd)",d
            i = 0
            while ( i < len( PyFunc ) ) :
                print i,'/',len( PyFunc ),PyFunc[ i ]
                os.write( fd , PyFunc[ i ] + '\n' )
                i = i + 1
            os.close( fd )
            edit = '${EDITOR} ' + file
            os.system( edit )
            fd = os.open( file , os.O_RDONLY )
            line = os.read( fd , 132 )
            res = ''
            while len( line ) :
                res = res + line
                line = os.read( fd , 132 )
            os.close( fd )
            PyFunc = res.splitlines()
            i = 0
            while i < len( PyFunc ) :
                print PyFunc[i]
                i = i + 1
        except :
            print "Open of",file,"failed."
        return PyFunc
    def SetPyFunction( self , FuncName , aPyFunction ):
        return self.N.SetPyFunction( FuncName , aPyFunction )
    def PyFunction( self ):
        return self.N.PyFunction()
    def PyFuncName( self ):
        return self.N.PyFuncName()
    def InPort( self , aParameterName , aParameterType ):
        aPort = self.N.InPort( aParameterName ,
                             aParameterType )
        if aPort != None :
            myPort = Port( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def OutPort( self , aParameterName , aParameterType ):
        aPort = self.N.OutPort( aParameterName ,
                                aParameterType )
        if aPort != None :
            myPort = Port( aPort , self.N.Verbose )
            return myPort
        if self.N.Verbose :
            print "Error while creating the Port : ",aParameterName
        return None
    def EPyFunc( self ):
        PyFunc = self.N.PyFunction()
        PyFunc = self.Edit( self.N.PyFuncName() , PyFunc )
        self.N.SetPyFunction( self.N.PyFuncName() , PyFunc )

##########################################################
class GNode(INode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose
    def SetCoupled( self , anInLineNode ):
        if self.N.IsGOTO() :
            node = self.N.SetCoupled( anInLineNode )
        else :
            node = None
        return node
    def Coupled( self ):
        node = self.N.Coupled()
        if node != None :
            if node.IsInLine() :
                node = INode( node , self.N.Verbose )
            elif node.IsLoop() :
                node = LNode( node , self.N.Verbose )
            elif node.IsEndLoop() :
                node = ELNode( node , self.N.Verbose )
            elif node.IsSwitch() :
                node = SNode( node , self.N.Verbose )
            elif node.IsEndSwitch() :
                node = ESNode( node , self.N.Verbose )
        return node

##########################################################
class LNode(GNode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose
    def SetPyInit( self , InitName , aPyFunction ):
        return self.N.SetPyInit( InitName , aPyFunction )
    def PyInit( self ):
        return self.N.PyInit()
    def PyInitName( self ) :
        return self.N.PyInitName()
    def EPyInit( self ):
        PyFunc = self.N.PyFunction()
        PyFunc = self.Edit( self.N.PyFuncName() , PyFunc )
        self.N.SetPyFunction( self.N.PyFuncName() , PyFunc )
    def SetPyMore( self , MoreName , aPyFunction ):
        return self.N.SetPyMore( MoreName , aPyFunction )
    def PyMore( self ):
        return self.N.PyMore()
    def PyMoreName( self ) :
        return self.N.PyMoreName()
    def EPyMore( self ):
        PyMore = self.N.PyMore()
        PyMore = self.Edit( self.N.PyMoreName() , PyMore )
        self.N.SetPyMore( self.N.PyMoreName() , PyMore )
    def SetPyNext( self , NextName , aPyFunction ):
        return self.N.SetPyNext( NextName , aPyFunction )
    def PyNext( self ):
        return self.N.PyNext()
    def PyNextName( self ) :
        return self.N.PyNextName()
    def EPyNext( self ):
        PyNext = self.N.PyNext()
        PyNext = self.Edit( self.N.PyNextName() , PyNext )
        self.N.SetPyNext( self.N.PyNextName() , PyNext )

##########################################################
class ELNode(GNode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose

##########################################################
class SNode(GNode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose

##########################################################
class ESNode(GNode):
##########################################################
    def __init__( self , aNode , Verbose ):
        self.N = aNode
        self.N.Verbose = Verbose

##########################################################
#class ServicesParameter_Swig :
##########################################################
#    def __init__( self , aServicesParameter ):
#        self.Parametertype = aServicesParameter.Parametertype
#        self.Parametername = aServicesParameter.Parametername

##########################################################
class Graph(GNode):
##########################################################
    def __init__( self , aName ):
        try:
            graph = SuperVision.Graph( aName )
        except:
            try:
                graph = aName.G.Copy()
            except:
                graph = aName
        if graph != None :
            if graph.IsStreamGraph() :
                aGraph = graph.ToStreamGraph()
                if aGraph != None :
                    graph = StreamGraph( aGraph )
                else :
                    print "Graph creation failed"
            self.G = graph
            self.G.Verbose = 1
            self.N = graph
            self.N.Verbose = 1
        else :
            print "Graph creation failed"
    def CNode( self , *args ):
        if len( args ) == 1 :
            aService = args[ 0 ]
        else :
            aComponent = args[ 0 ]
            anInterface = args[ 1 ]
            aService = args[ 2 ]
            NodeComponent = modulecatalog.GetComponent( aComponent )
            aService = NodeComponent.GetService( anInterface , aService )
        aNode = self.G.CNode( aService )
        if aNode != None :
            myNode = CNode( aNode , self.G.Verbose )
            return myNode
        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
        if self.G.Verbose :
            print ErrMsg
        return aNode
    def Node( self , aComponent , anInterface , aService ):
        #if SuperVision.Swig :
            #aService = Service_Swig( aService )
        return self.FNode( aComponent , anInterface , aService )
    def FNodeImpl( self , aComponent , anInterface , aService, isCimpl ):
        NodeComponent = modulecatalog.GetComponent( aComponent )
        aService = NodeComponent.GetService( anInterface , aService )
        aNode = self.G.FNode( aComponent , anInterface , aService, isCimpl )
        if aNode != None :
            aNode.isCimpl = isCimpl
            myNode = FNode( aNode , self.G.Verbose )
            return myNode
        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
        if self.G.Verbose :
            print ErrMsg
        return aNode
    def FNode( self , aComponent , anInterface , aService ):
        # create node with C++ implementation type by default
        NodeComponent = modulecatalog.GetComponent( aComponent )
        aService = NodeComponent.GetService( anInterface , aService )
        #if SuperVision.Swig :
            #aService = Service_Swig( aService )
        aNode = self.G.FNode( aComponent , anInterface , aService, SALOME_ModuleCatalog.SO )
        if aNode != None :
            aNode.isCimpl = 1
            myNode = FNode( aNode , self.G.Verbose )
            return myNode
        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
        if self.G.Verbose :
            print ErrMsg
        return aNode
    def INode( self , FuncName , aPythonFunction ):
        aNode = self.G.INode( FuncName , aPythonFunction )
        if aNode != None :
            myNode = INode( aNode , self.G.Verbose )
            return myNode
        ErrMsg = "Failed to create a Node"
        if self.G.Verbose :
            print ErrMsg
            return aNode
    def GNode( self , FuncName , aPythonFunction , anINode ):
        aNode = self.G.GNode( FuncName , aPythonFunction , anINode )
        if aNode != None :
            myNode = GNode( aNode , self.G.Verbose )
            return myNode
        ErrMsg = "Failed to create a Node"
        if self.G.Verbose :
            print ErrMsg
            return aNode
    def LNode( self , InitName , InitFunction , MoreName , MoreFunction , NextName , NextFunction ):
        aNode,anEndOfLoop = self.G.LNode( InitName , InitFunction , MoreName , MoreFunction , NextName , NextFunction )
        if aNode != None :
            myNode = LNode( aNode , self.G.Verbose )
            myEndOfLoop = INode( anEndOfLoop , self.G.Verbose )
            return myNode,myEndOfLoop
        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
        if self.G.Verbose :
            print ErrMsg
        return aNode
    def SNode( self , FuncName , aPythonFunction ):
        aNode,anEndOfSwitch = self.G.SNode( FuncName , aPythonFunction )
        if aNode != None :
            myNode = SNode( aNode , self.G.Verbose )
            myEndOfSwitch = INode( anEndOfSwitch , self.G.Verbose )
            return myNode,myEndOfSwitch
        ErrMsg = "Failed to create a Node"
        if self.G.Verbose :
            print ErrMsg
        return aNode
#    def LoopNode( self , aComponent , anInterface , aService , aNodeName ):
#        NodeComponent = modulecatalog.GetComponent( aComponent )
#        aService = NodeComponent.GetService( anInterface , aService )
#        aNode,anEndNode = self.G.LoopNode( aComponent , anInterface , aService , aNodeName )
#        myNode = aNode
#        myEndNode = anEndNode
#        if aNode != None :
#            myNode = Node( aNode , self.G.Verbose )
#        if anEndNode != None :
#            myEndNode = Node( anEndNode , self.G.Verbose )
#        if ( aNode != None ) & ( anEndNode != None ) :
#            return myNode,myEndNode
#        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
#        if self.G.Verbose :
#            print ErrMsg
#        return aNode,anEndNode
#    def SwitchNode( self , aComponent , anInterface , aService , aNodeName ):
#        NodeComponent = modulecatalog.GetComponent( aComponent )
#        aService = NodeComponent.GetService( anInterface , aService )
#        aNode,anEndNode = self.G.SwitchNode( aComponent , anInterface , aService , aNodeName )
#        myNode = aNode
#        myEndNode = anEndNode
#        if aNode != None :
#            myNode = Node( aNode , self.G.Verbose )
#        if anEndNode != None :
#            myEndNode = Node( anEndNode , self.G.Verbose )
#        if ( aNode != None ) & ( anEndNode != None ) :
#            return myNode,myEndNode
#        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
#        if self.G.Verbose :
#            print ErrMsg
#        return aNode,anEndNode
#    def LNode( self , aNodeName ):
#        aNode = self.G.LNode( aNodeName )
#        if aNode != None :
#            myNode = Node( aNode , self.G.Verbose )
#            return myNode
#        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
#        if self.G.Verbose :
#            print ErrMsg
#        return aNode
#    def LService( self , aComponent , anInterface , aService ):
#        NodeComponent = modulecatalog.GetComponent( aComponent )
#        aService = NodeComponent.GetService( anInterface , aService )
#        aNode = self.G.LService( aComponent , anInterface , aService )
#        if aNode != None :
#            myNode = Node( aNode , self.G.Verbose )
#            return myNode
#        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
#        if self.G.Verbose :
#            print ErrMsg
#        return aNode
#    def CNode( self , aNodeName ):
#        aNode = self.G.CNode( aNodeName )
#        if aNode != None :
#            myNode = Node( aNode , self.G.Verbose )
#            return myNode
#        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
#        if self.G.Verbose :
#            print ErrMsg
#        return aNode
#    def CService( self , aComponent , anInterface , aService ):
#        NodeComponent = modulecatalog.GetComponent( aComponent )
#        aService = NodeComponent.GetService( anInterface , aService )
#        aNode = self.G.CService( aComponent , anInterface , aService )
#        if aNode != None :
#            myNode = Node( aNode , self.G.Verbose )
#            return myNode
#        ErrMsg = "Failed to create a Node with Service " + aService.ServiceName
#        if self.G.Verbose :
#            print ErrMsg
#        return aNode
    def MNode( self , aGraphXml ):
        aMNode = self.G.MNode( aGraphXml )
        if aMNode != None :
            myMNode = MNode( aMNode , self.G.Verbose )
            return myMNode
        ErrMsg = "Failed to create a MNode"
        if self.G.Verbose :
            print ErrMsg
        return aMNode
    def GraphMNode( self , aGraph ):
        aGraphMNode = self.G.GraphMNode( aGraph.G )
        if aGraphMNode != None :
            myMNode = MNode( aGraphMNode , self.G.Verbose )
            return myMNode
        ErrMsg = "Failed to create a MNode"
        if self.G.Verbose :
            print ErrMsg
        return aGraphMNode
    def FlowObjRef( self ) :
        aGraph = self.G.FlowObjRef()
        if aGraph != None :
            myGraph = Graph( aGraph )
            return myGraph
        return aGraph
    def StreamObjRef( self ) :
        aGraph = self.G.StreamObjRef()
        if aGraph != None :
            myGraph = StreamGraph( aGraph )
            return myGraph
        return aGraph
    def PrintService( self , aComponent , anInterface , aService ):
        NodeComponent = modulecatalog.GetComponent( aComponent )
        aService = NodeComponent.GetService( anInterface , aService )
        print "ServiceName ",aService.ServiceName," :"
        lenin = len( aService.ServiceinParameter )
        print "   ",lenin," input argument(s) :"
        i = 0
        while i < lenin :
            print "      ",i,". ",aService.ServiceinParameter[ i ].Parametername," ",aService.ServiceinParameter[ i ].Parametertype
            i = i + 1
        lenout = len( aService.ServiceoutParameter )
        print "   ",lenout," output argument(s) :"
        i = 0
        while i < lenout :
            print "      ",i,". ",aService.ServiceoutParameter[ i ].Parametername," ",aService.ServiceoutParameter[ i ].Parametertype
            i = i + 1
    def Link( self , aFromNodePort , aToNodePort ):
        aLink = self.G.Link( aFromNodePort.P , aToNodePort.P )
        if aLink != None :
            myLink = Link( aLink , self.G.Verbose )
            return myLink
        ErrMsg = "Failed to create a Link from " + aFromNodePort.Node().Name() + "(" + aFromNodePort.Name() + ") to " + aToNodePort.Node().Name() + "(" + aToNodePort.Name() + ")"
        if self.G.Verbose :
            print ErrMsg
        return aLink
    def Messages( self ):
        return self.G.Messages()
    def Import( self , anXmlFileName ):
        return self.G.Import( anXmlFileName )
    def Export( self , anXmlFileName ):
        return self.G.Export( anXmlFileName )
    def IsReadOnly( self ) :
        return self.G.IsReadOnly()
    def ComponentRef( self , aFactoryServer , aComponent ) :
        return self.G.ComponentRef( aFactoryServer , aComponent )
    def IsValid( self ):
        return self.G.IsValid()
    def IsExecutable( self ):
        return self.G.IsExecutable()
    def IsEditing( self ):
        return self.G.IsEditing()
    def IsExecuting( self ):
        return self.G.IsExecuting()
    def LevelMax( self ):
        return self.G.LevelMax()
    def ThreadsMax( self ):
        return self.G.ThreadsMax()
    def SubGraphsNumber( self ):
        return self.G.SubGraphsNumber()
    def LevelNodes( self , aLevel ):
        nodes = self.G.LevelNodes( aLevel )
        return self.nodesTuple( nodes )
    def Run( self , *args ):
        self.S = -1
        aService = self.Service()
        nargs = len( args )
        i = 0
        while nargs > 0 :
            print aService.ServiceinParameter[ i ].Parametername," = ",args[i]
            self.Input( aService.ServiceinParameter[ i ].Parametername , args[i] )
            i = i + 1
            nargs = nargs - 1
        return self.G.Run()
    def Start( self , *args ):
        self.S = -1
        aService = self.Service()
        nargs = len( args )
        i = 0
        while nargs > 0 :
            print aService.ServiceinParameter[ i ].Parametername," = ",args[i]
            self.Input( aService.ServiceinParameter[ i ].Parametername , args[i] )
            i = i + 1
            nargs = nargs - 1
        return self.G.Start()
    def Event( self ):
        sts,aNode,anEvent,aState = self.G.Event()
        self.S = sts
        return sts,aNode,anEvent,aState
    def EventNoW( self ):
        sts,aNode,anEvent,aState = self.G.EventNoW()
        self.S = sts
        return sts,aNode,anEvent,aState
    def EventW( self ):
        sts,aNode,anEvent,aState = self.G.EventW()
        self.S = sts
        return sts,aNode,anEvent,aState
    def NextW( self ):
        if self.S != 0 :
            sts,aNode,anEvent,aState = self.G.EventW()
            self.S = sts
            print sts,aNode.Thread(),aNode.SubGraph(),aNode.Name(),anEvent,aState
        return sts
    def Threads( self ):
        return self.G.Threads()
    def SuspendedThreads( self ):
        return self.G.SuspendedThreads()
    def LastLevelDone( self ):
        return self.G.LastLevelDone()
    def Verbose( self , verbose ):
        preverbose = self.G.Verbose
        self.G.Verbose = verbose
        return preverbose
    def Nodes( self ):
        nodes = self.G.Nodes()
        return self.nodesTuple( nodes )
    def nodesTuple( self , nodes ) :
        n = len( nodes.CNodes )
        i = 0
        j = 0
        pynodes = []
        while i < n :
            pynodes.append( CNode( nodes.CNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.FNodes )
        i = 0
        while i < n :
            pynodes.append( FNode( nodes.FNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.INodes )
        i = 0
        while i < n :
            pynodes.append( INode( nodes.INodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.GNodes )
        i = 0
        while i < n :
            pynodes.append( GNode( nodes.GNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.LNodes )
        i = 0
        while i < n :
            pynodes.append( LNode( nodes.LNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.ELNodes )
        i = 0
        while i < n :
            pynodes.append( GNode( nodes.ELNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.SNodes )
        i = 0
        while i < n :
            pynodes.append( SNode( nodes.SNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.ESNodes )
        i = 0
        while i < n :
            pynodes.append( GNode( nodes.ESNodes[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        n = len( nodes.Graphs )
        i = 0
        while i < n :
            pynodes.append( MNode( nodes.Graphs[i] , self.G.Verbose ) )
            i = i + 1
            j = j + 1
        return pynodes
    def ListNodes( self , *args ):
        nodes = self.Nodes()
        listofnodes = ""
        if len( nodes ) :
            listofnodes = nodes[ 0 ].Name()
            i = 1
            while i < len( nodes ) :
                listofnodes = listofnodes + ',' + nodes[ i ].Name()
                i = i + 1
        print listofnodes
        if len( args ) == 0 :
            aName = self.N.Name()
        else :
            aName = args[ 0 ]
        if len( nodes ) == 1 :
            listofnodes = "[" + listofnodes + "] = " + aName + ".Nodes()"
        else :
            listofnodes = listofnodes + " = " + aName + ".Nodes()"
        return listofnodes
    def PrintNodes( self ):
        nodes = self.Nodes()
        n = len( nodes )
        i = 0
        while i < n :
            print ' '
            nodes[ i ].Print()
            i = i + 1
    def PrintThreads( self ):
        nodes = self.Nodes()
        i = 0
        while i < len( nodes ) :
            print nodes[i].Name(),nodes[i].Thread(),nodes[i].State(),nodes[i].Control(),nodes[i]
            i = i + 1
    def GLinks( self ):
        links = self.G.GLinks()
        i = 0
        linkslen = len( links )
        #print 'GLinks ',linkslen,'Links'
        while i < linkslen :
            links[ i ] = Link( links[i] , self.G.Verbose )
            i = i + 1
    def GStreamLinks( self ):
        streamlinks = self.G.GStreamLinks()
        i = 0 
        #print 'GStreamLinks ',len( streamlinks ),'StreamLinks'
        while i < len( streamlinks ) :
            streamlinks[ i ] = StreamLink( streamlinks[i] , self.G.Verbose )
            i = i + 1
        return streamlinks
    def GAllLinks( self ) :
        alllinks = self.G.GLinks()
        lenlinks = len( alllinks )
        i = 0
        while i < lenlinks :
            alllinks[ i ] = Link( alllinks[i] , self.N.Verbose )
            i = i + 1
        links = self.G.StreamLinks()
        j = 0
        while j < len(links) :
            alllinks.append( StreamLink( links[j] , self.N.Verbose ) )
            i = i + 1
            j = j + 1
        return alllinks
    def ListLinks( self , *args ):
        links = self.GAllLinks()
        if len( links ) :
            listoflinks = 'L' + links[ 0 ].OutPort().Node().Name()
            listoflinks = listoflinks + links[ 0 ].OutPort().Name()
            listoflinks = listoflinks + links[ 0 ].InPort().Node().Name()
            listoflinks = listoflinks + links[ 0 ].InPort().Name()
            i = 1
            while i < len( links ) :
                listoflinks = listoflinks + ',' + 'L' + links[ i ].OutPort().Node().Name()
                listoflinks = listoflinks + links[ i ].OutPort().Name()
                listoflinks = listoflinks + links[ i ].InPort().Node().Name()
                listoflinks = listoflinks + links[ i ].InPort().Name()
                i = i + 1
        print listoflinks
        if len( args ) == 0 :
            aName = self.N.Name()
        else :
            aName = args[ 0 ]
        if len( links ) == 1 :
            listoflinks = "[" + listoflinks + "] = " + aName + ".GAllLinks()"
        else :
            listoflinks = listoflinks + " = " + aName + ".GAllLinks()"
        return listoflinks
    def PrintLinks( self ):
        links = self.GAllLinks()
        i = 0
        while i < len( links ) :
            links[ i ].Print()
            i = i + 1
    def SubGraphsNodes( self , ):
        graphs = self.G.SubGraphsNodes()
        outgraphs = graphs
        i = 0
        while i < len( graphs ) :
            outgraphs[ i ] = Graph( graphs[i].Name() , self.G.Verbose )
            outgraphs[ i ].Merge( graphs[i] )
            i = i + 1
        return outgraphs
    def Copy( self ):
        aCopy = self.G.Copy()
        if aCopy != None:
            myCopy = Graph( aCopy )
            return myCopy
        ErrMsg = "Failed to get a Copy of " + self.G.Name()
        if self.G.Verbose :
            print ErrMsg
        return aCopy
    def ToStreamGraph( self ):
        aGraph = self.G.ToStreamGraph()
        if aGraph != None :
            return StreamGraph( aGraph )
        return None
    def SubGraphsNodes( self , aSubGraphsNumber ):
        nodes = self.G.SubGraphsNodes( aSubGraphsNumber )
        return self.nodesTuple( nodes )
    def Merge( self , aGraph ):
        return self.G.Merge( aGraph.G )
    def Destroy( self ):
        self.N.destroy()

##########################################################
class GraphE(Graph):
##########################################################
    def __init__( self , aName ):
        try:
            graph = SuperVision.GraphE( aName )
        except:
            try:
                graph = aName.G.Copy()
            except:
                graph = aName
        if graph != None :
            if graph.IsStreamGraph() :
                aGraph = graph.ToStreamGraph()
                if aGraph != None :
                    graph = StreamGraphE( aGraph )
                else :
                    print "Graph creation failed"
            self.G = graph
            self.G.Verbose = 1
            self.N = graph
            self.N.Verbose = 1
        else :
            print "Graph creation failed"
    def ToStreamGraph( self ):
        aGraph = self.G.ToStreamGraph()
        if aGraph != None :
            return StreamGraphE( aGraph )
        return None

##########################################################
class MNode(Graph):
##########################################################
    def __init__( self , aMNode , Verbose ):
        self.G = aMNode
        self.G.Verbose = Verbose
        self.N = aMNode
        self.N.Verbose = Verbose

##########################################################
class StreamGraph(Graph):
##########################################################
    def __init__( self , aName ):
        try:
            graph = SuperVision.StreamGraph( aName )
        except:
            try:
                graph = aName.G.StreamCopy()
            except:
                graph = aName
        if graph != None :
            self.G = graph
            self.G.Verbose = 1
            self.N = graph
            self.N.Verbose = 1
        else :
            print "StreamGraph creation failed"
    def StreamCopy( self ):
        aCopy = self.G.StreamCopy()
        if aCopy != None:
            myCopy = StreamGraph( aCopy )
            return myCopy
        ErrMsg = "Failed to get a Copy of " + self.G.Name()
        if self.G.Verbose :
            print ErrMsg
        return aCopy
    def StreamLink( self , anOutStreamPort , anInStreamPort ) :
        aStreamLink = self.G.StreamLink( anOutStreamPort.P , anInStreamPort.P )
        if aStreamLink!= None:
            myStreamLink = StreamLink( aStreamLink , self.G.Verbose )
            return myStreamLink
        ErrMsg = "Failed to make a StreamLink in " + self.G.Name()
        if self.G.Verbose :
            print ErrMsg
        return aStreamLink        
    def SetStreamParams( self , Timeout , DataStreamTrace , DeltaTime ):
        return self.G.SetStreamParams( Timeout , DataStreamTrace , DeltaTime )
    def StreamParams( self ):
        Timeout,DataStreamTrace,DeltaTime = self.G.StreamParams()
        return Timeout,DataStreamTrace,DeltaTime
    def ToFlowGraph( self ):
        return self.G.ToFlowGraph()
    def SubStreamGraphsNumber( self ):
        return self.G.SubStreamGraphsNumber()
    def SubStreamGraphsNodes( self , aSubStreamGraphsNumber ):
        nodes = self.G.SubStreamGraphsNodes( aSubStreamGraphsNumber )
        return self.nodesTuple( nodes )
    def StreamMerge( self , aStreamGraph ):
        return self.G.StreamMerge( aStreamGraph.G )

##########################################################
class StreamGraphE(StreamGraph):
##########################################################
    def __init__( self , aName ):
        graph = SuperVision.StreamGraphE( aName )
        self.G = graph
        self.G.Verbose = 1
        self.N = graph
        self.N.Verbose = 1

##########################################################
class Value:
##########################################################
    def __init__( self , aValue , Verbose ):
        self.V = aValue
        self.V.Verbose = Verbose
    def ToString( self ):
        return self.V.ToString()
    def ToAny( self ):
        return self.V.ToAny()
    def Destroy( self ):
        self.N.destroy()

##########################################################
class Port:
##########################################################
    def __init__( self , aPort , Verbose ):
        self.P = aPort
        self.P.Verbose = Verbose
    def Input( self , *args ):
        types, realargs = getRealArgs(self.P,args)
        fn = bs_AddInput_valuedict[types]
        anInput = apply( fn, realargs)
        return anInput
    def Node( self ):
        aNode = self.P.Node()
        if aNode != None :
            if aNode.IsComputing() :
                myNode = CNode( aNode , self.P.Verbose )
            elif aNode.IsFactory() :
                myNode = FNode( aNode , self.P.Verbose )
            elif aNode.IsInLine() :
                myNode = INode( aNode , self.P.Verbose )
            elif aNode.IsGOTO() :
                myNode = GNode( aNode , self.P.Verbose )
            elif aNode.IsLoop() :
                myNode = LNode( aNode , self.P.Verbose )
            elif aNode.IsEndLoop() :
                myNode = ELNode( aNode , self.P.Verbose )
            elif aNode.IsSwitch() :
                myNode = SNode( aNode , self.P.Verbose )
            elif aNode.IsEndSwitch() :
                myNode = ESNode( aNode , self.P.Verbose )
            else :
                myNode = None
            return myNode
        return aNode
    def Name( self ):
        return self.P.Name()
    def Type( self ):
        return self.P.Type()
    def Link( self ):
        aLink = self.P.Link()
        if aLink != None :
            myLink = Link( aLink , self.P.Verbose )
            return myLink
        ErrMsg = "Failed to get a Link to " + self.P.Node().Name() + "(" + self.P.Name() + ")"
        if self.P.Verbose :
            print ErrMsg
        return aLink
    def Links( self ):
        links = self.P.Links()
        i = 0
        while i < len( links ) :
            links[ i ] = Link( links[i] , self.P.Verbose )
            i = i + 1
        return links
    def PrintLinks( self ):
        links = self.P.Links()
        i = 0
        while i < len( links ) :
            Link( links[ i ] , self.P.Verbose ).Print()
            i = i + 1
    def IsInput( self ):
        return self.P.IsInput()
    def IsLinked( self ):
        return self.P.IsLinked()
    def HasInput( self ):
        return self.P.HasInput()
    def Kind( self ) :
        return self.P.Kind()
    def IsParam( self ):
        return self.P.IsParam()
    def IsGate( self ):
        return self.P.IsGate()
    def IsLoop( self ):
        return self.P.IsLoop()
    def IsInLine( self ):
        return self.P.IsInLine()
    def IsSwitch( self ):
        return self.P.IsSwitch()
    def IsEndSwitch( self ):
        return self.P.IsEndSwitch()
    def IsDataStream( self ):
        return self.P.IsDataStream()
#    def IsBus( self ):
#        return self.P.IsBus()
    def Done( self ):
        return self.P.Done()
    def State( self ):
        return self.P.State()
    def ToString( self ):
        return self.P.ToString()
    def ToAny( self ):
        return self.P.ToAny()
    def Print( self ):
        if self.P.IsInput() :
            if self.P.IsLinked() :
                print "In",self.P.Node().Name(),'(',self.P.Name(),self.P.Kind(),self.P.Type(),') = ',self.P.ToString(),' from ',self.Link().OutPort().Print()
            else :
                print "In",self.P.Node().Name(),'(',self.P.Name(),self.P.Kind(),self.P.Type(),') = ',self.P.ToString()
        else :
            print "Out",self.P.Node().Name(),'(',self.P.Name(),self.P.Kind(),self.P.Type(),') = ',self.P.ToString()
    def Destroy( self ):
        self.P.destroy()

##########################################################
class StreamPort(Port):
##########################################################
    def __init__( self , aPort , Verbose ):
        self.P = aPort
        self.P.Verbose = Verbose
    def StreamLink( self ) :
        return self.P.StreamLink()
    def Dependency( self ) :
        return self.P.Dependency()
    def SetDependency( self , aDependency ) :
        return self.P.SetDependency( aDependency )
    def Params( self ) :
        sts,aKindOfSchema,aKindOfInterpolation,aKindOfExtrapolation = self.P.Params()
        return sts,aKindOfSchema,aKindOfInterpolation,aKindOfExtrapolation
    def SetParams( self , aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) :
        return self.P.SetParams( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation )
    def NumberOfValues( self ) :
        return self.P.NumberOfValues()
    def SetNumberOfValues( self , aNumberOfValues ) :
        return self.P.SetNumberOfValues( aNumberOfValues )

##########################################################
class Link:
##########################################################
    def __init__( self , aLink , Verbose ):
        self.L = aLink
        self.L.Verbose = Verbose
    def OutPort( self ):
        aPort = self.L.OutPort()
        if aPort != None :
            myPort = Port( aPort , self.L.Verbose )
            return myPort
        if self.L.Verbose :
            print "Error while getting the Port : "
        return None
    def InPort( self ):
        aPort = self.L.InPort()
        if aPort != None :
            myPort = Port( aPort , self.L.Verbose )
            return myPort
        if self.L.Verbose :
            print "Error while getting the Port : "
        return None
    def Print( self ):
        anOutPort = self.L.OutPort()
        anOutPortName = anOutPort.Name()
        anOutPortKind = anOutPort.Kind()
        anOutPortValue = anOutPort.ToString()
        anOutNode = anOutPort.Node()
        anOutNodeName = anOutNode.Name()
        anInPort = self.L.InPort()
        anInPortName = anInPort.Name()
        anInPortKind = anInPort.Kind()
        anInNode = anInPort.Node()
        anInNodeName = anInNode.Name()
        print anOutNodeName,'(',anOutPortName,' ',anOutPortKind,') =',anOutPortValue,' --> ',anInNodeName,'(',anInPortName,')',' ',anInPortKind
    def CoordsSize( self ):
        return self.L.CoordsSize()
    def AddCoord( self , index , X , Y ):
        return self.L.AddCoord( index , X , Y )
    def ChangeCoord( self , index , X , Y ):
        return self.L.ChangeCoord( index , X , Y )
    def RemoveCoord( self , index ):
        return self.L.RemoveCoord( index )
    def Coords( self , index ):
        return self.L.Coords( index )
    def destroy( self ):
        self.L.destroy()
    def IsEqual( self, OtherLink ):
        return self.L.IsEqual( OtherLink.L )

##########################################################
class StreamLink(Link):
##########################################################
    def __init__( self , aLink , Verbose ):
        self.L = aLink
        self.L.Verbose = Verbose
    def OutStreamPort( self ):
        aPort = self.L.OutStreamPort()
        if aPort != None :
            myPort = StreamPort( aPort , self.L.Verbose )
            return myPort
        if self.L.Verbose :
            print "Error while getting the Port : "
        return None
    def InStreamPort( self ):
        aPort = self.L.InStreamPort()
        if aPort != None :
            myPort = StreamPort( aPort , self.L.Verbose )
            return myPort
        if self.L.Verbose :
            print "Error while getting the Port : "
        return None

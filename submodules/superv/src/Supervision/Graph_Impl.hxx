//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : Graph_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

#ifndef _GRAPH_IMPL_HXX_
#define _GRAPH_IMPL_HXX_

//#include <iostream.h>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "StreamLink_Impl.hxx"
#include "FNode_Impl.hxx"
#include "GNode_Impl.hxx"
#include "LNode_Impl.hxx"
#include "ELNode_Impl.hxx"
#include "SNode_Impl.hxx"
#include "ESNode_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

class Graph_Impl : public POA_SUPERV::Graph ,
                   public GNode_Impl {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;

    SALOME_NamingService *     _NamingService ;
    char * _DebugFileName ;

    pthread_mutex_t            _MutexExecutorWait ;
//    GraphExecutor::DataFlow *  _DataFlowExecutor ;

    SUPERV::Graph_var          myServant; // mpv 23.12.2002: store servant of graph to get constant IOR

  public:
    Graph_Impl();
    Graph_Impl( CORBA::ORB_ptr orb ,
		PortableServer::POA_ptr poa ,
		PortableServer::ObjectId * contId , 
		const char *instanceName ,
                const char *interfaceName ,
                const char *aDataFlowName ,
                const SUPERV::KindOfNode aKindOfNode ) ;
    Graph_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                GraphEditor::InNode * DataFlowNode ) ;
    virtual SUPERV::Graph_ptr Copy() ;

    virtual ~Graph_Impl() ;
    virtual void destroy() ;

    void DebugFileName( char * aDebugFileName ) {
         _DebugFileName = my_strdup( aDebugFileName ) ; } ;
    char * DebugFileName() {
           return _DebugFileName ; } ;

    virtual char* getIOR();

    virtual void ReadOnly() ;

    virtual SUPERV::INode_ptr Node() ;

//    virtual char * DataFlowInfo() ;
//    virtual char * DataNodeInfo() ;
//    virtual char * NodeInfo( const char * aNodeName ) ;

    virtual GraphBase::ListOfSGraphs * GetGraphs() ;

    virtual bool LoadGraphs( GraphBase::ListOfSGraphs *aListOfGraphs ) ;
    virtual bool Import(const char * aXmlFile ) ;

    virtual bool Export(const char * aXmlFile ) ;

    virtual char * SavePY( bool importSuperV ) ;

    virtual SUPERV::CNode_ptr CNode(
                          const SALOME_ModuleCatalog::Service &NodeService ) ;
    virtual SUPERV::FNode_ptr FNode(
                          const char * NodeComponentName ,
                          const char * InterfaceName ,
                          const SALOME_ModuleCatalog::Service &NodeService ,
			  const SALOME_ModuleCatalog::ImplType ImplType ) ; // mkr : PAL11273 : C++ implementation by default
    virtual SUPERV::INode_ptr INode(
                          const char * FuncName ,
                          const SUPERV::ListOfStrings & PythonFunction ) ;
    virtual SUPERV::GNode_ptr GNode(
                          const char * FuncName ,
                          const SUPERV::ListOfStrings & PythonFunction ,
                          const char * anInLineNode ) ;
    virtual SUPERV::LNode_ptr LNode(
                          const char * InitName ,
                          const SUPERV::ListOfStrings & InitFunction ,
                          const char * MoreName ,
                          const SUPERV::ListOfStrings & MoreFunction ,
                          const char * NextName ,
                          const SUPERV::ListOfStrings & NextFunction ,
                          SUPERV::INode_out anEndOfLoop ) ;
    virtual SUPERV::SNode_ptr SNode(
                          const char * FuncName ,
                          const SUPERV::ListOfStrings & PythonFunction ,
                          SUPERV::INode_out anEndOfSwitch ) ;

    SUPERV::Graph_var LoadDataFlows( GraphEditor::DataFlow * aDataFlowEditor ,
                                     GraphBase::ListOfSGraphs * aListOfDataFlows ,
                                     int index ) ;
    SUPERV::Graph_var LoadDataFlows( GraphExecutor::DataFlow * aDataFlowExecutor ,
                                     GraphBase::ListOfSGraphs * aListOfDataFlows ,
                                     int index ) ;
    virtual SUPERV::Graph_ptr MNode( const char * aXmlFileName ) ;

    virtual SUPERV::Graph_ptr MNode( GraphEditor::DataFlow * aDataFlowEditor ,
                                     GraphBase::ListOfSGraphs * aListOfDataFlows ) ;

    virtual SUPERV::Graph_ptr GraphMNode( SUPERV::Graph_ptr aGraph ) ;

    virtual SUPERV::Graph_ptr FlowObjRef() ;

    virtual SUPERV::StreamGraph_ptr StreamObjRef() ;

    virtual SUPERV::CNode_ptr Node( const char * NodeName );

    virtual SUPERV::Link_ptr Link( SUPERV::Port_ptr OutPort ,
                                   SUPERV::Port_ptr InPort ) ;

    virtual SUPERV::ListOfNodes_var SetNode( SUPERV::ListOfNodes_var RetVal ,
                                                         GraphBase::ComputingNode * aNode ) ;
    virtual SUPERV::ListOfNodes * Nodes() ;
    virtual void SetNodeObjRef( GraphEditor::InNode * anInNode ) ;

    virtual SUPERV::ListOfLinks * GLinks() ;
    virtual SUPERV::ListOfLinks * Links( GraphBase::ComputingNode * aNode ,
                                         const char * anInputParam ) ;

    virtual Engines::Component_ptr ComponentRef( const char * aComputerContainer ,
                                                 const char * aComponentName ) ;

    virtual char * Messages() ;

    virtual bool IsValid() ;
    virtual bool IsExecutable() ;

    virtual bool IsEditing() ;
    virtual CORBA::Boolean IsExecuting() ;

    virtual bool IsReadOnly() ;

    virtual CORBA::Long LevelMax() ;
    virtual SUPERV::ListOfNodes * LevelNodes(CORBA::Long aLevel ) ;
    virtual CORBA::Long ThreadsMax() ;
    virtual CORBA::Long Threads() ;
    virtual CORBA::Long SuspendedThreads() ;

    virtual bool Run() ;   // called on "Execute Graph" command
    virtual bool Start() ; // called on "Execute Graph Step-by-Step" command
 
    virtual bool Begin() ;
    virtual CORBA::Long LastLevelDone() ;

    virtual bool EventNoW( SUPERV::CNode_out aNode ,
                           SUPERV::GraphEvent & anEvent ,
                           SUPERV::GraphState & aState ) ;
    virtual bool Event( SUPERV::CNode_out aNode ,
                        SUPERV::GraphEvent & anEvent ,
                        SUPERV::GraphState & aState ) ;
    virtual bool EventW( SUPERV::CNode_out aNode ,
                         SUPERV::GraphEvent & anEvent ,
                         SUPERV::GraphState & aState ) ;
    virtual CORBA::Long EventQSize() ;

    virtual CORBA::Long SubGraphsNumber() ;
    virtual SUPERV::ListOfNodes * SubGraphsNodes( CORBA::Long aSubGraphNumber ) ;

    virtual bool Merge(const SUPERV::Graph_ptr aGraph ) ;
    virtual bool Merge(const SUPERV::Graph_ptr aGraph , map< string , int > & aMapOfNodes ) ;

    virtual SUPERV::StreamGraph_ptr ToStreamGraph() ;

    virtual void Editing(); // Destroy Executor and use only Editor and its data model
  
    virtual char* DefaultCContainerName() ;
    virtual char* DefaultPythonContainerName() ;
    virtual char* ContainerNameForComponent( const char * theComponentName) ;

private:
    bool run( const bool andSuspend = false ); // Run() calls run( false ), Start() calls run( true );

};


#endif

//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : FNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "FNode_Impl.hxx"

FNode_Impl::FNode_Impl() {
}

FNode_Impl::FNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char * instanceName ,
                        const char * interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const SALOME_ModuleCatalog::Service &NodeService ,
                        const char * NodeComponentName ,
                        const char * NodeInterfaceName ,
                        const char * NodeName ,
                        const SUPERV::KindOfNode NodeKindOfNode ,
			bool isCimpl ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  CNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , NodeService , NodeName , NodeKindOfNode , NULLSTRING , SUPERV::ListOfStrings() , isCimpl ) {
//  MESSAGE( NodeName << " " );
  beginService( "FNode_Impl::FNode_Impl" );
//  cout << "FNode_Impl::FNode_Impl -->" << endl ;
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _isCimpl = isCimpl ;
//  DataFlowNode( DataFlowEditor()->AddNode( NodeService , NodeComponentName ,
//                                           NodeInterfaceName , NodeName ,
//                                           NodeKindOfNode ) ) ;
  DataFlowNode()->ComponentName( NodeComponentName ) ;
  DataFlowNode()->InterfaceName( NodeInterfaceName ) ;

  // mkr : PAL13947 -->
  const char * aContainer = DataFlowEditor()->Graph()->RetrieveFromMapOfComponentNameContainer(NodeComponentName);
  if ( aContainer ) // container for NodeComponentName component was found in the map
    SetContainer( aContainer );
  // mkr : PAL13947 <--

  endService( "FNode_Impl::FNode_Impl" );  
//  cout << "<-- FNode_Impl::FNode_Impl" << endl ;
}

FNode_Impl::FNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char * instanceName ,
                        const char * interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  CNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
  MESSAGE( DataFlowNode()->Name() << " " );
  beginService( "FNode_Impl::FNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _isCimpl = true ;
  endService( "FNode_Impl::FNode_Impl" );  
}

FNode_Impl::~FNode_Impl() {
  beginService( "FNode_Impl::~Node_Impl" );
  endService( "FNode_Impl::~Node_Impl" );
}

void FNode_Impl::destroy() {
  beginService( "FNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("FNode_Impl::destroy ERROR ") ;
    }
  }
//  endService( "FNode_Impl::Destroy" );
}

bool FNode_Impl::Delete() {
//  beginService( "FNode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
    if ( RetVal ) {
      DataFlowEditor()->UnValid() ;
    }
  }
//  endService( "FNode_Impl::Delete" );
  return RetVal ;
}

char * FNode_Impl::GetComponentName() {
//  beginService( "FNode_Impl::GetComponentName" );
  char * RetVal = DataFlowNode()->ComponentName() ;
//  endService( "FNode_Impl::GetComponentName" );
  return CORBA::string_dup( RetVal );
}
bool FNode_Impl::SetComponentName( const char * aComponentName ) {
//  beginService( "FNode_Impl::SetComponentName" );
  bool RetVal = false ;
  RetVal = DataFlowNode()->ComponentName( aComponentName ) ;
//  endService( "FNode_Impl::SetComponentName" );
  return RetVal ;
}
char * FNode_Impl::GetInterfaceName() {
//  beginService( "FNode_Impl::GetInterfaceName" );
  char * RetVal = DataFlowNode()->InterfaceName() ;
//  endService( "FNode_Impl::GetInterfaceName" );
  return CORBA::string_dup( RetVal );
}
bool FNode_Impl::SetInterfaceName( const char * anInterfaceName ) {
//  beginService( "FNode_Impl::SetInterfaceName" );
  bool RetVal = false ;
    RetVal = DataFlowNode()->InterfaceName( anInterfaceName ) ;
//  endService( "FNode_Impl::SetInterfaceName" );
  return RetVal ;
}
char * FNode_Impl::GetContainer() {
//  beginService( "FNode_Impl::GetContainer" );
  char * RetVal = DataFlowNode()->Computer() ;
//  endService( "FNode_Impl::GetContainer" );
  return CORBA::string_dup( RetVal ) ;
}
bool FNode_Impl::SetContainer( const char * aDataFlowContainer ) {
//  beginService( "FNode_Impl::SetContainer" );
  bool RetVal = false ;
//  GraphExecutor::DataFlow * _DataFlowExecutor = DataFlowEditor()->Executor() ;
//  if ( DataFlowEditor()->IsEditing() ) {
    RetVal = DataFlowNode()->Computer( aDataFlowContainer ) ;

    // insert modified container into < ComponentName, Container > map
    DataFlowEditor()->Graph()->InsertToMapOfComponentNameContainer( GetComponentName(), aDataFlowContainer ) ; // mkr : PAL13947

//  }
//  else if ( _DataFlowExecutor && ( _DataFlowExecutor->IsSuspended() ||
//            _DataFlowExecutor->IsSuspended( DataFlowNode()->Name() ) ) ) {
//    if ( _DataFlowExecutor->Graph()->GetChangeGraphNode( DataFlowNode()->Name() ) ) {
//      RetVal = ((GraphEditor::InNode * ) _DataFlowExecutor->Graph()->GetChangeGraphNode( DataFlowNode()->Name() )->GetInNode())->Computer( aDataFlowContainer ) ;
      DataFlowEditor()->UnValid() ;
//    }
//  }
//  endService( "FNode_Impl::SetContainer" );
  return RetVal ;
}


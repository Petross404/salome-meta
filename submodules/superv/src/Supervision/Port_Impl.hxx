//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : Port_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

#ifndef _PORT_IMPL_HXX_
#define _PORT_IMPL_HXX_

#include <sstream>
#include <iostream>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "Value_Impl.hxx"
#include "Link_Impl.hxx"
#include "FNode_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"

class Port_Impl : public Value_Impl ,
                  public POA_SUPERV::Port {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;

    GraphEditor::DataFlow * _DataFlowEditor ;
    GraphEditor::InNode *   _DataFlowNode ;
    GraphBase::Port *       _DataFlowPort ;
    bool                    _InputPort ;

  public:
    Port_Impl();
    Port_Impl( CORBA::ORB_ptr orb ,
	       PortableServer::POA_ptr poa ,
	       PortableServer::ObjectId * contId , 
	       const char *instanceName ,
               const char *interfaceName ,
               GraphEditor::DataFlow * DataFlowEditor ,
               GraphEditor::InNode * DataFlowNode ,
               GraphBase::Port * DataFlowPort ,
               bool InputPort ,
//               const char *ParameterName ,
               const CORBA::Any * anAny ) ;
    Port_Impl( CORBA::ORB_ptr orb ,
	       PortableServer::POA_ptr poa ,
	       PortableServer::ObjectId * contId , 
	       const char *instanceName ,
               const char *interfaceName ,
               GraphEditor::DataFlow * DataFlowEditor ,
               GraphEditor::InNode * DataFlowNode ,
               GraphBase::Port * DataFlowPort ,
               bool InputPort ) ;
//               const char *ParameterName ) ;
    virtual ~Port_Impl() ;
    virtual void destroy() ;

    virtual void Remove() ;

    GraphEditor::DataFlow * DataFlowEditor() {
                            return _DataFlowEditor ; } ;
    GraphEditor::InNode * DataFlowNode() {
                          return _DataFlowNode ; } ;
    GraphBase::Port * DataFlowPort() {
                      return _DataFlowPort ; } ;

    virtual bool Input( const SUPERV::Value_ptr aValue ) ;
    virtual bool Input( const CORBA::Any * anAny ) ;

    virtual char * Name()  ;
    virtual char * Type()  ;

    virtual SUPERV::CNode_ptr Node()  ;

    virtual SUPERV::Link_ptr Link()  ;
    virtual SUPERV::ListOfLinks * Links()  ;

    virtual bool IsInput()  ;
    virtual bool IsLinked()  ;
    virtual bool HasInput()  ;

    virtual SUPERV::KindOfPort Kind()  ;
    virtual void SetKind( SUPERV::KindOfPort ) ;

    virtual bool IsParam()  ;
    virtual bool IsGate()  ;
    virtual bool IsInLine()  ;
    virtual bool IsLoop()  ;
    virtual bool IsSwitch()  ;
    virtual bool IsEndSwitch()  ;
    virtual bool IsDataStream()  ;

    virtual SUPERV::GraphState State()  ;
    virtual bool IsDone()  ;

} ;

#endif

//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : Value_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <strstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "Value_Impl.hxx"

#include "DataFlowExecutor_DataFlow.hxx"

Value_Impl::Value_Impl( CORBA::ORB_ptr orb ,
			PortableServer::POA_ptr poa ,
	       	        PortableServer::ObjectId * contId , 
			const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * DataFlowEditor ,
                        GraphEditor::InNode * DataFlowNode ,
                        const char *ParameterName ,
                        const CORBA::Any * anAny ,
                        const bool activate ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  if ( activate ) {
//    MESSAGE("Value_Impl::Value_Impl activate object instanceName("
//            << instanceName << ") interfaceName(" << interfaceName << ") --> "
//            << hex << (void *) this << dec )
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = DataFlowEditor ;
  _DataFlowNode = DataFlowNode ;
  if ( ParameterName != NULL ) {
    _ParameterName = new char[ strlen( ParameterName ) + 1 ] ;
    strcpy( _ParameterName , ParameterName ) ;
    if ( DataFlowNode ) {
      _NodeName = new char[ strlen( DataFlowNode->Name() ) + 1 ] ;
      strcpy( _NodeName , DataFlowNode->Name() ) ;
    }
    else {
    _NodeName = NULL ;
    }
  }
  else {
    _ParameterName = NULL ;
    _NodeName = NULL ;
  }
  _InputValue = false ;
  _Any = new CORBA::Any( *anAny ) ;
}

Value_Impl::Value_Impl( CORBA::ORB_ptr orb ,
			PortableServer::POA_ptr poa ,
	       	        PortableServer::ObjectId * contId , 
			const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * DataFlowEditor ,
                        GraphEditor::InNode * DataFlowNode ,
                        const char *ParameterName ,
                        const bool activate ) :
  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  if ( activate ) {
//    MESSAGE("Value_Impl::Value_Impl activate object instanceName("
//            << instanceName << ") interfaceName(" << interfaceName << ") --> "
//            << hex << (void *) this << dec )
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _DataFlowEditor = DataFlowEditor ;
  _DataFlowNode = DataFlowNode ;
  if ( ParameterName != NULL ) {
    _ParameterName = new char[ strlen( ParameterName ) + 1 ] ;
    strcpy( _ParameterName , ParameterName ) ;
    if ( DataFlowNode ) {
      _NodeName = new char[ strlen( DataFlowNode->Name() ) + 1 ] ;
      strcpy( _NodeName , DataFlowNode->Name() ) ;
    }
    else {
    _NodeName = NULL ;
    }
  }
  else {
    _ParameterName = NULL ;
    _NodeName = NULL ;
  }
  _InputValue = false ;
  _Any = new CORBA::Any() ;
}

Value_Impl::Value_Impl() {
}

Value_Impl::~Value_Impl() {
  beginService( "Value_Impl::~Value_Impl" );
  endService( "Value_Impl::~Value_Impl" );
}

void Value_Impl::destroy() {
  beginService( "Value_Impl::destroy" );
  _poa->deactivate_object(*_id) ;
//  CORBA::release(_poa) ;
  delete(_id) ;
//  _thisObj->_remove_ref();
  endService( "Value_Impl::destroy" );
}

void Value_Impl::InPort( bool anInput ) {
  _InputPort = anInput ;
}

bool Value_Impl::Input( const CORBA::Any * anAny ) {
//  _DataFlowEditor->AddInputData( _DataFlowNode->Name() ,
//                                 _ParameterName ,
//                                 *anAny ) ;
  delete _Any ;
  _Any = new CORBA::Any( *anAny ) ;
  _InputValue = true ;
  return true ;
}

CORBA::Any * Value_Impl::ToAny() {
  beginService( "Value_Impl::ToAny" );
  CORBA::Any * anAny ;
  if ( _DataFlowEditor ) {
    char * name ;
    if ( _DataFlowNode ) {
      name = _DataFlowNode->Name() ;
    }
    else {
      name = _DataFlowEditor->Graph()->Name() ;
    }
    MESSAGE( "ToAny " << name<< "( " << _ParameterName << " )" ) ;
    if ( _InputPort ) {
      if ( _DataFlowNode ) {
        _InputValue = _DataFlowNode->HasInput( _ParameterName ) ;
      }
      else {
        _InputValue = _DataFlowEditor->HasInput( _ParameterName ) ;
      }
      GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
      if ( _DataFlowExecutor ) {
//JR 30.03.2005        anAny = new CORBA::Any( * _DataFlowExecutor->GetInData( name ,
        anAny = new CORBA::Any( _DataFlowExecutor->GetInData( name ,
                                                              _ParameterName ) ) ;
      }
      else if ( _InputValue ) {
//JR 30.03.2005        anAny = new CORBA::Any( * _DataFlowEditor->GetInData( name ,
        anAny = new CORBA::Any( _DataFlowEditor->GetInData( name ,
                                                            _ParameterName ) ) ;
      }
      else {
        anAny = new CORBA::Any( *_Any ) ;
      }
    }
    else {
      GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
      if ( _DataFlowExecutor ) {
//JR 30.03.2005        anAny = new CORBA::Any( * _DataFlowExecutor->GetOutData( name ,
        anAny = new CORBA::Any( _DataFlowExecutor->GetOutData( name ,
                                                              _ParameterName ) ) ;
      }
      else {
        anAny = new CORBA::Any( *_Any ) ;
      }
    }
  }
  else {
    anAny = new CORBA::Any( *_Any ) ;
  }
  endService( "Value_Impl::ToAny" );
  return anAny ;
}

char * Value_Impl::ToString() {
//  beginService( "Value_Impl::ToString" );
//JR 22.03.2005 Memory Leak  CORBA::Any anAny = *ToAny() ;
  CORBA::Any anAny ;
  if ( _DataFlowEditor ) {
    char * name ;
    if ( _DataFlowNode ) {
      name = _DataFlowNode->Name() ;
    }
    else {
      name = _DataFlowEditor->Graph()->Name() ;
    }
//    MESSAGE( "ToString " << name<< "( " << _ParameterName << " )" ) ;
    if ( _InputPort ) {
      if ( _DataFlowNode ) {
        _InputValue = _DataFlowNode->HasInput( _ParameterName ) ;
      }
      else {
        _InputValue = _DataFlowEditor->HasInput( _ParameterName ) ;
      }
      GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
      if ( _DataFlowExecutor ) {
//        cout << "-->_DataFlowExecutor->GetInData " << name << " " << _ParameterName << endl ;
//JR 30.03.2005        const CORBA::Any * AnyPtr = _DataFlowExecutor->GetInData( name ,
        const CORBA::Any AnyRef = _DataFlowExecutor->GetInData( name ,
                                                                _ParameterName ) ;
//JR 30.03.2005        cout << "<--_DataFlowExecutor->GetInData " << AnyPtr << endl ;
//JR 30.03.2005        anAny = * AnyPtr ;
        anAny = AnyRef ;
      }
      else if ( _InputValue ) {
//        cout << "_DataFlowEditor->GetInData _DataFlowNode " << _DataFlowNode
//             << " _InputValue " << _InputValue << endl ;
//JR 30.03.2005        anAny = * _DataFlowEditor->GetInData( name ,
        anAny = _DataFlowEditor->GetInData( name ,
                                            _ParameterName ) ;
      }
      else {
        anAny = *_Any ;
      }
    }
    else {
      GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
      if ( _DataFlowExecutor ) {
//        cout << "-->_DataFlowExecutor->GetOutData " << name << " " << _ParameterName << endl ;
//JR 30.03.2005        const CORBA::Any * AnyPtr = _DataFlowExecutor->GetOutData( name ,
        const CORBA::Any AnyRef = _DataFlowExecutor->GetOutData( name ,
                                                                 _ParameterName ) ;
//JR 30.03.2005        cout << "<--_DataFlowExecutor->GetOutData " << AnyPtr << endl ;
//JR 30.03.2005        anAny = * AnyPtr ;
        anAny = AnyRef ;
      }
      else {
        anAny = *_Any ;
      }
    }
  }
  ostringstream astr ;
  const char * retstr ;
  string RetStr;
  int startstr = 0 ;
  switch (anAny.type()->kind()) {
    case CORBA::tk_string: {
      anAny >>= retstr;
      RetStr = string(retstr);
//      MESSAGE( "ToString( string ) '" << retstr << "'" );
      break ;
    }
    case CORBA::tk_long: {
      CORBA::Long l;
      anAny >>= l;
      astr << l << ends ;
      RetStr = astr.str() ;
//      MESSAGE( "ToString( CORBA::Long ) '" << l << " " << retstr << "'" );
      break ;
    }
    case CORBA::tk_double: {
      double d;
      anAny >>= d;
      astr << setw(25) << setprecision(18) << d << ends ;
      RetStr = astr.str();
      int i = 0 ;
      while ( i < (int ) RetStr.length() && RetStr.at(i++) == ' ' ) {
        startstr = i ;
      }
      RetStr = RetStr.substr(startstr) ;
//      MESSAGE( "ToString( double ) '" << d << "' '" << retstr << "' '" << &retstr[ startstr ] << "'");
      break ;
    }
    case CORBA::tk_objref: {
      CORBA::Object_ptr obj ;
      try {
#if OMNIORB_VERSION >= 4
        anAny >>= (CORBA::Any::to_object ) obj ;
#else
        anAny >>= obj ;
#endif
	RetStr = _Orb->object_to_string( obj );
//        MESSAGE( "ToString( object ) '" << retstr << "'" );
      }
      catch ( ... ) {
	RetStr = "object_to_string catched " ;
      }
      break ;
    }
    default: {
      RetStr = "Unknown CORBA::Any Type" ;
//      MESSAGE( retstr );
      break ;
    }
  }
//  endService( "Value_Impl::ToString" );
  return CORBA::string_dup( RetStr.c_str() ) ;
}

bool Value_Impl::IsIOR() {
  CORBA::Any anAny = *ToAny() ;
  if ( _DataFlowEditor ) {
    char * name ;
    if ( _DataFlowNode ) {
      name = _DataFlowNode->Name() ;
    }
    else {
      name = _DataFlowEditor->Graph()->Name() ;
    }
    if ( _InputPort ) {
      if ( _DataFlowNode ) {
        _InputValue = _DataFlowNode->HasInput( _ParameterName ) ;
      }
      else {
        _InputValue = _DataFlowEditor->HasInput( _ParameterName ) ;
      }
      if ( _InputValue && _DataFlowEditor->IsEditing() ) {
//JR 30.03.2005        anAny = * _DataFlowEditor->GetInData( name ,
        anAny = _DataFlowEditor->GetInData( name ,
                                            _ParameterName ) ;
      }
      else {
        GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
        if ( _DataFlowExecutor ) {
//JR 30.03.2005          anAny = * _DataFlowExecutor->GetInData( name ,
          anAny = _DataFlowExecutor->GetInData( name ,
                                                _ParameterName ) ;
	}
      }
    }
    else {
      if ( _InputValue && _DataFlowEditor->IsEditing() ) {
//JR 30.03.2005        anAny = * _DataFlowEditor->GetOutData( name ,
        anAny = _DataFlowEditor->GetOutData( name ,
                                             _ParameterName ) ;
      }
      else {
        GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
        if ( _DataFlowExecutor ) {
//JR 30.03.2005          anAny = * _DataFlowExecutor->GetOutData( name ,
          anAny = _DataFlowExecutor->GetOutData( name ,
                                                 _ParameterName ) ;
	}
      }
    }
  }
  return(anAny.type()->kind() == CORBA::tk_objref);
}

char* Value_Impl::ComponentDataType() {
  GraphExecutor::DataFlow* _DataFlowExecutor = _DataFlowEditor->Executor();
  const GraphBase::ComputingNode* node = _DataFlowExecutor->Graph()->GetGraphNode(_DataFlowNode->Name());
  if ( node->IsFactoryNode() ) {
    Engines::Component_var compo = ((GraphBase::FactoryNode * ) node )->Component();
    if ( CORBA::is_nil( compo ) ) {
      return ( strdup( "UnknownComponent" ) ) ;
    }
    else {
      return ( strdup( compo->instanceName() ) ) ;
    }
  }
  return ( strdup ( "UnknownComponent" ) ) ;
}

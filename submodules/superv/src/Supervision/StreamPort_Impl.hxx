//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : StreamPort_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

#ifndef _STREAMPORT_IMPL_HXX_
#define _STREAMPORT_IMPL_HXX_

#include "Port_Impl.hxx"

#include "StreamLink_Impl.hxx"

class StreamPort_Impl : public Port_Impl ,
                        public POA_SUPERV::StreamPort {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;

  public:
    StreamPort_Impl();
    StreamPort_Impl( CORBA::ORB_ptr orb ,
	             PortableServer::POA_ptr poa ,
	             PortableServer::ObjectId * contId , 
	             const char *instanceName ,
                     const char *interfaceName ,
                     GraphEditor::DataFlow * DataFlowEditor ,
                     GraphEditor::InNode * DataStreamNode ,
                     GraphBase::Port * DataStreamPort ,
                     bool InputPort ,
                     const CORBA::Any * anAny ) ;
    StreamPort_Impl( CORBA::ORB_ptr orb ,
	             PortableServer::POA_ptr poa ,
	             PortableServer::ObjectId * contId , 
	             const char *instanceName ,
                     const char *interfaceName ,
                     GraphEditor::DataFlow * DataFlowEditor ,
                     GraphEditor::InNode * DataFlowNode ,
                     GraphBase::Port * DataStreamPort ,
                     bool InputPort ) ;
    virtual ~StreamPort_Impl() ;
    virtual void destroy() ;

    virtual SUPERV::StreamLink_ptr StreamLink()  ;

    virtual SALOME_ModuleCatalog::DataStreamDependency Dependency() ;
    virtual bool SetDependency( const SALOME_ModuleCatalog::DataStreamDependency aDependency ) ;

    virtual bool SetParams( const SUPERV::KindOfSchema aKindOfSchema ,
                            const SUPERV::KindOfInterpolation aKindOfInterpolation ,
                            const SUPERV::KindOfExtrapolation aKindOfExtrapolation ) ;
    virtual bool Params( SUPERV::KindOfSchema & aKindOfSchema ,
                         SUPERV::KindOfInterpolation & aKindOfInterpolation ,
                         SUPERV::KindOfExtrapolation & aKindOfExtrapolation ) ;

    virtual bool SetNumberOfValues( CORBA::Long aNumberOfValues ) ;
    virtual CORBA::Long NumberOfValues() ;

} ;

#endif

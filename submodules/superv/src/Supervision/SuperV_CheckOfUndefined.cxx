//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SuperV_CheckOfUndefined.cxx
//  Module : SUPERV

using namespace std;
#include <iostream>
#include <fstream>
#include <unistd.h>

#include <Python.h>

#include "DataFlowBase_Base.hxx"
#include "DataFlowBase_Graph.hxx"
#include "DataFlowExecutor_InNode.hxx"
#include "DataFlowExecutor_OutNode.hxx"
#include "DataFlowExecutor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.lxx"
#include "DataFlowEditor_InNode.hxx"
#include "DataFlowEditor_OutNode.hxx"
#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowEditor_DataFlow.lxx"

int _ArgC ;
char ** _ArgV ;

static PyMethodDef MethodPyVoidMethod[] = {
  { NULL,        NULL }
};

int main(int argc, char **argv) {
  Py_Initialize() ;
  Py_InitModule( "InitPyRunMethod" , MethodPyVoidMethod ) ;
  return 0;
}


//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SuperV_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header$

#ifndef _SUPERV_IMPL_HXX_
#define _SUPERV_IMPL_HXX_

#include <iostream>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include CORBA_SERVER_HEADER(SALOMEDS)
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "StreamGraph_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"

class SuperV_Impl : public POA_SUPERV::SuperG ,
                    public Engines_Component_i  {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;
    char *myStrURL;

  public:
    SuperV_Impl();
    SuperV_Impl( CORBA::ORB_ptr orb ,
		 PortableServer::POA_ptr poa ,
		 PortableServer::ObjectId * contId , 
		 const char *instanceName ,
                 const char *interfaceName 
//                 , int argc ,
//                 char ** argv
               ) ;

    virtual ~SuperV_Impl() ;
    virtual void Destroy() ;

    Engines::TMPFile * DumpPython( CORBA::Object_ptr theStudy ,
				   CORBA::Boolean isPublished ,
                                   CORBA::Boolean & isValidScript ) ;

//      void Save(const char* IORSComponent, const char* aUrlOfFile);
//      void Load(const char* IORSComponent, const char* aUrlOfFile);
    SALOMEDS::TMPFile* Save(SALOMEDS::SComponent_ptr theComponent,
			    const char* theURL,
			    bool isMultiFile);
    SALOMEDS::TMPFile* SaveASCII(SALOMEDS::SComponent_ptr theComponent,
				 const char* theURL,
				 bool isMultiFile);
    CORBA::Boolean Load(SALOMEDS::SComponent_ptr theComponent,
			const SALOMEDS::TMPFile& theStream,
			const char* theURL,
			bool isMultiFile);
    CORBA::Boolean LoadASCII(SALOMEDS::SComponent_ptr theComponent,
			     const SALOMEDS::TMPFile& theStream,
			     const char* theURL,
			     bool isMultiFile);

    void Close(SALOMEDS::SComponent_ptr theComponent);
    char* ComponentDataType();

    char* IORToLocalPersistentID(SALOMEDS::SObject_ptr theSObject,
				 const char* IORString,
				 CORBA::Boolean isMultiFile,
				 CORBA::Boolean isASCII);
    char* LocalPersistentIDToIOR(SALOMEDS::SObject_ptr theSObject,
				 const char* aLocalPersistentID,
				 CORBA::Boolean isMultiFile,
				 CORBA::Boolean isASCII)
      throw(SALOME::SALOME_Exception);

    char* IORToLocalPersistentID(SALOMEDS::SObject_ptr theSObject, CORBA::Boolean isMultiFile);
    char* LocalPersistentIDToIOR(SALOMEDS::SObject_ptr theSObject, CORBA::Boolean isMultiFile);

    bool CanPublishInStudy(CORBA::Object_ptr theIOR);
    SALOMEDS::SObject_ptr PublishInStudy(SALOMEDS::Study_ptr theStudy,
		 	                 SALOMEDS::SObject_ptr theSObject,
			                 CORBA::Object_ptr theObject,
			                 const char* theName) throw (SALOME::SALOME_Exception) ;

    CORBA::Boolean CanCopy(SALOMEDS::SObject_ptr theObject);
    SALOMEDS::TMPFile* CopyFrom(SALOMEDS::SObject_ptr theObject, CORBA::Long& theObjectID);
    CORBA::Boolean CanPaste(const char* theComponentName, CORBA::Long theObjectID);
    SALOMEDS::SObject_ptr PasteInto(const SALOMEDS::TMPFile& theStream,
				    CORBA::Long theObjectID,
				    SALOMEDS::SObject_ptr theObject);

    virtual SUPERV::Value_ptr StringValue( const char * aStrIOR ) ;
    virtual SUPERV::Value_ptr AnyValue( const CORBA::Any & anAny ) ;
    virtual SUPERV::Value_ptr Value( CORBA::Any * anAny ) ;
    virtual CORBA::Object_ptr ToObject( CORBA::Object_ptr anObjectRef ) ;

    virtual SUPERV::Graph_ptr Graph( const char * aDataFlowName ) ;

    virtual SUPERV::Graph_ptr GraphE( const char * aDataFlowName ) ;

    virtual SUPERV::StreamGraph_ptr StreamGraph( const char * aDataFlowName ) ;

    virtual SUPERV::StreamGraph_ptr StreamGraphE( const char * aDataFlowName ) ;

    virtual SUPERV::StreamGraph_ptr getStreamGraph(const char* ior);
    virtual SUPERV::Graph_ptr getGraph(const char* ior);

};

extern "C"
  PortableServer::ObjectId * SUPERVEngine_factory
                                   ( CORBA::ORB_ptr orb ,
                                     PortableServer::POA_ptr poa , 
                                     PortableServer::ObjectId * contId ,
                                     const char *instanceName ,
                                     const char *interfaceName 
//                                     , int argc ,
//                                     char ** argv
                                     ) ;

#endif

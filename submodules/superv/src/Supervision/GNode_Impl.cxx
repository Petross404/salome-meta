//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : GNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "GNode_Impl.hxx"

GNode_Impl::GNode_Impl() {
}

GNode_Impl::GNode_Impl( CORBA::ORB_ptr orb ,
		       PortableServer::POA_ptr poa ,
	       	       PortableServer::ObjectId * contId , 
	               const char *instanceName ,
                       const char *interfaceName ,
                       const char *aDataFlowName ,
                       const SUPERV::KindOfNode aKindOfNode ) :
  INode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowName , aKindOfNode ) {
//  MESSAGE("GNode_Impl::GNode_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
//  _thisObj = this ;
//  _id = _poa->activate_object(_thisObj);
//  MESSAGE( "GNode_Impl::GNode_Impl " << aDataFlowName );
//  beginService( "GNode_Impl::GNode_Impl" );
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  endService( "INode_Impl::INode_Impl" );  
}

GNode_Impl::GNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const char * FuncName ,
                        const SUPERV::ListOfStrings & PythonFunction ,
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  INode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , FuncName , PythonFunction , NodeKindOfNode ) {
//  MESSAGE( NodeName << " " );
//  beginService( "GNode_Impl::GNode_Impl" );
  if ( FuncName ) {
//    cout << "GNode_Impl::GNode_Impl " << (void *) FuncName << " " << FuncName
//         << " " << strlen( FuncName ) << endl ;
  }
  if ( NodeKindOfNode == SUPERV::GOTONode ) {
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode( DataFlowEditor()->AddNode( NodeService , NodeComponentName ,
//                                           NodeInterfaceName , NodeName ,
//                                           NodeKindOfNode ) );
//  DataFlowNode( DataFlowEditor()->AddNode( SALOME_ModuleCatalog::Service() ,
//                                           "" , "" , "" ,
//                                           NodeKindOfNode ) ) ;
//  endService( "GNode_Impl::GNode_Impl" );  
}

GNode_Impl::GNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        GraphBase::ListOfFuncName FuncNameList ,
                        GraphBase::ListOfPythonFunctions PythonFunctionList ,
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  INode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , FuncNameList , PythonFunctionList , NodeKindOfNode ) {
//  MESSAGE( NodeName << " " );
//  beginService( "GNode_Impl::GNode_Impl" );
  if ( NodeKindOfNode == SUPERV::GOTONode ) {
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode( DataFlowEditor()->AddNode( NodeService , NodeComponentName ,
//                                           NodeInterfaceName , NodeName ,
//                                           NodeKindOfNode ) );
//  DataFlowNode( DataFlowEditor()->AddNode( SALOME_ModuleCatalog::Service() ,
//                                           "" , "" , "" ,
//                                           NodeKindOfNode ) ) ;
//  endService( "GNode_Impl::GNode_Impl" );  
}

GNode_Impl::GNode_Impl( CORBA::ORB_ptr orb ,
	              PortableServer::POA_ptr poa ,
	              PortableServer::ObjectId * contId , 
	              const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * aDataFlowEditor ,
                      GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  INode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
//  beginService( "GNode_Impl::GNode_Impl" );
  if ( DataFlowNode() && DataFlowNode()->IsGOTONode() ) {
//    MESSAGE( "GNode_Impl::GNode_Impl " << DataFlowNode()->PyFuncName()
//             << " _poa->activate_object" );
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj);
  }
  else {
//    MESSAGE( "GNode_Impl::GNode_Impl NO _poa->activate_object" );
  }
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  endService( "GNode_Impl::GNode_Impl" );  
}

GNode_Impl::~GNode_Impl() {
  beginService( "GNode_Impl::~GNode_Impl" );
  endService( "GNode_Impl::~GNode_Impl" );
}

void GNode_Impl::destroy() {
  beginService( "GNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("GNode_Impl::destroy ERROR ") ;
    }
  }
//  endService( "GNode_Impl::Destroy" );
}

bool GNode_Impl::Delete() {
  beginService( "GNode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    if ( DataFlowNode() ) {
      GraphBase::GOTONode * aGOTONode = DataFlowNode()->GOTONode() ;
      if ( aGOTONode ) {
        GraphBase::GOTONode * aCoupledNode = (GraphBase::GOTONode * ) aGOTONode->CoupledNode() ;
        if ( aCoupledNode && ( aCoupledNode->IsOneOfGOTONodes() || aCoupledNode->IsDataFlowNode() ||
                               aCoupledNode->IsDataStreamNode() ) ) {
          aCoupledNode->CoupledNode( NULL ) ;
          aCoupledNode->MacroObject( SUPERV::Graph::_nil() ) ;
          aCoupledNode->GraphMacroLevel( 0 ) ;
        }
      }
    }
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
    if ( RetVal )
      DataFlowEditor()->UnValid() ;
  }
  endService( "GNode_Impl::Delete" );
  return RetVal ;
}

SUPERV::INode_ptr GNode_Impl::Coupled() {
//  beginService( "GNode_Impl::Coupled" );
  SUPERV::INode_var iobject = SUPERV::INode::_nil() ;
  if ( DataFlowNode() ) {
    if ( DataFlowNode()->CoupledNode() ) {
      if ( CORBA::is_nil( DataFlowNode()->CoupledNode()->ObjRef() ) ) {
        INode_Impl * myNode = new INode_Impl( _Orb , _Poa , _ContId ,
                                              instanceName() , interfaceName() ,
                                              DataFlowEditor() ,
                                              (GraphEditor::InNode *) DataFlowNode()->CoupledNode()->GetInNode() ) ;
        PortableServer::ObjectId * id = myNode->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        iobject = SUPERV::INode::_narrow(obj) ;
        myNode->SetObjRef( SUPERV::INode::_duplicate( iobject ) ) ;
      }
      else {
        iobject = SUPERV::INode::_narrow( DataFlowNode()->CoupledNode()->ObjRef() ) ;
      }
    }
  }
  else {
    if ( DataFlowEditor()->Graph()->CoupledNode() ) {
      if ( CORBA::is_nil( DataFlowEditor()->Graph()->CoupledNode()->ObjRef() ) ) {
        INode_Impl * myNode = new INode_Impl( _Orb , _Poa , _ContId ,
                                              instanceName() , interfaceName() ,
                                              DataFlowEditor() ,
                                              (GraphEditor::InNode *) DataFlowEditor()->Graph()->CoupledNode()->GetInNode() ) ;
        PortableServer::ObjectId * id = myNode->getId() ;
        CORBA::Object_var obj = _poa->id_to_reference(*id);
        iobject = SUPERV::INode::_narrow(obj) ;
        myNode->SetObjRef( SUPERV::INode::_duplicate( iobject ) ) ;
      }
      else {
        iobject = SUPERV::INode::_narrow( DataFlowEditor()->Graph()->CoupledNode()->ObjRef() ) ;
      }
    }
  }
//  endService( "GNode_Impl::Coupled" );
  return SUPERV::INode::_duplicate( iobject ) ;
}

bool GNode_Impl::SetCoupled( const char * anInLineNode )
{
  GraphBase::InLineNode * CoupledINode = (GraphBase::InLineNode * ) DataFlowEditor()->Graph()->GetGraphNode( anInLineNode ) ;
  if ( !CoupledINode ) {
    
    // asv : 2.11.04 : nullify Coupled node if anInLineNode == "" (bug 7113)
    if ( anInLineNode==NULL || !strlen( anInLineNode ) ) {
      DataFlowNode()->CoupledNode( NULL );
    }

    return false;
  } 
  if ( ( IsGOTO() && ( CoupledINode->IsInLineNode() || CoupledINode->IsLoopNode() || CoupledINode->IsSwitchNode() ) ) ||
       ( !IsGOTO() && ( IsLoop() || IsSwitch() || IsEndLoop() || IsEndSwitch() ) ) ) {
    DataFlowNode()->CoupledNode( CoupledINode ) ;
    if ( CoupledINode && IsSwitch() ) {
//JR NPAL14793 05.02.2007 : do not add that link
//      DataFlowEditor()->AddLink( Name() , "Default" ,CoupledINode->Name() , "Default" ) ;
    }
    else if ( CoupledINode && ( IsLoop() || IsEndLoop() ) ) {
      DataFlowEditor()->AddLink( Name() , "DoLoop" , CoupledINode->Name() , "DoLoop" ) ;
    }
    else if ( CoupledINode && IsGOTO() ) {
      DataFlowEditor()->AddLink( Name() , "OutGate" ,CoupledINode->Name() , "InGate" ) ;
    }
    return  true ;
  }
  return false ; 
}

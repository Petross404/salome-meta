//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : StreamLink_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

#ifndef _STREAMLINK_IMPL_HXX_
#define _STREAMLINK_IMPL_HXX_

#include "StreamPort_Impl.hxx"

#include "Link_Impl.hxx"

class StreamLink_Impl : public POA_SUPERV::StreamLink ,
                        public Link_Impl {
  private:

    CORBA::ORB_ptr _Orb ;
    PortableServer::POA_ptr _Poa ;
    PortableServer::ObjectId * _ContId ;

  public:
    StreamLink_Impl();
    StreamLink_Impl( CORBA::ORB_ptr orb ,
	             PortableServer::POA_ptr poa ,
	             PortableServer::ObjectId * contId , 
	             const char *instanceName ,
                     const char *interfaceName ,
                     GraphEditor::DataFlow * DataFlowEditor ,
                     GraphEditor::InNode * DataFlowNode ,
                     const char *InputParameterName ,
                     GraphEditor::InNode * DataFlowOutNode ,
                     const char *OutputParameterName ,
                     const bool Create ,
                     bool & Success ) ;

    virtual ~StreamLink_Impl() ;
    virtual void destroy() ;
    virtual bool Delete() ;

    SUPERV::StreamPort_ptr OutStreamPort() ;
    SUPERV::StreamPort_ptr InStreamPort() ;

} ;

#endif

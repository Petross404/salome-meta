//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : ESNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "ESNode_Impl.hxx"

ESNode_Impl::ESNode_Impl() {
}

ESNode_Impl::ESNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const char * FuncName , 
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , FuncName , SUPERV::ListOfStrings() , NodeKindOfNode ) {
//  MESSAGE( NodeName << " " );
  beginService( "ESNode_Impl::ESNode_Impl" );
//  if ( FuncName ) {
//    cout << "ESNode_Impl::GNode_Impl " << (void *) FuncName << " " << FuncName
//         << " " << strlen( FuncName ) << endl ;
//  }
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode( DataFlowEditor()->AddNode( NodeService , NodeComponentName ,
//                                           NodeInterfaceName , NodeName ,
//                                           NodeKindOfNode ) ) ;
//  DataFlowNode( DataFlowEditor()->AddNode( SALOME_ModuleCatalog::Service() ,
//                                           "" , "" , "" ,
//                                           NodeKindOfNode ) ) ;
  endService( "ESNode_Impl::ESNode_Impl" );  
}

ESNode_Impl::ESNode_Impl( CORBA::ORB_ptr orb ,
	              PortableServer::POA_ptr poa ,
	              PortableServer::ObjectId * contId , 
	              const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * aDataFlowEditor ,
                      GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
  MESSAGE( DataFlowNode()->Name() << " " );
  beginService( "ESNode_Impl::ESNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  endService( "ESNode_Impl::ESNode_Impl" );  
}

ESNode_Impl::~ESNode_Impl() {
  beginService( "ESNode_Impl::~ESNode_Impl" );
  endService( "ESNode_Impl::~ESNode_Impl" );
}

void ESNode_Impl::destroy() {
  beginService( "ESNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
//    SUPERV::GNode_ptr aCoupled = SUPERV::GNode::_narrow( Coupled() ) ;
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("ESNode_Impl::destroy ERROR ") ;
    }
// destroy have to be used for the SwitchNode ==> destroy of the EndSwitchNode
//    if ( !CORBA::is_nil( aCoupled ) ) {
//      aCoupled->SetCoupled( "" ) ;
//      aCoupled->destroy() ;
//    }
  }
  endService( "ESNode_Impl::Destroy" );
}

bool ESNode_Impl::Delete() {
  beginService( "ESNode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
// IsValid is done in SNode_Impl::Delete
//    if ( RetVal ) {
//      RetVal = DataFlowEditor()->IsValid() ;
//    }
  }
  endService( "ESNode_Impl::Delete" );
  return RetVal ;
}


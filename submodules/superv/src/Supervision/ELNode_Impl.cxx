//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : ELNode_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "ELNode_Impl.hxx"

ELNode_Impl::ELNode_Impl() {
}

ELNode_Impl::ELNode_Impl( CORBA::ORB_ptr orb ,
	                PortableServer::POA_ptr poa ,
	                PortableServer::ObjectId * contId , 
	                const char *instanceName ,
                        const char *interfaceName ,
                        GraphEditor::DataFlow * aDataFlowEditor ,
                        const char * EndName , 
                        const SUPERV::KindOfNode NodeKindOfNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , EndName , SUPERV::ListOfStrings() , NodeKindOfNode ) {
//  MESSAGE( NodeName << " " );
  beginService( "ELNode_Impl::ELNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
//  DataFlowNode( DataFlowEditor()->AddNode( InitFunction , MoreFunction , NextFunction ,
//                                           NodeName , NodeKindOfNode ) ) ;
//  DataFlowNode( DataFlowEditor()->AddNode( SALOME_ModuleCatalog::Service() ,
//                                           "" , "" , "" ,
//                                           NodeKindOfNode ) ) ;
  endService( "ELNode_Impl::ELNode_Impl" );  
}

ELNode_Impl::ELNode_Impl( CORBA::ORB_ptr orb ,
	              PortableServer::POA_ptr poa ,
	              PortableServer::ObjectId * contId , 
	              const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * aDataFlowEditor ,
                      GraphEditor::InNode * aDataFlowNode ) :
//  Engines_Component_i(orb, poa, contId, instanceName, interfaceName, false, false) {
  GNode_Impl( orb , poa , contId , instanceName , interfaceName , aDataFlowEditor , aDataFlowNode ) {
  MESSAGE( DataFlowNode()->Name() << " " );
  beginService( "ELNode_Impl::ELNode_Impl" );
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  endService( "ELNode_Impl::ELNode_Impl" );  
}

ELNode_Impl::~ELNode_Impl() {
  beginService( "ELNode_Impl::~ELNode_Impl" );
  endService( "ELNode_Impl::~ELNode_Impl" );
}

void ELNode_Impl::destroy() {
  beginService( "ELNode_Impl::Destroy" );
  if ( DataFlowEditor()->IsEditing() ) {
//    SUPERV::GNode_ptr aCoupled = SUPERV::GNode::_narrow( Coupled() ) ;
    if ( Delete() ) {
      _poa->deactivate_object(*_id) ;
//      CORBA::release(_poa) ;
      delete(_id) ;
//      _thisObj->_remove_ref();
    }
    else {
      MESSAGE("ELNode_Impl::destroy ERROR ") ;
    }
// destroy have to be used for the LoopNode ==> destroy of the EndLoopNode
//    if ( !CORBA::is_nil( aCoupled ) ) {
//      aCoupled->SetCoupled( "" ) ;
//      aCoupled->destroy() ;
//    }
  }
  endService( "ELNode_Impl::Destroy" );
}

bool ELNode_Impl::Delete() {
  beginService( "ELNode_Impl::Delete" );
  bool RetVal = false ;
  if ( DataFlowEditor()->IsEditing() ) {
    DeletePorts() ;
    RetVal = DataFlowEditor()->RemoveNode( Name() ) ;
// IsValid is done in LNode_Impl::Delete
//    if ( RetVal ) {
//      RetVal = DataFlowEditor()->IsValid() ;
//    }
  }
  endService( "ELNode_Impl::Delete" );
  return RetVal ;
}

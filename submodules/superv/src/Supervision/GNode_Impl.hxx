//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : GNode_Impl.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _GNODE_IMPL_HXX_
#define _GNODE_IMPL_HXX_

//#include <iostream.h>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include "SALOME_Component_i.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "INode_Impl.hxx"
#include "Port_Impl.hxx"

#include "DataFlowEditor_DataFlow.hxx"
#include "DataFlowExecutor_DataFlow.hxx"

class GNode_Impl : public INode_Impl ,
                   public POA_SUPERV::GNode {
  private:

    CORBA::ORB_ptr             _Orb ;
    PortableServer::POA_ptr    _Poa ;
    PortableServer::ObjectId * _ContId ;

  public:
    GNode_Impl();
    GNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                const char *aDataFlowName ,
                const SUPERV::KindOfNode aKindOfNode ) ;
    GNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                const char * FuncName , 
                const SUPERV::ListOfStrings & PythonFunction ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::GOTONode ) ;
    GNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                GraphBase::ListOfFuncName FuncNameList , 
                GraphBase::ListOfPythonFunctions PythonFunctionList ,
                const SUPERV::KindOfNode NodeKindOfNode = SUPERV::GOTONode ) ;
    GNode_Impl( CORBA::ORB_ptr orb ,
	        PortableServer::POA_ptr poa ,
	        PortableServer::ObjectId * contId , 
	        const char *instanceName ,
                const char *interfaceName ,
                GraphEditor::DataFlow * DataFlowEditor ,
                GraphEditor::InNode * DataFlowNode ) ;
    virtual ~GNode_Impl() ;
    virtual void destroy() ;
    virtual bool Delete() ;

    virtual bool SetCoupled( const char * anInLineNode );

    virtual SUPERV::INode_ptr Coupled() ;

    virtual GraphBase::GOTONode * BaseNode() {
            return DataFlowNode()->GOTONode() ; } ;

} ;

#endif

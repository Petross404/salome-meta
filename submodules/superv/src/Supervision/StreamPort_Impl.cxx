//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : StreamPort_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

using namespace std;

#include "StreamPort_Impl.hxx"

StreamPort_Impl::StreamPort_Impl( CORBA::ORB_ptr orb ,
		                  PortableServer::POA_ptr poa ,
	       	                  PortableServer::ObjectId * contId , 
		                  const char *instanceName ,
                                  const char *interfaceName ,
                                  GraphEditor::DataFlow * DataFlowEditor ,
                                  GraphEditor::InNode * DataFlowNode ,
                                  GraphBase::Port * DataFlowPort ,
                                  bool InputPort ,
                                  const CORBA::Any * anAny ) :
  Port_Impl( orb , poa , contId , instanceName , interfaceName ,
             DataFlowEditor , DataFlowNode , DataFlowPort , InputPort , anAny ) {
//  MESSAGE("StreamPort_Impl::StreamPort_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
}

StreamPort_Impl::StreamPort_Impl( CORBA::ORB_ptr orb ,
		      PortableServer::POA_ptr poa ,
	       	      PortableServer::ObjectId * contId , 
		      const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * DataFlowEditor ,
                      GraphEditor::InNode * DataFlowNode ,
                      GraphBase::Port * DataFlowPort ,
                      bool InputPort ) :
  Port_Impl( orb , poa , contId , instanceName , interfaceName ,
             DataFlowEditor , DataFlowNode , DataFlowPort , InputPort ) {
//  MESSAGE("StreamPort_Impl::StreamPorte_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName << ") --> "
//          << hex << (void *) this << dec )
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  _thisObj = this ;
  _id = _poa->activate_object(_thisObj);
}

StreamPort_Impl::StreamPort_Impl() {
}

StreamPort_Impl::~StreamPort_Impl() {
  beginService( "StreamPort_Impl::~StreamPort_Impl" );
  endService( "StreamPort_Impl::~StreamPort_Impl" );
}

void StreamPort_Impl::destroy() {
  beginService( "StreamPort_Impl::destroy" );
  bool destroyed = false ;
  if ( DataFlowNode()->IsOneOfInLineNodes() ) {
    if ( IsInput() ) {
      GraphBase::InPort * anInPort = (GraphBase::InPort * ) DataFlowPort() ;
      if ( anInPort->GetOutPort() ) {
        anInPort->GetOutPort()->RemoveInPort( anInPort ) ;
      }
      DataFlowNode()->DelInPort( DataFlowPort()->PortName() ) ;
      destroyed = true ;
    }
    else {
      DataFlowNode()->DelOutPort( DataFlowPort()->PortName() ) ;
      destroyed = true ;
    }
    if ( destroyed ) {
      DataFlowEditor()->UnValid() ;
    }
  }
  if ( destroyed ) {
    _poa->deactivate_object(*_id) ;
//    CORBA::release(_poa) ;
    delete(_id) ;
//    _thisObj->_remove_ref();
  }
  endService( "StreamPort_Impl::destroy" );
}


SUPERV::StreamLink_ptr StreamPort_Impl::StreamLink() {
  bool begin = true ;
  SUPERV::StreamLink_var iobject = SUPERV::StreamLink::_nil() ;
  char * FromNodeName = NULL ;
  char * FromServiceParameterName = NULL ;
  bool status = DataFlowEditor()->GetLink( DataFlowNode()->Name() ,
                                           DataFlowPort()->PortName() ,
                                           & FromNodeName ,
                                           & FromServiceParameterName ) ;
  if ( status ) {
//    cout << "Port_Impl::Link " << FromNodeName << "("
//         << FromServiceParameterName << ") --> " << _DataFlowNode->Name()
//         << "(" << _DataFlowPort->PortName() << ")" << endl ;
//    SUPERV::Port_ptr theOutPort = Port( FromServiceParameterName ) ;

    GraphBase::InPort * anInPort = DataFlowNode()->GetChangeInPort( DataFlowPort()->PortName() ) ;
    if ( anInPort->GetOutPort() ) {
      GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
      if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
        if ( begin ) {
          beginService( "Port_Impl::Link" ) ;
          begin = false ;
        }
        bool Success ;
        StreamLink_Impl * myStreamLink = new StreamLink_Impl( _Orb , _Poa , _ContId ,
                                    instanceName() , interfaceName() ,
                                    DataFlowEditor() ,
                                    DataFlowNode() ,
                                    DataFlowPort()->PortName() ,
                                    (GraphEditor::InNode * ) DataFlowEditor()->Graph()->GetChangeGraphNode( FromNodeName )->GetInNode() ,
                                    FromServiceParameterName ,
                                    false , Success ) ;
        if ( Success ) {
          PortableServer::ObjectId * id = myStreamLink->getId() ;
          CORBA::Object_var obj = _poa->id_to_reference(*id);
          iobject = SUPERV::StreamLink::_narrow(obj) ;
          anOutPort->AddInPortObjRef( anInPort , SUPERV::StreamLink::_duplicate( iobject ) ) ;
	}
      }
      else {
        iobject = SUPERV::StreamLink::_narrow( anOutPort->InPortObjRef( anInPort ) ) ;
      }
    }
  }
  if ( !begin ) {
    MESSAGE( "Link " << iobject->OutPort()->Node()->Name() << "( " << iobject->OutPort()->Name() << " ) --> "
             << iobject->InPort()->Node()->Name() << "( " << iobject->InPort()->Name() << " ) " ) ;
    endService( "StreamPort_Impl::StreamLink" ) ;
  }
  return SUPERV::StreamLink::_duplicate( iobject ) ;
}

SALOME_ModuleCatalog::DataStreamDependency StreamPort_Impl::Dependency() {
//  beginService( "StreamPort_Impl::Dependency" );
  SALOME_ModuleCatalog::DataStreamDependency  RetVal = DataFlowPort()->Dependency() ;
//  endService( "StreamPort_Impl::Dependency" );
  return RetVal ;
}

bool StreamPort_Impl::SetDependency( const SALOME_ModuleCatalog::DataStreamDependency aDependency ) {
//  beginService( "StreamPort_Impl::SetDependency" );
  DataFlowPort()->Dependency( aDependency ) ;
//  endService( "StreamPort_Impl::SetDependency" );
  return true ;
}

bool StreamPort_Impl::SetParams( const SUPERV::KindOfSchema aKindOfSchema ,
                                 const SUPERV::KindOfInterpolation aKindOfInterpolation ,
                                 const SUPERV::KindOfExtrapolation aKindOfExtrapolation ) {
//  beginService( "StreamPort_Impl::SetParams" );
  bool RetVal = false ;
  if ( IsInput() ) {
    GraphBase::InDataStreamPort * anInStreamPort = (GraphBase::InDataStreamPort * ) DataFlowPort() ;
    RetVal = anInStreamPort->SetParams( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
    if ( RetVal ) {
      MESSAGE( "OutStreamPort_Impl::SetParams( " << aKindOfSchema << " , " << aKindOfInterpolation
               << " , " << aKindOfExtrapolation << " ) SUCCESS " ) ;
      DataFlowEditor()->UnValid() ;
    }
    else {
      MESSAGE( "OutStreamPort_Impl::SetParams( " << aKindOfSchema << " , " << aKindOfInterpolation
               << " , " << aKindOfExtrapolation << " ) FAILED " ) ;
    }
  }
  else {
    MESSAGE( "OutStreamPort_Impl::SetParams( " << aKindOfSchema << " , " << aKindOfInterpolation
             << " , " << aKindOfExtrapolation << " ) ERROR : OutPort" ) ;
  }
//  endService( "StreamPort_Impl::SetParams" );
  return RetVal ;
}

bool StreamPort_Impl::Params( SUPERV::KindOfSchema & aKindOfSchema ,
                              SUPERV::KindOfInterpolation & aKindOfInterpolation ,
                              SUPERV::KindOfExtrapolation & aKindOfExtrapolation ) {
//  beginService( "StreamPort_Impl::Params" );
  bool RetVal = false ;
  if ( IsInput() ) {
    GraphBase::InDataStreamPort * anInStreamPort = (GraphBase::InDataStreamPort * ) DataFlowPort() ;
    anInStreamPort->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
    MESSAGE( "InStreamPort_Impl::Params( " << aKindOfSchema << " , " << aKindOfInterpolation << " , " << aKindOfExtrapolation << " )" ) ;
    RetVal = true ;
  }
  else {
    aKindOfSchema = (SUPERV::KindOfSchema ) -1 ;
    aKindOfInterpolation = (SUPERV::KindOfInterpolation ) -1 ;
    aKindOfExtrapolation = (SUPERV::KindOfExtrapolation ) -1 ;
    MESSAGE( "OutStreamPort_Impl::Params( " << aKindOfSchema << " , " << aKindOfInterpolation
             << " , " << aKindOfExtrapolation << " ) ERROR: OutPort" ) ;
  }
//  endService( "StreamPort_Impl::Params" );
  return RetVal ;
}

bool StreamPort_Impl::SetNumberOfValues( CORBA::Long aNumberOfValues ) {
//  beginService( "StreamPort_Impl::SetNumberOfValues" );
  bool RetVal = false ;
  if ( !IsInput() ) {
    GraphBase::OutDataStreamPort * anOutStreamPort = (GraphBase::OutDataStreamPort * ) DataFlowPort() ;
    anOutStreamPort->NumberOfValues( aNumberOfValues ) ;
    DataFlowEditor()->UnValid() ;
    RetVal = true ;
  }
  else {
    MESSAGE( "OutStreamPort_Impl::SetNumberOfValues( " << aNumberOfValues << " ) ERROR : InPort" ) ;
  }
//  endService( "StreamPort_Impl::SetNumberOfValues" );
  return RetVal ;
}

CORBA::Long StreamPort_Impl::NumberOfValues() {
//  beginService( "StreamPort_Impl::NumberOfValues" );
  CORBA::Long RetVal = -1 ;
  if ( !IsInput() ) {
    GraphBase::OutDataStreamPort * anOutStreamPort = (GraphBase::OutDataStreamPort * ) DataFlowPort() ;
    RetVal = anOutStreamPort->NumberOfValues() ;
    MESSAGE( "OutStreamPort_Impl::NumberOfValues() " << RetVal ) ;
  }
  else {
    MESSAGE( "OutStreamPort_Impl::NumberOfValues() ERROR : InPort" ) ;
  }
//  endService( "StreamPort_Impl::NumberOfValues" );
  return RetVal ;
}


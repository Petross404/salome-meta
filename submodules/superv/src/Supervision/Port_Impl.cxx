//  SUPERV Supervision : contains the implementation of interfaces of SuperVision described in SUPERV.idl
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : Port_Impl.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header: 

using namespace std;
#include <stdio.h>
#include <fstream>
//#include <sstream>
#include <string>

//#include "utilities.h"

#include "Graph_Impl.hxx"
#include "Port_Impl.hxx"

Port_Impl::Port_Impl( CORBA::ORB_ptr orb ,
		      PortableServer::POA_ptr poa ,
	       	      PortableServer::ObjectId * contId , 
		      const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * DataFlowEditor ,
                      GraphEditor::InNode * DataFlowNode ,
                      GraphBase::Port * DataFlowPort ,
                      bool InputPort ,
                      const CORBA::Any * anAny ) :
  Value_Impl(orb, poa, contId, instanceName, interfaceName,
             DataFlowEditor , DataFlowNode , DataFlowPort->PortName(), anAny ,
             false ) {
//  MESSAGE("Port_Impl::Port_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName
//          << ") IsDataStream " << DataFlowPort->IsDataStream() )
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  if ( !DataFlowPort->IsDataStream() ) {
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj) ;
  }
  _DataFlowEditor = DataFlowEditor ;
  _DataFlowNode = DataFlowNode ;
  _DataFlowPort = DataFlowPort ;
  _InputPort = InputPort ;
  InPort( _InputPort ) ;
}

Port_Impl::Port_Impl( CORBA::ORB_ptr orb ,
		      PortableServer::POA_ptr poa ,
	       	      PortableServer::ObjectId * contId , 
		      const char *instanceName ,
                      const char *interfaceName ,
                      GraphEditor::DataFlow * DataFlowEditor ,
                      GraphEditor::InNode * DataFlowNode ,
                      GraphBase::Port * DataFlowPort ,
                      bool InputPort ) :
  Value_Impl(orb, poa, contId, instanceName, interfaceName,
             DataFlowEditor , DataFlowNode , DataFlowPort->PortName() , false ) {
  char * nodename = DataFlowEditor->Graph()->Name() ;
  if ( DataFlowNode ) {
    nodename = DataFlowNode->Name() ;
  }
//  MESSAGE("Port_Impl::Port_Impl activate object instanceName("
//          << instanceName << ") interfaceName(" << interfaceName
//          << ") " << nodename << "(" << DataFlowPort->PortName()
//          << ") IsDataStream " << DataFlowPort->IsDataStream() ) ;
  _Orb = CORBA::ORB::_duplicate(orb);
  _Poa = poa ;
  _ContId = contId ;
  if ( !DataFlowPort->IsDataStream() ) {
    _thisObj = this ;
    _id = _poa->activate_object(_thisObj) ;
  }
  _DataFlowEditor = DataFlowEditor ;
  _DataFlowNode = DataFlowNode ;
  _DataFlowPort = DataFlowPort ;
  _InputPort = InputPort ;
  InPort( _InputPort ) ;
}

Port_Impl::Port_Impl() {
}

Port_Impl::~Port_Impl() {
  beginService( "Port_Impl::~Port_Impl" );
  endService( "Port_Impl::~Port_Impl" );
}

void Port_Impl::destroy() {
  beginService( "Port_Impl::destroy" );
  bool destroyed = false ;
  if ( _DataFlowNode->IsOneOfInLineNodes() ) {
    if ( _InputPort ) {
//      GraphBase::InPort * anInPort = (GraphBase::InPort * ) _DataFlowPort ;
//      if ( anInPort->GetOutPort() ) {
//        anInPort->GetOutPort()->RemoveInPort( anInPort ) ;
//      }
      if ( _DataFlowNode->IsOneOfInLineNodes() ) {
        _DataFlowNode->DelInPort( _DataFlowPort->PortName() ) ;
        if ( _DataFlowNode->IsLoopNode() ) {
          _DataFlowNode->DelOutPort( _DataFlowPort->PortName() ) ;
          _DataFlowNode->CoupledNode()->DelInPort( _DataFlowPort->PortName() ) ;
          _DataFlowNode->CoupledNode()->DelOutPort( _DataFlowPort->PortName() ) ;
        }
      }
      destroyed = true ;
    }
    else if ( _DataFlowNode->IsOneOfInLineNodes() && !_DataFlowNode->IsLoopNode() ) {
      _DataFlowNode->DelOutPort( _DataFlowPort->PortName() ) ;
      destroyed = true ;
    }
    else {
      destroyed = true ;
    }
    if ( destroyed ) {
      _DataFlowEditor->UnValid() ;
    }
  }
  if ( destroyed ) {
    _poa->deactivate_object(*_id) ;
//    CORBA::release(_poa) ;
    delete(_id) ;
//    _thisObj->_remove_ref();
  }
  endService( "Port_Impl::destroy" );
}

// Only during delete of an InLineNode (because of Loop/EndLoop and Switch/EndSwitch
// CoupledNodes) ; look at destroy above ...
void Port_Impl::Remove() {
  beginService( "Port_Impl::Remove" );
  if ( _DataFlowNode->IsOneOfInLineNodes() ) {
    if ( _InputPort ) {
      _DataFlowNode->DelInPort( _DataFlowPort->PortName() ) ;
    }
    else {
      _DataFlowNode->DelOutPort( _DataFlowPort->PortName() ) ;
    }
    _poa->deactivate_object(*_id) ;
//  CORBA::release(_poa) ;
    delete(_id) ;
//    _thisObj->_remove_ref();
  }
  endService( "Port_Impl::Remove" );
}

bool Port_Impl::Input( const SUPERV::Value_ptr aValue ) {
  return Input( aValue->ToAny() ) ;
}

bool Port_Impl::Input( const CORBA::Any * anAny ) {
  beginService( "Port_Impl::Input" );
  bool RetVal = false ;
  MESSAGE( "Port_Impl::Input(CORBA::Any *) : " << _DataFlowNode->Name() << "( "
           << _DataFlowPort->PortName() << " ) " ) ;
  if ( _DataFlowEditor->Graph()->GraphMacroLevel() == 0 ) {
    if ( _InputPort && _DataFlowEditor->IsEditing() ) {
      RetVal = true ;
      Value_Impl::Input( anAny ) ;
      RetVal = _DataFlowEditor->AddInputData( _DataFlowNode->Name() ,
                                              _DataFlowPort->PortName() ,
                                              *anAny ) ;
    }
    else if ( _DataFlowEditor->IsExecuting() ) {
      GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
      
      /* commenting out the lines below because it seems to be never executed
         Editor::_Executing flag is set OFF in Executor::OutNode on execution finished
         and in CNode_Impl class (also not called probably)
      // asv: turn off Editor::_Executing flag
      if ( _DataFlowExecutor->IsDone() ) {
        _DataFlowEditor->Executing( false );
      }
      else 
      */ 
      if ( !_DataFlowExecutor->IsDone() ) {
        RetVal = _DataFlowExecutor->ChangeInputData( _DataFlowNode->Name() ,
                                                     _DataFlowPort->PortName() ,
                                                     *anAny ) ;
      }
    }
  }
  if ( !RetVal ) {
    MESSAGE( "Port_Impl::Input returns false _InputPort " << _InputPort << " IsEditing "
             << _DataFlowEditor->IsEditing() << " Executor " << _DataFlowEditor->Executor() ) ;
  }
  endService( "Port_Impl::Input" );
  return RetVal ;
}

char * Port_Impl::Name() {
//  beginService( "Port_Impl::Name" ) ;
  const char * RetVal = _DataFlowPort->PortName() ;
//  endService( "Port_Impl::Name" ) ;
  return CORBA::string_dup( RetVal ) ;
}

char * Port_Impl::Type() {
//  beginService( "Port_Impl::Type" ) ;
  const char * RetVal = _DataFlowPort->PortType() ;
//  endService( "Port_Impl::Type" ) ;
  return CORBA::string_dup( RetVal ) ;
}

SUPERV::CNode_ptr Port_Impl::Node() {
  bool begin = true ;
  SUPERV::CNode_var iobject ;
  if ( _DataFlowNode ) {
//    MESSAGE( "Port_Impl::Node " << _DataFlowNode->Name() ) ;
    iobject = SUPERV::CNode::_narrow( _DataFlowNode->ObjRef() ) ;
    if ( CORBA::is_nil( iobject ) ) {
      if ( begin ) {
        beginService( "Port_Impl::Node" ) ;
        begin = false ;
      }
      PortableServer::ObjectId * id ;
      CORBA::Object_var obj ;
      if ( _DataFlowNode->IsComputingNode() ) {
        CNode_Impl * myNode = NULL ;
        myNode = new CNode_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 _DataFlowEditor ,
                                 _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        iobject = SUPERV::CNode::_narrow( obj ) ;
        myNode->SetObjRef( SUPERV::CNode::_duplicate( iobject ) ) ;
      }
      else if ( _DataFlowNode->IsFactoryNode() ) {
        FNode_Impl * myNode = NULL ;
        myNode = new FNode_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 _DataFlowEditor ,
                                 _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::FNode_var Fiobject = SUPERV::FNode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( Fiobject ) ;
        myNode->SetObjRef( SUPERV::FNode::_duplicate( Fiobject ) ) ;
      }
      else if ( _DataFlowNode->IsInLineNode() ) {
        INode_Impl * myNode = NULL ;
        myNode = new INode_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 _DataFlowEditor ,
                                 _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::INode_var Iiobject = SUPERV::INode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( Iiobject ) ;
        myNode->SetObjRef( SUPERV::INode::_duplicate( Iiobject ) ) ;
      }
      else if ( _DataFlowNode->IsGOTONode() ) {
        GNode_Impl * myNode = NULL ;
        myNode = new GNode_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 _DataFlowEditor ,
                                 _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::GNode_var Giobject = SUPERV::GNode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( Giobject ) ;
        myNode->SetObjRef( SUPERV::GNode::_duplicate( Giobject ) ) ;
      }
      else if ( _DataFlowNode->IsLoopNode() ) {
        LNode_Impl * myNode = NULL ;
        myNode = new LNode_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 _DataFlowEditor ,
                                 _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::LNode_var Liobject = SUPERV::LNode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( Liobject ) ;
        myNode->SetObjRef( SUPERV::LNode::_duplicate( Liobject ) ) ;
      }
      else if ( _DataFlowNode->IsEndLoopNode() ) {
        ELNode_Impl * myNode = NULL ;
        myNode = new ELNode_Impl( _Orb , _Poa , _ContId ,
                                  instanceName() , interfaceName() ,
                                  _DataFlowEditor ,
                                  _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::ELNode_var ELiobject = SUPERV::ELNode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( ELiobject ) ;
        myNode->SetObjRef( SUPERV::ELNode::_duplicate( ELiobject ) ) ;
      }
      else if ( _DataFlowNode->IsSwitchNode() ) {
        SNode_Impl * myNode = NULL ;
        myNode = new SNode_Impl( _Orb , _Poa , _ContId ,
                                 instanceName() , interfaceName() ,
                                 _DataFlowEditor ,
                                 _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::SNode_var Siobject = SUPERV::SNode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( Siobject ) ;
        myNode->SetObjRef( SUPERV::SNode::_duplicate( Siobject ) ) ;
      }
      else if ( _DataFlowNode->IsEndSwitchNode() ) {
        ESNode_Impl * myNode = NULL ;
        myNode = new ESNode_Impl( _Orb , _Poa , _ContId ,
                                  instanceName() , interfaceName() ,
                                  _DataFlowEditor ,
                                  _DataFlowNode ) ;
        id = myNode->getId() ;
        obj = _poa->id_to_reference(*id);
        SUPERV::ESNode_var ESiobject = SUPERV::ESNode::_narrow( obj ) ;
        iobject = SUPERV::CNode::_narrow( ESiobject ) ;
        myNode->SetObjRef( SUPERV::ESNode::_duplicate( ESiobject ) ) ;
      }
    }
    else {
//      MESSAGE( "Port_Impl::Node known objref " << iobject->Name() ) ;
    }
  }
  else {
    MESSAGE( "Port_Impl::GraphNode " << _DataFlowEditor->Graph()->Name() ) ;
    iobject = SUPERV::CNode::_narrow( _DataFlowEditor->Graph()->ObjRef() ) ;
    if ( CORBA::is_nil( iobject ) ) {
      MESSAGE( "Port_Impl::GraphNode NilRef" ) ;
    }
  }
  if ( !begin ) {
    endService( "Port_Impl::Node" ) ;
  }
  return SUPERV::CNode::_duplicate( iobject ) ;
}

SUPERV::ListOfLinks * Port_Impl::Links() {
  beginService( "Port_Impl::Links" ) ;
  SUPERV::ListOfLinks_var RetVal = new SUPERV::ListOfLinks ;
  RetVal = _DataFlowEditor->Graph()->ObjImpl()->Links( /*_DataFlowNode->ComputingNode() ,
							 _DataFlowPort->PortName()*/ ) ;
  MESSAGE( "Links of Node " << _DataFlowNode->ComputingNode()->Name()
           << " and of Port " << _DataFlowPort->PortName()
           << " _InputPort " << _InputPort ) ;
  int i ;
  for ( i = 0 ; i < (int ) RetVal->length() ; i++ ) {
    MESSAGE( "Link " << RetVal[i]->OutPort()->Node()->Name() << "("
             << RetVal[i]->OutPort()->Name() << ") --> "
             << RetVal[i]->InPort()->Node()->Name() << "("
             << RetVal[i]->InPort()->Name() << ")" ) ;
  }
  endService( "Port_Impl::Links" ) ;
  return ( RetVal._retn() ) ;
}

SUPERV::Link_ptr Port_Impl::Link() {
  bool begin = true ;
  SUPERV::Link_var iobject = SUPERV::Link::_nil() ;
  if ( _DataFlowNode->IsEndSwitchNode() ) {
    beginService( "Port_Impl::Link" ) ;
    begin = false ;
    SUPERV::ListOfLinks_var Links = new SUPERV::ListOfLinks ;
//    cout << "Port_Impl::Link " << _DataFlowNode->Name() << " "
//         << _DataFlowPort->PortName() << endl ;
    Links = _DataFlowEditor->Graph()->ObjImpl()->Links( /*_DataFlowNode->ComputingNode() ,
							  _DataFlowPort->PortName()*/ ) ;
    if ( Links->length() ) {
      iobject = Links[ 0 ] ;
    }
  }
  else {
    char * FromNodeName = NULL ;
    char * FromServiceParameterName = NULL ;
    bool status = _DataFlowEditor->GetLink( _DataFlowNode->Name() ,
                                            _DataFlowPort->PortName() ,
                                            & FromNodeName ,
                                            & FromServiceParameterName ) ;
    if ( status ) {
//      cout << "Port_Impl::Link " << FromNodeName << "("
//           << FromServiceParameterName << ") --> " << _DataFlowNode->Name()
//           << "(" << _DataFlowPort->PortName() << ")" << endl ;
//      SUPERV::Port_ptr theOutPort = Port( FromServiceParameterName ) ;

      GraphBase::InPort * anInPort = _DataFlowNode->GetChangeInPort( _DataFlowPort->PortName() ) ;
      if ( anInPort->GetOutPort() ) {
        GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
        if ( _DataFlowNode->IsEndLoopNode() && !strcmp( _DataFlowNode->CoupledNode()->Name() ,
                                                        anOutPort->NodeName() ) ) {
//          MESSAGE( "Link " << anOutPort->NodeName() << "("
//                  << anOutPort->PortName() << ") --> " << _DataFlowNode->Name() << "("
//                  << anInPort->PortName() << ")" << " ignored" ) ;
	}
        else if ( CORBA::is_nil( anOutPort->InPortObjRef( anInPort ) ) ) {
          if ( begin ) {
            beginService( "Port_Impl::Link" ) ;
            begin = false ;
	  }
          bool Success ;
          Link_Impl * myLink = new Link_Impl( _Orb , _Poa , _ContId ,
                                    instanceName() , interfaceName() ,
                                    _DataFlowEditor ,
                                    _DataFlowNode ,
                                    _DataFlowPort->PortName() ,
                                    (GraphEditor::InNode * ) _DataFlowEditor->Graph()->GetChangeGraphNode( FromNodeName )->GetInNode() ,
                                    FromServiceParameterName ,
                                    false , true , Success ) ;
          if ( Success ) {
            PortableServer::ObjectId * id = myLink->getId() ;
            CORBA::Object_var obj = _poa->id_to_reference(*id);
            iobject = SUPERV::Link::_narrow(obj) ;
            anOutPort->AddInPortObjRef( anInPort , SUPERV::Link::_duplicate( iobject ) ) ;
	  }
        }
        else {
          iobject = anOutPort->InPortObjRef( anInPort ) ;
        }
      }
    }
  }
  if ( !begin ) {
    MESSAGE( "Link " << iobject->OutPort()->Node()->Name() << "( " << iobject->OutPort()->Name() << " ) --> "
             << iobject->InPort()->Node()->Name() << "( " << iobject->InPort()->Name() << " ) " ) ;
    endService( "Port_Impl::Link" ) ;
  }
  return SUPERV::Link::_duplicate( iobject ) ;
}

bool Port_Impl::IsInput() {
//  beginService( "Port_Impl::IsInput" ) ;
  bool RetVal = _InputPort ;
//  MESSAGE( Name() << "IsInput() " << _InputPort ) ;
//  endService( "Port_Impl::IsInput" ) ;
  return RetVal ;
}

bool Port_Impl::IsLinked() {
//  beginService( "Port_Impl::IsLinked" ) ;
  bool RetVal = false ;
  if ( _DataFlowNode ) {
    GraphBase::InPort * anInPort = _DataFlowNode->GetChangeInPort( _DataFlowPort->PortName() ) ;
    GraphBase::OutPort * anOutPort = NULL ;
    if ( anInPort) {
      anOutPort = anInPort->GetOutPort() ;
    }
    if ( _DataFlowNode->IsEndLoopNode() && anOutPort &&
         !strcmp( _DataFlowNode->CoupledNode()->Name() , anOutPort->NodeName() ) ) {
//      MESSAGE( "Link " << anOutPort->NodeName() << "("
//              << anOutPort->PortName() << ") --> " << _DataFlowNode->Name() << "("
//              << anInPort->PortName() << ")" << " ignored" ) ;
    }
    else {
      RetVal = _DataFlowNode->IsLinked( _DataFlowPort->PortName(), IsInput() ) ; // mkr : PAL8060
    }
  }
//  endService( "Port_Impl::IsLinked" ) ;
  return RetVal ;
}

SUPERV::KindOfPort Port_Impl::Kind() {
//  beginService( "Port_Impl::Kind" ) ;
  SUPERV::KindOfPort RetVal = _DataFlowPort->Kind() ;
//  endService( "Port_Impl::Kind" ) ;
  return RetVal ;
}

void Port_Impl::SetKind( SUPERV::KindOfPort aKindOfPort ) {
//  beginService( "Port_Impl::SetKind" ) ;
  _DataFlowPort->Kind( aKindOfPort ) ;
//  endService( "Port_Impl::SetKind" ) ;
}

bool Port_Impl::IsParam() {
//  beginService( "Port_Impl::IsParam" ) ;
  bool RetVal = _DataFlowPort->IsParam() ;
//  endService( "Port_Impl::IsParam" ) ;
  return RetVal ;
}

bool Port_Impl::IsGate() {
//  beginService( "Port_Impl::IsGate" ) ;
  bool RetVal = _DataFlowPort->IsGate() ;
//  endService( "Port_Impl::IsGate" ) ;
  return RetVal ;
}

bool Port_Impl::IsInLine() {
//  beginService( "Port_Impl::IsInLine" ) ;
//  bool RetVal = _DataFlowPort->IsBus() ;
  bool RetVal = _DataFlowPort->IsInLine() ;
//  endService( "Port_Impl::IsInLine" ) ;
  return RetVal ;
}

bool Port_Impl::IsLoop() {
//  beginService( "Port_Impl::IsLoop" ) ;
  bool RetVal = _DataFlowPort->IsLoop() ;
//  endService( "Port_Impl::IsLoop" ) ;
  return RetVal ;
}

bool Port_Impl::IsSwitch() {
//  beginService( "Port_Impl::IsSwitch" ) ;
  bool RetVal = _DataFlowPort->IsSwitch() ;
//  endService( "Port_Impl::IsSwitch" ) ;
  return RetVal ;
}

bool Port_Impl::IsEndSwitch() {
//  beginService( "Port_Impl::IsEndSwitch" ) ;
  bool RetVal = _DataFlowPort->IsEndSwitch() ;
//  endService( "Port_Impl::IsEndSwitch" ) ;
  return RetVal ;
}

bool Port_Impl::IsDataStream() {
//  beginService( "Port_Impl::IsDataStream" ) ;
  bool RetVal = _DataFlowPort->IsDataStream() ;
//  endService( "Port_Impl::IsDataStream" ) ;
  return RetVal ;
}

bool Port_Impl::HasInput() {
//  beginService( "Port_Impl::" ) ;
  bool RetVal = false ;
  if ( _DataFlowNode ) {
    RetVal = _DataFlowNode->HasInput( _DataFlowPort->PortName() ) ;
//    cout << "Port_Impl::HasInput " << RetVal << " NodeName " << _DataFlowPort->NodeName() << " PortName "
//         << _DataFlowPort->PortName() << endl ;
  }
//  endService( "Port_Impl::" ) ;
  return RetVal ;
}

SUPERV::GraphState Port_Impl::State() {
//  beginService( "Port_Impl::State" );
  SUPERV::GraphState RetVal = SUPERV::EditingState ;
  if ( _DataFlowEditor ) {
    GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
    if ( _DataFlowExecutor ) {
      RetVal = _DataFlowExecutor->State( _DataFlowPort->NodeName() ,
                                         _DataFlowPort->PortName() ) ;
    }
  }
//  endService( "Port_Impl::State" );
  return RetVal ;
}

bool Port_Impl::IsDone() {
//  beginService( "Port_Impl::IsDone" );
  bool RetVal = false ;
  if ( _DataFlowEditor ) {
    GraphExecutor::DataFlow * _DataFlowExecutor = _DataFlowEditor->Executor() ;
    if ( _DataFlowExecutor ) {
      RetVal = _DataFlowExecutor->PortDone( _DataFlowPort->NodeName() ,
                                            _DataFlowPort->PortName() ) ;
    }
  }
//  endService( "Port_Impl::IsDone" );
  return RetVal ;
}


//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_InPort.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_INPORT_HXX
#define _DATAFLOWBASE_INPORT_HXX

#include "DataFlowBase_Port.hxx"

namespace GraphBase {

  class OutPort ;

  class InPort : public Port {

    private:

      SUPERV::GraphState   _PortState ;
      OutPort *            _OutPort ;
      OutPort *            _InitialOutPort ;
      bool                 _BranchOfSwitchLinked ; // For Check from Branches to EndOfSwitch links

    public:   

      InPort() ;
      InPort( const char *const * NodeName  ,
              const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
              const SUPERV::KindOfPort aKind = SUPERV::ServiceParameter ,
              const SALOME_ModuleCatalog::DataStreamDependency aDependency = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ) ;
      virtual ~InPort() {
         cdebug << "GraphBase::InPort::~InPort " << PortName() << endl ; } ;
      virtual void destroy() {
         _OutPort = NULL ;
         _InitialOutPort = NULL ;
         cdebug << "GraphBase::InPort::destroy " << PortName() << " "
                << NodeName() << endl ; } ;

      void PortState( SUPERV::GraphState aPortState ) {
//           cdebug << pthread_self() << " " << PortName() << " from "
//                  << NodeName() << " SUPERV::GraphState " << _State << " "
//                  << " --> " << aPortState << " _EndSwitchPort "
//                  << IsEndSwitch() << endl ;
           _PortState = aPortState ; } ;
      SUPERV::GraphState PortState() { return _PortState ; } ;

      OutPort * GetOutPort() {
           return _OutPort ; } ;
      OutPort * GetOutPort() const {
           return _OutPort ; } ;
      const StatusOfPort PortStatus() const ;

      bool IsNotConnected() const ;
//      bool IsConnected() const ;
      bool IsPortConnected() const ;
      bool IsDataConnected() const ;
      bool IsExternConnected() const ;

      bool AddOutPort( OutPort * anOutPort ) {
           if ( _OutPort )
             return false ;
           _OutPort = anOutPort ;
           return true ; } ;
      bool ChangeOutPort( OutPort * anOutPort ) ;
      bool InitialOutPort() {
           if ( _InitialOutPort ) {
             _OutPort = _InitialOutPort ;
             _InitialOutPort = NULL ;
             return true ;
	   }
           return false ; } ;
      bool RemoveOutPort( bool DoRemoveInPort = true ) ;

      bool BranchOfSwitchLinked( bool aInPortLinked ) {
           if ( aInPortLinked && _BranchOfSwitchLinked ) {
             return false ; // Already setted
	   }
           _BranchOfSwitchLinked = aInPortLinked ;
           return true ; } ;
      bool BranchOfSwitchLinked() {
           return _BranchOfSwitchLinked ; } ;

      void StringValue(ostream & f ) const ;

  } ;

} ;

ostream & operator<< (ostream &,const GraphBase::InPort &);

ostream & operator<< (ostream &,const SUPERV::GraphState &);

#endif

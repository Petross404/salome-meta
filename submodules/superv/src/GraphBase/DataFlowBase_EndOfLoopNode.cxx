//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_EndOfLoopNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_EndOfLoopNode.hxx"

GraphBase::EndOfLoopNode::EndOfLoopNode() :
  GraphBase::GOTONode::GOTONode() {

  cdebug << "GraphBase::EndOfLoopNode::EndOfLoopNode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;

}

GraphBase::EndOfLoopNode::EndOfLoopNode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService* ptrNamingService ,
                               const char * FuncName ,
                               const SUPERV::ListOfStrings & anInitPythonFunction ,
                               const char *NodeName ,
                               const SUPERV::KindOfNode akind ,
                               const SUPERV::SDate NodeFirstCreation ,
                               const SUPERV::SDate NodeLastModification  ,
                               const char * NodeEditorRelease ,
                               const char * NodeAuthor ,
                               const char * NodeComment ,
                               const bool   GeneratedName ,
                               const long   X ,
                               const long   Y ,
                               int * Graph_prof_debug ,
                               ofstream * Graph_fdebug ) :
  GraphBase::GOTONode::GOTONode( ORB , ptrNamingService , FuncName , 
                                 anInitPythonFunction ,
                                 NodeName , akind , NodeFirstCreation ,
                                 NodeLastModification  , NodeEditorRelease ,
                                 NodeAuthor , NodeComment , GeneratedName ,
                                 X , Y , Graph_prof_debug , Graph_fdebug ) {

  cdebug_in << "GraphBase::EndOfLoopNode::EndOfLoopNode "  << this 
            << "' _Name " << (void *) Name() << " '" << Name() << " _Comment "
            << (void *) Comment() << " "  << Comment() << " " 
            << " KindOfNode " << Kind() << " FuncName " << FuncName
            << " ServiceName " << ServiceName() << " In("
            << ServiceInParameter().length()
            << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  cdebug_out << "GraphBase::EndOfLoopNode::EndOfLoopNode" << endl;
}

GraphBase::EndOfLoopNode::~EndOfLoopNode() {
  cdebug << "GraphBase::Node::~Node "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;
//  if ( _ComponentName != NULLSTRING )
//    delete [] _ComponentName ;
//  delete [] _Name ;
//  delete [] _EditorRelease ;
//  if ( _Author != NULLSTRING )
//    delete [] _Author ;
//  if ( _Computer != FACTORYSERVER )
//    delete [] _Computer;
//  if ( Comment() != NULLSTRING )
//    delete [] Comment();
}


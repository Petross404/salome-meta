//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_Port.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
#include "DataFlowBase_Port.hxx"

//GraphBase::Port::~Port() {
//  cout << "Port::~Port()" << endl ;
//}

bool GraphBase::Port::AddCoord( const int nxy , const int *x ,
                                const int *y ) {
  int i ;
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    _X.resize( nxy ) ;
    _Y.resize( nxy ) ;
    for ( i = 0 ; i < nxy ; i++ ) {
      _X[ i ] = x[ i ] ;
      _Y[ i ] = y[ i ] ;
    }
  }
  return true ;
}

bool GraphBase::Port::AddCoord( const int index ,
                                const int x , const int y ) {
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    if ( index <= 0 || index > (int ) _X.size()+1 )
      return false ;
    _X.resize( _X.size()+1 ) ;
    _Y.resize( _Y.size()+1 ) ;
    int i ;
    for ( i = _X.size() - 1 ; i >= index  ; i-- ) {
      _X[ i ] = _X[ i-1 ] ;
      _Y[ i ] = _Y[ i-1 ] ;
    }
    _X[ index - 1 ] = x ;
    _Y[ index - 1 ] = y ;
//    cdebug << "AddCoord " << NodeName() << "(" << PortName() << ") ["
//           << index-1 << "] " << x << " " << y << endl ;
//    for ( i = 0 ; i <  _X.size() ; i++ ) {
//      cdebug << " [" << i << "] " << _X[ i ] << " " << _Y[ i ] << endl ;
//    }
  }
  return true ;
}

bool GraphBase::Port::ChangeCoord( const int index ,
                                   const int x ,
                                   const int y ) {
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    if ( index <= 0 || index > (int ) _X.size() )
      return false ;
    _X[ index - 1 ] = x ;
    _Y[ index - 1 ] = y ;
//    cdebug << "ChangeCoord " << NodeName() << "(" << PortName() << ") ["
//           << index-1 << "] " << x << " " << y << endl ;
//    int i ;
//    for ( i = 0 ; i <  _X.size() ; i++ ) {
//      cdebug << " [" << i << "] " << _X[ i ] << " " << _Y[ i ] << endl ;
//    }
  }
  return true ;
}

bool GraphBase::Port::RemoveCoord( const int index ) {
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    if ( index <= 0 || index > (int ) _X.size() )
      return false ;
    int i ;
    for ( i = index - 1 ; i < (int ) _X.size() - 1 ; i++ ) {
      _X[ i ] = _X[ i+1 ] ;
      _Y[ i ] = _Y[ i+1 ] ;
    }
    _X.resize( _X.size()-1 ) ;
    _Y.resize( _Y.size()-1 ) ;
  }
  return true ;
}

bool GraphBase::Port::RemoveCoords() {
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    _X.resize( 0 ) ;
    _Y.resize( 0 ) ;
  }
  return true ;
}

int GraphBase::Port::GetCoord() const {
  int npt ;
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    npt = _X.size() ;
  }
  return npt ;
}

bool GraphBase::Port::GetCoord( int *x , int *y ) const {
  int i ;
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    for ( i = 0 ; i < (int ) _X.size() ; i++ ) {
      x[ i ] = _X[ i ] ;
      y[ i ] = _Y[ i ] ;
    }
  }
  return true ;
}

const GraphBase::ListOfCoords * GraphBase::Port::Coords() const {
  GraphBase::ListOfCoords * _list_Coords = new GraphBase::ListOfCoords ;
  int i ;
  if ( !IsEndSwitch() ) {
    _list_Coords->resize( _X.size() );
    for ( i = 0 ; i < (int ) _X.size() ; i++ ) {
      (*_list_Coords)[ i ].theX = _X[ i ] ;
      (*_list_Coords)[ i ].theY = _Y[ i ] ;
    }
  }
  return _list_Coords ;
}

bool GraphBase::Port::GetCoord( const int index , CORBA::Long &x , CORBA::Long &y ) const {
  if ( IsEndSwitch() ) {
    return false ;
  }
  else {
    if ( index <= 0 || index > (int ) _X.size() )
      return false ;
    x = _X[ index - 1 ] ;
    y = _Y[ index - 1 ] ;
//    cdebug << "GetCoord " << NodeName() << "(" << PortName() << ") ["
//           << index-1 << "] " << x << " " << y << endl ;
//    int i ;
//    for ( i = 0 ; i <  _X.size() ; i++ ) {
//      cdebug << " [" << i << "] " << _X[ i ] << " " << _Y[ i ] << endl ;
//    }
  }
  return true ;
}

ostream & operator<< (ostream & f ,const SUPERV::KindOfPort & s ) {
  switch (s) {
  case SUPERV::UndefinedParameter :
    f << "UndefinedParameter";
    break;
  case SUPERV::ServiceParameter :
    f << "ServiceParameter";
    break;
  case SUPERV::GateParameter :
    f << "GateParameter";
    break;
  case SUPERV::InLineParameter :
    f << "InLineParameter";
    break;
  case SUPERV::LoopParameter :
    f << "LoopParameter";
    break;
  case SUPERV::SwitchParameter :
    f << "SwitchParameter";
    break;
  case SUPERV::EndSwitchParameter :
    f << "EndSwitchParameter";
    break;
  case SUPERV::GOTOParameter :
    f << "GOTOParameter";
    break;
  case SUPERV::DataStreamParameter :
    f << "DataStreamParameter";
    break;
  default :
    f << "UnknownKindOfPort";
    break;
  }

  return f;
}

ostream & operator<< (ostream & f ,const SALOME_ModuleCatalog::DataStreamDependency & s ) {
  switch (s) {
  case SALOME_ModuleCatalog::DATASTREAM_UNDEFINED :
    f << "DATASTREAM_UNDEFINED";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_TEMPORAL :
    f << "DATASTREAM_TEMPORAL";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_ITERATIVE :
    f << "DATASTREAM_ITERATIVE";
    break;
  default :
    f << "DATASTREAM_?";
    break;
  }

  return f;
}

ostream & operator<< (ostream & f ,const StatusOfPort & s ) {
  switch (s) {
  case NotConnected :
    f << "NotConnected";
    break;
  case PortConnected :
    f << "PortConnected";
    break;
//  case PortAndDataConnected :
//    f << "PortAndDataConnected";
//    break;
  case DataConnected :
    f << "DataConnected";
    break;
  case ExternConnected :
    f << "ExternConnected";
    break;
  default :
    f << "UnknownStatusOfPort";
    break;
  }

  return f;
}

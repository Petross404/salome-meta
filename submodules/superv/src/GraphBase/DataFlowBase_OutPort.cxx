//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_OutPort.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_OutPort.hxx"
#include "DataFlowBase_InPort.hxx"

//GraphBase::OutPort::~OutPort() {
// In GraphExecutor::OutPort
//  int i ;
//  for ( i = 1 ; i <= _InPortsSize  ; i++ ) {
//    if ( !_InPorts[ i ]->RemoveLink() )
//      cout << "GraphBase::OutPort::~OutPort error "
//           << _InPorts[ i ]->PortName() << endl ;
//  }
//}

bool GraphBase::OutPort::HasInPort() const {
  return _InPortsSize != 0 ;
}

const GraphBase::InPort * GraphBase::OutPort::GetInPort(
                         const GraphBase::InPort * toPort ) {
  const GraphBase::InPort * anInPort = NULL ;
  int index = _MapOfInPorts[ toPort->NodePortName() ] ;
  if ( index > 0 && index <= _InPortsSize ) {
    anInPort = _InPorts[ index-1 ] ;
    if ( anInPort != toPort ) {
  //cout << "GraphBase::OutPort::GetInPort inconsistency toPort "
//           << hex << (void *) toPort << " != anInPort " << (void *) anInPort
//           << dec << endl ;
      anInPort = NULL ;
    }
  }
  return anInPort ;
}

SUPERV::Link_var GraphBase::OutPort::InPortObjRef(
                         const GraphBase::InPort * toPort ) {
  SUPERV::Link_var aLink = SUPERV::Link::_nil() ;
  int index = _MapOfInPorts[ toPort->NodePortName() ] ;
  if ( index > 0 && index <= _InPortsSize ) {
    aLink = _Links[ index-1 ] ;
  }
  return aLink ;
}

bool GraphBase::OutPort::AddInPort( GraphBase::InPort * toPort ) {
  int index = _MapOfInPorts[ toPort->NodePortName() ] ;
  if ( index > 0 && index <= _InPortsSize ) {
    return false ;
  }
  _InPortsSize += 1 ;
  _InPorts.resize( _InPortsSize ) ;
  _MapOfInPorts[ toPort->NodePortName() ] = _InPortsSize ;
  _InPorts[ _InPortsSize-1 ] = toPort ;
  _Links.resize( _InPortsSize ) ;
  _Links[ _InPortsSize-1 ] = SUPERV::Link::_nil() ;
  cdebug << "OutPort " << NodeName() << "(" << PortName() << ") --> InPort "
         << toPort->NodeName() << "(" << toPort->PortName() << ") SwitchPort "
         << toPort->IsEndSwitch() << endl ;
  return true ;
}

bool GraphBase::OutPort::AddInPortObjRef( GraphBase::InPort * toPort ,
                                          SUPERV::Link_var aLink ) {
  int index = _MapOfInPorts[ toPort->NodePortName() ] ;
  if ( index <= 0 || index > _InPortsSize ) {
    return false ;
  }
  _Links[ index - 1 ] = aLink ;
  return true ;
}

bool GraphBase::OutPort::RemoveInPort() {
  if ( _InPortsSize != 1 ) {
    return false ;
  }
  int index = 1 ;
  GraphBase::InPort * toPort = _InPorts[ index - 1 ] ;
  _InPortsSize -= 1 ;
  _InPorts.resize( _InPortsSize ) ;
  _Links.resize( _InPortsSize ) ;
  _MapOfInPorts.erase( toPort->NodePortName() ) ;
  if ( _InPortsSize == 0 ) {
    PortStatus(NotConnected ) ;
    if ( IsSwitch() ) {
      Kind( SUPERV::InLineParameter ) ;
    }
  }
  if ( toPort->GetOutPort() ) {
    toPort->RemoveOutPort( false ) ;
  }
  cdebug << "OutPort::RemoveInPort " << NodeName() << "( " << PortName() << " " << PortStatus()
         << " " << Kind() << " ) _InPortsSize " << _InPortsSize << " --> "
         << toPort->NodeName() << "( " << toPort->PortName() << " " << toPort->PortStatus()
         << " " << toPort->Kind() << " )" << endl ;
  return true ;
}

bool GraphBase::OutPort::RemoveInPort( GraphBase::InPort * toPort ) {
  int i ;
  int index = _MapOfInPorts[ toPort->NodePortName() ] ;
  if ( index <= 0 || index > _InPortsSize ) {
//JR NPAL14110 09.02.2007 : Not an error with MacroNodes ...
    //JRcdebug << "GraphBase::OutPort::RemoveInPort Error " << NodeName() << "( " << PortName()<< " ) --> "
    //JR       << toPort->NodeName() << "( " << toPort->PortName() << " )" << endl ;
    return false ;
  }
  cdebug << "OutPort::RemoveInPort " << NodeName() << "(" << PortName() << ") --> "
         << _InPorts[ index - 1 ]->NodeName() << "(" << _InPorts[ index - 1 ]->PortName() << ")" << endl ;
  _InPortsSize -= 1 ;
  for ( i = index - 1 ; i < _InPortsSize  ; i++ ) {
    _MapOfInPorts[ _InPorts[ i+1 ]->NodePortName() ] = i + 1 ;
    _InPorts[ i ] = _InPorts[ i+1 ]  ;
    _Links[ i ] = _Links[ i+1 ]  ;
  }
  _InPorts.resize( _InPortsSize ) ;
  _Links.resize( _InPortsSize ) ;
  _MapOfInPorts.erase( toPort->NodePortName() ) ;
  if ( _InPortsSize == 0 ) {
    PortStatus(NotConnected ) ;
    if ( IsSwitch() ) {
      Kind( SUPERV::InLineParameter ) ;
    }
  }
  if ( toPort->GetOutPort() ) {
    toPort->RemoveOutPort( false ) ;
  }
  return true ;
}

bool GraphBase::OutPort::ReNameInPort( const char* OldNodePortName ,
                                       const char* NewNodePortName ) {
  int index = _MapOfInPorts[ OldNodePortName ] ;
  if ( index <= 0 || index > _InPortsSize ) {
    cdebug << "GraphBase::OutPort::ReNameInPort Error in Node " << NodeName() << " " << OldNodePortName
           << " --> " << NewNodePortName << " index " << index << endl ;
    return false ;
  }
  _MapOfInPorts.erase( OldNodePortName ) ;
  _MapOfInPorts[ NewNodePortName ] = index ;
  return true ;
}

#if 0
bool GraphBase::OutPort::RemoveLinks() {
  bool RetVal = true ;
  int i ;
  for ( i = 0 ; i < _InPortsSize ; i++ ) {
    MESSAGE( "       to " << *_InPorts[ i ] );
    RetVal = _InPorts[ i ]->RemoveLink() ;
    if ( !RetVal )
      break ;
    RetVal = RemoveLink( _InPorts[ i ] ) ;
    if ( !RetVal )
      break ;
  }
  PortStatus(NotConnected ) ;
  return RetVal ;
}
#endif

ostream & operator<< (ostream &f ,const GraphBase::OutPort &P)
{
 f << P.PortName() << ", " << "type : " << P.PortType() << ", " ;
 f << "kind " << P.Kind() << ", ";
 f << "Status " << P.PortStatus() << ", ";
 f << "from Node " << P.NodeName() << ", ";
  return f;
}


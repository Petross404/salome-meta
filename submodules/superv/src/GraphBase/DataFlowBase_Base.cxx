//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Base.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_Base.hxx"

char *SuperVision_Version = "3.0" ;

char *NULLSTRING = "" ;

char *FACTORYSERVER = "localhost/FactoryServer" ;
char *FACTORYSERVERPY = "localhost/FactoryServerPy" ;

GraphBase::Base::Base() {
  _prof_debug = NULL ;
  _fdebug = NULL ;
}

void GraphBase::Base::SetDebug( CORBA::ORB_ptr ORB ,
                                int * theprof_debug , ofstream * thefdebug ) {
  if ( _prof_debug == NULL ) {
//    *thefdebug << "GraphBase::Base::SetDebug Done _fdebug " << _fdebug << " = thefdebug " << thefdebug
//           << endl ;
    if ( theprof_debug ) {
      _Orb = CORBA::ORB::_duplicate( ORB ) ;
      _prof_debug = theprof_debug ;
      _fdebug = thefdebug ;
    }
    else {
      MESSAGE( "GraphBase::Base::SetDebug with theprof_debug == NULL" ) ;
    }
  }
  else {
    cdebug << "GraphBase::Base::SetDebug already done" << endl ;
  }
//  cdebug_in << "GraphBase::Base::SetDebug" << endl ;
//  cdebug << "GraphBase::Base::SetDebug" << endl ;
//  cdebug_out << "GraphBase::Base::SetDebug" << endl ;
}

char * GraphBase::Base::ObjectToString( CORBA::Object_ptr obj ) const {
  return _Orb->object_to_string( obj );
}

CORBA::Object_ptr GraphBase::Base::StringToObject( const char * obj ) const {
  return _Orb->string_to_object( obj );
}

ostream & GraphBase::Base::AnyValue( const CORBA::Any & anAny ) {
  switch (anAny.type()->kind()) {
    case CORBA::tk_string: {
      const char * t;
      anAny >>= t;
      *_fdebug << " " << t << " (tk_string)" << endl ;
      break;
    }
    case CORBA::tk_boolean: {
      bool b ;
      anAny >>= (CORBA::Any::to_boolean ) b;
      *_fdebug << " " << b << " (tk_boolean)" << endl ;
      break;
    }
    case CORBA::tk_char: {
      unsigned char c ;
      anAny >>= (CORBA::Any::to_char ) c;
      *_fdebug << " " << c << "(tk_char)" << endl ;
      break;
    }
    case CORBA::tk_short: {
      short s;
      anAny >>= s;
      *_fdebug << " " << s << "(tk_short)" << endl ;
      break;
    }
    case CORBA::tk_long: {
      CORBA::Long l;
      anAny >>= l;
      *_fdebug << " " << l << "(tk_long)" << endl ;
      break;
    }
    case CORBA::tk_float: {
      float f;
      anAny >>= f;
      *_fdebug << " " << f << "(tk_float)" << endl ;
      break;
    }
    case CORBA::tk_double: {
      double d;
      anAny >>= d;
      *_fdebug << " " << d << "(tk_double)" << endl ;
      break;
    }
    case CORBA::tk_objref: {
      try {
        CORBA::Object_ptr obj ;
#if OMNIORB_VERSION >= 4
        anAny >>= (CORBA::Any::to_object ) obj ;
#else
        anAny >>= obj ;
#endif
        if ( CORBA::is_nil( obj ) ) {
          *_fdebug << "CORBA::Object_ptr " << obj << " nil reference (tk_object)"
                   << endl ;
        }
        else {
          *_fdebug << "CORBA::Object_ptr " << obj << "(tk_object)" << endl ;
        }
        try {
          char * retstr ;
          retstr = ObjectToString( obj ) ;
          *_fdebug << retstr << "(tk_object)" << endl ;
        }
        catch( ... ) {
          *_fdebug << "ObjectToString( CORBA::Object_ptr ) Catched ERROR" << endl ;
        }
      }
      catch( ... ) {
        *_fdebug << "anAny >>= CORBA::Object_ptr ( tk_object ) Catched ERROR" << endl ;
      }
      break;
    }
    default: {
      *_fdebug << " " << "(other ERROR)" << endl ;
    }
  }
  return *_fdebug ;
}

/*
string DataStreamTypeToString( const SALOME_ModuleCatalog::DataStreamType aDataStreamType ) {
  string aIdlType ;
  switch ( aDataStreamType ) {
  case SALOME_ModuleCatalog::DATASTREAM_UNKNOWN : {
    aIdlType = "Unknown" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_INTEGER : {
    aIdlType = "int" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_FLOAT : {
    aIdlType = "float" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_DOUBLE : {
    aIdlType = "double" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_STRING : {
    aIdlType = "string" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_BOOLEAN : {
    aIdlType = "bool" ;
    break;
  }
  default: {
    aIdlType = "Unknown" ;
    break;
  }
  }
  return aIdlType ;
}

SALOME_ModuleCatalog::DataStreamType StringToDataStreamType( const char * aIdlType ) {
  SALOME_ModuleCatalog::DataStreamType aDataStreamType ;
  if ( !strcmp( aIdlType ,  "Unknown" ) ) {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_UNKNOWN ;
  }
  else if ( !strcmp( aIdlType ,  "int" ) ) {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_INTEGER ;
  }
  else if ( !strcmp( aIdlType ,  "float" ) ) {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_FLOAT ;
  }
  else if ( !strcmp( aIdlType ,  "double" ) ) {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_DOUBLE ;
  }
  else if ( !strcmp( aIdlType ,  "string" ) ) {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_STRING ;
  }
  else if ( !strcmp( aIdlType ,  "bool" ) ) {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_BOOLEAN ;
  }
  else {
    aDataStreamType = SALOME_ModuleCatalog::DATASTREAM_UNKNOWN ;
  }
  return aDataStreamType ;
}
*/
int StringToDataStreamType (const char * aIdlType)
{
  int aDataStreamType;
  if      (!strcmp(aIdlType, "Unknown")) aDataStreamType = 0; //SALOME_ModuleCatalog::DATASTREAM_UNKNOWN
  else if (!strcmp(aIdlType, "int"))     aDataStreamType = 1; //SALOME_ModuleCatalog::DATASTREAM_INTEGER
  else if (!strcmp(aIdlType, "float"))   aDataStreamType = 2; //SALOME_ModuleCatalog::DATASTREAM_FLOAT
  else if (!strcmp(aIdlType, "double"))  aDataStreamType = 3; //SALOME_ModuleCatalog::DATASTREAM_DOUBLE
  else if (!strcmp(aIdlType, "string"))  aDataStreamType = 4; //SALOME_ModuleCatalog::DATASTREAM_STRING
  else if (!strcmp(aIdlType, "bool"))    aDataStreamType = 5; //SALOME_ModuleCatalog::DATASTREAM_BOOLEAN
  else                                   aDataStreamType = 0; //SALOME_ModuleCatalog::DATASTREAM_UNKNOWN
  return aDataStreamType;
}

string KindOfDataStreamTraceToString( SUPERV::KindOfDataStreamTrace aDataStreamTrace ) {
  string aTrace ;
  switch ( aDataStreamTrace ) {
  case SUPERV::WithoutTrace :
    aTrace = "SANS";
    break;
  case SUPERV::SummaryTrace :
    aTrace = "SUCCINT";
    break;
  case SUPERV::DetailedTrace :
    aTrace = "DETAILLE";
    break;
  default :
    aTrace = "UndefinedTrace";
    break;
  }
  return aTrace ;
}

string DataStreamDependencyToString( const SALOME_ModuleCatalog::DataStreamDependency aDataStreamDependency ) {
  string aDependency ;
  switch ( aDataStreamDependency ) {
  case SALOME_ModuleCatalog::DATASTREAM_UNDEFINED :
    aDependency = "U" ;
    break;
  case SALOME_ModuleCatalog::DATASTREAM_TEMPORAL :
    aDependency = "T" ;
    break;
  case SALOME_ModuleCatalog::DATASTREAM_ITERATIVE :
    aDependency = "I" ;
    break;
  default :
    aDependency = "?" ;
    break;
  }

  return aDependency;
}

string DataStreamToString (const char* aDataStreamType)
{
  string aStreamType ;
  string type (aDataStreamType);

  if      (type == "Unknown") aStreamType = "Unknown";
  else if (type == "int")     aStreamType = "ENTIER";
  else if (type == "float")   aStreamType = "REEL";
  else if (type == "double")  aStreamType = "DOUBLE";
  else if (type == "string")  aStreamType = "CHAINE";
  else if (type == "bool")    aStreamType = "LOGIQUE";
  else                        aStreamType = "?";

  return aStreamType;
}

/*
string DataStreamToString( const char* aDataStreamType ) {
  string aStreamType ;
  switch ( aDataStreamType ) {
  case SALOME_ModuleCatalog::DATASTREAM_UNKNOWN : {
    aStreamType = "Unknown" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_INTEGER : {
    aStreamType = "ENTIER" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_FLOAT : {
    aStreamType = "REEL" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_DOUBLE : {
    aStreamType = "DOUBLE" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_STRING : {
    aStreamType = "CHAINE" ;
    break;
  }
  case SALOME_ModuleCatalog::DATASTREAM_BOOLEAN : {
    aStreamType = "LOGIQUE" ;
    break;
  }
  default: {
    aStreamType = "?" ;
    break;
  }
  }
  return aStreamType ;
}

ostream & operator<< (ostream & f ,const SALOME_ModuleCatalog::DataStreamType & s ) {
  switch (s) {
  case SALOME_ModuleCatalog::DATASTREAM_UNKNOWN :
    f << "DATASTREAM_UNKNOWN";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_INTEGER :
    f << "DATASTREAM_INTEGER";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_FLOAT :
    f << "DATASTREAM_FLOAT";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_DOUBLE :
    f << "DATASTREAM_DOUBLE";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_STRING :
    f << "DATASTREAM_STRING";
    break;
  case SALOME_ModuleCatalog::DATASTREAM_BOOLEAN :
    f << "DATASTREAM_BOOLEAN";
    break;
  default :
    f << "DATASTREAM_UNKNOWN";
    break;
  }

  return f;
}
*/

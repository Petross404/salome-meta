//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_XmlHandler.cxx
//  Author : Arnaud RES
//  Module : SUPERV
//  $Header$

using namespace std;
#include "DataFlowBase_XmlHandler.hxx"

#include <qstring.h>
//#include <qxmlattributes.h>
#include <qxml.h>

#define TRACE 0

//static string NULLSTRING = string("") ;

GraphBase::XmlHandler::XmlHandler( CORBA::ORB_ptr Orb ,
                                   const bool aConstructor ) :
  _Orb( Orb ) , dataflowxml(false), depth(0), constructor(aConstructor) {
  int i ;
  for ( i = 0 ; i < maxlevel ; i++ ) {
    fieldname[i] = NULLSTRING ;
    fieldvalue[i] = NULLSTRING ;
    step[i] = 0 ;
  }
  VXSize = 0 ;
  GraphsNumber = 0 ;

  aLinkValue = CORBA::Any();
}

GraphBase::XmlHandler::XmlHandler() {}

GraphBase::XmlHandler::~XmlHandler() {}

QString GraphBase::XmlHandler::errorProtocol() {
  return errorProt;
}

bool GraphBase::XmlHandler::startDocument() {
  MESSAGE( "====================startDocument " ) ;
// at the beginning of parsing: do some initialization
  errorProt = "";
  return TRUE;
}

bool GraphBase::XmlHandler::endDocument() {
  MESSAGE( "====================endDocument " ) ;
  return TRUE;
}

bool GraphBase::XmlHandler::startElement( const QString&, const QString&, 
				          const QString& qName, 
				          const QXmlAttributes& atts ) {
  // do different actions depending on the name of the tag and the
  // state you are in

#if TRACE
  MESSAGE( "====================startElement " << depth << " " << qName)
   
#endif

  if ( ( qName == "dataflow" || qName == "supergraph" ) && depth == 0 ) {
    // Dataflow detected
    aListOfDataFlows.resize( GraphsNumber + 1 ) ;
    int i ;
    for ( i = 0 ; i < maxlevel ; i++ ) {
      fieldname[i] = NULLSTRING ;
      fieldvalue[i] = NULLSTRING ;
      step[i] = 0 ;
    }
    VXSize = 0 ;
    dataflowxml = TRUE;
  }
  if ( dataflowxml ) {
    if ( qName == "supergraph" ) {
      fieldname[depth] = qName.latin1() ;
    }
    else {
      fieldname[depth++] = qName.latin1() ;
    }
  }
  if ( depth == maxlevel+1 )
    return false ;
  return dataflowxml ;

}

static bool returnfalse( GraphBase::XmlHandler *myXmlHandler ,
                         const char *msg ,
                         const QString& qName ) {
  MESSAGE( "returnfalse ERROR qName " << qName << " " << msg
           << " fieldname " << myXmlHandler->getfieldname(myXmlHandler->getdepth())
           << " fieldvalue '"
           << myXmlHandler->getfieldvalue(myXmlHandler->getdepth()) << "'" )
  return false ;
}

bool GraphBase::XmlHandler::endElement( const QString&,
                                        const QString&,
                                        const QString& qName) {

  if ( qName == "supergraph" ) {
#if TRACE
    MESSAGE( " ====================endElement supergraph" ) ;
#endif
    return TRUE ;
  }

// ERRORs :
// * On a eu endElement(dataflow)
// * Ou bien on a eu uniquement startElement(dataflow) et qName != fieldname et fieldvalue != NULLSTRING
  if ( !dataflowxml ||
       ( qName != QString( fieldname[depth].c_str() ) &&
         fieldvalue[depth] != NULLSTRING ) ) {
    MESSAGE( "endElement ERROR dataflowxml " << dataflowxml << " qName " << qName
             << " fieldname[" << depth << "] "<< fieldname[depth] << " fieldvalue "
             << fieldvalue[depth] )
    return returnfalse( this , "top" , qName ) ;
  }
  if ( fieldvalue[depth] == NULLSTRING ) {
    depth -= 1 ;
  }

#if TRACE
  MESSAGE( " ====================endElement step[" << depth << "]="
           << step[depth] << " qName " << qName << " fieldvalue '"
           << fieldvalue[depth] << "'")
#endif
  switch ( depth ) {
    case 0 : {
      if ( step[depth] == 0 && qName == "dataflow" ) {
        GraphsNumber += 1 ;
        dataflowxml = false ;
      }
      else
        return returnfalse( this , "depth0" , qName ) ;
      break ;
    }

    case 1 : {
      switch ( step[depth] ) {
        case 0 :
          if ( qName == "info-list" ) {
//            aListOfDataFlows[ GraphsNumber ].Info = aNode ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth1-0" , qName ) ;
          break ;
        case 1 :
          if ( qName == "node-list" ) {
// node-list ok
            step[depth]++ ;
            step[3] = 16 ;
            step[4] = 3 ;
          }
          else
            return returnfalse( this , "depth1-1" , qName ) ;
          break ;
        case 2 :
          if ( qName == "link-list" ) {
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth1-2" , qName ) ;
          break ;
        case 3 :
          if ( qName == "data-list" ) {
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth1-3" , qName ) ;
          break ;
        default:
          return returnfalse( this , "depth1-d" , qName ) ;
          break ;
        }
      break ;
    }

    case 2 : {
      switch ( step[depth] ) {
        case 0 :
          if ( qName == "node" ) {
// Node ok
            if ( step[1] == 0 ) {
              aListOfDataFlows[ GraphsNumber ].Info = aNode ;
	    }
            else if ( step[1] == 1 ) {
              int sizenode = aListOfDataFlows[ GraphsNumber ].Nodes.size() ;
              aListOfDataFlows[ GraphsNumber ].Nodes.resize( sizenode+1 ) ;                
              aListOfDataFlows[ GraphsNumber ].Nodes[ sizenode ] = aNode ;               
	    }
            step[3] = 0 ;
            aNode.theService.ServiceinParameter.length( 0 ) ;
            aNode.theService.ServiceoutParameter.length( 0 ) ;
            aNode.theListOfInDataStreams.resize( 0 ) ;
            aNode.theListOfOutDataStreams.resize( 0 ) ;
            aNode.theListOfFuncName.resize( 0 ) ;
            aNode.theListOfPythonFunctions.resize( 0 ) ;
            break ;
          }
          else if ( qName == "link" || qName == "data" ) {
            step[depth]++ ;
          }
          else {
            return returnfalse( this , "depth2-0" , qName ) ;
            break ;
	  }
        case 1 :
          if ( qName == "link" ) {
            int sizelink = aListOfDataFlows[ GraphsNumber ].Links.size() ;
            aListOfDataFlows[ GraphsNumber ].Links.resize( sizelink+1 ) ;                
            aListOfDataFlows[ GraphsNumber ].Links[ sizelink ] = aLink ;               
            if ( VXSize ) {
              aListOfDataFlows[ GraphsNumber ].Links[ sizelink ].aListOfCoords.resize( VXSize ) ;
              int ic ;
              for ( ic = 0 ; ic < VXSize ; ic++ ) {
                aListOfDataFlows[ GraphsNumber ].Links[ sizelink ].aListOfCoords[ ic ].theX = VX[ic] ;
                aListOfDataFlows[ GraphsNumber ].Links[ sizelink ].aListOfCoords[ ic ].theY = VY[ic] ;
	      }
	    }
            VXSize = 0 ;
            step[3] = 16 ;
            break ;
          }
          else if ( qName == "data" ) {
            step[depth]++ ;
          }
          else {
            return returnfalse( this , "depth2-1" , qName ) ;
            break ;
	  }
        case 2 :
          if ( qName == "data" ) {
// Data ok
            if ( constructor ) {
              int sizedata = aListOfDataFlows[ GraphsNumber ].Datas.size() ;
              aListOfDataFlows[ GraphsNumber ].Datas.resize( sizedata+1 ) ;                

	      if ( aLink.aLinkValue.type()->kind() == CORBA::tk_double ) // mkr : PAL12235
		aLink.aLinkValue = *aLinkDoubleValue ;

	      aListOfDataFlows[ GraphsNumber ].Datas[ sizedata ] = aLink ;               

              if ( VXSize ) {
                aListOfDataFlows[ GraphsNumber ].Datas[ sizedata ].aListOfCoords.resize( VXSize ) ;
                int ic ;
                for ( ic = 0 ; ic < VXSize ; ic++ ) {
                  aListOfDataFlows[ GraphsNumber ].Datas[ sizedata ].aListOfCoords[ ic ].theX = VX[ic] ;
                  aListOfDataFlows[ GraphsNumber ].Datas[ sizedata ].aListOfCoords[ ic ].theY = VY[ic] ;
	        }
	      }
	    }
            VXSize = 0 ;
            step[3] = 16 ;
          }
          else
            return returnfalse( this , "depth2-2" , qName ) ;
          break ;
        default:
          return returnfalse( this , "depth2-d" , qName ) ;
          break ;
	}
      break ;
    }

    case 3 : {
      switch ( step[depth] ) {
        case 0 :
          if ( qName == "component-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-0" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theComponentName = fieldvalue[depth].c_str() ;
            else
              aNode.theComponentName = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-0" , qName ) ;
          break ;
        case 1 :
          if ( qName == "interface-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-1" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theInterfaceName = fieldvalue[depth].c_str() ;
            else
              aNode.theInterfaceName = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-1" , qName ) ;
          break ;
        case 2 :
          if ( qName == "node-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-2" , qName ) ;
            aNode.theName = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-2" , qName ) ;
          break ;
        case 3 :
          if ( qName == "kind" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-3" , qName ) ;
// kind ok
            sscanf( fieldvalue[depth].c_str() ,"%d" , (int * ) &aNode.theKind ) ;
            fieldvalue[depth] = NULLSTRING ;
            if ( aNode.theKind != SUPERV::DataStreamGraph ) {
              step[depth]++ ;
              step[4] = 0 ;
	    }
            break ;
          }
          else if ( qName == "streamgraph-timeout" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-3" , qName ) ;
            sscanf( fieldvalue[depth].c_str() ,"%d" , (int * ) &aNode.theTimeout ) ;
            fieldvalue[depth] = NULLSTRING ;
//              step[depth]++ ;
//              step[4] = 0 ;
            break ;
          }
          else if ( qName == "streamgraph-datastreamtrace" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-3" , qName ) ;
            sscanf( fieldvalue[depth].c_str() ,"%d" , (int * ) &aNode.theDataStreamTrace ) ;
            fieldvalue[depth] = NULLSTRING ;
//              step[depth]++ ;
//              step[4] = 0 ;
            break ;
          }
          else if ( qName == "streamgraph-deltatime" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-3" , qName ) ;
            sscanf( fieldvalue[depth].c_str() ,"%lf" , (double * ) &aNode.theDeltaTime ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[4] = 0 ;
            break ;
          }
          else {
            step[depth]++ ;
            step[4] = 0 ;
	  }
//            return returnfalse( this , "depth3-3" , qName ) ;
//          break ;
        case 4 :
          if ( qName == "coupled-node" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-4" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theCoupledNode = fieldvalue[depth].c_str() ;
            else
              aNode.theCoupledNode = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            break ;
          }
          else
            step[depth]++ ;
//            return returnfalse( this , "depth3-4" , qName ) ;
//          break ;
        case 5 :
          if ( qName == "service" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth3-5" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[4] = 6;
            step[5] = 4 ;
// service ok
          }
          else
            return returnfalse( this , "depth3-5" , qName ) ;
          break ;
        case 6 :
          if ( qName == "DataStream-list" || qName == "Parameter-list" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth3-6" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[4] = 8 ;
            step[5] = 8 ;
// Parameter-list ok
          break ;
          }
          else
            step[depth]++ ;
            step[5] = 0 ;
            step[6] = 0 ;
// NO Parameter-list
//            return returnfalse( this , "depth3-6" , qName ) ;
//          break ;
        case 7 :
          if ( qName == "PyFunction-list" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth3-7" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[5] = 0 ;
            step[6] = 0 ;
// Parameter-list ok
          break ;
          }
          else
            step[depth]++ ;
            step[5] = 0 ;
            step[6] = 0 ;
// NO PyFunction-list
//            return returnfalse( this , "depth3-7" , qName ) ;
//          break ;
        case 8 :
          if ( qName == "creation-date" ) {
            SUPERV::SDate D ;
            char Date[23] ;
            strcpy( Date , fieldvalue[depth].c_str() ) ;
            int i ;
            for ( i = 0 ; i < (int ) strlen(Date) ; i++ ) {
              if ( Date[i] == '/' || Date[i] == '-' || Date[i] == ':' )
                Date[i] = ' ' ;
	    }
//            MESSAGE( "Date(" << Date << ")" );
            int Day , Month , Year , Hour , Minute , Second ;
            sscanf( Date ,"%d%d%d%d%d%d" , &Day ,
                                           &Month ,
                                           &Year ,
                                           &Hour ,
                                           &Minute ,
                                           &Second ) ;
            D.Day = Day ;
            D.Month = Month ;
            D.Year = Year ;
            D.Hour = Hour ;
            D.Minute = Minute ;
            D.Second = Second ;
            aNode.theFirstCreation = D ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-8" , qName ) ;
          break ;
        case 9 :
          if ( qName == "lastmodification-date" ) {
            SUPERV::SDate D ;
            char Date[23] ;
            strcpy( Date , fieldvalue[depth].c_str() ) ;
            int i ;
            for ( i = 0 ; i < (int ) strlen(Date) ; i++ ) {
              if ( Date[i] == '/' || Date[i] == '-' || Date[i] == ':' )
                Date[i] = ' ' ;
	    }
//            MESSAGE( "Date(" << Date << ")" );
            int Day , Month , Year , Hour , Minute , Second ;
            sscanf( Date ,"%d%d%d%d%d%d" , &Day ,
                                           &Month ,
                                           &Year ,
                                           &Hour ,
                                           &Minute ,
                                           &Second ) ;
            D.Day = Day ;
            D.Month = Month ;
            D.Year = Year ;
            D.Hour = Hour ;
            D.Minute = Minute ;
            D.Second = Second ;
            aNode.theLastModification = D ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-9" , qName ) ;
          break ;
        case 10 :
          if ( qName == "editor-release" ) {
            aNode.theEditorRelease = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-10" , qName ) ;
          break ;
        case 11 :
          if ( qName == "author" ) {
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theAuthor = fieldvalue[depth].c_str() ;
            else
              aNode.theAuthor = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-11" , qName ) ;
          break ;
        case 12 :
          if ( qName == "container" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-12" , qName ) ;
// computer ok
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theContainer = fieldvalue[depth].c_str() ;
            else
              aNode.theContainer = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-12" , qName ) ;
          break ;
        case 13 :
          if ( qName == "comment" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-13" , qName ) ;
// comment ok
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theComment = fieldvalue[depth].c_str() ;
            else
              aNode.theComment = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-13" , qName ) ;
          break ;
        case 14 :
          if ( qName == "x-position" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-14" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%ld" , &aNode.theCoords.theX ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
// x-position ok
          }
          else
            return returnfalse( this , "depth3-14" , qName ) ;
          break ;
        case 15 :
          if ( qName == "y-position" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-15" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%ld" , &aNode.theCoords.theY ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
// y-position ok
          }
          else
            return returnfalse( this , "depth3-15" , qName ) ;
          break ;
        case 16 :
          if ( qName == "fromnode-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-16" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aLink.FromNodeName = fieldvalue[depth].c_str() ;
            else
              aLink.FromNodeName = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[4] = 3 ;
          }
          else
            return returnfalse( this , "depth3-16" , qName ) ;
          break ;
        case 17 :
          if ( qName == "fromserviceparameter-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-17" , qName ) ;
            aLink.FromServiceParameterName = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-17" , qName ) ;
          break ;
        case 18 :
          if ( qName == "tonode-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-18" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aLink.ToNodeName = fieldvalue[depth].c_str() ;
            else
              aLink.ToNodeName = "" ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-18" , qName ) ;
          break ;
        case 19 :
          if ( qName == "toserviceparameter-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth3-19" , qName ) ;
            aLink.ToServiceParameterName = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth3-19" , qName ) ;
          step[5] = 2 ;
          break ;
        case 20 :
          if ( qName == "data-value" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth3-20" , qName ) ;
// data-value ok
            step[depth]++ ;
//            step[4] = 3 ;
            break ;
          }
          else
            step[depth]++ ;
//            return returnfalse( this , "depth3-20" , qName ) ;
//          break ;
        case 21 :
          if ( qName == "coord-list" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth3-21" , qName ) ;
// coord-list ok
            step[depth] = 14 ;
            step[4] = 5 ;
          }
          else
            return returnfalse( this , "depth3-21" , qName ) ;
          break ;
        default:
          return returnfalse( this , "depth3-d" , qName ) ;
          break ;
	}
      break ;
    }


    case 4 : {
      switch ( step[depth] ) {
        case 0 :
          if ( qName == "service-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth4-0" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) )
              aNode.theService.ServiceName = fieldvalue[depth].c_str() ;
            else
              aNode.theService.ServiceName = "" ;
            aNode.theService.ServiceinParameter.length( 0 ) ;
            aNode.theService.ServiceoutParameter.length( 0 ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth4-0" , qName ) ;
          break ;
        case 1 :
          if ( qName == "inParameter-list" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth4-1" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[5] = 1 ;
            step[6] = 2 ; // First outParameter
// inParameterlist ok
          }
          else
            return returnfalse( this , "depth4-1" , qName ) ;
          break ;
        case 2 :
          if ( qName == "outParameter-list" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth4-2" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth] = 0 ;
// outParameterlist ok
            step[5] = 0 ;
            step[6] = 0 ;
          }
          else
            return returnfalse( this , "depth4-2" , qName ) ;
          break ;
        case 3 :
          if ( qName == "value-type" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth4-3" , qName ) ;
            int Kind ;
            sscanf( fieldvalue[depth].c_str() , "%d" , &Kind ) ;
//            switch ( D.Value.type()->kind() ) {
            aLink.aLinkValue = CORBA::Any() ;
            switch ( Kind ) {
            case CORBA::tk_string: {
//Mandrake 10.1/Salome 3              aLink.aLinkValue <<= (char *) NULL ;
              aLink.aLinkValue <<= (char *) "" ;
              const char * t;
              aLink.aLinkValue >>= t;
#if TRACE
              MESSAGE( t << " (string)" );
#endif
              break;
	    }
            case CORBA::tk_double: {
	      double d = 0.;
#ifdef REDHAT // mkr : debug for PAL12255
	      aLink.aLinkValue <<= (double ) 0. ;
#else
//JR	      aLink.aLinkValue.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
	      aLink.aLinkValue <<= (CORBA::Double) 0. ;
#endif
              aLink.aLinkValue >>= d;
#if TRACE
              MESSAGE( d << " (double)" );
#endif
              break;
	    }
            case CORBA::tk_long: {
              aLink.aLinkValue <<= (CORBA::Long ) 0 ;
              CORBA::Long l;
              aLink.aLinkValue >>= l;
#if TRACE
              MESSAGE( l << " (CORBA::Long)" );
#endif
              break;
	    }
            case CORBA::tk_objref: {
//              aLink.aLinkValue.replace(CORBA::_tc_Object, NULL);
              aLink.aLinkValue <<= CORBA::Object::_nil() ;
#if TRACE
              MESSAGE( " (object reference)" );
#endif
              break;
	    }
            default:
              MESSAGE( "(other ERROR)" );
            }
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            break ;
          }
          else
            step[depth]++ ;
//            return returnfalse( this , "depth4-3" , qName ) ;
//          break ;
        case 4 :
          if ( qName == "value" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth4-4" , qName ) ;
            if ( !strcmp( fieldvalue[depth].c_str() , "?" ) ) {
              aLink.aLinkValue <<= "" ;
	    }
            else {
              switch ( aLink.aLinkValue.type()->kind() ) {
              case CORBA::tk_string: {
                aLink.aLinkValue <<= fieldvalue[depth].c_str() ;
                const char * t;
                aLink.aLinkValue >>= t;
#if TRACE
                MESSAGE( t << " (string)" );
#endif
                break;
	      }
              case CORBA::tk_double: {
                double d;
                sscanf( fieldvalue[depth].c_str() , "%lf" , &d ) ;
#ifdef REDHAT // mkr : debug for PAL12255
		aLink.aLinkValue <<= d;
		aLinkValue <<= d;
#else
//JR		aLink.aLinkValue.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
//JR		aLinkValue.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
		aLink.aLinkValue <<= (CORBA::Double) d;
		aLinkValue <<= (CORBA::Double) d;
#endif		
		aLinkDoubleValue = new CORBA::Any( aLinkValue );
#if TRACE
		MESSAGE( d << " (double)" );
#endif
                break;
	      }
              case CORBA::tk_long: {
                long ll;
                sscanf( fieldvalue[depth].c_str() , "%ld" , &ll ) ;
                CORBA::Long l = ll ;
                aLink.aLinkValue <<= l;
#if TRACE
                MESSAGE( l << " (CORBA::Long)" );
#endif
                break;
	      }
              case CORBA::tk_objref: {
                CORBA::Object_ptr ObjRef ;
                const char * IORObjRef = fieldvalue[depth].c_str() ;
                ObjRef = _Orb->string_to_object( IORObjRef ) ;
                aLink.aLinkValue <<= ObjRef;
#if TRACE
                MESSAGE( IORObjRef << "(object reference)" );
#endif
                break;
	      }
              default: {
                aLink.aLinkValue <<= _Orb->string_to_object( fieldvalue[depth].c_str() ) ;
                MESSAGE( "(other ERROR)" );
              }
	      }
	    }
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
            step[5] = 2 ;
          break ;
          }
          else
            step[depth]++ ;
//            return returnfalse( this , "depth4-4" , qName ) ;
//          break ;
        case 5 :
          if ( qName == "coord" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth4-5" , qName ) ;
            if ( VXSize == (int ) VX.size() ) {
              VX.resize( VX.size() + 10 ) ;
              VY.resize( VY.size() + 10 ) ;
	    }
            VX[ VXSize ] = X ;
            VY[ VXSize ] = Y ;
            VXSize++ ;
            step[5] = 2 ;
          }
          else
            return returnfalse( this , "depth4-5" , qName ) ;
          break ;
        case 6 :
          if ( qName == "inParameter" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth4-6" , qName ) ;
            int size = aNode.theListOfInDataStreams.size() ;
            aNode.theListOfInDataStreams.resize( size+1 ) ;
            aNode.theListOfInDataStreams[ size ] = anInDataStreamParameter ;
            break ;
// One more aParameter input
          }
          else
            step[depth]++ ;
//            return returnfalse( this , "depth4-6" , qName ) ;
//          break ;
        case 7 :
          if ( qName == "outParameter" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth4-7" , qName ) ;
            int size = aNode.theListOfOutDataStreams.size() ;
            aNode.theListOfOutDataStreams.resize( size+1 ) ;
            aNode.theListOfOutDataStreams[ size ] = anOutDataStreamParameter ;
// One more aParameter output
            step[4] = 6 ;
            step[5] = 4 ;
          }
          else
            return returnfalse( this , "depth4-7" , qName ) ;
          break ;
        case 8 :
          if ( qName == "PyFunction" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth4-8" , qName ) ;
// One more PyFunction
            step[4] = 8 ;
            step[5] = 8 ;
          }
          else
            return returnfalse( this , "depth4-8" , qName ) ;
          break ;
        default:
          return returnfalse( this , "depth4-d" , qName ) ;
          break ;
	}
      break ;
    }


    case 5 : {
      switch ( step[depth] ) {
        case 0 :
          if ( qName == "inParameter" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth5-0" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            int size = aNode.theService.ServiceinParameter.length() ;
            aNode.theService.ServiceinParameter.length( size+1 ) ;
            aNode.theService.ServiceinParameter[size] = aParameter ;
// One more aParameter input
            step[6] = 0 ; // Next one
          }
          else
            return returnfalse( this , "depth5-0" , qName ) ;
          break ;
        case 1 :
          if ( qName == "outParameter" ) {
            if ( fieldvalue[depth] != NULLSTRING )
              return returnfalse( this , "depth5-1" , qName ) ;
            fieldvalue[depth] = NULLSTRING ;
            int size = aNode.theService.ServiceoutParameter.length() ;
            aNode.theService.ServiceoutParameter.length( size+1 ) ;
            aNode.theService.ServiceoutParameter[size] = aParameter ;
// One more aParameter Output
            step[6] = 2 ; // Next one
          }
          else
            return returnfalse( this , "depth5-1" , qName ) ;
          break ;
        case 2 :
          if ( qName == "x" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-2" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , &X ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth5-2" , qName ) ;
          break ;
        case 3 :
          if ( qName == "y" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-3" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , &Y ) ;
            fieldvalue[depth] = NULLSTRING ;
          }
          else
            return returnfalse( this , "depth5-3" , qName ) ;
          break ;
        case 4 :
          if ( qName == "inParameter-type" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-4" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anInDataStreamParameter.theDataStreamParameter.Parametertype ) ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
#if TRACE
  //cout << "InDataStreamParameter.inParameter-type " << anInDataStreamParameter.theDataStreamParameter.Parametertype << " step[" << depth << "]" << step[depth] << endl ;
#endif
            break ;
          }
          else
            step[depth] = 6 ;
//            return returnfalse( this , "depth5-4" , qName ) ;
//          break ;
        case 5 :
          if ( qName == "inParameter-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-5" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) ) {
              anInDataStreamParameter.theDataStreamParameter.Parametername = fieldvalue[depth].c_str() ;
	    }
            else {
              anInDataStreamParameter.theDataStreamParameter.Parametername = "" ;
	    }
            fieldvalue[depth] = NULLSTRING ;
#if TRACE
  //cout << "InDataStreamParameter.inParameter-name " << anInDataStreamParameter.theDataStreamParameter.Parametername << " step[" << depth << "]" << step[depth] << endl ;
#endif
//            step[depth]++ ;
//            step[depth] = 4 ;
            break ;
          }
          else if ( qName == "inParameter-dependency" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-5" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anInDataStreamParameter.theDataStreamParameter.Parameterdependency ) ;
            fieldvalue[depth] = NULLSTRING ;
#if TRACE
  //cout << "InDataStreamParameter.inParameter-dependency " << anInDataStreamParameter.theDataStreamParameter.Parameterdependency << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
//            step[depth]++ ;
//            step[depth] = 4 ;
            break ;
          }
          else if ( qName == "inParameter-schema" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-5" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anInDataStreamParameter.theKindOfSchema ) ;
            fieldvalue[depth] = NULLSTRING ;
#if TRACE
  //cout << "InDataStreamParameter.inParameter-schema " << anInDataStreamParameter.theKindOfSchema << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
//            step[depth]++ ;
//            step[depth] = 4 ;
            break ;
          }
          else if ( qName == "inParameter-interpolation" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-5" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anInDataStreamParameter.theKindOfInterpolation ) ;
            fieldvalue[depth] = NULLSTRING ;
#if TRACE
  //cout << "InDataStreamParameter.inParameter-interpolation " << anInDataStreamParameter.theKindOfInterpolation << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
//            step[depth]++ ;
//            step[depth] = 4 ;
            break ;
          }
          else if ( qName == "inParameter-extrapolation" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-5" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anInDataStreamParameter.theKindOfExtrapolation ) ;
            fieldvalue[depth] = NULLSTRING ;
//            step[depth]++ ;
            step[depth] = 4 ;
#if TRACE
  //cout << "InDataStreamParameter.inParameter-extrapolation " << anInDataStreamParameter.theKindOfExtrapolation << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
            break ;
          }
//          else
//            return returnfalse( this , "depth5-5" , qName ) ;
//          break ;
        case 6 :
          if ( qName == "outParameter-type" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-6" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anOutDataStreamParameter.theDataStreamParameter.Parametertype ) ;
            fieldvalue[depth] = NULLSTRING ;
//            step[depth]++ ;
            step[depth] = 7 ;
#if TRACE
  //cout << "OutDataStreamParameter.outParameter-type " << anOutDataStreamParameter.theDataStreamParameter.Parametertype << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
          }
          else
            return returnfalse( this , "depth5-6" , qName ) ;
          break ;
        case 7 : 
          if ( qName == "outParameter-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-7" , qName ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) ) {
              anOutDataStreamParameter.theDataStreamParameter.Parametername = fieldvalue[depth].c_str() ;
	    }
            else {
              anOutDataStreamParameter.theDataStreamParameter.Parametername = "" ;
	    }
            fieldvalue[depth] = NULLSTRING ;
#if TRACE
  //cout << "OutDataStreamParameter.outParameter-name " << anOutDataStreamParameter.theDataStreamParameter.Parametername << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
//            step[depth]++ ;
//            step[depth] = 6 ;
          }
          else if ( qName == "outParameter-dependency" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-7" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anOutDataStreamParameter.theDataStreamParameter.Parameterdependency ) ;
            fieldvalue[depth] = NULLSTRING ;
#if TRACE
  //cout << "OutDataStreamParameter.outParameter-dependency " << anOutDataStreamParameter.theDataStreamParameter.Parameterdependency << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
//            step[depth]++ ;
//            step[depth] = 6 ;
          }
          else if ( qName == "outParameter-values" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-7" , qName ) ;
            sscanf( fieldvalue[depth].c_str() , "%d" , (int * ) &anOutDataStreamParameter.theNumberOfValues ) ;
            fieldvalue[depth] = NULLSTRING ;
//            step[depth]++ ;
            step[depth] = 6 ;
#if TRACE
  //cout << "OutDataStreamParameter.outParameter-values " << anOutDataStreamParameter.theNumberOfValues << " step[" << depth << "]"
  //               << step[depth] << endl ;
#endif
          }
          else
            return returnfalse( this , "depth5-7" , qName ) ;
          break ;
        case 8 :
          if ( qName == "FuncName" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth5-8" , qName ) ;
            int l = aNode.theListOfFuncName.size() ;
            aNode.theListOfFuncName.resize( l+1 ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) ) {
              aNode.theListOfFuncName[ l ] = fieldvalue[depth].c_str() ;
	    }
            else {
              aNode.theListOfFuncName[ l ] = "" ;
	    }
            aNode.theListOfPythonFunctions.resize( l+1 ) ;
            aNode.theListOfPythonFunctions[ l ] = new SUPERV::ListOfStrings() ;
#if TRACE
            MESSAGE( " ==========theListOfFuncName/theListOfPythonFunctions size "
                     << l+1 )
#endif
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth5-8" , qName ) ;
          break ;
        case 9 :
          if ( qName == "PyFunc" ) {
	    // mkr : commented for PAL12309 (the function body can contains spaces, tabs, etc.)
	    //if ( fieldvalue[depth] == NULLSTRING )
            //  return returnfalse( this , "depth5-9" , qName ) ;
            int l = aNode.theListOfPythonFunctions.size() - 1 ;
            SUPERV::ListOfStrings aPythonFunction = *aNode.theListOfPythonFunctions[ l ] ;
            int ll = aPythonFunction.length() ;
            aPythonFunction.length( ll+1 ) ;
            if ( strcmp( fieldvalue[depth].c_str() , "?" ) ) {
              aPythonFunction[ ll ] = fieldvalue[depth].c_str() ;
	    }
            else {
              aPythonFunction[ ll ] = CORBA::String_var("") ;
	    }
            if ( ll ) {
              aNode.theListOfPythonFunctions.resize( l ) ;
              aNode.theListOfPythonFunctions.resize( l+1 ) ;
	    }
            aNode.theListOfPythonFunctions[ l ] = new SUPERV::ListOfStrings( aPythonFunction ) ;
#if TRACE
            MESSAGE( " ==========theListOfPythonFunctions[" << l << "] size " << ll+1
                     << " [" << ll << "] = " << aPythonFunction[ ll ] )
#endif
            fieldvalue[depth] = NULLSTRING ;
//            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth5-9" , qName ) ;
          break ;
        default:
          return returnfalse( this , "depth5-d" , qName ) ;
          break ;
 	}
      break ;
    }

    case 6 : {
      switch ( step[depth] ) {
        case 0 :
          if ( qName == "inParameter-type" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth6-0" , qName ) ;
            aParameter.Parametertype = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth6-0" , qName ) ;
          break ;
        case 1 :
          if ( qName == "inParameter-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth6-1" , qName ) ;
            aParameter.Parametername = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth6-1" , qName ) ;
          break ;
        case 2 :
          if ( qName == "outParameter-type" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth6-2" , qName ) ;
            aParameter.Parametertype = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth6-2" , qName ) ;
          break ;
        case 3 :
          if ( qName == "outParameter-name" ) {
            if ( fieldvalue[depth] == NULLSTRING )
              return returnfalse( this , "depth6-3" , qName ) ;
            aParameter.Parametername = fieldvalue[depth].c_str() ;
            fieldvalue[depth] = NULLSTRING ;
            step[depth]++ ;
          }
          else
            return returnfalse( this , "depth6-3" , qName ) ;
          break ;
        default:
          return returnfalse( this , "depth6-d" , qName ) ;
          break ;
	}
      break ;
    }
  }

#if TRACE
  //cout << "return from endElement " << qName << " step[" << depth << "]" << step[depth] << endl ;
#endif
  return TRUE;
}


bool GraphBase::XmlHandler::characters( const QString& ch ) {
  // we are not interested in whitespaces
  QString ch_simplified = ch.simplifyWhiteSpace();
  if ( ch_simplified.isEmpty() ) {
    return TRUE;
  }
  depth -= 1 ;
#if TRACE
  MESSAGE( "characters step[" << depth << "]=" << step[depth]
       << " ch " << ch << " fieldvalue_must_be_NULL : '" << fieldvalue[depth] << "'" ) ;
#endif
  if ( depth < 0 || fieldvalue[depth] != NULLSTRING )
    return returnfalse( this , "characters " , ch ) ;

//  fieldvalue[depth] = (const char * ) ch ;
  fieldvalue[depth] = ch.latin1() ;
  return TRUE;
}


QString GraphBase::XmlHandler::errorString() {
  //cout << "the document is not in the quote file format" << endl ;
  return "the document is not in the quote file format";
}


bool GraphBase::XmlHandler::fatalError( const QXmlParseException& exception ) {
  errorProt += QString( "fatal parsing error: %1 in line %2, column %3\n" )
    .arg( exception.message() )
    .arg( exception.lineNumber() )
    .arg( exception.columnNumber() );
  //cout << "GraphBase::XmlHandler::fatalError " << errorProt.latin1() << endl ;
  return QXmlDefaultHandler::fatalError( exception );
}

#if 0
SALOME_SuperVision::Date GraphBase::XmlHandler::StringToDate( QString& myStrDate) const
{
  SALOME_SuperVision::Date aDate;
  QString qstrDate(myStrDate);
  QString qstrTemp;

  // Date
  int iPos = qstrDate.find("/");
  qstrTemp = qstrDate.left(iPos).stripWhiteSpace();
  aDate.Day = qstrTemp.toShort();
  qstrDate = qstrDate.right(qstrDate.length()-iPos);
  iPos = qstrDate.find("/");
  qstrTemp = qstrDate.left(iPos).stripWhiteSpace();
  aDate.Month = qstrTemp.toShort();
  qstrDate = qstrDate.right(qstrDate.length()-iPos);
  iPos = qstrDate.find(" ");
  qstrTemp = qstrDate.left(iPos).stripWhiteSpace();
  aDate.Year = qstrTemp.toShort();

  // Time
  qstrDate = qstrDate.right(qstrDate.length()-iPos);
  iPos = qstrDate.find(":");
  qstrTemp = qstrDate.left(iPos).stripWhiteSpace();
  aDate.Hour = qstrTemp.toShort();
  qstrDate = qstrDate.right(qstrDate.length()-iPos);
  iPos = qstrDate.find(":");
  qstrTemp = qstrDate.left(iPos).stripWhiteSpace();
  aDate.Minute = qstrTemp.toShort();
  qstrDate = qstrDate.right(qstrDate.length()-iPos).stripWhiteSpace();
  aDate.Second = qstrDate.toShort();

  return aDate;
}
#endif



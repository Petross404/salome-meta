//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_LoopNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

#ifndef _DATAFLOWBASE_LOOPNODE_HXX
#define _DATAFLOWBASE_LOOPNODE_HXX

#include "DataFlowBase_GOTONode.hxx"

namespace GraphBase {

  class LoopNode : public GOTONode {

    private:

      char                  * _MoreName ;
      SUPERV::ListOfStrings * _MorePythonFunction ;
      PyObject              * _MyPyMoreMethod ;
      char                  * _NextName ;
      SUPERV::ListOfStrings * _NextPythonFunction ;
      PyObject              * _MyPyNextMethod ;

    public:

      LoopNode() ;
      LoopNode( CORBA::ORB_ptr ORB ,
                SALOME_NamingService* ptrNamingService ,
                const char * InitName ,
                const SUPERV::ListOfStrings & aInitPythonFunction ,
                const char * MoreName ,
                const SUPERV::ListOfStrings & aMorePythonFunction ,
                const char * NextName ,
                const SUPERV::ListOfStrings & aNextPythonFunction ,
                const char *NodeName ,
                const SUPERV::KindOfNode akind ,
                const SUPERV::SDate NodeFirstCreation ,
                const SUPERV::SDate NodeLastModification ,
                const char * NodeEditorRelease ,
                const char * NodeAuthor ,
                const char * NodeComment ,
                const bool   GeneratedName ,
                const long   X ,
                const long   Y ,
                int * Graph_prof_debug = NULL ,
                ofstream * Graph_fdebug = NULL ) ;
      virtual ~LoopNode() ;

      void SetMorePythonFunction( const char * MoreName ,
                                  const SUPERV::ListOfStrings & aMorePythonFunction ) {
           _MoreName = my_strblkdup( MoreName ) ;
           cdebug << "GraphBase::LoopNode::SetMorePythonFunction MoreName '" << MoreName << "' --> '"
                  << _MoreName << "'" << endl ;
           _MorePythonFunction = new SUPERV::ListOfStrings( aMorePythonFunction ) ; } ;

      SUPERV::ListOfStrings * MorePythonFunction() const {
              SUPERV::ListOfStrings * aMorePythonFunction ;
              aMorePythonFunction = new SUPERV::ListOfStrings( *_MorePythonFunction ) ;
              return aMorePythonFunction ; } ;

      char * PyMoreName() {
//JR 17.02.2005 Memory Leak             return my_strdup( _MoreName ) ; } ;
             return _MoreName ; } ;

      void PyMoreMethod( PyObject * MyPyMoreMethod ) {
           _MyPyMoreMethod = MyPyMoreMethod ; } ;

      PyObject * PyMoreMethod() {
        return _MyPyMoreMethod ; } ;

      void SetNextPythonFunction( const char * NextName ,
                                  const SUPERV::ListOfStrings & aNextPythonFunction ) {
           _NextName = my_strblkdup( NextName ) ;
           cdebug << "GraphBase::LoopNode::SetNextPythonFunction NextName '" << NextName << "' --> '"
                  << _NextName << "'" << endl ;
           _NextPythonFunction = new SUPERV::ListOfStrings( aNextPythonFunction ) ; } ;

      SUPERV::ListOfStrings * NextPythonFunction() const {
              SUPERV::ListOfStrings * aNextPythonFunction ;
              aNextPythonFunction = new SUPERV::ListOfStrings( *_NextPythonFunction ) ;
              return aNextPythonFunction ; } ;

      char * PyNextName() {
//JR 17.02.2005 Memory Leak             return my_strdup( _NextName ) ; } ;
             return _NextName ; } ;
      void PyNextMethod( PyObject * MyPyNextMethod ) {
           _MyPyNextMethod = MyPyNextMethod ; } ;

      PyObject * PyNextMethod() {
        return _MyPyNextMethod ; } ;

//      bool SaveXML(ostream &f , char *Tabs , int X , int Y ) ;
      bool SaveXML(QDomDocument & Graph , QDomElement & info , int X , int Y ) ;

      bool SavePY(ostream &f , const char * aGraphName , int X , int Y ) ;

  };
  
};

#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : SuperVisionBase_CheckOfUndefined.cxx
//  Module : SUPERV

using namespace std;
#include <iostream>
#include <fstream>
#include <unistd.h>

#include "Utils_ORB_INIT.hxx"
#include "Utils_SINGLETON.hxx"

#include "SALOME_NamingService.hxx"
#include "SALOME_LifeCycleCORBA.hxx"

#include "DataFlowBase_Base.hxx"

#include CORBA_CLIENT_HEADER(SALOME_Component)



int _ArgC ;
char ** _ArgV ;

int main(int argc, char **argv) {

  Engines::Component_ptr objComponent ;
  char * IOR ;

  ORB_INIT &init = *SINGLETON_<ORB_INIT>::Instance() ;
  ASSERT(SINGLETON_<ORB_INIT>::IsAlreadyExisting()) ;
  CORBA::ORB_var &orb = init( argc , argv ) ;

  SALOME_NamingService * NamingService = new SALOME_NamingService( orb ) ;

  SALOME_LifeCycleCORBA LCC( NamingService ) ;
  objComponent = LCC.FindOrLoad_Component( "FactoryServer" , "AddComponent" );
  if ( CORBA::is_nil( objComponent ) ) {
    cout << "ERROR LCC.FindOrLoad_Component( FactoryServer , AddComponent )" << endl;
    return 0 ;
  }
  objComponent->ping() ;

  IOR = orb->object_to_string( objComponent );
  cout << "objComponent " << objComponent << " IOR " << IOR << " nil "
       << CORBA::is_nil( objComponent ) << endl ;

  Engines::Component_ptr mySuperVision ;
  mySuperVision = LCC.FindOrLoad_Component( "SuperVisionContainer","SUPERV" ) ;
  SUPERV::SuperG_ptr mySuperVisionComponent ;
  mySuperVisionComponent =  SUPERV::SuperG::_narrow( mySuperVision ) ;

  CORBA::Any anAny ;
//  anAny <<= CORBA::Object::_nil() ;
  anAny <<= objComponent ;
//  CORBA::Any anAny = CORBA::Any( CORBA::_tc_Object, objComponent ) ;
//  anAny.replace( CORBA::_tc_Object, objComponent );

  CORBA::Object * obj ;
  anAny >>= obj ;

  IOR = orb->object_to_string( objComponent );
  cout << "objComponent " << objComponent << " IOR " << IOR << " nil "
       << CORBA::is_nil( objComponent ) << endl ;

// Crash
//  IOR = orb->object_to_string( obj );
//  cout << "obj " << obj << " IOR " << IOR << " nil " << CORBA::is_nil( obj )
//       << endl ;

  Engines::Component_ptr objComponentfromAny ;
  objComponentfromAny = Engines::Component::_narrow( obj ) ;
  IOR = orb->object_to_string( objComponentfromAny );
  cout << "obComponentfromAny " << objComponentfromAny << " IOR " << IOR << " nil "
       << CORBA::is_nil( objComponentfromAny ) << endl ;



//  CORBA::Object_var myObj = NamingService->ResolveComponent("xmen","FactoryServer" ,
//                                                            "AddComponent" );
//  SuperVisionTest::AddComponent_ptr myObjComponent ;
//  myObjComponent = SuperVisionTest::AddComponent::_narrow( myObj ) ;
//  IOR = orb->object_to_string( myObjComponent );
//  cout << "myObjComponent " << myObjComponent << " IOR " << IOR << " nil "
//       << CORBA::is_nil( myObjComponent ) << endl ;
//  anAny <<= myObjComponent ;
//  anAny >>= obj ;
//  objComponentfromAny = Engines::Component::_narrow( obj ) ;
//  IOR = orb->object_to_string( objComponentfromAny );
//  cout << "objComponentfromAny " << objComponentfromAny << " IOR " << IOR << " nil "
//       << CORBA::is_nil( objComponentfromAny ) << endl ;

//  SuperVisionTest::Adder_ptr myAdder = myObjComponent->Addition() ;
//  myAdder->ping() ;
//  IOR = orb->object_to_string( myAdder );
//  cout << "myAdder " << myAdder << " IOR " << IOR << " nil "
//       << CORBA::is_nil( myAdder ) << endl ;
//  anAny <<=  myAdder ;
//  anAny <<=  orb->string_to_object( IOR ) ;
//  anAny >>= obj ;
//  SuperVisionTest::Adder_ptr objAdderfromAny ;
//  objAdderfromAny = SuperVisionTest::Adder::_narrow( obj ) ;
//  IOR = orb->object_to_string( objAdderfromAny );
//  cout << "objAdderfromAny " << objAdderfromAny << " IOR " << IOR << " nil "
//       << CORBA::is_nil( objAdderfromAny ) << endl ;
  

//  Engines::Container_ptr myContainer = myObjComponent-> GetContainerRef() ;
//  IOR = orb->object_to_string( myContainer );
//  cout << "myContainer " << myContainer << " IOR " << IOR << " nil "
//       << CORBA::is_nil( myContainer ) << endl ;
//  anAny <<= myContainer ;
//  anAny >>= obj ;
//  Engines::Container_ptr objContainerfromAny ;
//  objContainerfromAny = Engines::Container::_narrow( obj ) ;
//  IOR = orb->object_to_string( objContainerfromAny );
//  cout << "objContainerfromAny " << objContainerfromAny << " IOR " << IOR << " nil "
//       << CORBA::is_nil( objContainerfromAny ) << endl ;


  return 0;
}


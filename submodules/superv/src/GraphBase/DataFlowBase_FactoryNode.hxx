//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_FactoryNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

#ifndef _DATAFLOWBASE_FACTORYNODE_HXX
#define _DATAFLOWBASE_FACTORYNODE_HXX

#include "DataFlowBase_ComputingNode.hxx"

namespace GraphBase {

  class FactoryNode : public ComputingNode {

    private:
    
      Engines::Container_var _Container ;
      Engines::Component_var _ObjComponent ;

      char *                 _ComponentName ;
      char *                 _InterfaceName ;

      char *                 _Computer ;

    public:

      FactoryNode() ;
      FactoryNode( CORBA::ORB_ptr ORB ,
            SALOME_NamingService* ptrNamingService ,
            const char * DataFlowName ,
            const SUPERV::KindOfNode DataFlowkind ,
            int * Graph_prof_debug = NULL ,
            ofstream * Graph_fdebug = NULL ) ;
      FactoryNode( CORBA::ORB_ptr ORB ,
            SALOME_NamingService* ptrNamingService ,
            const SALOME_ModuleCatalog::Service& NodeService ,
            const char *NodeComponentName ,
            const char* NodeInterfaceName ,
            const char *NodeName ,
            const SUPERV::KindOfNode akind ,
            const SUPERV::SDate NodeFirstCreation ,
            const SUPERV::SDate NodeLastModification ,
            const char * NodeEditorRelease ,
            const char * NodeAuthor ,
            const char * NodeComputer ,
            const char * NodeComment ,
            const bool   GeneratedName ,
            const long   X ,
            const long   Y ,
            int * Graph_prof_debug = NULL ,
            ofstream * Graph_fdebug = NULL ) ;
      virtual ~FactoryNode() ;

      Engines::Container_var Container() const { return _Container ; } ;
      void SetContainer(Engines::Container_var aContainer) {
                        _Container = aContainer ; } ;
      Engines::Component_var Component() const { return _ObjComponent ; } ;
      void SetComponent(Engines::Component_var anObjComponent) {
                        _ObjComponent = anObjComponent ; } ;
//JR 17.02.2005 Memory Leak      char * ComponentName() const { return my_strdup( _ComponentName ) ; } ;
      char * ComponentName() const { return _ComponentName ; } ;
//JR 17.02.2005 Memory Leak      char * InterfaceName() const { return my_strdup( _InterfaceName ) ; } ;
      char * InterfaceName() const { return _InterfaceName ; } ;
      char * Computer() const {
//             cdebug_in << "GraphBase::FactoryNode::Computer returns '" << _Computer << "'"
//                       << endl;
//JR 17.02.2005 Memory Leak             return my_strdup( _Computer ) ; } ;
             return _Computer ; } ;

      bool ComponentName( const char * aComponentName ) ;
      bool InterfaceName( const char * anInterfaceName ) ;
      bool Computer( const char *c ) ;

//      bool SaveXML( ostream &f , char *Tabs ,
      bool SaveXML( QDomDocument & Graph , QDomElement & info ,
                    int XCoordinate , int YCoordinate ) const ;
      bool SavePY( ostream &f , const char * aGraphName ,
                   int XCoordinate , int YCoordinate ) const ;

  };
  
};

#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_ComputingNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

#ifndef _DATAFLOWBASE_COMPUTINGNODE_HXX
#define _DATAFLOWBASE_COMPUTINGNODE_HXX

#include "SALOME_NamingService.hxx"

#include "DataFlowBase_StreamNode.hxx"

class CNode_Impl ;

namespace GraphBase {

  class Graph ;

  class InLineNode ;

  class LoopNode ;
  class EndOfLoopNode ;
  class EndOfSwitchNode ;

  class ComputingNode : public StreamNode {

    private:
    
      CORBA::ORB_ptr         _ORB ;
      SALOME_NamingService * _NamingService ;
//->StreamNode      int                  * _Graph_prof_debug ;
//->StreamNode      ofstream             * _Graph_fdebug ;
      SUPERV::CNode_var      _Node_var ;
      CNode_Impl           * _Node_Impl ;
      void                 * _InNode ; // From GraphExecutor:: or GraphEditor::
      bool                   _ObjInterface ;

      pthread_t              _ThreadNo ;

//->StreamNode      char                 * _Name ;
//->StreamNode      SUPERV::KindOfNode     _Kind ;
      SUPERV::SDate          _FirstCreation ;
      SUPERV::SDate          _LastModification ;
      char                 * _EditorRelease ;
      char                 * _Author ;
      char                 * _Comment ;

//->StreamNode      bool                   _HeadNode ;
//->StreamNode      int                    _LevelNumber ;
//->StreamNode      int                    _SubGraphNumber ;
      bool                   _GeneratedName ;

//->StreamNode      int                    _DataStreamInPortsNumber ;
//->StreamNode      int                    _DataStreamOutPortsNumber ;

      int                    _ConnectedInPortsNumber ;
      int                    _DecrConnectedInPortsNumber ;

// For generated NodeNames with ServiceName : number of Nodes using
// the same ServiceName. It is not the same Service if it belongs to
// a different Interface and/or a different Component ...
//->StreamNode      map< string , int > _MapOfServiceNames ;

// Nodes with LinkedInPortsNumber InPort(s) linked to Outport(s) of this node :
//->StreamNode      map< string , int >      _MapOfLinkedNodes ;
//->StreamNode      int                      _LinkedNodesSize ;
//->StreamNode      vector<ComputingNode * > _LinkedNodes ;
//->StreamNode      vector<int >             _LinkedInPortsNumber ;

    protected:

      long _X ;
      long _Y ;

    public:

      ComputingNode() ;
      ComputingNode( CORBA::ORB_ptr ORB ,
                     SALOME_NamingService* ptrNamingService ,
                     const char * DataFlowName ,
                     const SUPERV::KindOfNode DataFlowkind ,
                     int * Graph_prof_debug ,
                     ofstream * Graph_fdebug ) ;
      ComputingNode( CORBA::ORB_ptr ORB ,
                     SALOME_NamingService* ptrNamingService ,
                     const SALOME_ModuleCatalog::Service& NodeService ,
                     const char *NodeName ,
                     const SUPERV::KindOfNode akind ,
                     const SUPERV::SDate NodeFirstCreation ,
                     const SUPERV::SDate NodeLastModification ,
                     const char * NodeEditorRelease ,
                     const char * NodeAuthor ,
                     const char * NodeComment ,
                     const bool   GeneratedName ,
                     const long   X ,
                     const long   Y ,
                     int * Graph_prof_debug = NULL ,
                     ofstream * Graph_fdebug = NULL ) ;
      virtual ~ComputingNode() ;

      SALOME_NamingService * NamingService() const {
             return _NamingService ; } ;

      SUPERV::CNode_var ObjRef() const {
//                        char * _NodeName ;
//                        cdebug << "ComputingNode::ObjRef() " << _Node << " " << Name()
//                               << " " << endl ;
//                        SUPERV::CNode_var _NodeObjRef = SUPERV::CNode::_narrow( _Node ) ;
//                        if ( CORBA::is_nil( _NodeObjRef ) ) {
//                          _NodeName = "NilRef" ;
//			}
//			else {
//                          try {
//                            _NodeName = _NodeObjRef->Name() ;
//			  }
//                          catch ( ... ) {
//                            _NodeName = "Catch Error" ;
//			  }
//			}
//                        cdebug << _NodeName << " " << Name() << endl ;
                         return _Node_var ; } ;
      void SetObjRef( SUPERV::CNode_var aNode_var ) {
//                        char * _NodeName ;
//                        if ( CORBA::is_nil( _Node ) ) {
//                          _NodeName = "NilRef" ;
//			}
//			else {
//                          _NodeName = _Node->Name() ;
//			}
//                        cdebug << "ComputingNode::SetObjRef( " << _Node << " "
//                               << _NodeName << " ) " << Name() << " --> " ;
                        _Node_var = aNode_var ;
//                        cdebug << _Node << " " << _Node->Name() << " ) " << Name() << endl ;
                        } ;

      CNode_Impl * ObjImpl() const { return _Node_Impl ; } ;
      void SetObjImpl( CNode_Impl * aNode_Impl ) {
                       _Node_Impl = aNode_Impl ; } ;

      void InNode( void * anInNode ) {
//           cdebug << "GraphBase::ComputingNode::InNode " << Name() << endl ;
           _InNode = anInNode ; } ;
      void * GetInNode() const {
             return _InNode ; } ;

      void ObjInterface( bool k_interface ) { _ObjInterface = k_interface ; } ;
      bool ObjInterface() { return _ObjInterface ; } ;

      pthread_t ThreadNo() { return _ThreadNo ; } ;
      void ThreadNo( pthread_t aThread ) { _ThreadNo = aThread ; } ;

//->StreamNode      char * Name() const { return my_strdup( _Name ) ; } ;
//->StreamNode      const char *const * NamePtr() const { return &_Name ; } ;
//      const SALOME_ModuleCatalog::Service * Service() const ;
      SUPERV::SDate FirstCreation() const ;
      SUPERV::SDate LastModification() const ;
//JR 17.02.2005 Memory Leak      char* Author() const { return my_strdup( _Author ) ; } ;
      char* Author() const { return _Author ; } ;
//JR 17.02.2005 Memory Leak      char* EditorRelease() const { return my_strdup( _EditorRelease ) ; } ;
      char* EditorRelease() const { return _EditorRelease ; } ;
//JR 17.02.2005 Memory Leak      char * Comment() const { return my_strdup( _Comment ) ; } ;
      char * Comment() const { return _Comment ; } ;
      int XCoordinate() const { return _X ; } ;
      int YCoordinate() const { return _Y ; } ;

//->StreamNode      bool Name( const char * aName ) ;
//      void Service( const SALOME_ModuleCatalog::Service aService ) ;
      void FirstCreation( const SUPERV::SDate aFirstCreation ) ;
      void LastModification( const SUPERV::SDate aLastModification ) ;
      bool Author( const char * anAuthor ) ;
      bool EditorRelease( const char* anEditorRelease ) ;
      bool Comment( const char *c ) ;
      void Coordinates( const int X , const int Y ) {
           _X = X ; _Y = Y ; } ;

      void NodePort( const char * NodeName ,
                     const char * ServiceParameterName ,
                     char ** aNode , char ** aPort ) ;

      bool IsLinked(const char * ToServiceParameterName, bool isInput ) ; // mkr : PAL8060
      bool HasInput(const char * ToServiceParameterName ) ;

      GraphBase::SNode * GetInfo() ;

//      void HeadNode( bool aHeadNode ) { _HeadNode = aHeadNode ; } ;
//      const bool IsHeadNode() const { return _HeadNode ; } ;

      bool RemovePorts( const char* aNodeName ) ;

      void SetMacroPorts( GraphBase::Graph * aGraph ) ;
      void UpdateMacroPorts( GraphBase::Graph * aGraph ) ;
      void SetMacroDatas( GraphBase::Graph * aGraph , GraphBase::Graph * aGraphOfMacroGraph ) ;

      void DelInPort( const char * InputParameterName ) ;
      void DelOutPort( const char * OutputParameterName ) ;

      InPort * AddInPort( const char * InputParameterName ,
                          const char * InputParameterType ,
                          const SUPERV::KindOfPort aKindOfPort ,
                          int index = -1 ) ;
      OutPort * AddOutPort( const char * OutputParameterName ,
                            const char * OutputParameterType ,
                            const SUPERV::KindOfPort aKindOfPort ,
                            int index = -1 ) ;

      void DelInDataStreamPort( const char * InputParameterName ) ;
      void DelOutDataStreamPort( const char * OutputParameterName ) ;

      InDataStreamPort * AddInDataStreamPort( const char * InputParameterName ,
                                              const char * InputParameterType ,
                                              const SALOME_ModuleCatalog::DataStreamDependency aDependency ,
                                              const SUPERV::KindOfPort aKindOfPort ,
                                              int index = -1 ) ;
      OutDataStreamPort * AddOutDataStreamPort( const char * OutputParameterName ,
                                                const char * OutputParameterType ,
                                                const SALOME_ModuleCatalog::DataStreamDependency aDependency ,
                                                const SUPERV::KindOfPort aKindOfPort ,
                                                int index = -1 ) ;

//      ComputingNode * LinkedNodes( int i ) const { return (GraphBase::ComputingNode *) GraphBase::StreamNode::LinkedNodes( i ) ; } ;

      void AddLinkedNode( GraphBase::ComputingNode * ToNode ) {
           GraphBase::StreamNode::AddLinkedNode( ToNode ) ;
           cdebug << Name() << "->GraphBase::ComputingNode::AddLinkedNode( " << ToNode->Name()
                  << ") ConnectedInPortsNumber " << ToNode->ConnectedInPortsNumber()
                  << " + 1 Service " << ServiceName() << endl ;
           ToNode->IncrConnectedInPortsNumber() ; } ;

      bool RemoveLinkedNode( GraphBase::ComputingNode * ToNode ,
                             bool DoLinkedFromNode = true ) {
           bool RetVal = GraphBase::StreamNode::RemoveLinkedNode( ToNode , DoLinkedFromNode ) ;
           if ( RetVal ) {
             cdebug << "ComputingNode::RemoveLinkedNode to " << ToNode->Name() << " from "
                    << Name() << " ConnectedInPortsNumber "
                    << ToNode->ConnectedInPortsNumber() - 1 << endl ;
             ToNode->DecrConnectedInPortsNumber() ;
	   }
           return RetVal ; } ;

      void ReNameLinkedNode( const char* OldNodeName ,
                             const char* NewNodeName ) {
           GraphBase::StreamNode::ReNameLinkedNode( OldNodeName , NewNodeName ) ; } ;

      void IncrConnectedInPortsNumber() {
           cdebug << "ComputingNode::IncrConnectedInPortsNumber " << Name() << " -> "
                  << _ConnectedInPortsNumber+1 << endl ;
           _ConnectedInPortsNumber++ ; } ;
      void DecrConnectedInPortsNumber() {
           cdebug << "ComputingNode::DecrConnectedInPortsNumber " << Name() << " -> "
                  << _ConnectedInPortsNumber-1 << endl ;
           _ConnectedInPortsNumber-- ; } ;
      int ConnectedInPortsNumber() const {
          return _ConnectedInPortsNumber ; } ;
      void ConnectedInPortsNumber( int c ) {
           cdebug << "ConnectedInPortsNumber " << Name() << " -> " << c << endl ;
           _ConnectedInPortsNumber = c ; } ;

      void IncrDecrConnectedInPortsNumber(const int cnt ) {
           _DecrConnectedInPortsNumber += cnt ; } ;
      bool DecrIncrDecrConnectedInPortsNumber() {
           if ( _DecrConnectedInPortsNumber ) {
             _ConnectedInPortsNumber = _ConnectedInPortsNumber -
                                       _DecrConnectedInPortsNumber ;
             _DecrConnectedInPortsNumber = 0 ;
             return true ;
	   }
           return false ; } ;

      bool CheckLoop( GraphBase::LoopNode * aLoopNode ,
                      GraphBase::EndOfLoopNode * anEndLoopNode ,
                      string & anErrorMessage ) const ;
      bool CheckEndLoop( GraphBase::LoopNode * aLoopNode ,
                         GraphBase::EndOfLoopNode * anEndLoopNode ,
                         string & anErrorMessage ) const ;
      bool InitBranchOfSwitchDone( bool AllInit , GraphBase::EndOfSwitchNode * anEndSwitchNode ,
                                   string & anErrorMessage ) ;
      bool CheckSwitch( GraphBase::EndOfSwitchNode * anEndSwitchNode ,
                        string & anErrorMessage ) ;

//      const GraphBase::ListOfParameters * GetListOfParameters() const ;

      const long CpuUsed() ;

      bool SaveXML( QDomDocument & Graph , QDomElement & info , 
                    int X , int Y ) const {
                    ListOfFuncName aFuncNames ; 
                    GraphBase::ListOfPythonFunctions aVoidList ;
           return SaveXML( Graph , info , "" , "" , "" , "" , aFuncNames , aVoidList ,
                           X , Y ) ; } ;

      bool SavePY( ostream &f , const char * aGraphName ,
                   int X , int Y ) const {
           ListOfFuncName aFuncNames ; 
           GraphBase::ListOfPythonFunctions aVoidList ;
           return SavePY( f , aGraphName , "" , "" , "" , NULL , aFuncNames  , aVoidList ,
                          X , Y ) ; } ;
//      bool SaveXML(ostream &f , char *Tabs , 
      bool SaveXML(QDomDocument & Graph , QDomElement & info , 
                   const char * ComponentName ,
                   const char * InterfaceName ,
                   const char * Computer ,
                   const char * CoupledNode ,
                   const ListOfFuncName FuncNames ,
                   const GraphBase::ListOfPythonFunctions PythonFunctions ,
                   int X , int Y ) const ;

      bool SavePY(ostream &f , const char * aGraphName ,
                               const char * ComponentName ,
                               const char * InterfaceName ,
                               const char * Computer ,
                               const GraphBase::InLineNode * aCoupledNode ,
                               const ListOfFuncName FuncNames ,
                               const GraphBase::ListOfPythonFunctions PythonFunctions ,
                               int X , int Y ) const ;

      void NodeInfo(ostream & s) const ;

      void ListLinks(ostream &f ) const ;

  };
  
};

ostream & operator<< (ostream &,const GraphBase::ComputingNode & N );

ostream & operator<< (ostream &,const SUPERV::SDate & D );

#endif

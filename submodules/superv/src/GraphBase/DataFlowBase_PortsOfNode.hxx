//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_PortsOfNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_PORTSOFNODE_HXX
#define _DATAFLOWBASE_PORTSOFNODE_HXX

#include "DataFlowBase_Service.hxx"

#include "DataFlowBase_InDataStreamPort.hxx"
#include "DataFlowBase_OutDataStreamPort.hxx"

namespace GraphBase {

  class PortsOfNode : public Service {

    private:

// Only For getting Ports of InNodes and OutNode
      map< string , int > _MapOfNodeInPorts ;
      int _NodeInPortsSize ;
      vector<InPort *> _NodeInPorts;

      map< string , int > _MapOfNodeOutPorts ;
      int _NodeOutPortsSize ;
      vector<OutPort *> _NodeOutPorts;

      int                    _DataStreamInPortsNumber ;
      int                    _DataStreamOutPortsNumber ;

    public:

      PortsOfNode() ;
      PortsOfNode( const char *DataFlowName ) ;
      virtual ~PortsOfNode() ;

      void DefPortsOfNode( CORBA::ORB_ptr ORB ,
                           const SALOME_ModuleCatalog::Service& NodeService ,
                           const char *const * NodeName ,
                           const SUPERV::KindOfNode aKind ,
                           int * Graph_prof_debug ,
                           ofstream * Graph_fdebug ) ;

      InPort * AddInPort( CORBA::ORB_ptr ORB ,
                          const char *const * NodeName ,
                          const SUPERV::KindOfNode aKind ,
                          const char * InputParameterName ,
                          const char * InputParameterType ,
                          SUPERV::KindOfPort aKindOfPort ,
                          int index ,
                          int * Graph_prof_debug ,
                          ofstream * Graph_fdebug ) ;
      OutPort * AddOutPort( CORBA::ORB_ptr ORB ,
                            const char *const * NodeName ,
                            const SUPERV::KindOfNode aKind ,
                            const char * OutputParameterName ,
                            const char * InputParameterType ,
                            SUPERV::KindOfPort aKindOfPort ,
                            int index ,
                            int * Graph_prof_debug ,
                            ofstream * Graph_fdebug ) ;

      void MoveInPort( const char * InputParameterName , int toindex ) ;
      void MoveOutPort( const char * OutputParameterName , int toindex ) ;

      void DelInPort( const char * InputParameterName ) ;
      void DelOutPort( const char * OutputParameterName ) ;

      int IncrDataStreamInPorts() {
        _DataStreamInPortsNumber++ ;
	return _DataStreamInPortsNumber ;
      } ;
      int DecrDataStreamInPorts() {
        _DataStreamInPortsNumber-- ;
	return _DataStreamInPortsNumber ;
      } ;
      int IncrDataStreamOutPorts() {
        _DataStreamOutPortsNumber++ ;
	return _DataStreamOutPortsNumber ;
      } ;
      int DecrDataStreamOutPorts() {
        _DataStreamOutPortsNumber-- ;
	return _DataStreamOutPortsNumber ;
      } ;
      int DataStreamInPortsNumber() {
	return _DataStreamInPortsNumber ;
      } ;
      int DataStreamOutPortsNumber() {
	return _DataStreamOutPortsNumber ;
      } ;
      void DataStreamInPortsNumber( int aDataStreamInPortsNumber ) {
	_DataStreamInPortsNumber = aDataStreamInPortsNumber ;
      } ;
      void DataStreamOutPortsNumber(int aDataStreamOutPortsNumber ) {
	_DataStreamOutPortsNumber = aDataStreamOutPortsNumber ;
      } ;
      int HasDataStream() const {
	return _DataStreamInPortsNumber + _DataStreamOutPortsNumber ;
      } ;

      const int GetNodeInPortsSize() const { return _NodeInPortsSize ; } ;
      const InPort * GetNodeInLoop() const {
                   return _NodeInPorts[0] ; } ;
      const InPort * GetNodeInGate() const {
                   return _NodeInPorts[GetNodeInPortsSize()-1] ; } ;
      const InPort * GetNodeInPort(int i) const {
                   return _NodeInPorts[i] ; } ;
      InPort * GetChangeNodeInLoop() const {
                   return _NodeInPorts[0] ; } ;
      InPort * GetChangeNodeInGate() const {
                   return _NodeInPorts[GetNodeInPortsSize()-1] ; } ;
      InPort * GetChangeNodeInPort(int i) const {
                   return _NodeInPorts[i] ; } ;
      const int GetNodeOutPortsSize() const { return _NodeOutPortsSize ; } ;
      const OutPort * GetNodeOutLoop() const {
                   return _NodeOutPorts[0] ; } ;
      const OutPort * GetNodeOutGate() const {
                   return _NodeOutPorts[GetNodeOutPortsSize()-1] ; } ;
      const OutPort * GetNodeOutPort(int i) const {
                   return _NodeOutPorts[i] ; } ;
      OutPort * GetChangeNodeOutLoop() const {
                   return _NodeOutPorts[0] ; } ;
      OutPort * GetChangeNodeOutGate() const {
                   return _NodeOutPorts[GetNodeOutPortsSize()-1] ; } ;
      OutPort * GetChangeNodeOutPort(int i) const {
                   return _NodeOutPorts[i] ; } ;

      const InPort * GetInPort( const char *name ) ;
      const OutPort * GetOutPort( const char *name ) ;
      InPort * GetChangeInPort( const char *name ) ;
      OutPort * GetChangeOutPort( const char *name ) ;

      void ListPorts( ostream & , const bool klink = true ) const ;
  };
  
};

ostream & operator<< (ostream &,const SUPERV::KindOfNode &);

#endif

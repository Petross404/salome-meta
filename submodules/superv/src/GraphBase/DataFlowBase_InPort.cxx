//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_InPort.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_InPort.hxx"
#include "DataFlowBase_OutPort.hxx"

char ** _PortStateName ;

GraphBase::InPort::InPort() :
           Port() {
  _PortState = SUPERV::UndefinedState ;
  _OutPort = NULL ;
  _InitialOutPort = NULL ;
}

GraphBase::InPort::InPort(
            const char *const * Name  ,
            const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
            const SUPERV::KindOfPort aKind ,
            const SALOME_ModuleCatalog::DataStreamDependency aDependency ) :
          Port( Name , aserviceParameter , aKind , aDependency ) {
  _PortState = SUPERV::UndefinedState ;
  _OutPort = NULL ;
  _InitialOutPort = NULL ;
}

//GraphBase::InPort::~InPort() {
//  cout << "InPort::~InPort()" << endl ;
//}

const StatusOfPort GraphBase::InPort::PortStatus() const {
  if ( GetOutPort() ) {
    return GetOutPort()->PortStatus() ;
  }
  return NotConnected ;
}

bool GraphBase::InPort::IsNotConnected() const {
  return ( GetOutPort() == NULL || GetOutPort()->IsNotConnected() ) ;
}

//bool GraphBase::InPort::IsConnected() const {
//  return ( GetOutPort() && ( GetOutPort()->IsPortConnected() || GetOutPort()->IsExternConnected() ) ) ;
//}

bool GraphBase::InPort::IsPortConnected() const {
  return ( GetOutPort() && GetOutPort()->IsPortConnected() ) ;
}

bool GraphBase::InPort::IsDataConnected() const {
  return ( GetOutPort() && GetOutPort()->IsDataConnected() ) ;
}

bool GraphBase::InPort::IsExternConnected() const {
  return ( GetOutPort() && GetOutPort()->IsExternConnected() ) ;
}

void GraphBase::InPort::StringValue(ostream & f ) const {
  if ( GetOutPort() )
    GetOutPort()->StringValue( f ) ;
  else
    f << "(undefined)" ;
}

bool GraphBase::InPort::ChangeOutPort( OutPort * anOutPort ) {
  if ( _OutPort ) {
//    cdebug << "InPort::ChangeOutPort OutPort of InPort " << NodeName() << "( "
//           << PortName() << " ) : " << _OutPort->NodeName() << "( "
//           << _OutPort->PortName() << " ) changed to " << anOutPort->NodeName()
//           << "( " << anOutPort->PortName() << " )" << endl ;
//    cout << "InPort::ChangeOutPort OutPort of InPort " << NodeName() << "( "
//         << PortName() << " ) : " << _OutPort->NodeName() << "( "
//         << _OutPort->PortName() << " ) changed to " << anOutPort->NodeName()
//         << "( " << anOutPort->PortName() << " )" << endl ;
  }
  if ( _InitialOutPort == NULL ) {
    _InitialOutPort = _OutPort ;
  }
  _OutPort = anOutPort ;
  return true ;
}

bool GraphBase::InPort::RemoveOutPort( bool DoRemoveInPort ) {
// Before to return the real status, callers should be checked ...
  if ( _OutPort == NULL ) {
    cdebug << "InPort::RemoveOutPort no OutPort in InPort " << PortName() << " of " << NodeName() << endl ;
    return false ;
  }
  cdebug << "InPort::RemoveOutPort " << _OutPort->NodeName() << "(" << _OutPort->PortName() << ") --> "
         << NodeName() << "(" << PortName() << ") in InPort " << PortName() << " of " << NodeName() << endl ;
  if ( !IsEndSwitch() ) {
//JR 08.06.2005 : RemoveOutPort <==> delete of a link ==> RemoveInPort corresponding
    if ( DoRemoveInPort ) {
      _OutPort->RemoveInPort( this ) ;
    }
    RemoveCoords() ;
    _OutPort = NULL ;
  }
  else {
    _OutPort = NULL ;
    Kind( SUPERV::InLineParameter ) ;
  }
  return true ;
}

ostream & operator<< (ostream &f ,const GraphBase::InPort &P) {
  f << P.PortName() << ", " << "type : " << P.PortType() << ", " << P.PortStatus() << ", " ;
  f << "from Node " << P.NodeName() << ", ";
  f << "kind " << P.Kind() << ", ";
  f << "Status " << P.PortStatus() << ", ";
  GraphBase::OutPort * anOutPort = P.GetOutPort() ;
  if ( anOutPort ) {
    f << "Linked from OutPort " << anOutPort->PortName() << ", " << "type : " << anOutPort->PortType() << ", " ;
    f << "from Node " << anOutPort->NodeName() << ", ";
    f << "kind " << anOutPort->Kind() << ", ";
    f << "Status " << anOutPort->PortStatus() << ", ";
  }
  return f;
}

ostream & operator<< (ostream &f ,const SUPERV::GraphState & aPortState ) {
  switch (aPortState) {
  case SUPERV::UndefinedState :
    f << "UndefinedState";
    break;
  case SUPERV::NoState :
    f << "NoState";
    break;
  case SUPERV::EditingState :
    f << "EditingState";
    break;
  case SUPERV::SuspendState :
    f << "SuspendState";
    break;
  case SUPERV::WaitingState :
    f << "WaitingState";
    break;
  case SUPERV::ReadyState :
    f << "ReadyState";
    break;
  case SUPERV::SuspendReadyState :
    f << "SuspendReadyState";
    break;
  case SUPERV::RunningState :
    f << "RunningState";
    break;
  case SUPERV::DoneState :
    f << "DoneState";
    break;
  case SUPERV::ErrorState :
    f << "ErrorState";
    break;
  case SUPERV::SuspendDoneState :
    f << "SuspendDoneState";
    break;
  case SUPERV::SuspendErroredState :
    f << "SuspendErroredState";
    break;
  case SUPERV::KillState :
    f << "KillState";
    break;
  case SUPERV::StopState :
    f << "StopState";
    break;
  default :
    f << "SUPERV::GraphState_?";
    break;
  }

  return f;
}


//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_OutDataStreamPort.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;

#include "DataFlowBase_OutDataStreamPort.hxx"

GraphBase::OutDataStreamPort::OutDataStreamPort() :
  OutPort() {
  _NumberOfValues = 0 ;
  cdebug << "GraphBase::OutDataStreamPort::OutDataStreamPort " << this << " "  << endl ;
}

GraphBase::OutDataStreamPort::OutDataStreamPort(
           const char *const * NodeName ,
           const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
           const SALOME_ModuleCatalog::DataStreamDependency aDependency ,
           const long aNumberOfValues ) :
  OutPort( NodeName , aserviceParameter , SUPERV::DataStreamParameter , aDependency ) {
  _NumberOfValues = aNumberOfValues ;
  cdebug << "GraphBase::OutDataStreamPort::OutDataStreamPort " << this << " "  << endl ;
}

GraphBase::OutDataStreamPort::~OutDataStreamPort() {
  cdebug << "GraphBase::OutDataStreamPort::~OutDataStreamPort " << this << endl ;
}


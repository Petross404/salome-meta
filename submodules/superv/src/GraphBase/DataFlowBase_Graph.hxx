//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Graph.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_GRAPH_HXX
#define _DATAFLOWBASE_GRAPH_HXX

#include "DataFlowBase_FactoryNode.hxx"
#include "DataFlowBase_LoopNode.hxx"
#include "DataFlowBase_EndOfLoopNode.hxx"
#include "DataFlowBase_SwitchNode.hxx"
#include "DataFlowBase_EndOfSwitchNode.hxx"
#include "DataFlowBase_DataNode.hxx"

namespace GraphBase {

  class Graph : public DataNode {

    private:

//      int                 _Graph_prof_debug;
//      ofstream          * _Graph_fdebug;
      CORBA::ORB_ptr      _Orb ;
//      SUPERV::Graph_var   _Graph ;
//      Graph_Impl        * _GraphImpl ;

// Map of Nodes of the Graph
      map< string , int >      _MapOfGraphNodes ;
      long                     _GraphNodesSize ;
      vector<bool >            _Sorted ;
      vector<ComputingNode *>  _GraphNodes ;

// _SortedNodes[ _LevelsNumber ] : topological sort from 0 to _LevelsNumber
// _SortedNodes[ _level ][ _NodesNumber[ _level ] ] : _NodesNumber[ _level ] Nodes in the level
      int                              _LevelsNumber ;
      int                              _ThreadsMax ;
      vector<int >                     _NodesNumber ;
      vector<vector<ComputingNode *> > _SortedNodes ;

      vector<int >             _CnxInPortsNumber ;
      vector<int >             _DecrInPortsNumber ;

      long                     _HeadNodesSize ; 
      vector<ComputingNode *>  _HeadNodes ;
      long                     _QueueNodesSize ; 
      vector<ComputingNode *>  _QueueNodes ;
      bool                     _ParallelExecutionNodes ;

// For StreamGraphs. It is not the same Service if it belongs to
// a different Interface and/or a different Component ...
      map< string , GraphBase::Service * > _MapOfServiceNames ;

// For automactic rename of nodes if unique names in graphs
      map< string , int >      _MapOfServiceInstanceNumber ;

// Total number of SubGraphs
      int                              _SubGraphsNumber ;
// _SubGraphs[ SubGraphNumero ] [ NodeNumero ]
// SubGraphNumero : [ 0 , _SubGraphsSize [ ; NodeNumero : [ 0 , _SubGraphs[ SubGraphNumero ].size() [
      int                              _SubGraphsSize ;
      vector<vector<ComputingNode *> > _SubGraphs ;

// _SubStreamGraphs[ SubStreamGraphNumero ] [ NodeNumero ]
// SubStreamGraphNumero : [ 0 , _SubStreamGraphsSize [
// NodeNumero : [ 0 , _SubStreamGraphs[ SubStreamGraphNumero ].size() [
//      int                              _SubStreamGraphsSize ;
//      vector<vector<ComputingNode *> > _SubStreamGraphs ;

    string _Messages ;

    map< string, string > _MapOfComponentNameContainer ; // mkr : PAL13947
    
    bool AddLink( GraphBase::ComputingNode *fromNode ,
		  GraphBase::OutPort *fromPort ,
		  GraphBase::ComputingNode *toNode ,
		  GraphBase::InPort *toPort ) ;

    public:

      Graph() ;
      Graph( CORBA::ORB_ptr ORB ,
             SALOME_NamingService* ptrNamingService ,
             const char *DataFlowName ,
             const SUPERV::KindOfNode DataFlowkind ,
             int * Graph_prof_debug ,
             ofstream * Graph_fdebug ) ;
//             const char * DebugFileName ) ;
      Graph( CORBA::ORB_ptr ORB ,
             SALOME_NamingService* ptrNamingService ,
             const SALOME_ModuleCatalog::Service& DataFlowService ,
             const char *DataFlowComponentName ,
             const char *DataFlowInterfaceName ,
             const char *DataFlowName ,
             const SUPERV::KindOfNode DataFlowkind ,
             const SUPERV::SDate DataFlowFirstCreation ,
             const SUPERV::SDate DataFlowLastModification ,
             const char * DataFlowEditorRelease ,
             const char * DataFlowAuthor ,
             const char * DataFlowComputer ,
             const char * DataFlowComment ,
             int * Graph_prof_debug ,
             ofstream * Graph_fdebug ) ;
//             const char * DebugFileName ) ;
      virtual ~Graph() ;

//      void Set_prof_debug( CORBA::ORB_ptr ORB ,
//                           const char * DebugFileName ) ;
      void Set_prof_debug( int * Graph_prof_debug ,
                           ofstream * Graph_fdebug ) ;
      int * Graph_prof_debug() {
            return _prof_debug ; } ;
      ofstream * Graph_fdebug() { return _fdebug ; } ;

//      SUPERV::Graph_var ObjRef() const { return _Graph ; } ;
//      void SetObjRef( SUPERV::Graph_var aGraph ) {
//                      _Graph = aGraph ; } ;
//      Graph_Impl * ObjImpl() const { return _GraphImpl ; } ;
//      void SetObjImpl( Graph_Impl * aGraphImpl ) {
//                       _GraphImpl = aGraphImpl ; } ;

      void AddLinkedNode( const char* FromNodeName , GraphBase::ComputingNode * anOutNode ) {
           GetChangeGraphNode( FromNodeName )->AddLinkedNode( anOutNode ) ; } ;

      const int GetGraphNodeIndex( const char *name ) {
            const int index = _MapOfGraphNodes[ name ] - 1 ;
//            if ( index >= 0 )
//              cdebug << "GetGraphNodeIndex of " << name << " in _MapOfGraphNodes : "
//                     << index << " Node " << hex << (void *) _GraphNodes[ index ]
//                     << dec << " '" << _GraphNodes[ index ]->Name() << "'" << endl ;
#if 0
           cdebug << "Graph::GetGraphNodeIndex " << _GraphNodesSize << " known nodes" << endl;
           int i ;
           for ( i = 0 ; i < _GraphNodesSize ; i++ ) {
             cdebug << "Node" << i << _GraphNodes[ i ]->Name() << endl ;
           }
           map< string , int >::iterator aMapOfGraphNodesIterator ;
           i = 0 ;
           for ( aMapOfGraphNodesIterator = _MapOfGraphNodes.begin() ;
             aMapOfGraphNodesIterator != _MapOfGraphNodes.end() ; aMapOfGraphNodesIterator++ ) {
             cdebug << "MapOfGraphNodes " << i++ << " " << aMapOfGraphNodesIterator->first
                    << " --> " << aMapOfGraphNodesIterator->second << " - 1" << endl ;
           }
#endif
            return index ; } ;
      void SetGraphNodeIndex( const char *name , const int index ) {
           _MapOfGraphNodes[ name ] = index + 1 ;
           cdebug << "SetGraphNodeIndex of " << name << " in _MapOfGraphNodes : "
                  << index << " Node " << (void *) _GraphNodes[ index ]
                  << " '" << _GraphNodes[ index ]->Name() << "' MapOfGraphNodessize "
                  << GetGraphNodeSize() << endl ;
#if 1
           cdebug << "Graph::SetGraphNodeIndex " << _GraphNodesSize << " known nodes" << endl;
           int i ;
           for ( i = 0 ; i < _GraphNodesSize ; i++ ) {
             cdebug << "Node" << i << _GraphNodes[ i ]->Name() << endl ;
           }
           map< string , int >::iterator aMapOfGraphNodesIterator ;
           i = 0 ;
           for ( aMapOfGraphNodesIterator = _MapOfGraphNodes.begin() ;
             aMapOfGraphNodesIterator != _MapOfGraphNodes.end() ; aMapOfGraphNodesIterator++ ) {
             cdebug << "MapOfGraphNodes " << i++ << " " << aMapOfGraphNodesIterator->first
                    << " --> " << aMapOfGraphNodesIterator->second << " - 1" << endl ;
           }
#endif
           } ;
      void DelGraphNodeIndex( const char *name ) {
           _MapOfGraphNodes.erase( name ) ; } ;

      int GetGraphNodeSize() const {
          return _MapOfGraphNodes.size() ; }
      const GraphBase::ComputingNode * GetGraphNode( const int index ) const {
            const ComputingNode * aNode = GetChangeGraphNode( index ) ;
            return aNode ; } ;
      ComputingNode * GetChangeGraphNode( const int index ) const {
            if ( index >= 0 && index < _GraphNodesSize ) {
              ComputingNode * aNode = _GraphNodes[ index ] ;
              return aNode ;
            }
            else
              return NULL ; } ;
      const ComputingNode * GetGraphNode( const char *name ) {
            const ComputingNode * aNode = GetChangeGraphNode( name ) ;
            return aNode ; } ;
      ComputingNode * GetChangeGraphNode( const char *name ) {
            int index = GetGraphNodeIndex( name ) ;
            if ( index >= 0 && index < _GraphNodesSize ) {
              ComputingNode * aNode = GetChangeGraphNode( index ) ;
              return aNode ;
            }
            else
              return NULL ; } ;

      const int GraphNodesSize() const {
            return _GraphNodesSize ; } ;
      ComputingNode * GraphNodes( int i ) const {
            return _GraphNodes[ i ] ; } ;

      const int HeadNodesSize() const {
            return _HeadNodesSize ; } ;
      ComputingNode * HeadNodes( int i ) const {
            return _HeadNodes[ i ] ; } ;

      const int QueueNodesSize() const {
            return _QueueNodesSize ; } ;
      ComputingNode * QueueNodes( int i ) const {
            return _QueueNodes[ i ] ; } ;

      const GraphBase::InPort * GetInPort( const char * ToServiceParameterName ) {
                                return GraphBase::PortsOfNode::GetInPort( ToServiceParameterName ) ; } ;
      GraphBase::InPort * GetChangeInPort( const char * ToServiceParameterName ) {
                          return GraphBase::PortsOfNode::GetChangeInPort( ToServiceParameterName ) ; } ;
      const GraphBase::OutPort * GetOutPort( const char * FromServiceParameterName ) {
                                 return GraphBase::PortsOfNode::GetOutPort( FromServiceParameterName ) ; } ;
      GraphBase::OutPort * GetChangeOutPort( const char * FromServiceParameterName ) {
                           return GraphBase::PortsOfNode::GetChangeOutPort( FromServiceParameterName ); } ;

      const GraphBase::InPort * GetInPort( const char * ToNodeName ,
                                          const char * ToServiceParameterName ) {
            ComputingNode * aNode = GetChangeGraphNode( ToNodeName ) ;
            if ( aNode ) {
              return aNode->GetInPort(ToServiceParameterName);
	    }
            else
              return NULL ; } ;
      GraphBase::InPort * GetChangeInPort( const char * ToNodeName ,
                                          const char * ToServiceParameterName ) {
            ComputingNode * aNode = GetChangeGraphNode( ToNodeName ) ;
            if ( aNode ) {
              return aNode->GetChangeInPort( ToServiceParameterName ) ;
	    }
            else
              return NULL ; } ;
      const GraphBase::OutPort * GetOutPort( const char * FromNodeName ,
                                    const char * FromServiceParameterName ) {
            ComputingNode * aNode = GetChangeGraphNode( FromNodeName ) ;
            if ( aNode ) {
              return aNode->GetOutPort( FromServiceParameterName ) ;
	    }
            else
              return NULL ; } ;
      GraphBase::OutPort * GetChangeOutPort( const char * FromNodeName ,
                                            const char * FromServiceParameterName ) {
            ComputingNode * aNode = GetChangeGraphNode( FromNodeName ) ;
            if ( aNode ) {
              return aNode->GetChangeOutPort( FromServiceParameterName );
	    }
            else
              return NULL ; } ;

      SUPERV::GraphState PortState( const char* NodeName ,
                                    const char* ServiceParameterName ) ;
      bool PortDone( const char* NodeName ,
                     const char* ServiceParameterName ) {
        bool aRet = false ;
        SUPERV::GraphState aState = PortState( NodeName ,
                                                ServiceParameterName ) ;
        if ( aState == SUPERV::DoneState )
          aRet = true ;
        return aRet ; } ;
//JR 30.03.2005      const CORBA::Any * PortInData( const char* ToNodeName ,
      const CORBA::Any PortInData( const char* ToNodeName ,
                                   const char* ToServiceParameterName ) ;
//JR 30.03.2005      const CORBA::Any * PortOutData( const char* FromNodeName ,
      const CORBA::Any PortOutData( const char* FromNodeName ,
                                    const char* FromServiceParameterName ) ;

      bool LoadXml( CORBA::ORB_ptr Orb , const char* anXmlFile ,
                    GraphBase::ListOfSGraphs & aListOfDataFlows ) ;

      GraphBase::SNode * GetInfo() const ;
      GraphBase::ListOfSNodes * GetNodes() const ;
      GraphBase::ListOfSLinks * GetLinks(bool AllLinks = false ) const ;
      GraphBase::ListOfSGraphs * GetGraphs() const ;
      GraphBase::ListOfSLinks * GetDatas() const ;

      bool AddNode( ComputingNode * aNode ) ;
      bool RemoveNode( const char* NodeName ) ;
      bool ReNameNode( const char* OldNodeName ,
                       const char* NewNodeName ) ;

      bool AddLink( const char* FromNodeName ,
                    const char* FromServiceParameterName ,
	            const char* ToNodeName ,
                    const char* ToServiceParameterName ) ;
//                    , const CORBA::Any aValue ) ;
      bool RemoveLink( const char* FromNodeName ,
                       const char* FromServiceParameterName ,
	               const char* ToNodeName ,
                       const char* ToServiceParameterName ) ;
      bool GetLink( const char* ToNodeName ,
                    const char* ToServiceParameterName ,
                    char** FromNodeName ,
                    char** FromServiceParameterName ) ;
      GraphBase::SLink * GetLink( GraphBase::ComputingNode * aNode ,
                                  GraphBase::InPort* toPort ) ;

      bool AddInputData( const char* ToNodeName ,
                         const char* ToParameterName ,
                         const CORBA::Any aValue ) ;
      bool ChangeInputData( const char* ToNodeName ,
                            const char* ToParameterName ,
                            const CORBA::Any aValue ) ;
//      bool AddInputData( const char* ToNodeName ,
//                         const char* ToParameterName ,
//                         const CORBA::Any ** aValue ) ;

      bool AddOutputData( const char* FromNodeName ,
                          const char* FromParameterName ,
                          const CORBA::Any aValue ) ;

      void SetGraphPorts() ;

      string Messages() ;
      void ReSetMessages() ;
      void SetMessages( string anErrorMessage ) ;

      bool CreateService() ;
      bool InLineServices() ;

      bool Sort( int & SubStreamGraphsNumber ) ;
      bool ValidGOTO() ; // mkr : PAL12575
      bool ValidLoops() const ;
      bool ValidSwitchs() const ;
      bool ComputingNodes() const ;
      bool LinkLoopNodes(bool & NewLink ) ;
      bool DataServerNodes() const ;
  
      long LevelMax() {
           return _LevelsNumber + 1 ; } ;

      map< string , GraphBase::Service * > MapOfServiceNames() ;
      GraphBase::Service * GetServiceOfMap( char * name ) ;
      bool SetServiceOfMap( GraphBase::Service * aService ) ;
      int GetNewServiceInstanceNumber( char * aServiceName ) ;

      SUPERV::ListOfStrings * LevelNodes(long aLevel ) ;
      long ThreadsMax() {
           return _ThreadsMax ; } ;

      long SubGraphsNumber() {
           return _SubGraphsNumber ; } ;

      int NodesNumber(const int aLevel ) {
          return _NodesNumber[ aLevel ] ; } ;
      GraphBase::ComputingNode * SortedNodes( const int aLevel , const int index ) {
             return (_SortedNodes[ aLevel ])[ index ] ; } ;

      bool StartComponent( const int aThread ,
                           const char * ComputerContainer ,
                           const char * ComponentName ,
                           Engines::Container_var & myContainer ,
                           Engines::Component_var & objComponent ) ;

    // mkr : PAL13947
    void InsertToMapOfComponentNameContainer( const char * theComponentName, 
					      const char * theContainer) {
      _MapOfComponentNameContainer[ theComponentName ] = theContainer ;
    }

    // mkr : PAL13947    
    const char * RetrieveFromMapOfComponentNameContainer( const char * theComponentName ) {
      if ( _MapOfComponentNameContainer.empty() 
	   ||
	   _MapOfComponentNameContainer.find(theComponentName) == _MapOfComponentNameContainer.end() )
	return NULL ;
      
      return _MapOfComponentNameContainer[ theComponentName ].c_str() ;
    }

    // mkr : PAL13947
    void ClearMapOfComponentNameContainer() {
      if ( !_MapOfComponentNameContainer.empty() )
	_MapOfComponentNameContainer.clear();
    }

  };
  
};

#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Service.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_SERVICE_HXX
#define _DATAFLOWBASE_SERVICE_HXX

#include "DataFlowBase_ServicesParameter.hxx"

ostream & operator<< (ostream & f ,const SALOME_ModuleCatalog::Service & s ) ;

namespace GraphBase {

  class Service : public Base {

    private:

      SALOME_ModuleCatalog::Service _Service ;
//      int                           _Instance ;

    public:   

      Service( const SALOME_ModuleCatalog::Service aService ) {
             SetService( aService ) ;
//             MESSAGE( "GraphBase::Service::Service : " << _Service ) ;
//             cout << "GraphBase::Service::Service : " << _Service << endl ;
             cdebug << "GraphBase::Service::Service : " << _Service << endl ;
             } ;
      Service( const char * aServiceName ) {
               _Service.ServiceName = CORBA::string_dup( aServiceName ) ;
               cdebug << "GraphBase::Service::Service : " << _Service << endl ; } ;
//               _Instance = 0 ; } ;
      virtual ~Service() {
              cdebug << "GraphBase::Service::~Service" << endl ; } ;

      void SetService( const SALOME_ModuleCatalog::Service aService ) ;

      const SALOME_ModuleCatalog::Service * GetService() const {
            cdebug << "GraphBase::Service::GetService : " << _Service << endl ;
            return &_Service ; } ;
      const char * ServiceName() const {
//            cdebug << "ServiceName " << hex << (void *) _Service.ServiceName
//                   << dec << " = " << _Service.ServiceName << endl ;
            return _Service.ServiceName ; } ;
      const SALOME_ModuleCatalog::ListOfServicesParameter ServiceInParameter() const {
            return _Service.ServiceinParameter ; } ;
      const SALOME_ModuleCatalog::ListOfServicesParameter ServiceOutParameter() const {
            return _Service.ServiceoutParameter ; } ;
      const SALOME_ModuleCatalog::ListOfServicesDataStreamParameter ServiceInStreamParameter() const {
            return _Service.ServiceinDataStreamParameter ; } ;
      const SALOME_ModuleCatalog::ListOfServicesDataStreamParameter ServiceOutStreamParameter() const {
            return _Service.ServiceoutDataStreamParameter ; } ;

//      const int Instances() const { return _Instance ; } ;
//      int NewInstance() { _Instance += 1 ;
//                          return _Instance ; } ;
//      void Instance( int Inst = 1 ) { if ( Inst == 1 )
//                                        _Instance += 1 ;
//                                      else
//                                        _Instance = Inst ; } ; } ;
    } ;

} ;

#endif

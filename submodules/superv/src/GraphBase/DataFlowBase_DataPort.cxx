//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_DataPort.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include <stdio.h>
#include "DataFlowBase_DataPort.hxx"

GraphBase::DataPort::DataPort() :
              Port() {
  pthread_mutex_init( &_MutexWait , NULL ) ;
  _PortState = SUPERV::UndefinedState ;
  _Done = false ;
  InitialValues( CORBA::Any() ) ;
}

GraphBase::DataPort::DataPort( const char *const * NodeName  ,
                               const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
                               const SUPERV::KindOfPort aKind ,
                               const SALOME_ModuleCatalog::DataStreamDependency aDependency ) :
           Port( NodeName , aserviceParameter , aKind , aDependency ) {
  pthread_mutex_init( &_MutexWait , NULL ) ;
  _PortState = SUPERV::UndefinedState ;
  _Done = false ;
  InitialValues( CORBA::Any() ) ;
}

GraphBase::DataPort::~DataPort() {
}

void GraphBase::DataPort::InitialValues(CORBA::Any aValue ) {
  _theValue = new CORBA::Any( aValue ) ;
//JR 24.02.2005 Memory Leak  string _Type = CORBA::string_dup(GetServicesParameter().Parametertype) ;
  string _Type = string( GetServicesParameter().Parametertype ) ;
  const char * Type = _Type.c_str();
  CORBA::Any InitialValue ;
  cdebug << "InitialValues " << NodeName() << " " << PortName() << " " << PortType()
         << " : " ;
  if ( !strcmp( Type , "" ) ) {
    cdebug << "void" << endl ;
    InitialValue <<= (void *) NULL ;
  }
  else if ( !strcmp( Type , "string" ) ) {
    cdebug << "string" << endl ;
//    InitialValue <<= (char *) NULL ;
    InitialValue <<= "" ;
  }
  else if ( !strcmp( Type , "boolean" ) ) {
    cdebug << "boolean" << endl ;
    InitialValue <<= (CORBA::Long ) 0 ;
  }
  else if ( !strcmp( Type , "char" ) ) {
    cdebug << "char" << endl ;
    InitialValue <<= (CORBA::Long ) 0 ;
  }
  else if ( !strcmp( Type , "short" ) ) {
    cdebug << "short" << endl ;
    InitialValue <<= (CORBA::Long ) 0 ;
  }
  else if ( !strcmp( Type , "int" ) ) {
    cdebug << "long" << endl ;
    InitialValue <<= (CORBA::Long ) 0 ;
  }
  else if ( !strcmp( Type , "long" ) ) {
    cdebug << "long" << endl ;
    InitialValue <<= (CORBA::Long ) 0 ;
  }
  else if ( !strcmp( Type , "float" ) ) {
    cdebug << "float" << endl ;
#ifdef REDHAT // mkr : debug for PAL12255
    InitialValue <<= (float ) 0. ;
#else     
//JR    double d = 0.;
//JR    InitialValue.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
    InitialValue <<= (CORBA::Float) 0. ;
#endif
    
  }
  else if ( !strcmp( Type , "double" ) ) {
    cdebug << "double" << endl ;
#ifdef REDHAT // mkr : debug for PAL12255
    InitialValue <<= (double ) 0. ;
#else
//JR    double d = 0.;
//JR    InitialValue.replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
    InitialValue <<= (CORBA::Double) 0. ;
#endif
  }
  else { // Default
    cdebug << "objref" << endl ;
//    InitialValue.replace(CORBA::_tc_Object, NULL);
    InitialValue <<= CORBA::Object::_nil() ;
  }
//  else {
//    cdebug << "InitialValues ERROR (other) " << Type << endl ;
//    InitialValue <<= (CORBA::Long ) 0 ;
//  }
  _InitialValue = new CORBA::Any( InitialValue ) ;
  _Value = &_InitialValue ;
}

void GraphBase::DataPort::SetValue( const CORBA::Any & aDataValue ) {
  CORBA::Any * aValue = new CORBA::Any( aDataValue ) ;
  SetValue( aValue ) ;
}

#define ValueTrace 0
void GraphBase::DataPort::SetValue( const CORBA::Any * aDataValue ) {
//  cdebug << pthread_self() << " SetValue(aDataValue) --> pthread_mutex_lock " << &_MutexWait
//         << endl ;
  if ( pthread_mutex_lock( &_MutexWait ) ) {
    perror( "lock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " SetValue(aDataValue) pthread_mutex_locked " << &_MutexWait
//         << endl ;

  delete _theValue ;

//JR 21.02.2005 Debug Memory leak :  string _Type = CORBA::string_dup( GetServicesParameter().Parametertype ) ;
//  const char * Type = _Type.c_str() ;
  const char * Type = GetServicesParameter().Parametertype ;
  _Value = &_theValue ;
#if ValueTrace
  cdebug << "NewValue " << NodeName() << " " << PortName() << " " << PortType()
         << " : " << aDataValue << " kind " << aDataValue->type()->kind() << " " ;
#endif
  switch (aDataValue->type()->kind()) { // Input Value
  case CORBA::tk_string: { // Input string Value
    const char * t;
    *aDataValue >>= t;
#if ValueTrace
    cdebug << t << " (string)" ;
#endif
    if ( !strcmp( Type , "string" ) ) { // SuperVision Value
      _theValue = aDataValue ;
      *_theValue >>= t;
#if ValueTrace
      cdebug << " == Value( " << t << ") (string)";
#endif
    }
    else {
      CORBA::Any * theValue = new CORBA::Any() ;
      if ( !strcmp( Type , "boolean" ) || !strcmp( Type , "char" ) ||
           !strcmp( Type , "short" ) || !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
        long ll ;
        sscanf( t , "%ld" , &ll ) ;
        CORBA::Long l = ll ;
        *theValue <<= l ;
        *theValue >>= l;
#if ValueTrace
        cdebug << " --> Value( " << l << ") (CORBA::Long) kind " << theValue->type()->kind() ;
#endif
      }
      else if ( !strcmp( Type , "float" ) || !strcmp( Type , "double" ) ) {
	double d ;
	sscanf( t , "%lf" , &d ) ;
#ifdef REDHAT // mkr : debug for PAL12255
	*theValue <<= d ;
#else
//JR	theValue->replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
	*theValue <<= (CORBA::Double) d ;
#endif
        *theValue >>= d ;
#if ValueTrace
        cdebug << " --> Value( " << d << ") (double) kind " << theValue->type()->kind() ;
#endif
      }
      else { // Default
        CORBA::Object_ptr ObjRef ;
        try {
          ObjRef = StringToObject( t ) ;
          *theValue <<= ObjRef ;
        }
        catch( ... ) {
          *theValue <<= CORBA::Object::_nil() ;
        }
#if OMNIORB_VERSION >= 4
        *theValue >>= (CORBA::Any::to_object ) ObjRef ;
#else
        *theValue >>= ObjRef ;
#endif
#if ValueTrace
        cdebug << " --> Value( " << ObjectToString( ObjRef ) << ") (object reference) kind "
               << theValue->type()->kind() ;
#endif
      }
      //_theValue = theValue ;
      _theValue = new CORBA::Any( *theValue ) ;

      //*_Value = theValue ;
      _Value = &_theValue ;

//JR 21.02.2005 Debug Memory leak : 
      delete aDataValue ;
    }
    break;
  }
  case CORBA::tk_long: { // Input CORBA::Long Value
    CORBA::Long l;
    *aDataValue >>= l;
#if ValueTrace
    cdebug << "Value( " << l << ") (CORBA::Long)";
#endif
    if ( !strcmp( Type , "boolean" ) || !strcmp( Type , "char" ) ||
         !strcmp( Type , "short" ) || !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) { // SuperVision Value
      _theValue = aDataValue ;
      *_Value = aDataValue ;
      *_theValue >>= l;
#if ValueTrace
      cdebug << " == Value( " << l << ") (CORBA::Long)";
#endif
    }
    else {
      CORBA::Any * theValue = new CORBA::Any() ;
      if ( !strcmp( Type , "string" ) ) {
	long ll = l;
        char t[40] ;
        sprintf( t , "%ld" , ll ) ;
        *theValue <<=  t ;
        const char *tt ;
        *theValue >>= tt ;
#if ValueTrace
        cdebug << " --> Value( " << t << ") (string) kind " << theValue->type()->kind() ;
#endif
      }
      else if ( !strcmp( Type , "float" ) || !strcmp( Type , "double" ) ) {
        double d = l ;
#ifdef REDHAT // mkr : debug for PAL12255
	*theValue <<= d ;
#else
//JR	theValue->replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
	*theValue <<= (CORBA::Double) d ;
#endif
        *theValue >>= d ;

#if ValueTrace
        cdebug << " --> Value( " << d << ") (double) kind " << theValue->type()->kind() ;
#endif
      }
      else { // Default
        CORBA::Object_ptr ObjRef ;
        *theValue <<= CORBA::Object::_nil() ;
#if OMNIORB_VERSION >= 4
        *theValue >>= (CORBA::Any::to_object ) ObjRef ;
#else
        *theValue >>= ObjRef ;
#endif
#if ValueTrace
        cdebug << " --> Value( " << ObjectToString( ObjRef ) << ") (object reference) kind "
               << theValue->type()->kind() ;
#endif
      }

      //_theValue = theValue ;
      _theValue = new CORBA::Any( *theValue ) ;

      //*_Value = theValue ;
      _Value = &_theValue ;

//JR 21.02.2005 Debug Memory leak : 
      delete aDataValue ;
    }
    break;
  }
  case CORBA::tk_double: { // Input double Value
    double d;
    *aDataValue >>= d;
#if ValueTrace
    cdebug << "Value( " << d << ") (double)";
#endif
    if ( !strcmp( Type , "float" ) || !strcmp( Type , "double" ) ) { // SuperVision Value
      //_theValue = aDataValue ;
      _theValue = new CORBA::Any( *aDataValue ) ;

      //*_Value = aDataValue ;
      _Value = &_theValue ;

      *_theValue >>= d;
#if ValueTrace
      cdebug << " == Value( " << d << ") (double)";
#endif
    }
    else {
      CORBA::Any * theValue = new CORBA::Any() ;
      if ( !strcmp( Type , "string" ) ) {
        char t[40] ;
        sprintf( t , "%lf" , d ) ;
        *theValue <<=  t ;
        const char *tt ;
        *theValue >>= tt ;
#if ValueTrace
        cdebug << " --> Value( " << t << ") (string) kind " << theValue->type()->kind() ;
#endif
      }
      else if ( !strcmp( Type , "boolean" ) || !strcmp( Type , "char" ) ||
                !strcmp( Type , "short" ) || !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
        CORBA::Long l = (CORBA::Long ) d ;
        *theValue <<= l ;
        *theValue >>= l;
#if ValueTrace
        cdebug << " --> Value( " << l << ") (CORBA::Long) kind " << theValue->type()->kind() ;
#endif
      }
      else { // Default
        CORBA::Object_ptr ObjRef ;
        *theValue <<= CORBA::Object::_nil() ;
#if OMNIORB_VERSION >= 4
        *theValue >>= (CORBA::Any::to_object ) ObjRef ;
#else
        *theValue >>= ObjRef ;
#endif
#if ValueTrace
        cdebug << " --> Value( " << ObjectToString( ObjRef ) << ") (object reference) kind "
               << theValue->type()->kind() ;
#endif
      }
      //_theValue = theValue ;
      _theValue = new CORBA::Any( *theValue ) ;

      //*_Value = theValue ;
      _Value = &_theValue ;

//JR 21.02.2005 Debug Memory leak : 
      delete aDataValue ;
    }
    break;
  }
  case CORBA::tk_objref: { // Input objref Value
    CORBA::Object_ptr obj ;
#if OMNIORB_VERSION >= 4
    *aDataValue >>= (CORBA::Any::to_object ) obj;
#else
    *aDataValue >>= obj;
#endif
#if ValueTrace
    cdebug << "Value( " << ObjectToString( obj ) << ") (object reference)";
#endif
    if ( strcmp( Type , "string" ) &&
         strcmp( Type , "boolean" ) && strcmp( Type , "char" ) &&
         strcmp( Type , "short" ) && strcmp( Type , "long" ) &&
         strcmp( Type , "double" ) ) { // SuperVision Default Value
      _theValue = aDataValue ;
      *_Value = aDataValue ;
#if OMNIORB_VERSION >= 4
      *_theValue >>= (CORBA::Any::to_object ) obj ;
#else
      *_theValue >>= obj;
#endif
#if ValueTrace
      cdebug << " == Value( " << ObjectToString( obj ) << ") (object reference)";
#endif
    }
    else {
      CORBA::Any * theValue = new CORBA::Any() ;
      if ( !strcmp( Type , "string" ) ) {
        *theValue <<=  ObjectToString( obj ) ;
#if OMNIORB_VERSION >= 4
        *theValue >>= (CORBA::Any::to_object ) obj ;
#else
        *theValue >>= obj ;
#endif
#if ValueTrace
        cdebug << " --> Value( " << ObjectToString( obj ) << ") (string) kind " << theValue->type()->kind() ;
#endif
      }
      else if ( !strcmp( Type , "boolean" ) || !strcmp( Type , "char" ) ||
                !strcmp( Type , "short" ) || !strcmp( Type , "int" ) || !strcmp( Type , "long" ) ) {
#ifdef OMNI_LONG_IS_INT
        long ll = (long ) obj ;
        CORBA::Long l = (CORBA::Long ) ll ;
#else
        CORBA::Long l = (CORBA::Long ) obj ;
#endif
        *theValue <<= l ;
        *theValue >>= l;
#if ValueTrace
        cdebug << " --> Value( " << l << ") (CORBA::Long) kind " << theValue->type()->kind() ;
#endif
      }
      else if ( !strcmp( Type , "float" ) || !strcmp( Type , "double" ) ) {
	double d = (double ) 0. ;
#ifdef REDHAT // mkr : debug for PAL12255
	*theValue <<= d ;
#else
//JR	theValue->replace(CORBA::TypeCode::PR_double_tc(), (void*)(&d));
	*theValue <<= (CORBA::Double) d ;
#endif
        *theValue >>= d;
#if ValueTrace
        cdebug << " --> Value( " << d << ") (double) kind " << theValue->type()->kind() ;
#endif
      }
      //_theValue = theValue ;
      _theValue = new CORBA::Any( *theValue ) ;

      //*_Value = theValue ;
      _Value = &_theValue ;

//JR 21.02.2005 Debug Memory leak : 
      delete aDataValue ;
    }
    break;
  }
  default: {
    cdebug << "Value" << " (other(tk_string,tk_double,tk_long,tk_objref)) ERROR kind "
           << aDataValue->type()->kind() ;
    break;
  }
  }
#if ValueTrace
  cdebug << endl ;
#endif
  if ( pthread_mutex_unlock( &_MutexWait ) ) {
    perror( "unlock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " SetValue(aDataValue) --> pthread_mutex_unlocked " << &_MutexWait
//         << endl ;

}

// PAL8506
//JR 30.03.2005 Memory Leak + Debug(crash) CORBA::Any const * GraphBase::DataPort::Value() const {
const CORBA::Any GraphBase::DataPort::Value() const {
  pthread_mutex_t * aMutexWait = (pthread_mutex_t *) &_MutexWait ;
//  cdebug << pthread_self() << " Value() --> pthread_mutex_lock " << aMutexWait << endl ;
  if ( pthread_mutex_lock( aMutexWait ) ) {
    perror( "lock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " Value() --> pthread_mutex_locked " << aMutexWait << endl ;
  const CORBA::Any *const Value = *_Value ;
//JR 30.03.2005  CORBA::Any * theValue ;
  CORBA::Any theValue ;
//  cdebug  << pthread_self() << " Value " << NodeName() << " " << PortName() << " "
//          << PortType() << " _Value " << _Value << " *_Value " << *_Value  << " Value->type "
//          << Value->type() ;
  if ( Value->type() ) {
//    cdebug << " kind " << Value->type()->kind() << " :" << endl ;
  }
  else {
//    cdebug << " Null" << endl ;
  }
  if ( PortDone() ) {
//JR 21.02.2005 Debug Memory leak :     theValue = new CORBA::Any( *Value ) ;
//JR 30.03.2005     theValue = (CORBA::Any * ) Value ;
    theValue = *Value ;
//JR 30.03.2005    switch ( theValue->type()->kind() ) {
    switch ( theValue.type()->kind() ) {
    case CORBA::tk_string: {
      const char * t;
//JR 30.03.2005      *theValue >>= t;
      theValue >>= t;
//      cdebug << "GraphBase::DataPort::Value() : " << t << " (string) " << endl ;
      break;
    }
    case CORBA::tk_double: {
      double d;
//JR 30.03.2005      *theValue >>= d;
      theValue >>= d;
//      cdebug << "GraphBase::DataPort::Value() : " << d << " (double) " << endl ;
      break;
    }
    case CORBA::tk_long: {
      CORBA::Long l;
//JR 30.03.2005      *theValue >>= l;
      theValue >>= l;
//      cdebug << "GraphBase::DataPort::Value() : " << l << " (CORBA::Long) " << endl ;
      break;
    }
    case CORBA::tk_objref: {
      CORBA::Object_ptr obj ;
      char * retstr ;
      try {
//JR 30.03.2005        *theValue >>= obj ;
#if OMNIORB_VERSION >= 4
        theValue >>= (CORBA::Any::to_object ) obj ;
#else
        theValue >>= obj ;
#endif
        retstr = ObjectToString( obj );
//        cdebug << "GraphBase::DataPort::Value() : " << retstr
//               << "(object reference) " << endl;
      }
      catch( ... ) {
        cdebug << "ToString( object ) Catched ERROR" << endl ;
      }
      break;
    }
    default: {
      cdebug << "GraphBase::DataPort::Value() : " << NodeName() << "( " << PortName() << " ) " << PortType()
             << " (other(tk_string,tk_double,tk_long,tk_objref)) ERROR" << endl ;
      break;
    }
    }
  }
  else {
//    cdebug << "GraphBase::DataPort::InitialValue() " << endl ;
//JR 21.02.2005 Debug Memory leak :    theValue = new CORBA::Any( *Value ) ;
//JR 30.03.2005    theValue = (CORBA::Any * ) Value ;
    theValue = *Value ;
  }
  if ( pthread_mutex_unlock( aMutexWait ) ) {
    perror( "unlock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " Value() --> pthread_mutex_unlocked " << aMutexWait << endl ;
    
  return theValue ;
}

bool GraphBase::DataPort::BoolValue() const {
  bool RetVal = false ;
  pthread_mutex_t * aMutexWait = (pthread_mutex_t *) &_MutexWait ;
//  cdebug << pthread_self() << " BoolValue() --> pthread_mutex_lock " << aMutexWait << endl ;
  if ( pthread_mutex_lock( aMutexWait ) ) {
    perror( "lock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " BoolValue() --> pthread_mutex_locked " << aMutexWait
//         << endl ;
  if ( (*_Value)->type()->kind() == CORBA::tk_long ) {
    CORBA::Long val ;
    **_Value >>= val ;
    if ( val ) {
      RetVal = true ;
    }
  }
  else if ( (*_Value)->type()->kind() == CORBA::tk_boolean ) {
    bool val ;
    **_Value >>= val ;
    if ( val ) {
      RetVal = true ;
    }
  }
  if ( pthread_mutex_unlock( aMutexWait ) ) {
    perror( "unlock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " " << NodeName() << "( " << PortName() << " ) BoolValue " << RetVal << endl ;
  return RetVal ;
}

void GraphBase::DataPort::StringValue(ostream & f ) const {
  pthread_mutex_t * aMutexWait = (pthread_mutex_t *) &_MutexWait ;
//  cdebug_in << pthread_self() << " StringValue() --> pthread_mutex_lock " << aMutexWait
//            << endl ;
  if ( pthread_mutex_lock( aMutexWait ) ) {
    perror( "lock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug << pthread_self() << " StringValue() --> pthread_mutex_locked " << aMutexWait
//         << endl ;
  if ( PortDone() ) {
//    cdebug << "StringValue " << NodeName() << " " << PortName() << " " << PortType()
//           << " _Value "  << _Value << " *_Value "
//           << *_Value  << " " << endl ;
    const CORBA::Any * theValue = *_Value ;
    switch (theValue->type()->kind()) {
    case CORBA::tk_string:
      const char * t;
      *theValue >>= t;
      f << t << " (string)" ;
      break;
    case CORBA::tk_double:
      double d;
      *theValue >>= d;
      f << d << " (double)" ;
      break;
    case CORBA::tk_long:
      CORBA::Long l;
      *theValue >>= l;
      f << l << " (CORBA::Long)" ;
      break;
    case CORBA::tk_objref:
      CORBA::Object_ptr ObjRef ;
      try {
#if OMNIORB_VERSION >= 4
        *theValue >>= (CORBA::Any::to_object ) ObjRef ;
#else
        *theValue >>= ObjRef ;
#endif
        f << "(object reference) " << ObjectToString( ObjRef ) ;
      }
      catch(...) {
        f << "(object reference) catched error" ;
      }
      break;
    default:
      f << "(other ERROR)" ;
      break;
    }
  }
  else {
    f << " Default (undefined) DATAPORT NOT DONE"  ;
  }
  if ( pthread_mutex_unlock( aMutexWait ) ) {
    perror( "unlock DataValue" ) ;
    exit( 0 ) ;
  }
//  cdebug_out << pthread_self() << " StringValue() --> pthread_mutex_unlocked " << aMutexWait
//             << endl ;
    
}



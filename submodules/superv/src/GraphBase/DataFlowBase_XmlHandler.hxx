//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : GraphBase_XmlHandler.hxx
//  Author : Arnaud RES
//  Module : SUPERV
//  $Header$

#define  INCLUDE_MENUITEM_DEF 
#define QT_ALTERNATE_QTSMANIP

#include "DataFlowBase_Graph.hxx"

#include <qxml.h>
#include <qstringlist.h>
#include <qaction.h>
#include <qlist.h>

//#include <SALOMEconfig.h>
//#include CORBA_CLIENT_HEADER(SALOME_SuperVision)


#define maxlevel 7

namespace GraphBase {

  class XmlHandler : public QXmlDefaultHandler {

    CORBA::ORB_ptr _Orb ;
    bool dataflowxml ;
    string fieldname[maxlevel] ;
    string fieldvalue[maxlevel] ;
    long step[maxlevel] ;
    long depth ;
    bool constructor ;
    GraphBase::SNode aNode ;
    SALOME_ModuleCatalog::ServicesParameter aParameter ;
    GraphBase::InDataStreamParameter anInDataStreamParameter ;
    GraphBase::OutDataStreamParameter anOutDataStreamParameter ;
//    SALOME_ModuleCatalog::ServicesDataStreamParameter aDataStreamParameter ;
    SUPERV::ListOfStrings aPythonFunction ;
    GraphBase::SLink aLink ;
//    SALOME_SuperVisionBase::ServicesParameterValue aLinkValue ;
    CORBA::Any aLinkValue ;
    CORBA::Any const *aLinkDoubleValue ;
    int X ;
    int Y ;
    int VXSize ;
    vector<int > VX ;
    vector<int > VY ;
    QString     errorProt;

    int GraphsNumber ;
    GraphBase::ListOfSGraphs aListOfDataFlows ;

  public:

    XmlHandler();
    XmlHandler( CORBA::ORB_ptr Orb , const bool aConstructor );
    virtual ~XmlHandler();

    GraphBase::ListOfSGraphs & ListOfDataFlows() {
                               return aListOfDataFlows ; } ;

    // return the error protocol if parsing failed
    QString errorProtocol();
  
    // overloaded handler functions
    bool startDocument() ;
    bool endDocument() ;
    bool startElement( const QString& namespaceURI, const QString& localName, 
		       const QString& qName, const QXmlAttributes& atts ) ;
    bool endElement( const QString& namespaceURI, const QString& localName,
                     const QString& qName ) ;
    bool characters( const QString& ch ) ;
  
    QString errorString();
  
    bool fatalError( const QXmlParseException& exception);

    SUPERV::SDate StringToDate( QString& qstrDate) const; 

    const long getdepth() const { return depth ; } ;
    const string getfieldname( const long adepth ) const { return fieldname[adepth] ; } ;
    const string getfieldvalue( const long adepth ) const { return fieldvalue[adepth] ; } ;

  };

} ;


//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_DataPort.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_DATAPORT_HXX
#define _DATAFLOWBASE_DATAPORT_HXX

#include "DataFlowBase_Port.hxx"

namespace GraphBase {

  class DataPort : public Port {

    private:

      CORBA::Any const *    _InitialValue ;
      CORBA::Any const *    _theValue ;
      CORBA::Any const * *  _Value ;
//JR 08.03.2005 : the fields (and corresponding methods) _PortState and _Done are redundant
// probably. That should be clarified when I shall have time enough
      SUPERV::GraphState    _PortState ;
      bool                  _Done ;
      pthread_mutex_t       _MutexWait ;

      void SetValue( const CORBA::Any * aValue ) ;

    public :

      DataPort() ;
      DataPort( const char *const * NodeName  ,
                const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
                const SUPERV::KindOfPort aKind = SUPERV::ServiceParameter ,
                const SALOME_ModuleCatalog::DataStreamDependency aDependency = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ) ;
      virtual ~DataPort() ;

      void InitialValues(CORBA::Any aValue ) ;
//SetValue is under the control of _MutexWait because of concurrent access with Value()
      void SetValue (const CORBA::Any & aValue ) ;
      const CORBA::Any Value() const ;
      bool BoolValue() const ;

//PortState is NOT under the control of _MutexWait because there is no problem of concurrent access
      void PortState( SUPERV::GraphState aPortState ) {
           _PortState = aPortState ; } ;
      SUPERV::GraphState PortState() { return _PortState ; } ;

//PortDone is NOT under the control of _MutexWait because there is no problem of concurrent access
      void PortDone( bool aDone ) {
//           cdebug << "ChgDone(.) " << NodeName() << " " << PortName() << " "
//                  << _Done << " -> " << aDone << endl ;
           _Done = aDone ; } ;
      const bool PortDone() const { return ( _Done ) ; } ;

      void StringValue(ostream & f) const ;

  } ;

} ;
#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_StreamNode.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _DATAFLOWBASE_STREAMNODE_HXX
#define _DATAFLOWBASE_STREAMNODE_HXX

#include "DataFlowBase_PortsOfNode.hxx"

namespace GraphEditor {

  class DataFlow ;

}

namespace GraphBase {

  class Graph ;

  class StreamNode : public PortsOfNode {

    private:
    
      char                 * _Name ;
      SUPERV::KindOfNode     _Kind ;

// _LinkedNodes are the nodes with link(s) from OutPort(s) of this node
// Nodes with LinkedInPortsNumber InPort(s) linked to Outport(s) of this node :
      map< string , int >   _MapOfLinkedNodes ;
      int                   _LinkedNodesSize ;
      vector<StreamNode * > _LinkedNodes ;
      vector<int >          _LinkedInPortsNumber ;

// _LinkedFromNodes are the nodes with link(s) to InPort(s) of this node
      map< string , int >   _MapOfLinkedFromNodes ;
      int                   _LinkedFromNodesSize ;
      vector<StreamNode * > _LinkedFromNodes ;
      vector<int >          _LinkedFromInPortsNumber ;

// Nodes with InStreamPort(s) linked to OutStreamport(s) of this node :
// NodeName <--> index of that linked node in _LinkedNodes
      map< string , int >   _MapOfLinkedStreamNodes ;
      int                   _LinkedStreamNodesSize ;
      vector<StreamNode * > _LinkedStreamNodes ;
      vector<int >          _LinkedInStreamPortsNumber ;

      bool                  _NodeDoneInBranchOfSwitch ;

      bool                  _HeadNode ;
      int                   _LevelNumber ;
      int                   _SortedIndex ;

      GraphEditor::DataFlow   * _GraphEditor ;
// If DataFlowGraph/DataStreamGraph in another graph :
// the Graph if MacroNode and MacroNode if Graph
      SUPERV::Graph_var       _GraphMacroNode ;
      GraphBase::Graph        * _GraphOfNode ;

      int                   _GraphMacroLevel ;
      int                   _SubGraphNumber ;
      int                   _SubStreamGraphNumber ;

    protected:

      int                  * _Graph_prof_debug ;
      ofstream             * _Graph_fdebug ;

      StreamNode() ;

      StreamNode( const char * NodeName ,
                  const SUPERV::KindOfNode DataFlowkind ,
                  int * Graph_prof_debug ,
                  ofstream * Graph_fdebug ) ;

      StreamNode( int * Graph_prof_debug ,
                  ofstream * Graph_fdebug ) ;

      virtual ~StreamNode() ;

    public:

//JR 17.02.2005 Memory Leak      char * Name() const { return my_strdup( _Name ) ; } ;
      char * Name() const {
//             cdebug << "StreamNode::Name " << (void * ) _Name << " = " << _Name << endl ;
//JR 21.02.2005 Memory Leak             return my_strdup( _Name ) ; } ;
             return _Name ; } ;
      const char *const * NamePtr() const {
//            cdebug << "StreamNode::NamePtr " << (void ** ) &_Name << " = Name " << (void * ) _Name
//                   << " = " << _Name << endl ;
            return &_Name ; } ;
      bool Name( const char * aName ) ;

      SUPERV::KindOfNode Kind() const {
            return _Kind; } ;
      bool Kind( SUPERV::KindOfNode aKind) {
        _Kind = aKind ;
        return true ; } ;
      const bool IsComputingNode() const {
            return (Kind() == SUPERV::ComputingNode ) ; } ;
      const bool IsFactoryNode() const {
            return (Kind() == SUPERV::FactoryNode ) ; } ;
      const bool IsOneOfGOTONodes() const {
            return (Kind() == SUPERV::LoopNode ||
                    Kind() == SUPERV::EndLoopNode ||
                    Kind() == SUPERV::SwitchNode ||
                    Kind() == SUPERV::EndSwitchNode ||
                    Kind() == SUPERV::GOTONode ||
                    Kind() == SUPERV::MacroNode ) ; } ;
      const bool IsOneOfInLineNodes() const {
            return (Kind() == SUPERV::InLineNode || IsOneOfGOTONodes() ) ; } ;
      const bool IsInLineNode() const {
            return (Kind() == SUPERV::InLineNode ) ; } ;
      const bool IsMacroNode() const {
            return (Kind() == SUPERV::MacroNode ) ; } ;
      const bool IsDataFlowNode() const {
            return (Kind() == SUPERV::DataFlowGraph ) ; } ;
      const bool IsDataStreamNode() const {
            return (Kind() == SUPERV::DataStreamGraph ) ; } ;
      const bool IsLoopNode() const {
            return (Kind() == SUPERV::LoopNode ) ; } ;
      const bool IsEndLoopNode() const {
            return (Kind() == SUPERV::EndLoopNode ) ; } ;
      const bool IsSwitchNode() const {
            return (Kind() == SUPERV::SwitchNode ) ; } ;
      const bool IsEndSwitchNode() const {
            return (Kind() == SUPERV::EndSwitchNode ) ; } ;
      const bool IsGOTONode() const {
            return (Kind() == SUPERV::GOTONode ) ; } ;

      int GetLinkedNodeIndex( const char * name ) {
          int index = _MapOfLinkedNodes[ name ] -1 ;
          if ( index >= 0 ) {
            cdebug << "GetLinkedNodeIndex of " << name
                   << " in _MapOfLinkedNodes : "
                   << index << " Node " << hex << (void *) _LinkedNodes[ index ]
                   << dec << " '" << _LinkedNodes[ index ]->Name() << "'"
                   << endl ;
	  }
          return index ; } ;
      void SetLinkedNodeIndex( const char * name , const int index ) {
          _MapOfLinkedNodes[ name ] = index +1 ;
          cdebug << "SetLinkedNodeIndex of " << name << " in _MapOfLinkedNodes : index "
                 << index << " of " << Name() << " Node " << hex << (void *) _LinkedNodes[ index ]
                 << dec << " '" << _LinkedNodes[ index ]->Name() << "'"
                 << " _MapOfLinkedNodes " << _MapOfLinkedNodes[ name ] - 1
                 << endl ;
          } ;
      void DelLinkedNodeIndex( const char * name ) {
           _MapOfLinkedNodes.erase( name ) ; } ;

      int LinkedNodesSize() const { return _LinkedNodesSize ; } ;

      const int LinkedInPortsNumber( int i ) const { return _LinkedInPortsNumber[ i ] ; } ;
      void IncrLinkedInPortsNumber( int i ) {
           cdebug << Name() << "->IncrLinkedInPortsNumber LinkedInPortsNumber --> "
                  << _LinkedInPortsNumber[ i ] + 1 << endl ;
           _LinkedInPortsNumber[ i ]++ ; } ;
      void DecrLinkedInPortsNumber( int i ) {
           cdebug << Name() << "->DecrLinkedInPortsNumber LinkedInPortsNumber --> "
                  << _LinkedInPortsNumber[ i ] - 1 << endl ;
           _LinkedInPortsNumber[ i ]-- ; } ;

      StreamNode * LinkedNodes( int i ) const { return _LinkedNodes[ i ] ; } ;

      int GetLinkedFromNodeIndex( const char * name ) {
          int index = _MapOfLinkedFromNodes[ name ] -1 ;
          if ( index >= 0 ) {
            cdebug << "StreamNode::GetLinkedFromNodeIndex of " << name
                   << " in _MapOfLinkedFromNodes : "
                   << index << " FromNode " << hex << (void *) _LinkedFromNodes[ index ]
                   << dec << " '" << _LinkedFromNodes[ index ]->Name() << "'"
                   << endl ;
	  }
          else {
            cdebug << "StreamNode::GetLinkedFromNodeIndex of " << name
                   << " in _MapOfLinkedFromNodes not found. Known nodes :" << endl ;
#if 0
            map< string , int >::iterator aMapOfLinkedFromNodesIterator1 ;
            int iifrom = 0 ;
            for ( aMapOfLinkedFromNodesIterator1 = ToNode->_MapOfLinkedFromNodes.begin() ;
                  aMapOfLinkedFromNodesIterator1 != ToNode->_MapOfLinkedFromNodes.end() &&
                  iifrom < ToNode->_LinkedFromNodesSize ; aMapOfLinkedFromNodesIterator1++ ) {
              if ( aMapOfLinkedFromNodesIterator1->second ) {
                cdebug << "_MapOfLinkedFromNodes" << iifrom++ << " of " << ToNode->Name() << " : "
                       << aMapOfLinkedFromNodesIterator1->first << " --> "
                       << aMapOfLinkedFromNodesIterator1->second << " "
                       << ToNode->_LinkedFromNodes[ aMapOfLinkedFromNodesIterator1->second - 1 ]->Name()
                       << endl ;
              }
              else {
                cdebug << "_MapOfLinkedFromNodes" << iifrom << " of " << ToNode->Name() << " : "
                       << aMapOfLinkedFromNodesIterator1->second - 1 << endl ;
	      }
	    }
#endif
	  }
          return index ; } ;
      void SetLinkedFromNodeIndex( const char * name , const int index ) {
          _MapOfLinkedFromNodes[ name ] = index +1 ;
          cdebug << "SetLinkedFromNodeIndex of " << name << " in _MapOfLinkedFromNodes : index "
                 << index << " of " << Name() << " FromNode " << hex
                 << (void *) _LinkedFromNodes[ index ]
                 << dec << " '" << _LinkedFromNodes[ index ]->Name() << "'"
                 << " _MapOfLinkedFromNodes " << _MapOfLinkedFromNodes[ name ] - 1
                 << endl ;
          } ;
      void DelLinkedFromNodeIndex( const char * name ) {
           _MapOfLinkedFromNodes.erase( name ) ; } ;

      int LinkedFromNodesSize() const {
          return _LinkedFromNodesSize ; } ;
      void DecrLinkedFromNodesSize() {
           _LinkedFromNodesSize-- ; } ;

      const int LinkedFromInPortsNumber( int i ) const {
            return _LinkedFromInPortsNumber[ i ] ; } ;
      void IncrLinkedFromInPortsNumber( int i ) {
           cdebug << Name() << "->IncrLinkedFromInPortsNumber LinkedFromInPortsNumber --> "
                  << _LinkedFromInPortsNumber[ i ] + 1 << endl ;
           _LinkedFromInPortsNumber[ i ]++ ; } ;
      void DecrLinkedFromInPortsNumber( int i ) {
           cdebug << Name() << "->DecrLinkedFromInPortsNumber LinkedFromInPortsNumber --> "
                  << _LinkedFromInPortsNumber[ i ] - 1 << endl ;
           _LinkedFromInPortsNumber[ i ]-- ; } ;

      StreamNode * LinkedFromNodes( int i ) const { return _LinkedFromNodes[ i ] ; } ;

      int GetLinkedStreamNodeIndex( const char * name ) {
          int index = _MapOfLinkedStreamNodes[ name ] -1 ;
          if ( index >= 0 ) {
            cdebug << "GetLinkedStreamNodeIndex of " << name
                   << " in _MapOfLinkedStreamNodes : "
                   << index << " Node " << hex << (void *) _LinkedStreamNodes[ index ]
                   << dec << " '" << _LinkedStreamNodes[ index ]->Name() << "'"
                   << endl ;
	  }
          return index ; } ;
      void SetLinkedStreamNodeIndex( const char * name , const int index ) {
          _MapOfLinkedStreamNodes[ name ] = index +1 ;
          cdebug << "SetLinkedStreamNodeIndex of " << name << " in _MapOfLinkedStreamNodes : "
                 << index << " Node " << hex << (void *) _LinkedStreamNodes[ index ]
                 << dec << " '" << _LinkedStreamNodes[ index ]->Name() << "'"
                 << " _MapOfLinkedStreamNodes " << _MapOfLinkedStreamNodes[ name ] - 1
                 << endl ;
          } ;
      void DelLinkedStreamNodeIndex( const char * name ) {
           _MapOfLinkedStreamNodes.erase( name ) ; } ;

      int LinkedStreamNodesSize() const { return _LinkedStreamNodesSize ; } ;

      const int LinkedInStreamPortsNumber( int i ) const {
            return _LinkedInStreamPortsNumber[ i ] ; } ;

      StreamNode * LinkedStreamNodes( int i ) const {
                   return _LinkedStreamNodes[ i ] ; } ;

      bool BranchOfSwitchDone( bool aNodeDoneInBranchOfSwitch ) {
           bool prevalue = _NodeDoneInBranchOfSwitch ;
           _NodeDoneInBranchOfSwitch = aNodeDoneInBranchOfSwitch ;
           if ( prevalue && !aNodeDoneInBranchOfSwitch ) {
             return false ;
	   }
           return true ; } ;
      bool BranchOfSwitchDone() {
           return _NodeDoneInBranchOfSwitch ; } ;

      void HeadNode( bool aHeadNode ) { _HeadNode = aHeadNode ; } ;
      const bool IsHeadNode() const { return _HeadNode ; } ;

      int Level() const { return _LevelNumber ; } ;
      void Level( int LevelNumber ) {
           _LevelNumber = LevelNumber ; } ;

      void SortedIndex( int aSortedIndex ) {
           _SortedIndex = aSortedIndex ; } ;
      int SortedIndex() const {
           return _SortedIndex ; } ;

      void SetSubStreamGraph( int SubStreamGraphsNumber , int & RetVal ) ;

      void GraphEditor( GraphEditor::DataFlow * aGraphEditor ) {
           cdebug << "StreamNode::GraphEditor this " << this << " " << Name() << " : _GraphEditor = " << aGraphEditor << endl ;
           _GraphEditor = aGraphEditor ; } ;
      GraphEditor::DataFlow * GraphEditor() const {
           cdebug << "StreamNode::GraphEditor this " << this << " " << Name() << " : " << _GraphEditor << endl ;
                          return _GraphEditor ; } ;

      void GraphMacroLevel( int aGraphMacroLevel ) {
           cdebug << "GraphMacroLevel " << Name() << " " << aGraphMacroLevel << endl ;
           _GraphMacroLevel = aGraphMacroLevel ; } ;
      int GraphMacroLevel() const {
          return _GraphMacroLevel ; } ;

      void MacroObject( SUPERV::Graph_var aGraphMacroNode ) {
           if ( CORBA::is_nil( aGraphMacroNode ) ) {
             cdebug << "MacroObject GraphMacroNode " << this << " " << Name()
                    << " MacroObject(nil object). Error" << endl ;
	   }
           else {
             cdebug << "MacroObject GraphMacroNode " << this << " " << Name()
                    << " MacroObject " << _GraphMacroNode << endl ;
	   }
           _GraphMacroNode = aGraphMacroNode ; } ;
      SUPERV::Graph_var MacroObject() const {
                        if ( CORBA::is_nil( _GraphMacroNode ) ) {
                          cdebug << "MacroObject GraphMacroNode " << this << " " << Name()
                                 << " returns nil object. Error" << endl ;
	                }
                        return _GraphMacroNode ; } ;

      void GraphOfNode( GraphBase::Graph * aGraph ) {
           _GraphOfNode = aGraph ; } ;
      GraphBase::Graph * GraphOfNode() const {
                         return _GraphOfNode ; } ;
      GraphBase::Graph * GraphOfNode() {
                         return _GraphOfNode ; } ;

      int SubGraph() const { return _SubGraphNumber ; } ;
      void SubGraph( int SubGraphNumber ) {
           _SubGraphNumber = SubGraphNumber ; } ;

      int SubStreamGraph() const { return _SubStreamGraphNumber ; } ;
      void SubStreamGraph( int SubStreamGraphNumber ) {
           _SubStreamGraphNumber = SubStreamGraphNumber ; } ;

      void AddLinkedNode( StreamNode * ToNode ) ;
      bool RemoveLinkedNode( StreamNode * ToNode ,
                             bool DoLinkedFromNode = true ) ;
      void ReNameLinkedNode( const char* OldNodeName ,
                             const char* NewNodeName ) ;
      void ReNameLinkedFromNode( const char* OldNodeName ,
                                 const char* NewNodeName ) ;

      void AddStreamLinkedNode( StreamNode * ToNode ) ;
      bool RemoveStreamLinkedNode( StreamNode * ToNode ) ;
      void ReNameStreamLinkedNode( const char* OldNodeName ,
                                   const char* NewNodeName ) ;

  };
  
};

#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_GOTONode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_GOTONode.hxx"

GraphBase::GOTONode::GOTONode() :
  GraphBase::InLineNode::InLineNode() {

  CoupledNode( NULL ) ;
  _CoupledNodeName = NULL ;
  cdebug << "GraphBase::GOTONode::GOTONode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;

}

GraphBase::GOTONode::GOTONode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService* ptrNamingService ,
                               const char *DataFlowName ,
                               const SUPERV::KindOfNode DataFlowkind ,
                               int * Graph_prof_debug ,
                               ofstream * Graph_fdebug ) :
  GraphBase::InLineNode::InLineNode( ORB , ptrNamingService , DataFlowName , DataFlowkind , Graph_prof_debug , Graph_fdebug ) {

  CoupledNode( NULL ) ;
  _CoupledNodeName = NULL ;
  cdebug << "GraphBase::GOTONode::GOTONode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;

}

GraphBase::GOTONode::GOTONode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService* ptrNamingService ,
                               const char *FuncName ,
                               const SUPERV::ListOfStrings & aPythonFunction ,
                               const char *NodeName ,
                               const SUPERV::KindOfNode akind ,
                               const SUPERV::SDate NodeFirstCreation ,
                               const SUPERV::SDate NodeLastModification  ,
                               const char * NodeEditorRelease ,
                               const char * NodeAuthor ,
                               const char * NodeComment ,
                               const bool   GeneratedName ,
                               const long   X ,
                               const long   Y ,
                               int * Graph_prof_debug ,
                               ofstream * Graph_fdebug ) :
  GraphBase::InLineNode::InLineNode( ORB , ptrNamingService , FuncName , aPythonFunction ,
                                     NodeName , akind , NodeFirstCreation ,
                                     NodeLastModification  , NodeEditorRelease ,
                                     NodeAuthor , NodeComment , GeneratedName ,
                                     X , Y , Graph_prof_debug , Graph_fdebug ) {

  CoupledNode( NULL ) ;
  _CoupledNodeName = NULL ;
  cdebug_in << "GraphBase::GOTONode::GOTONode "  << this 
            << "' _Name " << (void *) Name() << " '" << Name() << " _Comment "
            << (void *) Comment() << " "  << Comment() << " " 
            << " KindOfNode " << Kind() << " FuncName " << FuncName
            << " ServiceName " << ServiceName() << " In("
            << ServiceInParameter().length()
            << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  cdebug_out << "GraphBase::GOTONode::GOTONode" << endl;
}

GraphBase::GOTONode::~GOTONode() {
  cdebug << "GraphBase::GOTONode::~GOTONode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;
//  if ( _ComponentName != NULLSTRING )
//    delete [] _ComponentName ;
//  delete [] _Name ;
//  delete [] _EditorRelease ;
//  if ( _Author != NULLSTRING )
//    delete [] _Author ;
//  if ( _Computer != FACTORYSERVER )
//    delete [] _Computer;
//  if ( _Comment != NULLSTRING )
//    delete [] _Comment;
}

#define CoupledNodeTrace 0
void GraphBase::GOTONode::CoupledNode( GraphBase::InLineNode * aCoupledNode ) {
#if CoupledNodeTrace
  cdebug << this << " " << Name() << " GraphBase::GOTONode::CoupledNode( " ;
#endif
  if ( aCoupledNode ) {
    CoupledNodeName( aCoupledNode->Name() ) ;
#if CoupledNodeTrace
    cdebug << aCoupledNode->Name() ;
#endif
  }
  else {
#if CoupledNodeTrace
    cdebug << "NULL" ;
#endif
  }
#if CoupledNodeTrace
  cdebug << " )" << endl ;
#endif
  _CoupledNode = aCoupledNode ;
}

const GraphBase::InLineNode * GraphBase::GOTONode::CoupledNode() const {
#if CoupledNodeTrace
  cdebug << this << " " << Name() << " GraphBase::GOTONode::CoupledNode() --> " ;
  if ( _CoupledNode ) {
    cdebug << _CoupledNode->Name() ;
  }
  else {
    cdebug << "NULL" ;
  }
  cdebug << endl ;
#endif
  return _CoupledNode ;
}

GraphBase::InLineNode * GraphBase::GOTONode::CoupledNode() {
#if CoupledNodeTrace
  cdebug << this << " " << Name() << " GraphBase::GOTONode::CoupledNode() --> " ;
  if ( _CoupledNode ) {
    cdebug << _CoupledNode->Name() ;
  }
  else {
    cdebug << "NULL" ;
  }
  cdebug << endl ;
#endif
  return _CoupledNode ;
}

void GraphBase::GOTONode::CoupledNodeName( const char * aCoupledNodeName ) {
  if ( _CoupledNodeName ) {
    delete [] _CoupledNodeName ;
  }
  _CoupledNodeName = new char [ strlen( aCoupledNodeName ) + 1 ] ;
  strcpy( _CoupledNodeName , aCoupledNodeName ) ;
#if CoupledNodeTrace
  cdebug << this << " " << Name() << " GraphBase::GOTONode::CoupledNodeName() --> "
         << (void * ) _CoupledNodeName << " = " << _CoupledNodeName << endl ;
#endif
}
char * GraphBase::GOTONode::CoupledNodeName() const {
#if CoupledNodeTrace
  cdebug << this << " " << Name() << " GraphBase::GOTONode::CoupledNodeName() --> " ;
  if ( _CoupledNodeName ) {
    cdebug << (void * ) _CoupledNodeName << " = " << _CoupledNodeName << endl ;
  }
  else {
    cdebug << "NULL" << endl ;
  }
#endif
//JR 17.02.2005 Memory Leak  return my_strdup( _CoupledNodeName ) ;
  return _CoupledNodeName ;
}

//bool GraphBase::GOTONode::SaveXML( ostream &f , char *Tabs ,
bool GraphBase::GOTONode::SaveXML( QDomDocument & Graph , QDomElement & info ,
                                   int XCoordinate , int YCoordinate ) const {
  GraphBase::ListOfFuncName aFuncNames ; 
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  if ( PyFuncName() ) {
    aFuncNames.resize( 1 ) ;
//JR 24.02.2005 Memory Leak    aFuncNames[0] = CORBA::string_dup( PyFuncName() ) ;
    aFuncNames[0] = string( PyFuncName() ) ;
    aPythonFunction.resize( 1 ) ;
    aPythonFunction[0] = PythonFunction() ;
  }
  char * CoupledName = "" ;
  if ( CoupledNode() ) {
    CoupledName = CoupledNode()->Name() ;
  }
//  return GraphBase::ComputingNode::SaveXML( f , Tabs , "" , "" , "" ,
  return GraphBase::ComputingNode::SaveXML( Graph , info , "" , "" , "" ,
                                            CoupledName ,
                                            aFuncNames , aPythonFunction ,
                                            XCoordinate , YCoordinate ) ;
}

bool GraphBase::GOTONode::SavePY( ostream &f , const char * aGraphName ,
                                  int XCoordinate , int YCoordinate ) const {
  GraphBase::ListOfFuncName aFuncNames ; 
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  if ( PyFuncName() ) {
    aFuncNames.resize( 1 ) ;
//JR 24.02.2005 Memory Leak    aFuncNames[0] = CORBA::string_dup( PyFuncName() ) ;
    aFuncNames[0] = string( PyFuncName() ) ;
    aPythonFunction.resize( 1 ) ;
    aPythonFunction[0] = PythonFunction() ;
  }
  return GraphBase::ComputingNode::SavePY( f , aGraphName , "" , "" , "" ,
                                           CoupledNode() ,
                                           aFuncNames , aPythonFunction ,
                                           XCoordinate , YCoordinate ) ;
}


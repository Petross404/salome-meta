//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_ServicesParameter.hxx
//  Module : SUPERV

#ifndef _DATAFLOWBASE_SERVICESPARAMETER_HXX
#define _DATAFLOWBASE_SERVICESPARAMETER_HXX

#include "DataFlowBase_Base.hxx"

namespace GraphBase {

  class ServicesParameter : public Base {

    private:

      SALOME_ModuleCatalog::ServicesParameter _ServicesParameter ;

    public:   

      ServicesParameter() {
         //cout << "ServicesParameter()" << endl ;
         _ServicesParameter.Parametername = (char *) NULL ;
         _ServicesParameter.Parametertype = (char *) NULL ;
    } ;
      ServicesParameter( const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ) {
          _ServicesParameter.Parametertype = CORBA::string_dup( aserviceParameter.Parametertype ) ;
          _ServicesParameter.Parametername = CORBA::string_dup( aserviceParameter.Parametername ) ; } ;
      virtual ~ServicesParameter() {
//            cout << "ServicesParameter::~ServicesParameter()" << endl ;
    } ;
      const SALOME_ModuleCatalog::ServicesParameter & GetServicesParameter() const {
            return _ServicesParameter ; } ;
      const char * ServicesParameterName() const {
            return _ServicesParameter.Parametername ; } ;
      const char * ServicesParameterType() const {
            return _ServicesParameter.Parametertype ; } ;
      void ServicesParameterType(char * aParametertype ) { 
           _ServicesParameter.Parametertype = CORBA::string_dup( aParametertype ) ; } ;

  } ;

} ;

#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Service.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_Service.hxx"

void GraphBase::Service::SetService( const SALOME_ModuleCatalog::Service aService ) {
  
  _Service.ServiceName = CORBA::string_dup( aService.ServiceName ) ;
  _Service.ServiceinParameter.length( aService.ServiceinParameter.length() ) ;
  _Service.ServiceoutParameter.length( aService.ServiceoutParameter.length() ) ;
  int i ;
  for ( i = 0 ; i < (int ) _Service.ServiceinParameter.length() ; i++ ) {
    _Service.ServiceinParameter[ i ].Parametertype = CORBA::string_dup( aService.ServiceinParameter[ i ].Parametertype ) ;
    _Service.ServiceinParameter[ i ].Parametername = CORBA::string_dup( aService.ServiceinParameter[ i ].Parametername ) ;
  }
  for ( i = 0 ; i < (int ) _Service.ServiceoutParameter.length() ; i++ ) {
    _Service.ServiceoutParameter[ i ].Parametertype = CORBA::string_dup( aService.ServiceoutParameter[ i ].Parametertype ) ;
    _Service.ServiceoutParameter[ i ].Parametername = CORBA::string_dup( aService.ServiceoutParameter[ i ].Parametername ) ;
  }

  _Service.ServiceinDataStreamParameter.length( aService.ServiceinDataStreamParameter.length() ) ;
  _Service.ServiceoutDataStreamParameter.length( aService.ServiceoutDataStreamParameter.length() ) ;
  for ( i = 0 ; i < (int ) _Service.ServiceinDataStreamParameter.length() ; i++ ) {
    _Service.ServiceinDataStreamParameter[ i ].Parametertype = aService.ServiceinDataStreamParameter[ i ].Parametertype ;
    _Service.ServiceinDataStreamParameter[ i ].Parametername = CORBA::string_dup( aService.ServiceinDataStreamParameter[ i ].Parametername ) ;
    _Service.ServiceinDataStreamParameter[ i ].Parameterdependency = aService.ServiceinDataStreamParameter[ i ].Parameterdependency ;
  }
  for ( i = 0 ; i < (int ) _Service.ServiceoutDataStreamParameter.length() ; i++ ) {
    _Service.ServiceoutDataStreamParameter[ i ].Parametertype = aService.ServiceoutDataStreamParameter[ i ].Parametertype ;
    _Service.ServiceoutDataStreamParameter[ i ].Parametername = CORBA::string_dup( aService.ServiceoutDataStreamParameter[ i ].Parametername ) ;
    _Service.ServiceoutDataStreamParameter[ i ].Parameterdependency = aService.ServiceoutDataStreamParameter[ i ].Parameterdependency ;
  }
//  cdebug << "GraphBase::Service::SetService : " << _Service << endl ;
//  _Instance = 0 ;
}

ostream & operator<< (ostream & f ,const SALOME_ModuleCatalog::Service & s ) {
  f << "Name          " << s.ServiceName << endl ;
  int i ;
  for ( i = 0 ; i < (int ) s.ServiceinParameter.length() ; i++ ) {
    if ( i == 0 )
      f << "                 Inparameters  " << i ;
    else
      f << "                               " << i ;
    f << ". " << s.ServiceinParameter[i].Parametername
      << ". " << s.ServiceinParameter[i].Parametertype << endl ;
  }
  for ( i = 0 ; i < (int ) s.ServiceoutParameter.length() ; i++ ) {
    if ( i == 0 )
      f << "                 Outparameters " << i ;
    else
      f << "                               " << i ;
    f << ". " << s.ServiceoutParameter[i].Parametername
      << ". " << s.ServiceoutParameter[i].Parametertype << endl ;
  }
  for ( i = 0 ; i < (int ) s.ServiceinDataStreamParameter.length() ; i++ ) {
    if ( i == 0 )
      f << "                 InStreamparameters  " << i ;
    else
      f << "                                     " << i ;
    f << ". " << s.ServiceinDataStreamParameter[i].Parametername
      << ". " << s.ServiceinDataStreamParameter[i].Parametertype
      << ". " << s.ServiceinDataStreamParameter[i].Parameterdependency << endl ;
  }
  for ( i = 0 ; i < (int ) s.ServiceoutDataStreamParameter.length() ; i++ ) {
    if ( i == 0 )
      f << "                 OutStreamparameters " << i ;
    else
      f << "                                     " << i ;
    f << ". " << s.ServiceoutDataStreamParameter[i].Parametername
      << ". " << s.ServiceoutDataStreamParameter[i].Parametertype
      << ". " << s.ServiceoutDataStreamParameter[i].Parameterdependency << endl ;
  }
  return f;
}


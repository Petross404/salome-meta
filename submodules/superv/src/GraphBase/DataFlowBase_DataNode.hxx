//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_DataNode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_DATANODE_HXX
#define _DATAFLOWBASE_DATANODE_HXX

#include "DataFlowBase_GOTONode.hxx"

namespace GraphBase {

  class DataNode : public GOTONode {

    private:

      bool                     _Created ;

    protected :

      GraphBase::PortsOfNode * _DataFlowDataPorts ;

    public:

      DataNode() ;
      DataNode( CORBA::ORB_ptr ORB ,
                SALOME_NamingService* ptrNamingService ,
                const char *DataFlowName ,
                const SUPERV::KindOfNode DataFlowkind ,
                int * Graph_prof_debug ,
                ofstream * Graph_fdebug ) ;
      DataNode( CORBA::ORB_ptr ORB ,
                SALOME_NamingService* ptrNamingService ,
                const SALOME_ModuleCatalog::Service& DataFlowService ,
                const char *DataFlowName ,
                const SUPERV::KindOfNode DataFlowkind ,
                const SUPERV::SDate DataFlowFirstCreation ,
                const SUPERV::SDate DataFlowLastModification ,
                const char * DataFlowEditorRelease ,
                const char * DataFlowAuthor ,
                const char * DataFlowComment ) ;
      virtual ~DataNode() ;

      void DataService( CORBA::ORB_ptr ORB ,
                        const SALOME_ModuleCatalog::Service aService ,
                        int * Graph_prof_debug ,
                        ofstream * Graph_fdebug ) ;

      int CheckDataServerNodes() const ;

// Mirrored methods of _DataFlowDataPorts of DataNode :
// GetIn... gives the Out which is the input of InPort of the DataFlow
// GetOut... gives the In which gets the output of OutPort of the DataFlow
      const int GetNodeInDataNodePortsSize() const {
                return _DataFlowDataPorts->GetNodeOutPortsSize() ; } ;
      const OutPort *GetNodeInDataNodePort(int i) const {
                    return _DataFlowDataPorts->GetNodeOutPort( i ) ; } ;
      OutPort *GetChangeNodeInDataNodePort(int i) const {
              return _DataFlowDataPorts->GetChangeNodeOutPort( i ) ; } ;
      const int GetNodeOutDataNodePortsSize() const {
                return _DataFlowDataPorts->GetNodeInPortsSize() ; } ;
      const InPort *GetNodeOutDataNodePort(int i) const {
                   return _DataFlowDataPorts->GetNodeInPort( i ) ; } ;
      InPort *GetChangeNodeOutDataNodePort(int i) const {
             return _DataFlowDataPorts->GetChangeNodeInPort( i ) ; } ;

      OutPort * GetChangeInDataNodePort( const char * DataFlowInPortName ) {
                return _DataFlowDataPorts->GetChangeOutPort( DataFlowInPortName ) ; } ;
      InPort * GetChangeOutDataNodePort( const char * DataFlowOutPortName ) {
               return _DataFlowDataPorts->GetChangeInPort( DataFlowOutPortName ) ; } ;

      void DataNodeInfo(ostream & s ) const ;

      void ListDatas(ostream & f) const ;

//      bool SaveXML( ostream &f , char *Tabs , 
      bool SaveXML( QDomDocument & Graph , QDomElement & info , 
                    int X , int Y ) const {
           GraphBase::ListOfFuncName aFuncNames ; 
           aFuncNames.resize( 0 ) ;
           GraphBase::ListOfPythonFunctions aVoidList ;
//           return GraphBase::ComputingNode::SaveXML( f , Tabs , "" , "" , "" , "" ,
           return GraphBase::ComputingNode::SaveXML( Graph , info , "" , "" , "" , "" ,
                                                     aFuncNames , aVoidList , X , Y ) ; } ;

      bool SavePY( ostream &f , const char * aGraphName ,
                   int X , int Y ) const {
           GraphBase::ListOfFuncName aFuncNames ; 
           aFuncNames.resize( 0 ) ;
           GraphBase::ListOfPythonFunctions aVoidList ;
           return GraphBase::ComputingNode::SavePY( f , aGraphName , "" , "" , "" , NULL ,
                                                    aFuncNames , aVoidList , X , Y ) ; } ;
  } ;

} ;

#endif

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_StreamGraph.cxx
//  Author : Jean Rahuel
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_StreamGraph.hxx"

#include "SALOME_LifeCycleCORBA.hxx"

GraphBase::StreamGraph::StreamGraph() :
           Graph() {
  cdebug << "GraphBase::StreamGraph::StreamGraph" << endl ;
}

GraphBase::StreamGraph::StreamGraph( CORBA::ORB_ptr ORB ,
                                     SALOME_NamingService* ptrNamingService ,
                                     const char *DataFlowName ,
                                     const SUPERV::KindOfNode DataFlowkind ,
                                     int * Graph_prof_debug ,
                                     ofstream * Graph_fdebug ) :
//                                             const char * DebugFileName ) :
//           Graph( ORB ,ptrNamingService , DataFlowName , DebugFileName ) {
           Graph( ORB ,ptrNamingService , DataFlowName , DataFlowkind , Graph_prof_debug , Graph_fdebug ) {
  _Timeout = 300 ;
  _DataStreamTrace = SUPERV::WithoutTrace ;
  _DeltaTime = 0 ;
  Kind( SUPERV::DataStreamGraph ) ;
  cdebug << "GraphBase::StreamGraph::StreamGraph" << endl ;
}

GraphBase::StreamGraph::StreamGraph( CORBA::ORB_ptr ORB ,
                       SALOME_NamingService* ptrNamingService ,
                       const SALOME_ModuleCatalog::Service& DataFlowService ,
                       const char *DataFlowComponentName ,
                       const char *DataFlowInterfaceName ,
                       const char *DataFlowName ,
                       const SUPERV::KindOfNode DataFlowkind ,
                       const SUPERV::SDate DataFlowFirstCreation ,
                       const SUPERV::SDate DataFlowLastModification ,
                       const char * DataFlowEditorRelease ,
                       const char * DataFlowAuthor ,
                       const char * DataFlowComputer ,
                       const char * DataFlowComment ,
                       int * Graph_prof_debug ,
                       ofstream * Graph_fdebug ) :
//                       const char * DebugFileName ) :
           Graph( ORB ,ptrNamingService , DataFlowService , DataFlowComponentName ,
                  DataFlowInterfaceName , DataFlowName , DataFlowkind ,
                  DataFlowFirstCreation , DataFlowLastModification  ,
                  DataFlowEditorRelease , DataFlowAuthor , DataFlowComputer ,
                  DataFlowComment , Graph_prof_debug , Graph_fdebug ) {
//                  DataFlowComment , DebugFileName ) {
  _Timeout = 300 ;
  _DataStreamTrace = SUPERV::WithoutTrace ;
  _DeltaTime = 0 ;
  cdebug << "GraphBase::StreamGraph::StreamGraph" << endl ;
}

GraphBase::StreamGraph::~StreamGraph() {
  cdebug << "GraphBase::StreamGraph::~StreamGraph" << endl ;
}

bool GraphBase::StreamGraph::SetStreamParams( CORBA::Long Timeout ,
                                              const SUPERV::KindOfDataStreamTrace DataStreamTrace ,
                                              CORBA::Double  DeltaTime ) {
  _Timeout = Timeout ;
  _DataStreamTrace = DataStreamTrace ;
  _DeltaTime = DeltaTime ;
  cdebug << "GraphBase::StreamGraph:::SetStreamGraph _Timeout " << _Timeout << " _DataStreamTrace " << _DataStreamTrace << " _DeltaTime " << _DeltaTime
         << endl ;
  return true ;
}

void GraphBase::StreamGraph::StreamParams( CORBA::Long & Timeout ,
                                           SUPERV::KindOfDataStreamTrace & DataStreamTrace ,
                                           CORBA::Double & DeltaTime ) const {
  Timeout = _Timeout ;
  DataStreamTrace = _DataStreamTrace ;
  DeltaTime = _DeltaTime ;
  cdebug << "GraphBase::StreamGraph:::StreamGraph _Timeout " << _Timeout << " _DataStreamTrace " << _DataStreamTrace << " _DeltaTime " << _DeltaTime
         << endl ;
}

bool GraphBase::StreamGraph::CreateStreamTopology( const char * aDirectory ) {
  cdebug_in << "GraphBase::StreamGraph::CreateStreamTopology()" << endl;
  int istream ;
  for ( istream = 1 ; istream <= SubStreamGraphsNumber() ; istream++ ) {
    ostringstream astr ;
    astr << istream << ends ;
    string filename = string( Name() ) + string( "_" ) + astr.str().c_str() ;
    string fullfilename = aDirectory + filename + ".cpl" ;
    ofstream f( fullfilename.c_str() ) ;
    f << "DEBUT	" << filename << endl ;
    f << "	TIMEOUT	" << _Timeout << endl ;
    f << "	TRACE	" << KindOfDataStreamTraceToString( _DataStreamTrace ) << endl ;
    f << "	DELTAT	" << _DeltaTime << endl ;
    f << endl ;
    f << endl ;
    f << "# Liste des codes" << endl ;
    f << endl ;
    map< string , GraphBase::Service * > aMapOfServiceNames = MapOfServiceNames() ;
    map< string , GraphBase::Service * >::iterator aMapOfServiceNamesIterator ;
    for ( aMapOfServiceNamesIterator = aMapOfServiceNames.begin() ;
          aMapOfServiceNamesIterator != aMapOfServiceNames.end() ; aMapOfServiceNamesIterator++ ) {
      GraphBase::Service * aService = aMapOfServiceNamesIterator->second ;
      int n ;
      for ( n = 0 ; n < GraphNodesSize() ; n++ ) {
        if ( GetGraphNode( n )->HasDataStream() && GetChangeGraphNode( n )->SubStreamGraph() == istream ) {
          if ( !strcmp( aService->ServiceName() , GetGraphNode( n )->ServiceName() ) ) {
            f << "	CODE	" << aService->ServiceName() << endl ;
            unsigned int j ;
            cdebug << "CreateStreamTopology " << aService->ServiceName() << " InStreamParameter("
                   << aService->ServiceInStreamParameter().length() << ") OutStreamParameter("
                   << aService->ServiceOutStreamParameter().length() << ")" << endl ;
            cdebug << aService->GetService() << endl ;
            for ( j = 0 ; j < aService->ServiceInStreamParameter().length() ; j++ ) {
              f << "		" << aService->ServiceInStreamParameter()[ j ].Parametername << "	"
                << DataStreamDependencyToString( aService->ServiceInStreamParameter()[ j ].Parameterdependency ) << "	IN	"
                << DataStreamToString( aService->ServiceInStreamParameter()[ j ].Parametertype ) << endl ;
            }
            for ( j = 0 ; j < aService->ServiceOutStreamParameter().length() ; j++ ) {
              f << "		" << aService->ServiceOutStreamParameter()[ j ].Parametername << "	"
                << DataStreamDependencyToString( aService->ServiceOutStreamParameter()[ j ].Parameterdependency ) << "	OUT	"
                << DataStreamToString( aService->ServiceOutStreamParameter()[ j ].Parametertype ) << endl ;
            }
            f << "	FIN	#" << aService->ServiceName() << endl ;
            f << endl ;
            f << endl ;
            break ;
          }
	}
      }
    }
    f << endl ;
    f << endl ;
    f << "# Liste des instances" << endl ;
    int j ;
    for ( j = 0 ; j < GraphNodesSize() ; j++ ) {
      if ( GetGraphNode( j )->HasDataStream() && GetChangeGraphNode( j )->SubStreamGraph() == istream ) {
        f << endl ;
        f << "	INSTANCE	" << GetGraphNode( j )->Name() << endl ;
        f << "		CODE	" << GetGraphNode( j )->ServiceName() << endl ;
        f << "		EXEC	\"" << GetGraphNode( j )->ServiceName() << "\"" << endl ;
        f << "	FIN" << endl ;
      }
    }
    f << endl ;
    f << endl ;
    f << endl ;
    f << endl ;
    f << endl ;
    f << "# Liste des liens" << endl ;
    f << endl ;
    f << "	LIEN" << endl ;
    for ( j = 0 ; j < GraphNodesSize() ; j++ ) {
      if ( GetGraphNode( j )->HasDataStream() && GetChangeGraphNode( j )->SubStreamGraph() == istream ) {
        int k ;
        for ( k = 0 ; k < GetGraphNode( j )->GetNodeOutPortsSize() ; k++ ) {
          if ( GetGraphNode( j )->GetNodeOutPort( k )->IsDataStream() ) {
            long aNumberOfValues = ((GraphBase::OutDataStreamPort *) GetGraphNode( j )->GetNodeOutPort( k ))->NumberOfValues() ;
            int n ;
            for ( n = 0 ; n < GetGraphNode( j )->GetNodeOutPort( k )->InPortsSize() ; n++ ) {
              SUPERV::KindOfSchema aKindOfSchema ;
              SUPERV::KindOfInterpolation aKindOfInterpolation ;
              SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
              ((GraphBase::InDataStreamPort *) GetGraphNode( j )->GetNodeOutPort( k )->InPorts( n ))->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
              f << "		" << GetGraphNode( j )->Name() << "."
                << GetGraphNode( j )->GetNodeOutPort( k )->PortName() ;
              if ( aNumberOfValues != 0 ) {
                f << "	NIVEAU " << aNumberOfValues ;
	      }
              f << "		=>	"
                << GetGraphNode( j )->GetNodeOutPort( k )->InPorts( n )->NodeName() << "."
                << GetGraphNode( j )->GetNodeOutPort( k )->InPorts( n )->PortName()
                << "	" << aKindOfSchema << "	" << aKindOfInterpolation ;
              if ( aKindOfExtrapolation != SUPERV::EXTRANULL ) {
                f << "	" << aKindOfExtrapolation ;
	      }
              f << " ;" << endl ;
	    }
  	  }
        }
      }
    }
    f << "	FIN" << endl ;
    f << endl ;
    f << "FIN" << endl ;
  }
  cdebug_out << "GraphBase::StreamGraph::CreateStreamTopology()" << endl;
  return true ;
}

ostream & operator<< (ostream & f ,const SUPERV::KindOfDataStreamTrace & s ) {
  switch (s) {
  case SUPERV::WithoutTrace :
    f << "WithoutTrace";
    break;
  case SUPERV::SummaryTrace :
    f << "SummaryTrace";
    break;
  case SUPERV::DetailedTrace :
    f << "DetailedTrace";
    break;
  default :
    f << "UndefinedKindOfDataStreamTrace";
    break;
  }

  return f;
}



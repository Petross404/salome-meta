//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_LoopNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_LoopNode.hxx"

GraphBase::LoopNode::LoopNode() :
  GraphBase::GOTONode::GOTONode() {

  cdebug << "GraphBase::LoopNode::LoopNode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;

}

GraphBase::LoopNode::LoopNode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService* ptrNamingService ,
                               const char * InitName ,
                               const SUPERV::ListOfStrings & anInitPythonFunction ,
                               const char * MoreName ,
                               const SUPERV::ListOfStrings & aMorePythonFunction ,
                               const char * NextName ,
                               const SUPERV::ListOfStrings & aNextPythonFunction ,
                               const char *NodeName ,
                               const SUPERV::KindOfNode akind ,
                               const SUPERV::SDate NodeFirstCreation ,
                               const SUPERV::SDate NodeLastModification  ,
                               const char * NodeEditorRelease ,
                               const char * NodeAuthor ,
                               const char * NodeComment ,
                               const bool   GeneratedName ,
                               const long   X ,
                               const long   Y ,
                               int * Graph_prof_debug ,
                               ofstream * Graph_fdebug ) :
  GraphBase::GOTONode::GOTONode( ORB , ptrNamingService , InitName , anInitPythonFunction ,
                                 NodeName , akind , NodeFirstCreation ,
                                 NodeLastModification  , NodeEditorRelease ,
                                 NodeAuthor , NodeComment , GeneratedName ,
                                 X , Y , Graph_prof_debug , Graph_fdebug ) {

  cdebug_in << "GraphBase::LoopNode::LoopNode "  << this 
            << "' _Name " << (void *) Name() << " '" << Name() << " _Comment "
            << (void *) Comment() << " "  << Comment() << " " 
            << " KindOfNode " << Kind() << " InitName " << InitName
            << " MoreName " << MoreName << " NextName " << NextName
            << " ServiceName " << ServiceName() << " In("
            << ServiceInParameter().length()
            << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  _MyPyMoreMethod = NULL ;
  _MyPyNextMethod = NULL ;

  cdebug << "GraphBase::LoopNode::LoopNode SetMorePythonFunction " << MoreName << " " ;
  if ( aMorePythonFunction.length() ) {
    cdebug << aMorePythonFunction[0] ;
  }
  cdebug << endl ;
  SetMorePythonFunction( MoreName , aMorePythonFunction ) ;
  cdebug << "GraphBase::LoopNode::LoopNode SetNextPythonFunction " << NextName << " " ;
  if ( aNextPythonFunction.length() ) {
    cdebug << aNextPythonFunction[0] ;
  }
  cdebug << endl ;
  SetNextPythonFunction( NextName , aNextPythonFunction ) ;

  cdebug_out << "GraphBase::LoopNode::LoopNode" << endl;
}

GraphBase::LoopNode::~LoopNode() {
  cdebug << "GraphBase::Node::~Node "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;
//  if ( _ComponentName != NULLSTRING )
//    delete [] _ComponentName ;
//  delete [] _Name ;
//  delete [] _EditorRelease ;
//  if ( _Author != NULLSTRING )
//    delete [] _Author ;
//  if ( _Computer != FACTORYSERVER )
//    delete [] _Computer;
//  if ( Comment() != NULLSTRING )
//    delete [] Comment();
}

//bool GraphBase::LoopNode::SaveXML( ostream &f , char *Tabs ,
bool GraphBase::LoopNode::SaveXML( QDomDocument & Graph , QDomElement & info ,
                                   int XCoordinate , int YCoordinate ) {
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  aPythonFunction.resize( 3 ) ;
  aPythonFunction[0] = PythonFunction() ;
  aPythonFunction[1] = MorePythonFunction() ;
  aPythonFunction[2] = NextPythonFunction() ;
  GraphBase::ListOfFuncName aFuncNames ; 
  aFuncNames.resize( 3 ) ;
//JR 24.02.2005 Memory Leak  aFuncNames[0] = CORBA::string_dup( PyFuncName() ) ;
  aFuncNames[0] = string( PyFuncName() ) ;
//JR 24.02.2005 Memory Leak  aFuncNames[1] = CORBA::string_dup( PyMoreName() ) ;
  aFuncNames[1] = string( PyMoreName() ) ;
//JR 24.02.2005 Memory Leak  aFuncNames[2] = CORBA::string_dup( PyNextName() ) ;
  aFuncNames[2] = string( PyNextName() ) ;
  char * CoupledName = "" ;
  if ( CoupledNode() ) {
    CoupledName = CoupledNode()->Name() ;
  }
//  return GraphBase::ComputingNode::SaveXML( f , Tabs , "" , "" , "" ,
  return GraphBase::ComputingNode::SaveXML( Graph , info , "" , "" , "" ,
                                            CoupledName ,
                                            aFuncNames , aPythonFunction ,
                                            XCoordinate , YCoordinate ) ;
}

bool GraphBase::LoopNode::SavePY( ostream &f , const char * aGraphName ,
                                  int XCoordinate , int YCoordinate ) {
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  aPythonFunction.resize( 3 ) ;
  aPythonFunction[0] = PythonFunction() ;
  aPythonFunction[1] = MorePythonFunction() ;
  aPythonFunction[2] = NextPythonFunction() ;
  GraphBase::ListOfFuncName aFuncNames ; 
  aFuncNames.resize( 3 ) ;
//JR 24.02.2005 Memory Leak  aFuncNames[0] = CORBA::string_dup( PyFuncName() ) ;
  aFuncNames[0] = string( PyFuncName() ) ;
//JR 24.02.2005 Memory Leak  aFuncNames[1] = CORBA::string_dup( PyMoreName() ) ;
  aFuncNames[1] = string( PyMoreName() ) ;
//JR 24.02.2005 Memory Leak  aFuncNames[2] = CORBA::string_dup( PyNextName() ) ;
  aFuncNames[2] = string( PyNextName() ) ;
  return GraphBase::ComputingNode::SavePY( f , aGraphName , "" , "" , "" ,
                                           CoupledNode() ,
                                           aFuncNames , aPythonFunction ,
                                           XCoordinate , YCoordinate ) ;
}


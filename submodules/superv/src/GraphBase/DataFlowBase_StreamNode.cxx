//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_StreamNode.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;

#include "DataFlowBase_Graph.hxx"
#include "DataFlowBase_StreamNode.hxx"

GraphBase::StreamNode::StreamNode() :
  GraphBase::PortsOfNode::PortsOfNode() {
  _Name = NULL ;
  _Kind = SUPERV::UnknownNode ;
  _LinkedNodesSize = 0 ;
  _LinkedFromNodesSize = 0 ;
  _LinkedStreamNodesSize = 0 ;
  _HeadNode = false ;
  _LevelNumber = -1 ;
  _SortedIndex = -1 ;
  _GraphEditor = NULL ;
  _GraphMacroNode =  SUPERV::Graph::_nil() ;
  _GraphOfNode = NULL ;
  _GraphMacroLevel = 0 ;
  _SubGraphNumber = 0 ;
  _SubStreamGraphNumber = 0 ;
  cdebug << "GraphBase::StreamNode::StreamNode " << this << " "  << endl ;
}

GraphBase::StreamNode::StreamNode( const char * NodeName ,
                                   const SUPERV::KindOfNode DataFlowkind ,
                                   int * Graph_prof_debug ,
                                   ofstream * Graph_fdebug ) :
  GraphBase::PortsOfNode::PortsOfNode( NodeName ) {
  if ( NodeName != NULLSTRING && strlen( NodeName ) ) {
    _Name = new char[ strlen( NodeName )+1 ] ;
    strcpy( _Name , NodeName ) ;
  }
  else {
    _Name = NULLSTRING ;
  }
  Kind( DataFlowkind ) ;
  _LinkedNodesSize = 0 ;
  _LinkedFromNodesSize = 0 ;
  _LinkedStreamNodesSize = 0 ;
  _HeadNode = false ;
  _LevelNumber = -1 ;
  _SortedIndex = -1 ;
  _GraphEditor = NULL ;
  _GraphMacroNode =  SUPERV::Graph::_nil() ;
  _GraphOfNode = NULL ;
  _GraphMacroLevel = 0 ;
  _SubGraphNumber = 0 ;
  _SubStreamGraphNumber = 0 ;
  cdebug << "GraphBase::StreamNode::StreamNode " << NodeName << " "
         << this << " "  << endl ;
}

GraphBase::StreamNode::~StreamNode() {
  cdebug << "GraphBase::StreamNode::~StreamNode " << this << endl ;
}

bool GraphBase::StreamNode::Name( const char * aName) {
  bool RetVal ;
  char * aGraphName = "" ;
  bool aNodeWithThatName ;
  if ( IsDataFlowNode() || IsDataStreamNode() ) {
    aGraphName = Name() ;
    aNodeWithThatName = ((GraphBase::Graph * ) this)->GetGraphNode( aName ) ;
  }
  else if ( GraphOfNode() ) {
    aGraphName = GraphOfNode()->Name() ;
    aNodeWithThatName = GraphOfNode()->GetGraphNode( aName ) ;
  }
  cdebug_in << "GraphBase::StreamNode::Name from '" << _Name << "' to '" << aName
            << "' in Graph '" << aGraphName << "' IsGraph "
            << ( IsDataFlowNode() || IsDataStreamNode() ) << endl ;
  if ( strcmp( aName , aGraphName ) && !aNodeWithThatName ) {
    cdebug << "GraphBase::StreamNode::ReName "  << _Name << " --> " << aName << endl ;
    if ( _Name ) {
      delete [] _Name ;
    }
    _Name = new char[strlen(aName)+1] ;
    strcpy( _Name , aName ) ;
    RetVal = true ;
  }
  else {
    if ( !strcmp( aName , Name() ) ) {
//ReName of the graph with the same name :
      cdebug << "GraphBase::StreamNode::ReName '" << Name() << "' to '" << aName
             << "' with same name" << endl ;
      RetVal = true ;
    }
    else {
      cdebug << "GraphBase::StreamNode::ReName ERROR '" << Name() << "' to '" << aName
             << "' already exists" << endl ;
      RetVal = false ;
    }
  }
  cdebug_out << "GraphBase::StreamNode::Name " << aName << " " << RetVal << endl;
  return RetVal ;
}

void GraphBase::StreamNode::SetSubStreamGraph( int SubStreamGraphsNumber , int & RetVal ) {
  int jnode ;
  cdebug_in << Name() << " GraphBase::StreamNode::SetSubStreamGraph Level "<< Level() << " SortedIndex "
            << SortedIndex() << " SubStreamGraphsNumber " << SubStreamGraphsNumber << " RetVal " << RetVal
            << endl ;
  if ( SubStreamGraph() == 0 || SubStreamGraph() == SubStreamGraphsNumber ) {
    SubStreamGraph( SubStreamGraphsNumber ) ;
    cdebug << Name() << " SubStreamGraph " << SubStreamGraph() << " SetSubStreamGraph LinkedStreamNodesSize "
           << LinkedStreamNodesSize() << endl ;
    for ( jnode = 0 ; jnode < LinkedStreamNodesSize() ; jnode++ ) {
      if ( LinkedStreamNodes( jnode )->Level() > Level() ||
           ( LinkedStreamNodes( jnode )->Level() == Level() &&
             LinkedStreamNodes( jnode )->SortedIndex() > SortedIndex() ) ) {
        LinkedStreamNodes( jnode )->SetSubStreamGraph( SubStreamGraphsNumber , RetVal ) ;
        if ( RetVal != SubStreamGraphsNumber ) {
          break ;
	}
      }
      else if ( LinkedStreamNodes( jnode )->SubStreamGraph() == 0 ) {
        LinkedStreamNodes( jnode )->SubStreamGraph( SubStreamGraphsNumber ) ;
        cdebug << LinkedStreamNodes( jnode )->Name() << " SubStreamGraph "
               << LinkedStreamNodes( jnode )->SubStreamGraph() << endl ;
      }
      else if ( LinkedStreamNodes( jnode )->SubStreamGraph() != SubStreamGraphsNumber ) {
        cdebug << LinkedStreamNodes( jnode )->Name() << " SubStreamGraph "
               << LinkedStreamNodes( jnode )->SubStreamGraph() << " != " << SubStreamGraphsNumber << endl ;
        RetVal = LinkedStreamNodes( jnode )->SubStreamGraph() ;
        break ;
      }
    }
  }
  else {
    cdebug << Name() << " SubStreamGraph " << SubStreamGraph() << " != " << SubStreamGraphsNumber << endl ;
    RetVal = SubStreamGraph() ;
  }

  cdebug_out << Name() << "->GraphBase::StreamNode::SetSubStreamGraph RetVal " << RetVal << endl ;
  return ;
}

void GraphBase::StreamNode::AddLinkedNode( GraphBase::StreamNode * ToNode ) {
  int index = GetLinkedNodeIndex( ToNode->Name() ) ;
  if ( index < 0 ) {
    cdebug << Name() << "->GraphBase::StreamNode::AddLinkedNode( " << ToNode->Name()
           << " ) new LinkedNode " << endl ;
    _LinkedNodes.resize( _LinkedNodesSize+1 ) ;
    _LinkedInPortsNumber.resize( _LinkedNodesSize+1 ) ;
    _LinkedNodes[ _LinkedNodesSize ] = ToNode ;
    _LinkedInPortsNumber[ _LinkedNodesSize ] = 0 ;
    IncrLinkedInPortsNumber( _LinkedNodesSize ) ;
//    _LinkedInPortsNumber[ _LinkedNodesSize ] = 1 ;
    SetLinkedNodeIndex( ToNode->Name() , _LinkedNodesSize ) ;
    index = _LinkedNodesSize ;
    _LinkedNodesSize++ ;
  }
  else {
    cdebug << Name() << "->GraphBase::StreamNode::AddLinkedNode( " << ToNode->Name()
           << " ) old LinkedNode " << _LinkedNodes[index ]->Name() << endl ;
    IncrLinkedInPortsNumber( index ) ;
//    _LinkedInPortsNumber[ index ] += 1 ;
  }
  cdebug << Name() << "->GraphBase::StreamNode::AddLinkedNode( " << ToNode->Name()
         << " ) LinkedNodesSize " << _LinkedNodesSize << " [ " << index
         << " ] _LinkedInPortsNumber " << _LinkedInPortsNumber[ index ] << endl ;

  index = ToNode->GetLinkedFromNodeIndex( Name() ) ;
  if ( index < 0 ) {
    cdebug << ToNode->Name() << "<-GraphBase::StreamNode::AddLinkedFromNode( " << Name()
           << " ) new LinkedFromNode " << endl ;
    ToNode->_LinkedFromNodes.resize( ToNode->_LinkedFromNodesSize+1 ) ;
    ToNode->_LinkedFromInPortsNumber.resize( ToNode->_LinkedFromNodesSize+1 ) ;
    ToNode->_LinkedFromNodes[ ToNode->_LinkedFromNodesSize ] = this ;
    ToNode->_LinkedFromInPortsNumber[ ToNode->_LinkedFromNodesSize ] = 0 ;
    ToNode->IncrLinkedFromInPortsNumber( ToNode->_LinkedFromNodesSize ) ;
//    ToNode->_LinkedFromInPortsNumber[ ToNode->_LinkedFromNodesSize ] = 1 ;
    ToNode->SetLinkedFromNodeIndex( Name() , ToNode->_LinkedFromNodesSize ) ;
    index = ToNode->_LinkedFromNodesSize ;
    ToNode->_LinkedFromNodesSize++ ;
  }
  else {
    cdebug << Name() << "->GraphBase::StreamNode::AddLinkedFromNode( " << ToNode->Name()
           << " ) old LinkedFromNode " << ToNode->_LinkedFromNodes[index ]->Name() << endl ;
    ToNode->IncrLinkedFromInPortsNumber( index ) ;
//    ToNode->_LinkedFromInPortsNumber[ index ] += 1 ;
  }
  cdebug << ToNode->Name() << "->GraphBase::StreamNode::AddLinkedFromNode( " << Name()
         << " ) LinkedFromNodesSize " << ToNode->_LinkedFromNodesSize << " [ " << index
         << " ] _LinkedFromInPortsNumber " << ToNode->_LinkedFromInPortsNumber[ index ] << endl ;
}

bool GraphBase::StreamNode::RemoveLinkedNode( GraphBase::StreamNode * ToNode ,
                                              bool DoLinkedFromNode ) {
  cdebug_in << "StreamNode::RemoveLinkedNode between " << Name() << " LinkedNodesSize "
            << LinkedNodesSize() << " and " << ToNode->Name() << " LinkedFromNodesSize "
            << ToNode->LinkedFromNodesSize() << endl ;
#if 1
  map< string , int >::iterator aMapOfLinkedNodesIterator ;
  int i = 0 ;
  for ( aMapOfLinkedNodesIterator = _MapOfLinkedNodes.begin() ;
        aMapOfLinkedNodesIterator != _MapOfLinkedNodes.end() && i < _LinkedNodesSize ;
        aMapOfLinkedNodesIterator++ ) {
    cdebug << "_MapOfLinkedNodes" << i++ << " of " << Name() << " : "
           << aMapOfLinkedNodesIterator->first << " --> " << aMapOfLinkedNodesIterator->second
           << endl ;
  }
#endif
#if 1
  map< string , int >::iterator aMapOfLinkedFromNodesIterator ;
  int ifrom = 0 ;
  for ( aMapOfLinkedFromNodesIterator = ToNode->_MapOfLinkedFromNodes.begin() ;
        aMapOfLinkedFromNodesIterator != ToNode->_MapOfLinkedFromNodes.end() &&
        ifrom < ToNode->_LinkedFromNodesSize ; aMapOfLinkedFromNodesIterator++ ) {
    cdebug << "_MapOfLinkedFromNodes" << ifrom++ << " of " << ToNode->Name() << " : "
           << aMapOfLinkedFromNodesIterator->first << " --> "
           << aMapOfLinkedFromNodesIterator->second << endl ;
  }
#endif

  int index = -1 ;
  if ( _LinkedNodesSize ) {
    index = GetLinkedNodeIndex( ToNode->Name() ) ;
  }
  if ( index >= 0 ) {
    cdebug << "GraphBase::StreamNode::RemoveLinkedNode to " << ToNode->Name() << " from "
           << Name() << " index : " << index << " " << ToNode->Name() << "->LinkedInPortsNumber "
           << _LinkedInPortsNumber[ index ] << " - 1" << endl ;
    DecrLinkedInPortsNumber( index ) ;
//    _LinkedInPortsNumber[ index ] -= 1 ;
    if ( _LinkedInPortsNumber[ index ] == 0 ) {
      _LinkedNodesSize-- ;
      cdebug << "GraphBase::StreamNode::RemoveLinkedNode new LinkedNodesSize "
             << _LinkedNodesSize << " " << ToNode->Name() << " removed from "
             << " linkednodes of " << Name() << endl ;
      int i ;
      for ( i = index ; i < _LinkedNodesSize ; i++ ) {
        _LinkedNodes[ i ] = _LinkedNodes[ i+1 ] ;
        _LinkedInPortsNumber[ i ] =  _LinkedInPortsNumber[ i+1 ] ;
        SetLinkedNodeIndex( _LinkedNodes[ i ]->Name() , i ) ;
      }
      DelLinkedNodeIndex( ToNode->Name() ) ;
      _LinkedNodes.resize( _LinkedNodesSize+1 ) ;
      _LinkedInPortsNumber.resize( _LinkedNodesSize+1 ) ;
    }
    else {
      cdebug << "StreamNode::RemoveLinkedNode to " << ToNode->Name() << " from "
             << Name() << " " << ToNode->Name() << "->LinkedInPortsNumber # 0 " << endl ;
    }
  }
  else if ( Kind() != SUPERV::GOTONode ) {
    cdebug << "StreamNode::RemoveLinkedNode index Error " << ToNode->Name() << " <-- " << Name() << " : " << index
           << " _LinkedNodesSize " << ToNode->_LinkedNodesSize << endl ;
  }
  int fromindex = -1 ;
  if ( ToNode->LinkedFromNodesSize() ) {
//  if ( ToNode->_LinkedFromNodesSize ) {
    fromindex = ToNode->GetLinkedFromNodeIndex( Name() ) ;
  }
  if ( fromindex >= 0 ) {
    cdebug << "GraphBase::StreamNode::RemoveLinkedFromNode from " << ToNode->Name() << " to "
           << Name() << " index : " << fromindex << " " << ToNode->Name()
           << "->LinkedFromInPortsNumber "
           << ToNode->_LinkedFromInPortsNumber[ fromindex ] << " - 1" << endl ;
//    ToNode->_LinkedFromInPortsNumber[ fromindex ] -= 1 ;
    ToNode->DecrLinkedFromInPortsNumber( fromindex ) ;
    if ( ToNode->LinkedFromInPortsNumber( fromindex ) == 0 ) {
//    if ( ToNode->_LinkedFromInPortsNumber[ fromindex ] == 0 ) {
      ToNode->DecrLinkedFromNodesSize() ;
//      ToNode->_LinkedFromNodesSize-- ;
      cdebug << "GraphBase::StreamNode::RemoveLinkedFromNode new LinkedFromNodesSize "
             << ToNode->_LinkedFromNodesSize << " " << Name() << " removed from "
             << " linkedFromnodes of " << ToNode->Name() << endl ;
      int i ;
      for ( i = 0 ; i < ToNode->_LinkedFromNodesSize ; i++ ) {
        if ( i >= fromindex ) {
          ToNode->_LinkedFromNodes[ i ] = ToNode->_LinkedFromNodes[ i+1 ] ;
          ToNode->_LinkedFromInPortsNumber[ i ] =  ToNode->_LinkedFromInPortsNumber[ i+1 ] ;
          ToNode->SetLinkedFromNodeIndex( ToNode->_LinkedFromNodes[ i ]->Name() , i ) ;
	}
        cdebug << "StreamNode::RemoveLinkedFromNode" << i << " "
               << ToNode->_LinkedFromNodes[ i ]->Name() << endl ;
      }
      ToNode->DelLinkedFromNodeIndex( Name() ) ;
      ToNode->_LinkedFromNodes.resize( ToNode->_LinkedFromNodesSize+1 ) ;
      ToNode->_LinkedFromInPortsNumber.resize( ToNode->_LinkedFromNodesSize+1 ) ;
    }
    else {
      cdebug << "StreamNode::RemoveLinkedFromNode from " << ToNode->Name()
             << "->LinkedFromInPortsNumber "
             << ToNode->_LinkedFromInPortsNumber[ fromindex ] << " # 0" << endl ;
    }
  }
  else if ( DoLinkedFromNode ) {
    cdebug << "StreamNode::RemoveLinkedFromNode index Error " << ToNode->Name() << " --> "
           << Name() << " : " << fromindex
           << " _LinkedFromNodesSize " << ToNode->_LinkedFromNodesSize << endl ;
  }
  else {
    cdebug << "StreamNode::RemoveLinkedFromNode index " << ToNode->Name() << " --> " << Name()
           << " : " << fromindex
           << " _LinkedFromNodesSize " << ToNode->_LinkedFromNodesSize << endl ;
    fromindex = 0 ; // No error in EndSwitchNodes
  }

#if 1
  map< string , int >::iterator aMapOfLinkedNodesIterator1 ;
  int ii = 0 ;
  for ( aMapOfLinkedNodesIterator1 = _MapOfLinkedNodes.begin() ;
        aMapOfLinkedNodesIterator1 != _MapOfLinkedNodes.end() && ii < _LinkedNodesSize ;
        aMapOfLinkedNodesIterator1++ ) {
    if ( aMapOfLinkedNodesIterator1->second ) {
      cdebug << "_MapOfLinkedNodes" << ii++ << " of " << Name() << " : "
             << aMapOfLinkedNodesIterator1->first << " --> " << aMapOfLinkedNodesIterator1->second
             << " " << _LinkedNodes[ aMapOfLinkedNodesIterator1->second - 1 ]->Name() << endl ;
    }
    else {
      cdebug << "_MapOfLinkedNodes" << ii << " of " << Name() << " : "
             << aMapOfLinkedNodesIterator1->second - 1 << endl ;
    }
  }
#endif
#if 1
  map< string , int >::iterator aMapOfLinkedFromNodesIterator1 ;
  int iifrom = 0 ;
  for ( aMapOfLinkedFromNodesIterator1 = ToNode->_MapOfLinkedFromNodes.begin() ;
        aMapOfLinkedFromNodesIterator1 != ToNode->_MapOfLinkedFromNodes.end() &&
        iifrom < ToNode->_LinkedFromNodesSize ; aMapOfLinkedFromNodesIterator1++ ) {
    if ( aMapOfLinkedFromNodesIterator1->second ) {
      cdebug << "_MapOfLinkedFromNodes" << iifrom++ << " of " << ToNode->Name() << " : "
             << aMapOfLinkedFromNodesIterator1->first << " --> "
             << aMapOfLinkedFromNodesIterator1->second << " "
             << ToNode->_LinkedFromNodes[ aMapOfLinkedFromNodesIterator1->second - 1 ]->Name()
             << endl ;
    }
    else {
      cdebug << "_MapOfLinkedFromNodes" << iifrom << " of " << ToNode->Name() << " : "
             << aMapOfLinkedFromNodesIterator1->second - 1 << endl ;
    }
  }
#endif
  cdebug_out << "StreamNode::RemoveLinkedNode between " << Name() << " and " << ToNode->Name()
             << " " << (index >= 0) << " " << (fromindex >= 0) << endl ;
  return (index >= 0 ) && (fromindex >= 0 ) ;
}

void GraphBase::StreamNode::ReNameLinkedNode( const char* OldNodeName ,
                                              const char* NewNodeName ) {
  cdebug_in << Name() << "->GraphBase::StreamNode::ReNameLinkedNode (" << OldNodeName << " , "
            << NewNodeName << ") _LinkedNodesSize " << _LinkedNodesSize << endl;
  int index = GetLinkedNodeIndex( OldNodeName ) ;
  if ( index >= 0 ) {
//    _MapOfLinkedNodes.erase( OldNodeName ) ;
    DelLinkedNodeIndex( OldNodeName ) ;
    SetLinkedNodeIndex( NewNodeName , index ) ;
  }
  else {
    cdebug << "GraphBase::StreamNode::ReNameLinkedNode ERROR " << OldNodeName << " not found in "
           << Name() << endl ;
  }
#if 1
  map< string , int >::iterator aMapOfLinkedNodesIterator1 ;
  int ii = 0 ;
  for ( aMapOfLinkedNodesIterator1 = _MapOfLinkedNodes.begin() ;
        aMapOfLinkedNodesIterator1 != _MapOfLinkedNodes.end() && ii < _LinkedNodesSize ;
        aMapOfLinkedNodesIterator1++ ) {
    cdebug << "_MapOfLinkedNodes" << ii++ << " of " << Name() << " : "
           << aMapOfLinkedNodesIterator1->first << " --> " << aMapOfLinkedNodesIterator1->second
           << " " << _LinkedNodes[ aMapOfLinkedNodesIterator1->second - 1 ]->Name() << endl ;
  }
#endif
  cdebug_out << Name() << "->GraphBase::StreamNode::ReNameLinkedNode" << endl ;
}

void GraphBase::StreamNode::ReNameLinkedFromNode( const char* OldNodeName ,
                                                  const char* NewNodeName ) {
  cdebug_in << Name() << "->GraphBase::StreamNode::ReNameLinkedFromNode (" << OldNodeName
            << " , " << NewNodeName << ") _LinkedFromNodesSize " << _LinkedFromNodesSize
            << endl;
  int index = GetLinkedFromNodeIndex( OldNodeName ) ;
  if ( index >= 0 ) {
//    _MapOfLinkedFromNodes.erase( OldNodeName ) ;
    DelLinkedFromNodeIndex( OldNodeName ) ;
    SetLinkedFromNodeIndex( NewNodeName , index ) ;
  }
  else {
    cdebug << Name() << "->GraphBase::StreamNode::ReNameLinkedFromNode ERROR " << OldNodeName
           << " not found in " << Name() << endl ;
  }
#if 1
  map< string , int >::iterator aMapOfLinkedFromNodesIterator1 ;
  int iifrom = 0 ;
  for ( aMapOfLinkedFromNodesIterator1 = _MapOfLinkedFromNodes.begin() ;
        aMapOfLinkedFromNodesIterator1 != _MapOfLinkedFromNodes.end() &&
        iifrom < _LinkedFromNodesSize ; aMapOfLinkedFromNodesIterator1++ ) {
    cdebug << "_MapOfLinkedFromNodes" << iifrom++ << " of " << Name() << " : "
           << aMapOfLinkedFromNodesIterator1->first << " --> "
           << aMapOfLinkedFromNodesIterator1->second << " "
           << _LinkedFromNodes[ aMapOfLinkedFromNodesIterator1->second - 1 ]->Name()
           << endl ;
  }
#endif
  cdebug_out << Name() << "->GraphBase::StreamNode::ReNameLinkedFromNode" << endl ;
}

void GraphBase::StreamNode::AddStreamLinkedNode( GraphBase::StreamNode * ToNode ) {
  int index = GetLinkedStreamNodeIndex( ToNode->Name() ) ;
  if ( index < 0 ) {
    cdebug_in << Name() << "->GraphBase::StreamNode::AddStreamLinkedNode( " << ToNode->Name()
              << " ) new LinkedNode " << endl ;
    _LinkedStreamNodes.resize( _LinkedStreamNodesSize+1 ) ;
    _LinkedInStreamPortsNumber.resize( _LinkedStreamNodesSize+1 ) ;
    _LinkedStreamNodes[ _LinkedStreamNodesSize ] = ToNode ;
    _LinkedInStreamPortsNumber[ _LinkedStreamNodesSize ] = 1 ;
    SetLinkedStreamNodeIndex( ToNode->Name() , _LinkedStreamNodesSize ) ;
    index = _LinkedStreamNodesSize ;
    _LinkedStreamNodesSize++ ;
  }
  else {
    cdebug_in << Name() << "->GraphBase::StreamNode::AddStreamLinkedNode( " << ToNode->Name()
              << " ) old LinkedNode " << _LinkedStreamNodes[index ]->Name() << endl ;
    _LinkedInStreamPortsNumber[ index ] += 1 ;
  }
  cdebug_out << Name() << "->GraphBase::StreamNode::AddStreamLinkedNode( " << ToNode->Name()
             << " ) LinkedStreamNodesSize " << _LinkedStreamNodesSize << " [ " << index
             << " ] _LinkedInStreamPortsNumber " << _LinkedInStreamPortsNumber[ index ] << endl ;
}

bool GraphBase::StreamNode::RemoveStreamLinkedNode( GraphBase::StreamNode * ToNode ) {
  int index = GetLinkedStreamNodeIndex( ToNode->Name() ) ;
  if ( index >= 0 ) {
    cdebug << "GraphBase::StreamNode::RemoveStreamLinkedNode( to " << ToNode->Name() << " from "
           << Name() << " index : " << index << " LinkedInStreamPortsNumber "
           << _LinkedInStreamPortsNumber[ index ] << " - 1" << endl ;
    _LinkedInStreamPortsNumber[ index ] -= 1 ;
    if ( _LinkedInStreamPortsNumber[ index ] == 0 ) {
      _LinkedStreamNodesSize-- ;
      cdebug << "GraphBase::StreamNode::RemoveStreamLinkedNode new LinkedNodesSize "
             << _LinkedStreamNodesSize << " " << ToNode->Name() << " removed from "
             << " linkednodes of " << Name() << endl ;
      int i ;
      for ( i = index ; i < _LinkedStreamNodesSize ; i++ ) {
        _LinkedStreamNodes[ i ] = _LinkedStreamNodes[ i+1 ] ;
        _LinkedInStreamPortsNumber[ i ] =  _LinkedInStreamPortsNumber[ i+1 ] ;
        SetLinkedStreamNodeIndex( _LinkedStreamNodes[ i ]->Name() , i ) ;
      }
      DelLinkedStreamNodeIndex( ToNode->Name() ) ;
      _LinkedStreamNodes.resize( _LinkedStreamNodesSize+1 ) ;
      _LinkedInStreamPortsNumber.resize( _LinkedStreamNodesSize+1 ) ;
    }
  }
  else {
    cdebug << "StreamNode::RemoveStreamLinkedNode Error index " << index << endl ;
  }
  return (index >= 0 ) ;
}

void GraphBase::StreamNode::ReNameStreamLinkedNode( const char* OldNodeName ,
                                              const char* NewNodeName ) {
  cdebug_in << "GraphBase::StreamNode::ReNameStreamLinkedNode (" << OldNodeName << " , "
            << NewNodeName << ")" << endl;
  int index = GetLinkedStreamNodeIndex( OldNodeName ) ;
  if ( index >= 0 ) {
//    _MapOfLinkedNodes.erase( OldNodeName ) ;
    DelLinkedStreamNodeIndex( OldNodeName ) ;
    SetLinkedStreamNodeIndex( NewNodeName , index ) ;
  }
  cdebug_out << "GraphBase::StreamNode::ReNameStreamLinkedNode" << endl ;
}

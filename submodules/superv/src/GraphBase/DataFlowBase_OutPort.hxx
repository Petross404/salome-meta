//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_OutPort.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_OUTPORT_HXX
#define _DATAFLOWBASE_OUTPORT_HXX

#include "DataFlowBase_DataPort.hxx"
#include "DataFlowBase_InPort.hxx"

namespace GraphBase {

  class OutPort : public DataPort {

    private:

      StatusOfPort _Connected ;

// Inports connected to this OutPort
      map< string , int >        _MapOfInPorts ;
      int                        _InPortsSize ;
      vector<InPort * >          _InPorts ;
      vector<SUPERV::Link_var >  _Links ;

    public:   

      OutPort() {
      };
      OutPort( const char *const * NodeName  ,
               const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
               const SUPERV::KindOfPort aKind = SUPERV::ServiceParameter ,
               const SALOME_ModuleCatalog::DataStreamDependency aDependency = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ) :
               DataPort( NodeName , aserviceParameter , aKind , aDependency) {
               _Connected = NotConnected ;
               _InPortsSize = 0 ; } ;
      virtual ~OutPort() {
         cdebug << "GraphBase::OutPort::~OutPort " << PortName() << endl ; } ;
      virtual void destroy() {
         int i ;
         for ( i = 0 ; i < _InPortsSize ; i++ ) {
           _MapOfInPorts.erase( _InPorts[ i ]->PortName() ) ;
//           _InPorts[ i ]->destroy() ; BUG ...
           _InPorts[ i ]->RemoveOutPort() ;
	 }
         _InPorts.resize( 0 ) ;
         cdebug << "GraphBase::OutPort::destroy " << PortName() << " "
                << NodeName() << endl ; } ;

// PortStatus has a meaning :
// It is NotConnected or PortConnected or DataConnected or ExternConnected
// Even if an OutPort may be connected to several ports the PortStatus is unique
      const StatusOfPort PortStatus() const {
            return _Connected; } ;
      void PortStatus( StatusOfPort PortSts ) {
//           cdebug << "OutPort::PortStatus " << NodeName() << " " << PortName() << " "
//                  << _Connected << " --> " << PortSts << endl ;
           _Connected = PortSts ; } ;
      const bool IsNotConnected() const {
            return ( _Connected == NotConnected ) ; } ;
//      const bool IsConnected() const {
//            return ( _Connected == PortConnected || _Connected == ExternConnected ) ; } ;
      const bool IsPortConnected() const {
            return ( _Connected == PortConnected ) ; } ;
      const bool IsDataConnected() const {
            return ( _Connected == DataConnected ) ; } ;
      const bool IsExternConnected() const {
            return ( _Connected == ExternConnected ) ; } ;
//      const bool IsPortAndDataConnected() const {
//            return ( _Connected == PortAndDataConnected ) ; } ;

      const int InPortsSize() const {
            return _InPortsSize ; } ;
      const InPort * InPorts( const int i ) const {
            return _InPorts[ i ] ; } ;
      InPort * ChangeInPorts( const int i ) {
            return _InPorts[ i ] ; } ;
      SUPERV::Link_var LinkObjRef( const int i ) const {
                        return _Links[ i ] ; } ;

//      bool RemoveLinks() ;

      bool HasInPort() const ;
      const InPort * GetInPort( const InPort * toPort ) ;
      SUPERV::Link_var InPortObjRef( const InPort * toPort ) ;
      bool AddInPort( InPort * toPort ) ;
      bool AddInPortObjRef( InPort * toPort , SUPERV::Link_var aLink ) ;

      bool RemoveInPort() ;
      bool RemoveInPort( InPort * toPort ) ;
      bool ReNameInPort( const char* OldNodePortName ,
                         const char* NewNodePortName ) ;

  } ;

} ;

ostream & operator<< (ostream &,const GraphBase::OutPort &);

#endif

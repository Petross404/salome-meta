//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_InLineNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_InLineNode.hxx"

GraphBase::InLineNode::InLineNode() :
  GraphBase::ComputingNode::ComputingNode() {
  _FuncName = NULL ;
  _PythonFunction = SUPERV::ListOfStrings() ;
  _MyPyRunMethod = NULL ;
}

GraphBase::InLineNode::InLineNode( CORBA::ORB_ptr ORB ,
                                   SALOME_NamingService* ptrNamingService ,
                                   const char * aDataFlowName ,
                                   const SUPERV::KindOfNode DataFlowkind ,
                                   int * Graph_prof_debug ,
                                   ofstream * Graph_fdebug ) :
  GraphBase::ComputingNode::ComputingNode( ORB , ptrNamingService , aDataFlowName , DataFlowkind ,
                                           Graph_prof_debug , Graph_fdebug ) {
  _FuncName = NULL ;
  _PythonFunction = SUPERV::ListOfStrings() ;
  _MyPyRunMethod = NULL ;
  cdebug << "GraphBase::InLineNode::Node "  << this 
         << "' _Name "
         << (void *) Name() << " '" << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << endl ;
}

GraphBase::InLineNode::InLineNode( CORBA::ORB_ptr ORB ,
                                   SALOME_NamingService* ptrNamingService ,
                                   const SALOME_ModuleCatalog::Service& NodeService ,
                                   const char *NodeName ,
                                   const SUPERV::KindOfNode akind ,
                                   const SUPERV::SDate NodeFirstCreation ,
                                   const SUPERV::SDate NodeLastModification  ,
                                   const char * NodeEditorRelease ,
                                   const char * NodeAuthor ,
                                   const char * NodeComment ,
                                   const bool   GeneratedName ,
                                   const long   X ,
                                   const long   Y ,
                                   int * Graph_prof_debug ,
                                   ofstream * Graph_fdebug ) :
  GraphBase::ComputingNode::ComputingNode( ORB , ptrNamingService ,
                                           NodeService ,
                                           NodeName , akind , NodeFirstCreation ,
                                           NodeLastModification  ,
                                           NodeEditorRelease ,
                                           NodeAuthor , NodeComment , GeneratedName ,
                                           X , Y ,
                                           Graph_prof_debug , Graph_fdebug ) {
  _FuncName = NULL ;
  _PythonFunction = SUPERV::ListOfStrings() ;
  _MyPyRunMethod = NULL ;
  cdebug_in << "GraphBase::InLineNode::Node "  << this 
            << "' _Name "
            << (void *) Name() << " '" << Name() << " _Comment "
            << (void *) Comment() << " "  << Comment() << " " 
            << " KindOfNode " << Kind()
            << " ServiceName " << ServiceName() << " In("
            << ServiceInParameter().length()
            << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  cdebug_out << "GraphBase::InLineNode::Node" << endl;
}

GraphBase::InLineNode::InLineNode( CORBA::ORB_ptr ORB ,
                                   SALOME_NamingService* ptrNamingService ,
                                   const char * FuncName ,
                                   const SUPERV::ListOfStrings & aPythonFunction ,
                                   const char *NodeName ,
                                   const SUPERV::KindOfNode akind ,
                                   const SUPERV::SDate NodeFirstCreation ,
                                   const SUPERV::SDate NodeLastModification  ,
                                   const char * NodeEditorRelease ,
                                   const char * NodeAuthor ,
                                   const char * NodeComment ,
                                   const bool   GeneratedName ,
                                   const long   X ,
                                   const long   Y ,
                                   int * Graph_prof_debug ,
                                   ofstream * Graph_fdebug ) :
  GraphBase::ComputingNode::ComputingNode( ORB , ptrNamingService ,
                                           SALOME_ModuleCatalog::Service() ,
                                           NodeName , akind , NodeFirstCreation ,
                                           NodeLastModification  , NodeEditorRelease ,
                                           NodeAuthor , NodeComment , GeneratedName ,
                                           X , Y ,
                                           Graph_prof_debug , Graph_fdebug ) {

  cdebug_in << "GraphBase::InLineNode::Node "  << this 
            << "' _Name "
            << (void *) Name() << " '" << Name() << " _Comment "
            << (void *) Comment() << " "  << Comment() << " " 
            << " KindOfNode " << Kind() << " FuncName " << FuncName
            << " ServiceName " << ServiceName() << " In("
            << ServiceInParameter().length()
            << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  _FuncName = NULL ;
  SetPythonFunction( FuncName , aPythonFunction ) ;
  _MyPyRunMethod = NULL ;
  SALOME_ModuleCatalog::Service aNodeService ;
  aNodeService.ServiceName = NodeName ;
// python -> service
  SetService( aNodeService ) ;
  cdebug_out << "GraphBase::InLineNode::Node" << endl;
}

GraphBase::InLineNode::~InLineNode() {
  cdebug << "GraphBase::InLineNode::~Node "  << this 
         << " _Name "
         << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;
//  if ( _ComponentName != NULLSTRING )
//    delete [] _ComponentName ;
//  delete [] _Name ;
//  delete [] _EditorRelease ;
//  if ( _Author != NULLSTRING )
//    delete [] _Author ;
//  if ( _Computer != FACTORYSERVER )
//    delete [] _Computer;
//  if ( _Comment != NULLSTRING )
//    delete [] _Comment;
}

void GraphBase::InLineNode::SetPythonFunction( const char * FuncName ,
                                               const SUPERV::ListOfStrings & aPythonFunction ) {
  int i ;
  cdebug << "GraphBase::InLineNode::PythonFunction length " << aPythonFunction.length() ;
  if ( aPythonFunction.length() == 1 ) {
    cdebug << " aPythonFunction[ 0 ] '" << aPythonFunction[ 0 ] << "'" ;
  }
  cdebug << " FuncName " << FuncName << endl ;
  _FuncName = my_strblkdup( FuncName ) ;
  cdebug << "GraphBase::InLineNode::SetPythonFunction FuncName '" << FuncName << "' --> '"
         << _FuncName << "'" << endl ;
  if ( ( aPythonFunction.length() == 0 ) ||
         ( aPythonFunction.length() == 1 && strlen( aPythonFunction[ 0 ] ) == 0 ) ) {
    _PythonFunction.length( 0 ) ;
  }
  else {
    _PythonFunction.length( aPythonFunction.length() ) ;
    for ( i = 0 ; i < (int ) aPythonFunction.length() ; i++ ) {
      cdebug << aPythonFunction[ i ] << endl ;
      _PythonFunction[ i ] = CORBA::string_dup( aPythonFunction[ i ] ) ;
    }
  }
}

//bool GraphBase::InLineNode::SaveXML( ostream &f , char *Tabs ,
bool GraphBase::InLineNode::SaveXML( QDomDocument & Graph , QDomElement & info ,
                                     int XCoordinate , int YCoordinate ) const {
  GraphBase::ListOfFuncName aFuncNames ;
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  if ( PyFuncName() ) {
    aFuncNames.resize( 1 ) ;
//JR 24.02.2005 Memory Leak    aFuncNames[0] = CORBA::string_dup( PyFuncName() ) ;
    aFuncNames[0] = string( PyFuncName() ) ;
    aPythonFunction.resize( 1 ) ;
    aPythonFunction[0] = PythonFunction() ;
  }
//  return GraphBase::ComputingNode::SaveXML( f , Tabs , "" , "" , "" , "" ,
  return GraphBase::ComputingNode::SaveXML( Graph , info , "" , "" , "" , "" ,
                                            aFuncNames , aPythonFunction ,
                                            XCoordinate , YCoordinate ) ;
}

bool GraphBase::InLineNode::SavePY(ostream &f , const char * aGraphName ,
                                   int XCoordinate , int YCoordinate ) const {
  GraphBase::ListOfFuncName aFuncNames ; 
  GraphBase::ListOfPythonFunctions aPythonFunction ;
  if ( PyFuncName() ) {
    aFuncNames.resize( 1 ) ;
//JR 24.02.2005 Memory Leak    aFuncNames[0] = CORBA::string_dup( PyFuncName() ) ;
    aFuncNames[0] = string( PyFuncName() ) ;
    aPythonFunction.resize( 1 ) ;
    aPythonFunction[0] = PythonFunction() ;
  }
  return GraphBase::ComputingNode::SavePY( f , aGraphName , "" , "" , "" , NULL ,
                                           aFuncNames , aPythonFunction ,
                                           XCoordinate , YCoordinate ) ;
}

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Base.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

#ifndef _DATAFLOW_BASE_HXX
#define _DATAFLOW_BASE_HXX

//#include <strstream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <map>
#include <vector>
#include <list>

#include <qdom.h>

#include "CORBA.h"

#include <SALOMEconfig.h>
#include CORBA_CLIENT_HEADER(SUPERV)

#include "utilities.h"
#include "OpUtil.hxx"

#define ExitWhenNodeAborted 1

extern char *SuperVision_Version ;

extern char *NULLSTRING ;

extern char *FACTORYSERVER ;
extern char *FACTORYSERVERPY ;

inline char * my_strdup( const char * s ) {
  if (s == NULL) return NULL;

  size_t l = strlen(s) + 1;
  char * t = new char[l];
  strcpy(t, s);
  return t;
}

inline char * my_strblkdup( const char * s ) {
  if (s == NULL) return NULL ;

  size_t l = strlen(s) + 1 ;
  int i ;
  for ( i = l - 2 ; i >= 0 ; i-- ) {
    if ( s[i] != ' ' ) {
      break ;
    }
    l = l - 1 ;
  }
  char * t = new char[l] ;
  strncpy(t, s , l-1) ;
  t[l-1] = '\0' ;
  return t;
}

//string DataStreamTypeToString( const SALOME_ModuleCatalog::DataStreamType aDataStreamType ) ;
int StringToDataStreamType( const char * aIdlType ) ;

string KindOfDataStreamTraceToString( SUPERV::KindOfDataStreamTrace aDataStreamTrace ) ;

string DataStreamDependencyToString( const SALOME_ModuleCatalog::DataStreamDependency aDataStreamDependency ) ;

string DataStreamToString( const char* aDataStreamType ) ;

//enum StatusOfPort { NotConnected , PortConnected , PortAndDataConnected ,
enum StatusOfPort { NotConnected , PortConnected ,
                    DataConnected , ExternConnected } ;

namespace GraphBase {

  struct SCoord {
    long theX ;
    long theY ;
  };
    
  typedef vector<SCoord> ListOfCoords;

  typedef string FuncName ;

  typedef vector< FuncName > ListOfFuncName ;

  typedef vector<const SUPERV::ListOfStrings *> ListOfPythonFunctions ;

  typedef vector<SALOME_ModuleCatalog::ServicesParameter> ListOfParameters;

  struct InDataStreamParameter {
     SALOME_ModuleCatalog::ServicesDataStreamParameter theDataStreamParameter ;
     SUPERV::KindOfSchema                              theKindOfSchema ;
     SUPERV::KindOfInterpolation                       theKindOfInterpolation ;
     SUPERV::KindOfExtrapolation                       theKindOfExtrapolation ;
   };
  typedef vector<InDataStreamParameter> ListOfInDataStreamParameters;

  struct OutDataStreamParameter {
     SALOME_ModuleCatalog::ServicesDataStreamParameter theDataStreamParameter ;
     long                                              theNumberOfValues ;
   };
  typedef vector<OutDataStreamParameter> ListOfOutDataStreamParameters;

  struct SNode {
    string                        theComponentName ;
    string                        theInterfaceName ;
    string                        theName ;
    SUPERV::KindOfNode            theKind ;
    long                          theTimeout ;
    SUPERV::KindOfDataStreamTrace theDataStreamTrace ;
    double                        theDeltaTime ;
    string                        theCoupledNode ;
//    int                           theDataStreamInArgsNumber ;
//    int                           theDataStreamOutArgsNumber ;
    SALOME_ModuleCatalog::Service theService ;
    ListOfInDataStreamParameters  theListOfInDataStreams ;
    ListOfOutDataStreamParameters theListOfOutDataStreams ;
    ListOfFuncName                theListOfFuncName ;
    ListOfPythonFunctions         theListOfPythonFunctions ;
    SUPERV::SDate                 theFirstCreation ;
    SUPERV::SDate                 theLastModification ;
    string                        theEditorRelease ;
    string                        theAuthor ;
    string                        theContainer;
    string                        theComment;
    SCoord                        theCoords ;
  };

  typedef vector<SNode> ListOfSNodes;

  struct ServicesData {
    SALOME_ModuleCatalog::ServicesParameter aDataParameter ;
    CORBA::Any                              aDataValue;
  };

  typedef vector<ServicesData> ListOfServicesData;

  struct SLink {
    string       FromNodeName ;
    string       FromServiceParameterName ;
    string       ToNodeName ;
    string       ToServiceParameterName ;
    CORBA::Any   aLinkValue;
    ListOfCoords aListOfCoords ;
  };

  typedef vector<SLink> ListOfSLinks;

  struct SGraph {
    SNode        Info ;
    ListOfSNodes  Nodes ;
    ListOfSLinks  Links ;
    ListOfSLinks  Datas ;
  };

  typedef vector<SGraph> ListOfSGraphs;

  class Base {

    private:   

      CORBA::ORB_ptr _Orb ;

    public:   

      int      * _prof_debug ;
      ofstream * _fdebug ;
                     
      Base() ;
      virtual ~Base() {} ;

      void SetDebug( CORBA::ORB_ptr ORB ,
                     int * prof_debug , ofstream * fdebug ) ;

      char * ObjectToString( CORBA::Object_ptr obj ) const ;
      CORBA::Object_ptr StringToObject(const char * obj ) const ;

      ostream & AnyValue( const CORBA::Any & anAny );

  } ;

# ifdef _DEBUG_

#   define cdebug if ( GraphBase::Base::_fdebug )  *GraphBase::Base::_fdebug << "      "
#   define cdebug_in if ( GraphBase::Base::_fdebug ) *GraphBase::Base::_fdebug << endl << "-->" << " "
#   define cdebug_out if ( GraphBase::Base::_fdebug ) *GraphBase::Base::_fdebug << endl << "<--" << " "

# else

#   define cdebug while (false) std::cerr
#   define cdebug_in  cdebug
#   define cdebug_out cdebug
#   define debugFile(x) 

#endif

} ;

//ostream & operator<< (ostream &,const SALOME_ModuleCatalog::DataStreamType &);

#endif

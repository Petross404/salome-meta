//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Port.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

#ifndef _DATAFLOWBASE_PORT_HXX
#define _DATAFLOWBASE_PORT_HXX

#include <list>
#include <vector>

#include "DataFlowBase_ServicesParameter.hxx"

namespace GraphBase {

  class Port : public ServicesParameter {

    private:

      const char *const * _NodeName ;
      SUPERV::Port_var    _Port ;
      SUPERV::KindOfPort  _KindOfPort ;

      vector<long > _X ;    
      vector<long > _Y ;    

      SALOME_ModuleCatalog::DataStreamDependency _Dependency ; // Time or Iteration only for DataStreamPorts

    public:   

      Port() {
           _NodeName = NULL ;
           _Port = SUPERV::Port::_nil() ;
           _KindOfPort = SUPERV::UndefinedParameter ;
           _Dependency  = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ; } ;
      Port( const char *const * NodeName ,
            const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
            const SUPERV::KindOfPort aKindOfPort = SUPERV::ServiceParameter ,
            const SALOME_ModuleCatalog::DataStreamDependency aDependency = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ) :
            ServicesParameter( aserviceParameter ) {
            _NodeName = NodeName ;
//            cdebug << "Port::Port _NodeName " << _NodeName << " = " << (void * ) *_NodeName
//                   << " = " << _NodeName << " in Port " << PortName() << endl ;
            _Port = SUPERV::Port::_nil() ;
            _KindOfPort = aKindOfPort ;
            if ( IsDataStream() ) {
              _Dependency = aDependency ;
	    }
            else {
              _Dependency = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ;
	    } } ;
      virtual ~Port() {
         cdebug << "~Port _NodeName " << *_NodeName << endl ; } ;

      SUPERV::Port_var ObjRef() const { return _Port ; } ;
      void ObjRef(SUPERV::Port_var aPort) {
                  _Port = aPort ; } ;

      const char * NodeName() const {
//            cdebug << "Port::NodeName _NodeName " << _NodeName << " = " << (void * ) *_NodeName
//                   << " = " << *_NodeName << " in Port " << PortName() << endl ;
            return *_NodeName ; } ;
      const char * NodePortName() const {
//            cout << "NodePortName " << hex << (void *) _NodeName << " "
//                 << dec << _NodeName << endl ;
            char * _NodePortName = new char [ strlen( *_NodeName ) +
                                   strlen( ServicesParameterName() ) + 4 ] ;
            strcpy( _NodePortName , *_NodeName ) ;
//            strcat( _NodePortName , "\\" ) ;
            strcat( _NodePortName , "__" ) ;
            strcat( _NodePortName , ServicesParameterName() ) ;
            return _NodePortName ; } ;          

      const char * PortName() const {
            if ( this == NULL )
              return NULLSTRING ;
            return ServicesParameterName() ; } ;
      const char * PortType() const { return ServicesParameterType() ; } ;

      void PortType(char * aParametertype ) { ServicesParameterType( aParametertype ) ; } ;
      void Kind( SUPERV::KindOfPort aKindOfPort ) {
           if ( _KindOfPort == SUPERV::GateParameter && aKindOfPort == SUPERV::InLineParameter ) {
             cdebug << "GraphBase::Port::Kind " << _KindOfPort << " --> " << aKindOfPort
                    << endl ;
  	   }
           _KindOfPort = aKindOfPort ; } ;
      const SUPERV::KindOfPort Kind() const {
            return _KindOfPort ; } ;
      bool IsParam() const {
           return _KindOfPort == SUPERV::ServiceParameter ; } ;
      bool IsGate() const {
           return _KindOfPort == SUPERV::GateParameter ||
                  _KindOfPort == SUPERV::GOTOParameter ; } ;
      bool IsInLine() const {
           return _KindOfPort == SUPERV::InLineParameter ; } ;
      bool IsLoop() const {
           return _KindOfPort == SUPERV::LoopParameter ; } ;
      bool IsSwitch() const {
           return _KindOfPort == SUPERV::SwitchParameter ; } ;
      bool IsEndSwitch() const {
           return _KindOfPort == SUPERV::EndSwitchParameter ; } ;
      bool IsGOTO() const {
           return _KindOfPort == SUPERV::GOTOParameter ; } ;
      bool IsDataStream() const {
           return _KindOfPort == SUPERV::DataStreamParameter ; } ;

      const SALOME_ModuleCatalog::DataStreamDependency Dependency() const {
                                     return _Dependency ; } ;
      bool Dependency( SALOME_ModuleCatalog::DataStreamDependency aDependency ) {
           if ( IsDataStream() ) {
             _Dependency = aDependency ;
             return true ;
	   }
           return false ; } ;

      bool AddCoord( const int nxy , const int *x , const int *y ) ;
      bool AddCoord( const int index , const int x , const int y ) ;
      bool ChangeCoord( const int index ,
                        const int x ,
                        const int y ) ;
      bool RemoveCoord( const int index ) ;
      bool RemoveCoords() ;
      int GetCoord() const ;
      bool GetCoord( int *x , int *y ) const ;
      const GraphBase::ListOfCoords * Coords() const ;
      bool GetCoord( const int index , CORBA::Long &x , CORBA::Long &y ) const ;

  } ;

} ;

ostream & operator<< (ostream &,const SUPERV::KindOfPort &);

ostream & operator<< (ostream &,const SALOME_ModuleCatalog::DataStreamDependency &);

ostream & operator<< (ostream &,const StatusOfPort &);

#endif

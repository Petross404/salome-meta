//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//  File   : DataFlowBase_ComputingNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_StreamGraph.hxx"
//#include "DataFlowBase_LoopNode.hxx"

// Screen single quotes
static string protectQuotes (const string theText)
{
  string aRes (theText);
  unsigned int pos;

  // screen back slash
  for (pos = 0; pos < aRes.size(); pos++) {
    pos = aRes.find("\\", pos);
    if (pos < 0 || pos > aRes.size()) break;

    aRes.insert(pos, "\\");
    pos++;

    // screen symbol after back slash (except single quote, which will be processed below)
    if (pos + 1 < aRes.size() && aRes[pos + 1] != '\'') {
      aRes.insert(pos + 1, "\\");
      pos += 2;
    }
  }

  // screen single quote
  for (pos = 0; pos < aRes.size(); pos++) {
    pos = aRes.find("'", pos);
    if (pos < 0 || pos > aRes.size()) break;

    aRes.insert(pos, "\\");
    pos++;
  }

  return aRes;
}

static void InitFields( //SUPERV::KindOfNode &_Kind ,
                        SUPERV::SDate      &_FirstCreation ,
                        SUPERV::SDate      &_LastModification ,
                        char *             &_EditorRelease ,
                        char *             &_Author ,
                        char *             &_Comment ,
                        //bool               &_HeadNode ,
                        bool               &_GeneratedName ,
                        //int                &_DataStreamInPortsNumber ,
                        //int                &_DataStreamOutPortsNumber ,
                        int                &_ConnectedInPortsNumber ,
                        int                &_DecrConnectedInPortsNumber ) {
                        //int                &_LinkedNodesSize ,
                        //int                &_SubGraphNumber ) {
  time_t T = time(NULL);
  struct tm * Tm = localtime(&T);

//  _Kind = SUPERV::DataFlowGraph ;

  _FirstCreation.Second = _LastModification.Second = Tm->tm_sec;
  _FirstCreation.Minute = _LastModification.Minute = Tm->tm_min;
  _FirstCreation.Hour   = _LastModification.Hour   = Tm->tm_hour;
  _FirstCreation.Day    = _LastModification.Day    = Tm->tm_mday;
  _FirstCreation.Month  = _LastModification.Month  = Tm->tm_mon + 1;
  _FirstCreation.Year   = _LastModification.Year   = Tm->tm_year + 1900;

  _EditorRelease = new char[ strlen( SuperVision_Version ) + 1 ] ;
  strcpy( _EditorRelease , SuperVision_Version ) ;
  _Author = NULLSTRING ;
//  _Computer = new char[ strlen( FACTORYSERVER ) + 1 ] ;
//  strcpy( _Computer  , FACTORYSERVER ) ;
  _Comment = NULLSTRING ;

//  _SubGraphNumber = 0 ;
//  _HeadNode = false ;
  _GeneratedName = false ;

//  _DataStreamInPortsNumber = 0 ;
//  _DataStreamOutPortsNumber = 0 ;

  _ConnectedInPortsNumber = 0 ;
  _DecrConnectedInPortsNumber = 0 ;
//  _LinkedNodesSize = 0 ;
//  _SubGraphNumber = 0 ;
}

GraphBase::ComputingNode::ComputingNode() :
//  GraphBase::PortsOfNode::PortsOfNode() {
  GraphBase::StreamNode::StreamNode() {

  InitFields( //_Kind ,
              _FirstCreation ,
              _LastModification ,
              _EditorRelease ,
              _Author ,
              _Comment ,
              //_HeadNode ,
              _GeneratedName ,
              //_DataStreamInPortsNumber ,
              //_DataStreamOutPortsNumber ,
              _ConnectedInPortsNumber ,
              _DecrConnectedInPortsNumber ) ;
              //_LinkedNodesSize ,
              //_SubGraphNumber ) ;
  Kind( SUPERV::DataFlowGraph ) ;
  _NamingService = NULL ;
  _Node_var = SUPERV::CNode::_nil() ;
  _Node_Impl = NULL ;
  _InNode = NULL ;
  _ThreadNo = pthread_self() ;
  cdebug << "GraphBase::Node::Node "  << this << " "  << endl ;

}

GraphBase::ComputingNode::ComputingNode( CORBA::ORB_ptr ORB ,
                                         SALOME_NamingService* ptrNamingService ,
                                         const char * aDataFlowName ,
                                         const SUPERV::KindOfNode DataFlowkind ,
                                         int * Graph_prof_debug ,
                                         ofstream * Graph_fdebug ) :
//  GraphBase::PortsOfNode::PortsOfNode( aDataFlowName ) {
  GraphBase::StreamNode::StreamNode( aDataFlowName , DataFlowkind ,
                                     Graph_prof_debug , Graph_fdebug ) {

//  MESSAGE( "GraphBase::ComputingNode::ComputingNode " << aDataFlowName << " Graph_prof_debug " << Graph_prof_debug ) ;
  InitFields( //_Kind ,
              _FirstCreation ,
              _LastModification ,
              _EditorRelease ,
              _Author ,
              _Comment ,
              //_HeadNode ,
              _GeneratedName ,
              //_DataStreamInPortsNumber ,
              //_DataStreamOutPortsNumber ,
              _ConnectedInPortsNumber ,
              _DecrConnectedInPortsNumber ) ;
              //_LinkedNodesSize ,
              //_SubGraphNumber ) ;

//  Kind( SUPERV::DataFlowGraph ) ;
  Kind( DataFlowkind ) ;
  _ORB = CORBA::ORB::_duplicate( ORB ) ;
  _NamingService = ptrNamingService ;
  _Node_var = SUPERV::CNode::_nil() ;
  _Node_Impl = NULL ;
  _InNode = NULL ;
  _ThreadNo = pthread_self() ;

  if ( Graph_prof_debug ) {
//    MESSAGE( "GraphBase::ComputingNode::ComputingNode --> SetDebug" ) ;
//    cout << "GraphBase::ComputingNode::ComputingNode --> SetDebug" << endl ;
    _Graph_prof_debug = Graph_prof_debug ;
    _Graph_fdebug = Graph_fdebug ;
    SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
  }
//  else {
//    cout << "GraphBase::ComputingNode::ComputingNode NO SetDebug" << endl ;
//  }
  DefPortsOfNode( ORB , SALOME_ModuleCatalog::Service() , NamePtr() , Kind() , Graph_prof_debug , Graph_fdebug ) ;
  cdebug << "GraphBase::ComputingNode::ComputingNode "  << this
         << " Name '" << Name() << "' "  << _FirstCreation
         << " "  << _LastModification << endl ;
}

GraphBase::ComputingNode::ComputingNode( CORBA::ORB_ptr ORB ,
                                         SALOME_NamingService* ptrNamingService ,
                                         const SALOME_ModuleCatalog::Service& aService ,
                                         const char *NodeName ,
                                         const SUPERV::KindOfNode akind ,
                                         const SUPERV::SDate NodeFirstCreation ,
                                         const SUPERV::SDate NodeLastModification  ,
                                         const char * NodeEditorRelease ,
                                         const char * NodeAuthor ,
                                         const char * NodeComment ,
                                         const bool   GeneratedName ,
                                         const long   X ,
                                         const long   Y ,
                                         int * Graph_prof_debug ,
                                         ofstream * Graph_fdebug ) :
//  GraphBase::PortsOfNode::PortsOfNode() {
  GraphBase::StreamNode::StreamNode( NodeName , akind , Graph_prof_debug , Graph_fdebug ) {

  _ORB = CORBA::ORB::_duplicate( ORB ) ;
  _NamingService = ptrNamingService ;
  _Node_var = SUPERV::CNode::_nil() ;
  _Node_Impl = NULL ;
  _InNode = NULL ;
  _ThreadNo = pthread_self() ;

  Kind( akind ) ;
//  _Kind = akind ;

  time_t T = time(NULL);
  struct tm * Tm = localtime(&T);

  _FirstCreation.Second = _LastModification.Second = Tm->tm_sec;
  _FirstCreation.Minute = _LastModification.Minute = Tm->tm_min;
  _FirstCreation.Hour   = _LastModification.Hour   = Tm->tm_hour;
  _FirstCreation.Day    = _LastModification.Day    = Tm->tm_mday;
  _FirstCreation.Month  = _LastModification.Month  = Tm->tm_mon + 1;
  _FirstCreation.Year   = _LastModification.Year   = Tm->tm_year + 1900;

  if ( NodeEditorRelease != NULLSTRING ) {
    _EditorRelease = new char[ strlen( NodeEditorRelease ) + 1 ] ;
    strcpy( _EditorRelease , NodeEditorRelease ) ;
  }
  else {
    _EditorRelease = new char[ strlen( SuperVision_Version ) + 1 ] ;
    strcpy( _EditorRelease , SuperVision_Version ) ;
  }

  if ( NodeAuthor != NULLSTRING ) {
    _Author = new char[ strlen( NodeAuthor ) + 1 ] ;
    strcpy( _Author  , NodeAuthor ) ;
  }
  else {
    _Author = NULLSTRING ;
  }

  if ( NodeComment != NULLSTRING ) {
    _Comment = new char[ strlen( NodeComment ) + 1 ] ;
    strcpy( _Comment  , NodeComment ) ;
  }
  else {
    _Comment = NULLSTRING ;
  }

//  _SubGraphNumber = 0 ;
//  _HeadNode = false ;
  _GeneratedName = GeneratedName ;

//  const char *aNodeName = NodeName ;
//  _Name = new char[strlen(aNodeName)+1];
//  strcpy(_Name , aNodeName);

//  _DataStreamInPortsNumber = 0 ;
//  _DataStreamOutPortsNumber = 0 ;

  _ConnectedInPortsNumber = 0 ;
  _DecrConnectedInPortsNumber = 0 ;
//  _LinkedNodesSize = 0 ;

  _X = X ;
  _Y = Y ;

  _Graph_prof_debug = Graph_prof_debug ;
  _Graph_fdebug = Graph_fdebug ;
//  MESSAGE( "GraphBase::ComputingNode::ComputingNode " << NodeName
//           << " _Graph_prof_debug " << _Graph_prof_debug ) ;
  SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
  cdebug_in << "GraphBase::ComputingNode::ComputingNode(" << aService.ServiceName << "," << NodeName << ","
            << akind << ")" << endl;
  

  DefPortsOfNode( ORB , aService , NamePtr() , Kind() , Graph_prof_debug , Graph_fdebug ) ;
  cdebug << "GraphBase::ComputingNode::ComputingNode "  << this
         << " Name '" << Name()
         << "' KindOfNode " << Kind()
         << " ServiceName '" << ServiceName() << "' In(" << ServiceInParameter().length()
         << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  cdebug_out << "GraphBase::ComputingNode::ComputingNode" << endl;
}

GraphBase::ComputingNode::~ComputingNode() {
  cdebug << "GraphBase::ComputingNode::~ComputingNode "  << this
         << " Name() "<< Name() << " _Comment "
         << (void *) _Comment << " "  << _Comment << " "  << endl ;
}

//->StreamNode bool GraphBase::ComputingNode::Name( const char * aName) {
//->StreamNode   cdebug_in << "GraphBase::ComputingNode::Name " << _Name << endl;
//->StreamNode   if ( _Name ) {
//->StreamNode     cdebug << "GraphBase::ComputingNode::ReName "  << _Name << " --> " << aName << endl ;
//->StreamNode     delete [] _Name ;
//->StreamNode   }
//->StreamNode   _Name = new char[strlen(aName)+1] ;
//->StreamNode   strcpy( _Name , aName ) ;
//->StreamNode   cdebug_out << "GraphBase::ComputingNode::Name " << _Name << endl;
//->StreamNode   return true ;
//->StreamNode }

SUPERV::SDate GraphBase::ComputingNode::FirstCreation () const {
//  cdebug << "GraphBase::ComputingNode::FirstCreation "
//         << " Name '" << Name() << "' "  << _FirstCreation << " "
//         << _LastModification << endl ;
  return _FirstCreation;
}

SUPERV::SDate GraphBase::ComputingNode::LastModification () const {
  return _LastModification ;
}

void GraphBase::ComputingNode::FirstCreation(const SUPERV::SDate aDate ) {
  _FirstCreation = aDate ;
}

void GraphBase::ComputingNode::LastModification(const SUPERV::SDate aDate ) {
  _LastModification = aDate ;
}

bool GraphBase::ComputingNode::EditorRelease(const char * c){
  if ( _EditorRelease && _EditorRelease != NULLSTRING )
    delete [] _EditorRelease;
  _EditorRelease = my_strdup(c);
  return true ;
}

bool GraphBase::ComputingNode::Author(const char * a) {
  cdebug_in << "GraphBase::ComputingNode::Author " << _Author << endl;
  if ( _Author && _Author != NULLSTRING )
    delete _Author;
  _Author = my_strdup(a);
  cdebug_out << "GraphBase::ComputingNode::Author " << _Author << endl;
  return true ;
}

bool GraphBase::ComputingNode::Comment(const char *c) {
  cdebug_in << "GraphBase::ComputingNode::Comment " << _Comment << endl;
  if ( _Comment != NULLSTRING )
    delete [] _Comment;
  _Comment = my_strdup(c);
  cdebug_out << "GraphBase::ComputingNode::Comment " << _Comment << endl;
  return true ;
}

void GraphBase::ComputingNode::NodePort( const char * NodeName ,
                                const char * ServiceParameterName ,
                                char ** aNode , char ** aPort ) {
  if ( strcmp( NodeName , Name() ) ) {
    *aNode = my_strdup( NodeName ) ;
    *aPort = my_strdup( ServiceParameterName ) ;
  }
  else {
//    char * BPort = strchr( ServiceParameterName , '\\' ) ;
    char * BPort = (char * ) ServiceParameterName ;
    while ( ( BPort = strchr( BPort , '_' ) ) ) {
      if ( BPort[1] == '_' ) {
        int len = BPort - ServiceParameterName ;
        *aNode = new char [ len + 1 ] ;
        strncpy( *aNode , ServiceParameterName , len ) ;
        (*aNode)[ len ] = '\0' ;
        *aPort = my_strdup( &BPort[ 2 ] ) ;
        break ;
      }
      else {
        BPort = &BPort[2] ;
      }
    }
    if ( BPort == NULL ) {
      *aNode = my_strdup( NULLSTRING ) ;
      *aPort = my_strdup( NULLSTRING ) ;
    }
  }
}

bool GraphBase::ComputingNode::IsLinked(const char * ToServiceParameterName, bool isInput ) {
  bool RetVal = false ;
  if ( isInput ) { // mkr : PAL8060
    // we have to know the type of the port, because of input and output ports
    // belong to one node may have the same names
    const GraphBase::InPort * thePort = GetInPort( ToServiceParameterName ) ;
    if ( thePort ) {
      RetVal = thePort->IsPortConnected() ;
    }
  }
  else {
    const GraphBase::OutPort * thePort = GetOutPort( ToServiceParameterName ) ;
    if ( thePort ) {
      RetVal = thePort->IsPortConnected() ;
    }
  }
  return RetVal ;
}

bool GraphBase::ComputingNode::HasInput(const char * ToServiceParameterName ) {
  bool RetVal = false ;
  const GraphBase::InPort * theInPort = GetInPort( ToServiceParameterName ) ;
  if ( theInPort ) {
    RetVal = theInPort->IsDataConnected() ;
  }
  else {
    const GraphBase::OutPort * theOutPort = GetOutPort( ToServiceParameterName ) ;
    if ( theOutPort ) {
      RetVal = theOutPort->IsDataConnected() ;
    }
  }
  return RetVal ;
}

GraphBase::SNode * GraphBase::ComputingNode::GetInfo() {
  cdebug_in << "GraphBase::ComputingNode::GetInfo" << endl;
  GraphBase::SNode * Info = new GraphBase::SNode ;
//  Info->theComponentName = ComponentName() ;
//  Info->theInterfaceName = InterfaceName() ;
  Info->theName = Name() ;
  Info->theKind = Kind() ;
  if ( IsDataStreamNode() ) {
    CORBA::Long Timeout ;
    SUPERV::KindOfDataStreamTrace DataStreamTrace ;
    CORBA::Double DeltaTime ;
    ((GraphBase::StreamGraph * ) this)->StreamParams( Timeout , DataStreamTrace , DeltaTime ) ;
    Info->theTimeout = Timeout ;
    Info->theDataStreamTrace = DataStreamTrace ;
    Info->theDeltaTime = DeltaTime ;
  }
  Info->theService = *GetService() ;
//  Info->theListOfParameters = *GetListOfParameters() ;
  Info->theFirstCreation = FirstCreation() ;
  Info->theLastModification = LastModification() ;
  Info->theEditorRelease = EditorRelease() ;
  Info->theAuthor = Author() ;
//  Info->theContainer = Computer() ;
  Info->theComment = Comment() ;
  Info->theCoords.theX = GraphBase::ComputingNode::XCoordinate() ;
  Info->theCoords.theY = GraphBase::ComputingNode::YCoordinate() ;
  cdebug_out << "GraphBase::ComputingNode::GetInfo" << endl;
  return Info ;
}

void GraphBase::ComputingNode::SetMacroPorts( GraphBase::Graph * aGraph ) {
  cdebug_in << "GraphBase::ComputingNode::SetMacroPorts fill ports of MacroNode" << endl;
  int i ;
  for ( i = 0 ; i < aGraph->GetNodeInPortsSize() ; i++ ) {
    const GraphBase::InPort * anInPort = aGraph->GetNodeInPort( i ) ;
    cdebug << "SetMacroPorts In" << i << " " << anInPort->PortName() << " " << anInPort->PortType()
           << " " << anInPort->Kind() << endl ;
    GraphBase::InPort * aNewInPort ;
    if ( anInPort->IsDataStream() ) {
      aNewInPort = AddInDataStreamPort( anInPort->PortName(), anInPort->PortType(),
                                        anInPort->Dependency(), anInPort->Kind() ) ;
    }
    else if ( anInPort->IsParam() || anInPort->IsInLine() ) {
      aNewInPort = AddInPort( anInPort->PortName() , anInPort->PortType() , anInPort->Kind() ) ;
    }
  }
  for ( i = 0 ; i < aGraph->GetNodeOutPortsSize() ; i++ ) {
    const GraphBase::OutPort * anOutPort = aGraph->GetNodeOutPort( i ) ;
    cdebug << "SetMacroPorts Out" << i << " " << anOutPort->PortName() << " " << anOutPort->PortType()
           << " " << anOutPort->Kind() << endl ;
    if ( anOutPort->IsDataStream() ) {
      AddOutDataStreamPort( anOutPort->PortName(), anOutPort->PortType(),
                            anOutPort->Dependency(), anOutPort->Kind() ) ;
    }
    else if ( anOutPort->IsParam() || anOutPort->IsInLine() ) {
      AddOutPort( anOutPort->PortName() , anOutPort->PortType() , anOutPort->Kind() ) ;
    }
  }
  cdebug_out << "GraphBase::ComputingNode::SetMacroPorts" << endl;
}

void GraphBase::ComputingNode::UpdateMacroPorts( GraphBase::Graph * aGraph ) {
  cdebug_in << "GraphBase::ComputingNode::UpdateMacroPorts fill ports of MacroNode from "
            << aGraph->Name() << " InPorts(" << aGraph->GetNodeInPortsSize() << ") OutPorts("
            << aGraph->GetNodeInPortsSize() << ") to MacroNode " << Name() << " InPorts(" << GetNodeInPortsSize()
            << ") OutPorts(" << GetNodeInPortsSize() << ")" << endl;
  cdebug << Name() << " : " << *GetService() << endl ;
  cdebug << aGraph->Name() << " : " << *aGraph->GetService() << endl ;
  int i ;
// Loop over InPorts of MacroNode ; if it does not exist in the MacroGraph ===> DelInPort in the MacroNode
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GraphBase::InPort * anInPort = aGraph->GetChangeInPort( GetNodeInPort( i )->PortName() ) ;
    if ( anInPort && !anInPort->IsGate() && !GetNodeInPort( i )->IsGate() ) {
      if ( strcmp( GetNodeInPort( i )->PortType() , anInPort->PortType() ) ) {
        cdebug << Name() << " " << GetNodeInPort( i )->PortName() << " " << GetNodeInPort( i )->PortType()
               << " " << GetNodeInPort( i )->Kind() << " " << GetNodeInPort( i )->Dependency() << endl ;
        cdebug << "  <--> " << aGraph->Name() << " " << anInPort->PortName() << " " << anInPort->PortType()
               << " " << anInPort->Kind() << " " << anInPort->Dependency() << endl ;
        GetChangeNodeInPort( i )->PortType( (char * ) anInPort->PortType() ) ;
      }
      if ( GetNodeInPort( i )->Kind() != anInPort->Kind() ) {
        cdebug << Name() << " " << GetNodeInPort( i )->PortName() << " " << GetNodeInPort( i )->PortType()
               << " " << GetNodeInPort( i )->Kind() << " " << GetNodeInPort( i )->Dependency() << endl ;
        cdebug << "  <--> " << aGraph->Name() << " " << anInPort->PortName() << " " << anInPort->PortType()
               << " " << anInPort->Kind() << " " << anInPort->Dependency() << endl ;
        GetChangeNodeInPort( i )->Kind( anInPort->Kind() ) ;
      }
      if ( GetNodeInPort( i )->Dependency() != anInPort->Dependency() ) {
        cdebug << Name() << " " << GetNodeInPort( i )->PortName() << " " << GetNodeInPort( i )->PortType()
               << " " << GetNodeInPort( i )->Kind() << " " << GetNodeInPort( i )->Dependency() << endl ;
        cdebug << "  <--> " << aGraph->Name() << " " << anInPort->PortName() << " " << anInPort->PortType()
               << " " << anInPort->Kind() << " " << anInPort->Dependency() << endl ;
        GetChangeNodeInPort( i )->Dependency( anInPort->Dependency() ) ;
      }
    }
    else if ( !GetNodeInPort( i )->IsGate() ) {
      cdebug << "UpdateMacroPorts DelInPort In" << i << " " << GetNodeInPort( i )->PortName() << " "
             << GetNodeInPort( i )->PortType() << " " << GetNodeInPort( i )->Kind() << endl ;
      DelInPort( GetNodeInPort( i )->PortName() ) ;
      i--; // mkr : fix for bug PAL8004
    }
  }
  int index ;
// Loop over InPorts of the MacroGraph : if it does not exist in the MacroNode ==> AddInPort in the MacroNode
  for ( index = 0 ; index < aGraph->GetNodeInPortsSize() ; index++ ) {
    GraphBase::InPort * anInPort = aGraph->GetChangeNodeInPort( index ) ;
    GraphBase::InPort * aMacroInPort = GetChangeInPort( anInPort->PortName() ) ;
    if ( !anInPort->IsGate() &&
         strcmp( GetChangeNodeInPort( index )->PortName() , anInPort->PortName() ) ) {
      cdebug << "UpdateMacroPorts Add/MoveInPort In" << index << " " << anInPort->PortName() << " "
             << anInPort->PortType() << " " << anInPort->Kind() << endl ;
      if ( aMacroInPort ) {
        MoveInPort( anInPort->PortName() , index ) ;
      }
      else {
        AddInPort( anInPort->PortName() , anInPort->PortType() , anInPort->Kind() , index ) ;
      }
    }
  }
// Loop over OutPorts of MacroNode ; if it does not exist in the MacroGraph ===> DelOutPort in the MacroNode
  for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
    GraphBase::OutPort * anOutPort = aGraph->GetChangeOutPort( GetNodeOutPort( i )->PortName() ) ;
    if ( anOutPort && !anOutPort->IsGate() && !GetNodeOutPort( i )->IsGate() ) {
      if ( strcmp( GetNodeOutPort( i )->PortType() , anOutPort->PortType() ) ) {
        cdebug << Name() << " " << GetNodeOutPort( i )->PortName() << " " << GetNodeOutPort( i )->PortType()
               << " " << GetNodeOutPort( i )->Kind() << " " << GetNodeOutPort( i )->Dependency() << endl ;
        cdebug << "  <--> " << aGraph->Name() << " " << anOutPort->PortName() << " " << anOutPort->PortType()
               << " " << anOutPort->Kind() << " " << anOutPort->Dependency() << endl ;
        GetChangeNodeOutPort( i )->PortType( (char * ) anOutPort->PortType() ) ;
      }
      if ( GetNodeOutPort( i )->Kind() != anOutPort->Kind() ) {
        cdebug << Name() << " " << GetNodeOutPort( i )->PortName() << " " << GetNodeOutPort( i )->PortType()
               << " " << GetNodeOutPort( i )->Kind() << " " << GetNodeOutPort( i )->Dependency() << endl ;
        cdebug << "  <--> " << aGraph->Name() << " " << anOutPort->PortName() << " " << anOutPort->PortType()
               << " " << anOutPort->Kind() << " " << anOutPort->Dependency() << endl ;
        GetChangeNodeOutPort( i )->Kind( anOutPort->Kind() ) ;
      }
      if ( GetNodeOutPort( i )->Dependency() != anOutPort->Dependency() ) {
        cdebug << Name() << " " << GetNodeOutPort( i )->PortName() << " " << GetNodeOutPort( i )->PortType()
               << " " << GetNodeOutPort( i )->Kind() << " " << GetNodeOutPort( i )->Dependency() << endl ;
        cdebug << "  <--> " << aGraph->Name() << " " << anOutPort->PortName() << " " << anOutPort->PortType()
               << " " << anOutPort->Kind() << " " << anOutPort->Dependency() << endl ;
        GetChangeNodeOutPort( i )->Dependency( anOutPort->Dependency() ) ;
      }
    }
    else if ( !GetNodeOutPort( i )->IsGate() ) {
      cdebug << "UpdateMacroPorts DelOutPort Out" << i << " " << GetNodeOutPort( i )->PortName() << " "
             << GetNodeOutPort( i )->PortType() << " " << GetNodeOutPort( i )->Kind() << endl ;
      DelOutPort( GetNodeOutPort( i )->PortName() ) ;
    }
  }
// Loop over OutPorts of the MacroGraph : if it does not exist in the MacroNode ==> AddOutPort in the MacroNode
  for ( index = 0 ; index < aGraph->GetNodeOutPortsSize() ; index++ ) {
    GraphBase::OutPort * anOutPort = aGraph->GetChangeNodeOutPort( index ) ;
    GraphBase::OutPort * aMacroOutPort = GetChangeOutPort( anOutPort->PortName() ) ;
    if ( !anOutPort->IsGate() && strcmp( GetNodeOutPort( index )->PortName() , anOutPort->PortName() ) ) {
      cdebug << "UpdateMacroPorts Add/MoveOutPort Out" << index << " " << anOutPort->PortName() << " "
             << anOutPort->PortType() << " " << anOutPort->Kind() << endl ;
      if ( aMacroOutPort ) {
        MoveOutPort( anOutPort->PortName() , index ) ;
      }
      else {
        AddOutPort( anOutPort->PortName() , anOutPort->PortType() , anOutPort->Kind() , index ) ;
      }
    }
  }
  bool ErrPort = false ;
  if ( aGraph->GetNodeInPortsSize() != GetNodeInPortsSize() ||
       aGraph->GetNodeOutPortsSize() != GetNodeOutPortsSize() ) {
    ErrPort = true ;
  }
  for ( i = 0 ; i < aGraph->GetNodeInPortsSize() && i < GetNodeInPortsSize() ; i++ ) {
    if ( !strcmp( aGraph->GetNodeInPort( i )->PortName() , GetNodeInPort( i )->PortName() ) ) {
      ErrPort = true ;
    }
  }
  for ( i = 0 ; i < aGraph->GetNodeOutPortsSize() && i < GetNodeOutPortsSize() ; i++ ) {
    if ( !strcmp( aGraph->GetNodeOutPort( i )->PortName() , GetNodeOutPort( i )->PortName() ) ) {
      ErrPort = true ;
    }
  }
  if ( ErrPort ) {
    for ( i = 0 ; i < aGraph->GetNodeInPortsSize() || i < GetNodeInPortsSize() ; i++ ) {
      if ( i < aGraph->GetNodeInPortsSize() && i < GetNodeInPortsSize() ) {
        cdebug << "ComputingNode::UpdateMacroPorts In" << i << " " << aGraph->GetNodeInPort( i )->PortName()
               << "  " << GetNodeInPort( i )->PortName() << endl ;
      }
      else if ( i >= GetNodeInPortsSize() ) {
        cdebug << "ComputingNode::UpdateMacroPorts In" << i << " " << aGraph->GetNodeInPort( i )->PortName()
               << endl ;
      }
      else {
        cdebug << "ComputingNode::UpdateMacroPorts In" << i << " " << GetNodeInPort( i )->PortName()
               << endl ;
      }
    }
    for ( i = 0 ; i < aGraph->GetNodeOutPortsSize() || i < GetNodeOutPortsSize() ; i++ ) {
      if ( i < aGraph->GetNodeOutPortsSize() && i < GetNodeOutPortsSize() ) {
        cdebug << "ComputingNode::UpdateMacroPorts Out" << i << " " << aGraph->GetNodeOutPort( i )->PortName()
               << " != " << GetNodeOutPort( i )->PortName() << endl ;
      }
      else if ( i >= GetNodeOutPortsSize() ) {
        cdebug << "ComputingNode::UpdateMacroPorts Out" << i << " " << aGraph->GetNodeOutPort( i )->PortName()
               << endl ;
      }
      else {
        cdebug << "ComputingNode::UpdateMacroPorts Out" << i << " " << GetNodeOutPort( i )->PortName()
               << endl ;
      }
    }
  }
  cdebug_out << "GraphBase::ComputingNode::UpdateMacroPorts"
             << aGraph->Name() << " InPorts(" << aGraph->GetNodeInPortsSize() << ") OutPorts("
             << aGraph->GetNodeOutPortsSize() << ") to MacroNode " << Name() << " InPorts("
             << GetNodeInPortsSize() << ") OutPorts(" << GetNodeOutPortsSize() << ")" << endl;
}

void GraphBase::ComputingNode::SetMacroDatas( GraphBase::Graph * aGraph ,
                                              GraphBase::Graph * aGraphOfMacroGraph ) {
  cdebug_in << "GraphBase::ComputingNode::SetMacroDatas fill inputs of MacroNode" << endl;
  int i ;
  for ( i = 0 ; i < aGraph->GetNodeInPortsSize() ; i++ ) {
    const GraphBase::InPort * anInPort = aGraph->GetNodeInPort( i ) ;
    GraphBase::OutPort * anOutPort = aGraph->GetChangeNodeInDataNodePort(i) ;
    cdebug << "                 " << Name() << " " << anOutPort->PortName() << " " << anOutPort->PortType()
           << " " << anOutPort->Kind() << " " << anOutPort->PortStatus() << endl ;
    if ( anOutPort->IsDataConnected() ) {
//JR 30.03.2005      aGraphOfMacroGraph->AddInputData( Name() , anInPort->PortName() , *(anOutPort->Value()) ) ;
      aGraphOfMacroGraph->AddInputData( Name() , anInPort->PortName() , anOutPort->Value() ) ;
      anOutPort->PortStatus( ExternConnected ) ;
    }
  }
  cdebug_out << "GraphBase::ComputingNode::SetMacroDatas" << endl;
}

void GraphBase::ComputingNode::DelInPort( const char * InputParameterName ) {
//PAL9122
//JR 07.06.2005 Debug : LinkedNodes and LinkedFromNode must be updated
//                      LinkedInPortsNumber and LinkedFromInPortsNumber must be updated
  cdebug_in << "ComputingNode::DelInPort " << Name() << "( " << InputParameterName << " )"
             << endl;
  bool RetVal = true ;
  if ( !IsEndSwitchNode() ) {
    GraphBase::InPort * anInPort = GetChangeInPort( InputParameterName ) ;
    cdebug << "ComputingNode::DelInPort InPort " << Name() << "( "
           << anInPort->PortName() << " ) " << anInPort->PortStatus() << " <-- " ;
    GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
    if ( anOutPort && !( IsEndLoopNode() && anInPort->IsLoop() ) ) {
      cdebug << anOutPort->NodeName() << "( " << anOutPort->PortName() << ") "
             << anOutPort->PortStatus() ;
    }
    cdebug << endl ;
    if ( !anInPort->IsNotConnected() ) {
      GraphBase::ComputingNode * FromNode = NULL ;
      RetVal = true ;
      if ( anOutPort->IsDataConnected() || anOutPort->IsExternConnected() ) {
        cdebug << "     Data/Extern ignored" << endl ;
      }
      else {
        FromNode = GraphOfNode()->GetChangeGraphNode( anOutPort->NodeName() ) ;
      }
      if ( FromNode ) {
        cdebug << "FromNode " << FromNode->Name() << " LinkedNodesSize "
               << FromNode->LinkedNodesSize() << " and " << Name()
               << " LinkedFromNodesSize " << LinkedFromNodesSize() << endl ;
        if ( !FromNode->IsGOTONode() &&
             !( FromNode->IsEndLoopNode() && anInPort->IsLoop() ) ) {
          if ( anInPort->IsDataStream() ) {
            RetVal = FromNode->RemoveStreamLinkedNode( this ) ;
          }
          else {
            RetVal = FromNode->RemoveLinkedNode( this ) ;
	  }
          if ( !RetVal ) {
            cdebug << "ComputingNode::DelInPort anOutPort->RemoveLinkedNode Error RetVal "
                   << RetVal << endl ;
	  }
	}
      }
    }
  }
//We have an EndSwitchNode :
  else {
    int i ;
    for ( i = LinkedFromNodesSize() - 1 ; i >= 0  ; i-- ) {
      GraphBase::ComputingNode * aLinkedFromNode = (GraphBase::ComputingNode * ) LinkedFromNodes( i ) ;
      cdebug << aLinkedFromNode->Name() << " linked to " << Name() << endl ;
      int j ;
      for ( j = aLinkedFromNode->GetNodeOutPortsSize() - 1 ; j >= 0 ; j-- ) {
        GraphBase::OutPort * anOutPort = aLinkedFromNode->GetChangeNodeOutPort( j ) ;
        cdebug << "ComputingNode::DelInPort OutPort" << j << ". Remove " << anOutPort->InPortsSize()
               << " InPortsof OutPort : " << anOutPort->NodeName() << "( "
               << anOutPort->PortName() << ") "
               << anOutPort->PortStatus() << " :" << endl ;
        int k ;
//Process concerned OutPorts of LinkedFromNodes of that EndSwitchNode
        for ( k = anOutPort->InPortsSize() - 1 ; k >= 0  ; k-- ) {
          GraphBase::InPort * anInPort = anOutPort->ChangeInPorts( k ) ;
          if ( !strcmp( anInPort->NodeName() , Name() ) &&
               !strcmp( anInPort->PortName() , InputParameterName ) ) {
// InPort of the EndSwitchNode
            cdebug << "       to InPort" << j << ". " << anInPort->NodeName() << "( "
                   << anInPort->PortName() << ") "
                   << anInPort->PortStatus() << endl ;
            anInPort->RemoveOutPort() ;
            if ( anOutPort->IsDataStream() ) {
              aLinkedFromNode->RemoveStreamLinkedNode( this ) ;
	    }
            else {
// false ==> No error for aNode->LinkedFromNode()
              RetVal = aLinkedFromNode->RemoveLinkedNode( this , false ) ;
              if ( !RetVal ) {
                cdebug << "ComputingNode::DelInPort aLinkedFromNode->RemoveLinkedNode Error RetVal "
                       << RetVal << endl ;
                break ;
	      }
	    }
// Remove the InPort of the EndSwitchNode from that OutPort
            RetVal = anOutPort->RemoveInPort( anInPort ) ;
            if ( !RetVal ) {
              cdebug << "ComputingNode::DelInPort anOutPort->RemoveInPort( anInPort ) Error RetVal "
                     << RetVal << endl ;
              break ;
	    }
	  }
	}
      }
    }
  }

  GraphBase::PortsOfNode::DelInPort( InputParameterName ) ;
  cdebug_out << "ComputingNode::DelInPort " << Name() << "( " << InputParameterName << " )"
             << endl;
}
void GraphBase::ComputingNode::DelOutPort( const char * OutputParameterName ) {
//PAL9122
//JR 07.06.2005 Debug : LinkedNodes and LinkedFromNode must be updated
//                      LinkedInPortsNumber and LinkedFromInPortsNumber must be updated
  cdebug_in << "ComputingNode::DelOutPort " << Name() << "( " << OutputParameterName << " )"
            << endl;
  bool RetVal = true ;
  GraphBase::OutPort * anOutPort = GetChangeOutPort( OutputParameterName ) ;
  cdebug << "ComputingNode::DelOutPort OutPort " << " InPortof OutPort : "
         << anOutPort->NodeName() << "( " << anOutPort->PortName() << ") "
         << anOutPort->PortStatus() << " :" << endl ;
  int j ;
  for ( j = anOutPort->InPortsSize() - 1 ; j >= 0  ; j-- ) {
    GraphBase::InPort * anInPort = anOutPort->ChangeInPorts( j ) ;
    GraphBase::ComputingNode * ToNode = NULL ;
    cdebug << "       to InPort" << j << ". " << anInPort->NodeName() << "( "
           << anInPort->PortName() << ") " << anInPort->PortStatus() ;
    if ( anInPort->IsExternConnected() ) {
      cdebug << " ExternConnected ignored" << endl ;
    }
    else {
      cdebug << endl ;
      ToNode = GraphOfNode()->GetChangeGraphNode( anInPort->NodeName() ) ;
    }
    if ( ToNode ) {
      RetVal = anInPort->RemoveOutPort() ;
// JR 04.02.2005 : Bug if it is a link to an EndSwitchNode. The InPort may be multiple linked !!!
// PAL7990
      if ( !RetVal ) {
        cdebug << "ComputingNode::DelOutPort anOutPort->RemoveOutPort Error RetVal " << RetVal
               << endl ;
        break ;
      }
      if ( ( IsGOTONode() && ToNode->IsOneOfInLineNodes() ) ||
           ( IsEndLoopNode() && ToNode->IsLoopNode( ) ) ) {
      }
      else if ( anOutPort->IsDataStream() ) {
        RetVal = RemoveStreamLinkedNode( ToNode ) ;
      }
      else {
        RetVal = RemoveLinkedNode( ToNode ) ;
      }
      if ( !RetVal ) {
        cdebug << "ComputingNode::DelOutPort anOutPort->RemoveLinkedNode Error RetVal " << RetVal
               << endl ;
        break ;
      }
      if ( ToNode->IsEndSwitchNode() ) {
        int i ;
        int done = false ;
        for ( i = 0 ; i < ToNode->LinkedFromNodesSize() ; i++ ) {
          GraphBase::StreamNode * fromNode = ToNode->LinkedFromNodes( i ) ;
// Not the node that we are deleting ... :
          if ( strcmp( fromNode->Name() , Name() ) ) {
            int j ;
            for ( j = 0 ; j < fromNode->GetNodeOutPortsSize() ; j++ ) {
              GraphBase::OutPort * fromOutPort = fromNode->GetChangeNodeOutPort( j ) ;
              int k ;
              for ( k = 0 ; k < fromOutPort->InPortsSize() ; k++ ) {
                if ( strcmp( ToNode->Name() , fromOutPort->InPorts( k )->NodeName() ) == 0 ) {
                  if ( strcmp( anInPort->PortName() , fromOutPort->InPorts( k )->PortName() ) == 0 ) {
// Restore an OutPort in the InPort
                    anInPort->ChangeOutPort( fromOutPort ) ;
                    cdebug << "ComputingNode::DelOutPort reestablish " << fromOutPort->NodeName() << "( "
                           << fromOutPort->PortName() << " ) in the InPort of EndSwitch : "
                           << ToNode->Name() << "( " << anInPort->PortName() << " )"
                           << anInPort->Kind() << " " << anInPort->PortStatus()  << endl;
                    done = true ;
                    break ;
		  }
	        }
	      }
              if ( done ) {
                break ;
	      }
	    }
            if ( done ) {
              break ;
	    }
	  }
	}
      }
    }
  }
  if ( !RetVal ) {
    cdebug << "Error RetVal " << RetVal << endl ;
  }

  GraphBase::PortsOfNode::DelOutPort( OutputParameterName ) ;
  cdebug_out << "ComputingNode::DelOutPort " << Name() << "( " << OutputParameterName << " )"
             << endl;
}

GraphBase::InPort * GraphBase::ComputingNode::AddInPort( const char * InputParameterName ,
                                                         const char * InputParameterType ,
                                                         const SUPERV::KindOfPort aKindOfPort ,
                                                         int index ) {
  cdebug << "AddInPort " << Name() << " ConnectedInPortsNumber " << ConnectedInPortsNumber() << endl ;
// JR 12.01.2005 : InitLoop and DoLoop are reserved parameter names in LoopNodes :
  if ( IsLoopNode() && ( strcmp( InputParameterName , "InitLoop" ) == 0 ||
                         strcmp( InputParameterName , "DoLoop" ) == 0 ) ) {
    return NULL ;
  }
  return GraphBase::PortsOfNode::AddInPort( _ORB , NamePtr() ,
                                            Kind() ,
                                            InputParameterName ,
                                            InputParameterType ,
                                            aKindOfPort ,
                                            index ,
                                            _Graph_prof_debug , _Graph_fdebug ) ;
}
GraphBase::OutPort * GraphBase::ComputingNode::AddOutPort( const char * OutputParameterName ,
                                                           const char * OutputParameterType ,
                                                           const SUPERV::KindOfPort aKindOfPort ,
                                                           int index ) {
  cdebug << "AddOutPort " << Name() << " ConnectedInPortsNumber " << ConnectedInPortsNumber() << endl ;
  return GraphBase::PortsOfNode::AddOutPort( _ORB , NamePtr() ,
                                             Kind() ,
                                             OutputParameterName ,
                                             OutputParameterType ,
                                             aKindOfPort ,
                                             index ,
                                             _Graph_prof_debug , _Graph_fdebug ) ;
}

void GraphBase::ComputingNode::DelInDataStreamPort( const char * InputParameterName ) {
  GraphBase::PortsOfNode::DelInPort( InputParameterName ) ;
}
void GraphBase::ComputingNode::DelOutDataStreamPort( const char * OutputParameterName ) {
  GraphBase::PortsOfNode::DelOutPort( OutputParameterName ) ;
}

GraphBase::InDataStreamPort * GraphBase::ComputingNode::AddInDataStreamPort( const char * InputParameterName,
                                                                             const char * InputParameterType,
                                                                             const SALOME_ModuleCatalog::DataStreamDependency aDependency ,
                                                                             const SUPERV::KindOfPort aKindOfPort ,
                                                                             int index ) {
//  IncrDataStreamInPorts() ;
  GraphBase::InDataStreamPort * aDataStreamPort ;
  aDataStreamPort = (GraphBase::InDataStreamPort * ) GraphBase::PortsOfNode::AddInPort( _ORB , NamePtr() ,
                                                                                        Kind() ,
                                                                                        InputParameterName ,
                                                                                        InputParameterType ,
                                                                                        aKindOfPort ,
                                                                                        index ,
                                                                                        _Graph_prof_debug ,
                                                                                        _Graph_fdebug ) ;
  aDataStreamPort->Dependency( aDependency ) ;
  if ( aDependency == SALOME_ModuleCatalog::DATASTREAM_TEMPORAL ) {
    aDataStreamPort->SetParams( SUPERV::TI , SUPERV::L1 , SUPERV::EXTRANULL ) ;
  }
  return aDataStreamPort ;
}
GraphBase::OutDataStreamPort * GraphBase::ComputingNode::AddOutDataStreamPort( const char * OutputParameterName,
                                                                               const char * OutputParameterType,
                                                                               const SALOME_ModuleCatalog::DataStreamDependency aDependency ,
                                                                               const SUPERV::KindOfPort aKindOfPort ,
                                                                               int index ) {
//  IncrDataStreamOutPorts() ;
  GraphBase::OutDataStreamPort * aDataStreamPort ;
  aDataStreamPort = (GraphBase::OutDataStreamPort * ) GraphBase::PortsOfNode::AddOutPort( _ORB , NamePtr(),
                                                                                          Kind(),
                                                                                          OutputParameterName,
                                                                                          OutputParameterType,
                                                                                          aKindOfPort ,
                                                                                          index ,
                                                                                          _Graph_prof_debug ,
                                                                                          _Graph_fdebug ) ;
  aDataStreamPort->Dependency( aDependency ) ;
  return aDataStreamPort ;
}


bool GraphBase::ComputingNode::CheckLoop( GraphBase::LoopNode * aLoopNode ,
                                          GraphBase::EndOfLoopNode * anEndLoopNode ,
                                          string & anErrorMessage ) const {
  cdebug_in << Name() << "->ComputingNode::CheckLoop( " << aLoopNode->Name() << " , "
            << anEndLoopNode->Name() << ") LinkedNodesSize "
            << LinkedNodesSize() << endl;

  int i ;
  if ( LinkedNodesSize() == 0 && !IsDataFlowNode() && !IsDataStreamNode() ) {
    anErrorMessage = anErrorMessage + string( "The node " ) + string( Name() ) +
                     string( " has no linked Nodes. Loop not valid\n" ) ;
    cdebug_out << Name() << "->ComputingNode::CheckLoop( " << aLoopNode->Name()
               << " , " << anEndLoopNode->Name() << ") LinkedNodesSize "
               << LinkedNodesSize() << " ERROR false" << endl;
    return false ;
  }
// We check that all nodes linked to that node go to the end of LoopNode :
  for ( i = 0 ; i < LinkedNodesSize() ; i++ ) {
    GraphBase::ComputingNode * aNode = (GraphBase::ComputingNode * ) LinkedNodes( i ) ;
    cdebug << i << ". " << Name() << " Linked to " << aNode->Name() << endl ;
    if ( !aNode->IsEndLoopNode() || aNode != anEndLoopNode ) {
      if ( !aNode->CheckLoop( aLoopNode , anEndLoopNode , anErrorMessage) ) {
        cdebug_out << Name() << "->ComputingNode::CheckLoop( " << aLoopNode->Name()
                   << " , " << anEndLoopNode->Name() << ") LinkedNodesSize "
                   << LinkedNodesSize() << " ERROR false" << endl;
        return false ;
      }
    }
  }
  cdebug_out << Name() << "->ComputingNode::CheckLoop( " << aLoopNode->Name() << " , "
             << anEndLoopNode->Name() << ") LinkedNodesSize "
             << LinkedNodesSize() << " true" << endl;
  return true ;
}

bool GraphBase::ComputingNode::CheckEndLoop(GraphBase::LoopNode * aLoopNode ,
                                            GraphBase::EndOfLoopNode * anEndLoopNode ,
                                            string & anErrorMessage ) const {
  cdebug_in << Name() << "->ComputingNode::CheckEndLoop( " << aLoopNode->Name() << " , "
            << anEndLoopNode->Name() << ") LinkedFromNodesSize "
            << LinkedFromNodesSize() << endl;

  int i ;
  if ( LinkedFromNodesSize() == 0 ) {
    anErrorMessage = anErrorMessage + string( "The node " ) + string( Name() ) +
                     string( " has no reversed linked Nodes. Loop not valid\n" ) ;
    cdebug_out << Name() << "->ComputingNode::CheckEndLoop( " << aLoopNode->Name()
               << " , " << anEndLoopNode->Name() << ") LinkedFromNodesSize "
               << LinkedFromNodesSize() << " ERROR false" << endl;
    return false ;
  }
// We have to check that all nodes linked from that node go to the beginning of LoopNode :
  for ( i = 0 ; i < LinkedFromNodesSize() ; i++ ) {
    GraphBase::ComputingNode * aNode = (GraphBase::ComputingNode * ) LinkedFromNodes( i ) ;
    cdebug << i << ". " << Name() << " Linked from " << aNode->Name() << endl ;
    if ( !aNode->IsLoopNode() || aNode != aLoopNode ) {
      if ( !aNode->CheckEndLoop( aLoopNode , anEndLoopNode , anErrorMessage ) ) {
        cdebug_out << Name() << "->ComputingNode::CheckEndLoop( " << aLoopNode->Name()
                   << " , " << anEndLoopNode->Name() << ") LinkedFromNodesSize "
                   << LinkedFromNodesSize() << " ERROR false" << endl;
        return false ;
      }
    }
  }
  cdebug_out << Name() << "->ComputingNode::CheckEndLoop( " << aLoopNode->Name() << " , "
             << anEndLoopNode->Name() << ") LinkedFromNodesSize "
             << LinkedFromNodesSize() << " true" << endl;
  return true ;
}

bool GraphBase::ComputingNode::InitBranchOfSwitchDone( bool AllInit ,
                                                       GraphBase::EndOfSwitchNode * anEndSwitchNode ,
                                                       string & anErrorMessage ) {
  bool RetVal = true ;
  bool sts = BranchOfSwitchDone( false ) ;
  if ( !sts && !AllInit && anEndSwitchNode != this ) {
// Common Node in branchs :
    anErrorMessage = anErrorMessage + string( "Common Node between some SwitchBranches : " ) +
                     string( Name() ) + string("\n" ) ;
    cdebug << Name() << "->ComputingNode::InitBranchOfSwitchDone ERROR false"
           << endl ;
    RetVal = false ;
  }
  int i ;
  if ( !IsGOTONode() && anEndSwitchNode != this ) {
    for ( i = 0 ; i < LinkedNodesSize() ; i++ ) {
      GraphBase::ComputingNode * aNode ;
      aNode = (GraphBase::ComputingNode * ) LinkedNodes( i ) ;
      if ( !aNode->InitBranchOfSwitchDone( AllInit , anEndSwitchNode , anErrorMessage ) ) {
        RetVal = false ;
      }
    }
  }
  return RetVal ;
}

bool GraphBase::ComputingNode::CheckSwitch( GraphBase::EndOfSwitchNode * anEndSwitchNode ,
                                            string & anErrorMessage ) {
  bool RetVal = true ;
  cdebug_in << Name() << "->ComputingNode::CheckSwitch( "
            << anEndSwitchNode->Name() << " ) " << Kind() << endl;
  if ( anEndSwitchNode == this ) {
  }
// if it is a SwitchNode, continue the check at the corresponding EndSwitchNode
  else if ( IsSwitchNode() ) {
    GraphBase::EndOfSwitchNode * anOtherEndSwitchNode ;
    anOtherEndSwitchNode = (GraphBase::EndOfSwitchNode * ) ((GraphBase::SwitchNode * ) this)->CoupledNode() ;
    cdebug << Name() << "->ComputingNode::CheckSwitch will Check "
           << anOtherEndSwitchNode->Name() << endl ;
    if ( !anOtherEndSwitchNode->CheckSwitch( anEndSwitchNode , anErrorMessage ) ) {
      cdebug_out << Name() << "->ComputingNode::CheckSwitch "
                 << anOtherEndSwitchNode->Name() << " ERROR false" << endl;
      RetVal = false ;
    }
  }
  else {
    int i ;
    for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
      GraphBase::OutPort * anOutPort = GetChangeNodeOutPort( i ) ;
      if ( !anOutPort->IsDataStream() ) {
        int j ;
        for ( j = 0 ; j < anOutPort->InPortsSize() ; j++ ) {
          GraphBase::InPort * anInPort = anOutPort->ChangeInPorts( j ) ;
          if ( !anInPort->IsDataStream() ) {
            GraphBase::ComputingNode * aNode ;
            aNode = GraphOfNode()->GetChangeGraphNode( anInPort->NodeName() ) ;
            GraphBase::LoopNode * aLoopNode = NULL ;
            GraphBase::EndOfLoopNode * aEndLoopNode = NULL ;
            if ( IsLoopNode() ) {
              aEndLoopNode = (GraphBase::EndOfLoopNode * ) ((GraphBase::LoopNode * ) this)->CoupledNode() ;
              cdebug << Name() << "->ComputingNode::CheckSwitch LoopNode " << Name() << " coupled to "
                     << aEndLoopNode << " " << aEndLoopNode->Name() << " aNode " << aNode << endl ;
            }
            else if ( IsEndLoopNode() ) {
              aLoopNode = (GraphBase::LoopNode * ) ((GraphBase::EndOfLoopNode * ) this)->CoupledNode() ;
              cdebug << Name() << "->ComputingNode::CheckSwitch EndLoopNode " << Name()
                     << " coupled to "
                     << aLoopNode << " " << aLoopNode->Name() << " aNode " << aNode << endl ;
            }
            if ( aNode == NULL ) {
              cdebug << Name() << "->ComputingNode::CheckSwitch ignore "
                     << anInPort->NodeName() << "( " << anInPort->PortName() << " )" << endl ;
            }
            else if ( aNode == anEndSwitchNode ) {
              if ( !((GraphBase::EndOfSwitchNode * ) aNode)->DecrEndSwitchInPortLinked( anInPort ,
                                                                                        anErrorMessage ) ) {
                RetVal = false ;
	      }
            }
            else if ( aNode->BranchOfSwitchDone() ) {
              cdebug << Name() << "->ComputingNode::CheckSwitch will NOT Check "
                     << anInPort->NodeName() << "( " << anInPort->PortName() << " ) : already checked"
                     << endl ;
            }
            else if ( IsLoopNode() && aNode == aEndLoopNode ) {
              cdebug << Name() << "->ComputingNode::CheckSwitch will NOT Check "
                     << anInPort->NodeName() << "( " << anInPort->PortName() << " )" << endl ;
            }
            else if ( IsEndLoopNode() && aNode == aLoopNode ) {
              cdebug << Name() << "->ComputingNode::CheckSwitch will NOT Check "
                     << anInPort->NodeName() << "( " << anInPort->PortName() << " )" << endl ;
            }
            else if ( aNode->IsGOTONode() ) {
              cdebug << Name() << "->ComputingNode::CheckSwitch will NOT Check "
                     << anInPort->NodeName() << "( " << anInPort->PortName() << " )" << endl ;
              anEndSwitchNode->SetSwitchWithGOTO() ;
            }
            else {
              cdebug << Name() << "->ComputingNode::CheckSwitch will Check "
                     << anInPort->NodeName() << "( " << anInPort->PortName() << " )" << endl ;
              if ( !aNode->CheckSwitch( anEndSwitchNode , anErrorMessage ) ) {
                cdebug_out << Name() << "->ComputingNode::CheckSwitch "
                         << anEndSwitchNode->Name() << " ERROR false" << endl;
                RetVal =  false ;
	      }
//            aNode->BranchOfSwitchDone( true ) ;
            }
          }
        }
      }
    }
  }
  if ( RetVal ) {
    BranchOfSwitchDone( true ) ;
  }
  cdebug_out << Name() << "->ComputingNode::CheckSwitch "
             << anEndSwitchNode->Name() << " RetVal " << RetVal << endl;
  return RetVal ;
}

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

const long GraphBase::ComputingNode::CpuUsed() {
  struct rusage usage ;
  if ( getrusage( RUSAGE_SELF , &usage ) == -1 ) {
    perror("GraphBase::CpuUsed") ;
    return 0 ;
  }
//  return usage.ru_utime.__time_t tv_sec ;
  cdebug << "CpuUsed " << usage.ru_utime.tv_sec << " " << usage.ru_utime.tv_usec << " "
         << usage.ru_stime.tv_sec << " " << usage.ru_stime.tv_usec << endl ;
  return usage.ru_utime.tv_sec ;
}

#if 0
const GraphBase::ListOfParameters * GraphBase::ComputingNode::GetListOfParameters() const {
  GraphBase::ListOfParameters * aListOfParameters = new GraphBase::ListOfParameters ;
  if ( IsInLineNode() || IsGOTONode() ) {
    unsigned int i;
    for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
      const InPort * anInPort = GetNodeInPort( i ) ;
      if ( anInPort->IsBus() ) {
        int size = aListOfParameters->size() ;
        aListOfParameters->resize( size + 1 ) ;
        (*aListOfParameters)[size].theInParameter.Parametername = anInPort->PortName() ;
        (*aListOfParameters)[size].theInParameter.Parametertype = anInPort->PortType() ;
        const OutPort * anOutPort = GetNodeOutPort( anInPort->PortIndex() ) ;
        (*aListOfParameters)[size].theOutParameter.Parametername = anOutPort->PortName() ;
        (*aListOfParameters)[size].theOutParameter.Parametertype = anOutPort->PortType() ;
      }
    }
  }
  return aListOfParameters ;
}
#endif

bool GraphBase::ComputingNode::SaveXML( QDomDocument & Graph , QDomElement & info ,
                                        const char * ComponentName ,
                                        const char * InterfaceName ,
                                        const char * Computer ,
                                        const char * CoupledNode ,
                                        const ListOfFuncName FuncNames ,
                                        const ListOfPythonFunctions PythonFunctions ,
                                        int XCoordinate , int YCoordinate ) const {
  cdebug_in << "SaveXML Node " << Name() << endl ;
  QDomElement node = Graph.createElement( "node" ) ;
  info.appendChild( node ) ;
  QDomElement componentname = Graph.createElement( "component-name" ) ;
  QDomText aField ;
  if ( strlen( ComponentName ) ) {
//    f << Tabs << "<component-name>" << ComponentName << "</component-name>"
//      << endl ;
//    componentname.setNodeValue( ComponentName ) ;
    aField = Graph.createTextNode( ComponentName ) ;
  }
  else {
//    f << Tabs << "<component-name>?</component-name>" << endl ;
//    componentname.setNodeValue( "?" ) ;
    aField = Graph.createTextNode( "?" ) ;
  }
  node.appendChild( componentname ) ;
  componentname.appendChild( aField ) ;

  QDomElement interfacename = Graph.createElement("interface-name") ;
  if ( strlen( InterfaceName ) ) {
//    f << Tabs << "<interface-name>" << InterfaceName << "</interface-name>"
//      << endl ;
//    interfacename.setAttribute("name" , InterfaceName ) ;
    aField = Graph.createTextNode( InterfaceName ) ;
  }
  else {
//    f << Tabs << "<interface-name>?</interface-name>" << endl ;
//    interfacename.setAttribute("name" , "?" ) ;
    aField = Graph.createTextNode( "?" ) ;
  }
  node.appendChild(interfacename) ;
  interfacename.appendChild( aField ) ;

//  f << Tabs << "<node-name>" << Name() << "</node-name>" << endl ;
  QDomElement nodename = Graph.createElement("node-name") ;
  aField = Graph.createTextNode( Name() ) ;
  node.appendChild( nodename ) ;
  nodename.appendChild( aField ) ;

//  f << Tabs << "<kind>" << (int ) Kind() << "</kind>" << endl ;
  QDomElement kind = Graph.createElement( "kind" ) ;
  QString aKind ;

  // san - Presumably, data stream graphs should be always saved with kind = DataStreamGraph
// JR : the bug was at line 566 : && HasDataStream() != 0 was missing
  if ( IsDataFlowNode() || ( IsDataStreamNode() && HasDataStream() == 0 ) ) {
    aKind = aKind.setNum( SUPERV::DataFlowGraph ) ;
  }
  else {
    aKind = aKind.setNum( Kind() ) ;
  }
  aField = Graph.createTextNode( aKind ) ;
  node.appendChild( kind ) ;
  kind.appendChild( aField ) ;

  if ( IsDataStreamNode() && HasDataStream() != 0 ) {
    CORBA::Long Timeout ;
    SUPERV::KindOfDataStreamTrace DataStreamTrace ;
    CORBA::Double DeltaTime ;
    ((GraphBase::StreamGraph * ) this)->StreamParams( Timeout , DataStreamTrace , DeltaTime ) ;

    QDomElement timeout = Graph.createElement("streamgraph-timeout") ;
    QString aTimeout ;
    aTimeout = aTimeout.setNum( Timeout ) ;
    aField = Graph.createTextNode( aTimeout ) ;
    node.appendChild( timeout ) ;
    timeout.appendChild( aField ) ;

    QDomElement datastreamtrace = Graph.createElement("streamgraph-datastreamtrace") ;
    QString aDataStreamTrace ;
    aDataStreamTrace = aDataStreamTrace.setNum( DataStreamTrace ) ;
    aField = Graph.createTextNode( aDataStreamTrace ) ;
    node.appendChild( datastreamtrace ) ;
    datastreamtrace.appendChild( aField ) ;

    QDomElement deltatime = Graph.createElement("streamgraph-deltatime") ;
    QString aDeltaTime ;
    aDeltaTime = aDeltaTime.setNum( DeltaTime ) ;
    aField = Graph.createTextNode( aDeltaTime ) ;
    node.appendChild( deltatime ) ;
    deltatime.appendChild( aField ) ;
  }

  QDomElement couplednode = Graph.createElement("coupled-node") ;
  if ( IsGOTONode() || IsLoopNode() || IsEndLoopNode() ||
       IsSwitchNode() || IsEndSwitchNode() || IsMacroNode() ) {
//    f << Tabs << "<coupled-node>" << CoupledNode << "</coupled-node>"
//      << endl ;
    aField = Graph.createTextNode( CoupledNode ) ;
  }
  else {
//    f << Tabs << "<coupled-node>?</coupled-node>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  node.appendChild(couplednode) ;
  couplednode.appendChild( aField ) ;

//  f << Tabs << "<service>" << endl ;
  QDomElement service = Graph.createElement("service") ;
  node.appendChild(service) ;
  QDomElement servicename = Graph.createElement("service-name") ;
  if ( strlen( ServiceName() ) ) {
//    f << Tabs << "	<service-name>" << ServiceName() << "</service-name>" << endl ;
    aField = Graph.createTextNode( ServiceName() ) ;
  }
  else {
//    f << Tabs << "	<service-name>?</service-name>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  service.appendChild(servicename) ;
  servicename.appendChild( aField ) ;

  cdebug << "SaveXML " << Name() << " In(" << ServiceInParameter().length()
         << ") Out(" << ServiceOutParameter().length() << ")" << endl ;
  QDomElement inParameterlist = Graph.createElement("inParameter-list") ;
  service.appendChild(inParameterlist) ;
  unsigned int i;
  GraphBase::ComputingNode * aNode = (GraphBase::ComputingNode * ) this ;
  for ( i = 0 ; i < ServiceInParameter().length() ; i++ ) {
    const GraphBase::InPort * anInPort ;
    anInPort = aNode->GetInPort( ServiceInParameter()[i].Parametername ) ;
    if ( !anInPort->IsDataStream() ) {
      cdebug << "SaveXML " << i << ". " << ServiceInParameter()[i].Parametername
             << " InParameterPort " << anInPort->Kind() << endl ;
      QDomElement inParameter = Graph.createElement("inParameter") ;
      inParameterlist.appendChild(inParameter) ;
      QDomElement inParametertype = Graph.createElement("inParameter-type") ;
      if ( strlen( ServiceInParameter()[i].Parametertype ) ) {
        aField = Graph.createTextNode( strdup( ServiceInParameter()[i].Parametertype ) ) ;
      }
      else {
        aField = Graph.createTextNode( "?" ) ;
      }
      inParameter.appendChild(inParametertype) ;
      inParametertype.appendChild( aField ) ;
      QDomElement inParametername = Graph.createElement("inParameter-name") ;
      if ( strlen( ServiceInParameter()[i].Parametername ) ) {
        aField = Graph.createTextNode( strdup(ServiceInParameter()[i].Parametername) ) ;
      }
      else {
        aField = Graph.createTextNode( "?" ) ;
      }
      inParameter.appendChild(inParametername) ;
      inParametername.appendChild( aField ) ;
    }
  }
  QDomElement outParameterlist = Graph.createElement("outParameter-list") ;
  service.appendChild(outParameterlist) ;
  for ( i = 0 ; i < ServiceOutParameter().length() ; i++ ) {
    const GraphBase::OutPort * anOutPort ;
    anOutPort = aNode->GetOutPort( ServiceOutParameter()[i].Parametername ) ;
    if ( !anOutPort->IsDataStream() ) {
      cdebug << "SaveXML " << i << ". " << ServiceOutParameter()[i].Parametername
             << " OutParameterPort " << anOutPort->Kind() << endl ;
      QDomElement outParameter = Graph.createElement("outParameter") ;
      outParameterlist.appendChild(outParameter) ;
      QDomElement outParametertype = Graph.createElement("outParameter-type") ;
      if ( strlen( ServiceOutParameter()[i].Parametertype ) ) {
        aField = Graph.createTextNode( strdup(ServiceOutParameter()[i].Parametertype) ) ;
      }
      else {
        aField = Graph.createTextNode( "?" ) ;
      }
      outParameter.appendChild(outParametertype) ;
      outParametertype.appendChild( aField ) ;
      QDomElement outParametername = Graph.createElement("outParameter-name") ;
      if ( strlen( ServiceOutParameter()[i].Parametername ) ) {
        aField = Graph.createTextNode( strdup(ServiceOutParameter()[i].Parametername) ) ;
      }
      else {
        aField = Graph.createTextNode( "?" ) ;
      }
      outParameter.appendChild(outParametername) ;
      outParametername.appendChild( aField ) ;
    }
  }

  QDomElement DataStreamlist = Graph.createElement("DataStream-list") ;
  node.appendChild( DataStreamlist ) ;
  for ( i = 0 ; i < (unsigned int ) GetNodeInPortsSize() ; i++ ) {
    const GraphBase::InPort * anInPort ;
    anInPort = aNode->GetNodeInPort( i ) ;
    if ( anInPort->IsDataStream() ) {
      cdebug << "SaveXML " << i << " " << Name() << " " << anInPort->PortName() << " " << anInPort->PortType()
             << " InDataStreamPort " << anInPort->Kind() << endl ;
      QDomElement inParameter = Graph.createElement("inParameter") ;
      DataStreamlist.appendChild(inParameter) ;
      QDomElement inParametertype = Graph.createElement("inParameter-type") ;
      QString aType ;
      aType = aType.setNum( StringToDataStreamType(anInPort->PortType()) ) ;
      cdebug << "SaveXML " << anInPort->PortType() << " --> " << anInPort->PortType()
             << " " << aType << endl ;
      aField = Graph.createTextNode( aType ) ;
      inParameter.appendChild(inParametertype) ;
      inParametertype.appendChild( aField ) ;
      QDomElement inParametername = Graph.createElement("inParameter-name") ;
      if ( strlen( anInPort->PortName() ) ) {
        aField = Graph.createTextNode( strdup(anInPort->PortName()) ) ;
      }
      else {
        aField = Graph.createTextNode( "?" ) ;
      }
      inParameter.appendChild(inParametername) ;
      inParametername.appendChild( aField ) ;
      cdebug << "SaveXML " << anInPort->PortName() << endl ;
      QDomElement inParameterdependency = Graph.createElement("inParameter-dependency") ;
      QString aDependency ;
      aDependency = aDependency.setNum( anInPort->Dependency() ) ;
      aField = Graph.createTextNode( aDependency ) ;
      inParameter.appendChild(inParameterdependency) ;
      inParameterdependency.appendChild( aField ) ;
      cdebug << "SaveXML Dependency " << anInPort->Dependency() << endl ;
      SUPERV::KindOfSchema        aKindOfSchema ;
      SUPERV::KindOfInterpolation aKindOfInterpolation ;
      SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
      ((GraphBase::InDataStreamPort * ) anInPort)->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
      QDomElement inParameterKindOfSchema = Graph.createElement("inParameter-schema") ;
      QString aSchema ;
      aSchema = aSchema.setNum( aKindOfSchema ) ;
      aField = Graph.createTextNode( aSchema ) ;
      inParameter.appendChild(inParameterKindOfSchema) ;
      inParameterKindOfSchema.appendChild( aField ) ;
      cdebug << "SaveXML aKindOfSchema " << aKindOfSchema << endl ;
      QDomElement inParameterKindOfInterpolation = Graph.createElement("inParameter-interpolation") ;
      QString anInterpolation ;
      anInterpolation = anInterpolation.setNum( aKindOfInterpolation ) ;
      aField = Graph.createTextNode( anInterpolation ) ;
      inParameter.appendChild(inParameterKindOfInterpolation) ;
      inParameterKindOfInterpolation.appendChild( aField ) ;
      cdebug << "SaveXML aKindOfInterpolation " << aKindOfInterpolation << endl ;
      QDomElement inParameterKindOfExtrapolation = Graph.createElement("inParameter-extrapolation") ;
      QString anExtrapolation ;
      anExtrapolation = anExtrapolation.setNum( aKindOfExtrapolation ) ;
      aField = Graph.createTextNode( anExtrapolation ) ;
      inParameter.appendChild(inParameterKindOfExtrapolation) ;
      inParameterKindOfExtrapolation.appendChild( aField ) ;
      cdebug << "SaveXML aKindOfExtrapolation " << aKindOfExtrapolation << endl ;
    }
  }
  for ( i = 0 ; i < (unsigned int ) GetNodeOutPortsSize() ; i++ ) {
    const GraphBase::OutPort * anOutPort ;
    anOutPort = aNode->GetNodeOutPort( i ) ;
    if ( anOutPort->IsDataStream() ) {
      cdebug << "SaveXML " << i << " " << Name() << " " << anOutPort->PortName() << " " << anOutPort->PortType()
             << " OutDataStreamPort " << anOutPort->Kind() << endl ;
      QDomElement outParameter = Graph.createElement("outParameter") ;
      DataStreamlist.appendChild(outParameter) ;
      QDomElement outParametertype = Graph.createElement("outParameter-type") ;
      QString aType ;
      aType = aType.setNum( StringToDataStreamType(anOutPort->PortType()) ) ;
      cdebug << "SaveXML " << anOutPort->PortType() << " --> " << anOutPort->PortType()
             << " " << aType << endl ;
      aField = Graph.createTextNode( aType ) ;
      outParameter.appendChild(outParametertype) ;
      outParametertype.appendChild( aField ) ;
      QDomElement outParametername = Graph.createElement("outParameter-name") ;
      if ( strlen( anOutPort->PortName() ) ) {
        aField = Graph.createTextNode( strdup(anOutPort->PortName() ) ) ;
      }
      else {
        aField = Graph.createTextNode( "?" ) ;
      }
      outParameter.appendChild(outParametername) ;
      outParametername.appendChild( aField ) ;
      cdebug << "SaveXML " << anOutPort->PortName() << endl ;
      QDomElement outParameterdependency = Graph.createElement("outParameter-dependency") ;
      QString aDependency ;
      aDependency = aDependency.setNum( anOutPort->Dependency() )  ;
      aField = Graph.createTextNode( aDependency ) ;
      outParameter.appendChild(outParameterdependency) ;
      outParameterdependency.appendChild( aField ) ;
      cdebug << "SaveXML Dependency " << anOutPort->Dependency() << endl ;
      long aNumberOfValues ;
      aNumberOfValues = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
      QDomElement outParameterNumberOfValues = Graph.createElement("outParameter-values") ;
      QString aValues ;
      aValues = aValues.setNum( aNumberOfValues ) ;
      aField = Graph.createTextNode( aValues ) ;
      outParameter.appendChild(outParameterNumberOfValues) ;
      outParameterNumberOfValues.appendChild( aField ) ;
      cdebug << "SaveXML NumberOfValues " << ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() << endl ;
    }
  }
//  f << Tabs << "</Parameter-list>" << endl ;

//  f << Tabs << "<PyFunction-list>" << endl ;
  QDomElement PyFunctionlist = Graph.createElement("PyFunction-list") ;
  node.appendChild( PyFunctionlist ) ;
  for ( i = 0 ; i < PythonFunctions.size() ; i++ ) {
//    f << Tabs << "	<PyFunction>" << endl ;
    QDomElement PyFunction = Graph.createElement("PyFunction") ;
    PyFunctionlist.appendChild( PyFunction ) ;
    int j ;
    QDomElement FuncName = Graph.createElement("FuncName") ;
    if ( strlen( FuncNames[i].c_str() ) ) {
      aField = Graph.createTextNode( FuncNames[i].c_str() ) ;
    }
    else {
      aField = Graph.createTextNode( "?" ) ;
    }
    PyFunction.appendChild( FuncName ) ;
    FuncName.appendChild( aField ) ;
    if ( (*PythonFunctions[i]).length() ) {
      for ( j = 0 ; j < (int ) (*PythonFunctions[i]).length() ; j++ ) {
        QDomElement PyFunc = Graph.createElement("PyFunc") ;
        QDomCDATASection aCDATA ;
      // mpv: Linux 8.0 compiler compatibility
        char * aCDATAChar = strdup ((*PythonFunctions[i])[j]) ;
        int i ;
        for ( i = 0 ; i < (int ) strlen( aCDATAChar ) ; i++ ) {
          if ( aCDATAChar[ i ] != ' ' ) {
            break ;
    	  }
        }
        if ( i == (int ) strlen( aCDATAChar ) ) {
          aCDATA = Graph.createCDATASection( "?" ) ;
        }
        else {
          aCDATA = Graph.createCDATASection( aCDATAChar ) ;
        }
        PyFunction.appendChild( PyFunc ) ;
        PyFunc.appendChild( aCDATA ) ;
      }
    }
    else {
      QDomElement PyFunc = Graph.createElement("PyFunc") ;
      QDomCDATASection aCDATA = Graph.createCDATASection( "?" ) ;
      PyFunction.appendChild( PyFunc ) ;
      PyFunc.appendChild( aCDATA ) ;
    }
  }

//  f << Tabs << "<creation-date>" << FirstCreation() << "</creation-date>"
//    << endl ;
  QDomElement creationdate = Graph.createElement("creation-date") ;
  char fdate[30] ;
  sprintf( fdate , "%d/%d/%d - %d:%d:%d" ,  FirstCreation().Day , FirstCreation().Month , FirstCreation().Year , FirstCreation().Hour , FirstCreation().Minute , FirstCreation().Second ) ;
  aField = Graph.createTextNode( fdate ) ;
  node.appendChild( creationdate ) ;
  creationdate.appendChild( aField ) ;
//  f << Tabs << "<lastmodification-date>" << LastModification()
//    << "</lastmodification-date>" << endl ;
  QDomElement lastmodificationdate = Graph.createElement("lastmodification-date") ;
  char ldate[30] ;
  sprintf( ldate , "%d/%d/%d - %d:%d:%d" , LastModification().Day , LastModification().Month , LastModification().Year , LastModification().Hour , LastModification().Minute , LastModification().Second ) ;
  aField = Graph.createTextNode( ldate ) ;
  node.appendChild( lastmodificationdate ) ;
  lastmodificationdate.appendChild( aField ) ;
//  f << Tabs << "<editor-release>" << EditorRelease() << "</editor-release>"
//    << endl ;
  QDomElement editorrelease = Graph.createElement("editor-release") ;
  aField = Graph.createTextNode( EditorRelease() ) ;
  node.appendChild( editorrelease ) ;
  editorrelease.appendChild( aField ) ;
  QDomElement author = Graph.createElement("author") ;
  if ( strlen( Author() ) ) {
//    f << Tabs << "<author>" << Author() << "</author>" << endl ;
    aField = Graph.createTextNode( Author() ) ;
  }
  else {
//    f << Tabs << "<author>?</author>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  node.appendChild( author ) ;
  author.appendChild( aField ) ;
  QDomElement container = Graph.createElement("container") ;
  if ( IsFactoryNode() && strlen( Computer) ) {
//    f << Tabs << "<container>" << Computer << "</container>" << endl ;
    aField = Graph.createTextNode( Computer ) ;
  }
  else {
//    f << Tabs << "<container>?</container>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  node.appendChild( container ) ;
  container.appendChild( aField ) ;
  QDomElement comment = Graph.createElement("comment") ;
  if ( strlen( Comment() ) ) {
//    f << Tabs << "<comment>" << Comment() << "</comment>" << endl ;
    aField = Graph.createTextNode( Comment() ) ;
  }
  else {
//    f << Tabs << "<comment>?</comment>" << endl ;
    aField = Graph.createTextNode( "?" ) ;
  }
  node.appendChild( comment ) ;
  comment.appendChild( aField ) ;
//  f << Tabs << "<x-position>" << XCoordinate << "</x-position>" << endl ;
  QDomElement xposition = Graph.createElement("x-position") ;
  QString aXCoordinate ;
  aXCoordinate = aKind.setNum( XCoordinate ) ;
  aField = Graph.createTextNode( aXCoordinate ) ;
  node.appendChild( xposition ) ;
  xposition.appendChild( aField ) ;
//  f << Tabs << "<y-position>" << YCoordinate << "</y-position>" << endl ;
  QDomElement yposition = Graph.createElement("y-position") ;
  QString aYCoordinate ;
  aYCoordinate = aKind.setNum( YCoordinate ) ;
  aField = Graph.createTextNode( aYCoordinate ) ;
  node.appendChild( yposition ) ;
  yposition.appendChild( aField ) ;
  cdebug_out << "SaveXML Node " << Name() << endl ;
  return true ;
}

bool GraphBase::ComputingNode::SavePY( ostream &f , const char * aGraphName ,
                                       const char * ComponentName ,
                                       const char * InterfaceName ,
                                       const char * Computer ,
                                       const GraphBase::InLineNode * aCoupledNode ,
                                       const ListOfFuncName FuncNames ,
                                       const ListOfPythonFunctions PythonFunctions ,
                                       int XCoordinate , int YCoordinate ) const {
  cdebug_in << "ComputingNode::SavePY " << Name() << endl ;
  // san - Presumably, data stream graphs should be always saved with kind = DataStreamGraph
// JR : the bug was at line 927 : && HasDataStream() != 0 was missing
  if ( IsDataFlowNode() || ( IsDataStreamNode() && HasDataStream() == 0 ) ) {
    f << "    " << Name() << " = Graph( '" << Name() << "' )" << endl ;
     if ( GraphMacroLevel() ) {
      f << "    " << Name() << ".SetCoupled( '"
        << ((GraphBase::GOTONode * ) this)->CoupledNodeName() << "' )" << endl ;
    }
  }
  else if ( IsDataStreamNode() && HasDataStream() != 0 ) {
    f << "    " << Name() << " = StreamGraph( '" << Name() << "' )" << endl ;
    CORBA::Long Timeout ;
    SUPERV::KindOfDataStreamTrace DataStreamTrace ;
    CORBA::Double DeltaTime ;
    ((GraphBase::StreamGraph * ) this)->StreamParams( Timeout , DataStreamTrace , DeltaTime ) ;
    f << "    " << aGraphName << ".SetStreamParams( " << Timeout << " , SUPERV." << DataStreamTrace
      << " , " << DeltaTime << " )" << endl ;
  }
  else if ( IsComputingNode() ) {
    int i ;
    f << "    " << Name() << "_ServiceinParameter = []" << endl ;
    for ( i = 0 ; i < (int ) ServiceInParameter().length() ; i++ ) {
      f << "    " << Name() << "_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( '"
        << ServiceInParameter()[i].Parametertype << "' , '"
        << ServiceInParameter()[i].Parametername << "' ) )" << endl ;
    }
    f << "    " << Name() << "_ServiceoutParameter = []" << endl ;
    for ( i = 0 ; i < (int ) ServiceOutParameter().length() ; i++ ) {
      f << "    " << Name() << "_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( '"
        << ServiceOutParameter()[i].Parametertype << "' , '"
        << ServiceOutParameter()[i].Parametername << "' ) )" << endl ;
    }
    f << "    " << Name() << "_ServiceinStreamParameter = []" << endl ;
    for ( i = 0 ; i < (int ) ServiceInStreamParameter().length() ; i++ ) {
      f << "    " << Name() << "_ServiceinStreamParameter.append( SALOME_ModuleCatalog.ServicesDataStreamParameter( '"
        << ServiceInStreamParameter()[i].Parametertype << "' , '"
        << ServiceInStreamParameter()[i].Parametername << "' , SALOME_ModuleCatalog."
        << ServiceInStreamParameter()[i].Parameterdependency << " ) )" << endl ;
    }
    f << "    " << Name() << "_ServiceoutStreamParameter = []" << endl ;
    for ( i = 0 ; i < (int ) ServiceOutStreamParameter().length() ; i++ ) {
      f << "    " << Name() << "_ServiceoutStreamParameter.append( SALOME_ModuleCatalog.ServicesDataStreamParameter( '"
        << ServiceOutStreamParameter()[i].Parametertype << "' , '"
        << ServiceOutStreamParameter()[i].Parametername << "' , SALOME_ModuleCatalog."
        << ServiceOutStreamParameter()[i].Parameterdependency << " ) )" << endl ;
    }
    f << "    " << Name() << "_Service = SALOME_ModuleCatalog.Service( '" << ServiceName()
      << "' , " << Name() << "_ServiceinParameter"
      << " , " << Name() << "_ServiceoutParameter"
      << " , " << Name() << "_ServiceinStreamParameter"
      << " , " << Name() << "_ServiceoutStreamParameter"
      << " , 0 , 0 )" << endl ;
    f << "    " << Name() << " = " << aGraphName << ".CNode( " << Name() << "_Service" << " )"
      << endl ;
  }
  else if ( IsFactoryNode() ) {
    f << "    " << Name() << " = " << aGraphName << ".FNode( '" << ComponentName
      << "' , '" << InterfaceName << "' , '" << ServiceName() << "' )"
      << endl ;
  }
  else if ( IsEndLoopNode() || IsEndSwitchNode() ) {
// It is done with LoopNode or SwitchNode with CoupledNode()
  }
  else {
    if ( !IsMacroNode() ) {
      f << "    " << "Py" << Name() << " = []" << endl ;
    }
    int i ;
    SUPERV::ListOfStrings aPyFunc ;
    if ( PythonFunctions.size() ) {
      aPyFunc = *PythonFunctions[0] ;
      for ( i = 0 ; i < (int ) aPyFunc.length() ; i++ ) {
        f << "    " << "Py" << Name() << ".append( '" << protectQuotes(aPyFunc[i].in()) << "' )" << endl;
      }
    }
    if ( IsInLineNode() ) {
      f << "    " << Name() << " = " << aGraphName << ".INode( '" << FuncNames[0].c_str() << "' , Py"
        << Name() << " )" << endl ;
    }
    else if ( IsGOTONode() ) {
      if ( aCoupledNode ) {
        f << "    " << Name() << " = " << aGraphName << ".GNode( '" << FuncNames[0].c_str() << "' , Py"
          << Name() << " , '" << aCoupledNode->Name() << "' )" << endl ;
      }
      else {
        f << "    " << Name() << " = " << aGraphName << ".GNode( '" << FuncNames[0].c_str() << "' , Py"
          << Name() << " , '' )" << endl ;
      }
    }
    else if ( IsMacroNode() ) {
      if ( aCoupledNode ) {
        f << "    " << aCoupledNode->Name() << " = Def" << aCoupledNode->Name() << "()" << endl ;
        f << "    " << Name() << " = " << aGraphName << ".GraphMNode( " << aCoupledNode->Name() << " )" << endl ;
      }
      else {
        f << "    " << Name() << " = " << aGraphName << ".GraphMNode( ? )" << endl ;
      }
      f << "    " << Name() << ".SetCoupled( '" << aCoupledNode->Name() << "' )" << endl ;
    }
    else {
//      char * EndName = NULL ;
//      EndName = new char[ 3 + strlen( Name() ) + 1 ] ;
//      strcpy( EndName , "End" ) ;
//      strcat( EndName , Name() ) ;
      char * EndName = aCoupledNode->Name() ;
      cdebug << "ComputingNode::SavePY Node " << Name() << " EndName " << EndName
             << endl ;
      if ( IsLoopNode() ) {
        int i ;
        SUPERV::ListOfStrings aPyMore = *PythonFunctions[1] ;
        SUPERV::ListOfStrings aPyNext = *PythonFunctions[2] ;
        f << "    " << "PyMore" << Name() << " = []" << endl ;

        for ( i = 0 ; i < (int ) aPyMore.length() ; i++ ) {
          f << "    " << "PyMore" << Name() << ".append( '"
            << protectQuotes(aPyMore[i].in()) << "' )" << endl;
        }
        f << "    " << "PyNext" << Name() << " = []" << endl ;
        for ( i = 0 ; i < (int ) aPyNext.length() ; i++ ) {
          f << "    " << "PyNext" << Name() << ".append( '"
            << protectQuotes(aPyNext[i].in()) << "' )" << endl ;
        }
        f << "    " << Name() << "," << EndName << " = " << aGraphName << ".LNode( '"
          << FuncNames[0].c_str() << "' , Py" << Name() << " , '"
          << FuncNames[1].c_str() << "' , PyMore" << Name()
          << " , '" << FuncNames[2].c_str() << "' , PyNext"
          << Name() << " )" << endl ;
        f << "    " << EndName << ".SetName( '" << aCoupledNode->Name() << "' )" << endl ;
        f << "    " << EndName << ".SetAuthor( '" << aCoupledNode->Author() << "' )" << endl ;
        f << "    " << EndName << ".SetComment( '" << aCoupledNode->Comment() << "' )" << endl ;
        f << "    " << EndName << ".Coords( " << aCoupledNode->XCoordinate() << " , "
          << aCoupledNode->YCoordinate() << " )" << endl ;
        SUPERV::ListOfStrings aPyFunc = *aCoupledNode->PythonFunction() ;
        f << "    " << "Py" << aCoupledNode->Name() << " = []" << endl ;
        for ( i = 0 ; i < (int ) aPyFunc.length() ; i++ ) {
          f << "    " << "Py" << aCoupledNode->Name() << ".append( '"
            << protectQuotes(aPyFunc[i].in()) << "' )" << endl;
        }
        f << "    " << EndName << ".SetPyFunction( '" << aCoupledNode->PyFuncName() << "' , Py" << aCoupledNode->Name() << " )" << endl ;
        for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
          const GraphBase::InPort * anInPort = GetNodeInPort(i) ;
          cdebug << "Node " << Name() << " InPort " << anInPort->PortName()
                 << " " << anInPort->Kind() << endl ;
          if ( anInPort->IsLoop() ) {
            f << "    " << "I" << Name() << anInPort->PortName() << " = "
              << Name() << ".GetInPort( '" << anInPort->PortName() << "' )" << endl ;
    	  }
          else if ( anInPort->IsInLine() ) {
            f << "    " << "I" << Name() << anInPort->PortName() << " = "
              << Name() << ".InPort( '" << anInPort->PortName() << "' , '"
              << anInPort->PortType() << "' )" << endl ;
          }
          else if ( anInPort->IsDataStream() ) {
            f << "    " << "I" << Name() << anInPort->PortName() << " = " << Name()
              << ".InStreamPort( '" << anInPort->PortName() << "' , '" << anInPort->PortType()
              << "' , SALOME_ModuleCatalog." << anInPort->Dependency() << " )" << endl ;
            SUPERV::KindOfSchema        aKindOfSchema ;
            SUPERV::KindOfInterpolation aKindOfInterpolation ;
            SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
            ((GraphBase::InDataStreamPort * ) anInPort)->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
            f << "    " << "I" << Name() << anInPort->PortName() << ".SetParams( SUPERV." << aKindOfSchema << " , SUPERV."
              << aKindOfInterpolation << " , SUPERV." << aKindOfExtrapolation << " )" << endl ;
	  }
          else if ( anInPort->IsGate() ) {
            f << "    " << "I" << Name() << anInPort->PortName() << " = "
              << Name() << ".GetInPort( '" << anInPort->PortName() << "' )" << endl ;
	  }
        }
        for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
          const GraphBase::OutPort * anOutPort = GetNodeOutPort(i) ;
          cdebug << "Node " << Name() << " OutPort " << anOutPort->PortName()
                 << " " << anOutPort->Kind() << endl ;
          if ( anOutPort->IsInLine() || anOutPort->IsLoop() ) {
            f << "    " << "O" << Name() << anOutPort->PortName() << " = "
              << Name() << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
          }
          else if ( anOutPort->IsDataStream() ) {
            f << "    " << "O" << Name() << anOutPort->PortName() << " = " << Name()
              << ".OutStreamPort( '" << anOutPort->PortName()
              << "' , '" << anOutPort->PortType()
              << "' , SALOME_ModuleCatalog." << anOutPort->Dependency() << " )" << endl ;
            long aNumberOfValues ;
            aNumberOfValues = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
            f << "    " << "O" << Name() << anOutPort->PortName() << ".SetNumberOfValues( " << aNumberOfValues << " )"
              << endl ;
	  }
//JR 02.09.2005 : GetOutPort for Gates was missing
          else if ( anOutPort->IsGate() ) {
            f << "    " << "O" << Name() << anOutPort->PortName() << " = "
              << Name() << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
          }
        }
        for ( i = 0 ; i < aCoupledNode->GetNodeInPortsSize() ; i++ ) {
          const GraphBase::InPort * anInPort = aCoupledNode->GetNodeInPort(i) ;
          cdebug << "Node " << aCoupledNode->Name() << " InPort " << anInPort->PortName()
                 << " " << anInPort->Kind() << endl ;
          if ( anInPort->IsInLine() || anInPort->IsLoop() || anInPort->IsGate() ) {
            f << "    " << "I" << EndName << anInPort->PortName() << " = " << EndName
              << ".GetInPort( '" << anInPort->PortName() << "' )" << endl ;
          }
          else if ( anInPort->IsDataStream() ) {
            f << "    " << "I" << EndName << anInPort->PortName() << " = " << EndName
              << ".InStreamPort( '" << anInPort->PortName()
              << "' , '" << anInPort->PortType()
              << "' , SALOME_ModuleCatalog." << anInPort->Dependency() << " )" << endl ;
            SUPERV::KindOfSchema        aKindOfSchema ;
            SUPERV::KindOfInterpolation aKindOfInterpolation ;
            SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
            ((GraphBase::InDataStreamPort * ) anInPort)->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
            f << "    " << "I" << EndName << anInPort->PortName() << ".SetParams( SUPERV." << aKindOfSchema << " , SUPERV."
              << aKindOfInterpolation << " , SUPERV." << aKindOfExtrapolation << " )" << endl ;
	  }
        }
        for ( i = 0 ; i < aCoupledNode->GetNodeOutPortsSize() ; i++ ) {
          const GraphBase::OutPort * anOutPort = aCoupledNode->GetNodeOutPort(i) ;
          cdebug << "Node " << aCoupledNode->Name() << " OutPort " << anOutPort->PortName()
                 << " " << anOutPort->Kind() << endl ;
          if ( anOutPort->IsInLine() || anOutPort->IsLoop() || anOutPort->IsGate() ) {
            f << "    " << "O" << EndName << anOutPort->PortName() << " = " << EndName
              << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
          }
          else if ( anOutPort->IsDataStream() ) {
            f << "    " << "O" << EndName << anOutPort->PortName() << " = " << EndName
              << ".OutStreamPort( '" << anOutPort->PortName()
              << "' , '" << anOutPort->PortType()
              << "' , SALOME_ModuleCatalog." << anOutPort->Dependency() << " )" << endl ;
            long aNumberOfValues ;
            aNumberOfValues = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
            f << "    " << "O" << EndName << anOutPort->PortName() << ".SetNumberOfValues( " << aNumberOfValues << " )"
              << endl ;
	  }
        }
      }
      else if ( IsSwitchNode() ) {
        f << "    " << Name() << "," << EndName << " = " << aGraphName << ".SNode( '"
          << FuncNames[0].c_str() << "' , Py" << Name() << " )" << endl ;
        f << "    " << EndName << ".SetName( '" << aCoupledNode->Name() << "' )" << endl ;
        f << "    " << EndName << ".SetAuthor( '" << aCoupledNode->Author() << "' )" << endl ;
        f << "    " << EndName << ".SetComment( '" << aCoupledNode->Comment() << "' )" << endl ;
        f << "    " << EndName << ".Coords( " << aCoupledNode->XCoordinate() << " , "
          << aCoupledNode->YCoordinate() << " )" << endl ;
        SUPERV::ListOfStrings aPyFunc = *aCoupledNode->PythonFunction() ;
        f << "    " << "Py" << aCoupledNode->Name() << " = []" << endl ;
        for ( i = 0 ; i < (int ) aPyFunc.length() ; i++ ) {
          f << "    " << "Py" << aCoupledNode->Name() << ".append( '"
            << protectQuotes(aPyFunc[i].in()) << "' )" << endl;
        }
        f << "    " << EndName << ".SetPyFunction( '" << aCoupledNode->PyFuncName() << "' , Py" << aCoupledNode->Name() << " )" << endl ;
        for ( i = 0 ; i < aCoupledNode->GetNodeInPortsSize() ; i++ ) {
          const GraphBase::InPort * anInPort = aCoupledNode->GetNodeInPort(i) ;
          cdebug << "Node " << aCoupledNode->Name() << " InPort " << anInPort->PortName()
                 << " " << anInPort->Kind() << endl ;
//          if ( anInPort->IsGate() || anInPort->IsEndSwitch() ) {
          if ( anInPort->IsGate() ) {
            f << "    " << "I" << EndName << anInPort->PortName() << " = " << EndName
              << ".GetInPort( '" << anInPort->PortName() << "' )" << endl ;
          }
          else if ( anInPort->IsInLine() || anInPort->IsEndSwitch() ) {
            f << "    " << "I" << EndName << anInPort->PortName() << " = " << EndName
              << ".InPort( '" << anInPort->PortName()
              << "' , '" << anInPort->PortType() << "' )" << endl ;
          }
          else if ( anInPort->IsDataStream() ) {
            f << "    " << "I" << EndName << anInPort->PortName() << " = " << EndName
              << ".InStreamPort( '" << anInPort->PortName()
              << "' , '" << anInPort->PortType()
              << "' , SALOME_ModuleCatalog." << anInPort->Dependency() << " )" << endl ;
            SUPERV::KindOfSchema        aKindOfSchema ;
            SUPERV::KindOfInterpolation aKindOfInterpolation ;
            SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
            ((GraphBase::InDataStreamPort * ) anInPort)->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
            f << "    " << "I" << EndName << anInPort->PortName() << ".SetParams( SUPERV." << aKindOfSchema << " , SUPERV."
              << aKindOfInterpolation << " , SUPERV." << aKindOfExtrapolation << " )" << endl ;
	  }
        }
        for ( i = 0 ; i < aCoupledNode->GetNodeOutPortsSize() ; i++ ) {
          const GraphBase::OutPort * anOutPort = aCoupledNode->GetNodeOutPort(i) ;
          cdebug << "Node " << aCoupledNode->Name() << " OutPort " << anOutPort->PortName()
                 << " " << anOutPort->Kind() << endl ;
//          if ( anOutPort->IsGate() || anOutPort->IsEndSwitch() ) {
          if ( anOutPort->IsGate() ) {
            f << "    " << "O" << EndName << anOutPort->PortName() << " = " << EndName
              << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
          }
          else if ( anOutPort->IsInLine() || anOutPort->IsSwitch() ) {
            f << "    " << "O" << EndName << anOutPort->PortName() << " = " << EndName
              << ".OutPort( '" << anOutPort->PortName()
              << "' , '" << anOutPort->PortType() << "' )" << endl ;
          }
          else if ( anOutPort->IsDataStream() ) {
            f << "    " << "O" << EndName << anOutPort->PortName() << " = " << EndName
              << ".OutStreamPort( '" << anOutPort->PortName()
              << "' , '" << anOutPort->PortType()
              << "' , SALOME_ModuleCatalog." << anOutPort->Dependency() << " )" << endl ;
            long aNumberOfValues ;
            aNumberOfValues = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
            f << "    " << "O" << EndName << anOutPort->PortName() << ".SetNumberOfValues( " << aNumberOfValues << " )"
              << endl ;
	  }
        }
      }
      cdebug << "ComputingNode::SavePY Node " << Name() << " EndName " << EndName
             << endl ;
// PAL8507
//JR 24.02.2005 Debug !!!... : I should not delete myself : what a stupid thing !
//      delete [] EndName ;
    }
  }

  if ( IsEndLoopNode() || IsEndSwitchNode() ) {
// It is done with LoopNode or SwitchNode with CoupledNode()
  }
  else {
    f << "    " << Name() << ".SetName( '" << Name() << "' )" << endl ;
    f << "    " << Name() << ".SetAuthor( '" << Author() << "' )" << endl ;
    if ( IsFactoryNode() ) {
      f << "    " << Name() << ".SetContainer( '" << Computer << "' )" << endl ;
    }
    f << "    " << Name() << ".SetComment( '" << Comment() << "' )" << endl ;
    f << "    " << Name() << ".Coords( " << XCoordinate << " , " << YCoordinate << " )" << endl ;

    if ( IsComputingNode() || IsFactoryNode() ) {
      int i ;
      for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
        const GraphBase::InPort * anInPort = GetNodeInPort(i) ;
        if ( !anInPort->IsDataStream() ) {
          cdebug << "Node " << Name() << " InPort " << anInPort->PortName()
                 << " " << anInPort->Kind() << endl ;
          f << "    " << "I" << Name() << anInPort->PortName() << " = "
            << Name() << ".GetInPort( '" << anInPort->PortName() << "' )" << endl ;
	}
      }
      for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
        const GraphBase::OutPort * anOutPort = GetNodeOutPort(i) ;
        if ( !anOutPort->IsDataStream() ) {
          cdebug << "Node " << Name() << " OutPort " << anOutPort->PortName()
                 << " " << anOutPort->Kind() << endl ;
          f << "    " << "O" << Name() << anOutPort->PortName() << " = "
            << Name() << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
	}
      }
    }
    else if ( IsOneOfInLineNodes() && !IsLoopNode() ) {
      int i ;
      for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
        const GraphBase::InPort * anInPort = GetNodeInPort(i) ;
        cdebug << "ComputingNode::SavePY Node " << Name() << " InPort " << anInPort->PortName()
               << " " << anInPort->Kind() << endl ;
//        if ( anInPort->IsGate() || ( anInPort->IsInLine() && IsMacroNode() ) ) {
        if ( anInPort->IsGate() || IsMacroNode() ) {
          f << "    " << "I" << Name() << anInPort->PortName() << " = "
            << Name() << ".GetInPort( '" << anInPort->PortName()  << "' )" << endl ;
        }
        else if ( anInPort->IsInLine() || anInPort->IsEndSwitch() ) {
          f << "    " << "I" << Name() << anInPort->PortName() << " = "
            << Name() << ".InPort( '" << anInPort->PortName() << "' , '"
            << anInPort->PortType() << "' )" << endl ;
        }
        else {
          cdebug << "Ignored " << Name() << " " << anInPort->PortName() << " " << anInPort->PortStatus() << endl ;
//          f << "    " << "I" << Name() << anInPort->PortName() << " = "
//            << Name() << ".GetInPort( '" << anInPort->PortName()  << "' )" << endl ;
        }
      }
      for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
        const GraphBase::OutPort * anOutPort = GetNodeOutPort(i) ;
        cdebug << "ComputingNode::SavePY Node " << Name() << " OutPort " << anOutPort->PortName()
               << " " << anOutPort->Kind() << endl ;
//        if ( anOutPort->IsGate() || ( anOutPort->IsInLine() && IsMacroNode() ) ) {
        if ( anOutPort->IsGate() || IsMacroNode() ) {
          f << "    " << "O" << Name() << anOutPort->PortName() << " = "
            << Name() << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
        }
        else if ( anOutPort->IsInLine() || anOutPort->IsSwitch() ) {
          f << "    " << "O" << Name() << anOutPort->PortName() << " = "
            << Name() << ".OutPort( '" << anOutPort->PortName()
            << "' , '" << anOutPort->PortType() << "' )" << endl ;
        }
        else {
          cdebug << "Ignored " << Name() << " " << anOutPort->PortName() << " " << anOutPort->PortStatus() << endl ;
//          f << "    " << "O" << Name() << anOutPort->PortName() << " = "
//            << Name() << ".GetOutPort( '" << anOutPort->PortName() << "' )" << endl ;
        }
      }
    }
    if ( !IsDataFlowNode() && !IsDataStreamNode() ) {
      int i ;
      for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
        const GraphBase::InPort * anInPort = GetNodeInPort(i) ;
        cdebug << "Node " << Name() << " InPort " << anInPort->PortName()
               << " " << anInPort->Kind() << endl ;
        if ( anInPort->IsDataStream() ) {
          if ( IsOneOfInLineNodes() ) {
            f << "    " << "I" << Name() << anInPort->PortName() << " = "
              << Name() << ".InStreamPort( '" << anInPort->PortName() << "' , '"
              << anInPort->PortType() << "' , SALOME_ModuleCatalog."
              << anInPort->Dependency() << " )" << endl ;
	  }
          else {
            f << "    " << "I" << Name() << anInPort->PortName() << " = "
              << Name() << ".GetInStreamPort( '" << anInPort->PortName() << "' )" << endl ;
  	  }
          SUPERV::KindOfSchema        aKindOfSchema ;
          SUPERV::KindOfInterpolation aKindOfInterpolation ;
          SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
          ((GraphBase::InDataStreamPort * ) anInPort)->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
          f << "    " << "I" << Name() << anInPort->PortName() << ".SetParams( SUPERV." << aKindOfSchema << " , SUPERV."
            << aKindOfInterpolation << " , SUPERV." << aKindOfExtrapolation << " )" << endl ;
        }
      }
      for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
        const GraphBase::OutPort * anOutPort = GetNodeOutPort(i) ;
        cdebug << "Node " << Name() << " OutPort " << anOutPort->PortName()
               << " " << anOutPort->Kind() << endl ;
        if ( anOutPort->IsDataStream() ) {
          if ( IsOneOfInLineNodes() ) {
            f << "    " << "O" << Name() << anOutPort->PortName() << " = "
              << Name() << ".OutStreamPort( '" << anOutPort->PortName() << "' , '"
              << anOutPort->PortType() << "' , SALOME_ModuleCatalog."
              << anOutPort->Dependency() << " )" << endl ;
	  }
          else {
            f << "    " << "O" << Name() << anOutPort->PortName() << " = "
              << Name() << ".GetOutStreamPort( '" << anOutPort->PortName() << "' )" << endl ;
	  }
          long aNumberOfValues ;
          aNumberOfValues = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
          f << "    " << "O" << Name() << anOutPort->PortName() << ".SetNumberOfValues( " << aNumberOfValues << " )" << endl ;
        }
      }
    }
  }

  cdebug_out << "ComputingNode::SavePY " ;
  NodeInfo( *_fdebug ) ;
  cdebug << endl ;
  return true ;
}

void GraphBase::ComputingNode::NodeInfo(ostream & s) const {
  s << *this ;
  ListPorts( s , true ) ;
  s << ends ;
}

ostream & operator<< (ostream & f,const GraphBase::ComputingNode & G) {
//  f << "ComponentName    " << G.ComponentName() << endl ;
  if ( G.IsComputingNode() ) {
    f << "NodeName         " << G.Name() << endl ;
  }
  else {
    f << "DataFlowName     " << G.Name() << endl ;
  }
  f << "Kind             " << G.Kind() << endl ;
  f << "Service          " << *G.GetService() ;
  f << "FirstCreation    " << G.FirstCreation () << endl ;
  f << "LastModification " << G.LastModification() << endl ;
  f << "EditorRelease    " << G.EditorRelease() << endl ;
  f << "Author           " << G.Author() << endl ;
//  f << "Computer         " << G.Computer() << endl ;
  f << "Comment          " << G.Comment() << endl ;
  f << endl ;
  
  return f;
}

void GraphBase::ComputingNode::ListLinks(ostream &f ) const {
  int i ;
  for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
    const GraphBase::OutPort* fromPort = GetNodeOutPort( i ) ;
    if ( fromPort->IsPortConnected() ) {
      int j ;
      for ( j = 0 ; j < fromPort->InPortsSize() ; j++ ) {
        if ( j == 0 ) {
          f << "FromNode " << Name() << endl ;
	}
        f << "         FromServiceParameterName "
          << fromPort->GetServicesParameter().Parametername ;
        const GraphBase::InPort* toPort = fromPort->InPorts( j ) ;
        f << " ToNode " << toPort->NodeName() ;
        f << " ToServiceParameterName "
          << toPort->GetServicesParameter().Parametername;
        f << " Value " ;
        fromPort->StringValue( f ) ;
        f << endl ;
      }
    }
  }
}

ostream & operator<< (ostream &fOut,const SUPERV::SDate &D) {
//  cdebug_in << "operator<< GraphEditor::Date" << endl;

  fOut  << D.Day << "/" 
	<< D.Month << "/" 
	<< D.Year << " - " 
	<< D.Hour << ":" 
	<< D.Minute <<  ":"  
	<< D.Second;

//  cdebug_out << "operator<< GraphEditor::Date" << endl;
  return fOut;
}

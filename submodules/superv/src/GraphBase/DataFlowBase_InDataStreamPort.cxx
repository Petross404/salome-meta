//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_InDataStreamPort.cxx
//  Author : Jean Rahuel
//  Module : SUPERV

using namespace std;

#include "DataFlowBase_InDataStreamPort.hxx"

GraphBase::InDataStreamPort::InDataStreamPort() :
  GraphBase::InPort::InPort() {
  _KindOfSchema = SUPERV::SCHENULL ;
  _KindOfInterpolation = SUPERV::INTERNULL ;
  _KindOfExtrapolation = SUPERV::EXTRANULL ;
  cdebug << "GraphBase::InDataStreamPort::InDataStreamPort " << this << " "  << PortName() << " " << _KindOfSchema << " " << _KindOfInterpolation
         << " " << _KindOfExtrapolation << endl ;
}

GraphBase::InDataStreamPort::InDataStreamPort( 
           const char *const * NodeName ,
           const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
           const SALOME_ModuleCatalog::DataStreamDependency aDependency ,
           const SUPERV::KindOfSchema aKindOfSchema ,
           const SUPERV::KindOfInterpolation aKindOfInterpolation ,
           const SUPERV::KindOfExtrapolation aKindOfExtrapolation ) :
  InPort( NodeName , aserviceParameter , SUPERV::DataStreamParameter , aDependency ) {
  if ( aDependency == SALOME_ModuleCatalog::DATASTREAM_TEMPORAL ) {
    _KindOfSchema = aKindOfSchema ;
    _KindOfInterpolation = aKindOfInterpolation ;
    _KindOfExtrapolation = aKindOfExtrapolation ;
  }
  else {
    _KindOfSchema = SUPERV::SCHENULL ;
    _KindOfInterpolation = SUPERV::INTERNULL ;
    _KindOfExtrapolation = SUPERV::EXTRANULL ;
  }
  cdebug << "GraphBase::InDataStreamPort::InDataStreamPort " << this << " " << PortName() << " " << _KindOfSchema << " " << _KindOfInterpolation
         << " " << _KindOfExtrapolation << endl ;
}

GraphBase::InDataStreamPort::~InDataStreamPort() {
  cdebug << "GraphBase::InDataStreamPort::~InDataStreamPort " << this << endl ;
}

bool GraphBase::InDataStreamPort::SetParams( const SUPERV::KindOfSchema aKindOfSchema ,
                                             const SUPERV::KindOfInterpolation aKindOfInterpolation ,
                                             const SUPERV::KindOfExtrapolation aKindOfExtrapolation ) {
  bool RetVal = true ;
  if ( Dependency() == SALOME_ModuleCatalog::DATASTREAM_TEMPORAL ) {
    _KindOfSchema = aKindOfSchema ;
    _KindOfInterpolation = aKindOfInterpolation ;
    _KindOfExtrapolation = aKindOfExtrapolation ;
  }
  else {
    _KindOfSchema = SUPERV::SCHENULL ;
    _KindOfInterpolation = SUPERV::INTERNULL ;
    _KindOfExtrapolation = SUPERV::EXTRANULL ;
    RetVal = false ;
  }
  cdebug << "GraphBase::InDataStreamPort::SetParams RetVal " << RetVal << " " << PortName() << " " << _KindOfSchema << " "
         << _KindOfInterpolation << " " << _KindOfExtrapolation << endl ;
  return RetVal ;
}

void GraphBase::InDataStreamPort::Params( SUPERV::KindOfSchema & aKindOfSchema ,
                                          SUPERV::KindOfInterpolation & aKindOfInterpolation ,
                                          SUPERV::KindOfExtrapolation & aKindOfExtrapolation ) const {
  aKindOfSchema = _KindOfSchema ;
  aKindOfInterpolation = _KindOfInterpolation ;
  aKindOfExtrapolation = _KindOfExtrapolation ;
  cdebug << "GraphBase::InDataStreamPort::Params " << PortName() << " " << _KindOfSchema << " " << _KindOfInterpolation << " "
         << _KindOfExtrapolation << endl ;
}

ostream & operator<< (ostream & f ,const SUPERV::KindOfSchema & s ) {
  switch (s) {
  case SUPERV::SCHENULL :
    f << "SCHENULL";
    break;
  case SUPERV::TI :
    f << "TI";
    break;
  case SUPERV::TF :
    f << "TF";
    break;
  case SUPERV::DELTA :
    f << "DELTA";
    break;
  default :
    f << "UndefinedKindOfSchema";
    break;
  }

  return f;
}

ostream & operator<< (ostream & f ,const SUPERV::KindOfInterpolation & s ) {
  switch (s) {
  case SUPERV::INTERNULL :
    f << "INTERNULL";
    break;
  case SUPERV::L0 :
    f << "L0";
    break;
  case SUPERV::L1 :
    f << "L1";
    break;
  default :
    f << "UndefinedKindOfInterpolation";
    break;
  }

  return f;
}

ostream & operator<< (ostream & f ,const SUPERV::KindOfExtrapolation & s ) {
  switch (s) {
  case SUPERV::EXTRANULL :
    f << "EXTRANULL";
    break;
  case SUPERV::E0 :
    f << "E0";
    break;
  case SUPERV::E1 :
    f << "E1";
    break;
  default :
    f << "UndefinedKindOfExtrapolation";
    break;
  }

  return f;
}


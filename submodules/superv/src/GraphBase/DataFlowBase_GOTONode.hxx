//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_GOTONode.hxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

#ifndef _DATAFLOWBASE_GOTONODE_HXX
#define _DATAFLOWBASE_GOTONODE_HXX

#include "DataFlowBase_InLineNode.hxx"

namespace GraphBase {

  class GOTONode : public InLineNode {

    private:
    
      InLineNode * _CoupledNode ;
      char       * _CoupledNodeName ;

    public:

      GOTONode() ;
      GOTONode( CORBA::ORB_ptr ORB ,
                SALOME_NamingService* ptrNamingService ,
                const char *DataFlowName ,
                const SUPERV::KindOfNode DataFlowkind ,
                int * Graph_prof_debug ,
                ofstream * Graph_fdebug ) ;
      GOTONode( CORBA::ORB_ptr ORB ,
                SALOME_NamingService* ptrNamingService ,
                const char * FuncName ,
                const SUPERV::ListOfStrings & PythonFunction ,
                const char *NodeName ,
                const SUPERV::KindOfNode akind ,
                const SUPERV::SDate NodeFirstCreation ,
                const SUPERV::SDate NodeLastModification ,
                const char * NodeEditorRelease ,
                const char * NodeAuthor ,
                const char * NodeComment ,
                const bool   GeneratedName ,
                const long   X ,
                const long   Y ,
                int * Graph_prof_debug = NULL ,
                ofstream * Graph_fdebug = NULL ) ;
      virtual ~GOTONode() ;

      void CoupledNode( InLineNode * aCoupledNode ) ;
      const InLineNode * CoupledNode() const ;
      InLineNode * CoupledNode() ;
      void CoupledNodeName( const char * aCoupledNodeName ) ;
      char * CoupledNodeName() const ;

//      bool SaveXML(ostream &f , char *Tabs , int X , int Y ) const ;
      bool SaveXML(QDomDocument & Graph , QDomElement & info , int X , int Y ) const ;

      bool SavePY(ostream &f , const char * aGraphName , int X , int Y ) const ;

  };
  
};

#endif

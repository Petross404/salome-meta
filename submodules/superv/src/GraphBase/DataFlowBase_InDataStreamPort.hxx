//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_InDataStreamPort.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _DATAFLOWBASE_INDATASTREAMPORT_HXX
#define _DATAFLOWBASE_INDATASTREAMPORT_HXX

#include "DataFlowBase_OutPort.hxx"

namespace GraphBase {

  class InDataStreamPort : public InPort {

    private:

// if TimeDependency :
      SUPERV::KindOfSchema        _KindOfSchema ;
      SUPERV::KindOfInterpolation _KindOfInterpolation ;
      SUPERV::KindOfExtrapolation _KindOfExtrapolation ;

    protected:

    public:

      InDataStreamPort() ;

      InDataStreamPort( const char *const * NodeName  ,
                        const SALOME_ModuleCatalog::ServicesParameter aserviceParameter ,
                        const SALOME_ModuleCatalog::DataStreamDependency aDependency = SALOME_ModuleCatalog::DATASTREAM_UNDEFINED ,
                        const SUPERV::KindOfSchema aKindOfSchema = SUPERV::SCHENULL ,
                        const SUPERV::KindOfInterpolation aKindOfInterpolation = SUPERV::INTERNULL ,
                        const SUPERV::KindOfExtrapolation aKindOfExtrapolation = SUPERV::EXTRANULL ) ;

      virtual ~InDataStreamPort() ;

      bool SetParams( const SUPERV::KindOfSchema aKindOfSchema ,
                      const SUPERV::KindOfInterpolation aKindOfInterpolation ,
                      const SUPERV::KindOfExtrapolation aKindOfExtrapolation ) ;

      void Params( SUPERV::KindOfSchema & aKindOfSchema ,
                   SUPERV::KindOfInterpolation & aKindOfInterpolation ,
                   SUPERV::KindOfExtrapolation & aKindOfExtrapolation ) const ;

  };
  
};

ostream & operator<< (ostream &,const SUPERV::KindOfSchema &);

ostream & operator<< (ostream &,const SUPERV::KindOfInterpolation &);

ostream & operator<< (ostream &,const SUPERV::KindOfExtrapolation &);

#endif





//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_PortsOfNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_PortsOfNode.hxx"

#define PortsOfNodeTrace 0

GraphBase::PortsOfNode::PortsOfNode() :
  GraphBase::Service::Service( SALOME_ModuleCatalog::Service() ) {
//  MESSAGE( "GraphBase::PortsOfNode::PortsOfNode "  << this ) ;
//  cout << "GraphBase::PortsOfNode::PortsOfNode "  << this << endl ;
//  cdebug << "GraphBase::PortsOfNode::PortsOfNode "  << this << endl ;
  _NodeInPortsSize = 0 ;
  _NodeOutPortsSize = 0 ;
  _DataStreamInPortsNumber = 0 ;
  _DataStreamOutPortsNumber = 0 ;
}

GraphBase::PortsOfNode::PortsOfNode( const char *DataFlowName ) :
  GraphBase::Service::Service( SALOME_ModuleCatalog::Service() ) {
//  MESSAGE( "GraphBase::PortsOfNode::PortsOfNode "  << this ) ;
//  cout << "GraphBase::PortsOfNode::PortsOfNode "  << this << endl ;
//  cdebug << "GraphBase::PortsOfNode::PortsOfNode "  << this << endl ;
  _NodeInPortsSize = 0 ;
  _NodeOutPortsSize = 0 ;
  _DataStreamInPortsNumber = 0 ;
  _DataStreamOutPortsNumber = 0 ;
}

GraphBase::PortsOfNode::~PortsOfNode() {
  cdebug << "GraphBase::PortsOfNode::~PortsOfNode "  << this 
         << endl ;
//  int i ;
//  for ( i = 0 ; i < _NodeInPortsSize ; i++ ) {
//    delete _NodeInPorts[ i ] ;
//  }
//  for ( i = 0 ; i < _NodeOutPortsSize ; i++ ) {
//    delete _NodeOutPorts[ i ] ;
//  }
}

void GraphBase::PortsOfNode::DefPortsOfNode(
                           CORBA::ORB_ptr ORB ,
                           const SALOME_ModuleCatalog::Service& aService ,
                           const char *const * NodeName ,
                           const SUPERV::KindOfNode aKind ,
                           int * Graph_prof_debug ,
                           ofstream * Graph_fdebug ) {
  int i ;
  SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
//  MESSAGE( "DefPortsOfNode " << NodeName << " Graph_prof_debug  "
//           << Graph_prof_debug << " _prof_debug " << _prof_debug ) ;
  cdebug_in << "DefPortsOfNode : " << *NodeName << " ServiceName " << aService.ServiceName << endl ;
  const bool DataFlowOrComputing = (aKind == SUPERV::DataFlowGraph) ||
                                   (aKind == SUPERV::ComputingNode) ||
                                   (aKind == SUPERV::FactoryNode) ;
// WithInLoop : InitLoop or DoLoop
  const bool WithInLoop = (aKind == SUPERV::LoopNode) || (aKind == SUPERV::EndLoopNode) ;
// WithInGate : InGate or Default
  const bool WithInGate = (aKind == SUPERV::DataFlowGraph) || (aKind == SUPERV::DataStreamGraph) ||
                          (aKind == SUPERV::ComputingNode) || (aKind == SUPERV::FactoryNode) ||
                          (aKind == SUPERV::InLineNode) || (aKind == SUPERV::GOTONode) ||
                          (aKind == SUPERV::LoopNode) || (aKind == SUPERV::EndLoopNode) ||
                          (aKind == SUPERV::SwitchNode) || (aKind == SUPERV::EndSwitchNode) ||
                          (aKind == SUPERV::MacroNode) ;
// WithOutGate : OutGate or Default
  const bool WithOutGate = (aKind == SUPERV::DataFlowGraph) || (aKind == SUPERV::DataStreamGraph) ||
                           (aKind == SUPERV::ComputingNode) || (aKind == SUPERV::FactoryNode) ||
                           (aKind == SUPERV::InLineNode) || (aKind == SUPERV::GOTONode) ||
			     /* asv : 15.12.04 : adding out gate ports for Loop/EndLoop, too. p2.5, PAL7385*/
			   (aKind == SUPERV::LoopNode) || (aKind == SUPERV::EndLoopNode) ||
                           (aKind == SUPERV::SwitchNode) || (aKind == SUPERV::EndSwitchNode) ||
                           (aKind == SUPERV::MacroNode) ;
  SUPERV::KindOfPort aPortKind = SUPERV::ServiceParameter ;
  if ( aKind == SUPERV::InLineNode ) {
    aPortKind = SUPERV::InLineParameter ;
  }
  else if ( aKind == SUPERV::LoopNode || aKind == SUPERV::EndLoopNode ) {
    aPortKind = SUPERV::InLineParameter ;
  }
  else if ( aKind == SUPERV::SwitchNode || aKind == SUPERV::EndSwitchNode ) {
    aPortKind = SUPERV::InLineParameter ;
  }
  else if ( aKind == SUPERV::GOTONode ) {
    aPortKind = SUPERV::InLineParameter ;
  }
  else if ( aKind == SUPERV::MacroNode ) {
    aPortKind = SUPERV::InLineParameter ;
  }
  for ( i = 0 ; i < _NodeInPortsSize ; i++ ) {
    cdebug << "DefPortsOfNode delete In" << i << ". "  << _NodeInPorts[i]->PortName()
           << endl ;
    _MapOfNodeInPorts.erase( _NodeInPorts[i]->PortName() ) ;
    delete _NodeInPorts[i] ;
  }
  _NodeInPorts.resize( 0 );
  for ( i = 0 ; i < _NodeOutPortsSize ; i++ ) {
    cdebug << "DefPortsOfNode delete Out" << i << ". " << _NodeOutPorts[i]->PortName()
           << endl ;
    _MapOfNodeOutPorts.erase( _NodeOutPorts[i]->PortName() ) ;
    delete _NodeOutPorts[i] ;
  }
  _NodeOutPorts.resize( 0 );

  SetService( aService ) ;

  int iVec = 0 ;
  _NodeInPortsSize = aService.ServiceinParameter.length() ;
  if ( WithInLoop ) {
    SALOME_ModuleCatalog::ServicesParameter anInLoopParameter ;
    _NodeInPortsSize = _NodeInPortsSize + 1 ;
    iVec += 1 ;
//    char *aParametername = "InitLoop" ;
//    if ( aKind == SUPERV::EndLoopNode ) {
      char * aParametername = "DoLoop" ;
//    }
    anInLoopParameter.Parametertype = CORBA::string_dup( "long" ) ;
    anInLoopParameter.Parametername = CORBA::string_dup( aParametername ) ;
    if ( _NodeInPortsSize > (int ) _NodeInPorts.size() ) {
      _NodeInPorts.resize( _NodeInPortsSize );
    }
    _MapOfNodeInPorts[ aParametername ] = iVec ;
    _NodeInPorts[iVec-1] = new GraphBase::InPort( NodeName , anInLoopParameter ,
                                                  SUPERV::LoopParameter );
    _NodeInPorts[iVec-1]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
    cdebug << "DefPortsOfNode : " << "_MapOfNodeInPorts[ " << aParametername
           << " ] = " << iVec << " " << _NodeInPorts[iVec-1]->Kind() << " "
            << _NodeInPorts[iVec-1]  << endl ;
  }
  if ( _NodeInPortsSize > (int ) _NodeInPorts.size() ) {
    _NodeInPorts.resize(_NodeInPortsSize);
  }
  cdebug << "NodeInPortsSize " << _NodeInPortsSize << endl ;
  for ( i = iVec ; i < _NodeInPortsSize ; i++ ) {
//JR 24.02.2005 Memory Leak    string _aParametername = CORBA::string_dup(aService.ServiceinParameter[i-iVec].Parametername) ;
    string _aParametername = string(aService.ServiceinParameter[i-iVec].Parametername) ;
    const char *aParametername = _aParametername.c_str() ;
    if ( _MapOfNodeInPorts[ aParametername ] ) {
      if ( !DataFlowOrComputing &&
           !strcmp( aParametername , "Gate" ) ) {
      }
      else {
        cdebug << "Error, Parametername duplicated : " << aParametername
               << endl ;
      }
    }
    else {
      _MapOfNodeInPorts[ aParametername ] = i+1 ;
//      cdebug << "DefPortsOfNode : " << "_MapOfNodeInPorts[ " << aParametername
//             << " ] = " << i+1 << endl ;
      const SALOME_ModuleCatalog::ServicesParameter aServiceParameter = aService.ServiceinParameter[i-iVec] ;
      _NodeInPorts[i] = new GraphBase::InPort( NodeName , aServiceParameter ,
                                               aPortKind );
      _NodeInPorts[i]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
      cdebug << i << ". " << *_NodeInPorts[i] << " " << _NodeInPorts[i]->Kind() << endl ;
    }
  }
  iVec += aService.ServiceinParameter.length() ;
  if ( WithInGate ) {
    SALOME_ModuleCatalog::ServicesParameter anInGateParameter ;
    _NodeInPortsSize = _NodeInPortsSize + 1 ;
    iVec += 1 ;
    char *aParametername = "Gate" ;
    if ( aKind == SUPERV::EndSwitchNode ) {
      aParametername = "Default" ;
    }
    anInGateParameter.Parametertype = CORBA::string_dup( "long" ) ;
    anInGateParameter.Parametername = CORBA::string_dup( aParametername ) ;
    _NodeInPorts.resize( _NodeInPortsSize );
    _MapOfNodeInPorts[ aParametername ] = iVec ;
    _NodeInPorts[iVec-1] = new GraphBase::InPort( NodeName , anInGateParameter ,
                                                  SUPERV::GateParameter );
    _NodeInPorts[iVec-1]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
//    cdebug << "DefPortsOfNode : " << "_MapOfNodeInPorts[ " << aParametername
//           << " ] = " << iVec << " " << _NodeInPorts[iVec-1]->Kind() << " "
//            << _NodeInPorts[iVec-1]  << endl ;
  }
  
  iVec = 0 ;
  _NodeOutPortsSize = aService.ServiceoutParameter.length() ;
  if ( WithInLoop ) {
    SALOME_ModuleCatalog::ServicesParameter anOutLoopParameter ;
    _NodeOutPortsSize = _NodeOutPortsSize + 1 ;
    iVec += 1 ;
    char *aParametername = "DoLoop" ;
    anOutLoopParameter.Parametertype = CORBA::string_dup( "long" ) ;
    anOutLoopParameter.Parametername = CORBA::string_dup( aParametername ) ;
    if ( _NodeOutPortsSize > (int ) _NodeOutPorts.size() ) {
      _NodeOutPorts.resize(_NodeOutPortsSize);
    }
    _MapOfNodeOutPorts[ aParametername ] = iVec ;
    _NodeOutPorts[iVec-1] = new GraphBase::OutPort( NodeName , anOutLoopParameter ,
                                                    SUPERV::LoopParameter );
    _NodeOutPorts[iVec-1]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
//    cdebug << "DefPortsOfNode : " << "_MapOfNodeOutPorts[ " << aParametername
//           << " ] = " << iVec << " " << _NodeOutPorts[iVec-1]->Kind() << " "
//           << _NodeOutPorts[iVec-1]  << endl ;
  }
  if ( _NodeOutPortsSize > (int ) _NodeOutPorts.size() ) {
    _NodeOutPorts.resize(_NodeOutPortsSize);
  }
  cdebug << "NodeOutPortsSize " << _NodeOutPortsSize << endl ;
  for ( i = iVec ; i < _NodeOutPortsSize ; i++ ) {
//JR 24.02.2005 Memory Leak    string _aParametername = CORBA::string_dup(aService.ServiceoutParameter[i-iVec].Parametername) ;
    string _aParametername = string(aService.ServiceoutParameter[i-iVec].Parametername) ;
    const char *aParametername = _aParametername.c_str() ;
    if ( _MapOfNodeOutPorts[ aParametername ] ) {
      if ( !DataFlowOrComputing &&
           !strcmp( aParametername , "Gate" ) ) {
      }
      else {
        cdebug << "Error, Parametername duplicated : " << aParametername
               << endl ;
      }
    }
    else {
//      cdebug << "DefPortsOfNode : " << "_MapOfNodeOutPorts[ " << aParametername
//             << " ] = " << i+1 << endl ;
      _MapOfNodeOutPorts[ aParametername ] = i+1 ;
      const SALOME_ModuleCatalog::ServicesParameter aServiceParameter = aService.ServiceoutParameter[i-iVec] ;
      _NodeOutPorts[i] = new GraphBase::OutPort( NodeName , aServiceParameter ,
                                                 aPortKind );
      _NodeOutPorts[i]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
      cdebug << i << ". " << *_NodeOutPorts[i] << " " << _NodeOutPorts[i]->Kind() << endl ;
    }
  }
  iVec += aService.ServiceoutParameter.length() ;
  if ( WithOutGate ) {
    SALOME_ModuleCatalog::ServicesParameter anOutGateParameter ;
    _NodeOutPortsSize = _NodeOutPortsSize + 1 ;
    iVec += 1 ;
    char *aParametername = "Gate" ;
    if ( aKind == SUPERV::SwitchNode ) {
      aParametername = "Default" ;
    }
    anOutGateParameter.Parametertype = CORBA::string_dup( "long" ) ;
    anOutGateParameter.Parametername = CORBA::string_dup( aParametername ) ;
    _NodeOutPorts.resize(_NodeOutPortsSize);
    _MapOfNodeOutPorts[ aParametername ] = iVec ;
    _NodeOutPorts[iVec-1] = new GraphBase::OutPort( NodeName , anOutGateParameter ,
                                                    SUPERV::GateParameter );
    _NodeOutPorts[iVec-1]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
//    cdebug << "DefPortsOfNode : " << "_MapOfNodeOutPorts[ " << aParametername
//           << " ] = " << iVec << " " << _NodeOutPorts[iVec-1]->Kind() << " "
//           << _NodeOutPorts[iVec-1]  << endl ;
  }
  cdebug_out << "DefPortsOfNode : " << *NodeName << endl ;
}

GraphBase::InPort * GraphBase::PortsOfNode::AddInPort( CORBA::ORB_ptr ORB ,
                                                       const char *const * NodeName ,
                                                       const SUPERV::KindOfNode aKindOfNode ,
                                                       const char * InputParameterName ,
                                                       const char * InputParameterType ,
                                                       SUPERV::KindOfPort aKindOfPort ,
                                                       int index ,
                                                       int * Graph_prof_debug ,
                                                       ofstream * Graph_fdebug ) {
//  MESSAGE( "DefPortsOfNode " << *NodeName << " Graph_prof_debug "
//           << Graph_prof_debug << " _prof_debug " << _prof_debug ) ;
  cdebug_in << "PortsOfNode::AddInPort : " << *NodeName << " " << aKindOfNode << " InputParameterName "
            << InputParameterName << " InputParameterType " << InputParameterType << " aKindOfPort "
            << aKindOfPort << " DataStreamInPortsNumber " << DataStreamInPortsNumber() << " _NodeInPortsSize "
            << _NodeInPortsSize << " index " << index << endl ;
  GraphBase::InPort * anInPort = NULL ;
//  int index = 0 ;
  anInPort = GetChangeInPort( InputParameterName ) ;
  if ( anInPort == NULL ) {
    _NodeInPortsSize = _NodeInPortsSize + 1 ;
    _NodeInPorts.resize(_NodeInPortsSize);
    SALOME_ModuleCatalog::ServicesParameter aServiceParameter ;
    aServiceParameter.Parametername = CORBA::string_dup( InputParameterName ) ;
    aServiceParameter.Parametertype = CORBA::string_dup( InputParameterType ) ;
    if ( index != -1 ) {
    }
    else if ( aKindOfPort == SUPERV::DataStreamParameter ) {
      index = _NodeInPortsSize-2 ;
      IncrDataStreamInPorts() ;
    }
    else {
      index = _NodeInPortsSize-2 - DataStreamInPortsNumber() ;
    }
    if ( index >= 0 ) {
      int i ;
      for ( i = _NodeInPortsSize - 2 ; i >= index ; i-- ) {
        _NodeInPorts[ i + 1 ] = _NodeInPorts[ i ] ; // Gate - Default
        _MapOfNodeInPorts.erase( _NodeInPorts[ i + 1 ]->PortName() ) ;
        _MapOfNodeInPorts[ _NodeInPorts[ i + 1 ]->PortName() ] =  i + 2 ;
      }
      _MapOfNodeInPorts[ InputParameterName ] = index + 1 ;
      if ( aKindOfPort == SUPERV::DataStreamParameter ) {
        _NodeInPorts[index] = new GraphBase::InDataStreamPort( NodeName ,
                                                               aServiceParameter ) ;
      }
      else {
        _NodeInPorts[index] = new GraphBase::InPort( NodeName ,
                                                     aServiceParameter ,
                                                     aKindOfPort ) ;
      }
      _NodeInPorts[index]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
      anInPort = _NodeInPorts[index] ;
      cdebug << "NodeInPorts[ " << index << " ]" << endl ;
    }
    else {
      cdebug << "GraphBase::PortsOfNode::AddInPort ERROR index " << index << " _NodeInPortsSize "
             << _NodeInPortsSize  << endl ;
    }
  }
  else {
    cdebug << "GraphBase::PortsOfNode::AddInPort InPort already exists" << endl ;
    anInPort->Kind( aKindOfPort ) ;
  }
  cdebug << "GraphBase::PortsOfNode::AddInPort index " << index << " _NodeInPortsSize " << _NodeInPortsSize  << endl ;
//#if 0
  int i ;
  for ( i = 0 ; i < _NodeInPortsSize ; i++ ) {
    cdebug << *NodeName << " _NodeInPorts[ " << i << " ] = " << _NodeInPorts[ i ]->PortName()
           << " _MapOfNodeInPorts[ " << _NodeInPorts[ i ]->PortName() << " ] = "
           << _MapOfNodeInPorts[ _NodeInPorts[ i ]->PortName() ] << " - 1 "
           << _NodeInPorts[ i ]->Kind() << " Dependency " << _NodeInPorts[ i ]->Dependency() ;
    if ( _NodeInPorts[ i ]->IsDataStream() ) {
       SUPERV::KindOfSchema aKindOfSchema ;
       SUPERV::KindOfInterpolation aKindOfInterpolation ;
       SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
       ((GraphBase::InDataStreamPort * ) _NodeInPorts[ i ])->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
       cdebug << " " << aKindOfSchema << " " << aKindOfInterpolation << " " << aKindOfExtrapolation ;
    }
    if ( _NodeInPorts[ i ]->GetOutPort() ) {
      cdebug << " <--OutPort " << _NodeInPorts[ i ]->GetOutPort()
             << " " << _NodeInPorts[ i ]->GetOutPort()->NodeName()
             << "(" << _NodeInPorts[ i ]->GetOutPort()->PortName() << ")" ;
    }
    else {
      cdebug << " <--NOOutPort " << _NodeInPorts[ i ]->GetOutPort() ;
    }
    cdebug << " " << _NodeInPorts[ i ]->PortStatus() << endl ;
  }
//#endif
  cdebug_out << "GraphBase::PortsOfNode::AddInPort _NodeInPortsSize " << _NodeInPortsSize
             << " " << anInPort->Kind() << endl ;
  return anInPort ;
}

GraphBase::OutPort * GraphBase::PortsOfNode::AddOutPort( CORBA::ORB_ptr ORB ,
                                                         const char * const * NodeName ,
                                                         const SUPERV::KindOfNode aKindOfNode ,
                                                         const char * OutputParameterName ,
                                                         const char * OutputParameterType ,
                                                         SUPERV::KindOfPort aKindOfPort ,
                                                         int index ,
                                                         int * Graph_prof_debug ,
                                                         ofstream * Graph_fdebug ) {
  cdebug_in << "GraphBase::PortsOfNode::AddOutPort : " << *NodeName << " " << aKindOfNode
            << " OutputParameterName " << OutputParameterName << " OutputParameterType " << OutputParameterType
            << " aKindOfPort " << aKindOfPort << " DataStreamOutPortsNumber " << DataStreamOutPortsNumber()
            << " _NodeOutPortsSize " << _NodeOutPortsSize << " index " << index << endl ;
  GraphBase::OutPort * anOutPort = NULL ;
//  int index = 0 ;
  anOutPort = GetChangeOutPort( OutputParameterName ) ;
  if ( anOutPort == NULL ) {
    _NodeOutPortsSize = _NodeOutPortsSize + 1 ;
    _NodeOutPorts.resize(_NodeOutPortsSize);
    SALOME_ModuleCatalog::ServicesParameter aServiceParameter ;
    aServiceParameter.Parametername = CORBA::string_dup( OutputParameterName ) ;
    aServiceParameter.Parametertype = CORBA::string_dup( OutputParameterType ) ;
    if ( index != -1 ) {
    }
    else if ( aKindOfPort == SUPERV::DataStreamParameter ) {
      index = _NodeOutPortsSize-2 ;
      IncrDataStreamOutPorts() ;
    }
    else {
      index = _NodeOutPortsSize-2 - DataStreamOutPortsNumber() ;
    }
//JR Debug 01.09.2005 : GraphEditGraphSwitchs for AddOutPort and not in AddInPort ???
//    if ( aKindOfNode == SUPERV::LoopNode || aKindOfNode == SUPERV::EndLoopNode ) {
//      index += 1 ;
//    }
    if ( index >= 0 ) {
      int i ;
      for ( i = _NodeOutPortsSize - 2 ; i >= index ; i-- ) {
        _NodeOutPorts[ i + 1 ] = _NodeOutPorts[ i ] ; // Gate - Default
        _MapOfNodeOutPorts.erase( _NodeOutPorts[ i + 1 ]->PortName() ) ;
        _MapOfNodeOutPorts[ _NodeOutPorts[ i + 1 ]->PortName() ] =  i + 2 ;
      }
      _MapOfNodeOutPorts[ OutputParameterName ] = index + 1 ;
      if ( aKindOfPort == SUPERV::DataStreamParameter ) {
        _NodeOutPorts[index] = new GraphBase::OutDataStreamPort( NodeName ,
                                                               aServiceParameter ) ;
      }
      else {
        _NodeOutPorts[index] = new GraphBase::OutPort( NodeName ,
                                                       aServiceParameter ,
                                                       aKindOfPort );
      }
//    MESSAGE( "GraphBase::PortsOfNode::AddOutPort " << OutputParameterName << " --> SetDebug" ) ;
      _NodeOutPorts[index]->SetDebug( ORB , Graph_prof_debug , Graph_fdebug ) ;
      anOutPort = _NodeOutPorts[index] ;
      cdebug << "NodeOutPorts[ " << index << " ]" << endl ;
    }
    else {
      cdebug << "GraphBase::PortsOfNode::AddOutPort ERROR index " << index << " _NodeOutPortsSize "
             << _NodeOutPortsSize  << endl ;
    }
  }
  else {
    cdebug << "GraphBase::PortsOfNode::AddOutPort OutPort already exists" << endl ;
    anOutPort->Kind( aKindOfPort ) ;
  }
  cdebug << "GraphBase::PortsOfNode::AddOutPort index " << index << " _NodeOutPortsSize " << _NodeOutPortsSize  << endl ;
//#if 0
  int i ;
  for ( i = 0 ; i < _NodeOutPortsSize ; i++ ) {
    cdebug << *NodeName << " _NodeOutPorts[ " << i << " ] = " << _NodeOutPorts[ i ]->PortName()
           << " _MapOfNodeOutPorts[ " << _NodeOutPorts[ i ]->PortName() << " ] = "
           << _MapOfNodeOutPorts[ _NodeOutPorts[ i ]->PortName() ] << " - 1 "
           << _NodeOutPorts[ i ]->Kind() << " Dependency " << _NodeOutPorts[ i ]->Dependency() ;
    if ( _NodeOutPorts[ i ]->IsDataStream() ) {
       cdebug << " NumberOfValues " << ((GraphBase::OutDataStreamPort * ) _NodeOutPorts[ i ])->NumberOfValues() ;
    }
    cdebug << " " << _NodeOutPorts[ i ]->PortStatus() << endl ;
  }
//#endif
  cdebug_out << "GraphBase::PortsOfNode::AddOutPort _NodeOutPortsSize "
             << _NodeOutPortsSize << " " << anOutPort->Kind() << endl ;
  return anOutPort ;
}

void GraphBase::PortsOfNode::MoveInPort( const char * InputParameterName , int toindex ) {
  int fromindex = _MapOfNodeInPorts[ InputParameterName ] - 1 ;
  GraphBase::InPort * anInPort = _NodeInPorts[ toindex ] ;
  _NodeInPorts[ toindex ] =  _NodeInPorts[ fromindex ] ;
  _NodeInPorts[ fromindex ] = anInPort ;
  _MapOfNodeInPorts.erase( _NodeInPorts[ toindex ]->PortName() ) ;
  _MapOfNodeInPorts[ _NodeInPorts[ toindex ]->PortName() ] = toindex + 1  ;
  _MapOfNodeInPorts.erase( _NodeInPorts[ fromindex ]->PortName() ) ;
  _MapOfNodeInPorts[ _NodeInPorts[ fromindex ]->PortName() ] = fromindex + 1  ;
}

void GraphBase::PortsOfNode::MoveOutPort( const char * OutputParameterName , int toindex ) {
  int fromindex = _MapOfNodeOutPorts[ OutputParameterName ] - 1 ;
  GraphBase::OutPort * anOutPort = _NodeOutPorts[ toindex ] ;
  _NodeOutPorts[ toindex ] =  _NodeOutPorts[ fromindex ] ;
  _NodeOutPorts[ fromindex ] = anOutPort ;
  _MapOfNodeOutPorts.erase( _NodeOutPorts[ toindex ]->PortName() ) ;
  _MapOfNodeOutPorts[ _NodeOutPorts[ toindex ]->PortName() ] = toindex + 1  ;
  _MapOfNodeOutPorts.erase( _NodeOutPorts[ fromindex ]->PortName() ) ;
  _MapOfNodeOutPorts[ _NodeOutPorts[ fromindex ]->PortName() ] = fromindex + 1  ;
}

void GraphBase::PortsOfNode::DelInPort( const char * InputParameterName ) {
  cdebug << "DefPortsOfNode::DelInPort : " << InputParameterName << endl ;
  int index = _MapOfNodeInPorts[ InputParameterName ] -1 ;
  if ( index >= 0 ) {
    cdebug << "DefPortsOfNode::DelInPort : _NodeInPorts[" << index << "] "
           << _NodeInPorts[ index ]->PortName() << " "
           << _NodeInPorts[ index ]->NodeName() << endl ;
//PAL9122
//JR Debug 08.06.2005 : RemoveOutPort was missing
    _NodeInPorts[ index ]->RemoveOutPort() ;
    if ( _NodeInPorts[ index ]->IsDataStream() ) {
      DecrDataStreamInPorts() ;
    }
    _MapOfNodeInPorts.erase( InputParameterName ) ;
    _NodeInPorts[ index ]->destroy() ;
    int i ;
    for ( i = index ; i < _NodeInPortsSize - 1 ; i++ ) {
      _MapOfNodeInPorts.erase( _NodeInPorts[ i+1 ]->PortName() ) ;
      _MapOfNodeInPorts[ _NodeInPorts[ i+1 ]->PortName() ] = i+1 ;
      _NodeInPorts[ i ] = _NodeInPorts[ i+1 ] ;
    }
    _NodeInPortsSize = _NodeInPortsSize - 1 ;
    _NodeInPorts.resize( _NodeInPortsSize ) ;
    for ( i = 0 ; i < _NodeInPortsSize ; i++ ) {
      cdebug << "DefPortsOfNode::DelInPort ListOfInPorts : _NodeInPorts[" << i << "] "
             << _NodeInPorts[ i ]->PortName() << " "
             << _NodeInPorts[ i ]->NodeName() << endl ;
    }
  }
  else {
    cdebug << "DefPortsOfNode::DelInPort : index out of range ERROR " << index
           << endl ;
  }
}

void GraphBase::PortsOfNode::DelOutPort( const char * OutputParameterName ) {
  cdebug << "DefPortsOfNode::DelOutPort : " << OutputParameterName << endl ;
  int index = _MapOfNodeOutPorts[ OutputParameterName ] - 1 ;
  if ( index >= 0 ) {
    cdebug << "DefPortsOfNode::DelOutPort : _NodeOutPorts[" << index << "] "
           << _NodeOutPorts[ index ]->PortName() << " "
           << _NodeOutPorts[ index ]->NodeName() << endl ;
    if ( _NodeOutPorts[ index ]->IsDataStream() ) {
      DecrDataStreamOutPorts() ;
    }
    _MapOfNodeOutPorts.erase( OutputParameterName ) ;
    _NodeOutPorts[ index ]->destroy() ;
    int i ;
    for ( i = index ; i < _NodeOutPortsSize - 1 ; i++ ) {
      _MapOfNodeOutPorts.erase( _NodeOutPorts[ i+1 ]->PortName() ) ;
      _MapOfNodeOutPorts[ _NodeOutPorts[ i+1 ]->PortName() ] = i+1 ;
      _NodeOutPorts[ i ] = _NodeOutPorts[ i+1 ] ;
    }
    _NodeOutPortsSize = _NodeOutPortsSize - 1 ;
    _NodeOutPorts.resize( _NodeOutPortsSize ) ;
    for ( i = 0 ; i < _NodeOutPortsSize ; i++ ) {
      cdebug << "DefPortsOfNode::DelOutPort ListOfOutPorts : _NodeOutPorts[" << i
             << "] " << _NodeOutPorts[ i ]->PortName() << " "
             << _NodeOutPorts[ i ]->NodeName() << endl ;
    }
  }
  else {
    cdebug << "DefPortsOfNode::DelOutPort : index out of range ERROR " << index
           << endl ;
  }
}

const GraphBase::InPort *GraphBase::PortsOfNode::GetInPort( const char *name) {
 return GetChangeInPort( name ) ;
}

const GraphBase::OutPort *GraphBase::PortsOfNode::GetOutPort( const char *name) {
 return GetChangeOutPort( name ) ;
}

GraphBase::InPort *GraphBase::PortsOfNode::GetChangeInPort( const char * name) {
#if PortsOfNodeTrace
  cdebug_in << "GraphBase::PortsOfNode::GetChangeInPort Service " << ServiceName()
            << " of OutPort " << name << endl;
#endif

  GraphBase::InPort * pP = NULL;
  if ( !_MapOfNodeInPorts.empty() ) {
    int index = 0 ;
    if ( !strcmp( name , "InGate" ) ) {
      index = _MapOfNodeInPorts[ "Gate" ] ;
    }
    else {
      index = _MapOfNodeInPorts[ name ] ;
    }
    if ( index > 0 ) {
#if PortsOfNodeTrace
      cdebug << "GraphBase::PortsOfNode::GetChangeInPort _MapOfNodeInPorts[ "
             << name << " ] : " << index-1 << " " << _NodeInPorts[ index-1 ]->NodeName()
             << endl ;
#endif
      pP = _NodeInPorts[ index-1 ] ;
    }
    else {
#if PortsOfNodeTrace
      cdebug << "GraphBase::PortsOfNode::GetChangeInPort Service " << ServiceName()
             << " _MapOfNodeInPorts[ " << name << " ] : " << index << " NOT FOUND. Known ports :"
             << endl ;
      map< string , int >::iterator aMapOfNodeInPortsIterator ;
      int i = 0 ;
      for ( aMapOfNodeInPortsIterator = _MapOfNodeInPorts.begin() ;
            aMapOfNodeInPortsIterator != _MapOfNodeInPorts.end() ; aMapOfNodeInPortsIterator++ ) {
        cdebug << "MapOfNodeInPorts" << i++ << " " << aMapOfNodeInPortsIterator->first << " --> "
               << aMapOfNodeInPortsIterator->second << endl ;
      }
#endif
    }
  }
#if PortsOfNodeTrace
  cdebug_out << "GraphBase::PortsOfNode::GetChangeInPort " << endl;
#endif

  return pP;
}

GraphBase::OutPort *GraphBase::PortsOfNode::GetChangeOutPort( const char * name ) {
#if PortsOfNodeTrace
  cdebug_in << "GraphBase::PortsOfNode::GetChangeOutPort " << name << endl;
#endif

  GraphBase::OutPort * pP = NULL;
  if ( !_MapOfNodeOutPorts.empty() ) {
    int index = 0 ;
    if ( !strcmp( name , "OutGate" ) ) {
      index = _MapOfNodeOutPorts[ "Gate" ] ;
    }
    else {
      index = _MapOfNodeOutPorts[ name ] ;
    }
    if ( index > 0 ) {
#if PortsOfNodeTrace
      cdebug << "GraphBase::PortsOfNode::GetChangeOutPort _MapOfNodeOutPorts[ "
             << name << " ] : " << index-1 << " " << _NodeOutPorts[ index-1 ]->NodeName() << endl ;
#endif
      pP = _NodeOutPorts[ index-1 ] ;
    }
    else {
#if PortsOfNodeTrace
      cdebug << "GraphBase::PortsOfNode::GetChangeOutPort _MapOfNodeOutPorts[ "
             << name << " ] : " << index << " NOT FOUND. Known ports :" << endl ;
      map< string , int >::iterator aMapOfNodeOutPortsIterator ;
      int i = 0 ;
      for ( aMapOfNodeOutPortsIterator = _MapOfNodeOutPorts.begin() ;
            aMapOfNodeOutPortsIterator != _MapOfNodeOutPorts.end() ; aMapOfNodeOutPortsIterator++ ) {
        cdebug << "MapOfNodeInPorts" << i++ << " " << aMapOfNodeOutPortsIterator->first << " --> "
               << aMapOfNodeOutPortsIterator->second << endl ;
      }
#endif
    }
  }
#if PortsOfNodeTrace
  cdebug_out << "GraphBase::PortsOfNode::GetChangeOutPort " << endl;
#endif
  return pP;
}

void GraphBase::PortsOfNode::ListPorts( ostream & f,
                                        const bool klink ) const {

  int i ;

  f << "      Input Ports : " ;
  if ( klink ) {
    f << GetNodeInPortsSize() << " port" 
      << (GetNodeInPortsSize() > 1 ? "s" : "") << endl;

    for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
      f << "         Port "
//        << hex << GetNodeInPort(i) << dec << " "
        << *(GetNodeInPort(i)) << " " ;
      GetNodeInPort(i)->StringValue( f ) ;
      f << endl;
    }

    f << "      Output Ports : " ;
  }

  f << GetNodeOutPortsSize() << " port" 
    << (GetNodeOutPortsSize() > 1 ? "s" : "") << endl;

  for ( i = 0 ; i < GetNodeOutPortsSize() ; i++ ) {
    f << "         Port "
//      << hex << GetNodeOutPort(i) << dec << " "
      << *(GetNodeOutPort(i)) << " " ;
    GetNodeOutPort(i)->StringValue( f ) ;
    f << endl;
  }

  if ( !klink ) {
    f << "      Output Ports : " ;
    f << GetNodeInPortsSize() << " port" 
      << (GetNodeInPortsSize() > 1 ? "s" : "") << endl;

    for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
      f << "         Port "
//        << hex << GetNodeInPort(i) << dec << " "
        << *(GetNodeInPort(i)) << " " ;
      GetNodeInPort(i)->StringValue( f ) ;
      f << endl;
    }
  }

}

ostream & operator<< (ostream & f ,const SUPERV::KindOfNode & s ) {
  switch (s) {
  case SUPERV::ComputingNode :
    f << "ComputingNode";
    break;
  case SUPERV::FactoryNode :
    f << "FactoryNode";
    break;
  case SUPERV::InLineNode :
    f << "InLineNode";
    break;
  case SUPERV::MacroNode :
    f << "MacroNode";
    break;
  case SUPERV::DataFlowGraph :
    f << "DataFlowGraph";
    break;
  case SUPERV::DataStreamGraph :
    f << "DataStreamGraph";
    break;
  case SUPERV::LoopNode :
    f << "LoopNode";
    break;
  case SUPERV::EndLoopNode :
    f << "EndLoopNode";
    break;
  case SUPERV::SwitchNode :
    f << "SwitchNode";
    break;
  case SUPERV::EndSwitchNode :
    f << "EndSwitchNode";
    break;
  case SUPERV::GOTONode :
    f << "GOTONode";
    break;
  default :
    f << "UnknownKindOfNode";
    break;
  }

  return f;
}


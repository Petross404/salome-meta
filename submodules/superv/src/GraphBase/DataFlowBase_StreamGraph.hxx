//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_StreamGraph.hxx
//  Author : Jean Rahuel
//  Module : SUPERV

#ifndef _DATAFLOWBASE_STREAMGRAPH_HXX
#define _DATAFLOWBASE_STREAMGRAPH_HXX

#include "DataFlowBase_Graph.hxx"

namespace GraphBase {

  class StreamGraph : public Graph {

    private:
    
// Configuration :
// The name is the name of the graph
      long                          _Timeout ;
      SUPERV::KindOfDataStreamTrace _DataStreamTrace ;
      double                        _DeltaTime ;

// Total number of SubStreamGraphs
      int                           _SubStreamGraphsNumber ;

    protected:

    public:

      StreamGraph() ;

      StreamGraph( CORBA::ORB_ptr ORB ,
                   SALOME_NamingService* ptrNamingService ,
                   const char * DataFlowName ,
                   const SUPERV::KindOfNode DataFlowkind ,
                   int * Graph_prof_debug ,
                   ofstream * Graph_fdebug ) ;

      StreamGraph( CORBA::ORB_ptr ORB ,
                   SALOME_NamingService * ptrNamingService ,
                   const SALOME_ModuleCatalog::Service & DataFlowService ,
                   const char * DataFlowComponentName ,
                   const char * DataFlowInterfaceName ,
                   const char * DataFlowName ,
                   const SUPERV::KindOfNode DataFlowkind ,
                   const SUPERV::SDate DataFlowFirstCreation ,
                   const SUPERV::SDate DataFlowLastModification ,
                   const char * DataFlowEditorRelease ,
                   const char * DataFlowAuthor ,
                   const char * DataFlowComputer ,
                   const char * DataFlowComment ,
                   int * Graph_prof_debug ,
                   ofstream * Graph_fdebug ) ;

      ~StreamGraph() ;

      bool SetStreamParams( CORBA::Long Timeout ,
                            const SUPERV::KindOfDataStreamTrace DataStreamTrace ,
                            CORBA::Double  DeltaTime ) ;
      void StreamParams( CORBA::Long & Timeout ,
                         SUPERV::KindOfDataStreamTrace & DataStreamTrace ,
                         CORBA::Double & DeltaTime ) const ;

      bool CreateStreamTopology( const char * aDirectory ) ;

      void SubStreamGraphsNumber( int SubStreamGraphsNumber ) {
           _SubStreamGraphsNumber = SubStreamGraphsNumber ; } ;
      long SubStreamGraphsNumber() const {
           return _SubStreamGraphsNumber ; } ;

  };

};

ostream & operator<< (ostream &,const SUPERV::KindOfDataStreamTrace &);

#endif

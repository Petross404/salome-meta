//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_EndOfSwitchNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_EndOfSwitchNode.hxx"

GraphBase::EndOfSwitchNode::EndOfSwitchNode() :
  GraphBase::GOTONode::GOTONode() {

  cdebug << "GraphBase::EndOfSwitchNode::EndOfSwitchNode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;

}

GraphBase::EndOfSwitchNode::EndOfSwitchNode( CORBA::ORB_ptr ORB ,
                                   SALOME_NamingService* ptrNamingService ,
                                   const char * FuncName ,
                                   const SUPERV::ListOfStrings & aPythonFunction ,
                                   const char *NodeName ,
                                   const SUPERV::KindOfNode akind ,
                                   const SUPERV::SDate NodeFirstCreation ,
                                   const SUPERV::SDate NodeLastModification  ,
                                   const char * NodeEditorRelease ,
                                   const char * NodeAuthor ,
                                   const char * NodeComment ,
                                   const bool   GeneratedName ,
                                   const long   X ,
                                   const long   Y ,
                                   int * Graph_prof_debug ,
                                   ofstream * Graph_fdebug ) :
  GraphBase::GOTONode::GOTONode( ORB , ptrNamingService , FuncName ,
                                 aPythonFunction ,
                                 NodeName , akind , NodeFirstCreation ,
                                 NodeLastModification  , NodeEditorRelease ,
                                 NodeAuthor , NodeComment , GeneratedName ,
                                 X , Y , Graph_prof_debug , Graph_fdebug ) {

  cdebug_in << "GraphBase::EndOfSwitchNode::EndOfSwitchNode "  << this 
            << "' _Name " << (void *) Name() << " '" << Name() << " _Comment "
            << (void *) Comment() << " "  << Comment() << " " 
            << " KindOfNode " << Kind() << " FuncName " << FuncName
            << " ServiceName " << ServiceName() << " In("
            << ServiceInParameter().length()
            << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  cdebug_out << "GraphBase::EndOfSwitchNode::EndOfSwitchNode" << endl;
}

GraphBase::EndOfSwitchNode::~EndOfSwitchNode() {
  cdebug << "GraphBase::EndOfSwitchNode::~EndOfSwitchNode "  << this 
         << " _Name " << (void *) Name() << " " << Name() << " _Comment "
         << (void *) Comment() << " "  << Comment() << " "  << endl ;
//  if ( _ComponentName != NULLSTRING )
//    delete [] _ComponentName ;
//  delete [] _Name ;
//  delete [] _EditorRelease ;
//  if ( _Author != NULLSTRING )
//    delete [] _Author ;
//  if ( _Computer != FACTORYSERVER )
//    delete [] _Computer;
//  if ( _Comment != NULLSTRING )
//    delete [] _Comment;
}

void GraphBase::EndOfSwitchNode::InitEndSwitchInPortLinked() { // without StreamInPorts
  cdebug << "Base::EndOfSwitchNode::InitEndSwitchInPortLinked of " << Name() << endl ;
//JR 06.07.2005 : the InGatePort(Default) must also be counted (enhancement) ...
//           _EndSwitchInPortLinkedNumber = GetNodeInPortsSize() - DataStreamInPortsNumber() - 1 ;
  _EndSwitchInPortLinkedNumber = GetNodeInPortsSize() - DataStreamInPortsNumber() ;
  _SwitchWithGOTO = false ;
  int i ;
//JR 06.07.2005 : the InGatePort must also be counted (enhancement) ...
//  for ( i = 0 ; i < GetNodeInPortsSize()-1 ; i++ ) {
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GetChangeNodeInPort( i )->BranchOfSwitchLinked( false ) ;
  }
}

bool GraphBase::EndOfSwitchNode::DecrEndSwitchInPortLinked( GraphBase::InPort * anInPort ,
                                                            string & anErrorMessage ) {
  bool RetVal = true ;
//JR 16.02.2005 : except for DefaultPort
//JR 06.07.2005 : the InGatePort must also be counted now (enhancement) ...
//  if ( !anInPort->IsGate() ) {
    if ( !anInPort->BranchOfSwitchLinked( true ) ) {
      cdebug << Name() << "->DecrEndSwitchInPortLinked ERROR " << anInPort->NodeName() << "( "
             << anInPort->PortName() << " ) was already linked in that branch." << endl ;
      anErrorMessage = anErrorMessage + string( Name() ) + string( " The port " ) +
                       string( anInPort->NodeName() ) + string( "( " ) +
                       string( anInPort->PortName() ) +
                       string( " ) was already linked in that branch.\n" ) ;
      RetVal = false ;
    }
    _EndSwitchInPortLinkedNumber -= 1 ;
//  }
  cdebug << Name()
         << "->EndOfSwitchNode::DecrEndSwitchInPortLinked EndSwitchInPortLinkedNumber "
         << _EndSwitchInPortLinkedNumber << " InPort '" << anInPort->PortName() << "' of "
         << anInPort->NodeName() << " RetVal " << RetVal << endl ;
  return RetVal ;
}

bool GraphBase::EndOfSwitchNode::CheckEndSwitchInPortsLinked( string & anErrorMessage ) {
  bool RetVal = true ;
  bool DefaultNotLinked = false ;
// Check that all inports of the EndSwitchNode are reachable in that branch(es)
  int i ;
//JR 06.07.2005 : the InGatePort must also be counted now (enhancement) ...
//  for ( i = 0 ; i < GetNodeInPortsSize()-1 ; i++ ) {
  for ( i = 0 ; i < GetNodeInPortsSize() ; i++ ) {
    GraphBase::InPort * anInPort = GetChangeNodeInPort( i ) ;
    if ( !anInPort->IsDataStream() && !anInPort->BranchOfSwitchLinked() ) {
      cdebug << Name() << "->EndOfSwitchNode::CheckEndSwitchInPortsLinked "
             << anInPort->PortName() << " have no link in that branch"
             << " SwitchWithGOTO() " << SwitchWithGOTO() << endl ;
      if ( !SwitchWithGOTO() ) {
        anErrorMessage = anErrorMessage + string( Name() ) + string( "( " ) +
                         string( anInPort->PortName() ) +
                         string( " ) has no link from that branch.\n" ) ;
        if ( anInPort->IsGate() ) {
          DefaultNotLinked = true ;
	}
        else {
          RetVal = false ; // that InPort is not linked
	}
      }
    }
  }
// Check that all inports of the EndSwitchNode will have a value only once
  if ( _EndSwitchInPortLinkedNumber != 0 ) {
    cdebug << Name()
           << "->EndOfSwitchNode::CheckEndSwitchInPortsLinked EndSwitchInPortLinkedNumber "
           << _EndSwitchInPortLinkedNumber << " SwitchWithGOTO() " << SwitchWithGOTO()
           << " DefaultNotLinked " << DefaultNotLinked << endl ;
    if ( !SwitchWithGOTO() ) {
      if ( RetVal && _EndSwitchInPortLinkedNumber == 1 && DefaultNotLinked ) {
      }
      else {
        if ( _EndSwitchInPortLinkedNumber > 0 ) {
          anErrorMessage = anErrorMessage + string( Name() ) +
                           string( " has some InPort(s) not linked.\n" ) ;
        }
        else {
          anErrorMessage = anErrorMessage + string( Name() ) +
                           string( " has some InPort(s) multiply linked.\n" ) ;
        }
        RetVal = false ; // Some InPort is not linked or multiply linked
      }
    }
  }
  return RetVal ;
}

//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_DataNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_DataNode.hxx"
#include "DataFlowBase_Graph.hxx"

GraphBase::DataNode::DataNode() :
           GOTONode() {
  MESSAGE( "GraphBase::DataNode::DataNode" );
  _Created = false ;
}

GraphBase::DataNode::DataNode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService* ptrNamingService ,
                               const char *DataFlowName ,
                               const SUPERV::KindOfNode DataFlowkind ,
                               int * Graph_prof_debug ,
                               ofstream * Graph_fdebug ) :
           GOTONode( ORB , ptrNamingService , DataFlowName , DataFlowkind , Graph_prof_debug , Graph_fdebug ) {
//  MESSAGE( "GraphBase::DataNode::DataNode" );
  cdebug << "GraphBase::DataNode::DataNode" << endl ;
  _Created = false ;
}

GraphBase::DataNode::DataNode( CORBA::ORB_ptr ORB ,
                               SALOME_NamingService* ptrNamingService ,
                               const SALOME_ModuleCatalog::Service& DataFlowService ,
                               const char *DataFlowName ,
                               const SUPERV::KindOfNode DataFlowkind ,
                               const SUPERV::SDate DataFlowFirstCreation ,
                               const SUPERV::SDate DataFlowLastModification ,
                               const char * DataFlowEditorRelease ,
                               const char * DataFlowAuthor ,
                               const char * DataFlowComment ) :
           GOTONode( ORB , ptrNamingService ,
                     DataFlowName , SUPERV::ListOfStrings() , DataFlowName , DataFlowkind ,
                     DataFlowFirstCreation , DataFlowLastModification  ,
                     DataFlowEditorRelease , DataFlowAuthor ,
                     DataFlowComment , false , 0 , 0) {
//  MESSAGE( "GraphBase::DataNode::DataNode" );
}

GraphBase::DataNode::~DataNode() {
//  MESSAGE( "GraphBase::DataNode::~DataNode" );
  cdebug << "GraphBase::DataNode::~DataNode" << endl ;
}

void GraphBase::DataNode::DataService( CORBA::ORB_ptr ORB ,
                                       SALOME_ModuleCatalog::Service aService ,
                                       int * Graph_prof_debug ,
                                       ofstream * Graph_fdebug ) {
  cdebug << "GraphBase::DataNode::DataService : DataFlowPortsOfNode of "
         << aService << endl ;
  DefPortsOfNode( ORB , aService , NamePtr() , Kind() ,
                  Graph_prof_debug , Graph_fdebug ) ;

  if ( _Created ) {
    delete _DataFlowDataPorts ;
  }
  SALOME_ModuleCatalog::Service aReversedService ;
  aReversedService.ServiceName = aService.ServiceName ;
  aReversedService.ServiceinParameter = aService.ServiceoutParameter ;
  aReversedService.ServiceoutParameter = aService.ServiceinParameter ;
  _DataFlowDataPorts = new PortsOfNode() ;
  cdebug << "GraphBase::DataNode::DataService : Mirrored DataFlowPortsOfNode of "
         << aReversedService << endl ;
  _DataFlowDataPorts->DefPortsOfNode( ORB , aReversedService , NamePtr() , Kind() ,
                                      Graph_prof_debug , Graph_fdebug ) ;
  _Created = true ;
}

int GraphBase::DataNode::CheckDataServerNodes() const {

  cdebug_in << "GraphBase::DataNode::CheckDataServerNodes :" << endl;

  int i , ierr = 0 ;

  for ( i = 0 ; i < _DataFlowDataPorts->GetNodeOutPortsSize() ; i++ ) {
// Not a BUG : Reversed Service of the graph : OutPort of the Graph give input values to nodes
    const GraphBase::OutPort * anOutPort = _DataFlowDataPorts->GetNodeOutPort(i) ;
//    if ( !anOutPort->IsGate() && !anOutPort->IsDataStream() && !anOutPort->IsDataConnected() ) {
    if ( !anOutPort->IsGate() && !anOutPort->IsDataConnected() ) {
      if ( anOutPort->IsExternConnected() && GraphMacroLevel() != 0 ) {
        cdebug << "Base::DataNode::CheckDataServerNodes InPort " << anOutPort->PortName() << " "
               << anOutPort->PortType() << " "
               << anOutPort->PortStatus() << " of DataFlow " << Name()
               << " is ExternConnected. GraphMacroLevel " << GraphMacroLevel() << endl ;
      }
      else {
        cdebug << "Base::DataNode::CheckDataServerNodes InPort " << anOutPort->PortName() << " "
               << anOutPort->PortType() << " " << anOutPort->PortStatus() << " of DataFlow " << Name()
               << " has NO Data. GraphMacroLevel " << GraphMacroLevel() << " InPortsSize "
               << anOutPort->InPortsSize() << " corresponding to :"
               << endl ;
        int i ;
        for ( i = 0 ; i < anOutPort->InPortsSize() ; i++ ) {
          cdebug << "           " << anOutPort->InPorts( i )->NodeName() << "( "
                 << anOutPort->InPorts( i )->PortName() << " ) " << endl ;
  	}
        if ( anOutPort->InPortsSize() == 1 &&
             GraphOfNode()->GetGraphNode( anOutPort->InPorts( 0 )->NodeName() )->IsEndSwitchNode() ) {
          cdebug << "Base::DataNode::CheckDataServerNodes InPort " << anOutPort->PortName()
                 << " is connected to an EndSwitchNode. Input Data is controlled by ValidSwitchs."
                 << endl ;
	}
        else {
          ostringstream astr ;
          astr << anOutPort->PortStatus() ;
          string anErrorMessage = string( "InPort " ) + string( anOutPort->NodeName() ) +
                                  string( "( " ) + string( anOutPort->PortName() ) + string( " ) " ) +
                                  astr.str() + string( " has NO Input Data.\n" ) ;
          GraphOfNode()->SetMessages( anErrorMessage ) ;
          ierr++ ;
	}
      }
    }
    else {
      cdebug << "Base::DataNode::CheckDataServerNodes InPort " << anOutPort->PortName() << " "
             << anOutPort->PortType() << " "
             << anOutPort->PortStatus() << " of DataFlow " << Name()  << " has Data or IsGate"
             << endl ;
    }
  }

  for ( i = 0 ; i < _DataFlowDataPorts->GetNodeInPortsSize() ; i++ ) {
    if ( _DataFlowDataPorts->GetNodeInPort(i)->IsPortConnected() ) {
      cdebug << "Base::DataNode::CheckDataServerNodes OutPort "
             << _DataFlowDataPorts->GetNodeInPort(i)->PortName()
             << " of DataFlow " << Name()  << " is Connected" << endl ;
    }
    else {
      cdebug << "Base::DataNode::CheckDataServerNodes OutPort "
             << _DataFlowDataPorts->GetNodeInPort(i)->PortName()
             << " of DataFlow " << Name()  << " is NOT Connected" << endl ;
    }
  }

  if ( ierr ) {
    GraphOfNode()->SetMessages( "Some Input Data are missing.\n" ) ;
    cdebug << "Base::DataNode::CheckDataServerNodes In the DataFlow " << Name() << " " << ierr
           << " Some Input Data are missing" << endl ;
  }

  cdebug_out << "GraphBase::DataNode::CheckDataServerNodes ierr " << ierr << endl;
  return ierr ;
}

void GraphBase::DataNode::ListDatas(ostream & f ) const {
  _DataFlowDataPorts->ListPorts( f , false ) ;
}

void GraphBase::DataNode::DataNodeInfo(ostream & s ) const {
  s << *this ;
  s << "Ports :" << endl ;
  ListPorts( s , true ) ;
  s << "Datas :" << endl ;
  _DataFlowDataPorts->ListPorts( s , false ) ;
  s << ends ;
}

// inlined :
//GraphBase::OutPort * GraphBase::DataNode::GetChangeInDataNodePort( const char * DataFlowInPortName ) {
//  return _DataFlowDataPorts->GetChangeOutPort( DataFlowInPortName ) ;
//}

//GraphBase::InPort * GraphBase::DataNode::GetChangeOutDataNodePort( const char * DataFlowOutPortName ) {
//  return _DataFlowDataPorts->GetChangeInPort( DataFlowOutPortName ) ;
//}


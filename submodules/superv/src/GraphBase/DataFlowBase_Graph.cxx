//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_Graph.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV
//  $Header:

using namespace std;
#include "DataFlowBase_StreamGraph.hxx"

#include "SALOME_LifeCycleCORBA.hxx"

GraphBase::Graph::Graph() :
           DataNode() {
  cdebug << "GraphBase::Graph::Graph" << endl ;
  _GraphNodesSize = 0 ;
  GraphOfNode( this ) ;
}

GraphBase::Graph::Graph( CORBA::ORB_ptr ORB ,
                         SALOME_NamingService* ptrNamingService ,
                         const char *DataFlowName ,
                         const SUPERV::KindOfNode DataFlowkind ,
                         int * Graph_prof_debug ,
                         ofstream * Graph_fdebug ) :
//                         const char * DebugFileName ) :
           DataNode( ORB ,ptrNamingService , DataFlowName , DataFlowkind , Graph_prof_debug , Graph_fdebug ) {
  _Orb = CORBA::ORB::_duplicate( ORB ) ;
  Set_prof_debug( Graph_prof_debug , Graph_fdebug ) ;
  cdebug << "GraphBase::Graph::Graph( " << DataFlowName << ") GraphNodesSize() " << GraphNodesSize() << endl ;
  _GraphNodesSize = 0 ;
  GraphOfNode( this ) ;
}

GraphBase::Graph::Graph( CORBA::ORB_ptr ORB ,
                         SALOME_NamingService* ptrNamingService ,
                         const SALOME_ModuleCatalog::Service& DataFlowService ,
                         const char *DataFlowComponentName ,
                         const char *DataFlowInterfaceName ,
                         const char *DataFlowName ,
                         const SUPERV::KindOfNode DataFlowkind ,
                         const SUPERV::SDate DataFlowFirstCreation ,
                         const SUPERV::SDate DataFlowLastModification ,
                         const char * DataFlowEditorRelease ,
                         const char * DataFlowAuthor ,
                         const char * DataFlowComputer ,
                         const char * DataFlowComment ,
                         int * Graph_prof_debug ,
                         ofstream * Graph_fdebug ) :
//                         const char * DebugFileName ) :
           DataNode( ORB , ptrNamingService , DataFlowService ,
                     DataFlowName , DataFlowkind ,
                     DataFlowFirstCreation , DataFlowLastModification  ,
                     DataFlowEditorRelease , DataFlowAuthor , DataFlowComment ) {
  _Orb = CORBA::ORB::_duplicate( ORB ) ;
  _GraphNodesSize = 0 ;
  GraphOfNode( this ) ;
  Set_prof_debug( Graph_prof_debug , Graph_fdebug ) ;
  cdebug_in << "GraphBase::Graph::Graph" << endl ;
  DataService( ORB , DataFlowService , Graph_prof_debug , Graph_fdebug ) ;
  cdebug_out << "GraphBase::Graph::Graph" << endl ;
}

GraphBase::Graph::~Graph() {
  cdebug << "GraphBase::Graph::~Graph" << endl ;
}

void GraphBase::Graph::Set_prof_debug( int * Graph_prof_debug ,
                                       ofstream * Graph_fdebug ) {
  SetDebug( _Orb , Graph_prof_debug , Graph_fdebug ) ;
  _Graph_prof_debug = Graph_prof_debug ;
  _Graph_fdebug = Graph_fdebug ;
}

GraphBase::SNode * GraphBase::Graph::GetInfo() const {
  GraphBase::SNode * Info = new GraphBase::SNode ;
  Info->theComponentName = "" ;
  Info->theInterfaceName = "" ;
  Info->theName = Name() ;
  Info->theKind = Kind() ;
  if ( IsDataStreamNode() ) {
    CORBA::Long Timeout ;
    SUPERV::KindOfDataStreamTrace DataStreamTrace ;
    CORBA::Double DeltaTime ;
    ((GraphBase::StreamGraph * ) this)->StreamParams( Timeout , DataStreamTrace , DeltaTime ) ;
    Info->theTimeout = Timeout ;
    Info->theDataStreamTrace = DataStreamTrace ;
    Info->theDeltaTime = DeltaTime ;
  }
  Info->theService = *GetService() ;
  Info->theFirstCreation = FirstCreation() ;
  Info->theLastModification = LastModification() ;
  Info->theEditorRelease = EditorRelease() ;
  Info->theAuthor = Author() ;
  Info->theContainer = "" ;
  Info->theComment = Comment() ;
  Info->theCoords.theX = GraphBase::ComputingNode::XCoordinate() ;
  Info->theCoords.theY = GraphBase::ComputingNode::YCoordinate() ;
  return Info ;
}

//----------------------------------------------------------------------
// Function : GetNodes
// Purpose  : get a nodes list
//----------------------------------------------------------------------
GraphBase::ListOfSNodes * GraphBase::Graph::GetNodes() const {
  GraphBase::ListOfSNodes * _list_nodes = new GraphBase::ListOfSNodes;

  cdebug_in << "GraphBase::Graph::GetNodes" << endl ;
  // All the nodes from _InNodes are taken
//  vector< InNode *> Nodes = InNodes() ;

  _list_nodes->resize( GraphNodesSize() );

  int igoto ;
  int ind ;
  GraphBase::ComputingNode * aCNode = NULL ;
  for ( igoto = 0 ; igoto < 2 ; igoto++ ) {
    for ( ind = 0 ; ind < GraphNodesSize() ; ind++ ) {
      aCNode = GraphNodes( ind ) ;
      if ( ( igoto == 0 && !aCNode->IsGOTONode() ) ||
           ( igoto == 1 && aCNode->IsGOTONode() ) ) {
        if ( aCNode->IsFactoryNode() ) {
          GraphBase::FactoryNode * aFNode = (GraphBase::FactoryNode * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theComponentName = CORBA::string_dup( aFNode->ComponentName());
          (*_list_nodes)[ind].theComponentName = string( aFNode->ComponentName());
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theInterfaceName = CORBA::string_dup( aFNode->InterfaceName());
          (*_list_nodes)[ind].theInterfaceName = string( aFNode->InterfaceName());
        }
        else {
          (*_list_nodes)[ind].theComponentName = string( "" );
          (*_list_nodes)[ind].theInterfaceName = string( "" );
        }
//JR 24.02.2005 Memory Leak        (*_list_nodes)[ind].theName = CORBA::string_dup( aCNode->Name() );
        (*_list_nodes)[ind].theName = string( aCNode->Name() );
        (*_list_nodes)[ind].theKind = aCNode->Kind();
        if ( aCNode->IsInLineNode() || aCNode->IsMacroNode() ) {
          GraphBase::InLineNode * aINode = (GraphBase::InLineNode * ) aCNode ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 1 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aINode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aINode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aINode->PythonFunction() ;
        }
        if ( aCNode->IsGOTONode() ) {
          GraphBase::GOTONode * aGNode = (GraphBase::GOTONode * ) aCNode ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 1 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aGNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aGNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aGNode->PythonFunction() ;
        }
        if ( aCNode->IsLoopNode() ) {
          GraphBase::LoopNode * aLNode = (GraphBase::LoopNode * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theCoupledNode = CORBA::string_dup( aLNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theCoupledNode = string( aLNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 3 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aLNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aLNode->PyFuncName() ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[1] = CORBA::string_dup( aLNode->PyMoreName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[1] = string( aLNode->PyMoreName() ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[2] = CORBA::string_dup( aLNode->PyNextName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[2] = string( aLNode->PyNextName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 3 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aLNode->PythonFunction() ;
          (*_list_nodes)[ind].theListOfPythonFunctions[1] = aLNode->MorePythonFunction() ;
          (*_list_nodes)[ind].theListOfPythonFunctions[2] = aLNode->NextPythonFunction() ;
        }
        else if ( aCNode->IsSwitchNode() ) {
          GraphBase::SwitchNode * aSNode = (GraphBase::SwitchNode * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theCoupledNode = CORBA::string_dup( aSNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theCoupledNode = string( aSNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 1 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aSNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aSNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aSNode->PythonFunction() ;
        }
        else if ( aCNode->IsEndLoopNode() ) {
          GraphBase::EndOfLoopNode * aELNode = (GraphBase::EndOfLoopNode * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theCoupledNode = CORBA::string_dup( aELNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theCoupledNode = string( aELNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 1 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aELNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aELNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aELNode->PythonFunction() ;
        }
        else if ( aCNode->IsEndSwitchNode() ) {
          GraphBase::EndOfSwitchNode * aESNode = (GraphBase::EndOfSwitchNode * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theCoupledNode = CORBA::string_dup( aESNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theCoupledNode = string( aESNode->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 1 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aESNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aESNode->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aESNode->PythonFunction() ;
        }
        else if ( aCNode->IsMacroNode() ) {
          GraphBase::Graph * aGraph = (GraphBase::Graph * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theCoupledNode = CORBA::string_dup( aGraph->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theCoupledNode = string( aGraph->CoupledNode()->Name() ) ;
          (*_list_nodes)[ind].theListOfFuncName.resize( 1 ) ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theListOfFuncName[0] = CORBA::string_dup( aGraph->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfFuncName[0] = string( aGraph->PyFuncName() ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
          (*_list_nodes)[ind].theListOfPythonFunctions[0] = aGraph->PythonFunction() ;
        }
        else {
          (*_list_nodes)[ind].theCoupledNode = string( "" ) ;
        }
//        (*_list_nodes)[ind].theDataStreamInArgsNumber = aCNode->DataStreamInPortsNumber() ;
//        (*_list_nodes)[ind].theDataStreamOutArgsNumber = aCNode->DataStreamOutPortsNumber() ;

        (*_list_nodes)[ind].theListOfInDataStreams.resize( aCNode->DataStreamInPortsNumber() ) ;
        cdebug << "GraphBase::Graph::GetNodes " << aCNode->Name() << " " << aCNode->DataStreamInPortsNumber() << " DataStreamInPortsNumber" << endl ;
        int iostream ;
        int streamind = 0 ;
        for ( iostream = 0 ; iostream < aCNode->GetNodeInPortsSize() ; iostream++ ) {
          const GraphBase::InPort * anInPort ;
          anInPort = aCNode->GetNodeInPort( iostream ) ;
          if ( anInPort->IsDataStream() ) {
            cdebug << "GraphBase::Graph::GetNodes In" << iostream << " " << aCNode->Name()
                   << " " << anInPort->NodeName() << " " << anInPort->PortName() << endl;
            (*_list_nodes)[ind].theListOfInDataStreams[streamind].theDataStreamParameter.Parametertype = CORBA::string_dup( anInPort->PortType() ) ;
            (*_list_nodes)[ind].theListOfInDataStreams[streamind].theDataStreamParameter.Parametername = CORBA::string_dup( anInPort->PortName() ) ;
            (*_list_nodes)[ind].theListOfInDataStreams[streamind].theDataStreamParameter.Parameterdependency = anInPort->Dependency() ;
            SUPERV::KindOfSchema        aKindOfSchema ;
            SUPERV::KindOfInterpolation aKindOfInterpolation ;
            SUPERV::KindOfExtrapolation aKindOfExtrapolation ;
            ((GraphBase::InDataStreamPort * ) anInPort)->Params( aKindOfSchema , aKindOfInterpolation , aKindOfExtrapolation ) ;
            (*_list_nodes)[ind].theListOfInDataStreams[streamind].theKindOfSchema = aKindOfSchema ;
            (*_list_nodes)[ind].theListOfInDataStreams[streamind].theKindOfInterpolation = aKindOfInterpolation ;
            (*_list_nodes)[ind].theListOfInDataStreams[streamind].theKindOfExtrapolation = aKindOfExtrapolation ;
            cdebug << "GraphBase::Graph::GetNodes " << aCNode->Name() << " " << anInPort->NodeName() << " " << anInPort->PortName() << " "
                   <<  anInPort->Dependency() << " " << aKindOfSchema << " " << aKindOfInterpolation << " " << aKindOfExtrapolation << " list_nodes "
                   << (*_list_nodes)[ind].theListOfInDataStreams[streamind].theDataStreamParameter.Parametername << " "
                   << (*_list_nodes)[ind].theListOfInDataStreams[streamind].theDataStreamParameter.Parametertype << " "
                   << (*_list_nodes)[ind].theListOfInDataStreams[streamind].theDataStreamParameter.Parameterdependency << " "
                   << (*_list_nodes)[ind].theListOfInDataStreams[streamind].theKindOfSchema << " "
                   << (*_list_nodes)[ind].theListOfInDataStreams[streamind].theKindOfInterpolation << " "
                   << (*_list_nodes)[ind].theListOfInDataStreams[streamind].theKindOfExtrapolation << " " << endl ;
            streamind += 1 ;
	  }
          else {
            cdebug << "GraphBase::Graph::GetNodes " << aCNode->Name() << " " << anInPort->NodeName() << " " << anInPort->PortName() << " "
                   << anInPort->Kind() << " IsDataStream " << anInPort->IsDataStream() << endl ;
	  }
	}

        (*_list_nodes)[ind].theListOfOutDataStreams.resize( aCNode->DataStreamOutPortsNumber() ) ;
        cdebug << "GraphBase::Graph::GetNodes " << aCNode->Name() << " " << aCNode->DataStreamOutPortsNumber() << " DataStreamOutPortsNumber" << endl ;
        streamind = 0 ;
        for ( iostream = 0 ; iostream < aCNode->GetNodeOutPortsSize() ; iostream++ ) {
          const GraphBase::OutPort * anOutPort ;
          anOutPort = aCNode->GetNodeOutPort( iostream ) ;
          if ( anOutPort->IsDataStream() ) {
            cdebug << "GraphBase::Graph::GetNodes Out" << iostream << " " << aCNode->Name() << " " << anOutPort->NodeName() << " " << anOutPort->PortName() << " "
                   << endl ;
            (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theDataStreamParameter.Parametertype = CORBA::string_dup( anOutPort->PortType() ) ;
            (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theDataStreamParameter.Parametername = CORBA::string_dup( anOutPort->PortName() ) ;
            (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theDataStreamParameter.Parameterdependency = anOutPort->Dependency() ;
            long aNumberOfValues = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
            (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theNumberOfValues = aNumberOfValues ;
            cdebug << "GraphBase::Graph::GetNodes " << aCNode->Name() << " " << anOutPort->NodeName() << " " << anOutPort->PortName() << " "
                   <<  anOutPort->Dependency() << " " << aNumberOfValues << " list_nodes "
                   << (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theDataStreamParameter.Parametername << " "
                   << (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theDataStreamParameter.Parametertype << " "
                   << (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theDataStreamParameter.Parameterdependency << " "
                   << (*_list_nodes)[ind].theListOfOutDataStreams[streamind].theNumberOfValues << endl ;
            streamind += 1 ;
	  }
          else {
            cdebug << "GraphBase::Graph::GetNodes " << aCNode->Name() << " " << anOutPort->NodeName() << " " << anOutPort->PortName() << " "
                   << anOutPort->Kind() << " IsDataStream " << anOutPort->IsDataStream() << endl ;
	  }
	}

        (*_list_nodes)[ind].theService = *aCNode->GetService();
        cdebug << "GraphBase::Graph::GetNodes theService " << &(*_list_nodes)[ind].theService.ServiceName << endl ;
        unsigned int i ;
        for ( i = 0 ; i < (*_list_nodes)[ind].theService.ServiceinParameter.length() ; i++ ) {
          cdebug << "     In" << i << " " << &(*_list_nodes)[ind].theService.ServiceinParameter[i].Parametername
                 << " " << (*_list_nodes)[ind].theService.ServiceinParameter[i].Parametername
                 << " " << &(*_list_nodes)[ind].theService.ServiceinParameter[i].Parametertype
                 << " " << (*_list_nodes)[ind].theService.ServiceinParameter[i].Parametertype << endl ;
	}
        for ( i = 0 ; i < (*_list_nodes)[ind].theService.ServiceoutParameter.length() ; i++ ) {
          cdebug << "     Out" << i << " " << &(*_list_nodes)[ind].theService.ServiceoutParameter[i].Parametername
                 << " " << (*_list_nodes)[ind].theService.ServiceoutParameter[i].Parametername
                 << " " << &(*_list_nodes)[ind].theService.ServiceoutParameter[i].Parametertype
                 << " " << (*_list_nodes)[ind].theService.ServiceoutParameter[i].Parametertype << endl ;
	}
        for ( i = 0 ; i < (*_list_nodes)[ind].theService.ServiceinDataStreamParameter.length() ; i++ ) {
          cdebug << "     InDataStream" << i << " " << &(*_list_nodes)[ind].theService.ServiceinDataStreamParameter[i].Parametername
                 << " " << (*_list_nodes)[ind].theService.ServiceinDataStreamParameter[i].Parametername
                 << " " << &(*_list_nodes)[ind].theService.ServiceinDataStreamParameter[i].Parametertype
                 << " " << (*_list_nodes)[ind].theService.ServiceinDataStreamParameter[i].Parametertype << endl ;
	}
        for ( i = 0 ; i < (*_list_nodes)[ind].theService.ServiceoutDataStreamParameter.length() ; i++ ) {
          cdebug << "     OutDataStream" << i << " " << &(*_list_nodes)[ind].theService.ServiceoutDataStreamParameter[i].Parametername
                 << " " << (*_list_nodes)[ind].theService.ServiceoutDataStreamParameter[i].Parametername
                 << " " << &(*_list_nodes)[ind].theService.ServiceoutDataStreamParameter[i].Parametertype
                 << " " << (*_list_nodes)[ind].theService.ServiceoutDataStreamParameter[i].Parametertype << endl ;
	}
        if ( aCNode->IsOneOfInLineNodes() ) {
          GraphBase::InLineNode * aINode = (GraphBase::InLineNode * ) aCNode ;
          GraphBase::LoopNode * aLNode = NULL ;
          if ( aCNode->IsLoopNode() ) {
            aLNode = (GraphBase::LoopNode * ) aCNode ;
            (*_list_nodes)[ind].theListOfPythonFunctions.resize( 3 ) ;
            (*_list_nodes)[ind].theListOfPythonFunctions[ 0 ] = aINode->PythonFunction() ;
            (*_list_nodes)[ind].theListOfPythonFunctions[ 1 ] = aLNode->MorePythonFunction() ;
            (*_list_nodes)[ind].theListOfPythonFunctions[ 2 ] = aLNode->NextPythonFunction() ;
          }
          else if ( aCNode->IsInLineNode() || aCNode->IsGOTONode() ||
                    aCNode->IsSwitchNode() || aCNode->IsEndSwitchNode() ) {
            (*_list_nodes)[ind].theListOfPythonFunctions.resize( 1 ) ;
            (*_list_nodes)[ind].theListOfPythonFunctions[ 0 ] = aINode->PythonFunction() ;
          }
        }
        (*_list_nodes)[ind].theFirstCreation = aCNode->FirstCreation();
        (*_list_nodes)[ind].theLastModification = aCNode->LastModification();
//JR 24.02.2005 Memory Leak        (*_list_nodes)[ind].theEditorRelease = CORBA::string_dup( aCNode->EditorRelease());
        (*_list_nodes)[ind].theEditorRelease = string( aCNode->EditorRelease());
//JR 24.02.2005 Memory Leak        (*_list_nodes)[ind].theAuthor = CORBA::string_dup( aCNode->Author());
	(*_list_nodes)[ind].theAuthor = string( aCNode->Author()); // mkr : IPAL10200
        if ( aCNode->IsFactoryNode() ) {
          GraphBase::FactoryNode * aFNode = (GraphBase::FactoryNode * ) aCNode ;
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theContainer = CORBA::string_dup( aFNode->Computer());
          (*_list_nodes)[ind].theContainer = string( aFNode->Computer());
        }
        else {
//JR 24.02.2005 Memory Leak          (*_list_nodes)[ind].theContainer = CORBA::string_dup( "" );
          (*_list_nodes)[ind].theContainer = string( "" );
        }
//JR 24.02.2005 Memory Leak        (*_list_nodes)[ind].theComment = CORBA::string_dup( aCNode->Comment());
        (*_list_nodes)[ind].theComment = string( aCNode->Comment());
        (*_list_nodes)[ind].theCoords.theX = aCNode->XCoordinate();
        (*_list_nodes)[ind].theCoords.theY = aCNode->YCoordinate();
#if 0
        int nports = 0 ;
        int i ;
        for ( i = 0 ; i < aCNode->GetNodeInPortsSize() ; i++ ) {
          const GraphBase::InPort * anInPort = aCNode->GetNodeInPort( i ) ;
          if ( anInPort->IsBus() ) {
            nports += 1 ;
            (*_list_nodes)[ind].theListOfParameters.resize( nports ) ;
//JR 24.02.2005 Memory Leak            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theInParameter.Parametertype = CORBA::string_dup( anInPort->PortType() ) ;
            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theInParameter.Parametertype = string( anInPort->PortType() ) ;
//JR 24.02.2005 Memory Leak            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theInParameter.Parametername = CORBA::string_dup( anInPort->PortName() ) ;
            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theInParameter.Parametername = string( anInPort->PortName() ) ;
//JR 24.02.2005 Memory Leak            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theOutParameter.Parametertype = CORBA::string_dup( aCNode->GetNodeOutPort( anInPort->PortIndex() )->PortType() ) ;
            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theOutParameter.Parametertype = string( aCNode->GetNodeOutPort( anInPort->PortIndex() )->PortType() ) ;
//JR 24.02.2005 Memory Leak            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theOutParameter.Parametername = CORBA::string_dup( aCNode->GetNodeOutPort( anInPort->PortIndex() )->PortName() ) ;
            (*_list_nodes)[ind].theListOfParameters[ nports-1 ].theOutParameter.Parametername = string( aCNode->GetNodeOutPort( anInPort->PortIndex() )->PortName() ) ;
          }
        }
#endif
      }
    }
  }
  cdebug_out << "GraphBase::Graph::GetNodes" << endl ;
  return _list_nodes ;
}


//----------------------------------------------------------------------
// Function : GetLinks
// Purpose  : get a links list
//----------------------------------------------------------------------
GraphBase::ListOfSLinks * GraphBase::Graph::GetLinks(bool AllLinks ) const {
  GraphBase::ListOfSLinks * _list_links = new GraphBase::ListOfSLinks;

// All the links from _LinksList are taken
//  vector< InNode *> Nodes = InNodes() ;

  cdebug_in << "Graph::GetLinks( " << AllLinks << " ) " << endl ;
  int ind = 0 ;
  int k ;
  for ( k = 0 ; k < GraphNodesSize() ; k++ ) {
    GraphBase::ComputingNode * fromNode = GraphNodes( k ) ;
//    cdebug << "GraphBase::Graph::GetLinks k" << k << ". :" << fromNode->Name() << endl ;
    int i ;
    for ( i = 0 ; i < fromNode->GetNodeOutPortsSize() ; i++ ) {
      const GraphBase::OutPort* fromPort = fromNode->GetNodeOutPort( i ) ;
//      cdebug << "GraphBase::Graph::GetLinks k" << k << ". :" << fromNode->Name()
//             << " i" << i << ". : " << fromPort->PortName() << endl ;
      int j ;
      for ( j = 0 ; j < fromPort->InPortsSize() ; j++ ) {
        const GraphBase::InPort* toPort = fromPort->InPorts( j ) ;
//        cdebug << "GraphBase::Graph::GetLinks k" << k << ". : fromNode " << fromNode << " = "
//               << fromNode->Name() << " i" << i << ". : fromPort " << fromPort << " = "
//               << fromPort->PortName() << " j" << j << ". : toPort " << toPort << " = "
//               << toPort->NodeName() << "( " << toPort->PortName() << " )" << endl ;
        if ( toPort->IsPortConnected() ) {
// Desole pour ce cast mais avec les maps difficile de faire const ...
//          const GraphBase::ComputingNode * toNode = ((GraphBase::Graph * ) this)->GetGraphNode( toPort->NodeName() ) ;
//          cdebug << "GraphBase::Graph::GetLinks " << fromNode->Name() << " ("
//                 << fromPort->PortName() << " ) IsPortConnected" << fromPort->IsPortConnected()
//                 << " --> toNode " << toNode << " " << toPort->NodeName() << "( "
//                 << toPort->PortName() << " ) " << " " << fromPort->InPortsSize() << " ports"
//                 << endl ;
          if ( !(fromPort->IsLoop() && toPort->IsLoop() ) || AllLinks ) {
//            cdebug << "GraphBase::Graph::GetLinks " << fromNode->Name() << " ("
//                   << fromPort->PortName() << " ) IsPortConnected" << fromPort->IsPortConnected()
//                   << " --> " << toNode->Name() << "( " << toPort->PortName() << " ) "
//                   << " " << fromPort->InPortsSize() << " ports" << endl ;
            _list_links->resize( ind+1 );
//JR 23.02.2005 Memory Leak            (*_list_links)[ind].FromNodeName = CORBA::string_dup( fromNode->Name() );
            (*_list_links)[ind].FromNodeName = string( fromNode->Name() );
            (*_list_links)[ind].FromServiceParameterName = fromPort->GetServicesParameter().Parametername;
//JR 23.02.2005 Memory Leak            (*_list_links)[ind].ToNodeName = CORBA::string_dup( toPort->NodeName() );
            (*_list_links)[ind].ToNodeName = string( toPort->NodeName() );
            (*_list_links)[ind].ToServiceParameterName = toPort->GetServicesParameter().Parametername;
//JR 30.03.2005            (*_list_links)[ind].aLinkValue = *fromPort->Value() ;
            (*_list_links)[ind].aLinkValue = fromPort->Value() ;
            if ( toPort->IsEndSwitch() ) {
              (*_list_links)[ind++].aListOfCoords = *(fromPort->Coords()) ;
	    }
            else {
              (*_list_links)[ind++].aListOfCoords = *(toPort->Coords()) ;
	    }
	  }
  	}
        else {
//          cdebug << "GraphBase::Graph::GetLinks " << fromNode->Name() << " "
//                 << fromPort->PortName() << " ) " << fromPort->PortStatus()
//                 << " --> " << toPort->NodeName() << "( " << toPort->PortName() << " ) "
//                 << " " << toPort->PortStatus() << " " << fromPort->InPortsSize()
//                 << " ports ignored" << endl ;
	}
      }
    }
  }

  cdebug_out << "Graph::GetLinks( " << AllLinks << " ) " << endl ;
  return _list_links;
}

GraphBase::ListOfSGraphs * GraphBase::Graph::GetGraphs() const {
  GraphBase::ListOfSGraphs * _list_graphs = new GraphBase::ListOfSGraphs;

  return _list_graphs;
}

GraphBase::SLink * GraphBase::Graph::GetLink( GraphBase::ComputingNode * aNode ,
                                              GraphBase::InPort* toPort ) {
  GraphBase::SLink * _link =  new GraphBase::SLink ;
  GraphBase::OutPort* fromPort = toPort->GetOutPort() ;
//JR 24.02.2005 Memory Leak  _link->FromNodeName = CORBA::string_dup( fromPort->NodeName() );
  _link->FromNodeName = string( fromPort->NodeName() );
  _link->FromServiceParameterName = fromPort->GetServicesParameter().Parametername;
//JR 24.02.2005 Memory Leak  _link->ToNodeName = CORBA::string_dup( toPort->NodeName() );
  _link->ToNodeName = string( toPort->NodeName() );
  _link->ToServiceParameterName = toPort->GetServicesParameter().Parametername;
//JR 30.03.2005  CORBA::Any aSPValue = *fromPort->Value() ;
  CORBA::Any aSPValue = fromPort->Value() ;
  _link->aLinkValue = aSPValue;
  _link->aListOfCoords = *(toPort->Coords()) ;
  return _link;
}
//----------------------------------------------------------------------
// Function : GetDatas
// Purpose  : get the datas list
//----------------------------------------------------------------------
GraphBase::ListOfSLinks * GraphBase::Graph::GetDatas() const {
  GraphBase::ListOfSLinks * _list_datalinks = new GraphBase::ListOfSLinks;

  int ind = 0 ;
  const GraphBase::DataNode * aDataNode = this ;
  int i ;
  for ( i = 0 ; i < aDataNode->GetNodeInDataNodePortsSize() ; i++ ) {
    const GraphBase::OutPort* fromDataPort = aDataNode->GetNodeInDataNodePort( i ) ;
    if ( GraphMacroLevel() == 0 ) {
      if ( fromDataPort->IsDataConnected() ) {
        int j ;
//We may have SharedData as input of a DataFlow : same input for one or several
// input ports. Input(s) of a DataFlow match one or several output-virtual-dataport
        for ( j = 0 ; j < fromDataPort->InPortsSize() ; j++ ) {
          _list_datalinks->resize( ind+1 );
//JR 24.02.2005 Memory Leak          (*_list_datalinks)[ind].FromNodeName = CORBA::string_dup( aDataNode->Name() );
          (*_list_datalinks)[ind].FromNodeName = string( aDataNode->Name() );
          (*_list_datalinks)[ind].FromServiceParameterName = fromDataPort->GetServicesParameter().Parametername;
          const GraphBase::InPort* toPort = fromDataPort->InPorts( j ) ;
//JR 24.02.2005 Memory Leak          (*_list_datalinks)[ind].ToNodeName = CORBA::string_dup( toPort->NodeName() );
          (*_list_datalinks)[ind].ToNodeName = string( toPort->NodeName() );
          (*_list_datalinks)[ind].ToServiceParameterName = toPort->GetServicesParameter().Parametername;
//JR 30.03.2005          CORBA::Any aSPValue = *fromDataPort->Value() ;
          CORBA::Any aSPValue = fromDataPort->Value() ;
          (*_list_datalinks)[ind].aLinkValue = aSPValue;
          (*_list_datalinks)[ind++].aListOfCoords = *(toPort->Coords()) ;
        }
      }
    }
  }

  return _list_datalinks ;
}

bool GraphBase::Graph::AddNode( GraphBase::ComputingNode * aNode ) {
  cdebug_in << "GraphBase::Graph::AddNode "  << (void *) aNode << " " << aNode->Name() << " " << aNode->ServiceName() << endl;
  bool RetVal = false ;
  int index = GetGraphNodeIndex( aNode->Name() ) ;
  if ( index < 0 ) {
    cdebug << "GraphBase::Graph::AddNode " << _GraphNodesSize << " nodes + 1" << endl ;
    _GraphNodes.resize( _GraphNodesSize+1 ) ;
    _GraphNodes[ _GraphNodesSize ] = aNode ;
    SetGraphNodeIndex( aNode->Name() , _GraphNodesSize ) ;
    _GraphNodesSize += 1 ;
    if ( SetServiceOfMap( (GraphBase::Service * ) aNode ) ) {
      cdebug << "Graph::AddNode SetServiceOfMap " << aNode->ServiceName() << " in MapOfServices" << endl ; 
    }
    else {
      cdebug << "Graph::AddNode SetServiceOfMap " << aNode->ServiceName()
             << " was already in MapOfServices" << endl ; 
    }

    RetVal = true ;
    int i ;
    cdebug << "GraphBase::Graph::AddNode " << _GraphNodesSize << " Known nodes :" << endl ;
    for ( i = 0 ; i < _GraphNodesSize ; i++ ) {
      const GraphBase::ComputingNode * aKnownNode = GetGraphNode( i ) ;
      if ( aKnownNode ) {
        cdebug << i << ". " << aKnownNode->Name() << " " << _MapOfGraphNodes[ GetGraphNode( i )->Name() ] - 1 << endl ;
      }
      else {
        cdebug << i << ". ERROR" << endl ;
      }
    }
    aNode->GraphOfNode( this ) ;
  }
  cdebug_out << "GraphBase::Graph::AddNode " << _GraphNodesSize << " Nodes. "
             << aNode->ServiceName() << endl;
  return RetVal ;
}

bool GraphBase::Graph::ReNameNode( const char* OldNodeName ,
                                   const char* NewNodeName ) {
  cdebug_in << "GraphBase::Graph::ReNameNode (" << OldNodeName << " , " << NewNodeName << ") "
            << endl;
  int i ;
  bool RetVal = true ;
//#if 0
  GraphBase::ComputingNode * anOldNode = GetChangeGraphNode( OldNodeName ) ;
  for ( i = 0 ; i < anOldNode->LinkedNodesSize() ; i++ ) {
    cdebug << "LinkedNode " << i << " " << anOldNode->LinkedNodes( i )->Name() << endl ;
  }
//#endif
  if ( !strcmp( OldNodeName , NewNodeName ) ) {
  }
  else if ( strcmp( Name() , OldNodeName ) ) {
    int index = GetGraphNodeIndex( OldNodeName ) ;
    int newindex = GetGraphNodeIndex( NewNodeName ) ;
    if ( index >= 0 && index < _GraphNodesSize &&
         ( newindex < 0 || newindex > _GraphNodesSize ) ) {
      DelGraphNodeIndex( _GraphNodes[ index ]->Name() ) ;
      _GraphNodes[ index ]->Name( NewNodeName ) ;
//JR 29.06.2005 Debug ???
//      SetGraphNodeIndex( NewNodeName , index ) ;
      SetGraphNodeIndex( _GraphNodes[ index ]->Name() , index ) ;

      GraphBase::ComputingNode * aNode = GetChangeGraphNode( index ) ;
// Update 'ToNode's Map from 'aNode'
      cdebug << "Graph::ReNameNode " << aNode->Name() << "/" << OldNodeName << " LinkedNodesSize : "
             << aNode->LinkedNodesSize() << endl ;
      for ( i = 0 ; i < aNode->LinkedNodesSize() ; i++ ) {
        GraphBase::StreamNode * ToNode = aNode->LinkedNodes( i ) ;
        ToNode->ReNameLinkedFromNode( OldNodeName , NewNodeName ) ;
      }
// Rename in _MapOfLinkedNodes of 'FromNode'
      cdebug << "Graph::ReNameNode " << aNode->Name() << "/" << OldNodeName
             << " LinkedFromNodesSize : " << aNode->LinkedFromNodesSize() << endl ;
      for ( i = 0 ; i < aNode->LinkedFromNodesSize() ; i++ ) {
        GraphBase::StreamNode * FromNode = aNode->LinkedFromNodes( i ) ;
        if ( FromNode->GetLinkedNodeIndex( OldNodeName ) >= 0 ) {
          FromNode->ReNameLinkedNode( OldNodeName , NewNodeName ) ;
	}
        else {
          FromNode->ReNameStreamLinkedNode( OldNodeName , NewNodeName ) ;
	}
      }
// Inports 'anInPort' of 'aNode'
      for ( i = aNode->GetNodeInPortsSize()-1 ; i >= 0 ; i-- ) {
        GraphBase::InPort * anInPort = aNode->GetChangeNodeInPort( i ) ;
// OutPort 'anOutPort' linked to 'anInPort'
        GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
        cdebug << i << ". " << aNode->Name() << "/" << OldNodeName
               << " GraphBase::Graph::ReNameNode of LinkedNode : from " ;
        if ( anOutPort ) {
          cdebug << anOutPort->NodeName() << "( " << anOutPort->PortName() << " )" ;
        }
        else {
          cdebug << "without link" ;
        }
        cdebug << " to " << i << ". " << anInPort->PortName() << " of " << NewNodeName
               << " OldNodeName " << OldNodeName ;
        if ( !anInPort->IsNotConnected() ) {
          if ( anOutPort->IsDataConnected() ) {
            cdebug << " fromDataConnected " << anOutPort->NodeName() << endl ;
	  }
          else {
// Node 'FromNode' of 'anOutPort'
            GraphBase::ComputingNode * FromNode ;
            if ( strcmp( anOutPort->NodeName() , Name() ) ) {
              FromNode = GetChangeGraphNode( anOutPort->NodeName() ) ;
	    }
            else {
              FromNode = this ;
	    }
            cdebug << " fromConnected " << anOutPort->NodeName() << endl ;
	  }
          char* OldNodePortName = new char[ strlen( OldNodeName ) +
                                            strlen( aNode->GetChangeNodeInPort( i )->PortName() ) + 3 ] ;
          char* NewNodePortName = new char[ strlen( NewNodeName ) +
                                            strlen( aNode->GetChangeNodeInPort( i )->PortName() ) + 3 ] ;
          strcpy( OldNodePortName , OldNodeName ) ;
          strcat( OldNodePortName , "__" ) ;
          strcat( OldNodePortName , aNode->GetChangeNodeInPort( i )->PortName() ) ;
          strcpy( NewNodePortName , NewNodeName ) ;
          strcat( NewNodePortName , "__" ) ;
          strcat( NewNodePortName , aNode->GetChangeNodeInPort( i )->PortName() ) ;
          RetVal = anOutPort->ReNameInPort( OldNodePortName , NewNodePortName ) ;
          delete [] OldNodePortName ;
          delete [] NewNodePortName ;
          if ( !RetVal )
            break ;
        }
        else {
          cdebug << " not connected" << endl ;
        }
      }
// PAL8508
//JR 02.02.2005 DEBUG : ReName in the CoupledNode was missing
      if ( aNode->IsLoopNode() || aNode->IsEndLoopNode() ||
           aNode->IsSwitchNode() || aNode->IsEndSwitchNode() ) {
        GraphBase::GOTONode * theCoupledNode = NULL ;
        theCoupledNode = (GraphBase::GOTONode * ) ((GraphBase::GOTONode * ) aNode)->CoupledNode() ;
        theCoupledNode->CoupledNodeName( aNode->Name() ) ;
      }
      else if ( aNode->IsInLineNode() ) {
        GraphBase::OutPort * anOutPort = aNode->GetNodeInGate()->GetOutPort() ;
        if ( anOutPort ) {
          GraphBase::ComputingNode * aLinkedNode ;
          aLinkedNode = GetChangeGraphNode( anOutPort->NodeName() ) ;
          if ( aLinkedNode->IsGOTONode() ) {
            ((GraphBase::GOTONode * ) aLinkedNode)->CoupledNodeName( aNode->Name() ) ;
	  }
	}
      }
    }
    else {
      cdebug << "ERROR Node not found or conflicting NodeName" << endl ;
      RetVal = false ;
    }
  }
  else {
    cdebug << "ReNameNode with same name." << endl ;
//    RetVal = Name( NewNodeName ) ;
  }

#if 1
  cdebug << "Graph::ReNameNode " << _GraphNodesSize << " known nodes" << endl;
  for ( i = 0 ; i < _GraphNodesSize ; i++ ) {
    cdebug << "Node" << i << _GraphNodes[ i ]->Name() << endl ;
  }
  map< string , int >::iterator aMapOfGraphNodesIterator ;
  i = 0 ;
  for ( aMapOfGraphNodesIterator = _MapOfGraphNodes.begin() ;
    aMapOfGraphNodesIterator != _MapOfGraphNodes.end() ; aMapOfGraphNodesIterator++ ) {
    cdebug << "MapOfGraphNodes " << i++ << " " << aMapOfGraphNodesIterator->first << " --> "
           << aMapOfGraphNodesIterator->second << " - 1" << endl ;
  }
#endif

  cdebug_out << "GraphBase::Graph::ReNameNode RetVal " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::RemoveNode( const char* aNodeName ) {
  cdebug_in << "Graph::RemoveNode (" << aNodeName << ") " << endl;
  int i ;
  bool RetVal = false ;
  int index = GetGraphNodeIndex( aNodeName ) ;
  GraphBase::ComputingNode * aNode = NULL ;
  if ( index >= 0 ) {
    aNode = GetChangeGraphNode( index ) ;
  }
  if ( aNode ) {
    cdebug << "Graph::RemoveNode " << aNode->Name() << " : " << aNode->GetNodeInPortsSize()
           << " InPorts " << aNode->GetNodeOutPortsSize() << " OutPorts "
           << aNode->LinkedNodesSize() << " LinkedNodes " << aNode->LinkedFromNodesSize()
           << " LinkedFromNodes IsEndSwitchNode " << aNode->IsEndSwitchNode()
           << " ConnectedInPortsNumber " << aNode->ConnectedInPortsNumber() << endl ;
    for ( i = 0 ; i < aNode->LinkedNodesSize() ; i++ ) {
      cdebug << "         LinkedNode" << i << ". " << aNode->LinkedNodes( i ) << " "
             << aNode->LinkedNodes( i )->Name() << endl ;
    }
    if ( !aNode->IsEndLoopNode() ) { // The LoopNode is already deleted
      for ( i = 0 ; i < aNode->LinkedFromNodesSize() ; i++ ) {
        cdebug << "         LinkedFromNode" << i << ". " << aNode->LinkedFromNodes( i ) << " "
               << aNode->LinkedFromNodes( i )->Name() << endl ;
      }
    }
//PAL8177-PAL8509
//JR 24.03.2005 : Debug : CoupledNode field of GOTONodes was not erased
    if ( aNode->IsInLineNode() && aNode->GetNodeInPortsSize() ) { //mkr : IPAL10078
      GraphBase::OutPort * anOutPort = aNode->GetNodeInGate()->GetOutPort() ;
      if ( anOutPort ) {
        GraphBase::ComputingNode * aLinkedNode ;
        aLinkedNode = GetChangeGraphNode( anOutPort->NodeName() ) ;
        if ( aLinkedNode->IsGOTONode() ) {
          cdebug << "Graph::RemoveNode erase CoupledNode " << aLinkedNode->Name() << endl ;
          ((GraphBase::GOTONode * ) aLinkedNode)->CoupledNode( NULL ) ;
          ((GraphBase::GOTONode * ) aLinkedNode)->CoupledNodeName( "" ) ;
        }
      }
    }
    if ( !aNode->IsEndSwitchNode() ) {
      if ( aNode->GetNodeInPortsSize() ) {
        for ( i = aNode->GetNodeInPortsSize()-1 ; i >= 0 ; i-- ) {
          GraphBase::InPort * anInPort = aNode->GetChangeNodeInPort( i ) ;
          cdebug << "Graph::RemoveNode InPort" << i << ". " << aNodeName << "( "
                 << anInPort->PortName() << ") " << anInPort->PortStatus() << " <-- " ;
          GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
          if ( anOutPort && !( aNode->IsEndLoopNode() && anInPort->IsLoop() ) ) {
            cdebug << anOutPort->NodeName() << "( " << anOutPort->PortName() << ") "
                   << anOutPort->PortStatus() ;
          }
          cdebug << endl ;
          if ( !anInPort->IsNotConnected() ) {
            GraphBase::ComputingNode * FromNode = NULL ;
            RetVal = true ;
            if ( anOutPort->IsDataConnected() || anOutPort->IsExternConnected() ) {
              cdebug << "     Data/Extern ignored" << endl ;
	    }
            else {
              FromNode = GetChangeGraphNode( anOutPort->NodeName() ) ;
	    }
            if ( FromNode ) {
              cdebug << "FromNode " << FromNode->Name() << " LinkedNodesSize "
                     << FromNode->LinkedNodesSize() << " and " << aNode->Name()
                     << " LinkedFromNodesSize " << aNode->LinkedFromNodesSize() << endl ;
	      // asv : 04.11.04 : IsGOTO -> IsOneOfGOTO, I believe that it fixes the bug 7120.
// JR 05.01.05 : IsOneOfGOTO is not ok : I shall check the bug 7120 with IsGOTONode
//	      if ( !FromNode->IsOneOfGOTONodes() ) {
//	      if ( !FromNode->IsGOTONode() ) {
	      if ( !FromNode->IsGOTONode() &&
                   !( FromNode->IsEndLoopNode() && anInPort->IsLoop() ) ) {
                if ( anInPort->IsDataStream() ) {
                  RetVal = FromNode->RemoveStreamLinkedNode( aNode ) ;
	        }
                else {
                  RetVal = FromNode->RemoveLinkedNode( aNode ) ;
	        }
                if ( !RetVal ) {
                  cdebug << "Graph::RemoveNode anOutPort->RemoveLinkedNode Error RetVal "
                         << RetVal << endl ;
                  break ;
	        }
	      }
	    }
            RetVal = anOutPort->RemoveInPort( anInPort ) ;
            if ( !RetVal ) {
              cdebug << "Graph::RemoveNode anOutPort->RemoveInPort( anInPort ) Error RetVal "
                     << RetVal << endl ;
              break ;
	    }
          }
          else {
            RetVal = true ;
          }
        }
      }
      else {
        RetVal = true ;
      }
    }
    else {
// Special case of EndSwitchNodes : each Input port must be multi-connected.
// There is one link for each branch of the switch
// But in GraphBase::InPort there is only one field GraphBase::OutPort *
// At execution time there is no problem because only one branch of the switch
// may be executed and the Executor set the right OutPort in the field
// GraphBase::OutPort * of each InPort of the EndSwitchNode
// But here we must remove that node EndSwitch from the LinkedNodes field of
// terminal nodes of all branches.
// And also in the OutPorts of that terminal nodes of branches we must remove
// references to InPorts of the EndSwitchNode.

// Explore LinkedFromnodes of the EndSwitchNode :
      for ( i = aNode->LinkedFromNodesSize() - 1 ; i >= 0  ; i-- ) {
        GraphBase::ComputingNode * aLinkedFromNode = (GraphBase::ComputingNode * ) aNode->LinkedFromNodes( i ) ;
        cdebug << aLinkedFromNode->Name() << " linked to " << aNode->Name() << endl ;
        int j ;
        for ( j = aLinkedFromNode->GetNodeOutPortsSize() - 1 ; j >= 0 ; j-- ) {
          GraphBase::OutPort * anOutPort = aLinkedFromNode->GetChangeNodeOutPort( j ) ;
          cdebug << "Graph::RemoveNode OutPort" << j << ". Remove " << anOutPort->InPortsSize()
                 << " InPortsof OutPort : " << anOutPort->NodeName() << "( "
                 << anOutPort->PortName() << ") "
                 << anOutPort->PortStatus() << " :" << endl ;
          int k ;
          for ( k = anOutPort->InPortsSize() - 1 ; k >= 0  ; k-- ) {
            GraphBase::InPort * anInPort = anOutPort->ChangeInPorts( k ) ;
            if ( !strcmp( anInPort->NodeName() , aNode->Name() ) ) {
// InPort of the EndSwitchNode
              cdebug << "       to InPort" << j << ". " << anInPort->NodeName() << "( "
                     << anInPort->PortName() << ") "
                     << anInPort->PortStatus() << endl ;
              anInPort->RemoveOutPort() ;
              if ( anOutPort->IsDataStream() ) {
                aLinkedFromNode->RemoveStreamLinkedNode( aNode ) ;
	      }
              else {
// false ==> No error for aNode->LinkedFromNode()
                RetVal = aLinkedFromNode->RemoveLinkedNode( aNode , false ) ;
                if ( !RetVal ) {
                  cdebug << "Graph::RemoveNode aLinkedFromNode->RemoveLinkedNode Error RetVal "
                         << RetVal << endl ;
                  break ;
	        }
	      }
// the InPort of the EndSwitchNode from that OutPort was already removed by
// anInPort->RemoveOutPort done above :
#if 0
              RetVal = anOutPort->RemoveInPort( anInPort ) ;
              if ( !RetVal ) {
                cdebug << "Graph::RemoveNode anOutPort->RemoveInPort( anInPort ) Error RetVal "
                       << RetVal << endl ;
                break ;
	      }
#endif
	    }
	  }
	}
      }
      //mkr : fix for bugs IPAL9814 and IPAL9630
      RetVal = true;
    }

    if ( RetVal ) {
      for ( i = aNode->GetNodeOutPortsSize() - 1 ; i >= 0 ; i-- ) {
        GraphBase::OutPort * anOutPort = aNode->GetChangeNodeOutPort( i ) ;
        cdebug << "Graph::RemoveNode OutPort" << i << ". Remove " << anOutPort->InPortsSize()
               << " InPortsof OutPort : " << anOutPort->NodeName() << "( "
               << anOutPort->PortName() << ") "
               << anOutPort->PortStatus() << " :" << endl ;
        int j ;
        for ( j = anOutPort->InPortsSize() - 1 ; j >= 0  ; j-- ) {
          GraphBase::InPort * anInPort = anOutPort->ChangeInPorts( j ) ;
          GraphBase::ComputingNode * ToNode = NULL ;
          cdebug << "       to InPort" << j << ". " << anInPort->NodeName() << "( "
                 << anInPort->PortName() << ") "
                 << anInPort->PortStatus() ;
          if ( anInPort->IsExternConnected() ) {
            cdebug << " ExternConnected ignored" << endl ;
	  }
          else {
            cdebug << endl ;
            ToNode = GetChangeGraphNode( anInPort->NodeName() ) ;
          }
          if ( ToNode ) {
            RetVal = anInPort->RemoveOutPort() ;
// JR 04.02.2005 : Bug if it is a link to an EndSwitchNode. The InPort may be multiple linked !!!
// PAL7990
            if ( !RetVal ) {
              cdebug << "Graph::RemoveNode anOutPort->RemoveOutPort Error RetVal " << RetVal
                     << endl ;
              break ;
	    }
            if ( ( aNode->IsGOTONode() && ToNode->IsOneOfInLineNodes() ) ||
                 ( aNode->IsEndLoopNode() && ToNode->IsLoopNode( ) ) ) {
	    }
            else if ( anOutPort->IsDataStream() ) {
              RetVal = aNode->RemoveStreamLinkedNode( ToNode ) ;
	    }
            else {
              RetVal = aNode->RemoveLinkedNode( ToNode ) ;
	    }
            if ( !RetVal ) {
              cdebug << "Graph::RemoveNode anOutPort->RemoveLinkedNode Error RetVal " << RetVal
                     << endl ;
              break ;
	    }
// the InPort of the EndSwitchNode from that OutPort was already removed by
// anInPort->RemoveOutPort done above :
#if 0
            RetVal = anOutPort->RemoveInPort( anInPort ) ;
            if ( !RetVal ) {
              cdebug << "Graph::RemoveNode anOutPort->RemoveInPort( anInPort ) Error RetVal "
                     << RetVal << endl ;
              break ;
	    }
#endif
            if ( ToNode->IsEndSwitchNode() ) {
              int i ;
              int done = false ;
              for ( i = 0 ; i < ToNode->LinkedFromNodesSize() ; i++ ) {
                GraphBase::StreamNode * fromNode = ToNode->LinkedFromNodes( i ) ;
// Not the node that we are deleting ... :
                if ( strcmp( fromNode->Name() , aNode->Name() ) ) {
                  int j ;
                  for ( j = 0 ; j < fromNode->GetNodeOutPortsSize() ; j++ ) {
                    GraphBase::OutPort * fromOutPort = fromNode->GetChangeNodeOutPort( j ) ;
                    int k ;
                    for ( k = 0 ; k < fromOutPort->InPortsSize() ; k++ ) {
                      if ( strcmp( ToNode->Name() , fromOutPort->InPorts( k )->NodeName() ) == 0 ) {
                        if ( strcmp( anInPort->PortName() , fromOutPort->InPorts( k )->PortName() ) == 0 ) {
// Restore an OutPort in the InPort
                          anInPort->ChangeOutPort( fromOutPort ) ;
                          cdebug << "Graph::RemoveNode reestablish " << fromOutPort->NodeName() << "( "
                                 << fromOutPort->PortName() << " ) in the InPort of EndSwitch : "
                                 << ToNode->Name() << "( " << anInPort->PortName() << " )"
                                 << anInPort->Kind() << " " << anInPort->PortStatus()  << endl;
                          done = true ;
                          break ;
		        }
	              }
	            }
                    if ( done ) {
                      break ;
	            }
	          }
                  if ( done ) {
                    break ;
	          }
		}
	      }
	    }
          }
	}
        if ( !RetVal ) {
          cdebug << "Error RetVal " << RetVal << endl ;
          break ;
	}
      }
    }
    if ( aNode->LinkedNodesSize() || aNode->LinkedFromNodesSize() ) {
      cdebug << "Graph::RemoveNode ERROR LinkedNodesSize " << aNode->LinkedNodesSize()
             << " LinkedFromNodesSize " << aNode->LinkedFromNodesSize() << endl ;
      RetVal = 0 ;
    }
    cdebug << "Graph::RemoveNode " << aNode->Name() << " : " << aNode->GetNodeInPortsSize()
           << " InPorts " << aNode->GetNodeOutPortsSize() << " OutPorts "
           << aNode->LinkedNodesSize() << " LinkedNodes " << aNode->LinkedFromNodesSize()
           << " LinkedFromNodes " << endl ;
    for ( i = 0 ; i < aNode->LinkedNodesSize() ; i++ ) {
      cdebug << "         LinkedNode" << i << ". " << aNode->LinkedNodes( i ) << " "
             << aNode->LinkedNodes( i )->Name() << endl ;
    }
    for ( i = 0 ; i < aNode->LinkedFromNodesSize() ; i++ ) {
      cdebug << "         LinkedFromNode" << i << ". " << aNode->LinkedFromNodes( i ) << " "
             << aNode->LinkedFromNodes( i )->Name() << endl ;
    }
// Delete of aNode from the Graph :
// The LoopNode is needed by the delete of the EndLoopNode. So the LoopNode will be deleted
// when the EndLoopNode is deleted. It is the same for SwitchNode & EndSwitchNode
    if ( RetVal && !aNode->IsLoopNode() && !aNode->IsSwitchNode() ) {
      GraphBase::InLineNode * theCoupledNode = NULL ;
      char * theCoupledNodeName = NULL ;
      int CoupledIndex = -1 ;
      if ( aNode->IsEndLoopNode() || aNode->IsEndSwitchNode() ) {
        theCoupledNodeName = ((GraphBase::GOTONode * ) aNode)->CoupledNodeName() ;
        theCoupledNode = ((GraphBase::GOTONode * ) aNode)->CoupledNode() ;
      }
      cdebug << "Graph::RemoveNode Node" << index << " " << _GraphNodes[ index ]->Name()
             << " will be deleted" << endl ;
      delete aNode ;
      _GraphNodesSize -= 1 ;
      for ( i = index ; i < _GraphNodesSize ; i++ ) {
        _GraphNodes[ i ] = _GraphNodes[ i+1 ] ;
        SetGraphNodeIndex( _GraphNodes[ i ]->Name() , i ) ;
      }
      _GraphNodes.resize( _GraphNodesSize+1 ) ;
      _MapOfGraphNodes.erase( aNodeName ) ;
// PAL8510
// Delete of the LoopNode of that EndLoopNode or of the SwitchNode of that EndSwitchNode :
      if ( theCoupledNode ) {
//JR 02.02.2005 DEBUG : CoupledIndex must be here after the delete of aNode (not before)
        CoupledIndex = GetGraphNodeIndex( theCoupledNodeName ) ;
        cdebug << "Graph::RemoveNode CoupledNode" << CoupledIndex << " "
               << _GraphNodes[ CoupledIndex ]->Name() << " will be deleted" << endl ;
        delete theCoupledNode ;
        _GraphNodesSize -= 1 ;
        for ( i = CoupledIndex ; i < _GraphNodesSize ; i++ ) {
          _GraphNodes[ i ] = _GraphNodes[ i+1 ] ;
          SetGraphNodeIndex( _GraphNodes[ i ]->Name() , i ) ;
        }
        _GraphNodes.resize( _GraphNodesSize+1 ) ;
        _MapOfGraphNodes.erase( theCoupledNodeName ) ;
      }
      RetVal = true ;
    }
  }
  else {
    cdebug << "Graph::RemoveNode Node not found ERROR " << index << endl ;
  }

#if 1
  cdebug << "Graph::RemoveNode " << _GraphNodesSize << " known nodes" << endl;
  for ( i = 0 ; i < _GraphNodesSize ; i++ ) {
    cdebug << "Node" << i << _GraphNodes[ i ]->Name() << endl ;
  }
  map< string , int >::iterator aMapOfGraphNodesIterator ;
  i = 0 ;
  for ( aMapOfGraphNodesIterator = _MapOfGraphNodes.begin() ;
    aMapOfGraphNodesIterator != _MapOfGraphNodes.end() ; aMapOfGraphNodesIterator++ ) {
    cdebug << "MapOfGraphNodes " << i++ << " " << aMapOfGraphNodesIterator->first << " --> "
           << aMapOfGraphNodesIterator->second << " - 1" << endl ;
  }
#endif

  cdebug_out << "Graph::RemoveNode " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::AddLink( const char* FromNodeName ,
                                const char* FromServiceParameterName ,
	                        const char* ToNodeName ,
                                const char* ToServiceParameterName ) {
//                                , const CORBA::Any aValue ) {
  bool RetVal ;
//  int index ;
  cdebug_in << "GraphBase::Graph::AddLink(" << FromNodeName << "("
            << FromServiceParameterName << ") ---> " << ToNodeName << "("
            << ToServiceParameterName << ") , aValue )" << endl;

  GraphBase::ComputingNode *fromNode = GetChangeGraphNode( FromNodeName ) ;
  GraphBase::ComputingNode *toNode = GetChangeGraphNode( ToNodeName ) ;

  GraphBase::OutPort *fromPort = NULL ;
  GraphBase::InPort *toPort = NULL ;

  if ( fromNode ) {
    // asv : 29.12.04 : as I understand, replacement of "InitLoop" by "DoLoop" was
    // done by JR in order to keep old graphs and batch test scripts working after change
    // of special Loop's port name from "InitLoop" to "DoLoop".  I suppose, it is not
    // needed, all old graphs and tests must be changed to use "DoLoop" name for addressing
    // this port..  It we don't do it, ANY port with "InitLoop" name will be replaced by "DoLoop"
    // and it can cause unexpected bugs in future...  So I comment out the replacement code. 
    //if ( strcmp( FromServiceParameterName , "InitLoop" ) ) {
//JR 12.01.05 : Yes the test is not good ; now I test that PortName only in LoopNodes ==>
//   + old graphs will runs normally (it is not possible to ask CEA to edit their graphs)
//   + in old graphs "InitLoop" and "DoLoop" were reserved parameter names (but not controlled)
//   + in new graphs, I have added a control at port creation of that names : they are refused
    if ( !(fromNode->IsLoopNode() && strcmp( FromServiceParameterName , "InitLoop" ) == 0 ) ) {
      fromPort = fromNode->GetChangeOutPort( FromServiceParameterName ) ;
    }
    else {
      fromPort = fromNode->GetChangeOutPort( "DoLoop" ) ;
    }
    if ( !fromPort ) {
      cdebug << "ERROR AddLink fromPort " << FromServiceParameterName << " FromNode("
           << FromNodeName << ") not found." << endl ;
    }
  }
  else {
    cdebug << "ERROR AddLink FromNode " << FromNodeName << " not found." << endl ;
  }
  if ( toNode ) {
    // see comment above
//    if ( strcmp( ToServiceParameterName , "InitLoop" ) ) {
    if ( !(toNode->IsLoopNode() && strcmp( ToServiceParameterName , "InitLoop" ) == 0 ) ) {
      toPort = toNode->GetChangeInPort( ToServiceParameterName ) ;
    }
    else {
      toPort = toNode->GetChangeInPort( "DoLoop" ) ;
    }
    if ( !toPort ) {
      cdebug << "ERROR AddLink toPort " << ToServiceParameterName << " ToNode("
             << ToNodeName << ") not found." << endl ;
    }
  }
  else {
    cdebug << "ERROR AddLink toNode " << ToNodeName << " not found." << endl ;
  }

  RetVal = AddLink( fromNode , fromPort , toNode , toPort ) ;

  if ( RetVal ) {
    if ( fromPort->IsGate() && toPort->IsGate() ) {
      CORBA::Any aValue ;
//JR 29.09.2005 Debug for CEA : we do not do that for MacroNodes :
      if ( !fromNode->IsMacroNode() ) {
        aValue <<= (CORBA::Long ) 1 ;
      }
      else {
        aValue <<= (CORBA::Long ) 0 ;
      }
      fromPort->SetValue( aValue ) ;
    }
//    else {
//      const CORBA::Any * aDataValue = new CORBA::Any( aValue ) ;
//      fromPort->Value( aDataValue ) ;
//    }
//    fromPort->PortStatus( DataConnected );
  }

  cdebug_out << "GraphBase::Graph::AddLink " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::AddLink( GraphBase::ComputingNode *fromNode ,
                                GraphBase::OutPort *fromPort ,
                                GraphBase::ComputingNode *toNode ,
                                GraphBase::InPort *toPort ) {
//  bool RetVal ;

  if ( !fromNode ) {
    cdebug << "ERROR AddLink fromNode not found." << endl ;
    return false ;
  }
  if ( !fromPort ) {
    cdebug << "ERROR AddLink fromPort not found." << endl ;
    return false ;
  }

  if ( !toNode ) {
    cdebug << "ERROR AddLink toNode not found." << endl ;
    return false ;
  }
  if ( !toPort ) {
    cdebug << "ERROR AddLink toPort not found." << endl ;
    return false ;
  }
  if ( ( fromPort->IsDataStream() && !toPort->IsDataStream() ) ||
       ( !fromPort->IsDataStream() && toPort->IsDataStream() ) ) {
    cdebug << "AddLink fromPort/toPort Stream/Flow. ERROR" << endl ;
    return false ;
  }
  if ( fromPort->IsDataStream() && fromPort->Dependency() == SALOME_ModuleCatalog::DATASTREAM_TEMPORAL &&
       toPort->Dependency() == SALOME_ModuleCatalog::DATASTREAM_ITERATIVE ) {
    cdebug << "AddLink fromPort/toPort Stream DATASTREAM_TEMPORAL --> DATASTREAM_ITERATIVE. ERROR" << endl ;
    return false ;
  }

  cdebug_in << "Graph::AddLink( " << fromNode->Name() << "("
            << fromPort->PortName() << " " << fromPort->PortStatus() << ") " << fromPort->Kind()
            << " ---> " << toNode->Name() << "(" << toPort->PortName() << " " << toPort->PortStatus()
            << ") " << toPort->Kind() << " )" << endl;
  ostringstream LinkStrError ;
  LinkStrError << "Creation of link from " << fromNode->Name() << "( "
               << fromPort->PortName() << " " << fromPort->PortStatus() << " " << fromPort->Kind()
               << " ) ---> " << toNode->Name() << "( " << toPort->PortName() << " "
               << toPort->PortStatus() << " " << toPort->Kind() << " ) :\n" ;
  if ( fromPort->IsDataConnected() || fromPort->IsExternConnected() ) {
    if ( !fromPort->RemoveInPort() ) { // There is only one ===> RemoveOutPort()
      cdebug_out << "GraphBase::Graph::AddLink in OutPort ( " << fromNode->Name() << "("
                 << fromPort->PortName() << " " << fromPort->PortStatus()
                 << ") RemoveInPort ERROR InPortsSize " << fromPort->InPortsSize() << endl ;
      return false ;
    }
  }
  if ( toPort->IsDataConnected() || toPort->IsExternConnected() ) {
    if ( !toPort->GetOutPort()->RemoveInPort() ) { // There is only one ===> RemoveOutPort()
      cdebug_out << "GraphBase::Graph::AddLink in InPort connected to the OutPort ( "
                 << toNode->Name() << "(" << toPort->PortName() << " " << toPort->PortStatus()
                 << ") RemoveInPort ERROR InPortsSize " << toPort->GetOutPort()->InPortsSize()
                 << endl ;
      return false ;
    }
  }
  else if ( toPort->IsPortConnected() && !toNode->IsEndSwitchNode() ) {
    if ( !RemoveLink( toPort->GetOutPort()->NodeName() ,
                      toPort->GetOutPort()->PortName() ,
                      toNode->Name() , toPort->PortName() ) ) {
      cdebug_out << "RemoveLink(AddLink) toPort->GetOutPort()->NodeName() RemoveLink Error." << endl ;
      return false ;
    }
    if ( fromNode->IsGOTONode() && fromPort->IsGate() ) { // mkr : PAL12575
      ((GraphBase::GOTONode * ) fromNode)->CoupledNode( (GraphBase::InLineNode * ) toNode ) ;
    }
  }

  if ( !fromNode->IsDataFlowNode() && !fromNode->IsDataStreamNode() &&
       !toNode->IsDataFlowNode() && !toNode->IsDataStreamNode() &&
       toPort->GetOutPort() ) {
//JR Debug 15.06.2005 : the case of multiple links to EndSwitchNode was missing :
    if ( toNode->IsEndSwitchNode() && fromPort->GetInPort( toPort ) == toPort ) {
      LinkStrError << "Link already exists.\n" ;
      SetMessages( LinkStrError.str() ) ;
      cdebug << "Link already exists" << endl ;
      cdebug_out << "GraphBase::Graph::AddLink 1" << endl;
      return true ;
    }
    else if ( !strcmp( toPort->GetOutPort()->NodePortName() ,
                  fromPort->NodePortName() ) ) {
      LinkStrError << "Link already exists.\n" ;
      SetMessages( LinkStrError.str() ) ;
      cdebug << "Link already exists" << endl ;
      cdebug_out << "GraphBase::Graph::AddLink 1" << endl;
      return true ;
    }
    else if ( toPort->IsDataConnected() || toPort->IsExternConnected() ) {
      toPort->GetOutPort()->RemoveInPort( toPort ) ; //dbg
    }
    else if ( fromNode->IsGOTONode() ) {
      if ( !RemoveLink( toPort->GetOutPort()->NodeName() ,
                        toPort->GetOutPort()->PortName() ,
                        toNode->Name() , toPort->PortName() ) ) {
        cdebug << "RemoveLink(AddLink) Error."
               << endl ;
        cdebug_out << "GraphBase::Graph::AddLink" << endl;
        return false ;
      }
      if ( fromPort->InPortsSize() ) {
        if ( !RemoveLink( fromNode->Name() , fromPort->PortName() ,
                          fromPort->ChangeInPorts(0)->NodeName() ,
                          fromPort->ChangeInPorts(0)->PortName() ) ) {
          cdebug << "RemoveLink(AddLink) fromNode->IsGOTONode Error." << endl ;
          cdebug_out << "GraphBase::Graph::AddLink" << endl;
          return false ;
        }
      }
    }
//PAL8511
// JR 17.01.2005 : toPort is already linked : RemoveLink
    else if ( !toNode->IsEndSwitchNode() ) {
//    else if ( !toNode->IsEndSwitchNode() ||
//              ( toNode->IsEndSwitchNode() && toPort->IsGate() ) ) {
//JR Debug : the fromPort was wrong !
//      if ( !RemoveLink( fromNode->Name() , fromPort->PortName() ,
      if ( !RemoveLink( toPort->GetOutPort()->NodeName() , toPort->GetOutPort()->PortName() ,
                        toNode->Name() , toPort->PortName() ) ) {
        cdebug << "RemoveLink(AddLink) toNode->IsEndSwitchNode Error." << endl ;
        cdebug_out << "GraphBase::Graph::AddLink" << endl;
        return false ;
      }
    }
//JR 15.06.2005 : disabled : GraphSwitchBranchGates ...
//    else if ( fromNode->IsSwitchNode() && toNode->IsEndSwitchNode() && !toPort->IsGate() ) {
//      cdebug << "fromNode->IsSwitchNode() && toNode->IsEndSwitchNode toPort(NotGate)Error."
//             << endl ;
//      LinkStrError << "Link from SwitchNode to EndSwitchNode is not allowed.\n" ;
//      SetMessages( LinkStrError.str() ) ;
//      cdebug << "Graph::AddLink ERROR Link from SwitchNode to EndSwitchNode is not allowed."
//             << endl ;
//      cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
//      return false ;
//    }
//JR 15.06.2005 : SwitchNode(Default) may be only linked to EndSwitchNode(Default)
//PAL8511
//JR Debug 15.02.2005 : Not multiple links in that case except if toPort is the DefaultPort
    else if ( fromNode->IsSwitchNode() && fromPort->IsGate() &&
              !toNode->IsEndSwitchNode() ) {
      LinkStrError << "Link from SwitchNode(Default) is allowed only to EndSwitchNode(Default).\n" ;
      SetMessages( LinkStrError.str() ) ;
      cdebug << "SwitchNode(Default) may be only linked to EndSwitchNode(Default) AddLink Error."
             << endl ;
      cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
      return false ;
    }
  }
//PAL7990
//JR 24.03.2005 : No Link between OutPort of SwitchNode to InPort of EndSwitchNode except if
// they have the same name for a special case :
// Switch with no branch ==> Default is applied always : GraphSyrControl example
//JR 22.06.2005 : Enhancement of the behaviour :
//                For the DefaultBranch, if we have same PortsNames, Values are automatically
//                    transmitted
//                But for the DefaultBranch, if we have NOT same PortsNames, we may create links.
//                Look at GraphSyrControlAve* ...
#if 0
  else if ( fromNode->IsSwitchNode() && !fromPort->IsGate() &&
            toNode->IsEndSwitchNode() && !toPort->IsGate() &&
            strcmp( fromPort->PortName() , toPort->PortName() ) ) {
    LinkStrError << "Link from SwitchNode to EndSwitchNode with different PortNames is not allowed.\n" ;
    SetMessages( LinkStrError.str() ) ;
    cdebug << "fromNode->IsSwitchNode() && toNode->IsEndSwitchNode with different PortNames Error."
           << endl ;
    cdebug_out << "GraphBase::Graph::AddLink" << endl;
    return false ;
  }
#endif
  else if ( fromNode->IsGOTONode() && fromPort->InPortsSize() ) {
    if ( !RemoveLink( fromNode->Name() , fromPort->PortName() ,
                      fromPort->ChangeInPorts(0)->NodeName() ,
                      fromPort->ChangeInPorts(0)->PortName() ) ) {
      cdebug << "RemoveLink(AddLink) fromNode->IsGOTONode Error." << endl ;
      cdebug_out << "GraphBase::Graph::AddLink" << endl;
      return false ;
    }
  }
  else if ( ( fromNode->IsDataFlowNode() || fromNode->IsDataStreamNode() ) &&
       ( toPort->IsDataConnected() || toPort->IsExternConnected() ) ) {
    fromPort->RemoveInPort( toPort ) ; //dbg
  }
  else if ( ( toNode->IsDataFlowNode() || toNode->IsDataStreamNode() ) &&
       ( fromPort->IsDataConnected() || fromPort->IsExternConnected() ) ) {
    fromPort->RemoveInPort() ; // There is only one
  }


// JR 12.01.2005 : an OutPort of a SwitchNode connected to a Gate can be connected only to Gate
  if ( !toNode->IsDataFlowNode() && !toNode->IsDataStreamNode() &&
       fromNode->IsSwitchNode() && fromPort->IsSwitch() && !toPort->IsGate() ) {
    LinkStrError << "Link from SwitchNode Port linked to a Gate may be connected only to Gates.\n" ;
    SetMessages( LinkStrError.str() ) ;
    cdebug << "toPort->AddLink Error( fromPort->IsSwitch() && !toPort->IsGate())." << endl ;
    cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
    return false ;
  }
//JR 11.02.2005 : SwitchNode(Default) may be only linked to EndSwitchNode(Default)
//                SwitchNode(Param) may be linked to EndSwitchNode(Param)
//                SwitchNode(Param-UserDefault) may be linked to EndSwitchNode(Default)
  else if ( fromNode->IsSwitchNode() && toNode->IsEndSwitchNode() &&
            !( ( fromPort->IsGate() && toPort->IsGate() ) ||
               ( !fromPort->IsGate() ) ) ) {
//               ( !fromPort->IsGate() && !toPort->IsGate() ) ) ) {
    LinkStrError << "Link from SwitchNode to EndSwitchNode : different kinds of ports not allowed.\n" ;
    SetMessages( LinkStrError.str() ) ;
    cdebug << "Link ERROR from SwitchNode to EndSwitchNode : different kinds of ports not allowed."
           << endl ;
    cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
    return false ;
  }
//JR 15.06.2005 : SwitchNode(Default) may be only linked to EndSwitchNode(Default)
  else if ( fromNode->IsSwitchNode() && fromPort->IsGate() &&
            !toNode->IsEndSwitchNode() ) {
    LinkStrError << "Link from SwitchNode(Default) is allowed only to EndSwitchNode(Default).\n" ;
    SetMessages( LinkStrError.str() ) ;
    cdebug << "SwitchNode(Default) may be only linked to EndSwitchNode(Default) AddLink Error."
           << endl ;
    cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
    return false ;
  }
  else if ( toNode->IsEndSwitchNode() && !toPort->IsGate() ) {
//    if ( !toPort->IsEndSwitch() || !fromPort->AddInPort( toPort ) ) {
    if ( !fromPort->AddInPort( toPort ) ) {
      cdebug << "toNode->IsEndSwitchNode() : !fromPort->AddInPort( toPort ) AddLink Error."
             << endl ;
      cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
      return false ;
    }
    if ( !toPort->IsDataStream() ) {
      toPort->Kind( SUPERV::EndSwitchParameter ) ;
    }
  }
  else if ( !fromPort->AddInPort( toPort ) ) { // --> MapOfInports in the OutPort
    cdebug << "!fromPort->AddLink Error." << endl ;
    cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
    return false ;
  }

  if ( fromNode->IsGOTONode() && ( !fromPort->IsGate() || !toPort->IsGate() ||
       toNode->IsEndLoopNode() || toNode->IsEndSwitchNode() ) ) {
    cdebug << "toPort->AddLink Error( fromNode->IsGOTONode() && toNode->IsEndInLineNode() )." << endl ;
    cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
    return false ;
  }
  else if ( !fromNode->IsSwitchNode() &&
            ( fromPort->IsParam() || fromPort->IsInLine() ) && toPort->IsGate() ) {
    LinkStrError << "Link from OutPort to a Gate is not allowed.\n" ;
    SetMessages( LinkStrError.str() ) ;
    cdebug << "toPort->AddLink Error( fromPort->IsParam() && toPort->IsGate())." << endl ;
    cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
    return false ;
  }
  else if ( !toNode->IsEndSwitchNode() && !toNode->IsEndLoopNode() ) {
    if ( !toPort->AddOutPort( fromPort ) ) { // --> Unique OutPort of the InPort
      cdebug << "toPort->AddLink Error." << endl ;
      cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
      return false ;
    }
  }
  else {
    toPort->AddOutPort( fromPort ) ;
  }

  if ( ( fromNode->IsGOTONode() && toNode->IsOneOfInLineNodes() ) ||
       ( fromNode->IsEndLoopNode() && toNode->IsLoopNode() ) ) {
    cdebug << "AddLink fromPort->PortStatus( PortConnected ) "
           << "GOTONode/EndLoopNode-->InLineNode/LoopNode/SwitchNode "
           << fromNode->Name() << " " << fromPort->PortName()
           << " InLineNode/LoopNode_ConnectedInPortsNumber "
           << toNode->ConnectedInPortsNumber() << endl;
    if ( fromNode->IsGOTONode() ) {
      fromPort->Kind( SUPERV::GOTOParameter ) ;
    }
    fromPort->PortStatus( PortConnected ); // GOTO - Loop
  }
  else if ( fromNode->IsDataFlowNode() || fromNode->IsDataStreamNode() ) {
    cdebug << "AddLink IsDataFlow/StreamNode fromPort->PortStatus( DataConnected ) : "
           << fromNode->Name() << " " << fromPort->PortName() << endl ;
    fromPort->PortStatus( ExternConnected );
  }
  else if ( toNode->IsDataFlowNode() || toNode->IsDataStreamNode() ) {
    cdebug << "AddLink IsDataFlow/StreamNode fromPort->PortStatus( ExternConnected ) : "
           << fromNode->Name() << " " << fromPort->PortName() << endl ;
    fromPort->PortStatus( ExternConnected ) ;
  }
  else {
    cdebug << "AddLink fromPort->PortStatus( PortConnected ) & fromNode->toNode "
           << fromNode->Name() << " " << fromPort->PortName() << " "
           << fromPort->Kind() << " OldPortStatus " << fromPort->PortStatus() << " -> " << toNode->Name()
           << " " << " " << toPort->PortName() << " " << toPort->Kind() << endl;
    fromPort->PortStatus( PortConnected );
    if ( fromPort->IsDataStream() && toPort->IsDataStream() ) {
      fromNode->AddStreamLinkedNode( toNode ) ;
    }
    else {
      fromNode->AddLinkedNode( toNode ) ;
    }
  }
  if ( fromNode->IsSwitchNode() ) {
    if ( !fromPort->IsDataStream() ) {
//JR 08.02.2005      if ( fromPort->IsInLine() && toPort->IsGate() && !toNode->IsEndSwitchNode() ) {
      if ( fromPort->IsInLine() && toPort->IsGate() ) {
        fromPort->Kind( SUPERV::SwitchParameter ) ;
      }
      else if ( !fromPort->IsGate() && !toPort->IsGate() ){
        fromPort->Kind( SUPERV::InLineParameter ) ;
      }
    }
    if ( fromPort->IsGate() && !toNode->IsEndSwitchNode() ) {
      GraphBase::InLineNode * anEndSwitchNode ;
      anEndSwitchNode = ((GraphBase::SwitchNode * ) fromNode)->CoupledNode() ;
      GraphBase::InPort * anInPort = anEndSwitchNode->GetChangeNodeInGate() ;
      GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
      if ( anOutPort && !strcmp( fromNode->Name() , anOutPort->NodeName() ) &&
           !strcmp( fromPort->PortName() , anOutPort->PortName() ) &&
           anOutPort->IsGate() ) {
        if ( !RemoveLink( fromNode->Name() , anOutPort->PortName() ,
                          anEndSwitchNode->Name() ,
                          anInPort->PortName() ) ) {
          cdebug << "AddLink Error Removelink ( Switch , Default , EndSwitch , Default )"
                 << endl ;
          cdebug_out << "GraphBase::Graph::AddLink 0" << endl;
          return false ;
	}
      }
    }
  }
//  cdebug << fromNode->ServiceName() << " " << toNode->ServiceName() << endl ;
  cdebug_out << "GraphBase::Graph::AddLink 1" << endl;
  return true ;
}

bool GraphBase::Graph::RemoveLink( const char* FromNodeName ,
                                   const char* FromServiceParameterName ,
	                           const char* ToNodeName ,
                                   const char* ToServiceParameterName ) {
  cdebug_in << "GraphBase::Graph::RemoveLink from " << FromNodeName << "(" << FromServiceParameterName << ")"
            << " to " << ToNodeName << "(" << ToServiceParameterName << ")" << endl;
  bool RetVal = false ;
  GraphBase::ComputingNode * toNode = GetChangeGraphNode( ToNodeName ) ;
  if ( toNode ) {
    GraphBase::InPort * anInPort = toNode->GetChangeInPort( ToServiceParameterName ) ;
    if ( anInPort && ( anInPort->IsPortConnected() ||
                       anInPort->IsDataConnected() ||
                       anInPort->IsExternConnected() ) ) {
// JR 13.01.2005 : DeBug : if it is a link to an EndSwitchNode. The OutPort may be an other one !!!
      GraphBase::ComputingNode * fromNode = GetChangeGraphNode( FromNodeName ) ;
      GraphBase::OutPort * anOutPort = NULL ;
      if ( toNode->IsEndSwitchNode() ) {
        anOutPort = fromNode->GetChangeOutPort( FromServiceParameterName ) ;
        RetVal = anOutPort->RemoveInPort( anInPort ) ;
      }
      else {
        anOutPort = anInPort->GetOutPort() ;
        RetVal = anOutPort->RemoveInPort( anInPort ) ;
      }
//      anInPort->RemoveOutPort() ; // Already done in RemoveInPort
//      const char * FromNodeName = anOutPort->NodeName() ;
      if ( RetVal ) {
        if ( fromNode ) {
          if ( anInPort->IsDataStream() ) {
            fromNode->RemoveStreamLinkedNode( toNode ) ;
	  }
          else {
            fromNode->RemoveLinkedNode( toNode ) ;
	    // mkr : PAL12575 -->
	    if ( fromNode->IsGOTONode() ) {
	      GraphBase::GOTONode * aGOTONode = (GraphBase::GOTONode * ) fromNode ;
	      if ( aGOTONode->CoupledNode() && strcmp( aGOTONode->CoupledNode()->Name(), toNode->Name() ) == 0 ) {
		aGOTONode->CoupledNode(NULL);
		aGOTONode->CoupledNodeName("");
		
		// set ExternConnected status for output data port of GoTo node
		for ( int aPortId = 0 ; aPortId < fromNode->GetNodeOutPortsSize() ; aPortId++ ) {
		  GraphBase::OutPort * anOutPort = fromNode->GetChangeNodeOutPort(aPortId) ;
		  if ( anOutPort->IsNotConnected() && !anOutPort->IsGate() )
		    anOutPort->PortStatus( ExternConnected ) ;
		}
	      }
	    }
	    // mkr : PAL12575 <--
	  }
        }
//JR PAL7990 : a SwitchPort is an OutPort of a SwitchNode connected to a InGate Port
//Debug : change the kind only if there is no other InGate connected to the OutPort
        if ( fromNode->IsSwitchNode() && !anOutPort->IsGate() && anOutPort->InPortsSize() == 0 ) {
          if ( !toNode->IsEndSwitchNode() ) {
            anOutPort->Kind( SUPERV::InLineParameter ) ;
	  }
          else if ( !anInPort->IsGate() ) {
            anOutPort->Kind( SUPERV::InLineParameter ) ;
	  }
        }
// JR 13.01.2005 : DeBug : if it is a link to an EndSwitchNode. The InPort may be multiple linked !!!
        if ( toNode->IsEndSwitchNode() ) {
          int i ;
          int done = false ;
          for ( i = 0 ; i < toNode->LinkedFromNodesSize() ; i++ ) {
            GraphBase::StreamNode * fromNode = toNode->LinkedFromNodes( i ) ;
            int j ;
            for ( j = 0 ; j < fromNode->GetNodeOutPortsSize() ; j++ ) {
              GraphBase::OutPort * fromOutPort = fromNode->GetChangeNodeOutPort( j ) ;
              int k ;
              for ( k = 0 ; k < fromOutPort->InPortsSize() ; k++ ) {
                if ( strcmp( toNode->Name() , fromOutPort->InPorts( k )->NodeName() ) == 0 ) {
                  if ( strcmp( anInPort->PortName() , fromOutPort->InPorts( k )->PortName() ) == 0 ) {
// Restore an OutPort in the InPort
                    anInPort->ChangeOutPort( fromOutPort ) ;
                    cdebug << "Graph::RemoveLink reestablish " << fromOutPort->NodeName() << "( "
                           << fromOutPort->PortName() << " ) in the InPort of EndSwitch : "
                           << toNode->Name() << "( " << anInPort->PortName() << " )"
                           << anInPort->Kind() << " " << anInPort->PortStatus() << endl;
                    done = true ;
                    break ;
		  }
	        }
	      }
              if ( done ) {
                break ;
	      }
	    }
            if ( done ) {
              break ;
	    }
	  }
	}
      }
    }
  }
  cdebug_out << "GraphBase::Graph::RemoveLink " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::GetLink(const char* ToNodeName ,
                               const char* ToServiceParameterName ,
                               char** FromNodeName ,
                               char** FromServiceParameterName ) {
//  cdebug_in << "GraphBase::Graph::GetLink " << ToNodeName << "("
//            << ToServiceParameterName << ")" << endl;
  bool RetVal = false ;
  GraphBase::ComputingNode * toNode = GetChangeGraphNode( ToNodeName ) ;
  if ( toNode ) {
    GraphBase::InPort * anInPort = toNode->GetChangeInPort( ToServiceParameterName ) ;
    if ( anInPort && !anInPort->IsNotConnected() ) {
      GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
//JR 17.02.2005 Memory Leak      *FromNodeName = my_strdup( anOutPort->NodeName() ) ;
      *FromNodeName = (char * ) anOutPort->NodeName() ;
//JR 17.02.2005 Memory Leak      *FromServiceParameterName = my_strdup( anOutPort->PortName() ) ;
      *FromServiceParameterName = (char * ) anOutPort->PortName() ;
      RetVal = true ;
    }
    else {
      cdebug << "GraphBase::Graph::GetLink ERROR no InPort or NotConnected " << ToNodeName
             << " " << ToServiceParameterName << " " << RetVal << endl;
    }
  }
  else {
    cdebug << "GraphBase::Graph::GetLink ERROR no Node " << ToNodeName << " " << RetVal
           << endl;
  }
//  cdebug_out << "GraphBase::Graph::GetLink " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::AddInputData( const char* ToNodeName ,
                                     const char* ToServiceParameterName ,
                                     const CORBA::Any aValue ) {
  bool RetVal = false ;
  cdebug_in << "GraphBase::Graph::AddInputData(" << ToNodeName << ","
            << ToServiceParameterName << " , Any " ;
  bool isUnknown = false;
  switch (aValue.type()->kind()) {
    case CORBA::tk_string:
      const char * t;
      aValue >>= t;
      cdebug << t << " (string) " ;
      if ( !strcmp( t, "Unknown CORBA::Any Type" ) ) 
	// mkr : IPAL8054 : in this case port has no input value
	isUnknown = true;
      break;
    case CORBA::tk_double:
      double d;
      aValue >>= d;
      cdebug << d << " (double) " ;
      break;
    case CORBA::tk_long:
      CORBA::Long l;
      aValue >>= l;
      cdebug << l << " (CORBA::Long) " ;
      break;
    case CORBA::tk_objref:
      cdebug << "(object reference) " ;
      break;
    default:
      cdebug << "(other(tk_string(" << CORBA::tk_string << "),tk_double(" << CORBA::tk_double << "),tk_long("
             << CORBA::tk_long << "),tk_objref)(" << CORBA::tk_objref << ")) ERROR : " << aValue.type()->kind() ;
      break;
    }
  cdebug << " ) in Graph " << Name() << endl;
 
  GraphBase::ComputingNode *toNode ;
  GraphBase::InPort *toPort ;
  char *aNodeName ;
  char *aPortName ;
  NodePort( ToNodeName , ToServiceParameterName , &aNodeName , &aPortName ) ;
  toNode = GetChangeGraphNode( aNodeName ) ;
  cdebug << "in Graph " << Name() << " aNodeName '" << aNodeName << "' aPortName '" << aPortName
         << "'" << endl;
  if ( toNode ) {
    toPort = toNode->GetChangeInPort( aPortName ) ;
    if ( toPort && !toPort->IsDataConnected() && toPort->GetOutPort() ) {
      toPort->RemoveOutPort() ;
    }
  }
  else {
    toPort = NULL ;
  }

  int i ;
  cdebug << endl << "Graph::AddInputData ListOfInOutPorts of the Graph : " << endl ;
  for ( i = 0 ; i < GetNodeOutDataNodePortsSize() ; i++ ) {
    cdebug << "Graph::AddInputData In" << i << " " << *GetNodeOutDataNodePort(i) << endl ;
  }
  for ( i = 0 ; i < GetNodeInDataNodePortsSize() ; i++ ) {
    cdebug << "Graph::AddInputData Out" << i << " " << *GetNodeInDataNodePort(i) << endl ;
  }

  if ( toNode && toPort ) {
    GraphBase::OutPort *fromDataNodePort = NULL ;
    fromDataNodePort = GraphBase::DataNode::GetChangeInDataNodePort( toPort->NodePortName() ) ;
    if ( fromDataNodePort && fromDataNodePort->GetInPort( toPort ) ) {
      fromDataNodePort->RemoveInPort( toPort ) ;
    }
    cdebug << "Try AddLink " << toPort->NodePortName() << " : " << Name() << "( "
           << fromDataNodePort->PortName() << " ) -->" << toNode->Name() << "( "
           << toPort->PortName() << " )" << endl ;
    RetVal = AddLink( (GraphBase::ComputingNode * ) this , fromDataNodePort ,
                      toNode , toPort ) ;
    if ( RetVal ) {
      fromDataNodePort->SetValue( aValue ) ;
      if ( !isUnknown ) // mkr : IPAL8054 : to avoid setting "strange" value to a port
	fromDataNodePort->PortStatus( DataConnected ) ;
    }
  }
  else {
    cdebug << "ERROR Node and/or Port not found : " << aNodeName << " " << aPortName << endl ;
  }
  delete [] aNodeName ;
  delete [] aPortName ;

  cdebug_out << "GraphBase::Graph::AddInputData " << RetVal << endl;
  return RetVal ;
}

// Used in the Executor in order to change an Input Value and ReRun
bool GraphBase::Graph::ChangeInputData( const char * ToNodeName ,
                                        const char * ToServiceParameterName ,
                                        const CORBA::Any aValue ) {
  bool RetVal = false ;
  cdebug_in << "GraphBase::Graph::ChangeInputData( '" << ToNodeName << "' , '"
            << ToServiceParameterName << "' , Any " ;
  switch (aValue.type()->kind()) {
    case CORBA::tk_string:
      const char * t;
      aValue >>= t;
      cdebug << t << " (string) " ;
      break;
    case CORBA::tk_double:
      double d;
      aValue >>= d;
      cdebug << d << " (double) " ;
      break;
    case CORBA::tk_long:
      CORBA::Long l;
      aValue >>= l;
      cdebug << l << " (CORBA::Long) " ;
      break;
    case CORBA::tk_objref:
      cdebug << "(object reference) " ;
      break;
    default:
      cdebug << "(other(tk_string,tk_double,tk_long,tk_objref)) ERROR";
      break;
    }
  cdebug << " ) in Graph " << Name() << endl;

  GraphBase::ComputingNode * toNode ;
  GraphBase::InPort * toPort = NULL ;
  char * aNodeName ;
  char * aPortName ;
  NodePort( ToNodeName , ToServiceParameterName , &aNodeName , &aPortName ) ;
  toNode = GetChangeGraphNode( aNodeName ) ;
  cdebug << "in Graph " << Name() << " aNodeName " << aNodeName << " aPortName " << aPortName << endl;
  if ( toNode ) {
    toPort = toNode->GetChangeInPort( aPortName ) ;
    if ( toPort && toPort->IsDataConnected() ) {
      GraphBase::OutPort * fromPort = toPort->GetOutPort();
      if ( fromPort ) {
        RetVal = true ;
        fromPort->SetValue( aValue ) ;
        fromPort->PortStatus( DataConnected ) ;
        toPort->PortState( SUPERV::ReadyState ) ;
      }
      else {
        cdebug << "ERROR ChangeInputData fromPort not found" << endl ;
      }
    }
    else {
      cdebug << "ERROR ChangeInputData toPort not found" << endl ;
    }
  }
  else {
    cdebug << "ERROR ChangeInputData Node not found : " << ToNodeName << endl ;
  }

  cdebug_out << "GraphBase::Graph::ChangeInputData" << endl;
  return RetVal ;
}

#if 0
// To restore InputData in CreateService
bool GraphBase::Graph::AddInputData( const char* ToNodeName ,
                                     const char* ToServiceParameterName ,
                                     const CORBA::Any ** aValue ) {
  bool RetVal = false ;
  cdebug_in << "GraphBase::Graph::AddInputData( " << ToNodeName << " , "
            << ToServiceParameterName << ", **Any " ;
  const CORBA::Any theValue = **aValue ;
  switch (theValue.type()->kind()) {
    case CORBA::tk_string:
      char * t;
      theValue >>= t;
      cdebug << t << " (string) " ;
      break;
    case CORBA::tk_double:
      CORBA::Double d;
      theValue >>= d;
      cdebug << d << " (double) " ;
      break;
    case CORBA::tk_long:
      CORBA::Long l;
      theValue >>= l;
      cdebug << l << " (CORBA::Long) " ;
      break;
    case CORBA::tk_objref:
      cdebug << "(object reference) " ;
      break;
    default:
      cdebug << "(other(tk_string,tk_double,tk_long,tk_objref)) ERROR";
      break;
    }
  cdebug << ") of Graph " << Name() << endl;

  GraphBase::ComputingNode *toNode = GetChangeGraphNode( ToNodeName ) ;

  if ( toNode ) {
    GraphBase::InPort *toPort = NULL ;
    GraphBase::OutPort *fromDataNodePort = NULL ;
    toPort = toNode->GetChangeInPort( ToServiceParameterName ) ;
    fromDataNodePort = GraphBase::DataNode::GetChangeInDataNodePort( toPort->NodePortName() ) ;
    cdebug << "Try AddLink " << Name() << "( " << fromDataNodePort << " ) -->"
           << toNode->Name() << "( " << toPort->PortName() << " )" << endl ;
    RetVal = AddLink( (GraphBase::ComputingNode * ) this , fromDataNodePort ,
                      toNode , toPort ) ;
    if ( RetVal ) {
      fromDataNodePort->Value( aValue ) ;
      fromDataNodePort->PortStatus( DataConnected ) ;
    }
  }
  else {
    cdebug << "ERROR Node not found " << ToNodeName << " ERROR. Known nodes :" << endl ;
    map< string , int >::iterator aMapOfGraphNodesIterator ;
    int i = 0 ;
    for ( aMapOfGraphNodesIterator = _MapOfGraphNodes.begin() ;
          aMapOfGraphNodesIterator != _MapOfGraphNodes.end() ; aMapOfGraphNodesIterator++ ) {
      cdebug << "MapOfGraphNodes " << i++ << " " << aMapOfGraphNodesIterator->first << " --> "
             << aMapOfGraphNodesIterator->second << endl ;
    }
  }

  cdebug_out << "GraphBase::Graph::AddInputData " << RetVal << endl;
  return RetVal ;
}
#endif

bool GraphBase::Graph::AddOutputData( const char* FromNodeName ,
                                      const char* FromServiceParameterName ,
                                      const CORBA::Any aValue ) {
  bool RetVal = true ;
  cdebug_in << "GraphBase::Graph::AddOutputData(" << FromNodeName << ","
            << FromServiceParameterName << ")" << endl;
  GraphBase::ComputingNode *fromNode = GetChangeGraphNode( FromNodeName ) ;

  GraphBase::OutPort *anOutPort = NULL ;

  if ( fromNode ) {
    anOutPort = fromNode->GetChangeOutPort( FromServiceParameterName ) ;
//    toDataNodePort = GraphBase::DataNode::GetChangeOutDataNodePort( fromPort->NodePortName() ) ;
//    RetVal = AddLink( fromNode , fromPort ,
//                      (GraphBase::ComputingNode * ) this , toDataNodePort ) ;
    anOutPort->SetValue( aValue ) ;
    anOutPort->PortState(  SUPERV::ReadyState ) ;
    anOutPort->PortDone( true ) ;
    cdebug << *anOutPort ;
#ifdef _DEBUG_
    anOutPort->StringValue( *_fdebug ) ;
#endif
    cdebug << endl ;
  }
  else {
    cdebug << "ERROR Node not found" << endl ;
    RetVal = false ;
  }
  cdebug_out << "GraphBase::Graph::AddOutputData " << RetVal << endl;
  return RetVal ;
}

//JR This is used by StreamGraphs to list "CODES" and after that there is instances
// for each Code (Nodes).
map< string , GraphBase::Service * > GraphBase::Graph::MapOfServiceNames() {
                                           return _MapOfServiceNames ; }

GraphBase::Service * GraphBase::Graph::GetServiceOfMap( char * name ) {
  return _MapOfServiceNames[ name ] ;
}

bool GraphBase::Graph::SetServiceOfMap( GraphBase::Service * aService ) {
  GraphBase::Service * theService = _MapOfServiceNames[ aService->ServiceName() ] ;
  if ( theService ) {
    cdebug << "SetServiceOfMap of " << aService->ServiceName()
           << " already in MapOfServiceNames : erase" << endl ;
    _MapOfServiceNames.erase( aService->ServiceName() ) ;
  }
  _MapOfServiceNames[ (char * ) aService->ServiceName() ] = aService ;
  cdebug << "SetServiceOfMap of " << aService->ServiceName() << " done" << endl ;
  return true ;
}

//PAL7961
//JR Debug 25.03.2005 : with multiple instances et delete of instances, it was wrong
//Bug created with the developpement of StreamGraphs
int GraphBase::Graph::GetNewServiceInstanceNumber( char * aServiceName ) {
  int InstanceNumber = _MapOfServiceInstanceNumber[ aServiceName ] + 1 ;
  _MapOfServiceInstanceNumber[ aServiceName ] = InstanceNumber ;
  return InstanceNumber ;
}

void GraphBase::Graph::SetGraphPorts() {
  cdebug_in << "GraphBase::Graph::SetGraphPorts fill ports of Graph" << endl;
  int i ;
  for ( i = 0 ; i < GetNodeInDataNodePortsSize() ; i++ ) {
    GraphBase::OutPort * anOutPort = GetChangeNodeInDataNodePort( i ) ;
    cdebug << "SetGraphPorts Out" << i << " " << anOutPort->PortName() << " " << anOutPort->PortType()
           << " " << anOutPort->Kind() << " " << anOutPort->PortStatus() << " " << anOutPort->PortState() << endl ;
    if ( !anOutPort->IsGate() ) {
      anOutPort->PortStatus( ExternConnected ) ;
    }
  }
  for ( i = 0 ; i < GetNodeOutDataNodePortsSize() ; i++ ) {
    GraphBase::InPort * anInPort = GetChangeNodeOutDataNodePort( i ) ;
    cdebug << "SetGraphPorts In" << i << " " << anInPort->PortName() << " " << anInPort->PortType()
           << " " << anInPort->Kind() << " " << anInPort->PortStatus() << " " << anInPort->PortState() << endl ;
    if ( !anInPort->IsGate() ) {
      GraphBase::OutPort * anOutPort = anInPort->GetOutPort() ;
      anOutPort->PortStatus( ExternConnected ) ;
    }
  }
  cdebug_out << "GraphBase::Graph::SetGraphPorts" << endl;
}

string GraphBase::Graph::Messages() {
//  cdebug_in << "Graph::Messages" << endl ;
//  cdebug_out << "Graph::Messages" << endl ;
  return ( CORBA::string_dup( _Messages.c_str() ) ) ;
}

void GraphBase::Graph::ReSetMessages() {
  cdebug << "Graph::ReSetMessages" << endl ;
  _Messages = string( "" ) ;
}

void GraphBase::Graph::SetMessages( string anErrorMessage ) {
  cdebug_in << "Graph::SetMessages" << endl ;
  _Messages = _Messages + anErrorMessage + string( "\n" ) ;
  cdebug_out << "Graph::SetMessages :" << endl << _Messages << endl ;
}

bool GraphBase::Graph::CreateService() {
  cdebug_in << "GraphBase::Graph::CreateService " << Name() << " GraphMacroLevel : " << GraphMacroLevel() << endl;
  bool RetVal = true ;
  SALOME_ModuleCatalog::Service aService ;
  int i , j ;
  GraphBase::ComputingNode * aToNode ;
  int dostore ;
  int innbr = 0 ;
  int instreamnbr = 0 ;
  int outnbr = 0 ;
  int outstreamnbr = 0 ;
  vector< SUPERV::KindOfPort > InPortsKind ;
  vector< SALOME_ModuleCatalog::DataStreamDependency > InDataStreamDependency ;
  vector< SUPERV::KindOfSchema > InKindOfSchema ;
  vector< SUPERV::KindOfInterpolation > InKindOfInterpolation ;
  vector< SUPERV::KindOfExtrapolation > InKindOfExtrapolation ;
  vector< SUPERV::KindOfPort > OutPortsKind ;
  vector< SALOME_ModuleCatalog::DataStreamDependency > OutDataStreamDependency ;
  vector< long > OutNumberOfValues ;
//  if ( GraphMacroLevel() == 0 ) {
    for ( dostore = 0 ; dostore <= 2 ; dostore++ ) {
      if ( dostore == 1 ) {
//JR 17.02.2005 Memory Leak        aService.ServiceName = Name() ;
        aService.ServiceName = my_strdup( Name() ) ;
        aService.ServiceinParameter.length( 0 ) ;
        aService.ServiceinParameter.length( innbr ) ;
        aService.ServiceoutParameter.length( 0 ) ;
        aService.ServiceoutParameter.length( outnbr ) ;
        InPortsKind.resize( innbr ) ;
        InDataStreamDependency.resize( innbr ) ;
        InKindOfSchema.resize( innbr ) ;
        InKindOfInterpolation.resize( innbr ) ;
        InKindOfExtrapolation.resize( innbr ) ;
        OutPortsKind.resize( outnbr ) ;
        OutDataStreamDependency.resize( outnbr ) ;
        OutNumberOfValues.resize( outnbr ) ;
      }
      else if ( dostore == 2 ) {
//JR Creation of the Service (and of the Ports) with GraphBase::DataNode::DataService :
        cdebug << "GraphBase::Graph::CreateService ->DataService innbr " << innbr
               << " instreamnbr " << instreamnbr << " outnbr " << outnbr
               << " outstreamnbr " << outstreamnbr << endl ;
        DataService( _Orb , aService , Graph_prof_debug() , Graph_fdebug() ) ;
        aService.ServiceinParameter.length( innbr + instreamnbr ) ;
        aService.ServiceoutParameter.length( outnbr + outstreamnbr ) ;
        InPortsKind.resize( innbr + instreamnbr ) ;
        InDataStreamDependency.resize( innbr + instreamnbr ) ;
        InKindOfSchema.resize( innbr + instreamnbr ) ;
        InKindOfInterpolation.resize( innbr + instreamnbr ) ;
        InKindOfExtrapolation.resize( innbr + instreamnbr ) ;
        OutPortsKind.resize( outnbr + outstreamnbr ) ;
        OutDataStreamDependency.resize( outnbr + outstreamnbr ) ;
        OutNumberOfValues.resize( outnbr + outstreamnbr ) ;
      }
      if ( dostore == 0 ) {
        innbr = 0 ;
        instreamnbr = 0 ;
        outnbr = 0 ;
        outstreamnbr = 0 ;
      }
      if ( dostore == 1 ) {
        innbr = 0 ;
        outnbr = 0 ;
      }
      for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
        aToNode = GraphNodes( i ) ;
        for ( j = 0 ; j < aToNode->GetNodeInPortsSize() ; j++ ) {
          GraphBase::InPort *anInPort = aToNode->GetChangeNodeInPort(j) ;
          bool isfromcouplednode = false ;
          if ( !anInPort->IsNotConnected() ) {
            const char * aFromNodeName = anInPort->GetOutPort()->NodeName() ;
            GraphBase::ComputingNode * aFromNode = GetChangeGraphNode( aFromNodeName ) ;
            cdebug << "GraphBase::Graph::CreateService aFromNodeName " << aFromNodeName << " aToNode "
                   << aToNode->Name() << " InPort" << j << " " << anInPort->PortName() << endl ;
            if ( aFromNode && aFromNode->IsEndLoopNode() &&
                 ((GraphBase::EndOfLoopNode * ) aFromNode)->CoupledNode() == aToNode ) {
              isfromcouplednode = true ;
	    }
	  }
          if ( !anInPort->IsGate() && !anInPort->IsLoop() &&
               ( !anInPort->IsPortConnected() ||
//                 ( anInPort->IsPortConnected() && isfromcouplednode ) ) ) {
//JR PAL8914 : InPorts of EndLoopNodes may not be in the Service of the Graph
                 ( anInPort->IsPortConnected() && isfromcouplednode ) ) &&
               !aToNode->IsEndLoopNode() ) {
            if ( dostore == 0 ) {
              cdebug << "CreateService " << aToNode->Name() << " Input port "
                     << anInPort->PortName() << " " << anInPort->Kind() << " " << anInPort->PortStatus() ;
              if ( anInPort->GetOutPort() ) {
                cdebug << "DataConnected from " <<  *(anInPort->GetOutPort() ) ;
	      }
              cdebug << endl ;
              if ( anInPort->IsDataStream() ) {
                instreamnbr += 1 ;
	      }
              else {
                innbr += 1 ;
	      }
	    }
            else if ( ( dostore == 1 && !anInPort->IsDataStream() ) ||
                      ( dostore == 2 && anInPort->IsDataStream() ) ) {
              aService.ServiceinParameter[innbr].Parametertype = CORBA::string_dup( anInPort->PortType() ) ;
              aService.ServiceinParameter[innbr].Parametername = CORBA::string_dup( anInPort->NodePortName() ) ;
              InPortsKind[ innbr ] = anInPort->Kind() ;
              InDataStreamDependency[ innbr ] = anInPort->Dependency() ;
              if ( dostore == 2 && anInPort->IsDataStream() ) {
                ((GraphBase::InDataStreamPort * ) anInPort)->Params( InKindOfSchema[ innbr ] ,
                                                                     InKindOfInterpolation[ innbr ] ,
                                                                     InKindOfExtrapolation[ innbr ] ) ;
	      }
              cdebug << "In" << innbr << " " << aService.ServiceinParameter[ innbr ].Parametername << " "
                     << anInPort->Kind() << " " << anInPort->PortStatus() << endl ;
              innbr += 1 ;
	    }
          }
          else if ( dostore == 0 ) {
            cdebug << "CreateService " << aToNode->Name() << " Input port " << anInPort->PortName()
                   << " " << anInPort->PortStatus() ;
            if ( anInPort->IsPortConnected() ) {
              cdebug << " is connected " ;
	    }
            else {
              cdebug << " is NOT connected " ;
	    }
            if ( anInPort->IsGate() ) {
              cdebug << " IsGate " ;
	    }
            if ( anInPort->GetOutPort() ) {
              cdebug << "DataConnected from " <<  *(anInPort->GetOutPort()) ;
	    }
            else {
              cdebug << "NOT DataConnected" ;
	    }
            cdebug << endl ;
	  }
        }
        GraphBase::ComputingNode * aFromNode = aToNode ;
        for ( j = 0 ; j < aFromNode->GetNodeOutPortsSize() ; j++ ) {
          GraphBase::OutPort *anOutPort = aFromNode->GetChangeNodeOutPort(j) ;
//          cdebug << "CreateService Node " << aFromNode->Name() << ". Output port[" << j << "] ";
//          if ( anOutPort ) {
//            cdebug << anOutPort->PortName() << " " << anOutPort->ServicesParameterType() << endl ;
//	  }
//          else {
//            cdebug << " NULL" << endl ;
//	  }
          if ( !aFromNode->IsGOTONode() ) {
            if ( !anOutPort->IsGate() && ( anOutPort->IsNotConnected() || anOutPort->IsDataConnected()  ||
//JR PAL8914 : OutPorts of LoopNodes may not be in the Service of the Graph
//                                           anOutPort->IsExternConnected() ) {
                                           anOutPort->IsExternConnected() ) &&
                 !aFromNode->IsLoopNode() ) {
              if ( dostore == 0 ) {
//                cdebug << "CreateService " << aFromNode->Name() << " Output port "
//                       << anOutPort->PortName() << " " << anOutPort->PortStatus() << endl ;
                if ( anOutPort->IsDataStream() ) {
                  outstreamnbr += 1 ;
	        }
                else {
                  outnbr += 1 ;
	        }
	      }
              else if ( ( dostore == 1 && !anOutPort->IsDataStream() ) ||
                        ( dostore == 2 && anOutPort->IsDataStream() ) ) {
                aService.ServiceoutParameter[outnbr].Parametertype = CORBA::string_dup( anOutPort->PortType() ) ;
                aService.ServiceoutParameter[outnbr].Parametername = CORBA::string_dup( anOutPort->NodePortName() ) ;
                OutPortsKind[ outnbr ] = anOutPort->Kind() ;
                OutDataStreamDependency[ outnbr ] = anOutPort->Dependency() ;
                if ( dostore == 2 && anOutPort->IsDataStream() ) {
                  OutNumberOfValues[ outnbr ] = ((GraphBase::OutDataStreamPort * ) anOutPort)->NumberOfValues() ;
	        }
//                cdebug << "Out" << outnbr << " " << aService.ServiceoutParameter[ outnbr ].Parametername << " "
//                       << anOutPort->Kind() << " " << anOutPort->PortStatus() << endl ;
                outnbr += 1 ;
	      }
	    }
	  }
        }
      }
    }
//  }

#if 1
  cdebug << "DataFlowNode ServiceName " << aService.ServiceName << " InPorts " << innbr
         << " OutPorts " << outnbr << endl ;
  for ( i = 0 ; i < innbr ; i++ ) {
    cdebug << "aService.ServiceinParameter[" << i << "].Parametertype "
         << aService.ServiceinParameter[i].Parametertype << " Parametername "
         << aService.ServiceinParameter[i].Parametername << " " << InPortsKind[ i ] << endl ;
    if ( InPortsKind[ i ] != SUPERV::DataStreamParameter ) {
      cdebug << "NodeInPort[" << i << "] " << *GetChangeNodeInPort( i ) << endl ;
    }
  }
  for ( i = 0 ; i < outnbr ; i++ ) {
    cdebug << "aService.ServiceoutParameter[" << i << "].Parametertype "
         << aService.ServiceoutParameter[i].Parametertype << " Parametername "
         << aService.ServiceoutParameter[i].Parametername << " " << OutPortsKind[ i ] << endl ;
    if ( OutPortsKind[ i ] != SUPERV::DataStreamParameter ) {
      cdebug << "NodeOutPort[" << i << "] " << *GetChangeNodeOutPort( i ) << endl ;
    }
  }
#endif

//  DataService( _Orb , aService , InPortsKind , OutPortsKind , Graph_prof_debug() , Graph_fdebug() ) ;
  for ( i = 0 ; i < innbr ; i++ ) {
    if ( InPortsKind[ i ] == SUPERV::DataStreamParameter ) {
      GraphBase::InDataStreamPort * anInPort = AddInDataStreamPort( aService.ServiceinParameter[ i ].Parametername,
                                                                    aService.ServiceinParameter[ i ].Parametertype,
                                                                    InDataStreamDependency[ i ] ,
                                                                    SUPERV::DataStreamParameter ) ;
      anInPort->SetParams( InKindOfSchema[ i ] ,
                           InKindOfInterpolation[ i ] ,
                           InKindOfExtrapolation[ i ] ) ;
// NOT A BUG : AddOutPort for an inport (Reversed service)
      anInPort = (GraphBase::InDataStreamPort * ) _DataFlowDataPorts->AddOutPort(
                                                           _Orb , NamePtr() ,
                                                           Kind() ,
                                                           aService.ServiceinParameter[ i ].Parametername ,
                                                           aService.ServiceinParameter[ i ].Parametertype ,
                                                           SUPERV::DataStreamParameter ,
                                                           - 1 ,
                                                           _Graph_prof_debug , _Graph_fdebug ) ;
      anInPort->Dependency( InDataStreamDependency[ i ] ) ;
// Attention : revoir les reversed DataStreamPorts
//      anInPort->SetParams( InKindOfSchema[ i ] ,
//                           InKindOfInterpolation[ i ] ,
//                           InKindOfExtrapolation[ i ] ) ;
      cdebug << "InStreamPort " << GetChangeNodeInPort( i )->PortName() << " " << GetChangeNodeInPort( i )->Kind() << endl ;
      cdebug << "ReversedInStreamPort " << _DataFlowDataPorts->GetChangeNodeOutPort( i )->PortName() << " " << _DataFlowDataPorts->GetChangeNodeOutPort( i )->Kind()
             << endl ;
    }
    else {
      GetChangeNodeInPort( i )->Kind( InPortsKind[ i ] ) ;
      _DataFlowDataPorts->GetChangeNodeOutPort( i )->Kind( InPortsKind[ i ] ) ;
      GetChangeNodeInPort( i )->Dependency( InDataStreamDependency[ i ] ) ;
      _DataFlowDataPorts->GetChangeNodeOutPort( i )->Dependency( InDataStreamDependency[ i ] ) ;
      cdebug << "InPort " << GetChangeNodeInPort( i )->PortName() << " "
             << GetChangeNodeInPort( i )->Kind() << " " << GetChangeNodeInPort( i )->PortStatus()
             << endl ;
      cdebug << "ReversedInPort " << _DataFlowDataPorts->GetChangeNodeOutPort( i )->PortName() << " "
             << _DataFlowDataPorts->GetChangeNodeOutPort( i )->Kind() << " "
             << _DataFlowDataPorts->GetChangeNodeOutPort( i )->PortStatus()
             << endl ;
    }
  }
  for ( i = 0 ; i < outnbr ; i++ ) {
    if ( OutPortsKind[ i ] == SUPERV::DataStreamParameter ) {
      GraphBase::OutDataStreamPort * anOutPort = AddOutDataStreamPort( aService.ServiceoutParameter[ i ].Parametername ,
                                                                       aService.ServiceoutParameter[ i ].Parametertype ,
                                                                       OutDataStreamDependency[ i ] ,
                                                                       SUPERV::DataStreamParameter ) ;
      anOutPort->NumberOfValues( OutNumberOfValues[ i ] ) ;
// NOT A BUG : AddInPort for an outport (Reversed service)
      anOutPort = (GraphBase::OutDataStreamPort * ) _DataFlowDataPorts->AddInPort(
                                                             _Orb , NamePtr() ,
                                                             Kind() ,
                                                             aService.ServiceoutParameter[ i ].Parametername ,
                                                             aService.ServiceoutParameter[ i ].Parametertype ,
                                                             SUPERV::DataStreamParameter ,
                                                             - 1 ,
                                                             _Graph_prof_debug , _Graph_fdebug ) ;
      anOutPort->Dependency( OutDataStreamDependency[ i ] ) ;
// Attention : revoir les reversed DataStreamPorts
//      anOutPort->NumberOfValues( OutNumberOfValues[ i ] ) ;
      cdebug << "OutStreamPort " << GetChangeNodeOutPort( i )->PortName() << " " << GetChangeNodeOutPort( i )->Kind() << endl ;
      cdebug << "ReversedOutStreamPort " << _DataFlowDataPorts->GetChangeNodeInPort( i )->PortName() << " " << _DataFlowDataPorts->GetChangeNodeInPort( i )->Kind()
             << endl ;
    }
    else {
      GetChangeNodeOutPort( i )->Kind( OutPortsKind[ i ] ) ;
      _DataFlowDataPorts->GetChangeNodeInPort( i )->Kind( OutPortsKind[ i ] ) ;
      GetChangeNodeOutPort( i )->Dependency( OutDataStreamDependency[ i ] ) ;
      _DataFlowDataPorts->GetChangeNodeInPort( i )->Dependency( OutDataStreamDependency[ i ] ) ;
      GetChangeNodeOutPort( i )->Dependency( OutDataStreamDependency[ i ] ) ;
      _DataFlowDataPorts->GetChangeNodeInPort( i )->Dependency( OutDataStreamDependency[ i ] ) ;
      cdebug << "OutPort " << GetChangeNodeOutPort( i )->PortName() << " "
             << GetChangeNodeOutPort( i )->Kind() << " " << GetChangeNodeOutPort( i )->PortStatus()
             << endl ;
      cdebug << "ReversedOutPort " << _DataFlowDataPorts->GetChangeNodeInPort( i )->PortName()
             << " " << _DataFlowDataPorts->GetChangeNodeInPort( i )->Kind()
             << " " << _DataFlowDataPorts->GetChangeNodeInPort( i )->PortStatus()
             << endl ;
    }
  }
// Restore input datas and links :
  DataStreamInPortsNumber( 0 ) ;
  DataStreamOutPortsNumber( 0 ) ;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    aToNode = GraphNodes( i ) ;
    DataStreamInPortsNumber( DataStreamInPortsNumber() + aToNode->DataStreamInPortsNumber() ) ;
    DataStreamOutPortsNumber( DataStreamOutPortsNumber() + aToNode->DataStreamOutPortsNumber() ) ;
    for ( j = 0 ; j < aToNode->GetNodeInPortsSize() ; j++ ) {
      GraphBase::InPort *anInPort = aToNode->GetChangeNodeInPort(j) ;
      if ( !anInPort->IsGate() && anInPort->IsDataConnected() ) {
        if ( !AddInputData( anInPort->NodeName() ,
                            anInPort->PortName() ,
//JR 21.02.2005 Debug Memory leak :                            anInPort->GetOutPort()->ValuePtr() ) ) {
//JR 30.03.2005                            *anInPort->GetOutPort()->Value() ) ) {
                            anInPort->GetOutPort()->Value() ) ) {
          cdebug << "CreateService In" << j << " " << anInPort->NodeName() << "(" << anInPort->PortName() << ") "
                 << anInPort->PortStatus() << " --> AddInputData ERROR" << endl ;
          RetVal = false ;
	}
        else {
          cdebug << "CreateService In" << j << " " << anInPort->NodeName() << "(" << anInPort->PortName() << ") "
                 << anInPort->PortStatus() << " --> AddInputData" << endl ;
	}
      }
//JR PAL8914 : InPorts of EndLoopNodes may not be in the Service of the Graph
//      else if ( !anInPort->IsGate() && anInPort->IsNotConnected() ) {
      else if ( !anInPort->IsGate() &&
                ( anInPort->IsNotConnected() || anInPort->IsExternConnected() ) &&
                !aToNode->IsEndLoopNode() ) {
        if ( !AddLink( this , GetChangeInDataNodePort( anInPort->NodePortName() ) , aToNode , anInPort ) ) {
          cdebug << "CreateService In" << j << " " << anInPort->NodeName() << "(" << anInPort->PortName() << ") "
                 << anInPort->PortStatus() << " --> AddInLink ERROR" << endl ;
          RetVal = false ;
	}
        else {
          cdebug << "CreateService In" << j << " " << anInPort->NodeName() << "(" << anInPort->PortName() << ") "
                 << anInPort->PortStatus() << " --> AddInLink" << endl ;
	}
      }
      else {
        cdebug << "CreateService In" << j << " " << anInPort->NodeName() << "(" << anInPort->PortName() << ") "
               << anInPort->PortStatus() << " --> NO AddInputData/AddInLink" << endl ;
      }
    }
    for ( j = 0 ; j < aToNode->GetNodeOutPortsSize() ; j++ ) {
      GraphBase::OutPort *anOutPort = aToNode->GetChangeNodeOutPort(j) ;
      cdebug << "CreateService Out" << j << " " << anOutPort->NodeName() << "(" << anOutPort->PortName()
             << ") " << anOutPort->PortStatus() << " :" << endl ;
//JR PAL8914 : OutPorts of LoopNodes may not be in the Service of the Graph
//      if ( !anOutPort->IsGate() && anOutPort->IsNotConnected() && !aToNode->IsGOTONode() ) {
      if ( !anOutPort->IsGate() && !aToNode->IsGOTONode() && !aToNode->IsLoopNode() &&
//JR Debug : 06.09.2005 : recreate that link if the OutPort is ExternConnected !!!...
           ( anOutPort->IsNotConnected() || anOutPort->IsExternConnected() ) ) {
        GraphBase::InPort *anInPort = GetChangeOutDataNodePort( anOutPort->NodePortName() ) ;
        if ( !AddLink( aToNode , anOutPort , this , anInPort ) ) {
          cdebug << "       --> " << Name() << "(" << anInPort->PortName()
                 << ") " << anInPort->PortStatus() << " AddOutLink ERROR" << endl ;
          RetVal = false ;
	}
        else {
          cdebug << "       --> " << Name() << "(" << anInPort->PortName()
                 << ") " << anInPort->PortStatus() << " AddOutLink" << endl ;
	}
      }
    }
  }

#ifdef _DEBUG_
  ListPorts( *_Graph_fdebug , 0 ) ;
#endif

  if ( !RetVal ) {
    SetMessages( "Fail to create the Service of the graph.\n" ) ;
  }

  cdebug_out << "GraphBase::Graph::CreateService " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::InLineServices() {
  cdebug_in << "GraphBase::Graph::InLineServices" << endl;

  int i , j ;

#ifdef _DEBUG_
  ListPorts( *_Graph_fdebug , 0 ) ;
#endif

  GraphBase::InLineNode * aINode ;
  SALOME_ModuleCatalog::Service aService ;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    aINode = (GraphBase::InLineNode * ) GraphNodes( i ) ;
    if ( aINode->IsOneOfInLineNodes() ) {
//      cdebug << "GraphBase::Graph::InLineServices " << aINode->Name() << endl;
      aService.ServiceName = aINode->ServiceName() ;
      aService.ServiceinParameter.length( 0 ) ;
      aService.ServiceoutParameter.length( 0 ) ;
      aINode->DataStreamInPortsNumber( 0 ) ;
      aINode->DataStreamOutPortsNumber( 0 ) ;
      int InService = 0 ;
      int InStreamService = 0 ;
      for ( j = 0 ; j < aINode->GetNodeInPortsSize() ; j++ ) {
        if ( aINode->GetChangeNodeInPort(j)->IsGate() || aINode->GetChangeNodeInPort(j)->IsLoop() ) {
#if 0
          cdebug << "GraphBase::Graph::InLineServices In" << j << " "
                 << aINode->GetChangeNodeInPort(j)->Kind() << " " 
                 << aINode->GetChangeNodeInPort(j)->PortType() << " " 
                 << aINode->GetChangeNodeInPort(j)->PortName() << " ignored "
                 << aINode->GetChangeNodeInPort(j)->IsGate() << " "
                 << aINode->GetChangeNodeInPort(j)->IsLoop() << endl;
#endif
	}
        else if ( aINode->GetChangeNodeInPort(j)->IsDataStream() ) {
          aService.ServiceinDataStreamParameter.length( InStreamService+1 ) ;
          aService.ServiceinDataStreamParameter[InStreamService].Parametertype = CORBA::string_dup( aINode->GetChangeNodeInPort(j)->PortType() ) ;
          aService.ServiceinDataStreamParameter[InStreamService].Parametername = CORBA::string_dup( aINode->GetChangeNodeInPort(j)->PortName() ) ;
          aService.ServiceinDataStreamParameter[InStreamService++].Parameterdependency = aINode->GetChangeNodeInPort(j)->Dependency() ;
          aINode->IncrDataStreamInPorts() ;
#if 0
          cdebug << "GraphBase::Graph::InLineServices In" << j << " "
                 << aINode->GetChangeNodeInPort(j)->Kind() << " " 
                 << aINode->GetChangeNodeInPort(j)->PortType() << " " 
                 << aINode->GetChangeNodeInPort(j)->PortName() << " "
                 << aINode->GetChangeNodeInPort(j)->IsGate() << " "
                 << aINode->GetChangeNodeInPort(j)->IsLoop() << endl;
#endif
	}
        else {
          aService.ServiceinParameter.length( InService+1 ) ;
          aService.ServiceinParameter[InService].Parametertype = CORBA::string_dup( aINode->GetChangeNodeInPort(j)->PortType() ) ;
          aService.ServiceinParameter[InService++].Parametername = CORBA::string_dup( aINode->GetChangeNodeInPort(j)->PortName() ) ;
#if 0
          cdebug << "GraphBase::Graph::InLineServices In" << j << " "
                 << aINode->GetChangeNodeInPort(j)->Kind() << " " 
                 << aINode->GetChangeNodeInPort(j)->PortType() << " " 
                 << aINode->GetChangeNodeInPort(j)->PortName() << " "
                 << aINode->GetChangeNodeInPort(j)->IsGate() << " "
                 << aINode->GetChangeNodeInPort(j)->IsLoop() << endl;
#endif
	}
      }
      int OutService = 0 ;
      int OutStreamService = 0 ;
      for ( j = 0 ; j < aINode->GetNodeOutPortsSize() ; j++ ) {
//        if ( aINode->GetChangeNodeOutPort(j)->IsGate() || aINode->GetChangeNodeOutPort(j)->IsLoop() || aINode->GetChangeNodeOutPort(j)->IsDataStream() ) {
        if ( aINode->GetChangeNodeOutPort(j)->IsGate() || aINode->GetChangeNodeOutPort(j)->IsLoop() ) {
#if 0
          cdebug << "GraphBase::Graph::InLineServices Out" << j << " "
                 << aINode->GetChangeNodeOutPort(j)->Kind() << " " 
                 << aINode->GetChangeNodeOutPort(j)->PortType() << " " 
                 << aINode->GetChangeNodeOutPort(j)->PortName() << " ignored "
                 << aINode->GetChangeNodeOutPort(j)->IsGate() << " "
                 << aINode->GetChangeNodeOutPort(j)->IsLoop() << endl;
#endif
	}
        else if ( aINode->GetChangeNodeOutPort(j)->IsDataStream() ) {
          aService.ServiceoutDataStreamParameter.length( OutStreamService+1 ) ;
          aService.ServiceoutDataStreamParameter[OutStreamService].Parametertype = CORBA::string_dup( aINode->GetChangeNodeOutPort(j)->PortType() ) ;
          aService.ServiceoutDataStreamParameter[OutStreamService].Parametername = CORBA::string_dup( aINode->GetChangeNodeOutPort(j)->PortName() ) ;
          aService.ServiceoutDataStreamParameter[OutStreamService++].Parameterdependency = aINode->GetChangeNodeOutPort(j)->Dependency() ;
          aINode->IncrDataStreamOutPorts() ;
#if 0
          cdebug << "GraphBase::Graph::InLineServices Out" << j << " "
                 << aINode->GetChangeNodeOutPort(j)->Kind() << " " 
                 << aINode->GetChangeNodeOutPort(j)->PortType() << " " 
                 << aINode->GetChangeNodeOutPort(j)->PortName() << " "
                 << aINode->GetChangeNodeOutPort(j)->IsGate() << " "
                 << aINode->GetChangeNodeOutPort(j)->IsLoop() << endl;
#endif
	}
        else {
          aService.ServiceoutParameter.length( OutService+1 ) ;
          aService.ServiceoutParameter[OutService].Parametertype = CORBA::string_dup( aINode->GetChangeNodeOutPort(j)->PortType() ) ;
          aService.ServiceoutParameter[OutService++].Parametername = CORBA::string_dup( aINode->GetChangeNodeOutPort(j)->PortName() ) ;
#if 0
          cdebug << "GraphBase::Graph::InLineServices Out" << j << " "
                 << aINode->GetChangeNodeOutPort(j)->Kind() << " " 
                 << aINode->GetChangeNodeOutPort(j)->PortType() << " " 
                 << aINode->GetChangeNodeOutPort(j)->PortName()  << " "
                 << aINode->GetChangeNodeOutPort(j)->IsGate() << " "
                 << aINode->GetChangeNodeOutPort(j)->IsLoop() << endl;
#endif
	}
      }
      aINode->SetService( aService ) ;
      if ( SetServiceOfMap( (GraphBase::Service * ) aINode ) ) {
#if 0
        cdebug << "InLineServices SetServiceOfMap " << aINode->ServiceName() << " in MapOfServices"
               << " InStreamPort(" << aINode->DataStreamInPortsNumber() 
               << ") OutStreamPort(" << aINode->DataStreamOutPortsNumber() << ")" << endl ; 
#endif
      }
//      cdebug << "GraphBase::Graph::InLineServices" << *aINode->GetService() << endl;
    }
  }

  cdebug_out << "GraphBase::Graph::InLineServices" << endl;
  return true ;
}

bool GraphBase::Graph::Sort( int & SubStreamGraphsNumber ) {
#ifdef _DEBUG_
  ListPorts( *_Graph_fdebug , 0 ) ;
#endif

  int i ;
  int j ;
  int NotSortedNumber = GraphNodesSize() ;
  bool NewSorted ;
  cdebug_in << "GraphBase::Graph::Sort" << endl;
  if ( NotSortedNumber ) {
    _LevelsNumber = 0 ;
    _ParallelExecutionNodes = false ;
    _Sorted.resize( GraphNodesSize() ) ;
    _CnxInPortsNumber.resize( GraphNodesSize() ) ;
    _DecrInPortsNumber.resize( GraphNodesSize() ) ;
    if ( _NodesNumber.size() ) {
      _NodesNumber.resize( 0 ) ;
      _SortedNodes.resize( 0 ) ;
    }
          
// This is a simulation of the computation of the graph :
// The initial state of nodes is :
// - Sorted = false : the node is not sorted
// - CnxInPortsNumber = ConnectedInPortsNumber : the number of ports that wait for a value from a link
// - DecrInPortsNumber = 0 : there is no value available.
#if 0
    cdebug << "GraphBase::Graph::Sort initial values :" << endl ;
    for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
      GraphBase::ComputingNode * aNode = GraphNodes( i ) ;
      cdebug << aNode->Name() << " --> " << aNode->LinkedNodesSize() << " LinkedNodes :" << endl ;
      for ( j = 0 ; j < aNode->LinkedNodesSize()  ; j++ ) {
        cdebug << "              " << aNode->LinkedNodes( j )->Name() << endl ;
      }
    }
#endif
    for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
      _Sorted[ i ] = false ;
      _CnxInPortsNumber[ i ] = GraphNodes( i )->ConnectedInPortsNumber() ;
      _DecrInPortsNumber[ i ] = 0 ;
//      cdebug << "Sort Node [" << i << "] " << GraphNodes( i )->Name() << " initial count "
//             << _CnxInPortsNumber[ i ] << endl ;
    }
    
// Nodes are linked ONLY if Ports are NOT DataStream for topological sort of node
// ==============================================================================
    while ( NotSortedNumber ) {
      NewSorted = false ;
      
      for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
        cdebug << "Sort level " << _LevelsNumber << ". [" << i << "] "
               << GraphNodes( i )->Name() << " count "
               << _CnxInPortsNumber[ i ] << endl ;
        if ( !_Sorted[ i ] && _CnxInPortsNumber[ i ] == 0 ) {
// All inputs of GraphNodes( i ) are available : "AllDataReady"
	  if ( (int ) _NodesNumber.size() != _LevelsNumber+1 ) {
            _NodesNumber.resize( _LevelsNumber+1 ) ;
            _SortedNodes.resize( _LevelsNumber+1 ) ;
            _NodesNumber[ _LevelsNumber ] = -1 ;
	  }
// There is one more node at that level
	  _NodesNumber[ _LevelsNumber ]++ ;
          (_SortedNodes[ _LevelsNumber ]).resize( _NodesNumber[ _LevelsNumber ]+1 ) ;
          (_SortedNodes[ _LevelsNumber ])[ _NodesNumber[ _LevelsNumber ] ] = GraphNodes( i ) ;
          _Sorted[ i ] = true ;
          NewSorted = true ;
          NotSortedNumber -= 1 ;
          cdebug << GraphNodes( i )->Name() << " belongs to level "
                 << _LevelsNumber << ". " << GraphNodes( i )->LinkedNodesSize() << " linkednodes" << endl ;
// GraphNodes( i ) has the state "AllDataReady". We simulate the end of its execution :
// So we loop over all nodes that have a link from that node
// And we increment the number of input ports of GraphNodes( i )->LinkedNodes( j ) that this
// execution will give a value in DecrInPortsNumber : "SomeDataReady"
	  for ( j = 0 ; j < GraphNodes( i )->LinkedNodesSize() ; j++ ) {
// OutputPorts of GraphNodes( i ) will satisfy 
//   GraphNodes( i )->LinkedInPortsNumber( j ) InPorts of
//      GraphNodes( i )->LinkedNodes( j )
	    GraphBase::StreamNode * aLinkedNode = GraphNodes( i )->LinkedNodes( j ) ;
            int aLinkedInPortsNumber = GraphNodes( i )->LinkedInPortsNumber( j ) ;
            cdebug << j << ". LinkedNode " << aLinkedNode->Name() ;
            if ( !aLinkedNode->IsDataFlowNode() && !aLinkedNode->IsDataStreamNode() ) {
              cdebug << " _DecrInPortsNumber[ "
                     << GetGraphNodeIndex( aLinkedNode->Name() )
                     << " ] = "
                     << _DecrInPortsNumber[ GetGraphNodeIndex( aLinkedNode->Name() ) ]
                     << " += " << aLinkedInPortsNumber ;
              _DecrInPortsNumber[ GetGraphNodeIndex( aLinkedNode->Name() ) ] += aLinkedInPortsNumber ;
	    }
            cdebug << endl ;
	  }
        }
      }
// If no node was found, that FlowGraph is not valid : if we try to execute it, that FlowGraph
// will wait for data(s) from node(s) not executed or not executing for ever
      if ( !NewSorted ) {
        SetMessages( "That graph is cyclic.\n" ) ;
        cdebug << "ERROR Loop detected at level " << _LevelsNumber << endl ;
	return false ; // Loop in the Graph
      }
//      cdebug << "Bilan level " << _LevelsNumber << " : " << endl ;
      bool ChangeCount = false ;
// We update now the number of InPorts with a link that are waiting for a value
      for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
        if ( _DecrInPortsNumber[ i ] ) {
//          int prevcount = _CnxInPortsNumber[ i ] ;
          _CnxInPortsNumber[ i ] -= _DecrInPortsNumber[ i ]  ;
          _DecrInPortsNumber[ i ] = 0 ;
          ChangeCount = true ;
//          cdebug << "    " << GraphNodes( i )->Name() << " count " << prevcount
//                 << " --> new count " << _CnxInPortsNumber[ i ] << endl ;
        }
      }
// If there is no loop and if the number of InPorts with a link that are waiting for a value
// does not change, the sort is finished. But there is also NotSortedNumber ...
//      if ( !ChangeCount )
//        break ;
      if ( NotSortedNumber ) {
        _LevelsNumber += 1 ;
      }
    }
    _ThreadsMax = 0 ;
    int AllSortedNodes = 0 ;
    _HeadNodes = _SortedNodes[ 0 ] ;
    _HeadNodesSize = _SortedNodes[ 0 ].size() ;
// QueueNodes was used in the past in order to know if the execution of a graph is finished
// But because of loop nodes that was changed. So that part of code is a "clutter"
// Now a graph has finished its execution when the number of executing threads is zero
    _QueueNodes = _SortedNodes[ _LevelsNumber ] ;
    _QueueNodesSize = _SortedNodes[ _LevelsNumber ].size() ;
    for ( i = 0 ; i < _QueueNodesSize ; i++ ) {
      if ( !_QueueNodes[ i ]->IsGOTONode() ) {
        break ;
      }
    }
    if ( i == _QueueNodesSize && _LevelsNumber ) {
      _QueueNodes = _SortedNodes[ _LevelsNumber - 1 ] ;
      _QueueNodesSize = _SortedNodes[ _LevelsNumber - 1 ].size() ;
    }
// Computation of the maximum number of threads == the maximum number of nodes of all levels
    for ( i = 0 ; i <= _LevelsNumber ; i++ ) {
      AllSortedNodes += _NodesNumber[ i ]+1 ;
      if ( _NodesNumber[ i ] + 1 > _ThreadsMax ) {
        _ThreadsMax = _NodesNumber[ i ] + 1 ;
      }
      cdebug << _NodesNumber[ i ]+1 << " Nodes of level " << i << " : "
             << (_SortedNodes[ i ])[ 0 ]->Name() << endl ;
      for ( j = 1 ; j <= _NodesNumber[ i ] ; j++ ) {
        _ParallelExecutionNodes = true ;
        cdebug << "                     " << (_SortedNodes[ i ])[ j ]->Name()
               << endl ;
      }
    }
    if ( _ParallelExecutionNodes ) {
//      cdebug << "Parallel Execution Nodes detected." << endl ;
    }
    else {
//      cdebug << "No Parallel Execution Nodes detected." << endl ;
    }
    if ( AllSortedNodes != GraphNodesSize() ) {
      SetMessages( "That graph is cyclic.\n" ) ;
      cdebug << "ERROR Loop detected at level " << _LevelsNumber << endl ;
      return false ; // Loop in the Graph
    }

    cdebug << "Graph::Sort " << _NodesNumber[ 0 ] << " HeadNodes :" << endl ;
    for ( i= 0 ; i <= _NodesNumber[ 0 ] ; i++ ) {
      (_SortedNodes[ 0 ])[ i ]->HeadNode( true ) ;
      cdebug << "            " << i << ". : " << (_SortedNodes[ 0 ])[ i ]->Name() << endl ;
    }

// Give a subgraph/substreamgraph number > 0 to HeadNodes (level 0)
// There is no substreamgraph for nodes without StreamPorts
    for ( j = 0 ; j <= _NodesNumber[ 0 ] ; j++ ) {
      (_SortedNodes[ 0 ])[ j ]->Level( 0 ) ;
      (_SortedNodes[ 0 ])[ j ]->SortedIndex( j ) ;
      (_SortedNodes[ 0 ])[ j ]->SubGraph( j + 1 ) ;
      if ( (_SortedNodes[ 0 ])[ j ]->HasDataStream() ) {
        (_SortedNodes[ 0 ])[ j ]->SubStreamGraph( j + 1 ) ;
      }
      else {
        (_SortedNodes[ 0 ])[ j ]->SubStreamGraph( -1 ) ;
      }
    }

// Give a subgraph/substreamgraph number = 0 to all other nodes :
// the SubGraph of that nodes is unknown
// There is no substreamgraph for nodes without StreamPorts
    for ( i = 1 ; i <= _LevelsNumber ; i++ ) {
      for ( j = 0 ; j <= _NodesNumber[ i ] ; j++ ) {
        (_SortedNodes[ i ])[ j ]->Level( i ) ;
        (_SortedNodes[ i ])[ j ]->SortedIndex( j ) ;
        (_SortedNodes[ i ])[ j ]->SubGraph( 0 ) ;
        if ( (_SortedNodes[ i ])[ j ]->HasDataStream() ) {
          (_SortedNodes[ i ])[ j ]->SubStreamGraph( 0 ) ;
        }
        else {
          (_SortedNodes[ i ])[ j ]->SubStreamGraph( -1 ) ;
        }
      }
    }

// Computation of independent SubGraphs which have NO link between them
    cdebug << endl << "Computation of SubGraphs" << endl ;
    bool Graphs = true ;
    while ( Graphs ) {
      for ( i = 0 ; i <= _LevelsNumber ; i++ ) {
        cdebug << endl << "LevelsNumber " << _LevelsNumber << " " << _NodesNumber[ i ]
               << " Nodes :" << endl ;
        for ( j = 0 ; j <= _NodesNumber[ i ] ; j++ ) {
          GraphBase::ComputingNode * aNode = (_SortedNodes[ i ])[ j ] ;
          cdebug << "    " << aNode->Name() << " SubGraph " << aNode->SubGraph() << endl ;
          int k ;
          for ( k = 0 ; k < aNode->LinkedNodesSize() ; k++ ) {
	  if ( aNode->LinkedNodes( k )->SubGraph() ) {
              if ( aNode->LinkedNodes( k )->SubGraph() != aNode->SubGraph() ) {
//                   && aNode->LinkedNodes( k )->Level() == aNode->Level()+1 ) {
//	      aNode->SubGraph( aNode->LinkedNodes( k )->SubGraph() ) ;
                int OldSub = aNode->SubGraph() ;
                int NewSub = aNode->LinkedNodes( k )->SubGraph() ;
	      cdebug << "        " << aNode->Name() << " changed SubGraph "
                       << aNode->LinkedNodes( k )->SubGraph() << endl ;
                int l ;
                for ( l = 0 ; l <= _LevelsNumber ; l++ ) {
                  int m ;
                  for ( m = 0 ; m <= _NodesNumber[ l ] ; m++ ) {
                    if ( (_SortedNodes[ l ])[ m ]->SubGraph() == OldSub ) {
                      GraphBase::ComputingNode * aSubNode = (_SortedNodes[ l ])[ m ] ;
                      aSubNode->SubGraph( NewSub ) ;
		}
	        }
	      }
                Graphs = false ;
                break ;
	    }
              else {
                cdebug << "        Linked " << aNode->LinkedNodes( k )->Name()
                       << " same SubGraph " << aNode->LinkedNodes( k )->SubGraph() << endl ;
	    }
	  }
            else {
              cdebug << "        LinkedNode " << aNode->LinkedNodes( k )->Name()
                     << " initial SubGraph " << aNode->SubGraph() << endl ;
              aNode->LinkedNodes( k )->SubGraph( aNode->SubGraph() ) ;
	  }
	}
          if ( !Graphs ) {
            break ;
          }
        }
        if ( !Graphs ) {
          break ;
        }
      }
      if ( Graphs ) {
        cdebug << endl << "SubGraphs result : " << endl ;
        break ;
      }
      cdebug << endl << "One more time" << endl ;
      Graphs = true ;
    }
// Make a sequential renumbering of SubGraphs :
    _SubGraphsNumber = 0 ;
    int CurrGraphsNumber = 0;
    int k ;
    for ( k = 0 ; k <= _NodesNumber[ 0 ] ; k++ ) {
      if ( (_SortedNodes[ 0 ])[ k ]->SubGraph() > CurrGraphsNumber ) {
        CurrGraphsNumber = (_SortedNodes[ 0 ])[ k ]->SubGraph() ;
        _SubGraphsNumber += 1 ;
        for ( i = 0 ; i <= _LevelsNumber ; i++ ) {
          for ( j = 0 ; j <= _NodesNumber[ i ] ; j++ ) {
            if ( (_SortedNodes[ i ])[ j ]->SubGraph() == CurrGraphsNumber ) {
              (_SortedNodes[ i ])[ j ]->SubGraph( _SubGraphsNumber ) ;
              cdebug << "SubGraphsNumber " << _SubGraphsNumber << " " << " Level "
                     << i << " : " << (_SortedNodes[ i ])[ j ]->Name()
                     << endl ;
	    }
          }
        }
      }
    }

//    cdebug << endl << "Computation of SubStreamGraphs" << endl ;
    for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
      GraphBase::ComputingNode * aNode = GraphNodes( i ) ;
      if ( aNode->SubStreamGraph() < 0 ) {
//	cdebug << "Graph " << aNode->Name() << " has NO streamport" << endl ;
      }
      else {
//        cdebug << aNode->Name() << " SubGraph " << aNode->SubGraph() << " --> "
//               << aNode->LinkedStreamNodesSize() << " LinkedStreamNodes :" << endl ;
        for ( j = 0 ; j < aNode->LinkedStreamNodesSize()  ; j++ ) {
//          cdebug << "              " << aNode->LinkedStreamNodes( j )->Name() << endl ;
	}
      }
    }

// Computation of independent SubStreamGraphs which have NO StreamLink between them
    SubStreamGraphsNumber = 0 ;
    int ilevel ;
    for ( ilevel = 0 ; ilevel <= _LevelsNumber ; ilevel++ ) {
// Loop for _NodesNumber[ ilevel ] nodes of the level ilevel
      int jnode ;
      for ( jnode = 0 ; jnode <= _NodesNumber[ ilevel ] ; jnode++ ) {
        GraphBase::ComputingNode * aNode = (_SortedNodes[ ilevel ])[ jnode ] ;
        if ( aNode->SubStreamGraph() < 0 ) {
//	  cdebug << "Graph " << aNode->SubStreamGraph() << " " << aNode->Name()
//                 << " has NO streamport" << endl ;
	}
        else {
          int CurStreamGraphsNumber ;
          if ( aNode->SubStreamGraph() > 0 ) {
//	    cdebug << "SubStreamGraph " << aNode->SubStreamGraph() << " " << aNode->Name()
//                   << " has streamport LinkedStreamNodesSize already in a SubStreamGraph" << endl ;
            CurStreamGraphsNumber = aNode->SubStreamGraph() ;
	  }
          else {
            CurStreamGraphsNumber = SubStreamGraphsNumber+1 ;
	  }
          int RetVal = 0 ;
          while ( RetVal != CurStreamGraphsNumber ) {
            RetVal = CurStreamGraphsNumber ;
            aNode->SetSubStreamGraph( CurStreamGraphsNumber , RetVal ) ;
            if ( RetVal != CurStreamGraphsNumber ) {
              for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
                GraphBase::ComputingNode * aNode = GraphNodes( i ) ;
                if ( aNode->SubStreamGraph() == CurStreamGraphsNumber ) {
                  aNode->SubStreamGraph( RetVal ) ;
		}
	      }
              CurStreamGraphsNumber = RetVal ;
              RetVal = 0 ;
	    }
            if ( CurStreamGraphsNumber > SubStreamGraphsNumber ) {
              SubStreamGraphsNumber = CurStreamGraphsNumber ;
	    }
	  }
	}
      }
    }
// Make a sequential renumbering of SubGraphs :
//    cdebug << endl << "Last SubStreamGraphsNumber : " << SubStreamGraphsNumber << endl ;
    int CurrStreamGraphsNumber = 0 ;
    int count = 0 ;
    for ( CurrStreamGraphsNumber = 0 ; CurrStreamGraphsNumber <= SubStreamGraphsNumber ; CurrStreamGraphsNumber++ ) {
      bool SearchCurrStreamGraphsNumber = true ;
      for ( k = 0 ; k <= _LevelsNumber && SearchCurrStreamGraphsNumber ; k++ ) {
        int l ;
        for ( l = 0 ; l <= _NodesNumber[ k ] && SearchCurrStreamGraphsNumber ; l++ ) {
          if ( (_SortedNodes[ k ])[ l ]->SubStreamGraph() == CurrStreamGraphsNumber ) {
            SearchCurrStreamGraphsNumber = false ;
            count = count + 1 ;
            if ( CurrStreamGraphsNumber != count ) {
//              cdebug << "CurrStreamGraphsNumber " << CurrStreamGraphsNumber << " count " << count
//                     << " Level " << k << " n " << l << endl ;
              for ( i = k ; i <= _LevelsNumber ; i++ ) {
                for ( j = 0 ; j <= _NodesNumber[ i ] ; j++ ) {
                  if ( (_SortedNodes[ i ])[ j ]->SubStreamGraph() == CurrStreamGraphsNumber ) {
                    (_SortedNodes[ i ])[ j ]->SubStreamGraph( count ) ;
//                    cdebug << "SubStreamGraphsNumber " << CurrStreamGraphsNumber << " --> " << count << " "
//                           << " Level " << i << " : " << (_SortedNodes[ i ])[ j ]->Name() << endl ;
	  	  }
                  else if ( (_SortedNodes[ i ])[ j ]->SubStreamGraph() > 0 ) {
//                    cdebug << "SubStreamGraphsNumber " << (_SortedNodes[ i ])[ j ]->SubStreamGraph()
//                           << " != " << CurrStreamGraphsNumber << " Level " << i << " : "
//                           << (_SortedNodes[ i ])[ j ]->Name() << endl ;
		  }
		}
	      }
	    }
          }
	}
      }
    }
    SubStreamGraphsNumber = count ;
//    cdebug << endl << "SubStreamGraphs result : " << SubStreamGraphsNumber << " SubStreamGraphs" << endl ;
//    int CurrStreamGraphsNumber ;
//    for ( CurrStreamGraphsNumber = 1 ; CurrStreamGraphsNumber <= SubStreamGraphsNumber  ; CurrStreamGraphsNumber++ ) {
//      for ( ilevel = 0 ; ilevel <= _LevelsNumber ; ilevel++ ) {
//        for ( k = 0 ; k <= _NodesNumber[ ilevel ] ; k++ ) {
//          if ( (_SortedNodes[ ilevel ])[ k ]->SubStreamGraph() == CurrStreamGraphsNumber ) {
//            cdebug << "SubStreamGraphsNumber " << CurrStreamGraphsNumber << " : "
//                   << (_SortedNodes[ ilevel ])[ k ]->Name() << endl ;
//	  }
//	}
//      }
//    }
  }

#ifdef _DEBUG_
  ListPorts( *_Graph_fdebug , 0 ) ;
#endif

  cdebug_out << "GraphBase::Graph::Sort" << endl;
  return true ;
}

// mkr : PAL12575
bool GraphBase::Graph::ValidGOTO() {
  cdebug_in << "GraphBase::Graph::ValidGOTO" << endl;
  bool RetVal = true ;
  int i, j ;
  GraphBase::ComputingNode * aNode ;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    aNode = GraphNodes( i ) ;
    if ( aNode->IsGOTONode() ) {
      GraphBase::GOTONode * aGoToNode = (GraphBase::GOTONode * ) aNode ;
      GraphBase::InLineNode * aCoupledNode ;
      aCoupledNode = (GraphBase::InLineNode * ) aGoToNode->CoupledNode() ;
      if ( aCoupledNode ) {
	ostringstream ErrorMessage ;
	// 1. Check the number of ports (the number of OutPorts of a GOTONode and of InPorts of its linked
	//    InLine node must be the same)
	if ( aNode->GetNodeOutPortsSize() != aCoupledNode->GetNodeInPortsSize() ) {
	  cdebug << "GraphBase::Graph::ValidGOTO : Different ports number!" << endl ;
	  ReSetMessages() ;
	  ErrorMessage << "Different ports number : the number of OutPorts of a " << aNode->Name()
		       << " node (GoTo node) and of InPorts of its linked " << aCoupledNode->Name()
		       << " node (InLine node) must be the same!\n" ;
	  SetMessages( ErrorMessage.str() ) ;
	  RetVal = false ;
	}
	// 2. Check ports names (the names of OutPorts of a GOTONode and of InPorts of its linked
	//    InLine node must be the same)
	else {
	  for ( j = 0 ; j < aNode->GetNodeOutPortsSize() ; j++ ) {
	    GraphBase::OutPort * aBusParamOutPort = aNode->GetChangeNodeOutPort( j ) ;
	    if ( !aBusParamOutPort->IsGate() ) {
	      GraphBase::InPort * aBusParamChangeInPort = NULL ;
	      aBusParamChangeInPort = aCoupledNode->GetChangeInPort( aBusParamOutPort->PortName() ) ;
	      if ( !aBusParamChangeInPort ) {
		cdebug << "GraphBase::Graph::ValidGOTO : Different names of ports!" << endl ;
		ReSetMessages() ;
		ErrorMessage << "Different names of ports : there is no input port of " << aCoupledNode->Name()
			     << " node (InLine node) coupled to " << aBusParamOutPort->PortName() 
			     << " output port of a " << aNode->Name()
			     << " node (GoTo node)\n" ;
		SetMessages( ErrorMessage.str() ) ;
		RetVal = false ;
	      }
	    }
	  }
	}
      }
    }
  }
  cdebug_out << "GraphBase::Graph::ValidGOTO RetVal " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::ValidLoops() const {
  cdebug_in << "GraphBase::Graph::ValidLoops" << endl;
  bool RetVal = true ;
  int i ;
  GraphBase::ComputingNode * aNode ;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    aNode = GraphNodes( i ) ;
    if ( aNode->IsLoopNode() ) {
      GraphBase::LoopNode * aLoopNode = (GraphBase::LoopNode * ) aNode ;
      GraphBase::EndOfLoopNode * anEndLoopNode ;
      anEndLoopNode = (GraphBase::EndOfLoopNode * ) aLoopNode->CoupledNode() ;
      string anErrorMessage = string( "CheckLoop " ) + string( aLoopNode->Name() ) +
                              string( " --> " ) + string( anEndLoopNode->Name() ) +
                              string( " :\n" ) ;
      if ( !aLoopNode->CheckLoop( aLoopNode , anEndLoopNode , anErrorMessage ) ) {
        ((GraphBase::Graph * ) this)->SetMessages( anErrorMessage ) ;
        cdebug << "GraphBase::Graph::ValidLoops false aLoopNode " << aLoopNode->Name()
               << endl;
        RetVal = false ;
      }
      anErrorMessage = string( "CheckEndLoop " ) + string( aLoopNode->Name() ) +
                       string( " --> " ) + string( anEndLoopNode->Name() ) + string( " :\n" ) ;
      if ( !anEndLoopNode->CheckEndLoop( aLoopNode , anEndLoopNode , anErrorMessage ) ) {
        ((GraphBase::Graph * ) this)->SetMessages( anErrorMessage ) ;
        cdebug << "GraphBase::Graph::ValidLoops false anEndLoopNode " << anEndLoopNode->Name()
               << endl;
        RetVal = false ;
      }
    }
  }
  cdebug_out << "GraphBase::Graph::ValidLoops RetVal " << RetVal << endl;
  return RetVal ;
}

bool GraphBase::Graph::ValidSwitchs() const {
  cdebug_in << "GraphBase::Graph::ValidSwitchs" << endl;
  bool RetVal = true ;
  int i ;
  GraphBase::ComputingNode * aNode ;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    aNode = GraphNodes( i ) ;
    if ( aNode->IsSwitchNode() ) {
      GraphBase::SwitchNode * aSwitchNode = (GraphBase::SwitchNode * ) aNode ;
      if ( !aSwitchNode->CheckSwitch() ) {
        cdebug << "GraphBase::Graph::ValidSwitchs false aSwitchNode " << aSwitchNode->Name()
               << endl;
        RetVal = false ;
      }
    }
  }
  cdebug_out << "GraphBase::Graph::ValidSwitchs RetVal " << RetVal << endl;
  return RetVal ;
}

// Controle de la coherence des noeuds de calcul
// Enregistrement dans la liste des noeuds "tete" de graphe

bool GraphBase::Graph::ComputingNodes() const {
  bool RetVal = true ;
  cdebug << "GraphExecutor::Graph::ComputingNodes ()" << endl;

//  list<GraphEditor::Node *> aComputingNodesList = _G->ComputingNodesList() ;
//  list<GraphEditor::Node *>::iterator aNode = aComputingNodesList.begin();
//  GraphEditor::Port *  iP;

//  for (aNode = _G->ComputingNodesList().begin(); aNode !=_G->ComputingNodesList().end(); aNode++) {
//  while ( aNode != aComputingNodesList.end() ) {
  int i ;
  GraphBase::ComputingNode * aNode ;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    aNode = GraphNodes( i ) ;
    cdebug << "Graph::ComputingNodes " << i << ". " << aNode->Name() << " Head " << aNode->IsHeadNode()
           << endl ;
    // Tous les ports d'entree de tous les noeuds de calcul sont bien connectes
    // Enregistrement eventuel dans la liste des noeuds "tete" de graphe
//    if ((*aNode)->GetNodeInPortsSize() == 0)
//      _headNodes.push_front(*aNode);
    int j ;
    for ( j = 1 ; j < aNode->GetNodeInPortsSize() ; j++ ) {
      const GraphBase::InPort *iP = aNode->GetNodeInPort(j);
      if ( iP->IsNotConnected() ){
//	cdebug << "Le port d'entree " << iP->PortName()
//               << " du noeud de calcul " << aNode->Name() << " du graphe  "
//               << Name() << " n'est pas connecte "<< endl;
//        string anErrorMessage = string( "That graph have InPort not connected " ) +
//                                string( iP->NodeName() ) +
//                                string( "( " ) + string( iP->PortName() ) + string( " )\n" ) ;
//        ((GraphBase::Graph * ) this)->SetMessages( anErrorMessage ) ;
        RetVal = false ;
      };

    };

    // Tous les noeuds de calcul ont au moins un port de sortie connecte
    bool oneOutPortConnected = false;
    for ( j = 1 ; j < aNode->GetNodeOutPortsSize() ; j++) {
      const GraphBase::OutPort *iP = aNode->GetNodeOutPort(j);
      if ( iP->IsPortConnected() || iP->IsDataConnected() ){
	oneOutPortConnected = true ;
//	break;
      }
      else {
//	cdebug << "Le port de sortie " << iP->PortName()
//               << " du noeud de calcul " << aNode->Name() << " du graphe  "
//               << Name() << " n'est pas connecte "<< endl;
//        string anErrorMessage = string( "That graph have OutPort not connected " ) +
//                                string( iP->NodeName() ) +
//                                string( "( " ) + string( iP->PortName() ) + string( " )\n" ) ;
//        ((GraphBase::Graph * ) this)->SetMessages( anErrorMessage ) ;
        RetVal = false ;
      }
    };
    if (!oneOutPortConnected) {
//	cdebug << "Le noeud de calcul " << aNode->Name() << " du graphe  "
//               << Name() << " n'a aucun port de sortie connecte "<< endl;
//	_computingError.push_front(2);
    }
  };
//  return _computingError.size () ;
  return RetVal ;
}

bool GraphBase::Graph::LinkLoopNodes(bool & NewLink ) {
  bool RetVal = true ;
  NewLink = false ;
  int i , j ;
  cdebug_in << "GraphBase::Graph::LinkLoopNodes()" << endl;
  for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
    GraphBase::ComputingNode * aNode = GraphNodes( i ) ;
    if ( aNode->IsLoopNode() ) {
      GraphBase::LoopNode * aLoopNode = (GraphBase::LoopNode * ) aNode ;
      for ( j = 0 ; j < aLoopNode->GetNodeInPortsSize() ; j++ ) {
        GraphBase::InPort *anInPort = aLoopNode->GetChangeNodeInPort(j) ;
        cdebug << "Graph::LinkLoopNodes " << aLoopNode->Name() << "( " << anInPort->PortName()
               << " ) InPort " << anInPort->PortStatus() << endl ;
        if ( !anInPort->IsGate() && anInPort->IsNotConnected() ) {
          if ( !AddLink( aLoopNode->CoupledNode() , aLoopNode->CoupledNode()->GetChangeNodeOutPort( j ) ,
                         aLoopNode , anInPort ) ) {
            cdebug << "Graph::LinkLoopNodes AddLink ERROR "
                   << aLoopNode->CoupledNode()->Name()
                   << "( " << aLoopNode->CoupledNode()->GetChangeNodeOutPort( j )->PortName()
                   << " ) --> " << aLoopNode->Name() << "( " << anInPort->PortName() << " )"
                   << endl ;
            RetVal = false ;
	  }
          else {
            NewLink = true ;
	  }
	}
      }
      for ( j = 0 ; j < aLoopNode->CoupledNode()->GetNodeInPortsSize() ; j++ ) {
        GraphBase::EndOfLoopNode * anEndLoopNode ;
        anEndLoopNode = (GraphBase::EndOfLoopNode * ) aLoopNode->CoupledNode() ;
        GraphBase::InPort *anInPort = anEndLoopNode->GetChangeNodeInPort( j ) ;
        cdebug << "Graph::LinkLoopNodes " << anEndLoopNode->Name() << "( " << anInPort->PortName()
               << " ) InPort " << anInPort->PortStatus() << endl ;
        if ( !anInPort->IsGate() && anInPort->IsNotConnected() ) {
          if ( !AddLink( aLoopNode , aLoopNode->GetChangeNodeOutPort( j ) ,
                         anEndLoopNode , anInPort ) ) {
            cdebug << "Graph::LinkLoopNodes AddLink ERROR " << aLoopNode->Name() << "( "
                   << aLoopNode->GetChangeNodeOutPort( j )->PortName() << " ) --> "
                   << anEndLoopNode->Name() << "( " << anInPort->PortName() << " )"
                   << endl ;
            RetVal = false ;
	  }
          else {
            NewLink = true ;
	  }
	}
      }
    }
    else if ( aNode->IsGOTONode() ) {
      GraphBase::GOTONode * aGOTONode = (GraphBase::GOTONode * ) aNode ;
      GraphBase::OutPort *anOutGate = aGOTONode->GetChangeNodeOutGate() ;
      if ( anOutGate->IsNotConnected() ) {
	if ( aGOTONode->CoupledNode() ) {
          cdebug << "Graph::LinkLoopNodes aNode " << aNode << " : " << aNode->Name()
                 << aGOTONode->CoupledNode() << " : " << aGOTONode->CoupledNode()->Name()
                 << endl ;
	  RetVal = AddLink( aGOTONode , anOutGate , aGOTONode->CoupledNode() ,
                            aGOTONode->CoupledNode()->GetChangeNodeInGate() );
	  cdebug << "GraphBase::Graph::LinkLoopNodes AddLink ERROR " << aGOTONode->Name() << "( "
	         << anOutGate->PortName() << " ) --> " << aGOTONode->CoupledNode()->Name() << "( "
	         << aGOTONode->CoupledNode()->GetChangeNodeInGate()->PortName() << " )" << endl ;
	  if ( !RetVal )
	    NewLink = true;
	}
      }
    }
  }
  cdebug_out << "GraphBase::Graph::LinkLoopNodes() NewLink " << NewLink << " returns " << RetVal
             << endl;
  return RetVal ;
}

// Controle de la coherence des noeuds de calcul
// Enregistrement dans la liste des noeuds "tete" de graphe

bool GraphBase::Graph::DataServerNodes() const {

  cdebug_in << "GraphBase::Graph::DataServerNodes ()" << endl;

//  const GraphExecutor::InNode *DataFlowDatas = GetDataFlowDatas() ;
  int i , j , ierr = 0 ;

//  if ( DataFlowDatas == NULL ) {
//    MESSAGE( "No input data available in the DataFlow " << _G->Name() );
//    return 0 ;
//  }

//  const GraphExecutor::InNode *DataFlowNode = _G->GetDataFlowNode() ;
//  MESSAGE( "DataServerNodes DataFlowNode : " << *DataFlowNode );

//  MESSAGE( "DataServerNodes DataFlowDatas : " << *DataFlowDatas );

// cout are inversed because DataFlowDatas is a mirror node of the DataFlow

  ierr = CheckDataServerNodes() ;

//  list<GraphExecutor::InNode *> aComputingNodesList = _G->ComputingNodesList() ;
//  list<GraphExecutor::InNode *>::iterator aNode = aComputingNodesList.begin();
  if ( GraphNodesSize() == 0 ) {
    ierr++ ;
  }
  else {
    GraphBase::ComputingNode * aNode ;
    for ( i = 0 ; i < GraphNodesSize() ; i++ ) {
      aNode = GraphNodes( i ) ;
//  while ( aNode != aComputingNodesList.end() ) {
      if ( aNode->IsFactoryNode() ) {
        GraphBase::FactoryNode * FaNode = (GraphBase::FactoryNode * ) aNode ;
        if ( !strlen( FaNode->Computer() ) ) {
//          cdebug << "ComputerName missing for node " << FaNode->Name() ;
          if ( !strlen( FaNode->Computer() ) ) {
            ierr++ ;
            cdebug << endl ;
          }
          else {
//            cdebug << ". " << FaNode->Computer() << " will be used." << endl ;
          }
	}
    }
// For Outputs of the DataFlow :
      for ( j = 0 ; j < aNode->GetNodeOutPortsSize() ; j++ ) {
        const GraphBase::OutPort * anOutPort = aNode->GetNodeOutPort(j) ;
//JR Debug 06.06.2005 : if it is not a Gate !
//        if ( anOutPort->IsNotConnected() ) {
        if ( anOutPort->IsNotConnected() && !anOutPort->IsGate() && !aNode->IsGOTONode() ) { // mkr : PAL12575
          cdebug << "Graph::DataServerNodes Add[Stream]LinkedNode for OutPort "
                 << aNode->Name() << "( " << anOutPort->PortName() << " , "
                 << anOutPort->PortStatus() << " )" << endl ;
          if ( anOutPort->IsDataStream() ) {
            aNode->AddStreamLinkedNode( (GraphBase::ComputingNode *) this ) ;
	  }
          else {
            aNode->AddLinkedNode( (GraphBase::ComputingNode *) this ) ;
	  }
        }
      }
    }
  }

// Chaque noeud dataserver ne possede qu'un seul port entree ou sortie et il est bien connecte

//  list<GraphExecutor::InNode *> aDataServerNodesList = _G->DataServerNodesList() ;
//  list<GraphExecutor::InNode *>::iterator it = aDataServerNodesList.begin() ;

//  for (it = _G->DataServerNodesList().begin(); it !=_G->DataServerNodesList().end(); it++){
//  while ( it != aDataServerNodesList.end() ) {
//    if ( (*it)->GetNodeOutPortsSize() && (*it)->GetNodeInPortsSize() ) {
//      cerr << "Erreur dans le noeud serveur de donnees " 
//           << (*it)->Name() << " du graphe  " << _G->Name() << endl;
//      cerr << "A la fois importation et exportation de donnees" << endl;
//      _dataServerError.push_front(1);
//    }

//    if ((*it)->GetNodeOutPortsSize()) {
//      if ((*it)->GetNodeOutPortsSize()==1){
//	if ( (*it)->GetNodeOutPort(0)->IsPortConnected() ){
//	  _headNodes.push_front(*it);
//	}
//	else {
//	  cerr << "Le port de sortie du noeud serveur de donnees "
//               << (*it)->Name() << " du graphe  " << _G->Name()
//               << " n'est pas connecte "<< endl;
//	  _dataServerError.push_front(4);
//	}
//      }
//      else {
//	cerr << "Plus d'une donnee importee du noeud serveur de donnees "
//             << (*it)->Name() << " du graphe  " << _G->Name() << endl;
//	_dataServerError.push_front(2);
//      }
//    }

//    if ((*it)->GetNodeInPortsSize()) {
//      if ((*it)->GetNodeInPortsSize() > 1){
//	cerr << "Plus d'une donnee exportee vers le noeud serveur de donnees "
//             << (*it)->Name() << " du graphe  " << _G->Name() << endl;
//	_dataServerError.push_front(3);
//      }
//      else
//      if ( !(*it)->GetNodeInPort(0)->IsPortConnected() ){
//	cerr << "Le port d'entree du noeud serveur de donnees "
//             << (*it)->Name() << " du graphe  " << _G->Name()
//             << " n'est pas connecte "<< endl;
//	_dataServerError.push_front(5);
//      }
//    }
//    it++ ;
//  };

//  return _dataServerError.size ();
  cdebug_out << "GraphBase::Graph::DataServerNodes ierr " << ierr << endl;
  return ( ierr == 0 ) ;
}

SUPERV::GraphState GraphBase::Graph::PortState(
                                   const char* NodeName ,
                                   const char* ServiceParameterName ) {
  SUPERV::GraphState aRet = SUPERV::UndefinedState ;
//  cdebug << "GraphBase::Graph::PortState( " << NodeName << " , "
//         << ServiceParameterName << " )" << endl ;

  char *theNode ;
  char *aPort ;
  NodePort( NodeName , ServiceParameterName , &theNode , &aPort ) ;

  GraphBase::ComputingNode * aNode = GetChangeGraphNode( theNode ) ;
  if ( aNode ) {
    GraphBase::OutPort *anOutPort ;
    anOutPort = aNode->GetChangeOutPort( aPort );
    if ( anOutPort == NULL ) {
      GraphBase::InPort *anInPort = aNode->GetChangeInPort( aPort );
      if ( anInPort )
        anOutPort = anInPort->GetOutPort() ;
      else {
        cdebug << "ERROR GraphBase::Graph::PortState OutPort/InPort not found --> "
               << aRet << endl ;
      }
    }
    if ( anOutPort ) {
      aRet = anOutPort->PortState() ;
//      cdebug << "GraphBase::Graph::PortState --> " << aRet << endl ;
    }
  }
  else {
    cdebug << "ERROR GraphBase::Graph::PortState Node not found --> " << aRet
           << endl ;
  }
  delete theNode ;
  delete aPort ;
  return aRet ;
}

#define TracePortInOutData 0
//JR 30.03.2005const CORBA::Any * GraphBase::Graph::PortInData( const char* ToNodeName ,
const CORBA::Any GraphBase::Graph::PortInData( const char* ToNodeName ,
                                               const char* ToServiceParameterName ) {
//JR 30.03.2005  const CORBA::Any * aRet = new CORBA::Any() ;
  CORBA::Any aRet = CORBA::Any() ;
  char *theNode ;
  char *aPort ;
#if TracePortInOutData
  cdebug_in << "GraphBase::Graph::PortInData " << ToNodeName << " "
            << ToServiceParameterName << endl ;
#endif
  NodePort( ToNodeName , ToServiceParameterName , &theNode , &aPort ) ;

  GraphBase::ComputingNode * aNode = GetChangeGraphNode( theNode ) ;

  if ( aNode ) {
    GraphBase::OutPort *anOutPort = NULL ;
    GraphBase::InPort *anInPort = aNode->GetChangeInPort( aPort );
    if ( anInPort ) {
      anOutPort = anInPort->GetOutPort() ;
      if ( anOutPort ) {
#if TracePortInOutData
        cdebug << "-->PortInData " << ToNodeName << " " << ToServiceParameterName << " "
               << endl ;
#endif
        aRet = anOutPort->Value() ;
#if TracePortInOutData
        cdebug << "<--PortInData " << ToNodeName << " " << ToServiceParameterName << " " ;
        anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
        cdebug << endl ;
        cdebug << "Graph::PortInData OutPort value " << anOutPort->NodeName() << "("
               << anOutPort->PortName() << ") --> InPort " << anInPort->NodeName()
               << "(" << anInPort->PortName() << ") " << anInPort->PortStatus() << endl ;
#endif
      }
      else {
        cdebug << "Graph::PortInData GraphBase::Graph::PortInData " << ToNodeName << " "
               << ToServiceParameterName << " ERROR no OutPort" << endl ;
      }
    }
    else {
      cdebug << "GraphBase::Graph::PortInData " << ToNodeName << " "
             << ToServiceParameterName << " ERROR no InPort" << endl ;
    }
  }
  else if ( !strcmp( ToNodeName , Name() ) &&
            !strcmp( ToServiceParameterName , "Gate" ) ) {
    cdebug << "GraphBase::Graph::PortInData " << ToNodeName << " "
           << ToServiceParameterName << " InGate ignored" << endl ;
  }
  else {
    cdebug << "GraphBase::Graph::PortInData " << ToNodeName << " "
           << ToServiceParameterName << " ERROR no Node" << endl ;
  }
  delete [] theNode ;
  delete [] aPort ;
#if TracePortInOutData
  cdebug_out << "GraphBase::Graph::PortInData " << endl ;
#endif
  return aRet ;
}

//JR 30.03.2005const CORBA::Any * GraphBase::Graph::PortOutData( const char* FromNodeName ,
const CORBA::Any GraphBase::Graph::PortOutData( const char* FromNodeName ,
                                                const char* FromServiceParameterName ) {
//JR 30.03.2005  const CORBA::Any * aRet = new CORBA::Any() ;
  CORBA::Any aRet = CORBA::Any() ;
  char *theNode ;
  char *aPort ;
#if TracePortInOutData
  cdebug_in << "GraphBase::Graph::PortOutData " << FromNodeName << " "
            << FromServiceParameterName << endl ;
#endif
  NodePort( FromNodeName , FromServiceParameterName , &theNode , &aPort ) ;

  GraphBase::ComputingNode * aNode = GetChangeGraphNode( theNode ) ;

  if ( aNode ) {
    GraphBase::OutPort *anOutPort ;
    anOutPort = aNode->GetChangeOutPort( aPort );
    if ( anOutPort ) {
#if TracePortInOutData
      cdebug << "-->PortOutData " << FromNodeName << " " << FromServiceParameterName << " "
             << endl ;
#endif
      aRet = anOutPort->Value() ;
#if TracePortInOutData
      cdebug << "<--PortOutData " << FromNodeName << " " << FromServiceParameterName << " " ;
      anOutPort->StringValue( *GraphBase::Base::_fdebug ) ;
      cdebug << endl ;
#endif
    }
    else {
      cdebug << "GraphBase::Graph::PortOutData " << FromNodeName << " "
             << FromServiceParameterName << " ERROR no OutPort" << endl ;
    }
  }
  else if ( !strcmp( FromNodeName , Name() ) &&
            !strcmp( FromServiceParameterName , "Gate" ) ) {
    cdebug << "GraphBase::Graph::PortOutData " << FromNodeName << " "
           << FromServiceParameterName << " OutGate ignored" << endl ;
  }
  else {
    cdebug << "GraphBase::Graph::PortOutData " << FromNodeName << " "
           << FromServiceParameterName << " ERROR no Node" << endl ;
  }
  delete [] theNode ;
  delete [] aPort ;
#if TracePortInOutData
  cdebug_out << "GraphBase::Graph::PortOutData " << endl ;
#endif
  return aRet ;
}

SUPERV::ListOfStrings * GraphBase::Graph::LevelNodes(long aLevel ) {
  SUPERV::ListOfStrings_var RetVal = new SUPERV::ListOfStrings;
  if ( aLevel >= 0 && aLevel <= _LevelsNumber ) {
    int i ;
    RetVal->length( _NodesNumber[ aLevel ] + 1 );
    for ( i= 0 ; i <= _NodesNumber[ aLevel ] ; i++ ) {
      RetVal[i] = CORBA::string_dup( (_SortedNodes[ aLevel ])[ i ]->Name() ) ;
    }
  }
  return ( RetVal._retn() ) ;
}

bool GraphBase::Graph::StartComponent(const int aThread ,
                                      const char * aComputerContainer ,
                                      const char * ComponentName ,
                                      Engines::Container_var & myContainer ,
                                      Engines::Component_var & objComponent ) {
  cdebug_in << aThread
            << " GraphBase::Graph::StartComponent " << aComputerContainer
            << " " << ComponentName << endl;
  bool RetVal = true ;
  objComponent = Engines::Component::_nil() ;
  SALOME_LifeCycleCORBA LCC( NamingService() ) ;
  try {
    objComponent = LCC.FindOrLoad_Component( aComputerContainer ,
					     ComponentName );
  }
  catch (...) {
    RetVal = 0 ;
  }
  if ( CORBA::is_nil( objComponent ) ) {
    cdebug_out << aThread 
               << " StartComponent cannot FindOrLoad " << aComputerContainer
               << " " << ComponentName << " ERROR" << endl;
    RetVal = 0 ;
  }
  /* asv : 24.01.05 : FindContainer() method does not exist any more.  FindOrLoad_Component()
                      starts the container for us.  If it fails to start the container, then it
                      returns nil component.  We don't need to check for if the container was 
		      successfully started..
  else {
    myContainer = LCC.FindContainer( aComputerContainer );
    if ( CORBA::is_nil( myContainer ) ) {
      cdebug_out << aThread
                 << " GraphBase::Graph::StartComponent myContainer = LCC.FindContainer( '"
                 << aComputerContainer << "' ) Container does not exist ERROR"
                 << endl ;
      return 0 ;
    }
  }
  */
  if ( !RetVal ) {
    string anErrorMessage = string( "Error while FindOrLoad_Component " ) +
                            string( ComponentName ) + string( " in Container " ) +
                            string( aComputerContainer )  ;
    SetMessages( anErrorMessage ) ;
  }
  cdebug_out << aThread << " GraphBase::Graph::StartComponent RetVal " << RetVal << endl;
  return RetVal ;
}

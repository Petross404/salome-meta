//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//  File   : DataFlowBase_FactoryNode.cxx
//  Author : Jean Rahuel, CEA
//  Module : SUPERV

using namespace std;
//#include <sstream>
//#include <iostream>

#include "DataFlowBase_FactoryNode.hxx"

static void InitFields( char *              &_ComponentName ,
                        char *              &_InterfaceName ,
                        char *              &_Computer ) {
  _ComponentName = NULLSTRING ;
  _InterfaceName = NULLSTRING ;

  _Computer = FACTORYSERVER ;
}

GraphBase::FactoryNode::FactoryNode() :
  GraphBase::ComputingNode::ComputingNode() {

  InitFields( _ComponentName ,
              _InterfaceName ,
              _Computer ) ;
  _Container = Engines::Container::_nil() ;
  _ObjComponent = Engines::Component::_nil() ;
  cdebug << "GraphBase::FactoryNode::FactoryNode "  << this 
       << " _ComponentName " 
       << (void *) _ComponentName << " " << _ComponentName << " _Name "
       << (void *) Name() << " " << Name() << " _Computer "
       << (void *) _Computer << " "  << _Computer << " _Comment "
       << (void *) Comment() << " "  << Comment() << " "  << endl ;

}

GraphBase::FactoryNode::FactoryNode( CORBA::ORB_ptr ORB ,
                       SALOME_NamingService* ptrNamingService ,
                       const char * aDataFlowName ,
                       const SUPERV::KindOfNode DataFlowkind ,
                       int * Graph_prof_debug ,
                       ofstream * Graph_fdebug ) :
  GraphBase::ComputingNode::ComputingNode( ORB , ptrNamingService , aDataFlowName , DataFlowkind ,
                                           Graph_prof_debug , Graph_fdebug ) {

  InitFields( _ComponentName ,
              _InterfaceName ,
              _Computer ) ;

  _Container = Engines::Container::_nil() ;
  _ObjComponent = Engines::Component::_nil() ;

  cdebug << "GraphBase::FactoryNode::FactoryNode "  << this 
       << " _ComponentName " 
       << (void *) _ComponentName << " '" << _ComponentName
       << "' _InterfaceName "
       << (void *) _InterfaceName << " '" << _InterfaceName << "' _Name "
       << (void *) Name() << " '" << Name() << "' _Computer "
       << (void *) _Computer << " "  << _Computer << " _Comment "
       << (void *) Comment() << " "  << Comment() << " "  << endl ;
}

GraphBase::FactoryNode::FactoryNode( CORBA::ORB_ptr ORB ,
                       SALOME_NamingService * ptrNamingService ,
                       const SALOME_ModuleCatalog::Service& aService ,
                       const char * ComponentName ,
                       const char * InterfaceName ,
                       const char * NodeName ,
                       const SUPERV::KindOfNode akind ,
                       const SUPERV::SDate NodeFirstCreation ,
                       const SUPERV::SDate NodeLastModification  ,
                       const char * NodeEditorRelease ,
                       const char * NodeAuthor ,
                       const char * NodeComputer ,
                       const char * NodeComment ,
                       const bool   GeneratedName ,
                       const long   X ,
                       const long   Y ,
                       int * Graph_prof_debug ,
                       ofstream * Graph_fdebug ) :
  GraphBase::ComputingNode::ComputingNode( ORB , ptrNamingService , aService ,
                                           NodeName , akind ,
                                           NodeFirstCreation , NodeLastModification  ,
                                           NodeEditorRelease , NodeAuthor , NodeComment ,
                                           GeneratedName , X , Y ,
                                           Graph_prof_debug , Graph_fdebug ) {

  _Container = Engines::Container::_nil() ;
  _ObjComponent = Engines::Component::_nil() ;

  _ComponentName = new char[strlen(ComponentName)+1];
  strcpy(_ComponentName , ComponentName ) ;

  _InterfaceName = new char[strlen(InterfaceName)+1];
  strcpy(_InterfaceName , InterfaceName ) ;

  if ( NodeComputer != NULLSTRING) {
    _Computer = new char[ strlen( NodeComputer ) + 1 ] ;
    strcpy( _Computer  , NodeComputer ) ;
  }
  else {
    _Computer = FACTORYSERVER ;
  }

  cdebug_in << "GraphBase::FactoryNode::FactoryNode( '" << aService.ServiceName << "','"
            << _ComponentName << "','" << Name() << "'," << akind << ",'" << _Computer << "'" << endl;
  
  cdebug << "GraphBase::FactoryNode::FactoryNode "  << this 
       << " _ComponentName " 
       << (void *) _ComponentName << " '" << _ComponentName
       << "' _InterfaceName "
       << (void *) _InterfaceName << " '" << _InterfaceName << "' _Name "
       << (void *) Name() << " '" << Name() << "' _Computer "
       << (void *) _Computer << " "  << _Computer << " _Comment "
       << (void *) Comment() << " '"  << Comment() << " " 
       << "' KindOfNode " << Kind()
       << " ServiceName " << ServiceName() << " In(" << ServiceInParameter().length()
       << ") Out(" << ServiceOutParameter().length() << ")" << endl ;

  cdebug_out << "GraphBase::FactoryNode::FactoryNode" << endl;
}

GraphBase::FactoryNode::~FactoryNode() {
  cdebug << "GraphBase::Node::~Node "  << this 
       << "_ComponentName " 
       << (void *) _ComponentName << " " << _ComponentName << " _Name "
       << (void *) Name() << " " << Name() << " _Computer "
       << (void *) _Computer << " "  << _Computer << " _Comment "
       << (void *) Comment() << " "  << Comment() << " "  << endl ;
//  if ( _ComponentName != NULLSTRING )
//    delete [] _ComponentName ;
//  delete [] _Name ;
//  delete [] _EditorRelease ;
//  if ( _Author != NULLSTRING )
//    delete [] _Author ;
//  if ( _Computer != FACTORYSERVER )
//    delete [] _Computer;
//  if ( _Comment != NULLSTRING )
//    delete [] _Comment;
}

bool GraphBase::FactoryNode::ComponentName(const char * aComponentName) {
  if ( _ComponentName && _ComponentName != NULLSTRING )
    delete [] _ComponentName ;
  _ComponentName = new char[strlen(aComponentName)+1] ;
  strcpy( _ComponentName , aComponentName ) ;
  return true ;
}

bool GraphBase::FactoryNode::InterfaceName(const char * anInterfaceName) {
  if ( _InterfaceName && _InterfaceName != NULLSTRING )
    delete [] _InterfaceName ;
  _InterfaceName = new char[strlen(anInterfaceName)+1] ;
  strcpy( _InterfaceName , anInterfaceName ) ;
  return true ;
}

bool GraphBase::FactoryNode::Computer(const char *c) {
  cdebug_in << "GraphBase::FactoryNode::Computer" << endl;
  if ( _Computer != FACTORYSERVER )
    delete [] _Computer ;
  _Computer = my_strdup(c);
  cdebug << "GraphBase::FactoryNode::Computer('" << _Computer << "')" << endl;
  cdebug_out << "GraphBase::FactoryNode::Computer" << endl;
  return true ;
}

//bool GraphBase::FactoryNode::SaveXML( ostream &f , char *Tabs ,
bool GraphBase::FactoryNode::SaveXML( QDomDocument & Graph , QDomElement & info ,
                                      int XCoordinate , int YCoordinate ) const {
  GraphBase::ListOfFuncName aFuncNames ; 
  GraphBase::ListOfPythonFunctions aVoidList ;
//  return GraphBase::ComputingNode::SaveXML( f , Tabs , ComponentName() ,
  return GraphBase::ComputingNode::SaveXML( Graph , info , ComponentName() ,
                                            InterfaceName() , Computer() , "" ,
                                            aFuncNames , aVoidList ,
                                            XCoordinate , YCoordinate ) ;
}

bool GraphBase::FactoryNode::SavePY(ostream &f , const char * aGraphName ,
                                    int XCoordinate , int YCoordinate ) const {
  GraphBase::ListOfFuncName aFuncNames ; 
  GraphBase::ListOfPythonFunctions aVoidList ;
  return GraphBase::ComputingNode::SavePY( f , aGraphName , ComponentName() ,
                                           InterfaceName() , Computer() , NULL ,
                                           aFuncNames  , aVoidList ,
                                           XCoordinate , YCoordinate ) ;
}


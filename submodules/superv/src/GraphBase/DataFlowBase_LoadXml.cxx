//  SUPERV GraphBase : contains fondamental classes for Services, Input Ports, Output Ports Links and Nodes.
//
//  Copyright (C) 2003  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
//  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS 
// 
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License. 
// 
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA 
// 
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//
//
//
//  File   : DataFlowBase_LoadXml.cxx
//  Module : SUPERV

using namespace std;
#include "DataFlowBase_XmlHandler.hxx"

//#include <qaccel.h>
#include <qxml.h>
#include <qmessagebox.h>

// Pour Jean
// Fonction Load qui utilise un Parser de fichier xml de Dataflow
// Utilise DataFlowEditor_XmlHandler.hxx et .cxx

//#include "SALOME_NamingService.hxx"

//#include <SALOMEconfig.h>
//#include CORBA_CLIENT_HEADER(SALOME_SuperVision)

#include "DataFlowBase_Graph.hxx"

bool GraphBase::Graph::LoadXml( CORBA::ORB_ptr _Orb ,
                                const char* myFileName ,
                                GraphBase::ListOfSGraphs & ListOfDataFlows )
{
  char * FileName = new char[ strlen( myFileName ) + 5 ] ;
  strcpy( FileName , myFileName ) ;
  QString aqstrFileName(FileName) ;
  QFile afile( aqstrFileName) ;

  if ( !afile.exists() || !afile.open( IO_ReadOnly ) )  {
    strcat( FileName , ".xml" ) ;
    aqstrFileName = QString(FileName) ;
    QFile afile( aqstrFileName ) ;
    if ( !afile.exists() || !afile.open( IO_ReadOnly ) )  {
      return false ;
    }
    afile.close() ;
  }
  else {
    afile.close() ;
  }

  QString qstrFileName(FileName);
  QFile file( qstrFileName);
//  GraphBase::XmlHandler::XmlHandler * myXmlHandler = new GraphBase::XmlHandler::XmlHandler( _Orb , true );
  QXmlInputSource source( file );
//  QXmlSimpleReader reader;
//  reader.setContentHandler( myXmlHandler );
//  reader.setErrorHandler( myXmlHandler );

  bool RetVal = true ;
  int status = 1 ;
  GraphBase::XmlHandler::XmlHandler * myXmlHandler = new GraphBase::XmlHandler::XmlHandler( _Orb , true );
  QXmlSimpleReader reader;
  reader.setContentHandler( myXmlHandler );
  reader.setErrorHandler( myXmlHandler );
  status = reader.parse( source ) ;
  if ( status == 1 ) {
    ListOfDataFlows = myXmlHandler->ListOfDataFlows() ;
    cdebug << "Graph::LoadXml " << ListOfDataFlows.size() << " Graphs" << endl ;
  }
  else {
    cdebug << "Graph::LoadXml Graph status " << status << endl ;
    RetVal = false ;
  }
  delete myXmlHandler ;

  file.close();
//  delete myXmlHandler ;
  // Dataflow is now loaded.
  return RetVal ;
}



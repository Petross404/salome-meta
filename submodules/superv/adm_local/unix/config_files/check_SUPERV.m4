#  Check availability of SUPERV module binary distribution
#
#  Author : Marc Tajchman (CEA, 2002)
#------------------------------------------------------------

AC_DEFUN([CHECK_SUPERV],[

AC_CHECKING(for Superv)

Superv_ok=no

SUPERV_LDFLAGS=""
SUPERV_CXXFLAGS=""

AC_ARG_WITH(superv,
	    --with-superv=DIR root directory path of SUPERV installation,
	    SUPERV_DIR="$withval",SUPERV_DIR="")

if test "x$SUPERV_DIR" = "x" ; then

# no --with-gui-dir option used

  if test "x$SUPERV_ROOT_DIR" != "x" ; then

    # SUPERV_ROOT_DIR environment variable defined
    SUPERV_DIR=$SUPERV_ROOT_DIR

  else

    # search SUPERV binaries in PATH variable
    AC_PATH_PROG(TEMP, libSUPERV.so)
    if test "x$TEMP" != "x" ; then
      SUPERV_BIN_DIR=`dirname $TEMP`
      SUPERV_DIR=`dirname $SUPERV_BIN_DIR`
    fi

  fi
#
fi

if test -f ${SUPERV_DIR}/lib/salome/libSUPERV.so  ; then
  Superv_ok=yes
  AC_MSG_RESULT(Using SUPERV distribution in ${SUPERV_DIR})

  if test "x$SUPERV_ROOT_DIR" == "x" ; then
    SUPERV_ROOT_DIR=${SUPERV_DIR}
  fi
  AC_SUBST(SUPERV_ROOT_DIR)

  SUPERV_LDFLAGS=-L${SUPERV_DIR}/lib${LIB_LOCATION_SUFFIX}/salome
  SUPERV_CXXFLAGS=-I${SUPERV_DIR}/include/salome

  AC_SUBST(SUPERV_LDFLAGS)
  AC_SUBST(SUPERV_CXXFLAGS)

else
  AC_MSG_WARN("Cannot find compiled SUPERV distribution")
fi
  
AC_MSG_RESULT(for SUPERV: $Superv_ok)
 
])dnl
 
